<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * Forward To Friend module
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Cron.Executer', array());
// Plug-in hook - End

// Setup module properties - Start {
$ProcessCode			= 'cron_gif';
$ProcessRunInterval		= CRON_PHP_RUN_INTERVAL; // every 5 minutes
$ProcessLogID			= 0;
// Setup module properties - End }

// Check if it's the time for the show - Start {
if (Core::CheckProcessLog($ProcessCode, $ProcessRunInterval) == false)
	{
	// Display transparent image - Start
	Core::DisplayTransImage();
	// Display transparent image - End
	}
// Check if it's the time for the show - End }

// Register to the process log - Start {
$ProcessLogID = Core::RegisterToProcessLog($ProcessCode);
// Register to the process log - End }

// Execute each cron via HTTP request - Start {
$ArrayCronsToExecute = array('pop3_bounce', 'pop3_fbl', 'pop3_requests', 'web_general', 'web_send', 'web_sync', 'web_transactional_send');

foreach ($ArrayCronsToExecute as $EachCron)
	{
	$Return = Core::DataPostToRemoteURL(APP_URL.'cli/'.$EachCron.'.php', array(), 'GET', false, '', '', 60, false);
	}
// Execute each cron via HTTP request - End }

Core::RegisterToProcessLog($ProcessCode, 'Completed', 'All cron jobs have been executed', $ProcessLogID);

// Display transparent image - Start
Core::DisplayTransImage();
// Display transparent image - End

exit;
?>