<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * Report Abuse module
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('users');
Core::LoadObject('campaigns');
Core::LoadObject('statistics');
Core::LoadObject('form');
// Load other modules - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'report_abuse');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Retrieve information - Start
try
	{
	// Retrieve and parse message information - Start
	$MessageID = ($_GET['mid'] == '' ? ($_POST['FormValue_MessageID'] == '' ? '' : $_POST['FormValue_MessageID']) : $_GET['mid']);
		$_POST['FormValue_MessageID'] = $MessageID;

	list($CampaignID, $SubscriberID, $EmailAddress, $ListID, $UserID, $AutoResponderID) = O_Email_Helper::decodeAbuseXMessageID($MessageID);
	// Retrieve and parse message information - End

	// Retrieve campaign information - Start
	$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));
	if ($ArrayCampaign == false)
		{
		throw new Exception('Campaign not found');
		}
	// Retrieve campaign information - End

	// Retrieve list information - Start
	$ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ListID), false);
	if ($ArrayList == false)
		{
		throw new Exception('Subscriber list not found');
		}
	// Retrieve list information - End

	// Retrieve subscriber information - Start
	$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
	if ($ArraySubscriber == false)
		{
		throw new Exception('Subscriber not found');
		}
	// Retrieve subscriber information - End		

	// Retrieve user information - Start
	$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID));
	if ($ArrayUser == false)
		{
		throw new Exception('User not found');
		}
	// Retrieve user information - End
	}
catch (Exception $e)
	{
	if ($e->GetMessage() != '')
		{
		print($e->GetMessage());
		exit;
		}
	}
// Retrieve information - End

// Page parsing - Start
$ObjectTemplate 					= new TemplateEngine();

// EVENT: Send abuse report message to the system administrator - Start
if ($_POST['FormButton_Submit'] != '')
	{
	$ObjectTemplate->ArrayErrorMessages = array();

	try
		{
		// Check for required fields - Start
		$ArrayRequiredFields = array(
									"MessageID"			=>		$ArrayLanguageStrings['Screen']['1567'],
									"Message"			=>		$ArrayLanguageStrings['Screen']['1568'],
									"Name"				=>		$ArrayLanguageStrings['Screen']['1569'],
									"Email"				=>		$ArrayLanguageStrings['Screen']['1570'],
									);
		$ObjectTemplate->ArrayErrorMessages = $ObjectTemplate->CheckRequiredFields($ArrayRequiredFields, $ArrayErrorMessages, $_POST, 'FormValue_');

		if (count($ObjectTemplate->ArrayErrorMessages) > 0)
			{
			throw new Exception($ArrayLanguageStrings['Screen']['1571']);
			}
		// Check for required fields - End

		// Email address validations - Start
		if (Subscribers::ValidateEmailAddress($_POST['FormValue_Email']) == false)
			{
			$ObjectTemplate->ArrayErrorMessages['Email'] = $ArrayLanguageStrings['Screen']['1572'];
			}

		if (count($ObjectTemplate->ArrayErrorMessages) > 0)
			{
			throw new Exception($ArrayLanguageStrings['Screen']['1571']);
			}
		// Email address validations - End

		// Un-subscribe subscriber
		Subscribers::Unsubscribe($ArrayList, $ArrayUser['UserID'], $ArrayCampaign['CampaignID'], 0,
			$ArraySubscriber['EmailAddress'], $ArraySubscriber['SubscriberID'], (isset($_SERVER['REMOTE_ADDR']) == true ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0'), 'SPAM complaint');

		// Add email address to suppression list
		Core::LoadObject('suppression_list');
		SuppressionList::Add(
			array(
				'SuppressionID' => '',
				'RelListID' => 0,
				'RelOwnerUserID' => $ArrayUser['UserID'],
				'SuppressionSource' => 'SPAM complaint',
				'EmailAddress' => $ArraySubscriber['EmailAddress']
			)
		);

		// Send abuse report to alert email address - Start
		O_Email_Sender_ForAdmin::send(
			O_Email_Factory::abuseReport($MessageID, $_POST['FormValue_Message'],
				$_POST['FormValue_Name'], $_POST['FormValue_Email'], $ArrayCampaign,
				$ArrayList, $ArraySubscriber, $ArrayUser));
		// Send abuse report to alert email address - End

		$ShowResultPage = true;
		}
	catch (Exception $e)
		{
		if ($e->GetMessage() != '')
			{
			$ObjectTemplate->ArrayErrorMessages['PageErrorMessage'] = $e->GetMessage();
			}
		}
	}
// EVENT: Send abuse report message to the system administrator - End

$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1575'];
$ObjectTemplate->ArrayFormFields 	= array(
											'PageErrorMessage'		=> 'TextField',
											'PageNoticeMessage'		=> 'TextField',
											'MessageID'				=> 'TextField',
											'Message'				=> 'TextArea',
											'Name'					=> 'TextField',
											'Email'					=> 'TextField',
											);

if (REPORT_ABUSE_FRIEND_HEADER == '')
	{
	$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/forward_header.tpl', 'file');
	}
else
	{
	$HeaderTemplateString = (substr(REPORT_ABUSE_FRIEND_HEADER, 0, 7) == 'http://' || substr(REPORT_ABUSE_FRIEND_HEADER, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent(REPORT_ABUSE_FRIEND_HEADER) : file_get_contents(REPORT_ABUSE_FRIEND_HEADER));
	$ObjectTemplate->AddToTemplateList($HeaderTemplateString, 'string');
	}

$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/report_abuse.tpl', 'file');

if (REPORT_ABUSE_FRIEND_FOOTER == '')
	{
	$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/forward_footer.tpl', 'file');
	}
else
	{
	$FooterTemplateString = (substr(REPORT_ABUSE_FRIEND_FOOTER, 0, 7) == 'http://' || substr(REPORT_ABUSE_FRIEND_FOOTER, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent(REPORT_ABUSE_FRIEND_FOOTER) : file_get_contents(REPORT_ABUSE_FRIEND_FOOTER));
	$ObjectTemplate->AddToTemplateList($FooterTemplateString, 'string');
	}

$ObjectTemplate->LoadTemplates();

$ArrayReplaceList = array(
						);
$ObjectTemplate->Replace($ArrayReplaceList);

// Display correct section - Start
if ($ShowResultPage == true)
	{
	$ObjectTemplate->RemoveBlock('SHOW:Form');
	}
else
	{
	$ObjectTemplate->RemoveBlock('SHOW:Result');
	}
// Display correct section - End

$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
// Page parsing - End

exit;
?>