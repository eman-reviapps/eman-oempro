<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Forward To Friend module
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('user_auth');
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
Core::LoadObject('form');
Core::LoadObject('users');
Core::LoadObject('user_groups');
Core::LoadObject('campaigns');
// Load other modules - End
// Check the login session, redirect based on the login session status - Start
UserAuth::IsLoggedIn(false, false);
// Check the login session, redirect based on the login session status - End
// Check if user sign up functionality enabled. If not, redirect to login page - Start
if (USER_SIGNUP_ENABLED == false) {
    header('Location: ./login.php');
    exit;
}
// Check if user sign up functionality enabled. If not, redirect to login page - End
// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');
// Load language - End
// Page parsing - Start
$ObjectTemplate = new TemplateEngine($ArrayUserInformation['GroupInformation']['Permissions']);

// EVENT: Perform new user sign-up - Start
if ($_POST['FormButton_SignUp'] != '') {
    $ObjectTemplate->ArrayErrorMessages = array();

    try {
        // Check for required fields - Start
        $ArrayRequiredFields = array(
            "EmailAddress" => $ArrayLanguageStrings['Screen']['1693'],
            "Username" => $ArrayLanguageStrings['Screen']['1694'],
            "Password" => $ArrayLanguageStrings['Screen']['1695']
        );

        $ArrayExtraFields = explode(',', USER_SIGNUP_FIELDS);

        if (count($ArrayExtraFields) > 0) {
            foreach ($ArrayExtraFields as $EachField) {
                if (substr($EachField, strlen($EachField) - 1, strlen($EachField)) == '*') {
                    $ArrayRequiredFields[substr($EachField, 0, strlen($EachField) - 1)] = $ArrayLanguageStrings['Screen']['1697'];
                }
            }
        }

        $ObjectTemplate->ArrayErrorMessages = $ObjectTemplate->CheckRequiredFields($ArrayRequiredFields, $ArrayErrorMessages, $_POST, 'FormValue_');

        if (count($ObjectTemplate->ArrayErrorMessages) > 0) {
            throw new Exception($ArrayLanguageStrings['Screen']['1696']);
        }
        // Check for required fields - End
        // Email address validations - Start
        Core::LoadObject('subscribers');

        if (Subscribers::ValidateEmailAddress($_POST['FormValue_EmailAddress']) == false) {
            $ObjectTemplate->ArrayErrorMessages['EmailAddress'] = $ArrayLanguageStrings['Screen']['1698'];
        }

        if (count($ObjectTemplate->ArrayErrorMessages) > 0) {
            throw new Exception($ArrayLanguageStrings['Screen']['1696']);
        }
        // Email address validations - End
        // Check if email address is registered before - Start
        $TotalFound = Users::RetrieveUsers(array('COUNT(*) AS TotalFound'), array('EmailAddress' => $_POST['FormValue_EmailAddress']), array('UserID' => 'ASC'));
        $TotalFound = $TotalFound[0]['TotalFound'];

        if ($TotalFound > 0) {
            $ObjectTemplate->ArrayErrorMessages['EmailAddress'] = $ArrayLanguageStrings['Screen']['1699'];
            throw new Exception($ArrayLanguageStrings['Screen']['1700']);
        }
        // Check if email address is registered before - End
        // Check if username is registered before - Start
        $TotalFound = Users::RetrieveUsers(array('COUNT(*) AS TotalFound'), array('Username' => $_POST['FormValue_Username']), array('UserID' => 'ASC'));
        $TotalFound = $TotalFound[0]['TotalFound'];

        if ($TotalFound > 0) {
            $ObjectTemplate->ArrayErrorMessages['Username'] = $ArrayLanguageStrings['Screen']['1702'];
            throw new Exception($ArrayLanguageStrings['Screen']['1700']);
        }
        // Check if username is registered before - End
        // Check if selected user group is allowed - Start
        if (USER_SIGNUP_GROUPID == -1 && $_POST['FormValue_RelUserGroupID'] != '') {
            $SelectedUserGroupIDs = explode(',', USER_SIGNUP_GROUPIDS);
            if (in_array($_POST['FormValue_RelUserGroupID'], $SelectedUserGroupIDs) == false) {
                $ObjectTemplate->ArrayErrorMessages['RelUserGroupID'] = $ArrayLanguageStrings['Screen']['1704'];
            }
            if (count($ObjectTemplate->ArrayErrorMessages) > 0) {
                throw new Exception($ArrayLanguageStrings['Screen']['1696']);
            }
        }
        // Check if selected user group is allowed - End
        // Sign-up the new user - Start
        $ArrayFieldAndValues = array(
            'RelUserGroupID' => (USER_SIGNUP_GROUPID == -1 ? $_POST['FormValue_RelUserGroupID'] : USER_SIGNUP_GROUPID),
            'EmailAddress' => $_POST['FormValue_EmailAddress'],
            'Username' => $_POST['FormValue_Username'],
            'Password' => md5(OEMPRO_PASSWORD_SALT . $_POST['FormValue_Password'] . OEMPRO_PASSWORD_SALT),
            'ReputationLevel' => USER_SIGNUP_REPUTATION,
            'UserSince' => date('Y-m-d H:i:s'),
            'FirstName' => ($_POST['FormValue_FirstName'] != '' ? $_POST['FormValue_FirstName'] : ''),
            'LastName' => ($_POST['FormValue_LastName'] != '' ? $_POST['FormValue_LastName'] : ''),
            'CompanyName' => ($_POST['FormValue_CompanyName'] != '' ? $_POST['FormValue_CompanyName'] : ''),
            'Website' => ($_POST['FormValue_Website'] != '' ? $_POST['FormValue_Website'] : ''),
            'Street' => ($_POST['FormValue_Street'] != '' ? $_POST['FormValue_Street'] : ''),
            'City' => ($_POST['FormValue_City'] != '' ? $_POST['FormValue_City'] : ''),
            'State' => ($_POST['FormValue_State'] != '' ? $_POST['FormValue_State'] : ''),
            'Zip' => ($_POST['FormValue_Zip'] != '' ? $_POST['FormValue_Zip'] : ''),
            'Country' => ($_POST['FormValue_Country'] != '' ? $_POST['FormValue_Country'] : ''),
            'Phone' => ($_POST['FormValue_Phone'] != '' ? $_POST['FormValue_Phone'] : ''),
            'Fax' => ($_POST['FormValue_Fax'] != '' ? $_POST['FormValue_Fax'] : ''),
            'TimeZone' => ($_POST['FormValue_TimeZone'] != '' ? $_POST['FormValue_TimeZone'] : ''),
            'SignUpIPAddress' => $_SERVER['REMOTE_ADDR'],
            'APIKey' => '',
            'Language' => ($_POST['FormValue_Language'] != '' ? $_POST['FormValue_Language'] : USER_SIGNUP_LANGUAGE),
            'LastActivityDateTime' => date('Y-m-d H:i:s'),
            'PreviewMyEmailAccount' => '',
            'PreviewMyEmailAPIKey' => '',
        );
        $NewUserID = Users::Create($ArrayFieldAndValues);
        // Sign-up the new user - End
        // Retrieve new user information - Start
        $ArrayNewUser = Users::RetrieveUser(array('*'), array('UserID' => $NewUserID));
        // Retrieve new user information - End
        // Set the API key for the new user - Start
        Users::UpdateUser(array('APIKey' => md5(MD5_SALT . $NewUserID)), array('UserID' => $NewUserID));
        // Set the API key for the new user - End

        $ShowResultPage = true;
    } catch (Exception $e) {
        if ($e->GetMessage() != '') {
            $ObjectTemplate->ArrayErrorMessages['PageErrorMessage'] = $e->GetMessage();
        }
    }
}
// EVENT: Perform new user sign-up - End

$ObjectTemplate->PageTitle = $ArrayLanguageStrings['Screen']['1705'];
$ObjectTemplate->ArrayFormFields = array(
    'PageErrorMessage' => 'TextField',
    'PageNoticeMessage' => 'TextField',
    'EmailAddress' => 'TextField',
    'Username' => 'TextField',
    'Password' => 'TextField',
    'RelUserGroupID' => 'DropDown',
);

if (USER_SIGNUP_HEADER == '') {
    $ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/forward_header.tpl', 'file');
} else {
    $HeaderTemplateString = (substr(USER_SIGNUP_HEADER, 0, 7) == 'http://' || substr(USER_SIGNUP_HEADER, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent(USER_SIGNUP_HEADER) : file_get_contents(USER_SIGNUP_HEADER));
    $ObjectTemplate->AddToTemplateList($HeaderTemplateString, 'string');
}

$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/signup.tpl', 'file');

if (USER_SIGNUP_FOOTER == '') {
    $ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/forward_footer.tpl', 'file');
} else {
    $FooterTemplateString = (substr(USER_SIGNUP_FOOTER, 0, 7) == 'http://' || substr(USER_SIGNUP_FOOTER, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent(USER_SIGNUP_FOOTER) : file_get_contents(USER_SIGNUP_FOOTER));
    $ObjectTemplate->AddToTemplateList($FooterTemplateString, 'string');
}

$ObjectTemplate->LoadTemplates();

// Make replacements - Start
if ($ShowResultPage == true) {
    $ArrayReplaceList = array(
        'Insert:SuccessMessage' => $ArrayLanguageStrings['Screen']['1688'],
        'Insert:LoginLink' => Core::InterfaceAppURL() . '/user/',
    );
    $ObjectTemplate->Replace($ArrayReplaceList);
}
// Make replacements - End
// Parse user group selection - Start
$ObjectTemplate->DefineBlock('SHOWHIDE:PlanSelection');
$ObjectTemplate->DefineBlockInBlock('LIST:UserGroups', 'SHOWHIDE:PlanSelection');
if (USER_SIGNUP_GROUPID == -1) {
    $ArraySelectedUserGroups = array();
    $ArrayUserGroups = UserGroups::RetrieveUserGroups(array('*'), array(), array('GroupName' => 'ASC'));
    $SelectedUserGroupIDs = explode(',', USER_SIGNUP_GROUPIDS);
    foreach ($ArrayUserGroups as $EachGroup) {
        for ($i = 0; $i < count($SelectedUserGroupIDs); $i++) {
            if ($EachGroup['UserGroupID'] == $SelectedUserGroupIDs[$i]) {
                $ArraySelectedUserGroups[] = $EachGroup;
                break;
            }
        }
    }
    foreach ($ArraySelectedUserGroups as $EachGroup) {
        $ObjectTemplate->DuplicateBlockInBlock('LIST:UserGroups', 'SHOWHIDE:PlanSelection', array(
            "UserGroupID" => $EachGroup['UserGroupID'],
            "GroupName" => $EachGroup['GroupName']
        ));
    }
    $ObjectTemplate->DuplicateBlock('SHOWHIDE:PlanSelection', 'SHOWHIDE:PlanSelection', array());
} else {
    $ObjectTemplate->RemoveBlock('SHOWHIDE:PlanSelection');
}
// Parse user group selection - End
// Parse extra fields - Start
if ($ShowResultPage == false) {
    $ObjectTemplate->DefineBlock('ROW:ExtraFields:TextField');
    $ObjectTemplate->DefineBlock('ROW:ExtraFields:DropList');
    $ObjectTemplate->DefineBlockInBlock('LIST:Options', 'ROW:ExtraFields:DropList');

    $ArrayExtraFields = explode(',', USER_SIGNUP_FIELDS);

    if (count($ArrayExtraFields) > 0) {
        foreach ($ArrayExtraFields as $EachField) {
            $IsRequired = false;

            if (substr($EachField, strlen($EachField) - 1, strlen($EachField)) == '*') {
                $IsRequired = true;
                $EachField = substr($EachField, 0, strlen($EachField) - 1);
            }

            if (($EachField == 'Country') || ($EachField == 'TimeZone') || ($EachField == 'Language')) {
                $ObjectTemplate->ArrayFormFields[$EachField] = 'DropList';

                // List drop list options - Start
                if ($EachField == 'Country') {
                    asort($ArrayLanguageStrings['Screen']['9052']);

                    foreach ($ArrayLanguageStrings['Screen']['9052'] as $CountryCode => $CountryName) {
                        $ArrayReplaceList = array(
                            'Option:Value' => $CountryCode,
                            'Option:Name' => $CountryName,
                        );
                        $ObjectTemplate->DuplicateBlockInBlock('LIST:Options', 'ROW:ExtraFields:DropList', $ArrayReplaceList);
                    }
                } elseif ($EachField == 'TimeZone') {
                    foreach (Core::GetTimeZoneList() as $TimeZone) {
                        $ArrayReplaceList = array(
                            'Option:Value' => $TimeZone,
                            'Option:Name' => $TimeZone,
                        );
                        $ObjectTemplate->DuplicateBlockInBlock('LIST:Options', 'ROW:ExtraFields:DropList', $ArrayReplaceList);
                    }
                } elseif ($EachField == 'Language') {
                    foreach (Core::DetectLanguages() as $Index => $EachLanguage) {
                        $ArrayReplaceList = array(
                            'Option:Value' => $EachLanguage['Code'],
                            'Option:Name' => $EachLanguage['Name'],
                        );
                        $ObjectTemplate->DuplicateBlockInBlock('LIST:Options', 'ROW:ExtraFields:DropList', $ArrayReplaceList);
                    }
                }
                // List drop list options - End

                $ArrayReplaceList = array(
                    'List:FieldTitle' => $ArrayLanguageStrings['Screen']['1692'][$EachField],
                    'List:FieldName' => $EachField,
                );
                $ObjectTemplate->DuplicateBlock('ROW:ExtraFields:DropList', 'ROW:ExtraFields:DropList', $ArrayReplaceList);
            } else {
                $ObjectTemplate->ArrayFormFields[$EachField] = 'TextField';

                $ArrayReplaceList = array(
                    'List:FieldTitle' => $ArrayLanguageStrings['Screen']['1692'][$EachField],
                    'List:FieldName' => $EachField,
                );
                $ObjectTemplate->DuplicateBlock('ROW:ExtraFields:TextField', 'ROW:ExtraFields:TextField', $ArrayReplaceList);
            }
        }
    }
}
// Parse extra fields - End
// Display correct section - Start
if ($ShowResultPage == true) {
    $ObjectTemplate->RemoveBlock('SHOW:Form');
} else {
    $ObjectTemplate->RemoveBlock('SHOW:Result');
}
// Display correct section - End

$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
// Page parsing - End


exit;
?>