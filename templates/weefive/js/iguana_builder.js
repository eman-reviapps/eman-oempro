Array.prototype.inArray = function (value) {
	var i;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			return true;
		}
	}
	return false;
};

var iguana_builder = {
	iframe_selector							: '#email-template-source',
	iframe_body								: null,
	element_counter							: 0,
	properties_panel_status					: 'hidden',
	current_element							: null,
	current_panel_button					: null,
	sync_panel_status						: 'disabled',
	link_panel_status						: 'disabled',
	tmp_sync_element						: null,
	tmp_link_element						: null,
	appropriateTagsFirRichEditable			: ['div','td','body'],
	init									: function() {
		$('#iguana-builder-test-drive-button').click(iguana_builder.on_test_drive_click);
		$('#iguana-builder-save-button').click(iguana_builder.on_save_click);
		$('#iguana-builder-property-editable').click(iguana_builder.on_make_editable_change);
		$('#iguana-builder-property-duplicatable').click(iguana_builder.on_make_duplicatable_change);
		$('input[name="iguana-builder-property-editable-type"]').change(iguana_builder.on_editable_type_change);
		$('#properties-panel-sync-button').click(iguana_builder.on_sync_with_button_click);
		$('#properties-panel-link-button').click(iguana_builder.on_link_to_button_click);
		$('#iguana-builder-cancel-sync-link').click(iguana_builder.on_cancel_sync_with_link_click);
		$('#iguana-builder-cancel-link-link').click(iguana_builder.on_cancel_link_to_link_click);
		$('#iguana-builder-confirm-sync-button').click(iguana_builder.on_sync_confirm_button_click);
		$('#iguana-builder-confirm-link-button').click(iguana_builder.on_link_confirm_button_click);
		$('#properties-panel-sync-remove-button').click(iguana_builder.on_sync_remove_button_click);
		$('#properties-panel-link-remove-button').click(iguana_builder.on_link_remove_button_click);
	},
	on_frame_load							: function() {
		// Get frame body
		iguana_builder.iframe_body = $(iguana_builder.iframe_selector).contents().find("body");
		// Add styling to frame
		iguana_builder.iframe_body.css('cursor','pointer');
		iguana_builder.iframe_body.prepend('<link id="iguana-builder-stylesheet" rel="stylesheet" href="'+BUILDER_CSS_URL+'" type="text/css" media="screen, projection">');
		// Add click behavior to all frame body - Start
		$('*', iguana_builder.iframe_body).click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			$('.element-panel').html('');
			if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
				$('.element-selected', iguana_builder.iframe_body).removeClass('element-selected');
			} else {
				$('.element-selected-sync-link', iguana_builder.iframe_body).removeClass('element-selected-sync-link');
			}
			iguana_builder.find_parent_elements(this, 0);
		});
		// Add click behavior to all frame body - End
		iguana_builder.find_parent_elements(iguana_builder.iframe_body.get(0), 0);
		
		var resizeTimer = null;
		$(window).bind('resize', function() {
			if (resizeTimer) clearTimeout(resizeTimer);
			resizeTimer = setTimeout(iguana_builder.on_window_resize, 10, true);
		});
		iguana_builder.on_window_resize();
	},
	on_tooltip_element_over					: function(element) {
		if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
			$(element).addClass('element-mouse-over');
		} else {
			$(element).addClass('element-mouse-over-sync-link');
		}
	},
	on_tooltip_element_out					: function(element) {
		if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
			$(element).removeClass('element-mouse-over');
		} else {
			$(element).removeClass('element-mouse-over-sync-link');
		}
	},
	find_parent_elements					: function(element, counter) {
		if (counter > 15) {
			var button = iguana_builder.add_panel_button('...');
			$('.element-panel').prepend(button);
			return;
		}

		// Add tag button - Start
		var tag_name = element.tagName;
		if (iguana_builder.check_if_element_has_iguana_classes('any', element) || iguana_builder.check_if_element_has_iguana_attributes('any', element)) {
			tag_name = tag_name + '*';
		}
		var button = iguana_builder.add_panel_button(tag_name);
		$('.element-panel').prepend(button);
		// Add tag button - End
		
		// If this is the first element, add 'last' class and add iguana tools - Start
		if (counter == 0) {
			if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
				iguana_builder.clean_up_css_classes_from_frame_body();
			}
			
			iguana_builder.current_element = element;
			iguana_builder.current_panel_button = button;
			button.addClass('last');

			// Add iguana tools - Start
			var properties = new Array();
			if (iguana_builder.check_if_element_has_iguana_classes('editable', element)) {
				properties.push(Language['0001']);
			}
			if (iguana_builder.check_if_element_has_iguana_classes('duplicatable', element)) {
				properties.push(Language['0003']);
			}
			if (iguana_builder.check_if_element_has_iguana_attributes('sync', element)) {
				properties.push(Language['0005']);
				$('#properties-panel-sync-remove-button').show();
				$('#properties-panel-sync-button').hide();
			} else {
				$('#properties-panel-sync-remove-button').hide();
				$('#properties-panel-sync-button').show();
			}
			if (iguana_builder.check_if_element_has_iguana_attributes('link', element)) {
				properties.push(Language['0006']);
				$('#properties-panel-link-remove-button').show();
				$('#properties-panel-link-button').hide();
			} else {
				$('#properties-panel-link-remove-button').hide();
				$('#properties-panel-link-button').show();
			}
			if (properties.length > 0) {
				var text = $('<span class="properties">'+properties.join(', ')+'</span>');
				$('.element-panel').append(text);
			}

			if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
				var button_edit = iguana_builder.add_panel_button((iguana_builder.properties_panel_status == 'hidden' ? Language['0002'] : Language['0004']), 'iguana-builder-edit-properties-button');
				$('.element-panel').append(button_edit);
				button_edit.click(iguana_builder.on_properties_button_click);
			} else {
				$('.element-panel').append('<a name="x"></a>');
			}

				// Add selected class to element in email body - Start {
				if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
					$(element, iguana_builder.iframe_body).addClass('element-selected');
				} else {
					$(element, iguana_builder.iframe_body).addClass('element-selected-sync-link');
				}
				// Add selected class to element in email body - End }
				
				// Add synced class to synced elements in email body - Start {
				if (iguana_builder.check_if_element_has_iguana_attributes('sync', element)) {
					$('* [iguana_syncode="'+$(element).attr('iguana_syncode')+'"]', iguana_builder.iframe_body).not($(element)).addClass('element-selected-sync-link');
				}
				// Add synced class to synced elements in email body - End }

				// Add linked class to linked elements in email body - Start {
				if (iguana_builder.check_if_element_has_iguana_attributes('link', element)) {
					$('* [iguana_linkcode="'+$(element).attr('iguana_linkcode')+'"]', iguana_builder.iframe_body).not($(element)).addClass('element-selected-sync-link');
				}
				// Add linked class to linked elements in email body - End }
			// Add iguana tools - End
			
			// Setup properties panel - Start
			iguana_builder.setup_properties_panel(element);
			// Setup properties panel - End
		}
		// If this is the first element, add 'last' class and add iguana tools - End

		// Add click behavior to panel button - Start
		button.click(function(event) {
			iguana_builder.on_panel_button_click(this, event, element);
		});
		// Add click behavior to panel button - End

		// Add hover behavior to panel button - Start
		button.hover(function() {
				iguana_builder.on_tooltip_element_over(element);
		}, function() {
				iguana_builder.on_tooltip_element_out(element);
		});
		// Add hover behavior to panel button - End
	
		
		if ($(element).parent().length > 0) {
			if ($(element).parent().get(0).tagName != 'HTML') {
				iguana_builder.find_parent_elements($(element).parent().get(0),counter+1);
			}
		}
	},
	add_panel_button						: function(label, id) {
		return $('<a href="#"></a>').html(label).attr('id', (id ? id : ''));
	},
	setup_properties_panel					: function(element) {
		if (iguana_builder.check_if_element_has_iguana_classes('editable', element)) {
			$('#iguana-builder-property-editable').get(0).checked = true;
			$('input[name="iguana-builder-property-editable-type"]').attr('disabled', '');

			$('#iguana-builder-property-editable-type-single').get(0).checked = false;
			$('#iguana-builder-property-editable-type-plain').get(0).checked = false;
			$('#iguana-builder-property-editable-type-rich').get(0).checked = false;

			if ($(element).hasClass('iguana-single-editable')) {
				$('#iguana-builder-property-editable-type-single').get(0).checked = true;
			} else if ($(element).hasClass('iguana-plain-editable')) {
				$('#iguana-builder-property-editable-type-plain').get(0).checked = true;
			} else if ($(element).hasClass('iguana-rich-editable')) {
				$('#iguana-builder-property-editable-type-rich').get(0).checked = true;
			}
		} else {
			$('#iguana-builder-property-editable').get(0).checked = false;
			$('#iguana-builder-property-editable-type-single').get(0).checked = false;
			$('#iguana-builder-property-editable-type-plain').get(0).checked = false;
			$('#iguana-builder-property-editable-type-rich').get(0).checked = false;
			$('input[name="iguana-builder-property-editable-type"]').attr('disabled', 'disabled');
		}
		if (iguana_builder.check_if_element_has_iguana_classes('duplicatable', element)) {
			$('#iguana-builder-property-duplicatable').get(0).checked = true;
		} else {
			$('#iguana-builder-property-duplicatable').get(0).checked = false;
		}
	},
	on_panel_button_click					: function(current_element, event, element) {
		event.preventDefault();
		event.stopPropagation();
		$('.element-panel').html('');
		if (iguana_builder.sync_panel_status == 'disabled' && iguana_builder.link_panel_status == 'disabled') {
			$('.element-selected', iguana_builder.iframe_body).removeClass('element-selected');
		} else {
			$('.element-selected-sync-link', iguana_builder.iframe_body).removeClass('element-selected-sync-link');
		}
		iguana_builder.find_parent_elements(element, 0);
	},
	on_properties_button_click				: function() {
		if (iguana_builder.properties_panel_status == 'hidden') {
			$('.panel-container').animate({height:'150'},200);
			$(iguana_builder.iframe_selector).animate({height:'-=120'},200);
			$(iguana_builder.iframe_body).animate({scrollTop:'+=120'});
			$('#iguana-builder-edit-properties-button').text(Language['0004']);
			iguana_builder.properties_panel_status = 'visible';
		} else {
			$('.panel-container').animate({height:'30'},200);
			$(iguana_builder.iframe_selector).animate({height:'+=120'},200);
			$(iguana_builder.iframe_body).animate({scrollTop:'-=120'});
			$('#iguana-builder-edit-properties-button').text(Language['0002']);
			iguana_builder.properties_panel_status = 'hidden';
		}
	},
	on_make_editable_change					: function() {
		if ($(this).get(0).checked == true) {
			$('input[name="iguana-builder-property-editable-type"]').attr('disabled', '');
			$('input[name="iguana-builder-property-editable-type"]').val(['single']);
			
			var classes_to_be_added = 'iguana-single-editable';
			if ($(iguana_builder.current_element).hasClass('iguana') == false) {
				classes_to_be_added = 'iguana ' + classes_to_be_added;
				$(iguana_builder.current_panel_button).append('*');
			}
			$(iguana_builder.current_element).addClass(classes_to_be_added);
		} else {
			$('#iguana-builder-property-editable-type-single').get(0).checked = false;
			$('#iguana-builder-property-editable-type-plain').get(0).checked = false;
			$('#iguana-builder-property-editable-type-rich').get(0).checked = false;
			$('input[name="iguana-builder-property-editable-type"]').attr('disabled', 'disabled');

			var classes_to_be_removed = 'iguana-plain-editable iguana-single-editable iguana-rich-editable';
			if (iguana_builder.check_if_element_has_iguana_classes('duplicatable', iguana_builder.current_element) == false) {
				classes_to_be_removed = classes_to_be_removed + ' iguana';
				var panel_button_text = $(iguana_builder.current_panel_button).text();
				$(iguana_builder.current_panel_button).text(panel_button_text.substr(0, panel_button_text.length-1));
			}
			$(iguana_builder.current_element).removeClass(classes_to_be_removed);
		}
	},
	on_make_duplicatable_change				: function() {
		if ($(this).get(0).checked == true) {
			var classes_to_be_added = 'iguana-duplicatable';
			if ($(iguana_builder.current_element).hasClass('iguana') == false) {
				classes_to_be_added = 'iguana ' + classes_to_be_added;
				$(iguana_builder.current_panel_button).append('*');
			}
			$(iguana_builder.current_element).addClass(classes_to_be_added);
		} else {
			var classes_to_be_removed = 'iguana-duplicatable';
			if (iguana_builder.check_if_element_has_iguana_classes('editable', iguana_builder.current_element) == false) {
				classes_to_be_removed = classes_to_be_removed + ' iguana';
				var panel_button_text = $(iguana_builder.current_panel_button).text();
				$(iguana_builder.current_panel_button).text(panel_button_text.substr(0, panel_button_text.length-1));
			}
			$(iguana_builder.current_element).removeClass(classes_to_be_removed);
		}
	},
	on_editable_type_change					: function(event) {
		event.preventDefault();
		if (iguana_builder.current_element != null) {
			var value = $('input[name="iguana-builder-property-editable-type"]:checked').val();
			var tag = iguana_builder.current_element.tagName.toLowerCase();
			if (value == 'rich' && iguana_builder.appropriateTagsFirRichEditable.inArray(tag) == false) {
				$('#message').slideDown();
			} else {
				$('#message').slideUp();
			}
			$(iguana_builder.current_element).removeClass('iguana-plain-editable iguana-single-editable iguana-rich-editable').addClass('iguana-'+$('input[name="iguana-builder-property-editable-type"]:checked').val()+'-editable');
		}
	},
	on_test_drive_click						: function() {
		iguana_builder.clean_up_frame_body();
		var html = $(iguana_builder.iframe_selector).get(0).contentDocument.documentElement.innerHTML;
		var frame_html = '<html>'+html+'</html>';
		$('#frame-html-container-for-post').text(frame_html);
		var form_action = $('#test-drive-form').attr('action')+'testDrive/';
		$('#test-drive-form').attr('action', form_action).submit();
	},
	on_save_click							: function() {
		iguana_builder.clean_up_frame_body();
		var html = $(iguana_builder.iframe_selector).get(0).contentDocument.documentElement.innerHTML;
		var frame_html = '<html>'+html+'</html>';

		var doctype_string = '';
		if (doctype_object = $(iguana_builder.iframe_selector).get(0).contentDocument.doctype) {
			doctype_string = '<!DOCTYPE ';
			doctype_string += doctype_object.name.toUpperCase() + ' ';
			doctype_string += 'PUBLIC "' + doctype_object.publicId + '" ';
			doctype_string += '"'+doctype_object.systemId+'">' + "\n";
		}

		$('#frame-html-container-for-post').text(doctype_string + frame_html);
		var form_action = $('#test-drive-form').attr('action')+'save/';
		$('#test-drive-form').attr('action', form_action).submit();
	},
	on_window_resize						: function(adjust_iframe_scrolltop) {
		var height_difference = 0;
		var new_window_height = $(window).height();
		if (iguana_builder.properties_panel_status == 'hidden') {
			height_difference = new_window_height - 91;
		} else {
			height_difference = new_window_height - 211;
		}
		$(iguana_builder.iframe_selector).css('height',height_difference+'px');
	},
	on_sync_with_button_click				: function() {
		iguana_builder.sync_panel_status = 'enabled';
		iguana_builder.tmp_sync_element = iguana_builder.current_element;
		iguana_builder.switch_to_sync_link_panel();
		$('#sync-properties').fadeIn('fast').animate({top:'-=25'}, 200);
	},
	on_cancel_sync_with_link_click			: function() {
		iguana_builder.sync_panel_status = 'disabled';
		iguana_builder.clean_up_css_classes_from_frame_body();
		$('#sync-properties').animate({top:'+=25'}, 200).fadeOut('fast');
		$('.element-panel').html('');
		iguana_builder.switch_to_standard_panel();
		iguana_builder.find_parent_elements(iguana_builder.tmp_sync_element, 0);
		iguana_builder.tmp_sync_element = null;
	},
	on_sync_confirm_button_click			: function() {
		var uid = iguana_builder.generate_unique_identifier('sync');
		if ($(iguana_builder.current_element).hasClass('iguana') == false) {
			$(iguana_builder.current_element).addClass('iguana');
		}
		$(iguana_builder.current_element).attr('iguana_syncode', uid);
		$(iguana_builder.current_element).attr('iguana_sync_target', 'true');
		$(iguana_builder.tmp_sync_element).attr('iguana_syncode', uid);
		$(iguana_builder.tmp_sync_element).attr('iguana_sync_source', 'true');
		iguana_builder.on_cancel_sync_with_link_click();
	},
	on_sync_remove_button_click				: function() {
		if ($(iguana_builder.current_element).attr('iguana_syncode') != undefined) {
			uid = $(iguana_builder.current_element).attr('iguana_syncode');
			$(iguana_builder.current_element).removeAttr('iguana_syncode').removeAttr('iguana_sync_source').removeAttr('iguana_sync_target');
			$('[iguana_syncode="'+uid+'"]', iguana_builder.iframe_body).removeAttr('iguana_syncode').removeAttr('iguana_sync_source').removeAttr('iguana_sync_target');
		}
		$('.element-panel').html('');
		iguana_builder.find_parent_elements(iguana_builder.current_element, 0);
	},
	on_link_to_button_click					: function() {
		iguana_builder.link_panel_status = 'enabled';
		iguana_builder.tmp_link_element = iguana_builder.current_element;
		iguana_builder.switch_to_sync_link_panel();
		$('#link-properties').fadeIn('fast').animate({top:'-=25'}, 200);
	},
	on_cancel_link_to_link_click			: function() {
		iguana_builder.link_panel_status = 'disabled';
		iguana_builder.clean_up_css_classes_from_frame_body();
		$('#link-properties').animate({top:'+=25'}, 200).fadeOut('fast');
		$('.element-panel').html('');
		iguana_builder.switch_to_standard_panel();
		iguana_builder.find_parent_elements(iguana_builder.tmp_link_element, 0);
		iguana_builder.tmp_link_element = null;
	},
	on_link_confirm_button_click			: function() {
		var uid = iguana_builder.generate_unique_identifier('link');
		if ($(iguana_builder.current_element).hasClass('iguana') == false) {
			$(iguana_builder.current_element).addClass('iguana');
		}
		$(iguana_builder.current_element).attr('iguana_linkcode', uid);
		$(iguana_builder.current_element).attr('iguana_link_target', 'true');
		$(iguana_builder.tmp_link_element).attr('iguana_linkcode', uid);
		$(iguana_builder.tmp_link_element).attr('iguana_link_source', 'true');
		iguana_builder.on_cancel_link_to_link_click();
	},
	on_link_remove_button_click				: function() {
		if ($(iguana_builder.current_element).attr('iguana_linkcode') != undefined) {
			uid = $(iguana_builder.current_element).attr('iguana_linkcode');
			$(iguana_builder.current_element).removeAttr('iguana_linkcode').removeAttr('iguana_link_source').removeAttr('iguana_link_target');
			$('[iguana_linkcode="'+uid+'"]', iguana_builder.iframe_body).removeAttr('iguana_linkcode').removeAttr('iguana_link_source').removeAttr('iguana_link_target');
		}
		$('.element-panel').html('');
		iguana_builder.find_parent_elements(iguana_builder.current_element, 0);
	},
	switch_to_sync_link_panel				: function() {
		$('.panel-container').animate({height:'115'},200);
		$(iguana_builder.iframe_selector).animate({height:'+=35'},200);
		$(iguana_builder.iframe_body).animate({scrollTop:'-=35'});
		$('#standard-properties').fadeOut('fast');
		$('.element-panel').animate({
			top		: '+=50',
			opacity : 0
			}, function() { 
				$(this).html('').css({opacity:100}); 
			});
	},
	switch_to_standard_panel				: function() {
		$('.panel-container').animate({height:'150'},200);
		$(iguana_builder.iframe_selector).animate({height:'-=35'},200);
		$(iguana_builder.iframe_body).animate({scrollTop:'+=35'});
		$('.element-panel').animate({top:'-=50'});
		$('#standard-properties').fadeIn('fast');
	},
	clean_up_frame_body						: function() {
		iguana_builder.iframe_body.css('cursor','');
		iguana_builder.iframe_body.removeClass('element-selected');
		iguana_builder.iframe_body.removeClass('element-mouse-over');
		iguana_builder.clean_up_css_classes_from_frame_body();
		$('#iguana-builder-stylesheet', iguana_builder.iframe_body).remove();
	},
	clean_up_css_classes_from_frame_body	: function() {
		$('*', iguana_builder.iframe_body).removeClass('element-selected');
		$('*', iguana_builder.iframe_body).removeClass('element-selected-sync-link');
		$('*', iguana_builder.iframe_body).removeClass('element-mouse-over-sync-link');
		$('*', iguana_builder.iframe_body).removeClass('element-mouse-over');
	},
	check_if_element_has_iguana_classes		: function(type, element) {
		if (type == 'any') {
			if ($(element).hasClass('iguana-plain-editable') || $(element).hasClass('iguana-single-editable') || $(element).hasClass('iguana-rich-editable') || $(element).hasClass('iguana-duplicatable')) {
				return true;
			} else {
				return false;
			}
		} else if (type == 'editable') {
			if ($(element).hasClass('iguana-plain-editable') || $(element).hasClass('iguana-single-editable') || $(element).hasClass('iguana-rich-editable')) {
				return true;
			} else {
				return false;
			}
		} else if (type == 'duplicatable') {
			if ($(element).hasClass('iguana-duplicatable')) {
				return true;
			} else {
				return false;
			}
		}
	},
	check_if_element_has_iguana_attributes	: function(type, element) {
		if (type == 'any') {
			if ($(element).attr('iguana_syncode') != undefined && $(element).attr('iguana_linkcode') != undefined) {
				return true;
			} else {
				return false;
			}
		} else if (type == 'sync') {
			if ($(element).attr('iguana_syncode') != undefined) {
				return true;
			} else {
				return false;
			}
		} else if (type == 'link') {
			if ($(element).attr('iguana_linkcode') != undefined) {
				return true;
			} else {
				return false;
			}
		}
	},
	generate_unique_identifier				: function(prefix) {
		var newDate = new Date;
		return prefix + '-' + newDate.getTime();
	}	
};

$(document).ready(function () {
	iguana_builder.init();
	$("iframe").attr('src', BUILDER_IFRAME_SOURCE).load(iguana_builder.on_frame_load);
});