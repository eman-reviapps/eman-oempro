/* -------------------------------------------- Global variables - Start */
var search_widget_status = 'hidden';
var keyboard_side_bar_mode = 'off';
var current_keyboard_sidebar_index = 0;
var current_keyboard_sidebar_element = null;
var current_keyboard_sidebar_sub_index = 0;
var current_keyboard_sidebar_sub_element = null;
/* -------------------------------------------- Global variables - End */

/* -------------------------------------------- Plugins and widgets - Start */
/* Form row focus highlighter plugin - Start */
$(document).ready(function () {
    (function ($) {
        $.form_row_focus = function () {};
        /* Utility functions - Start */
        get_form_row_id = function (element) {
            var element_type = $(element).attr('type');
            if (element_type == 'checkbox' || element_type == 'radio') {
                var form_row_id = $(element).attr('id').split('-');
                form_row_id = form_row_id[0];
            } else {
                if ($(element).hasClass('personalization-select')) {
                    form_row_id = $(element).attr('id').replace('personalization-select-', '');
                } else if ($(element).hasClass('personalization-link')) {
                    form_row_id = $(element).attr('id').replace('personalization-link-', '');
                } else {
                    form_row_id = $(element).attr('id');
                }
            }
            return form_row_id;
        };
        highligh_form_row = function (element) {
            var form_row_id = get_form_row_id($(element));
            $('div[id="form-row-' + form_row_id + '"]').addClass('focus');
            $('div[id="personalization-' + form_row_id + '"]').addClass('focus');
        };
        remove_highlight_from_form_row = function (element) {
            var form_row_id = get_form_row_id($(element));
            $('div[id="form-row-' + form_row_id + '"]').removeClass('focus');
            $('div[id="personalization-' + form_row_id + '"]').removeClass('focus');
        };
        /* Utility functions - End */
        /* Main plugin function - Start */
        $.fn.form_row_focus = function (options) {
            return this.each(function () {
                $(this).focus(function (event) {
                    highligh_form_row(this);
                });
                $(this).blur(function () {
                    remove_highlight_from_form_row(this);
                });
            });
        };
        /* Main plugin function - End */
    })(jQuery);
    /* Form row focus highlighter plugin - End */

    /* Personalization content plugin - S§tart */
    (function ($) {
        /* Utility functions - Start */
        set_selection_range = function (input, selectionStart, selectionEnd) {
            if (input.get(0).setSelectionRange) {
                input.focus();
                input.get(0).setSelectionRange(selectionStart, selectionEnd);
            } else if (input.get(0).createTextRange) {
                var range = input.get(0).createTextRange();
                range.collapse(true);
                range.moveEnd('character', selectionEnd);
                range.moveStart('character', selectionStart);
                range.select();
            }
        };
        add_content_to_field = function (content, field) {
            // tinyMCE.activeEditor.selection.setContent(Content);
            if (field.get(0).setSelectionRange) {
                var scrollTop = field.get(0).scrollTop;
                var selectionStart = field.get(0).selectionStart;
                var selectionEnd = field.get(0).selectionEnd;
                field.val(field.val().substring(0, selectionStart) + content + field.val().substring(selectionEnd));

                if (selectionStart != selectionEnd) {
                    set_selection_range(field, selectionStart, selectionStart + content.length);
                } else {
                    set_selection_range(field, selectionStart + content.length, selectionStart + content.length);
                }
                field.get(0).scrollTop = scrollTop;
            } else if (document.selection) {
                field.focus();
                var sel = document.selection.createRange();
                sel.text = content;
            }
        };
        /* Utility functions - End */
        $.personalization = function () {};
        $.fn.personalization = function () {
            return this.each(function () {
                var id = $(this).attr('id');
                $('#personalization-select-' + id).change(function () {
                    var content = $(this).val();
                    add_content_to_field(content, $('#' + id));
                    $(this).get(0).options[0].selected = true;
                });
            });
        };
    })(jQuery);
    /* Personalization content plugin - End */

    /* Tab plugin - Start */
    (function ($) {
        $.tab = function () {};
        /* Utility functions - Start */
        select_tab = function (element, check_callback_return) {
            check_callback_return = check_callback_return == undefined ? false : check_callback_return;
            var tab_collection = $(element).parent().attr('tabcollection');
            var tab_ul_element = $('ul.livetabs[tabcollection="' + tab_collection + '"]')[0];
            var clicked_tab_id = $(element).attr('id');
            var clicked_tab_id_stripped = clicked_tab_id.replace('tab-', '');
            if ($(tab_ul_element).attr('tabcallback')) {
                result = eval($(tab_ul_element).attr('tabcallback'))(tab_collection, clicked_tab_id);
                if (check_callback_return) {
                    if (result == true) {
                        return false;
                    }
                }
            }
            $('ul.livetabs[tabcollection="' + tab_collection + '"] > li.selected').removeClass('selected');
            $(element).addClass('selected');
            $('div[tabcollection="' + tab_collection + '"]').hide();
            $('#tab-content-' + clicked_tab_id_stripped).show();
        };
        /* Utility functions - End */
        /* Main plugin function - Start */
        $.fn.tab = function (options) {
            var url = document.location.toString();
            var url_tab_select = false;
            if (url.match('#')) {
                var url_anchor = url.split('#')[1].split('/');
                var url_tab_collection = '';
                var url_tab = '';
                if (url_anchor.length == 2) {
                    url_tab_collection = url_anchor[0];
                    url_tab = url_anchor[1];
                    if ($('ul[tabcollection="' + url_tab_collection + '"]').length > 0 && $('div#tab-content-' + url_tab.replace('tab-', '') + '[tabcollection="' + url_tab_collection + '"]').length > 0) {
                        url_tab_select = true;
                    }
                }
            }
            return this.each(function () {
                var tab_ul_element = this;
                var tab_ul_element_collection = $(tab_ul_element).attr('tabcollection');
                if (url_tab_select && tab_ul_element_collection == url_tab_collection) {
                    select_tab($('#' + url_tab).get(0));
                } else {
                    var selected_tabs = $(tab_ul_element).children('li.selected');
                    if (selected_tabs.length < 1) {
                        var first_tab_element = $(tab_ul_element).children('li:first')[0];
                        if ($(first_tab_element).hasClass('label')) {
                            first_tab_element = $(first_tab_element).next();
                        }
                        select_tab(first_tab_element, false);
                    } else {
                        select_tab(selected_tabs.get(0), false);
                    }
                }
                $('> li', this).not('.header').click(function () {
                    if ($(this).hasClass('label')) {
                        return false;
                    }
                    select_tab(this, true);
                    return false;
                });
            });
        };
        /* Main plugin function - End */
    })(jQuery);
    /* Tab plugin - End */

    /* Dropdown menu plugin - Start */
    (function ($) {
        $.dropdown = function () {};
        /* Utility functions - Start */
        select_dropdown_item = function (element, select_parent) {
            var parent_ul_element = $(element).parent();
            var parent_li_element = parent_ul_element.parent();
            var selected_item_text = $(element).html();
            $('> a:first', parent_li_element).remove();
            $(parent_li_element).prepend(selected_item_text);
            $('> ul li.selected', parent_li_element).removeClass('selected');
            $(element).addClass('selected');

            var most_parent_ul_element = $(element).parent().parent().parent();
            var tab_collection = $(most_parent_ul_element).attr('tabcollection');
            if (select_parent == true) {
                select_tab(parent_li_element);
            }

            var selected_item_id = $(element).attr('id');
            var selected_item_id_stripped = selected_item_id.replace('item-', '');

            $('div[tabcollection="' + tab_collection + '"] div.dropdown-menu-content').hide();
            $('#item-content-' + selected_item_id_stripped).show();

            if ($(parent_ul_element).attr('tabcallback')) {
                result = eval($(parent_ul_element).attr('tabcallback'))(selected_item_id);
            }
        };
        /* Utility functions - End */
        /* Main plugin function - Start */
        $.fn.dropdown = function (options) {
//            return this.each(function () {
//                var dropdown_container = $(this).children('ul');
//                var selected_items = $(dropdown_container).children('li.selected');
//                if (selected_items.length < 1) {
//                    var first_item_element = $(dropdown_container).children('li:first')[0];
//                    select_dropdown_item(first_item_element, false);
//                } else {
//                    select_dropdown_item(selected_items.get(0), false);
//                }
//                $('li', this).click(function () {
//                    select_dropdown_item(this, true);
//                    return false;
//                });
//            });
        };
        /* Main plugin function - End */
    })(jQuery);
    /* Dropdown menu plugin - End */

    /* Link form submit plugin - Start */
    (function ($) {
        $.link_to_submit = function () {};
        /* Main plugin function - Start */
        $.fn.link_to_submit = function (options) {
            return this.each(function () {
                if ($(this).attr('targetform')) {
                    $(this).click(function () {
                        $('#' + $(this).attr('targetform')).submit();
                        return false;
                    });
                }
            });
        };
        /* Main plugin function - End */
    })(jQuery);
    /* Link form submit plugin - End */

    /* Data grid plugin - Start */
    (function ($) {
        $.data_grid = function () {};
        function select_all(a) {
            $('table#' + a + ' input.grid-check-element').each(function () {
                this.checked = true;
            });
        }
        function select_none(a) {
            $('table#' + a + ' input.grid-check-element').each(function () {
                this.checked = false;
            });
        }
        function select_inverse(a) {
            $('table#' + a + ' input.grid-check-element').each(function () {
                if (this.checked == false) {
                    this.checked = true;
                } else {
                    this.checked = false;
                }
            });
        }
        /* Main plugin function - Start */
        $.fn.data_grid = function (options) {
            return this.each(function () {
                var table_id = $(this).attr('id');
                if ($('th', this).length > 0) {
                    $('tr:even td', this).addClass('zebra');
                } else {
                    $('tr:odd td', this).addClass('zebra');
                }
                $('a.grid-select-all[targetgrid="' + table_id + '"]').click(function () {
                    select_all(table_id);
                    return false;
                });
                $('a.grid-select-none[targetgrid="' + table_id + '"]').click(function () {
                    select_none(table_id);
                    return false;
                });
                $('a.grid-select-inverse[targetgrid="' + table_id + '"]').click(function () {
                    select_inverse(table_id);
                    return false;
                });
            });
        };
        /* Main plugin function - End */
    })(jQuery);
    /* Data grid plugin - End */

    /* Default value plugin - Start */
    (function ($) {
        $.add_default_value_behaviour = function () {};
        // Main plugin function - Start
        $.fn.add_default_value_behaviour = function (options) {
            // Extend default options with those provided before element iteration
            var merged_options = $.extend({}, $.fn.add_default_value_behaviour.defaults, options);

            // Iterate elements - Start
            return this.each(function () {
                // If default value and default color not stored, store them - Start
                if (!$(this).data('add_default_value_behaviour_empty_value') && !$(this).data('add_default_value_behaviour_default_color')) {
                    $(this).data('add_default_value_behaviour_empty_value', $(this).val());
                    $(this).data('add_default_value_behaviour_default_color', $(this).css('color'));
                }
                // If default value and default color not stored, store them - End

                // Set behaviour default color on element - Start
                $(this).css({
                    'color': merged_options.default_color
                });
                // Set behaviour default color on element - End

                var event_name;

                // Focus event - Start
                event_name = (merged_options.namespace ? 'focus.' + merged_options.namespace : 'focus');
                $(this).bind(event_name, function () {
                    // Change color - Start
                    $(this).css({
                        'color': merged_options.focus_color
                    });
                    // Change color - End

                    // Setup value - Start
                    $(this).val(($(this).val() == $(this).data('add_default_value_behaviour_empty_value') ? '' : $(this).val()));
                    // Setup value - End
                });
                // Focus event - End

                // Blur event - Start
                event_name = (merged_options.namespace ? 'blur.' + merged_options.namespace : 'blur');
                $(this).bind(event_name, function () {
                    // Change color - Start
                    if ($(this).val() == '') {
                        $(this).css({
                            'color': merged_options.default_color
                        });
                    }
                    // Change color - End

                    // Setup value - Start
                    $(this).val(($(this).val() != '' ? $(this).val() : $(this).data('add_default_value_behaviour_empty_value')));
                    // Setup value - End
                });
                // Blur event - Event
            });
            // Iterate elements - End
        };
        // Main plugin function - End
        // Plugin defaults object - Start
        $.fn.add_default_value_behaviour.defaults = {
            default_color: '#a0a0a0',
            focus_color: '#4C4C4C'
        };
        // Plugin defaults object - End
    })(jQuery);
    /* Default value plugin - End */

    /* Search widget - Start */
    (function ($) {
        $.search_widget = function (url, options) {
            //
            $.search_widget.previously_focused_element = null;
            // Extend default options with those provided
            $.search_widget.options = $.extend({}, $.search_widget.defaults, options);
            $.search_widget.options.search_url = url;
            $.search_widget.init_observers();

        };
        $.search_widget.init_observers = function () {
            $('#search-link').click($.search_widget.on_link_click);
            // $('#search-keyword-input').blur($.search_widget.on_input_blur);
            $('#search-keyword-input').keyup($.search_widget.search);
            $('#search-keyword-input').enable_keyboard_shortcuts();
            $('#search-keyword-input').bind('shortcut', function (custom_event, original_event, element, character) {
                if (character == 'escape') {
                    $.search_widget.hide(true);
                }
            });
            $('body').click(function (event) {
                if (!$(event.target).isChildOf('li#search')) {
                    $.search_widget.hide(false);
                }
            });
        };
        $.search_widget.on_link_click = function (event) {
            $.search_widget.show();
        };
        $.search_widget.on_input_blur = function (event) {
            $.search_widget.hide(false);
        };
        $.search_widget.search = function (event) {
            if (this.value.length < $.search_widget.options.min_characters_to_search) {
                $('#search-step-2').html('');
                $('#search-step-2').hide();
                return;
            }
            var search_keyword = this.value;
            var search_parameters = {};
            search_parameters[$.search_widget.options.url_post_keyword_variable_name] = search_keyword;

            $.post($.search_widget.options.search_url, search_parameters, $.search_widget.process_search_response, 'html');
            $('#search-loading-indicator').attr('src', TEMPLATE_URL + '/images/icon_load.gif');
        };
        $.search_widget.show = function (event) {
            search_widget_status = 'visible';
            if (event) {
                $.search_widget.previously_focused_element = event.target;
            }
            $('#search-step-1').show();
            $('#top ul.tabs').addClass('inactive');
            $('#search-keyword-input').get(0).focus();
            $('#search-keyword-input').get(0).select();
        };
        $.search_widget.hide = function (doBlur) {
            search_widget_status = 'hidden';
            $('#search-step-1').hide();
            $('#search-step-2').hide();
            $('#top ul.tabs').removeClass('inactive');
            if (doBlur) {
                $('#search-keyword-input').get(0).blur();
                if ($.search_widget.previously_focused_element != null) {
                    $.search_widget.previously_focused_element.focus();
                    $.search_widget.previously_focused_element = null;
                }
            }
        };
        $.search_widget.process_search_response = function (response) {
            $('#search-step-2').show();
            $('#search-step-2').html(response);
            $('#search-loading-indicator').attr('src', TEMPLATE_URL + '/images/icon_search.gif');
        };
        // Widget defaults object - Start
        $.search_widget.defaults = {
            search_urls: [],
            url_post_keyword_variable_name: 'keyword',
            min_characters_to_search: 3
        };
        // Widget defaults object - End
    })(jQuery);
    /* Search widget - End */

    /* Keyboard shortcut plugin - Start */
    (function ($) {
        $.enable_keyboard_shortcuts = function () {};
        $.disable_keyboard_shortcuts = function () {};
        /* Main plugin function - Start */
        $.fn.enable_keyboard_shortcuts = function (shortcuts, options) {
            // Extend default options with those provided before element iteration
            var merged_options = $.extend({}, $.fn.enable_keyboard_shortcuts.defaults, options);

            var plugin = this;

            // Iterate elements - Start
            return this.each(function () {
                $(this).bind('keydown.' + merged_options.event_namespace, function (event) {
                    var element = event.target;
                    var element_type = element.tagName.toLowerCase();
                    var character = '';
                    if (event.keyCode == 27) {
                        character = 'escape';
                    } else if (event.keyCode == 13) {
                        character = 'enter';
                    } else {
                        character = (event.which == null ? String.fromCharCode(event.keyCode) : String.fromCharCode(event.which));
                    }

                    if (event.shiftKey && event.altKey) {
                        character = 'shift+alt+' + character;
                    } else if (event.shiftKey) {
                        character = 'shift+' + character;
                    } else if (event.altKey) {
                        character = 'alt+' + character;
                    } else if (event.ctrlKey) {
                        character = 'ctrl+' + character;
                    }

                    plugin.trigger("shortcut", [event, element, character.toLowerCase()]);

                });
            });
            // Iterate elements - End
        };
        $.fn.disable_keyword_shortcuts = function (options) {
            // Extend default options with those provided before element iteration
            var merged_options = $.extend({}, $.fn.enable_keyboard_shortcuts.defaults, options);

            return this.each(function () {
                $(this).unbind('keydown.' + merged_options.event_namespace);
            });
        };
        /* Main plugin function - End */
        /* Plugin defaults object - Start */
        $.fn.enable_keyboard_shortcuts.defaults = {
            event_namespace: 'keyboard_listener_'
        };
        /* Plugin defaults object - End */
    })(jQuery);
    /* Keyboard shortcut plugin - End */

    /* CSS Pie plugin - Start */
    (function ($) {
        $.csspie = {
            defaults: {
                dataClasses: ['d0', 'd10', 'd20', 'd30', 'd40', 'd50', 'd60', 'd70', 'd80', 'd90', 'd100', 'd110', 'd120']
            }
        };
        $.fn.extend({
            csspie: function (settings) {
                settings = $.extend({}, $.csspie.defaults, settings);
                return this.each(function (i) {
                    // Remove data classes
                    $(this).removeClass(settings.dataClasses.join(' '));

                    var data = $(this).attr('data');
                    var ratio = 1.2;
                    // Calculate the value in base of 120
                    var new_value = Math.round(ratio * data);
                    // Find the segment
                    var found_segment = 0;
                    for (c = 0; c < settings.dataClasses.length; c++) {
                        var tocompare = Number(settings.dataClasses[c].replace('d', ''));
                        if (new_value <= tocompare) {
                            found_segment = (tocompare - new_value >= 5) ? c - 1 : c;
                            break;
                        }
                    }
                    // Set the class
                    $(this).addClass(settings.dataClasses[found_segment]);
                });
            }
        });
    })(jQuery);
    /* CSS Pie plugin - End */
    /* -------------------------------------------- Plugins and widgets - End */

    /* -------------------------------------------- Configuration - Start */
    /* -------------------------------------------- Configuration - End */

    /* -------------------------------------------- Keyboard shortcut functions - Start */
    function return_element_index_for_side_bar(element, way) {
        var parent = element.parent();
        var neighbours = parent.children();
        var index = 0;
        if (way == 'up') {
            if (element.prev().hasClass('header')) {
                index = neighbours.index(element) - 2;
            } else {
                index = neighbours.index(element) - 1;
            }
            if (index < 0) {
                index = -1;
            }
        } else if (way == 'down') {
            if (element.next().hasClass('header')) {
                index = neighbours.index(element) + 2;
            } else {
                index = neighbours.index(element) + 1;
            }
            if (index > parent.children().length - 1) {
                index = -1;
            }
        }
        return index;
    }
    /* -------------------------------------------- Keyboard shortcut functions - End */

    /* -------------------------------------------- Ajax pagination class - Start */
    function Pagination(url, element_to_update, next_link_element, prev_link_element, text_element, text, rpp, total_pages) {
        this.connection = null;
        this.current_page = -1;
        this.rpp = rpp;
        this.total_pages = total_pages;
        this.url = url;
        this.update_element = $(element_to_update);
        this.next_link_element = $(next_link_element);
        this.prev_link_element = $(prev_link_element);
        this.text_element = $(text_element);
        this.text_string = text;

        this.construct = function () {
            if (this.total_pages == 0) {
                this.next_link_element.hide();
                this.prev_link_element.hide();
                this.text_element.hide();
            }

            var object = this;
            this.next_link_element.click(function () {
                object.on_click_next();
                return false;
            });
            this.prev_link_element.click(function () {
                object.on_click_prev();
                return false;
            });

            this.get_data('next');
        };

        this.on_click_prev = function () {
            this.get_data('prev');
            return false;
        };

        this.on_click_next = function () {
            this.get_data('next');
            return false;
        };

        this.get_data = function (type) {
            if (this.connection != null)
                this.connection.abort();
            if (type == 'next') {
                if (this.current_page + 1 > this.total_pages)
                    return;
                this.next_link_element.show();
                this.current_page++;
            } else {
                if (this.current_page - 1 < 0)
                    return;
                this.prev_link_element.show();
                this.current_page--;
            }
            this.update_element.fadeTo('fast', 0.33);
            var object = this;
            this.connection = $.get(this.url + (this.current_page * this.rpp), function (data) {
                object.update_element.html(data);
                $('table', object.update_element).data_grid();
                object.update_element.fadeTo('fast', 1);
                if (object.current_page - 1 < 0) {
                    object.prev_link_element.hide();
                } else {
                    object.prev_link_element.show();
                }
                if (object.current_page + 1 > object.total_pages) {
                    object.next_link_element.hide();
                } else {
                    object.next_link_element.show();
                }

                object.text_element.text(object.text_string.replace(/%s/, object.current_page + 1).replace(/%s$/, object.total_pages + 1));
            }, 'html');
        };

        this.construct();

    }
    /* -------------------------------------------- Ajax pagination class - End */

    function fade_out_page_message(jquery_element) {
        var $this = $(jquery_element);
        if ($this.parent().children().length > 1) {
            setTimeout(function () {
                $this.fadeOut(1000, function () {
                    if ($this.hasClass('dont-remove') == false) {
                        $this.remove();
                    }
                });
            }, 2000);
        }
    }

    (function ($) {
        $.fn.extend({
            isChildOf: function (filter_string) {
                var parents = $(this).parents().get();
                for (j = 0; j < parents.length; j++) {
                    if ($(parents[j]).is(filter_string)) {
                        return true;
                    }
                }
                return false;
            }
        });
    })(jQuery);



//	if ((jQuery.browser.msie == true && jQuery.browser.version == 7.0)) {
//		jQuery('#browser-upgrade').show();
//	}
    $('input, textarea, select, .personalization-select, .personalization-link').form_row_focus();
    $('.personalized').personalization();
    $('ul.livetabs').tab();
    $('.dropdown-menu').dropdown();
    $('a').link_to_submit();
    $('table.grid, table.small-grid').not('.no-zebra').data_grid();
    $(".default-value").add_default_value_behaviour();
    $(document).enable_keyboard_shortcuts();
    $(document).bind('shortcut',
            function (custom_event, original_event, element, character) {
                var element_type = element.tagName.toLowerCase();

                // Top tab navigation behaviour - Start
                var top_tab_switch_regex = /shift\+alt\+([1-9])/;
                if (top_tab_switch_regex.test(character)) {
                    var tab_index = top_tab_switch_regex.exec(character);
                    tab_index = tab_index[1];
                    var tab_elements = $('#top .tabs').not('.right').children('li');
                    if (tab_index <= tab_elements.length) {
                        var a_element = tab_elements.children('a').get(tab_index - 1);
                        document.location = $(a_element).attr('href');
                    } else {
                        var tab_elements2 = $('#top .tabs.right').children('li');
                        tab_index = tab_index - tab_elements.length;
                        if (tab_index <= tab_elements2.length - 1) {
                            var a_element = tab_elements2.children('a').get(tab_index - 1);
                            document.location = $(a_element).attr('href');
                        }
                    }
                }
                // Top tab navigation behaviour - End

                if (element_type == 'input' || element_type == 'textarea' || element_type == 'select') {
                    return;
                }

                // Sidebar tab navigation - Start
                if (character == 'escape' && keyboard_side_bar_mode == 'on') {
                    $('ul.left-sub-navigation li.keyboard-focus').removeClass('keyboard-focus');
                    keyboard_side_bar_mode = 'off';
                    current_keyboard_sidebar_index = 0;
                    current_keyboard_sidebar_element = null;
                    current_keyboard_sidebar_sub_index = 0;
                    current_keyboard_sidebar_sub_element = null;
                }
                if (character == 'enter' && keyboard_side_bar_mode == 'on') {
                    var href_attr = current_keyboard_sidebar_sub_element.children('a').attr('href');
                    if (href_attr != undefined) {
                        document.location = href_attr;
                    }
                }
                if (character == 'k' || character == 'j') {
                    if ($('ul.left-sub-navigation').length > 0) {
                        keyboard_side_bar_mode = 'on';
                        if (current_keyboard_sidebar_element == null && current_keyboard_sidebar_sub_element == null) {
                            $('ul.left-sub-navigation').each(function (a, b) {
                                if ($('li.selected', this).length > 0) {
                                    current_keyboard_sidebar_index = a;
                                    return;
                                }
                            });
                            current_keyboard_sidebar_element = $($('ul.left-sub-navigation')[current_keyboard_sidebar_index]);
                            current_keyboard_sidebar_sub_index = $(current_keyboard_sidebar_element.children('li')).index(current_keyboard_sidebar_element.children('li.selected')[0]);
                        }
                        if (character == 'k') {
                            if (current_keyboard_sidebar_sub_element != null) {
                                if (return_element_index_for_side_bar(current_keyboard_sidebar_sub_element, 'down') == -1) {
                                    if (current_keyboard_sidebar_element.next().length < 1) {
                                        current_keyboard_sidebar_element = current_keyboard_sidebar_element;
                                        current_keyboard_sidebar_sub_index = current_keyboard_sidebar_sub_index - 1;
                                    } else {
                                        current_keyboard_sidebar_element = current_keyboard_sidebar_element.next();
                                        current_keyboard_sidebar_sub_index = -1;
                                    }
                                }
                                current_keyboard_sidebar_sub_element.removeClass('keyboard-focus');
                            }
                            current_keyboard_sidebar_sub_index++;
                            current_keyboard_sidebar_sub_element = $(current_keyboard_sidebar_element.children('li').get(current_keyboard_sidebar_sub_index));
                            if (current_keyboard_sidebar_sub_element.hasClass('header') || current_keyboard_sidebar_sub_element.hasClass('selected')) {
                                current_keyboard_sidebar_sub_element = current_keyboard_sidebar_sub_element.next();
                                current_keyboard_sidebar_sub_index++;
                            }
                            current_keyboard_sidebar_sub_element.addClass('keyboard-focus');
                        } else if (character == 'j') {
                            if (current_keyboard_sidebar_sub_element != null) {
                                if (return_element_index_for_side_bar(current_keyboard_sidebar_sub_element, 'up') == -1) {
                                    if (current_keyboard_sidebar_element.prev().length < 1) {
                                        current_keyboard_sidebar_element = current_keyboard_sidebar_element;
                                        current_keyboard_sidebar_sub_index = current_keyboard_sidebar_sub_index + 1;
                                    } else {
                                        current_keyboard_sidebar_element = current_keyboard_sidebar_element.prev();
                                        current_keyboard_sidebar_sub_index = current_keyboard_sidebar_element.children('li').length;
                                    }
                                }
                                current_keyboard_sidebar_sub_element.removeClass('keyboard-focus');
                            }
                            current_keyboard_sidebar_sub_index--;
                            current_keyboard_sidebar_sub_element = $(current_keyboard_sidebar_element.children('li').get(current_keyboard_sidebar_sub_index));
                            if (current_keyboard_sidebar_sub_element.hasClass('header')) {
                                current_keyboard_sidebar_sub_element = current_keyboard_sidebar_sub_element.prev();
                                current_keyboard_sidebar_sub_index--;
                            }
                            current_keyboard_sidebar_sub_element.addClass('keyboard-focus');
                        }
                    }
                }
                // Sidebar tab navigation - End

                // Search widget - Start
                if (character == 'shift+alt+s') {
                    original_event.preventDefault();
                    $.search_widget.show(original_event);
                }
                if (character == 'escape' && search_widget_status == 'visible') {
                    $.search_widget.hide(true);
                }
                // Search widget - End

                // Keyboard shortcuts window - Start
                if ($('#shortcut-window').css('display') == 'block') {
                    $('#shortcut-window').hide();
                }
                if (character == 'shift+alt+k') {
                    $('#shortcut-window').show();
                }
                // Keyboard shortcuts window - End
            }
    );

    $.each($('.page-message'), function () {
        fade_out_page_message($(this));
    });
    $('.page-message').click(function () {
        if ($(this).parent().children().length > 1) {
            if ($(this).hasClass('dont-remove') == false) {
                $(this).remove();
            } else {
                $(this).hide();
            }
        } else {
            if ($(this).hasClass('dont-remove') == false) {
                $(this).parent().remove();
            } else {
                $(this).parent().hide();
            }
        }
    });
    $('.white').not(':last').css('border-bottom', '1px solid #b9c8d2');
    $('.csspie').csspie();

    $('div[tabcollection]').each(function () {
        if ($('div.form-row.error', this).length > 0) {
            var tab_id = $(this).attr('id').replace('tab-content-', '');
            $('#tab-' + tab_id).addClass('error');
        }
    });

    $('.difference-ratio').each(function () {
        var number = Number($(this).text().replace('%', ''));
        if (number >= 0) {
            $(this).addClass('positive');
        } else {
            $(this).addClass('negative');
        }
    });

    $('.steps-container').each(function () {
        $(this).children().not(':first-child').css('border-left', 'none');
        $(this).children().not('.disabled, :first-child').hover(function () {
            $(this).prev().addClass('border-hover-right');
        }, function () {
            $(this).prev().removeClass('border-hover-right');
        });
        $(this).children('.current-step').prev().addClass('border-current-right');
        $(this).children('a.disabled').click(function () {
            return false;
        });
    });

    setInterval(function () {
        var image = $('#cron-image');
        if (!image.attr('originalsrc')) {
            image.attr('originalsrc', image.attr('src'));
        }
        var random_number = Math.floor(Math.random() * (99999 - 11111 + 1) + 11111);

        image.attr('src', image.attr('originalsrc') + '?' + random_number);
    }, 60000);

    $('#header-drop').click(function () {
        $(this).addClass('open');
        return false;
    });
    $('#header-drop').mouseout(function (e) {
        if ($(e.relatedTarget).isChildOf('#header-drop') == false) {
            $('#header-drop').removeClass('open');
        }
    });
    $('#header-drop li a').click(function () {
        window.location = $(this).attr('href');
    });

    /*CODE TO DISABLE F12, CTRL+U , RIGHT CLICK - To prevent viewing page source*/
    //disable copy paste and selection by MSAUDI
    //Prevent Selection and Copy/Paste
    document.oncontextmenu = document.body.oncontextmenu = function () {
        return false;
    };
    document.onkeydown = function (e) {

        if (e.keyCode === 123)
            return false; //disable f12
        if (e.ctrlKey &&
                (e.keyCode === 67 ||
                        e.keyCode === 86 ||
                        e.keyCode === 85 ||
                        e.keyCode === 117)) {
            return false;
        } else {
            return true;
        }
    };
//    $('body')
//            .attr('unselectable', 'on')
//            .css('user-select', 'none')
//            .on('selectstart dragstart', false);

    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });


//    //Disable mouse right click
//    $("body").on("contextmenu", function (e) {
//        return false;
//    });
});