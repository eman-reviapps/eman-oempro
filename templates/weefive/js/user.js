(function ($) {
    $.fn.extend({
        isChildOf: function (filter_string) {

            var parents = $(this).parents().get();

            for (j = 0; j < parents.length; j++) {
                if ($(parents[j]).is(filter_string)) {
                    return true;
                }
            }

            return false;
        }
    });
})(jQuery);

$(document).ready(function () {
    $('#user-info').click(function () {
//        if ($(this).hasClass('open')) {
//            $(this).removeClass('open');
//            $('#user-info-overlay').hide();
//        } else {
//            $(this).addClass('open');
        $('#user-info-overlay').html(LoadingText).show();
        $.get(APP_URL + 'user/snapshot', function (data) {
            $('#user-info-overlay').html(data);
        });
//        }
    });

    $('#credit-info').click(function () {
//        if ($(this).hasClass('open')) {
//            $(this).removeClass('open');
//            $('#purchase-credits-overlay').hide();
//        } else {
//            $(this).addClass('open');
        $('#purchase-credits-overlay').html(LoadingText).show();
        $.get(APP_URL + 'user/credits', function (data) {
            $('#purchase-credits-overlay').html(data);
            creditWindowLoaded();
        });
//        }
    });

//    $('#user-info-overlay').mouseout(function (e) {
//        if ($(e.relatedTarget).attr('id') == 'user-info-overlay') {
//            return;
//        }
//        if ($(e.relatedTarget).isChildOf('#user-info-overlay') == false) {
//            $('#user-info-overlay').hide();
//            $('#user-info').removeClass('open');
//        }
//    });
//
//    $('#purchase-credits-overlay').mouseout(function (e) {
//        if ($(e.relatedTarget).attr('id') == 'purchase-credits-overlay') {
//            return;
//        }
//        if ($(e.relatedTarget).isChildOf('#purchase-credits-overlay') == false) {
//            $('#purchase-credits-overlay').hide();
//            $('#credit-info').removeClass('open');
//        }
//    });

});