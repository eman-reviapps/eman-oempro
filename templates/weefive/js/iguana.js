var iguana = {
	iframe_selector				: '#email-source',
	iframe_body					: null,
	context_menu				: null,
	context_menu_inner			: null,
	properties_panel_status		: 'hidden',
	current_element				: null,
	last_syncto_id				: 0,
	last_syncfrom_id			: 0,
	last_linkto_id				: 0,
	last_linkfrom_id			: 0,
	config						: {
		attributes				: {
			syncode		: 'iguana_syncode',
			linkcode	: 'iguana_linkcode',
			syncto		: 'iguana_sync_source',
			syncfrom	: 'iguana_sync_target',
			linkto		: 'iguana_link_source',
			linkfrom	: 'iguana_link_target'
		},
		classes					: {
			iguana					: 'iguana',
			iguana_menu				: 'iguana-menu',
			iguana_menu_plain_edit	: 'iguana-menu-plain-edit',
			iguana_menu_duplicate	: 'iguana-menu-duplicate',
			iguana_menu_remove		: 'iguana-menu-remove',
			iguana_menu_move_up		: 'iguana-menu-move-up',
			iguana_menu_move_down	: 'iguana-menu-move-down',
			plain_editable			: 'iguana-plain-editable',
			rich_editable			: 'iguana-rich-editable',
			single_editable			: 'iguana-single-editable',
			duplicatable			: 'iguana-duplicatable',
			removable				: 'iguana-removable',
			movable					: 'iguana-movable',
			block_hover				: 'block-hover',
			sync_to					: 'iguana-sync-to'
		},
		ids						: {
			context_menu			: 'iguana-context-menu',
			context_menu_inner		: 'iguana-context-menu-inner'
		}
	},
	init						: function() {
		$("iframe").attr('src', EMAIL_CONTENT_URL).load(iguana.on_frame_load);
		iguana.init_main_events();
	},
	init_main_events			: function() {
		$('#single-editable-text-input').keyup(function () {
			iguana.update_element_content($(this).val());
		});
		$('#plain-editable-text-input').keyup(function () {
			iguana.update_element_content($(this).val());
		});
		$('#hide-panel-button').click(iguana.hide_panel);
		$('#save-content-form').submit(iguana.on_save_click);
	},
	init_frame_events			: function() {
		/* Observe click event on body element - Start */
		$('*', iguana.iframe_body).mousedown(function (event, a) {
			event.stopPropagation();

			if ($(event.target).attr('id') != iguana.config.ids.context_menu && $(event.target).parents('#'+iguana.config.ids.context_menu).length < 1) {
				/* If click is not on toolbar, clear menu and hide toolbar - Start */
				iguana.hide_context_menu();
				var offset = 0;
				if ($.browser.msie) {
					offset = $(iguana.iframe_body).scrollTop();
				}
				iguana.context_menu.css({
					top: event.pageY-10+offset+'px',
					left: event.pageX-35+'px'
				});
				/* If click is not on toolbar, clear menu and hide toolbar - End */
			}
			
			/* If event element has iguana class, show context menu - Start */
			if ($(event.target).hasClass(iguana.config.classes.iguana)) {
				iguana.update_context_menu($(event.target).attr('class').split(' '), $(event.target));
				iguana.context_menu.show();
			}
			/* If event element has iguana class, show context menu - End */
			
			/* If event element's parent elements have iguana class, show context menu - Start */
			if ($(event.target).parents('.'+iguana.config.classes.iguana).length > 0) {
				iguana.context_menu.show();
			}
			$(event.target).parents('.'+iguana.config.classes.iguana).each(function () {
				iguana.update_context_menu($(this).attr('class').split(' '), $(this));
			});
			/* If event element's parent elements have iguana class, show context menu - End */
		});
		/* Observe click event on body element - End */

		/* Observe hover event on all elements that have class iguana - Start */
		$('.'+iguana.config.classes.iguana).hover(
			function () {
				/* If event element has syncto attribute, add syncto class to synced element - Start */
				if ($(this).attr(iguana.config.attributes.syncto)) {
					$('.'+iguana.config.classes.iguana+'['+iguana.config.attributes.syncfrom+'=\''+$(this).attr(iguana.config.attributes.syncto)+'\']').addClass(iguana.config.classes.sync_to);
				}
				/* If event element has syncto attribute, add syncto class to synced element - End */
			},
			function () {
				/* If event element has syncto attribute, remove syncto class from synced element - Start */
				if ($(this).attr(iguana.config.attributes.syncto)) {
					$('.'+iguana.config.classes.iguana+'['+iguana.config.attributes.syncfrom+'=\''+$(this).attr(iguana.config.attributes.syncto)+'\']').removeClass(iguana.config.classes.sync_to);
				}
				/* If event element has syncto attribute, remove syncto class from synced element - End */
			}
		);
		/* Observe hover event on all elements that have class iguana - End */
		
		/* Disable link clicks - Start */
		$('a', iguana.iframe_body).click(function() { return false; });
		/* Disable link clicks - End */
	},
	create_context_menu			: function() {
		$(iguana.iframe_body).append('<div id="'+iguana.config.ids.context_menu+'"></div>');
		iguana.context_menu = $('#'+iguana.config.ids.context_menu, iguana.iframe_body);
		iguana.context_menu.hide();
		iguana.context_menu.append('<div id="'+iguana.config.ids.context_menu_inner+'"></div>');
		iguana.context_menu_inner = $('#'+iguana.config.ids.context_menu_inner, iguana.iframe_body);

		// If mouse leaves menu, hide menu - Start
		iguana.context_menu_inner.bind('mouseleave', function () {
			iguana.hide_context_menu();
			$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);
		});
		// If mouse leaves menu, hide menu - End
		
		// Bind event handlers for menu buttons - Start
		$(iguana.context_menu).click(iguana.on_context_menu_click);
		$(iguana.context_menu).mousemove(iguana.on_context_menu_mouse_move);
		// Bind event handlers for menu buttons - End
	},
	hide_context_menu			: function() {
		iguana.context_menu.hide();
		iguana.clear_context_menu();
	},
	clear_context_menu			: function() {
		iguana.context_menu_inner.html('');
	},
	update_context_menu			: function (array_classes, element) {
		var section = $('<div class="section"></div>');
		iguana.context_menu_inner.append(section);
		for (var i=0,length=array_classes.length;i<length;i++) {
			if (array_classes[i] == iguana.config.classes.duplicatable) {
				var button = $('<a href="#" class="iguana-menu-button '+iguana.config.classes.iguana_menu_duplicate+'">'+Language['0003']+'</a>').data('iguana_block', $(element));
				section.append(button);
				
				if (element.prev('.'+iguana.config.classes.duplicatable).length > 0) {
					var button = $('<a href="#" class="iguana-menu-button '+iguana.config.classes.iguana_menu_move_up+'">'+Language['0004']+'</a>').data('iguana_block', $(element));
					section.append(button);
				}

				if (element.next('.'+iguana.config.classes.duplicatable).length > 0) {
					var button = $('<a href="#" class="iguana-menu-button '+iguana.config.classes.iguana_menu_move_down+'">'+Language['0005']+'</a>').data('iguana_block', $(element));
					section.append(button);
				}

				if ($('.'+iguana.config.classes.duplicatable, $(element).parent()).length > 1) {
					var button = $('<a href="#" class="iguana-menu-button '+iguana.config.classes.iguana_menu_remove+'">'+Language['0006']+'</a>').data('iguana_block', $(element));
					section.append(button);
				}
			} else if (array_classes[i] == iguana.config.classes.plain_editable || array_classes[i] == iguana.config.classes.rich_editable || array_classes[i] == iguana.config.classes.single_editable) {
				var button = $('<a href="#" class="iguana-menu-button '+iguana.config.classes.iguana_menu_plain_edit+'">'+Language['0007']+'</a>').data('iguana_block', $(element));
				section.append(button);
			} else if (array_classes[i] == iguana.config.classes.removable) {
			}
		}
	},
	get_frame_document			: function() {
		var documentElement;
		if ($(iguana.iframe_selector).get(0).contentDocument) {
			documentElement = $(iguana.iframe_selector).get(0).contentDocument.documentElement;
		} else if ($(iguana.iframe_selector).get(0).contentWindow) {
			documentElement = $(iguana.iframe_selector).get(0).contentWindow.document;
		}
		return documentElement;
	},
	get_document_html			: function(d) {
		if (! d.innerHTML) {
			return d.all[0].innerHTML;
		} else {
			return d.innerHTML;
		}
	},
	on_frame_load				: function() {
		// Get frame body
		iguana.iframe_body = $(iguana.iframe_selector).contents().find("body");
		// Add styling to frame
		iguana.iframe_body.prepend('<link id="iguana-stylesheet" rel="stylesheet" href="'+IGUANA_CSS_URL+'" type="text/css" media="screen, projection">');
		// Init event handlers
		iguana.init_frame_events();
		// Create context menu
		iguana.create_context_menu();

		var documentElement = iguana.get_frame_document();

		var frame_html = '<html>'+iguana.get_document_html(documentElement)+'</html>';
	},
	on_context_menu_click		: function(event) {
		event.preventDefault();
		
		var element			= $(event.target);
		var target_element	= element.data('iguana_block');

		iguana.hide_context_menu();

		// Click on edit menu - Start
		if (element.hasClass(iguana.config.classes.iguana_menu_plain_edit)) {
			iguana.element_edit(target_element);
		}
		// Click on edit menu - End
		
		// Click on duplicate menu - Start
		else if (element.hasClass(iguana.config.classes.iguana_menu_duplicate)) {
			iguana.element_duplicate(target_element);
		}
		// Click on duplicate menu - End
		
		// Click on remove menu - Start
		else if (element.hasClass(iguana.config.classes.iguana_menu_remove)) {
			iguana.element_remove(target_element);
		}
		// Click on remove menu - End
		
		// Click on move up menu - Start
		else if (element.hasClass(iguana.config.classes.iguana_menu_move_up)) {
			iguana.element_move_up(target_element);
		}
		// Click on move up menu - End
		
		// Click on move down menu - Start
		else if (element.hasClass(iguana.config.classes.iguana_menu_move_down)) {
			iguana.element_move_down(target_element);
		}
		// Click on move down menu - End
		
		return false;
	},
	on_context_menu_mouse_move	: function(event) {
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);
		if ($(event.target).data('iguana_block')) {
			$(event.target).data('iguana_block').addClass(iguana.config.classes.block_hover);
		}
	},
	on_context_menu_mouse_out	: function(event) {
		$(event.target).data('iguana_block').removeClass(iguana.config.classes.block_hover);
	},
	on_save_click							: function() {
		iguana.clean_up_frame_body();

		var documentElement = iguana.get_frame_document();
		var frame_html = '<html>'+iguana.get_document_html(documentElement)+'</html>';

		if ($(iguana.iframe_selector).get(0).contentDocument) {
			domDocument = $(iguana.iframe_selector).get(0).contentDocument;
		} else if ($(iguana.iframe_selector).get(0).contentWindow) {
			domDocument = $(iguana.iframe_selector).get(0).contentWindow;
		}

		var doctype_string = '';
		if (doctype_object = domDocument.doctype) {
			doctype_string = '<!DOCTYPE ';
			doctype_string += doctype_object.name.toUpperCase() + ' ';
			doctype_string += 'PUBLIC "' + doctype_object.publicId + '" ';
			doctype_string += '"'+doctype_object.systemId+'">' + "\n";
		}

		$('#frame-html-container-for-post').text(doctype_string + frame_html);

		return true;
	},
	clean_up_frame_body						: function() {
		$('*', iguana.iframe_body).removeClass('element-selected element-selected-sync-link element-mouse-over-sync-link element-mouse-over');
		$('#iguana-stylesheet', iguana.iframe_body).remove();
		$('#iguana-context-menu', iguana.iframe_body).remove();
		$('title', iguana.iframe_body).remove();
	},
	element_edit				: function(element) {
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);
		iguana.current_element = element;
		// Rich editable - Start
		if (element.hasClass(iguana.config.classes.rich_editable)) {
			iguana.show_panel('rich');
		}
		// Rich editable - End
		
		// Plain editable - Start
		else if (element.hasClass(iguana.config.classes.plain_editable)) {
			iguana.show_panel('plain');
		}
		// Plain editable - End

		// Single editable - Start
		else if (element.hasClass(iguana.config.classes.single_editable)) {
			iguana.show_panel('single');
		}
		// Single editable - End
	},
	element_duplicate			: function(element) {
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);
		iguana.current_element = element;

		/* Duplicate block - Start */
		var new_element = element.clone(true);
		new_element.insertAfter(element);
		if (new_element.attr(iguana.config.attributes.linkto)) {
			new_element.attr(iguana.config.attributes.linkcode, new_element.attr(iguana.config.attributes.linkcode)+iguana.last_linkto_id++);
		}
		/* Duplicate block - End */

		/* If duplicated block has any target block, duplicate it too - Start */
		if (element.attr(iguana.config.attributes.linkto)) {
			var linked_element = $($('* ['+iguana.config.attributes.linkcode+'="'+element.attr(iguana.config.attributes.linkcode)+'"]', iguana.iframe_body).not($(element)).get(0));
			var new_linked_element = linked_element.clone(true);
			new_linked_element.insertAfter(linked_element);
			new_linked_element.attr(iguana.config.attributes.linkcode, new_linked_element.attr(iguana.config.attributes.linkcode)+iguana.last_linkfrom_id++);
		}
		/* If duplicated block has any target block, duplicate it too - End */

		/* If duplicated block has any elements that have synco attribute, incremenet attribute value by one - Start */
		if (new_element.attr(iguana.config.attributes.syncode)) {
			new_element.attr(iguana.config.attributes.syncode, new_element.attr(iguana.config.attributes.syncode)+iguana.last_syncto_id++);
		}
		
		$('['+iguana.config.attributes.syncode+']', new_element).each(function (a) {
			$(this).attr(iguana.config.attributes.syncode, $(this).attr(iguana.config.attributes.syncode)+iguana.last_syncto_id++);
		});
		/* If duplicated block has any elements that have synco attribute, incremenet attribute value by one - End */

		/* If duplicated target block has any elements that have syncfrom attribute, incremenet attribute value by one - Start */
		if (new_linked_element && new_linked_element.attr(iguana.config.attributes.syncode)) {
			new_linked_element.attr(iguana.config.attributes.syncode, new_linked_element.attr(iguana.config.attributes.syncode)+iguana.last_syncfrom_id++);
		}
		
		$('['+iguana.config.attributes.syncode+']', new_linked_element).each(function (a) {
			$(this).attr(iguana.config.attributes.syncode, $(this).attr(iguana.config.attributes.syncode)+iguana.last_syncfrom_id++);
		});
		/* If duplicated target block has any elements that have syncfrom attribute, incremenet attribute value by one - End */

		// Clear hover classes from new duplicated block and original block
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);

		// Hide menu
		iguana.hide_panel();
		iguana.hide_context_menu();
	},
	element_remove				: function(element) {
		// Do not allow to remove element if there is only 1 left
		if ($('.'+iguana.config.classes.duplicatable, $(element).parent()).length <= 1) {
			return false;
		}
		
		/* If selected block has any target blocks, remove them too  - Start */
		if (element.attr(iguana.config.attributes.linkto)) {
			$('* ['+iguana.config.attributes.linkfrom+'="'+element.attr(iguana.config.attributes.linkto)+'"]['+iguana.config.attributes.linkcode+'="'+element.attr(iguana.config.attributes.linkcode)+'"]', iguana.iframe_body).remove();
		}
		/* If selected block has any target blocks, remove them too  - End */

		// Remove selected block
		element.remove();
		
		// Hide menu
		iguana.hide_panel();
		iguana.hide_context_menu();
	},
	element_move_up				: function(element) {
		element.prev('.'+iguana.config.classes.duplicatable).before(element);

		/* If moved block has any target block, move it too - Start */
		if (element.attr(iguana.config.attributes.linkcode)) {
			var target_element = $($('* ['+iguana.config.attributes.linkcode+'="'+element.attr(iguana.config.attributes.linkcode)+'"]', iguana.iframe_body).not(element).get(0));
			target_element.prev('['+iguana.config.attributes.linkcode+']').before(target_element);
		}
		/* If moved block has any target block, move it too - End */

		// Clear hover classes from new duplicated block and original block
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);

		// Hide menu
		iguana.hide_panel();
		iguana.hide_context_menu();
	},
	element_move_down			: function(element) {
		element.next('.'+iguana.config.classes.duplicatable).after(element);

		/* If moved block has any target block, move it too - Start */
		if (element.attr(iguana.config.attributes.linkcode)) {
			var target_element = $($('* ['+iguana.config.attributes.linkcode+'="'+element.attr(iguana.config.attributes.linkcode)+'"]', iguana.iframe_body).not(element).get(0));
			target_element.next('['+iguana.config.attributes.linkcode+']').after(target_element);
		}
		/* If moved block has any target block, move it too - End */

		// Clear hover classes from new duplicated block and original block
		$('.'+iguana.config.classes.block_hover, iguana.iframe_body).removeClass(iguana.config.classes.block_hover);

		// Hide menu
		iguana.hide_panel();
		iguana.hide_context_menu();
	},
	show_panel					: function(type) {
		var panel_height = 0;
		// Single editable panel - Start
		if (type == 'single') {
			panel_height = 70;
			$('#single-editable-text-input').val(iguana.current_element.html());
			$('#single-editable-panel').show();
			$('#plain-editable-panel').hide();
			$('#rich-editable-panel').hide();
			$('#single-editable-text-input').focus();
		}
		// Single editable panel - End
		
		// Plain editable panel - Start
		else if (type == 'plain') {
			panel_height = 152;
			$('#plain-editable-text-input').val(iguana.current_element.html());
			$('#single-editable-panel').hide();
			$('#plain-editable-panel').show();
			$('#rich-editable-panel').hide();
			$('#plain-editable-text-input').focus();
		}
		// Plain editable panel - End
		
		// Rich editable panel - Start
		else if (type == 'rich') {
			panel_height = 300;
			$('#rich-editable-text-input').tinymce().setContent(iguana.current_element.html());
			$('#single-editable-panel').hide();
			$('#plain-editable-panel').hide();
			$('#rich-editable-panel').show();
			$('#rich-editable-text-input').focus();
		}
		// Rich editable panel - End
		$('body').css({marginBottom:panel_height+'px'});
		
		$('.panel-container').animate({height:panel_height}, 200);
		iguana.properties_panel_status = 'visible';
	},
	hide_panel					: function() {
		$('.panel-container').animate({height:0}, 200, function() {
			$('body').css({marginBottom:'0px'});
		});
		iguana.current_element = null;
	},
	update_element_content		: function(content) {
		if (iguana.current_element) {
	   		iguana.current_element.html(content);
			if (iguana.current_element.attr(iguana.config.attributes.syncode)) {
				$('* ['+iguana.config.attributes.syncode+'="'+iguana.current_element.attr(iguana.config.attributes.syncode)+'"]', iguana.iframe_body).not($(iguana.current_element)).html(content);
			}
		}
	}
};

$(document).ready(function () {
	iguana.init();
	$('#rich-editable-text-input').tinymce(tinymce_config);
	// $('#rich-editable-text-input').tinymce({
	// 			script_url : TEMPLATE_URL+'js/tiny_mce/tiny_mce.js',
	// 			auto_reset_designmode : true,
	// 			browsers: "msie,gecko,safari,opera",
	// 			dialog_type : "modal",
	// 			directionality : "ltr",
	// 			docs_language : "en",
	// 			mode : "exact",
	// 			language: "en",
	// 			nowrap : false,
	// 			// plugins : "fullscreen,inlinepopups,safari,table,SendloopMediaLibrary,xhtmlxtras",
	// 			plugins : "fullscreen,inlinepopups,safari,table,xhtmlxtras",
	// 			theme : 'advanced',
	// 			theme_advanced_toolbar_location : "top",
	// 			theme_advanced_toolbar_align : 'left',
	// 			theme_advanced_source_editor_wrap : true,
	// 			theme_advanced_statusbar_location : "bottom",
	// 			theme_advanced_buttons1 : "fullscreen,code,separator,bold,italic,underline,strikethrough,hr,separator,,justifyleft,justifycenter,justifyright,justifyfull,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,anchor,link,unlink,SendloopMediaLibrary,image,separator,cleanup,removeformat,separator,table,row_after,row_before,col_after,col_before,delete_col,delete_row,delete_table",
	// 			theme_advanced_buttons2 : "formatselect,,fontselect,fontsizeselect,separator,forecolor,backcolor",
	// 			theme_advanced_buttons3 : "",
	// 			theme_advanced_resizing : true,
	// 			theme_advanced_default_foreground_color : "#000",
	// 			theme_advanced_default_background_color : "#fff",
	// 			theme_advanced_more_colors : true,
	// 			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
	// 			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
	// 			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
	// 			table_cell_limit : 100,
	// 			table_row_limit : 5,
	// 			table_col_limit : 5,
	// 			entity_encoding : "raw",
	// 			apply_source_formatting : false,
	// 			cleanup : false,
	// 			convert_newlines_to_brs : false,
	// 			element_format : "xhtml",
	// 			fix_list_elements : false,
	// 			fix_table_elements : false,
	// 			fix_nesting : false,
	// 			font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
	// 			force_p_newlines : true,
	// 			force_hex_style_colors : true,
	// 			convert_urls : false,
	// 			preformatted : false,
	// 			relative_urls : false,
	// 			paste_auto_cleanup_on_paste : true,
	// 			// file_browser_callback : "SendloopMediaLibrary",
	// 			valid_child_elements: "table[tr|td]",
	// 			content_css : TEMPLATE_URL+"styles/tiny_mce.css",
	// 			setup : function(ed) {
	// 				ed.onKeyUp.add(function(ed, e) {
	// 					iguana.update_element_content(ed.getContent());
	// 				});
	// 				ed.onNodeChange.add(function(ed, cm, e) {
	// 					iguana.update_element_content(ed.getContent());
	// 				});
	// 			}
	// 		});
});