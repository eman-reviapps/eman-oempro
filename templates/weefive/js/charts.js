(function($) {
	var defaults = {
		type: '',
		settings: '',
		data: '',
		anim: false,
		tP: 10,
		bP: 20,
		lP: 20,
		rP: 20,
		pLC: '0077CC'
	};

	if (defaults.anim && ! window.requestAnimationFrame) defaults.anim = false;
	
	var hex2rgba = function(hex, opacity) {
		var rgb = hex.replace('#', '').match(/(.{2})/g), i = 3;
		while (i--) rgb[i] = parseInt(rgb[i], 16);
		return 'rgba(' + rgb.join(', ') + ', ' + opacity + ')';
	};

	var createCanvas = function($parent, width, height) {
		var $canvas = $('<canvas></canvas>');
		$canvas[0].width = width;
		$canvas[0].height = height;
		$parent.append($canvas);
		return $canvas;
	};

	var init = function($parent, options) {
		$('canvas', $parent).remove();
		$('.oempro-chart-labels', $parent).remove();
		var $canvas = createCanvas($parent, $parent.width(), $parent.height());
		var ctx = $canvas[0].getContext('2d');
		$parent.data('oempro_context', ctx).css('position', 'relative');
		$parent.append('<div class="oempro-chart-labels"></div>');
		$('.oempro-chart-labels', $parent).css({
			'poisition': 'absolute',
			'z-index': 99999999,
			'top':0,
			'left':0
		});
	};

	var loadData = function($parent, options, callback) {
		$.get(options.data, function(response) {
			if (response.length < 1) return false;
			if (typeof(response) != 'object') return false;
			$parent.data('oempro_data', response);
			if (callback !== undefined) callback($parent, options, response);
		});
	};

	var loadSettings = function($parent, options, callback) {
		$.get(options.settings, function(response) {
			if (response.length < 1) return false;
			if (typeof(response) != 'object') return false;
			$parent.data('oempro_settings', response);
			if (callback !== undefined) callback($parent, options, response);
		});
	};

	var startLoad = function($parent, options) {
		var ctx = $parent.data('oempro_context');
		var settings = $parent.data('oempro_settings');
		var a = 0, f = 1, w = $parent.width(), h = $parent.height(), c = options.pLC;
		var loadAnim = setInterval(function() {
			ctx.clearRect(0, 0, w, h);
			if (a == 100) f = -5;
			if (a == 0) f = 5;
			ctx.lineWidth = 2;
			ctx.strokeStyle = hex2rgba(c, a / 100);
			ctx.strokeRect(options.lP, options.tP, w - options.rP - options.lP, h - options.bP - options.tP - 2);
			a += f;
		}, 1000 / 60);
		$parent.data('oempro_load', loadAnim);
	};

	var stopLoad = function($parent, options) {
		clearInterval($parent.data('oempro_load'));
	};

	var drawLine = function($parent, options) {
		var ctx = $parent.data('oempro_context');
		var data = $parent.data('oempro_data');
		var settings = $parent.data('oempro_settings');
		var labels = {x: [], y: []};
		var baloons = {};

		var v = {
			bP: options.bP,
			tP: options.tP,
			lP: options.lP,
			rP: options.rP,
			h: $parent.height(),
			w: $parent.width(),
			max: 0,
			n: 0,
			r: function(val) {  return v.h - v.bP - ((v.h - v.tP - v.bP) * parseInt(val) / v.max); },
			s: function(index) { return ((v.w - v.lP - v.rP)  / (v.n - 1) * index) + v.lP; }
		};

		v.n = $('series value', data).length;
		$('graph value', data).each(function() { v.max = (i = parseInt($(this).text())) > v.max ? i : v.max; });

		if (options.anim === false) {
			drawSteps(1, 1);
			drawLabels();
			drawBaloons();
		} else {
			var ms = 30, cs = 0;
			var xxx = function() {
				drawSteps(cs, ms);
				cs++;
				if (cs <= ms) {
					requestAnimationFrame(xxx);
				} else {
					drawLabels();
					drawBaloons();
				}
			};
			requestAnimationFrame(xxx);
		}


		function drawYAxis(cs, ms) {
			if (cs != ms) return;
			var y1 = v.r(v.max / 2), y2 = v.r(v.max),
				x1 = v.s(0), x2 = v.s(v.n - 1);
			ctx.beginPath();
			ctx.strokeStyle = '#E0E0E0';
			ctx.lineWidth = 1;
			if (v.max > 1) {
				ctx.moveTo(x1, y1);
				ctx.lineTo(x2, y1);
			}
			ctx.moveTo(x1, y2);
			ctx.lineTo(x2, y2);
			ctx.stroke();
			ctx.closePath();

			if (v.max > 1) {
				labels.y.push([x1, y1, v.max / 2]);
			}

			if (v.max > 0) {
				labels.y.push([x1, y2, v.max]);
			}
		}

		function drawSteps(cs, ms) {
			ctx.clearRect(0, 0, v.w, v.h);
			drawYAxis(cs, ms);
			$('graph', data).each(function() {
				var gid = $(this).attr('gid');
				var graphSettings = $('graph[gid="' + gid + '"]', settings);
				var bullets = [];
				ctx.beginPath();
				ctx.strokeStyle = '#' + $('fill_color', graphSettings).text();
				ctx.fillStyle = hex2rgba($('fill_color', graphSettings).text(), '.1');
				ctx.moveTo(v.lP, v.h - v.bP);
				$('value', this).each(function(i) {
					var x = v.s(i), y = v.max == 0 ? v.h - v.bP : ((v.r($(this).text()) / (v.max / ms * cs)) * v.max);
					ctx.lineTo(x, y);
				});
				ctx.lineTo(v.s(v.n - 1), v.h - v.bP);
				ctx.fill();
				ctx.closePath();

				ctx.beginPath();
				ctx.lineJoin = 'round';
				ctx.lineWidth = $('line_width', graphSettings).text();
				ctx.strokeStyle = '#' + $('fill_color', graphSettings).text();
				$('value', this).each(function(i) {
					var x = v.s(i), y = v.max == 0 ? v.h - v.bP : ((v.r($(this).text()) / (v.max / ms * cs)) * v.max);
					ctx.lineTo(x, y);
					bullets.push({x: x, y: y});
					if (cs == ms) {
						if (! baloons[x]) baloons[x] = [];
						baloons[x].push([$(this).attr('description'), '#' + $('fill_color', graphSettings).text(), $('series value:nth-child(' + (i + 1) + ')', data).text()]);
					}
				});
				ctx.stroke();
				ctx.closePath();

				$.each(bullets, function() {
					ctx.beginPath();
					ctx.fillStyle = '#' + $('bullet_color', graphSettings).text();
					ctx.arc(this.x, this.y, $('bullet_size', graphSettings).text() / 1.5, 0, Math.PI*2, true);
					ctx.fill();
				});
			});
		}

		function drawLabels() {
			$container = $('.oempro-chart-labels', $parent);
			$.each(labels.y, function() {
				$label = $('<div></div>');
				$label.text(Math.floor(this[2])).css({
					position: 'absolute',
					top: this[1] + 10,
					left: this[0] + 5,
					fontSize: 10,
					color: '#AAAAAA'
				});
				$container.append($label);
			});
		}

		function drawBaloons() {
			$container = $('.oempro-chart-labels', $parent);
			var w = $parent.width() / v.n, pw = $parent.width();
			var step = -1;
			$.each(baloons, function(k, i) {
				step++;
				var $trigger = $('<div></div>');
				var $hair = $('<div></div>');
				var data = this;
				$hair.css({
					display: 'none',
					position:'absolute',
					left:'50%',
					top:0,
					width:'1px',
					height:'100%',
					'border-left':'1px solid #0077CC'
				});
				$trigger.css({
					position: 'absolute',
					top: 0,
					left: (k - (w / 2)) + 'px',
					width: w + 'px',
					height: '100%',
					'z-index': 999999
				}).bind('mouseenter', function() {
					$('div', this).show();
				}).bind('mouseleave', function() {
					$('div', this).hide();
				}).append($hair);
				$.each(this, function(i) {
					$baloon = $('<div></div>');
					$baloon.html('<strong>' + this[0] + '</strong>').css({
						'display':'none',
						'font-size':'10px',
						'line-height':'10px',
						position: 'absolute',
						top:(20 * i) + 'px',
						padding:'5px 8px',
						'white-space':'nowrap',
						background:this[1],
						'z-index': 999999
					});
					if (k < pw / 2) {
						$baloon.css('left','50%');
					} else {
						$baloon.css('right','50%');
					}
					$trigger.append($baloon);
				});
				$baloon = $('<div></div>');
				$baloon.html('<strong>' + this[0][2] + '</strong>').css({
					'display':'none',
					'font-size':'10px',
					'line-height':'10px',
					position: 'absolute',
					bottom:0,
					padding:'5px 8px',
					'white-space':'nowrap',
					color:'#0077CC'
				});
				if (k < pw / 2) {
					$baloon.css('left','50%');
				} else {
					$baloon.css('right','50%');
				}
				$trigger.append($baloon);
				$container.append($trigger);
			});
		}
	};

	var drawPie = function($parent, options) {
		var ctx = $parent.data('oempro_context');
		var data = $parent.data('oempro_data');
		var settings = $parent.data('oempro_settings');
		var labels = {x: [], y: []};
		var baloons = {};

		var v = {
			bP: options.bP,
			tP: options.tP,
			lP: options.lP,
			rP: options.rP,
			h: $parent.height(),
			w: $parent.width(),
			max: 0,
			n: 0
		};

		$('slice', data).each(function() { v.max += parseInt($(this).text(), 0); });
		v.n = $('slice', data).length;

		var deg2rad = function(deg) {
			return (Math.PI / 180) * deg;
		};
		var drawSlices = function() {
			ctx.clearRect(0, 0, v.w, v.h);
			var lastAngle = 0;

			$('slice', data).each(function() {
				var val = (100 * parseInt($(this).text(), 0)) / v.max;
				var rad = deg2rad(360 * val / 100);

				var x = ((v.w - (v.lP + v.rP)) / 2) + v.lP,
					y = ((v.h - (v.tP + v.bP)) / 2) + v.tP,
					r = (v.w / 2) - v.lP;

				var color = $(this).attr('color');
				ctx.fillStyle = color[0] != '#' ? '#' + color : color;

				ctx.beginPath();
				ctx.moveTo(x, y);
				ctx.arc(x, y, r, lastAngle, lastAngle + rad, false);
				ctx.lineTo(x, y);
				ctx.fill();

				lastAngle += rad;
			});
		};

		drawSlices();
	};

	$.fn.oemproChart = function(options) {
		return this.each(function() {
			var $this = $(this);

			var initChart = function() {
				var options = {};
				options.type = $this.attr('data-oempro-chart-options-type');
				options.settings = $this.attr('data-oempro-chart-options-settings');
				options.data = $this.attr('data-oempro-chart-options-data');

				options = $.extend({}, defaults, options);

				init($this, options);
				startLoad($this, options);
				loadSettings($this, options, function($parent, options, settings_response) {
					loadData($this, options, function($parent, options, data_response) {
						stopLoad($this, options);
						if (options.type == 'line') drawLine($parent, options);
						if (options.type == 'pie') drawPie($parent, options);
					});
				});
			};
			$this.data('init', initChart);
			initChart();
		});
	};
})(jQuery);

$(document).ready(function() {
	$('[data-oempro-chart-options-type]').oemproChart();
});