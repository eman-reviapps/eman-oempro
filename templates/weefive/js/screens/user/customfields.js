var last_option_id = 0;
var array_field_options = new Array();

function setup_validation_rule(context) {
	if ($('#ValidationMethod', context).val() == 'Custom') {
		$('#form-row-ValidationRule', context).show();
	} else {
		$('#form-row-ValidationRule', context).hide();
	}
}

var template_container = '';
var template_html = '';

function setup_default_value(context) {
	if ($('#FieldType', context).val() == 'Date field') {
		$('#form-row-FieldDefaultValue', context).hide();
		$('#form-row-options', context).hide();
		$('#form-row-ValidationMethod', context).hide();
		$('#ValidationMethod option[value="Disabled"]', context).get(0).selected = true;
		$('#form-row-ValidationRule', context).hide();
		$('#form-row-DateFieldYears', context).show();
	} else if ($('#FieldType', context).val() == 'Time field') {
		$('#form-row-FieldDefaultValue', context).hide();
		$('#form-row-options', context).hide();
		$('#form-row-ValidationMethod', context).hide();
		$('#ValidationMethod option[value="Disabled"]', context).get(0).selected = true;
		$('#form-row-ValidationRule', context).hide();
		$('#form-row-DateFieldYears', context).hide();
	} else if ($('#FieldType', context).val() == 'Single line' || $('#FieldType', context).val() == 'Paragraph text' || $('#FieldType', context).val() == 'Hidden field') {
		$('#form-row-FieldDefaultValue', context).show();
		$('#form-row-ValidationMethod', context).show();
		$('#form-row-options', context).hide();
		$('#form-row-DateFieldYears', context).hide();
	} else {
		$('#form-row-FieldDefaultValue', context).hide();
		$('#form-row-options', context).show();
		$('#form-row-DateFieldYears', context).hide();

		$('.form-row-options-container', context).html('');

		if ($('#FieldType', context).val() == 'Checkboxes') {
			template_container = $('#field-option-checkbox-template');
		} else if ($('#FieldType', context).val() == 'Multiple choice') {
			template_container = $('#field-option-multiple-choice-template');
		} else if ($('#FieldType', context).val() == 'Drop down') {
			template_container = $('#field-option-multiple-choice-template');
		}

		template_html = template_container.html();

		array_field_options = unserialize_options(context);
		array_selected_field_options = unserialize_selected_options(context);

		if (array_field_options.length > 0) {
			$.each(array_field_options, function() {
				add_new_option(-1, this[0], this[1], context);
			});
		} else {
			add_new_option(-1, '', '', context);
		}
		if (array_selected_field_options != false) {
			$.each(array_selected_field_options, function(a, b) {
				if ($('.option-default[optionid='+b+']', context).length > 0) {
					$('.option-default[optionid='+b+']', context).get(0).checked = true;
				}
			});
		}
	}
}

function unserialize_selected_options(context) {
	tmp_array = $('#SelectedOptions', context).val().split(',');
	return tmp_array.length > 0 ? tmp_array : false;
}

function serialize_options(context) {
	var return_array = new Array();
	var return_selected_array = new Array();
	$('#form-row-options .field-option-container', context).each(function() {
		return_array.push('[['+$('.option-label[optionid='+$(this).attr('optionid')+']', context).val()+']||['+$('.option-value[optionid='+$(this).attr('optionid')+']', context).val()+']]')
	});
	$('#form-row-options .option-default', context).each(function(a,b) {
		if (this.checked)
		{
			return_selected_array.push(a);
		}
	});
	$('#Options', context).val(return_array.join(',,,'));
	$('#SelectedOptions', context).val(return_selected_array.join(','));
	return true;
}

function unserialize_options(context) {
	var return_array = new Array();
	if ($('#Options', context).val() != '') {
		var tmp_options = $('#Options', context).val().split(',,,');
		$.each(tmp_options, function() {
			var tmp_option = this.split('||');
			return_array.push(new Array(tmp_option[0].replace(/^\[+|\]+$/g, ''), tmp_option[1].replace(/^\[+|\]+$/g, '')));
		});
	}
	return return_array;
}

function add_new_option(next_to, label, value, context) {
	if (next_to == -1) {
		$('.form-row-options-container', context).append(template_html.replace(/\#id\#/gi, last_option_id++).replace(/\#label\#/gi, label).replace(/\#value\#/gi, value));
	} else {
		$('.field-option-container[optionid='+next_to+']', context).after(template_html.replace(/\#id\#/gi, last_option_id++).replace(/\#label\#/gi, '').replace(/\#value\#/gi, ''));
	}
}

function remove_option(optionid, context) {
	if ($('.field-option-container', context).length <= 3) return false;
	$('.field-option-container[optionid='+optionid+']', context).remove();
}

function option_event_handler(ev, context) {
	var element = $(ev.target);
	if ($(element, context).hasClass('add-button')) {
		add_new_option($(element, context).attr('optionid'));
	}
	if ($(element, context).hasClass('remove-button')) {
		remove_option($(element, context).attr('optionid'));
	}
}

$(document).ready(function () {
	$('#custom-fields-table-form').submit(function() {
		if (!confirm(language_object.screen['1164'])) {
			return false;
		}
	});

	$('#custom-field-create').submit(function() {
		serialize_options('#custom-field-create');
	});
	$('#ValidationMethod', '#custom-field-create').change(function() {
		setup_validation_rule('#custom-field-create');
	});
	$('#FieldType', '#custom-field-create').change(function() {
		setup_default_value('#custom-field-create');
	});
	setup_validation_rule('#custom-field-create');
	setup_default_value('#custom-field-create');
	$('#form-row-options', '#custom-field-create').live('click', function(ev) {
		option_event_handler(ev, '#custom-field-create'); 
	});
	
	if (is_edit) {
		$('#custom-field-edit').submit(function() {
			serialize_options('#custom-field-edit');
		});
		$('#ValidationMethod', '#custom-field-edit').change(function() {
			setup_validation_rule('#custom-field-edit');
		});
		$('#FieldType', '#custom-field-edit').change(function() {
			setup_default_value('#custom-field-edit');
		});
		setup_validation_rule('#custom-field-edit');
		setup_default_value('#custom-field-edit');
		$('#form-row-options', '#custom-field-edit').live('click', function(ev) {
			option_event_handler(ev, '#custom-field-edit'); 
		});
	}

});