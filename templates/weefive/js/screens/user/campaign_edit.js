function on_tab_change(collection, tab_id) {
    if (tab_id == 'tab-test') {
        setTimeout(test_size_slide, 100);
    }
}

var all_library = {
    init_handlers: function () {
        if (cf === 'no') {
            // $('#campaign-edit').submit(function() {
            // return false;
            // });
        }
    }
};

var settings_library = {
    init_handlers: function () {
        $('#SameNameEmail').click(function () {
            if (this.checked == true) {
                $('#replyto-name-email-container').hide();
            } else {
                $('#replyto-name-email-container').show();
            }
        });
        $('#EnableGoogleAnalytics').click(function () {
            if (this.checked == true) {
                $('#form-row-GoogleAnalyticsDomains').show();
            } else {
                $('#form-row-GoogleAnalyticsDomains').hide();
            }
        });

        if ($('#SameNameEmail').length > 0) {
            if ($('#SameNameEmail').get(0).checked == true) {
                $('#replyto-name-email-container').hide();
            } else {
                $('#replyto-name-email-container').show();
            }
        }

        if ($('#EnableGoogleAnalytics').length > 0) {
            if ($('#EnableGoogleAnalytics').get(0).checked == true) {
                $('#form-row-GoogleAnalyticsDomains').show();
            } else {
                $('#form-row-GoogleAnalyticsDomains').hide();
            }
        }
    }
};

var content_library = {
    init_handlers: function () {
        $('#ContentType').change(function () {
            content_library.switch_content_type($(this).val());
        });

        content_library.switch_content_type($('#ContentType').val());

        if (attachments.length > 0) {
            $.each(attachments, function () {
                content_library.add_attachment_element(this);
            });
        }

        $('#AttachmentFile').change(function () {
            $('#uploading-indicator').show();
            $('#file-upload-form').submit();
        });

        $('.attachment-checkbox').live('click', function () {
            if (this.checked == false) {
                content_library.remove_attachment_element($(this).val());
            }
        });

        if (wysiwyg_enabled) {
            $('#HTMLContent').tinymce(tinymce_config);
        }
    },
    switch_content_type: function (type) {
        switch (type) {
            case 'HTML':
                $('#form-row-HTMLContent').show();
                $('#form-row-PlainContent').hide();
                $('#form-row-FetchURL').show();
                $('#form-row-FetchPlainURL').hide();
                break;
            case 'Plain':
                $('#form-row-HTMLContent').hide();
                $('#form-row-PlainContent').show();
                $('#form-row-FetchURL').hide();
                $('#form-row-FetchPlainURL').show();
                break;
            case 'Both':
                $('#form-row-HTMLContent').show();
                $('#form-row-PlainContent').show();
                $('#form-row-FetchURL').show();
                $('#form-row-FetchPlainURL').show();
                break;
        }
    },
    add_attachment_element: function (attachment_object) {
        $('#AttachmentFile').val('');
        $('#uploading-indicator').hide();

        attachment_object.FileSize = (attachment_object.FileSize / 1024).toFixed(2);
        attachment_object.FileSize = attachment_object.FileSize > 1000 ? ((attachment_object.FileSize / 1024).toFixed(2)) + 'MB' : attachment_object.FileSize + 'KB';

        var content = $('#attachment-template').html().replace('_FileName_', attachment_object.FileName);
        content = content.replace('_FileSize_', attachment_object.FileSize);
        content = content.replace('_AttachmentID_', attachment_object.AttachmentID);
        content = content.replace('_AttachmentMD5ID_', attachment_object.AttachmentMD5ID);

        $('#attachment-template')
                .clone()
                .attr('id', 'attachment-element-' + attachment_object.AttachmentID)
                .html(content)
                .appendTo('#attachment-container')
                .show('slow');
    },
    remove_attachment_element: function (id) {
        $('#attachment-element-' + id).fadeOut();
        $.post(api_url, {Command: 'Attachment.Delete', ResposeFormat: 'JSON', AttachmentID: id});
    }
};

var template_library = {
    init_handlers: function () {
        $('.email-template').click(function () {
            $('.email-template-gallery-container .email-template.selected').removeClass('selected');
            $(this).addClass('selected');
            $('#EmailTemplateID').val($(this).attr('id').replace('email-template-', ''));
        });
        if ($('.email-template-gallery-container .email-template.selected').length < 1) {
            $('.email-template:first-child').click();
        }
    }
};

var content_url_library = {
    init_handlers: function () {
        $('#ContentType').change(function () {
            content_url_library.switch_content_type($(this).val());
        });

        content_url_library.switch_content_type($('#ContentType').val());

        if (attachments.length > 0 && screen_type != 'edit') {
            $.each(attachments, function () {
                content_library.add_attachment_element(this);
            });
        }

        if (screen_type != 'edit') {
        } else {
            $('#AttachmentFileTemp').change(function () {
                $('#AttachmentFile').val($(this).val());
                $('#uploading-indicator').show();
                $('#file-upload-form').submit();
            });
        }

        $('.attachment-checkbox').live('click', function () {
            if (this.checked == false) {
                content_library.remove_attachment_element($(this).val());
            }
        });
    },
    switch_content_type: function (type) {
        switch (type) {
            case 'HTML':
                $('#form-row-FetchURL').show();
                $('#form-row-FetchPlainURL').hide();
                break;
            case 'Plain':
                $('#form-row-FetchURL').hide();
                $('#form-row-FetchPlainURL').show();
                break;
            case 'Both':
                $('#form-row-FetchURL').show();
                $('#form-row-FetchPlainURL').show();
                break;
        }
    }
};

var preview_library = {
    init_handlers: function () {
        var val = $('#form-row-PreviewType input:checked').val();
        if (val == 'browser') {
            $('#form-row-PreviewEmailAddress').hide();
            $('#preview-button').show();
            $('#preview-button strong').html(Language['0812']);
        } else {
            $('#form-row-PreviewEmailAddress').show();
            $('#preview-button').show();
            $('#preview-button strong').html(Language['0811']);
        }

        $('#form-row-PreviewType input').click(function () {
            var val = $('#form-row-PreviewType input:checked').val();
            if (val == 'browser') {
                $('#form-row-PreviewEmailAddress').hide();
                $('#preview-button').show();
                $('#preview-button strong').html(Language['0812']);
            } else {
                $('#form-row-PreviewEmailAddress').show();
                $('#preview-button').show();
                $('#preview-button strong').html(Language['0811']);
            }
        });

        $('#preview-button').click(function () {
            if (screen_type == 'edit') {
                $('#campaign-review #PreviewEmailAddress').val($('#campaign-edit #PreviewEmailAddress').val());
                $('#PreviewType').val($('#form-row-PreviewType input:checked').val());
            }
            if ($('#form-row-PreviewType input:checked').val() == 'browser') {
                window.open(preview_url);
            } else {
                $('#Command').val('Preview');
                $('#campaign-create').submit();
            }
        });

        $('#test-button').click(function () {
            $('#Command').val('Test');
            $('#campaign-review').submit();
        });

        $('#DesignTest').change(preview_library.get_preview_details);

        preview_library.get_preview_details();
    },
    get_preview_details: function () {
        $.post(api_url, {
            command: 'Email.DesignPreview.Details',
            responseformat: 'JSON',
            emailid: EmailID,
            jobid: $('#DesignTest').val()
        }, preview_library.display_preview_results, 'json');
    },
    display_preview_results: function (data) {
        if (data.Success == true) {
            var gallery = $('<div class="email-template-gallery-container clearfix"></div>');
            $.each(data.PreviewRequest.PreviewRequest.Results, function () {
                gallery.append('<div class="email-template"><div class="image"><img src="' + this.ThumbnailURL + '" /></div><div class="meta">' + this.ClientCode + '</div></div>');
            });
            $('#test-container').html(gallery);
        }
    }
};

var schedule_library = {
    init_handlers: function () {
        var SendDateObject = $('#SendDate').val().split('-');
        SendDateObject = new Date(Number(SendDateObject[0]), Number(SendDateObject[1]) - 1, Number(SendDateObject[2]));
        $('#ScheduleType').change(schedule_library.setup_schedule_type);
        $('#datepicker').datepicker({
            firstDay: 1,
            dateFormat: 'yy-mm-dd',
            defaultDate: SendDateObject,
            inline: true,
            onSelect: function (dateText, inst) {
                $('#SendDate').val(dateText);
            }
        });
        schedule_library.setup_schedule_type();
        $('#SendRepeteadlyDayType').change(schedule_library.setup_day_type);
        schedule_library.setup_day_type();
        $('#SendRepeatedlyMonthType').change(schedule_library.setup_month_type);
        schedule_library.setup_month_type();
        $('#SendRepeatedlyWeekDays').change(schedule_library.convert_to_words);
        $('#SendRepeatedlyMonthDays').keyup(schedule_library.convert_to_words);
        $('#SendRepeatedlyMonths').change(schedule_library.convert_to_words);
        $('#ScheduleRecHours').change(schedule_library.convert_to_words);
        $('#ScheduleRecMinutes').change(schedule_library.convert_to_words);
    },
    setup_day_type: function () {
        var select_value = $('#SendRepeteadlyDayType').val();
        if (select_value == 'Days of week') {
            $('#SendRepeatedlyWeekDaysContainter').show();
            $('#SendRepeteadlyMonthDaysContainter').hide();
        } else if (select_value == 'Days of month') {
            $('#SendRepeatedlyWeekDaysContainter').hide();
            $('#SendRepeteadlyMonthDaysContainter').show();
        } else {
            $('#SendRepeatedlyWeekDaysContainter').hide();
            $('#SendRepeteadlyMonthDaysContainter').hide();
        }
        schedule_library.convert_to_words();
    },
    setup_month_type: function () {
        var select_value = $('#SendRepeatedlyMonthType').val();
        if (select_value == 'Every month') {
            $('#SendRepeatedlyMonthsContainter').hide();
        } else {
            $('#SendRepeatedlyMonthsContainter').show();
        }
        schedule_library.convert_to_words();
    },
    convert_to_words: function () {
        var words = '';
        var day_select_value = $('#SendRepeteadlyDayType').val();
        if (day_select_value == 'Every day') {
            words = Language['0852'];
        } else if (day_select_value == 'Every weekday') {
            words = Language['0853'];
        } else if (day_select_value == 'Monday, wednesday and friday') {
            words = Language['0854'];
        } else if (day_select_value == 'Tuesday and thursday') {
            words = Language['0855'];
        } else if (day_select_value == 'Days of week') {
            words = Language['0865'];
            var value = new Array();
            $('#SendRepeatedlyWeekDays option:selected').each(function (a) {
                value.push($(this).val());
            });
            value = value.join(',');
            if (value == '1,2,3,4,5') {
                words = Language['0853'];
            } else if (value == '1,3,5') {
                words = Language['0854'];
            } else if (value == '2,4') {
                words = Language['0855'];
            } else if (value == '1,2,3,4,5,6,0') {
                words = Language['0852'];
            } else {
                var count = $('#SendRepeatedlyWeekDays option:selected').length;
                $('#SendRepeatedlyWeekDays option:selected').each(function (ind) {
                    var index = $(this).val() == 0 ? 6 : $(this).val() - 1;
                    if (ind == count - 1 && count > 1) {
                        words += ' ' + Language['0866'] + ' ' + Language['0859'][index];
                    } else if (ind == 0) {
                        words += ' ' + Language['0859'][index];
                    } else {
                        words += ', ' + Language['0859'][index];
                    }
                });
            }
        } else if (day_select_value == 'Days of month') {
            words = Language['0865'];
            var array_days = $('#SendRepeatedlyMonthDays').val().split(',');
            array_days.sort(function (a, b) {
                return a - b;
            });
            var array_suffix = new Array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
            var values = new Array();
            $.each(array_days, function () {
                var value = this.replace(/^\s*/, "").replace(/\s*$/, "");
                if (value != '' && Number(value) < 32 && Number(value) > 0) {
                    values.push(value + array_suffix[value.substr(value.length - 1)]);
                }
            });
            $.each(values, function (ind) {
                words += ' ' + this;
                if (ind != values.length - 1) {
                    if (ind == values.length - 2) {
                        words += ' ' + Language['0866'];
                    } else {
                        words += ', ';
                    }
                }
            });
            words += ' ' + (values.length > 1 ? Language['0851'] : Language['0867']);
        }
        words += ' ' + Language['0864'] + ' ';
        var month_select_value = $('#SendRepeatedlyMonthType').val();
        if (month_select_value == 'Every month') {
            words += Language['0861'];
        } else {
            var count = $('#SendRepeatedlyMonths option:selected').length;
            $('#SendRepeatedlyMonths option:selected').each(function (ind) {
                if (ind == count - 1 && count > 1) {
                    words += ' ' + Language['0866'] + ' ' + Language['0862'][$(this).val() - 1];
                } else if (ind == 0) {
                    words += ' ' + Language['0862'][$(this).val() - 1];
                } else {
                    words += ', ' + Language['0862'][$(this).val() - 1];
                }
            });
        }

        words += ' ' + Language['0871'];

        var count = $('#ScheduleRecHours option:selected').length;
        var count2 = $('#ScheduleRecMinutes option:selected').length;
        var ind3 = 0;
        if ($('#ScheduleRecHours option:selected').length > 0) {
            $('#ScheduleRecHours option:selected').each(function (ind) {
                var hour = $(this).val();
                $('#ScheduleRecMinutes option:selected').each(function (ind2) {
                    if (ind3 == (count * count2) - 1 && (count * count2) > 1) {
                        words += ' ' + Language['0866'] + ' ' + hour + ':' + $(this).val();
                    } else if (ind3 == 0) {
                        words += ' ' + hour + ':' + $(this).val();
                    } else {
                        words += ', ' + hour + ':' + $(this).val();
                    }
                    ind3++;
                });
            });
        } else {
            words += ' ...';
        }

        $('#in-words').text(words);
    },
    setup_schedule_type: function () {
        var select_value = $('#ScheduleType').val();
        if (select_value == 'Future') {
            $('#send-date-time').show();
            $('#send-time-zone').show();
            $('#send-repeatedly').hide();
            $('#finish-button strong').text(Language['0751']);
        } else if (select_value == 'Immediate') {
            $('#send-date-time').hide();
            $('#send-time-zone').hide();
            $('#send-repeatedly').hide();
            $('#finish-button strong').text(Language['0828']);
        } else if (select_value == 'Recursive') {
            $('#send-date-time').hide();
            $('#send-time-zone').show();
            $('#send-repeatedly').show();
            $('#finish-button strong').text(Language['0751']);
        } else if (select_value == 'Not Scheduled') {
            $('#send-date-time').hide();
            $('#send-time-zone').hide();
            $('#send-repeatedly').hide();
            $('#finish-button strong').text(Language['0304']);
        }

    }
};