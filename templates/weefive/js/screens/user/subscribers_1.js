var timesSelect = '';
var campaignSelect = '';

Array.prototype.inArray = function (value) {
	var i;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			return true;
		}
	}
	return false;
};

Array.prototype.removeFromArray = function (value) {
	var i, index;
	index = -1;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			index = i;
			break;
		}
	}
	if (index === -1) return false;
	this.splice(index, 1);
	return true;
};

function isArray(input) {
	return typeof(input)=='object' && (input instanceof Array);
}

var RuleBoard = {
	campaignSelect: '',
	timesSelect: '<select class="times-select"><option value="At most">'+lang['1408']+'</option><option value="At least">'+lang['1409']+'</option><option value="Only">'+lang['1410']+'</option></select>',
	filterOperator: 'and',
	addSegmentRules: function(selectedSegmentId) {
		$('.add-segment-rules-menu').removeClass('open');
		var listId = $('#SearchList').val();
		var selectedSegmentObject = {};
		$.each(segments[listId], function (index, data) {
			if (data.id == selectedSegmentId) {
				selectedSegmentObject = data;
			}
		});
		var ruleArray = RuleBoard.convertSegmentRulesStringToArray(selectedSegmentObject.rules);
		RuleBoard.convertRuleArrayToRuleBoard(ruleArray, selectedSegmentObject);
		segmentId = selectedSegmentId;
	},
	init: function() {
		$('.main-action-with-menu').live('click', function() {
			$(this).addClass('open');
			return false;
		});
		$('.main-action-with-menu').live('mouseout', function(e) {
			if ($(e.relatedTarget).isChildOf('.main-action-with-menu') == false) {
				$(this).removeClass('open');
			}
		});
		$('.add-new-rule-menu ul a').live('click', function() {
			var parent = null;
			if ($(this).parent().isChildOf('li')) {
				var parentLi = $(this).parent().parent().parent().parent().parent();
				parent = $('ul.sub-group', parentLi);
			} else {
				parent = $('ul#rule-list');
			}
			RuleBoard.addRule($(this).attr('customfieldid'), $(this).text(), $(this).attr('fieldtype'), $(this).attr('validationmethod'), $(this).attr('values'), parent);
			$('.add-new-rule-menu').removeClass('open');
			return false;
		});
		$('.add-segment-rules-menu ul a').live('click', function() {
			RuleBoard.addSegmentRules($(this).attr('segmentid'));
		});
		$('.filter-operator-menu ul a').live('click', function() {
			RuleBoard.filterOperator = $(this).parent().attr('operator');
			RuleBoard.updateRuleSeparators();
			$('#filter-operator').text($(this).text());
			$('.filter-operator-menu').removeClass('open');
			return false;
		});
		$('#rule-list .delete').live('click', function() {
			var parentSubGroup = $(this).parent().parent('.sub-group');
			if ($(this).parent().next().hasClass('rule-separator')) {
				$(this).parent().next().remove();
			} else {
				if ($(this).parent().prev().hasClass('rule-separator')) {
					$(this).parent().prev().remove();
				} else {
					$('.specific-list-rule-separator').remove();
				}
			}
			$(this).parent().remove();
			if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length == 1) {
				var item = $('li.rule', parentSubGroup);
				parentSubGroup.parent().replaceWith(item);
			} else if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length < 1) {
				parentSubGroup.parent().remove();
			}
			$('.rule-separator + .rule-separator').remove();
		});
		$('#rule-list .add').live('click', function() {
			RuleBoard.addRuleGroup($(this).parent(), true);
		});
		$('#remove-segment-rules-link').click(function() {
			RuleBoard.removeSegmentRules();
			return false;
		});
		$('.segment-rule-group-label').live('click', function() {
			$('div.segment-rules').toggleClass('opened');
			return false;
		});
		$('#clear-board-link').click(RuleBoard.clearBoard);
		$('.add-specific-link').live('click', function() {
			var parentLi = $(this).parent().parent();
			$('.specific-link', parentLi).show();
			$('.remove-specific-link-container', parentLi).show();
			$('.specific-link', parentLi).focus();
			$(this).hide();
			return false;
		});
		$('.remove-specific-link').live('click', function() {
			var parentLi = $(this).parent().parent().parent();
			$('.specific-link', parentLi).val('').hide();
			$('.add-specific-link', parentLi).show();
			$(this).parent().hide();
			return false;
		});

		RuleBoard.campaignSelect = '<select class="campaign-select"><option value="Total">'+lang['1413']+'</option><option value="Any">'+lang['1411']+'</option>';
		if (campaigns.length > 0) {
			RuleBoard.campaignSelect += '<optgroup label="'+lang['1412']+'">';
			$.each(campaigns, function(index, data) {
				RuleBoard.campaignSelect += '<option value="'+data['CampaignID']+'">'+data['CampaignName']+'</option>';
			});
			RuleBoard.campaignSelect += '</optgroup>';
		}
		RuleBoard.campaignSelect += '</select>';

		if (listId != 0) {
			$('#SearchList option[value="'+listId+'"]').get(0).selected = true;
			onSearchListChange();
			if (segmentId != 0) {
				RuleBoard.addSegmentRules(segmentId);
			}
			applyFilters();
		}

		$('#rule-list .rule-operator').live('change', function() {
			if ($(this).val() == 'Is set' || $(this).val() == 'Is not set') {
				$(this).next().hide();
			} else {
				$(this).next().show();
			}
		});
	},
	clearBoard: function() {
		$('#rule-list').html('');
		$('.specific-list-rule-separator').remove();
		RuleBoard.removeSegmentRules();
		SubscribersTable.clear();
		return false;
	},
	convertRuleBoardToString: function(parentId, lookForSegmentRules) {
		var ruleString = '';
		var ruleArray = [];
		var subGroupRuleArray = [];
		var ruleBoardId = parentId;
		var activityRules = $(ruleBoardId + ' > li.rule.activity');
		$.each(activityRules, function(index, data) {
			ruleArray.push(RuleBoard.convertActivityRuleLiToString(data));
		});
		var informationRules = $(ruleBoardId + ' > li.rule.information');
		$.each(informationRules, function(index, data) {
			ruleArray.push(RuleBoard.convertInformationRuleLiToString(data));
		});
		var subGroupRules = $(ruleBoardId + ' > li.sub-group-container');
		$.each(subGroupRules, function(index, eachSubGroup) {
			var subGroupInformationRules = $('ul > li.rule.information', eachSubGroup);
			var tmpGroupArray = [];
			$.each(subGroupInformationRules, function(index, data) {
				tmpGroupArray.push(RuleBoard.convertInformationRuleLiToString(data));
			});
			subGroupRuleArray.push('((!'+tmpGroupArray.join(',,,')+'!))');
		});

		ruleString = ruleArray.join(',,,');
		if (subGroupRuleArray.length > 0) {
			ruleString += (ruleString != '' ? ',::,' : '') + subGroupRuleArray.join(',#,');
		}

		var ruleString2 = '';
		if (lookForSegmentRules && $('#segment-rule-list').length > 0) {
			ruleString2 = RuleBoard.convertRuleBoardToString('#segment-rule-list', false);
		}

		if (lookForSegmentRules && ruleString2 != '') {
			ruleString = RuleBoard.joinRuleStrings(ruleString2, ruleString);
		}

		return ruleString;
	},
	joinRuleStrings: function(rule1, rule2) {
		ruleAll = '';
		ruleString = '';
		ruleGroupString = '';

		rule1 = rule1.split(',::,');
		rule2 = rule2.split(',::,');

		rule1Rules = rule1[0];
		rule2Rules = rule2[0];
		rule1Groups = (rule1.length > 1 ? rule1[1] : '');
		rule2Groups = (rule2.length > 1 ? rule2[1] : '');

		ruleString += rule1Rules;
		if (rule2Rules != '') {
			ruleString += (ruleString != '' ? ',,,' : '') + rule2Rules;
		}

		ruleGroupString += rule1Groups;
		if (rule2Groups != '') {
			ruleGroupString += (ruleGroupString != '' ? ',#,' : '') + rule2Groups;
		}

		ruleAll += ruleString;
		if (ruleGroupString != '') {
			ruleAll += (ruleAll != '' ? ',::,' : '') + ruleGroupString;
		}
		return ruleAll;
	},
	convertInformationRuleLiToString: function(liElement) {
		var field = $(liElement).attr('fieldid');
		var operator = $('.rule-operator', liElement).val();
		var filter = $('.filter', liElement).val();
		if (! defaultFields.inArray(field) && ! pluginFields.inArray(field)) {
			field = 'CustomField' + field;
		}
		return '[['+field+']||['+operator+']||['+filter+']]';
	},
	convertActivityRuleLiToString: function(liElement) {
		var field = $(liElement).attr('fieldid');
		var campaignFilter = $('.campaign-select', liElement).val();
		var campaignID = (campaignFilter != 'Total' && campaignFilter != 'Any') ? $('.campaign-select', liElement).val() : 0;
		if (campaignID != 0) {
			campaignFilter = 'Specific';
		}
		var timesFilter = $('.times-select', liElement).val();
		var timesValue = $('.times-input', liElement).val();
		if (field == 'Clicks') {
			var specificLink = $('.specific-link', liElement).val();
		}
		return '[['+field+']||['+campaignFilter+']||['+campaignID+']||['+timesFilter+']||['+timesValue+']'+(field == 'Clicks' ? '||['+specificLink+']': '')+']';
	},
	convertSegmentRulesStringToArray: function(ruleString) {
		returnArraySegmentRules = [];
		arrayRules = ruleString.split(',::,');
		arrayRuleGroups = (arrayRules.length > 1 ? arrayRules[1] : []);
		arrayRules = arrayRules[0];
		arrayRules = arrayRules.split(',,,');
		$.each(arrayRules, function(index, rule) {
			returnArraySegmentRules.push(RuleBoard.convertRuleStringToArray(rule));
		});
		if (arrayRuleGroups.length > 0) {
			arrayRuleGroupStrings = arrayRuleGroups.split(',#,');
			$.each(arrayRuleGroupStrings, function(index, eachRuleGroupString) {
				tmpArrayGroup = [];
				eachRuleArray = eachRuleGroupString.split(',,,');
				$.each(eachRuleArray, function(index, rule) {
					tmpArrayGroup.push(RuleBoard.convertRuleStringToArray(rule));
				});
				returnArraySegmentRules.push(tmpArrayGroup);
			});
		}
		return returnArraySegmentRules;
	},
	convertRuleStringToArray: function(ruleString) {
		returnRuleObject = {};
		ruleString = ruleString.replace('((!', '').replace('[[', '').replace(']]', '').replace('!))', '');

		arrayEachRule = ruleString.split(']||[');
		isActivityRule = false;

		$.each(activityFields, function(index, field) {
			if (field == arrayEachRule[0]) {
				isActivityRule = true;
			}
		});

		if (! isActivityRule) {
			returnRuleObject = {'field':arrayEachRule[0], 'operator':arrayEachRule[1], 'filter':arrayEachRule[2]};
		} else {
			returnRuleObject = {'field':arrayEachRule[0], 'campaignFilter':arrayEachRule[1], 'campaignID':arrayEachRule[2], 'timesFilter':arrayEachRule[3], 'timesValue':arrayEachRule[4]};
			if (arrayEachRule.length == 6) {
				returnRuleObject.specificLink = arrayEachRule[5];
			}
		}
		return returnRuleObject;
	},
	convertRuleArrayToRuleBoard: function(rules, segmentInformation) {
		var parent = $('ul#rule-list');
		if (segmentInformation != false) {
			RuleBoard.addSegmentRulesContainer(segmentInformation);
			var parent = $('ul#segment-rule-list');
		}
		$.each(rules, function(index, data) {
			if (! isArray(data)) {
				data.field = data.field.replace('CustomField', '');
				if (data.field == '0' || data.field == '') {
					return;
				}
				data.field = data.field.replace('CustomField', '');
				var newRule = RuleBoard.addRule(data.field, fieldLabels[data.field].fieldLabel, fieldLabels[data.field].fieldType, fieldLabels[data.field].validationMethod, fieldLabels[data.field].values, parent);
				if (data.field == 'Opens' || data.field == 'Clicks' || data.field == 'BrowserViews' || data.field == 'Forwards') {
					RuleBoard.applyRuleActivityData(newRule, data.campaignFilter, data.campaignID, data.timesFilter, data.timesValue, data.field, data.field == 'Clicks' ? data.specificLink : '');
				} else {
					RuleBoard.applyRuleData(newRule, data.operator, data.filter);
				}
			} else {
				var newRuleGroup;
				$.each(data, function(index, subData) {
					subData.field = subData.field.replace('CustomField', '');
					if (subData.field == '0' || subData.field == '') {
						return;
					}
					if (index < 1) {
						var newRule = RuleBoard.addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, parent);
						var array = RuleBoard.addRuleGroup(newRule, segmentInformation != false ? false : true);
						newRuleGroup = array[0];
						RuleBoard.applyRuleData(array[1], subData.operator, subData.filter);
					} else {
						var newRule = RuleBoard.addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, newRuleGroup);
						RuleBoard.applyRuleData(newRule, subData.operator, subData.filter);
					}
				});
			}
		});
		if (segmentInformation != false) {
			$('select, input', parent).attr('disabled', true);
		}
	},
	addSegmentRulesContainer: function(segmentInformation) {
		var segmentRuleListContainer = $('<div class="segment-rules"></div>');
		var segmentRuleListLabel = $('<div class="segment-rules-label segment-rule-group-label"">'+segmentInformation.name.toUpperCase()+'</div>');
		var segmentRuleListLabel2 = $('<div class="segment-rules-label-closed">'+lang['1454']+'<br /><span class="segment-rule-group-label">'+segmentInformation.name.toUpperCase()+'</span></div>');
		var segmentRuleContainer = $('<ul id="segment-rule-list"></ul>');
		segmentRuleListContainer.append(segmentRuleListLabel);
		segmentRuleListContainer.append(segmentRuleListLabel2);
		segmentRuleListContainer.append(segmentRuleContainer);
		$('#specific-list-rule').after(segmentRuleListContainer);
		$('.add-segment-rules-menu').hide();
		$('#remove-segment-rules-link').show();
		if ($('ul#rule-list li.rule').length > 0) {
			RuleBoard.addRuleSeparator($('#rule-list'), true);
		}
		if ($('.specific-list-rule-separator').length < 1) {
			$('#specific-list-rule').append('<li class="specific-list-rule-separator rule-separator">'+RuleBoard.filterOperator.toUpperCase()+'</li>');
		}
	},
	removeSegmentRules: function() {
		if ($('.segment-rules li').length > 0) {
			$('.segment-rules').remove();
			$('.add-segment-rules-menu').show();
			$('#remove-segment-rules-link').hide();
			if ($('#rule-list li:first-child').hasClass('rule-separator')) {
				$('#rule-list li.rule-separator:first-child').remove();
			}
		}
	},
	updateRuleSeparators: function() {
		$('#rule-list li.rule-separator').each(function() {
			if ($(this).text() == 'AND') {
				$(this).text('OR');
			} else {
				$(this).text('AND');
			}
		});
		$('.segment-rules .rule-separator').each(function() {
			if ($(this).text() == 'AND') {
				$(this).text('OR');
			} else {
				$(this).text('AND');
			}
		});
		$('.specific-list-rule-separator').text($('.specific-list-rule-separator').text() == 'AND' ? 'OR' : 'AND');

		return false;
	},
	applyRuleActivityData: function(ruleElement, campaignFilter, campaignID, timesFilter, timesValue, field, specificLink) {
		$('.specific-link', ruleElement).val(specificLink);
		if (specificLink != '') {
			$('.add-specific-link', ruleElement).hide();
			$('.specific-link', ruleElement).show();
			$('.remove-specific-link-container', ruleElement).show();
		}
		$('.campaign-select option[value="'+(campaignFilter == 'Specific' ? campaignID : campaignFilter)+'"]', ruleElement).attr('selected', 'true');
		$('.times-select option[value="'+timesFilter+'"]', ruleElement).attr('selected', 'true');
		$('.times-input', ruleElement).val(timesValue);
	},
	applyRuleData: function(ruleElement, operator, filter) {
		$('.rule-operator > option[value="'+operator+'"]', ruleElement).attr('selected','true');
		if ($('select.filter', ruleElement).length > 0) {
			$('select.filter > option[value="'+filter.replace('"', '\\"')+'"]', ruleElement).attr('selected','true');
		} else {
			$('input.filter', ruleElement).val(filter);
		}
		if ($('.rule-operator', ruleElement).val() == 'Is set' || $('.rule-operator', ruleElement).val() == 'Is not set') {
			$('.rule-operator', ruleElement).next().hide();
		} else {
			$('.rule-operator', ruleElement).next().show();
		}
	},
	addRuleSeparator: function(parent, first) {
		var separatorLabel = '';
		if (parent.attr('id') != 'rule-list' && parent.attr('id') != 'segment-rule-list') {
			separatorLabel = (RuleBoard.filterOperator.toUpperCase() == 'AND' ? 'OR' : 'AND');
		} else {
			separatorLabel = RuleBoard.filterOperator.toUpperCase();
		}
		var html = $('<li class="rule-separator">'+separatorLabel+'</li>');
		if (first) {
			if ($('#rule-list li:first-child').hasClass('sub-group-container')) {
				$('> li.sub-group-container:first', parent).before(html);
			} else {
				$('> li.rule:first', parent).before(html);
			}
		} else {
			$('> li.rule:last', parent).before(html);
		}
	},
	addRuleGroup: function(item, displayAddMenu) {
		var selectValues = [];
		$('select', item).each(function() {
			selectValues.push($(this).val());
		});
		var ruleItem = item.clone();
		$('select', ruleItem).each(function(index) {
			$('option[value="'+selectValues[index]+'"]', this).attr('selected','true');
		});
		var ruleGroupLI = $('<li class="sub-group-container"></li>');
		var ruleGroupUL = $('<ul class="sub-group"></ul>');
		ruleGroupLI.append(ruleGroupUL);

		if (displayAddMenu == true) {
			var ruleActionsDiv = $('<div class="rule-board-actions"></div>');
			ruleGroupLI.append(ruleActionsDiv);
			var addRuleMenu = $('.add-new-rule-menu:last').clone();
			$('.main-action', addRuleMenu).text(lang['1407']);
			$('li.activity', addRuleMenu).remove();
			ruleActionsDiv.append(addRuleMenu);
		}


		item.replaceWith(ruleGroupLI);

		ruleGroupUL.append(ruleItem);
		return [ruleGroupUL, ruleItem];
	},
	addRule: function(fieldId, fieldLabel, fieldType, validationMethod, values, parent) {
		if (fieldId == 'Opens' || fieldId == 'Clicks' || fieldId == 'BrowserViews' || fieldId == 'Forwards') {
			var description = '';
			if (fieldId == 'Clicks') {
				description = '<div class="rule-description"><a href="#" class="add-specific-link">'+lang['1414']+'</a><span class="remove-specific-link-container" style="display:none">'+lang['1416']+'<br /><a href="#" class="remove-specific-link">'+lang['1415']+'</a></span></div>';
			}
			var html = $('<li fieldid="'+fieldId+'" class="rule activity"><div class="delete">X</div><span class="field">'+fieldLabel+'</span> '+'<input type="text" value="" class="specific-link" style="width:250px;display:none" />'+RuleBoard.campaignSelect+' '+RuleBoard.timesSelect+'<input class="times-input" type="text" value="0" style="width:14px" /> '+languageObject['1750']+description+'</li>');
		} else {
			var operatorOptions = '';
			if (validationMethod != '') {
				operatorOptions += '<select class="rule-operator">';
				if (fieldType == 'Date field') {
					validationMethod = 'Date';
				} else if (fieldType == 'Time field') {
					validationMethod = 'Time';
				}
				for (i=0;i<ruleOperators[validationMethod].length;i++) {
					data = ruleOperators[validationMethod][i];
					if (values != '' && (data.Value == 'Contains' || data.Value == 'Does not contain' || data.Value == 'Begins with' || data.Value == 'Ends with' || data.Value == 'Equals to' || data.Value == 'Is greated than' || data.Value == 'Is smaller than' || data.Value == 'Is before' || data.Value == 'Is after')) {
						continue;
					};
					operatorOptions += '<option value="'+data.Value+'">'+data.Label+'</option>';
				}
				operatorOptions += '</select>';
			}
			var description = '';
			if (validationMethod == 'Date') {
				description = '<div class="rule-description">'+lang['1406']+'</div>';
			}
			var field = '';
			if (values != '') {
				field += '<select class="filter">';
				var tmpArray1 = values.split(',,,');
				for (i=0;i<tmpArray1.length;i++) {
					tmpArray1[i] = tmpArray1[i].replace(']]*', '').replace(']]', '').replace('[[','');
					tmpArray1[i] = tmpArray1[i].split(']||[');
					field += '<option value="'+tmpArray1[i][1]+'">'+tmpArray1[i][0]+'</option>';
				}
				field += '</select>';
			} else {
				field = '<input type="text" class="filter" />';
			}
			var html = $('<li fieldid="'+fieldId+'" class="rule information"><div class="delete">X</div><div class="add">+</div><span class="field">'+fieldLabel+'</span> '+operatorOptions+' '+field+description+'</li>');
		}
		$(parent).append(html);
		if ($('li.rule', parent).length == 1 && $('.segment-rules').length == 1 && $(parent).attr('id') != 'segment-rule-list') {
			RuleBoard.addRuleSeparator(parent, false);
		}
		if ($('li.rule', parent).length > 1) {
			RuleBoard.addRuleSeparator(parent, false);
		}
		if ($('.specific-list-rule-separator').length < 1) {
			$('#specific-list-rule').append('<li class="specific-list-rule-separator rule-separator">'+RuleBoard.filterOperator.toUpperCase()+'</li>');
		}
		return html;
	}
};

var SubscribersTable = {
	displayedFields: [],
	currentPage: 1,
	recordsPerPage: 25,
	totalFields: 0,
	cachedResults: null,
	sortField: '',
	sortType: '',
	clear: function() {
		SubscribersTable.displayedFields = [];
		$('#display-fields a').removeClass('selected');
		$('#subscribers-table tr').remove();
		$('.module-container').hide();
		$('#subscribers-table').append('<tr class="message-row"><td>&nbsp;</td></tr><tr class="message-row"><td>'+lang['1458']+'</td></tr>');
	},
	init: function() {
		$('#display-fields a').live('click', function() {
			var fieldLabel = $(this).text();
			var fieldId = $(this).parent().attr('customfieldid');
			if (SubscribersTable.addNewField(fieldId, fieldLabel)) {
				$(this).toggleClass('selected');
				SubscribersTable.populate(SubscribersTable.cachedResults);
			}
			return false;
		});
		$('#sort-fields a').live('click', function() {
			$('#sort-fields a').removeClass('selected');
			$(this).addClass('selected');
			var fieldId = $(this).parent().attr('customfieldid');
			SubscribersTable.sortField = fieldId;
			SubscribersTable.sortBy();
			return false;
		});
		$('#sort-type a').live('click', function() {
			$('#sort-type a').removeClass('selected');
			$(this).addClass('selected');
			var sortType = $(this).parent().attr('sorttype');
			SubscribersTable.sortType = sortType;
			SubscribersTable.sortBy();
			return false;
		});
		$('#subscriber-page').change(function() {
			SubscribersTable.currentPage = $(this).val();
			applyFilters();
		});
		$('.subscriber-row td:not(.checkbox-column)').live('click', function() {
			window.location = APP_URL+'user/subscriber/edit/'+$('#SearchList').val()+'/'+$(this).parent().attr('subscriberid')+'/'+segmentId;
		});
	},
	sortBy: function() {
		applyFilters();
	},
	populate: function(data) {
		SubscribersTable.cachedResults = data;
		$('#subscribers-table .subscriber-row').remove();
		$('#subscribers-table .message-row').remove();
		if ($('#display-fields a.selected').length < 1) {
			$('#subscribers-table').append($('<tr id="subscribers-table-header"></tr>'));
			SubscribersTable.addNewField('CheckBox', '');
			SubscribersTable.addNewField('EmailAddress', fieldLabels['EmailAddress'].fieldLabel);
			$('#display-fields li[customfieldid="EmailAddress"] a').addClass('selected');
		}
		if (SubscribersTable.cachedResults.TotalSubscribers == '0') {
			$('.module-container').hide();
			SubscribersTable.displayNoRecords();
			$('#subscriber-count').text('0');
			$('#subscriber-page').html('<option value="1">1</option>');
		} else {
			$('.module-container').show();
			$('#subscriber-count').text(SubscribersTable.cachedResults.TotalSubscribers);
			var numberOfPages = Math.ceil(parseInt(SubscribersTable.cachedResults.TotalSubscribers, 10) / SubscribersTable.recordsPerPage);
			numberOfPages = (numberOfPages < 1 ? 1 : numberOfPages);
			var pageOptions = '';
			var fromPage = $('#subscriber-page').val();
			fromPage = fromPage == null ? 1 : fromPage;
			fromPage = fromPage - 10;
			fromPage = fromPage < 1 ? 1 : fromPage;
			var toPage = fromPage + 20;
			toPage = toPage > numberOfPages ? numberOfPages : toPage;
			var i = fromPage;
			if (fromPage > 1) {
				pageOptions += '<option value="1" '+(1 == SubscribersTable.currentPage ? 'selected' : '')+'>1</option>';
			}
			for (i;i<toPage+1;i++) {
				pageOptions += '<option value="'+i+'" '+(i == SubscribersTable.currentPage ? 'selected' : '')+'>'+i+'</option>';
			}
			if (toPage < numberOfPages) {
				pageOptions += '<option value="'+numberOfPages+'" '+(numberOfPages == SubscribersTable.currentPage ? 'selected' : '')+'>'+numberOfPages+'</option>';
			}
			$('#subscriber-page').html(pageOptions);
			$.each(SubscribersTable.cachedResults.Subscribers, function(index, data) {
				var subscriberRowHtml = '<tr subscriberid="'+data.SubscriberID+'" class="subscriber-row">';
				subscriberRowHtml += '<td width="15" class="checkbox-column" style="width:15px;padding:0px;vertical-align:top;"><input class="grid-check-element" type="checkbox" name="SelectedSubscribers[]" value="'+data.SubscriberID+'" id="SelectedSubscriber'+data.SubscriberID+'" /></td>';
				$.each(SubscribersTable.displayedFields, function(index, eachField) {
					subscriberRowHtml += '<td>'+data[eachField]+'</td>';
				});
				subscriberRowHtml += '</tr>';
				$('#subscribers-table').append($(subscriberRowHtml));
			});
			$('#subscribers-table tr:even td').addClass('zebra');
		}
	},
	displayNoRecords: function() {
		$('#subscribers-table tr').remove();
		var row = jQuery('<tr class="message-row"><td>&nbsp;</td></tr><tr class="message-row"><td>'+lang['1457']+'</td></tr>');
		$('#subscribers-table').append(row);
	},
	addNewField: function(fieldId, fieldLabel) {
		var removed = false;
		if (SubscribersTable.displayedFields.inArray(fieldId) == true) {
			if (SubscribersTable.displayedFields.length > 1) {
				SubscribersTable.displayedFields.removeFromArray(fieldId);
				$('th#header-'+fieldId).remove();
				removed = true;
			} else {
				return false;
			}
		}
		if (! removed) {
			if (fieldId != 'CheckBox') {
				SubscribersTable.displayedFields.push(fieldId);
			}
			var headerRow = $('#subscribers-table-header');
			headerRow.append('<th id="header-'+fieldId+'" '+(fieldId == 'CheckBox' ? 'width="15"' : '')+'>'+fieldLabel+'</th>');
		}
		$('th:not(#header-CheckBox)', $('#subscribers-table')).css('width', Math.round(100/SubscribersTable.displayedFields.length)+'%');
		return true;
	}
};

function onSearchListChange() {
	var listId = $('#SearchList').val();
	if (segments[listId].length > 0) {
		$('.add-segment-rules-menu .main-action-menu li').remove();
		$.each(segments[listId], function (index, data) {
			$('.add-segment-rules-menu .main-action-menu').append('<li><a href="#" segmentid="'+data.id+'">'+data.name+'</a></li>');
		});
		$('.add-segment-rules-menu').show();
	} else {
		$('.add-segment-rules-menu .main-action-menu li').remove();
		$('.add-segment-rules-menu').hide();
	}
	$('.custom-fields-li').remove();
	$('#display-fields li.customfieldli').remove();
	$('#sort-fields li.customfieldli').remove();
	if (customFields[listId].length > 0) {
		$.each(customFields[listId], function(index, data) {
			var customFieldLi = $('<li class="custom-fields-li"><a href="#" customfieldid="'+data.id+'" fieldtype="'+data.fieldtype+'" validationmethod="'+data.validationmethod+'" values="'+data.fieldoptions+'">'+data.name+'</a></li>');
			if ($('.custom-fields-li').length < 1) {
				$('.default-fields:last').after(customFieldLi);
			} else {
				$('.custom-fields-li:last').after(customFieldLi);
			}
			var option = $('<li class="customfieldli" customfieldid="CustomField'+data.id+'"><a href="#">'+data.name+'</a></li>');
			$('#display-fields').append(option);
			$('#sort-fields').append(option.clone());
		});
	}
	RuleBoard.clearBoard();
	SubscribersTable.clear();
	$('#form-row-RuleBoard').show();
}

function applyFilters() {
	var ruleString = '';
	ruleString = RuleBoard.convertRuleBoardToString('#rule-list', true);
        console.log(ruleString);
	$('.page-overlay').fadeIn('fast');
	$.post(API_URL, {
		responseformat: 'JSON',
		command: 'Subscribers.Search',
		listid: $('#SearchList').val(),
		operator: RuleBoard.filterOperator,
		rules: ruleString,
		recordsfrom: SubscribersTable.recordsPerPage * (SubscribersTable.currentPage - 1),
		recordsperrequest: SubscribersTable.recordsPerPage,
		orderfield: SubscribersTable.sortField,
		ordertype: SubscribersTable.sortType
	}, function(data) {
            console.log(ruleString)
            console.log(API_URL)
            console.log(data)
		SubscribersTable.populate(data);
		$('.page-overlay').fadeOut('fast');
	}, 'json');
}

$(document).ready(function () {
	$('#page.collapsible div.page-collapsed-bar, #page.collapsible div.page-bar').click(function() {
		$(this).parent().toggleClass('collapsed');
	});
	$('#SearchList').change(onSearchListChange);
	onSearchListChange();
	RuleBoard.init();
	applyFilters();
	$('#apply-filter-button').click(function() {
		applyFilters();
		return false;
	});
	$('#subscribers-table-form').submit(function() {
		if (!confirm(languageObject['1335'])) {
			return false;
		}
	});
	$('#subscribers-export-form').submit(function() {
		$('#ExportRules').val(RuleBoard.convertRuleBoardToString('#rule-list', true));
		$('#ExportOperator').val(RuleBoard.filterOperator);
		$('#ExportListID').val($('#SearchList').val());
	});
});
