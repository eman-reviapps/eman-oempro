var SWFObjectContainer = [];
var flash_chart_element = null;
var selected_stat = '';

function setChartOptions(chartel, option, value) {
	chartel.attr('data-oempro-chart-options-' + option, value);
}

function initChart(chartel) {
	chartel.data('init')();
}


function amChartInited(chart_id){
	// flash_chart_element = $('#comparison-chart-swf').get(0);
	// select_column('opens');
}

function select_column(id) {
	if (selected_stat != '') {
		$('.'+selected_stat+'-column').removeClass('selected');
		$('#'+selected_stat).removeClass('selected');
	}
	selected_stat = id;

	setChartOptions($('#activity-chart-container'), 'data', app_url+'/user/campaigns/comparestatistics/'+campaign_ids+'/'+id);
	initChart($('#activity-chart-container'));

	$('.'+selected_stat+'-column').addClass('selected');
	$('#'+selected_stat).addClass('selected');
}

$(document).ready(function() {
	$('#comparison-chart th').not(':first-child').css({'cursor':'pointer'}).click(function() {
		select_column($(this).attr('id'));
	});
	select_column('opens');
});