$(document).ready(function () {
//    $('#Credits').on('keyup', function () {
//        dotView(removeDots(this.value), $('#Credits'));
//        CalculateCreditPrice(removeDots($('#Credits').val()));
//    });

//    $('#form-button').on('click', function () {
//        var CreditsAmount = removeDots($('#Credits').val());
//        window.location.href = PurchaseURL + CreditsAmount;
//         $("#purchase-credits-form").submit();
//    });
});

function creditWindowLoaded() {
//    dotView(removeDots($('#Credits').val()), $('#Credits'));
    CalculateCreditPrice($('#Credits').val());
}

function CalculateCreditPrice(CreditsToBeLoaded) {
    var TotalCost = 0;

    for (var TMPCounter = 0; TMPCounter < ArrayPricingList.length; TMPCounter++)
    {
        if (ArrayPricingList[TMPCounter] >= CreditsToBeLoaded)
        {
            TotalCost += CreditsToBeLoaded * ArrayPrices[TMPCounter];
            break;
        } else
        {
            CreditsToBeLoaded = CreditsToBeLoaded - ArrayPricingList[TMPCounter];
            CreditsToBeLoaded = (CreditsToBeLoaded < 0 ? 0 : CreditsToBeLoaded);
            TotalCost += ArrayPricingList[TMPCounter] * ArrayPrices[TMPCounter];
        }
    }

    if (TaxRate > 0) {
        TotalCost = TotalCost + (TotalCost * (TaxRate / 100));
    }

    $('#CreditsAmount').html(TotalCost.toFixed(2));
}

function dotView(NumberToReplace, ObjectToReplace) {
    var Pattern = /(-?\d+)(\d{3})/;
    var NewNumber = NumberToReplace;

    while (Pattern.test(NewNumber))
    {
        NewNumber = NewNumber.replace(Pattern, "$1,$2");
    }

    ObjectToReplace.val(NewNumber);
}

function removeDots(NumberToReplace) {
    var NewNumber = "";

    for (var TMPCounter = 0; TMPCounter < NumberToReplace.length; TMPCounter++)
    {
        var SubString = NumberToReplace.substring(TMPCounter, TMPCounter + 1);

        if ((SubString != ',') && (SubString != '.'))
        {
            NewNumber += SubString;
        }
    }

    return NewNumber;
}

