$(document).ready(function () {
    $('#tags-table-form').submit(function () {
        if ($('#Command').val() == 'DeleteTags') {
            if (!confirm(language_object.screen['1088'])) {
                return false;
            }
        }
    });
    $('#create-tag-link').click(function () {
        var tag_name = prompt(language_object.screen['1091']);
        if (tag_name != null) {
            $('#Command').val('CreateTag');
            $('#Tag').val(tag_name);
            $('#tags-table-form').submit();
        }
        return false;
    });
    $('.update-link').click(function () {
        var new_tag_name = prompt(language_object.screen['1091'], $(this).text());
        if (new_tag_name != null) {
            var tag_id = $(this).attr('tagid');
            $('#Command').val('UpdateTag');
            $('#Tag').val(new_tag_name);
            $('#TagID').val(tag_id);
            $('#tags-table-form').submit();
        }
        return false;
    });
    
    $("#create-tag-form").validate({
        rules: {
            Tag: {
                required: true
            }
        },
        messages: {
            Tag: {
                required: "TagName is required."
            }
        }
    });
});