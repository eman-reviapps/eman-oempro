function on_opt_in_subscribe_to_click() {
    var value = $('#OptInSubscribeToEnabled').get(0).checked;
    if (value) {
        $('#OptInSubscribeTo').show();
    } else {
        $('#OptInSubscribeTo').hide();
    }
}

function on_opt_in_unsubscribe_from_click() {
    var value = $('#OptInUnsubscribeFromEnabled').get(0).checked;
    if (value) {
        $('#OptInUnsubscribeFrom').show();
    } else {
        $('#OptInUnsubscribeFrom').hide();
    }
}

function on_subscription_confirmation_pending_click() {
    var value = $('#SubscriptionConfirmationPendingPageURLEnabled').get(0).checked;
    if (value) {
        $('#SubscriptionConfirmationPendingPageURLContainer').show();
        $('#SubscriptionConfirmationPendingPageURL').focus();
    } else {
        $('#SubscriptionConfirmationPendingPageURL').val('');
        $('#SubscriptionConfirmationPendingPageURLContainer').hide();
    }
}

function on_subscription_confirmed_click() {
    var value = $('#SubscriptionConfirmedPageURLEnabled').get(0).checked;
    if (value) {
        $('#SubscriptionConfirmedPageURLContainer').show();
        $('#SubscriptionConfirmedPageURL').focus();
    } else {
        $('#SubscriptionConfirmedPageURL').val('');
        $('#SubscriptionConfirmedPageURLContainer').hide();
    }
}

function on_subscription_error_click() {
    var value = $('#SubscriptionErrorPageURLEnabled').get(0).checked;
    if (value) {
        $('#SubscriptionErrorPageURLContainer').show();
        $('#SubscriptionErrorPageURL').focus();
    } else {
        $('#SubscriptionErrorPageURL').val('');
        $('#SubscriptionErrorPageURLContainer').hide();
    }
}

function on_opt_out_subscribe_to_click() {
    if ($('#OptOutSubscribeToEnabled').length > 0) {
        var value = $('#OptOutSubscribeToEnabled').get(0).checked;
        if (value) {
            $('#OptOutSubscribeTo').show();
        } else {
            $('#OptOutSubscribeTo').hide();
        }
    }
}

function on_opt_out_unsubscribe_from_click() {
    var value = $('#OptOutUnsubscribeFromEnabled').get(0).checked;
    if (value) {
        $('#OptOutUnsubscribeFrom').show();
        $('#OptOutScope').get(0).checked = false;
    } else {
        $('#OptOutUnsubscribeFrom').hide();
    }
}

function on_opt_out_scope_click() {
    var value = $('#OptOutScope').get(0).checked;
    if (value) {
        $('#OptOutUnsubscribeFromEnabled').get(0).checked = false;
        on_opt_out_unsubscribe_from_click();
    }
}

function on_unsubscription_confirmed_click() {
    var value = $('#UnsubscriptionConfirmedPageURLEnabled').get(0).checked;
    if (value) {
        $('#UnsubscriptionConfirmedPageURLContainer').show();
        $('#UnsubscriptionConfirmedPageURL').focus();
    } else {
        $('#UnsubscriptionConfirmedPageURL').val('');
        $('#UnsubscriptionConfirmedPageURLContainer').hide();
    }
}

function on_unsubscription_error_click() {
    var value = $('#UnsubscriptionErrorPageURLEnabled').get(0).checked;
    if (value) {
        $('#UnsubscriptionErrorPageURLContainer').show();
        $('#UnsubscriptionErrorPageURL').focus();
    } else {
        $('#UnsubscriptionErrorPageURL').val('');
        $('#UnsubscriptionErrorPageURLContainer').hide();
    }
}

function on_opt_out_global_suppression_click() {
    var value = $('#OptOutAddToGlobalSuppressionList').get(0).checked;
    if (value) {
        $('#OptOutAddToSuppressionList').get(0).checked = false;
    }
}

function on_opt_out_suppression_click() {
    var value = $('#OptOutAddToSuppressionList').get(0).checked;
    if (value) {
        $('#OptOutAddToGlobalSuppressionList').get(0).checked = false;
    }
}

$(document).ready(function () {
    $('#OptInSubscribeToEnabled').click(on_opt_in_subscribe_to_click);
    $('#OptInUnsubscribeFromEnabled').click(on_opt_in_unsubscribe_from_click);
    $('#SubscriptionConfirmationPendingPageURLEnabled').click(on_subscription_confirmation_pending_click);
    $('#SubscriptionConfirmedPageURLEnabled').click(on_subscription_confirmed_click);
    $('#SubscriptionErrorPageURLEnabled').click(on_subscription_error_click);
    $('#OptOutSubscribeToEnabled').click(on_opt_out_subscribe_to_click);
//    $('#OptOutUnsubscribeFromEnabled').click(on_opt_out_unsubscribe_from_click);
    $('#OptOutScope').click(on_opt_out_scope_click);
    $('#OptOutAddToGlobalSuppressionList').click(on_opt_out_global_suppression_click);
//    $('#OptOutAddToSuppressionList').click(on_opt_out_suppression_click);
    $('#UnsubscriptionConfirmedPageURLEnabled').click(on_unsubscription_confirmed_click);
    $('#UnsubscriptionErrorPageURLEnabled').click(on_unsubscription_error_click);
    on_opt_in_subscribe_to_click();
    on_opt_in_unsubscribe_from_click();
    on_subscription_confirmation_pending_click();
    on_subscription_confirmed_click();
    on_subscription_error_click();
    on_opt_out_subscribe_to_click();
//    on_opt_out_unsubscribe_from_click();
    on_unsubscription_confirmed_click();
    on_unsubscription_error_click();

    $('.service-url').each(function () {
        var td = this;
        $('.service-url-status', td).text(Language['1386']);
        $.post(API_URL, {command: 'ListIntegration.TestURL', responseformat: 'JSON', url: $('.service-url-link', this).attr('href')}, function (data) {
            if (data.Success) {
                $('.service-url-status', td).text(Language['1383']);
            } else {
                $('.service-url-status', td).text(Language['1384']).removeClass('small');
            }
        }, 'json');
    });

    $('.remove-service-url').click(function () {
        $('#service-row-' + $(this).attr('urlid')).fadeOut();
        $.post(API_URL, {command: 'ListIntegration.DeleteURLs', urls: $(this).attr('urlid')});
        return false;
    });

    $('#service-url-form-button').click(function () {
        $('#Command').val('NewURL');
        $('#list-settings').attr('action', $('#list-settings').attr('action') + '#WebServiceIntegration').submit();
        return false;
    });
});