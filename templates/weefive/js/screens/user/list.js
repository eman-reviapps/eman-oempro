$(document).ready(function() {
	$('#list-subscribers-menu').click(function() {
		$(this).addClass('open');
	});
	$('#list-subscribers-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#list-subscribers-menu') == false) {
			$('#list-subscribers-menu').removeClass('open');
		}
	});
});