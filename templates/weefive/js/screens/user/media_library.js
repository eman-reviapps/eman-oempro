var media_library = {
	on_upload: function() {
		$('#loading-indicator').show();
		$('#media-upload-form').hide();
	},
	upload_success: function() {
		window.location = media_library_url.replace('_folderid_', current_folder) + 'upload_success';
	},
	upload_error: function(message) {
		if (message != '') {
			alert(message);
		}
		window.location = media_library_url + 'upload_error';
	},
	delete_file: function(file_id) {
		if (confirm(language_object['1055'])) {
			$('#file-'+file_id).fadeOut();
			$.post(API_URL, {
				Command: 'Media.Delete',
				MediaID: file_id,
				ResponseFormat: 'JSON'
			}, function(data) {

			});
		}
	}
};

$(document).ready(function() {
	$('#media-upload-form').submit(media_library.on_upload);
	$('#new-media-file').change(function() {
		$('#media-upload-form').submit();
	});
	$('.folder').click(function() {
		window.location.href = media_library_url.replace('_folderid_', $(this).attr('folderid'));
		return false;
	});
	$('.file-delete').click(function() {
		media_library.delete_file($(this).attr('fileid'));
		return false;
	});

	$('#create-folder-link').click(function() {
		var folder_name = prompt(language_object['1880']);
		if (folder_name == '')
			return false;

		$.post(API_URL, {
			Command: 'Media.FolderCreate',
			FolderName: folder_name,
			ParentFolderID: current_folder,
			ResponseFormat: 'JSON'
		}, function(data) {
			window.location.href = media_library_url.replace('_folderid_', data.FolderID) + 'folder_create';
		}, 'json');

		return false;
	});

	$('#delete-folder-link').click(function() {
		if (current_folder == 0)
			return false;

		if (! confirm(language_object['1883']))
			return false;

		$.post(API_URL, {
			Command: 'Media.FolderDelete',
			FolderID: current_folder,
			ResponseFormat: 'JSON'
		}, function(data) {
			window.location.href = media_library_url.replace('_folderid_', parent_folder) + 'folder_delete';
		}, 'json');

		return false;
	});
});
