function setup_form_button_label() {

	if ($('#OptInMode').val() == 'Double') {
		$('#form-button strong').text(Language['0948']);
	} else {
		$('#form-button strong').text(Language['0947']);
	}
}

$(document).ready(function() {
	$('#OptInMode').change(setup_form_button_label);
	setup_form_button_label();
});