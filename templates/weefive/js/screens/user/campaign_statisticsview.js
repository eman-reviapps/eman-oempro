var active_date_range = '7';
var flash_chart_element = '';

function setChartOptions(chartel, option, value) {
	chartel.attr('data-oempro-chart-options-' + option, value);
}

function initChart(chartel) {
	chartel.data('init')();
}

function amChartInited(chart_id) {
	// flash_chart_element = document.getElementById('activity-chart');
}

function switch_chart_data_range(tabcollection, tabid) {
	if (tabid == 'tab-7d') {
		active_date_range = '7';
	}
	if (tabid == 'tab-15d') {
		active_date_range = '15';
	} else if (tabid == 'tab-30d') {
		active_date_range = '30';
	}

	setChartOptions($('#activity-chart-container'), 'data', statistic_url + active_date_range);
	initChart($('#activity-chart-container'));
}

$(document).ready(function() {
	$('#activity-table a').live('click', function() {
		$(this).next().toggle();
		$(this).toggleClass('subscriber-row-selected');
		return false;
	});
});