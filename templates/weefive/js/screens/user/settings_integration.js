$(document).ready(function () {
	if ($('#PreviewMyEmailAccount').length > 0) {
		$('#PreviewMyEmailAccount').keyup(function() {
			$('#account-url-preview').text($(this).val());
		});
		$('#account-url-preview').text($('#PreviewMyEmailAccount').val());
	}
	if ($('#HighriseAccount').length > 0) {
		$('#HighriseAccount').keyup(function() {
			$('#account-url-preview').text($(this).val());
		});
		$('#account-url-preview').text($('#HighriseAccount').val());
	}
});