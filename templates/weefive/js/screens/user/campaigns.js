(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {
          
          var parents = $(this).parents().get();
         
          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }
          
          return false;
        }
    });
})(jQuery);

$(document).ready(function () {
	$('#campaigns-table-form').submit(function() {
		if ($('#Command').val() != 'AssignTag') {
			if (!confirm(language_object.screen['0727'])) {
				return false;
			}
		}
	});
	
	$('#assign-tag-menu').click(function() {
		$(this).addClass('open');
		return false;
	});
	$('#assign-tag-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#assign-tag-menu') == false) {
			$('#assign-tag-menu').removeClass('open');
		}
	});
	$('#assign-tag-menu li a').click(function() {
		$('#Command').val('AssignTag');
		$('#TagID').val($(this).attr('id').replace('tag-id-', ''));
		$('#campaigns-table-form').submit();
	});

	$('#compare-performances-action').click(function() {
		var selected_campaigns = new Array();
		$('#campaigns-table input.grid-check-element:checked').each(function() {
			selected_campaigns.push($(this).val());
		});
		if (selected_campaigns.length < 2) {
			$('#compare-error .message').text(language_object.screen['0957']);
			$('#compare-error').show();
			fade_out_page_message($('#compare-error'));
			return false;
		} else if (selected_campaigns.length > 5) {
			$('#compare-error .message').text(language_object.screen['0959']);
			$('#compare-error').show();
			fade_out_page_message($('#compare-error'));
			return false;
		}
		window.location = $(this).attr('href')+selected_campaigns.join('-');
		return false;
	});
});