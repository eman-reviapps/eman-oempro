function on_trigger_type_change() {
	value = $('#auto-responder-create #AutoResponderTriggerType').val();
	if (value == 'OnSubscriberLinkClick') {
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-DateField').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue2').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue').show();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-Campaign').hide();
	} else if (value == 'OnSubscriberCampaignOpen') {
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-DateField').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue2').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-Campaign').show();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue').hide();
	} else if (value == 'OnSubscriberDate') {
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-DateField').show();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue2').show();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-Campaign').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue').hide();
	} else {
		$('#auto-responder-create #form-row-AutoResponderTriggerValue').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue-DateField').hide();
		$('#auto-responder-create #form-row-AutoResponderTriggerValue2').hide();
	}
}

function on_trigger_time_type_change() {
	value = $('#auto-responder-create #TriggerTimeType').val();
	if (value != 'Immediately') {
		$('#auto-responder-create #TriggerTime').show();
	} else {
		$('#auto-responder-create #TriggerTime').hide();
	}
}

$(document).ready(function () {
	$('#auto-responder-create #AutoResponderTriggerType').change(on_trigger_type_change);
	$('#auto-responder-create #TriggerTimeType').change(on_trigger_time_type_change);
	on_trigger_type_change();
	on_trigger_time_type_change();
});