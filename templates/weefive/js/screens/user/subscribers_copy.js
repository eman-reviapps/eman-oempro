function switch_combo_description() {
    var value = $('#Subscribers option:selected').val().replace(/\s/gi, '-');
    $('#combo-description p').hide();
    if (value == 'Suppressed') {
        $('#form-row-AddToSuppresionList').hide();
    } else {
        $('#form-row-AddToSuppresionList').show();
    }

    if (value == 'Not-opted-in-for-days')
    {
        $('#opt-in-days').show();
        $('#NotOptedInForDays').focus();
    } else {
        $('#opt-in-days').hide();
    }

    if (value == 'Copy-and-paste')
    {
        $('#copy-and-paste').show();
        $('#CopyAndPaste').focus();
    } else {
        $('#copy-and-paste').hide();
    }

    if (value != 'Active' && value != 'Suppressed' && value != 'Not-opted-in-for-days' && value != 'Copy-and-paste' && value != 'Soft-bounced' && value != 'Hard-bounced') {
        $('#message-Segments').show();
    } else {
        $('#message-' + value).show();
    }
}

$(document).ready(function () {
    $('#Subscribers').change(switch_combo_description);
    switch_combo_description();

    // add the rule here
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");

    // configure your validation
    $("#subscribers-copy").validate({
        rules: {
            Lists: {valueNotEquals: ""}
        },
        messages: {
            Lists: {valueNotEquals: "<br/>Please select list first!"}
        }
    });
});