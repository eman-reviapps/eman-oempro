var content_edit_library = {
	init_handlers : function() {
		$('#SameNameEmail').click(function() {
			if (this.checked == true) {
				$('#replyto-name-email-container').hide();
			} else {
				$('#replyto-name-email-container').show();
			}
		});
		$('#EnableGoogleAnalytics').click(function() {
			if (this.checked == true) {
				$('#form-row-GoogleAnalyticsDomains').show();
			} else {
				$('#form-row-GoogleAnalyticsDomains').hide();
			}
		});

		if ($('#SameNameEmail').length > 0) {
			if ($('#SameNameEmail').get(0).checked == true) {
				$('#replyto-name-email-container').hide();
			} else {
				$('#replyto-name-email-container').show();
			}
		}

		if ($('#EnableGoogleAnalytics').length > 0) {
			if ($('#EnableGoogleAnalytics').get(0).checked == true) {
				$('#form-row-GoogleAnalyticsDomains').show();
			} else {
				$('#form-row-GoogleAnalyticsDomains').hide();
			}
		}


		$('#ContentType').change(function() {
			content_edit_library.switch_content_type($(this).val());
		});
		
		content_edit_library.switch_content_type($('#ContentType').val());
		
		if (attachments.length > 0) {
			$.each(attachments, function() {
				content_edit_library.add_attachment_element(this);
			});
		}
		
		$('#AttachmentFile').change(function() {
			$('#uploading-indicator').show();
			$('#file-upload-form').submit();
		});
		
		$('.attachment-checkbox').live('click', function() {
			if (this.checked == false) {
				content_edit_library.remove_attachment_element($(this).val());
			}
		});
	
		if (wysiwyg_enabled) {
			$('#HTMLContent').tinymce(tinymce_config);
		}
	},
	switch_content_type : function(type) {
		switch (type) {
			case 'HTML':
				$('#form-row-HTMLContent').show();
				$('#form-row-PlainContent').hide();
				break;
			case 'Plain':
				$('#form-row-HTMLContent').hide();
				$('#form-row-PlainContent').show();
				break;
			case 'Both':
				$('#form-row-HTMLContent').show();
				$('#form-row-PlainContent').show();
				break;
		}
	},
	add_attachment_element : function(attachment_object) {
		$('#AttachmentFile').val('');
		$('#uploading-indicator').hide();
		
		attachment_object.FileSize = (attachment_object.FileSize/1024).toFixed(2);
		attachment_object.FileSize = attachment_object.FileSize > 1000 ? ((attachment_object.FileSize/1024).toFixed(2))+'MB' : attachment_object.FileSize+'KB';
		
		var content = $('#attachment-template').html().replace('_FileName_', attachment_object.FileName);
			content = content.replace('_FileSize_', attachment_object.FileSize);
			content = content.replace('_AttachmentID_', attachment_object.AttachmentID);
			content = content.replace('_AttachmentMD5ID_', attachment_object.AttachmentMD5ID);
		
		$('#attachment-template')
			.clone()
			.attr('id', 'attachment-element-'+attachment_object.AttachmentID)
			.html(content)
			.appendTo('#attachment-container')
			.show('slow');
	},
	remove_attachment_element : function(id) {
		$('#attachment-element-'+id).fadeOut();
		$.post(api_url, { Command: 'Attachment.Delete', ResposeFormat: 'JSON', AttachmentID: id});
	}
};
