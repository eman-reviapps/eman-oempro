$(document).ready(function () {
    $("#delete_btn").click(function () {
        bootbox.confirm({
            title: "<span class='bold' >" + "Confirm Delete" + "</span>",
            message: language_object.screen['0938'],
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn purple-sharp'
                },
                cancel: {
                    label: 'No',
                    className: 'btn btn-danger'
                }
            },
            callback: function (result) {

                if (result == true)
                {
                    $('#campaigns-table-form').submit();
                }
            }
        });
    });
//    $('#campaigns-table-form').submit(function (e) {
//        if (!confirm(language_object.screen['0938'])) {
//            return false;
//        }
//    });
});