function applyFilters()
{
    if ($('#Lists').val() == 0)
    {
        $(".btn_move").hide();
    } else
    {
        $(".btn_move").show();
    }

    url = APP_URL + "/user/suppressionlist/search";

    $.ajax({
        type: "POST",
        data: {
            'listid': $('#Lists').val(),
        },
        url: url,
        success: function (data) {
            $("#suppression_div").html(data)
        },
        async: false // <- this turns it into synchronous
    });

}
$(document).ready(function () {
    $('#Lists').change(function () {
        applyFilters();
    });
});