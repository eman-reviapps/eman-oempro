var active_chart = 'opens-clicks';
var active_date_range = '7';
var flash_chart_element = '';
var reload_interval = false;
var active_tab_id = 'item-sub-report-opens-vs-clicks';

function setChartOptions(chartel, option, value) {
	chartel.attr('data-oempro-chart-options-' + option, value);
}

function initChart(chartel) {
	chartel.data('init')();
}

function amChartInited(chart_id) {
}

function set_active_chart_tab(tabid) {
	active_tab_id = tabid;
	if (tabid == 'item-sub-report-opens-vs-clicks') {
		active_chart = 'opens-clicks';
	} else if (tabid == 'item-sub-report-opens-vs-unsubscriptions') {
		active_chart = 'opens-unsubscriptions';
	}
	setChartOptions($('#activity-chart-container'), 'data', APP_URL+"/user/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range);
	initChart($('#activity-chart-container'));
}

function switch_chart_data_range(tabcollection, tabid) {
	if (tabid == 'tab-7d' && flash_chart_element != '') {
		active_date_range = '7';
	}
	if (tabid == 'tab-15d' && flash_chart_element != '') {
		active_date_range = '15';
	} else if (tabid == 'tab-30d') {
		active_date_range = '30';
	}
	setChartOptions($('#activity-chart-container'), 'data', APP_URL+"/user/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range);
	initChart($('#activity-chart-container'));
}

var sparkline_images = null;

function update_statistics() {
	var api_parameters = {
		command: 'Campaign.Get',
		campaignid: CampaignID,
		responseformat: 'JSON'
	};
	$.post(API_URL, api_parameters, function(data) {
		var campaign = data.Campaign;
		var splitTest = campaign.SplitTest;
		
		var isCampaignButtonContainerHidden = false;
		if (splitTest != false && splitTest.RelWinnerEmailID == 0) {
			$('#campaign-button-container').hide();
			isCampaignButtonContainerHidden = true;
		} else {
			$('#campaign-button-container').show();
		}
		
		if (campaign.CampaignStatus == 'Sending' || campaign.CampaignStatus == 'Paused') {
			$('.campaign-status-sending').show();
			$('.campaign-status-sent').hide();

			var send_percentage = Math.round((100 * campaign.TotalSent) / campaign.TotalRecipients);
			send_percentage = send_percentage + '%';
			$('#campaign-send-progress').animate({width: send_percentage}, {duration:500, queue: false, step: function(i) {
				$('#campaign-send-progress').text((i > 0 ? Number(i).toFixed() + (i > 7 ? '%' : '') : ''));
			}});
		} else {
			$('.campaign-status-sending').hide();
			$('.campaign-status-sent').show();
		}

		if (campaign.CampaignStatus == 'Sending') {
			$('#campaign-pause-button').show();
		} else {
			$('#campaign-pause-button').hide();
		}

		if (campaign.CampaignStatus == 'Ready' && (campaign.ScheduleType == 'Future' || campaign.ScheduleType == 'Recursive')) {
			$('#campaign-cancel-schedule-button').show();
		} else {
			$('#campaign-cancel-schedule-button').hide();
		}

		if (campaign.CampaignStatus == 'Sending' || (campaign.CampaignStatus == 'Ready' && campaign.ScheduleType == 'Immediate')) {
			$('#campaign-cancel-sending-button').show();
		} else {
			$('#campaign-cancel-sending-button').hide();
		}
		
		if (campaign.CampaignStatus == 'Paused') {
			$('#campaign-resume-button').show();
		} else {
			$('#campaign-resume-button').hide();
		}

		var displayedButtons = 0;
		$('#campaign-button-container div a').each(function() {
			if ($(this).css('display') != 'none') {
				displayedButtons++;
			}
		});

		if (displayedButtons < 1) {
			$('#campaign-button-container').hide();
		} else {
			if (isCampaignButtonContainerHidden == false) {
				$('#campaign-button-container').show();
			}
		}
		
		$('#unique-open-count').text(campaign.UniqueOpens);
		$('#unique-click-count').text(campaign.UniqueClicks);
		$('#unqiue-forward-count').text(campaign.UniqueForwards);
		$('#unique-browser-view-count').text(campaign.UniqueViewsOnBrowser);
		$('#total-unsubscription-count').text(campaign.TotalUnsubscriptions);
		var totalBounces = parseInt(campaign.TotalHardBounces, 10) + parseInt(campaign.TotalSoftBounces, 10);
		$('#total-bounces').text(totalBounces);
		$('#hard-bounce-ratio').text(Math.round((campaign.TotalHardBounces * 100) / totalBounces) + '%');
		$('#soft-bounce-ratio').text(Math.round((campaign.TotalSoftBounces * 100) / totalBounces) + '%');
		$('#total-sent-count').text(campaign.TotalSent);
		$('#total-sent-count2').text(campaign.TotalSent);
		$('#send-process-finished-on').text(campaign.SendProcessFinishedOn);
		$('#total-recipients').text(campaign.TotalRecipients);
		$('#total-recipients').text(campaign.TotalRecipients);
		$('#total-failed').text(campaign.TotalFailed);
		$('#open-ratio').text(campaign.TotalSent > 0 ? Math.ceil((campaign.UniqueOpens * 100) / campaign.TotalSent) + '%' : '0%');
		$('#bounce-ratio').text(campaign.TotalSent > 0 ? campaign.HardBounceRatio + '%' : '0%');
		$('#not-opened-ratio').text(campaign.TotalSent > 0 ? 100 - (Math.ceil(((campaign.UniqueOpens * 100) / campaign.TotalSent)) + (campaign.HardBounceRatio * 1)) + '%' : '100%');

		update_sparkline_images();
		setTimeout(update_statistics, 5000);
		
	}, 'json');
}

function update_sparkline_images() {
	random_number = Math.floor(Math.random() * (99999 - 11111 + 1) + 11111);
	sparkline_images.each(function() {
		if (! $(this).attr('originalsrc')) {
			$(this).attr('originalsrc', $(this).attr('src'));
		}
		$(this).attr('src', $(this).attr('originalsrc') + '/?' + random_number);
	});
}

$(document).ready(function() {
	sparkline_images = $('#sparkline-container img');
	update_statistics();
	// reload_interval = setInterval('set_active_chart_tab(active_tab_id)', 5000);
});