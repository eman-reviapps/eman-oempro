function setup_split_testing() {
	if ($('#SplitTesting').length > 0) {
		if ($('#SplitTesting').get(0).checked) {
			$('#split-testing-options').show();
		} else {
			$('#split-testing-options').hide();
		}
	}
}

function test_size_slide() {
	value = $("#slider-range-min").slider('value');
	position = $('.ui-slider-handle', '#slider-range-min').position();

	$('#test-size').text(value + '%');
	$('#winner-size').text((100 - value) + '%');
	$('#test-recipients-bar').css('width', position.left+'px');
	$('#test-recipients-bar-ratio').text(value + '%');
	$('#winning-recipients-bar-ratio').text((100 - value) + '%');
	$('#winning-recipients-bar').css('width', ($('#slider-range-min').width() - position.left)+'px');

	$('#TestSize').val(value);
}

$(document).ready(function() {
	$("#slider-range-min").slider({
		change: test_size_slide,
		slide: test_size_slide,
		range: 'min',
		step: 2,
		min: 2,
		max: 98
	});
	$("#slider-range-min").slider('option', 'value', $('#TestSize').val());
	test_size_slide();
	setup_split_testing();
	$('#SplitTesting').click(setup_split_testing);
	if ($('#TestDuration').val() > 0) {
		var base = 3600;
		var value = $('#TestDuration').val() / base;
		if (value > 23) {
			base = 86400;
			value = $('#TestDuration').val() / base;
		}
		$('#TestDurationMultiplier option[value="'+value+'"]').get(0).selected = true;
		$('#TestDurationBaseSeconds option[value="'+base+'"]').get(0).selected = true;
	}
});