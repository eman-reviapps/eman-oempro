var timesSelect = '';
var campaignSelect = '';

Array.prototype.inArray = function (value) {
	var i;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			return true;
		}
	}
	return false;
};

function isArray(input) {
	return typeof(input)=='object' && (input instanceof Array);
}

function convertArrayToRuleBoard() {
	$.each(rules, function(index, data) {
		if (! isArray(data)) {
			if (! data) {
				return;
			}
			data.field = data.field.replace('CustomField', '');
			if (data.field == '0' || data.field == '') {
				return;
			}
			var newRule = addRule(data.field, fieldLabels[data.field].fieldLabel, fieldLabels[data.field].fieldType, fieldLabels[data.field].validationMethod, fieldLabels[data.field].values, $('ul#rule-list'));
			if (data.field == 'Opens' || data.field == 'Clicks' || data.field == 'BrowserViews' || data.field == 'Forwards') {
				applyRuleActivityData(newRule, data.campaignFilter, data.campaignID, data.timesFilter, data.timesValue, data.field, data.field == 'Clicks' ? data.specificLink : '');
			} else {
				applyRuleData(newRule, data.operator, data.filter);
			}
		} else {
			var newRuleGroup;
			$.each(data, function(index, subData) {
				subData.field = subData.field.replace('CustomField', '');
				if (subData.field == '0' || subData.field == '') {
					return;
				}
				if (index < 1) {
					var newRule = addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, $('ul#rule-list'));
					var array = addRuleGroup(newRule);
					newRuleGroup = array[0];
					applyRuleData(array[1], subData.operator, subData.filter);
				} else {
					var newRule = addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, newRuleGroup);
					applyRuleData(newRule, subData.operator, subData.filter);
				}
			});
		}
	});
}

function applyRuleData(ruleElement, operator, filter) {
	$('.rule-operator > option[value="'+operator+'"]', ruleElement).attr('selected','true');
	$('input.filter', ruleElement).val(filter);
	$('select.filter > option[value="'+filter+'"]', ruleElement).attr('selected','true');
	if ($('.rule-operator', ruleElement).val() == 'Is set' || $('.rule-operator', ruleElement).val() == 'Is not set') {
		$('.rule-operator', ruleElement).next().hide();
	} else {
		$('.rule-operator', ruleElement).next().show();
	}
}

function applyRuleActivityData(ruleElement, campaignFilter, campaignID, timesFilter, timesValue, field, specificLink) {
	$('.specific-link', ruleElement).val(specificLink);
	if (specificLink != '') {
		$('.add-specific-link', ruleElement).hide();
		$('.specific-link', ruleElement).show();
		$('.remove-specific-link-container', ruleElement).show();
	}
	$('.campaign-select option[value="'+(campaignFilter == 'Specific' ? campaignID : campaignFilter)+'"]', ruleElement).attr('selected', 'true');
	$('.times-select option[value="'+timesFilter+'"]', ruleElement).attr('selected', 'true');
	$('.times-input', ruleElement).val(timesValue);
}

function convertRuleBoardToString() {
	var ruleArray = [];
	var subGroupRuleArray = [];
	var ruleBoardId = '#rule-list';
	var activityRules = $(ruleBoardId + ' > li.rule.activity');
	$.each(activityRules, function(index, data) {
		ruleArray.push(convertActivityRuleLiToString(data));
	});
	var informationRules = $(ruleBoardId + ' > li.rule.information');
	$.each(informationRules, function(index, data) {
		ruleArray.push(convertInformationRuleLiToString(data));
	});
	var subGroupRules = $(ruleBoardId + ' > li.sub-group-container');
	$.each(subGroupRules, function(index, eachSubGroup) {
		var subGroupInformationRules = $('ul > li.rule.information', eachSubGroup);
		var tmpGroupArray = [];
		$.each(subGroupInformationRules, function(index, data) {
			tmpGroupArray.push(convertInformationRuleLiToString(data));
		});
		subGroupRuleArray.push('((!'+tmpGroupArray.join(',,,')+'!))');
	});

	ruleString = ruleArray.join(',,,');
	if (subGroupRuleArray.length > 0) {
		ruleString += ',::,' + subGroupRuleArray.join(',#,');
	}
	return ruleString;
}

function convertInformationRuleLiToString(liElement) {
	var field = $(liElement).attr('fieldid');
	var operator = $('.rule-operator', liElement).val();
	var filter = $('.filter', liElement).val();
	if (! defaultFields.inArray(field)) {
		field = 'CustomField' + field;
	}
	return '[['+field+']||['+operator+']||['+filter+']]';
}

function convertActivityRuleLiToString(liElement) {
	var field = $(liElement).attr('fieldid');
	var campaignFilter = $('.campaign-select', liElement).val();
	var campaignID = (campaignFilter != 'Total' && campaignFilter != 'Any') ? $('.campaign-select', liElement).val() : 0;
	if (campaignID != 0) {
		campaignFilter = 'Specific';
	}
	var timesFilter = $('.times-select', liElement).val();
	var timesValue = $('.times-input', liElement).val();
	if (field == 'Clicks') {
		var specificLink = $('.specific-link', liElement).val();
	}
	return '[['+field+']||['+campaignFilter+']||['+campaignID+']||['+timesFilter+']||['+timesValue+']'+(field == 'Clicks' ? '||['+specificLink+']': '')+']';
}

function clearBoard() {
	$('#rule-list').html('');
	return false;
}

function updateRuleSeparators() {
	$('#rule-list li.rule-separator').each(function() {
		if ($(this).text() == 'AND') {
			$(this).text('OR');
		} else {
			$(this).text('AND');
		}
	});
	return false;
}

function addRuleSeparator(parent) {
	var separatorLabel = '';
	
	if (parent.attr('id') != 'rule-list') {
		separatorLabel = ($('#SegmentOperator').val().toUpperCase() == 'AND' ? 'OR' : 'AND');
	} else {
		separatorLabel = $('#SegmentOperator').val().toUpperCase();
	}
	
	var html = $('<li class="rule-separator">'+separatorLabel+'</li>');
	$('li.rule:last', parent).before(html);
}

function addRule(fieldId, fieldLabel, fieldType, validationMethod, values, parent) {
	if (fieldId == 'Opens' || fieldId == 'Clicks' || fieldId == 'BrowserViews' || fieldId == 'Forwards') {

		var description = '';
		if (fieldId == 'Clicks') {
			description = '<div class="rule-description"><a href="#" class="add-specific-link">'+lang['1414']+'</a><span class="remove-specific-link-container" style="display:none">'+lang['1416']+'<br /><a href="#" class="remove-specific-link">'+lang['1415']+'</a></span></div>';
		}

		var html = $('<li fieldid="'+fieldId+'" class="rule activity"><div class="delete">X</div><span class="field">'+fieldLabel+'</span> '+'<input type="text" value="" class="specific-link" style="width:250px;display:none" />'+campaignSelect+' '+timesSelect+'<input class="times-input" type="text" value="0" style="width:14px" /> times'+description+'</li>');
	} else {
		var operatorOptions = '';
		if (validationMethod != '') {
			operatorOptions += '<select class="rule-operator">';
			if (fieldType == 'Date field') {
				validationMethod = 'Date';
			} else if (fieldType == 'Time field') {
				validationMethod = 'Time';
			}
			for (i=0;i<ruleOperators[validationMethod].length;i++) {
				data = ruleOperators[validationMethod][i];
				if (values != '' && (data.Value == 'Contains' || data.Value == 'Does not contain' || data.Value == 'Begins with' || data.Value == 'Ends with' || data.Value == 'Equals to' || data.Value == 'Is greated than' || data.Value == 'Is smaller than' || data.Value == 'Is before' || data.Value == 'Is after')) {
					continue;
				};
				operatorOptions += '<option value="'+data.Value+'">'+data.Label+'</option>';
			}
			operatorOptions += '</select>';
		}

		var description = '';
		if (validationMethod == 'Date') {
			description = '<div class="rule-description">'+lang['1406']+'</div>';
		}

		var field = '';
		if (values != '') {
			field += '<select class="filter">';
			var tmpArray1 = values.split(',,,');
			for (i=0;i<tmpArray1.length;i++) {
				tmpArray1[i] = tmpArray1[i].replace(']]*', '').replace(']]', '').replace('[[','');
				tmpArray1[i] = tmpArray1[i].split(']||[');
				field += '<option value="'+tmpArray1[i][1]+'">'+tmpArray1[i][0]+'</option>';
			}
			field += '</select>';
		} else {
			field = '<input type="text" class="filter" />';
		}

		var html = $('<li fieldid="'+fieldId+'" class="rule information"><div class="delete">X</div><div class="add">+</div><span class="field">'+fieldLabel+'</span> '+operatorOptions+' '+field+description+'</li>');
	}
	
	$(parent).append(html);
	if ($('li.rule', parent).length > 1) {
		addRuleSeparator(parent);
	}
	return html;
}

function addRuleGroup(item) {
	var selectValues = [];
	$('select', item).each(function() {
		selectValues.push($(this).val());
	});
	var ruleItem = item.clone();
	$('select', ruleItem).each(function(index) {
		$('option[value="'+selectValues[index]+'"]', this).attr('selected','true');
	});
	var ruleGroupLI = $('<li class="sub-group-container"></li>');
	var ruleGroupUL = $('<ul class="sub-group"></ul>');
	var ruleActionsDiv = $('<div class="rule-board-actions"></div>');
	ruleGroupLI.append(ruleGroupUL);
	
	ruleGroupLI.append(ruleActionsDiv);
	var addRuleMenu = $('.add-new-rule-menu:last').clone();
	$('.main-action', addRuleMenu).text(lang['1407']);
	$('li.activity', addRuleMenu).remove();
	ruleActionsDiv.append(addRuleMenu);
	item.replaceWith(ruleGroupLI);
	
	ruleGroupUL.append(ruleItem);
	return [ruleGroupUL, ruleItem];
}

$(document).ready(function() {
	timesSelect = '<select class="times-select"><option value="At most">'+lang['1408']+'</option><option value="At least">'+lang['1409']+'</option><option value="Only">'+lang['1410']+'</option></select>';
	campaignSelect = '<select class="campaign-select"><option value="Total">'+lang['1413']+'</option><option value="Any">'+lang['1411']+'</option>';
	if (campaigns.length > 0) {
		campaignSelect += '<optgroup label="'+lang['1412']+'">';
		$.each(campaigns, function(index, data) {
			campaignSelect += '<option value="'+data['CampaignID']+'">'+data['CampaignName']+'</option>';
		});
		campaignSelect += '</optgroup>';
	}
	campaignSelect += '</select>';

	convertArrayToRuleBoard();
	
	$('#rule-list .delete').live('click', function() {
		var parentSubGroup = $(this).parent().parent('.sub-group');
		$(this).parent().remove();
		if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length == 1) {
			var item = $('li.rule', parentSubGroup);
			parentSubGroup.parent().replaceWith(item);
		} else if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length < 1) {
			parentSubGroup.parent().remove();
		}
		$('.rule-separator + .rule-separator').remove();
		$('.rule-separator:first-child').remove();
		$('.rule-separator:last-child').remove();
	});
	$('#rule-list .add').live('click', function() {
		addRuleGroup($(this).parent());
	});
	$('#clear-board-link').click(clearBoard);
	$('#SegmentOperator').change(updateRuleSeparators);
	$('.rule-operator').live('change', function() {
		if ($(this).val() == 'Is set' || $(this).val() == 'Is not set') {
			$(this).next().hide();
		} else {
			$(this).next().show();
		}
	});
	$('.add-specific-link').live('click', function() {
		var parentLi = $(this).parent().parent();
		$('.specific-link', parentLi).show();
		$('.remove-specific-link-container', parentLi).show();
		$('.specific-link', parentLi).focus();
		$(this).hide();
		return false;
	});
	$('.remove-specific-link').live('click', function() {
		var parentLi = $(this).parent().parent().parent();
		$('.specific-link', parentLi).val('').hide();
		$('.add-specific-link', parentLi).show();
		$(this).parent().hide();
		return false;
	});
	$('.main-action-with-menu').live('click', function() {
		$(this).addClass('open');
		return false;
	});
	$('.main-action-with-menu').live('mouseout', function(e) {
		if ($(e.relatedTarget).isChildOf('.main-action-with-menu') == false) {
			$(this).removeClass('open');
		}
	});
	$('.add-new-rule-menu ul a').live('click', function() {
		var parent = null;
		if ($(this).parent().isChildOf('li')) {
			var parentLi = $(this).parent().parent().parent().parent().parent();
			parent = $('ul.sub-group', parentLi);
		} else {
			parent = $('ul#rule-list');
		}
		addRule($(this).attr('customfieldid'), $(this).text(), $(this).attr('fieldtype'), $(this).attr('validationmethod'), $(this).attr('values'), parent);
		$('.add-new-rule-menu').removeClass('open');
		return false;
	});

	$('#segment-create').submit(function() {
		$('#SegmentRules').val(convertRuleBoardToString());
	});
});