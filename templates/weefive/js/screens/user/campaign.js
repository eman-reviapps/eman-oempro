(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {
          
          var parents = $(this).parents().get();
         
          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }
          
          return false;
        }
    });
})(jQuery);

$(document).ready(function() {
	$('#share-campaign-menu').click(function() {
		$(this).addClass('open');
	});
	$('#share-campaign-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#share-campaign-menu') == false) {
			$('#share-campaign-menu').removeClass('open');
		}
	});
	
	$('.tag-label a').click(function() {
		var a = $(this);
		var parent_span = $(this).parent('.tag-label');
		parent_span.fadeTo('fast', 0.33);
		$.post(api_url, {command: 'Tag.UnassignFromCampaigns', tagid: $(this).attr('id').replace('tag-id-', ''), campaignids: CampaignID}, function() {
			parent_span.remove();
		});
		a.click(function() { return false; });
	});
});