$(document).ready(function() {
	$('#EnableGoogleAnalytics').click(function() {
		if (this.checked == true) {
			$('#form-row-GoogleAnalyticsDomains').show();
		} else {
			$('#form-row-GoogleAnalyticsDomains').hide();
		}
	});

	if ($('#EnableGoogleAnalytics').length > 0) {
		if ($('#EnableGoogleAnalytics').get(0).checked == true) {
			$('#form-row-GoogleAnalyticsDomains').show();
		} else {
			$('#form-row-GoogleAnalyticsDomains').hide();
		}
	}

});