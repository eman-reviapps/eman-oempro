var styleCache = {};

function loadStyle() {
	var selectedStyle = $('#FormStyles option:selected').val();
	if (! styleCache[selectedStyle]) {
		$.get(STYLE_URL+selectedStyle+'.css', function(data) {
			styleCache[selectedStyle] = '<style type="text/css" media="screen">'+data+'</style>';
			applyStyle(styleCache[selectedStyle]);
		});
	} else {
		applyStyle(styleCache[selectedStyle]);
	}
}

function applyStyle(styleString) {
	if ($('.o-form style').length > 0) {
		$('.o-form style').remove();
	}
	$('.o-form').append(styleString);
	updateHtmlCode();
}

function updateHtmlCode() {
	$('#subscription-html-code').val($('#form-preview').html());
}

function applySettings() {
	var selectedCustomFields = [];
	$('#CustomFields > option:selected').each(function() {
		selectedCustomFields.push(this.value);
	});
	var selectedLists = [LIST_ID];
	$('#Lists > option:selected').each(function() {
		selectedLists.push(this.value);
	});
	$('#form-preview').html('');
	$.post(API_URL, {
		command: 'ListIntegration.GenerateSubscriptionFormHTMLCode',
		responseformat: 'JSON',
		subscriberlistid: selectedLists.join(','),
		emailaddressstring: lang['1582'],
		subscribebuttonstring: lang['1308'],
		htmlspecialchars: 'false',
		customfields: selectedCustomFields.join(',')
	}, function(data) {
		formHtml = '<div class="o-form"><div class="o-form-header"><h2 id="o-form-title">'+lang['1716']+'</h2>';
		formHtml += '<p id="o-form-description">'+lang['1717']+'</p></div>';
		for (var i=0; i<data.HTMLCode.length; i++) {
			formHtml += data.HTMLCode[i];
		}
		formHtml += '</div>';
		$('#form-preview').html(formHtml);
		loadStyle();
	}, 'json');
}

$(document).ready(function() {
	$('#form-button').click(function() {
		applySettings();
		return false;
	});
	$('#FormStyles').change(loadStyle);
	$('.form-preview-container .tabs a').click(function() {
		$('.form-preview-container-content').hide();
		$('#'+$(this).attr('data-target')).show();
		$('.form-preview-container .tabs a').removeClass('selected');
		$(this).addClass('selected');
	});
	$('#form-preview').sortable({ items: 'div.o-form-row', axis: 'y', update: function() {
		updateHtmlCode();
	}});
	$('#form-preview input[type=submit]').live('click', function() { return false; });
	$('#form-preview #o-form-title').live('click', function() {
		var newtitle = prompt('Enter form title');
		if (newtitle == '') return;
		$(this).text(newtitle);
		updateHtmlCode();
	});
	$('#form-preview #o-form-description').live('click', function() {
		var newdesc = prompt('Enter form description');
		if (newdesc == '') return;
		$(this).text(newdesc);
		updateHtmlCode();
	});
	$('#form-preview label').live('click', function() {
		var newlabel = prompt('Enter label text');
		if (newlabel == '') return false;
		$(this).text(newlabel);
		updateHtmlCode();
		return false;
	});
	applySettings();
});