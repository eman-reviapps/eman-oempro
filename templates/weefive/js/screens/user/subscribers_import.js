function suppression_list_radio_click() {
	if ($(this).val() == 'none') {
		$('#import-settings').show();
	} else {
		$('#import-settings').hide();
	}
}

$(document).ready(function() {
	$('#suppression-list-radio-container input[type="radio"]').click(suppression_list_radio_click);
});