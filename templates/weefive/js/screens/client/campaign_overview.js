(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {
          
          var parents = $(this).parents().get();
         
          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }
          
          return false;
        }
    });
})(jQuery);


var active_chart = 'opens-clicks';
var active_date_range = '7';
var flash_chart_element = '';
var reload_interval = false;
var active_tab_id = 'item-sub-report-opens-vs-clicks';

function amChartInited(chart_id) {
	flash_chart_element = document.getElementById('activity-chart');
	if (! reload_interval) {
		reload_interval = setInterval('set_active_chart_tab(active_tab_id)', 5000);
	}
}

function set_active_chart_tab(tabid) {
	active_tab_id = tabid;
	if (tabid == 'item-sub-report-opens-vs-clicks') {
		active_chart = 'opens-clicks';
		if (flash_chart_element != '')  {
			$.get(APP_URL+"/client/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range, {}, 
				function(data)  {
					flash_chart_element.setData(data);
				}, 'text');
		}
	} else if (tabid == 'item-sub-report-opens-vs-unsubscriptions') {
		active_chart = 'opens-unsubscriptions';
		if (flash_chart_element != '')  {
			$.get(APP_URL+"/client/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range, {}, 
				function(data)  {
					flash_chart_element.setData(data);
				}, 'text');
		}
	}
}

function switch_chart_data_range(tabcollection, tabid) {
	if (tabid == 'tab-7d' && flash_chart_element != '') {
		active_date_range = '7';
		if (flash_chart_element != '')  {
			$.get(APP_URL+"/client/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range, {}, 
				function(data)  {
					flash_chart_element.setData(data);
				}, 'text');
		}
	}
	if (tabid == 'tab-15d' && flash_chart_element != '') {
		active_date_range = '15';
		if (flash_chart_element != '')  {
			$.get(APP_URL+"/client/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range, {}, 
				function(data)  {
					flash_chart_element.setData(data);
				}, 'text');
		}
	} else if (tabid == 'tab-30d') {
		active_date_range = '30';
		if (flash_chart_element != '')  {
			$.get(APP_URL+"/client/campaign/statistics/"+CampaignID+"/"+active_chart+"/"+active_date_range, {}, 
				function(data) {
					flash_chart_element.setData(data);
				}, 'text');
		}
	}
}

var sparkline_images = null;

function update_statistics() {
	var api_parameters = {
		command: 'Client.Campaign.Get',
		campaignid: CampaignID,
		responseformat: 'JSON'
	};
	$.post(API_URL, api_parameters, function(data) {
		var campaign = data.Campaign;
		
		if (campaign.CampaignStatus == 'Sending' || campaign.CampaignStatus == 'Paused') {
			$('.campaign-status-sending').show();
			$('.campaign-status-sent').hide();

			var send_percentage = Math.round((100 * campaign.TotalSent) / campaign.TotalRecipients);
			send_percentage = send_percentage + '%';
			$('#campaign-send-progress').animate({width: send_percentage}, {duration:500, queue: false, step: function(i) {
				$('#campaign-send-progress').text((i > 0 ? Number(i).toFixed() + (i > 7 ? '%' : '') : ''));
			}});
		} else {
			$('.campaign-status-sending').hide();
			$('.campaign-status-sent').show();
		}

		$('#unique-open-count').text(campaign.UniqueOpens);
		$('#unique-click-count').text(campaign.UniqueClicks);
		$('#unqiue-forward-count').text(campaign.UniqueForwards);
		$('#unique-browser-view-count').text(campaign.UniqueViewsOnBrowser);
		$('#total-unsubscription-count').text(campaign.TotalUnsubscriptions);
		var totalBounces = parseInt(campaign.TotalHardBounces, 10) + parseInt(campaign.TotalSoftBounces, 10);
		$('#total-bounces-count').text(totalBounces);
		$('#hard-bounce-ratio').text(Math.round((campaign.TotalHardBounces * 100) / totalBounces) + '%');
		$('#soft-bounce-ratio').text(Math.round((campaign.TotalSoftBounces * 100) / totalBounces) + '%');
		$('#total-sent-count').text(campaign.TotalSent);
		$('#total-sent-count2').text(campaign.TotalSent);
		$('#send-process-finished-on').text(campaign.SendProcessFinishedOn);
		$('#total-recipients').text(campaign.TotalRecipients);
		$('#total-recipients').text(campaign.TotalRecipients);
		$('#total-failed').text(campaign.TotalFailed);
		$('#open-ratio').text(campaign.TotalSent > 0 ? Math.ceil((campaign.UniqueOpens * 100) / campaign.TotalSent) + '%' : '0%');
		$('#bounce-ratio').text(campaign.TotalSent > 0 ? campaign.HardBounceRatio + '%' : '0%');
		$('#not-opened-ratio').text(campaign.TotalSent > 0 ? 100 - (Math.ceil(((campaign.UniqueOpens * 100) / campaign.TotalSent)) + (campaign.HardBounceRatio * 1)) + '%' : '100%');

		update_sparkline_images();
		setTimeout(update_statistics, 5000);
		
	}, 'json');
}

function update_sparkline_images() {
	random_number = Math.floor(Math.random() * (99999 - 11111 + 1) + 11111);
	sparkline_images.each(function() {
		if (! $(this).attr('originalsrc')) {
			$(this).attr('originalsrc', $(this).attr('src'));
		}
		$(this).attr('src', $(this).attr('originalsrc') + '/?' + random_number);
	});
}

$(document).ready(function() {
	sparkline_images = $('#sparkline-container img');
	update_statistics();
	$('#share-campaign-menu').click(function() {
		$(this).addClass('open');
	});
	$('#share-campaign-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#share-campaign-menu') == false) {
			$('#share-campaign-menu').removeClass('open');
		}
	});
});