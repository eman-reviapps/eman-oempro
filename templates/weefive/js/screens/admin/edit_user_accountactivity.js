var active_chart = 'Campaigns/Unsubscriptions';
var active_date_range = '30';
var flash_chart_element = '';

function amChartInited(chart_id)
	{
	flash_chart_element = document.getElementById('activity-chart');
	}

function set_active_chart_tab(tabid)
	{
	if (tabid == 'item-sub-report-unsubscriptions')
		{
		active_chart = 'Campaigns/Unsubscriptions';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	else if (tabid == 'item-sub-report-bounces')
		{
		active_chart = 'Campaigns/Bounces';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	else if (tabid == 'item-sub-report-spam')
		{
		active_chart = 'Campaigns/Spam';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	else if (tabid == 'item-sub-report-subscriptions')
		{
		active_chart = 'Subscriptions/Unsubscriptions';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	}

function switch_chart_data_range(tabcollection, tabid)
	{
	if (tabid == 'tab-1m' && flash_chart_element != '')
		{
		active_date_range = '30';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	else if (tabid == 'tab-3m')
		{
		active_date_range = '90';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
		}
	else if (tabid == 'tab-6m')
		{
		active_date_range = '180';
		if (flash_chart_element != '') 
			{
			$.get(APP_URL+"/admin/users/edit/"+UserID+"/AccountActivity/0/ChartData/"+active_chart+"/"+active_date_range, {}, 
				function(data) 
					{
					flash_chart_element.setData(data);
					}, 'text');
			}
	}
	}