$(document).ready(function () {
	$('#ButtonTestEmailDeliverySettings').click(function() {
		$('#Command').val('TestEmailDeliverySettings');
		$('#EmailDeliverySettings').submit();
		return false;
	});

	ShowHideDeliverySettings($('#SEND_METHOD option:selected').val());
	$('select[name="SEND_METHOD"]').change(function() {
		ShowHideDeliverySettings(this.value);
	});

	ShowHideSMTPAuth($('#SEND_METHOD_SMTP_AUTH > option:selected').val());
	$('select[name="SEND_METHOD_SMTP_AUTH"]').change(function() {
		ShowHideSMTPAuth(this.value);
	});

	ShowHideLoadBalancing($('#LOAD_BALANCE_STATUS > option:selected').val());
	$('select[name="LOAD_BALANCE_STATUS"]').change(function() {
		ShowHideLoadBalancing(this.value);
	});
	
	ShowHideBouncePOP3Settings($('#POP3_BOUNCE_STATUS > option:selected').val());
	$('select[name="POP3_BOUNCE_STATUS"]').change(function() {
		ShowHideBouncePOP3Settings(this.value);
	});

	ShowHideFBLPOP3Settings($('#POP3_FBL_STATUS > option:selected').val());
	$('select[name="POP3_FBL_STATUS"]').change(function() {
		ShowHideFBLPOP3Settings(this.value);
	});

	headerEmailTypeChange($('#emailType').val());
	$('#emailType').change(headerEmailTypeChange);

	$('#ButtonAddHeader').click(addEmailHeader);

	$('#removeEmailHeadersLink').click(removeEmailHeaders);
});

function ShowHideBouncePOP3Settings(ObjectValue)
	{
	if (ObjectValue == 'Enabled')
		{
		$('#bounce-pop3-settings').show();
		}
	else
		{
		$('#bounce-pop3-settings').hide();
		}
	}

function ShowHideFBLPOP3Settings(ObjectValue)
	{
	if (ObjectValue == 'Enabled')
		{
		$('#fbl-pop3-settings').show();
		}
	else
		{
		$('#fbl-pop3-settings').hide();
		}
	}

function ShowHideLoadBalancing(ObjectValue)
	{
	if (ObjectValue == 'true')
		{
		$('#sub-loadbalance-settings').show();
		}
	else
		{
		$('#sub-loadbalance-settings').hide();
		}
	}

function ShowHideSMTPAuth(ObjectValue)
	{
	if (ObjectValue == 'true')
		{
		$('#sub-smtp-settings-auth').show();
		}
	else
		{
		$('#sub-smtp-settings-auth').hide();
		}
	}

function ShowHideDeliverySettings(ObjectValue)
	{
	if (ObjectValue == 'SMTP')
		{
		$('#sub-smtp-settings').show();	
		$('#sub-octeth-smtp-settings').hide();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();	
		$('#sub-powermta-settings').hide();	
		$('#sub-savetodisk-settings').hide();	
		}
	else if (ObjectValue == 'SMTP-OCTETH')
		{
		$('#sub-smtp-settings').hide();	
		$('#sub-octeth-smtp-settings').show();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();	
		$('#sub-powermta-settings').hide();	
		$('#sub-savetodisk-settings').hide();	
		}
	else if (ObjectValue == 'SMTP-SENDGRID')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').show();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-MAILGUN')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').show();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-MAILJET')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').show();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-SES')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').show();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-POSTMARK')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').show();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'LocalMTA')
		{
		$('#sub-smtp-settings').hide();	
		$('#sub-octeth-smtp-settings').hide();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').show();
		$('#sub-php-mail-settings').hide();	
		$('#sub-powermta-settings').hide();	
		$('#sub-savetodisk-settings').hide();	
		}
	else if (ObjectValue == 'PHPMail')
		{
		$('#sub-smtp-settings').hide();	
		$('#sub-octeth-smtp-settings').hide();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').show();	
		$('#sub-powermta-settings').hide();	
		$('#sub-savetodisk-settings').hide();	
		}
	else if (ObjectValue == 'PowerMTA')
		{
		$('#sub-smtp-settings').hide();	
		$('#sub-octeth-smtp-settings').hide();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();	
		$('#sub-powermta-settings').show();	
		$('#sub-savetodisk-settings').hide();	
		}
	else if (ObjectValue == 'SaveToDisk')
		{
		$('#sub-smtp-settings').hide();	
		$('#sub-octeth-smtp-settings').hide();	
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();	
		$('#sub-powermta-settings').hide();	
		$('#sub-savetodisk-settings').show();	
		}
	}

function emailDeliveryTabCallback(tabcollection, tabid)
{
	if (tabid == 'tab-5') {
		$('.form-action-container').hide();
	} else {
		$('.form-action-container').show();
	}
}

function headerEmailTypeChange(selectedEmailType)
{
	selectedEmailType = typeof(selectedEmailType) != 'string' ? $(this).val() : selectedEmailType;
	selectedPresets = valuePresets[selectedEmailType];

	selectedValues = [];
	for (var i=0;i<selectedPresets.length;i++) {
		selectedValues.push(values[selectedPresets[i]]);
	}

	var selectElement = $('#personalization-select-HeaderValue');
	$('option', selectElement).not(':first').remove();
	$.each(selectedValues, function() {
		selectElement.append('<option value="'+this.value+'">'+this.label+'</option>');
	});
}

function addEmailHeader()
{
	$.post(CONTROLLER_URL+'snippet_addEmailHeader', {
		'HeaderName': $('#HeaderName').val(),
		'EmailType': $('#emailType').val(),
		'HeaderValue': $('#HeaderValue').val()
	}, function(data) {
		if (!data.success) {
			$('#addHeaderMessage').text(languageObject['0037']).addClass('error');
		} else {
			var newRow = $('<tr style="display:none"><td width="144"><input type="checkbox" name="SelectedEmailHeaders" value="'+data.id+'">'+data.name+'</td><td>'+languageObject[1764][data.type]+'</td><td>'+data.value+'</td></td></tr>');
			$('#last-row').before(newRow);
			newRow.fadeIn();
			$('#header-table').data_grid();
		}
	}, 'json');
}

function removeEmailHeaders()
{
	var selectedHeaderIds = [];
	$('input[name="SelectedEmailHeaders"]:checked').each(function() {
		selectedHeaderIds.push($(this).val());
		$(this).parent().parent().fadeOut(function() {
			$(this).remove();
		});
	});
	selectedHeaderIds = selectedHeaderIds.join(',');
	if (selectedHeaderIds == '') return false;

	$.post(CONTROLLER_URL+'snippet_removeEmailHeaders', {
		'HeaderIDs': selectedHeaderIds
	}, function(data) {
		if (!data.success) {
			$('#addHeaderMessage').text(languageObject['0037']).addClass('error');
		} else {
			$('#header-table td.zebra').removeClass('zebra');
			$('#header-table').data_grid();
		}
	}, 'json');
	return false;
}