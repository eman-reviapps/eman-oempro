$(document).ready(function () {
	if (tax != '0') {
		$('#tax-row').show();
		$('#tax-button').val(language['0662']);
		$('#IncludeTax').val('Include');
	} else {
		$('#tax-row').hide();
		$('#tax-button').val(language['0659']);
		$('#IncludeTax').val('Exclude');
	}
	if (discount != '0') {
		$('#discount-row').show();
		$('#discount-button').val(language['0663']);
	} else {
		$('#discount-row').hide();
		$('#discount-button').val(language['0660']);
	}
	$('#tax-button').click(function() {
		if ($('#IncludeTax').val() == 'Include') {
			$('#IncludeTax').val('Exclude');
		} else {
			$('#IncludeTax').val('Include');
		}
	});
	$('#discount-button').click(function() {
		if (discount != 0) {
			$('#Discount').val('0');
		} else {
			$('#Discount').val('1');
		}
	});
	$('#payment-request-button').click(function() {
		$('#SendReceipt').val('Yes');
		$('#InvoiceEditForm').submit();
	});
	$('#print-button').click(function() {
		window.print();
	});
});