$(document).ready(function () {
	ShowHideS3UploadSettings($('input[name="MEDIA_UPLOAD_METHOD"]:checked').val());
	$('input[name="MEDIA_UPLOAD_METHOD"]').change(function() {
		ShowHideS3UploadSettings(this.value);
	});
});


function ShowHideS3UploadSettings(ObjectValue)
	{
	if (ObjectValue == 's3')
		{
		$('#s3-upload-settings').show();	
		}
	else
		{
		$('#s3-upload-settings').hide();	
		}
	}