(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {
          
          var parents = $(this).parents().get();
         
          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }
          
          return false;
        }
    });
})(jQuery);

$(document).ready(function() {
	$('#user-login-menu').click(function() {
		$(this).addClass('open');
	});
	$('#user-login-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#user-login-menu') == false) {
			$('#user-login-menu').removeClass('open');
		}
	});
});