$(document).ready(function () {
    $('#proceed-to-builder-link').click(function () {
        $('#Command').val('ProceedToBuilder');
        $('#EmailTemplateEditForm').submit();
    });
    $('#save-link').click(function () {
        $('#Command').val('Save');
        $('#EmailTemplateEditForm').submit();
    });
});