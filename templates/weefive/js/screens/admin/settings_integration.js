$(document).ready(function () {
	ShowHidePMEConnectionDetails($('input[name="PME_USAGE_TYPE"]:checked').val());
	$('input[name="PME_USAGE_TYPE"]').change(function() {
		ShowHidePMEConnectionDetails(this.value);
	});
	$('#PME_DEFAULT_ACCOUNT').keyup(function() {
		$('#account-url-preview').text($(this).val());
	});
	$('#account-url-preview').text($('#PME_DEFAULT_ACCOUNT').val());
});


function ShowHidePMEConnectionDetails(ObjectValue)
	{
	if (ObjectValue == 'Admin')
		{
		$('#pme-integration-settings').show();	
		}
	else
		{
		$('#pme-integration-settings').hide();	
		}
	}