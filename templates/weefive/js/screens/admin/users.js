(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {
          
          var parents = $(this).parents().get();
         
          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }
          
          return false;
        }
    });
})(jQuery);


$(document).ready(function () {
	$('#users-table-form').submit(function() {
		if ($('#Command').val() == 'DeleteUser') {
			if (!confirm(language_object.screen['0058'])) {
				return false;
			}
		}
	});
	
	$('#change-status-menu').click(function() {
		$(this).addClass('open');
		return false;
	});
	$('#change-status-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#change-status-menu') == false) {
			$('#change-status-menu').removeClass('open');
		}
	});
	$('#change-status-menu li a').click(function() {
		$('#Command').val('ChangeStatus');
		$('#Status').val($(this).attr('statustype'));
		$('#users-table-form').submit();
	});
	
});