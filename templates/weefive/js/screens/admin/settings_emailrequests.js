$(document).ready(function () {
	ShowHideRequestsPOP3Settings($('#POP3_REQUESTS_STATUS > option:selected').val());
	$('select[name="POP3_REQUESTS_STATUS"]').change(function() {
		ShowHideRequestsPOP3Settings(this.value);
	});
});

function ShowHideRequestsPOP3Settings(ObjectValue)
	{
	if (ObjectValue == 'Enabled')
		{
		$('#requests-pop3-settings').show();
		}
	else
		{
		$('#requests-pop3-settings').hide();
		}
	}
