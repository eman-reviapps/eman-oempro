$(document).ready(function () {
	ShowHideUserSignUpSettings($('input[name="USER_SIGNUP_ENABLED"]:checked').val());
	$('input[name="USER_SIGNUP_ENABLED"]').change(function() {
		ShowHideUserSignUpSettings(this.value);
	});

	ShowHideUserSignUpGroups($('#USER_SIGNUP_GROUPID > option:selected').val());
	$('select[name="USER_SIGNUP_GROUPID"]').change(function() {
		ShowHideUserSignUpGroups(this.value);
	});

	ShowHidePayPalExpressCheckoutPaymentGateway($('input[name="PAYPALEXPRESSSTATUS"]:checked').val());
	$('input[name="PAYPALEXPRESSSTATUS"]').change(function() {
		ShowHidePayPalExpressCheckoutPaymentGateway();
	});

	ShowHideThirdPartyPaymentGateway($('input[name="PAYMENT_CREDITS_GATEWAY_URL_STATUS"]:checked').val());
	$('input[name="PAYMENT_CREDITS_GATEWAY_URL_STATUS"]').change(function() {
		ShowHideThirdPartyPaymentGateway();
	});
});


function ShowHideUserSignUpSettings(ObjectValue)
	{
	if (ObjectValue == 'true')
		{
		$('#user-signup-settings').show();	
		}
	else
		{
		$('#user-signup-settings').hide();	
		}
	}

function ShowHideUserSignUpGroups(ObjectValue)
	{
	if (ObjectValue == '-1')
		{
		$('#form-row-USER_SIGNUP_GROUPIDS').show();	
		}
	else
		{
		$('#form-row-USER_SIGNUP_GROUPIDS').hide();	
		}
	}

function ShowHidePayPalExpressCheckoutPaymentGateway()
	{
	if ($('input[name="PAYPALEXPRESSSTATUS"]:checked').length > 0)
		{
		$('#paypal-settings').show();
		}
	else
		{
		$('#paypal-settings').hide();
		}
	}
	
function ShowHideThirdPartyPaymentGateway()
	{
	if ($('input[name="PAYMENT_CREDITS_GATEWAY_URL_STATUS"]:checked').length > 0)
		{
		$('#third-party-payment-settings').show();
		}
	else
		{
		$('#third-party-payment-settings').hide();
		}
	}