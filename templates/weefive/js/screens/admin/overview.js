var active_chart = 'forecast';
var active_date_range = '30';
var flash_chart_element = '';
var active_tab_id = 'tab-report-forecast';

function amChartInited(chart_id)
{
	// flash_chart_element = document.getElementById('history-chart');
}

function setChartOptions(chartel, option, value) {
	chartel.attr('data-oempro-chart-options-' + option, value);
}

function initChart(chartel) {
	chartel.data('init')();
}

function switch_between_forecast_and_history(tabcollection, tabid)
{
	active_tab_id = tabid;
	if (tabid != 'tab-report-history') {
		$('#history-date-ranges').hide();
	}
	else {
		$('#history-date-ranges').show();
		setTimeout(function() {
			initChart($('#history-chart-container'));
		}, 500);		
	}
}

function switch_chart_data_range(tabcollection, tabid)
{
	if (tabid == 'tab-1m') {
		active_date_range = '30';
	}
	else if (tabid == 'tab-3m') {
		active_date_range = '90';
	}
	else if (tabid == 'tab-6m') {
		active_date_range = '180';
	}
	setChartOptions($('#history-chart-container'), 'data', APP_URL + "/admin/overview/chartdata/DeliveryHistory/" + active_date_range);
	initChart($('#history-chart-container'));

}

$(document).ready(function()
{
	$('.approve-campaign').click(function()
	{
		var campaignId = $(this).attr('data-campaign-id');
		if (confirm(Language['1808'])) {
			$('tr[data-campaign-id="' + campaignId + '"]').fadeOut('slow', function()
			{
				$(this).remove();
				if ($('tr.pending-campaign-row').length < 1) {
					$('#no-campaign-pending-message').show();
				}
			});
			$.post(API_URL, {
				responseformat: 'JSON',
				command: 'Campaign.Approve',
				campaignid: campaignId
			});

		}
		return false;
	});
});