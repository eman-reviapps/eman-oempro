var color_picker = null;
var current_color_input = null;

function ShowHideSMTPAuth(ObjectValue)
	{
	if (ObjectValue == 'true')
		{
		$('#sub-smtp-settings-auth').show();
		}
	else
		{
		$('#sub-smtp-settings-auth').hide();
		}
	}

function ShowHideDeliverySettings(ObjectValue)
	{
	if (ObjectValue == 'System')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP')
		{
		$('#sub-smtp-settings').show();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-OCTETH')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').show();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-SENDGRID')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').show();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-MAILGUN')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').show();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-MAILJET')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').show();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-SES')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').show();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SMTP-POSTMARK')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').show();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'LocalMTA')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').show();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'PHPMail')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').show();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'PowerMTA')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').show();
		$('#sub-savetodisk-settings').hide();
		}
	else if (ObjectValue == 'SaveToDisk')
		{
		$('#sub-smtp-settings').hide();
		$('#sub-octeth-smtp-settings').hide();
		$('#sub-sendgrid-settings').hide();
		$('#sub-mailgun-settings').hide();
		$('#sub-mailjet-settings').hide();
		$('#sub-ses-settings').hide();
		$('#sub-postmark-settings').hide();
		$('#sub-local-mta-settings').hide();
		$('#sub-php-mail-settings').hide();
		$('#sub-powermta-settings').hide();
		$('#sub-savetodisk-settings').show();
		}
	}
	
function setup_permissions(permissions) {
	$('#tab-content-4 input[type="checkbox"]').attr('checked', false);
	
	var array_permissions = permissions.split(',');
	var array_permissions_length = array_permissions.length;

	for (var i = 0; i < array_permissions_length; i++) {
		if ($('input[id="Permission-'+array_permissions[i]+'"]').length > 0) {
			$('input[id="Permission-'+array_permissions[i]+'"]').get(0).checked = true;
		}
	}
}

function show_theme_css_settings(template) {
	$('.theme-css-settings-containers').hide();
	$('#css-settings-'+template).show();
}

function show_trial_options(option) {
	if (option == 'Yes')
		{
		$('#form-row-TrialExpireSeconds').show();
		}
	else
		{
		$('#form-row-TrialExpireSeconds').hide();
		}
}

function add_pricing_range(event, emails_value, cost_value) {
	
	var first_row_template = pricing_range_first_row_template;
	var row_template = pricing_range_row_template;

	if ($('#CreditSystem').val() == 'Enabled') {
		first_row_template = pricing_credit_range_first_row_template;
		row_template = pricing_credit_range_row_template;
	}
	if (pricing_range_count < 1) {
		$('#add-range-button-row').before($(first_row_template.replace(/__/g, pricing_range_last_id)));
	} else {
		$('#add-range-button-row').before($(row_template.replace(/__/g, pricing_range_last_id)));
	}
	if (emails_value == undefined) {
		emails_value = '';
		cost_value = '';
	}
	$('#pricing-range-'+pricing_range_last_id+'-emails').val(emails_value);
	$('#pricing-range-'+pricing_range_last_id+'-cost').val(cost_value);
	pricing_range_count++;
	pricing_range_last_id++;
}

function remove_pricing_range() {
	$('#'+$(this).attr('id').replace('pricing-range-remove-button-', 'pricing-range-row-')).remove();
	pricing_range_count--;
}

function setup_pricing_range(remove_pricing_range_rows) {
	$('#PaymentPricingRange').val('');
	var tmpArray = [];
	$('.pricing-range-row').each(function () {
		var id = $(this).attr('id').replace('pricing-range-row-','');
		var emails = $('#pricing-range-'+id+'-emails').val();
		var cost = $('#pricing-range-'+id+'-cost').val();
		if (emails != '' || cost != '') {
			tmpArray.push(emails + '|' + cost);
		}
	});
	if (tmpArray.length > 0) {
		$('#PaymentPricingRange').val($('#PaymentPricingRange').val()+tmpArray.join("\n"));
	}
	if (remove_pricing_range_rows) {
		$('.pricing-range-row').remove();
		pricing_range_count = 0;
	}
}

function display_pricing_ranges() {
	var pricing_ranges = $('#PaymentPricingRange').val().split("\n");
	$.each(pricing_ranges, function() {
		var array = this.split('|');
		add_pricing_range(null, array[0],array[1]);
	});
}

function before_form_submit() {
	setup_pricing_range();
}

function setup_credit_system(setup_pricing_ranges) {
	if ($('#CreditSystem').val() == 'Enabled') {
		$('#form-row-PaymentAutoRespondersPerRecipient').hide();
		$('#form-row-PaymentCampaignsPerRecipient').hide();
		$('#PaymentAutoRespondersPerRecipient option[value="Disabled"]').get(0).selected = true;
		$('#PaymentCampaignsPerRecipient option[value="Disabled"]').get(0).selected = true;
	} else {
		$('#form-row-PaymentAutoRespondersPerRecipient').show();
		$('#form-row-PaymentCampaignsPerRecipient').show();
	}
	toggle_pricing_range();
	if (setup_pricing_ranges) {
		setup_pricing_range(true);
		display_pricing_ranges();
	}
}

function toggle_pricing_range() {
	var per_recipient_fee_for_auto_respoders = $('#PaymentAutoRespondersPerRecipient').val();
	var per_recipient_fee_for_campaigns = $('#PaymentCampaignsPerRecipient').val();
	var credit_system = $('#CreditSystem').val();

	if (per_recipient_fee_for_auto_respoders == 'Enabled' || per_recipient_fee_for_campaigns == 'Enabled' || credit_system == 'Enabled')
		{
		$('#form-row-PricingRange').show();
		}
	else
		{
		$('#form-row-PricingRange').hide();
		}
}

$(document).ready(function () {

	ShowHideDeliverySettings($('#SendMethod option:selected').val());
	$('select[name="SendMethod"]').change(function() {
		ShowHideDeliverySettings(this.value);
	});

	ShowHideSMTPAuth($('#SendMethodSMTPAuth > option:selected').val());
	$('select[name="SendMethodSMTPAuth"]').change(function() {
		ShowHideSMTPAuth(this.value);
	});

	if (permissions != null && permissions != '') {
		setup_permissions(permissions);
	}
	
	$('#permission-preset-select').change(function() {
		setup_permissions(this.value);
	});

	var payment_system_select_jq_element = $('#PaymentSystem');
	payment_system_select_jq_element.change(function() {
		if (this.options[this.options.selectedIndex].value == 'Enabled') {
			$('#payment-system-settings').show();
		} else {
			$('#payment-system-settings').hide();
		}
	});

	if (payment_system_select_jq_element.get(0).options[payment_system_select_jq_element.get(0).options.selectedIndex].value == 'Enabled') {
		$('#payment-system-settings').show();
	} else {
		$('#payment-system-settings').hide();
	}
	
	$('#add-range-button').click(add_pricing_range);
	
	$('.pricing-range-remove-buttons').live('click', remove_pricing_range);
	
	$('#template-select').change(function() {
		show_theme_css_settings(this.value);
	});

	$('#TrialGroup').change(function() {
		show_trial_options(this.value);
	});

	show_trial_options($('#TrialGroup').val());
	show_theme_css_settings($('#template-select').val());
	
	$('#tab-content-5 input.color-input').click(function(event) {
		var input = $(event.target);
		
		$(input).after($('#colorpicker'));
		
		var position = $(input).position();
		var width = $(input).width();
		// $('#colorpicker').css({
		// 	top:event.pageY+'px',
		// 	left:position.left+width+'px'
		// });
		$('#colorpicker').show();
	});
	
	$('#tab-content-5 input.color-input').focus(function() {
		current_color_input = $(this);
		color_picker.setColor('#'+current_color_input.val());
	});
	
	$('#tab-content-5 input.color-input').blur(function() {
		// $('#colorpicker').hide();
	});

	color_picker = $.farbtastic('#colorpicker', function() {
		if (current_color_input != null) {
			current_color_input.val(this.color.replace('#', ''));
		}
	});

	if ($('#UserGroupCreateForm').length > 0) {
		$('#UserGroupCreateForm').submit(before_form_submit);
	}
	if ($('#UserGroupEditForm').length > 0) {
		$('#UserGroupEditForm').submit(before_form_submit);
	}
	
	if ($('#PaymentPricingRange').val() != '') {
		display_pricing_ranges();
	}

	$('#PaymentAutoRespondersPerRecipient').change(toggle_pricing_range);
	$('#PaymentCampaignsPerRecipient').change(toggle_pricing_range);
	
	toggle_pricing_range();
		
	$('#CreditSystem').change(function() { setup_credit_system(true); });
	setup_credit_system();

});