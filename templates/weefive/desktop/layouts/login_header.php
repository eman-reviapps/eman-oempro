<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>" />
        <title><?php print($PageTitle); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

<!--<title>Register</title>-->

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/select2/css/select2.min.css' ?>" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css' ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/login.css" rel="stylesheet" type="text/css" />
       
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico" />

        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>

        <script type="text/javascript"> //<![CDATA[ 
            var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
            document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
            //]]>
        </script>
    </head>

    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php InterfaceAppURL(false); ?>/user/">
                <img style="width: 190px" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/logo-flying.png" alt="" />
            </a>
            <span class="">
                <script language="JavaScript" type="text/javascript" >
                    TrustLogo("https://flyinglist.com/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                </script>
                <!--<a style="margin-left: 150px;margin-top: -25px;margin-bottom: 10px;" href="https://ssl.comodo.com" id="comodoTL">SSL Certificates</a>-->
            </span>
        </div>
