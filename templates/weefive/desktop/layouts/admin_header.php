<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

	<title><?php print($PageTitle); ?></title>

	<!-- BLUEPRINT CSS FRAMEWORK - START -->
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">
	<!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	<!-- BLUEPRINT CSS FRAMEWORK - END -->
	
	<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection">
	<link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">

	<script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/charts.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
	<script>
		var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
		var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
		var APP_URL = '<?php InterfaceAppURL(); ?>/';
		$(document).ready(function() {
			$.search_widget('<?php InterfaceAppURL(); ?>/admin/search/');
		});
	</script>
</head>

<body>
	<div id="browser-upgrade">
		<p><?php InterfaceLanguage('Screen', '0624', false, '', false, false); ?></p>
	</div>
	<div id="shortcut-window">
		<div class="inner">
			<h2><?php InterfaceLanguage('Screen', '0614', false, '', false, true); ?></h2>
			<hr />
			<ul>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + 1 :</span><?php InterfaceLanguage('Screen', '0615', false, '', false, false); ?></li>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + 2 :</span><?php InterfaceLanguage('Screen', '0616', false, '', false, false); ?></li>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + 3 :</span><?php InterfaceLanguage('Screen', '0617', false, '', false, false); ?></li>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + 4 :</span><?php InterfaceLanguage('Screen', '0618', false, '', false, false); ?></li>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + 5 :</span><?php InterfaceLanguage('Screen', '0619', false, '', false, false); ?></li>
			</ul>
			<ul>
				<li><span>&lt;Shift&gt; + &lt;Alt&gt; + s :</span><?php InterfaceLanguage('Screen', '0620', false, '', false, false); ?></li>
			</ul>
			<ul>
				<li><span>k / j <?php InterfaceLanguage('Screen', '0625', false, '', false, false); ?> <?php InterfaceLanguage('Screen', '0626', false, '', false, false); ?> :</span><?php InterfaceLanguage('Screen', '0627', false, '', false, false); ?></li>
			</ul>
			<ul>
				<li class="note"><?php InterfaceLanguage('Screen', '0621', false, '', false, false); ?></li>
			</ul>
		</div>
	</div>
	<div id="top">
		<div class="container">
			<div class="span-5">
				<h1><?php print(PRODUCT_NAME); ?></h1>
			</div>
			<div class="span-18 last">
				<ul class="tabs">
					<li <?php if ($CurrentMenuItem == 'Overview'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/admin/overview/"><?php if ($CurrentMenuItem == 'Overview'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><?php endif; ?><strong><?php InterfaceLanguage('Screen', '0735'); ?></strong></a></li>
					<li <?php if ($CurrentMenuItem == 'Users'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/admin/users/browse/"><?php if ($CurrentMenuItem == 'Users'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><?php endif; ?><strong><?php InterfaceLanguage('Screen', '9075'); ?></strong></a></li>
					<li <?php if ($CurrentMenuItem == 'Suppression'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/admin/suppression/"><?php if ($CurrentMenuItem == 'Suppression'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><?php endif; ?><strong><?php InterfaceLanguage('Screen', '1885'); ?></strong></a></li>
					<?php
					InterfacePluginMenuHook('Admin.TopMenu', 
											$CurrentMenuItem, 
											'<li id="plugin-menu-_ID_" class="top-menu-plugin"><a href="_LINK_"><strong>_TITLE_</strong></a></li>', 
											'<li id="plugin-menu-_ID_" class="top-menu-plugin selected"><a href="_LINK_"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>_TITLE_</strong></a></li>'
											); 
					?>
					<?php if ($CurrentMenuItem == 'Drop'): ?>
					<li class="selected"><a href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo $CurrentDropMenuItem; ?></strong></a></li>
					<?php endif; ?>
					<li id="header-drop" class="drop">
						<a href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>&nbsp</strong><div class="down-arrow"><span></span></div></a>
						<div class="menu">
							<ul>
								<li><a href="<?php InterfaceAppURL(false); ?>/admin/paymentreports/"><?php InterfaceLanguage('Screen', '9079'); ?></a></li>
								<li><a href="<?php InterfaceAppURL(); ?>/admin/plugins/"><?php InterfaceLanguage('Screen', '0131', false, '', false); ?></a></li>
								<?php
								InterfacePluginMenuHook('Admin.TopDropMenu',
														array($CurrentMenuItem, $CurrentDropMenuItem),
														'<li id="plugin-drop-menu-_ID_" class="top-drop-menu-plugin"><a href="_LINK_">_TITLE_</a></li>',
														''
														);
								?>
							</ul>
						</div>
					</li>

				</ul>


				<ul class="tabs right">
					<?php 
					InterfacePluginMenuHook('Admin.TopRightMenu', 
											$CurrentMenuItem, 
											'<li><a href="_LINK_" class="transparent">_TITLE_</a></li>', 
											'<li class="selected"><a href="_LINK_" class="transparent"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>_TITLE_</strong></a></li>'
											); 
					?>
					<li <?php if ($CurrentMenuItem == 'Settings'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/admin/account/" class="transparent"><?php if ($CurrentMenuItem == 'Settings'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php endif; ?><?php InterfaceLanguage('Screen', '0110', false, '', false, true); ?><?php if ($CurrentMenuItem == 'Settings'): ?></strong><?php endif; ?></a></li>
					<li><a href="<?php InterfaceAppURL(false); ?>/admin/logout/" class="transparent"><?php InterfaceLanguage('Screen', '0619', false, '', false); ?></a></li>
					<?php
					// echo InterfaceDisplayMenu('AdminTopRightMain', $CurrentMenuItem, '<li><a href="_Replace:Link_" class="transparent">_Replace:Name_</a></li>', '<li class="selected"><a href="_Replace:Link_" class="transparent"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>_Replace:Name_</strong></a></li>');
					?>
					<li id="search">
						<a href="#" id="search-link"><img src="<?php InterfaceTemplateURL(); ?>/images/icon_search.gif" /></a>
						<div id="search-step-1">
							<span class="left">&nbsp;</span><span class="right">&nbsp;</span>
							<p><?php InterfaceLanguage('Screen', '0728', false, '', false, true); ?><input type="text" name="search-keyword-input" value="" id="search-keyword-input" class="text"><img id="search-loading-indicator" src="<?php InterfaceTemplateURL(); ?>/images/icon_search.gif" width="16" height="16" /></p>
						</div>
						<div id="search-step-2"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div id="middle">
