<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>" />
        <title><?php print($PageTitle); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />


        <link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection" />
        <link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/iguana.css" type="text/css" media="screen, projection" />
        <link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico" />

        <script>
            var APP_URL = '<?php InterfaceAppURL(); ?>';
            var BUILDER_IFRAME_SOURCE = APP_URL + '/user/emailtemplates/builder/displayHTMLContent';
            var BUILDER_CSS_URL = '<?php InterfaceTemplateURL(); ?>styles/iguana_builder_frame.css';
            var Language = {
                '0001': '<?php InterfaceLanguage('Screen', '0300', false); ?>',
                '0002': '<?php InterfaceLanguage('Screen', '0301', false); ?>',
                '0003': '<?php InterfaceLanguage('Screen', '0302', false); ?>',
                '0004': '<?php InterfaceLanguage('Screen', '0303', false); ?>',
                '0005': '<?php InterfaceLanguage('Screen', '0706', false); ?>',
                '0006': '<?php InterfaceLanguage('Screen', '0709', false); ?>'
            };
        </script>
        <script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/iguana_builder.js" type="text/javascript" charset="utf-8"></script>

    </head>

    <body>