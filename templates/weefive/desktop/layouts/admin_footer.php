	</div>

	<div id="bottom">

		<?php 
		InterfaceAdminFooter(false, '');
		?>
	</div>

	<?php if (RUN_CRON_IN_USER_AREA == 'true'): ?>
		<img id="cron-image" src="<?php print(InterfaceInstallationURL(true).'cron.php'); ?>" width="1" height="1" alt="Cron Executer">
	<?php endif; ?>

	<?php if (isset($AdminInformation) == true && isset($AdminInformation['AdminID']) == true && (defined('OCTETH_TRACKERS') == true && OCTETH_TRACKERS == true)): ?>
		<script id="IntercomSettingsScriptTag">
			window.intercomSettings = {
				"user_id": "<?php print(md5($LicenseKey)); ?>",
				"user_hash": "<?php print(hash_hmac("sha256", md5($LicenseKey), "PHVrXl_4euSkPwWrUnvaB_3EEbQzaOkDKC2P7Pfw")); ?>",
				"email": "<?php print($AdminInformation['EmailAddress']); ?>",
				"created_at": <?php print(strtotime('0000-00-00 00:00:00')); ?>,
				"app_id": "thfbhnpm",
				"name": "<?php print($AdminInformation['Name']); ?>",
				"license": "<?php print($LicenseKey); ?>",
				"current_url": "http://<?php print($_SERVER['HTTP_HOST']); ?><?php print($_SERVER['REQUEST_URI']); ?>",
				"version": "<?php print(PRODUCT_VERSION); ?>",
				"max_users": <?php print(OEMPRO_LICENSE_MAX_USERS); ?>,
				"existing_users": <?php print(OEMPRO_EXISTING_USERS); ?>,
				"plugins": "<?php print(ENABLED_PLUGINS); ?>"
			};
		</script>
		<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}};})()</script>

		<!-- Sendloop Engage - Start -->
		<script type="text/javascript" charset="utf-8" id="js-sendloop-engage">
			window.sendloop_engage_settings = {
				sl_user_id: "Bcs-78/ad564198",
				user_id: "<?php print(md5($LicenseKey)); ?>",
				secured_user_id: "<?php print(hash_hmac('sha256', md5($LicenseKey), "59f032a33e7d46e6bf527be642512ea2")); ?>",
				"email": "<?php print($AdminInformation['EmailAddress']); ?>",
				extra_info: {
					"name": "<?php print($AdminInformation['Name']); ?>",
					"license": "<?php print($LicenseKey); ?>",
					"current_url": "http://<?php print($_SERVER['HTTP_HOST']); ?><?php print($_SERVER['REQUEST_URI']); ?>",
					"version": "<?php print(PRODUCT_VERSION); ?>",
					"max_users": <?php print(OEMPRO_LICENSE_MAX_USERS); ?>,
					"existing_users": <?php print(OEMPRO_EXISTING_USERS); ?>,
					"plugins": "<?php print(ENABLED_PLUGINS); ?>"
				}
			};
		</script>
		<script type="text/javascript" charset="utf-8">(function () {var w = window; var d = document; function l() {var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'sendloop.com/media/engage/engage.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);}if (w.attachEvent) {w.attachEvent('onload', l);} else {w.addEventListener('load', l, false);}})();</script>
		<!-- Sendloop Engage - End -->

	<?php endif; ?>
</body>
</html>
