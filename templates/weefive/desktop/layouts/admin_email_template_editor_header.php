<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

	<title><?php print($PageTitle); ?></title>

	<!-- BLUEPRINT CSS FRAMEWORK - START -->
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">
	<!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	<!-- BLUEPRINT CSS FRAMEWORK - END -->
	
	<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/iguana.css" type="text/css" media="screen, projection">
	<link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">

	<script>
		var APP_URL = '<?php InterfaceAppURL(); ?>';
		var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
		var IGUANA_CSS_URL = '<?php InterfaceTemplateURL(); ?>styles/iguana_frame.css';
		var EMAIL_CONTENT_URL = APP_URL+'/admin/emailtemplates/testdrive/displayHTMLContent';
		var Language = {
			'0001'	: '<?php InterfaceLanguage('Screen', '0301', false); ?>',
			'0002'	: '<?php InterfaceLanguage('Screen', '0303', false); ?>',
			'0003'	: '<?php InterfaceLanguage('Screen', '0504', false); ?>',
			'0004'	: '<?php InterfaceLanguage('Screen', '0505', false); ?>',
			'0005'	: '<?php InterfaceLanguage('Screen', '0506', false); ?>',
			'0006'	: '<?php InterfaceLanguage('Screen', '0507', false); ?>',
			'0007'	: '<?php InterfaceLanguage('Screen', '0508', false); ?>'
		};
	</script>
	<script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce_3432/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/iguana.js" type="text/javascript" charset="utf-8"></script>
	
	<script type="text/javascript" charset="utf-8">
	var	tinymce_config = {
		// Location of tinymce script
		script_url		: '<?php print(InterfaceTemplateURL()); ?>js/tiny_mce_3432/tiny_mce.js',
		
		// General options
		theme						: 'advanced',
		plugins						: 'fullpage,safari,table,style,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,personalizer',
		auto_reset_designmode		: true,
		browsers					: 'msie,gecko,safari,opera',
		dialog_type					: 'modal',
		directionality				: "ltr",
		docs_language				: "en",
		language					: "en",
		nowrap						: false,
		apply_source_formatting		: true,
		entity_encoding				: 'named',
		cleanup						: true,
		cleanup_on_startup			: true,
		verify_html					: false,
		forced_root_block			: '',
		convert_newlines_to_brs		: false,
		element_format				: 'xhtml',
		fix_list_elements			: true,
		fix_table_elements			: true,
		fix_nesting					: true,
		force_p_newlines			: true,
		force_br_newlines			: false,
		force_hex_style_colors		: true,
		preformatted				: false,
		valid_child_elements		: 'table[tr|td]',
		paste_auto_cleanup_on_paste	: true,
		relative_urls				: false, 
		convert_urls				: false,

		// Theme options
		theme_advanced_buttons1				: "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor",
		theme_advanced_buttons2				: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,cleanup,fullscreen,|,code",
		theme_advanced_buttons3				: "personalizer,|,table,row_after,row_before,col_after,col_before,delete_col,delete_row,delete_table,|,link,unlink,image",
		theme_advanced_toolbar_location 	: "top",
		theme_advanced_toolbar_align		: "left",
		theme_advanced_statusbar_location	: "bottom",
		theme_advanced_resizing				: true,
		
		// Personalizer tags
		personalizer_tags					: [
		<?php foreach($ContentTags as $Label => $TagGroup): ?>
			{ label : true, title : '<?php print($Label); ?>' },
			<?php foreach($TagGroup as $Tag => $Label): ?>
				{ title : '<?php print($Label); ?>', tags : '<?php print($Tag); ?>' },
			<?php endforeach; ?>
		<?php endforeach; ?>
		],
		
		setup : function(ed) {
			ed.onKeyUp.add(function(ed, e) {
				iguana.update_element_content(ed.getContent());
			});
			ed.onNodeChange.add(function(ed, cm, e) {
				iguana.update_element_content(ed.getContent());
			});
		}
	};
	</script>
</head>
<body>