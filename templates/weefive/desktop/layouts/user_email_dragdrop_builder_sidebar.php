<!-- Sidebar -->
<div id="sidebar-wrapper">
    <br>
    <ul class="sidebar-nav" id="sidebar">
        <li>
        <a href="javascript:void(0);" class="em_menu" data-group="personalize"><i
                style="" class="fa fa-link fa-2x"></i></a></li>

        <li><a href="javascript:void(0);" class="em_menu" data-group="title"><i
                    style="" class="fa fa-header fa-2x"></i></a></li>
        <li><a href="javascript:void(0);" class="em_menu" data-group="text"><i
                    style="" class="fa fa-font fa-2x"></i></a></li>
        <li><a href="javascript:void(0);" class="em_menu" data-group="tools"> <i
                    class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0);" class="em_menu" data-group="image"><i
                    class="fa fa-file-image-o fa-2x"></i></a></li>
        <li><a href="javascript:void(0);" class="em_menu"  data-group="video"><i
                    class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0);" class="em_menu"  data-group="social"><i class="fa fa-facebook fa-2x"  aria-hidden="true"></i></a></li>
        <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="#credits"><i class="fa fa-life-ring fa-2x" aria-hidden="true"></i></a></li>-->

    </ul>
</div>

<!-- sidebar with elements -->
<div id="sidebar-opzioni" class="sidebar-nav">
    <a href="javascript:void(0);" id="closeSideBarnav"> <i class="pull-right fa fa-times" aria-hidden="true"   style="padding: 10px"></i></a> <br>

    <div id="em_elements">
        <ul class="nav nav-list accordion-group">
            <li class="rows" id="estRows">
                <!-- load elements -->
            </li>
        </ul>
    </div>
</div>
<!-- sidebar with elements -->