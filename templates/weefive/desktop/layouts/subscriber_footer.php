
</div>
<div id="bottom">
    <?php
    $FooterTemplate = '
		<div class="container">
			<div class="span-23 last">
				<div class="custom-column-container cols-2 clearfix" style="margin-top:18px;">
					<div class="col">
						<p>
						</p>
					</div>
					<div class="col">
						<p class="dimmed">
						</p>
					</div>
				</div>
			</div>
		</div>';
    InterfaceUserFooter(false, $FooterTemplate);
    ?>

</div>
<!-- Email Automation Tracker - Start -->
<script type = "text/javascript" charset = "utf-8" id = "js-email-automation" >
    window.email_automation_settings = {
        automation_user_id: "C11-1C/1bb5089c",
        automation_url: "http://flyinglist.com/oempa/app/octautomation/track",
        user_id: '<?php echo $UserInformation['Username'] ?>',
        secured_user_id: '<?php echo hash_hmac('sha256', $UserInformation['Username'], "69db646cf8482fc9b2a8149ea9e3a680"); ?>',
        email: '<?php echo $UserInformation['EmailAddress'] ?>',
        extra_info: {
        }
    };
</script>
<script type="text/javascript" charset="utf-8">(function () {
        var w = window;
        var d = document;
        function l() {
            var s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'flyinglist.com/oempa/plugins/octautomation/js/tracker.js';
            var x = d.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (w.attachEvent) {
            w.attachEvent('onload', l);
        } else {
            w.addEventListener('load', l, false);
        }
    })();</script>
<!-- Email Automation Tracker - End -->
</body>
</html>
