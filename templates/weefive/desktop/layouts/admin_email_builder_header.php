<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

	<title><?php print($PageTitle); ?></title>

	<!-- BLUEPRINT CSS FRAMEWORK - START -->
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">
	<!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	<!-- BLUEPRINT CSS FRAMEWORK - END -->
	
	<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/iguana.css" type="text/css" media="screen, projection">
	<link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">

	<script>
		var APP_URL = '<?php InterfaceAppURL(); ?>';
		var BUILDER_IFRAME_SOURCE = APP_URL+'/admin/emailtemplates/builder/displayHTMLContent';
		var BUILDER_CSS_URL = '<?php InterfaceTemplateURL(); ?>styles/iguana_builder_frame.css';
		var Language = {
			'0001'	: '<?php InterfaceLanguage('Screen', '0300', false); ?>',
			'0002'	: '<?php InterfaceLanguage('Screen', '0301', false); ?>',
			'0003'	: '<?php InterfaceLanguage('Screen', '0302', false); ?>',
			'0004'	: '<?php InterfaceLanguage('Screen', '0303', false); ?>',
			'0005'	: '<?php InterfaceLanguage('Screen', '0706', false); ?>',
			'0006'	: '<?php InterfaceLanguage('Screen', '0709', false); ?>'
		};
	</script>
	<script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/iguana_builder.js" type="text/javascript" charset="utf-8"></script>
	
</head>

<body>		