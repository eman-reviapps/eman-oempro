<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

	<title><?php echo (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.$PageTitle; ?></title>

	<!-- BLUEPRINT CSS FRAMEWORK - START -->
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">
	<!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	<!-- BLUEPRINT CSS FRAMEWORK - END -->
	
	<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/user/css" type="text/css" media="screen, projection">
	<link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">
	
	<script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/user.js" type="text/javascript" charset="utf-8"></script>
	<script>
		var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
		var APP_URL = '<?php InterfaceAppURL(); ?>';
		var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
	</script>
</head>

<body>
	
	<div id="browser-upgrade">
		<p><?php InterfaceLanguage('Screen', '0624', false, '', false, false); ?></p>
	</div>

	<div id="top">
		<div class="container">
			<div class="span-5">
				<h1><?php echo empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']; ?></h1>
			</div>
			<div class="span-18 last" style="position:relative;">
				<div id="user-info-container">
					<div class="user-info" id="user-info" style="text-decoration:none;background-color:transparent;right:10px;cursor:default;padding-right:18px;">
						<?php InterfaceLanguage('Screen', '1435'); ?> <?php echo $ClientInformation['ClientName']; ?>!
					</div>
				</div>
				<ul class="tabs">
					<li <?php if ($CurrentMenuItem == 'Campaigns'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/client/campaigns/browse/"><?php if ($CurrentMenuItem == 'Campaigns'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><?php endif; ?><strong><?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?></strong></a></li>
					<li <?php if ($CurrentMenuItem == 'Lists'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/client/lists/browse/"><?php if ($CurrentMenuItem == 'Lists'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><?php endif; ?><strong><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?></strong></a></li>
				</ul>
				<ul class="tabs right">
					<li <?php if ($CurrentMenuItem == 'Account'): ?>class="selected"<?php endif; ?>><a href="<?php InterfaceAppURL(false); ?>/client/account/" class="transparent"><?php if ($CurrentMenuItem == 'Account'): ?><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php endif; ?><?php InterfaceLanguage('Screen', '0034', false, '', false, true); ?><?php if ($CurrentMenuItem == 'Account'): ?></strong><?php endif; ?></a></li>
					<li><a href="<?php InterfaceAppURL(false); ?>/client/logout/" class="transparent"><?php InterfaceLanguage('Screen', '0619', false, '', false); ?></a></li>
				</ul>
			</div>
		</div>
	</div>

	<div id="middle">
