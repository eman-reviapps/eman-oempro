<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

	<title><?php print($PageTitle); ?></title>

	<!-- BLUEPRINT CSS FRAMEWORK - START -->
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection, print">
	<!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection, print"><![endif]-->
	<!-- BLUEPRINT CSS FRAMEWORK - END -->
	
	<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection, print">
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/invoice.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/invoice_print.css" type="text/css" media="print">
	<link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">

	<script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
	
</head>

<body>