<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>" />
        <title><?php echo (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']) . ' ' . $PageTitle; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BLUEPRINT CSS FRAMEWORK - START -->
        <!--<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection" />-->
        <!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
        <!-- BLUEPRINT CSS FRAMEWORK - END -->

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/user/css" type="text/css" media="screen, projection" />
        <link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>favicon.ico" type="image/x-icon"/>
        <link rel="icon" href="<?php InterfaceInstallationURL(); ?>favicon.ico" type="image/x-icon"/>



        <script>
            var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
            var APP_URL = '<?php InterfaceAppURL(); ?>/';
            var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
            var LoadingText = '<?php InterfaceLanguage('Screen', '1429'); ?>';
//            $(document).ready(function () {
//                $.search_widget('<?php InterfaceAppURL(); ?>/user/search/');
//            });
        </script>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/respond.min.js"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/charts.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php InterfaceTemplateURL(); ?>js/user.js" type="text/javascript" charset="utf-8"></script>

        <?php if ($UserInformation['GroupInformation']['PaymentSystem'] == 'Enabled' && $UserInformation['GroupInformation']['CreditSystem'] == 'Enabled'): ?>
            <script src="<?php InterfaceTemplateURL(); ?>js/screens/user/credit.js" type="text/javascript" charset="utf-8"></script>
        <?php endif; ?>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.stack.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.crosshair.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/flot/jquery.flot.axislabels.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="https://code.jquery.com/jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript"> //<![CDATA[ 
            var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
            document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
            //]]>
        </script>
    </head>

    <body class="page-container-bg-solid">

        <div id="browser-upgrade">
            <p><?php InterfaceLanguage('Screen', '0624', false, '', false, false); ?></p>
        </div>
        <div id="shortcut-window">
            <div class="inner">
                <h2><?php InterfaceLanguage('Screen', '0614', false, '', false, true); ?></h2>
                <hr />
                <ul>
                    <li><span>&lt;Shift&gt; + &lt;Alt&gt; + s :</span><?php InterfaceLanguage('Screen', '0620', false, '', false, false); ?></li>
                </ul>
                <ul>
                    <li><span>k / j <?php InterfaceLanguage('Screen', '0625', false, '', false, false); ?> <?php InterfaceLanguage('Screen', '0626', false, '', false, false); ?> :</span><?php InterfaceLanguage('Screen', '0627', false, '', false, false); ?></li>
                </ul>
                <ul>
                    <li class="note"><?php InterfaceLanguage('Screen', '0621', false, '', false, false); ?></li>
                </ul>
            </div>
        </div>
        <?php
        $ArrayReturn = InterfaceAdministratorControlBar();
        if ($ArrayReturn[0] == true) :
            ?>
            <div id="administrator-control-bar">
                <div class="container">
                    <div class="col-md-5">
                        <h3><?php InterfaceLanguage('Screen', '0732', false, '', false, true); ?></h3>
                    </div>
                    <div class="col-md-7">
                        <form class="form-horizontal" role="form">
                            <div style="float: right;">                            
                                <?php InterfaceLanguage('Screen', '0733', false, '', false, true); ?>                               
                                <select id="administrator-user-switch-select" class="">
                                    <?php foreach ($ArrayReturn[1] as $EachUser): ?>
                                        <option value="<?php print($EachUser['UserID']); ?>" <?php
                                        if ($UserInformation['UserID'] == $EachUser['UserID']) {
                                            print 'selected';
                                        }
                                        ?>><?php print($EachUser['FirstName'] . ' ' . $EachUser['LastName']); ?></option>
                                            <?php endforeach ?>
                                </select>
                                <a style="color:#8ea3b6" href="<?php InterfaceAppURL(false); ?>/admin/users/edit/<?php echo $UserInformation['UserID']; ?>/Logout/" ><?php InterfaceLanguage('Screen', '0734', false, '', false, true); ?></a>                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function () {
                    $('#administrator-user-switch-select').change(function () {
                        window.location = '<?php InterfaceAppURL(); ?>/admin/users/edit/' + $(this).val() + '/Login';
                    });
                });
            </script>
        <?php endif; ?>

        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?php InterfaceAppURL(false); ?>/user/overview/">
                            <img style="width: 190px" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/logo-flying.png" alt="logo" class="logo-default" />
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                                <a href="<?php InterfaceAppURL(); ?>/user/credits" id="credit-info" >
                                    <?php if ($UserInformation['GroupInformation']['PaymentSystem'] == 'Enabled' && $UserInformation['GroupInformation']['CreditSystem'] == 'Enabled'): ?>
                                        <div class="user-info">
                                            <span class="badge badge-danger">1</span>
                                            <?php InterfaceLanguage('Screen', '1491', false, '', false, false, array(number_format($UserInformation['AvailableCredits']))); ?>&nbsp;&nbsp;&nbsp;
                                            <span style="text-decoration:underline;"><strong><?php InterfaceLanguage('Screen', '1872'); ?></strong></span>
                                        </div>
                                    <?php endif; ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php InterfaceAppURL(); ?>/user/account/" class="dropdown-toggle" data-toggle="" data-hover="dropdown" data-close-others="true">
                                    <span class="username"><?php InterfaceLanguage('Screen', '9196', false, '', false); ?> <?php echo $UserInformation['FirstName'] . " " . $UserInformation['LastName'] ?></span>
                                </a>
                            </li>
                            <li>
                                <a target="_blank" href="https://support.flyinglist.com/index.php?p=support" class="dropdown-toggle" data-toggle="" data-hover="dropdown" data-close-others="true">
                                    <span class="username"><?php InterfaceLanguage('Screen', '9223', false); ?></span>
                                </a>
                            </li>
                            <!--dropdown-dark-->
                            <li class="dropdown dropdown-user  dropdown-dark">
                                <a href="<?php InterfaceAppURL(); ?>/user/account/" class="dropdown-toggle" data-toggle="" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-settings" style="margin-right: 3px"></i>
                                    <span class="username"> <?php InterfaceLanguage('Screen', '0110', false, '', false); ?> </span>
                                    <i class="fa fa-angle-down font-purple-sharp"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--                                    <li>
                                                                            <a href="<?php InterfaceAppURL(false); ?>/user/account/">
                                                                                <i class="icon-settings"></i> <?php InterfaceLanguage('Screen', '0110', false, '', false); ?> </a>
                                                                        </li>-->

                                    <?php if (InterfacePrivilegeCheck('User.Update', $UserInformation)) : ?>
                                        <li class="with-module<?php print($SubSection == 'Account' ? ' selected active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/account/"><i class="icon-user"></i><?php InterfaceLanguage('Screen', '1106', false, '', false); ?></a></li>
                                        <li class="<?php print($SubSection == 'APIKeys' ? ' active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/apikeys/"><i class="icon-bag"></i><?php InterfaceLanguage('Screen', '1940', false, '', false); ?></a></li>
                                    <?php endif; ?>
                                    <?php if (InterfacePrivilegeCheck('Clients.Get', $UserInformation)) : ?>
                                        <li class="<?php print($SubSection == 'Clients' ? 'selected active' : ''); ?> <?php echo InterfacePrivilegeCheck('User.Update', $UserInformation) ? '' : 'with-module'; ?>"><a href="<?php InterfaceAppURL(); ?>/user/clients/"><i class="icon-users"></i><?php InterfaceLanguage('Screen', '0572', false, '', false); ?></a></li>
                                    <?php endif; ?>
                                    <?php if (InterfacePrivilegeCheck('EmailTemplates.Manage', $UserInformation)) : ?>
                                        <li <?php print($SubSection == 'EmailTemplates' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/"><i class="icon-eye"></i><?php InterfaceLanguage('Screen', '0111', false, '', false); ?></a></li>
                                    <?php endif; ?>
                                    <li class="divider"> </li>
                                    <?php if (PME_USAGE_TYPE == 'User' || WUFOO_INTEGRATION_ENABLED): ?>
                                        <li <?php print(substr($SubSection, 0, 11) == 'Integration' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/integration/"><i class="icon-refresh"></i><?php InterfaceLanguage('Screen', '0132', false, '', false); ?></a></li>
                                    <?php endif; ?>
                                    <li <?php print($SubSection == 'Tags' ? 'class="selected active"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/tags/"><i class="icon-tag"></i><?php InterfaceLanguage('Screen', '0723', false, '', false); ?></a></li>
                                    <li class="divider"> </li>
                                    <?php
                                    InterfacePluginMenuHook('User.Settings', $SubSection, '<li><a href="_LINK_"><i class="icon-info"></i>_TITLE_</a></li>', '<li class="selected active"><a href="_LINK_"><i class="icon-info"></i>_TITLE_</a></li>'
                                    );
                                    ?>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php InterfaceAppURL(false); ?>/user/logout/">
                                    <i class="icon-power"></i> <?php InterfaceLanguage('Screen', '0619', false, '', false); ?> </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="<?php InterfaceAppURL(); ?>/user/search/" method="POST">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php InterfaceLanguage('Screen', '0728', false, '', false, true); ?>" name="search-keyword-input" value="" id="search-keyword-input" />
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">
                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Overview' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/overview/">
                                    <?php InterfaceLanguage('Screen', '0735', false, '', false, true); ?>
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Campaigns' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/campaigns/browse/">
                                    <?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?>
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Lists' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/lists/browse/">
                                    <?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?>
                                </a>
                            </li>

                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Subscribers' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/subscribers/browse/">
                                    <?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?>
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Email Templates' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/emailtemplates/">
                                    <?php InterfaceLanguage('Screen', '0111', false, '', false, true); ?>
                                </a>
                            </li>
<!--                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentDropMenuItem == 'Media Library' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/medialibrary/">
                            <?php InterfaceLanguage('Screen', '0552', false, '', false, true); ?>
                                </a>
                            </li>-->
                            <?php
                            $class = $CurrentDropMenuItem == $CurrentMenuItem ? "active" : ""
                            ?>
                            <?php
                            InterfacePluginMenuHook('User.TopMenu', array($CurrentMenuItem, $CurrentDropMenuItem), '<li id="plugin-drop-menu-_ID_" class="menu-dropdown classic-menu-dropdown "' . $class . '><a href="_LINK_">_TITLE_</a></li>', ''
                            );
                            ?>
<!--                            <li class="menu-dropdown classic-menu-dropdown <?php echo $CurrentMenuItem == 'Email Templates' ? 'active' : '' ?> ">
                                <a href="<?php InterfaceAppURL(false); ?>/user/photoeditor/">
                                    Photo editor
                                </a>
                            </li>-->
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->


        <div class="page-content">
            <div class="container">
                <?php Plugins::HookListener('Action', 'UI.All', array('cem')); ?>
                <?php
                if ($CurrentMenuItem != 'Checkout') {
                    Plugins::HookListener('Action', 'UI.Invoices');
                }
                ?>
                <?php
                $disable = Plugins::HookListener('Action', 'UI.AccountChecker')[0];
                $disabled_class = $disable ? 'disabled' : '';
                ?>
                <?php
                if (isset($_SESSION[SESSION_NAME]['PAYMENTFAILED'])) {
                    ?>
                    <div class="alert alert-danger alert-dismissable" style="width: 910px">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php
                        echo $_SESSION[SESSION_NAME]['PAYMENTFAILED'];
                        unset($_SESSION[SESSION_NAME]['PAYMENTFAILED']);
                        ?>
                    </div>
                    <?php
                } elseif (isset($_SESSION[SESSION_NAME]['PAYMENTSUCCESS'])) {
                    ?>
                    <div class="alert alert-info alert-dismissable" style="width: 910px">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php
                        echo $_SESSION[SESSION_NAME]['PAYMENTSUCCESS'];
                        unset($_SESSION[SESSION_NAME]['PAYMENTSUCCESS']);
                        ?>
                    </div>
                    <?php
                }
                ?>
