<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <title><?php print($PageTitle); ?></title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />


        <!-- BLUEPRINT CSS FRAMEWORK - START -->
        <!--<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">-->
        <!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
        <!-- BLUEPRINT CSS FRAMEWORK - END -->

        <link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/user/css" type="text/css" media="screen, projection">
            <link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/iguana.css" type="text/css" media="screen, projection">
                <link rel="shortcut icon" href="<?php InterfaceInstallationURL(); ?>/favicon.ico">

                    <script>
                        var APP_URL = '<?php InterfaceAppURL(); ?>';
                        var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
                        var IGUANA_CSS_URL = '<?php InterfaceTemplateURL(); ?>styles/iguana_frame.css';
                        var EMAIL_CONTENT_URL = APP_URL + '/user/emailcontentbuilder/content/<?php echo $EmailID ?>';
                        var Language = {
                            '0001': '<?php InterfaceLanguage('Screen', '0301', false); ?>',
                            '0002': '<?php InterfaceLanguage('Screen', '0303', false); ?>',
                            '0003': '<?php InterfaceLanguage('Screen', '0504', false); ?>',
                            '0004': '<?php InterfaceLanguage('Screen', '0505', false); ?>',
                            '0005': '<?php InterfaceLanguage('Screen', '0506', false); ?>',
                            '0006': '<?php InterfaceLanguage('Screen', '0507', false); ?>',
                            '0007': '<?php InterfaceLanguage('Screen', '0508', false); ?>'
                        };
                    </script>
                    <script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
                    <script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce_3432/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
                    <script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
                    <script src="<?php InterfaceTemplateURL(); ?>js/iguana.js" type="text/javascript" charset="utf-8"></script>
                    <script type="text/javascript" charset="utf-8">
                        var tinymce_config = {
                            // Location of tinymce script
                            script_url: '<?php print(InterfaceTemplateURL()); ?>js/tiny_mce_3432/tiny_mce.js',
                            // General options
                            theme: 'advanced',
                            plugins: 'fullpage,safari,table,style,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,personalizer',
                            auto_reset_designmode: true,
                            browsers: 'msie,gecko,safari,opera',
                            dialog_type: 'modal',
                            directionality: "ltr",
                            docs_language: "en",
                            language: "en",
                            nowrap: false,
                            apply_source_formatting: true,
                            entity_encoding: 'named',
                            cleanup: true,
                            cleanup_on_startup: true,
                            verify_html: false,
                            forced_root_block: '',
                            convert_newlines_to_brs: false,
                            element_format: 'xhtml',
                            fix_list_elements: true,
                            fix_table_elements: true,
                            fix_nesting: true,
                            force_p_newlines: true,
                            force_br_newlines: false,
                            force_hex_style_colors: true,
                            preformatted: false,
                            valid_child_elements: 'table[tr|td]',
                            paste_auto_cleanup_on_paste: true,
                            relative_urls: false,
                            convert_urls: false,
                            file_browser_callback: "oempro_media_library",
                            // Theme options
                            theme_advanced_buttons1: "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor",
                            theme_advanced_buttons2: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,cleanup,fullscreen,|,code",
                            theme_advanced_buttons3: "personalizer,|,table,row_after,row_before,col_after,col_before,delete_col,delete_row,delete_table,|,link,unlink,image",
                            theme_advanced_toolbar_location: "top",
                            theme_advanced_toolbar_align: "left",
                            theme_advanced_statusbar_location: "bottom",
                            theme_advanced_resizing: true,
                            // Personalizer tags
                            personalizer_tags: [
<?php
$javascriptStringArray = array();
foreach ($ContentTags as $Label => $TagGroup) {
    $javascriptStringArray[] = "{ label : true, title : '" . htmlspecialchars($Label, ENT_QUOTES) . "' }";
    foreach ($TagGroup as $Tag => $Label) {
        $javascriptStringArray[] = "{ title : '" . htmlspecialchars($Label, ENT_QUOTES) . "', tags : '" . $Tag . "' }";
    }
}
echo implode(',', $javascriptStringArray);
?>
                            ],
                            setup: function (ed) {
                                ed.onKeyUp.add(function (ed, e) {
                                    iguana.update_element_content(ed.getContent());
                                });
                                ed.onNodeChange.add(function (ed, cm, e) {
                                    iguana.update_element_content(ed.getContent());
                                });
                            }
                        };
                        function oempro_media_library(field_name, url, type, win) {
                            tinyMCE.activeEditor.windowManager.open({
                                file: '<?php InterfaceAppURL(); ?>/user/medialibrary/browse/0/popup', // PHP session ID is now included if there is one at all
                                width: 450,
                                height: 500,
                                resizable: "yes",
                                close_previous: "no",
                                scrollbars: "yes",
                                popup_css: false
                            }, {
                                window: win,
                                input: field_name
                            });
                            return false;
                        }

                    </script>
                    </head>
                    <body>