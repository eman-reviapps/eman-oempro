<?php
$folder = 'images' . DIRECTORY_SEPARATOR;          //nome della cartella da cui prendere le immagini
$sitoweb = getFullUrl();
$sitoweb = str_replace("admin/editor", "", $sitoweb);

$immagini = isset($_GET['immagini']) ? $_GET['immagini'] : '';
$op = isset($_GET['op']) ? $_GET['op'] : '';
?>

<div id="contenutoimmagini"></div>

<form  class="form-horizontal" enctype="multipart/form-data" id="form-id">
    <div class="form-body">
        <div class="form-group">
            <div class="col-md-6">
                <input name="nomefile" type="file" />
            </div>
            <div class="col-md-6">
                <input class="button btn blue" type="button" value="Upload" />
            </div>
        </div>
    </div>
</form>
<br/>
<progress value="0"></progress>
<br/>
<br>

<script>
    $(document).ready(function () {
        $("#previewimg").on('shown.bs.modal', function () {
            $('progress').attr({value: 0});
        });

        $('.button').click(function () {
            var formx = document.getElementById('form-id');
            var formData = new FormData(formx);
            $('progress').attr({value: 0});

            $.ajax({
                url: contr_path + '/user/emaildragdropbuilder/uploadImage/', //Server script to process data
                type: 'POST',
                Origin: document.domain,
                xhr: function () {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                //Ajax events
//                success: completeHandler,
                success: function (data) {
                    inserisci(data, true);
                    loadimages();
                    $('progress').attr({value: 0});
                },
                error: errorHandler,
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });

            function completeHandler() {
                loadimages();
            }
            function errorHandler() {
                alert('errore caricamento');
            }
            function progressHandlingFunction(e) {
                if (e.lengthComputable) {
                    $('progress').attr({value: e.loaded, max: e.total});
                }
            }
        });
        loadimages();
    });

    function inserisci(image, direct) {

        if (direct)
        {
            $('#image-url').val(image);
        } else
        {
            var link = $(image);
            var image_e = link.data('image');
            $('#image-url').val(image_e);
        }

        var id = $('#image-url').data('id');
        $('#' + id).attr('src', $('#image-url').val());
        $('#' + id).attr('width', $('#image-w').val());
        $('#' + id).attr('height', $('#image-h').val());

        $('progress').attr({value: 0});
//        $('#previewimg').modal('toggle');
    }

    function loadimages() {

        var request = $.ajax({
            url: contr_path + '/user/emaildragdropbuilder/loadimages/',
            method: "POST",

            data: {nome: ''},
            dataType: "html"
        });

        request.done(function (msg) {
            $("#contenutoimmagini").html(msg);
        });

    }
</script>

<?php

function trovaStringa($text, $wordToSearch) {
    $offset = 0;
    $pos = 0;
    while (is_integer($pos)) {
        $pos = strpos($text, $wordToSearch, $offset);
        if (is_integer($pos)) {
            $arrPos[] = $pos;
            $offset = $pos + strlen($wordToSearch);
        }
    }
    if (isset($arrPos)) {
        return 1;
    } else {
        return 0;
    }
}

function makeSafe($file) {
    return str_replace('..', '', urldecode($file));
}

function getFullUrl() {
    if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) {
        $https = strlen($_SERVER['HTTPS']) != 0 && $_SERVER['HTTPS'] !== 'off';
    }$https = "";
    return
            ($https ? 'https://' : 'http://') .
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'] .
            ($https && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':' . $_SERVER['SERVER_PORT']))) .
            substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
}
?>
