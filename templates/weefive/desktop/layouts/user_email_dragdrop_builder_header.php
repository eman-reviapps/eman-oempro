
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title><?php print($PageTitle); ?></title>

        <!-- styles -->

        <link href="<?php InterfaceTemplateURL(); ?>styles/dragdrop/colpick.css" rel="stylesheet"  type="text/css"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php InterfaceTemplateURL(); ?>styles/dragdrop/responsive-table.css" rel="stylesheet"/>
        <link href="<?php InterfaceTemplateURL(); ?>styles/dragdrop/template.editor.css" rel="stylesheet"/>
        <link href="<?php InterfaceTemplateURL(); ?>styles/dragdrop/css.css" rel="stylesheet"/>

        <!--[if lt IE 9]>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <![endif]-->
        <script type="text/javascript"> var path = '<?= InterfaceTemplateURL(); ?>';</script>
        <script type="text/javascript"> var contr_path = '<?= InterfaceAppURL(); ?>';</script>
        <script type="text/javascript"> var dir = '<?= TEMPLATE_PATH; ?>dragdrop';</script>
        <script type="text/javascript"> var data_url = '<?= DATA_URL; ?>dragdrop/';</script>
        <!--<script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>-->
        
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-ui.min.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.2.6/plugins/colorpicker/plugin.min.js"></script>
        <script type="text/javascript" src="<?= InterfaceTemplateURL(); ?>/js/dragdrop/colpick.js"></script>
        <script type="text/javascript">var scrippath="<?= InterfaceTemplateURL(); ?>";</script>
<!--        <script type="text/javascript" src="<?= InterfaceTemplateURL(); ?>/js/dragdrop/template.editor.js"></script>-->
        
    </head>
    <body>
         <div id="wrapper">