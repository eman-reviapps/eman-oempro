</div>
</div>
<!--<div id="bottom">
<?php
$FooterTemplate = '
		<div class="container">
			<div class="span-23 last">
				<div class="custom-column-container cols-2 clearfix" style="margin-top:18px;">
					<div class="col">
						' . USERAREA_FOOTER . '
						<p>
							' . InterfaceUserTrialNotice($UserInformation, true, InterfaceLanguage('Screen', '1615', true, '', false, false)) . '
						</p>
					</div>
					<div class="col">
						<p class="dimmed">
						</p>
					</div>
				</div>
			</div>
		</div>';
InterfaceUserFooter(false, $FooterTemplate);
?>

</div>-->


<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12 footer-block">
                <h2 style="margin-bottom: 0">About</h2>
                <p> Flying List tools help you grow your audience, engagement, and sales.
                    Flying List Email offers an integrated, Engagement tools, Auto responder, Automation, Landing pages and High professional reports robust feature set built to help the enterprise of any size. A powerfully simple and simply powerful email marketing automation solution designed to help you create and send compelling campaigns with ease
                </p>
                <?php if (RUN_CRON_IN_USER_AREA == 'true'): ?>
                    <img id="cron-image" src="<?php print(InterfaceInstallationURL(true) . 'cron.php'); ?>" width="1" height="1" alt="Cron Executer">
                <?php endif; ?>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                <ul class="menu-footer-vertical">
                    <li>
                        <a target="_blank" href="https://support.flyinglist.com/"><?php InterfaceLanguage('Screen', '9223', false); ?></a>
                        ( <a target="_blank" style="margin: 0 5px" href="https://support.flyinglist.com/index.php?p=chat"><?php InterfaceLanguage('Screen', '9225', false); ?></a>
                        <a target="_blank" style="margin: 0 5px" href="https://support.flyinglist.com/index.php?p=support&sp=create&ssp=1"><?php InterfaceLanguage('Screen', '9224', false); ?></a>                 
                        <a target="_blank" style="margin: 0 5px" href="https://support.flyinglist.com/index.php?p=faq"><?php InterfaceLanguage('Screen', '9226', false); ?></a> )
                    </li>
                </ul>
                <div class="social_div col-sm-6">
                    <ul class="social-icons">
                        <li>
                            <a target="_blank" href="https://www.facebook.com/flyinglistmail" data-original-title="facebook" class="facebook"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://twitter.com/flyinglistmail" data-original-title="twitter" class="twitter"></a>
                        </li>
                        <li>
                            <!--instgram-->
                            <a target="_blank" href="https://www.instagram.com/flyinglistmail" data-original-title="googleplus" class="instagram"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.youtube.com/channel/UCQpAyDXZrl0xR8voBHUdzEw" data-original-title="youtube" class="youtube"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://plus.google.com/u/0/112590362307223510170" data-original-title="google-plus" class="googleplus"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.pinterest.com/flyinglist/" data-original-title="pinterest" class="pintrest"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.linkedin.com/in/flyinglist/" data-original-title="linkedin" class="linkedin"></a>
                        </li>
                    </ul>
                </div>
                <div class="ssl_certificate  col-sm-6" >

                    <script language="JavaScript" type="text/javascript" >
                        TrustLogo("https://flyinglist.com/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN INNER FOOTER -->
<div class="page-footer">
    <div class="container"> 
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="width:70%">
                <ul class="menu-footer">
                    <li class="menu-item"><a target="_blank" href="http://flyinglist.com/anti-spam-policy/">Anti-Spam Policy</a></li>
                    <li class="menu-item"><a target="_blank" href="http://flyinglist.com/privacy-policy/">Privacy Policy</a></li>
                    <li class="menu-item"><a target="_blank" href="http://flyinglist.com/spam-free-zone/">Spam Free Zone</a></li>
                    <li class="menu-item"><a target="_blank" href="http://flyinglist.com/terms-and-conditions/">Terms and Conditions</a></li>
                    <li class="menu-item"><a target="_blank" href="http://flyinglist.com/user-pledge/">User Pledge</a></li>
                    <li class="menu-item"><a target="_blank" href="https://flyinglist.com/ar/distance-sales-contract/">Distance Sales Contract</a></li>
                </ul>
            </div>
            <div class="col-md-4  col-md-offset-5" style="width:37%">
                Copyright &copy; <a href="http://flyinglist.com/" target="_blank">FlyingList</a> 2016. All Rights Reserved.
            </div>
        </div>
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>


</body>
</html>
