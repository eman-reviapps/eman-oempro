<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0714', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
	<div class="span-5 snap-5">
		<ul class="left-sub-navigation">
			<li class="header first with-module"><?php InterfaceLanguage('Screen', '0716', false, '', true); ?></li>
			<li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Sent' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Sent/"><?php InterfaceLanguage('Screen', '0717', false, '', false, true); ?><span class="item-count"><?php if ($TotalSentCampaigns > 0) { print('('.$TotalSentCampaigns.')'); } ?></span></a></li>
			<li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Outbox' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Outbox/"><?php InterfaceLanguage('Screen', '0718', false, '', false, true); ?><span class="item-count"><?php if ($TotalOutboxCampaigns > 0) { print('('.$TotalOutboxCampaigns.')'); } ?></span></a></li>
			<li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Paused' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Paused/"><?php InterfaceLanguage('Screen', '0721', false, '', false, true); ?><span class="item-count"><?php if ($TotalPausedCampaigns > 0) { print('('.$TotalPausedCampaigns.')'); } ?></span></a></li>
		</ul>
	</div>
	<div class="span-18 last">
		<div id="page-shadow">
			<div id="page">
				<div class="white" style="min-height:300px;">
					<form id="campaigns-table-form" action="<?php InterfaceAppURL(); ?>/client/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/" method="post">
						<?php if ($FilterData == 'Sent'): ?>
							<table border="0" cellspacing="0" cellpadding="0" class="grid" id="campaigns-table">
								<tr>
									<th width="300"><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php print count($FilterTags) < 1 ? '-1' : implode(':', $FilterTags); ?>/CampaignName/ASC"><?php InterfaceLanguage('Screen', '0140', false, '', true); ?> / <?php InterfaceLanguage('Screen', '0717', false, '', true); ?></a></th>
									<th width="130"><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php print count($FilterTags) < 1 ? '-1' : implode(':', $FilterTags); ?>/SendProcessFinishedOn/ASC"><?php InterfaceLanguage('Screen', '0725', false, '', true); ?></a></th>
									<th width="70"><?php InterfaceLanguage('Screen', '0726', false, '', true); ?></th>
								</tr>
								<?php if (count($Campaigns) < 1): ?>
									<tr>
										<td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
									</tr>
								<?php endif; ?>
								<?php 
								foreach ($Campaigns as $EachCampaign): 
									// Unique open percentage calculation - Start {
									$UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
									// Unique open percentage calculation - End }
								?>
									<tr>
										<td width="285">
											<?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
												<span class="label"><?php print($EachTag['Tag']); ?></span>
											<?php endforeach; ?>
											<?php if ($EachCampaign['SplitTest'] != false): ?>
												<span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
											<?php endif; ?>
											<?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
												<a href="<?php InterfaceAppURL(); ?>/client/campaign/overview/<?php print($EachCampaign['CampaignID']); ?>"><?php print($EachCampaign['CampaignName']); ?></a>
											<?php else: ?>
												<?php print($EachCampaign['CampaignName']); ?>												
											<?php endif; ?>
										</td>
										<td width="130" class="small-text"><?php print(date('M d, Y - H:i', strtotime($EachCampaign['SendProcessFinishedOn']))); ?></td>
										<td width="70" class="small-text"><div class="csspie thumb-blue" data="<?php print($UniqueOpenRate); ?>"></div> <span class="data"><?php print(number_format($UniqueOpenRate, 0)); ?>%</span></td>
									</tr>
								<?php endforeach; ?>
							</table>
						<?php elseif ($FilterData == 'Outbox'): ?>
							<table border="0" cellspacing="0" cellpadding="0" class="grid" id="campaigns-table">
								<tr>
									<th width="700"><?php InterfaceLanguage('Screen', '0140', false, '', true); ?> / <?php InterfaceLanguage('Screen', '0718', false, '', true); ?></th>
								</tr>
								<?php if (count($Campaigns) < 1): ?>
									<tr>
										<td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
									</tr>
								<?php endif; ?>
								<?php 
								foreach ($Campaigns as $EachCampaign): 
									// Unique open percentage calculation - Start {
									$UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
									// Unique open percentage calculation - End }
								?>
								<tr>
									<td width="485" style="padding-right:18px;">
										<?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
											<span class="label"><?php print($EachTag['Tag']); ?></span>
										<?php endforeach; ?>
										<?php if ($EachCampaign['SplitTest'] != false): ?>
											<span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
										<?php endif; ?>
										<?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
											<a href="<?php InterfaceAppURL(); ?>/client/campaign/overview/<?php print($EachCampaign['CampaignID']); ?>"><?php print($EachCampaign['CampaignName']); ?></a>
										<?php else: ?>
											<?php print($EachCampaign['CampaignName']); ?>												
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</table>
						<?php elseif ($FilterData == 'Paused'): ?>
							<table border="0" cellspacing="0" cellpadding="0" class="grid" id="campaigns-table">
								<tr><th width="700"><?php InterfaceLanguage('Screen', '0140', false, '', true); ?> / <?php InterfaceLanguage('Screen', '0721', false, '', true); ?></th></tr>
								<?php if (count($Campaigns) < 1): ?>
									<tr>
										<td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
									</tr>
								<?php endif; ?>
								<?php 
								foreach ($Campaigns as $EachCampaign): 
									// Unique open percentage calculation - Start {
									$UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
									// Unique open percentage calculation - End }
								?>
								<tr>
									<td width="485" style="padding-right:18px;">
										<?php if ($EachCampaign['SplitTest'] != false): ?>
											<span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
										<?php endif; ?>
										<a href="<?php InterfaceAppURL(); ?>/client/campaign/overview/<?php print($EachCampaign['CampaignID']); ?>"><?php print($EachCampaign['CampaignName']); ?></a>
									</td>
								</tr>
								<?php endforeach; ?>
							</table>
						<?php endif; ?>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php if ($TotalPages > 1): ?>
	<div class="span-1 last">
		<ul class="pagination with-module">
			<?php
			if ($CurrentPage > 1):
			?>
			<li class="first"><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/1/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
			<?php
			endif;
			?>
			<?php
				$CounterStart	= ($CurrentPage < 3 ? 1 : $CurrentPage - 2);
				$CounterFinish	= ($CurrentPage > ($TotalPages - 2) ? $TotalPages : $CurrentPage + 2);
			for ($PageCounter = $CounterStart; $PageCounter <= $CounterFinish; $PageCounter++):
			?>
			<li <?php print(($CurrentPage == $PageCounter ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/<?php print($PageCounter); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/"><?php print(number_format($PageCounter)); ?></a></li>
			<?php
			endfor;
			?>
			<?php
			if ($CurrentPage < $TotalPages):
			?>
			<li class="last"><a href="<?php InterfaceAppURL(); ?>/client/campaigns/browse/<?php print($TotalPages); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
			<?php
			endif;
			?>
		</ul>
	</div>
	<?php endif; ?>
</div>
<!-- Page - End -->

<script type="text/javascript" charset="utf-8">
	var language_object = {
		screen : {
			'0727'	: '<?php InterfaceLanguage('Screen', '0727', false, '', false); ?>',
			'0957'	: '<?php InterfaceLanguage('Screen', '0957', false, '', false); ?>',
			'0959'	: '<?php InterfaceLanguage('Screen', '0959', false, '', false); ?>'
		}
	};
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaigns.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_footer.php'); ?>