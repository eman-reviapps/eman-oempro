<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0034', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - START -->
<form id="account-save" action="<?php InterfaceAppURL(); ?>/client/account/" method="post">
	<input type="hidden" name="Command" value="save" id="Command">
	<div class="container">
		<div class="span-18">
			<div id="page-shadow">
				<div id="page">
					<div class="white">
						<?php
						if (isset($PageErrorMessage) == true):
						?>
							<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
						<?php
						elseif (isset($PageSuccessMessage) == true):
						?>
							<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
						<?php
						elseif (validation_errors()):
						?>
						<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php else: ?>
						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
						<?php
						endif;
						?>
						<div class="form-row <?php print((form_error('ClientUsername') != '' ? 'error' : '')); ?>" id="form-row-ClientUsername">
							<label for="ClientUsername"><?php InterfaceLanguage('Screen', '0002', false, '', false, true); ?>: *</label>
							<input type="text" name="ClientUsername" value="<?php echo set_value('ClientUsername', $ClientInformation['ClientUsername']); ?>" id="ClientUsername" class="text" />
							<?php print(form_error('ClientUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row <?php print((form_error('ClientPassword') != '' ? 'error' : '')); ?>" id="form-row-ClientPassword">
							<label for="ClientPassword"><?php InterfaceLanguage('Screen', '0003', false, '', false, true); ?>: *</label>
							<input type="password" name="ClientPassword" value="" id="ClientPassword" class="text" />
							<?php print(form_error('ClientPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row <?php print((form_error('ClientName') != '' ? 'error' : '')); ?>" id="form-row-ClientName">
							<label for="ClientName"><?php InterfaceLanguage('Screen', '0051', false, '', false, true); ?>: *</label>
							<input type="text" name="ClientName" value="<?php echo set_value('ClientName', $ClientInformation['ClientName']); ?>" id="ClientName" class="text" />
							<?php print(form_error('ClientName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row <?php print((form_error('ClientEmailAddress') != '' ? 'error' : '')); ?>" id="form-row-ClientEmailAddress">
							<label for="ClientEmailAddress"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
							<input type="text" name="ClientEmailAddress" value="<?php echo set_value('ClientEmailAddress', $ClientInformation['ClientEmailAddress']); ?>" id="ClientEmailAddress" class="text" />
							<?php print(form_error('ClientEmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_listcreate.php'); ?>
		</div>
		
		<div class="span-18 last">
			<div class="form-action-container">
				<a class="button" targetform="account-save" id="form-button" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1473', false, '', true); ?></strong></a>
			</div>
		</div>
		
	</div>
</form>
<!-- Page - END -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_footer.php'); ?>