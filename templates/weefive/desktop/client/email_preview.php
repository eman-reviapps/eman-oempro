<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_campaign_preview_header.php'); ?>
<div id="top" class="iguana">
	<div class="container">
		<div class="span-14">
			<ul class="tabs">
				<li class="label"><?php InterfaceLanguage('Screen', '0988', false, '', false); ?></li>
				<?php if ($EmailInformation['ContentType'] != 'Plain'): ?>
				<li <?php if($Type == 'html'): ?>class="selected"<?php endif; ?>>
					<a href="<?php echo InterfaceAppURL('true').'/client/email/preview/'.$EmailID.'/html/'.$Mode.'/'.$EntityID; ?>">
						<?php if ($Type == 'html'): ?>
							<span class="left">&nbsp;</span>
							<span class="right">&nbsp;</span>
						<?php endif ?>
						<strong><?php InterfaceLanguage('Screen', '0175', false, '', true, false, array()); ?></strong>
					</a>
				</li>
				<?php endif; ?>
				<?php if ($EmailInformation['ContentType'] != 'HTML'): ?>
				<li <?php if($Type == 'plain'): ?>class="selected"<?php endif; ?>>
					<a href="<?php echo InterfaceAppURL('true').'/client/email/preview/'.$EmailID.'/plain/'.$Mode.'/'.$EntityID; ?>">
						<?php if ($Type == 'plain'): ?>
							<span class="left">&nbsp;</span>
							<span class="right">&nbsp;</span>
						<?php endif ?>
						<strong><?php InterfaceLanguage('Screen', '0176', false, '', true, false, array()); ?></strong>
					</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
	<div class="container">
		<div class="span-24 last" style="background-color:#fff;">
			<iframe src="<?php InterfaceAppURL(); ?>/client/email/preview/<?php echo $EmailID ?>/<?php echo $Type; ?>/<?php echo $Mode; ?>/<?php echo $EntityID ?>/source" name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
		</div>
	</div>
</div>




<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_campaign_preview_footer.php'); ?>