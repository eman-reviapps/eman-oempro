<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php print(ucwords($ListInformation['Name'])); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->


<!-- Page - Start -->
<div class="container">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<?php if (isset($PageSuccessMessage) == true): ?>
				<div class="module-container">
					<div class="page-message success">
						<div class="inner">
							<span class="close">X</span>
							<?php print($PageSuccessMessage); ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="page-bar">
					<ul class="livetabs">
						<li><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0141', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0841', false, '', false); ?></a></li>
					</ul>
				</div>
				<div class="white" style="min-height:450px;">
					<div class="inner flash-chart-container" id="activity-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/client/list/chartdata/<?php print($ListInformation['ListID']) ?>/30" style="height:150px;">
					</div>
					<div class="custom-column-container cols-2 clearfix">
						<div class="col">
							<img src="<?php echo InterfaceAppURL(true).'/client/list/sparkline/opens/'.$ListInformation['ListID']; ?>" width="70" /> <span class="data big"><?php echo $ListInformation['OverallOpenPerformance']; ?>%</span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0975', false, '', false, true); ?></span><br />
							<span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?></span> <span class="data"><?php echo $ListInformation['OverallAccountOpenPerformance']; ?>%</span> <span class="data small" style="color:#<?php if ($ListInformation['OpenPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['OpenPerformanceDifference']; ?>%)</span><br />
							<?php if ($ListInformation['OverallOpenPerformance'] != 0): ?><span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $ListInformation['HighestOpenDay']; ?></span><br /><?php endif; ?>
							<img src="<?php echo InterfaceAppURL(true).'/client/list/sparkline/clicks/'.$ListInformation['ListID']; ?>" width="70" /> <span class="data big"><?php echo $ListInformation['OverallClickPerformance']; ?>%</span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0976', false, '', false, true); ?></span><br />
							<span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?></span> <span class="data"><?php echo $ListInformation['OverallAccountClickPerformance']; ?>%</span> <span class="data small" style="color:#<?php if ($ListInformation['ClickPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['ClickPerformanceDifference']; ?>%)</span><br />
							<?php if ($ListInformation['OverallClickPerformance'] != 0): ?><span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $ListInformation['HighestClickDay']; ?></span><br /><?php endif; ?>
							<img src="<?php echo InterfaceAppURL(true).'/client/list/sparkline/forwards/'.$ListInformation['ListID']; ?>" width="70" /> <span class="data-label"><?php InterfaceLanguage('Screen', '0839', false, '', false, true); ?></span><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/list/sparkline/views/'.$ListInformation['ListID']; ?>" width="70" /> <span class="data-label"><?php InterfaceLanguage('Screen', '0840', false, '', false, true); ?></span><br />
						</div>
						<div class="col" style="background-color:#f2f2f2;border:1px solid #e1e1e1;">
							<div class="flash-chart-container" style="float:right;text-align:right;width:140px;padding:18px 18px 0 0;">
								<div id="bounce-pie-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie/5" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/client/list/chartdata/<?php print($ListInformation['ListID']) ?>/30/bounces" style="width:140px;height:140px;float:right;text-align:right;">
								</div>
								<div style="width:140px;text-align:center;">
									<span class="data-label"><?php InterfaceLanguage('Screen', '0977', false, '', false, false); ?></span>
								</div>
							</div>
							<div style="padding:9px 12px;" class="clearfix">
								<span class="data big"><?php echo $ListInformation['SubscriberCount']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?></span><br />
								<span class="data"><?php echo $ListInformation['TotalSpamComplaints']; ?></span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0095', false, '', false, true); ?></span><br />
								<div id="email-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/client/list/chartdata/<?php print($ListInformation['ListID']) ?>/30/email-domain" style="width:80px;height:80px;">
								</div>
								<div style="width:80px;text-align:center;">
									<span class="data-label"><?php InterfaceLanguage('Screen', '0978', false, '', false, false); ?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row no-bg">
						<div class="form-action-container">
							<a style="float:left" class="button" href="<?php InterfaceAppURL(); ?>/client/list/exportstatistics/<?php echo $ListInformation['ListID']; ?>/csv"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0844', false, '', true); ?></strong></a>
							<a style="float:left" class="button" href="<?php InterfaceAppURL(); ?>/client/list/exportstatistics/<?php echo $ListInformation['ListID']; ?>/xml"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0845', false, '', true); ?></strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="help-column span-5 push-1 last">
		<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_lists.php'); ?>
	</div>
</div>
<!-- Page - End -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_footer.php'); ?>