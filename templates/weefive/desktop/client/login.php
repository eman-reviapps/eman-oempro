<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_header.php'); ?>

<div class="span-11 push-1">
	<div class="login-box">
		<div class="inner">
			<form id="Form_Login" method="post" action="<?php InterfaceAppURL(); ?>/client/">
				<?php
				if (isset($PageErrorMessage) == true):
				?>
					<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
				<?php
				elseif (isset($PageSuccessMessage) == true):
				?>
					<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
				<?php
				else:
				?>
					<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1470', false); ?></h3>
				<?php
				endif;
				?>
				<div class="form-row <?php print((form_error('Username') != '' ? 'error' : '')); ?> no-background-color" id="form-row-Username">
					<label for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>:</label>
					<input type="text" name="Username" value="<?php echo set_value('Username', ''); ?>" id="Username" class="text" style="width:260px;" />
					<?php print(form_error('Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row <?php print((form_error('Password') != '' ? 'error' : '')); ?> no-background-color" id="form-row-Password">
					<label for="Password"><?php InterfaceLanguage('Screen', '0003', false); ?>:</label>
					<input type="password" name="Password" value="<?php echo set_value('Password', ''); ?>" id="Password" class="text" style="width:260px;" />
					<?php print(form_error('Password', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row forgot no-background-color small clearfix" id="form-row-RememberMe">
					<label for="RememberMe">&nbsp;</label>
					<div class="checkbox-container">
						<div class="checkbox-row">
							<input type="checkbox" name="RememberMe" value="Yes" id="RememberMe" <?php echo set_checkbox('RememberMe', 'Yes'); ?>> <label for="RememberMe"><?php InterfaceLanguage('Screen', '0005', false); ?></label>
						</div>
					</div>
				</div>
				<div class="form-row forgot no-background-color small clearfix" id="form-row-input5">
					<label>&nbsp;</label>
					<img src="<?php InterfaceTemplateURL(); ?>images/icon_login_question_mark.png" />
					<a href="<?php InterfaceAppURL(); ?>/client/passwordreminder/"><?php InterfaceLanguage('Screen', '0006', false); ?></a>
				</div>
				<div class="form-row no-background-color clearfix">
					<label>&nbsp;</label>
					<a href="#" id="FormButton_Submit" class="button" style="float:left;" targetform="Form_Login"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0266', false, '', true); ?></strong></a>
					<input type="hidden" name="Command" value="Login" id="Command" />
				</div>
			</form>
		</div>
	</div>
</div>
<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_footer.php'); ?>
