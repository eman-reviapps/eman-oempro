<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0933', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<div class="white" style="min-height:500px;">
					<form id="campaigns-table-form" action="<?php InterfaceAppURL(); ?>/user/lists/browse/" method="post">
						<table border="0" cellspacing="0" cellpadding="0" class="grid" id="users-table">
							<tr>
								<th width="570" colspan="2"><?php InterfaceLanguage('Screen', '0541', false, '', true); ?></th>
							</tr>
							<?php if ($Lists === false): ?>
								<tr>
									<td width="355" class="no-padding-left"><?php InterfaceLanguage('Screen', '0105', false, '', false, false); ?></td>
									<td width="130">&nbsp;</td>
								</tr>
							<?php else: ?>
								<?php 
								foreach ($Lists as $EachList): 
								?>
									<tr>
										<td width="355">
											<a href="<?php InterfaceAppURL(); ?>/client/list/statistics/<?php print($EachList['ListID']); ?>"><?php print($EachList['Name']); ?></a>
										</td>
										<td width="130" class="small-text">
											<a href="<?php InterfaceInstallationURL(); ?>rss.php?q=<?php echo $EachList['EncryptedSaltedListID']; ?>" style="float:right;margin-right:18px;" title="<?php InterfaceLanguage('Screen', '1105'); ?>"><img src="<?php InterfaceTemplateURL(); ?>images/icon_rss.png" alt="<?php InterfaceLanguage('Screen', '1105'); ?>" border="0" /></a>
											<span class="data"><?php print(number_format($EachList['SubscriberCount'])); ?></span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0104', false, '', false, false); ?></span>
										</td>
									</tr>
								<?php endforeach; ?>
							<?php endif;?>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="help-column span-5 push-1 last">
		<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_lists.php'); ?>
	</div>
</div>
<!-- Page - End -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_footer.php'); ?>