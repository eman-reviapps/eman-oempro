<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_header.php'); ?>

<script src="<?php InterfaceTemplateURL(false); ?>js/swfobject.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
	var CampaignID = <?php echo $CampaignInformation['CampaignID']; ?>;
	var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
</script>
<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php print(ucwords($CampaignInformation['CampaignName'])); ?>
				<?php if ($CampaignInformation['SplitTest'] != false): ?>
					<span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
				<?php endif; ?>
				</h1>
			</div>
			<div class="actions">
				<?php
				if (($CampaignInformation['SplitTest'] != false) && ($CampaignInformation['RelWinnerEmailID'] == 0))
					{
					$TestEmailVersions	= Emails::RetrieveEmailsOfTest($CampaignInformation['SplitTest']['TestID'], $CampaignInformation['CampaignID']);
					}

				// Encrypted query parameters - Start
				$ArrayQueryParameters = array(
											'CampaignID'		=> $CampaignInformation['CampaignID'],
											'EmailID'			=> ($CampaignInformation['SplitTest'] == false ? 0 : ($CampaignInformation['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $CampaignInformation['SplitTest']['RelWinnerEmailID'])),
											'AutoResponderID'	=> 0,
											);
				$EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
				// Encrypted query parameters - End

				$CampaignPublicLink = APP_URL.'web_browser.php?p='.$EncryptedQuery;

				$Hash = Core::ShortenLink($CampaignPublicLink);
				$CampaignPublicLink = APP_URL.'link.php?p='.rawurlencode($Hash);
				?>

				<div class="main-action-with-menu" id="share-campaign-menu">
					<a href="#" class="main-action"><?php InterfaceLanguage('Screen', '1424'); ?></a>
					<div class="down-arrow"><span></span></div>
					<ul class="main-action-menu">
						<li><a href="http://twitter.com/home/?status=<?php print(rawurlencode(InterfaceLanguage('Screen', '1635', true, '', false, false, array(rawurldecode($CampaignPublicLink))))); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1629', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-twitter-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1629', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1629', false, '', false); ?></a></li>
						<li><a href="http://www.facebook.com/share.php?u=<?php print($CampaignPublicLink); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1630', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-facebook-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1630', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1630', false, '', false); ?></a></li>
						<li><a href="http://www.myspace.com/Modules/PostTo/Pages/?u=<?php print($CampaignPublicLink); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1631', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-myspace-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1631', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1631', false, '', false); ?></a></li>
						<li><a href="http://www.stumbleupon.com/submit?url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1632', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-stumbleupon-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1632', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1632', false, '', false); ?></a></li>
						<li><a href="http://digg.com/submit?phase=2&url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1633', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-digg-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1633', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1633', false, '', false); ?></a></li>
						<li><a href="http://del.icio.us/post?url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1634', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-delicious-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1634', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1634', false, '', false); ?></a></li>
					</ul>
				</div>

				<a class="button" href="<?php InterfaceAppURL() ?>/client/email/preview/<?php echo $CampaignInformation['RelEmailID']; ?>/html/campaign/<?php echo $CampaignInformation['CampaignID']; ?>" target="_blank"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0850', false, '', false, true); ?></strong></a>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->


<!-- Page - Start -->
<div class="container">
	<div class="span-5 snap-5">
		<ul class="left-sub-navigation">
			<li class="header first"><?php InterfaceLanguage('Screen', '0843', false, '', true, false); ?></li>
			<li><a href="<?php InterfaceAppURL(); ?>/client/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/overview/csv"><?php InterfaceLanguage('Screen', '0844', false, '', false, true); ?></a></li>
			<li><a href="<?php InterfaceAppURL(); ?>/client/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/overview/xml"><?php InterfaceLanguage('Screen', '0845', false, '', false, true); ?></a></li>
		</ul>
	</div>
	<div class="span-18 last">
		<div id="page-shadow">
			<div id="page" style="min-height:530px">
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs">
						<li id="tab-chart-selection" class="dropdown-menu">
							<ul tabcallback="set_active_chart_tab">
								<li id="item-sub-report-opens-vs-clicks"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0837', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0838', false, '', false); ?></a></li>
								<li id="item-sub-report-opens-vs-unsubscriptions"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0837', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0841', false, '', false); ?></a></li>
							</ul>
						</li>
					</ul>
					<ul class="livetabs right" tabcollection="report-date-tabs" tabcallback="switch_chart_data_range">
						<li class="label"><?php InterfaceLanguage('Screen', '0093', false, '', false); ?></li>
						<li id="tab-7d"><?php InterfaceLanguage('Screen', '0883', false, '', false, false); ?></li>
						<li id="tab-15d"><?php InterfaceLanguage('Screen', '0881', false, '', false, false); ?></li>
						<li id="tab-30d"><?php InterfaceLanguage('Screen', '0882', false, '', false, false); ?></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner flash-chart-container" id="activity-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/client/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/opens-clicks/" style="height:150px;">
					</div>
					<div class="custom-column-container cols-2 clearfix">
						<div id="sparkline-container" class="col">
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/opens-sparkline'; ?>" /><span class="data big" id="unique-open-count"><?php echo $CampaignInformation['UniqueOpens']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0875', false, '', false, true); ?></span> <?php if ($CampaignInformation['UniqueOpens'] != 0): ?><span class="data-label small"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $HighestOpens[0][0]; ?></span><?php endif; ?><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/clicks-sparkline'; ?>" /><span class="data big" id="unique-click-count"><?php echo $CampaignInformation['UniqueClicks']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0876', false, '', false, true); ?></span> <?php if ($CampaignInformation['UniqueOpens'] != 0): ?><span class="data-label small"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $HighestClicks[0][0]; ?></span><?php endif; ?><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/forwards-sparkline'; ?>" /><span class="data big" id="unqiue-forward-count"><?php echo $CampaignInformation['UniqueForwards']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0877', false, '', false, true); ?></span><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/views-sparkline'; ?>" /><span class="data big" id="unique-browser-view-count"><?php echo $CampaignInformation['UniqueViewsOnBrowser']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0878', false, '', false, true); ?></span><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/unsubscriptions-sparkline'; ?>" /><span class="data big" id="total-unsubscription-count"><?php echo $CampaignInformation['TotalUnsubscriptions']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0097', false, '', false, true); ?></span><br />
							<img src="<?php echo InterfaceAppURL(true).'/client/campaign/statistics/'.$CampaignInformation['CampaignID'].'/bounces-sparkline'; ?>" /><span class="data big" id="total-bounces"><?php echo $CampaignInformation['TotalBounces']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0098', false, '', false, true); ?></span> <?php if ($CampaignInformation['TotalBounces'] > 0): ?><span class="data-label small"><?php InterfaceLanguage('Screen', '0930', false, '', false, false); ?></span> <span class="data" id="hard-bounce-ratio"><?php echo $CampaignInformation['HardBounceRatio']; ?>%</span><span class="data-label small"><?php InterfaceLanguage('Screen', '0931', false, '', false, false); ?></span> <span class="data" id="soft-bounce-ratio"><?php echo $CampaignInformation['SoftBounceRatio']; ?>%</span><?php endif; ?><br />
							<a href="<?php InterfaceAppURL(); ?>client/campaign/exportstatistics/29/bounces/csv" style="font-size:11px;"><?php InterfaceLanguage('Screen', '1427', false, '', false, false, array()); ?></a>
						</div>
						<div class="col">
							<div style="background-color:#f2f2f2;border:1px solid #e1e1e1;">
								<div class="progress-bar campaign-status-sending" <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused') { echo 'style="display:none;"'; } ?>>
									<div id="campaign-send-progress" class="progress"></div>
								</div>
								<div class="flash-chart-container" id="open-pie-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie/5" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/client/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/overview/" style="width:140px;height:140px;float:right;text-align:right;">
								</div>
								<div style="padding:9px 12px;" class="clearfix">
									<div class="campaign-status-sent" <?php if ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused') { echo 'style="display:none;"'; } ?>>
										<span class="data big" id="total-sent-count2"><?php echo $CampaignInformation['TotalSent']; ?></span> <span class="data-label"><?php InterfaceLanguage('Screen', '0147', false, '', false, true); ?></span><br />
										<span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '0884', false, '', false, false); ?>:</span> <span class="data" id="send-process-finished-on"><?php echo date('M d, Y', strtotime($CampaignInformation['SendProcessFinishedOn'])); ?></span>
									</div>
									<?php if ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused'): ?>
									<div class="campaign-status-sending">
										<span class="data" id="total-sent-count"><?php echo $CampaignInformation['TotalSent'] ?></span> <span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '0147', false, '', false, false); ?></span>
									</div>
									<?php endif; ?>
									<span class="data" id="total-recipients"><?php echo $CampaignInformation['TotalRecipients'] ?></span> <span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '0953', false, '', false, false); ?></span><br />
									<span class="data" id="total-failed"><?php echo $CampaignInformation['TotalFailed']; ?></span> <span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '0954', false, '', false, false); ?></span><br />
									<span class="data"><?php echo $CampaignInformation['TotalSpamReports']; ?> (<?php echo number_format((100*$CampaignInformation['TotalSpamReports'])/$CampaignInformation['TotalSent']); ?>%)</span> <span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '0961', false, '', false, false); ?></span><br />
									<span class="data big" id="open-ratio"><?php echo $OpenedRatio; ?>%</span> <span class="data-label"><?php InterfaceLanguage('Screen', '0726', false, '', false, true); ?></span><br />
									<span class="data big" id="not-opened-ratio" style="color:#259E01"><?php echo $NotOpenedRatio; ?>%</span> <span class="data-label"><?php InterfaceLanguage('Screen', '0885', false, '', false, true); ?></span><br />
									<span class="data big" id="bounce-ratio" style="color:#FF9701"><?php echo $BouncedRatio; ?>%</span> <span class="data-label"><?php InterfaceLanguage('Screen', '1113', false, '', false, true); ?></span><br />
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if ($CampaignInformation['SplitTest'] != false): ?>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-3">
						<li id="tab-most-test-statistics"><a href="#"><?php InterfaceLanguage('Screen', '1388', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div class="custom-column-container cols-2 clearfix">
							<div class="col">
								<div class="inner flash-chart-container" id="test-opens-chart-container">
									<script type="text/javascript" charset="utf-8">
										SWFObjectContainer1 = new SWFObject("<?php InterfaceInstallationURL(); ?>assets/charts/amcolumn/amcolumn.swf", "activity-chart", "328", "220", "8", "#FFFFFF");
										SWFObjectContainer1.addVariable("path", "<?php InterfaceInstallationURL(); ?>assets/charts/amcolumn/");
										SWFObjectContainer1.addVariable("settings_file", escape("<?php InterfaceAppURL(); ?>/admin/chart/settings/bar/5"));
										SWFObjectContainer1.addVariable("data_file", escape("<?php InterfaceAppURL(); ?>/client/campaign/teststatistics/<?php print($CampaignInformation['CampaignID']) ?>/"));
										SWFObjectContainer1.addVariable("preloader_color", "#FFFFFF");
										SWFObjectContainer1.addParam("wmode", "opaque");
										SWFObjectContainer1.write("test-opens-chart-container");
									</script>
								</div>
							</div>
							<div class="col">
								<div style="background-color:#f2f2f2;border:1px solid #e1e1e1;">
									<div style="padding:9px 12px;">
										<span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '1340', false, '', false, false); ?></span>: <span class="data"><?php echo $CampaignInformation['SplitTest']['TestSize']; ?>%</span>
										<?php if ($CampaignInformation['SplitTest']['RelWinnerEmailID'] != 0): ?>
											<br /><span class="data-label" style="margin:0px;"><?php InterfaceLanguage('Screen', '1348', false, '', false, false); ?>:</span> <span class="data"><?php echo $WinnerEmail; ?></span>
										<?php endif; ?>
									</div>
								</div>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid" style="margin-top:18px;">
									<tr>
										<th width="60%"><?php InterfaceLanguage('Screen', '0008', false, '', true); ?></th>
										<th style="text-align:center;padding:0px;" width="20%"><?php InterfaceLanguage('Screen', '0837', false, '', true); ?></th>
										<th style="text-align:center;padding:0px;" width="20%"><?php InterfaceLanguage('Screen', '0838', false, '', true); ?></th>
									</tr>
									<?php foreach ($SplitTestEmails as $index=>$EachTestEmail): ?>
										<tr>
											<td style="color:#<?php echo $ChartColors[$index] ?>;"><?php echo $EachTestEmail['EmailName']; ?></td>
											<td>
												<span class="data"><?php echo $EachTestEmail['UniqueOpens']; ?></span><br />
											</td>
											<td>
												<span class="data"><?php echo $EachTestEmail['UniqueClicks']; ?></span><br />
											</td>
										</tr>
									<?php endforeach ?>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-2">
						<li id="tab-most-clicked-links"><a href="#"><?php InterfaceLanguage('Screen', '0849', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-2" id="tab-content-most-clicked-links">
							<?php
							if (count($MostClickedLinks) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0872', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($MostClickedLinks as $Index=>$EachLink):
									?>
										<tr>
											<td width="80%"><?php InterfaceTextCut($EachLink['LinkURL'], 80, '...', false); ?></td>
											<td width="20%" class="right-align"><span class="data"><?php print(number_format($EachLink['TotalClicks'])); ?></span> <?php InterfaceLanguage('Screen', '0838', false, '', false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- Page - End -->

<script>
var APP_URL = '<?php InterfaceAppURL(); ?>';
var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
var CampaignID = <?php print($CampaignInformation['CampaignID']) ?>;
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/client/campaign_overview.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH.'desktop/layouts/client_footer.php'); ?>