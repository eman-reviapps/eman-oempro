<div tabcollection="form-tabs" id="tab-content-5">
	<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1762', false); ?></h3>
	<div class="form-row no-bg">
		<p>Custom email headers help all emails delivered from Oempro to your MTA to be categorized and prioritized easily.</p>
	</div>
	<div style="padding:0 18px;margin:18px 0 0 0;">
		<table class="small-grid" id="header-table">
			<tr>
				<th width="144"><?php InterfaceLanguage('Screen', '1765'); ?></th>
				<th width="145"><?php InterfaceLanguage('Screen', '1766'); ?></th>
				<th width="273"><?php InterfaceLanguage('Screen', '1767'); ?></th>
			</tr>
			<?php if ($EmailHeaders !== FALSE && count($EmailHeaders) > 0) { ?>
			<?php foreach ($EmailHeaders as $eachEmailHeader) { ?>
			<tr>
				<td width="144"><input type="checkbox" name="SelectedEmailHeaders" value="<?php echo $eachEmailHeader->getId(); ?>"><?php echo $eachEmailHeader->getName(); ?></td>
				<td width="145"><?php InterfaceLanguage('Screen', '1764', false, $eachEmailHeader->getEmailType()); ?></td>
				<td width="273"><?php echo $eachEmailHeader->getValue(); ?></td>
			</tr>
			<?php } ?>
			<?php } ?>
			<tr id="last-row">
				<td colspan="3" style="background-color:#fff;border-top:1px solid #B9C8D2;"><a href="#" id="removeEmailHeadersLink"><?php InterfaceLanguage('Screen', '1770'); ?></a></td>
			</tr>
		</table>
	</div>

	<h3 class="form-legend" id="addHeaderMessage"><?php InterfaceLanguage('Screen', '1768', false); ?></h3>

	<div class="form-row <?php print((form_error('HeaderName') != '' ? 'error' : '')); ?>" id="form-row-HeaderName">
		<label for="HeaderName"><?php InterfaceLanguage('Screen', '1765'); ?>:*</label>
		<input type="text" class="text" name="HeaderName" id="HeaderName" value="X-">
		<?php print(form_error('Header', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>

	<div class="form-row" id="form-row-EmailType">
		<label for="EmailType"><?php InterfaceLanguage('Screen', '1766'); ?>:*</label>
		<select name="EmailType" id="emailType" class="select">
			<option value="all"><?php InterfaceLanguage('Screen', '1764', false, 'all'); ?></option>
			<option value="campaign"><?php InterfaceLanguage('Screen', '1764', false, 'campaign'); ?></option>
			<option value="autoresponder"><?php InterfaceLanguage('Screen', '1764', false, 'autoresponder'); ?></option>
			<option value="transactional"><?php InterfaceLanguage('Screen', '1764', false, 'transactional'); ?></option>
		</select>
	</div>

	<div class="form-row <?php print((form_error('HeaderValue') != '' ? 'error' : '')); ?>" id="form-row-HeaderValue">
		<label for="HeaderValue"><?php InterfaceLanguage('Screen', '1767'); ?>:*</label>
		<input type="text" name="HeaderValue" value="" id="HeaderValue" class="text personalized" style="width:500px;" />
		<div class="form-row-note personalization" id="personalization-HeaderValue">
			<p>
				<strong><?php InterfaceLanguage('Screen', '1763', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
				<select name="personalization-select-HeaderValue" id="personalization-select-HeaderValue" class="personalization-select">
					<option value=""><?php InterfaceLanguage('Screen', '1763', false, '', false, false); ?></option>
				</select>&nbsp;&nbsp;
			</p>
		</div>
		<?php print(form_error('Subject', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>

	<div class="form-row no-bg" style="margin-bottom:18px;">
		<a style="float:left;" class="button" id="ButtonAddHeader"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1769', false, '', true); ?></strong></a>
	</div>
</div>

<script type="text/javascript">
	var languageObject = {
		'0037' : "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '0037', true), ENT_QUOTES); ?>",
		'1764' : {
			'all' : "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1764', true, 'all'), ENT_QUOTES); ?>",
			'campaign' : "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1764', true, 'campaign'), ENT_QUOTES); ?>",
			'autoresponder' : "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1764', true, 'autoresponder'), ENT_QUOTES); ?>",
			'transactional' : "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1764', true, 'transactional'), ENT_QUOTES); ?>"
		}
	};
	var CONTROLLER_URL = "<?php InterfaceAppURL(); ?>/admin/emaildelivery/";
	var values = [
		{'value':'%User:UserID%', 'label':'User id'},
		{'value':'%User:EmailAddress%', 'label':'User email address'},
		{'value':'%UserGroup:UserGroupID%', 'label':'User group id'},
		{'value':'%Campaign:CampaignID%', 'label':'Campaign id'},
		{'value':'%AutoResponder:AutoResponderID%', 'label':'Auto responder id'},
		{'value':'%AutoResponder:TriggerType%', 'label':'Trigger type'},
		{'value':'%List:ListID%', 'label':'List id'},
		{'value':'%Subscriber:SubscriberID%', 'label':'Subscriber id'},
		{'value':'%Subscriber:EmailAddress%', 'label':'Subscriber email address'},
		{'value':'%Subscriber:SubscriptionIP%', 'label':'Subscriber subscription IP address'},
		{'value':'%Subscriber:SubscriptionDate%', 'label':'Subscriber subscription date'}
	];
	var valuePresets = {
		'all'			: [0,1,2],
		'campaign'		: [0,1,2,3,6,7,8,9,10],
		'autoresponder'	: [0,1,2,4,5,6,7,8,9,10],
		'transactional'	: [0,1,2]
	};
</script>