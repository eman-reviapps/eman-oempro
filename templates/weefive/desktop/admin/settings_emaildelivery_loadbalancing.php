<div tabcollection="form-tabs" id="tab-content-2">
	<?php if (isset($PageErrorMessage) == true): ?>
		<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
	<?php elseif (isset($PageSuccessMessage) == true): ?>
		<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
	<?php elseif (validation_errors()): ?>
		<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
	<?php else: ?>
		<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
	<?php endif; ?>

	<div class="form-row <?php print((form_error('LOAD_BALANCE_STATUS') != '' ? 'error' : '')); ?>" id="form-row-LOAD_BALANCE_STATUS">
		<label for="LOAD_BALANCE_STATUS"><?php InterfaceLanguage('Screen', '0117', false); ?>: *</label>
		<select name="LOAD_BALANCE_STATUS" id="LOAD_BALANCE_STATUS" class="select">
			<option value="true" <?php echo set_select('LOAD_BALANCE_STATUS', 'true', (LOAD_BALANCE_STATUS == true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0049', false, '', false); ?></option>
			<option value="false" <?php echo set_select('LOAD_BALANCE_STATUS', 'false', (LOAD_BALANCE_STATUS == false ? true : false)); ?>><?php InterfaceLanguage('Screen', '0050', false, '', false); ?></option>
		</select>
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0394', false, '', false); ?></p></div>
		<?php print(form_error('LOAD_BALANCE_STATUS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div id="sub-loadbalance-settings">
		<div class="form-row <?php print((form_error('LOAD_BALANCE_EMAILS') != '' ? 'error' : '')); ?>" id="form-row-LOAD_BALANCE_EMAILS">
			<label for="LOAD_BALANCE_EMAILS"><?php InterfaceLanguage('Screen', '0395', false); ?>: *</label>
			<input type="text" name="LOAD_BALANCE_EMAILS" value="<?php echo set_value('LOAD_BALANCE_EMAILS', LOAD_BALANCE_EMAILS); ?>" id="LOAD_BALANCE_EMAILS" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0396', false, '', false); ?></p></div>
			<?php print(form_error('LOAD_BALANCE_EMAILS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('LOAD_BALANCE_SLEEP') != '' ? 'error' : '')); ?>" id="form-row-LOAD_BALANCE_SLEEP">
			<label for="LOAD_BALANCE_SLEEP"><?php InterfaceLanguage('Screen', '0397', false); ?>: *</label>
			<input type="text" name="LOAD_BALANCE_SLEEP" value="<?php echo set_value('LOAD_BALANCE_SLEEP', LOAD_BALANCE_SLEEP); ?>" id="LOAD_BALANCE_SLEEP" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0398', false, '', false); ?></p></div>
			<?php print(form_error('LOAD_BALANCE_SLEEP', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
</div>
