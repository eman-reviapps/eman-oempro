				<form id="EmailDeliverySettings" method="post" action="<?php InterfaceAppURL(); ?>/admin/emaildelivery/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<ul class="livetabs" tabcollection="form-tabs" tabcallback="emailDeliveryTabCallback">
											<li id="tab-1">
												<a href="#"><?php InterfaceLanguage('Screen', '0115', false, '', false); ?></a>
											</li>
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0368', false, '', false); ?></a>
											</li>
											<li id="tab-3">
												<a href="#"><?php InterfaceLanguage('Screen', '0367', false, '', false); ?></a>
											</li>
											<li id="tab-4">
												<a href="#"><?php InterfaceLanguage('Screen', '0370', false, '', false); ?></a>
											</li>
											<li id="tab-5">
												<a href="#"><?php InterfaceLanguage('Screen', '1761', false, '', false); ?></a>
											</li>
										</ul>
									</div>
									<div class="white" style="min-height:430px;">
										<!-- Email Delivery - START -->
										<?php include TEMPLATE_PATH.'desktop/admin/settings_emaildelivery_delivery.php'; ?>
										<!-- Email Delivery - END -->

										<!-- Load Balancing - START -->
										<?php include TEMPLATE_PATH.'desktop/admin/settings_emaildelivery_loadbalancing.php'; ?>
										<!-- Load Balancing - END -->

										<!-- Bounce Handling - START -->
										<?php include TEMPLATE_PATH.'desktop/admin/settings_emaildelivery_bounces.php'; ?>
										<!-- Bounce Handling - END -->

										<!-- SPAM Complaint Handling - START -->
										<?php include TEMPLATE_PATH.'desktop/admin/settings_emaildelivery_spamcomplaints.php'; ?>
										<!-- SPAM Complaint Handling - END -->

										<!-- Headers - START -->
										<?php include TEMPLATE_PATH.'desktop/admin/settings_emaildelivery_headers.php'; ?>
										<!-- Headers - END -->
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="EmailDeliverySettings" id="ButtonEmailDeliverySettings"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0366', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="UpdateEmailDeliverySettings" id="Command" />
						</div>
					</div>
				</form>

				<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/settings_emaildelivery.js" type="text/javascript" charset="utf-8"></script>		
				