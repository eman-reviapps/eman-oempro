	<div id="page-shadow">
		<div id="page">
			<div class="white" style="min-height:420px;">
				<form id="user-groups-table-form" action="<?php InterfaceAppURL(); ?>/admin/usergroups/" method="post">
					<input type="hidden" name="Command" value="DeleteUserGroups" id="Command">
					<div class="module-container">
						<?php
						if (isset($PageSuccessMessage) == true):
						?>
						<div class="page-message success">
							<div class="inner">
								<span class="close">X</span>
								<?php print($PageSuccessMessage); ?>
							</div>
						</div>
						<?php
						elseif (isset($PageErrorMessage) == true):									
						?>
						<div class="page-message error">
							<div class="inner">
								<span class="close">X</span>
								<?php print($PageErrorMessage); ?>
							</div>
						</div>
						<?php
						endif;
						?>
						<div class="grid-operations">
							<div class="inner">
								<span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
								<a href="#" class="grid-select-all" targetgrid="user-groups-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>, 
								<a href="#" class="grid-select-none" targetgrid="user-groups-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
								&nbsp;&nbsp;-&nbsp;&nbsp;
								<a href="#" class="main-action" targetform="user-groups-table-form"><?php InterfaceLanguage('Screen', '0042'); ?></a>
								&nbsp;|&nbsp;
								<a href="<?php InterfaceAppURL(); ?>/admin/usergroups/create/" class="main-action"><?php InterfaceLanguage('Screen', '0060'); ?></a>
							</div>
						</div>
					</div>
					<table border="0" cellspacing="0" cellpadding="0" class="grid" id="user-groups-table">
						<tr>
							<th width="100%" colspan="3"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
						</tr>
						<?php
						if ($UserGroups == false):
						?>
						<tr>
							<td colspan="3"><?php InterfaceLanguage('Screen', '0438'); ?></td>
						</tr>
						<?php
						endif;
						foreach ($UserGroups as $EachUserGroup):
						?>
							<tr>
								<td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedUserGroups[]" value="<?php print($EachUserGroup->UserGroupID); ?>" id="SelectedUserGroups<?php print($EachUserGroup->UserGroupID); ?>"></td>
								<td width="600" class="no-padding-left"><a href="<?php InterfaceAppURL(); ?>/admin/usergroups/edit/<?php print($EachUserGroup->UserGroupID); ?>"><?php print($EachUserGroup->GroupName); ?></a></td>
							</tr>
						<?php
						endforeach;
						?>
					</table>
				</form>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" charset="utf-8">
		var language_object = {
			screen : {
				'0587'	: '<?php InterfaceLanguage('Screen', '0587', false, '', false); ?>'
			}
		};
	</script>
	<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/settings_usergroups.js" type="text/javascript" charset="utf-8"></script>		
