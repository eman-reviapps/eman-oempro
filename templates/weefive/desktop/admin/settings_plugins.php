				<form id="PlugIns" method="post" action="<?php InterfaceAppURL(); ?>/admin/plugins/">
							<div id="page-shadow">
								<div id="page">
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
											unset($_SESSION['PluginMessage']);
										?>
										<div class="module-container-2">
											<div class="page-message error">
												<div class="inner">
													<span class="close">X</span>
													<?php print($PageErrorMessage); ?>
												</div>
											</div>
										</div>
										<?php 
										elseif (isset($PageSuccessMessage) == true):
											unset($_SESSION['PluginMessage']);
										?>
										<div class="module-container-2">
											<div class="page-message success">
												<div class="inner">
													<span class="close">X</span>
													<?php print($PageSuccessMessage); ?>
												</div>
											</div>
										</div>
										<?php
										elseif (validation_errors()):
											unset($_SESSION['PluginMessage']);
										?>
										<div class="module-container-2">
											<div class="page-message error">
												<div class="inner">
													<span class="close">X</span>
													<?php InterfaceLanguage('Screen', '0275', false); ?>												
												</div>
											</div>
										</div>
										<?php 
										endif;
										if (count($PlugIns) == 0):
										?>
											<p class="no-data-message"><?php InterfaceLanguage('Screen', '0188', false, '', false); ?></p>
										<?php
										else:
										?>
												<table border="0" cellspacing="0" cellpadding="0" class="grid">
													<tr>
														<th width="80%"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?> / DESCRIPTION</th>
														<th width="20%">&nbsp;</th>
													</tr>
													<tr>
														<td width="80%">
															<strong><?php print(InterfaceLanguage('Screen', '1933')); ?></strong>
															<br><small><?php print(InterfaceLanguage('Screen', '1934')); ?></small>
														</td>
														<td width="20%">
															<a href="http://octeth.com/plugins/" target="_blank"><?php InterfaceLanguage('Screen', '1935', false, '', false); ?></a>
														</td>
													</tr>
												<?php foreach ($PlugIns as $Index=>$EachPlugIn): ?>
													<tr>
														<td width="80%">
															<strong><?php print($EachPlugIn['Name']); ?></strong>
															<?php
															if ($EachPlugIn['Status'] == false):
															?>
															<span class="label float-none"><?php InterfaceLanguage('Screen', '0012', false, 'Disabled'); ?></span>
															<?php
															endif;
															?>
															<br>
															<small><?php print($EachPlugIn['Description']); ?></small>
														</td>
														<td width="20%">
															<?php
															// Check if the minimum required version is higher than the current version - Start {
															if (version_compare(PRODUCT_VERSION, $EachPlugIn['MinOemproVersion']) == -1)
																{
																// Not compatible system version
																print(sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0189'], $EachPlugIn['MinOemproVersion']));
																}
															else
																{
																if ($EachPlugIn['Status'] == true):
															?>
																<a href="<?php InterfaceAppURL(); ?>/admin/plugins/disable/<?php print($EachPlugIn['Code']); ?>" class="plugin-disable-link"><?php InterfaceLanguage('Screen', '0187', false, '', false); ?></a>
															<?php
																else:
															?>
																<a href="<?php InterfaceAppURL(); ?>/admin/plugins/enable/<?php print($EachPlugIn['Code']); ?>"><?php InterfaceLanguage('Screen', '0186', false, '', false); ?></a>
															<?php
																endif;
																}
															// Check if the minimum required version is higher than the current version - End }
															?>
														</td>
													</tr>
													<?php endforeach; ?>
												</table>
										<?php
										endif;
										?>
									</div>
								</div>
							</div>
						
				</form>

				<script type="text/javascript">
				$(document).ready(function() {
					$('.plugin-disable-link').click(function() {
						return confirm('<?php InterfaceLanguage('Screen', '1938', false, '', false); ?>');
					});
				})
				</script>
