<div tabcollection="form-tabs" id="tab-content-4">
	<?php if (isset($PageErrorMessage) == true): ?>
		<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
	<?php elseif (isset($PageSuccessMessage) == true): ?>
		<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
	<?php elseif (validation_errors()): ?>
		<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
	<?php else: ?>
		<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
	<?php endif; ?>

	<div class="form-row <?php print((form_error('REPORT_ABUSE_EMAIL') != '' ? 'error' : '')); ?>" id="form-row-REPORT_ABUSE_EMAIL">
		<label for="REPORT_ABUSE_EMAIL"><?php InterfaceLanguage('Screen', '0412', false); ?>: *</label>
		<input type="text" name="REPORT_ABUSE_EMAIL" value="<?php echo set_value('REPORT_ABUSE_EMAIL', REPORT_ABUSE_EMAIL); ?>" id="REPORT_ABUSE_EMAIL" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0413', false, '', false); ?></p></div>
		<?php print(form_error('REPORT_ABUSE_EMAIL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('X_COMPLAINTS_TO') != '' ? 'error' : '')); ?>" id="form-row-X_COMPLAINTS_TO">
		<label for="X_COMPLAINTS_TO"><?php InterfaceLanguage('Screen', '0414', false); ?>: *</label>
		<input type="text" name="X_COMPLAINTS_TO" value="<?php echo set_value('X_COMPLAINTS_TO', X_COMPLAINTS_TO); ?>" id="X_COMPLAINTS_TO" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0415', false, '', false); ?></p></div>
		<?php print(form_error('X_COMPLAINTS_TO', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('FBL_INCOMING_EMAILADDRESS') != '' ? 'error' : '')); ?>" id="form-row-FBL_INCOMING_EMAILADDRESS">
		<label for="FBL_INCOMING_EMAILADDRESS"><?php InterfaceLanguage('Screen', '0416', false); ?>:</label>
		<input type="text" name="FBL_INCOMING_EMAILADDRESS" value="<?php echo set_value('FBL_INCOMING_EMAILADDRESS', FBL_INCOMING_EMAILADDRESS); ?>" id="FBL_INCOMING_EMAILADDRESS" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0417', false, '', false); ?></p></div>
		<?php print(form_error('FBL_INCOMING_EMAILADDRESS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('POP3_FBL_STATUS') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_STATUS">
		<label for="POP3_FBL_STATUS"><?php InterfaceLanguage('Screen', '0405', false); ?>: *</label>
		<select name="POP3_FBL_STATUS" id="POP3_FBL_STATUS" class="select">
			<option value="Enabled" <?php echo set_select('POP3_FBL_STATUS', 'Enabled', (POP3_FBL_STATUS == 'Enabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0406', false, '', false); ?></option>
			<option value="Disabled" <?php echo set_select('POP3_FBL_STATUS', 'Disabled', (POP3_FBL_STATUS == 'Disabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0407', false, '', false); ?></option>
		</select>
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0408', false, '', false); ?></p></div>
		<?php print(form_error('POP3_FBL_STATUS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div id="fbl-pop3-settings">
		<div class="form-row <?php print((form_error('POP3_FBL_HOST') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_HOST">
			<label for="POP3_FBL_HOST"><?php InterfaceLanguage('Screen', '0409', false); ?>: *</label>
			<input type="text" name="POP3_FBL_HOST" value="<?php echo set_value('POP3_FBL_HOST', POP3_FBL_HOST); ?>" id="POP3_FBL_HOST" class="text" />
			<?php print(form_error('POP3_FBL_HOST', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_FBL_PORT') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_PORT">
			<label for="POP3_FBL_PORT"><?php InterfaceLanguage('Screen', '0410', false); ?>: *</label>
			<input type="text" name="POP3_FBL_PORT" value="<?php echo set_value('POP3_FBL_PORT', POP3_FBL_PORT); ?>" id="POP3_FBL_PORT" class="text" />
			<?php print(form_error('POP3_FBL_PORT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_FBL_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_USERNAME">
			<label for="POP3_FBL_USERNAME"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
			<input type="text" name="POP3_FBL_USERNAME" value="<?php echo set_value('POP3_FBL_USERNAME', POP3_FBL_USERNAME); ?>" id="POP3_FBL_USERNAME" class="text" />
			<?php print(form_error('POP3_FBL_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_FBL_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_PASSWORD">
			<label for="POP3_FBL_PASSWORD"><?php InterfaceLanguage('Screen', '0003', false); ?>: *</label>
			<input type="password" name="POP3_FBL_PASSWORD" value="<?php echo set_value('POP3_FBL_PASSWORD', POP3_FBL_PASSWORD); ?>" id="POP3_FBL_PASSWORD" class="text" />
			<?php print(form_error('POP3_FBL_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_FBL_SSL') != '' ? 'error' : '')); ?>" id="form-row-POP3_FBL_SSL">
			<label for="POP3_FBL_SSL"><?php InterfaceLanguage('Screen', '1656', false); ?>: *</label>
			<div class="checkbox-container">
				<div class="checkbox-row">
					<input type="checkbox" name="POP3_FBL_SSL" value="Yes" id="POP3_FBL_SSL_Yes" <?php echo set_checkbox('POP3_FBL_SSL', 'Yes', (POP3_FBL_SSL == 'Yes' ? true : false)); ?>> <label for="POP3_FBL_SSL_Yes"><?php InterfaceLanguage('Screen', '1658', false, '', false); ?></label>
				</div>
			</div>
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1657', false, '', false); ?></p></div>
			<?php print(form_error('POP3_FBL_SSL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
</div>
