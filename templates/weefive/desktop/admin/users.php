		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>
		
		<!-- Section bar - Start -->
		<div class="container">
			<div class="span-23 last">
				<div id="section-bar">
					<div class="header">
						<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0011', false, '', false, true); ?></h1>
					</div>
					<div class="actions">
						<a class="button" href="<?php InterfaceAppURL(); ?>/admin/users/create/"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0013', false, '', true); ?></strong></a>
					</div>
				</div>
			</div>
		</div>
		<!-- Section bar - End -->

		<!-- Page - Start -->
		<div class="container">
			<div class="span-5 snap-5">
				<ul class="left-sub-navigation">
					<li class="header first with-module"><?php InterfaceLanguage('Screen', '0043', false, '', true); ?></li>
					<li <?php print(($FilterType == 'ByActivity' && $FilterData == 'All' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByActivity/All/"><?php InterfaceLanguage('Screen', '0172', false, '', false, true); ?><span class="item-count">(<?php print($TotalUsers); ?>)</span></a></li>
					<li <?php print(($FilterType == 'ByActivity' && $FilterData == 'OnlineUsers' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByActivity/OnlineUsers/"><?php InterfaceLanguage('Screen', '0045'); ?></a></li>
				</ul>
				<ul class="left-sub-navigation">
					<li class="header"><?php InterfaceLanguage('Screen', '0046', false, '', true); ?></li>
					<li <?php print(($FilterType == 'ByUserGroup' && $FilterData == 'AnyUserGroup' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByUserGroup/AnyUserGroup/"><?php InterfaceLanguage('Screen', '0047'); ?></a></li>
					<?php
					foreach ($UserGroups as $EachUserGroup):
					?>
						<li <?php print(($FilterType == 'ByUserGroup' && $FilterData == $EachUserGroup['UserGroupID'] ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByUserGroup/<?php print($EachUserGroup['UserGroupID']) ?>/"><?php InterfaceTextCut($EachUserGroup['GroupName'], 20, '...'); ?><span class="item-count">(<?php print($EachUserGroup['TotalUsers']); ?>)</span></a></li>
					<?php
					endforeach;
					?>
				</ul>
				<ul class="left-sub-navigation">
					<li class="header"><?php InterfaceLanguage('Screen', '0048', false, '', true); ?></li>
					<li <?php print(($FilterType == 'ByAccountStatus' && $FilterData == 'Enabled' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByAccountStatus/Enabled/"><?php InterfaceLanguage('Screen', '0049'); ?></a></li>
					<li <?php print(($FilterType == 'ByAccountStatus' && $FilterData == 'Disabled' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByAccountStatus/Disabled/"><?php InterfaceLanguage('Screen', '0050'); ?></a></li>
				</ul>
				<ul class="left-sub-navigation">
					<li class="header"><?php InterfaceLanguage('Screen', '1809', false, '', true); ?></li>
					<li <?php print(($FilterType == 'ByReputationLevel' && $FilterData == 'Trusted' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByReputationLevel/Trusted/"><?php InterfaceLanguage('Screen', '1796'); ?></a></li>
					<li <?php print(($FilterType == 'ByReputationLevel' && $FilterData == 'Untrusted' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/ByReputationLevel/Untrusted/"><?php InterfaceLanguage('Screen', '1797'); ?></a></li>
				</ul>
			</div>
			<div class="span-18 last">
				<div id="page-shadow">
					<div id="page">
						<div class="white" style="min-height:520px;">
							<form id="users-table-form" action="<?php InterfaceAppURL(); ?>/admin/users/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/" method="post">
								<div class="module-container">
									<?php
									if (isset($PageSuccessMessage) == true):
									?>
									<div class="page-message success">
										<div class="inner">
											<span class="close">X</span>
											<?php print($PageSuccessMessage); ?>
										</div>
									</div>
									<?php
									elseif (isset($PageErrorMessage) == true):									
									?>
									<div class="page-message error">
										<div class="inner">
											<span class="close">X</span>
											<?php print($PageErrorMessage); ?>
										</div>
									</div>
									<?php
									endif;
									?>
									<div class="grid-operations">
										<div class="inner">
											<span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
											<a href="#" class="grid-select-all" targetgrid="users-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>, 
											<a href="#" class="grid-select-none" targetgrid="users-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
											&nbsp;&nbsp;-&nbsp;&nbsp;
											<a href="#" class="main-action" targetform="users-table-form" id="delete-users"><?php InterfaceLanguage('Screen', '0042'); ?></a>
											<div class="main-action-with-menu" id="change-status-menu">
												<a href="#" class="main-action"><?php InterfaceLanguage('Screen', '1396'); ?></a>
												<div class="down-arrow"><span></span></div>
												<ul class="main-action-menu" style="width:120px;">
													<li><a href="#" statustype="Enabled"><?php InterfaceLanguage('Screen', '0049', false, '', false, false, array()); ?></a></li>
													<li><a href="#" statustype="Disabled"><?php InterfaceLanguage('Screen', '0050', false, '', false, false, array()); ?></a></li>
												</ul>
											</div>
											<input type="hidden" name="Command" value="DeleteUser" id="Command">
											<input type="hidden" name="Status" value="" id="Status">
										</div>
									</div>
								</div>
								<table border="0" cellspacing="0" cellpadding="0" class="grid" id="users-table">
									<tr>
										<th width="250" colspan="3"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
										<th width="250"><?php InterfaceLanguage('Screen', '0002', false, '', true); ?></th>
									</tr>
									<?php
									if ($Users == false):
									?>
									<tr>
										<td colspan="3"><?php InterfaceLanguage('Screen', '0075'); ?></td>
									</tr>
									<?php
									endif;
									foreach ($Users as $EachUser):
									?>
										<tr>
											<td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedUsers[]" value="<?php print($EachUser->UserID); ?>" id="SelectedUsers<?php print($EachUser->UserID); ?>"></td>
											<td width="200" class="no-padding-left">
												<?php if ($EachUser->AccountStatus == 'Disabled'): ?><span class="label"><?php InterfaceLanguage('Screen', '0012', false, $EachUser->AccountStatus); ?></span><?php endif; ?>
												<a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($EachUser->UserID); ?>"><?php print($EachUser->FirstName.' '.$EachUser->LastName); ?></a>
												<br><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($EachUser->UserID); ?>/Login" class="grid-row-action"><?php InterfaceLanguage('Screen', '1753'); ?></a>
												<a style="margin-left:9px;" href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($EachUser->UserID); ?>/Edit" class="grid-row-action"><?php InterfaceLanguage('Screen', '0508'); ?></a>
											</td>
											<td width="35" class="no-padding-left" style="text-align:left"><img src="<?php InterfaceAppURL(); ?>/admin/users/activity_spark/<?php print(implode('x', $UserStatistics[$EachUser->UserID])); ?>" /></td>
											<td width="250"><?php print($EachUser->Username); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php if ($TotalPages > 1): ?>
			<div class="span-1 last">
				<ul class="pagination with-module">
					<?php
					if ($CurrentPage > 1):
					?>
					<li class="first"><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/1/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
					<?php
					endif;
					?>
					<?php
						$CounterStart	= ($CurrentPage < 3 ? 1 : $CurrentPage - 2);
						$CounterFinish	= ($CurrentPage > ($TotalPages - 2) ? $TotalPages : $CurrentPage + 2);
					for ($PageCounter = $CounterStart; $PageCounter <= $CounterFinish; $PageCounter++):
					?>
					<li <?php print(($CurrentPage == $PageCounter ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/<?php print($PageCounter); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/"><?php print(number_format($PageCounter)); ?></a></li>
					<?php
					endfor;
					?>
					<?php
					if ($CurrentPage < $TotalPages):
					?>
					<li class="last"><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/<?php print($TotalPages); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
					<?php
					endif;
					?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
		<!-- Page - End -->
		
		<script type="text/javascript" charset="utf-8">
			var language_object = {
				screen : {
					'0058'	: '<?php InterfaceLanguage('Screen', '0058', false, '', false); ?>'
				}
			};
		</script>
		<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/users.js" type="text/javascript" charset="utf-8"></script>		
		
		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>