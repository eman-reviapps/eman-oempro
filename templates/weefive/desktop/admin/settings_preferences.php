				<form id="Preferences" method="post" action="<?php InterfaceAppURL(); ?>/admin/preferences/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<h2><?php InterfaceLanguage('Screen', '0135', false, '', true); ?></h2>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>
										<div class="form-row <?php print((form_error('DEFAULT_LANGUAGE') != '' ? 'error' : '')); ?>" id="form-row-DEFAULT_LANGUAGE">
											<label for="DEFAULT_LANGUAGE"><?php InterfaceLanguage('Screen', '0421', false); ?>:</label>
											<select name="DEFAULT_LANGUAGE" id="DEFAULT_LANGUAGE" class="select">
												<?php
												foreach ($Languages as $EachLanguage):
												?>
												<option value="<?php print($EachLanguage['Code']); ?>" <?php echo set_select('DEFAULT_LANGUAGE', $EachLanguage['Code'], (DEFAULT_LANGUAGE == $EachLanguage['Code'] ? true : false)); ?>><?php print($EachLanguage['Name']); ?></option>
												<?php
												endforeach;
												?>
											</select>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0422', false, '', false); ?></p></div>
											<?php print(form_error('DEFAULT_LANGUAGE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('ADMIN_CAPTCHA') != '' ? 'error' : '')); ?>" id="form-row-ADMIN_CAPTCHA">
											<label for="ADMIN_CAPTCHA"><?php InterfaceLanguage('Screen', '0427', false); ?>:</label>
											<select name="ADMIN_CAPTCHA" id="ADMIN_CAPTCHA" class="select">
												<option value="true" <?php echo set_select('ADMIN_CAPTCHA', 'true', (ADMIN_CAPTCHA == true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0049', false, '', false); ?></option>
												<option value="false" <?php echo set_select('ADMIN_CAPTCHA', 'false', (ADMIN_CAPTCHA == false ? true : false)); ?>><?php InterfaceLanguage('Screen', '0050', false, '', false); ?></option>
											</select>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0428', false, '', false); ?></p></div>
											<?php print(form_error('ADMIN_CAPTCHA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('USER_CAPTCHA') != '' ? 'error' : '')); ?>" id="form-row-USER_CAPTCHA">
											<label for="USER_CAPTCHA"><?php InterfaceLanguage('Screen', '0430', false); ?>:</label>
											<select name="USER_CAPTCHA" id="USER_CAPTCHA" class="select">
												<option value="true" <?php echo set_select('USER_CAPTCHA', 'true', (USER_CAPTCHA == true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0049', false, '', false); ?></option>
												<option value="false" <?php echo set_select('USER_CAPTCHA', 'false', (USER_CAPTCHA == false ? true : false)); ?>><?php InterfaceLanguage('Screen', '0050', false, '', false); ?></option>
											</select>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0429', false, '', false); ?></p></div>
											<?php print(form_error('USER_CAPTCHA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('RUN_CRON_IN_USER_AREA') != '' ? 'error' : '')); ?>" id="form-row-RUN_CRON_IN_USER_AREA">
											<label for="RUN_CRON_IN_USER_AREA"><?php InterfaceLanguage('Screen', '1480', false); ?>:</label>
											<select name="RUN_CRON_IN_USER_AREA" id="RUN_CRON_IN_USER_AREA" class="select">
												<option value="true" <?php echo set_select('RUN_CRON_IN_USER_AREA', 'true', (RUN_CRON_IN_USER_AREA == true ? true : false)); ?>><?php InterfaceLanguage('Screen', '1481', false, '', false); ?></option>
												<option value="false" <?php echo set_select('RUN_CRON_IN_USER_AREA', 'false', (RUN_CRON_IN_USER_AREA == false ? true : false)); ?>><?php InterfaceLanguage('Screen', '1482', false, '', false); ?></option>
											</select>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1483', false, '', false); ?></p></div>
											<?php print(form_error('RUN_CRON_IN_USER_AREA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										
										<div class="form-row" id="form-row-FORBIDDEN_FROM_ADDRESSES">
											<label for="FORBIDDEN_FROM_ADDRESSES"><?php InterfaceLanguage('Screen', '1618', false, '', false, true); ?>:</label>
											<textarea name="FORBIDDEN_FROM_ADDRESSES" id="FORBIDDEN_FROM_ADDRESSES" class="textarea"><?php echo set_value('FORBIDDEN_FROM_ADDRESSES', FORBIDDEN_FROM_ADDRESSES); ?></textarea>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1619', false, '', false); ?></p></div>
											<?php print(form_error('FORBIDDEN_FROM_ADDRESSES', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row" id="form-row-USERAREA_FOOTER">
											<label for="USERAREA_FOOTER"><?php InterfaceLanguage('Screen', '1621', false, '', false, true); ?>:</label>
											<textarea name="USERAREA_FOOTER" id="USERAREA_FOOTER" class="textarea"><?php echo set_value('USERAREA_FOOTER', USERAREA_FOOTER); ?></textarea>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1622', false, '', false); ?></p></div>
											<?php print(form_error('USERAREA_FOOTER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="Preferences" id="ButtonPreferences"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0420', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="UpdatePreferences" id="Command" />
						</div>
					</div>
				</form>
