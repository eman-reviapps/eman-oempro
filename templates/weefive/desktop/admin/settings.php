<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<!-- Section bar - Start -->
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container" style="margin-top:18px;">
	<div class="span-5 snap-5">
		<ul class="left-sub-navigation">
			<li class="first with-module <?php print($SubSection == 'Account' ? 'selected' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/admin/account/"><?php InterfaceLanguage('Screen', '0127', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'DefaultOptInEmail' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/defaultoptinemail/"><?php InterfaceLanguage('Screen', '0128', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'EmailDelivery' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/emaildelivery/"><?php InterfaceLanguage('Screen', '0115', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'EmailRequests' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/emailrequests/"><?php InterfaceLanguage('Screen', '0126', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'EmailTemplates' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/"><?php InterfaceLanguage('Screen', '0111', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'ESPSettings' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/espsettings/"><?php InterfaceLanguage('Screen', '0120', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'Integration' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/integration/"><?php InterfaceLanguage('Screen', '0132', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'Plugins' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/plugins/"><?php InterfaceLanguage('Screen', '0131', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'Preferences' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/preferences/"><?php InterfaceLanguage('Screen', '0135', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'Rebranding' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/rebranding/"><?php InterfaceLanguage('Screen', '0124', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'UploadsAndMediaLibrary' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/uploadsandmedialibrary/"><?php InterfaceLanguage('Screen', '0125', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'UserGroups' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/usergroups/"><?php InterfaceLanguage('Screen', '0112', false, '', false); ?></a></li>
			<?php 
			InterfacePluginMenuHook('Admin.Settings', 
									$SubSection, 
									'<li><a href="_LINK_">_TITLE_</a></li>', 
									'<li class="selected"><a href="_LINK_">_TITLE_</a></li>'
									); 
			?>
			<li <?php print($SubSection == 'License' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/license/"><?php InterfaceLanguage('Screen', '1825', false, '', false); ?></a></li>
			<li <?php print($SubSection == 'About' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/about/"><?php InterfaceLanguage('Screen', '0134', false, '', false); ?></a></li>
		</ul>
	</div>
	<div class="span-18 last">
		<?php
		if ($SubSection == 'About')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_about.php');
			}
		elseif ($SubSection == 'Account')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_account.php');
			}
		elseif ($SubSection == 'DefaultOptInEmail')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_defaultoptinemail.php');
			}
		elseif ($SubSection == 'Plugins')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_plugins.php');
			}
		elseif ($SubSection == 'Rebranding')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_rebranding.php');
			}
		elseif ($SubSection == 'Integration')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_integration.php');
			}
		elseif ($SubSection == 'UploadsAndMediaLibrary')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_uploadsandmedialibrary.php');
			}
		elseif ($SubSection == 'ESPSettings')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_espsettings.php');
			}
		elseif ($SubSection == 'EmailDelivery')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_emaildelivery.php');
			}
		elseif ($SubSection == 'EmailRequests')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_emailrequests.php');
			}
		elseif ($SubSection == 'Preferences')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_preferences.php');
			}
		elseif ($SubSection == 'EmailTemplates')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_email_templates.php');
			}
		elseif ($SubSection == 'UserGroups')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_usergroups.php');
			}
		elseif ($SubSection == 'License')
			{
			include_once(TEMPLATE_PATH.'desktop/admin/settings_license.php');
			}
		elseif (($SubSection != '') && (isset($PluginView) == true && $PluginView != ''))
			{
			include_once($PluginView);
			}
		?>
	</div>
</div>
<!-- Page - End -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
