				<form id="DefaultOptInEmail" method="post" action="<?php InterfaceAppURL(); ?>/admin/defaultoptinemail/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<h2><?php InterfaceLanguage('Screen', '0128', false, '', true); ?></h2>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>
										<div class="form-row no-bg">
											<p><?php InterfaceLanguage('Screen', '0185', false, '', false); ?></p>
										</div>
										<div class="form-row <?php print((form_error('DEFAULT_OPTIN_EMAIL_SUBJECT') != '' ? 'error' : '')); ?>" id="form-row-DEFAULT_OPTIN_EMAIL_SUBJECT">
											<label for="DEFAULT_OPTIN_EMAIL_SUBJECT"><?php InterfaceLanguage('Screen', '0183', false); ?>: *</label>
											<input type="text" name="DEFAULT_OPTIN_EMAIL_SUBJECT" value="<?php echo set_value('DEFAULT_OPTIN_EMAIL_SUBJECT', DEFAULT_OPTIN_EMAIL_SUBJECT); ?>" id="DEFAULT_OPTIN_EMAIL_SUBJECT" class="text personalized" style="width:500px;" />
											<div class="form-row-note personalization" id="personalization-DEFAULT_OPTIN_EMAIL_SUBJECT">
												<p>
													<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
													<select name="personalization-select-DEFAULT_OPTIN_EMAIL_SUBJECT" id="personalization-select-DEFAULT_OPTIN_EMAIL_SUBJECT" class="personalization-select">
														<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
														<?php foreach($Tags as $Label => $TagGroup): ?>
															<optgroup label="<?php print($Label); ?>">
																<?php foreach($TagGroup as $Tag => $Label): ?>
																	<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
																<?php endforeach; ?>
															</optgroup>
														<?php endforeach; ?>
													</select>&nbsp;&nbsp;
												</p>
											</div>
											<?php print(form_error('DEFAULT_OPTIN_EMAIL_SUBJECT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('DEFAULT_OPTIN_EMAIL_BODY') != '' ? 'error' : '')); ?>" id="form-row-DEFAULT_OPTIN_EMAIL_BODY">
											<label for="DEFAULT_OPTIN_EMAIL_BODY"><?php InterfaceLanguage('Screen', '0184', false); ?>: *</label>
											<textarea name="DEFAULT_OPTIN_EMAIL_BODY" id="DEFAULT_OPTIN_EMAIL_BODY" class="textarea personalized plain-text" style="height:300px; width:500px;"><?php echo set_value('DEFAULT_OPTIN_EMAIL_BODY', DEFAULT_OPTIN_EMAIL_BODY); ?></textarea>
											<div class="form-row-note personalization" id="personalization-DEFAULT_OPTIN_EMAIL_BODY">
												<p>
													<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
													<select name="personalization-select-DEFAULT_OPTIN_EMAIL_BODY" id="personalization-select-DEFAULT_OPTIN_EMAIL_BODY" class="personalization-select">
														<option value="asdf"><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
														<?php foreach($Tags as $Label => $TagGroup): ?>
															<optgroup label="<?php print($Label); ?>">
																<?php foreach($TagGroup as $Tag => $Label): ?>
																	<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
																<?php endforeach; ?>
															</optgroup>
														<?php endforeach; ?>
													</select>&nbsp;&nbsp;
												</p>
											</div>
											<?php print(form_error('DEFAULT_OPTIN_EMAIL_BODY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="DefaultOptInEmail" id="ButtonDefaultOptInEmail"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0269', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditDefaultOptInEmail" id="Command" />
						</div>
					</div>
				</form>
