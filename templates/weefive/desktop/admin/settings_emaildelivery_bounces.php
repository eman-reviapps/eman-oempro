<div tabcollection="form-tabs" id="tab-content-3">
	<?php if (isset($PageErrorMessage) == true): ?>
		<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
	<?php elseif (isset($PageSuccessMessage) == true): ?>
		<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
	<?php elseif (validation_errors()): ?>
		<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
	<?php else: ?>
		<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
	<?php endif; ?>

	<div class="form-row <?php print((form_error('BOUNCE_CATCHALL_DOMAIN') != '' ? 'error' : '')); ?>" id="form-row-BOUNCE_CATCHALL_DOMAIN">
		<label for="BOUNCE_CATCHALL_DOMAIN"><?php InterfaceLanguage('Screen', '0399', false); ?>: *</label>
		<input type="text" name="BOUNCE_CATCHALL_DOMAIN" value="<?php echo set_value('BOUNCE_CATCHALL_DOMAIN', BOUNCE_CATCHALL_DOMAIN); ?>" id="BOUNCE_CATCHALL_DOMAIN" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0400', false, '', false); ?></p></div>
		<?php print(form_error('BOUNCE_CATCHALL_DOMAIN', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('THRESHOLD_SOFT_BOUNCE_DETECTION') != '' ? 'error' : '')); ?>" id="form-row-THRESHOLD_SOFT_BOUNCE_DETECTION">
		<label for="THRESHOLD_SOFT_BOUNCE_DETECTION"><?php InterfaceLanguage('Screen', '0401', false); ?>: *</label>
		<input type="text" name="THRESHOLD_SOFT_BOUNCE_DETECTION" value="<?php echo set_value('THRESHOLD_SOFT_BOUNCE_DETECTION', THRESHOLD_SOFT_BOUNCE_DETECTION); ?>" id="THRESHOLD_SOFT_BOUNCE_DETECTION" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0402', false, '', false); ?></p></div>
		<?php print(form_error('THRESHOLD_SOFT_BOUNCE_DETECTION', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('SEND_BOUNCE_NOTIFICATIONEMAIL') != '' ? 'error' : '')); ?>" id="form-row-SEND_BOUNCE_NOTIFICATIONEMAIL">
		<label for="SEND_BOUNCE_NOTIFICATIONEMAIL"><?php InterfaceLanguage('Screen', '0403', false); ?>:</label>
		<div class="checkbox-container">
			<div class="checkbox-row">
				<input type="checkbox" name="SEND_BOUNCE_NOTIFICATIONEMAIL" value="Enabled" id="SEND_BOUNCE_NOTIFICATIONEMAIL" <?php echo set_checkbox('SEND_BOUNCE_NOTIFICATIONEMAIL', 'Enabled', (SEND_BOUNCE_NOTIFICATIONEMAIL == 'Enabled' ? true : false)); ?>> <label for="SEND_BOUNCE_NOTIFICATIONEMAIL"><?php InterfaceLanguage('Screen', '0404', false, '', false); ?></label>
			</div>
		</div>
		<?php print(form_error('SEND_BOUNCE_NOTIFICATIONEMAIL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('POP3_BOUNCE_STATUS') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_STATUS">
		<label for="POP3_BOUNCE_STATUS"><?php InterfaceLanguage('Screen', '0405', false); ?>: *</label>
		<select name="POP3_BOUNCE_STATUS" id="POP3_BOUNCE_STATUS" class="select">
			<option value="Enabled" <?php echo set_select('POP3_BOUNCE_STATUS', 'Enabled', (POP3_BOUNCE_STATUS == 'Enabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0406', false, '', false); ?></option>
			<option value="Disabled" <?php echo set_select('POP3_BOUNCE_STATUS', 'Disabled', (POP3_BOUNCE_STATUS == 'Disabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0407', false, '', false); ?></option>
		</select>
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0408', false, '', false); ?></p></div>
		<?php print(form_error('POP3_BOUNCE_STATUS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div id="bounce-pop3-settings">
		<div class="form-row <?php print((form_error('POP3_BOUNCE_HOST') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_HOST">
			<label for="POP3_BOUNCE_HOST"><?php InterfaceLanguage('Screen', '0409', false); ?>: *</label>
			<input type="text" name="POP3_BOUNCE_HOST" value="<?php echo set_value('POP3_BOUNCE_HOST', POP3_BOUNCE_HOST); ?>" id="POP3_BOUNCE_HOST" class="text" />
			<?php print(form_error('POP3_BOUNCE_HOST', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_BOUNCE_PORT') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_PORT">
			<label for="POP3_BOUNCE_PORT"><?php InterfaceLanguage('Screen', '0410', false); ?>: *</label>
			<input type="text" name="POP3_BOUNCE_PORT" value="<?php echo set_value('POP3_BOUNCE_PORT', POP3_BOUNCE_PORT); ?>" id="POP3_BOUNCE_PORT" class="text" />
			<?php print(form_error('POP3_BOUNCE_PORT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_BOUNCE_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_USERNAME">
			<label for="POP3_BOUNCE_USERNAME"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
			<input type="text" name="POP3_BOUNCE_USERNAME" value="<?php echo set_value('POP3_BOUNCE_USERNAME', POP3_BOUNCE_USERNAME); ?>" id="POP3_BOUNCE_USERNAME" class="text" />
			<?php print(form_error('POP3_BOUNCE_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_BOUNCE_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_PASSWORD">
			<label for="POP3_BOUNCE_PASSWORD"><?php InterfaceLanguage('Screen', '0003', false); ?>: *</label>
			<input type="password" name="POP3_BOUNCE_PASSWORD" value="<?php echo set_value('POP3_BOUNCE_PASSWORD', POP3_BOUNCE_PASSWORD); ?>" id="POP3_BOUNCE_PASSWORD" class="text" />
			<?php print(form_error('POP3_BOUNCE_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('POP3_BOUNCE_SSL') != '' ? 'error' : '')); ?>" id="form-row-POP3_BOUNCE_SSL">
			<label for="POP3_BOUNCE_SSL"><?php InterfaceLanguage('Screen', '1656', false); ?>: *</label>
			<div class="checkbox-container">
				<div class="checkbox-row">
					<input type="checkbox" name="POP3_BOUNCE_SSL" value="Yes" id="POP3_BOUNCE_SSL_Yes" <?php echo set_checkbox('POP3_BOUNCE_SSL', 'Yes', (POP3_BOUNCE_SSL == 'Yes' ? true : false)); ?>> <label for="POP3_BOUNCE_SSL_Yes"><?php InterfaceLanguage('Screen', '1658', false, '', false); ?></label>
				</div>
			</div>
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1657', false, '', false); ?></p></div>
			<?php print(form_error('POP3_BOUNCE_SSL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
</div>
