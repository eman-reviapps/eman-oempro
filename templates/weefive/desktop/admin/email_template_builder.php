<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_email_builder_header.php'); ?>

<div id="top" class="iguana">
	<div class="container">
		<div class="span-14">
			<ul class="tabs">
				<li class="label"><?php InterfaceLanguage('Screen', '0501', false, '', false); ?></li>
				<li class="selected">
					<a href="#">
						<span class="left">&nbsp;</span>
						<span class="right">&nbsp;</span>
						<strong><?php print $TemplateInformation['TemplateName']; ?></strong>
					</a>
				</li>
			</ul>
		</div>
		<div class="span-10 last iguana-actions">
			<a id="iguana-builder-save-button" class="button"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
			<a id="iguana-builder-test-drive-button" class="button"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0305', false, '', true); ?></strong></a>
			<a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/edit/<?php print $TemplateInformation['TemplateID']; ?>" class="text-action"><?php InterfaceLanguage('Screen', '0503', false); ?></a>
		</div>
	</div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
	<div class="container">
		<div class="span-24 last">
			<iframe name="email-template-source" id="email-template-source" width="100%" height="550" marginwidth="0" marginheight="0" frameborder="0"></iframe>
		</div>
	</div>
</div>

<div class="panel-container builder">
	<div class="panel-inner">
		<div id="element-panel" class="element-panel"></div>
		<div class="properties-panel">
			<div id="standard-properties" class="clearfix">
				<div style="float:left;width:220px;">
					<p style="margin:0px;"><label><input type="checkbox" name="iguana-builder-property-editable" value="" id="iguana-builder-property-editable" /> <?php InterfaceLanguage('Screen', '0306', false); ?></label></p>
					<p style="margin:0px;padding-left:18px;">
						<label><input type="radio" name="iguana-builder-property-editable-type" value="single" id="iguana-builder-property-editable-type-single" /> <?php InterfaceLanguage('Screen', '0307', false); ?></label><br />
						<label><input type="radio" name="iguana-builder-property-editable-type" value="plain" id="iguana-builder-property-editable-type-plain" /> <?php InterfaceLanguage('Screen', '0308', false); ?></label><br />
						<label><input type="radio" name="iguana-builder-property-editable-type" value="rich" id="iguana-builder-property-editable-type-rich" /> <?php InterfaceLanguage('Screen', '0309', false); ?></label>
					</p>
					<p style="margin:0px;"><label><input type="checkbox" name="iguana-builder-property-duplicatable" value="" id="iguana-builder-property-duplicatable" /> <?php InterfaceLanguage('Screen', '0310', false); ?></label></p>
				</div>
				<div id="message" style="float:left;width:220px;margin-right:18px;display:none">
					<h4>&nbsp;</h4>
					<p style="color:#C73E00;"><?php InterfaceLanguage('Screen', '1423'); ?></p>
				</div>
				<div style="float:left;width:220px;margin-right:18px;">
					<h4><?php InterfaceLanguage('Screen', '0698', false, '', false, true); ?></h4>
					<p>
						<?php InterfaceLanguage('Screen', '0699', false, '', false, false); ?><br />
						<a href="#" id="properties-panel-sync-button" class="panel-button"><?php InterfaceLanguage('Screen', '0695', false, '', false, false); ?></a>
						<a href="#" id="properties-panel-sync-remove-button" class="panel-button"><?php InterfaceLanguage('Screen', '0707', false, '', false, false); ?></a>
					</p>
					<p><a href="#" class="panel-button"><?php InterfaceLanguage('Screen', '0696', false, '', false, false); ?></a></p>
				</div>
				<div style="float:left;width:220px;">
					<h4><?php InterfaceLanguage('Screen', '0697', false, '', false, true); ?></h4>
					<p>
						<?php InterfaceLanguage('Screen', '0700', false, '', false, false); ?><br />
						<a href="#" id="properties-panel-link-button" class="panel-button"><?php InterfaceLanguage('Screen', '0696', false, '', false, false); ?></a>
						<a href="#" id="properties-panel-link-remove-button" class="panel-button"><?php InterfaceLanguage('Screen', '0710', false, '', false, false); ?></a>
					</p>
				</div>
			</div>
			<div id="sync-properties">
				<h4><?php InterfaceLanguage('Screen', '0698', false, '', false, true); ?></h4>
				<p><?php InterfaceLanguage('Screen', '0704', false, '', false, false); ?></p>
				<p style="padding-top:9px;"><a href="#" id="iguana-builder-confirm-sync-button" class="panel-button"><?php InterfaceLanguage('Screen', '0703', false, '', false, false); ?></a> &nbsp;&nbsp;&nbsp;<a href="#" id="iguana-builder-cancel-sync-link"><?php InterfaceLanguage('Screen', '0705', false, '', false, false); ?></a></p>
			</div>
			<div id="link-properties">
				<h4><?php InterfaceLanguage('Screen', '0697', false, '', false, true); ?></h4>
				<p><?php InterfaceLanguage('Screen', '0708', false, '', false, false); ?></p>
				<p style="padding-top:9px;"><a href="#" id="iguana-builder-confirm-link-button" class="panel-button"><?php InterfaceLanguage('Screen', '0702', false, '', false, false); ?></a> &nbsp;&nbsp;&nbsp;<a href="#" id="iguana-builder-cancel-link-link"><?php InterfaceLanguage('Screen', '0705', false, '', false, false); ?></a></p>
			</div>
		</div>
	</div>
</div>

<form id="test-drive-form" action="<?php InterfaceAppURL(); ?>/admin/emailtemplates/builder/" method="post">
	<textarea id="frame-html-container-for-post" name="TemplateHTMLContent" style="display:none"></textarea>
</form>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_email_builder_footer.php'); ?>
