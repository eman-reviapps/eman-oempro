		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

		<!-- Section bar - Start -->
		<div class="container">
			<div class="span-23 last">
				<div id="section-bar">
					<div class="header">
						<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0013', false, '', false, true); ?></h1>
					</div>
				</div>
			</div>
		</div>
		<!-- Section bar - End -->

		<!-- Page - Start -->
		<div class="container">
			<form id="UserCreateForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/users/create/">
				<div class="span-18">
					<div id="page-shadow">
						<div id="page">
							<div class="page-bar">
								<div class="actions">
									<a href="<?php InterfaceAppURL(); ?>/admin/users/"><?php InterfaceLanguage('Screen', '0059', false, '', false); ?></a>
								</div>
								<ul class="livetabs" tabcollection="form-tabs">
									<li id="tab-1">
										<a href="#"><?php InterfaceLanguage('Screen', '0034', false, '', false); ?></a>
									</li>
									<li id="tab-2">
										<a href="#"><?php InterfaceLanguage('Screen', '0035', false, '', false); ?></a>
									</li>
								</ul>
							</div>
							<div class="white">
								<?php
								if (isset($PageErrorMessage) == true):
								?>
									<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
								<?php
								elseif (isset($PageSuccessMessage) == true):
								?>
									<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
								<?php
								elseif (validation_errors()):
								?>
								<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
								<?php
								else :
								?>
								<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
								<?php
								endif;
								?>
								<!-- Account Information Section - START -->
								<div tabcollection="form-tabs" id="tab-content-1">
									<div class="form-row <?php print((form_error('FirstName') != '' ? 'error' : '')); ?>" id="form-row-FirstName">
										<label for="FirstName"><?php InterfaceLanguage('Screen', '0021', false); ?>: *</label>
										<input type="text" name="FirstName" value="<?php echo set_value('FirstName', ''); ?>" id="FirstName" class="text" />
										<?php print(form_error('FirstName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('LastName') != '' ? 'error' : '')); ?>" id="form-row-LastName">
										<label for="LastName"><?php InterfaceLanguage('Screen', '0022', false); ?>: *</label>
										<input type="text" name="LastName" value="<?php echo set_value('LastName', ''); ?>" id="LastName" class="text" />
										<?php print(form_error('LastName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Username') != '' ? 'error' : '')); ?>" id="form-row-Username">
										<label for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
										<input type="text" name="Username" value="<?php echo set_value('Username', ''); ?>" id="Username" class="text" />
										<?php print(form_error('Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Password') != '' ? 'error' : '')); ?>" id="form-row-Password">
										<label for="Password"><?php InterfaceLanguage('Screen', '0003', false); ?>: *</label>
										<input type="password" name="Password" value="<?php echo set_value('Password', ''); ?>" id="Password" class="text" />
										<?php print(form_error('Password', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress">
										<label for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false); ?>: *</label>
										<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', ''); ?>" id="EmailAddress" class="text" />
										<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('TimeZone') != '' ? 'error' : '')); ?>" id="form-row-TimeZone">
										<label for="TimeZone"><?php InterfaceLanguage('Screen', '0016', false); ?>: *</label>
										<select name="TimeZone" id="TimeZone" class="select">
											<option value="" <?php echo set_select('TimeZone', '', true) ?>><?php InterfaceLanguage('Screen', '0017', false); ?></option>
											<?php
											foreach (Core::GetTimeZoneList() as $TimeZone):
											?>
												<option value="<?php print($TimeZone); ?>" <?php echo set_select('TimeZone', $TimeZone) ?>><?php print($TimeZone); ?></option>
											<?php
											endforeach;
											?>
										</select>
										<?php print(form_error('TimeZone', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										<div class="form-row-note">
											<p><?php InterfaceLanguage('Screen', '0033', false); ?></p>
										</div>
									</div>
									<div class="form-row <?php print((form_error('Language') != '' ? 'error' : '')); ?>" id="form-row-Language">
										<label for="Language"><?php InterfaceLanguage('Screen', '0018', false); ?>: *</label>
										<select name="Language" id="Language" class="select">
											<?php
											foreach (Core::DetectLanguages() as $EachLanguage):
											?>
												<option value="<?php print($EachLanguage['Code']); ?>" <?php echo set_select('Language', $EachLanguage['Code'], ((($this->input->post('Command') != 'CreateUser') && (DEFAULT_LANGUAGE == $EachLanguage['Code']) ? true : false))) ?>><?php print($EachLanguage['Name']); ?></option>
											<?php
											endforeach;
											?>
										</select>
										<?php print(form_error('Language', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										<div class="form-row-note">
											<p><?php InterfaceLanguage('Screen', '0032', false); ?></p>
										</div>
									</div>
									<div class="form-row <?php print((form_error('RelUserGroupID') != '' ? 'error' : '')); ?>" id="form-row-RelUserGroupID">
										<label for="RelUserGroupID"><?php InterfaceLanguage('Screen', '0019', false); ?>: *</label>
										<select name="RelUserGroupID" id="RelUserGroupID" class="select">
											<option value="" <?php echo set_select('RelUserGroupID', '', true) ?>><?php InterfaceLanguage('Screen', '0017', false); ?></option>
											<?php
											foreach (UserGroups::RetrieveUserGroups(array('*'), array()) as $EachGroup):
											?>
												<option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('RelUserGroupID', $EachGroup['UserGroupID']); ?>><?php print($EachGroup['GroupName']); ?></option>
											<?php
											endforeach;
											?>
										</select>
										<?php print(form_error('RelUserGroupID', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										<div class="form-row-note">
											<p><?php InterfaceLanguage('Screen', '0020', false); ?></p>
										</div>
									</div>
								</div>
								<!-- Account Information Section - END -->

								<!-- Personal Information Section - START -->
								<div tabcollection="form-tabs" id="tab-content-2">
									<div class="form-row <?php print((form_error('CompanyName') != '' ? 'error' : '')); ?>" id="form-row-CompanyName">
										<label for="CompanyName"><?php InterfaceLanguage('Screen', '0023', false); ?>:</label>
										<input type="text" name="CompanyName" value="<?php echo set_value('CompanyName', ''); ?>" id="CompanyName" class="text" />
										<?php print(form_error('CompanyName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Website') != '' ? 'error' : '')); ?>" id="form-row-Website">
										<label for="Website"><?php InterfaceLanguage('Screen', '0024', false); ?>:</label>
										<input type="text" name="Website" value="<?php echo set_value('Website', ''); ?>" id="Website" class="text" />
										<?php print(form_error('Website', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Street') != '' ? 'error' : '')); ?>" id="form-row-Street">
										<label for="Street"><?php InterfaceLanguage('Screen', '0025', false); ?>:</label>
										<textarea name="Street" id="Street" class="textarea"><?php echo set_value('Street', ''); ?></textarea>
										<?php print(form_error('Street', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('City') != '' ? 'error' : '')); ?>" id="form-row-City">
										<label for="City"><?php InterfaceLanguage('Screen', '0026', false); ?>:</label>
										<input type="text" name="City" value="<?php echo set_value('City', ''); ?>" id="City" class="text" />
										<?php print(form_error('City', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('State') != '' ? 'error' : '')); ?>" id="form-row-State">
										<label for="State"><?php InterfaceLanguage('Screen', '0027', false); ?>:</label>
										<input type="text" name="State" value="<?php echo set_value('State', ''); ?>" id="State" class="text" />
										<?php print(form_error('State', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Zip') != '' ? 'error' : '')); ?>" id="form-row-Zip">
										<label for="Zip"><?php InterfaceLanguage('Screen', '0028', false); ?>:</label>
										<input type="text" name="Zip" value="<?php echo set_value('Zip', ''); ?>" id="Zip" class="text" />
										<?php print(form_error('Zip', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Country') != '' ? 'error' : '')); ?>" id="form-row-Country">
										<label for="Country"><?php InterfaceLanguage('Screen', '0029', false); ?>:</label>
										<select name="Country" id="Country" class="select">
											<option value="" <?php echo set_select('Country', '', true) ?>><?php InterfaceLanguage('Screen', '0017', false); ?></option>
											<?php
												$ArrayCountries = ApplicationHeader::$ArrayLanguageStrings['Screen']['0036'];
												asort($ArrayCountries);
											foreach ($ArrayCountries as $Code=>$Name):
											?>
												<option value="<?php print($Code); ?>" <?php echo set_select('Country', $Code); ?>><?php print($Name); ?></option>
											<?php
											endforeach;
											?>
										</select>
										<?php print(form_error('Country', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Phone') != '' ? 'error' : '')); ?>" id="form-row-Phone">
										<label for="Phone"><?php InterfaceLanguage('Screen', '0030', false); ?>:</label>
										<input type="text" name="Phone" value="<?php echo set_value('Phone', ''); ?>" id="Phone" class="text" />
										<?php print(form_error('Phone', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Fax') != '' ? 'error' : '')); ?>" id="form-row-Fax">
										<label for="Fax"><?php InterfaceLanguage('Screen', '0031', false); ?>:</label>
										<input type="text" name="Fax" value="<?php echo set_value('Fax', ''); ?>" id="Fax" class="text" />
										<?php print(form_error('Fax', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
								</div>
								<!-- Personal Information Section - END -->
							</div>
						</div>
					</div>
				</div>
				<div class="help-column span-5 push-1 last">
					<?php include_once(TEMPLATE_PATH.'desktop/help/help_admin_createuser.php'); ?>
				</div>
				<div class="span-18 last">
					<div class="form-action-container">
						<a class="button" targetform="UserCreateForm"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0013', false, '', true); ?></strong></a>
						<input type="hidden" name="Command" value="CreateUser" id="Command" />
					</div>
				</div>
			</form>
		</div>
		<!-- Page - End -->

		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
