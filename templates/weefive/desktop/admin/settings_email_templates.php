	<div id="page-shadow">
		<div id="page">
			<div class="white" style="min-height:420px;">
				<form id="email-templates-table-form" action="<?php InterfaceAppURL(); ?>/admin/emailtemplates/" method="post">
					<input type="hidden" name="Command" value="DeleteEmailTemplates" id="Command">
					<div class="module-container">
						<?php
						if (isset($PageSuccessMessage) == true):
						?>
						<div class="page-message success">
							<div class="inner">
								<span class="close">X</span>
								<?php print($PageSuccessMessage); ?>
							</div>
						</div>
						<?php
						elseif (isset($PageErrorMessage) == true):									
						?>
						<div class="page-message error">
							<div class="inner">
								<span class="close">X</span>
								<?php print($PageErrorMessage); ?>
							</div>
						</div>
						<?php
						endif;
						?>
						<div class="grid-operations">
							<div class="inner">
								<span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
								<a href="#" class="grid-select-all" targetgrid="email-templates-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>, 
								<a href="#" class="grid-select-none" targetgrid="email-templates-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
								&nbsp;&nbsp;-&nbsp;&nbsp;
								<a href="#" class="main-action" targetform="email-templates-table-form"><?php InterfaceLanguage('Screen', '0042'); ?></a>
								&nbsp;|&nbsp;
								<a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/create/" class="main-action"><?php InterfaceLanguage('Screen', '0114'); ?></a>
							</div>
						</div>
					</div>
					<table border="0" cellspacing="0" cellpadding="0" class="grid" id="email-templates-table">
						<tr>
							<th width="100%" colspan="3"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
						</tr>
						<?php
						if ($EmailTemplates == false):
						?>
						<tr>
							<td colspan="3"><?php InterfaceLanguage('Screen', '0113'); ?></td>
						</tr>
						<?php
						endif;
						foreach ($EmailTemplates as $EachTemplate):
						?>
							<tr>
								<td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedEmailTemplates[]" value="<?php print($EachTemplate->TemplateID); ?>" id="SelectedEmailTemplates<?php print($EachTemplate->TemplateID); ?>"></td>
								<td width="600" class="no-padding-left"><a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/edit/<?php print($EachTemplate->TemplateID); ?>"><?php print($EachTemplate->TemplateName); ?></a></td>
							</tr>
						<?php
						endforeach;
						?>
					</table>
				</form>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" charset="utf-8">
		var language_object = {
			screen : {
				'0312'	: '<?php InterfaceLanguage('Screen', '0312', false, '', false); ?>'
			}
		};
	</script>
	<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/settings_emailtemplates.js" type="text/javascript" charset="utf-8"></script>		
