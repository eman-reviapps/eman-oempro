<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_header.php'); ?>

<link rel="stylesheet" href="<?php InterfaceTemplateURL(false); ?>/styles/farbtastic.css" type="text/css" media="screen, projection">

<!-- Section bar - Start -->
<div class="container">
    <div class="span-23 last">
        <div id="section-bar">
            <div class="header">
                <h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0588', false, '', false, true); ?></h1>
            </div>
        </div>
    </div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
    <form id="UserGroupEditForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/usergroups/edit/<?php print($UserGroup['UserGroupID']); ?>">
        <input type="hidden" name="UserGroupID" value="<?php print($UserGroup['UserGroupID']); ?>" id="UserGroupID">
        <input type="hidden" name="PaymentPricingRange" value="<?php print($UserGroup['PaymentPricingRange']); ?>" id="PaymentPricingRange">
        <div class="span-18 last">
            <div id="page-shadow">
                <div id="page">
                    <div class="page-bar">
                        <ul class="livetabs" tabcollection="user-group-tabs">
                            <li id="tab-1">
                                <a href="#"><?php InterfaceLanguage('Screen', '0135', false, '', false); ?></a>
                            </li>
                            <li id="tab-2">
                                <a href="#"><?php InterfaceLanguage('Screen', '0509', false, '', false); ?></a>
                            </li>
                            <li id="tab-3">
                                <a href="#"><?php InterfaceLanguage('Screen', '0510', false, '', false, true); ?></a>
                            </li>
                            <li id="tab-4">
                                <a href="#"><?php InterfaceLanguage('Screen', '0511', false, '', false); ?></a>
                            </li>
                            <li id="tab-5">
                                <a href="#"><?php InterfaceLanguage('Screen', '0512', false, '', false); ?></a>
                            </li>
                        </ul>
                        <div class="actions">
                            <a href="<?php InterfaceAppURL(); ?>/admin/usergroups/"><?php InterfaceLanguage('Screen', '0143', false); ?></a>
                        </div>
                    </div>
                    <div class="white">
                        <?php
                        if (isset($PageErrorMessage) == true):
                            ?>
                            <h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
                            <?php
                        elseif (validation_errors()):
                            ?>
                            <h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
                            <?php
                        else :
                            ?>
                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
                        <?php
                        endif;
                        ?>

                        <!-- Preferences Section - START -->
                        <div tabcollection="user-group-tabs" id="tab-content-1">
                            <div class="form-row <?php print((form_error('GroupName') != '' ? 'error' : '')); ?>" id="form-row-GroupName">
                                <label for="GroupName"><?php InterfaceLanguage('Screen', '0513', false, '', false, true); ?>: *</label>
                                <input type="text" name="GroupName" value="<?php echo set_value('GroupName', $UserGroup['GroupName']); ?>" id="GroupName" class="text" />
                                <?php print(form_error('GroupName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="form-row" id="form-row-ProductName">
                                <label for="ProductName"><?php InterfaceLanguage('Screen', '0156', false, '', false, true); ?>:</label>
                                <input type="text" name="ProductName" value="<?php echo set_value('ProductName', $UserGroup['ThemeInformation']['ProductName']); ?>" id="ProductName" class="text" />
                            </div>
                            <div class="form-row" id="form-row-PaymentSystem">
                                <label><?php InterfaceLanguage('Screen', '9189', false, '', false, true); ?></label>
                                <select name="SubscriptionType" id="SubscriptionType" class="select">
                                    <option value="Monthly" <?php echo set_select('SubscriptionType', 'Monthly', ($UserGroup['SubscriptionType'] == 'Monthly' ? true : false)); ?>><?php InterfaceLanguage('Screen', '9190', false, '', false, false); ?></option>
                                    <option value="Annually" <?php echo set_select('SubscriptionType', 'Annually', ($UserGroup['SubscriptionType'] == 'Annually' ? true : false)); ?>><?php InterfaceLanguage('Screen', '9191', false, '', false, false); ?></option>                                    
                                </select>
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '9193', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div class="form-row" id="form-row-Preferences">
                                <label>&nbsp;</label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="ForceUnsubscriptionLink" value="Enabled" id="Preferences-ForceUnsubscriptionLink" <?php echo set_checkbox('ForceUnsubscriptionLink', 'Enabled', ($UserGroup['ForceUnsubscriptionLink'] == 'Enabled' ? true : false)); ?> /> <label for="Preferences-ForceUnsubscriptionLink"><?php InterfaceLanguage('Screen', '0514', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="ForceRejectOptLink" value="Enabled" id="Preferences-ForceRejectOptLink" <?php echo set_checkbox('ForceRejectOptLink', 'Enabled', ($UserGroup['ForceRejectOptLink'] == 'Enabled' ? true : false)); ?> /> <label for="Preferences-ForceRejectOptLink"><?php InterfaceLanguage('Screen', '0515', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="ForceOptInList" value="Enabled" id="Preferences-ForceOptInList" <?php echo set_checkbox('ForceOptInList', 'Enabled', ($UserGroup['ForceOptInList'] == 'Enabled' ? true : false)); ?> /> <label for="Preferences-ForceOptInList"><?php InterfaceLanguage('Screen', '0516', false, '', false); ?></label>
                                    </div>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0518', false, '', false, false, array(InterfaceAppURL(true) . '/admin/defaultoptinemail/')); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row <?php print((form_error('SubscriberAreaLogoutURL') != '' ? 'error' : '')); ?>" id="form-row-SubscriberAreaLogoutURL">
                                <label for="SubscriberAreaLogoutURL" style="width:230px;"><?php InterfaceLanguage('Screen', '0517', false, '', false, true); ?>: *</label>
                                <input type="text" name="SubscriberAreaLogoutURL" value="<?php echo set_value('SubscriberAreaLogoutURL', $UserGroup['SubscriberAreaLogoutURL']); ?>" id="SubscriberAreaLogoutURL" class="text" />
                                <?php print(form_error('SubscriberAreaLogoutURL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>

                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '1606', false, '', false, true); ?></h3>
                            <div class="form-row no-bg">
                                <p><?php InterfaceLanguage('Screen', '1607', false, '', false, false); ?></p>
                            </div>
                            <div class="form-row" id="form-row-TrialGroup">
                                <label for="TrialGroup"><?php InterfaceLanguage('Screen', '1608', false, '', false, true); ?>:</label>
                                <select name="TrialGroup" id="TrialGroup" class="select">
                                    <option value="Yes" <?php echo set_select('TrialGroup', 'Yes', ($UserGroup['TrialGroup'] == 'Yes' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1610', false, '', false, false); ?></option>
                                    <option value="No" <?php echo set_select('TrialGroup', 'No', ($UserGroup['TrialGroup'] == 'No' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1611', false, '', false, false); ?></option>
                                </select>
                                <?php print(form_error('TrialGroup', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="form-row <?php print((form_error('TrialExpireSeconds') != '' ? 'error' : '')); ?>" id="form-row-TrialExpireSeconds">
                                <label for="TrialExpireSeconds"><?php InterfaceLanguage('Screen', '1609', false, '', false, true); ?>:</label>
                                <input type="text" name="TrialExpireSeconds" value="<?php echo set_value('TrialExpireSeconds', ($UserGroup['TrialExpireSeconds'] > 0 ? $UserGroup['TrialExpireSeconds'] / 86400 : '')); ?>" id="TrialExpireSeconds" class="text" style="width:25px;" /> <?php InterfaceLanguage('Screen', '1612', false, '', false, false); ?>
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '1613', false, '', false, false); ?></p>
                                </div>
                                <?php print(form_error('TrialExpireSeconds', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>

                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '0519', false, '', false, true); ?></h3>
                            <div class="form-row no-bg">
                                <p><?php InterfaceLanguage('Screen', '0520', false, '', false, false); ?></p>
                            </div>
                            <div class="form-row" id="form-row-ThresholdImport">
                                <label for="ThresholdImport"><?php InterfaceLanguage('Screen', '0521', false, '', false, true); ?>:</label>
                                <input type="text" name="ThresholdImport" value="<?php echo set_value('ThresholdImport', ($UserGroup['ThresholdImport'] == '0' ? '' : $UserGroup['ThresholdImport'])); ?>" id="ThresholdImport" class="text" style="width:100px;" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0522', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div class="form-row" id="form-row-ThresholdEmailSend">
                                <label for="ThresholdEmailSend" style="width:160px;"><?php InterfaceLanguage('Screen', '0523', false, '', false, true); ?>:</label>
                                <input type="text" name="ThresholdEmailSend" value="<?php echo set_value('ThresholdEmailSend', ($UserGroup['ThresholdEmailSend'] == '0' ? '' : $UserGroup['ThresholdEmailSend'])); ?>" id="ThresholdEmailSend" class="text" style="width:100px;" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0524', false, '', false, false); ?></p>
                                </div>
                            </div>

                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '1600', false, '', false, true); ?></h3>
                            <div class="form-row no-bg">
                                <p><?php InterfaceLanguage('Screen', '1601', false, '', false, false); ?></p>
                            </div>
                            <div class="form-row" id="form-row-PlainEmailHeader">
                                <label for="PlainEmailHeader"><?php InterfaceLanguage('Screen', '1602', false, '', false, true); ?>:</label>
                                <textarea name="PlainEmailHeader" id="PlainEmailHeader" class="textarea"><?php echo set_value('PlainEmailHeader', $UserGroup['PlainEmailHeader']); ?></textarea>
                                <?php print(form_error('PlainEmailHeader', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="form-row" id="form-row-PlainEmailFooter">
                                <label for="PlainEmailFooter"><?php InterfaceLanguage('Screen', '1603', false, '', false, true); ?>:</label>
                                <textarea name="PlainEmailFooter" id="PlainEmailFooter" class="textarea"><?php echo set_value('PlainEmailFooter', $UserGroup['PlainEmailFooter']); ?></textarea>
                                <?php print(form_error('PlainEmailFooter', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>

                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '1604', false, '', false, true); ?></h3>
                            <div class="form-row no-bg">
                                <p><?php InterfaceLanguage('Screen', '1605', false, '', false, false); ?></p>
                            </div>
                            <div class="form-row" id="form-row-HTMLEmailHeader">
                                <label for="PlainEmailHeader"><?php InterfaceLanguage('Screen', '1602', false, '', false, true); ?>:</label>
                                <textarea name="HTMLEmailHeader" id="HTMLEmailHeader" class="textarea"><?php echo set_value('HTMLEmailHeader', $UserGroup['HTMLEmailHeader']); ?></textarea>
                                <?php print(form_error('HTMLEmailHeader', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="form-row" id="form-row-HTMLEmailFooter">
                                <label for="HTMLEmailFooter"><?php InterfaceLanguage('Screen', '1603', false, '', false, true); ?>:</label>
                                <textarea name="HTMLEmailFooter" id="HTMLEmailFooter" class="textarea"><?php echo set_value('HTMLEmailFooter', $UserGroup['HTMLEmailFooter']); ?></textarea>
                                <?php print(form_error('HTMLEmailFooter', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>

                            <h3 class="form-legend"><?php InterfaceLanguage('Screen', '1623', false, '', false, true); ?></h3>
                            <div class="form-row no-bg">
                                <p><?php InterfaceLanguage('Screen', '1624', false, '', false, false); ?></p>
                            </div>
                            <div class="form-row <?php print((form_error('XMailer') != '' ? 'error' : '')); ?>" id="form-row-XMailer">
                                <label for="XMailer"><?php InterfaceLanguage('Screen', '0298', false); ?>:</label>
                                <input type="text" name="XMailer" value="<?php echo set_value('XMailer', $UserGroup['XMailer']); ?>" id="XMailer" class="text" />
                                <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1626', false, '', false); ?></p></div>
                                <?php print(form_error('XMailer', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="form-row <?php print((form_error('SendMethod') != '' ? 'error' : '')); ?>" id="form-row-SendMethod">
                                <label for="SendMethod"><?php InterfaceLanguage('Screen', '0118', false); ?>:</label>
                                <select name="SendMethod" id="SendMethod" class="select">
                                    <option value="System" <?php echo set_select('SendMethod', 'System', ($UserGroup['SendMethod'] == 'System' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1625', false, '', false); ?></option>

                                    <optgroup label="<?php InterfaceLanguage('Screen', '1866', false, '', false); ?>">
                                        <option value="SMTP" <?php echo set_select('SendMethod', 'SMTP', ($UserGroup['SendMethod'] == 'SMTP' && in_array($UserGroup['SendMethodSMTPHost'], array('octeth.smtp.com', 'smtp.sendgrid.net', 'smtp.mailgun.org', 'in.mailjet.com', 'smtp.postmarkapp.com')) != true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0292', false, '', false); ?></option>
                                    </optgroup>

                                    <optgroup label="<?php InterfaceLanguage('Screen', '1863', false, '', false); ?>">
                                        <option value="SMTP-OCTETH" <?php echo set_select('SendMethod', 'SMTP-OCTETH', ($UserGroup['SendMethod'] == 'SMTP' && $UserGroup['SendMethodSMTPHost'] == 'octeth.smtp.com' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0316', false, '', false); ?></option>
                                        <option value="SMTP-SENDGRID" <?php echo set_select('SendMethod', 'SMTP-SENDGRID', ($UserGroup['SendMethod'] == 'SMTP' && $UserGroup['SendMethodSMTPHost'] == 'smtp.sendgrid.net' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1864', false, '', false); ?></option>
                                        <option value="SMTP-MAILGUN" <?php echo set_select('SendMethod', 'SMTP-MAILGUN', ($UserGroup['SendMethod'] == 'SMTP' && $UserGroup['SendMethodSMTPHost'] == 'smtp.mailgun.org' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1917', false, '', false); ?></option>
                                        <option value="SMTP-MAILJET" <?php echo set_select('SendMethod', 'SMTP-MAILJET', ($UserGroup['SendMethod'] == 'SMTP' && $UserGroup['SendMethodSMTPHost'] == 'in.mailjet.com' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1922', false, '', false); ?></option>
                                        <option value="SMTP-SES" <?php echo set_select('SendMethod', 'SMTP-SES', ($UserGroup['SendMethod'] == 'SMTP' && preg_match('/amazonaws.com$/i', SEND_METHOD_SMTP_HOST) > 0 ? true : false)); ?>><?php InterfaceLanguage('Screen', '1923', false, '', false); ?></option>
                                    </optgroup>

                                    <optgroup label="<?php InterfaceLanguage('Screen', '1865', false, '', false); ?>">
                                        <option value="LocalMTA" <?php echo set_select('SendMethod', 'LocalMTA', ($UserGroup['SendMethod'] == 'LocalMTA' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0293', false, '', false); ?></option>
                                        <option value="SaveToDisk" <?php echo set_select('SendMethod', 'SaveToDisk', ($UserGroup['SendMethod'] == 'SaveToDisk' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0296', false, '', false); ?></option>
                                        <option value="PowerMTA" <?php echo set_select('SendMethod', 'PowerMTA', ($UserGroup['SendMethod'] == 'PowerMTA' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0295', false, '', false); ?></option>
                                        <option value="PHPMail" <?php echo set_select('SendMethod', 'PHPMail', ($UserGroup['SendMethod'] == 'PHPMail' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0294', false, '', false); ?></option>
                                    </optgroup>
                                </select>
                                <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1627', false, '', false); ?></p></div>
                                <?php print(form_error('SendMethod', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div id="sub-smtp-settings">
                                <div class="form-row <?php print((form_error('SendMethodSMTPHost') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPHost">
                                    <label for="SendMethodSMTPHost"><?php InterfaceLanguage('Screen', '0379', false); ?>: *</label>
                                    <input type="text" name="SendMethodSMTPHost" value="<?php echo set_value('SendMethodSMTPHost', $UserGroup['SendMethodSMTPHost']); ?>" id="SendMethodSMTPHost" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0380', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSMTPHost', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSMTPPort') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPPort">
                                    <label for="SendMethodSMTPPort"><?php InterfaceLanguage('Screen', '0381', false); ?>: *</label>
                                    <input type="text" name="SendMethodSMTPPort" value="<?php echo set_value('SendMethodSMTPPort', $UserGroup['SendMethodSMTPPort']); ?>" id="SendMethodSMTPPort" class="text" />
                                    <?php print(form_error('SendMethodSMTPPort', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSMTPSecure') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPSecure">
                                    <label for="SendMethodSMTPSecure"><?php InterfaceLanguage('Screen', '0382', false); ?>:</label>
                                    <select name="SendMethodSMTPSecure" id="SendMethodSMTPSecure" class="select">
                                        <option value="" <?php echo set_select('SendMethodSMTPSecure', '', ($UserGroup['SendMethodSMTPSecure'] == '' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0384', false, '', false); ?></option>
                                        <option value="ssl" <?php echo set_select('SendMethodSMTPSecure', 'ssl', ($UserGroup['SendMethodSMTPSecure'] == 'ssl' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0385', false, '', false); ?></option>
                                        <option value="tls" <?php echo set_select('SendMethodSMTPSecure', 'tls', ($UserGroup['SendMethodSMTPSecure'] == 'tls' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0386', false, '', false); ?></option>
                                    </select>
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0383', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSMTPSecure', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSMTPTimeOut') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPTimeOut">
                                    <label for="SendMethodSMTPTimeOut"><?php InterfaceLanguage('Screen', '0387', false); ?>: *</label>
                                    <input type="text" name="SendMethodSMTPTimeOut" value="<?php echo set_value('SendMethodSMTPTimeOut', $UserGroup['SendMethodSMTPTimeOut']); ?>" id="SendMethodSMTPTimeOut" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0388', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSMTPTimeOut', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSMTPAuth') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPAuth">
                                    <label for="SendMethodSMTPAuth"><?php InterfaceLanguage('Screen', '0389', false); ?>: *</label>
                                    <select name="SendMethodSMTPAuth" id="SendMethodSMTPAuth" class="select">
                                        <option value="true" <?php echo set_select('SendMethodSMTPAuth', 'true', ($UserGroup['SendMethodSMTPAuth'] == 'true' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0390', false, '', false); ?></option>
                                        <option value="false" <?php echo set_select('SendMethodSMTPAuth', 'false', ($UserGroup['SendMethodSMTPAuth'] == 'false' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0391', false, '', false); ?></option>
                                    </select>
                                    <?php print(form_error('SendMethodSMTPSecure', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div id="sub-smtp-settings-auth">
                                    <div class="form-row <?php print((form_error('SendMethodSMTPUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPUsername">
                                        <label for="SendMethodSMTPUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                        <input type="text" name="SendMethodSMTPUsername" value="<?php echo set_value('SendMethodSMTPUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodSMTPUsername" class="text" />
                                        <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0392', false, '', false); ?></p></div>
                                        <?php print(form_error('SendMethodSMTPUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                    </div>
                                    <div class="form-row <?php print((form_error('SendMethodSMTPPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSMTPPassword">
                                        <label for="SendMethodSMTPPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                        <input type="password" name="SendMethodSMTPPassword" value="<?php echo set_value('SendMethodSMTPPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodSMTPPassword" class="text" />
                                        <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0393', false, '', false); ?></p></div>
                                        <?php print(form_error('SendMethodSMTPPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                    </div>
                                </div>
                            </div>
                            <div id="sub-octeth-smtp-settings">
                                <div class="form-row <?php print((form_error('SendMethodOctethSMTPUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodOctethSMTPUsername">
                                    <label for="SendMethodOctethSMTPUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                    <input type="text" name="SendMethodOctethSMTPUsername" value="<?php echo set_value('SendMethodOctethSMTPUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodOctethSMTPUsername" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0377', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodOctethSMTPUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodOctethSMTPPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodOctethSMTPPassword">
                                    <label for="SendMethodOctethSMTPPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                    <input type="password" name="SendMethodOctethSMTPPassword" value="<?php echo set_value('SendMethodOctethSMTPPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodOctethSMTPPassword" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0378', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodOctethSMTPPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-sendgrid-settings">
                                <div class="form-row <?php print((form_error('SendMethodSendgridUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSendgridUsername">
                                    <label for="SendMethodSendgridUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                    <input type="text" name="SendMethodSendgridUsername" value="<?php echo set_value('SendMethodSendgridUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodSendgridUsername" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1867', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSendgridUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSendgridPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSendgridPassword">
                                    <label for="SendMethodSendgridPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                    <input type="password" name="SendMethodSendgridPassword" value="<?php echo set_value('SendMethodSendgridPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodSendgridPassword" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1868', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSendgridPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-mailgun-settings">
                                <div class="form-row <?php print((form_error('SendMethodMailgunUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodMailgunUsername">
                                    <label for="SendMethodMailgunUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                    <input type="text" name="SendMethodMailgunUsername" value="<?php echo set_value('SendMethodMailgunUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodMailgunUsername" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1918', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodMailgunUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodMailgunPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodMailgunPassword">
                                    <label for="SendMethodMailgunPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                    <input type="password" name="SendMethodMailgunPassword" value="<?php echo set_value('SendMethodMailgunPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodMailgunPassword" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1919', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodMailgunPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-mailjet-settings">
                                <div class="form-row <?php print((form_error('SendMethodMailjetUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodMailjetUsername">
                                    <label for="SendMethodMailjetUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                    <input type="text" name="SendMethodMailjetUsername" value="<?php echo set_value('SendMethodMailjetUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodMailjetUsername" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1918', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodMailjetUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodMailjetPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodMailjetPassword">
                                    <label for="SendMethodMailjetPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                    <input type="password" name="SendMethodMailjetPassword" value="<?php echo set_value('SendMethodMailjetPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodMailjetPassword" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1919', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodMailjetPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-ses-settings">
                                <div class="form-row <?php print((form_error('SendMethodSESHost') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSESHost">
                                    <label for="SendMethodSESHost"><?php InterfaceLanguage('Screen', '0379', false); ?>: *</label>
                                    <input type="text" name="SendMethodSESHost" value="<?php echo set_value('SendMethodSMTPHost', $UserGroup['SendMethodSESHost']); ?>" id="SendMethodSESHost" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0380', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSESHost', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSESSecure') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSESSecure">
                                    <label for="SendMethodSESSecure"><?php InterfaceLanguage('Screen', '0382', false); ?>:</label>
                                    <select name="SendMethodSESSecure" id="SendMethodSESSecure" class="select">
                                        <option value="tls" <?php echo set_select('SendMethodSMTPSecure', 'tls', ($UserGroup['SendMethodSMTPSecure'] == 'tls' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0386', false, '', false); ?></option>
                                    </select>
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0383', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSESSecure', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSESUsername') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSESUsername">
                                    <label for="SendMethodSESUsername"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
                                    <input type="text" name="SendMethodSESUsername" value="<?php echo set_value('SendMethodSESUsername', $UserGroup['SendMethodSMTPUsername']); ?>" id="SendMethodSESUsername" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1924', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSESUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodSESPassword') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSESPassword">
                                    <label for="SendMethodSESPassword"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
                                    <input type="password" name="SendMethodSESPassword" value="<?php echo set_value('SendMethodSESPassword', $UserGroup['SendMethodSMTPPassword']); ?>" id="SendMethodSESPassword" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1925', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSESPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>

                            <?php if (1 == 2): ?>
                                <!--							<div id="sub-postmark-settings">-->
                                <!--								<div class="form-row --><?php //print((form_error('SendMethodPostmarkAPIKey') != '' ? 'error' : ''));  ?><!--" id="form-row-SendMethodPostmarkAPIKey">-->
                                <!--									<label for="SendMethodPostmarkAPIKey">--><?php //InterfaceLanguage('Screen', '1870', false);  ?><!--: *</label>-->
                                <!--									<input type="text" name="SendMethodPostmarkAPIKey" value="--><?php //echo set_value('SendMethodPostmarkAPIKey', $UserGroup['SendMethodSMTPUsername']);  ?><!--" id="SendMethodPostmarkAPIKey" class="text" />-->
                                <!--									<div class="form-row-note"><p>--><?php //InterfaceLanguage('Screen', '1871', false, '', false);  ?><!--</p></div>-->
                                <!--									--><?php //print(form_error('SendMethodPostmarkAPIKey', '<div class="form-row-note error"><p>', '</p></div>'));  ?>
                                <!--								</div>-->
                                <!--							</div>-->
                            <?php endif; ?>
                            <div id="sub-local-mta-settings">
                                <div class="form-row <?php print((form_error('SendMethodLocalMTAPath') != '' ? 'error' : '')); ?>" id="form-row-SendMethodLocalMTAPath">
                                    <label for="SendMethodLocalMTAPath"><?php InterfaceLanguage('Screen', '0373', false); ?>: *</label>
                                    <input type="text" name="SendMethodLocalMTAPath" value="<?php echo set_value('SendMethodLocalMTAPath', $UserGroup['SendMethodLocalMTAPath']); ?>" id="SendMethodLocalMTAPath" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0374', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodLocalMTAPath', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-php-mail-settings">
                                <!-- No settings for PHP mail() function -->
                            </div>
                            <div id="sub-powermta-settings">
                                <div class="form-row <?php print((form_error('SendMethodPowerMTAVMTA') != '' ? 'error' : '')); ?>" id="form-row-SendMethodPowerMTAVMTA">
                                    <label for="SendMethodPowerMTAVMTA"><?php InterfaceLanguage('Screen', '0320', false); ?>: *</label>
                                    <input type="text" name="SendMethodPowerMTAVMTA" value="<?php echo set_value('SendMethodPowerMTAVMTA', $UserGroup['SendMethodPowerMTAVMTA']); ?>" id="SendMethodPowerMTAVMTA" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0369', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodPowerMTAVMTA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                                <div class="form-row <?php print((form_error('SendMethodPowerMTADir') != '' ? 'error' : '')); ?>" id="form-row-SendMethodPowerMTADir">
                                    <label for="SendMethodPowerMTADir"><?php InterfaceLanguage('Screen', '0371', false); ?>: *</label>
                                    <input type="text" name="SendMethodPowerMTADir" value="<?php echo set_value('SendMethodPowerMTADir', $UserGroup['SendMethodPowerMTADir']); ?>" id="SendMethodPowerMTADir" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0372', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodPowerMTADir', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                            <div id="sub-savetodisk-settings">
                                <div class="form-row <?php print((form_error('SendMethodSaveToDiskDir') != '' ? 'error' : '')); ?>" id="form-row-SendMethodSaveToDiskDir">
                                    <label for="SendMethodSaveToDiskDir"><?php InterfaceLanguage('Screen', '0317', false); ?>: *</label>
                                    <input type="text" name="SendMethodSaveToDiskDir" value="<?php echo set_value('SendMethodSaveToDiskDir', $UserGroup['SendMethodSaveToDiskDir']); ?>" id="SendMethodSaveToDiskDir" class="text" />
                                    <div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0318', false, '', false); ?></p></div>
                                    <?php print(form_error('SendMethodSaveToDiskDir', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                </div>
                            </div>
                        </div>
                        <!-- Preferences Section - END -->

                        <!-- Limits Section - START -->
                        <div tabcollection="user-group-tabs" id="tab-content-2">
                            <div class="form-row" id="form-row-LimitSubscribers">
                                <label for="LimitSubscribers"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?>:</label>
                                <input type="text" name="LimitSubscribers" value="<?php echo set_value('LimitSubscribers', ($UserGroup['LimitSubscribers'] == 0 ? '' : $UserGroup['LimitSubscribers'])); ?>" id="LimitSubscribers" class="text" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0525', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div class="form-row" id="form-row-LimitLists">
                                <label for="LimitLists"><?php InterfaceLanguage('Screen', '0100', false, '', false, true); ?>:</label>
                                <input type="text" name="LimitLists" value="<?php echo set_value('LimitLists', ($UserGroup['LimitLists'] == 0 ? '' : $UserGroup['LimitLists'])); ?>" id="LimitLists" class="text" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0526', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div class="form-row" id="form-row-LimitCampaignSendPerPeriod">
                                <label for="LimitCampaignSendPerPeriod"><?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?>:</label>
                                <input type="text" name="LimitCampaignSendPerPeriod" value="<?php echo set_value('LimitCampaignSendPerPeriod', ($UserGroup['LimitCampaignSendPerPeriod'] == 0 ? '' : $UserGroup['LimitCampaignSendPerPeriod'])); ?>" id="LimitCampaignSendPerPeriod" class="text" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0527', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div class="form-row" id="form-row-LimitEmailSendPerPeriod">
                                <label for="LimitEmailSendPerPeriod"><?php InterfaceLanguage('Screen', '0528', false, '', false, true); ?>:</label>
                                <input type="text" name="LimitEmailSendPerPeriod" value="<?php echo set_value('LimitEmailSendPerPeriod', ($UserGroup['LimitEmailSendPerPeriod'] == 0 ? '' : $UserGroup['LimitEmailSendPerPeriod'])); ?>" id="LimitEmailSendPerPeriod" class="text" />
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0529', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <?php
                            InterfacePluginFormItemsHook('FormItem.AddTo.Admin.UserGroupLimitsForm', array($UserGroup));
                            ?>

                        </div>
                        <!-- Limits Section - END -->

                        <!-- Payment settings section - START -->
                        <div tabcollection="user-group-tabs" id="tab-content-3">
                            <div class="form-row" id="form-row-PaymentSystem">
                                <label><?php InterfaceLanguage('Screen', '0592', false, '', false, true); ?></label>
                                <select name="PaymentSystem" id="PaymentSystem" class="select">
                                    <option value="Enabled" <?php echo set_select('PaymentSystem', 'Enabled', $UserGroup['PaymentSystem'] == 'Enabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0590', false, '', false, false); ?></option>
                                    <option value="Disabled" <?php echo set_select('PaymentSystem', 'Disabled', $UserGroup['PaymentSystem'] == 'Disabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0591', false, '', false, false); ?></option>
                                </select>
                                <div class="form-row-note">
                                    <p><?php InterfaceLanguage('Screen', '0593', false, '', false, false); ?></p>
                                </div>
                            </div>
                            <div id="payment-system-settings" style="display:none">
                                <div class="form-row" id="form-row-PaymentSystemChargeAmount">
                                    <label for="PaymentSystemChargeAmount"><?php InterfaceLanguage('Screen', '0601', false, '', false, true); ?></label>
                                    <input type="text" name="PaymentSystemChargeAmount" value="<?php echo set_value('PaymentSystemChargeAmount', $UserGroup['PaymentSystemChargeAmount'] == '0' ? '' : $UserGroup['PaymentSystemChargeAmount']); ?>" id="PaymentSystemChargeAmount" class="text" style="width:50px;" /> <?php InterfaceLanguage('Screen', '0605', false, '', false, false, array(PAYMENT_CURRENCY)); ?>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0602', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentCampaignsPerCampaignCost">
                                    <label for="PaymentCampaignsPerCampaignCost"><?php InterfaceLanguage('Screen', '0594', false, '', false, true); ?></label>
                                    <input type="text" name="PaymentCampaignsPerCampaignCost" value="<?php echo set_value('PaymentCampaignsPerCampaignCost', $UserGroup['PaymentCampaignsPerCampaignCost'] == '0' ? '' : $UserGroup['PaymentCampaignsPerCampaignCost']); ?>" id="PaymentCampaignsPerCampaignCost" class="text" style="width:50px;" /> <?php InterfaceLanguage('Screen', '0603', false, '', false, false, array(PAYMENT_CURRENCY)); ?>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0595', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentDesignPrevChargePerReq">
                                    <label for="PaymentDesignPrevChargePerReq"><?php InterfaceLanguage('Screen', '0596', false, '', false, true); ?></label>
                                    <input type="text" name="PaymentDesignPrevChargePerReq" value="<?php echo set_value('PaymentDesignPrevChargePerReq', $UserGroup['PaymentDesignPrevChargePerReq'] == '0' ? '' : $UserGroup['PaymentDesignPrevChargePerReq']); ?>" id="PaymentDesignPrevChargePerReq" class="text" style="width:50px;" /> <?php InterfaceLanguage('Screen', '0604', false, '', false, false, array(PAYMENT_CURRENCY)); ?>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0597', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentDesignPrevChargeAmount">
                                    <label for="PaymentDesignPrevChargeAmount"><?php InterfaceLanguage('Screen', '0596', false, '', false, true); ?></label>
                                    <input type="text" name="PaymentDesignPrevChargeAmount" value="<?php echo set_value('PaymentDesignPrevChargeAmount', $UserGroup['PaymentDesignPrevChargeAmount'] == '0' ? '' : $UserGroup['PaymentDesignPrevChargeAmount']); ?>" id="PaymentDesignPrevChargeAmount" class="text" style="width:50px;" /> <?php InterfaceLanguage('Screen', '0605', false, '', false, false, array(PAYMENT_CURRENCY)); ?>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0600', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentAutoRespondersChargeAmount">
                                    <label for="PaymentAutoRespondersChargeAmount" style="width:150px;"><?php InterfaceLanguage('Screen', '0598', false, '', false, true); ?></label>
                                    <input type="text" name="PaymentAutoRespondersChargeAmount" value="<?php echo set_value('PaymentAutoRespondersChargeAmount', $UserGroup['PaymentAutoRespondersChargeAmount'] == '0' ? '' : $UserGroup['PaymentAutoRespondersChargeAmount']); ?>" id="PaymentAutoRespondersChargeAmount" class="text" style="width:50px;" /> <?php InterfaceLanguage('Screen', '0605', false, '', false, false, array(PAYMENT_CURRENCY)); ?>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0599', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-CreditSystem">
                                    <label for="CreditSystem" style="width:250px;"><?php InterfaceLanguage('Screen', '1653', false, '', false, true); ?></label>
                                    <select name="CreditSystem" id="CreditSystem" class="select">
                                        <option value="Enabled" <?php echo set_select('CreditSystem', 'Enabled', $UserGroup['CreditSystem'] == 'Enabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0590', false, '', false, false, array()); ?></option>
                                        <option value="Disabled" <?php echo set_select('CreditSystem', 'Disabled', $UserGroup['CreditSystem'] == 'Disabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0591', false, '', false, false, array()); ?></option>
                                    </select>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '1654', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentAutoRespondersPerRecipient">
                                    <label for="PaymentAutoRespondersPerRecipient" style="width:290px;"><?php InterfaceLanguage('Screen', '0606', false, '', false, true); ?></label>
                                    <select name="PaymentAutoRespondersPerRecipient" id="PaymentAutoRespondersPerRecipient" class="select">
                                        <option value="Enabled" <?php echo set_select('PaymentAutoRespondersPerRecipient', 'Enabled', $UserGroup['PaymentAutoRespondersPerRecipient'] == 'Enabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0590', false, '', false, false, array()); ?></option>
                                        <option value="Disabled" <?php echo set_select('PaymentAutoRespondersPerRecipient', 'Disabled', $UserGroup['PaymentAutoRespondersPerRecipient'] == 'Disabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0591', false, '', false, false, array()); ?></option>
                                    </select>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0607', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PaymentCampaignsPerRecipient">
                                    <label for="PaymentCampaignsPerRecipient" style="width:250px;"><?php InterfaceLanguage('Screen', '0608', false, '', false, true); ?></label>
                                    <select name="PaymentCampaignsPerRecipient" id="PaymentCampaignsPerRecipient" class="select">
                                        <option value="Enabled" <?php echo set_select('PaymentCampaignsPerRecipient', 'Enabled', $UserGroup['PaymentCampaignsPerRecipient'] == 'Enabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0590', false, '', false, false, array()); ?></option>
                                        <option value="Disabled" <?php echo set_select('PaymentCampaignsPerRecipient', 'Disabled', $UserGroup['PaymentCampaignsPerRecipient'] == 'Disabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '0591', false, '', false, false, array()); ?></option>
                                    </select>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0609', false, '', false, false); ?></p>
                                    </div>
                                </div>
                                <div class="form-row" id="form-row-PricingRange">
                                    <label><?php InterfaceLanguage('Screen', '0610', false, '', false, true); ?></label>
                                    <div class="checkbox-container" id="range-container">
                                        <div class="checkbox-row" id="add-range-button-row" style="margin-top:9px;">
                                            <input type="button" name="add-range-button" value="<?php InterfaceLanguage('Screen', '0613', false, '', false, false, array()); ?>" id="add-range-button" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Payment settings section - END -->

                        <!-- Permissions Section - START -->
                        <div tabcollection="user-group-tabs" id="tab-content-4">
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0577', false, '', false, true); ?></label>
                                <select id="permission-preset-select" class="select">
                                    <option value=""><?php InterfaceLanguage('Screen', '0578', false, '', false, false); ?></option>
                                    <option value="EmailTemplates.Manage,User.Update,Email.SpamTest,Email.DesignPreview,Campaign.Create,Campaign.Update,Campaigns.Get,Campaign.Get,Campaign.Delete,List.Create,List.Update,List.Delete,Lists.Get,List.Get,CustomField.Create,CustomField.Update,CustomFields.Get,CustomFields.Delete,Media.Upload,Media.Browse,Media.Retrieve,Media.Delete,AutoResponder.Create,AutoResponders.Get,AutoResponders.Delete,AutoResponder.Get,AutoResponder.Update,Segments.Get,Segments.Delete,Segment.Update,Segment.Create,Subscribers.Get,Subscriber.Update,Subscribers.Delete,Subscribers.Import,Clients.Get,Clients.Delete,Client.Create,Client.Update,User.ForwardHeaderFooter.Set"><?php InterfaceLanguage('Screen', '0579', false, '', false, false); ?></option>
                                    <option value="User.Update,List.Create,List.Update,List.Delete,Lists.Get,List.Get,CustomField.Create,CustomField.Update,CustomFields.Get,CustomFields.Delete,AutoResponder.Create,AutoResponders.Get,AutoResponders.Delete,AutoResponder.Get,AutoResponder.Update,Segments.Get,Segments.Delete,Segment.Update,Segment.Create,Subscribers.Get,Subscriber.Update,Subscribers.Delete,Subscribers.Import"><?php InterfaceLanguage('Screen', '0580', false, '', false, false); ?></option>
                                    <option value="EmailTemplates.Manage,User.Update,Email.SpamTest,Email.DesignPreview,Campaign.Create,Campaign.Update,Campaigns.Get,Campaign.Get,Campaign.Delete,Media.Upload,Media.Browse,Media.Retrieve,Media.Delete"><?php InterfaceLanguage('Screen', '0581', false, '', false, false); ?></option>
                                    <option value="User.Update,Campaigns.Get,Campaign.Get,Lists.Get,List.Get,AutoResponders.Get,AutoResponder.Get"><?php InterfaceLanguage('Screen', '0582', false, '', false, false); ?></option>
                                </select>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0530', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="User.Update" id="Permission-User.Update" <?php echo set_checkbox('Permissions[]', 'User.Update'); ?> /> <label for="Permission-User.Update"><?php InterfaceLanguage('Screen', '0531', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="User.ForwardHeaderFooter.Set" id="Permission-User.ForwardHeaderFooter.Set" <?php echo set_checkbox('Permissions[]', 'User.ForwardHeaderFooter.Set'); ?> /> <label for="Permission-User.ForwardHeaderFooter.Set"><?php InterfaceLanguage('Screen', '0532', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0528', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Email.SpamTest" id="Permission-Email.SpamTest" <?php echo set_checkbox('Permissions[]', 'Email.SpamTest'); ?> /> <label for="Permission-Email.SpamTest"><?php InterfaceLanguage('Screen', '0533', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Email.DesignPreview" id="Permission-Email.DesignPreview" <?php echo set_checkbox('Permissions[]', 'Email.DesignPreview'); ?> /> <label for="Permission-Email.DesignPreview"><?php InterfaceLanguage('Screen', '0534', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="EmailTemplates.Manage" id="Permission-EmailTemplates.Manage" <?php echo set_checkbox('Permissions[]', 'EmailTemplates.Manage'); ?> /> <label for="Permission-EmailTemplates.Manage"><?php InterfaceLanguage('Screen', '0535', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Campaign.Create" id="Permission-Campaign.Create" <?php echo set_checkbox('Permissions[]', 'Campaign.Create'); ?> /> <label for="Permission-Campaign.Create"><?php InterfaceLanguage('Screen', '0536', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Campaign.Update" id="Permission-Campaign.Update" <?php echo set_checkbox('Permissions[]', 'Campaign.Update'); ?> /> <label for="Permission-Campaign.Update"><?php InterfaceLanguage('Screen', '0537', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Campaigns.Get" id="Permission-Campaigns.Get" <?php echo set_checkbox('Permissions[]', 'Campaigns.Get'); ?> /> <label for="Permission-Campaigns.Get"><?php InterfaceLanguage('Screen', '0538', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Campaign.Get" id="Permission-Campaign.Get" <?php echo set_checkbox('Permissions[]', 'Campaign.Get'); ?> /> <label for="Permission-Campaign.Get"><?php InterfaceLanguage('Screen', '0539', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Campaign.Delete" id="Permission-Campaign.Delete" <?php echo set_checkbox('Permissions[]', 'Campaign.Delete'); ?> /> <label for="Permission-Campaign.Delete"><?php InterfaceLanguage('Screen', '0540', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="List.Create" id="Permission-List.Create" <?php echo set_checkbox('Permissions[]', 'List.Create'); ?> /> <label for="Permission-List.Create"><?php InterfaceLanguage('Screen', '0542', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="List.Update" id="Permission-List.Update" <?php echo set_checkbox('Permissions[]', 'List.Update'); ?> /> <label for="Permission-List.Update"><?php InterfaceLanguage('Screen', '0543', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="List.Delete" id="Permission-List.Delete" <?php echo set_checkbox('Permissions[]', 'List.Delete'); ?> /> <label for="Permission-List.Delete"><?php InterfaceLanguage('Screen', '0544', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Lists.Get" id="Permission-Lists.Get" <?php echo set_checkbox('Permissions[]', 'Lists.Get'); ?> /> <label for="Permission-Lists.Get"><?php InterfaceLanguage('Screen', '0545', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="List.Get" id="Permission-List.Get" <?php echo set_checkbox('Permissions[]', 'List.Get'); ?> /> <label for="Permission-List.Get"><?php InterfaceLanguage('Screen', '0546', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0547', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="CustomField.Create" id="Permission-CustomField.Create" <?php echo set_checkbox('Permissions[]', 'CustomField.Create'); ?> /> <label for="Permission-CustomField.Create"><?php InterfaceLanguage('Screen', '0548', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="CustomField.Update" id="Permission-CustomField.Update" <?php echo set_checkbox('Permissions[]', 'CustomField.Update'); ?> /> <label for="Permission-CustomField.Update"><?php InterfaceLanguage('Screen', '0549', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="CustomFields.Get" id="Permission-CustomFields.Get" <?php echo set_checkbox('Permissions[]', 'CustomFields.Get'); ?> /> <label for="Permission-CustomFields.Get"><?php InterfaceLanguage('Screen', '0550', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="CustomFields.Delete" id="Permission-CustomFields.Delete" <?php echo set_checkbox('Permissions[]', 'CustomFields.Delete'); ?> /> <label for="Permission-CustomFields.Delete"><?php InterfaceLanguage('Screen', '0551', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0552', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Media.Upload" id="Permission-Media.Upload" <?php echo set_checkbox('Permissions[]', 'Media.Upload'); ?> /> <label for="Permission-Media.Upload"><?php InterfaceLanguage('Screen', '0553', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Media.Browse" id="Permission-Media.Browse" <?php echo set_checkbox('Permissions[]', 'Media.Browse'); ?> /> <label for="Permission-Media.Browse"><?php InterfaceLanguage('Screen', '0554', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Media.Retrieve" id="Permission-Media.Retrieve" <?php echo set_checkbox('Permissions[]', 'Media.Retrieve'); ?> /> <label for="Permission-Media.Retrieve"><?php InterfaceLanguage('Screen', '0555', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Media.Delete" id="Permission-Media.Delete" <?php echo set_checkbox('Permissions[]', 'Media.Delete'); ?> /> <label for="Permission-Media.Delete"><?php InterfaceLanguage('Screen', '0556', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0557', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="AutoResponder.Create" id="Permission-AutoResponder.Create" <?php echo set_checkbox('Permissions[]', 'AutoResponder.Create'); ?> /> <label for="Permission-AutoResponder.Create"><?php InterfaceLanguage('Screen', '0558', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="AutoResponders.Get" id="Permission-AutoResponders.Get" <?php echo set_checkbox('Permissions[]', 'AutoResponders.Get'); ?> /> <label for="Permission-AutoResponders.Get"><?php InterfaceLanguage('Screen', '0559', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="AutoResponders.Delete" id="Permission-AutoResponders.Delete" <?php echo set_checkbox('Permissions[]', 'AutoResponders.Delete'); ?> /> <label for="Permission-AutoResponders.Delete"><?php InterfaceLanguage('Screen', '0560', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="AutoResponder.Get" id="Permission-AutoResponder.Get" <?php echo set_checkbox('Permissions[]', 'AutoResponder.Get'); ?> /> <label for="Permission-AutoResponder.Get"><?php InterfaceLanguage('Screen', '0561', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="AutoResponder.Update" id="Permission-AutoResponder.Update" <?php echo set_checkbox('Permissions[]', 'AutoResponder.Update'); ?> /> <label for="Permission-AutoResponder.Update"><?php InterfaceLanguage('Screen', '0562', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Segments.Get" id="Permission-Segments.Get" <?php echo set_checkbox('Permissions[]', 'Segments.Get'); ?> /> <label for="Permission-Segments.Get"><?php InterfaceLanguage('Screen', '0564', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Segments.Delete" id="Permission-Segments.Delete" <?php echo set_checkbox('Permissions[]', 'Segments.Delete'); ?> /> <label for="Permission-Segments.Delete"><?php InterfaceLanguage('Screen', '0565', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Segment.Update" id="Permission-Segment.Update" <?php echo set_checkbox('Permissions[]', 'Segment.Update'); ?> /> <label for="Permission-Segment.Update"><?php InterfaceLanguage('Screen', '0566', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Segment.Create" id="Permission-Segment.Create" <?php echo set_checkbox('Permissions[]', 'Segment.Create'); ?> /> <label for="Permission-Segment.Create"><?php InterfaceLanguage('Screen', '0567', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Subscribers.Get" id="Permission-Subscribers.Get" <?php echo set_checkbox('Permissions[]', 'Subscribers.Get'); ?> /> <label for="Permission-Subscribers.Get"><?php InterfaceLanguage('Screen', '0568', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Subscriber.Update" id="Permission-Subscriber.Update" <?php echo set_checkbox('Permissions[]', 'Subscriber.Update'); ?> /> <label for="Permission-Subscriber.Update"><?php InterfaceLanguage('Screen', '0569', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Subscribers.Delete" id="Permission-Subscribers.Delete" <?php echo set_checkbox('Permissions[]', 'Subscribers.Delete'); ?> /> <label for="Permission-Subscribers.Delete"><?php InterfaceLanguage('Screen', '0570', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Subscribers.Import" id="Permission-Subscribers.Import" <?php echo set_checkbox('Permissions[]', 'Subscribers.Import'); ?> /> <label for="Permission-Subscribers.Import"><?php InterfaceLanguage('Screen', '0571', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <label><?php InterfaceLanguage('Screen', '0572', false, '', false, true); ?></label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Clients.Get" id="Permission-Clients.Get" <?php echo set_checkbox('Permissions[]', 'Clients.Get'); ?> /> <label for="Permission-Clients.Get"><?php InterfaceLanguage('Screen', '0573', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Clients.Delete" id="Permission-Clients.Delete" <?php echo set_checkbox('Permissions[]', 'Clients.Delete'); ?> /> <label for="Permission-Clients.Delete"><?php InterfaceLanguage('Screen', '0574', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Client.Create" id="Permission-Client.Create" <?php echo set_checkbox('Permissions[]', 'Client.Create'); ?> /> <label for="Permission-Client.Create"><?php InterfaceLanguage('Screen', '0575', false, '', false); ?></label>
                                    </div>
                                    <div class="checkbox-row">
                                        <input type="checkbox" name="Permissions[]" value="Client.Update" id="Permission-Client.Update" <?php echo set_checkbox('Permissions[]', 'Client.Update'); ?> /> <label for="Permission-Client.Update"><?php InterfaceLanguage('Screen', '0576', false, '', false); ?></label>
                                    </div>
                                </div>
                            </div>
                            <?php if (count($Plugins) > 0): ?>
                                <div class="form-row">
                                    <label><?php InterfaceLanguage('Screen', '0131', false, '', false, true); ?></label>
                                    <div class="checkbox-container">
                                        <?php foreach ($Plugins as $EachPlugin): ?>
                                            <div class="checkbox-row">
                                                <input type="checkbox" name="Permissions[]" value="Plugin.<?php print($EachPlugin['Code']); ?>" id="Permission-Plugin.<?php print($EachPlugin['Code']); ?>"> <label for="Permission-Plugin.<?php print($EachPlugin['Code']); ?>"><?php print($EachPlugin['Name']); ?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- Permissions Section - END -->

                        <!-- User interface section - START -->
                        <div tabcollection="user-group-tabs" id="tab-content-5">
                            <input type="hidden" name="RelThemeID" value="<?php print($UserGroup['RelThemeID']); ?>" id="RelThemeID">
                            <div class="form-row <?php print((form_error('Template') != '' ? 'error' : '')); ?>" id="form-row-template-select">
                                <label><?php InterfaceLanguage('Screen', '0583', false, '', false, true); ?></label>
                                <select name="Template" id="template-select" class="select">
                                    <option value=""><?php InterfaceLanguage('Screen', '0578', false, '', false, false); ?></option>
                                    <?php foreach ($Templates as $EachTemplate): ?>
                                        <option value="<?php print($EachTemplate['Code']); ?>" <?php echo set_select('Template', $EachTemplate['Code'], ($EachTemplate['Code'] == $UserGroup['ThemeInformation']['Template'] ? true : false)); ?> ><?php print($EachTemplate['Name']); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php print(form_error('Template', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                            </div>

                            <?php foreach ($Templates as $EachTemplate): ?>
                                <div id="css-settings-<?php print($EachTemplate['Code']); ?>" class="theme-css-settings-containers" style="display:none">
                                    <?php foreach ($CSSSettings[$EachTemplate['Code']] as $EachSetting): ?>
                                        <div class="form-row" id="form-row-<?php print($EachTemplate['Code'] . $EachSetting['Tag']); ?>">
                                            <label for="<?php print($EachTemplate['Code'] . $EachSetting['Tag']); ?>" style="width:200px;"><?php print($EachSetting['Description']); ?></label>
                                            <input type="text" name="<?php echo $EachTemplate['Code']; ?>_ThemeCSSSettings_<?php print($EachSetting['Tag']); ?>" id="<?php print($EachTemplate['Code'] . $EachSetting['Tag']); ?>" value="<?php print ($UserGroup['ThemeInformation']['Template'] == $EachTemplate['Code'] ? $UserGroup['ThemeInformation']['ArrayThemeSettings'][$EachSetting['Tag']] : $EachSetting['Default']); ?>" class="text color-input" />
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- User interface section - END -->
                    </div>
                </div>
            </div>
        </div>
        <div class="span-18 last">
            <div class="form-action-container">
                <a class="button" targetform="UserGroupEditForm"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                <input type="hidden" name="Command" value="SaveUserGroup" id="Command" />
            </div>
        </div>
    </form>
</div>
<!-- Page - End -->

<div id="colorpicker"></div>
<script>
    var permissions = '<?php print($UserGroup['Permissions']); ?>';
    var pricing_range_count = 0;
    var pricing_range_last_id = 0;
    var pricing_range_first_row_template = '<div class="checkbox-row pricing-range-row" id="pricing-range-row-__" style="margin-bottom:3px">';
    pricing_range_first_row_template += '<?php InterfaceLanguage('Screen', '0611', false, '', false, false, array('<input type="text" name="pricing-range-__-emails" value="" id="pricing-range-__-emails" class="text" style="width:40px" />', '<input type="text" name="pricing-range-__-cost" value="" id="pricing-range-__-cost" class="text" style="width:40px" />', PAYMENT_CURRENCY)); ?>';
    pricing_range_first_row_template += '&nbsp;<input type="button" value="-" id="pricing-range-remove-button-__" class="pricing-range-remove-buttons" /></div>';
    var pricing_range_row_template = '<div class="checkbox-row pricing-range-row" id="pricing-range-row-__" style="margin-bottom:3px">';
    pricing_range_row_template += '<?php InterfaceLanguage('Screen', '0612', false, '', false, false, array('<input type="text" name="pricing-range-__-emails" value="" id="pricing-range-__-emails" class="text" style="width:40px" />', '<input type="text" name="pricing-range-__-cost" value="" id="pricing-range-__-cost" class="text" style="width:40px" />', PAYMENT_CURRENCY)); ?>';
    pricing_range_row_template += '&nbsp;<input type="button" value="-" id="pricing-range-remove-button-__" class="pricing-range-remove-buttons" /></div>';
    var pricing_credit_range_first_row_template = '<div class="checkbox-row pricing-range-row" id="pricing-range-row-__" style="margin-bottom:3px">';
    pricing_credit_range_first_row_template += '<?php InterfaceLanguage('Screen', '1489', false, '', false, false, array('<input type="text" name="pricing-range-__-emails" value="" id="pricing-range-__-emails" class="text" style="width:40px" />', '<input type="text" name="pricing-range-__-cost" value="" id="pricing-range-__-cost" class="text" style="width:40px" />', PAYMENT_CURRENCY)); ?>';
    pricing_credit_range_first_row_template += '&nbsp;<input type="button" value="-" id="pricing-range-remove-button-__" class="pricing-range-remove-buttons" /></div>';
    var pricing_credit_range_row_template = '<div class="checkbox-row pricing-range-row" id="pricing-range-row-__" style="margin-bottom:3px">';
    pricing_credit_range_row_template += '<?php InterfaceLanguage('Screen', '1490', false, '', false, false, array('<input type="text" name="pricing-range-__-emails" value="" id="pricing-range-__-emails" class="text" style="width:40px" />', '<input type="text" name="pricing-range-__-cost" value="" id="pricing-range-__-cost" class="text" style="width:40px" />', PAYMENT_CURRENCY)); ?>';
    pricing_credit_range_row_template += '&nbsp;<input type="button" value="-" id="pricing-range-remove-button-__" class="pricing-range-remove-buttons" /></div>';
</script>
<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/create_user_group.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php InterfaceTemplateURL(false); ?>js/farbtastic.js" type="text/javascript" charset="utf-8"></script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_footer.php'); ?>
