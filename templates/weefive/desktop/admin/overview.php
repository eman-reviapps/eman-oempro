<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<div class="container" style="margin-top:18px;">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs" tabcallback="switch_between_forecast_and_history">
						<li id="tab-report-start"><a href="#"><?php InterfaceLanguage('Screen', '1908', false, '', false, true)?></a></li>
						<?php if ($TotalUsers > 0): ?>
						<li id="tab-report-forecast" class="selected"><a href="#"><?php InterfaceLanguage('Screen', '0685', false, '', false, true)?></a></li>
						<li id="tab-report-history"><a href="#"><?php InterfaceLanguage('Screen', '1394', false, '', false, true)?></a></li>
						<?php endif; ?>
					</ul>
					<ul id="history-date-ranges" class="livetabs right" tabcollection="report-history-date-tabs" tabcallback="switch_chart_data_range" style="display:none">
						<li class="label"><?php InterfaceLanguage('Screen', '0093', false, '', false); ?></li>
						<li id="tab-1m"><?php InterfaceLanguage('Screen', '0973', false, '', false, false); ?></li>
						<li id="tab-3m"><?php InterfaceLanguage('Screen', '0974', false, '', false, false); ?></li>
						<li id="tab-6m"><?php InterfaceLanguage('Screen', '1395', false, '', false, false); ?></li>
					</ul>
				</div>
				<div class="white">
					<div tabcollection="report-tabs" id="tab-content-report-start">
						<div class="inner" style="padding:18px;">
							<p><?php InterfaceLanguage('Screen', '1909'); ?></p>
							<p style="margin-bottom:0;"><strong><?php InterfaceLanguage('Screen', '1910'); ?></strong></p>
							<ol style="margin:0;list-style:inside;">
								<li><a href="<?php InterfaceAppURL(); ?>/admin/emaildelivery/"><?php InterfaceLanguage('Screen', '1911'); ?></a></li>
								<li><a href="<?php InterfaceAppURL(); ?>/admin/users/create/"><?php InterfaceLanguage('Screen', '1912'); ?></a></li>
								<li><a href="<?php InterfaceAppURL(); ?>/admin/users/browse/"><?php InterfaceLanguage('Screen', '1913'); ?></a></li>
							</ol>
						</div>
					</div>
					<div tabcollection="report-tabs" id="tab-content-report-forecast">
						<div class="inner flash-chart-container" id="forecast-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/admin/overview/ChartData/DeliveryForecast" style="height:150px;">
						</div>
					</div>
					<div tabcollection="report-tabs" id="tab-content-report-history">
						<div class="inner flash-chart-container" id="history-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/admin/overview/ChartData/DeliveryHistory" style="height:150px;">
						</div>
					</div>
				</div>
				<?php if ($TotalUsers > 0): ?>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-2">
						<li id="tab-top-users"><a href="#"><?php InterfaceLanguage('Screen', '0686', false, '', false, true); ?></a></li>
						<li id="tab-online-users"><a href="#"><?php InterfaceLanguage('Screen', '0045', false, '', false, true); ?></a></li>
						<li id="tab-top-bounces"><a href="#"><?php InterfaceLanguage('Screen', '0687', false, '', false, true); ?></a></li>
						<li id="tab-top-complaints"><a href="#"><?php InterfaceLanguage('Screen', '0688', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-2" id="tab-content-top-users">
							<?php
							if (count($ActiveUsers) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0689', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($ActiveUsers as $Each):
									?>
										<tr>
											<td width="30%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each['UserInformation']['UserID']); ?>/"><?php print($Each['UserInformation']['FirstName'].' '.$Each['UserInformation']['LastName']); ?></a></td>
											<td width="20%"><span class="data"><?php print($Each['TotalCampaigns']); ?></span> <?php InterfaceLanguage('Screen', '0140', false, '', false, false); ?></td>
											<td width="50%"><span class="data"><?php print($Each['TotalLists']); ?></span> <?php InterfaceLanguage('Screen', '0100', false, '', false, false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
						<div tabcollection="report-tabs-2" id="tab-content-online-users">
							<?php
							if (count($OnlineUsers) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0690', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($OnlineUsers as $Each):
									?>
										<tr>
											<td width="100%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each->UserID); ?>/"><?php print($Each->FirstName.' '.$Each->LastName); ?></a></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
						<div tabcollection="report-tabs-2" id="tab-content-top-bounces">
							<?php
							if (count($BounceUsers) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0691', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($BounceUsers as $Each):
									?>
										<tr>
											<td width="30%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each['UserInformation']['UserID']); ?>/"><?php print($Each['UserInformation']['FirstName'].' '.$Each['UserInformation']['LastName']); ?></a></td>
											<td width="70%"><span class="data"><?php print($Each['TotalBounces']); ?></span> <?php InterfaceLanguage('Screen', '0098', false, '', false, false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
						<div tabcollection="report-tabs-2" id="tab-content-top-complaints">
							<?php
							if (count($ComplaintUsers) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0692', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($ComplaintUsers as $Each):
									?>
										<tr>
											<td width="30%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each['UserInformation']['UserID']); ?>/"><?php print($Each['UserInformation']['FirstName'].' '.$Each['UserInformation']['LastName']); ?></a></td>
											<td width="70%"><span class="data"><?php print($Each['TotalComplaints']); ?></span> <?php InterfaceLanguage('Screen', '0095', false, '', false, false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
					</div>
				</div>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-3">
						<li id="tab-pending-campaigns"><a href="#"><?php InterfaceLanguage('Screen', '1800', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-3" id="tab-content-pending-campaigns">
							<?php if (count($ApprovalPendingCampaigns) < 1) { ?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '1801'); ?></p>
							<?php } else { ?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php foreach ($ApprovalPendingCampaigns as $each) { ?>
										<tr class="pending-campaign-row" data-campaign-id="<?php echo $each['CampaignID']; ?>">
											<td>
												<span style="font-size:14px;line-height:21px;"><?php echo $each['Email']['Subject']; ?></span><br>
												<span class="data-label small" style="margin-left:0;">From: </span> <span class="data small"><?php echo $each['Email']['FromName']; ?> &lt;<?php echo $each['Email']['FromEmail']; ?>&gt;</span>
												<span class="data-label small">Username: </span> <span class="data small"><?php echo $each['UserInformation']['Username']; ?></span><br>
												<a href="<?php InterfaceAppURL(); ?>/admin/email/preview/<?php echo $each['Email']['EmailID']; ?>/html/campaign/<?php echo $each['CampaignID']; ?>" class="small" target="_blank"><?php InterfaceLanguage('Screen', '1802'); ?></a>
												<a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php echo $each['UserInformation']['UserID']; ?>/SendMessage" style="margin-left:18px;" class="small"><?php InterfaceLanguage('Screen', '1804'); ?></a>
												<a data-campaign-id="<?php echo $each['CampaignID']; ?>" href="#" style="margin-left:18px;" class="approve-campaign small"><?php InterfaceLanguage('Screen', '1803'); ?></a>
											</td>
										</tr>
									<?php } ?>
								</table>
								<p class="no-data-message" id="no-campaign-pending-message" style="display:none"><?php InterfaceLanguage('Screen', '1801'); ?></p>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="help-column span-5 push-1 last">
		<div style="margin-bottom:18px;">
			<h3><?php InterfaceLanguage('Screen', '0693', false, '', false, true); ?></h3>
			<a class="button" href="<?php InterfaceAppURL(); ?>/admin/users/create/"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0013', false, '', true); ?></strong></a>
			<a class="button" href="<?php InterfaceAppURL(); ?>/admin/usergroups/create/" style="margin-top:6px;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0060', false, '', true); ?></strong></a>
			<a class="button" href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/create/" style="margin-top:6px;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0114', false, '', true); ?></strong></a>
		</div>
		<?php if (is_array($LatestProductNews) == true && count($LatestProductNews) > 0): ?>
			<div style="margin-bottom:18px;overflow-y: auto; max-height:250px;">
				<h3><?php InterfaceLanguage('Screen', '1932', false, '', false, true); ?></h3>
				<style>
					ul.product-updates {
						padding:0;
						margin:0;
					}

					ul.product-updates li {
						padding:0;
						margin:0 0 10px 0;
					}

					ul.product-updates span.label.new_feature {
						background-color:#517B44;
						border-radius:2px;
						color:#fff;
						font-weight:bold;
						padding:3px 5px;
						font-size:0.75em;
					}

					ul.product-updates span.label.info {
						background-color: #75bad4;
						border-radius:2px;
						color:#fff;
						font-weight:bold;
						padding:3px 5px;
						font-size:0.75em;
					}

					ul.product-updates span.label.update {
						background-color: #ddb055;
						border-radius:2px;
						color:#fff;
						font-weight:bold;
						padding:3px 5px;
						font-size:0.75em;
					}

					ul.product-updates span.date {
						font-size:0.75em;
						color:#a1a1a1;
					}
				</style>
				<ul class="product-updates">
					<?php foreach ($LatestProductNews as $Index=>$EachNews): ?>
						<li>
							<?php if ($EachNews->Tags != ''): ?>
								<?php
								$Tag = explode(',', $EachNews->Tags);
								$Tag = $Tag[0];
								?>
								<span class="label <?php print(strtolower(str_replace(' ', '_', $Tag))); ?>"><?php print(strtoupper($Tag)); ?></span>
							<?php endif; ?>
							<?php if ($EachNews->RedirectURL != ''): ?>
								<a href="<?php print($EachNews->RedirectURL); ?>" target="_blank" style="text-decoration: none;"><?php print($EachNews->Title); ?></a>
							<?php else: ?>
								<?php print($EachNews->Title); ?>
							<?php endif; ?>
							<br><span class="date"><?php print(date('jS F, Y', strtotime($EachNews->PublishDate))); ?></span>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
</div>

<script>
var APP_URL = '<?php InterfaceAppURL(); ?>';
var Language = {
	'1808' : "<?php InterfaceLanguage('Screen', '1808'); ?>"
};
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/overview.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
