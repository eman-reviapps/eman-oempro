				<form id="Uploads" method="post" action="<?php InterfaceAppURL(); ?>/admin/uploadsandmedialibrary/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<ul class="livetabs" tabcollection="form-tabs">
											<li id="tab-1">
												<a href="#"><?php InterfaceLanguage('Screen', '0209', false, '', false); ?></a>
											</li>
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0220', false, '', false); ?></a>
											</li>
										</ul>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>

										<!-- Media Library - START -->
										<div tabcollection="form-tabs" id="tab-content-1">
											<div class="form-row <?php print((form_error('MEDIA_UPLOAD_METHOD') != '' ? 'error' : '')); ?>" id="form-row-MEDIA_UPLOAD_METHOD">
												<label for="MEDIA_UPLOAD_METHOD"><?php InterfaceLanguage('Screen', '0210', false); ?>:</label>
												<div class="checkbox-container">
													<div class="checkbox-row">
														<input type="radio" name="MEDIA_UPLOAD_METHOD" value="file" id="MEDIA_UPLOAD_METHOD_file" <?php echo set_radio('MEDIA_UPLOAD_METHOD', 'file', (MEDIA_UPLOAD_METHOD == 'file' ? true : false)); ?>> <label for="MEDIA_UPLOAD_METHOD_file"><?php InterfaceLanguage('Screen', '0211', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="radio" name="MEDIA_UPLOAD_METHOD" value="database" id="MEDIA_UPLOAD_METHOD_database" <?php echo set_radio('MEDIA_UPLOAD_METHOD', 'database', (MEDIA_UPLOAD_METHOD == 'database' ? true : false)); ?>> <label for="MEDIA_UPLOAD_METHOD_database"><?php InterfaceLanguage('Screen', '0212', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="radio" name="MEDIA_UPLOAD_METHOD" value="s3" id="MEDIA_UPLOAD_METHOD_s3" <?php echo set_radio('MEDIA_UPLOAD_METHOD', 's3', (MEDIA_UPLOAD_METHOD == 's3' ? true : false)); ?>> <label for="MEDIA_UPLOAD_METHOD_s3"><?php InterfaceLanguage('Screen', '0213', false, '', false); ?></label>
													</div>
												</div>
												<?php print(form_error('MEDIA_UPLOAD_METHOD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div id="s3-upload-settings">
												<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0214', false); ?></h3>
												<div class="form-row <?php print((form_error('S3_ACCESS_ID') != '' ? 'error' : '')); ?>" id="form-row-S3_ACCESS_ID">
													<label for="S3_ACCESS_ID"><?php InterfaceLanguage('Screen', '0215', false); ?>: *</label>
													<input type="text" name="S3_ACCESS_ID" value="<?php echo set_value('S3_ACCESS_ID', S3_ACCESS_ID); ?>" id="S3_ACCESS_ID" class="text" />
													<?php print(form_error('S3_ACCESS_ID', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('S3_SECRET_KEY') != '' ? 'error' : '')); ?>" id="form-row-S3_SECRET_KEY">
													<label for="S3_SECRET_KEY"><?php InterfaceLanguage('Screen', '0216', false); ?>: *</label>
													<input type="text" name="S3_SECRET_KEY" value="<?php echo set_value('S3_SECRET_KEY', S3_SECRET_KEY); ?>" id="S3_SECRET_KEY" class="text" />
													<?php print(form_error('S3_SECRET_KEY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('S3_BUCKET') != '' ? 'error' : '')); ?>" id="form-row-S3_BUCKET">
													<label for="S3_BUCKET"><?php InterfaceLanguage('Screen', '0217', false); ?>: *</label>
													<input type="text" name="S3_BUCKET" value="<?php echo set_value('S3_BUCKET', S3_BUCKET); ?>" id="S3_BUCKET" class="text" />
													<?php print(form_error('S3_BUCKET', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('S3_MEDIALIBRARY_PATH') != '' ? 'error' : '')); ?>" id="form-row-S3_MEDIALIBRARY_PATH">
													<label for="S3_MEDIALIBRARY_PATH"><?php InterfaceLanguage('Screen', '0218', false); ?>: *</label>
													<input type="text" name="S3_MEDIALIBRARY_PATH" value="<?php echo set_value('S3_MEDIALIBRARY_PATH', S3_MEDIALIBRARY_PATH); ?>" id="S3_MEDIALIBRARY_PATH" class="text" />
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1846', false, '', false); ?></p></div>
													<?php print(form_error('S3_MEDIALIBRARY_PATH', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('S3_URL') != '' ? 'error' : '')); ?>" id="form-row-S3_URL">
													<label for="S3_URL"><?php InterfaceLanguage('Screen', '0219', false); ?>: *</label>
													<input type="text" name="S3_URL" value="<?php echo set_value('S3_URL', S3_URL); ?>" id="S3_URL" class="text" />
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1846', false, '', false); ?></p></div>
													<?php print(form_error('S3_URL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
											</div>
										</div>
										<!-- Media Library - END -->

										<!-- File Upload Limits - START -->
										<div tabcollection="form-tabs" id="tab-content-2">
											<div class="form-row <?php print((form_error('IMPORT_MAX_FILESIZE') != '' ? 'error' : '')); ?>" id="form-row-IMPORT_MAX_FILESIZE">
												<label for="IMPORT_MAX_FILESIZE"><?php InterfaceLanguage('Screen', '0221', false); ?>: *</label>
												<input type="text" name="IMPORT_MAX_FILESIZE" value="<?php echo set_value('IMPORT_MAX_FILESIZE', IMPORT_MAX_FILESIZE); ?>" id="IMPORT_MAX_FILESIZE" class="text" />
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0222', false, '', false); ?></p></div>
												<?php print(form_error('IMPORT_MAX_FILESIZE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('ATTACHMENT_MAX_FILESIZE') != '' ? 'error' : '')); ?>" id="form-row-ATTACHMENT_MAX_FILESIZE">
												<label for="ATTACHMENT_MAX_FILESIZE"><?php InterfaceLanguage('Screen', '0223', false); ?>: *</label>
												<input type="text" name="ATTACHMENT_MAX_FILESIZE" value="<?php echo set_value('ATTACHMENT_MAX_FILESIZE', ATTACHMENT_MAX_FILESIZE); ?>" id="ATTACHMENT_MAX_FILESIZE" class="text" />
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0222', false, '', false); ?></p></div>
												<?php print(form_error('ATTACHMENT_MAX_FILESIZE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('MEDIA_MAX_FILESIZE') != '' ? 'error' : '')); ?>" id="form-row-MEDIA_MAX_FILESIZE">
												<label for="MEDIA_MAX_FILESIZE"><?php InterfaceLanguage('Screen', '0224', false); ?>: *</label>
												<input type="text" name="MEDIA_MAX_FILESIZE" value="<?php echo set_value('MEDIA_MAX_FILESIZE', MEDIA_MAX_FILESIZE); ?>" id="MEDIA_MAX_FILESIZE" class="text" />
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0222', false, '', false); ?></p></div>
												<?php print(form_error('MEDIA_MAX_FILESIZE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>
										<!-- File Upload Limits - END -->
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="Uploads" id="ButtonUploads"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0273', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="Uploads" id="Command" />
						</div>
					</div>
				</form>

				<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/settings_uploadsandmedialibrary.js" type="text/javascript" charset="utf-8"></script>		
