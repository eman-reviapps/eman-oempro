<?php include_once(TEMPLATE_PATH . 'desktop/layouts/login_header.php'); ?>

<div class="content">
    <form id="Form_Login" class="login-form" method="post" action="<?php InterfaceAppURL(); ?>/admin/">
        <h3 class="form-title font-purple-sharp"><?php InterfaceLanguage('Screen', '0001', false); ?></h3>
        <?php
        if (isset($PageErrorMessage) == true):
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php print($PageErrorMessage); ?>
            </div>
            <?php
        elseif (isset($PageSuccessMessage) == true):
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php print($PageSuccessMessage); ?>
            </div>
            <?php
        endif;
        ?>
        <div class="form-group <?php print((form_error('Username') != '' ? 'error' : '')); ?> no-background-color" id="form-row-Username">
            <label class="control-label visible-ie8 visible-ie9" for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>:</label>
            <input type="text" name="Username" value="<?php echo set_value('Username', ''); ?>" id="Username" placeholder="<?php InterfaceLanguage('Screen', '0002', false); ?>" class="form-control form-control-solid placeholder-no-fix" />
            <?php print(form_error('Username', '<div class="form-group-note error"><p>', '</p></div>')); ?>
        </div>
        <div class="form-group <?php print((form_error('Password') != '' ? 'error' : '')); ?> no-background-color" id="form-row-Password">
            <label class="control-label visible-ie8 visible-ie9" for="Password"><?php InterfaceLanguage('Screen', '0003', false); ?>:</label>
            <input type="password" name="Password" value="<?php echo set_value('Password', ''); ?>" id="Password" placeholder="<?php InterfaceLanguage('Screen', '0003', false); ?>" class="form-control form-control-solid placeholder-no-fix" />
            <?php print(form_error('Password', '<div class="form-group-note error"><p>', '</p></div>')); ?>
        </div>
        <?php
        if (ADMIN_CAPTCHA == true):
            ?>
            <div class="form-group captcha <?php print((form_error('Captcha') != '' ? 'error' : '')); ?> no-background-color" id="form-row-Captcha">
                <label for="Captcha"><?php InterfaceLanguage('Screen', '0004', false); ?>:</label>
                <img src="<?php InterfaceAppURL(); ?>/admin/captcha/index/AdminCaptcha" id="captcha-image" />
                <input type="text" name="Captcha" value="<?php echo set_value('Captcha', ''); ?>" id="Captcha" class="text" style="width:160px;" /> 
                <?php print(form_error('Captcha', '<div class="form-group-note error"><p>', '</p></div>')); ?>
            </div>
            <?php
        endif;
        ?>
        <div class="form-actions">
            <a id="FormButton_Submit" class="btn purple uppercase" targetform="Form_Login"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0266', false, '', true); ?></strong></a>
            <input type="hidden" name="Command" value="Login" id="Command" />

            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="RememberMe" value="Yes" id="RememberMe" <?php echo set_checkbox('RememberMe', 'Yes'); ?>> <label for="RememberMe"><?php InterfaceLanguage('Screen', '0005', false); ?></label>
                <span></span>
            </label>
        </div>
        
        <div class="create-account">
            <p>
                <a href="<?php InterfaceAppURL(); ?>/admin/passwordreminder/"><?php InterfaceLanguage('Screen', '0006', false); ?></a>
            </p>
        </div>
    </form>
</div>


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/login_footer.php'); ?>
