<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_campaign_preview_header.php'); ?>
<div id="top" class="iguana">
	<div class="container">
		<div class="span-24">
			<ul class="tabs">
				<li class="label"><?php InterfaceLanguage('Screen', '0988', false, '', false); ?></li>
				<?php if ($EmailInformation['ContentType'] != 'Plain'): ?>
				<li <?php if($Type == 'html'): ?>class="selected"<?php endif; ?>>
					<a href="<?php echo InterfaceAppURL('true').'/user/email/preview/'.$EmailID.'/html/'.$Mode.'/'.$EntityID; ?>">
						<?php if ($Type == 'html'): ?>
							<span class="left">&nbsp;</span>
							<span class="right">&nbsp;</span>
						<?php endif ?>
						<strong><?php InterfaceLanguage('Screen', '0175', false, '', true, false, array()); ?></strong>
					</a>
				</li>
				<?php endif; ?>
				<?php if ($EmailInformation['ContentType'] != 'HTML'): ?>
				<li <?php if($Type == 'plain'): ?>class="selected"<?php endif; ?>>
					<a href="<?php echo InterfaceAppURL('true').'/user/email/preview/'.$EmailID.'/plain/'.$Mode.'/'.$EntityID; ?>">
						<?php if ($Type == 'plain'): ?>
							<span class="left">&nbsp;</span>
							<span class="right">&nbsp;</span>
						<?php endif ?>
						<strong><?php InterfaceLanguage('Screen', '0176', false, '', true, false, array()); ?></strong>
					</a>
				</li>
				<?php endif; ?>
			</ul>
			<ul class="tabs right">
				<li>
					<a href="#" class="transparent" id="toggle-details"></a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
	<div class="container" style="padding-bottom:18px;" id="details">
		<div class="span-24 last">
			<div class="form-row">
				<label for="From"><?php InterfaceLanguage('Screen', '1817', false); ?>:</label>
				<input type="text" name="From" value="<?php echo $EmailInformation['FromName'] . ' &lt;' . $EmailInformation['FromEmail'] . '&gt;'; ?>" id="From" class="text" readonly="readonly" style="width:750px;"/>
			</div>
			<div class="form-row">
				<label for="Subject"><?php InterfaceLanguage('Screen', '0106', false); ?>:</label>
				<input type="text" name="Subject" value="<?php echo $EmailInformation['Subject']; ?>" id="Subject" class="text" readonly="readonly" style="width:750px;"/>
			</div>
		</div>
	</div>
	<div style="background-color:#fff;padding:18px;">
		<iframe src="<?php InterfaceAppURL(); ?>/admin/email/preview/<?php echo $EmailID ?>/<?php echo $Type; ?>/<?php echo $Mode; ?>/<?php echo $EntityID ?>/source" name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
	</div>
</div>

<script type="text/javascript">
	var Language = {
		'1818' : '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1818', true), ENT_QUOTES); ?>',
		'1819' : '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1819', true), ENT_QUOTES); ?>'
	};

	var toggleDetailsEl, details;
	$(document).ready(function()
	{
		toggleDetailsEl = $('#toggle-details'),
				details = $('#details');
		toggleDetailsEl.click(toggleDetails);
		toggleDetails();
	});

	function toggleDetails()
	{
		var details = $('#details');
		details.toggle();
		if (details.is(':visible')) {
			toggleDetailsEl.text(Language['1818']);
		} else {
			toggleDetailsEl.text(Language['1819']);
		}
		return false;
	}
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_campaign_preview_footer.php'); ?>