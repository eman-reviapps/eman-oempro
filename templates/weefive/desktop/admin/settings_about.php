						<div id="page-shadow">
							<div id="page">
								<?php if (isset($PageSuccessMessage) == true): ?>
									<div class="module-container">
										<div class="page-message success">
											<div class="inner">
												<span class="close">X</span>
												<?php print($PageSuccessMessage); ?>
											</div>
										</div>
									</div>
								<?php elseif (isset($PageErrorMessage) == true): ?>
									<div class="module-container">
										<div class="page-message error">
											<div class="inner">
												<span class="close">X</span>
												<?php print($PageErrorMessage); ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

								<div class="page-bar">
									<ul class="livetabs" tabcollection="form-tabs">
										<li id="tab-1">
											<a href="#"><?php InterfaceLanguage('Screen', '0167', false, '', false); ?></a>
										</li>
										<?php if (DEMO_MODE_ENABLED == false): ?>
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0168', false, '', false); ?></a>
											</li>
											<li id="tab-3">
												<a href="#"><?php InterfaceLanguage('Screen', '0463', false, '', false); ?></a>
											</li>
											<li id="tab-4">
												<a href="#"><?php InterfaceLanguage('Screen', '0481', false, '', false); ?></a>
											</li>
										<?php endif; ?>
									</ul>
								</div>
								<div class="white" style="min-height:430px;">
									<!-- Version, License and Copyright - START -->
									<div tabcollection="form-tabs" id="tab-content-1">
										<div class="custom-column-container no-padding cols-2 clearfix">
											<div class="col">
												<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0153', false); ?></h3>
												<div class="form-row" id="form-row-LicenseOwner">
													<label for="LicenseDomain"><?php InterfaceLanguage('Screen', '0160', false); ?>:</label>
													<div class="form-row-note"><p><?php print($LicenseRegistrantName); ?></p></div>
												</div>
												<div class="form-row" id="form-row-LicenseDomain">
													<label for="LicenseDomain"><?php InterfaceLanguage('Screen', '0161', false); ?>:</label>
													<div class="form-row-note"><p><?php print($LicenseAuthorizedDomain); ?></p></div>
												</div>
												<div class="form-row" id="form-row-LicenseUsers">
													<label for="LicenseUsers"><?php InterfaceLanguage('Screen', '0163', false); ?>:</label>
													<div class="form-row-note"><p><?php print($LicenseMaxUsers); ?></p></div>
												</div>
											</div>
											<div class="col">
												<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0152', false); ?></h3>
												<div class="form-row" id="form-row-ProductName">
													<label for="ProductName"><?php InterfaceLanguage('Screen', '0156', false); ?>:</label>
													<div class="form-row-note"><p><?php print(PRODUCT_NAME); ?></p></div>
												</div>
												<div class="form-row" id="form-row-CurrentVersion">
													<label for="CurrentVersion"><?php InterfaceLanguage('Screen', '0157', false); ?>:</label>
													<div class="form-row-note"><p><?php print(PRODUCT_VERSION); ?></p></div>
												</div>
												<div class="form-row" id="form-row-LatestVersion">
													<label for="LatestVersion"><?php InterfaceLanguage('Screen', '0158', false); ?>:</label>
													<div class="form-row-note"><p><?php print($LatestVersion); ?> <?php if ($NewVersionExists == true): ?> &mdash; <a href="<?php InterfaceAppURL(); ?>/admin/upgrade/"><?php InterfaceLanguage('Screen', '0159', false, '', false)?></a><?php endif;?></p></div>
												</div>
											</div>
										</div>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0155', false); ?></h3>
										<div class="form-row no-background-color" id="form-row-Copyright1">
											<p><?php InterfaceLanguage('Screen', '0258', false, '', false); ?></p>
											<p><?php InterfaceLanguage('Screen', '0259', false, '', false); ?></p>
										</div>
									</div>
									<!-- Version, License and Copyright - End -->

									<!-- System Check - START -->
									<div tabcollection="form-tabs" id="tab-content-2">
										<?php
										if (count($SystemCheckResults) > 0):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0276', false); ?></h3>
										<?php
											$PrevCategory = '';
											foreach ($SystemCheckResults as $Category=>$Errors):
												foreach ($Errors as $EachError):
													if ($PrevCategory == $Category):
										?>
													<p><?php print($EachError); ?></p>
										<?php
													else:
										?>
												<div class="form-row error" id="form-row-<?php print($Category); ?>">
													<label for="<?php print($Category); ?>"><?php print($Category); ?>:</label>
													<div class="form-row-note"><p><?php print($EachError); ?></p>
										<?php
													endif;
													$PrevCategory = $Category;
												endforeach;
										?>
													</div>
												</div>
										<?php
											endforeach;
											else:
										?>
											<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0133', false); ?></h3>
											<div class="form-row no-background-color" id="form-row-SystemCheck">
												<p><?php InterfaceLanguage('Screen', '0166', false, '', false); ?></p>
											</div>
										<?php
											endif;
										?>

										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0443', false); ?></h3>
										<div class="form-row no-background-color" id="form-row-SystemInformation">
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0444', false); ?>:</label>
												<div class="form-row-note"><p><?php print(date('r')); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0445', false); ?>:</label>
												<div class="form-row-note"><p><?php print(date_default_timezone_get()); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0446', false); ?>:</label>
												<div class="form-row-note"><p><?php print(phpversion()); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0447', false); ?>:</label>
												<div class="form-row-note"><p><?php print((ini_get('safe_mode') == false ? InterfaceLanguage('Screen', '0448', false) : InterfaceLanguage('Screen', '0449', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0450', false); ?>:</label>
												<div class="form-row-note"><p><?php print((ini_get('register_globals') == false ? InterfaceLanguage('Screen', '0448', false) : InterfaceLanguage('Screen', '0449', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0451', false); ?>:</label>
												<div class="form-row-note"><p><?php print((ini_get('magic_quotes_gpc') == false ? InterfaceLanguage('Screen', '0448', false) : InterfaceLanguage('Screen', '0449', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0452', false); ?>:</label>
												<div class="form-row-note"><p><?php print((ini_get('magic_quotes_runtime') == false ? InterfaceLanguage('Screen', '0448', false) : InterfaceLanguage('Screen', '0449', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0453', false); ?>:</label>
												<div class="form-row-note"><p><?php print(ini_get('memory_limit')); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0454', false); ?>:</label>
												<div class="form-row-note"><p><?php print((ini_get('max_execution_time') == 0 ? InterfaceLanguage('Screen', '0456', false) : ini_get('max_execution_time').' '.InterfaceLanguage('Screen', '0455', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0457', false); ?>:</label>
												<div class="form-row-note"><p><?php print((extension_loaded('curl') == false ? InterfaceLanguage('Screen', '0457', false) : InterfaceLanguage('Screen', '0458', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0460', false); ?>:</label>
												<div class="form-row-note"><p><?php print((extension_loaded('imap') == false ? InterfaceLanguage('Screen', '0457', false) : InterfaceLanguage('Screen', '0458', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0461', false); ?>:</label>
												<div class="form-row-note"><p><?php print((extension_loaded('gd') == false ? InterfaceLanguage('Screen', '0457', false) : InterfaceLanguage('Screen', '0458', false))); ?></p></div>
											</div>
											<div class="form-row">
												<label style="width:180px;"><?php InterfaceLanguage('Screen', '0462', false); ?>:</label>
												<div class="form-row-note"><p><?php print($MySQLVersion); ?></p></div>
											</div>
										</div>
										
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0165', false); ?></h3>
										<div class="form-row no-background-color" id="form-row-PhpSettings">
											<p><?php InterfaceLanguage('Screen', '0694', false, '', false, false, array(InterfaceAppURL(true).'/admin/about/phpsettings/')); ?></p>
										</div>
									</div>
									<!-- System Check - END -->

									<!-- Database Check - START -->
									<div tabcollection="form-tabs" id="tab-content-3">
										<form id="form-db-check" method="post" action="<?php InterfaceAppURL(); ?>/admin/about/#form-tabs/tab-3">
											<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1667', false); ?></h3>
											<p id="database-health-test-results" style="padding:9px 18px;"><a href="#"><?php InterfaceLanguage('Screen', '1821'); ?></a></p>

											<div class="form-row no-bg">
												<div class="form-action-container clearfix left" style="margin:0px;padding:0px;">
													<input type="hidden" name="Command" value="" id="js-command">
													<a class="button" href="#" id="js-button-optimize"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0464', false, '', true); ?></strong></a>
													<a class="button" href="#" id="js-button-repair"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0465', false, '', true); ?></strong></a>
													<a class="button" href="#" id="js-button-export"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1708', false, '', true); ?></strong></a>
												</div>
											</div>

											<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1668', false); ?></h3>
											<p id="database-structure-test-results" style="padding:9px 18px;"><a href="#"><?php InterfaceLanguage('Screen', '1822'); ?></a></p>
										</form>
									</div>
									<!-- Database Check - END -->

									<!-- File Integrity Check - START -->
									<div tabcollection="form-tabs" id="tab-content-4">
										<form id="form-file-integrity" method="post" action="<?php InterfaceAppURL(); ?>/admin/about/#form-tabs/tab-4">
											<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1824', false); ?></h3>
											<p id="file-integrity-test-results" style="padding:9px 18px;"><a href="#"><?php InterfaceLanguage('Screen', '1823'); ?></a></p>
										</form>
									</div>
									<!-- File Integrity Check - END -->
								</div>
							</div>
						</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#js-button-optimize').click(function() {
			$('#js-command').val('DatabaseOptimize');
			$('#form-db-check').submit();
			return false;
		});

		$('#js-button-repair').click(function() {
			$('#js-command').val('DatabaseRepair');
			$('#form-db-check').submit();
			return false;
		});		

		$('#js-button-export').click(function() {
			$('#js-command').val('DatabaseExport');
			$('#form-db-check').submit();
			return false;
		});

		$('#database-health-test-results a').click(function() {
			$(this).text('<?php InterfaceLanguage('Screen', '1429'); ?>');
			$.get('<?php InterfaceAppURL(); ?>/admin/about/databaseHealthTestResults/', function(data) {
				$('#database-health-test-results').html(data);
			});
			return false;
		});
		$('#database-structure-test-results a').click(function() {
			$(this).text('<?php InterfaceLanguage('Screen', '1429'); ?>');
			$.get('<?php InterfaceAppURL(); ?>/admin/about/databaseStructureTestResults/', function(data) {
				$('#database-structure-test-results').html(data);
			});
			return false;
		});
		$('#file-integrity-test-results a').click(function() {
			$(this).text('<?php InterfaceLanguage('Screen', '1429'); ?>');
			$.get('<?php InterfaceAppURL(); ?>/admin/about/fileIntegrityTestResults/', function(data) {
				$('#file-integrity-test-results').html(data);
			});
			return false;
		});
	});
</script>