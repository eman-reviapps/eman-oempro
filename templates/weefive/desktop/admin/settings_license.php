<script type="text/javascript">
	var UserAccountPrices = <?php print(json_encode($LicenseInformation['UserAccountPrices'])); ?>;

	$(document).ready(function() {
		UpdateUserAccountPrice($('#AddUserAccounts').val());

		$('#AddUserAccounts').keyup(function() {
			UpdateUserAccountPrice($('#AddUserAccounts').val());
		});
		$('#AddUserAccounts').mouseup(function() {
			UpdateUserAccountPrice($('#AddUserAccounts').val());
		});
		$('#AddUserAccounts').change(function() {
			UpdateUserAccountPrice($('#AddUserAccounts').val());
		});
		$('#ButtonLicensePurchase').click(function() {
			var Result = confirm("You will be redirected to our payment gateway to complete the new user account purchase process. Click 'Okay' to continue...");

			if (Result == true) {
				$('#LicenseInfo').submit();
				return false;
			} else {
				return false;
			}
		});
		$('#ButtonUpdateLicenseFile').click(function() {
			UpdateLicenseFile();
		});
		$('#ButtonUpdateLicenseFile2').click(function() {
			UpdateLicenseFile();
		});
		$('#ButtonUpdateLicenseFile3').click(function() {
			UpdateLicenseFile();
		});
		$('#js-service-extend-link').click(function() {
			var Result = confirm("You will be redirected to our payment gateway to complete the service purchase process. Click 'Okay' to continue...");

			if (Result == true) {
				window.location.href="<?php InterfaceAppURL(); ?>/admin/license/extend";
				return false;
			}
			return false;
		});
	});

	function UpdateLicenseFile() {
		var Result = confirm("System will try to connect and retrieve your license from Octeth servers. Your current license will be archived as data/license.dat.bck file. Click 'Okay' when you are ready.");

		if (Result == true) {
			$('#LicenseInfo').attr('action', '<?php InterfaceAppURL(); ?>/admin/license/download/');
			$('#LicenseInfo').attr('target', '');
			$('#LicenseInfo').submit();
			return false;
		} else {
			return false;
		}
	}

	function UpdateUserAccountPrice(TotalNewUserAccounts) {
		TotalNewUserAccounts = parseInt((TotalNewUserAccounts == '' || TotalNewUserAccounts == false || TotalNewUserAccounts < 0 ? 0 : TotalNewUserAccounts));
		var Tmp_PricePerUser = 0;
		var TotalCost = 0;

		$.each(UserAccountPrices, function(UserLevel, PricePerUser) {
			if (TotalNewUserAccounts < UserLevel) {
				Tmp_PricePerUser = PricePerUser;
				return false;
			}
		});

		TotalCost = Tmp_PricePerUser * TotalNewUserAccounts;

		$('#js-user-account-price').html(addCommas(parseFloat(TotalCost).toFixed(2)));
	}

	function addCommas(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
</script>

<form id="LicenseInfo" method="post" action="<?php InterfaceAppURL(); ?>/admin/license/purchase" target="_blank">
	<div id="page-shadow">
		<div id="page">
			<?php if (isset($PageSuccessMessage) == true): ?>
				<div class="module-container">
					<div class="page-message success">
						<div class="inner">
							<span class="close">X</span>
							<?php print($PageSuccessMessage); ?>
						</div>
					</div>
				</div>
			<?php elseif (isset($PageErrorMessage) == true): ?>
				<div class="module-container">
					<div class="page-message error">
						<div class="inner">
							<span class="close">X</span>
							<?php print($PageErrorMessage); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<div class="page-bar">
				<h2><?php InterfaceLanguage('Screen', '1825', false, '', true); ?></h2>
			</div>
			<div class="white" style="min-height:430px;">
				<div class="custom-column-container no-padding clearfix">
					<h3 class="form-legend">License Information</h3>
					<div class="form-row" id="form-row-LicenseKey">
						<label style="width:180px;"><?php InterfaceLanguage('Screen', '1826', false); ?>:</label>
						<div class="form-row-note" style="margin-left:200px;font-family: 'courier new';"><p><?php print($LicenseInformation['License']); ?></p></div>
					</div>
					<div class="form-row" id="form-row-LicenseUsers">
						<label style="width:180px;"><?php InterfaceLanguage('Screen', '1829', false); ?>:</label>
						<div class="form-row-note" style="margin-left:200px;">
							<p><?php print(($LicenseInformation['LicenseUsers'] == 0 ? ($LicenseProperties['MaxUsers']['value'] == -1 ? InterfaceLanguage('Screen', 1833, true) : $LicenseProperties['MaxUsers']['value']) : ($LicenseInformation['LicenseUsers'] == -1 ? InterfaceLanguage('Screen', 1833, true) : $LicenseInformation['LicenseUsers']))); ?> <?php InterfaceLanguage('Screen', 1837, false) ?></p>
							<?php if ($LicenseInformation['LicenseUsers'] != $LicenseProperties['MaxUsers']['value']): ?>
								<p><a href="#" id="ButtonUpdateLicenseFile3"><?php InterfaceLanguage('Screen', 1845, false) ?></a></p>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-row" id="form-row-RegisteredTo">
						<label style="width:180px;"><?php InterfaceLanguage('Screen', '1827', false); ?>:</label>
						<div class="form-row-note" style="margin-left:200px;"><p><?php print($LicenseInformation['RegisteredTo']); ?> &lt;<?php print($LicenseInformation['RegisteredToEmail']); ?>&gt;</p></div>
					</div>
					<?php if ($LicenseInformation['IsTrial'] == false): ?>
						<div class="form-row <?php if (strtotime($LicenseInformation['ServiceExpiresOn']) <= time()): ?>error<?php endif; ?>" id="form-row-ServiceExpires">
							<label style="width:180px;"><?php InterfaceLanguage('Screen', '1828', false); ?>:</label>
							<?php if (strtotime($LicenseInformation['ServiceExpiresOn']) <= time()): ?>
								<div class="form-row-note error" style="margin-left:200px;"><p><?php InterfaceLanguage('Screen', 1835, false) ?> <?php print(date(InterfaceLanguage('Config', 'LongDateFormat', true), strtotime($LicenseInformation['ServiceExpiresOn']))); ?> - <a href="#" id="js-service-extend-link" target="_blank"><?php InterfaceLanguage('Screen', 1834, false) ?></a></p></div>
							<?php else: ?>
								<div class="form-row-note" style="margin-left:200px;">
									<p><?php InterfaceLanguage('Screen', 1844, false) ?> <?php print(date(InterfaceLanguage('Config', 'LongDateFormat', true), strtotime($LicenseInformation['ServiceExpiresOn']))); ?></p>
								</div>
							<?php endif; ?>
						</div>
					<?php else: ?>
						<div class="form-row <?php if (strtotime($LicenseInformation['ServiceExpiresOn']) <= time()): ?>error<?php endif; ?>" id="form-row-ServiceExpires">
							<label style="width:180px;"><?php InterfaceLanguage('Screen', '1850', false); ?>:</label>
							<?php if (strtotime($LicenseInformation['ServiceExpiresOn']) <= time()): ?>
								<div class="form-row-note error" style="margin-left:200px;"><p><?php InterfaceLanguage('Screen', 1835, false) ?> <?php print(date(InterfaceLanguage('Config', 'LongDateFormat', true), strtotime($LicenseInformation['ServiceExpiresOn']))); ?> - <a href="#" id="js-service-extend-link" target="_blank"><?php InterfaceLanguage('Screen', 1834, false) ?></a></p></div>
							<?php else: ?>
								<div class="form-row-note" style="margin-left:200px;">
									<p><?php InterfaceLanguage('Screen', 1844, false) ?> <?php print(date(InterfaceLanguage('Config', 'LongDateFormat', true), strtotime($LicenseInformation['ServiceExpiresOn']))); ?></p>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if ($LicenseProperties['MaxUsers']['value'] != -1 && $LicenseInformation['IsTrial'] == false): ?>
						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1831', false, '', false, true); ?></h3>
						<div class="form-row <?php print((form_error('AddUserAccounts') != '' ? 'error' : '')); ?>" id="form-row-AddUserAccounts">
							<label for="AddUserAccounts" style="width:200px;"><?php InterfaceLanguage('Screen', '1830', false); ?>:</label>
							<input type="number" name="AddUserAccounts" style="width:50px; text-align: center; font-weight: bold;" value="<?php echo set_value('AddUserAccounts', 0); ?>" id="AddUserAccounts" class="text" maxlength="3" />
							$<span id="js-user-account-price" style="font-weight:bold;"></span> USD
							<div class="form-row-note" style="margin-left:200px;"><p><?php InterfaceLanguage('Screen', 1836, false) ?></p></div>
							<?php print(form_error('AddUserAccounts', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row no-bg" style="margin-bottom:18px;">
							<a style="float:left;" class="button" id="ButtonLicensePurchase"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1832', false, '', true); ?></strong></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php if ($LicenseInformation['IsTrial'] == false): ?>
		<div class="span-18 last">
			<div class="form-action-container">
				<a class="button" id="ButtonUpdateLicenseFile"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1840', false, '', true); ?></strong></a>
			</div>
		</div>
	<?php endif; ?>
</form>