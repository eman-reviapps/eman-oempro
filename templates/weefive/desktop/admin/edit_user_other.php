	<div id="page-shadow">
		<div id="page">

			<?php
			if ($SubSection == 'DisableAccount'):
			?>
				<div class="page-bar">
					<h2><?php InterfaceLanguage('Screen', '0136', false, '', false); ?></h2>
				</div>
				<div class="white" style="min-height:230px">
					<div class="form-row no-bg" style="margin-top:0px;padding-top:15px;">
						<p style="margin-bottom:0px;"><?php InterfaceLanguage('Screen', '0137', false, '', false); ?></p>
					</div>
					<div class="form-row no-bg">
						<div class="form-action-container clearfix left" style="margin:0px;padding:0px;">
							<a class="button" href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/<?php print($SubSection); ?>/1"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0262', false, '', true); ?></strong></a>
						</div>
					</div>
				</div>
			<?php
			elseif ($SubSection == 'EnableAccount'):
			?>
				<div class="page-bar">
					<h2><?php InterfaceLanguage('Screen', '0136', false, '', false); ?></h2>
				</div>
				<div class="white" style="min-height:230px">
					<div class="form-row no-bg" style="margin-top:0px;padding-top:15px;">
						<p style="margin-bottom:0px;"><?php InterfaceLanguage('Screen', '0138', false, '', false); ?></p>
					</div>
					<div class="form-row no-bg">
						<div class="form-action-container clearfix left" style="margin:0px;padding:0px;">
							<a class="button" href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/<?php print($SubSection); ?>/1"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0263', false, '', true); ?></strong></a>
						</div>
					</div>
				</div>
			<?php
			elseif ($SubSection == 'DeleteAccount'):
			?>
				<div class="page-bar">
					<h2><?php InterfaceLanguage('Screen', '0136', false, '', false); ?></h2>
				</div>
				<div class="white" style="min-height:230px">
					<div class="form-row no-bg" style="margin-top:0px;padding-top:15px;">
						<p style="margin-bottom:0px;"><?php InterfaceLanguage('Screen', '0139', false, '', false); ?></p>
					</div>
					<div class="form-row no-bg">
						<div class="form-action-container clearfix left" style="margin:0px;padding:0px;">
							<a class="button" href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/<?php print($SubSection); ?>/1"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0264', false, '', true); ?></strong></a>
						</div>
					</div>
				</div>
			<?php
			endif;
			?>
		</div>
	</div>


<div class="span-18 last">
	</div>
</div>
