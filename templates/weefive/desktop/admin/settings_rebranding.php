				<form id="Rebranding" method="post" action="<?php InterfaceAppURL(); ?>/admin/rebranding/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<h2><?php InterfaceLanguage('Screen', '0124', false, '', true); ?></h2>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>

										<div class="form-row <?php print((form_error('PRODUCT_NAME') != '' ? 'error' : '')); ?>" id="form-row-PRODUCT_NAME">
											<label for="PRODUCT_NAME"><?php InterfaceLanguage('Screen', '0156', false); ?>: *</label>
											<input type="text" name="PRODUCT_NAME" value="<?php echo set_value('PRODUCT_NAME', PRODUCT_NAME); ?>" id="DEFAULT_OPTIN_EMAIL_SUBJECT" class="text" />
											<?php print(form_error('PRODUCT_NAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>

										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0193', false); ?></h3>
										<div class="form-row <?php print((form_error('FORWARD_TO_FRIEND_HEADER') != '' ? 'error' : '')); ?>" id="form-row-FORWARD_TO_FRIEND_HEADER">
											<label for="FORWARD_TO_FRIEND_HEADER"><?php InterfaceLanguage('Screen', '0194', false); ?>:</label>
											<input type="text" name="FORWARD_TO_FRIEND_HEADER" value="<?php echo set_value('FORWARD_TO_FRIEND_HEADER', FORWARD_TO_FRIEND_HEADER); ?>" id="FORWARD_TO_FRIEND_HEADER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('FORWARD_TO_FRIEND_HEADER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('FORWARD_TO_FRIEND_FOOTER') != '' ? 'error' : '')); ?>" id="form-row-FORWARD_TO_FRIEND_FOOTER">
											<label for="FORWARD_TO_FRIEND_FOOTER"><?php InterfaceLanguage('Screen', '0195', false); ?>:</label>
											<input type="text" name="FORWARD_TO_FRIEND_FOOTER" value="<?php echo set_value('FORWARD_TO_FRIEND_FOOTER', FORWARD_TO_FRIEND_FOOTER); ?>" id="FORWARD_TO_FRIEND_FOOTER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('FORWARD_TO_FRIEND_HEADER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>

										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0197', false); ?></h3>
										<div class="form-row <?php print((form_error('REPORT_ABUSE_FRIEND_HEADER') != '' ? 'error' : '')); ?>" id="form-row-REPORT_ABUSE_FRIEND_HEADER">
											<label for="REPORT_ABUSE_FRIEND_HEADER"><?php InterfaceLanguage('Screen', '0194', false); ?>:</label>
											<input type="text" name="REPORT_ABUSE_FRIEND_HEADER" value="<?php echo set_value('REPORT_ABUSE_FRIEND_HEADER', REPORT_ABUSE_FRIEND_HEADER); ?>" id="REPORT_ABUSE_FRIEND_HEADER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('REPORT_ABUSE_FRIEND_HEADER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('REPORT_ABUSE_FRIEND_FOOTER') != '' ? 'error' : '')); ?>" id="form-row-REPORT_ABUSE_FRIEND_FOOTER">
											<label for="REPORT_ABUSE_FRIEND_FOOTER"><?php InterfaceLanguage('Screen', '0195', false); ?>:</label>
											<input type="text" name="REPORT_ABUSE_FRIEND_FOOTER" value="<?php echo set_value('REPORT_ABUSE_FRIEND_FOOTER', REPORT_ABUSE_FRIEND_FOOTER); ?>" id="REPORT_ABUSE_FRIEND_FOOTER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('REPORT_ABUSE_FRIEND_FOOTER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>

										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0198', false); ?></h3>
										<div class="form-row <?php print((form_error('USER_SIGNUP_HEADER') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_HEADER">
											<label for="USER_SIGNUP_HEADER"><?php InterfaceLanguage('Screen', '0194', false); ?>:</label>
											<input type="text" name="USER_SIGNUP_HEADER" value="<?php echo set_value('USER_SIGNUP_HEADER', USER_SIGNUP_HEADER); ?>" id="USER_SIGNUP_HEADER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('USER_SIGNUP_HEADER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('USER_SIGNUP_FOOTER') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_FOOTER">
											<label for="USER_SIGNUP_FOOTER"><?php InterfaceLanguage('Screen', '0195', false); ?>:</label>
											<input type="text" name="USER_SIGNUP_FOOTER" value="<?php echo set_value('USER_SIGNUP_FOOTER', USER_SIGNUP_FOOTER); ?>" id="USER_SIGNUP_FOOTER" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0196', false, '', false); ?></p>
											</div>
											<?php print(form_error('USER_SIGNUP_FOOTER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>

										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0199', false); ?></h3>
										<div class="form-row <?php print((form_error('DEFAULT_SUBSCRIBERAREA_LOGOUT_URL') != '' ? 'error' : '')); ?>" id="form-row-DEFAULT_SUBSCRIBERAREA_LOGOUT_URL">
											<label for="DEFAULT_SUBSCRIBERAREA_LOGOUT_URL"><?php InterfaceLanguage('Screen', '0068', false); ?>:</label>
											<input type="text" name="DEFAULT_SUBSCRIBERAREA_LOGOUT_URL" value="<?php echo set_value('DEFAULT_SUBSCRIBERAREA_LOGOUT_URL', DEFAULT_SUBSCRIBERAREA_LOGOUT_URL); ?>" id="DEFAULT_SUBSCRIBERAREA_LOGOUT_URL" class="text" />
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0201', false, '', false); ?></p>
											</div>
											<?php print(form_error('DEFAULT_SUBSCRIBERAREA_LOGOUT_URL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="Rebranding" id="ButtonRebranding"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0272', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditRebranding" id="Command" />
						</div>
					</div>
				</form>
