						<div id="page-shadow">
							<div id="page">
								<div class="page-bar">
									<ul class="livetabs" tabcollection="form-tabs">
										<li id="tab-1">
											<a href="#"><?php InterfaceLanguage('Screen', '0034', false, '', false); ?></a>
										</li>
										<li id="tab-2">
											<a href="#"><?php InterfaceLanguage('Screen', '0035', false, '', false); ?></a>
										</li>
									</ul>
								</div>
								<div class="white">
									<?php
									if (isset($PageErrorMessage) == true):
									?>
										<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
									<?php
									elseif (isset($PageSuccessMessage) == true):
									?>
										<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
									<?php
									elseif (validation_errors()):
									?>
									<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
									<?php
									else :
									?>
									<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
									<?php
									endif;
									?>
									<!-- Account Information Section - START -->
									<div tabcollection="form-tabs" id="tab-content-1">
										<?php if ($User['GroupInformation']['CreditSystem'] == 'Enabled'): ?>
											<div class="form-row <?php print((form_error('AvailableCredits') != '' ? 'error' : '')); ?>" id="form-row-AvailableCredits">
												<label for="AvailableCredits"><?php InterfaceLanguage('Screen', '1488', false); ?>: *</label>
												<input type="text" name="AvailableCredits" value="<?php echo set_value('AvailableCredits', $User['AvailableCredits']); ?>" id="AvailableCredits" class="text" />
												<?php print(form_error('AvailableCredits', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										<?php endif; ?>
										<div class="form-row <?php print((form_error('ReputationLevel') != '' ? 'error' : '')); ?>" id="form-row-ReputationLevel">
											<label for="ReputationLevel"><?php InterfaceLanguage('Screen', '1798', false); ?>: *</label>
											<select name="ReputationLevel" id="ReputationLevel" class="select">
												<option value="Trusted" <?php echo set_select('ReputationLevel', 'Trusted', ($User['ReputationLevel'] == 'Trusted' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1796', false); ?></option>
												<option value="Untrusted" <?php echo set_select('ReputationLevel', 'Untrusted', ($User['ReputationLevel'] == 'Untrusted' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1797', false); ?></option>
											</select>
										</div>
										<div class="form-row <?php print((form_error('FirstName') != '' ? 'error' : '')); ?>" id="form-row-FirstName">
											<label for="FirstName"><?php InterfaceLanguage('Screen', '0021', false); ?>: *</label>
											<input type="text" name="FirstName" value="<?php echo set_value('FirstName', $User['FirstName']); ?>" id="FirstName" class="text" />
											<?php print(form_error('FirstName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('LastName') != '' ? 'error' : '')); ?>" id="form-row-LastName">
											<label for="LastName"><?php InterfaceLanguage('Screen', '0022', false); ?>: *</label>
											<input type="text" name="LastName" value="<?php echo set_value('LastName', $User['LastName']); ?>" id="LastName" class="text" />
											<?php print(form_error('LastName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Username') != '' ? 'error' : '')); ?>" id="form-row-Username">
											<label for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
											<input type="text" name="Username" value="<?php echo set_value('Username', $User['Username']); ?>" id="Username" class="text" />
											<?php print(form_error('Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('NewPassword') != '' ? 'error' : '')); ?>" id="form-row-NewPassword">
											<label for="NewPassword"><?php InterfaceLanguage('Screen', '0054', false); ?>:</label>
											<input type="password" name="NewPassword" value="<?php echo set_value('NewPassword', ''); ?>" id="NewPassword" class="text" />
											<?php print(form_error('NewPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress">
											<label for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false); ?>: *</label>
											<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', $User['EmailAddress']); ?>" id="EmailAddress" class="text" />
											<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('TimeZone') != '' ? 'error' : '')); ?>" id="form-row-TimeZone">
											<label for="TimeZone"><?php InterfaceLanguage('Screen', '0016', false); ?>: *</label>
											<select name="TimeZone" id="TimeZone" class="select">
												<?php
												foreach (Core::GetTimeZoneList() as $TimeZone):
												?>
													<option value="<?php print($TimeZone); ?>" <?php echo set_select('TimeZone', html_entity_decode($TimeZone), ($User['TimeZone'] == html_entity_decode($TimeZone) ? true : false)) ?>><?php print($TimeZone); ?></option>
												<?php
												endforeach;
												?>
											</select>
											<?php print(form_error('TimeZone', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0033', false); ?></p>
											</div>
										</div>
										<div class="form-row <?php print((form_error('Language') != '' ? 'error' : '')); ?>" id="form-row-Language">
											<label for="Language"><?php InterfaceLanguage('Screen', '0018', false); ?>:</label>
											<select name="Language" id="Language" class="select">
												<?php
												foreach (Core::DetectLanguages() as $EachLanguage):
												?>
													<option value="<?php print($EachLanguage['Code']); ?>" <?php echo set_select('Language', $EachLanguage['Code'], ($User['Language'] == $EachLanguage['Code'] ? true : false)) ?>><?php print($EachLanguage['Name']); ?></option>
												<?php
												endforeach;
												?>
											</select>
											<?php print(form_error('Language', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0032', false); ?></p>
											</div>
										</div>
										<div class="form-row <?php print((form_error('RelUserGroupID') != '' ? 'error' : '')); ?>" id="form-row-RelUserGroupID">
											<label for="RelUserGroupID"><?php InterfaceLanguage('Screen', '0019', false); ?>: *</label>
											<select name="RelUserGroupID" id="RelUserGroupID" class="select">
												<?php
												foreach (UserGroups::RetrieveUserGroups(array('*'), array()) as $EachGroup):
												?>
													<option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('RelUserGroupID', $EachGroup['UserGroupID'], ($User['RelUserGroupID'] == $EachGroup['UserGroupID'] ? true : false)); ?>><?php print($EachGroup['GroupName']); ?></option>
												<?php
												endforeach;
												?>
											</select>
											<?php print(form_error('RelUserGroupID', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											<div class="form-row-note">
												<p><?php InterfaceLanguage('Screen', '0020', false); ?></p>
											</div>
										</div>
									</div>
									<!-- Account Information Section - END -->

									<!-- Personal Information Section - START -->
									<div tabcollection="form-tabs" id="tab-content-2">
										<div class="form-row <?php print((form_error('CompanyName') != '' ? 'error' : '')); ?>" id="form-row-CompanyName">
											<label for="CompanyName"><?php InterfaceLanguage('Screen', '0023', false); ?>:</label>
											<input type="text" name="CompanyName" value="<?php echo set_value('CompanyName', $User['CompanyName']); ?>" id="CompanyName" class="text" />
											<?php print(form_error('CompanyName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Website') != '' ? 'error' : '')); ?>" id="form-row-Website">
											<label for="Website"><?php InterfaceLanguage('Screen', '0024', false); ?>:</label>
											<input type="text" name="Website" value="<?php echo set_value('Website', $User['Website']); ?>" id="Website" class="text" />
											<?php print(form_error('Website', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Street') != '' ? 'error' : '')); ?>" id="form-row-Street">
											<label for="Street"><?php InterfaceLanguage('Screen', '0025', false); ?>:</label>
											<textarea name="Street" id="Street" class="textarea"><?php echo set_value('Street', $User['Street']); ?></textarea>
											<?php print(form_error('Street', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('City') != '' ? 'error' : '')); ?>" id="form-row-City">
											<label for="City"><?php InterfaceLanguage('Screen', '0026', false); ?>:</label>
											<input type="text" name="City" value="<?php echo set_value('City', $User['City']); ?>" id="City" class="text" />
											<?php print(form_error('City', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('State') != '' ? 'error' : '')); ?>" id="form-row-State">
											<label for="State"><?php InterfaceLanguage('Screen', '0027', false); ?>:</label>
											<input type="text" name="State" value="<?php echo set_value('State', $User['State']); ?>" id="State" class="text" />
											<?php print(form_error('State', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Zip') != '' ? 'error' : '')); ?>" id="form-row-Zip">
											<label for="Zip"><?php InterfaceLanguage('Screen', '0028', false); ?>:</label>
											<input type="text" name="Zip" value="<?php echo set_value('Zip', $User['Zip']); ?>" id="Zip" class="text" />
											<?php print(form_error('Zip', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Country') != '' ? 'error' : '')); ?>" id="form-row-Country">
											<label for="Country"><?php InterfaceLanguage('Screen', '0029', false); ?>:</label>
											<select name="Country" id="Country" class="select">
												<option value="" <?php echo set_select('Country', '', ($User['Country'] == '' ? true : false)) ?>><?php InterfaceLanguage('Screen', '0017', false); ?></option>
												<?php
													$ArrayCountries = ApplicationHeader::$ArrayLanguageStrings['Screen']['0036'];
													asort($ArrayCountries);
												foreach ($ArrayCountries as $Code=>$Name):
												?>
													<option value="<?php print($Code); ?>" <?php echo set_select('Country', $Code, ($User['Country'] == $Code ? true : false)); ?>><?php print($Name); ?></option>
												<?php
												endforeach;
												?>
											</select>
											<?php print(form_error('Country', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Phone') != '' ? 'error' : '')); ?>" id="form-row-Phone">
											<label for="Phone"><?php InterfaceLanguage('Screen', '0030', false); ?>:</label>
											<input type="text" name="Phone" value="<?php echo set_value('Phone', $User['Phone']); ?>" id="Phone" class="text" />
											<?php print(form_error('Phone', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div class="form-row <?php print((form_error('Fax') != '' ? 'error' : '')); ?>" id="form-row-Fax">
											<label for="Fax"><?php InterfaceLanguage('Screen', '0031', false); ?>:</label>
											<input type="text" name="Fax" value="<?php echo set_value('Fax', $User['Fax']); ?>" id="Fax" class="text" />
											<?php print(form_error('Fax', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
									</div>
									<!-- Personal Information Section - END -->
								</div>
							</div>
						</div>


					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="UserEditForm" id="ButtonUpdateUserAccount"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0261', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditUser" id="Command" />
							<input type="hidden" name="UserID" value="<?php print($User['UserID']); ?>" id="UserID" />
						</div>
					</div>
