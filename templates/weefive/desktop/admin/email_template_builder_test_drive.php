<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_email_template_editor_header.php'); ?>

<div id="top" class="iguana">
	<div class="container">
		<div class="span-14">
			<ul class="tabs">
				<li class="label"><?php InterfaceLanguage('Screen', '0502', false, '', false); ?></li>
				<li class="selected">
					<a href="#">
						<span class="left">&nbsp;</span>
						<span class="right">&nbsp;</span>
						<strong><?php print $TemplateInformation['TemplateName']; ?></strong>
					</a>
				</li>
			</ul>
		</div>
		<div class="span-10 last iguana-actions">
			<a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/builder/" class="text-action">Back to template builder</a>
		</div>
	</div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
	<div class="container">
		<div class="span-24 last">
			<iframe name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
		</div>
	</div>
</div>

<div class="panel-container">
	<div class="panel-inner">

		<div id="single-editable-panel" style="display:none">
			<div class="properties-panel">
				<input type="text" name="single-editable-text-input" value="" id="single-editable-text-input" class="text" style="width:960px" />
			</div>
		</div>
		<div id="plain-editable-panel" style="display:none">
			<div class="properties-panel">
				<textarea name="plain-editable-text-input" id="plain-editable-text-input" class="textarea" style="width:960px"></textarea>
			</div>
		</div>
		<div id="rich-editable-panel" style="display:none">
			<div class="properties-panel">
				<textarea name="rich-editable-text-input" id="rich-editable-text-input" class="textarea" style="width:960px;height:250px;"></textarea>
			</div>
		</div>
		<div class="element-panel" style="position:static">
			<p style="margin-top:9px;">
				<a href="#" id="hide-panel-button" class="last" style="margin-left:0px;"><?php InterfaceLanguage('Screen', '0500', false); ?></a>
			</p>
		</div>
	</div>

</div>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_email_template_editor_footer.php'); ?>
