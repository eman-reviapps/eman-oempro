<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<div class="container" style="margin-top:18px;">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs">
						<li id="item-sub-report-unsubscriptions"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0669', false, '', false)?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner flash-chart-container" id="revenue-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/admin/paymentreports/ChartData/Revenue" style="height:150px;">
					</div>
					<div class="custom-column-container cols-2 clearfix">
						<div class="col">
							<span class="data big"><?php print(Core::FormatCurrency($CurrentMonthsRevenue, false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span> <span class="data-label"><?php InterfaceLanguage('Screen', '0674', false, '', false, true); ?></span><br />
							<span class="difference-ratio" id="revenue-difference-ratio" class="data"><?php print($RevenueDifferenceRatio); ?>%</span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0675', false, '', false, false); ?></span><br /><br />
							<span class="data big"><?php print(Core::FormatCurrency($TopUserInformation['PaymentPeriodInformation']['PeriodTotal'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span> <span class="data-label"><?php InterfaceLanguage('Screen', '0682', false, '', false, true); ?></span><br />
							<span class="data-label small"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($TopUserInformation['UserInformation']['UserID']); ?>"><?php print($TopUserInformation['UserInformation']['FirstName'].' '.$TopUserInformation['UserInformation']['LastName']); ?></a>, <?php print($TopUserInformation['UserInformation']['GroupInformation']['GroupName']); ?></span><br /><br />
						</div>
						<?php
						$ArrayChartColors = explode(',',CHART_COLORS);
						$ArrayChartColors = array_reverse($ArrayChartColors);
						?>
						<div class="col" style="background-color:#f2f2f2;border:1px solid #e1e1e1;">
							<div style="padding:9px 12px;" class="clearfix">
								<span class="data big"><?php print(Core::FormatCurrency($TotalRevenue, false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span> <span class="data-label"><?php InterfaceLanguage('Screen', '0676', false, '', false, false); ?></span><br />
								<span class="data"><?php print(Core::FormatCurrency($TotalNotPaidRevenue, false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0681', false, '', false, false); ?></span><br />
								<br />
								<div class="flash-chart-container" id="revenue-pie-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie/5" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/admin/paymentreports/ChartData/RevenueStatus" style="float:left;text-align:left;width:130px;height:130px;">
								</div>
								<br />
								<span class="data big" style="color:#<?php print($ArrayChartColors[0]); ?>"><?php print($TotalPaidRevenueRatio); ?></span><span class="data" style="color:#<?php print($ArrayChartColors[0]); ?>">%</span> <span class="data-label"><?php InterfaceLanguage('Screen', '0677', false, '', false, true); ?></span><br />
								<span class="data big" style="color:#<?php print($ArrayChartColors[1]); ?>"><?php print($TotalNotPaidRevenueRatio); ?></span><span class="data" style="color:#<?php print($ArrayChartColors[1]); ?>">%</span> <span class="data-label"><?php InterfaceLanguage('Screen', '0678', false, '', false, true); ?></span><br />
							</div>
						</div>
					</div>
				</div>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-2">
						<li id="tab-top-users"><a href="#"><?php InterfaceLanguage('Screen', '0679', false, '', false, true); ?></a></li>
						<li id="tab-receivables"><a href="#"><?php InterfaceLanguage('Screen', '0680', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-2" id="tab-content-top-users">
							<?php
							if (count($AllTimeTopUsers) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0102', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($AllTimeTopUsers as $Each):
									?>
										<tr>
											<td width="30%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each['UserInformation']['UserID']); ?>/PaymentHistory"><?php print($Each['UserInformation']['FirstName'].' '.$Each['UserInformation']['LastName']); ?></a></td>
											<td width="25%"><?php print($Each['UserInformation']['GroupInformation']['GroupName']); ?></td>
											<td width="20%" class="right-align"><span class="data"><?php print(Core::FormatCurrency($Each['PaymentPeriodInformation']['AllTimeTotalAmount'])); ?></span></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
						<div tabcollection="report-tabs-2" id="tab-content-receivables">
							<?php
							if (count($AllTimeReceivables) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0102', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($AllTimeReceivables as $Each):
									?>
										<tr>
											<td width="30%"><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($Each['UserInformation']['UserID']); ?>/PaymentHistory"><?php print($Each['UserInformation']['FirstName'].' '.$Each['UserInformation']['LastName']); ?></a></td>
											<td width="25%"><?php print($Each['UserInformation']['GroupInformation']['GroupName']); ?></td>
											<td width="20%" class="right-align"><span class="data"><?php print(Core::FormatCurrency($Each['PaymentPeriodInformation']['SumTotalAmount'])); ?></span></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="help-column span-5 push-1 last">
		<?php include_once(TEMPLATE_PATH.'desktop/help/help_admin_paymentreports.php'); ?>
	</div>
</div>
<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
