<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0471', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - START -->
<form id="form-upgrade" action="<?php InterfaceAppURL(); ?>/admin/upgrade/" method="post">
	<div class="container">
		<div class="span-18">
			<div id="page-shadow">
				<div id="page">
					<div class="white">
						<?php if (isset($PageErrorMessage) == true): ?>
							<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
						<?php elseif (isset($PageSuccessMessage) == true): ?>
							<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
						<?php elseif (validation_errors()): ?>
							<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php else: ?>
							<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
						<?php endif; ?>

						<div class="form-row <?php print((form_error('FTPHost') != '' ? 'error' : '')); ?>" id="form-row-FTPHost">
							<label for="FTPHost"><?php InterfaceLanguage('Screen', '0473', false, '', false, true); ?>: *</label>
							<input type="text" name="FTPHost" value="<?php echo set_value('FTPHost'); ?>" id="FTPHost" class="text" style="width:450px;" />
							<?php print(form_error('FTPHost', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0476', false, '', false, false); ?></p></div>
						</div>
						<div class="form-row <?php print((form_error('FTPPort') != '' ? 'error' : '')); ?>" id="form-row-FTPPort">
							<label for="FTPPort"><?php InterfaceLanguage('Screen', '0478', false, '', false, true); ?>: *</label>
							<input type="text" name="FTPPort" value="<?php echo set_value('FTPPort'); ?>" id="FTPPort" class="text" style="width:50px;" />
							<?php print(form_error('FTPPort', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0477', false, '', false, false); ?></p></div>
						</div>
						<div class="form-row <?php print((form_error('FTPUsername') != '' ? 'error' : '')); ?>" id="form-row-FTPUsername">
							<label for="FTPUsername"><?php InterfaceLanguage('Screen', '0474', false, '', false, true); ?>: *</label>
							<input type="text" name="FTPUsername" value="<?php echo set_value('FTPUsername'); ?>" id="FTPUsername" class="text" style="width:450px;" />
							<?php print(form_error('FTPUsername', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row <?php print((form_error('FTPPassword') != '' ? 'error' : '')); ?>" id="form-row-FTPPassword">
							<label for="FTPPassword"><?php InterfaceLanguage('Screen', '0475', false, '', false, true); ?>: *</label>
							<input type="password" name="FTPPassword" value="<?php echo set_value('FTPPassword'); ?>" id="FTPPassword" class="text" style="width:450px;" />
							<?php print(form_error('FTPPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row <?php print((form_error('FTPPath') != '' ? 'error' : '')); ?>" id="form-row-FTPPath">
							<label for="FTPPath"><?php InterfaceLanguage('Screen', '0485', false, '', false, true); ?>: *</label>
							<input type="text" name="FTPPath" value="<?php echo set_value('FTPPath'); ?>" id="FTPPath" class="text" style="width:450px;" />
							<?php print(form_error('FTPPath', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0486', false, '', false, false); ?></p></div>
						</div>
						<div class="form-row">
							<label><?php InterfaceLanguage('Screen', '0157', false); ?>:</label>
							<div class="form-row-note"><p><?php print(PRODUCT_VERSION); ?></p></div>
						</div>
						<div class="form-row">
							<label for="LatestVersion"><?php InterfaceLanguage('Screen', '0480', false); ?>:</label>
							<div class="form-row-note"><p><?php print($LatestVersion); ?></p></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_admin_upgrade.php'); ?>
		</div>
		
		<?php if ($NewVersionExists == true): ?>
			<div class="span-18 last">
				<div class="form-action-container">
					<input type="hidden" name="Command" value="Upgrade" id="Command">
					<a class="button" id="form-button" targetform="form-upgrade" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0472', false, '', true); ?></strong></a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</form>
<!-- Page - END -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>