				<form id="Integration" method="post" action="<?php InterfaceAppURL(); ?>/admin/integration/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<ul class="livetabs" tabcollection="form-tabs">
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0432', false, '', false); ?></a>
											</li>
											<li id="tab-1">
												<a href="#"><?php InterfaceLanguage('Screen', '0202', false, '', false); ?></a>
											</li>
											<li id="tab-3">
												<a href="#"><?php InterfaceLanguage('Screen', '1861', false, '', false); ?></a>
											</li>
										</ul>
									</div>
									<div class="white" style="min-height:430px;">
										<?php if (isset($PageErrorMessage) == true): ?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php elseif (isset($PageSuccessMessage) == true): ?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php elseif (validation_errors()): ?>
											<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php else: ?>
											<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php endif; ?>

										<!-- PreviewMyEmail.com - START -->
										<div tabcollection="form-tabs" id="tab-content-1">
											<div class="form-row no-bg">
												<p><?php InterfaceLanguage('Screen', '0203', false, '', false); ?></p>
											</div>
											<div class="form-row <?php print((form_error('PME_USAGE_TYPE') != '' ? 'error' : '')); ?>" id="form-row-PME_USAGE_TYPE">
												<label for="PME_USAGE_TYPE"><?php InterfaceLanguage('Screen', '0204', false); ?>:</label>
												<div class="checkbox-container">
													<div class="checkbox-row">
														<input type="radio" name="PME_USAGE_TYPE" value="Disabled" id="PME_USAGE_TYPE_Disabled" <?php echo set_radio('PME_USAGE_TYPE', 'Disabled', $IsPreviewMyEmailDisabled); ?>> <label for="PME_USAGE_TYPE_Disabled"><?php InterfaceLanguage('Screen', '0050', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="radio" name="PME_USAGE_TYPE" value="Admin" id="PME_USAGE_TYPE_Admin" <?php echo set_radio('PME_USAGE_TYPE', 'Admin', ($IsPreviewMyEmailDisabled == FALSE && PME_USAGE_TYPE == 'Admin' ? true : false)); ?>> <label for="PME_USAGE_TYPE_Admin"><?php InterfaceLanguage('Screen', '0205', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="radio" name="PME_USAGE_TYPE" value="User" id="PME_USAGE_TYPE_User" <?php echo set_radio('PME_USAGE_TYPE', 'User', ($IsPreviewMyEmailDisabled == FALSE && PME_USAGE_TYPE == 'User' ? true : false)); ?>> <label for="PME_USAGE_TYPE_User"><?php InterfaceLanguage('Screen', '0206', false, '', false); ?></label>
													</div>
												</div>
												<?php print(form_error('PME_USAGE_TYPE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div id="pme-integration-settings">
												<div class="form-row <?php print((form_error('PME_DEFAULT_APIKEY') != '' ? 'error' : '')); ?>" id="form-row-PME_DEFAULT_APIKEY">
													<label for="PME_DEFAULT_APIKEY"><?php InterfaceLanguage('Screen', '0208', false); ?>: *</label>
													<input type="text" name="PME_DEFAULT_APIKEY" value="<?php echo set_value('PME_DEFAULT_APIKEY', PME_DEFAULT_APIKEY); ?>" id="PME_DEFAULT_APIKEY" class="text" />
													<?php print(form_error('PME_DEFAULT_APIKEY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
											</div>
										</div>
										<!-- PreviewMyEmail.com - END -->
										
										<!-- Google Analytics - START -->
										<div tabcollection="form-tabs" id="tab-content-2">
											<div class="form-row no-bg">
												<p><?php InterfaceLanguage('Screen', '0433', false, '', false); ?></p>
											</div>
											<div class="form-row <?php print((form_error('GOOGLE_ANALYTICS_SOURCE') != '' ? 'error' : '')); ?>" id="form-row-GOOGLE_ANALYTICS_SOURCE">
												<label for="GOOGLE_ANALYTICS_SOURCE"><?php InterfaceLanguage('Screen', '0434', false); ?>:</label>
												<input type="text" name="GOOGLE_ANALYTICS_SOURCE" value="<?php echo set_value('GOOGLE_ANALYTICS_SOURCE', GOOGLE_ANALYTICS_SOURCE); ?>" id="GOOGLE_ANALYTICS_SOURCE" class="text" />
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0436', false, '', false); ?></p></div>
												<?php print(form_error('GOOGLE_ANALYTICS_SOURCE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('GOOGLE_ANALYTICS_MEDIUM') != '' ? 'error' : '')); ?>" id="form-row-GOOGLE_ANALYTICS_MEDIUM">
												<label for="GOOGLE_ANALYTICS_MEDIUM"><?php InterfaceLanguage('Screen', '0435', false); ?>:</label>
												<input type="text" name="GOOGLE_ANALYTICS_MEDIUM" value="<?php echo set_value('GOOGLE_ANALYTICS_MEDIUM', GOOGLE_ANALYTICS_MEDIUM); ?>" id="GOOGLE_ANALYTICS_MEDIUM" class="text" />
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0437', false, '', false); ?></p></div>
												<?php print(form_error('GOOGLE_ANALYTICS_MEDIUM', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>
										<!-- Google Analytics - END -->

										<!-- Cockpito.com - START -->
										<div tabcollection="form-tabs" id="tab-content-3">
											<div class="form-row no-bg">
												<p><?php InterfaceLanguage('Screen', '1860', false, '', false); ?></p>
											</div>
										</div>
										<!-- Cockpito.com - END -->
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="Integration" id="ButtonIntegration"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0271', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditIntegration" id="Command" />
						</div>
					</div>
				</form>

				<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/settings_integration.js" type="text/javascript" charset="utf-8"></script>		
