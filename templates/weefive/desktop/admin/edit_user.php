		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

		<!-- Section bar - Start -->
		<div class="container">
			<div class="span-23 last">
				<div id="section-bar">
					<div class="header">
						<h1 class="left-side-padding"><?php print(ucwords($User['FirstName'].' '.$User['LastName'])); ?>
						<?php
						if ($User['AccountStatus'] == 'Disabled'):
						?>
							<span class="label"><?php InterfaceLanguage('Screen', '0050', false, '', false); ?></span>
						<?php
						endif;
						?>
						</h1>
					</div>
					<div class="actions">
						<div class="main-action-with-menu" id="user-login-menu">
							<a href="#" class="main-action"><?php InterfaceLanguage('Screen', '0077', false, '', true); ?></a>
							<div class="down-arrow"><span></span></div>
							<ul class="main-action-menu" style="width:220px;">
								<li><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/LoginFull"><?php InterfaceLanguage('Screen', '1436', false, '', false); ?></a></li>
								<li><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/Login"><?php InterfaceLanguage('Screen', '1437', false, '', false); ?></a></li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- Section bar - End -->

		<!-- Page - Start -->
		<div class="container">
			<div class="span-5 snap-5">
				<ul class="left-sub-navigation">
					<li class="header first"><?php InterfaceLanguage('Screen', '0076', false, '', true); ?></li>
					<li <?php print($SubSection == 'AccountActivity' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/AccountActivity"><?php InterfaceLanguage('Screen', '0078', false, '', false); ?></a></li>
					<li <?php print($SubSection == 'PaymentHistory' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/PaymentHistory"><?php InterfaceLanguage('Screen', '0074', false, '', false); ?></a></li>
				</ul>
				<ul class="left-sub-navigation">
					<li class="header"><?php InterfaceLanguage('Screen', '0079', false, '', true); ?></li>
					<li <?php print($SubSection == 'Edit' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/Edit"><?php InterfaceLanguage('Screen', '0080', false, '', false); ?></a></li>
					<li <?php print($SubSection == 'SendMessage' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/SendMessage"><?php InterfaceLanguage('Screen', '0081', false, '', false); ?></a></li>
					<li <?php print($SubSection == 'EnableAccount' || $SubSection == 'DisableAccount' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/<?php print(($User['AccountStatus'] == 'Enabled' ? 'DisableAccount' : 'EnableAccount')); ?>"><?php InterfaceLanguage('Screen', ($User['AccountStatus'] == 'Enabled' ? '0083' : '0082'), false, '', false); ?></a></li>
					<li <?php print($SubSection == 'DeleteAccount' ? 'class="selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/DeleteAccount"><?php InterfaceLanguage('Screen', '0109', false, '', false); ?></a></li>
				</ul>
			</div>
			<form id="UserEditForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']); ?>/<?php print($SubSection); ?>">
				<div class="span-18 last">
					<?php
					if ($SubSection == 'AccountActivity')
						{
						include_once(TEMPLATE_PATH.'desktop/admin/edit_user_accountactivity.php');
						}
					elseif (($SubSection == 'PaymentHistory'))
						{
						include_once(TEMPLATE_PATH.'desktop/admin/edit_user_paymenthistory.php');
						}
					elseif ($SubSection == 'Edit')
						{
						include_once(TEMPLATE_PATH.'desktop/admin/edit_user_account.php');
						}
					elseif ($SubSection == 'SendMessage')
						{
						include_once(TEMPLATE_PATH.'desktop/admin/edit_user_sendmessage.php');
						}
					elseif (($SubSection == 'EnableAccount') || ($SubSection == 'DisableAccount') || ($SubSection == 'DeleteAccount'))
						{
						include_once(TEMPLATE_PATH.'desktop/admin/edit_user_other.php');
						}
					?>
				</div>
			</form>
		</div>
		<!-- Page - End -->

		<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/user_edit.js" type="text/javascript" charset="utf-8"></script>		

		<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
