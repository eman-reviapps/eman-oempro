<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0471', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - START -->
<form id="form-upgrade" action="<?php InterfaceAppURL(); ?>/admin/upgrade/" method="post">
	<div class="container">
		<div class="span-24">
			<div id="page-shadow">
				<div id="page">
					<div class="white">
						<?php if ($NewVersionExists == true): ?>
							<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1928', false, '', false, true); ?></h3>
							<div class="form-row no-background-color" id="form-row-Copyright1">
								<?php InterfaceLanguage('Screen', '1930', false, '', false, true); ?>
							</div>
						<?php else: ?>
							<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1929', false, '', false, true); ?></h3>
							<div class="form-row no-background-color" id="form-row-Copyright1">
								<?php InterfaceLanguage('Screen', '1931', false, '', false, true); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- Page - END -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>