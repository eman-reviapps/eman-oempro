<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '1884', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<div class="container" style="margin-top:18px;">
	<div class="span-7">
		<div id="page-shadow">
			<div id="page">
				<div class="white">
					<p style="padding:18px;margin:0;">
						<span class="data big"><?php echo is_array($RatiosBySource) == false ? '0' : InterfaceNumber($RatiosBySource[0]['Total'], true); ?></span><br>
						<span class="data-label" style="margin:0;"><?php InterfaceLanguage('Screen', '1888'); ?></span>
					</p>
					<?php if (is_array($RatiosBySource) != false): ?>
					<hr />
					<p style="padding:0 18px 18px 18px;margin:0;">
						<span class="data-label" style="margin:0;"><?php InterfaceLanguage('Screen', '1889'); ?></span><br>
						<?php foreach ($RatiosBySource as $each): ?>
						<span class="data small"><?php InterfacePercentage($each['Percent']); ?></span> <span class="data-label small"><?php echo $each['Source']; ?></span><br>
						<?php endforeach; ?>
					</p>
					<?php endif; ?>
					<hr />
					<p style="padding:0 18px;margin:0;">
						<span class="data-label small" style="margin:0;line-height: 12px;"><?php InterfaceLanguage('Screen', '1893'); ?></span>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="span-15 push-1">
		<div id="page-shadow">
			<div id="page">
				<div class="page-bar">
					<ul class="livetabs" tabcollection="tabs">
						<li id="tab-search"><a href="#"><?php InterfaceLanguage('Screen', '1886', false, '', false, true)?></a></li>
						<li id="tab-add"><a href="#"><?php InterfaceLanguage('Screen', '1894', false, '', false, true)?></a></li>
						<li id="tab-smart"><a href="#"><?php InterfaceLanguage('Screen', '1887', false, '', false, true)?></a></li>					</ul>
				</div>
				<div class="white">
					<div tabcollection="tabs" id="tab-content-search">
						<div class="inner" style="padding-top:9px;">
							<form id="SearchForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/suppression/#tabs/tab-search">
								<?php if ($DeleteEventReturn): ?>
								<h3 class="form-legend success" style="margin:0;padding:9px 18px 18px 18px;"><?php InterfaceLanguage('Screen', '1907'); ?></h3>
								<?php endif; ?>
								<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress" style="margin-top:0;">
									<label for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false); ?>: *</label>
									<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', ''); ?>" id="EmailAddress" class="text" />
									<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
								</div>
								<div class="form-row no-bg">
									<a class="button" style="display:inline-block;" targetform="SearchForm"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1886', false, '', true); ?></strong></a>
									<input type="hidden" name="Command" value="Search" id="Command" />
								</div>
							</form>
							<?php if (is_array($FoundSuppressions) && count($FoundSuppressions) > 0): ?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid" id="activity-table" style="margin-top:18px;">
									<tr>
										<th>Email address</th>
										<th>User - List ID</th>
										<th>Source</th>
										<th>&nbsp;</th>
									</tr>
									<?php foreach ($FoundSuppressions as $each): ?>
										<tr>
											<td width="50%"><?php echo $each->getEmailAddress(); ?></td>
											<td width="20%"><?php echo $each->getUserId(); ?> - <?php echo $each->getListId(); ?></td>
											<td width="25%"><?php echo $each->getSource(); ?></td>
											<td width="5%"><a href="#" class="remove-suppression-link" suppressionid="<?php echo $each->getId(); ?>" style="color:#FF0000;">X</a></td>
										</tr>
									<?php endforeach; ?>
								</table>
							<?php endif; ?>
							<form id="DeleteSuppressionForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/suppression/#tabs/tab-search">
								<input type="hidden" name="Command" value="DeleteSuppression" id="Command" />
								<input type="hidden" name="SuppressionID" value="" id="SuppressionID" />
							</form>
						</div>
					</div>
					<div tabcollection="tabs" id="tab-content-add">
						<div class="inner" style="padding-top:9px;">
							<?php if ($AddEventReturn): ?>
								<h3 class="form-legend success" style="margin:0;padding:9px 18px 18px 18px;"><?php InterfaceLanguage('Screen', '1892', false, '', false, false, array($TotalEmailAddressesAdded)); ?></h3>
							<?php endif; ?>
							<form id="AddSuppressionForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/suppression/#tabs/tab-add">
								<div class="form-row <?php print((form_error('EmailAddresses') != '' ? 'error' : '')); ?>" id="form-row-EmailAddresses" style="margin-top:0;">
									<label for="EmailAddresses"><?php InterfaceLanguage('Screen', '0129', false); ?>: *</label>
									<textarea name="EmailAddresses" id="EmailAddresses" class="textarea"><?php echo set_value('EmailAddresses', ''); ?></textarea>
									<div class="form-row-note">
										<p><?php InterfaceLanguage('Screen', '1891'); ?></p>
									</div>
									<?php print(form_error('EmailAddresses', '<div class="form-row-note error"><p>', '</p></div>')); ?>
								</div>
								<div class="form-row no-bg">
									<a class="button" style="display:inline-block;" targetform="AddSuppressionForm"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1890', false, '', true); ?></strong></a>
									<input type="hidden" name="Command" value="AddSuppression" id="Command" />
								</div>
							</form>
						</div>
					</div>
					<div tabcollection="tabs" id="tab-content-smart">
						<div class="inner" style="padding-top:9px;">
							<form id="AddSuppressionPatternForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/suppression/#tabs/tab-smart">
								<?php if ($AddPatternEventReturn): ?>
									<h3 class="form-legend success" style="margin:0;padding:9px 18px 18px 18px;"><?php InterfaceLanguage('Screen', '1905'); ?></h3>
								<?php endif; ?>
								<p style="padding:9px 18px 0 18px;"><?php InterfaceLanguage('Screen', '1900'); ?></p>
								<div class="form-row <?php print((form_error('Description') != '' ? 'error' : '')); ?>" id="form-row-Description" style="margin-top:0;">
									<label for="Description"><?php InterfaceLanguage('Screen', '1903', false); ?>: *</label>
									<input type="text" name="Description" value="<?php echo set_value('Description', ''); ?>" id="Description" class="text" />
									<div class="form-row-note">
										<p><?php InterfaceLanguage('Screen', '1902'); ?></p>
									</div>
									<?php print(form_error('Description', '<div class="form-row-note error"><p>', '</p></div>')); ?>
								</div>
								<div class="form-row <?php print((form_error('Pattern') != '' ? 'error' : '')); ?>" id="form-row-Pattern" style="margin-top:0;">
									<label for="Pattern"><?php InterfaceLanguage('Screen', '1897', false); ?>: *</label>
									<input type="text" name="Pattern" value="<?php echo set_value('Pattern', ''); ?>" id="Pattern" class="text" />
									<div class="form-row-note">
										<p><?php InterfaceLanguage('Screen', '1901'); ?></p>
									</div>
									<?php print(form_error('Pattern', '<div class="form-row-note error"><p>', '</p></div>')); ?>
								</div>
								<div class="form-row" id="form-row-PatternType">
									<label for="PatternType"><?php InterfaceLanguage('Screen', '1899', false, '', false, true); ?>:</label>
									<select name="PatternType" id="PatternType" class="select">
										<option value="REGEXP" <?php echo set_select('PatternType', 'REGEXP'); ?>>REGEXP</option>
										<option value="NOT REGEXP" <?php echo set_select('PatternType', 'NOT REGEXP'); ?>>NOT REGEXP</option>
									</select>
								</div>
								<div class="form-row no-bg">
									<a class="button" style="display:inline-block;" targetform="AddSuppressionPatternForm"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1898', false, '', true); ?></strong></a>
									<input type="hidden" name="Command" value="AddSuppressionPattern" id="Command" />
								</div>
							</form>
							<?php if ($SuppressionPatterns !== false): ?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid" id="activity-table" style="margin-top:18px;">
									<tr>
										<th><?php InterfaceLanguage('Screen', '1903'); ?></th>
										<th><?php InterfaceLanguage('Screen', '1897'); ?></th>
										<th><?php InterfaceLanguage('Screen', '1899'); ?></th>
										<th>&nbsp;</th>
									</tr>
									<?php foreach ($SuppressionPatterns as $eachPattern): ?>
										<tr>
											<td width="35%"><?php echo $eachPattern->getDescription(); ?></td>
											<td width="40%"><?php echo $eachPattern->getPattern(); ?></td>
											<td width="20%"><?php echo $eachPattern->getType(); ?></td>
											<td width="5%"><a class="pattern-remove-link" href="#" style="color:#FF0000;" patternid="<?php echo $eachPattern->getId(); ?>">X</a></td>
										</tr>
									<?php endforeach; ?>
								</table>
								<form id="DeleteSuppressionPatternForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/suppression/#tabs/tab-smart">
									<input type="hidden" name="Command" value="DeleteSuppressionPattern" id="Command" />
									<input type="hidden" name="PatternID" value="" id="PatternID" />
								</form>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var Language = {
		'1904': '<?php InterfaceLanguage('Screen', '1904'); ?>',
		'1906': '<?php InterfaceLanguage('Screen', '1906'); ?>'
	};
	$(document).ready(function() {
		$('.pattern-remove-link').click(function() {
			if (! confirm(Language['1904']))
				return false;

			$('#PatternID').val($(this).attr('patternid'));
			$('#DeleteSuppressionPatternForm').submit();
			return false;
		});
		$('.remove-suppression-link').click(function() {
			if (! confirm(Language['1906']))
				return false;

			$('#SuppressionID').val($(this).attr('suppressionid'));
			$('#DeleteSuppressionForm').submit();
			return false;
		});
	});
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
