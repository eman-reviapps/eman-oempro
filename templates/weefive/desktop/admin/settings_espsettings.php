				<form id="ESPSettings" method="post" action="<?php InterfaceAppURL(); ?>/admin/espsettings/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<ul class="livetabs" tabcollection="form-tabs">
											<li id="tab-1">
												<a href="#"><?php InterfaceLanguage('Screen', '0225', false, '', false); ?></a>
											</li>
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0226', false, '', false); ?></a>
											</li>
											<li id="tab-3">
												<a href="#"><?php InterfaceLanguage('Screen', '0227', false, '', false); ?></a>
											</li>
											<li id="tab-4">
												<a href="#"><?php InterfaceLanguage('Screen', '0228', false, '', false); ?></a>
											</li>
										</ul>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>

										<!-- User Sign-Up - START -->
										<div tabcollection="form-tabs" id="tab-content-1">
											<div class="form-row <?php print((form_error('USER_SIGNUP_ENABLED') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_ENABLED">
												<label for="USER_SIGNUP_ENABLED"><?php InterfaceLanguage('Screen', '0229', false); ?>: *</label>
												<div class="checkbox-container">
													<div class="checkbox-row">
														<input type="radio" name="USER_SIGNUP_ENABLED" value="true" id="USER_SIGNUP_ENABLED_true" <?php echo set_radio('USER_SIGNUP_ENABLED', 'true', (USER_SIGNUP_ENABLED == true ? true : false)); ?>> <label for="USER_SIGNUP_ENABLED_true"><?php InterfaceLanguage('Screen', '0230', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="radio" name="USER_SIGNUP_ENABLED" value="false" id="USER_SIGNUP_ENABLED_false" <?php echo set_radio('USER_SIGNUP_ENABLED', 'false', (USER_SIGNUP_ENABLED == false ? true : false)); ?>> <label for="USER_SIGNUP_ENABLED_false"><?php InterfaceLanguage('Screen', '0231', false, '', false); ?></label>
													</div>
												</div>
												<?php print(form_error('USER_SIGNUP_ENABLED', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div id="user-signup-settings">
												<div class="form-row" id="form-row-USER_SIGNUP_REPUTATION">
													<label for="USER_SIGNUP_REPUTATION"><?php InterfaceLanguage('Screen', '1798', false); ?>: *</label>
													<div class="checkbox-container">
														<div class="checkbox-row">
															<input type="radio" name="USER_SIGNUP_REPUTATION" value="Trusted" id="USER_SIGNUP_REPUTATION_Trusted" <?php echo set_radio('USER_SIGNUP_REPUTATION', 'Trusted', (USER_SIGNUP_REPUTATION == "Trusted" ? true : false)); ?>> <label for="USER_SIGNUP_REPUTATION_Trusted"><?php InterfaceLanguage('Screen', '1812', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="radio" name="USER_SIGNUP_REPUTATION" value="Untrusted" id="USER_SIGNUP_REPUTATION_Untrusted" <?php echo set_radio('USER_SIGNUP_REPUTATION', 'Untrusted', (USER_SIGNUP_REPUTATION ==  "Untrusted" ? true : false)); ?>> <label for="USER_SIGNUP_REPUTATION_Untrusted"><?php InterfaceLanguage('Screen', '1813', false, '', false); ?></label>
														</div>
													</div>
													<?php print(form_error('USER_SIGNUP_ENABLED', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('USER_SIGNUP_FIELDS') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_FIELDS">
													<label for="USER_SIGNUP_FIELDS"><?php InterfaceLanguage('Screen', '0232', false); ?>:</label>
													<div class="checkbox-container">
														<?php
														$ArraySignUpFields = explode(',', USER_SIGNUP_FIELDS);
														?>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="FirstName*" id="USER_SIGNUP_FIELDS_FirstName" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'FirstName*', (in_array('FirstName*', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_FirstName"><?php InterfaceLanguage('Screen', '0021', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="LastName*" id="USER_SIGNUP_FIELDS_LastName" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'LastName*', (in_array('LastName*', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_LastName"><?php InterfaceLanguage('Screen', '0022', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="CompanyName*" id="USER_SIGNUP_FIELDS_CompanyName" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'CompanyName*', (in_array('CompanyName*', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_CompanyName"><?php InterfaceLanguage('Screen', '0023', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Website" id="USER_SIGNUP_FIELDS_Website" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Website', (in_array('Website', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Website"><?php InterfaceLanguage('Screen', '0024', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Street" id="USER_SIGNUP_FIELDS_Street" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Street', (in_array('Street', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Street"><?php InterfaceLanguage('Screen', '0025', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="City" id="USER_SIGNUP_FIELDS_City" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'City', (in_array('City', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_City"><?php InterfaceLanguage('Screen', '0026', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="State" id="USER_SIGNUP_FIELDS_State" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'State', (in_array('State', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_State"><?php InterfaceLanguage('Screen', '0027', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Zip" id="USER_SIGNUP_FIELDS_Zip" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Zip', (in_array('Zip', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Zip"><?php InterfaceLanguage('Screen', '0028', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Country" id="USER_SIGNUP_FIELDS_Country" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Country', (in_array('Country', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Country"><?php InterfaceLanguage('Screen', '0029', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Phone" id="USER_SIGNUP_FIELDS_Phone" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Phone', (in_array('Phone', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Phone"><?php InterfaceLanguage('Screen', '0030', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Fax" id="USER_SIGNUP_FIELDS_Fax" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Fax', (in_array('Fax', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Fax"><?php InterfaceLanguage('Screen', '0031', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="TimeZone" id="USER_SIGNUP_FIELDS_TimeZone" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'TimeZone', (in_array('TimeZone', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_TimeZone"><?php InterfaceLanguage('Screen', '0016', false, '', false); ?></label>
														</div>
														<div class="checkbox-row">
															<input type="checkbox" name="USER_SIGNUP_FIELDS[]" value="Language" id="USER_SIGNUP_FIELDS_Language" <?php echo set_checkbox('USER_SIGNUP_FIELDS[]', 'Language', (in_array('Language', $ArraySignUpFields) == true ? true : false)); ?>> <label for="USER_SIGNUP_FIELDS_Language"><?php InterfaceLanguage('Screen', '0018', false, '', false); ?></label>
														</div>
													</div>
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0233', false, '', false); ?></p></div>
													<?php print(form_error('USER_SIGNUP_FIELDS[]', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('USER_SIGNUP_LANGUAGE') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_LANGUAGE">
													<label for="USER_SIGNUP_LANGUAGE"><?php InterfaceLanguage('Screen', '0018', false); ?>: *</label>
													<select name="USER_SIGNUP_LANGUAGE" id="USER_SIGNUP_LANGUAGE" class="select">
														<?php
														$ArrayLanguages = Core::DetectLanguages();
														foreach ($ArrayLanguages as $Each):
														?>
														<option value="<?php print($Each['Code']); ?>" <?php echo set_select('USER_SIGNUP_LANGUAGE', $Each['Code'], (USER_SIGNUP_LANGUAGE == $Each['Code'] ? true : false)); ?>><?php print($Each['Name']); ?></option>
														<?php
														endforeach;
														?>
													</select>
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0234', false, '', false); ?></p></div>
													<?php print(form_error('USER_SIGNUP_LANGUAGE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('USER_SIGNUP_GROUPID') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_GROUPID">
													<label for="USER_SIGNUP_GROUPID"><?php InterfaceLanguage('Screen', '0235', false); ?>: *</label>
													<select name="USER_SIGNUP_GROUPID" id="USER_SIGNUP_GROUPID" class="select">
														<option value="-1" <?php echo set_select('USER_SIGNUP_GROUPID', -1, (USER_SIGNUP_GROUPID == -1 ? true : false)); ?>><?php InterfaceLanguage('Screen', '0236', false, '', false); ?></option>
														<?php
														$ArrayUserGroups = UserGroups::RetrieveUserGroups(array('*'), array(), array('GroupName'=>'ASC'));
														foreach ($ArrayUserGroups as $Each):
														?>
														<option value="<?php print($Each['UserGroupID']); ?>" <?php echo set_select('USER_SIGNUP_GROUPID', $Each['UserGroupID'], (USER_SIGNUP_GROUPID == $Each['UserGroupID'] ? true : false)); ?>><?php print($Each['GroupName']); ?></option>
														<?php
														endforeach;
														?>
													</select>
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0237', false, '', false); ?></p></div>
													<?php print(form_error('USER_SIGNUP_GROUPID', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('USER_SIGNUP_GROUPIDS[]') != '' ? 'error' : '')); ?>" id="form-row-USER_SIGNUP_GROUPIDS">
													<label for="USER_SIGNUP_GROUPIDS"><?php InterfaceLanguage('Screen', '0239', false); ?>: *</label>
													<select name="USER_SIGNUP_GROUPIDS[]" id="USER_SIGNUP_GROUPIDS" class="select" multiple="multiple" size="5" style="height:100px;">
														<?php
														$ArraySelectedUserGroups = explode(',', USER_SIGNUP_GROUPIDS);
														foreach ($ArrayUserGroups as $Each):
														?>
														<option value="<?php print($Each['UserGroupID']); ?>" <?php echo set_select('USER_SIGNUP_GROUPIDS[]', $Each['UserGroupID'], (in_array($Each['UserGroupID'], $ArraySelectedUserGroups) == true ? true : false)); ?>><?php print($Each['GroupName']); ?></option>
														<?php
														endforeach;
														?>
													</select>
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0238', false, '', false); ?></p></div>
													<?php print(form_error('USER_SIGNUP_GROUPIDS[]', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>

											</div>
										</div>
										<!-- User Sign-Up - END -->

										<!-- Currency and Tax - START -->
										<div tabcollection="form-tabs" id="tab-content-2">
											<div class="form-row <?php print((form_error('PAYMENT_CURRENCY') != '' ? 'error' : '')); ?>" id="form-row-PAYMENT_CURRENCY">
												<label for="PAYMENT_CURRENCY"><?php InterfaceLanguage('Screen', '0243', false); ?>: *</label>
												<input type="text" name="PAYMENT_CURRENCY" value="<?php echo set_value('PAYMENT_CURRENCY', PAYMENT_CURRENCY); ?>" id="PAYMENT_CURRENCY" class="text" />
												<?php print(form_error('PAYMENT_CURRENCY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('PAYMENT_TAX_PERCENT') != '' ? 'error' : '')); ?>" id="form-row-PAYMENT_TAX_PERCENT">
												<label for="PAYMENT_TAX_PERCENT"><?php InterfaceLanguage('Screen', '0244', false); ?>: *</label>
												% <input type="text" name="PAYMENT_TAX_PERCENT" value="<?php echo set_value('PAYMENT_TAX_PERCENT', PAYMENT_TAX_PERCENT); ?>" id="PAYMENT_TAX_PERCENT" class="text" />
												<?php print(form_error('PAYMENT_TAX_PERCENT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>

										</div>
										<!-- Currency and Tax - END -->

										<!-- Notification Emails - START -->
										<div tabcollection="form-tabs" id="tab-content-3">
											<div class="form-row no-bg">
												<p><?php InterfaceLanguage('Screen', '0247', false, '', false); ?></p>
											</div>
											<div class="form-row <?php print((form_error('PAYMENT_RECEIPT_EMAIL_SUBJECT') != '' ? 'error' : '')); ?>" id="form-row-PAYMENT_RECEIPT_EMAIL_SUBJECT">
												<label for="PAYMENT_RECEIPT_EMAIL_SUBJECT"><?php InterfaceLanguage('Screen', '0183', false); ?>: *</label>
												<input type="text" name="PAYMENT_RECEIPT_EMAIL_SUBJECT" value="<?php echo set_value('PAYMENT_RECEIPT_EMAIL_SUBJECT', PAYMENT_RECEIPT_EMAIL_SUBJECT); ?>" id="PAYMENT_RECEIPT_EMAIL_SUBJECT" class="text personalized" style="width:500px;" />
												<div class="form-row-note personalization" id="personalization-PAYMENT_RECEIPT_EMAIL_SUBJECT">
													<p>
														<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
														<select name="personalization-select-PAYMENT_RECEIPT_EMAIL_SUBJECT" id="personalization-select-PAYMENT_RECEIPT_EMAIL_SUBJECT" class="personalization-select">
															<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
															<option value="%User:FirstName%"><?php InterfaceLanguage('Screen', '0630', false, '', false, false); ?></option>
															<option value="%User:LastName%"><?php InterfaceLanguage('Screen', '0631', false, '', false, false); ?></option>
															<option value="%Period:StartDate%"><?php InterfaceLanguage('Screen', '0632', false, '', false, false); ?></option>
															<option value="%Period:FinishDate%"><?php InterfaceLanguage('Screen', '0633', false, '', false, false); ?></option>
															<option value="%Period:TotalAmount%"><?php InterfaceLanguage('Screen', '0634', false, '', false, false); ?></option>
														</select>&nbsp;&nbsp;
													</p>
												</div>
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0256', false, '', false); ?></p></div>
												<?php print(form_error('PAYMENT_RECEIPT_EMAIL_SUBJECT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('PAYMENT_RECEIPT_EMAIL_MESSAGE') != '' ? 'error' : '')); ?>" id="form-row-PAYMENT_RECEIPT_EMAIL_MESSAGE">
												<label for="PAYMENT_RECEIPT_EMAIL_MESSAGE"><?php InterfaceLanguage('Screen', '0184', false); ?>: *</label>
												<textarea name="PAYMENT_RECEIPT_EMAIL_MESSAGE" id="PAYMENT_RECEIPT_EMAIL_MESSAGE" class="textarea personalized" style="height:300px; width:500px;"><?php echo set_value('PAYMENT_RECEIPT_EMAIL_MESSAGE', PAYMENT_RECEIPT_EMAIL_MESSAGE); ?></textarea>
												<div class="form-row-note personalization" id="personalization-PAYMENT_RECEIPT_EMAIL_MESSAGE">
													<p>
														<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
														<select name="personalization-select-PAYMENT_RECEIPT_EMAIL_MESSAGE" id="personalization-select-PAYMENT_RECEIPT_EMAIL_MESSAGE" class="personalization-select">
															<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
															<option value="%User:FirstName%"><?php InterfaceLanguage('Screen', '0630', false, '', false, false); ?></option>
															<option value="%User:LastName%"><?php InterfaceLanguage('Screen', '0631', false, '', false, false); ?></option>
															<option value="%Period:StartDate%"><?php InterfaceLanguage('Screen', '0632', false, '', false, false); ?></option>
															<option value="%Period:FinishDate%"><?php InterfaceLanguage('Screen', '0633', false, '', false, false); ?></option>
															<option value="%Period:TotalAmount%"><?php InterfaceLanguage('Screen', '0634', false, '', false, false); ?></option>
															<option value="%Period:ReceiptDetails%"><?php InterfaceLanguage('Screen', '0635', false, '', false, false); ?></option>
															<option value="%Payment:Links%"><?php InterfaceLanguage('Screen', '0636', false, '', false, false); ?></option>
														</select>&nbsp;&nbsp;
													</p>
												</div>
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0257', false, '', false); ?></p></div>
												<?php print(form_error('PAYMENT_RECEIPT_EMAIL_MESSAGE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>
										<!-- Notification Emails - END -->

										<!-- Payment Gateway - START -->
										<div tabcollection="form-tabs" id="tab-content-4">
											<div class="form-row <?php print((form_error('PAYPALEXPRESSSTATUS') != '' ? 'error' : '')); ?>" id="form-row-PAYPALEXPRESSSTATUS">
												<label for="PAYPALEXPRESSSTATUS"><?php InterfaceLanguage('Screen', '0248', false); ?>:</label>
												<div class="checkbox-container">
													<div class="checkbox-row">
														<input type="checkbox" name="PAYPALEXPRESSSTATUS" value="Enabled" id="PAYPALEXPRESSSTATUS" <?php echo set_checkbox('PAYPALEXPRESSSTATUS', 'Enabled', (PAYPALEXPRESSSTATUS == 'Enabled' ? true : false)); ?>> <label for="PAYPALEXPRESSSTATUS"><?php InterfaceLanguage('Screen', '0249', false, '', false); ?></label>
													</div>
													<div class="checkbox-row">
														<input type="checkbox" name="PAYMENT_CREDITS_GATEWAY_URL_STATUS" value="Enabled" id="PAYMENT_CREDITS_GATEWAY_URL_STATUS" <?php echo set_checkbox('PAYMENT_CREDITS_GATEWAY_URL_STATUS', 'Enabled', (PAYMENT_CREDITS_GATEWAY_URL != '' ? true : false)); ?>> <label for="PAYMENT_CREDITS_GATEWAY_URL_STATUS"><?php InterfaceLanguage('Screen', '1678', false, '', false); ?></label>
													</div>
												</div>
												<?php print(form_error('PAYPALEXPRESSSTATUS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div id="paypal-settings">
												<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0253', false); ?></h3>
												<div class="form-row no-bg">
													<p><?php InterfaceLanguage('Screen', '0254', false, '', false); ?></p>
												</div>
												<div class="form-row <?php print((form_error('PAYPALEXPRESSBUSINESSNAME') != '' ? 'error' : '')); ?>" id="form-row-PAYPALEXPRESSBUSINESSNAME">
													<label for="PAYPALEXPRESSBUSINESSNAME"><?php InterfaceLanguage('Screen', '0250', false); ?>: *</label>
													<input type="text" name="PAYPALEXPRESSBUSINESSNAME" value="<?php echo set_value('PAYPALEXPRESSBUSINESSNAME', PAYPALEXPRESSBUSINESSNAME); ?>" id="PAYPALEXPRESSBUSINESSNAME" class="text" />
													<?php print(form_error('PAYPALEXPRESSBUSINESSNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('PAYPALEXPRESSPURCHASEDESCRIPTION') != '' ? 'error' : '')); ?>" id="form-row-PAYPALEXPRESSPURCHASEDESCRIPTION">
													<label for="PAYPALEXPRESSPURCHASEDESCRIPTION"><?php InterfaceLanguage('Screen', '0170', false); ?>: *</label>
													<input type="text" name="PAYPALEXPRESSPURCHASEDESCRIPTION" value="<?php echo set_value('PAYPALEXPRESSPURCHASEDESCRIPTION', PAYPALEXPRESSPURCHASEDESCRIPTION); ?>" id="PAYPALEXPRESSPURCHASEDESCRIPTION" class="text" />
													<?php print(form_error('PAYPALEXPRESSPURCHASEDESCRIPTION', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
												<div class="form-row <?php print((form_error('PAYPALEXPRESSCURRENCY') != '' ? 'error' : '')); ?>" id="form-row-PAYPALEXPRESSCURRENCY">
													<label for="PAYPALEXPRESSCURRENCY" style="width:170px;"><?php InterfaceLanguage('Screen', '0252', false); ?>: *</label>
													<select name="PAYPALEXPRESSCURRENCY" id="PAYPALEXPRESSCURRENCY" class="select">
														<option value='AUD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'AUD', (PAYPALEXPRESSCURRENCY == 'AUD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'AUD', false); ?></option>
														<option value='GBP' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'GBP', (PAYPALEXPRESSCURRENCY == 'GBD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'GBP', false); ?></option>
														<option value='CAD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'CAD', (PAYPALEXPRESSCURRENCY == 'CAD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'CAD', false); ?></option>
														<option value='CZK' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'CZK', (PAYPALEXPRESSCURRENCY == 'CZK' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'CZK', false); ?></option>
														<option value='DKK' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'DKK', (PAYPALEXPRESSCURRENCY == 'DKK' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'DKK', false); ?></option>
														<option value='EUR' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'EUR', (PAYPALEXPRESSCURRENCY == 'EUR' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'EUR', false); ?></option>
														<option value='HKD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'HKD', (PAYPALEXPRESSCURRENCY == 'HKD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'HKD', false); ?></option>
														<option value='HUF' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'HUF', (PAYPALEXPRESSCURRENCY == 'HUF' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'HUF', false); ?></option>
														<option value='JPY' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'JPY', (PAYPALEXPRESSCURRENCY == 'JPY' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'JPY', false); ?></option>
														<option value='NZD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'NZD', (PAYPALEXPRESSCURRENCY == 'NZD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'NZD', false); ?></option>
														<option value='NOK' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'NOK', (PAYPALEXPRESSCURRENCY == 'NOK' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'NOK', false); ?></option>
														<option value='PLN' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'PLN', (PAYPALEXPRESSCURRENCY == 'PLN' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'PLN', false); ?></option>
														<option value='SGD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'SGD', (PAYPALEXPRESSCURRENCY == 'SGD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'SGD', false); ?></option>
														<option value='SEK' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'SEK', (PAYPALEXPRESSCURRENCY == 'SEK' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'SEK', false); ?></option>
														<option value='CHF' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'CHF', (PAYPALEXPRESSCURRENCY == 'CHF' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'CHF', false); ?></option>
														<option value='USD' <?php echo set_select('PAYPALEXPRESSCURRENCY', 'USD', (PAYPALEXPRESSCURRENCY == 'USD' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0255', false, 'USD', false); ?></option>

													</select>
													<?php print(form_error('PAYPALEXPRESSCURRENCY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
											</div>

											<div id="third-party-payment-settings">
												<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1678', false, '', false, true, array()); ?></h3>
												<div class="form-row <?php print((form_error('PAYMENT_CREDITS_GATEWAY_URL') != '' ? 'error' : '')); ?>" id="form-row-PAYMENT_CREDITS_GATEWAY_URL">
													<label for="PAYMENT_CREDITS_GATEWAY_URL"><?php InterfaceLanguage('Screen', '1679', false); ?>: *</label>
													<input type="text" name="PAYMENT_CREDITS_GATEWAY_URL" value="<?php echo set_value('PAYMENT_CREDITS_GATEWAY_URL', PAYMENT_CREDITS_GATEWAY_URL); ?>" id="PAYMENT_CREDITS_GATEWAY_URL" class="text" />
													<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1680', false); ?></p></div>
													<?php print(form_error('PAYMENT_CREDITS_GATEWAY_URL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
												</div>
											</div>
										</div>
										<!-- Payment Gateway - END -->

									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="ESPSettings" id="ButtonESPSettings"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0270', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="UpdateESPSettings" id="Command" />
						</div>
					</div>
				</form>

				<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/settings_espsettings.js" type="text/javascript" charset="utf-8"></script>		
