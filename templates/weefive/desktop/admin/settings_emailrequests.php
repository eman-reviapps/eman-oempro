				<form id="EmailRequests" method="post" action="<?php InterfaceAppURL(); ?>/admin/emailrequests/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<h2><?php InterfaceLanguage('Screen', '0126', false, '', true); ?></h2>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>
										<div class="form-row no-bg">
											<p><?php InterfaceLanguage('Screen', '0418', false, '', false); ?></p>
										</div>
										<div class="form-row <?php print((form_error('POP3_REQUESTS_STATUS') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_STATUS">
											<label for="POP3_REQUESTS_STATUS"><?php InterfaceLanguage('Screen', '0405', false); ?>:</label>
											<select name="POP3_REQUESTS_STATUS" id="POP3_REQUESTS_STATUS" class="select">
												<option value="Enabled" <?php echo set_select('POP3_REQUESTS_STATUS', 'Enabled', (POP3_REQUESTS_STATUS == 'Enabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0406', false, '', false); ?></option>
												<option value="Disabled" <?php echo set_select('POP3_REQUESTS_STATUS', 'Disabled', (POP3_REQUESTS_STATUS == 'Disabled' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0407', false, '', false); ?></option>
											</select>
											<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0408', false, '', false); ?></p></div>
											<?php print(form_error('POP3_REQUESTS_STATUS', '<div class="form-row-note error"><p>', '</p></div>')); ?>
										</div>
										<div id="requests-pop3-settings">
											<div class="form-row <?php print((form_error('POP3_REQUESTS_HOST') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_HOST">
												<label for="POP3_REQUESTS_HOST"><?php InterfaceLanguage('Screen', '0409', false); ?>: *</label>
												<input type="text" name="POP3_REQUESTS_HOST" value="<?php echo set_value('POP3_REQUESTS_HOST', POP3_REQUESTS_HOST); ?>" id="POP3_REQUESTS_HOST" class="text" />
												<?php print(form_error('POP3_REQUESTS_HOST', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('POP3_REQUESTS_PORT') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_PORT">
												<label for="POP3_REQUESTS_PORT"><?php InterfaceLanguage('Screen', '0410', false); ?>: *</label>
												<input type="text" name="POP3_REQUESTS_PORT" value="<?php echo set_value('POP3_REQUESTS_PORT', POP3_REQUESTS_PORT); ?>" id="POP3_REQUESTS_PORT" class="text" />
												<?php print(form_error('POP3_REQUESTS_PORT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('POP3_REQUESTS_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_USERNAME">
												<label for="POP3_REQUESTS_USERNAME"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
												<input type="text" name="POP3_REQUESTS_USERNAME" value="<?php echo set_value('POP3_REQUESTS_USERNAME', POP3_REQUESTS_USERNAME); ?>" id="POP3_REQUESTS_USERNAME" class="text" />
												<?php print(form_error('POP3_REQUESTS_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('POP3_REQUESTS_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_PASSWORD">
												<label for="POP3_REQUESTS_PASSWORD"><?php InterfaceLanguage('Screen', '0003', false); ?>: *</label>
												<input type="password" name="POP3_REQUESTS_PASSWORD" value="<?php echo set_value('POP3_REQUESTS_PASSWORD', POP3_REQUESTS_PASSWORD); ?>" id="POP3_REQUESTS_PASSWORD" class="text" />
												<?php print(form_error('POP3_REQUESTS_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('POP3_REQUESTS_SSL') != '' ? 'error' : '')); ?>" id="form-row-POP3_REQUESTS_SSL">
												<label for="POP3_REQUESTS_SSL"><?php InterfaceLanguage('Screen', '1656', false); ?>: *</label>
												<div class="checkbox-container">
													<div class="checkbox-row">
														<input type="checkbox" name="POP3_REQUESTS_SSL" value="Yes" id="POP3_REQUESTS_SSL_Yes" <?php echo set_checkbox('POP3_REQUESTS_SSL', 'Yes', (POP3_REQUESTS_SSL == 'Yes' ? true : false)); ?>> <label for="POP3_REQUESTS_SSL_Yes"><?php InterfaceLanguage('Screen', '1658', false, '', false); ?></label>
													</div>
												</div>
												<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1657', false, '', false); ?></p></div>
												<?php print(form_error('POP3_REQUESTS_SSL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>

									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="EmailRequests" id="ButtonEmailRequests"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0269', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditEmailRequests" id="Command" />
						</div>
					</div>
				</form>

				<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/settings_emailrequests.js" type="text/javascript" charset="utf-8"></script>		
