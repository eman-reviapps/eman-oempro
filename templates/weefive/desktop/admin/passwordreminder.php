		<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_header.php'); ?>

		<div class="span-11 push-1">
			<div class="login-box">
				<div class="inner">
					<form id="Form_Reminder" method="post" action="<?php InterfaceAppURL(); ?>/admin/passwordreminder/">
						<?php
						if (isset($PageErrorMessage) == true):
						?>
							<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
						<?php
						else:
						?>
							<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0006', false); ?></h3>
						<?php
						endif;
						?>
						<p><?php InterfaceLanguage('Screen', '0007', false); ?></p>
						<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?> no-background-color" id="form-row-EmailAddress">
							<label for="input1"><?php InterfaceLanguage('Screen', '0008', false); ?>:</label>
							<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', ''); ?>" id="EmailAddress" class="text" style="width:260px;" />
							<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row no-background-color clearfix">
							<label>&nbsp;</label>
							<a id="FormButton_Submit" class="button" style="float:left;" targetform="Form_Reminder"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0267', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="ResetPassword" id="Command" />
						</div>
						<div class="form-row forgot no-background-color small" style="margin-top:9px">
							<label>&nbsp;</label>
							<img src="<?php InterfaceTemplateURL(); ?>images/icon_login_return.png" />
							<a href="<?php InterfaceAppURL(); ?>/admin/"><?php InterfaceLanguage('Screen', '0009', false); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_footer.php'); ?>
