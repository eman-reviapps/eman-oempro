<div id="page-shadow">
	<div id="page">
		<div class="page-bar">
			<ul class="livetabs">
				<li><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0637', false, '', false, false, array(PAYMENT_CURRENCY)); ?></a></li>
			</ul>
		</div>
		<div class="white">
			<div class="clearfix" style="padding-top:9px;">
				<div class="inner flash-chart-container" id="payment-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']) ?>/PaymentHistory/0/ChartData/Payments" style="width:500px;float:left;height:175px;">
				</div>
				<div>
					<div class="data-row">
						<span class="data-label"><?php InterfaceLanguage('Screen', '0639', false, 'Paid', false, false); ?></span>
						<span class="data big"><?php print(Core::FormatCurrency($PaymentStatusTotals['Paid'], false)); ?></span>
						<span class="data"><?php print(PAYMENT_CURRENCY); ?></span>
					</div>
					<div class="data-row">
						<span class="data-label"><?php InterfaceLanguage('Screen', '0639', false, 'Unpaid', false, false); ?></span>
						<span class="data big"><?php print(Core::FormatCurrency($PaymentStatusTotals['Unpaid'], false)); ?></span>
						<span class="data"><?php print(PAYMENT_CURRENCY); ?></span>
					</div>
					<div class="data-row">
						<span class="data-label"><?php InterfaceLanguage('Screen', '0639', false, 'Waiting', false, false); ?></span>
						<span class="data big"><?php print(Core::FormatCurrency($PaymentStatusTotals['Waiting'], false)); ?></span>
						<span class="data"><?php print(PAYMENT_CURRENCY); ?></span>
					</div>
					<div class="data-row">
						<span class="data-label"><?php InterfaceLanguage('Screen', '0639', false, 'Waived', false, false); ?></span>
						<span class="data big"><?php print(Core::FormatCurrency($PaymentStatusTotals['Waived'], false)); ?></span>
						<span class="data"><?php print(PAYMENT_CURRENCY); ?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="page-bar">
			<ul class="livetabs" tabcollection="periods-tab">
				<li id="tab-periods"><a href="#"><?php InterfaceLanguage('Screen', '0638', false, '', false, true); ?></a></li>
			</ul>
		</div>
		<div class="white">
			<div class="inner">
				<div tabcollection="periods-tab" id="tab-content-periods">
					<?php
					if (count($Periods) < 1):
					?>
						<p class="no-data-message"><?php InterfaceLanguage('Screen', '0640', false, '', false); ?></p>
					<?php
					else:
					?>
					<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
						<?php foreach ($Periods as $EachPeriod): ?>
						<tr>
							<td width="60%"><a href="<?php print(InterfaceAppURL(true).'/admin/users/invoice/'.$User['UserID'].'/'.$EachPeriod['LogID']); ?>"><?php print(date('jS, F Y', strtotime($EachPeriod['PeriodStartDate'])).' - '.date('jS, F Y', strtotime($EachPeriod['PeriodEndDate']))); ?></a></td>
							<td width="30%" class="right-align"><span class="data"><?php print(Core::FormatCurrency($EachPeriod['TotalAmount'])); ?></span></td>
							<td width="10%" class="right-align"><?php InterfaceLanguage('Screen', '0639', false, $EachPeriod['PaymentStatus'], false, false, array()); ?></td>
						</tr>
						<?php endforeach; ?>
					</table>
					<?php
					endif;
					?>
				</div>
			</div>
		</div>
	</div>
</div>
