						<div id="page-shadow">
							<div id="page">
								<div class="page-bar">
									<h2><?php InterfaceLanguage('Screen', '0081', false, '', false); ?></h2>
								</div>
								<div class="white">
									<?php
									if (isset($PageErrorMessage) == true):
									?>
										<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
									<?php
									elseif (isset($PageSuccessMessage) == true):
									?>
										<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
									<?php
									elseif (validation_errors()):
									?>
									<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
									<?php
									else :
									?>
									<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
									<?php
									endif;
									?>

									<div class="form-row <?php print((form_error('Recipient') != '' ? 'error' : '')); ?>" id="form-row-Recipient">
										<label for="Recipient"><?php InterfaceLanguage('Screen', '0108', false); ?>: *</label>
										<input type="text" name="Recipient" value="<?php echo set_value('Recipient', $User['EmailAddress']); ?>" id="Recipient" class="text" readonly="readonly" />
										<?php print(form_error('Recipient', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Subject') != '' ? 'error' : '')); ?>" id="form-row-Subject">
										<label for="Subject"><?php InterfaceLanguage('Screen', '0106', false); ?>: *</label>
										<input type="text" name="Subject" value="<?php echo set_value('Subject'); ?>" id="Subject" class="text" />
										<?php print(form_error('Subject', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
									<div class="form-row <?php print((form_error('Message') != '' ? 'error' : '')); ?>" id="form-row-Message">
										<label for="Message"><?php InterfaceLanguage('Screen', '0107', false); ?>: *</label>
										<textarea name="Message" id="Message" class="textarea"><?php echo set_value('Message'); ?></textarea>
										<?php print(form_error('Message', '<div class="form-row-note error"><p>', '</p></div>')); ?>
									</div>
								</div>
							</div>
						</div>


					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="UserEditForm" id="ButtonSendMessage"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0081', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="SendMessage" id="Command" />
						</div>
					</div>
