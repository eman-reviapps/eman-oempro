						<script src="<?php InterfaceTemplateURL(false); ?>js/swfobject.js" type="text/javascript" charset="utf-8"></script>

						<div id="page-shadow">
							<div id="page">
								<?php
								if (isset($PageSuccessMessage) == true):
								?>
								<div class="module-container">
									<div class="page-message success">
										<div class="inner">
											<span class="close">X</span>
											<?php print($PageSuccessMessage); ?>
										</div>
									</div>
								</div>
								<?php
								elseif (isset($PageErrorMessage) == true):									
								?>
								<div class="module-container">
									<div class="page-message error">
										<div class="inner">
											<span class="close">X</span>
											<?php print($PageErrorMessage); ?>
										</div>
									</div>
								</div>
								<?php
								endif;
								?>

								<div class="page-bar">
									<ul class="livetabs" tabcollection="report-tabs">
										<li id="tab-chart-selection" class="dropdown-menu">
											<ul tabcallback="set_active_chart_tab">
												<li id="item-sub-report-unsubscriptions"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0147', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0097', false, '', false); ?></a></li>
												<li id="item-sub-report-bounces"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0147', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0098', false, '', false); ?></a></li>
												<li id="item-sub-report-spam"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0147', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0095', false, '', false); ?></a></li>
												<li id="item-sub-report-subscriptions"><a href="#"><img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0141', false, '', false)?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0097', false, '', false); ?></a></li>
											</ul>
										</li>
									</ul>
									<ul class="livetabs right" tabcollection="report-date-tabs" tabcallback="switch_chart_data_range">
										<li class="label"><?php InterfaceLanguage('Screen', '0093', false, '', false); ?></li>
										<li id="tab-1m">1m</li>
										<li id="tab-3m">3m</li>
										<li id="tab-6m">6m</li>
									</ul>
								</div>
								<div class="white">
									<div class="inner flash-chart-container" id="activity-chart-container">
										<script type="text/javascript" charset="utf-8">
											SWFObjectContainer1 = new SWFObject("<?php InterfaceInstallationURL(); ?>assets/charts/amline/amline.swf", "activity-chart", "706", "150", "8", "#FFFFFF");
											SWFObjectContainer1.addVariable("path", "<?php InterfaceInstallationURL(); ?>assets/charts/amline/");
											SWFObjectContainer1.addVariable("settings_file", escape("<?php InterfaceAppURL(); ?>/admin/chart/settings/line"));
											SWFObjectContainer1.addVariable("data_file", escape("<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print($User['UserID']) ?>/AccountActivity/0/ChartData/Campaigns/Unsubscriptions"));
											SWFObjectContainer1.addVariable("preloader_color", "#FFFFFF");
											SWFObjectContainer1.addParam("wmode", "opaque");
											SWFObjectContainer1.write("activity-chart-container");
										</script>
									</div>
								</div>
								<div class="page-bar">
									<ul class="livetabs" tabcollection="report-tabs-2">
										<li id="tab-latest-campaigns"><a href="#"><?php InterfaceLanguage('Screen', '0099', false, '', false); ?></a></li>
										<li id="tab-lists"><a href="#"><?php InterfaceLanguage('Screen', '0100', false, '', false); ?></a></li>
									</ul>
								</div>
								<div class="white">
									<div class="inner">
										<div tabcollection="report-tabs-2" id="tab-content-latest-campaigns">
											<?php
											if ($Campaigns == false):
											?>
												<p class="no-data-message"><?php InterfaceLanguage('Screen', '0101', false, '', false); ?></p>
											<?php
											else:
											?>
												<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
													<?php
													foreach ($Campaigns as $Index=>$EachCampaign):
														// Unique open percentage calculation - Start {
														$UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
														// Unique open percentage calculation - End }
													?>
														<tr>
															<td width="50%"><?php InterfaceTextCut($EachCampaign['CampaignName'], 50, '...', false); ?></td>
															<td width="20%" class="right-align"><span class="data"><?php print(number_format($EachCampaign['TotalRecipients'])); ?></span> <?php InterfaceLanguage('Screen', '0092', false, '', false); ?></td>
															<td width="30%"><div class="csspie thumb-blue" data="<?php print($UniqueOpenRate); ?>"></div> <span class="data"><?php print(number_format($UniqueOpenRate, 0)); ?>%</span> <?php InterfaceLanguage('Screen', '0094', false, '', false); ?></td>
														</tr>
													<?php
													endforeach;
													?>
												</table>
											<?php
											endif;
											?>
										</div>
										<div tabcollection="report-tabs-2" id="tab-content-lists">
											<?php
											if ($SubscriberLists == false):
											?>
												<p class="no-data-message"><?php InterfaceLanguage('Screen', '0103', false, '', false); ?></p>
											<?php
											else:
											?>
												<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
													<?php
													foreach ($SubscriberLists as $Index=>$EachSubscriberList):
													?>
														<tr>
															<td width="70%"><?php InterfaceTextCut($EachSubscriberList['Name'], 50, '...', false); ?></td>
															<td width="30%" class="right-align"><span class="data"><?php print(number_format($EachSubscriberList['SubscriberCount'])); ?></span> <?php InterfaceLanguage('Screen', '0104', false, '', false); ?></td>
														</tr>
													<?php
													endforeach;
													?>
												</table>
											<?php
											endif;
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script>
						var APP_URL = '<?php InterfaceAppURL(); ?>';
						var UserID = <?php print($User['UserID']) ?>;
						</script>
						<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/edit_user_accountactivity.js" type="text/javascript" charset="utf-8"></script>		
