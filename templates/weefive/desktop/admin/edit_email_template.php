<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0311', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
	<form id="EmailTemplateEditForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/emailtemplates/edit/<?php print($TemplateInformation->TemplateID); ?>" enctype="multipart/form-data">
		<input type="hidden" name="TemplateID" value="<?php print($TemplateInformation->TemplateID); ?>" id="TemplateID" />
		<div class="span-18 last">
			<div id="page-shadow">
				<div id="page">
					<div class="page-bar">
						<ul class="livetabs" tabcollection="email-template-wizard-tabs">
							<li id="tab-1">
								<a href="#"><?php InterfaceLanguage('Screen', '0135', false, '', false); ?></a>
							</li>
							<li id="tab-2">
								<a href="#"><?php InterfaceLanguage('Screen', '0142', false, '', false); ?></a>
							</li>
						</ul>
						<div class="actions">
							<a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/"><?php InterfaceLanguage('Screen', '0143', false); ?></a>
						</div>
					</div>
					<div class="white">
						<?php
						if (isset($PageErrorMessage) == true):
						?>
						<?php
						elseif (isset($PageSuccessMessage) == true):
						?>
							<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
						<?php
						elseif (validation_errors()):
						?>
						<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php
						else :
						?>
						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
						<?php
						endif;
						?>

						<!-- Preferences Section - START -->
						<div tabcollection="email-template-wizard-tabs" id="tab-content-1">
							<div class="form-row <?php print((form_error('TemplateName') != '' ? 'error' : '')); ?>" id="form-row-TemplateName">
								<label for="TemplateName"><?php InterfaceLanguage('Screen', '0051', false); ?>: *</label>
								<input type="text" name="TemplateName" value="<?php echo set_value('TemplateName', $TemplateInformation->TemplateName); ?>" id="TemplateName" class="text" />
								<?php print(form_error('TemplateName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
							<div class="form-row <?php print((form_error('TemplateDescription') != '' ? 'error' : '')); ?>" id="form-row-TemplateDescription">
								<label for="TemplateDescription"><?php InterfaceLanguage('Screen', '0170', false); ?>:</label>
								<textarea name="TemplateDescription" id="TemplateDescription" class="textarea"><?php echo set_value('TemplateDescription', $TemplateInformation->TemplateDescription); ?></textarea>
								<?php print(form_error('TemplateDescription', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
							<div class="form-row <?php print((form_error('TemplateThumbnail') != '' ? 'error' : '')); ?>" id="form-row-TemplateThumbnail">
								<label for="TemplateThumbnail"><?php InterfaceLanguage('Screen', '0171', false); ?>:</label>
								<input type="file" name="TemplateThumbnail" value="" id="TemplateThumbnail">
								<div class="form-row-note" style="margin-top:9px;">
									<a href="<?php InterfaceAppURL(); ?>/admin/emailtemplates/thumbnail/<?php print($TemplateInformation->TemplateID); ?>" target="_blank"><img src="<?php InterfaceAppURL(); ?>/admin/emailtemplates/thumbnail/<?php print($TemplateInformation->TemplateID); ?>" width="50" border="0" /></a>
								</div>
								<?php print(form_error('TemplateThumbnail', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
							<div class="form-row <?php print((form_error('RelOwnerUserID') != '' ? 'error' : '')); ?>" id="form-row-RelOwnerUserID">
								<label for="RelOwnerUserID"><?php InterfaceLanguage('Screen', '0173', false); ?>: *</label>
								<select name="RelOwnerUserID" id="RelOwnerUserID">
									<optgroup label="<?php InterfaceLanguage('Screen', '0112', false, '', false, true); ?>">
										<option value="0"><?php InterfaceLanguage('Screen', '1484', false); ?></option>
										<?php foreach ($UserGroups as $EachUserGroup): ?>
											<option value="-<?php print($EachUserGroup->UserGroupID); ?>" <?php echo set_select('RelOwnerUserID', '-'.$EachUserGroup->UserGroupID, $TemplateInformation->RelOwnerUserID == '-'.$EachUserGroup->UserGroupID); ?>><?php echo $EachUserGroup->GroupName; ?></option>
										<?php endforeach; ?>
									</optgroup>
									<optgroup label="<?php InterfaceLanguage('Screen', '0086', false, '', false, true); ?>">
										<?php foreach ($Users as $EachUser): ?>
											<option value="<?php print($EachUser->UserID); ?>" <?php if ($TemplateInformation->RelOwnerUserID == $EachUser->UserID) { print 'selected'; }?>><?php print($EachUser->FirstName.' '.$EachUser->LastName.' ('.$EachUser->EmailAddress.')'); ?></option>
										<?php endforeach; ?>
									</optgroup>
								</select>
								<?php print(form_error('RelOwnerUserID', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
						</div>
						<!-- Preferences Section - END -->

						<!-- Email Contents Section - START -->
						<div tabcollection="email-template-wizard-tabs" id="tab-content-2">
							<div class="form-row <?php print((form_error('TemplateSubject') != '' ? 'error' : '')); ?>" id="form-row-TemplateSubject">
								<label for="TemplateSubject"><?php InterfaceLanguage('Screen', '0106', false); ?>: *</label>
								<input type="text" name="TemplateSubject" value="<?php echo set_value('TemplateSubject', $TemplateInformation->TemplateSubject); ?>" id="TemplateSubject" class="text personalized" style="width:500px;" />
								<div class="form-row-note personalization" id="personalization-TemplateSubject">
									<p>
										<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
										<select name="personalization-select-TemplateSubject" id="personalization-select-TemplateSubject" class="personalization-select">
											<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
											<?php foreach($Tags as $Label => $TagGroup): ?>
												<optgroup label="<?php print($Label); ?>">
													<?php foreach($TagGroup as $Tag => $Label): ?>
														<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
													<?php endforeach; ?>
												</optgroup>
											<?php endforeach; ?>
										</select>&nbsp;&nbsp;
									</p>
								</div>
								<?php print(form_error('TemplateSubject', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
							<div class="form-row <?php print((form_error('TemplateHTMLContent') != '' ? 'error' : '')); ?>" id="form-row-TemplateHTMLContent">
								<label for="TemplateHTMLContent"><?php InterfaceLanguage('Screen', '0175', false); ?>:</label>
								<textarea name="TemplateHTMLContent" id="TemplateHTMLContent" class="textarea personalized html-code" style="width:500px;height:200px;"><?php echo set_value('TemplateHTMLContent', $TemplateInformation->TemplateHTMLContent); ?></textarea>
								<div class="form-row-note personalization" id="personalization-TemplateHTMLContent">
									<p>
										<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
										<select name="personalization-select-TemplateHTMLContent" id="personalization-select-TemplateHTMLContent" class="personalization-select">
											<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
											<?php foreach($Tags as $Label => $TagGroup): ?>
												<optgroup label="<?php print($Label); ?>">
													<?php foreach($TagGroup as $Tag => $Label): ?>
														<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
													<?php endforeach; ?>
												</optgroup>
											<?php endforeach; ?>
										</select>&nbsp;&nbsp;
									</p>
								</div>
								<?php print(form_error('TemplateHTMLContent', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
							<div class="form-row <?php print((form_error('TemplatePlainContent') != '' ? 'error' : '')); ?>" id="form-row-TemplatePlainContent">
								<label for="TemplatePlainContent"><?php InterfaceLanguage('Screen', '0176', false); ?>:</label>
								<textarea name="TemplatePlainContent" id="TemplatePlainContent" class="textarea personalized plain-text" style="width:500px;height:200px;"><?php echo set_value('TemplatePlainContent', $TemplateInformation->TemplatePlainContent); ?></textarea>
								<div class="form-row-note personalization" id="personalization-TemplatePlainContent">
									<p>
										<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
										<select name="personalization-select-TemplatePlainContent" id="personalization-select-TemplatePlainContent" class="personalization-select">
											<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
											<?php foreach($Tags as $Label => $TagGroup): ?>
												<optgroup label="<?php print($Label); ?>">
													<?php foreach($TagGroup as $Tag => $Label): ?>
														<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
													<?php endforeach; ?>
												</optgroup>
											<?php endforeach; ?>
										</select>&nbsp;&nbsp;
									</p>
								</div>
								<?php print(form_error('TemplatePlainContent', '<div class="form-row-note error"><p>', '</p></div>')); ?>
							</div>
						</div>
						<!-- Email Contents Section - END -->
					</div>
						
				</div>
			</div>
		</div>
		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_admin_editemailtemplate.php'); ?>
		</div>
		<div class="span-18 last">
			<div class="form-action-container">
				<a class="button" id="proceed-to-builder-link"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0146', false, '', true); ?></strong></a>
				<a class="button" id="save-link"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
				<input type="hidden" name="Command" value="ProceedToBuilder" id="Command" />
			</div>
		</div>
	</form>
</div>
<!-- Page - End -->

<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/create_email_template.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
