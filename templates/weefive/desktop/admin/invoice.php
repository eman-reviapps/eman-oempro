<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_invoice_header.php'); ?>

<div id="top" class="small-header">
    <div class="container">
        <div class="span-10">
            <ul class="tabs">
                <li class="selected">
                    <a href="#">
                        <span class="left">&nbsp;</span>
                        <span class="right">&nbsp;</span>
                        <strong><?php InterfaceLanguage('Screen', '0641', false, '', false); ?></strong>
                    </a>
                </li>
            </ul>
        </div>
        <div class="span-14 last small-actions">
            <a id="print-button" class="button"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0643', false, '', true); ?></strong></a>
            <a id="payment-request-button" class="button"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0664', false, '', true); ?></strong></a>
            <a href="<?php InterfaceAppURL(); ?>/admin/users/edit/<?php print $User['UserID']; ?>/PaymentHistory" class="text-action"><?php InterfaceLanguage('Screen', '0642', false); ?></a>
        </div>
    </div>
</div>

<form id="InvoiceEditForm" method="post" action="<?php InterfaceAppURL(); ?>/admin/users/invoice/<?php print($User['UserID']); ?>/<?php print($Period['LogID']); ?>">
    <input type="hidden" name="LogID" value="<?php print($Period['LogID']); ?>" id="LogID">
    <input type="hidden" name="RelUserID" value="<?php print($Period['RelUserID']); ?>" id="RelUserID">
    <input type="hidden" name="Command" value="Update" id="Command">
    <input type="hidden" name="IncludeTax" value="" id="IncludeTax">
    <input type="hidden" name="SendReceipt" value="No" id="SendReceipt">

    <div id="middle" class="small-header">
        <div class="container">
            <div class="span-24 last">
                <div id="invoice">
                    <table border="0" cellspacing="0" cellpadding="0" class="grid">
                        <tr>
                            <td colspan="2" class="no-zebra" style="vertical-align:top">
                                <span class="small"><?php InterfaceLanguage('Screen', '0644', false, '', false, true); ?></span><br />
                                <span class="big"><?php print($User['FirstName'] . ' ' . $User['LastName']); ?></span><br />
                            </td>
                            <td width="260">
                                <br />
                                <span style="font-size:12px;">
                                    <?php if ($User['CompanyName'] != '') {
                                        print($User['CompanyName'] . '<br />');
                                    } ?>
                                    <?php print($User['Street']); ?><br />
<?php print($User['City'] . ' ' . $User['State'] . ' ' . $User['Zip']); ?><br />
<?php if ($User['Country'] != '') {
    InterfaceLanguage('Screen', '0036', false, $User['Country'], false, false);
} ?><br />
                                </span>
                            </td>
                        </tr>
                        <tr class="border-bottom-dark no-zebra">
                            <td colspan="2">
                                <span class="small"><?php InterfaceLanguage('Screen', '0645', false, '', false, true); ?></span><br />
                                <span class="big"><?php print($User['Username']); ?></span>
                            </td>
                            <td>
                                <span class="small"><?php InterfaceLanguage('Screen', '0646', false, '', false, true); ?></span><br />
                                <strong><?php print(date('jS, F Y', strtotime($Period['PeriodStartDate'])) . ' - ' . date('jS, F Y', strtotime($Period['PeriodEndDate']))); ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="280"><?php InterfaceLanguage('Screen', '0647', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left"><span class="data"><?php print($Period['CampaignsSent']); ?></span></td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargePerCampaignSent'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>

<?php if ($User['GroupInformation']['CreditSystem'] == 'Disabled'): ?>
                            <tr>
                                <td width="280"><?php InterfaceLanguage('Screen', '0648', false, '', false, true); ?></td>
                                <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['CampaignsTotalRecipients'])); ?></span></td>
                                <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeTotalCampaignRecipients'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                            </tr>
                            <tr>
                                <td width="280"><?php InterfaceLanguage('Screen', '0649', false, '', false, true); ?></td>
                                <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['AutoRespondersSent'])); ?></span></td>
                                <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeTotalAutoRespondersSent'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                            </tr>
<?php endif; ?>

                        <tr class="border-bottom">
                            <td width="280"><?php InterfaceLanguage('Screen', '0650', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['DesignPreviewRequests'])); ?></span></td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeTotalDesignPrevRequests'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                        <tr>
                            <td width="280"><?php InterfaceLanguage('Screen', '0651', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left">&nbsp;</td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeAutoResponderPeriod'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                        <tr>
                            <td width="280"><?php InterfaceLanguage('Screen', '0652', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left">&nbsp;</td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeDesignPreviewPeriod'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                        <tr class="border-bottom">
                            <td width="280"><?php InterfaceLanguage('Screen', '0653', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left">&nbsp;</td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeSystemPeriod'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                        <tr id="discount-row" style="display:none">
                            <td width="280"><?php InterfaceLanguage('Screen', '0654', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left">&nbsp;</td>
                            <td width="260"><input type="text" name="Discount" value="<?php print(Core::FormatCurrency($Period['Discount'], false)); ?>" id="Discount" class="text" style="width:50px;"> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span>&nbsp;&nbsp;<input type="submit" name="update-total-button" value="<?php InterfaceLanguage('Screen', '0661', false, '', false, false); ?>" id="update-total-button"></td>
                        </tr>
                        <tr id="tax-row" style="display:none">
                            <td width="280"><?php InterfaceLanguage('Screen', '0655', false, '', false, true); ?></td>
                            <td width="100" style="text-align:left"><span class="data"><?php print(PAYMENT_TAX_PERCENT); ?>%</span></td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['Tax'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                        <tr class="border-top-dark">
                            <td width="280"><strong><?php InterfaceLanguage('Screen', '0656', false, '', false, true); ?></strong></td>
                            <td width="100" style="text-align:left">&nbsp;</td>
                            <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['TotalAmount'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <br />
    <br />
    <br />
    <br />

    <div id="invoice-tool-palette">
        <div class="inner">
            <span style="margin-left:18px;">
                <?php InterfaceLanguage('Screen', '0657', false, '', false, false); ?>: 
                <input type="submit" name="tax-button" value="<?php InterfaceLanguage('Screen', '0659', false, '', false, false); ?>" id="tax-button">
                <input type="submit" name="discount-button" value="<?php InterfaceLanguage('Screen', '0660', false, '', false, false); ?>" id="discount-button"> 
            </span>
            <span style="margin-left:18px;">
<?php InterfaceLanguage('Screen', '0658', false, '', false, false); ?>: 
                <select name="PaymentStatus" id="PaymentStatus">
                    <option value="Paid" <?php if ($Period['PaymentStatus'] == 'Paid') {
    print 'selected';
}; ?>><?php InterfaceLanguage('Screen', '0639', false, 'Paid', false, false); ?></option>
                    <option value="Unpaid" <?php if ($Period['PaymentStatus'] == 'Unpaid') {
    print 'selected';
}; ?>><?php InterfaceLanguage('Screen', '0639', false, 'Unpaid', false, false); ?></option>
                    <option value="Waiting" <?php if ($Period['PaymentStatus'] == 'Waiting') {
    print 'selected';
}; ?>><?php InterfaceLanguage('Screen', '0639', false, 'Waiting', false, false); ?></option>
                    <option value="Waived" <?php if ($Period['PaymentStatus'] == 'Waived') {
    print 'selected';
}; ?>><?php InterfaceLanguage('Screen', '0639', false, 'Waived', false, false); ?></option>
                    <option value="NA" <?php if ($Period['PaymentStatus'] == 'NA') {
    print 'selected';
}; ?>><?php InterfaceLanguage('Screen', '0639', false, 'NA', false, false); ?></option>
                </select> 
                <input type="submit" name="status-button" value="<?php InterfaceLanguage('Screen', '0304', false, '', false, false); ?>" id="status-button">
            </span>
        </div>
    </div>

</form>

<script>
    var tax = '<?php print($Period['Tax']); ?>';
    var discount = '<?php print($Period['Discount']); ?>';
    var language = {
        '0659': '<?php InterfaceLanguage('Screen', '0659', false, '', false, false); ?>',
        '0660': '<?php InterfaceLanguage('Screen', '0660', false, '', false, false); ?>',
        '0662': '<?php InterfaceLanguage('Screen', '0662', false, '', false, false); ?>',
        '0663': '<?php InterfaceLanguage('Screen', '0663', false, '', false, false); ?>'
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/invoice.js" type="text/javascript" charset="utf-8"></script>		
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_invoice_footer.php'); ?>
