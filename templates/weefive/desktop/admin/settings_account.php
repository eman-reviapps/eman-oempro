				<form id="AccountUpdate" method="post" action="<?php InterfaceAppURL(); ?>/admin/account/">
							<div id="page-shadow">
								<div id="page">
									<div class="page-bar">
										<ul class="livetabs" tabcollection="form-tabs">
											<li id="tab-1">
												<a href="#"><?php InterfaceLanguage('Screen', '0034', false, '', false, true); ?></a>
											</li>
											<li id="tab-2">
												<a href="#"><?php InterfaceLanguage('Screen', '0129', false, '', false); ?></a>
											</li>
										</ul>
									</div>
									<div class="white" style="min-height:430px;">
										<?php
										if (isset($PageErrorMessage) == true):
										?>
											<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
										<?php
										elseif (isset($PageSuccessMessage) == true):
										?>
											<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
										<?php
										elseif (validation_errors()):
										?>
										<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
										<?php
										else :
										?>
										<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
										<?php
										endif;
										?>
										<!-- Account Information - START -->
										<div tabcollection="form-tabs" id="tab-content-1">
											<div class="form-row <?php print((form_error('Name') != '' ? 'error' : '')); ?>" id="form-row-Name">
												<label for="Name"><?php InterfaceLanguage('Screen', '0051', false); ?>: *</label>
												<input type="text" name="Name" value="<?php echo set_value('Name', $Admin['Name']); ?>" id="Name" class="text" />
												<?php print(form_error('Name', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress">
												<label for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false); ?>: *</label>
												<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', $Admin['EmailAddress']); ?>" id="EmailAddress" class="text" />
												<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('Username') != '' ? 'error' : '')); ?>" id="form-row-Username">
												<label for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
												<input type="text" name="Username" value="<?php echo set_value('Username', $Admin['Username']); ?>" id="Username" class="text" />
												<?php print(form_error('Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('NewPassword') != '' ? 'error' : '')); ?>" id="form-row-NewPassword">
												<label for="NewPassword"><?php InterfaceLanguage('Screen', '0054', false); ?>:</label>
												<input type="password" name="NewPassword" value="<?php echo set_value('NewPassword', ''); ?>" id="NewPassword" class="text" />
												<?php print(form_error('NewPassword', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>
										<!-- Account Information - END -->

										<!-- Email Addresses - START -->
										<div tabcollection="form-tabs" id="tab-content-2">
											<div class="form-row no-bg">
												<p><?php InterfaceLanguage('Screen', '0182', false, '', false); ?></p>
											</div>
											<div class="form-row <?php print((form_error('SYSTEM_EMAIL_FROM_NAME') != '' ? 'error' : '')); ?>" id="form-row-SYSTEM_EMAIL_FROM_NAME">
												<label for="SYSTEM_EMAIL_FROM_NAME"><?php InterfaceLanguage('Screen', '0179', false); ?>: *</label>
												<input type="text" name="SYSTEM_EMAIL_FROM_NAME" value="<?php echo set_value('SYSTEM_EMAIL_FROM_NAME', SYSTEM_EMAIL_FROM_NAME); ?>" id="SYSTEM_EMAIL_FROM_NAME" class="text" />
												<?php print(form_error('SYSTEM_EMAIL_FROM_NAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('SYSTEM_EMAIL_FROM_EMAIL') != '' ? 'error' : '')); ?>" id="form-row-SYSTEM_EMAIL_FROM_EMAIL">
												<label for="SYSTEM_EMAIL_FROM_EMAIL"><?php InterfaceLanguage('Screen', '0180', false); ?>: *</label>
												<input type="text" name="SYSTEM_EMAIL_FROM_EMAIL" value="<?php echo set_value('SYSTEM_EMAIL_FROM_EMAIL', SYSTEM_EMAIL_FROM_EMAIL); ?>" id="SYSTEM_EMAIL_FROM_EMAIL" class="text" />
												<?php print(form_error('SYSTEM_EMAIL_FROM_EMAIL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
											<div class="form-row <?php print((form_error('ALERT_RECIPIENT_EMAIL') != '' ? 'error' : '')); ?>" id="form-row-ALERT_RECIPIENT_EMAIL">
												<label for="ALERT_RECIPIENT_EMAIL"><?php InterfaceLanguage('Screen', '0181', false); ?>: *</label>
												<input type="text" name="ALERT_RECIPIENT_EMAIL" value="<?php echo set_value('ALERT_RECIPIENT_EMAIL', ALERT_RECIPIENT_EMAIL); ?>" id="ALERT_RECIPIENT_EMAIL" class="text" />
												<?php print(form_error('ALERT_RECIPIENT_EMAIL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
											</div>
										</div>
										<!-- Email Addresses - END -->
									</div>
								</div>
							</div>
						
					<div class="span-18 last">
						<div class="form-action-container">
							<a class="button" targetform="AccountUpdate" id="ButtonUpdateUserAccount"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0268', false, '', true); ?></strong></a>
							<input type="hidden" name="Command" value="EditAdmin" id="Command" />
						</div>
					</div>
				</form>
