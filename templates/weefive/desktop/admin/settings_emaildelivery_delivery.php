<div tabcollection="form-tabs" id="tab-content-1">
	<?php if (isset($PageErrorMessage) == true): ?>
		<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
	<?php elseif (isset($PageSuccessMessage) == true): ?>
		<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
	<?php elseif (validation_errors()): ?>
		<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
	<?php else: ?>
		<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
	<?php endif; ?>

	<div class="form-row <?php print((form_error('X_MAILER') != '' ? 'error' : '')); ?>" id="form-row-X_MAILER">
		<label for="X_MAILER"><?php InterfaceLanguage('Screen', '0298', false); ?>: *</label>
		<input type="text" name="X_MAILER" value="<?php echo set_value('X_MAILER', X_MAILER); ?>" id="X_MAILER" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0314', false, '', false); ?></p></div>
		<?php print(form_error('X_MAILER', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('CENTRALIZED_SENDER_DOMAIN') != '' ? 'error' : '')); ?>" id="form-row-CENTRALIZED_SENDER_DOMAIN">
		<label for="CENTRALIZED_SENDER_DOMAIN"><?php InterfaceLanguage('Screen', '1665', false); ?>:</label>
		<input type="text" name="CENTRALIZED_SENDER_DOMAIN" value="<?php echo set_value('CENTRALIZED_SENDER_DOMAIN', CENTRALIZED_SENDER_DOMAIN); ?>" id="CENTRALIZED_SENDER_DOMAIN" class="text" />
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1666', false, '', false); ?></p></div>
		<?php print(form_error('CENTRALIZED_SENDER_DOMAIN', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('SEND_METHOD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD">
		<label for="SEND_METHOD"><?php InterfaceLanguage('Screen', '0118', false); ?>: *</label>
		<select name="SEND_METHOD" id="SEND_METHOD" class="select">
			<optgroup label="<?php InterfaceLanguage('Screen', '1866', false, '', false); ?>">
				<option value="SMTP" <?php echo set_select('SEND_METHOD', 'SMTP', (SEND_METHOD == 'SMTP' && in_array(SEND_METHOD_SMTP_HOST, array('octeth.smtp.com', 'smtp.sendgrid.net', 'smtp.postmarkapp.com', 'smtp.mailgun.org', 'in.mailjet.com') && preg_match('/amazonaws\.com$/i', SEND_METHOD_SMTP_HOST) > 0) != true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0292', false, '', false); ?></option>
			</optgroup>

			<optgroup label="<?php InterfaceLanguage('Screen', '1863', false, '', false); ?>">
				<option value="SMTP-OCTETH" <?php echo set_select('SEND_METHOD', 'SMTP-OCTETH', (SEND_METHOD == 'SMTP' && SEND_METHOD_SMTP_HOST == 'octeth.smtp.com' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0316', false, '', false); ?></option>
				<option value="SMTP-SENDGRID" <?php echo set_select('SEND_METHOD', 'SMTP-SENDGRID', (SEND_METHOD == 'SMTP' && SEND_METHOD_SMTP_HOST == 'smtp.sendgrid.net' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1864', false, '', false); ?></option>
				<option value="SMTP-MAILGUN" <?php echo set_select('SEND_METHOD', 'SMTP-MAILGUN', (SEND_METHOD == 'SMTP' && SEND_METHOD_SMTP_HOST == 'smtp.mailgun.org' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1917', false, '', false); ?></option>
				<option value="SMTP-MAILJET" <?php echo set_select('SEND_METHOD', 'SMTP-MAILJET', (SEND_METHOD == 'SMTP' && SEND_METHOD_SMTP_HOST == 'in.mailjet.com' ? true : false)); ?>><?php InterfaceLanguage('Screen', '1922', false, '', false); ?></option>
				<option value="SMTP-SES" <?php echo set_select('SEND_METHOD', 'SMTP-SES', (SEND_METHOD == 'SMTP' && preg_match('/amazonaws.com$/i', SEND_METHOD_SMTP_HOST) > 0 ? true : false)); ?>><?php InterfaceLanguage('Screen', '1923', false, '', false); ?></option>
				<?php if (1==2): ?>
<!--					<option value="SMTP-POSTMARK" --><?php //echo set_select('SEND_METHOD', 'SMTP-POSTMARK', (SEND_METHOD == 'SMTP' && SEND_METHOD_SMTP_HOST == 'smtp.postmarkapp.com' ? true : false)); ?><!-->--><?php //InterfaceLanguage('Screen', '1869', false, '', false); ?><!--</option>-->
				<?php endif; ?>
			</optgroup>

			<optgroup label="<?php InterfaceLanguage('Screen', '1865', false, '', false); ?>">
				<option value="LocalMTA" <?php echo set_select('SEND_METHOD', 'LocalMTA', (SEND_METHOD == 'LocalMTA' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0293', false, '', false); ?></option>
				<option value="SaveToDisk" <?php echo set_select('SEND_METHOD', 'SaveToDisk', (SEND_METHOD == 'SaveToDisk' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0296', false, '', false); ?></option>
				<option value="PowerMTA" <?php echo set_select('SEND_METHOD', 'PowerMTA', (SEND_METHOD == 'PowerMTA' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0295', false, '', false); ?></option>
				<option value="PHPMail" <?php echo set_select('SEND_METHOD', 'PHPMail', (SEND_METHOD == 'PHPMail' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0294', false, '', false); ?></option>
			</optgroup>
		</select>
		<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0297', false, '', false); ?></p></div>
		<?php print(form_error('SEND_METHOD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div id="sub-smtp-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_HOST') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_HOST">
			<label for="SEND_METHOD_SMTP_HOST"><?php InterfaceLanguage('Screen', '0379', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SMTP_HOST" value="<?php echo set_value('SEND_METHOD_SMTP_HOST', SEND_METHOD_SMTP_HOST); ?>" id="SEND_METHOD_SMTP_HOST" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0380', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SMTP_HOST', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_PORT') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_PORT">
			<label for="SEND_METHOD_SMTP_PORT"><?php InterfaceLanguage('Screen', '0381', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SMTP_PORT" value="<?php echo set_value('SEND_METHOD_SMTP_PORT', SEND_METHOD_SMTP_PORT); ?>" id="SEND_METHOD_SMTP_PORT" class="text" />
			<?php print(form_error('SEND_METHOD_SMTP_PORT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_SECURE') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_SECURE">
			<label for="SEND_METHOD_SMTP_SECURE"><?php InterfaceLanguage('Screen', '0382', false); ?>:</label>
			<select name="SEND_METHOD_SMTP_SECURE" id="SEND_METHOD_SMTP_SECURE" class="select">
				<option value="" <?php echo set_select('SEND_METHOD_SMTP_SECURE', '', (SEND_METHOD_SMTP_SECURE == '' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0384', false, '', false); ?></option>
				<option value="ssl" <?php echo set_select('SEND_METHOD_SMTP_SECURE', 'ssl', (SEND_METHOD_SMTP_SECURE == 'ssl' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0385', false, '', false); ?></option>
				<option value="tls" <?php echo set_select('SEND_METHOD_SMTP_SECURE', 'tls', (SEND_METHOD_SMTP_SECURE == 'tls' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0386', false, '', false); ?></option>
			</select>
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0383', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SMTP_SECURE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_TIMEOUT') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_TIMEOUT">
			<label for="SEND_METHOD_SMTP_TIMEOUT"><?php InterfaceLanguage('Screen', '0387', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SMTP_TIMEOUT" value="<?php echo set_value('SEND_METHOD_SMTP_TIMEOUT', SEND_METHOD_SMTP_TIMEOUT); ?>" id="SEND_METHOD_SMTP_TIMEOUT" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0388', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SMTP_TIMEOUT', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_AUTH') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_AUTH">
			<label for="SEND_METHOD_SMTP_AUTH"><?php InterfaceLanguage('Screen', '0389', false); ?>: *</label>
			<select name="SEND_METHOD_SMTP_AUTH" id="SEND_METHOD_SMTP_AUTH" class="select">
				<option value="true" <?php echo set_select('SEND_METHOD_SMTP_AUTH', 'true', (SEND_METHOD_SMTP_AUTH == true ? true : false)); ?>><?php InterfaceLanguage('Screen', '0390', false, '', false); ?></option>
				<option value="false" <?php echo set_select('SEND_METHOD_SMTP_AUTH', 'false', (SEND_METHOD_SMTP_AUTH == false ? true : false)); ?>><?php InterfaceLanguage('Screen', '0391', false, '', false); ?></option>
			</select>
			<?php print(form_error('SEND_METHOD_SMTP_AUTH', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div id="sub-smtp-settings-auth">
			<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_USERNAME">
				<label for="SEND_METHOD_SMTP_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
				<input type="text" name="SEND_METHOD_SMTP_USERNAME" value="<?php echo set_value('SEND_METHOD_SMTP_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_SMTP_USERNAME" class="text" />
				<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0392', false, '', false); ?></p></div>
				<?php print(form_error('SEND_METHOD_SMTP_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
			</div>
			<div class="form-row <?php print((form_error('SEND_METHOD_SMTP_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SMTP_PASSWORD">
				<label for="SEND_METHOD_SMTP_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
				<input type="password" name="SEND_METHOD_SMTP_PASSWORD" value="<?php echo set_value('SEND_METHOD_SMTP_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_SMTP_PASSWORD" class="text" />
				<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0393', false, '', false); ?></p></div>
				<?php print(form_error('SEND_METHOD_SMTP_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
			</div>
		</div>
	</div>
	<div id="sub-octeth-smtp-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_OCTETHSMTP_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_OCTETHSMTP_USERNAME">
			<label for="SEND_METHOD_OCTETHSMTP_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_OCTETHSMTP_USERNAME" value="<?php echo set_value('SEND_METHOD_OCTETHSMTP_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_OCTETHSMTP_USERNAME" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0377', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_OCTETHSMTP_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_OCTETHSMTP_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_OCTETHSMTP_PASSWORD">
			<label for="SEND_METHOD_OCTETHSMTP_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SEND_METHOD_OCTETHSMTP_PASSWORD" value="<?php echo set_value('SEND_METHOD_OCTETHSMTP_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_OCTETHSMTP_PASSWORD" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0378', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_OCTETHSMTP_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-sendgrid-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_SENDGRID_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SENDGRID_USERNAME">
			<label for="SEND_METHOD_SENDGRID_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SENDGRID_USERNAME" value="<?php echo set_value('SEND_METHOD_SENDGRID_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_SENDGRID_USERNAME" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1867', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SENDGRID_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SENDGRID_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SENDGRID_PASSWORD">
			<label for="SEND_METHOD_SENDGRID_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SEND_METHOD_SENDGRID_PASSWORD" value="<?php echo set_value('SEND_METHOD_SENDGRID_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_SENDGRID_PASSWORD" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1868', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SENDGRID_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-mailgun-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_MAILGUN_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_MAILGUN_USERNAME">
			<label for="SEND_METHOD_MAILGUN_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_MAILGUN_USERNAME" value="<?php echo set_value('SEND_METHOD_MAILGUN_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_MAILGUN_USERNAME" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1918', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_MAILGUN_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_MAILGUN_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_MAILGUN_PASSWORD">
			<label for="SEND_METHOD_MAILGUN_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SEND_METHOD_MAILGUN_PASSWORD" value="<?php echo set_value('SEND_METHOD_MAILGUN_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_MAILGUN_PASSWORD" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1919', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_MAILGUN_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-mailjet-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_MAILJET_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_MAILJET_USERNAME">
			<label for="SEND_METHOD_MAILJET_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_MAILJET_USERNAME" value="<?php echo set_value('SEND_METHOD_MAILJET_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_MAILJET_USERNAME" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1920', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_MAILJET_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_MAILJET_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_MAILJET_PASSWORD">
			<label for="SEND_METHOD_MAILJET_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SEND_METHOD_MAILJET_PASSWORD" value="<?php echo set_value('SEND_METHOD_MAILJET_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_MAILJET_PASSWORD" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1921', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_MAILJET_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-ses-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_SES_HOST') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SES_HOST">
			<label for="SEND_METHOD_SES_HOST"><?php InterfaceLanguage('Screen', '0379', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SES_HOST" value="<?php echo set_value('SEND_METHOD_SES_HOST', SEND_METHOD_SMTP_HOST); ?>" id="SEND_METHOD_SES_HOST" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1926', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SES_HOST', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SES_SECURE') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SES_SECURE">
			<label for="SEND_METHOD_SES_SECURE"><?php InterfaceLanguage('Screen', '0382', false); ?>:</label>
			<select name="SEND_METHOD_SES_SECURE" id="SEND_METHOD_SES_SECURE" class="select">
				<option value="tls" <?php echo set_select('SEND_METHOD_SES_SECURE', 'tls', (SEND_METHOD_SMTP_SECURE == 'tls' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0386', false, '', false); ?></option>
			</select>
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1927', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SES_SECURE', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SES_USERNAME') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SES_USERNAME">
			<label for="SEND_METHOD_SES_USERNAME"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SES_USERNAME" value="<?php echo set_value('SEND_METHOD_SES_USERNAME', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_SES_USERNAME" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1924', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SES_USERNAME', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_SES_PASSWORD') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SES_PASSWORD">
			<label for="SEND_METHOD_SES_PASSWORD"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SEND_METHOD_SES_PASSWORD" value="<?php echo set_value('SEND_METHOD_SES_PASSWORD', SEND_METHOD_SMTP_PASSWORD); ?>" id="SEND_METHOD_SES_PASSWORD" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1925', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SES_PASSWORD', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-postmark-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_POSTMARK_APIKEY') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_POSTMARK_APIKEY">
			<label for="SEND_METHOD_POSTMARK_APIKEY"><?php InterfaceLanguage('Screen', '1870', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_POSTMARK_APIKEY" value="<?php echo set_value('SEND_METHOD_POSTMARK_APIKEY', SEND_METHOD_SMTP_USERNAME); ?>" id="SEND_METHOD_POSTMARK_APIKEY" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '1871', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_POSTMARK_APIKEY', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-local-mta-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_LOCALMTA_PATH') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_LOCALMTA_PATH">
			<label for="SEND_METHOD_LOCALMTA_PATH"><?php InterfaceLanguage('Screen', '0373', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_LOCALMTA_PATH" value="<?php echo set_value('SEND_METHOD_LOCALMTA_PATH', SEND_METHOD_LOCALMTA_PATH); ?>" id="SEND_METHOD_LOCALMTA_PATH" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0374', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_LOCALMTA_PATH', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-php-mail-settings">
		<!-- No settings for PHP mail() function -->
	</div>
	<div id="sub-powermta-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_POWERMTA_VMTA') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_POWERMTA_VMTA">
			<label for="SEND_METHOD_POWERMTA_VMTA"><?php InterfaceLanguage('Screen', '0320', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_POWERMTA_VMTA" value="<?php echo set_value('SEND_METHOD_POWERMTA_VMTA', SEND_METHOD_POWERMTA_VMTA); ?>" id="SEND_METHOD_POWERMTA_VMTA" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0369', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_POWERMTA_VMTA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SEND_METHOD_POWERMTA_DIR') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_POWERMTA_DIR">
			<label for="SEND_METHOD_POWERMTA_DIR"><?php InterfaceLanguage('Screen', '0371', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_POWERMTA_DIR" value="<?php echo set_value('SEND_METHOD_POWERMTA_DIR', SEND_METHOD_POWERMTA_DIR); ?>" id="SEND_METHOD_POWERMTA_DIR" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0372', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_POWERMTA_DIR', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<div id="sub-savetodisk-settings">
		<div class="form-row <?php print((form_error('SEND_METHOD_SAVETODISK_DIR') != '' ? 'error' : '')); ?>" id="form-row-SEND_METHOD_SAVETODISK_DIR">
			<label for="SEND_METHOD_SAVETODISK_DIR"><?php InterfaceLanguage('Screen', '0317', false); ?>: *</label>
			<input type="text" name="SEND_METHOD_SAVETODISK_DIR" value="<?php echo set_value('SEND_METHOD_SAVETODISK_DIR', SEND_METHOD_SAVETODISK_DIR); ?>" id="SEND_METHOD_SAVETODISK_DIR" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0318', false, '', false); ?></p></div>
			<?php print(form_error('SEND_METHOD_SAVETODISK_DIR', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1465', false, '', false, true); ?></h3>
	<div class="form-row no-bg" style="margin-bottom:18px;">
		<p style="margin-bottom:9px;"><?php InterfaceLanguage('Screen', '1466'); ?></p>
		<a style="float:left;" class="button" id="ButtonTestEmailDeliverySettings"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1464', false, '', true); ?></strong></a>
	</div>
</div>