<SHOW:Form>
	<form id="form1" name="form1" action="./forward_email.php" method="post">
		<div id="messagebox">
			<h2 class="title">_{Language:1513}_</h2>
			<h3>_{Language:1514}_</h3>
			<p>_{Language:1515}_</p>

			<ERROR:PageErrorMessage>
				<p>_Error_</p>
			</ERROR:PageErrorMessage>

			<ERROR:PageNoticeMessage>
				<p>_Error_</p>
			</ERROR:PageNoticeMessage>

			<fieldset>
				<div>
					<label>_{Language:1516}_</label>
					<input type="text" name="FormValue_FriendName" value="_EnteredFriendName_" id="FormValue_FriendName" class="_ErrorClassFriendName_" />
					<ERROR:FriendName>
						<span>_Error_</span>
					</ERROR:FriendName>
				</div>
				<div>
					<label>_{Language:1517}_</label>
					<input type="text" name="FormValue_FriendEmail" value="_EnteredFriendEmail_" id="FormValue_FriendEmail" class="_ErrorClassFriendName_" />
					<ERROR:FriendEmail>
						<span>_Error_</span>
					</ERROR:FriendEmail>
				</div>
				<div>
					<label>_{Language:1518}_</label>
					<input type="text" name="FormValue_SenderName" value="_EnteredSenderName_" id="FormValue_SenderName" class="_ErrorClassSenderName_" />
					<ERROR:SenderName>
						<span>_Error_</span>
					</ERROR:SenderName>
				</div>
				<div>
					<label>_{Language:1519}_</label>
					<input type="text" name="FormValue_SenderEmail" value="_EnteredSenderEmail_" id="FormValue_SenderEmail" class="_ErrorClassSenderName_" />
					<ERROR:SenderEmail>
						<span>_Error_</span>
					</ERROR:SenderEmail>
				</div>
				<div>
					<label>_{Language:1520}_</label>
					<textarea name="FormValue_Message" class="_ErrorClassMessage_" style="height:50px;">_EnteredMessage_</textarea>
					<ERROR:Message>
						<span>_Error_</span>
					</ERROR:Message>
				</div>
			</fieldset>
		
			<div>
				<input type="submit" name="FormButton_Forward" value="_{Language:1513}_" id="FormValue_Forward" />
				<input type="hidden" name="p" value="_Insert:QueryParameter_" id="p" />
			</div>
			<p>&nbsp;</p>
			<p class="notice">_{Language:1521}_</p>
		</div>
	</form>
</SHOW:Form>

<SHOW:Result>
	<div id="messagebox">
		<h2 class="title">_{Language:1522}_</h2>
		<h3>_{Language:1523}_</h3>
		<p>_Insert:SuccessMessage_</p>

		<p class="notice">_{Language:1521}_</p>
	</div>
</SHOW:Result>
