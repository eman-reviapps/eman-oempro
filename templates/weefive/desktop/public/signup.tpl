<SHOW:Form>
	<form id="form1" name="form1" action="./signup.php" method="post">
		<div id="messagebox">
			<h2 class="title">_{Language:1681}_</h2>
			<h3>_{Language:1682}_</h3>
			<p>_{Language:1683}_</p>

			<ERROR:PageErrorMessage>
				<p>_Error_</p>
			</ERROR:PageErrorMessage>

			<ERROR:PageNoticeMessage>
				<p>_Error_</p>
			</ERROR:PageNoticeMessage>

			<fieldset>
				<SHOWHIDE:PlanSelection>
				<div>
					<label>_{Language:1703}_</label>
					<select id="FormValue_RelUserGroupID" name="FormValue_RelUserGroupID" class="_ErrorClassRelUserGroupID_">
						<LIST:UserGroups>
							<option value="_UserGroupID_">_GroupName_</option>
						</LIST:UserGroups>
					</select>
					<ERROR:RelUserGroupID>
						<span>_Error_</span>
					</ERROR:RelUserGroupID>
				</div>
				</SHOWHIDE:PlanSelection>
				<div>
					<label>_{Language:1689}_</label>
					<input type="text" name="FormValue_EmailAddress" value="_EnteredEmailAddress_" id="FormValue_EmailAddress" class="_ErrorClassEmailAddress_" />
					<ERROR:EmailAddress>
						<span>_Error_</span>
					</ERROR:EmailAddress>
				</div>
				<div>
					<label>_{Language:1690}_</label>
					<input type="text" name="FormValue_Username" value="_EnteredUsername_" id="FormValue_Username" class="_ErrorClassUsername_" />
					<ERROR:Username>
						<span>_Error_</span>
					</ERROR:Username>
				</div>
				<div>
					<label>_{Language:1691}_</label>
					<input type="password" name="FormValue_Password" value="_EnteredPassword_" id="FormValue_Password" class="_ErrorClassPassword_" />
					<ERROR:Password>
						<span>_Error_</span>
					</ERROR:Password>
				</div>
				<ROW:ExtraFields:TextField>
					<div>
						<label>_List:FieldTitle_</label>
						<input type="text" name="FormValue__List:FieldName_" value="_Entered_List:FieldName__" id="FormValue__List:FieldName_" class="_ErrorClass_List:FieldName__" />
						<ERROR:_List:FieldName_>
							<span>_Error_</span>
						</ERROR:_List:FieldName_>
					</div>
				</ROW:ExtraFields:TextField>
				<ROW:ExtraFields:DropList>
					<div>
						<label>_List:FieldTitle_</label>
						<select id="FormValue__List:FieldName_" name="FormValue__List:FieldName_" class="_ErrorClass_List:FieldName__">
							<LIST:Options>
								<option value="_Option:Value_" _Selected_List:FieldName__Option:Value__>_Option:Name_</option>
							</LIST:Options>
						</select>
						<ERROR:_List:FieldName_>
							<span>_Error_</span>
						</ERROR:_List:FieldName_>
					</div>
				</ROW:ExtraFields:DropList>
			</fieldset>
		
			<div>
				<input type="submit" name="FormButton_SignUp" value="_{Language:1684}_" id="FormButton_SignUp" />
			</div>
			<p>&nbsp;</p>
			<p class="notice">_{Language:1685}_</p>
		</div>
	</form>
</SHOW:Form>

<SHOW:Result>
	<div id="messagebox">
		<h2 class="title">_{Language:1686}_</h2>
		<h3>_{Language:1687}_</h3>
		<p>_Insert:SuccessMessage_</p>
		<p>_{Language:1701}_</p>
		<p><a href="_Insert:LoginLink_">_Insert:LoginLink_</a></p>

		<p class="notice">_{Language:1685}_</p>
	</div>
</SHOW:Result>
