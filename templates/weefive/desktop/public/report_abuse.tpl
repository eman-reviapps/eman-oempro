<SHOW:Form>
	<form id="form1" name="form1" action="./report_abuse.php" method="post">
		<div id="messagebox">
			<h2 class="title">_{Language:1576}_</h2>
			<h3>_{Language:1577}_</h3>
			<p>_{Language:1578}_</p>

			<ERROR:PageErrorMessage>
				<p>_Error_</p>
			</ERROR:PageErrorMessage>

			<ERROR:PageNoticeMessage>
				<p>_Error_</p>
			</ERROR:PageNoticeMessage>

			<fieldset>
				<div>
					<label>_{Language:1579}_</label>
					<input type="text" name="FormValue_MessageID" value="_EnteredMessageID_" id="FormValue_MessageID" class="_ErrorClassMessageID_" readonly="readonly" />
					<ERROR:MessageID>
						<span>_Error_</span>
					</ERROR:MessageID>
				</div>
				<div>
					<label>_{Language:1580}_</label>
					<textarea name="FormValue_Message" id="FormValue_Message" class="_ErrorClassMessage_" style="height:75px;">_EnteredMessage_</textarea>
					<ERROR:Message>
						<span>_Error_</span>
					</ERROR:Message>
				</div>
				<div>
					<label>_{Language:1581}_</label>
					<input type="text" name="FormValue_Name" value="_EnteredName_" id="FormValue_Name" class="_ErrorClassName_" />
					<ERROR:Name>
						<span>_Error_</span>
					</ERROR:Name>
				</div>
				<div>
					<label>_{Language:1582}_</label>
					<input type="text" name="FormValue_Email" value="_EnteredEmail_" id="FormValue_Email" class="_ErrorClassEmail_" />
					<ERROR:Email>
						<span>_Error_</span>
					</ERROR:Email>
				</div>
			</fieldset>
		
			<div>
				<input type="submit" name="FormButton_Submit" value="_{Language:1583}_" id="FormButton_Submit" />
			</div>
		</div>
	</form>
</SHOW:Form>

<SHOW:Result>
	<div id="messagebox">
		<h2 class="title">_{Language:1584}_</h2>
		<h3>&nbsp;</h3>
		<p>_{Language:1585}_</p>
	</div>
</SHOW:Result>
