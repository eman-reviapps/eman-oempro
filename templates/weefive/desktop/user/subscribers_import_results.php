<div class="row">
    <div class="col-md-8" >
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post">

                    <div class="alert alert-info">
                        <?php InterfaceLanguage('Screen', '1186', false, '', false, true); ?>
                    </div>

                    <div class="form-row no-bg">
                        <span class="help-block"><?php InterfaceLanguage('Screen', '1187'); ?></span>
                    </div>
                    <div class="form-row no-bg">
                        <p><span class="data big"><?php echo $ImportResult[0] == '' ? 0 : $ImportResult[0]; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '1188', false, '', false, true); ?></span></p>
                        <p><span class="data big"><?php echo $ImportResult[1] == '' ? 0 : $ImportResult[1]; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '1189', false, '', false, true); ?></span></p>
                        <p><span class="data big"><?php echo $ImportResult[2] == '' ? 0 : $ImportResult[2]; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '1190', false, '', false, true); ?></span> <?php if ($ImportResult[2] > 0): ?><a href="<?php InterfaceAppURL(); ?>/user/subscribers/download/duplicateimportdata/<?php echo $ImportID; ?>"><?php InterfaceLanguage('Screen', '1054', false); ?></a><?php endif; ?></p>
                        <p><span class="data big"><?php echo $ImportResult[3] == '' ? 0 : $ImportResult[3]; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '1191', false, '', false, true); ?></span> <?php if ($ImportResult[3] > 0): ?><a href="<?php InterfaceAppURL(); ?>/user/subscribers/download/failedimportdata/<?php echo $ImportID; ?>"><?php InterfaceLanguage('Screen', '1054', false); ?></a><?php endif; ?></p>
                        <?php if (isset($ImportResult[4])): ?>
                            <p><span class="data big"><?php echo $ImportResult[4] == '' ? 0 : $ImportResult[4]; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '1935', false, '', false, true); ?></span> (<?php InterfaceLanguage('Screen', '1936', false, '', false, true); ?>)</p>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersimportresults.php'); ?>
    </div>
</div>
<div class="row">
<div class="col-md-8">
    <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
</div>
</div>

