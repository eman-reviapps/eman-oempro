<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-purple-sharp sbold">
                        <?php InterfaceLanguage('Screen', '1126', false, '', false, true); ?>
                    </span>
                </div>
                <div class="actions">
                    <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php echo $ListInformation['ListID'] ?>"><strong><?php InterfaceLanguage('Screen', '1134', false, '', false); ?></strong></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscribers-export" action="<?php InterfaceAppURL(); ?>/user/subscribers/export/<?php echo $ListInformation['ListID']; ?>" method="post">
                    <input type="hidden" name="Command" value="ExportSubscribers" id="Command">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0037', false); ?></h3>
                        <?php
                        endif;
                        ?>
                        <div class="form-group clearfix <?php print((form_error('Name') != '' ? 'error' : '')); ?>" id="form-row-Name">
                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0940', false, '', false, true); ?>:</label>
                            <div class="col-md-6">
                                <input type="text"value="<?php echo $ListInformation['Name']; ?>" class="form-control" readonly="readonly" />
                            </div>
                        </div>
                        <div class="form-group clearfix" id="form-row-Subscribers">
                            <label class="col-md-3 control-label" for="Subscribers"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?>:</label>
                            <div class="col-md-6">
                                <select name="Subscribers" id="Subscribers" class="form-control">
                                    <option value="Active" <?php echo set_select('Subscribers', 'Active'); ?>><?php InterfaceLanguage('Screen', '1109'); ?></option>
                                    <option value="Suppressed" <?php echo set_select('Subscribers', 'Suppressed'); ?>><?php InterfaceLanguage('Screen', '1110'); ?></option>
                                    <option value="Unsubscribed" <?php echo set_select('Subscribers', 'Unsubscribed'); ?>><?php InterfaceLanguage('Screen', '0910'); ?></option>
                                    <optgroup label="<?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?>">
                                        <?php foreach ($Segments as $EachSegment): ?>
                                            <option value="<?php echo $EachSegment['SegmentID']; ?>" <?php echo set_select('Subscribers', $EachSegment['SegmentID']); ?>><?php echo $EachSegment['SegmentName']; ?></option>
                                        <?php endforeach; ?>
                                    </optgroup>
                                    <optgroup label="<?php InterfaceLanguage('Screen', '1111', false, '', false, true); ?>">
                                        <option value="Soft bounced" <?php echo set_select('Subscribers', 'Soft bounced'); ?>><?php InterfaceLanguage('Screen', '1112'); ?></option>
                                        <option value="Hard bounced" <?php echo set_select('Subscribers', 'Hard bounced'); ?>><?php InterfaceLanguage('Screen', '1113'); ?></option>
                                    </optgroup>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group clearfix" id="form-row-FileFormat">
                            <label class="col-md-3 control-label" for="FileFormat"><?php InterfaceLanguage('Screen', '1127', false, '', false, true); ?>:</label>
                            <div class="col-md-6">
                                <select name="FileFormat" id="FileFormat" class="form-control">
                                    <option value="csv"><?php InterfaceLanguage('Screen', '1128'); ?></option>
                                    <option value="xml"><?php InterfaceLanguage('Screen', '1129'); ?></option>
                                    <option value="tab"><?php InterfaceLanguage('Screen', '1130'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix <?php print((form_error('Fields') != '' ? 'error' : '')); ?>" id="form-row-Fields">
                            <label class="col-md-3 control-label" for="Fields"><?php InterfaceLanguage('Screen', '1131', false, '', false, true); ?>: *</label>
                            <div class="col-md-3">
                                <select name="Fields[]" id="Fields" class="select" multiple style="height:150px;">
                                    <?php foreach ($DefaultFields as $Each): ?>
                                        <option value="<?php echo $Each['CustomFieldID']; ?>"><?php echo ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'][$Each['CustomFieldID']]; ?></option>
                                    <?php endforeach; ?>
                                    <?php foreach ($CustomFields as $Each): ?>
                                        <option value="CustomField<?php echo $Each['CustomFieldID']; ?>"><?php echo $Each['FieldName']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php print(form_error('Fields', '<div class="form-group-note error"><p>', '</p></div>')); ?>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group-note">
                                    <p><?php InterfaceLanguage('Screen', '1132'); ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn default" targetform="subscribers-export" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1126', false, '', true); ?></strong></a>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersexport.php'); ?>
    </div>
</div>


<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers_remove.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>