<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>

<div class="container" style="margin-top:72px;">
	<div class="span-5">&nbsp;</div>
	<div class="span-11 push-1">
		<div class="login-box">
			<div class="inner">
				<h3 class="form-legend"><?php echo $message_title; ?></h3>
				<p><?php echo $message; ?></p>
			</div>
		</div>
	</div>
</div>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>