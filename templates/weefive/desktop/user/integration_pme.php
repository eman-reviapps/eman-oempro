<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1721', false, '', false, true, array('PreviewMyEmail')); ?> </span>                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >                                                              
                        <a href="<?php InterfaceAppURL(); ?>/user/integration/" class="btn default btn-transparen btn-sm" ><?php InterfaceLanguage('Screen', '1722', false); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <?php if ($PageSuccessMessage != ''): ?>
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php print($PageSuccessMessage); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <form id="Integration" method="post" action="<?php InterfaceAppURL(); ?>/user/integration/pme/">
                        <input type="hidden" name="Command" value="EditPMESettings" id="Command"/>
                        <div class="form-row no-bg">
                            <p><?php InterfaceLanguage('Screen', '1492'); ?></p>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('PreviewMyEmailAPIKey') != '' ? 'has-error' : '')); ?>" id="form-row-PreviewMyEmailAPIKey">
                                    <label class="col-md-3 control-label" for="PreviewMyEmailAPIKey" style="width:170px;"><?php InterfaceLanguage('Screen', '0208', false); ?> : *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="PreviewMyEmailAPIKey" value="<?php echo set_value('PreviewMyEmailAPIKey', $UserInformation['PreviewMyEmailAPIKey']); ?>" id="PreviewMyEmailAPIKey" class="form-control"/>
                                        <?php print(form_error('PreviewMyEmailAPIKey', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn default" targetform="Integration" id="ButtonIntegration"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0274', false, '', true); ?></strong></a>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="page-shadow">
    <div id="page">
        <div class="white" style="min-height:380px;padding-top:9px;" id="page-user-pme">


        </div>
    </div>
</div>

<div class="span-18 last">
    <div class="form-action-container">

    </div>
</div>

<script src="<?php InterfaceTemplateURL(false); ?>js/screens/user/settings_integration.js"
type="text/javascript" charset="utf-8"></script>
