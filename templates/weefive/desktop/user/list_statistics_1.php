<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>
<?php // include_once(TEMPLATE_PATH . 'desktop/user/home-flot-chart.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet">
                            <!--<div class="portlet-title">-->
<!--                                <div class="caption">
                                    <i class="icon-doc font-purple-sharp"></i>
                                    <span class="caption-subject bold font-purple-sharp"> 
                                        <img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag_orange.png" /> <?php InterfaceLanguage('Screen', '0141', false, '', false) ?> &nbsp;<img src="<?php InterfaceTemplateURL(); ?>images/icon_zigzag.png" /> <?php InterfaceLanguage('Screen', '0841', false, '', false); ?>
                                    </span>                    
                                </div>-->
                            <!--</div>-->
                            <div class="portlet-body">
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if (isset($PageSuccessMessage) == true): ?>
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <?php print($PageSuccessMessage); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet light  portlet-fit">
                                            <div class="portlet-title">
                                                <div class="caption" style="padding: 0">
                                                    <div class="form-group" style="margin-bottom: 0">
                                                        <select class="form-control input-small" id="DaysSelect">
                                                            <option value="7" selected>7 days</option>
                                                            <option value="15">15 days</option>
                                                            <option value="30">30 days</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="actions" style="padding: 0">
                                                    <div class="form-group" style="margin-bottom: 0">
                                                        <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxOpens" value="<?php InterfaceLanguage('Screen', '0837') ?>"> <?php InterfaceLanguage('Screen', '0837') ?>
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxClicks" value="<?php InterfaceLanguage('Screen', '0838') ?>"> <?php InterfaceLanguage('Screen', '0838') ?>
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxUnsubscriptions" value="<?php InterfaceLanguage('Screen', '0841') ?>"> <?php InterfaceLanguage('Screen', '0841') ?>
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxForwards" value="<?php InterfaceLanguage('Screen', '0839') ?>"> <?php InterfaceLanguage('Screen', '0839') ?>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="chart_3" class="chart" style="padding: 0px; position: relative;">
                                                    <canvas class="flot-base" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                    <canvas class="flot-overlay" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div id="chart_5" style="height:350px;"> </div>-->
<!--                                <div class="inner flash-chart-container" id="activity-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/user/list/chartdata/<?php print($ListInformation['ListID']) ?>/30" style="height:150px;">
                                </div>-->
                            </div>
                        </div>
                        <div class="portlet light">
                            <div class="portlet-body">

                                <div class="custom-column-container cols-2 clearfix">
                                    <div class="col">
                                        <img src="<?php echo InterfaceAppURL(true) . '/user/list/sparkline/opens/' . $ListInformation['ListID']; ?>" width="70" /> <span class="data big"><?php echo $ListInformation['OverallOpenPerformance']; ?>%</span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0975', false, '', false, true); ?></span><br />
                                        <span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?></span> <span class="data"><?php echo $ListInformation['OverallAccountOpenPerformance']; ?>%</span> <span class="data small" style="color:#<?php if ($ListInformation['OpenPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['OpenPerformanceDifference']; ?>%)</span><br />
                                        <?php if ($ListInformation['OverallOpenPerformance'] != 0): ?><span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $ListInformation['HighestOpenDay']; ?></span><br /><?php endif; ?>
                                        <img src="<?php echo InterfaceAppURL(true) . '/user/list/sparkline/clicks/' . $ListInformation['ListID']; ?>" width="70" /> <span class="data big"><?php echo $ListInformation['OverallClickPerformance']; ?>%</span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0976', false, '', false, true); ?></span><br />
                                        <span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?></span> <span class="data"><?php echo $ListInformation['OverallAccountClickPerformance']; ?>%</span> <span class="data small" style="color:#<?php if ($ListInformation['ClickPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['ClickPerformanceDifference']; ?>%)</span><br />
                                        <?php if ($ListInformation['OverallClickPerformance'] != 0): ?><span class="data-label small" style="margin-left:75px;"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?></span> <span class="data"><?php echo $ListInformation['HighestClickDay']; ?></span><br /><?php endif; ?>
                                        <img src="<?php echo InterfaceAppURL(true) . '/user/list/sparkline/forwards/' . $ListInformation['ListID']; ?>" width="70" /> <span class="data-label"><?php InterfaceLanguage('Screen', '0839', false, '', false, true); ?></span><br />
                                        <img src="<?php echo InterfaceAppURL(true) . '/user/list/sparkline/views/' . $ListInformation['ListID']; ?>" width="70" /> <span class="data-label"><?php InterfaceLanguage('Screen', '0840', false, '', false, true); ?></span><br />
                                    </div>
                                    <div class="col" style="background-color:#f2f2f2;border:1px solid #e1e1e1;">
                                        <div class="flash-chart-container" style="float:right;text-align:right;width:140px;padding:18px 18px 0 0;">
                                            <div id="bounce-pie-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie/5" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/user/list/chartdata/<?php print($ListInformation['ListID']) ?>/30/bounces" style="width:140px;height:140px;float:right;text-align:right;">
                                            </div>
                                            <div style="width:140px;text-align:center;">
                                                <span class="data-label"><?php InterfaceLanguage('Screen', '0977', false, '', false, false); ?></span>
                                            </div>
                                        </div>
                                        <div style="padding:9px 12px;" class="clearfix">
                                            <span class="data big"><?php echo $ListInformation['SubscriberCount']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?></span><br />
                                            <span class="data"><?php echo $ListInformation['TotalSpamComplaints']; ?></span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0095', false, '', false, true); ?></span><br />
                                            <div id="email-chart" data-oempro-chart-options-type="pie" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/pie" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/user/list/chartdata/<?php print($ListInformation['ListID']) ?>/30/email-domain" style="width:80px;height:80px;">
                                            </div>
                                            <div style="width:80px;text-align:center;">
                                                <span class="data-label"><?php InterfaceLanguage('Screen', '0978', false, '', false, false); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/list/exportstatistics/<?php echo $ListInformation['ListID']; ?>/csv"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0844', false, '', true); ?></strong></a>
                                        <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/list/exportstatistics/<?php echo $ListInformation['ListID']; ?>/xml"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0845', false, '', true); ?></strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>