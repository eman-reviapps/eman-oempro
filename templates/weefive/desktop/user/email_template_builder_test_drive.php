<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_template_editor_header.php'); ?>
<div class="container" style="margin-top: 0px">
    <div class="col-md-12">
        <div class="portlet box blue-hoki">
            <div class="portlet-title"  style="background: #444d58">
                <div class="caption">
                    <i class="fa fa-tag"></i>
                    <?php InterfaceLanguage('Screen', '0502', false, '', false); ?>
                    ( <?php print $TemplateInformation['TemplateName']; ?> )
                </div>
                <div class="actions">
                    <a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/builder/" class="btn btn-default">Back to template builder</a>
                </div>
            </div>
            <div class="portlet-body" style="background: #f2f2f2;">
                <iframe name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>


<div class="panel-container">
    <div class="panel-inner">

        <div id="single-editable-panel" style="display:none">
            <div class="properties-panel">
                <input type="text" name="single-editable-text-input" value="" id="single-editable-text-input" class="text" style="width:960px" />
            </div>
        </div>
        <div id="plain-editable-panel" style="display:none">
            <div class="properties-panel">
                <textarea name="plain-editable-text-input" id="plain-editable-text-input" class="textarea" style="width:960px"></textarea>
            </div>
        </div>
        <div id="rich-editable-panel" style="display:none">
            <div class="properties-panel">
                <textarea name="rich-editable-text-input" id="rich-editable-text-input" class="textarea" style="width:960px;height:250px;"></textarea>
            </div>
        </div>
        <div class="element-panel" style="position:static">
            <p style="margin-top:9px;">
                <a href="#" id="hide-panel-button" class="last" style="margin-left:0px;"><?php InterfaceLanguage('Screen', '0500', false); ?></a>
            </p>
        </div>
    </div>

</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_email_template_editor_footer.php'); ?>
