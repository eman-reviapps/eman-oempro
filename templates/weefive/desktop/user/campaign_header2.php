<script type="text/javascript" charset="utf-8">
    var CampaignID = <?php echo $CampaignInformation['CampaignID']; ?>;
    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
</script>
<div class="portlet light bg-inverse" style="padding-bottom: 0px;margin-bottom: 15px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php print(ucwords($CampaignInformation['CampaignName'])); ?>
                <?php if ($CampaignInformation['SplitTest'] != false): ?>
                    <span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                <?php endif; ?>
                <?php
                if (count($CampaignInformation['Tags']) > 0):
                    foreach ($CampaignInformation['Tags'] as $EachTag):
                        ?>
                        <span class="tag-label label"><?php echo $EachTag['Tag']; ?> <a href="#" id="tag-id-<?php echo $EachTag['TagID']; ?>" class="remove-label-link">x</a></span>
                        <?php
                    endforeach;
                endif;
                ?>
            </span>
        </div>
        <div class="actions">
            <a class="btn default btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/campaign/overview/<?php echo $CampaignInformation['CampaignID']; ?>"><strong><?php InterfaceLanguage('Screen', '9202', false, '', false); ?></strong></a>
        </div>
    </div>
    <?php Plugins::HookListener('Action', 'UI.Campaign.Details', array($CampaignInformation)); ?>
</div>
