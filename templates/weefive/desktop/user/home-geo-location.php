
<script>
    var Dashboard = function () {

        return {
            initJQVMAP: function () {
                if (!jQuery().vectorMap) {
                    return;
                }

                var showMap = function (name) {
                    jQuery('.vmaps').hide();
                    jQuery('#vmap_' + name).show();
                }

                var setMap = function (name, geo_data, type) {
                    var map = jQuery('#vmap_' + name);

                    if (map.size() !== 1) {
                        return;
                    }

                    var data = {
                        map: 'world_en',
                        backgroundColor: null,
                        borderColor: '#333333',
                        borderOpacity: 0.5,
                        borderWidth: 1,
                        color: '#c6c6c6',
                        enableZoom: true,
                        hoverColor: '#c9dfaf',
                        hoverOpacity: 0.7,
                        values: geo_data,
                        normalizeFunction: 'linear',
                        scaleColors: ['#b6da93', '#909cae'],
                        selectedColor: '#c9dfaf',
                        selectedRegion: null,
                        showTooltip: true,
                        onLabelShow: function (event, label, code) {
                            if (geo_data[code] > 0)
                            {
                                var pos = label.text().indexOf(":");
                                if (pos != -1)
                                {
                                    var region = label.text().substring(0, pos);
                                    label.text(region + ': ' + geo_data[code] + ' ' + type);
                                } else
                                {
                                    label.append(': ' + geo_data[code] + ' ' + type);
                                }
                            } else
                            {
                                var pos = label.text().indexOf(":");
                                if (pos != -1)
                                {
                                    var region = label.text().substring(0, pos);
                                    label.text(region);
                                }
                            }
                        },
                        onRegionOver: function (event, code) {
//                            if (code == 'ca') {
//                                event.preventDefault();
//                            }
                        },
                        onRegionClick: function (element, code, region) {
                            var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
//                            alert(message);
                        }
                    };

                    data.map = name + '_en';

                    map.width(map.parent().parent().width());
                    map.show();
                    map.vectorMap(data);
                    map.hide();

                }

                var get_geo_data = function (type, no_of_months) {
                    var url_geo;
                    var user_id = <?php echo $UserInformation['UserID']; ?>;
                    var geo_data = null;
                    var countries = null;

                    if (type == 'people')
                    {
                        url_geo = "<?php echo InterfaceAppURL() . '/user/overview/PoepleGeoStatistics/' ?>";
                    } else if (type == 'open')
                    {
                        url_geo = "<?php echo InterfaceAppURL() . '/user/overview/OpenGeoStatistics/' ?>";
                    } else if (type == 'click')
                    {
                        url_geo = "<?php echo InterfaceAppURL() . '/user/overview/ClickGeoStatistics/' ?>";
                    }

                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months,
                        },
                        url: url_geo,
                        success: function (data) {
                            console.log(data);
                            data = JSON.parse(data);
                            geo_data = data.geo_data;
//                            console.log(geo_data);
                            countries = data.countries;
//                            console.log(countries);
                        },
                        async: false // <- this turns it into synchronous
                    });

                    jQuery('#vmap_world').empty();

                    setMap("world", geo_data, type + 's');
                    showMap("world");

                    var top_html = "<div class='top_countries'> Top Countries <br/><br/><div class='clearfix'></div><table class='table table-striped table-bordered table-hover' >";
                    var i = 1
                    for (var key in geo_data) {

                        if (i > 3)
                            break;
//                        console.log(i + " : " + key + " : " + geo_data[key])
//<td>" + i + " </td>
                        top_html += "<tr><td> " + countries[key] + " </td><td>" + geo_data[key] + "</td></tr>";
                        i++;
                    }
                    top_html += "</table></div>";
                    jQuery('#vmap_world').append(top_html);

                }

                var change_geo_map = function (no_of_months) {

                    if (jQuery("#people_geo_home").hasClass('active') == true)
                    {
                        get_geo_data('people', no_of_months);
                    } else if (jQuery("#open_geo_home").hasClass('active') == true)
                    {
                        get_geo_data('open', no_of_months);
                    } else
                    {
                        get_geo_data('click', no_of_months);
                    }

                }
                jQuery('#open_geo_home').click(function () {
                    jQuery("#people_geo_home").removeClass("active");
                    jQuery("#click_geo_home").removeClass("active");
                    jQuery('#open_geo_home').addClass('active');

                    jQuery("#option_selected").text('All');

                    change_geo_map(0);
                });
                jQuery('#click_geo_home').click(function () {
                    jQuery("#people_geo_home").removeClass("active");
                    jQuery("#open_geo_home").removeClass("active");
                    jQuery('#click_geo_home').addClass('active');

                    jQuery("#option_selected").text('All');

                    change_geo_map(0);
                });
                jQuery('#people_geo_home').click(function () {
                    jQuery("#click_geo_home").removeClass("active");
                    jQuery("#open_geo_home").removeClass("active");
                    jQuery('#people_geo_home').addClass('active');

                    jQuery("#option_selected").text('All');

                    change_geo_map(0);
                });

                change_geo_map(0)

                jQuery('#view_geo_all').click(function () {
                    jQuery("#option_selected").text(jQuery(this).text());

                    change_geo_map(0);
                });

                jQuery('#view_geo_last_3_month').click(function () {
                    jQuery("#option_selected").text(jQuery(this).text());

                    change_geo_map(3)
                });

                jQuery('#view_geo_last_6_month').click(function () {
                    jQuery("#option_selected").text(jQuery(this).text());

                    change_geo_map(6)
                });

                jQuery('#view_geo_last_year').click(function () {
                    jQuery("#option_selected").text(jQuery(this).text());

                    change_geo_map(12)
                });

                $('#region_statistics_loading').hide();
                $('#region_statistics_content').show();

                App.addResizeHandler(function () {
                    jQuery('.vmaps').each(function () {
                        var map = jQuery(this);
                        map.width(map.parent().width());
                    });
                });
            },
            init: function () {

                this.initJQVMAP();
            }
        };

    }();

    if (App.isAngularJsApp() === false) {
        jQuery(document).ready(function () {
            Dashboard.init(); // init metronic core componets
        });
    }
</script>

