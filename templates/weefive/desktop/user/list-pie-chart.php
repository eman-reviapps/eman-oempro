

<script>
    var ChartsAmcharts = function () {


        var initChartSample7 = function () {
            var data = [];
            var series = Math.floor(Math.random() * 10) + 1;
            series = series < 5 ? 5 : series;

            data = [{
                    "label": "<?php InterfaceLanguage('Screen', '1171'); ?>" + " : " + <?php echo $BounceStatisticsArr['Not Bounced']['percentage'] ?> + "%",
                    "data": <?php echo $BounceStatisticsArr['Not Bounced']['percentage'] ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '9213') ?>" + " : " + <?php echo $BounceStatisticsArr['Soft']['percentage'] ?> + "%",
                    "data": <?php echo $BounceStatisticsArr['Soft']['percentage'] ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '9208') ?>" + " : " + <?php echo $BounceStatisticsArr['Hard']['percentage'] ?> + "%",
                    "data": <?php echo $BounceStatisticsArr['Hard']['percentage'] ?>
                }];

            $.plot($("#donut"), data, {
                title: {text: 'Legend Location'},
                series: {
                    pie: {
                        innerRadius: 0.6,
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                legend: {
                    show: true,
                },
                colors: ["#2AB4C0","#F3C200","#A52D69","#D91E18", "#8E44AD", "#FF8040","#FF00FF"]
            });

//            var chart = AmCharts.makeChart("chart_7", {
//                grid: {
//                    hoverable: true,
//                    clickable: true
//                },
//                "legend": {
//                    verticalAlign: "center",
//                    horizontalAlign: "right"
//                },
//                "type": "pie",
//                "theme": "light",
//                "showInLegend": true,
//                "fontFamily": 'Open Sans',
//                "color": '#888',
//                "dataProvider": [{
//                        "title": "<?php InterfaceLanguage('Screen', '0885'); ?>",
//                        "value": <?php echo 50; //$NotOpenedRatio;                             ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0875') ?>",
//                        "value": <?php echo 100; //$OpenedRatio;                             ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0876') ?>",
//                        "value": <?php echo 20; //$ClickedRatio;                           ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0841') ?>",
//                        "value": <?php echo $UnsubscriptionRatio ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0961'); ?>",
//                        "value": <?php echo number_format((100 * $CampaignInformation['TotalSpamReports']) / $CampaignInformation['TotalSent']); ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0098'); ?>",
//                        "value": <?php echo $BounceRatio ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '1113'); ?>",
//                        "value": <?php echo $HardBouncedRatio ?>
//                    }],
//                "valueField": "value",
//                "titleField": "title",
//                "outlineAlpha": 0.4,
//                "depth3D": 0,
//                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
//                "angle": 0,
//                "exportConfig": {
//                    menuItems: [{
//                            icon: '/lib/3/images/export.png',
//                            format: 'png'
//                        }]
//                }
//            });
//
//            jQuery('.chart_7_chart_input').off().on('input change', function () {
//                var property = jQuery(this).data('property');
//                var target = chart;
//                var value = Number(this.value);
//                chart.startDuration = 0;
//
//                if (property == 'innerRadius') {
//                    value += "%";
//                }
//
//                target[property] = value;
//                chart.validateNow();
//            });
//
//            $('#chart_7').closest('.portlet').find('.fullscreen').click(function () {
//                chart.invalidateSize();
//            });
        }

        var initChartSample8 = function () {
            if (!jQuery().easyPieChart) {
                return;
            }

            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('yellow')
            });

            $('.easy-pie-chart .number.visits').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('green')
            });

            $('.easy-pie-chart .number.bounce').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('red')
            });

            $('.easy-pie-chart-reload').click(function () {
                $('.easy-pie-chart .number').each(function () {
                    var newValue = Math.floor(100 * Math.random());
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
            });
        }

        return {
            //main function to initiate the module

            init: function () {

                initChartSample7();
                initChartSample8();
            }

        };

    }();

    jQuery(document).ready(function () {
        ChartsAmcharts.init();
    });
</script>

