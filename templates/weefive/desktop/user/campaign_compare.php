<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/camp-compare-flot-chart.php'); ?>


<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />


<link href="<?php InterfaceTemplateURL(false); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />


<?php include_once(TEMPLATE_PATH . 'desktop/user/campaigns_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/campaigns_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-body">
                                <div class="portlet-title">
                                    <div class="caption">
                                         <span class="caption-subject font-purple-sharp sbold"> 
                                            <?php InterfaceLanguage('Screen', '0958', false, '', false, true); ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light  portlet-fit">
                                                <div class="portlet-title">
                                                    <div class="caption" style="padding: 0">
                                                        <div class="form-group" style="margin-bottom: 0">
                                                            <select class="form-control input-small" id="DaysSelect">
                                                                <option value="7" selected>7 days</option>
                                                                <option value="15">15 days</option>
                                                                <option value="30">30 days</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="actions" style="padding: 0">
                                                        <div class="form-group" style="margin-bottom: 0">
                                                            <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                    <input type="radio" checked name="CheckboxGraph" id="CheckboxOpens" value="opens"> <?php InterfaceLanguage('Screen', '0837') ?>
                                                                    <span></span>
                                                                </label>
                                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                    <input type="radio" name="CheckboxGraph" id="CheckboxClicks" value="clicks"> <?php InterfaceLanguage('Screen', '0838') ?>
                                                                    <span></span>
                                                                </label>
                                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                    <input type="radio" name="CheckboxGraph" id="CheckboxUnsubscriptions" value="unsubscriptions"> <?php InterfaceLanguage('Screen', '9205') ?>
                                                                    <span></span>
                                                                </label>
                                                                <!--                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                                                                    <input type="radio" name="CheckboxGraph" id="CheckboxForwards" value="forwards"> <?php InterfaceLanguage('Screen', '0839') ?>
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                                                                    <input type="radio" name="CheckboxGraph" id="CheckboxBounces" value="bounces"> <?php InterfaceLanguage('Screen', '0842') ?>
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                                                                    <input type="radio" name="CheckboxGraph" id="CheckboxSpam" value="spams"> <?php InterfaceLanguage('Screen', '9206') ?>
                                                                                                                    <span></span>
                                                                                                                </label>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div id="chart_3" class="chart" style="padding: 0px; position: relative;">
                                                        <canvas class="flot-base" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                        <canvas class="flot-overlay" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="grid items items-custom table table-bordered" >
                                                <tr>
                                                    <th><?php InterfaceLanguage('Screen', '0140', false, '', true); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '0837', false, '', true); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '0838', false, '', true); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '0839', false, '', true); ?></th>
                                                    <!--<th><?php InterfaceLanguage('Screen', '0840', false, '', true); ?></th>-->
                                                    <th><?php InterfaceLanguage('Screen', '0841', false, '', true); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '0842', false, '', true); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '9206', false, '', true); ?></th>
                                                </tr>
                                                <?php foreach ($Campaigns as $index => $EachCampaign): ?>
                                                    <?php
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <!--                                            <div class="campaign-details">
                                                                                                            <a  target="_blank"  class="preview email-preview" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center" ng-href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>">
                                                                                                                <img width="120" height="105" class="ng-scope" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/scrrenshot.png">
                                                                                                            </a>
                                                                                                        </div>-->
                                                            <strong><?php echo $EachCampaign['CampaignName']; ?></strong><br />
                                                            <span class="data small"><?php echo $EachCampaign['TotalSent']; ?></span> <span class="data-label small"><?php InterfaceLanguage('Screen', '0147', false, '', false, false); ?>, <?php echo date('M d, Y', strtotime($EachCampaign['SendProcessFinishedOn'])); ?></span>
                                                        </td>
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['OpenRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['UniqueOpens']; ?></span>
                                                        </td>
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['ClickRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['UniqueClicks']; ?></span>
                                                        </td>
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['ForwardRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['UniqueForwards']; ?></span>
                                                        </td>
                    <!--                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['ViewRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['TotalViewsOnBrowser']; ?></span>
                                                        </td>-->
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['UnsubscriptionRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['TotalUnsubscriptions']; ?></span>
                                                        </td>
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['BounceRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['TotalHardBounces'] + $EachCampaign['TotalSoftBounces']; ?></span>
                                                        </td>
                                                        <td>
                                                            <span class="data"><?php echo $EachCampaign['SpamRatio']; ?>%</span><br />
                                                            <span class="data small"><?php echo $EachCampaign['TotalSpamReports']; ?></span>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="portlet" style="padding-bottom: 0px;margin-bottom: 15px">

</div>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var app_url = '<?php InterfaceAppURL(); ?>';
    var campaign_ids = '<?php echo $CampaignIDs; ?>';
</script>

<!--<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_compare.js" type="text/javascript" charset="utf-8"></script>-->		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>