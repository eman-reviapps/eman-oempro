<?php
if (count($Subscribers) < 1):
?>
	<p class="no-data-message"><?php InterfaceLanguage('Screen', '0895', false, '', false); ?></p>
<?php
else:
?>
	<table border="0" cellspacing="0" cellpadding="0" class="small-grid no-zebra" id="activity-table">
		<?php
		foreach ($Subscribers as $Index=>$EachSubscriber):
		?>
			<tr>
				<td width="80%">
					<a href="#" subscriberid="<?php echo $EachSubscriber['SubscriberID']; ?>" listid="<?php echo $EachSubscriber['ListID']; ?>"><?php echo $EachSubscriber['Email'] ?></a>
					<div class="subscriber-detailed-activity-table">
						<table class="small-grid" cellpadding="0" cellspacing="0">
						<?php foreach ($EachSubscriber['Activities'] as $EachActivity): ?>
							<tr>
								<td width="130"><?php echo $EachActivity['time']; ?></td>
								<td><?php echo $EachActivity['activity']; ?></td>
							</tr>
						<?php endforeach; ?>
						</table>
					</div>
				</td>
				<td width="20%" class="right-align"><span class="data"><?php echo $EachSubscriber['TotalClicks'] ?></span> <?php InterfaceLanguage('Screen', '0838', false, '', false); ?></td>
			</tr>
		<?php
		endforeach;
		?>
	</table>
<?php
endif;
?>
