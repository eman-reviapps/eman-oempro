<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-purple-sharp sbold">
                        <i class="icon-doc font-purple-sharp"></i>
                        <?php InterfaceLanguage('Screen', '1319', false, '', false, true); ?>
                    </span>
                </div>
                <div class="actions">
                    <a class="btn default" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/<?php echo $SegmentID; ?>"><strong><?php InterfaceLanguage('Screen', '1322', false, '', false); ?></strong></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-edit" action="<?php InterfaceAppURL(); ?>/user/subscriber/edit/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>" method="post">
                    <input type="hidden" name="Command" value="Save" id="Command">

                    <?php if (count($SubscribedLists) > 0) { ?>
                        <div class="tabbable-line">
                            <!--<label class="col-md-2 control-label" style="width:80px;">List:</label>-->
                            <ul class="nav nav-tabs" tabcallback="switchList">
                                <!--<li class="label">List:</li>-->
                                <?php foreach ($SubscribedLists as $EachList): ?>
                                    <li <?php echo $EachList['ListID'] == $ListInformation['ListID'] ? 'class="active" ' : ''; ?>id="switch-list-item-<?php echo $EachList['ListID']; ?>" data-url="<?php InterfaceAppURL(); ?>/user/subscriber/edit/<?php echo $EachList['ListID'] . '/' . $SubscriberInformation['SubscriberID']; ?>">
                                        <a href="<?php InterfaceAppURL(); ?>/user/subscriber/edit/<?php echo $EachList['ListID'] . '/' . $SubscriberInformation['SubscriberID']; ?>"><?php echo $EachList['Name']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <br/>
                    <ul class="nav nav-tabs" tabcollection="subscriber-information-tabs" >
                        <li class="active">
                            <a href="#tab-information" data-toggle="tab"> <?php InterfaceLanguage('Screen', '1755'); ?> </a>
                        </li>
                        <li>
                            <a href="#tab-activity" data-toggle="tab"> <?php InterfaceLanguage('Screen', '1756'); ?> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" tabcollection="subscriber-information-tabs"  id="tab-information">
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php
                                    if (isset($PageSuccessMessage) == true):
                                        ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageSuccessMessage); ?>
                                        </div>
                                        <?php
                                    elseif (isset($PageErrorMessage) == true):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageErrorMessage); ?>
                                        </div>
                                        <?php
                                    elseif (validation_errors()):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                                        </div>
                                    <?php else: ?>
                                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1336'); ?></h4>
                                    <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <?php if ($SuppressionRecord['IsGloballySuppressed'] || $SuppressionRecord['IsLocallySuppressed']): ?>
                                <span class="help-block">
                                    <p><?php InterfaceLanguage('Screen', $SuppressionRecord['IsGloballySuppressed'] ? '1873' : '1874'); ?></p>
                                </span>
                            <?php endif; ?>

                            <div class="form-group clearfix<?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress">
                                <label class="col-md-3 control-label" for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', $SubscriberInformation['EmailAddress']); ?>" id="EmailAddress" class="form-control" />
                                    <?php print(form_error('EmailAddress', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-BounceType">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1320', false, '', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('EmailAddress', $SubscriberInformation['BounceType']); ?></span>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-SubscriptionStatus">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0665', false, 'SubscriptionStatus', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('SubscriptionStatus', $SubscriberInformation['SubscriptionStatus']); ?></span>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-SubscriptionDate">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1321', false, '', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('SubscriptionDate', date('M d, Y', strtotime($SubscriberInformation['SubscriptionDate']))); ?></span>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-SubscriberIP">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '9239', false, '', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('Subscriber_IP', $SubscriberInformation['Subscriber_IP']); ?></span>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-SubscriberCity">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '9240', false, '', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('City', $SubscriberInformation['City']); ?></span>
                                </div>
                            </div>
                            <div class="form-group clearfix" id="form-row-SubscriberCountry">
                                <label class="col-md-3 control-label" style="width:180px"><?php InterfaceLanguage('Screen', '9241', false, '', false, true); ?>:</label>
                                <div class="col-md-6">
                                    <span class="help-block"><?php echo set_value('Country', $SubscriberInformation['Country']); ?></span>
                                </div>
                            </div>
                            <?php foreach ($CustomFields as $EachField): ?>
                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label"><?php echo $EachField['FieldName']; ?>:<?php echo $EachField['IsRequired'] == 'Yes' ? ' *' : ''; ?></label>
                                    <div class="col-md-6">
                                        <?php echo $EachField['html']; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="tab-pane" tabcollection="subscriber-information-tabs" id="tab-activity">
                            <br/>
                            <br/>
                            <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1754', false, '', false, false, array($SubscriberInformation['EmailAddress'])); ?></h4>
                            <div id="timeline-container" style="height:120px;position:relative;">

                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn default" targetform="subscriber-edit" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                                <?php if ($SubscriberInformation['SubscriptionStatus'] == 'Subscribed'): ?>
                                    <a class="btn default" id="form-button" href="<?php InterfaceAppURL(); ?>/user/subscriber/unsubscribe/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1309', false, '', true); ?></strong></a>
                                <?php endif ?>
                                <a class="btn default" id="form-button-delete" href="<?php InterfaceAppURL(); ?>/user/subscriber/delete/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>" ><strong><?php InterfaceLanguage('Screen', '0042', false, '', true); ?></strong></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--    <div class="col-md-4">
    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscriberedit.php'); ?>
        </div>-->
</div>


<script type="text/javascript">
    var timelineData = '';


//    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
//        var view = $(e.target).attr("href") // activated tab
//        if (view == '#tab-activity') {
//
//        }
//    });

    function switchList(el) {
        var listUrl = $('#' + el).attr('data-url');
        if (window.location != listUrl) {
            window.location = $('#' + el).attr('data-url');
        }
    }

    function alignCustomFieldRadios() {
        $('.form-row-radio-field input').each(function (i) {
            if (i > 1) {
                $(this).css('margin-left', '145px');
            }
        });
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        switchView(target)
    });
    function switchView(view) {

        if (view == '#tab-activity') {
            if (timelineData == '') {
                $.getJSON('<?php InterfaceAppURL(); ?>/user/subscriber/snippetActivity/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>', function (data) {
                                    timelineData = data;
                                    drawTimeline(timelineData);
                                });
                            }
                        }
                    }

                    var titles = {
                        'subscription': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Subscribed'), ENT_QUOTES); ?>',
                        'campaignopen': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Opened campaign'), ENT_QUOTES); ?>',
                        'autoresponderopen': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Opened autoresponder'), ENT_QUOTES); ?>',
                        'campaignclick': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Clicked campaign'), ENT_QUOTES); ?>',
                        'autoresponderclick': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Clicked autoresponder'), ENT_QUOTES); ?>',
                        'campaignbounce': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Bounced'), ENT_QUOTES); ?>',
                        'autoresponderbounce': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Bounced'), ENT_QUOTES); ?>',
                        'campaignforward': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Forwarded campaign'), ENT_QUOTES); ?>',
                        'autoresponderforward': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Forwarded autoresponder'), ENT_QUOTES); ?>',
                        'campaignbrowserView': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Viewed campaign in browser'), ENT_QUOTES); ?>',
                        'autoresponderbrowserView': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Viewed autoresponder in browser'), ENT_QUOTES); ?>',
                        'campaignunsubscription': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Unsubscribed'), ENT_QUOTES); ?>',
                        'autoresponderunsubscription': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Unsubscribed'), ENT_QUOTES); ?>',
                        'unsubscription': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1760', true, 'Unsubscribed'), ENT_QUOTES); ?>'
                    };

                    function drawTimeline(data) {
                        var length = data.end - data.start,
                                percentMultiplier = length / 100,
                                timelineContainer = $('#timeline-container'),
                                timelineLength = timelineContainer.width();

                        if (data.nodata) {
                            error = $('<div><?php echo htmlspecialchars(InterfaceLanguage('Screen', '1758', true), ENT_QUOTES); ?></div>');
                            error.css({
                                'color': '#c5c5c5',
                                'font-size': '18px',
                                'position': 'absolute',
                                'top': '35px',
                                'left': '50%'
                            });
                            timelineContainer.append(error);
                            var pos = error.offset();
                            error.css('left', (pos.left - error.width()) + 'px');
                            return;
                        }

                        // Draw baseline
                        timeline = $('<div id="timeline"></div>');
                        timeline.css({
                            'height': '5px',
                            'width': (timelineLength - 40) + 'px',
                            'background-color': '#eee',
                            'position': 'absolute',
                            'top': '80px',
                            'left': '20px',
                            'z-index': 2
                        });
                        timelineContainer.append(timeline)

                        // Write start and end dates
                        label = $('<div><?php echo htmlspecialchars(InterfaceLanguage('Screen', '1759', true), ENT_QUOTES); ?></div>');
                        label.css({
                            'color': '#f7f7f7',
                            'font-size': '54px',
                            'font-weight': 'bold',
                            'letter-spacing': '5px',
                            'position': 'absolute',
                            'top': '56px',
                            'left': '20px',
                            'line-height': '18px',
                            'z-index': 1
                        });
                        timelineContainer.append(label);

                        startDate = $('<div>' + data.startRead + '</div>');
                        startDate.css({
                            'color': '#c5c5c5',
                            'font-size': '10px',
                            'position': 'absolute',
                            'top': '90px',
                            'left': '20px',
                            'z-index': 3
                        });
                        timelineContainer.append(startDate);

                        endDate = $('<div>' + data.endRead + '</div>');
                        endDate.css({
                            'color': '#c5c5c5',
                            'font-size': '10px',
                            'position': 'absolute',
                            'top': '90px',
                            'right': '16px',
                            'z-index': 3
                        });
                        timelineContainer.append(endDate);

                        // Draw activity ticks and table
                        var tickColors = {'open': '#2a59cc', 'click': '#a44cf2', 'unsubscription': '#ff0000', 'subscription': '#83c063', 'forward': '#cfcf1c', 'browserView': '', 'bounce': '#e26c03'};
                        var table = $('<table class="small-grid"></table>');
                        $.each(data.activities, function (i) {
                            var activity = this;
                            var tooltip;
                            var left = ((this.timestamp - data.start) * 100) / length;
                            left = (((timelineLength - 40) * left) / 100) + 20;
                            activityDiv = $('<div data-activity-id="' + i + '"></div>');
                            activityPointer = $('<div data-activity-pointer-id="' + i + '"></div>');
                            activityPointer.css({
                                'height': '10px',
                                'width': '4px',
                                'background-color': '#000',
                                'position': 'absolute',
                                'top': '60px',
                                'left': left + 'px',
                                'z-index': 4
                            }).hide();
                            activityDiv.css({
                                'height': '10px',
                                'width': '4px',
                                'background-color': tickColors[this.type],
                                'position': 'absolute',
                                'top': '77px',
                                'left': left + 'px',
                                'cursor': 'pointer',
                                'z-index': 4
                            }).hover(function () {
                                showActivityDetail(this, i, activity);
                            }, function () {
                                hideActivityDetail(this);
                            });
                            tableRow = $('<tr data-activity-row-id="' + i + '"><td width="10" style="padding:0;background-color:' + tickColors[this.type] + ';">&nbsp;</td><td width="90" style="vertical-align: top"><span style="font-size:11px;">' + activity.date + '</span></td><td width="130" style="vertical-align: top"><span class="data-label">' + titles[activity.entityType + activity.type] + '</span></td>' + (activity.type != 'bounce' && activity.type != 'subscription' && activity.type != 'unsubscription' ? '<td style="vertical-align: top"><span>' + activity.entityName + (activity.type == 'click' ? '<br><span class="data small">' + (activity.linkTitle != '' ? activity.linkTitle : activity.linkURL) + '</span><br>' : '') + '</td>' : '<td>&nbsp;</td>') + '</tr>');
                            table.prepend(tableRow);
                            tableRow.hover(function () {
                                var id = $(this).attr('data-activity-row-id');
                                $('[data-activity-pointer-id=' + id + ']').show();
                            }, function () {
                                var id = $(this).attr('data-activity-row-id');
                                $('[data-activity-pointer-id=' + id + ']').hide();
                            });
                            timelineContainer.append(activityDiv);
                            timelineContainer.append(activityPointer);
                        });
                        timelineContainer.after($('<div class="inner" style="padding:15px;"></div>').append(table));
                        table.data_grid()
                    }

                    function showActivityDetail(el, id, activity) {
                        if ($('.tooltip', el).length > 1) {
                            $('.tooltip', el).show();
                        } else {
                            tooltip = $('<div class="tooltip" data-activity-tooltop-id="' + id + '"></div>');
                            tooltip.css({
                                'border': '3px solid #fff',
                                'background-color': '#f2f2f2',
                                'padding': '6px 9px',
                                'font-size': '10px',
                                'position': 'absolute',
                                'bottom': '13px',
                                'left': '5px',
                                'width': '200px'
                            }).html('<strong>' + titles[activity.entityType + activity.type] + '</strong>' + (activity.type != 'bounce' && activity.type != 'subscription' && activity.type != 'unsubscription' ? '<br><span style="font-size:9px">' + activity.entityName + '</span>' : '') + '<br>' + (activity.type == 'click' ? '<span style="font-size:9px">' + (activity.linkTitle != '' ? activity.linkTitle : activity.linkURL) + '</span><br>' : '') + activity.date);
                            $(el).append(tooltip);
                        }
                    }

                    function hideActivityDetail(el) {
                        $('.tooltip', el).hide();
                    }

                    $(document).ready(function () {
                        alignCustomFieldRadios();

                        $('#form-button-delete').click(function () {
                            if (!confirm('<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1778', true), ENT_QUOTES); ?>'))
                                return false;
                            return true;
                        });
                    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>