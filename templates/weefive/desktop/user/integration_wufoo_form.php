<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1721', false, '', false, true, array('Wufoo')); ?> - <?php InterfaceLanguage('Screen', '1723', false, '', false, true); ?> </span>                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >        
                        <a href="<?php InterfaceAppURL(); ?>/user/integration/wufoo/" class="btn default btn-transparent btn-sm"><?php InterfaceLanguage('Screen', '1724', false); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <?php if ($ErrorMessage != FALSE): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php print($ErrorMessage); ?>
                    </div>
                <?php endif; ?>
                <form id="Integration" method="post" action="<?php InterfaceAppURL(); ?>/user/integration/wufoo/form/">
                    <?php if ($Step == 1): ?>
                        <input type="hidden" name="Command" value="RetrieveForms" id="Command"/>
                    <?php elseif ($Step == 2): ?>
                        <input type="hidden" name="Command" value="RetrieveFields" id="Command"/>
                    <?php elseif ($Step == 3): ?>
                        <input type="hidden" name="Command" value="MapFields" id="Command"/>
                    <?php endif; ?>
                    <div <?php if ($Step > 1): ?>style="display:none"<?php endif; ?>>
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php InterfaceLanguage('Screen', '0275', false); ?>
                            </div>
                        <?php endif; ?>
                        <div class="form-group clearfix  <?php print((form_error('WufooSubdomain') != '' ? 'error' : '')); ?>" id="form-row-WufooSubdomain">
                            <label class="col-md-3 control-label" for="WufooSubdomain" style="width:170px;"><?php InterfaceLanguage('Screen', '1725', false); ?> : *</label>
                            <div class="col-md-4"> 
                                <input type="text" name="WufooSubdomain" value="<?php echo set_value('WufooSubdomain', (!$LastIntegration ? '' : $LastIntegration->getWufooSubDomain())); ?>" id="WufooSubdomain" class="form-control"/>
                                <?php print(form_error('WufooSubdomain', '<span class="help-block">', '</span>')); ?>
                            </div>
                            <div class="col-md-5">
                                <span class="help-block"><?php InterfaceLanguage('Screen', '1731', false); ?></span>
                            </div>                        
                        </div>
                        <div class="form-group  <?php print((form_error('WufooAPIKey') != '' ? 'error' : '')); ?>" id="form-row-WufooAPIKey">
                            <label class="col-md-3 control-label" for="WufooAPIKey" style="width:170px;"><?php InterfaceLanguage('Screen', '0208', false); ?> : *</label>
                            <div class="col-md-4"> 
                                <input type="text" name="WufooAPIKey" value="<?php echo set_value('WufooAPIKey', (!$LastIntegration ? '' : $LastIntegration->getApiKey())); ?>" id="WufooAPIKey" class="form-control"/>
                                <?php print(form_error('WufooAPIKey', '<span class="help-block">', '</span>')); ?>
                            </div>
                            <div class="col-md-5">
                                <span class="help-block"><?php InterfaceLanguage('Screen', '1732', false); ?></span>
                            </div>
                        </div>
                    </div>

                    <?php if ($Step > 1): ?>
                        <div  <?php if ($Step > 2): ?>style="display:none"<?php endif; ?>>
                            <div class="form-group clearfix " id="form-row-OemproList">
                                <label class="col-md-3 control-label" for="OemproList" style="width:170px;"><?php InterfaceLanguage('Screen', '1444', false); ?> : *</label>
                                <div class="col-md-4"> 
                                    <select class="form-control" id="OemproList" name="OemproList">
                                        <?php foreach ($Lists as $List): ?>
                                            <option value="<?php echo $List['ListID']; ?>" <?php echo set_select('OemproList', $List['ListID']); ?>><?php echo $List['Name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group clearfix " id="form-row-WufooForm">
                                <label class="col-md-3 control-label" for="WufooForm" style="width:170px;"><?php InterfaceLanguage('Screen', '1727', false); ?> : *</label>
                                <div class="col-md-4"> 
                                    <select class="form-control" id="WufooForm" name="WufooForm">
                                        <?php foreach ($Forms as $Hash => $Form): ?>
                                            <option value="<?php echo $Hash . '-' . $Form->Name; ?>" <?php echo set_select('WufooForm', $Hash); ?>><?php echo $Form->Name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($Step > 2): ?>
                        <h3 class="form-legend"><?php InterfaceLanguage('Screen', '1729', false); ?></h3>
                        <table class="small-grid" style="margin-top:18px;">
                            <tr>
                                <td style="text-align:right;">Email Address</td>
                                <td>
                                    <select id="WufooFieldMapping[EmailAddress]" name="WufooFieldMapping[EmailAddress]">
                                        <?php foreach ($WufooFields as $Field): ?>
                                            <?php if ($Field['type'] != 'email') continue; ?>
                                            <option value="<?php echo $Field['value']; ?>=>email"><?php echo $Field['title']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php foreach ($OemproFields as $OemproField): ?>
                                <tr>
                                    <td style="text-align:right;"><?php echo $OemproField['FieldName']; ?></td>
                                    <td>
                                        <select id="WufooFieldMapping[CustomField<?php echo $OemproField['CustomFieldID']; ?>]" name="WufooFieldMapping[CustomField<?php echo $OemproField['CustomFieldID']; ?>]">
                                            <option value=""><?php echo InterfaceLanguage('Screen', '1183'); ?></option>
                                            <?php foreach ($WufooFields as $Field): ?>
                                                <option value="<?php echo $Field['value']; ?>=><?php echo $Field['type']; ?>"><?php echo $Field['title']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                    <?php endif; ?>
                </form>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-2">
                            <?php if ($Step == 1): ?>
                                <a class="btn default" targetform="Integration" id="ButtonIntegration"><strong><?php InterfaceLanguage('Screen', '1726', false, '', true); ?></strong></a>
                            <?php elseif ($Step == 2): ?>
                                <a class="btn default" targetform="Integration" id="ButtonIntegration"><strong><?php InterfaceLanguage('Screen', '1728', false, '', true); ?></strong></a>
                            <?php elseif ($Step == 3): ?>
                                <a class="btn default" targetform="Integration" id="ButtonIntegration"><strong><?php InterfaceLanguage('Screen', '1730', false, '', true); ?></strong></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="page-shadow">
    <div id="page">
        <div class="page-bar">
            <h2></h2>

            <div class="actions">

            </div>
        </div>
        <div class="white" style="min-height:180px;padding-top:9px;" id="page-user-integration-wufoo-form">

        </div>
        <div class="span-18 last">
            <div class="form-action-container">

            </div>
        </div>
    </div>
</div>