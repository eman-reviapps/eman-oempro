<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH.'desktop/user/campaign_header.php'); ?>

<!-- Page - Start -->
<div class="container">
	<div class="span-5 snap-5">
		<?php include_once(TEMPLATE_PATH.'desktop/user/campaign_navigation.php'); ?>
	</div>
	<div class="span-18 last">
		<div id="page-shadow">
			<div id="page" style="min-height:530px">
				<div class="page-bar">
					<h2><?php InterfaceLanguage('Screen', '0907', false, '', false, false); ?></h2>
					<ul class="livetabs right" tabcollection="report-date-tabs" tabcallback="switch_chart_data_range">
						<li class="label"><?php InterfaceLanguage('Screen', '0093', false, '', false); ?></li>
						<li id="tab-7d"><?php InterfaceLanguage('Screen', '0883', false, '', false, false); ?></li>
						<li id="tab-15d"><?php InterfaceLanguage('Screen', '0881', false, '', false, false); ?></li>
						<li id="tab-30d"><?php InterfaceLanguage('Screen', '0882', false, '', false, false); ?></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner flash-chart-container" id="activity-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/user/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/browserviews-all/" style="height:150px;">
					</div>
					<div class="custom-column-container cols-2 clearfix">
						<div class="col">
							<span class="data big"><?php echo $CampaignInformation['TotalViewsOnBrowser']; ?></span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0904', false, '', false, true); ?></span>
						</div>
						<div class="col">
						</div>
					</div>
					
				</div>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-2">
						<li id="tab-most-clicked-links"><a href="#"><?php InterfaceLanguage('Screen', '0903', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-2" id="tab-content-most-clicked-links">
							<?php
							if (count($Views) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0905', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($Views as $Index=>$EachSubscriber):
									?>
										<tr>
											<td width="80%"><a href="#"><?php echo $EachSubscriber['Email'] ?></a></td>
											<td width="20%" class="right-align"><span class="data"><?php echo $EachSubscriber['TotalViews'] ?></span> <?php InterfaceLanguage('Screen', '0906', false, '', false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- Page - End -->

<script>
var statistic_url = '<?php InterfaceAppURL(); ?>/user/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/browserviews-all/';
var activity_url = '<?php InterfaceAppURL(); ?>/user/campaign/snippetsubscriberactivitydetailed/<?php print($CampaignInformation['CampaignID']) ?>/';
var CampaignID = <?php print($CampaignInformation['CampaignID']) ?>;
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_statisticsview.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>