<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!--light bg-inverse-->
<?php
//print_r('<pre>');
//print_r($Campaigns);
//print_r('</pre>');
?>
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.css" rel="stylesheet">
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {

        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    if (window.console) {
                        return console.log('onShown event fired');
                    }
                },
                onContentLoaded: function () {
                    if (window.console) {
                        return console.log('onContentLoaded event fired');
                    }
                },
                onNavigate: function (direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });

    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaigns_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/campaigns_navigation.php'); ?>
                        <div class="clearfix"></div>

                        <div class="portlet light portlet-transparent">
                            <div class="portlet-body">
                                <div class="table-container">
                                    <form id="campaigns-table-form" action="<?php InterfaceAppURL(); ?>/user/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/" method="post">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <?php
                                                if (isset($PageSuccessMessage) == true):
                                                    ?>
                                                    <div class="alert alert-info alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageSuccessMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                endif;
                                                ?>
                                                <div id="compare-error" class="alert alert-danger alert-dismissable" style="display: none">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <?php InterfaceLanguage('Screen', '0957', false, '', false, false); ?>
                                                </div>
                                                <div class="operations-custom">
                                                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="campaigns-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="campaigns-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                                    <!-- <a href="#" class="grid-select-inverse" targetgrid="campaigns-table"><?php InterfaceLanguage('Screen', '0041'); ?></a> -->
                                                    <?php if (InterfacePrivilegeCheck('Campaign.Delete', $UserInformation)): ?>
                                                        <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="campaigns-table-form"><?php InterfaceLanguage('Screen', '0736'); ?></a>
                                                    <?php endif; ?>
                                                    <?php if ($FilterData == 'Sent'): ?>
                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/compare/" id="compare-performances-action" class="main-action btn default btn-transparen btn-sm"><?php InterfaceLanguage('Screen', '0956', false, '', false, false); ?></a>
                                                    <?php endif; ?>
                                                    <?php if (count($Tags) > 0): ?>
                                                        <div class="main-action-with-menu" id="assign-tag-menu">
                                                            <button class="main-action btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> <?php InterfaceLanguage('Screen', '0932'); ?>
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu main-action-menu" role="menu">
                                                                <?php foreach ($Tags as $EachTag): ?>
                                                                    <li><a href="#" id="tag-id-<?php echo $EachTag->TagID; ?>"><?php echo $EachTag->Tag; ?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php // if (count($Tags) > 0): ?>
                                                    <!--                                                        <div class="main-action-with-menu" id="assign-tag-menu">
                                                                                                                <a href="#" class="main-action"><?php InterfaceLanguage('Screen', '0932'); ?></a>
                                                                                                                <div class="down-arrow"><span></span></div>
                                                                                                                <ul class="main-action-menu">
                                                    <?php foreach ($Tags as $EachTag): ?>
                                                                                                                                                            <li><a href="#" id="tag-id-<?php echo $EachTag->TagID; ?>"><?php echo $EachTag->Tag; ?></a></li>
                                                    <?php endforeach; ?>
                                                                                                                </ul>
                                                                                                            </div>-->
                                                    <?php // endif; ?>

                                                    <input type="hidden" name="Command" value="DeleteCampaign" id="Command">
                                                    <input type="hidden" name="TagID" value="" id="TagID">
                                                    <?php
                                                    if ($Tags) {
                                                        ?>
                                                        <div class="clearfix" style="margin-top: 10px"></div>
                                                        <div class="col-md-1" style="padding: 0;width:45px;">
                                                            <span class="caption-md small bold font-dark">Tags:</span>
                                                        </div>
                                                        <div class="col-md-11" style="padding: 0;margin-top: -5px">
                                                            <ul class="tags-horiz">
                                                                <?php foreach ($Tags as $EachTag): ?>
                                                                    <li <?php
                                                                    if (in_array($EachTag->TagID, $FilterTags)) {
                                                                        print 'class="active multiple"';
                                                                    }
                                                                    ?>>
                                                                            <?php if (in_array($EachTag->TagID, $FilterTags)): ?>
                                                                            <!--<div class="remove-tag-button" >-->
                                                                            <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php
                                                                            $pos = array_search($EachTag->TagID, $FilterTags);
                                                                            $TMPFilterTags = $FilterTags;
                                                                            unset($TMPFilterTags[$pos]);
                                                                            print implode(':', $TMPFilterTags);
                                                                            ?>"><i class="fa fa-close"></i></a>
                                                                            <!--</div>-->
                                                                        <?php endif; ?>
                                                                        <a class=" btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php print((in_array($EachTag->TagID, $FilterTags) ? implode(':', $FilterTags) : implode(':', array_merge($FilterTags, array($EachTag->TagID))) )); ?>"><?php print($EachTag->Tag); ?></a>
                                                                    </li>
                                                                <?php endforeach ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($FilterData == 'Sent'): ?>
                                            <table class="grid items items-custom table" id="campaigns-table">
    <!--                                                <tr class="item ng-scope ng-isolate-scope">
                                                    <th width="300" colspan="2"><a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php print count($FilterTags) < 1 ? '-1' : implode(':', $FilterTags); ?>/CampaignName/ASC"><?php InterfaceLanguage('Screen', '0140', false, '', false); ?> / <?php InterfaceLanguage('Screen', '0717', false, '', false); ?></a></th>
                                                    <th width="130"><a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/<?php print($CurrentPage); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php print count($FilterTags) < 1 ? '-1' : implode(':', $FilterTags); ?>/SendProcessFinishedOn/ASC"><?php InterfaceLanguage('Screen', '0725', false, '', false); ?></a></th>
                                                    <th width="70"><?php InterfaceLanguage('Screen', '0726', false, '', false); ?></th>
                                                </tr>-->
                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));


                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item">
                                                        <td width="15" style="vertical-align:top;float:left"><input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>"></td>
                                                        <td style="float: left">
                                                            <div class="campaign-details">
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>

                                                                    <span class="type ng-binding"><?php print(date('M d, Y - H:i', strtotime($EachCampaign['SendProcessFinishedOn']))); ?></span>
                                                                    <!--<div class="csspie thumb-blue" data="<?php print($UniqueOpenRate); ?>"></div> <span class="data"><?php print(number_format($UniqueOpenRate, 0)); ?>%</span>-->

                                                                    <div class="btn-group">
                                                                        <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php print($EachCampaign['CampaignID']); ?>/">View report</a>
                                                                        <a class="btn default <?php echo $disabled_class ?>" href="<?php InterfaceAppURL(); ?>/user/campaign/resend/<?php print($EachCampaign['CampaignID']); ?>" class="grid-row-action"><?php InterfaceLanguage('Screen', '9181'); ?></a>
                                                                    </div>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <!--<span class="label label-sm label-success label-mini"><?php print($EachTag['Tag']); ?></span>-->
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="float: right">
                                                            <div class="stats visible-lg visible-md">
                                                                <ul>
                                                                    <li>
                                                                        <span class="number ng-binding bold"><?php print($EachCampaign['TotalSent']); ?></span>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9203', false); ?></span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/opens">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalOpens']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0837'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['opened_ratio'] ?>% </span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/clicks">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalClicks']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0838'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['clicked_ratio'] ?>% </span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/unsubscriptions">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalUnsubscriptions']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9205'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['unsubscription_ratio'] ?>% </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php elseif ($FilterData == 'Outbox'): ?>
                                            <table class="grid items items-custom table table-hover" id="campaigns-table">

                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));

                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td width="25" style="vertical-align:top;float: left">
                                                            <input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>">
                                                        </td>
                                                        <td class="no-padding-left" style="padding-right:18px;float: left">
                                                            <div class="campaign-details">
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>

                                                                    <span class="type ng-binding"><?php echo $EachCampaign['CreateDateTime'] == '' ? '' : date('M d, Y - H:i', strtotime($EachCampaign['CreateDateTime'])); ?></span>

                                                                    <div class="btn-group">
                                                                        <a type="button" class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php print($EachCampaign['CampaignID']); ?>/">View report</a>
                                                                    </div>
                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="float: right">
                                                            <div class="stats visible-lg visible-md">
                                                                <ul>
                                                                    <li>
                                                                        <span class="number bold"><?php print($EachCampaign['TotalSent']); ?></span>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9203', false); ?></span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/opens">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalOpens']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0837'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['opened_ratio'] ?>% </span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/clicks">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalClicks']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0838'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['clicked_ratio'] ?>% </span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/unsubscriptions">
                                                                            <span class="number ng-binding bold"><?php print($EachCampaign['TotalUnsubscriptions']); ?></span>
                                                                        </a>
                                                                        <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9205'); ?></span>
                                                                        <span class="number number-small small"><?php echo $EachCampaign['unsubscription_ratio'] ?>% </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php elseif ($FilterData == 'Draft'): ?>
                                            <table class="grid items items-custom table table-hover" id="campaigns-table">
    <!--                                                <tr class="item ng-scope ng-isolate-scope">
                                                    <th colspan="2"><?php InterfaceLanguage('Screen', '0140', false, '', false); ?> / <?php InterfaceLanguage('Screen', '0719', false, '', false); ?></th>
                                                    <th><?php InterfaceLanguage('Screen', '1858', false, '', false); ?></th>
                                                    <th></th>
                                                </tr>-->
                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));

                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>"></td>
                                                        <td class="no-padding-left" style="padding-right:18px;">
                                                            <div class="campaign-details">
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>

                                                                    <span class="type ng-binding"><?php echo $EachCampaign['CreateDateTime'] == '' ? '' : date('M d, Y - H:i', strtotime($EachCampaign['CreateDateTime'])); ?></span>

                                                                    <div class="btn-group">
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Update', $UserInformation)): ?>
                                                                            <a  class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php print($EachCampaign['CampaignID']); ?>" class="btn default btn-sm purple"><?php InterfaceLanguage('Screen', '1752'); ?></a>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php elseif ($FilterData == 'Scheduled'): ?>
                                            <table class="grid items items-custom table table-hover" id="campaigns-table">
    <!--                                                <tr class="item ng-scope ng-isolate-scope">
                                                    <th width="370" colspan="2"><?php InterfaceLanguage('Screen', '0140', false, '', false); ?> / <?php InterfaceLanguage('Screen', '0720', false, '', false); ?></th>
                                                    <th width="130"><?php InterfaceLanguage('Screen', '0729', false, '', false); ?></th>
                                                </tr>-->
                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));

                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>"></td>
                                                        <td width="355" class="no-padding-left">
                                                            <div class="campaign-details">
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>

                                                                    <span class="type ng-binding">
                                                                        <?php
                                                                        if ($EachCampaign['ScheduleType'] == 'Future') {
                                                                            print(date('M d, Y - H:i', strtotime($EachCampaign['SendDate'] . ' ' . $EachCampaign['SendTime'])));
                                                                        } elseif ($EachCampaign['ScheduleType'] == 'Recursive') {
                                                                            print(date('M d, Y - H:i', strtotime($EachCampaign['RecSendDate'] . ' ' . $EachCampaign['RecSendTime'])));
                                                                        }
                                                                        ?>
                                                                    </span>

                                                                    <?php if ($EachCampaign['ScheduleType'] == 'Recursive'): ?>
                                                                        <span class="label label-primary">
                                                                            <?php InterfaceLanguage('Screen', '0730', false, '', false, false, array()); ?>
                                                                            <?php if ($EachCampaign['ScheduleRecSendMaxInstance'] > 0): ?>
                                                                                x<?php echo $EachCampaign['ScheduleRecSendMaxInstance'] - $EachCampaign['ScheduleRecSentInstances']; ?>
                                                                            <?php endif; ?>
                                                                        </span>
                                                                    <?php endif; ?>
                                                                    <div class="btn-group">
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Update', $UserInformation)): ?>
                                                                            <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php print($EachCampaign['CampaignID']); ?>" class="grid-row-action"><?php InterfaceLanguage('Screen', '1752'); ?></a>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td width="130" class="small-text" style="vertical-align:top">

                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php elseif ($FilterData == 'Paused'): ?>
                                            <table class="grid items items-custom table table-hover" id="campaigns-table">
                                                <!--<tr class="item ng-scope ng-isolate-scope"><th width="700" colspan="2"><?php InterfaceLanguage('Screen', '0140', false, '', false); ?> / <?php InterfaceLanguage('Screen', '0721', false, '', false); ?></th></tr>-->
                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));

                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>"></td>
                                                        <td width="485" class="no-padding-left" style="padding-right:18px;">
                                                            <div class="campaign-details">
        <!--                                                                <a  target="_blank"  class="preview email-preview" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center" ng-href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>">
                                                                    <img width="120" height="105" class="ng-scope" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/scrrenshot.png">
                                                                </a>-->
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>

                                                                    <span class="type ng-binding"><?php echo $EachCampaign['CreateDateTime'] == '' ? '' : date('M d, Y - H:i', strtotime($EachCampaign['CreateDateTime'])); ?></span>

                                                                    <div class="btn-group">
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Update', $UserInformation)): ?>
                                                                            <a  class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php print($EachCampaign['CampaignID']); ?>" class="btn default btn-sm purple"><?php InterfaceLanguage('Screen', '1752'); ?></a>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php elseif ($FilterData == 'PendingApproval'): ?>
                                            <table class="grid items items-custom table table-hover" id="campaigns-table">
                                                <!--<tr class="item ng-scope ng-isolate-scope"><th width="700" colspan="2"><?php InterfaceLanguage('Screen', '0140', false, '', false); ?> / <?php InterfaceLanguage('Screen', '0722', false, '', false); ?></th></tr>-->
                                                <?php if (count($Campaigns) < 1): ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td><?php InterfaceLanguage('Screen', '0923', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                                <?php
                                                foreach ($Campaigns as $EachCampaign):
                                                    if (($EachCampaign['SplitTest'] != false) && ($EachCampaign['RelWinnerEmailID'] == 0)) {
                                                        $TestEmailVersions = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
                                                    }
                                                    $EmailID = ($EachCampaign['SplitTest'] == false ? $EachCampaign['RelEmailID'] : ($EachCampaign['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $EachCampaign['SplitTest']['RelWinnerEmailID']));

                                                    // Unique open percentage calculation - Start {
                                                    $UniqueOpenRate = Campaigns::CalculateCampaignStatisticsRates($EachCampaign, 'UniqueOpenRate');
                                                    // Unique open percentage calculation - End }
                                                    ?>
                                                    <tr class="item ng-scope ng-isolate-scope">
                                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedCampaigns[]" value="<?php print($EachCampaign['CampaignID']); ?>" id="SelectedCampaigns<?php print($EachCampaign['CampaignID']); ?>"></td>
                                                        <td width="485" class="no-padding-left" style="padding-right:18px;">
                                                            <div class="campaign-details">
        <!--                                                                <a  target="_blank"  class="preview email-preview" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center" ng-href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>">
                                                                    <img width="120" height="105" class="ng-scope" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/scrrenshot.png">
                                                                </a>-->
                                                                <?php
                                                                $img_url = isset($EachCampaign['Email']['ScreenshotImage']) && !empty($EachCampaign['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $EachCampaign['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                                                ?>
                                                                <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                   data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                    <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                </a>
                                                                <div class="column-block">
                                                                    <h4>
                                                                        <?php if (InterfacePrivilegeCheck('Campaign.Get', $UserInformation)): ?>
                                                                            <a target="_blank"  class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $EachCampaign['CampaignID']; ?>" dir="ltr">
                                                                                <?php print($EachCampaign['CampaignName']); ?>
                                                                            </a>
                                                                        <?php else: ?>
                                                                            <?php print($EachCampaign['CampaignName']); ?>												
                                                                        <?php endif; ?>

                                                                    </h4>
                                                                    <span class="type ng-binding"><?php echo $EachCampaign['CreateDateTime'] == '' ? '' : date('M d, Y - H:i', strtotime($EachCampaign['CreateDateTime'])); ?></span>


                                                                    <?php foreach ($EachCampaign['Tags'] as $EachTag): ?>
                                                                        <span class="badge badge-danger"><?php print($EachTag['Tag']); ?></span>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($EachCampaign['SplitTest'] != false): ?>
                                                                        <span class="badge badge-danger"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php endif; ?>
                                    </form>

                                    <?php if ($TotalPages > 1): ?>
                                        <div class="span-1 last">
                                            <ul class="pagination with-module">
                                                <?php
                                                if ($CurrentPage > 1):
                                                    ?>
                                                    <li class="first"><a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
                                                    <?php
                                                endif;
                                                ?>
                                                <?php
                                                $CounterStart = ($CurrentPage < 3 ? 1 : $CurrentPage - 2);
                                                $CounterFinish = ($CurrentPage > ($TotalPages - 2) ? $TotalPages : $CurrentPage + 2);
                                                for ($PageCounter = $CounterStart; $PageCounter <= $CounterFinish; $PageCounter++):
                                                    ?>
                                                    <li <?php print(($CurrentPage == $PageCounter ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/<?php print($PageCounter); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/<?php echo implode(':', $FilterTags); ?>"><?php print(number_format($PageCounter)); ?></a></li>
                                                    <?php
                                                endfor;
                                                ?>
                                                <?php
                                                if ($CurrentPage < $TotalPages):
                                                    ?>
                                                    <li class="last"><a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/<?php print($TotalPages); ?>/<?php print($RPP); ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/">&nbsp;</a></li>
                                                        <?php
                                                    endif;
                                                    ?>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '0727': '<?php InterfaceLanguage('Screen', '0727', false, '', false); ?>',
            '0957': '<?php InterfaceLanguage('Screen', '0957', false, '', false); ?>',
            '0959': '<?php InterfaceLanguage('Screen', '0959', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaigns.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>