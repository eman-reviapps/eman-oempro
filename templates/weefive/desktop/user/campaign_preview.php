<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_campaign_preview_header.php'); ?>
<div id="top" class="iguana">
    <div class="container">
        <div class="span-14">
            <ul class="tabs">
                <li class="label"><?php InterfaceLanguage('Screen', '0813', false, '', false); ?></li>
                <li <?php if ($Mode == 'html'): ?>class="selected"<?php endif; ?>>
                    <a href="<?php echo InterfaceAppURL('true') . '/user/campaigns/preview/' . $CampaignInformation['CampaignID'] . '/html'; ?>">
                        <?php if ($Mode == 'html'): ?>
                            <span class="left">&nbsp;</span>
                            <span class="right">&nbsp;</span>
                        <?php endif ?>
                        <strong><?php InterfaceLanguage('Screen', '0175', false, '', true, false, array()); ?></strong>
                    </a>
                </li>
                <li <?php if ($Mode == 'plain'): ?>class="selected"<?php endif; ?>>
                    <a href="<?php echo InterfaceAppURL('true') . '/user/campaigns/preview/' . $CampaignInformation['CampaignID'] . '/plain'; ?>">
                        <?php if ($Mode == 'plain'): ?>
                            <span class="left">&nbsp;</span>
                            <span class="right">&nbsp;</span>
                        <?php endif ?>
                        <strong><?php InterfaceLanguage('Screen', '0176', false, '', true, false, array()); ?></strong>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
    <div class="container">
        <div class="span-24 last">
            <iframe src="<?php InterfaceAppURL(); ?>/user/campaigns/preview/<?php echo $CampaignInformation['CampaignID'] ?>/<?php echo $Mode; ?>/source" name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
        </div>
    </div>
</div>




<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_campaign_preview_footer.php'); ?>