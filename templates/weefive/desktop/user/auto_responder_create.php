<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', $IsEditEvent ? '1025' : '0996', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="auto-responder-create" action="<?php InterfaceAppURL(); ?>/user/autoresponders/<?php echo $IsEditEvent ? 'edit/' . $ListID . '/' . $AutoResponder['AutoResponderID'] : 'create/' . $ListID; ?>" method="post">
                                                <input type="hidden" name="Command" value="<?php echo $IsEditEvent ? 'Edit' : 'Create'; ?>" id="Command">
                                                <?php
                                                if (isset($PageCreateErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageCreateErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <h4 class="form-section bold font-blue" style="margin-top: 10px !important"><?php InterfaceLanguage('Screen', '0037', false); ?></h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('AutoResponderName') != '' ? 'has-error' : '')); ?>" id="form-row-AutoResponderName">
                                                    <label class="col-md-3 control-label" for="AutoResponderName"><?php InterfaceLanguage('Screen', '0051', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="AutoResponderName" value="<?php echo set_value('AutoResponderName', $AutoResponder['AutoResponderName']); ?>" id="AutoResponderName" class="form-control" />
                                                        <?php print(form_error('AutoResponderName', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-AutoResponderTriggerType">
                                                    <label class="col-md-3 control-label" for="AutoResponderTriggerType"><?php InterfaceLanguage('Screen', '1004', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select name="AutoResponderTriggerType" id="AutoResponderTriggerType" class="form-control">
                                                            <option value="OnSubscription" <?php echo set_select('AutoResponderTriggerType', 'OnSubscription', $AutoResponder['AutoResponderTriggerType'] == 'OnSubscription'); ?>><?php InterfaceLanguage('Screen', '1005', false, '', false, false); ?></option>
                                                            <option value="OnSubscriberLinkClick" <?php echo set_select('AutoResponderTriggerType', 'OnSubscriberLinkClick', $AutoResponder['AutoResponderTriggerType'] == 'OnSubscriberLinkClick'); ?>><?php InterfaceLanguage('Screen', '1006', false, '', false, false); ?></option>
                                                            <option value="OnSubscriberCampaignOpen" <?php echo set_select('AutoResponderTriggerType', 'OnSubscriberCampaignOpen', $AutoResponder['AutoResponderTriggerType'] == 'OnSubscriberCampaignOpen'); ?>><?php InterfaceLanguage('Screen', '1461', false, '', false, false); ?></option>
                                                            <option value="OnSubscriberForwardToFriend" <?php echo set_select('AutoResponderTriggerType', 'OnSubscriberForwardToFriend', $AutoResponder['AutoResponderTriggerType'] == 'OnSubscriberForwardToFriend'); ?>><?php InterfaceLanguage('Screen', '1007', false, '', false, false); ?></option>
                                                            <option value="OnSubscriberDate" <?php echo set_select('AutoResponderTriggerType', 'OnSubscriberDate', $AutoResponder['AutoResponderTriggerType'] == 'OnSubscriberDate'); ?>><?php InterfaceLanguage('Screen', '1587', false, '', false, false); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1008', false, '', false, false); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('AutoResponderTriggerValue') != '' ? 'has-error' : '')); ?>" id="form-row-AutoResponderTriggerValue" style="display:none">
                                                    <label class="col-md-3 control-label" for="AutoResponderTriggerValue"><?php InterfaceLanguage('Screen', '1009', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="AutoResponderTriggerValue" value="<?php echo set_value('AutoResponderTriggerValue', $AutoResponder['AutoResponderTriggerValue']); ?>" id="AutoResponderTriggerValue" class="form-control" />
                                                        <?php print(form_error('AutoResponderTriggerValue', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-AutoResponderTriggerValue-Campaign" style="display:none">
                                                    <label class="col-md-3 control-label" for="AutoResponderTriggerValue-Campaign"><?php InterfaceLanguage('Screen', '1462', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select name="AutoResponderTriggerValue-Campaign" id="AutoResponderTriggerValue-Campaign" class="form-control">
                                                            <?php if (count($Campaigns) > 0): ?>
                                                                <?php foreach ($Campaigns as $EachCampaign): ?>
                                                                    <option value="<?php echo $EachCampaign['CampaignID']; ?>" <?php echo set_select('AutoResponderTriggerValue', $EachCampaign['CampaignID'], $AutoResponder['AutoResponderTriggerValue'] == $EachCampaign['CampaignID']); ?>><?php echo $EachCampaign['CampaignName']; ?></option>
                                                                <?php endforeach ?>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-AutoResponderTriggerValue-DateField" style="display:none">
                                                    <label class="col-md-3 control-label" for="AutoResponderTriggerValue-DateField"><?php InterfaceLanguage('Screen', '1588', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select name="AutoResponderTriggerValue-DateField" id="AutoResponderTriggerValue-DateField" class="form-control">
                                                            <?php if (count($Fields) > 0): ?>
                                                                <?php foreach ($Fields as $EachField): ?>
                                                                    <option value="<?php echo $EachField['CustomFieldID']; ?>" <?php echo set_select('AutoResponderTriggerValue', $EachField['CustomFieldID'], $AutoResponder['AutoResponderTriggerValue'] == $EachField['CustomFieldID']); ?>><?php echo $EachField['FieldName']; ?></option>
                                                                <?php endforeach ?>
                                                            <?php endif; ?>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1589', false, '', false, false); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-AutoResponderTriggerValue2" style="display:none">
                                                    <label class="col-md-3 control-label" for="AutoResponderTriggerValue2"><?php InterfaceLanguage('Screen', '1593', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select name="AutoResponderTriggerValue2" id="AutoResponderTriggerValue2" class="form-control">
                                                            <option value="Every month" <?php echo set_select('AutoResponderTriggerValue2', $AutoResponder['AutoResponderTriggerValue2'], $AutoResponder['AutoResponderTriggerValue2'] == 'Every month'); ?>><?php InterfaceLanguage('Screen', '1594'); ?></option>
                                                            <option value="Every year" <?php echo set_select('AutoResponderTriggerValue2', $AutoResponder['AutoResponderTriggerValue2'], $AutoResponder['AutoResponderTriggerValue2'] == 'Every year'); ?>><?php InterfaceLanguage('Screen', '1595'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('TriggerTime') != '' ? 'has-error' : '')); ?>" id="form-row-TriggerTimeType">
                                                    <label class="col-md-3 control-label" for="TriggerTimeType"><?php InterfaceLanguage('Screen', '0828', false, '', false, true); ?>:</label>

                                                    <input type="text" name="TriggerTime" value="<?php echo set_value('TriggerTime', $AutoResponder['TriggerTime']); ?>" id="TriggerTime" class="col-md-1 form-control" style="width:50px;display:none" />

                                                    <div class="col-md-4">
                                                        <select name="TriggerTimeType" id="TriggerTimeType" class="form-control">
                                                            <option value="Immediately" <?php echo set_select('TriggerTimeType', 'Immediately', $AutoResponder['TriggerTimeType'] == 'Immediately'); ?>><?php InterfaceLanguage('Screen', '1010', false, '', false, false); ?></option>
                                                            <option value="Seconds later" <?php echo set_select('TriggerTimeType', 'Seconds later', $AutoResponder['TriggerTimeType'] == 'Seconds later'); ?>><?php InterfaceLanguage('Screen', '1011', false, '', false, false); ?></option>
                                                            <option value="Minutes later" <?php echo set_select('TriggerTimeType', 'Minutes later', $AutoResponder['TriggerTimeType'] == 'Minutes later'); ?>><?php InterfaceLanguage('Screen', '1012', false, '', false, false); ?></option>
                                                            <option value="Hours later" <?php echo set_select('TriggerTimeType', 'Hours later', $AutoResponder['TriggerTimeType'] == 'Hours later'); ?>><?php InterfaceLanguage('Screen', '1013', false, '', false, false); ?></option>
                                                            <option value="Days later" <?php echo set_select('TriggerTimeType', 'Days later', $AutoResponder['TriggerTimeType'] == 'Days later'); ?>><?php InterfaceLanguage('Screen', '1014', false, '', false, false); ?></option>
                                                            <option value="Weeks later" <?php echo set_select('TriggerTimeType', 'Weeks later', $AutoResponder['TriggerTimeType'] == 'Weeks later'); ?>><?php InterfaceLanguage('Screen', '1015', false, '', false, false); ?></option>
                                                            <option value="Months later" <?php echo set_select('TriggerTimeType', 'Months later', $AutoResponder['TriggerTimeType'] == 'Months later'); ?>><?php InterfaceLanguage('Screen', '1016', false, '', false, false); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1017', false, '', false, false); ?>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php print(form_error('TriggerTime', '<span class="help-block">', '</span>')); ?>
                                                    </div>


                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($IsEditEvent): ?>
                                                                <?php if ($AutoResponder['RelEmailID'] == 0): ?>
                                                                    <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/create/autoresponder/<?php echo $AutoResponder['AutoResponderID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1170', false, '', false); ?></strong></a>
                                                                <?php else: ?>
                                                                    <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/edit/autoresponder/<?php echo $AutoResponder['AutoResponderID']; ?>/<?php echo $AutoResponder['RelEmailID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1422', false, '', false); ?></strong></a>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                            <a class="btn default" targetform="auto-responder-create" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', $IsEditEvent ? '0562' : '1003', false, '', false); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_autorespondercopy.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/autoresponder_create.js" type="text/javascript" charset="utf-8"></script>		
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>