
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">                                 
            <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
            <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="tags-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
            <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="tags-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
            <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="apikeys-table-form"><?php InterfaceLanguage('Screen', '1941'); ?></a>                                        
            <!--<i class="icon-microphone font-purple-sharp"></i>-->
            <!--<span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1940', false, '', false); ?> </span>-->                    
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" >                        
                <a href="<?php InterfaceAppURL(); ?>/user/apikeys/create" class="btn default btn-transparen btn-sm" ><?php InterfaceLanguage('Screen', '1942'); ?></a>                                       
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container">
            <form id="apikeys-table-form" action="<?php InterfaceAppURL(); ?>/user/apikeys/" method="post">
                <input type="hidden" name="Command" value="DeleteAPIKeys" id="Command">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php
                        if (isset($PageSuccessMessage) == true):
                            ?>
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php print($PageSuccessMessage); ?>
                            </div>
                            <?php
                        elseif (isset($PageErrorMessage) == true):
                            ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php print($PageErrorMessage); ?>
                            </div>
                            <?php
                        endif;
                        ?>



                    </div>
                </div>
                <div class="table-responsive">
                    <table class="grid table table-bordered" id="tags-table">
                        <tr>
                            <th colspan="2"><?php InterfaceLanguage('Screen', '1943', false, '', true); ?></th>
                            <th><?php InterfaceLanguage('Screen', '1946', false, '', true); ?></th>
                            <th><?php InterfaceLanguage('Screen', '1947', false, '', true); ?></th>
                        </tr>
                        <?php
                        if (count($APIKeys) < 1):
                            ?>
                            <tr>
                                <td colspan="4"><?php InterfaceLanguage('Screen', '1944'); ?></td>
                            </tr>
                            <?php
                        endif;
                        foreach ($APIKeys as $Each):
                            ?>
                            <tr>
                                <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedKeys[]" value="<?php print($Each->getId()); ?>" id="SelectedKeys<?php print($Each->getId()); ?>"></td>
                                <td><?php print($Each->getAPIKey()); ?></td>
                                <td width="170"><?php print($Each->getNote()); ?></td>
                                <td><?php print($Each->getIPAddress()); ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="page-shadow">
    <div id="page">
        <div class="white" style="min-height:420px;" id="page-user-tags">

        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1088': '<?php InterfaceLanguage('Screen', '1088', false, '', false); ?>',
            '1091': '<?php InterfaceLanguage('Screen', '1091', false, '', false, true); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/tags.js" type="text/javascript" charset="utf-8"></script>		
