<div class="col-md-11" style="padding: 0">
    <?php if ($Wizard->get_current_step() != $CurrentFlow->get_step_count()): ?>

        <nav>
            <ol class="cd-breadcrumb triangle">
                <?php for ($i = 0; $i < $CurrentFlow->get_step_count(); $i++): ?>

                    <?php
                    $class = "";
                    if ($Wizard->get_current_step() == ($i + 1)) {
                        ?>
                        <li class="current">
                            <em><?php echo $i + 1; ?> / <?php echo $CurrentFlow->get_step($i)->get_title(); ?></em>
                        </li>                           
                        <?php
                    } else {
                        if ($Wizard->get_last_step() < $i + 1) {
                            $class = 'disabled';
                        } else {
                            $class = 'previous-step';
                        }
                        ?>
                        <li>
                            <a class="<?php echo $class ?>" href="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>/<?php echo $i + 1; ?>"><?php echo $i + 1; ?> / <?php echo $CurrentFlow->get_step($i)->get_title(); ?></a>
                        </li>
                        <?php
                    }
                    ?>
                <?php endfor; ?>
            </ol>
        </nav>
    <?php endif; ?>
</div>
<div class="col-md-1" style="padding: 0;text-align: right">
    <?php if ($Wizard->get_current_step() != $CurrentFlow->get_step_count()): ?>
        <ol class="cd-breadcrumb triangle cd-breadcrumb-custom" >
            <li class="current">
                <a class="btn default control-next" targetform="subscriber-import" href="#">
                    <em><?php InterfaceLanguage('Screen', '0761', false, '', true); ?> &rarr;</em>
                </a>
            </li>
        </ol>

        
        <!--<a class="btn default control-next disabled" href="#" style="display:none;"><strong><?php InterfaceLanguage('Screen', '1939', false, '', true); ?></strong></a>-->
    <?php endif; ?>
</div>
<div class="col-md-2" style="padding-left: 0">
    <a class="btn default control-previous" href="<?php echo InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>"><strong><?php InterfaceLanguage('Screen', '1208', false, '', true); ?></strong></a>
</div>




<script type="text/javascript">
    $(document).ready(function () {
        $('.control-next').not('.disabled').click(function (ev) {
            var $this = $(this);
            $this.hide();
            $('.control-next.disabled').show();
        });
        $('.control-next.disabled').click(function (ev) {
            ev.preventDefault();
            return false;
        });
    });
</script>