
<?php
//print_r('<pre>');
//print_r($TemplateInformation);
//print_r($_SESSION['template_builder']['TemplateID']);
//print_r($_SESSION['template_builder']['ScreenshotImage']);
//print_r('</pre>');
if (isset($TemplateInformation['HTMLPure'])) {
    $contenuto = $TemplateInformation['HTMLPure'];
} else {
    $contenuto = '';
}
//echo $contenuto;

$ContentTags = $TemplateInformation['ContentTags'];

$Tags = array();
foreach ($ContentTags as $LabelTag => $TagGroup) {
    $key_array = array();
    foreach ($TagGroup as $Tag => $Label) {
        $key_array[$Label] = $Tag;
    }
    $Tags[$LabelTag] = $key_array;
}
?>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_dragdrop_builder_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_dragdrop_builder_sidebar.php'); ?>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/html2canvas/html2canvas.js"></script>

<script>
    var APP_URL = '<?php InterfaceAppURL(); ?>';
    var BUILDER_IFRAME_SOURCE = APP_URL + '/admin/emailtemplates/builder/displayHTMLContent';
</script>

<script type="text/javascript">
    var controller = 'emailtemplates';
    var save_ajax_function = 'saveajax';
</script>
<script type="text/javascript" src="<?= InterfaceTemplateURL(); ?>/js/dragdrop/template.editor.js"></script>

<div class="row" style="padding-top:10px">
    <div class="col-md-4">
        <!--eturn confirm('message for exit action');-->
        <a style="margin-left:70px" onclick="" id="backtoprefrences" class="btn default" href="<?php InterfaceAppURL(); ?>/user/emailtemplates/edit/<?php print $TemplateInformation['TemplateID']; ?>"><i class="fa fa-arrow-left"></i> <?php InterfaceLanguage('Screen', '0503', false); ?></a>

        <div class="btn-group">
            <a class="btn default" id="photoeditor" target="_blank" href="<?php InterfaceAppURL(false); ?>/user/photoeditor/" >Photo Editor</a>
        </div>
    </div>
    <div class="col-md-4">
        <div id="messagefromphp"></div>
        <div id="messagefromphp2"></div>
    </div>

    <div class="col-md-4">
        <div class="pull-right">
            <div class="btn-group" data-toggle="buttons-radio">
                <button type="button" class="btn default" id="sourcepreview"><i class="glyphicon-eye-open glyphicon"></i> Preview</button>
            </div>
            <div class="btn-group">
                <a class="btn default" href="#save" id="save" ><i class="glyphicon glyphicon-floppy-disk"></i> <?php InterfaceLanguage('Screen', '0304', false); ?></a>                
            </div>

        </div>
    </div>
</div>

<br />
<?php ?>
<div id="tosave" data-id="<?php echo $id ?>"  data-paramone="11" data-paramtwo="22" data-paramthree="33">

    <?php
    if (!strlen($contenuto)) {
        ?>
        <table  width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #eeeeee" >
            <tr>
                <td width="100%" id="primary" class="main demo" align="center" valign="top" >
                    <div class="lyrow">
                        <div class="view">
                            <div class="row clearfix">
                                <!-- Content starts here-->
                                <table width="640" class="preheader" align="center" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                    <div class="footer-data">

                                        <p>
                                            <a href="<?php echo $Tags['List links']['Unsubscription Link'] ?>">Unsubscribe</a>
                                        </p>
                                    </div>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="column">

                        <!-- default element text -->
                        <div class="lyrow">
                            <a href="#close" class="remove label label-danger"><i class="glyphicon-remove glyphicon"></i></a>
                            <span class="drag label label-default"><i class="glyphicon glyphicon-move"></i></span>
                            <div class="view">

                                <div class="row clearfix">
                                    <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center" data-type='text-block' style="background-color: #FFFFFF;">
                                        <tbody>
                                            <tr>
                                                <td  class="block-text" align="left" style="padding:10px 50px 10px 50px;font-family:Arial;font-size:13px;color:#000000;line-height:22px">
                                                    <p style="margin:0px 0px 10px 0px;line-height:22px">
                                        <center>
                                            <i class="fa fa-arrow-up fa-3x"></i> <br><br>
                                            Modify me or drag the content of email in top or bottom <br><br>
                                            <i class="fa fa-arrow-down fa-3x"></i>
                                        </center>
                                        </p>

                                        </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="lyrow">
                            <div class="view">
                                <div class="row clearfix">
                                    <!-- Content starts here-->
                                    <table width="640" class="preheader" align="center" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td>
                                                <div class="footer-data">
                                                    <p style="text-align: center;">
                                                        2016 &copy; <?php echo $Tags['User information']['Company Name'] ?> | <?php echo $Tags['User information']['Street'] ?>
                                                        / <?php echo $Tags['User information']['City'] ?> , <?php echo $Tags['User information']['Country'] ?>
                                                    </p>
                                                    <p style="text-align: center;">
                                                        This email was sent to <?php echo $Tags['Subscriber information']['Email Address'] ?> <a href="<?php echo $Tags['List links']['Unsubscription Link'] ?>">Unsubscribe</a>
                                                    </p>
                                                    <p style="text-align: center;">
                                                        To enusre that you continue receiving our emails, Please add us to your address book
                                                    </p>
                                                    <p style="text-align: center;">
                                                        Powered by <a target="_blank" href="http://flyinglist.com/">FlyingList</a>
                                                    </p>
                                                    <p style="text-align: center;">
                                                        <img style="width: 190px" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/logo-flying.png" alt="logo" class="logo-default">
                                                    </p>
                                                </div>
                                            </td>
        <!--                                            <td align="left" class="preheader-text" width="420" style="padding: 15px 0px; font-family: Arial; font-size: 11px; color: #666666"></td>
                                        <td class="preheader-gap" width="20"></td>
                                        <td class="preheader-link" align="right" width="200" style="padding: 15px 0px; font-family: Arial; font-size: 11px; color: #666666">
                                            [linkcancellazione]
                                            <br/>
                                            
                                        </td>-->
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
        </table>
        <?php
    } else {
        echo $contenuto;
    }
    ?>


</div>

<div id="download-layout">

</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_dragdrop_builder_footer.php'); ?>