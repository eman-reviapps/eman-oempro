<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1721', false, '', false, true, array('Wufoo')); ?> </span>                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >             
                        <a href="<?php InterfaceAppURL(); ?>/user/integration/wufoo/form/" class="btn default btn-transparent btn-sm"><?php InterfaceLanguage('Screen', '1723'); ?></a>
                        <a href="<?php InterfaceAppURL(); ?>/user/integration/" class="btn default btn-sm" ><?php InterfaceLanguage('Screen', '1722', false); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive" id="page-user-wufoo">
                        <table class="grid table table-bordered table-striped table-condensed flip-content" id="integrations-table">
                            <?php if (count($Integrations) < 1): ?>
                                <tr>
                                    <td width="355"><?php InterfaceLanguage('Screen', '1751'); ?></td>
                                </tr>
                            <?php else: ?>
                                <?php
                                foreach ($Integrations as $EachIntegration):
                                    ?>
                                    <tr>
                                        <td width="355">
                                            <span class="data-label"><?php InterfaceLanguage('Screen', '1727', false); ?>: </span>
                                            <a href="http://<?php echo $EachIntegration->getWufooSubDomain(); ?>.wufoo.com/forms/<?php echo $EachIntegration->getFormHash(); ?>" target="_blank"><?php echo $EachIntegration->getFormName(); ?></a>
                                            &rarr;
                                            <?php foreach ($Lists as $Each): ?>
                                                <?php
                                                if ($Each['ListID'] == $EachIntegration->getListId()) {
                                                    echo '<span class="data-label">' . InterfaceLanguage('Screen', '1444', true) . ': </span>';
                                                    echo '<a href="' . InterfaceAppURL(true) . '/user/list/statistics/' . $Each['ListID'] . '">' . $Each['Name'] . '</a>';
                                                    break;
                                                }
                                                ?>
                                            <?php endforeach; ?>
                                            <br>
                                            <span class="data-label small"><?php InterfaceLanguage('Screen', '1734', false); ?></span> <span class="data small"><?php echo $EachIntegration->getSuccessfulLogCount(); ?></span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="<?php InterfaceAppURL(); ?>/user/integration/wufoo/delete/<?php echo $EachIntegration->getId(); ?>" class="red small">Remove</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceTemplateURL(false); ?>js/screens/user/settings_integration.js" type="text/javascript" charset="utf-8"></script>
