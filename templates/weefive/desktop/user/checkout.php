<?php
include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php');
//print_r($PluginLanguage);
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/birdie.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL STYLES -->

<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php InterfaceLanguage('CheckOut', '0001', false, '', false, false); ?>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-transparent">
            <div class="portlet-body">
                <div class="portlet portlet-fit">
                    <div class="portlet-body">
                        <div class="row" style="margin-right: 0px">
                            <div class="col-md-7 col-sm-7">
                                <?php
                                if (isset($PageErrorMessage) == true && isset($ErrorCode)) {
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>         
                                    <?php
                                }

                                if (isset($CheckoutFormContent) == true && !isset($ErrorCode) && isset($Status) && $Status == 'success') {
                                    ?>
                                    <div id="iyzipay-checkout-form" class="responsive"><?php print_r($CheckoutFormContent) ?></div>
                                    <?php
                                }
                                ?>

                                <ul class="payment-icons">

                                    <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/amex.png">

                                    <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/visa_electron.png">

                                    <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/visa.png">

                                    <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/maestro.png">

                                    <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/mastercard.png">

                                    <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/iyzico.png">

                                </ul>
                            </div>
                            <div class="col-md-5 col-sm-5  pull-right">
                                <div class="col-md-12">
                                    <div class="portlet sale-summary">
                                        <div class="portlet-title">
                                            <div class="caption font-purple-sharp sbold">  <?php InterfaceLanguage('CheckOut', '0002', false, '', false, false); ?> </div>
                                        </div>
                                        <div class="portlet-body">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <span class="sale-info bold"> Package <i class="fa fa-img-up"></i>
                                                    </span>
                                                    <span class="sale-desc"> <?= $UserInformation['GroupInformation']['GroupName'] ?></span> 
                                                </li>
                                                <li>
                                                    <span class="sale-info bold"> Total <i class="fa fa-img-down"></i>
                                                    </span>
                                                    <span class="sale-desc"> <?= $PaidPrice ?> $</span>
                                                </li>
                                                <?php
                                                if ($ExchangeInfo[0] != 1) {
                                                    ?>
                                                    <li>
                                                        <span class="sale-info bold"> Echange Rate <i class="fa fa-img-down"></i>
                                                        </span>
                                                        <span class="sale-desc"> <?= "1 " . "$" . " = " . $ExchangeInfo[0] . " " . $ExchangeInfo[1] ?> </span>
                                                    </li>
                                                    <li>
                                                        <span class="sale-info bold"> <?= $PaidPrice ?> $ =  <i class="fa fa-img-down"></i>
                                                        </span>
                                                        <span class="sale-desc"> <?= $ExchangePrice ?> $ <?= $ExchangeInfo[1] ?></span>
                                                    </li>
                                                    <?php
                                                }
                                                ?>

                                            </ul>
                                            <p class="sc-checkout-box__notes" translate=""><span>By completing your purchase, you agree to these <a href="https://flyinglist.com/ar/distance-sales-contract/" class="bold" target="_blank"> Distance Sales Contract </a>.</span></p>
                                        </div>
                                    </div>

                                    <script language="JavaScript" type="text/javascript" >
                                        TrustLogo("https://flyinglist.com/comodo_secure_seal_100x85_transp.png", "CL1", "none");
                                    </script>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php');


