<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_campaign_preview_header.php'); ?>

<div id="top" class="iguana">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-8">
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="label" style="margin-top: 5px"><?php InterfaceLanguage('Screen', '0988', false, '', false); ?></li>
                        <?php if ($EmailInformation['ContentType'] != 'Plain'): ?>
                            <li <?php if ($Type == 'html'): ?>class="active"<?php endif; ?>>
                                <a href="<?php echo InterfaceAppURL('true') . '/user/email/preview/' . $EmailID . '/html/' . $Mode . '/' . $EntityID; ?>">
                                    <?php if ($Type == 'html'): ?>
                                        <span class="left">&nbsp;</span>
                                        <span class="right">&nbsp;</span>
                                    <?php endif ?>
                                    <strong><?php InterfaceLanguage('Screen', '0175', false, '', true, false, array()); ?></strong>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($EmailInformation['ContentType'] != 'HTML'): ?>
                            <li <?php if ($EmailInformation['ContentType'] == 'Plain' || $Type == 'plain'): ?>class="active"<?php endif; ?>>
                                <a href="<?php echo InterfaceAppURL('true') . '/user/email/preview/' . $EmailID . '/plain/' . $Mode . '/' . $EntityID; ?>">
                                    <?php if ($Type == 'plain'): ?>
                                        <span class="left">&nbsp;</span>
                                        <span class="right">&nbsp;</span>
                                    <?php endif ?>
                                    <strong><?php InterfaceLanguage('Screen', '0176', false, '', true, false, array()); ?></strong>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="tabs right">
                    <li>
                        <a href="#" class="btn btn-sm default" id="toggle-details"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="middle" class="iguana row" style="padding-top:0;background-color:#fff;">
    <div class="col-md-8 col-md-offset-2">
        <div class="container" style="padding-bottom:0;margin-top: 10px" id="details">
            <div class="last">
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label class="col-md-3 control-label"  for="From"><?php InterfaceLanguage('Screen', '1817', false); ?>:</label>
                            <div class="col-md-6">
                                <input type="text" name="From" value="<?php echo $EmailInformation['FromName'] . ' &lt;' . $EmailInformation['FromEmail'] . '&gt;'; ?>" id="From" class="form-control" readonly="readonly" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label class="col-md-3 control-label"  for="Subject"><?php InterfaceLanguage('Screen', '0106', false); ?>:</label>
                            <div class="col-md-6">
                                <input type="text" name="Subject" value="<?php echo $EmailInformation['Subject']; ?>" id="Subject" class="form-control" readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 10px">
            <iframe src="<?php InterfaceAppURL(); ?>/user/email/preview/<?php echo $EmailID ?>/<?php echo $Type; ?>/<?php echo $Mode; ?>/<?php echo $EntityID ?>/source" name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
        </div>
    </div>

</div>


<script type="text/javascript">
    var Language = {
        '1818': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1818', true), ENT_QUOTES); ?>',
        '1819': '<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1819', true), ENT_QUOTES); ?>'
    };

    var toggleDetailsEl, details;
    $(document).ready(function ()
    {
        toggleDetailsEl = $('#toggle-details'),
                details = $('#details');
        toggleDetailsEl.click(toggleDetails);
        toggleDetails();
    });

    function toggleDetails()
    {
        var details = $('#details');
        details.toggle();
        if (details.is(':visible')) {
            toggleDetailsEl.text(Language['1818']);
        } else {
            toggleDetailsEl.text(Language['1819']);
        }
        return false;
    }
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_campaign_preview_footer.php'); ?>