<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign_header2.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption font-purple-sharp sbold"><?= $Title ?></span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <?php
                            if (count($ResultRows) > 0) {
//                                print_r($ResultRows[0]);
                                foreach ($ResultRows[0] as $key => $value) {
                                    ?>
                                    <th> <?php echo $key ?> </th>
                                    <?php
                                }
                            } else {
                                foreach ($ResultColumns as $key => $value) {
                                    ?>
                                    <th> <?php echo $key ?> </th>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                    </thead>
                    <?php
                    if (count($ResultRows) > 0) {
                        ?>
                        <tbody>
                            <?php
                            foreach ($ResultRows as $Row) {
                                ?>
                                <tr>
                                    <?php
                                    foreach ($ResultRows[0] as $key => $value) {
                                        ?>
                                        <td> <?php echo isset($Row[$key]) ? $Row[$key] : '' ?> </td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <?php
                    }
                    ?>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>