<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<div class="page-content-inner">    
    <div class="row">
        <div class="col-md-12">
            <div style="padding-right: 15px">
                <div class="portlet light bg-inverse" style="padding-bottom: 0px;margin-bottom: 15px">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-purple-sharp sbold">
                                <?php echo ucwords($Title); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet">
                <div class="portlet-body">
                    <?php // echo $SubSection ?>
                    <?php
                    if ($SubSection == 'flowlist') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_flow_list.php');
                    } elseif ($SubSection == 'settings') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_settings.php');
                    } elseif ($SubSection == 'content') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_content.php');
                    } elseif ($SubSection == 'preview') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_preview.php');
                    } elseif ($SubSection == 'copyfromlist') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_from_another_list.php');
                    } elseif ($SubSection == 'copycampaign') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_copy_campaign.php');
                    } elseif ($SubSection == 'fetchurl') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_import.php');
                    } elseif ($SubSection == 'gallery') {
                        include_once(TEMPLATE_PATH . 'desktop/user/email_create_gallery.php');
                    }
//elseif ($SubSection == 'dragdropemail')
//	{
//	include_once(TEMPLATE_PATH.'desktop/user/email_create_dragdrop.php');
//
//	}
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/email_create.js" type="text/javascript" charset="utf-8"></script>		
<script>
    $("#flow_fromscratch").click(function (e) {
        e.preventDefault();
        hash = $(this).attr("href");
        bootbox.alert({
            title: "<span class='bold' > Message </span>",
            message: "Please note that in case you create plain email, we won't be able to track the email for fetching open/click/forward/spam ..etc statistics. <br/><br/>\n\
                        Or you can create text email using drag-drop builder for Full Report",
            buttons: {
                ok: {
                    label: 'Ok',
                    className: 'btn purple-sharp'
                }
            },
            callback: function (result) {
                console.log(hash)
                window.location = hash;
            }
        });
    });

</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>