<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', '1151', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="autoresponders-copy" action="<?php InterfaceAppURL(); ?>/user/autoresponders/copy/<?php echo $ListInformation['ListID']; ?>" method="post">
                                                <input type="hidden" name="Command" value="Copy" id="Command">
                                                <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageCreateErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <h4 class="form-section bold font-blue" style="margin-top: 10px !important"><?php InterfaceLanguage('Screen', '0037', false); ?></h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('Name') != '' ? 'has-error' : '')); ?>" id="form-row-Name">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1154', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" value="<?php echo $ListInformation['Name']; ?>" class="form-control" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-SourceListID">
                                                    <label class="col-md-3 control-label" for="SourceListID"><?php InterfaceLanguage('Screen', '1152', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select name="SourceListID" id="SourceListID" class="form-control">
                                                            <?php foreach ($Lists as $Each): ?>
                                                                <option value="<?php echo $Each['ListID']; ?>"><?php echo $Each['Name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn default" targetform="autoresponders-copy" id="form-button" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1153', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_autorespondercopy.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>