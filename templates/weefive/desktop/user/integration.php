<div class="row">
    <div class="col-md-10 col-sm-10">
        <div class="portlet ">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"><?php InterfaceLanguage('Screen', '0132', false, '', false); ?> List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="mt-element-list">
                    <div class="mt-list-container list-news ext-1">
                        <ul>
                            <?php if (HIGHRISE_INTEGRATION_ENABLED) { ?>
                                <li class="mt-list-item">
                                    <div class="list-icon-container">
                                        <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="list-thumb">
                                        <a href="<?php InterfaceAppURL(); ?>/user/integration/highrise/">
                                            <img alt="Highrisehq.com" src="<?php InterfaceTemplateURL(); ?>images/highriseico.gif">
                                        </a>
                                    </div>
                                    <div class="list-datetime bold uppercase font-blue"> Highrise </div>
                                    <div class="list-item-content">                                    
                                        <p><?php InterfaceLanguage('Screen', '1735'); ?></p>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if (PME_USAGE_TYPE == 'User') { ?>
                                <li class="mt-list-item">
                                    <div class="list-icon-container">
                                        <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="list-thumb">
                                        <a href="<?php InterfaceAppURL(); ?>/user/integration/pme/">
                                            <img alt="PreviewMyEmail.com" src="<?php InterfaceTemplateURL(); ?>images/pmeico.gif">
                                        </a>
                                    </div>
                                    <div class="list-datetime bold uppercase font-blue"> PreviewMyEmail </div>
                                    <div class="list-item-content">                                    
                                        <p><?php InterfaceLanguage('Screen', '1719'); ?></p>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if (WUFOO_INTEGRATION_ENABLED) { ?>
                                <li class="mt-list-item">
                                    <div class="list-icon-container">
                                        <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                    <div class="list-thumb">
                                        <a href="<?php InterfaceAppURL(); ?>/user/integration/wufoo/">
                                            <img alt="Wufoo.com" src="<?php InterfaceTemplateURL(); ?>images/wufooico.gif">
                                        </a>
                                    </div>
                                    <div class="list-datetime bold uppercase font-blue"> Wufoo </div>
                                    <div class="list-item-content">                                    
                                        <p><?php InterfaceLanguage('Screen', '1720'); ?></p>
                                    </div>
                                </li>
                            <?php } ?>

                            <?php InterfacePluginIntegrationMenuHook('
                        <li class="mt-list-item">
                            <div class="list-icon-container">
                                <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <div class="list-thumb">
                                <a href="_LINK_">
                                    <img alt="Wufoo.com" src="_ICON_">
                                </a>
                            </div>
                            <div class="list-datetime bold uppercase font-blue"> _TITLE_ </div>
                            <div class="list-item-content">                                    
                                <p>_DESCRIPTION_</p>
                            </div>
                        </li>
                        '); ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceTemplateURL(false); ?>js/screens/user/settings_integration.js" type="text/javascript" charset="utf-8"></script>
