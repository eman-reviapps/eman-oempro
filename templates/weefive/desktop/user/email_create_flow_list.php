<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<div class="col-md-8" style="padding: 0;" >
    <div class="mt-element-list" style="background: #fff">
        <div class="mt-list-head list-news ext-1 font-white bg-grey-gallery">
            <div class="list-head-title-container">
                <h3 class="list-title"><?php InterfaceLanguage('Screen', '0980', false, '', false, false); ?></h3>
            </div>
            <div class="list-count pull-right bg-primary"></div>
        </div>
        <div class="mt-list-container list-news ">
            <ul>
                <?php foreach ($AvailableFlows as $Flow): ?>
                    
                    <li class="mt-list-item">
                        <div class="list-icon-container">
                            <a href="<?php InterfaceAppURL(); ?>/user/email/create/<?php echo $Mode; ?>/<?php echo $EntityID; ?>/<?php echo $Flow->get_id(); ?>">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                        <div class="list-item-content">
                            <h3>
                                <a id="flow_<?php echo $Flow->get_id(); ?>" href="<?php InterfaceAppURL(); ?>/user/email/create/<?php echo $Mode; ?>/<?php echo $EntityID; ?>/<?php echo $Flow->get_id(); ?>">
                                    <?php echo $Flow->get_title(); ?>
                                </a>
                            </h3>
                            <p style="margin: 0"><?php echo $Flow->get_description(); ?></p>
                        </div>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
    <div class="blue">
        <div style="padding:9px 9px 0 9px;">

        </div>
    </div>
</div>
<div class="col-md-4">
    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreateflowlist.php'); ?>
</div>

