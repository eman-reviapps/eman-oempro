<div class="row">
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post"  enctype="multipart/form-data">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1198', false); ?></h4>
                    <?php
                    endif;
                    ?>
                    <div class="form-group no-bg">
                        <p><?php InterfaceLanguage('Screen', '1199', false, '', false, false, array(round(IMPORT_MAX_FILESIZE / 1024))); ?>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('Host') != '' ? 'error' : '')); ?>" id="form-row-Host">
                        <label class="col-md-3 control-label" for="Host"><?php InterfaceLanguage('Screen', '1200', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <input type="text" name="Host" value="<?php echo set_value('Host') ?>" id="Host" class="form-control">
                            <?php print(form_error('Host', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('Port') != '' ? 'error' : '')); ?>" id="form-row-Port">
                        <label class="col-md-3 control-label" for="Port"><?php InterfaceLanguage('Screen', '1201', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <input type="text" name="Port" value="<?php echo set_value('Port', '3306') ?>" id="Port" class="form-control" style="width:100px;">
                            <?php print(form_error('Host', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('Username') != '' ? 'error' : '')); ?>" id="form-row-Username">
                        <label class="col-md-3 control-label" for="Username"><?php InterfaceLanguage('Screen', '1202', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <input type="text" name="Username" value="<?php echo set_value('Username') ?>" id="Username" class="form-control">
                            <?php print(form_error('Username', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('Password') != '' ? 'error' : '')); ?>" id="form-row-Password">
                        <label class="col-md-3 control-label" for="Password"><?php InterfaceLanguage('Screen', '1203', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <input type="password" name="Password" value="<?php echo set_value('Password') ?>" id="Password" class="form-control">
                            <?php print(form_error('Password', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('Database') != '' ? 'error' : '')); ?>" id="form-row-Database">
                        <label class="col-md-3 control-label" for="Database"><?php InterfaceLanguage('Screen', '1204', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <input type="text" name="Database" value="<?php echo set_value('Database') ?>" id="Database" class="form-control">
                            <?php print(form_error('Database', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('SQLQuery') != '' ? 'error' : '')); ?>" id="form-row-SQLQuery">
                        <label class="col-md-3 control-label" for="SQLQuery"><?php InterfaceLanguage('Screen', '1205', false, '', false, true); ?></label>
                        <div class="col-md-6">
                            <textarea name="SQLQuery" id="SQLQuery" rows="5" class="form-control"><?php echo set_value('SQLQuery') ?></textarea>
                            <?php print(form_error('SQLQuery', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersimportmysqlinformation.php'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
    </div>
</div>
