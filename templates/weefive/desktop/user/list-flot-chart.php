<script>
    var ChartsFlotcharts = function () {

        return {
            //main function to initiate the module

            init: function () {

//                App.addResizeHandler(function () {
//                    Charts.initPieCharts();
//                });

            },
            initCharts: function () {

                if (!jQuery.plot) {
                    return;
                }

                function chart3(no_of_days) {
                    if ($('#chart_3').size() != 1) {
                        return;
                    }
                    //tracking curves:

                    var url_list_statistics = "<?php echo InterfaceAppURL() . '/user/list/listSubsUnsubscData/' ?>";
//                    var url_list_unsubscription = "<?php echo InterfaceAppURL() . '/user/list/listSubsUnsubscData/' ?>";


                    var list_id = <?php echo $ListInformation['ListID']; ?>;

//                    var no_of_days = 7;
                    var subscription = null;
                    var unsubscription = null;

                    $.ajax({
                        type: "POST",
                        data: {
                            'list_id': list_id,
                            'days': no_of_days,
                            'type': 'subscriptions',
                        },
                        url: url_list_statistics,
                        success: function (data) {
//                            console.log(data)
                            subscription = JSON.parse(data);
                        },
                        async: false // <- this turns it into synchronous
                    });

                    $.ajax({
                        type: "POST",
                        data: {
                            'list_id': list_id,
                            'days': no_of_days,
                            'type': 'unsubscriptions',
                        },
                        url: url_list_statistics,
                        success: function (data) {
//                            console.log(data)
                            unsubscription = JSON.parse(data);
                        },
                        async: false // <- this turns it into synchronous
                    });

                    var subscription_data = [], unsubscriptions_data = [];
//                    console.log(opens)
//                    console.log(clicks)
//                    console.log(unsubscription)
//                    console.log(forwards)
                    subscription.forEach(function (entry) {
                        subscription_data.push([entry.day, parseInt(entry.TotalSubscriptions)]);

                    });
                    unsubscription.forEach(function (entry) {
                        unsubscriptions_data.push([entry.day, parseInt(entry.TotalUnsubscriptions)]);

                    });

                    draw_chart(no_of_days, 1, 1);

                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 15,
                            border: '1px solid #333',
                            padding: '4px',
                            color: '#fff',
                            'border-radius': '3px',
                            'background-color': '#333',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }

                    var previousPoint = null;
                    $("#chart_3").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));

                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                        y = item.datapoint[1].toFixed(2);

                                var item_pos = Math.round(x);
                                var seriesIndex = item.seriesIndex;
                                var title = null;

                                if (seriesIndex == 0)
                                {
                                    //opens
                                    title = subscription[Math.round(item_pos - 1)].date;
                                } else if (seriesIndex == 1)
                                {
                                    //clicks
                                    title = unsubscription[Math.round(item_pos - 1)].date;
                                } 

                                showTooltip(item.pageX, item.pageY, item.series.label + " of " + title + " = " + y);
                            }

                        } else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });

                    $("#chart_3").bind("plotclick", function (event, pos, item) {
//
//                        if (item) {
//
//                            var item_pos = Math.round(pos.x);
//                            var seriesIndex = item.seriesIndex;
//
//                            console.log(item_pos);
//                            console.log(item);
//
//                            var title = null;
//                            var date = null;
//
//                            if (seriesIndex == 0)
//                            {
//                                //opens
//                                title = 'opens';
//                                date = opens[Math.round(item_pos - 1)].date;
//                            } else if (seriesIndex == 1)
//                            {
//                                //clicks
//                                title = 'clicks';
//                                date = clicks[Math.round(item_pos - 1)].date;
//                            } else if (seriesIndex == 2)
//                            {
//                                //unsubscribtions
//                                title = 'unsubscriptions';
//                                date = unsubscription[Math.round(item_pos - 1)].date;
//                            } else if (seriesIndex == 3)
//                            {
//                                //forwards
//                                title = 'forwards';
//                                date = forwards[Math.round(item_pos - 1)].date;
//                            }
//                            console.log(item.seriesIndex);
////                            console.log(clicks);
//                            console.log(title);
//                            console.log(date);
////                            console.log(clicks[Math.round(item_pos)]);
//
////                            var link = "<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/" + title + "/" + date;
////                            window.open(link, '_blank');
////                            console.log(link);
//                        }
                    });

                    function draw_chart(no_of_days, subscriptions, unsubscriptions)
                    {
                        var all_data = [];
                        if (subscriptions == 1)
                        {
                            all_data.push({
                                data: subscription_data,
                                label: "Subscriptions",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                        if (unsubscriptions == 1)
                        {
                            all_data.push({
                                data: unsubscriptions_data,
                                label: "Unsubscriptions",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                        plot = $.plot($("#chart_3"), all_data, {
                            series: {
                                lines: {
                                    show: true,
                                    lineWidth: 2,
                                    fill: true,
                                    fillColor: {
                                        colors: [{
                                                opacity: 0.05
                                            }, {
                                                opacity: 0.01
                                            }]
                                    }
                                },
                                points: {
                                    show: true,
                                    radius: 3,
                                    lineWidth: 1
                                },
                                shadowSize: 2
                            },
                            crosshair: {
                                mode: "x"
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                autoHighlight: false,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            },
                            colors: ["#D91E18", "#2AB4C0","#8E44AD", "#FF8040","#A52D69","#FF00FF"],
                            yaxis: {
                                min: 0
                            },
                            xaxis: {
                                min: 1,
                                max: no_of_days,
                                ticks: 100,
                                tickDecimals: 0
                            }
                        });

//                        var dataset = plot.getData();
//                        var series = dataset[1];
//                        console.log(series)
//                        console.log(series.label)
//                        console.log(series.data)
//                        var legends = $("#chart_3 .legendLabel");
//                        console.log(legends.eq(1))
//                        legends.eq(1).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
                    }
                    $('input[name="CheckboxGraph"]').change(function () {
                        var subscriptions = $("#CheckboxSubscriptions").prop('checked') ? 1 : 0;
                        var unsubscriptions = $("#CheckboxUnsubscriptions").prop('checked') ? 1 : 0;

                        draw_chart($('#DaysSelect').val(), subscriptions, unsubscriptions);
                    });

                }
                $('#DaysSelect').on('change', function () {
                    $("#CheckboxSubscriptions").prop('checked', true);
                    $("#CheckboxUnsubscriptions").prop('checked', true);
                    $("#CheckboxForwards").prop('checked', true);
                    chart3($(this).val());
                });

                chart3($('#DaysSelect').val());
            },
        };
    }();

    jQuery(document).ready(function () {
        ChartsFlotcharts.init();
        ChartsFlotcharts.initCharts();



    });
</script>

