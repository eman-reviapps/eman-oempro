<div class="row">
    <div class="col-md-8" >
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1794', false); ?></h3>
                        <?php
                        endif;
                        ?>
                        <div class="form-group clearfix <?php print((form_error('EmailAddress') != '' ? 'has-error' : '')); ?>" id="form-row-EmailAddress">
                            <label class="col-md-3 control-label" for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false, '', false, true); ?>: *</label>
                            <div class="col-md-5">
                                <input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress'); ?>" id="EmailAddress" class="form-control"  />
                                <?php print(form_error('EmailAddress', '<span class="help-block">', '</span>')); ?>
                            </div>
                        </div>
                        <?php foreach ($CustomFieldsHtml as $EachField): ?>
                            <div class="form-group clearfix">
                                <label class="col-md-3 control-label"><?php echo $EachField['FieldName']; ?>:<?php echo $EachField['IsRequired'] == 'Yes' ? ' *' : ''; ?></label>
                                <div class="col-md-5">
                                    <?php echo $EachField['html']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="form-group clearfix" id="import-settings">
                            <div class="checkbox-container">
                                <div class="checkbox-row" style="margin-left:0px;">
                                    <input type="checkbox" name="UpdateDuplicates" value="true" id="UpdateDuplicates" checked="checked">
                                    <label for="UpdateDuplicates"><?php InterfaceLanguage('Screen', '1795', false); ?></label>
                                </div>
                                <?php if ($ListInformation['OptInMode'] == 'Double'): ?>
                                    <?php if ($UserInformation['GroupInformation']['ForceOptInList'] == 'Disabled'): ?>
                                        <div class="checkbox-row" style="margin-left:0px;">
                                            <input type="checkbox" name="DoNotSendOptInConfirmationEmail" value="true" id="DoNotSendOptInConfirmationEmail" checked="checked">
                                            <label for="DoNotSendOptInConfirmationEmail"><?php InterfaceLanguage('Screen', '1327', false); ?></label>
                                        </div>
                                    <?php else: ?>
                                        <input type="hidden" name="DoNotSendOptInConfirmationEmail" value="false" id="DoNotSendOptInConfirmationEmail">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <input type="hidden" name="DoNotSendOptInConfirmationEmail" value="true" id="DoNotSendOptInConfirmationEmail">
                                <?php endif; ?>
                                <div class="checkbox-row" style="margin-left:0px;">
                                    <input type="checkbox" name="TriggerBehaviors" value="true" id="TriggerBehaviors">
                                    <label for="TriggerBehaviors"><?php InterfaceLanguage('Screen', '1328', false); ?></label>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
    </div>
</div>