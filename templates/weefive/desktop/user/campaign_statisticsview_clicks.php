<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH.'desktop/user/campaign_header.php'); ?>

<!-- Page - Start -->
<div class="container">
	<div class="span-5 snap-5">
		<?php include_once(TEMPLATE_PATH.'desktop/user/campaign_navigation.php'); ?>
	</div>
	<div class="span-18 last">
		<div id="page-shadow">
			<div id="page" style="min-height:530px">
				<div class="page-bar">
					<h2><?php InterfaceLanguage('Screen', '0897', false, '', false, false); ?></h2>
					<ul class="livetabs right" tabcollection="report-date-tabs" tabcallback="switch_chart_data_range">
						<li class="label"><?php InterfaceLanguage('Screen', '0093', false, '', false); ?></li>
						<li id="tab-7d"><?php InterfaceLanguage('Screen', '0883', false, '', false, false); ?></li>
						<li id="tab-15d"><?php InterfaceLanguage('Screen', '0881', false, '', false, false); ?></li>
						<li id="tab-30d"><?php InterfaceLanguage('Screen', '0882', false, '', false, false); ?></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner flash-chart-container" id="activity-chart-container" data-oempro-chart-options-type="line" data-oempro-chart-options-settings="<?php InterfaceAppURL(); ?>/admin/chart/settings/line" data-oempro-chart-options-data="<?php InterfaceAppURL(); ?>/user/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/clicks-all/" style="height:150px;">
					</div>
					<div class="custom-column-container cols-2 clearfix">
						<div class="col">
							<span class="data big"><?php echo $CampaignInformation['UniqueClicks']; ?></span> <span class="data">(<?php echo $ClickPerformance; ?>%)</span> <span class="data-label big"><?php InterfaceLanguage('Screen', '0876', false, '', false, true); ?></span> / <span class="data small"><?php echo $CampaignInformation['TotalClicks']; ?></span><span class="data-label small"><?php InterfaceLanguage('Screen', '1779', false, '', false, true); ?></span><br />
							<span class="data-label small"><?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?></span> <span class="data"><?php echo $AccountPerformance; ?>%</span> <span class="data small" style="color:#<?php if ($PerformanceDifference > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $PerformanceDifference; ?>%)</span><br />
							<?php if ($CampaignInformation['UniqueClicks'] != 0): ?>
								<span class="data-label small"><?php InterfaceLanguage('Screen', '0880', false, '', false, false); ?> <span class="data"><?php echo $HighestLowestClicks[0][0]; ?></span> 
								<span class="data-label small"><?php InterfaceLanguage('Screen', '0887', false, '', false, false); ?> <span class="data"><?php echo $HighestLowestClicks[count($HighestLowestClicks)-1][0]; ?></span><br />						
							<?php endif; ?>
						</div>
						<div class="col">
						</div>
					</div>
					
				</div>
				<div class="page-bar">
					<ul class="livetabs" tabcollection="report-tabs-2">
						<li id="tab-subscribers-clicked"><a href="#"><?php InterfaceLanguage('Screen', '0896', false, '', false, true); ?></a></li>
						<li id="tab-clicked-links"><a href="#"><?php InterfaceLanguage('Screen', '0894', false, '', false, true); ?></a></li>
					</ul>
				</div>
				<div class="white">
					<div class="inner">
						<div tabcollection="report-tabs-2" id="tab-content-clicked-links">
							<?php
							if (count($ClickedLinks) < 1):
							?>
								<p class="no-data-message"><?php InterfaceLanguage('Screen', '0895', false, '', false); ?></p>
							<?php
							else:
							?>
								<table border="0" cellspacing="0" cellpadding="0" class="small-grid">
									<?php
									foreach ($ClickedLinks as $Index=>$EachLink):
									?>
										<tr>
											<td width="80%"><a href="<?php echo $EachLink['LinkURL']; ?>" target="_blank"><?php echo ($EachLink['LinkTitle'] != '' ? $EachLink['LinkTitle'] : substr($EachLink['LinkURL'], 0, 73).'...'); ?></a></td>
											<td width="20%" class="right-align"><span class="data"><?php echo $EachLink['TotalClicks'] ?></span> <?php InterfaceLanguage('Screen', '0838', false, '', false); ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</table>
							<?php
							endif;
							?>
						</div>
						<div tabcollection="report-tabs-2" id="tab-content-subscribers-clicked">
							<div id="table-container"></div>
							<div class="small-grid-controls">
								<a href="#" id="prev-link">&larr; previous</a>
								<span class="current-page" id="pagination-text"></span>
								<a href="#" id="next-link">next &rarr;</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- Page - End -->

<script>
var statistic_url = '<?php InterfaceAppURL(); ?>/user/campaign/statistics/<?php print($CampaignInformation['CampaignID']) ?>/clicks-all/';
var activity_url = '<?php InterfaceAppURL(); ?>/user/campaign/snippetsubscriberactivitydetailed/<?php print($CampaignInformation['CampaignID']) ?>/';
var CampaignID = <?php print($CampaignInformation['CampaignID']) ?>;
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_statisticsview.js" type="text/javascript" charset="utf-8"></script>		
<script type="text/javascript" charset="utf-8">
	var language = {
		'0962'	: '<?php InterfaceLanguage('Screen', '0962', false, '', false, false); ?>'
	};

	var url = '<?php InterfaceAppURL(); ?>/user/campaign/snippetsubscriberactivity/<?php echo $CampaignInformation['CampaignID'] ?>/clicks/';
	var rpp = 25;
	var total_subscribers = <?php echo $TotalSubscribers ? $TotalSubscribers : 0; ?>;
	var total_pages = Math.floor(total_subscribers / rpp);
	var data_pagination = new Pagination(
		url, 
		$('#table-container'),
		$('#next-link'),
		$('#prev-link'),
		$('#pagination-text'),
		language['0962'],
		rpp,
		total_pages
		);
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>