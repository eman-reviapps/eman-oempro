<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/search.css" rel="stylesheet" type="text/css" />

<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold font-purple-sharp"> 
                Search Results
            </span>                    
        </div>
    </div>
</div>
<div class="page-content-inner">
    <div class="search-page search-content-1">
        <div class="row">
            <?php if (count($Lists) > 0): ?>
                <div class="col-md-4">
                    <div class="portlet light">
                        <div class="portlet-title" style="margin-bottom: 0">
                            <div class="caption">
                                <span class="caption-subject font-purple-sharp sbold">
                                    <?php InterfaceLanguage('Screen', '0541'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body" style="padding-top: 0">
                            <ul style="list-style: none;padding-left: 0">
                                <?php foreach ($Lists as $EachList): ?>
                                    <li class="search-item clearfix">
                                        <div class="search-content">
                                            <h2 class="search-title">
                                                <a href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php print($EachList['ListID']) ?>"><?php print($EachList['Name']); ?></a>
                                            </h2>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>


                </div>
            <?php endif; ?>
            <?php if (count($Campaigns) > 0): ?>
                <div class="col-md-4">
                    <div class="portlet light">
                        <div class="portlet-title" style="margin-bottom: 0">
                            <div class="caption">
                                <span class="caption-subject font-purple-sharp sbold">
                                    <?php InterfaceLanguage('Screen', '0140'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body" style="padding-top: 0">
                            <ul style="list-style: none;padding-left: 0">
                                <?php foreach ($Campaigns as $EachCampaign): ?>
                                    <li class="search-item clearfix">
                                        <div class="search-content">
                                            <h2 class="search-title">
                                                <a href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php print($EachCampaign['CampaignID']) ?>"><?php print($EachCampaign['CampaignName']); ?></a>
                                            </h2>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>


                </div>
            <?php endif; ?>
            <?php if (count($Subscribers) > 0): ?>
                <div class="col-md-4">
                    <div class="portlet light">
                        <div class="portlet-title" style="margin-bottom: 0">
                            <div class="caption">
                                <span class="caption-subject font-purple-sharp sbold">
                                    <?php InterfaceLanguage('Screen', '0104'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body" style="padding-top: 0">
                            <ul style="list-style: none;padding-left: 0">
                                <?php foreach ($Subscribers as $EachSubscriber): ?>
                                    <li class="search-item clearfix">
                                        <div class="search-content">
                                            <h2 class="search-title">
                                                <a href="<?php InterfaceAppURL(); ?>/user/subscriber/edit/<?php print($EachSubscriber['ListID']) ?>/<?php print($EachSubscriber['SubscriberID']) ?>"><?php print($EachSubscriber['EmailAddress']); ?><br><span>(<?php echo $EachSubscriber['ListName']; ?>)</span></a>
                                            </h2>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>


                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>