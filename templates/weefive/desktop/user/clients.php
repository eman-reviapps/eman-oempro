
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="clients-table"><?php InterfaceLanguage('Screen', '0039'); ?></a> 
                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="clients-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="clients-table-form"><?php InterfaceLanguage('Screen', '1081'); ?></a>
                    <!--<i class="icon-microphone font-purple-sharp"></i>-->
                    <!--<span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0572', false, '', false); ?> </span>-->                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >                                                              
                        <a href="<?php InterfaceAppURL(); ?>/user/clients/create/" class="btn default btn-transparen btn-sm"><?php InterfaceLanguage('Screen', '1079'); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <form id="clients-table-form" action="<?php InterfaceAppURL(); ?>/user/clients/" method="post">
                        <input type="hidden" name="Command" value="DeleteClients" id="Command">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <?php
                                if (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                    <?php
                                elseif (isset($PageErrorMessage) == true):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                    <?php
                                endif;
                                ?>


                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="grid table table-bordered" id="clients-table">
                                <tr>
                                    <th colspan="2"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
                                    <th width="300"><?php InterfaceLanguage('Screen', '0758', false, '', true); ?></th>
                                </tr>
                                <?php
                                if (count($Clients) < 1):
                                    ?>
                                    <tr>
                                        <td colspan="2"><?php InterfaceLanguage('Screen', '1082'); ?></td>
                                    </tr>
                                    <?php
                                endif;
                                foreach ($Clients as $EachClient):
                                    ?>
                                    <tr>
                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedClients[]" value="<?php print($EachClient['ClientID']); ?>" id="SelectedClients<?php print($EachClient['ClientID']); ?>"></td>
                                        <td width="300">
                                            <?php if (InterfacePrivilegeCheck('Client.Update', $UserInformation)) : ?>
                                                <a href="<?php InterfaceAppURL(); ?>/user/clients/edit/<?php print($EachClient['ClientID']); ?>"><?php print($EachClient['ClientName']); ?></a>
                                            <?php else: ?>
                                                <?php print($EachClient['ClientName']); ?>
                                            <?php endif; ?>
                                        </td>
                                        <td width="300">
                                            <?php echo $EachClient['ClientEmailAddress']; ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1080': '<?php InterfaceLanguage('Screen', '1080', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/clients.js" type="text/javascript" charset="utf-8"></script>		
