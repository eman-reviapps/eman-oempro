<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', '0969', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="list-form-form" action="<?php InterfaceAppURL(); ?>/user/list/forms/<?php echo $ListInformation['ListID']; ?>" method="post">
                                                <input type="hidden" name="Command" value="GenerateHTMLCode" id="Command">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab-content-subscription" data-toggle="tab">
                                                            <?php InterfaceLanguage('Screen', '1138', false, '', false); ?>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab-content-unsubscription" data-toggle="tab">
                                                            <?php InterfaceLanguage('Screen', '1139', false, '', false); ?>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="tab-content-unsubscription" class="tab-pane">
                                                        <br/>
                                                        <br/>
                                                        <br/>   
                                                        <div class="alert alert-info">
                                                            <?php InterfaceLanguage('Screen', '1141', false); ?>
                                                        </div>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1140'); ?>
                                                        </span>
                                                        <br/>
                                                        <div class="form-group clearfix">
                                                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1142', false, '', false, true); ?></label>
                                                            <div class="col-md-8">
                                                                <textarea class="form-control" rows="10" readonly="readonly"><?php echo $UnsubscriptionHTML; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="tab-content-subscription" class="tab-pane active">
                                                        <br/>
                                                        <br/>
                                                        <br/>   
                                                        <div class="alert alert-info">
                                                            <?php InterfaceLanguage('Screen', '1143', false); ?>
                                                        </div>
                                                        <span class="help-block">
                                                            <p><?php InterfaceLanguage('Screen', '1140'); ?></p>
                                                        </span>
                                                        <br/>
                                                        <div class="clearfix">
                                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                                <ul class="nav nav-tabs tabs-left">
                                                                    <li class="active">
                                                                        <a href="#form-preview-tab" data-toggle="tab"> <?php InterfaceLanguage('Screen', '1714', false); ?> </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#form-settings" data-toggle="tab"> <?php InterfaceLanguage('Screen', '0070', false); ?> </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#form-code" data-toggle="tab"> <?php InterfaceLanguage('Screen', '1142', false); ?> </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                                <div class="tab-content" style="border: none">
                                                                    <div class="tab-pane active" id="form-preview-tab">
                                                                        <?php if (count($FormStyles) > 0): ?>
                                                                            <div id="form-style-chooser">
                                                                                <label class="control-label"><?php InterfaceLanguage('Screen', '1718', false, '', false, false, array()); ?></label><br>
                                                                                <select id="FormStyles" class="form-control">
                                                                                    <?php foreach ($FormStyles as $each): ?>
                                                                                        <option value="<?php echo $each; ?>"><?php echo $each; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                        <div class="clearfix"></div>

                                                                        <div id="form-preview">
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="form-settings">
                                                                        <div class="form-group" id="form-row-Lists">
                                                                            <label class="control-label" for="Lists" ><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?></label>
                                                                            <span class="help-block">
                                                                                <p><?php InterfaceLanguage('Screen', '1438'); ?></p>
                                                                            </span>
                                                                            <select class="form-control" name="Lists[]" id="Lists" multiple style="height:80px;width:200px;">
                                                                                <?php foreach ($Lists as $Each): ?>
                                                                                    <option value="<?php echo $Each['ListID']; ?>"><?php echo $Each['Name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group" id="form-row-CustomFields">
                                                                            <label class="control-label" for="CustomFields"><?php InterfaceLanguage('Screen', '1131', false, '', false, true); ?></label>
                                                                            <?php if ($CustomFields === false): ?>
                                                                                <span class="help-block">
                                                                                    <p><?php InterfaceLanguage('Screen', '1286'); ?></p>
                                                                                </span>
                                                                            <?php else: ?>
                                                                                <span class="help-block" style="margin-bottom:9px;">
                                                                                    <p><?php InterfaceLanguage('Screen', '1144'); ?></p>
                                                                                </span>
                                                                                <select name="CustomFields[]" id="CustomFields" multiple class="form-control" style="height:100px;width:200px">
                                                                                    <?php foreach ($CustomFields as $Each): ?>
                                                                                        <option value="<?php echo $Each['CustomFieldID']; ?>" <?php echo set_select('CustomFields[]', $Each['CustomFieldID']); ?>><?php echo $Each['FieldName']; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <a class="btn default" id="form-button" href="#" ><strong><?php InterfaceLanguage('Screen', '1715', false, '', true); ?></strong></a>

                                                                    </div>
                                                                    <div class="tab-pane fade" id="form-code">
                                                                        <div class="form-group">
                                                                            <textarea class="form-control" id="subscription-html-code" readonly="readonly" style="height:300px;"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_listforms.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var APP_URL = '<?php InterfaceAppURL(); ?>';
    var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
    var STYLE_URL = '<?php InterfaceInstallationURL(); ?>data/form_styles/';
    var LIST_ID = <?php echo $ListInformation['ListID'] ?>;
    var lang = {
        '1308': "<?php InterfaceLanguage('Screen', '1308', false, '', false, false, array()); ?>",
        '1582': "<?php InterfaceLanguage('Screen', '1582', false, '', false, false, array()); ?>",
        '1716': "<?php InterfaceLanguage('Screen', '1716', false, '', false, false, array()); ?>",
        '1717': "<?php InterfaceLanguage('Screen', '1717', false, '', false, false, array()); ?>"
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/jquery.ui.formdrag.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/listform.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>