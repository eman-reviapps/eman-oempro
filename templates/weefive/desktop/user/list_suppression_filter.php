<?php
$AllLists = array();
$AllLists[] = array('value' => '', 'text' => ' -- Select List -- ');
foreach ($Lists as $EachList) {
    if ($EachList['IsAutomationList'] == 'No' && $EachList['ListID'] != $ListID)
        $AllLists[] = array('value' => $EachList['ListID'], 'text' => $EachList['Name']);
}

$keys = array('SuppressionID', 'SuppressionSource', 'EmailAddress', 'PrevBounceType', 'PrevSubscriptionStatus');
?>
<script>
    move_back_to_list = APP_URL + "/user/suppressionlist/move_back_to_list";
    move_to_another_list = APP_URL + "/user/suppressionlist/move_to_another_list";
    move_to_global_supprression = APP_URL + "/user/suppressionlist/move_to_global_supprression";

    var lang_subscribers = {
        '9244': '<?php InterfaceLanguage('Screen', '9244'); ?>',
        '9250': '<?php InterfaceLanguage('Screen', '9250'); ?>',
        '9265': '<?php InterfaceLanguage('Screen', '9265'); ?>',
        '9266': '<?php InterfaceLanguage('Screen', '9266'); ?>',
        '9261': '<?php InterfaceLanguage('Screen', '9261'); ?>',
        '9267': '<?php InterfaceLanguage('Screen', '9267'); ?>',
    };
    var ListsArray = JSON.parse('<?php print_r(json_encode($AllLists)) ?>');
</script>
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-suppression.js" type="text/javascript"></script>

<div class="module-container" style="display:none;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?php
            if (isset($PageSuccessMessage) == true):
                ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageSuccessMessage); ?>
                </div>
                <?php
            elseif (isset($PageErrorMessage) == true):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageErrorMessage); ?>
                </div>
                <?php
            endif;
            ?>
            <div class="grid-operations">
                <div class="inner">

                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover dt-responsive dataTable no-footer dtr-inline" id="sample_1">
    <thead>
        <tr>
            <?php if ($ListID != 0) { ?>
                <th>
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                        <span></span>
                    </label>
                </th>
            <?php } ?>
            <?php
            foreach ($keys as $key) {
//                if (in_array($key, $keys)) {
                ?>
                <th <?php !in_array($key, $keys) ? '' : '' ?> > <?php echo $key ?> </th>
                <?php
//                }
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($Emails) {
            foreach ($Emails as $Email) {
                ?>
                <tr suppressionId = "<?php echo $Email['SuppressionID'] ?>">
                    <?php if ($ListID != 0) { ?>
                        <td class="mt-checkbox-td">
                            <label style="margin-left: 8px" class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkboxes" value="<?php print($Email['SuppressionID']); ?>" />
                                <span></span>
                            </label>
                        </td>
                    <?php } ?>
                    <?php
                    foreach ($Email as $key => $value) {
                        if (in_array($key, $keys)) {
                            ?>
                            <td> <?php echo $value ?> </td>
                            <?php
                        }
                    }
                    ?>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
