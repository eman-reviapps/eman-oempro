
<script>
    var ChartsFlotcharts = function () {

        return {
            //main function to initiate the module

            init: function () {

                App.addResizeHandler(function () {
                    Charts.initPieCharts();
                });
            },
            initCharts: function () {

                if (!jQuery.plot) {
                    return;
                }

                function chart5(no_of_months) {
                    if ($('#chart_5').size() != 1) {
                        return;
                    }
                    //moment().month(i+1).format("MMMM")
                    var url_opens = "<?php echo InterfaceAppURL() . '/user/overview/OpenStatistics/' ?>";
                    var url_subscribes = "<?php echo InterfaceAppURL() . '/user/overview/SubscribeStatistics/' ?>";
                    var url_clicks = "<?php echo InterfaceAppURL() . '/user/overview/ClickStatistics/' ?>";

                    var user_id = <?php echo $UserInformation['UserID']; ?>;

                    var opens = null;
                    var clicks = null;
                    var subscribes = null;

                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months
                        },
                        url: url_opens,
                        success: function (data) {
                            opens = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });


                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months
                        },
                        url: url_subscribes,
                        success: function (data) {
                            subscribes = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });
//                    alert(subscribes)

                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months
                        },
                        url: url_clicks,
                        success: function (data) {
                            clicks = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });

                    var open_data = [], click_data = [], subscribes_data = [];
//                    console.log(opens)
//                    console.log(clicks)
//                    console.log(subscribes)

                    opens.forEach(function (entry) {
                        open_data.push([entry.MonthName, parseInt(entry.TotalOpens)]);

                    });
                    clicks.forEach(function (entry) {
                        click_data.push([entry.MonthName, parseInt(entry.TotalClicks)]);

                    });
                    subscribes.forEach(function (entry) {
                        subscribes_data.push([entry.MonthName, parseInt(entry.TotalSubscriptions)]);

                    });

                    draw_chart(no_of_months, 1, 1, 1);

                    $('input[name="CheckboxGraph"]').change(function () {
                        var opens = $("#CheckboxOpens").prop('checked') ? 1 : 0;
                        var clicks = $("#CheckboxClicks").prop('checked') ? 1 : 0;
                        var subscriptions = $("#CheckboxSubscriptions").prop('checked') ? 1 : 0;

                        draw_chart($('#MonthsSelect').val(), opens, clicks, subscriptions);
                    });

                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 15,
                            border: '1px solid #333',
                            padding: '4px',
                            color: '#fff',
                            'border-radius': '3px',
                            'background-color': '#333',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }

                    var previousPoint = null;
                    $("#chart_5").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));

                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                        y = item.datapoint[1].toFixed(2);

                                var item_pos = Math.round(x);

                                var title = opens[Math.round(item_pos)].MonthName + " : ";
                                if ($("#CheckboxOpens").prop('checked'))
                                    title += " <?php InterfaceLanguage('Screen', '0837') ?> = " + opens[Math.round(item_pos)].TotalOpens ;
                                if ($("#CheckboxClicks").prop('checked'))
                                    title += " <?php InterfaceLanguage('Screen', '0838') ?> = " + clicks[Math.round(item_pos)].TotalClicks;
                                if ($("#CheckboxSubscriptions").prop('checked'))
                                    title += " <?php InterfaceLanguage('Screen', '9214') ?> = " + subscribes[Math.round(item_pos)].TotalSubscriptions;


//                                showTooltip(item.pageX, item.pageY, item.series.label + " of " + title + " = " + y);
                                showTooltip(item.pageX, item.pageY, title);
                            }

                        } else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });


                    function draw_chart(no_of_days, opens, clicks, subscriptions)
                    {
                        var all_data = [];
                        if (opens == 1)
                        {
                            all_data.push({
                                data: open_data,
                                label: "<?php InterfaceLanguage('Screen', '0837') ?>",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                        if (clicks == 1)
                        {
                            all_data.push({
                                data: click_data,
                                label: "<?php InterfaceLanguage('Screen', '0838') ?>",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                        if (subscriptions == 1)
                        {
                            all_data.push({
                                data: subscribes_data,
                                label: "<?php InterfaceLanguage('Screen', '9214') ?>",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                        plot = $.plot($("#chart_5"), all_data, {
                            series: {
                                lines: {
                                    show: true,
                                    lineWidth: 2,
                                    fill: true,
                                    fillColor: {
                                        colors: [{
                                                opacity: 0.05
                                            }, {
                                                opacity: 0.01
                                            }]
                                    }
                                },
                                points: {
                                    show: true,
                                    radius: 3,
                                    lineWidth: 1
                                },
                                shadowSize: 2
                            },
                            crosshair: {
                                mode: "x"
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                autoHighlight: false,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            },
                            colors: ["#D91E18", "#2AB4C0", "#8E44AD", "#FF8040", "#A52D69", "#FF00FF"],
                            yaxis: {
                                min: 0
                            },
                            xaxis: {
                                mode: "categories",
                            }
                        });
                    }
                }

                $('#MonthsSelect').on('change', function () {
                    $("#CheckboxOpens").prop('checked', true);
                    $("#CheckboxClicks").prop('checked', true);
                    $("#CheckboxSubscriptions").prop('checked', true);
                    chart5($(this).val());
                });

                //graph
                chart5($('#MonthsSelect').val());
            }
        };
    }();
    jQuery(document).ready(function () {
//        ChartsFlotcharts.init();
        ChartsFlotcharts.initCharts();
    });
</script>

