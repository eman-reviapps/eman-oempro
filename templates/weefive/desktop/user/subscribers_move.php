<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>
<?php
//echo $ActiveListItem;
?>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', '9254', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="subscribers-move" action="<?php InterfaceAppURL(); ?>/user/subscribers/move/<?php echo $ListInformation['ListID']; ?>" method="post">
                                                <input type="hidden" name="Command" value="MoveSubscribers" id="Command">
                                                <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div><?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('Name') != '' ? 'error' : '')); ?>" id="form-row-Name">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0940', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <input type="text"value="<?php echo $ListInformation['Name']; ?>" class="form-control" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-Subscribers">
                                                    <label class="col-md-3 control-label" for="Subscribers"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <select name="Subscribers" id="Subscribers" class="form-control">
                                                            <option value="Active" <?php echo set_select('Subscribers', 'Active'); ?>><?php InterfaceLanguage('Screen', '1109'); ?></option>
                                                            <!--<option value="Suppressed" <?php echo set_select('Subscribers', 'Suppressed'); ?>><?php InterfaceLanguage('Screen', '1110'); ?></option>-->
                                                            <option value="Not opted in for days" <?php echo set_select('Subscribers', 'Not opted in for days'); ?>><?php InterfaceLanguage('Screen', '1114'); ?></option>
                                                            <option value="Copy and paste" <?php echo set_select('Subscribers', 'Copy and paste'); ?>><?php InterfaceLanguage('Screen', '1115'); ?></option>
                                                            <optgroup label="<?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?>">
                                                                <?php foreach ($Segments as $EachSegment): ?>
                                                                    <option value="<?php echo $EachSegment['SegmentID']; ?>" <?php echo set_select('Subscribers', $EachSegment['SegmentID']); ?>><?php echo $EachSegment['SegmentName']; ?></option>
                                                                <?php endforeach; ?>
                                                            </optgroup>
                                                            <optgroup label="<?php InterfaceLanguage('Screen', '1111', false, '', false, true); ?>">
                                                                <option value="Soft bounced" <?php echo set_select('Subscribers', 'Soft bounced'); ?>><?php InterfaceLanguage('Screen', '1112'); ?></option>
                                                                <!--<option value="Hard bounced" <?php echo set_select('Subscribers', 'Hard bounced'); ?>><?php InterfaceLanguage('Screen', '1113'); ?></option>-->
                                                            </optgroup>
                                                        </select> 
                                                    </div>

                                                    <span id="opt-in-days" style="display:none">
                                                        <div class="col-md-2">
                                                            <input type="text" name="NotOptedInForDays" id="NotOptedInForDays" class="form-control" />                                     
                                                            <?php InterfaceLanguage('Screen', '0851'); ?>
                                                        </div>
                                                    </span>
                                                </div>
                                                <div class="form-group clearfix" style="margin-bottom: 0">
                                                    <div class="col-md-9">
                                                        <div class="form-group-note" id="combo-description">
                                                            <p style="display:none" id="message-Active"><?php InterfaceLanguage('Screen', '1116', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Suppressed"><?php InterfaceLanguage('Screen', '1117', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Not-opted-in-for-days"><?php InterfaceLanguage('Screen', '1118', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Copy-and-paste"><?php InterfaceLanguage('Screen', '1119', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Soft-bounced"><?php InterfaceLanguage('Screen', '1120', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Hard-bounced"><?php InterfaceLanguage('Screen', '1121', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Segments"><?php InterfaceLanguage('Screen', '1122', false, '', false, false); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" style="margin-bottom: 0">
                                                    <span id="copy-and-paste" style="display:none;" >
                                                        <div class="col-md-3">

                                                        </div>
                                                        <div class="col-md-6" style="padding-left: 0">
                                                            <textarea name="CopyAndPaste" id="CopyAndPaste" rows="5" class="form-control" ></textarea>
                                                        </div>
                                                    </span>

                                                </div>

                                                <div class="form-group clearfix" id="form-row-Move" style="margin-bottom: 0">
                                                    <div class="col-md-8">
                                                        <div class="mt-radio-list">
                                                            <label class="mt-radio mt-radio-outline"> <?php InterfaceLanguage('Screen', '9243', false, '', false, false); ?>
                                                                <input type="radio" value="1" checked="" name="move_to_option">
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio mt-radio-outline"> <?php InterfaceLanguage('Screen', '9250', false, '', false, false); ?>
                                                                <input type="radio" value="2" name="move_to_option">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-lists" >
                                                    <label class="col-md-3 control-label" for="Subscribers"><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <select name="Lists" id="Lists" class="form-control">
                                                            <option value=""> -- <?php InterfaceLanguage('Screen', '0992', false, '', false, true); ?> -- </option>
                                                            <?php
                                                            foreach ($Lists as $EachList):
                                                                if ($EachList['IsAutomationList'] == 'No' && $EachList['ListID'] != $ListInformation['ListID']) {
                                                                    ?>
                                                                    <option value="<?php echo $EachList['ListID']; ?>"><?php echo $EachList['Name']; ?></option>
                                                                    <?php
                                                                }
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group" id="form-row-notice" style="margin-bottom: 0">
                                                    <div class="col-md-12">
                                                        <div class="alert alert-info">
                                                            <strong><?php InterfaceLanguage('Screen', '9255', false, '', false, false); ?></strong> 
                                                            <br/>
                                                            <br/>
<?php InterfaceLanguage('Screen', '1124', false, '', false, false); ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <a class="btn default" targetform="subscribers-move" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '9254', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Page - END -->

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers_move.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>