<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>


<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-close font-purple-sharp"></i>
                        <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '9194', false, '', false, true); ?></span>                    
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="page-bar">
                        <h2><?php InterfaceLanguage('Screen', '0136', false, '', false, false); ?></h2>
                    </div>
                        <form id="campaign-delete-form" method="post" action="<?php InterfaceAppURL(); ?>/user/campaign/delete/<?php echo $CampaignInformation['CampaignID']; ?>">
                            <input type="hidden" name="CampaignDeleteConfirm" value="true" id="CampaignDeleteConfirm">
                            <div class="form-row no-bg" style="margin-top:0px;padding-top:15px;">
                                <p style="margin-bottom:0px;"><?php InterfaceLanguage('Screen', '0912', false, '', false); ?></p>
                            </div>
                            <div class="form-row no-bg">
                                <div class="form-action-container clearfix left" style="margin:0px;padding:0px;">
                                    <a class="btn default" href="#" targetform="campaign-delete-form"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0913', false, '', true); ?></strong></a>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>