<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-doc font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0934', false, '', false, true); ?> </span>                    
                </div>
            </div>
            <div class="portlet-body form">
                <form id="list-create" class="form-inputs" action="<?php InterfaceAppURL(); ?>/user/list/create/" method="post">
                    <input type="hidden" name="Command" value="Create" id="Command">
                    <?php if ($UserInformation['GroupInformation']['ForceOptInList'] == 'Enabled'): ?>
                        <input type="hidden" name="OptInMode" value="Double" id="OptInMode">
                    <?php endif; ?>

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>>
                    <?php else: ?>
                        <div class="alert alert-info alert-info-custom">
                            <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                        </div>
                    <?php
                    endif;
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Name') != '' ? 'has-error' : '')); ?>" id="form-row-Name">
                                <label class="col-md-3 control-label" for="Name"><?php InterfaceLanguage('Screen', '0940', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="Name" value="<?php echo set_value('Name'); ?>" id="Name" class="form-control"  />
                                    <?php print(form_error('Name', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($UserInformation['GroupInformation']['ForceOptInList'] == 'Disabled'): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="form-row-OptInMode">
                                    <label class="col-md-3 control-label" for="OptInMode"><?php InterfaceLanguage('Screen', '0941', false, '', false, true); ?>:</label>
                                    <div class="col-md-6">
                                        <select name="OptInMode" id="OptInMode" class="form-control">
                                            <option value="Double" <?php echo set_select('OptInMode', 'Double'); ?>><?php InterfaceLanguage('Screen', '0942', false, '', false, false); ?></option>
                                            <option value="Single" <?php echo set_select('OptInMode', 'Single'); ?>><?php InterfaceLanguage('Screen', '0943', false, '', false, false); ?></option>
                                        </select>
                                        <span class="help-block"><?php InterfaceLanguage('Screen', '0944', false, '', false, false); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="form-row-HideInSubscriberArea">
                                <label>&nbsp;</label>
                                <div class="checkbox-container">
                                    <div class="checkbox-row" style="margin-left:0px;">
                                        <input type="checkbox" name="HideInSubscriberArea" value="true" id="HideInSubscriberArea" <?php echo set_checkbox('HideInSubscriberArea', 'true'); ?>>
                                        <label for="HideInSubscriberArea"><?php InterfaceLanguage('Screen', '0945', false, '', false, false); ?></label>
                                    </div>
                                </div>
                                <div class="form-group-note checkbox-note">
                                    <p><?php InterfaceLanguage('Screen', '0946', false, '', false, false); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span-18 last">
                        <div class="form-action-container">

                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <a class="btn default" targetform="list-create" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '0948', false, '', true); ?></strong></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_listcreate.php'); ?>
    </div>
</div>


<script type="text/javascript" charset="utf-8">
    var Language = {
        '0947': '<?php InterfaceLanguage('Screen', '0947', false, '', true, false); ?>',
        '0948': '<?php InterfaceLanguage('Screen', '0948', false, '', true, false); ?>'
    };
</script>

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/list_create.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>