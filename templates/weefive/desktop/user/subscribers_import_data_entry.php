<div class="row">
    <div class="col-md-8" >
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1281', false); ?></h4>
                    <?php
                    endif;
                    ?>
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <p><span class="help-block"><?php InterfaceLanguage('Screen', '1179'); ?></span></p>
                            <p><span class="help-block"><?php InterfaceLanguage('Screen', '1180'); ?></span></p>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('CopyAndPasteData') != '' ? 'has-error' : '')); ?>">
                        <div class="col-md-12">
                            <textarea name="CopyAndPasteData" id="CopyAndPasteData" class="form-control" rows="5"></textarea>
                            <?php print(form_error('CopyAndPasteData', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>

                    <div class="form-group clearfix <?php print((form_error('FieldTerminator') != '' ? 'has-error' : '')); ?>" id="form-row-FieldTerminator">
                        <label class="col-md-3 control-label" for="FieldTerminator" style="width:190px;"><?php InterfaceLanguage('Screen', '1636', false); ?>: *</label>
                        <div class="col-md-3">
                            <select name="FieldTerminator" id="FieldTerminator" class="form-control">
                                <option value="," <?php echo set_select('FieldTerminator', ','); ?>><?php InterfaceLanguage('Screen', '1638', false, '', false, false); ?></option>
                                <option value=";" <?php echo set_select('FieldTerminator', ';'); ?>><?php InterfaceLanguage('Screen', '1639', false, '', false, false); ?></option>
                                <option value="\t" <?php echo set_select('FieldTerminator', '\t'); ?>><?php InterfaceLanguage('Screen', '1640', false, '', false, false); ?></option>
                            </select>
                            <?php print(form_error('FieldTerminator', '<span class="help-block">', '</span>')); ?>
                        </div>
                        <div class="col-md-5">
                            <span class="help-block"><?php InterfaceLanguage('Screen', '1637', false, '', false); ?></span>
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('FieldEncloser') != '' ? 'has-error' : '')); ?>" id="form-row-FieldEncloser">
                        <label class="col-md-3 control-label"  for="FieldEncloser" style="width:190px;"><?php InterfaceLanguage('Screen', '1641', false); ?>: *</label>
                        <div class="col-md-3">
                            <select name="FieldEncloser" id="FieldEncloser" class="form-control">
                                <option value="" <?php echo set_select('FieldEncloser', ''); ?>><?php InterfaceLanguage('Screen', '1644', false, '', false, false); ?></option>
                                <option value="'" <?php echo set_select('FieldEncloser', '\''); ?>><?php InterfaceLanguage('Screen', '1642', false, '', false, false); ?></option>
                                <option value="&quot;" <?php echo set_select('FieldEncloser', '"'); ?>><?php InterfaceLanguage('Screen', '1643', false, '', false, false); ?></option>
                            </select>
                            <?php print(form_error('FieldEncloser', '<span class="help-block">', '</span>')); ?>
                        </div>
                        <div class="col-md-5">
                            <span class="help-block"><?php InterfaceLanguage('Screen', '1645', false, '', false); ?></span>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersimportdataentry.php'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
    </div>
</div>
