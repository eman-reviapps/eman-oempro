<div class="row">
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php if ($Campaigns === FALSE || $Tags === FALSE): ?>
                        <h3 class="form-legend error"><?php InterfaceLanguage('Screen', '1746', false); ?></h3>
                    <?php else: ?>
                        <?php if (isset($PageErrorMessage) == true): ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php print($PageErrorMessage); ?>
                            </div>
                        <?php else: ?>
                            <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1745', false); ?></h4>
                        <?php endif; ?>
                        <div class="form-group no-bg">
                            <p><?php InterfaceLanguage('Screen', '1744', false); ?></p>
                        </div>
                        <div class="form-group clearfix" id="form-row-ContactSelection">
                            <div class="col-md-6">
                                <select name="ContactSelection" id="ContactSelection" class="form-control">
                                    <option value="all"><?php InterfaceLanguage('Screen', '9001', false); ?></option>
                                    <optgroup label="Companies">
                                        <?php foreach ($Companies as $each): ?>
                                            <option value="c<?php echo $each['id']; ?>"><?php echo $each['name']; ?></option>
                                        <?php endforeach; ?>
                                    </optgroup>
                                    <optgroup label="Tags">
                                        <?php foreach ($Tags as $each): ?>
                                            <option value="t<?php echo $each['id']; ?>"><?php echo $each['name']; ?></option>
                                        <?php endforeach; ?>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
    </div>
</div>


