<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>


<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="auto-responders-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="auto-responders-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="auto-responders-table-form"><?php InterfaceLanguage('Screen', '0993'); ?></a>
                                    <input type="hidden" name="Command" value="DeleteAutoResponders" id="Command">
<!--                                    <i class="icon-microphone font-purple-sharp"></i>
                                <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0557', false, '', false, true); ?></span>                    -->
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" >    
                                        <?php if (InterfacePrivilegeCheck('AutoResponder.Create', $UserInformation)): ?>
                                            <a  href="<?php InterfaceAppURL(); ?>/user/autoresponders/copy/<?php echo $ListID; ?>" class="btn default"><?php InterfaceLanguage('Screen', '0997'); ?></a>
                                        <?php endif; ?>
                                        <?php if (InterfacePrivilegeCheck('AutoResponder.Create', $UserInformation)): ?>
                                            <a href="<?php InterfaceAppURL(); ?>/user/autoresponders/create/<?php echo $ListID; ?>" class="btn default"><?php InterfaceLanguage('Screen', '0996', false, '', false, true); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <?php
                                    if (isset($PageSuccessMessage) == true):
                                        ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageSuccessMessage); ?>
                                        </div>
                                        <?php
                                    elseif (isset($PageErrorMessage) == true):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageErrorMessage); ?>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                </div>
                                <form id="auto-responders-table-form" action="<?php InterfaceAppURL(); ?>/user/autoresponders/browse/<?php echo $ListID; ?>" method="post">
                                    <input type="hidden" name="Command" value="DeleteAutoResponders" id="Command">
                                    <table class="grid table table-bordered" id="auto-responders-table">
                                        <?php if ($AutoResponders === false): ?>
                                            <tr>
                                                <td><?php InterfaceLanguage('Screen', '0994', false, '', false, false); ?></td>
                                            </tr>
                                        <?php else: ?>
                                            <?php
                                            foreach ($AutoResponders as $EachAutoResponder):
                                                ?>
                                                <tr>
                                                    <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedAutoResponders[]" value="<?php print($EachAutoResponder['AutoResponderID']); ?>" id="SelectedAutoResponders<?php print($EachAutoResponder['AutoResponderID']); ?>"></td>
                                                    <td width="485">
                                                        <?php if (InterfacePrivilegeCheck('AutoResponder.Get', $UserInformation)): ?>
                                                            <a href="<?php InterfaceAppURL(); ?>/user/autoresponders/edit/<?php echo $ListID; ?>/<?php print($EachAutoResponder['AutoResponderID']); ?>"><?php print($EachAutoResponder['AutoResponderName']); ?></a>
                                                        <?php else: ?>
                                                            <?php print($EachAutoResponder['AutoResponderName']); ?>
                                                        <?php endif; ?>
                                                        <br />
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0837', false, '', false, false, array()); ?></span> <span class="data small"><?php echo $EachAutoResponder['UniqueOpens']; ?></span>
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0838', false, '', false, false, array()); ?></span> <span class="data small"><?php echo $EachAutoResponder['UniqueClicks']; ?></span>
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0839', false, '', false, false, array()); ?></span> <span class="data small"><?php echo $EachAutoResponder['UniqueForwards']; ?></span>
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0840', false, '', false, false, array()); ?></span> <span class="data small"><?php echo $EachAutoResponder['UniqueViewsOnBrowser']; ?></span>
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0841', false, '', false, false, array()); ?></span> <span class="data small"><?php echo $EachAutoResponder['TotalUnsubscriptions']; ?></span>
                                                        <br />
                                                        <span class="data-label small"><?php InterfaceLanguage('Screen', '0995', false, $EachAutoResponder['AutoResponderTriggerType'], false, false, array(InterfaceLanguage('Screen', '1001', true, $EachAutoResponder['TriggerTimeType'], false, false, array($EachAutoResponder['TriggerTime'])))); ?></span>
                                                        <?php if ($EachAutoResponder['RelEmailID'] == 0): ?>
                                                            <br /><span class="small"><?php InterfaceLanguage('Screen', '1215'); ?></span>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1002': '<?php InterfaceLanguage('Screen', '1002', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/autoresponders.js" type="text/javascript" charset="utf-8"></script>		


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>