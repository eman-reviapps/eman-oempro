<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_email_content_builder_header.php'); ?>

<div id="top" class="iguana">
	<div class="container">
		<div class="span-14">
			<ul class="tabs">
				<li class="selected">
					<a href="#">
						<span class="left">&nbsp;</span>
						<span class="right">&nbsp;</span>
						<strong><?php InterfaceLanguage('Screen', '0801', false, '', true, false, array()); ?></strong>
					</a>
				</li>
			</ul>
		</div>
		<div class="span-10 last iguana-actions">
			<?php if ($CampaignID == ''): ?>
				<a href="<?php InterfaceAppURL(); ?>/user/campaigns/create/<?php echo $Flow ?>" class="text-action" style="margin-left:18px;"><?php InterfaceLanguage('Screen', '0964', false, '', false, true); ?></a>
			<?php else: ?>
				<a href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php echo $CampaignID; ?>" class="text-action" style="margin-left:18px;"><?php InterfaceLanguage('Screen', '0964', false, '', false, true); ?></a>
			<?php endif; ?>
			<a id="content-save-button" class="button" href="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/" targetform="save-content-form"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true, false); ?></strong></a>
		</div>
	</div>
</div>

<div id="middle" class="iguana" style="padding-top:9px;">
	<div class="container">
		<div class="span-24 last">
			<iframe name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
		</div>
	</div>
</div>

<div class="panel-container">
	<div class="panel-inner">

		<div id="single-editable-panel" style="display:none">
			<div class="properties-panel">
				<input type="text" name="single-editable-text-input" value="" id="single-editable-text-input" class="text" style="width:960px" />
			</div>
		</div>
		<div id="plain-editable-panel" style="display:none">
			<div class="properties-panel">
				<textarea name="plain-editable-text-input" id="plain-editable-text-input" class="textarea" style="width:960px"></textarea>
			</div>
		</div>
		<div id="rich-editable-panel" style="display:none">
			<div class="properties-panel">
				<textarea name="rich-editable-text-input" id="rich-editable-text-input" class="textarea" style="width:960px;height:250px;"></textarea>
			</div>
		</div>
		<div class="element-panel" style="position:static">
			<p style="margin-top:9px;">
				<a href="#" id="hide-panel-button" class="last" style="margin-left:0px;"><?php InterfaceLanguage('Screen', '0500', false); ?></a>
			</p>
		</div>
	</div>

</div>

<form id="save-content-form" action="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/save/<?php echo $CampaignID; ?>" method="post">
	<textarea id="frame-html-container-for-post" name="EmailHTMLContent" style="display:none"></textarea>
</form>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_email_content_builder_footer.php'); ?>
