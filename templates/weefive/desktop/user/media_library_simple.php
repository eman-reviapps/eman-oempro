<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php print(CHARSET); ?>"/>

        <title><?php print($PageTitle); ?></title>

        <!-- BLUEPRINT CSS FRAMEWORK - START -->
        <link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/screen.css" type="text/css" media="screen, projection">
        <!--[if IE]><link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
            <!-- BLUEPRINT CSS FRAMEWORK - END -->

            <link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/user/css" type="text/css" media="screen, projection">

                <script src="<?php InterfaceTemplateURL(); ?>js/jquery.js" type="text/javascript" charset="utf-8"></script>
                <script src="<?php InterfaceTemplateURL(); ?>js/template.js" type="text/javascript" charset="utf-8"></script>
                <?php if ($RemoveTinyMCE == false): ?>
                    <script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce_3432/tiny_mce_popup.js" type="text/javascript" charset="utf-8"></script>
                <?php endif; ?>
                <script>
                    var APP_URL = '<?php InterfaceAppURL(); ?>/';
                    var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
                    var TEMPLATE_URL = '<?php InterfaceTemplateURL(); ?>';
                </script>
                </head>

                <body class="popup">

                    <div id="browser-upgrade">
                        <p><?php InterfaceLanguage('Screen', '0624', false, '', false, false); ?></p>
                    </div>

                    <div class="module-container">
                        <?php if ($Message != '' && $Message == 'upload_success'): ?>
                            <div class="page-message success">
                                <div class="inner">
                                    <span class="close">X</span>
                                    <?php InterfaceLanguage('Screen', '1876'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($Message != '' && $Message == 'folder_create'): ?>
                            <div class="page-message success">
                                <div class="inner">
                                    <span class="close">X</span>
                                    <?php InterfaceLanguage('Screen', '1881'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($Message != '' && $Message == 'folder_delete'): ?>
                            <div class="page-message success">
                                <div class="inner">
                                    <span class="close">X</span>
                                    <?php InterfaceLanguage('Screen', '1882'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($Message != '' && $Message == 'upload_error'): ?>
                            <div class="page-message error">
                                <div class="inner">
                                    <span class="close">X</span>
                                    <?php InterfaceLanguage('Screen', '1848'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <p style="line-height:24px;padding:0 18px;">
                            <?php for ($i = count($Breadcrumb) - 1; $i >= 0; $i--): ?>
                                <?php if ($i == 0): ?>
                                    / <?php echo $Breadcrumb[$i][1]; ?>
                                <?php else: ?>
                                    / <a href="#" class="folder" folderid="<?php echo $Breadcrumb[$i][0]; ?>"><?php echo $Breadcrumb[$i][1]; ?></a>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </p>
                    </div>
                    <div id="upload-form-container" class="clearfix">
                        <form target="media-upload-frame" id="media-upload-form" name="media-upload-form" action=" 	<?php InterfaceAppURL(); ?>/user/medialibrary/upload" method="post" enctype="multipart/form-data" style="margin:0;padding:0;">
                            <input type="hidden" name="ParentFolderID" value="<?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getId(); ?>">
                                <p style="padding:9px 18px 0 18px;margin:0;">
                                    <a href="#" id="create-folder-link"><?php InterfaceLanguage('Screen', '1877'); ?></a>
                                    <?php if ($CurrentFolder != false): ?>
                                        <a href="#" id="delete-folder-link" style="margin-left:9px;"><?php InterfaceLanguage('Screen', '1878'); ?></a>
                                    <?php endif; ?>
                                </p>
                                <p style="padding:9px 18px;margin:0;">
                                    Upload file: <input type="file" name="new-media-file" value="" id="new-media-file">
                                </p>
                        </form>
                        <p id="loading-indicator" style="display:none;margin:18px 0 0 18px;"><?php InterfaceLanguage('Screen', '1058'); ?></p>
                        <iframe name="media-upload-frame" id="media-upload-frame" style="height:1px;width:1px;display:none"></iframe>
                    </div>
                    <div class="email-template-gallery-container clearfix">
                        <?php if ($Files !== false && count($Files) > 0): ?>

                            <?php
                            foreach ($Files as $key => $file):
                                $uri = AWS_END_POINT . S3_BUCKET . "/" . $file['name'];
                                ?>
                                <div class="email-template small media" mediaurl="<?php echo $uri; ?>" id="file-<?php echo $file['name']; ?>">
                                    <div class="image">
                                        <img crossOrigin="anonymous"  src="<?php echo $uri; ?>" width="100" height="100" />
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>


                    <script type="text/javascript" charset="utf-8">
                        var current_folder = <?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getId(); ?>;
                        var parent_folder = <?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getFolderId(); ?>;
                        var language_object = {
                            '1055': '<?php InterfaceLanguage('Screen', '1055'); ?>', '1880': '<?php InterfaceLanguage('Screen', '1880'); ?>',
                            '1883': '<?php InterfaceLanguage('Screen', '1883'); ?>'
                        };
                        var media_library_url = '<?php InterfaceAppURL(); ?>/user/medialibrary/browse_simple/';

<?php if ($RemoveTinyMCE == false): ?>
                            var win = tinyMCEPopup.getWindowArg("window");
                            var input = tinyMCEPopup.getWindowArg("input");
                            var res = tinyMCEPopup.getWindowArg("resizable");
                            var inline = tinyMCEPopup.getWindowArg("inline");
                            var id = tinyMCEPopup.getWindowArg("editor_id");
<?php endif; ?>
                    </script>

                    <script src="<?php InterfaceTemplateURL(); ?>js/screens/user/media_library.js" type="text/javascript" charset="utf-8"></script>

                    <script type="text/javascript" charset="utf-8">
                        $('.media .image').click(function () {
                            media_library.set_media($(this).parent().attr('mediaurl'));
                            return false;
                        });

<?php if ($RemoveTinyMCE == false): ?>
                            media_library.set_media = function (URL) {
                                if (input == 'src') {
                                    win.document.getElementById(input).value = URL;
                                    win.ImageDialog.showPreviewImage(URL);
                                } else {
                                    win.document.getElementById(input).value = URL;
                                }
                                tinyMCEPopup.close();
                            };
<?php endif; ?>
                    </script>


                </body>
                </html>
