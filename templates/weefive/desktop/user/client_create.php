<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/settings_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1096', false, '', false, true); ?> </span>                    
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="client-create" class="form-inputs" action="<?php InterfaceAppURL(); ?>/user/clients/create/" method="post">
                                                <input type="hidden" name="Command" value="CreateClient" id="Command">
                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <h4 class="form-section bold font-blue" style="margin-top: 10px !important">
                                                        <?php InterfaceLanguage('Screen', '0037', false); ?>
                                                    </h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="row" >
                                                    <div class="col-md-12" style="padding-left: 0">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group <?php print((form_error('ClientName') != '' ? 'has-error' : '')); ?>" id="form-row-ClientName">
                                                                    <label class="col-md-3 control-label" for="ClientName"><?php InterfaceLanguage('Screen', '1097', false, '', false, true); ?>: *</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="ClientName" value="<?php echo set_value('ClientName'); ?>" id="ClientName" class="form-control" />
                                                                        <?php print(form_error('ClientName', '<span class="help-block">', '</span>')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group <?php print((form_error('ClientEmailAddress') != '' ? 'has-error' : '')); ?>" id="form-row-ClientEmailAddress">
                                                                    <label class="col-md-3 control-label" for="ClientEmailAddress"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="ClientEmailAddress" value="<?php echo set_value('ClientEmailAddress'); ?>" id="ClientEmailAddress" class="form-control" />
                                                                        <?php print(form_error('ClientEmailAddress', '<span class="help-block">', '</span>')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group <?php print((form_error('ClientUsername') != '' ? 'has-error' : '')); ?>" id="form-row-ClientUsername">
                                                                    <label class="col-md-3 control-label" for="ClientUsername"><?php InterfaceLanguage('Screen', '0002', false, '', false, true); ?>: *</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="ClientUsername" value="<?php echo set_value('ClientUsername'); ?>" id="ClientUsername" class="form-control" />
                                                                        <?php print(form_error('ClientUsername', '<span class="help-block">', '</span>')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group <?php print((form_error('ClientPassword') != '' ? 'has-error' : '')); ?>" id="form-row-ClientPassword">
                                                                    <label class="col-md-3 control-label" for="ClientPassword"><?php InterfaceLanguage('Screen', '0003', false, '', false, true); ?>: *</label>
                                                                    <div class="col-md-6">
                                                                        <input type="password" name="ClientPassword" value="<?php echo set_value('ClientPassword'); ?>" id="ClientPassword" class="form-control" />
                                                                        <?php print(form_error('ClientPassword', '<span class="help-block">', '</span>')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" id="form-row-ClientCampaigns">
                                                                    <label class="col-md-3 control-label" for="ClientCampaigns"><?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?></label>
                                                                    <?php if ($Campaigns === false): ?>
                                                                        <div class="col-md-9">
                                                                            <span class="help-block">
                                                                                <?php InterfaceLanguage('Screen', '1285'); ?>
                                                                            </span>
                                                                        </div>
                                                                    <?php else: ?>
                                                                        <div class="col-md-6">
                                                                            <select type="select" name="ClientCampaigns[]" id="ClientCampaigns" class="form-control" multiple style="height:100px;">
                                                                                <?php foreach ($Campaigns as $EachCampaign): ?>
                                                                                    <option value="<?php echo $EachCampaign['CampaignID']; ?>"><?php echo $EachCampaign['CampaignName'] ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group-note">
                                                                                <span class="help-block"><?php InterfaceLanguage('Screen', '1098'); ?></span>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" id="form-row-ClientLists">
                                                                    <label class="col-md-3 control-label" for="ClientLists"><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?></label>
                                                                    <?php if ($Lists === false): ?>
                                                                        <div class="col-md-9">
                                                                            <span class="help-block">
                                                                                <p><?php InterfaceLanguage('Screen', '1284'); ?></p>
                                                                            </span>
                                                                        </div>
                                                                    <?php else: ?>
                                                                        <div class="col-md-6">
                                                                            <select type="select" name="ClientLists[]" id="ClientLists" class="form-control" multiple style="height:100px;">
                                                                                <?php foreach ($Lists as $EachList): ?>
                                                                                    <option value="<?php echo $EachList['ListID']; ?>"><?php echo $EachList['Name'] ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group-note">
                                                                                <span class="help-block"><?php InterfaceLanguage('Screen', '1099'); ?></span>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <a class="btn default" targetform="client-create" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1096', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_clientcreate.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" charset="utf-8">
    var Language = {};
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>