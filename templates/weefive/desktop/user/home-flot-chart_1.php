
<script>
    var ChartsFlotcharts = function () {

        return {
            //main function to initiate the module

            init: function () {

                App.addResizeHandler(function () {
                    Charts.initPieCharts();
                });
            },
            initCharts: function () {

                if (!jQuery.plot) {
                    return;
                }

                function chart5() {
                    if ($('#chart_5').size() != 1) {
                        return;
                    }
                    //moment().month(i+1).format("MMMM")
                    var url_opens = "<?php echo InterfaceAppURL() . '/user/overview/OpenStatistics/' ?>";
                    var url_subscribes = "<?php echo InterfaceAppURL() . '/user/overview/SubscribeStatistics/' ?>";
                    var url_clicks = "<?php echo InterfaceAppURL() . '/user/overview/ClickStatistics/' ?>";

                    var user_id = <?php echo $UserInformation['UserID']; ?>;
                    var no_of_months = 6;

                    var opens = null;
                    var clicks = null;
                    var subscribes = null;

                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months,
                        },
                        url: url_opens,
                        success: function (data) {
                            opens = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });


                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months,
                        },
                        url: url_subscribes,
                        success: function (data) {
                            subscribes = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });
//                    alert(subscribes)

                    $.ajax({
                        type: "POST",
                        data: {
                            'user_id': user_id,
                            'no_of_months': no_of_months,
                        },
                        url: url_clicks,
                        success: function (data) {
                            clicks = JSON.parse(data)
                        },
                        async: false // <- this turns it into synchronous
                    });
//                    alert(clicks)

                    //open
                    var d1 = [];
                    subscribes.forEach(function (entry) {
                        d1.push([entry.MonthName, parseInt(entry.TotalSubscriptions)]);

                    });
                    //console.log(d1);

                    //click
                    var d2 = [];
                    clicks.forEach(function (entry) {
                        d2.push([entry.MonthName, parseInt(entry.TotalClicks)]);

                    });
                    //console.log(d2);

                    var d3 = [];
                    opens.forEach(function (entry) {
                        d3.push([entry.MonthName, parseInt(entry.TotalOpens)]);

                    });
                    //console.log(d3);


//                    for (var i = 0; i <= 5; i += 1)
//                        d1.push([moment().month(i + 1).format("MMMM"), parseInt(Math.random() * 30)]);

//                    for (var i = 0; i <= 5; i += 1)
//                        d2.push([moment().month(i + 1).format("MMMM"), parseInt(Math.random() * 30)]);
//                    var d3 = [];
//                    for (var i = 0; i <= 5; i += 1)
//                        d3.push([moment().month(i + 1).format("MMMM"), parseInt(Math.random() * 30)]);
                    var stack = 0,
                            bars = true,
                            lines = false,
                            steps = false;
                    function plotWithOptions() {
                        $.plot($("#chart_5"),
                                [{
                                        label: "subscribe",
                                        data: d1,
                                        lines: {
                                            lineWidth: 1,
                                        },
                                        shadowSize: 0
                                    }, {
                                        label: "click",
                                        data: d2,
                                        lines: {
                                            lineWidth: 1,
                                        },
                                        shadowSize: 0
                                    }, {
                                        label: "open",
                                        data: d3,
                                        lines: {
                                            lineWidth: 1,
                                        },
                                        shadowSize: 0
                                    }]

                                , {
                                    series: {
                                        stack: stack,
                                        lines: {
                                            show: lines,
                                            fill: true,
                                            steps: steps,
                                            lineWidth: 0, // in pixels
                                        },
                                        bars: {
                                            show: bars,
                                            barWidth: 0.5,
                                            lineWidth: 0, // in pixels
                                            shadowSize: 0,
                                            align: 'center'
                                        }
                                    },
                                    grid: {
                                        tickColor: "#eee",
                                        borderColor: "#eee",
                                        borderWidth: 1
                                    }
                                    ,
                                    xaxis: {
                                        mode: "categories",
                                        tickLength: 0
                                    }
                                }
                        );
                    }

                    $(".stackControls input").click(function (e) {
                        e.preventDefault();
                        stack = $(this).val() == "With stacking" ? true : null;
                        plotWithOptions();
                    });
                    $(".graphControls input").click(function (e) {
                        e.preventDefault();
                        bars = $(this).val().indexOf("Bars") != -1;
                        lines = $(this).val().indexOf("Lines") != -1;
                        steps = $(this).val().indexOf("steps") != -1;
                        plotWithOptions();
                    });
                    plotWithOptions();
                }

                //graph
                chart5();
            }
        };
    }();
    jQuery(document).ready(function () {
//        ChartsFlotcharts.init();
        ChartsFlotcharts.initCharts();
    });
</script>

