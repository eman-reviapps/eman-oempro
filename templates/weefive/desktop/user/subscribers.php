<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
    <div class="span-23 last">
        <div id="section-bar">
            <div class="header">
                <h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '1316', false, '', false, true, array($ListInformation['Name'])); ?></h1>
            </div>
            <div class="actions">
                <a class="button" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0967', false, '', true); ?></strong></a>
                <a class="button" href="<?php echo InterfaceAppUrl(); ?>/user/list/statistics/<?php echo $ListInformation['ListID']; ?>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1134', false, '', true); ?></strong></a>
            </div>
        </div>
    </div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
    <div class="span-5 snap-5">
        <ul class="left-sub-navigation">
            <li class="header first with-module"><?php InterfaceLanguage('Screen', '0053', false, '', true); ?></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'Active' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/Active/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '0052', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'Unsubscribed' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/Unsubscribed/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '0909', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'Pending' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/Pending/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '1314', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'SoftBounced' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/SoftBounced/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '1112', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'HardBounced' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/HardBounced/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '1113', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
            <li <?php print(($FilterType == 'ByStatus' && $FilterData == 'Suppressed' ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/ByStatus/Suppressed/<?php echo '1/' . $RPP; ?>"><?php InterfaceLanguage('Screen', '1317', false, '', false, true); ?><span class="item-count"><?php
                        if ($TotalSentCampaigns > 0) {
                            print('(' . $TotalSentCampaigns . ')');
                        }
                        ?></span></a></li>
        </ul>
        <?php if ($Segments != false): ?>
            <ul class="left-sub-navigation">
                <li class="header"><?php InterfaceLanguage('Screen', '0563', false, '', true); ?></li>
                <?php foreach ($Segments as $EachSegment): ?>
                    <li <?php print(($FilterType == 'Segment' && $FilterData == $EachSegment['SegmentID'] ? 'class="selected"' : '')); ?>>
                        <a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/Segment/<?php echo $EachSegment['SegmentID']; ?>"><?php echo $EachSegment['SegmentName']; ?> <span class="item-count">(<?php echo $EachSegment['SubscriberCount']; ?>)</span></a>
                    </li>
                <?php endforeach ?>
                <li><a href="<?php InterfaceAppURL(); ?>/user/segments/browse/<?php echo $ListInformation['ListID']; ?>" class="action"><?php InterfaceLanguage('Screen', '1315', false, '', false, true); ?></a></li>
            </ul>
        <?php endif; ?>
    </div>
    <div class="span-18 last">
        <div id="page-shadow">
            <div id="page">
                <div class="white" style="min-height:400px;">
                    <form id="subscribers-table-form" action="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/<?php print($FilterType); ?>/<?php print($FilterData) ?>/<?php echo $CurrentPage . '/' . $RPP; ?>" method="post">
                        <div class="module-container">
                            <?php
                            if (isset($PageSuccessMessage) == true):
                                ?>
                                <div class="page-message success">
                                    <div class="inner">
                                        <span class="close">X</span>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                </div>
                                <?php
                            elseif (isset($PageErrorMessage) == true):
                                ?>
                                <div class="page-message error">
                                    <div class="inner">
                                        <span class="close">X</span>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                </div>
                                <?php
                            endif;
                            ?>
                            <div class="grid-operations">
                                <div class="inner">
                                    <span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="campaigns-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>, 
                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="campaigns-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                    &nbsp;&nbsp;-&nbsp;&nbsp;
                                    <?php if (InterfacePrivilegeCheck('Subscribers.Delete', $UserInformation)): ?>
                                        <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="subscribers-table-form"><?php InterfaceLanguage('Screen', '0570'); ?></a>
                                    <?php endif; ?>
                                    <input type="hidden" name="Command" value="DeleteSubscriber" id="Command">
                                </div>
                            </div>

                        </div>
                        <table border="0" cellspacing="0" cellpadding="0" class="grid" id="campaigns-table">
                            <tr>
                                <th width="500" colspan="2"><?php InterfaceLanguage('Screen', '0758', false, '', true); ?></th>
                            </tr>
                            <?php if ($Subscribers == false || count($Subscribers) < 1): ?>
                                <tr>
                                    <td><?php InterfaceLanguage('Screen', '1318', false, '', false, false); ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php foreach ($Subscribers as $EachSubscriber): ?>
                                <tr>
                                    <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedSubscribers[]" value="<?php echo $EachSubscriber['SubscriberID']; ?>" id="SelectedSubscribers<?php print($EachCampaign['CampaignID']); ?>"></td>
                                    <td width="485" class="no-padding-left">
                                        <?php if (InterfacePrivilegeCheck('Subscriber.Update', $UserInformation)): ?>
                                            <a href="<?php InterfaceAppURL(); ?>/user/subscriber/edit/<?php echo $ListInformation['ListID']; ?>/<?php echo $EachSubscriber['SubscriberID']; ?>"><?php echo $EachSubscriber['EmailAddress']; ?></a>
                                        <?php else: ?>
                                            <?php echo $EachSubscriber['EmailAddress']; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php if ($TotalPages > 1): ?>
        <div class="span-1 last">
            <ul class="pagination with-module">
                <?php
                if ($CurrentPage > 1):
                    ?>
                    <li class="first"><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID'] ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/<?php echo '1/' . $RPP; ?>">&nbsp;</a></li>
                    <?php
                endif;
                ?>
                <?php
                $CounterStart = ($CurrentPage < 3 ? 1 : $CurrentPage - 2);
                $CounterFinish = ($CurrentPage > ($TotalPages - 2) ? $TotalPages : $CurrentPage + 2);
                for ($PageCounter = $CounterStart; $PageCounter <= $CounterFinish; $PageCounter++):
                    ?>
                    <li <?php print(($CurrentPage == $PageCounter ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/<?php echo $PageCounter . '/' . $RPP; ?>"><?php print(number_format($PageCounter)); ?></a></li>
                    <?php
                endfor;
                ?>
                <?php
                if ($CurrentPage < $TotalPages):
                    ?>
                    <li class="last"><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>/<?php print($FilterType); ?>/<?php print($FilterData); ?>/<?php echo $TotalPages . '/' . $RPP; ?>">&nbsp;</a></li>
                        <?php
                    endif;
                    ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
<!-- Page - End -->

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1335': '<?php InterfaceLanguage('Screen', '1335', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>