<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!--light bg-inverse-->
<div class="note note-info">
    <span>( <a href="<?php InterfaceAppURL(); ?>/user/subscribers/details/"><strong><?php echo $TotalSubscribersOnTheAccount ?></strong> 
            <?php
            if ($UserInformation['IsAdmin'] == 'No') {
                ?>
                of <strong><?php echo $UserInformation['GroupInformation']['LimitSubscribers']; ?></strong> 
                <?php
            }
            ?>
        </a> <?php InterfaceLanguage('Screen', '9272'); ?> )</span>
</div>
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0933', false, '', false, true); ?></span>                    
            <!--<span class="small">( <a href="<?php InterfaceAppURL(); ?>/user/subscribers/details/"><strong><?php echo $TotalSubscribersOnTheAccount ?></strong> of <strong><?php echo $UserInformation['GroupInformation']['LimitSubscribers']; ?></strong> </a> <?php InterfaceLanguage('Screen', '9272'); ?> )</span>-->
        </div>
        <div class="actions">
            <?php if (InterfacePrivilegeCheck('List.Create', $UserInformation)): ?>
                <a id="button-lists-create-list" class="btn default btn-transparen btn-sm <?php echo $disabled_class ?>" href="<?php InterfaceAppURL(); ?>/user/list/create/"><strong><?php InterfaceLanguage('Screen', '0934', false, '', false); ?></strong></a>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="page-content-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-transparent">
                <div class="portlet-body form">
                    <form id="campaigns-table-form" action="<?php InterfaceAppURL(); ?>/user/lists/browse/" method="post">
                        <?php if (InterfacePrivilegeCheck('List.Delete', $UserInformation)): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if (isset($PageSuccessMessage) == true):
                                        ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageSuccessMessage); ?>
                                        </div>
                                        <?php
                                    elseif (isset($PageErrorMessage) == true):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageErrorMessage); ?>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                    <div class="operations-custom">
                                        <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                        <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="users-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                        <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="users-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                        <a href="#" id="delete_btn" class="main-action btn default btn-transparen btn-sm" ><?php InterfaceLanguage('Screen', '0935'); ?></a>
                                        <input type="hidden" name="Command" value="DeleteLists" id="Command"> 
                                        <!--targetform="campaigns-table-form"-->
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="grid items items-custom table" id="users-table">
<!--                                        <tr class="item">
                                            <th width="570" colspan="3"><?php InterfaceLanguage('Screen', '0541', false, '', true); ?></th>
                                        </tr>-->
                                        <?php if ($Lists === false): ?>
                                            <tr class="item">
                                                <td width="355"><?php InterfaceLanguage('Screen', '0105', false, '', false, false); ?></td>
                                                <td width="130">&nbsp;</td>
                                            </tr>
                                        <?php else: ?>
                                            <?php
                                            foreach ($Lists as $EachList):
                                                ?>
                                                <tr class="item">
                                                    <td width="15" style="vertical-align:top;float:left"><input class="grid-check-element" type="checkbox" name="SelectedLists[]" value="<?php print($EachList['ListID']); ?>" id="SelectedLists<?php print($EachList['ListID']); ?>"></td>
                                                    <td style="float: left">
                                                        <div class="campaign-details campaign-details-custom">
                                                            <h4>
                                                                <?php if (InterfacePrivilegeCheck('List.Get', $UserInformation)): ?>
                                                                    <a class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php print($EachList['ListID']); ?>"><?php print($EachList['Name']); ?></a>
                                                                <?php else: ?>
                                                                    <?php print($EachList['Name']); ?>												
                                                                <?php endif; ?>
                                                                <a href="<?php InterfaceInstallationURL(); ?>rss.php?q=<?php echo $EachList['EncryptedSaltedListID']; ?>" style="position: absolute;left:280px;top:17px" title="<?php InterfaceLanguage('Screen', '1105'); ?>"><img src="<?php InterfaceTemplateURL(); ?>images/icon_rss.png" alt="<?php InterfaceLanguage('Screen', '1105'); ?>" border="0" /></a>
                                                            </h4>

                                                            <?php if ($EachList['OptInMode'] == 'Double' && $EachList['RelOptInConfirmationEmailID'] == 0): ?>
                                                                <span class="clearfix"><?php InterfaceLanguage('Screen', '1167'); ?></span>
                                                                <!--<br/>-->
                                                            <?php endif; ?>


                                                            <div class="btn-group">
                                                                <a class="btn default" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $EachList['ListID']; ?>"><?php InterfaceLanguage('Screen', '0568', false); ?></a>
                                                                <?php if ($EachList['IsAutomationList'] == 'No') { ?>
                                                                    <a class="btn default" href="<?php echo InterfaceAppUrl(); ?>/user/suppressionlist/browse/<?php echo $EachList['ListID']; ?>"><?php InterfaceLanguage('Screen', '9252', false); ?></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <?php // print_r($EachList)  ?>
                                                    <td style="float: right" >
                                                        <div class="stats visible-lg visible-md">
                                                            <ul>
                                                                <li>
                                                                    <a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $EachList['list_information']['ListID']; ?>">
                                                                        <span class="number ng-binding bold"><?php print(number_format($EachList['SubscriberCount'])); ?></span>
                                                                    </a>
                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0104', false, '', false, false); ?></span>
                                                                </li>
                                                                <!--                                                                <li class="clicked">
                                                                                                                                    <span class="number ng-binding bold"><?php print($EachList['list_information']['TotalSent']); ?></span>
                                                                                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9203', false); ?></span>
                                                                                                                                </li>-->

                                                                <li class="clicked">
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $EachList['list_information']['ListID']; ?>/unique_opens">
                                                                        <span class="number ng-binding bold"><?php print($EachList['list_information']['OpenStatistics']['UniqueOpens']); ?></span>
                                                                    </a>
                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0837'); ?></span>
                                                                    <span class="number number-small small"><?php print($EachList['list_information']['OpenStatistics']['UniqueOpensPercent']); ?>%</span>
                                                                </li>

                                                                <li class="clicked">
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $EachList['list_information']['ListID']; ?>/unique_clicks">
                                                                        <span class="number ng-binding bold"><?php print($EachList['list_information']['ClickStatistics']['UniqueClicks']); ?></span>
                                                                    </a>
                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0838'); ?></span>
                                                                    <span class="number number-small small"><?php print($EachList['list_information']['ClickStatistics']['UniqueClicksPercent']); ?>%</span>
                                                                </li>

                                                                <!--                                                                <li class="clicked">
                                                                                                                                    <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/bounces">
                                                                                                                                    <span class="number ng-binding bold"><?php print($EachList['list_information']['BounceStatistics']['TotalBounces']); ?></span>
                                                                                                                                    </a>
                                                                                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0098'); ?></span>
                                                                                                                                    <span class="number number-small small"><?php print($EachList['list_information']['BounceStatistics']['BouncesPercent']); ?>%</span>
                                                                                                                                </li>-->
                                                                <li class="clicked">
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $EachList['list_information']['ListID']; ?>/unsubscriptions">
                                                                        <span class="number ng-binding bold"><?php print($EachList['list_information']['UnsubscribesStatistics']['TotalUnsubscriptions']); ?></span>
                                                                    </a>
                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9205'); ?></span>
                                                                    <span class="number number-small small"><?php print($EachList['list_information']['UnsubscribesStatistics']['TUnsubscriptionsPercent']); ?>%</span>
                                                                </li>
                                                                <!--                                                                <li class="clicked">
                                                                                                                                   <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $EachCampaign['CampaignID']; ?>/spams">
                                                                                                                                    <span class="number ng-binding bold"><?php print($EachList['list_information']['SpamStatistics']['TotalSPAMComplaints']); ?></span>
                                                                                                                                    </a>
                                                                                                                                    <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9206'); ?></span>
                                                                                                                                    <span class="number number-small small"><?php print($EachList['list_information']['SpamStatistics']['TotalSPAMPercent']); ?>%</span>
                                                                                                                                </li>-->
                                                            </ul>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <tr class="item">
                                            <td width="15" style="vertical-align:top">&nbsp;</td>
                                            <td width="355">
                                                <div class="campaign-details">
                                                    <h4>
                                                        <a class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/user/suppressionlist/browse/"><?php InterfaceLanguage('Screen', '1029', false, '', false); ?></a>
                                                    </h4>
                                                    <?php if ($EachList['OptInMode'] == 'Double' && $EachList['RelOptInConfirmationEmailID'] == 0): ?>
                                                        <br /><span class="label label-sm label-success label-mini"><?php InterfaceLanguage('Screen', '1167'); ?></span>
                                                    <?php endif; ?>
                                                    <span class="small"><?php InterfaceLanguage('Screen', '1283'); ?></span>
                                                    <br />
                                                </div>
                                            </td>
                                            <td width="130" class="small-text">
                                                <div class="stats visible-lg visible-md">
                                                    <span class="number ng-binding bold"><?php echo $GlobalSuppressionTotal ?></span> 
                                                    <span class="description font-purple-sharp bold ">
                                                        <?php InterfaceLanguage('Screen', '0129', false, '', false, false); ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_lists.php'); ?>
            </div>-->
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '0938': '<?php InterfaceLanguage('Screen', '0938', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/lists.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>