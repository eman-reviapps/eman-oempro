<div class="portlet light bg-inverse" style="padding-bottom: 0px;margin-bottom: 15px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php print(ucwords($ListInformation['Name'])); ?>
            </span>
        </div>
        <div class="actions">
            <a class="btn default btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/list/statistics/<?php echo $ListInformation['ListID']; ?>"><strong><?php InterfaceLanguage('Screen', '1134', false, '', false); ?></strong></a>
        </div>
    </div>
</div>
