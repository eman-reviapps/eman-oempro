<?php include_once(TEMPLATE_PATH . 'desktop/layouts/login_header.php'); ?>
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form id="Form_Reminder" method="post" action="<?php InterfaceAppURL(); ?>/user/passwordreminder/">
        <h3 class="font-purple-sharp"><?php InterfaceLanguage('Screen', '0006', false); ?></h3>

        <?php
        if (isset($PageErrorMessage) == true):
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php print($PageErrorMessage); ?>
            </div>
            <?php
        else:
            ?>
            <p> <?php InterfaceLanguage('Screen', '0007', false); ?> </p>
        <?php
        endif;
        ?>
        <div class="form-group <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?> no-background-color" id="form-row-EmailAddress">
            <input type="text" name="EmailAddress" placeholder="<?php InterfaceLanguage('Screen', '0008', false); ?>" value="<?php echo set_value('EmailAddress', ''); ?>" id="EmailAddress" class="form-control placeholder-no-fix"  />
            <?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
        </div>
        <div class="form-actions">
            <a id="FormButton_Submit" class="btn default uppercase pull-right" targetform="Form_Reminder"><strong><?php InterfaceLanguage('Screen', '0267', false, '', true); ?></strong></a>
            <input type="hidden" name="Command" value="ResetPassword" id="Command" />
            
            <a class="btn green btn-outline" href="<?php InterfaceAppURL(); ?>/user/"><?php InterfaceLanguage('Screen', '0009', false); ?></a>            
        </div>
    </form>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/login_footer.php'); ?>
