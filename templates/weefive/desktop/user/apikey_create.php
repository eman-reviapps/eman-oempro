<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/settings_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1942', false, '', false, true); ?> </span>                    
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form">
                                            <!-- Page - START -->
                                            <form id="apikey-create" class="form-horizontal" action="<?php InterfaceAppURL(); ?>/user/apikeys/create/" method="post">
                                                <input type="hidden" name="Command" value="CreateAPIKey" id="Command">
                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <h4 class="form-section bold font-blue" style="margin-top: 10px !important">
                                                        <?php InterfaceLanguage('Screen', '0037', false); ?>
                                                    </h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group <?php print((form_error('Note') != '' ? 'has-error' : '')); ?>" id="form-group-Note">
                                                    <label class="col-md-3 control-label" for="Note"><?php InterfaceLanguage('Screen', '1946', false, '', false, true); ?>: *</label>

                                                    <div class="col-md-4">
                                                        <input type="text" name="Note" value="<?php echo set_value('Note'); ?>" id="Note" class="form-control" />
                                                        <?php print(form_error('Note', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="help-block"><?php InterfaceLanguage('Screen', '1949', false, '', false, false); ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group <?php print((form_error('IPAddress') != '' ? 'has-error' : '')); ?>" id="form-group-IPAddress">
                                                    <label class="col-md-3 control-label" for="IPAddress"><?php InterfaceLanguage('Screen', '1947', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="IPAddress" value="<?php echo set_value('IPAddress'); ?>" id="IPAddress" class="form-control" />                                
                                                        <?php print(form_error('IPAddress', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="help-block"><?php InterfaceLanguage('Screen', '1950', false, '', false, false); ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input type="submit" class="btn default" value="<?php InterfaceLanguage('Screen', '1942', false, '', true); ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- Page - END -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" charset="utf-8">
    var Language = {};
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>