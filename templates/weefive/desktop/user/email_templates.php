<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- Section bar - Start -->
<!-- Section bar - End -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/profile.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.css" rel="stylesheet">
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {

        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    if (window.console) {
                        return console.log('onShown event fired');
                    }
                },
                onContentLoaded: function () {
                    if (window.console) {
                        return console.log('onContentLoaded event fired');
                    }
                },
                onNavigate: function (direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });

    });
</script>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <ul class="nav nav-tabs ">
                            <?php if (InterfacePrivilegeCheck('User.Update', $UserInformation)) : ?>
                                <li class="with-module<?php print($SubSection == 'Account' ? ' selected active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/account/"><?php InterfaceLanguage('Screen', '1106', false, '', false); ?></a></li>
                                <li class="<?php print($SubSection == 'APIKeys' ? ' active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/apikeys/"><?php InterfaceLanguage('Screen', '1940', false, '', false); ?></a></li>
                            <?php endif; ?>
                            <?php if (InterfacePrivilegeCheck('Clients.Get', $UserInformation)) : ?>
                                <li class="<?php print($SubSection == 'Clients' ? 'selected active' : ''); ?> <?php echo InterfacePrivilegeCheck('User.Update', $UserInformation) ? '' : 'with-module'; ?>"><a href="<?php InterfaceAppURL(); ?>/user/clients/"><?php InterfaceLanguage('Screen', '0572', false, '', false); ?></a></li>
                            <?php endif; ?>
                            <?php if (InterfacePrivilegeCheck('EmailTemplates.Manage', $UserInformation)) : ?>
                                <li <?php print($SubSection == 'EmailTemplates' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/"><?php InterfaceLanguage('Screen', '0111', false, '', false); ?></a></li>
                            <?php endif; ?>
                            <?php if (PME_USAGE_TYPE == 'User' || WUFOO_INTEGRATION_ENABLED): ?>
                                <li <?php print(substr($SubSection, 0, 11) == 'Integration' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/integration/"><?php InterfaceLanguage('Screen', '0132', false, '', false); ?></a></li>
                            <?php endif; ?>
                            <li <?php print($SubSection == 'Tags' ? 'class="selected active"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/tags/"><?php InterfaceLanguage('Screen', '0723', false, '', false); ?></a></li>
                            <?php
                            InterfacePluginMenuHook('User.Settings', $SubSection, '<li><a href="_LINK_"></i>_TITLE_</a></li>', '<li class="selected active"><a href="_LINK_">_TITLE_</a></li>'
                            );
                            ?>
                        </ul>
                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-transparent">
                                    <div class="portlet-title" style="margin-bottom: 0">
                                        <div class="caption">
                                            <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                            <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="email-templates-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                            <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="email-templates-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                            <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="email-templates-table-form"><?php InterfaceLanguage('Screen', '0042'); ?></a>
                                        <!--<i class="icon-docs font-purple-sharp"></i>-->
                                        <!--<span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0111', false, '', false); ?> </span>-->                    
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" >   
                                                <a id="button-campaigns-create-campaign" class="btn default btn-transparen btn-sm <?php echo $disabled_class ?>" href="<?php InterfaceAppURL(); ?>/user/emailtemplates/create/">
                                                    <strong><?php InterfaceLanguage('Screen', '0114', false, '', false); ?></strong>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="padding-top: 0">
                                        <div class="table-container">
                                            <form id="email-templates-table-form" action="<?php InterfaceAppURL(); ?>/user/emailtemplates/" method="post">
                                                <input type="hidden" name="Command" value="DeleteEmailTemplates" id="Command">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <?php
                                                        if (isset($PageSuccessMessage) == true):
                                                            ?>
                                                            <div class="alert alert-info alert-dismissable">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <?php print($PageSuccessMessage); ?>
                                                            </div>
                                                            <?php
                                                        elseif (isset($PageErrorMessage) == true):
                                                            ?>
                                                            <div class="alert alert-danger alert-dismissable">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <?php print($PageErrorMessage); ?>
                                                            </div>
                                                            <?php
                                                        endif;
                                                        ?>

                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="grid items items-custom table" id="email-templates-table">
<!--                                                        <tr class="item">
                                                            <th width="100%" colspan="3"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
                                                        </tr>-->
                                                        <?php
                                                        if (count($EmailTemplates) < 1 || $EmailTemplates === false):
                                                            ?>
                                                            <tr class="item">
                                                                <td colspan="3"><?php InterfaceLanguage('Screen', '1078'); ?></td>
                                                            </tr>
                                                            <?php
                                                        endif;
                                                        foreach ($EmailTemplates as $EachTemplate):
                                                            ?>
                                                            <tr class="item">
                                                                <td width="15" style="vertical-align:top;float:left"><input class="grid-check-element" type="checkbox" name="SelectedEmailTemplates[]" value="<?php print($EachTemplate['TemplateID']); ?>" id="SelectedEmailTemplates<?php print($EachTemplate['TemplateID']); ?>"></td>
                                                                <td style="float: left">
                                                                    <div class="campaign-details">
                                                                        <?php
                                                                        $img_url = ($EachTemplate['ScreenshotImage'] != '') ? AWS_END_POINT . S3_BUCKET . "/" . $EachTemplate['ScreenshotImage'] : TEMPLATE_URL . "images/no_thumbnail.png";
                                                                        ?>
                                                                        <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                                           data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                                            <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                                        </a>
                                                                        <div class="column-block">                                                        
                                                                            <h4>
                                                                                <a class="font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/user/emailtemplates/edit/<?php print($EachTemplate['TemplateID']); ?>"><?php print($EachTemplate['TemplateName']); ?></a>
                                                                            </h4>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                                <td style="float: right" >
                                                                    <div class="stats visible-lg visible-md">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/edit/<?php print($EachTemplate['TemplateID']); ?>" class="btn default"><?php InterfaceLanguage('Screen', '0508', false); ?></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                        ?>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '0312': '<?php InterfaceLanguage('Screen', '0312', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/admin/settings_emailtemplates.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>