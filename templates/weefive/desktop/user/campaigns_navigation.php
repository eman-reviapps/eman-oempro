<ul class="nav nav-tabs ">
    <li <?php print(( ( ($FilterType == 'ByCategory' && $FilterData == 'Sent') || $ActiveCampaignItem == 'Compare') ? 'class="active"' : '')); ?> >
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Sent/<?php print(implode(':', $FilterTags)); ?>" >
            <?php InterfaceLanguage('Screen', '0717', false, '', false, true); ?>
            <?php
            if ($TotalSentCampaigns > 0) {
                print('(' . $TotalSentCampaigns . ')');
            }
            ?>
        </a>
    </li>
    <li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Outbox' ? 'class="active"' : '')); ?>>
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Outbox/<?php print(implode(':', $FilterTags)); ?>" >
            <?php InterfaceLanguage('Screen', '0718', false, '', false, true); ?>
            <?php
            if ($TotalOutboxCampaigns > 0) {
                print('(' . $TotalOutboxCampaigns . ')');
            }
            ?>
        </a>
    </li>
    <li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Draft' ? 'class="active"' : '')); ?>>
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Draft/<?php print(implode(':', $FilterTags)); ?>">
            <?php InterfaceLanguage('Screen', '0719', false, '', false, true); ?>
            <?php
            if ($TotalDraftCampaigns > 0) {
                print('(' . $TotalDraftCampaigns . ')');
            }
            ?>
        </a>
    </li>
    <li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Scheduled' ? 'class="active"' : '')); ?> >
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Scheduled/<?php print(implode(':', $FilterTags)); ?>">
            <?php InterfaceLanguage('Screen', '0720', false, '', false, true); ?>
            <?php
            if ($TotalScheduledCampaigns > 0) {
                print('(' . $TotalScheduledCampaigns . ')');
            }
            ?>
        </a>
    </li>
    <li <?php print(($FilterType == 'ByCategory' && $FilterData == 'Paused' ? 'class="active"' : '')); ?> >
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/Paused/<?php print(implode(':', $FilterTags)); ?>">
            <?php InterfaceLanguage('Screen', '0721', false, '', false, true); ?>
            <?php
            if ($TotalPausedCampaigns > 0) {
                print('(' . $TotalPausedCampaigns . ')');
            }
            ?>
        </a>
    </li>
    <li <?php print(($FilterType == 'ByCategory' && $FilterData == 'PendingApproval' ? 'class="active"' : '')); ?> >
        <a href="<?php InterfaceAppURL(); ?>/user/campaigns/browse/1/<?php print($RPP); ?>/ByCategory/PendingApproval/<?php print(implode(':', $FilterTags)); ?>">
            <?php InterfaceLanguage('Screen', '0722', false, '', false, true); ?>
            <?php
            if ($TotalPendingApprovalCampaigns > 0) {
                print('(' . $TotalPendingApprovalCampaigns . ')');
            }
            ?>
        </a>
    </li>

</ul>