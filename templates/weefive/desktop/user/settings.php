<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- Section bar - Start -->
<!-- Section bar - End -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/profile.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/settings_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <?php
                        if ($SubSection == 'EmailTemplates') {
                            include_once(TEMPLATE_PATH . 'desktop/user/email_templates.php');
                        } elseif ($SubSection == 'Clients') {
                            include_once(TEMPLATE_PATH . 'desktop/user/clients.php');
                        } elseif ($SubSection == 'Client') {
                            include_once(TEMPLATE_PATH . 'desktop/user/clients.php');
                        } elseif ($SubSection == 'Tags') {
                            include_once(TEMPLATE_PATH . 'desktop/user/tags.php');
                        } elseif ($SubSection == 'APIKeys') {
                            include_once(TEMPLATE_PATH . 'desktop/user/apikeys.php');
                        } elseif ($SubSection == 'Account') {
                            include_once(TEMPLATE_PATH . 'desktop/user/account.php');
                        } elseif ($SubSection == 'Integration') {
                            include_once(TEMPLATE_PATH . 'desktop/user/integration.php');
                        } elseif ($SubSection == 'IntegrationWufoo') {
                            include_once(TEMPLATE_PATH . 'desktop/user/integration_wufoo.php');
                        } elseif ($SubSection == 'IntegrationHighrise') {
                            include_once(TEMPLATE_PATH . 'desktop/user/integration_highrise.php');
                        } elseif ($SubSection == 'IntegrationWufooForm') {
                            include_once(TEMPLATE_PATH . 'desktop/user/integration_wufoo_form.php');
                        } elseif ($SubSection == 'IntegrationPreviewMyEmail') {
                            include_once(TEMPLATE_PATH . 'desktop/user/integration_pme.php');
                        } elseif (($SubSection != '') && (isset($PluginView) == true && $PluginView != '')) {
                            include_once($PluginView);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>
