<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/list.js" type="text/javascript" charset="utf-8"></script>		
<!--light bg-inverse-->
<?php // echo $ActiveListItem ?>
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 15px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold" title="<?php InterfaceLanguage('Screen', '1790', false); ?> <?php echo $ListInformation['ListID']; ?>">
                <?php print(ucwords($ListInformation['Name'])); ?>
            </span>
        </div>
        <div class="actions">
            <?php if ($ListInformation['IsAutomationList'] == 'No') { ?>
                <a class="btn default btn-sm <?php echo $disabled_class?> <?php echo isset($ActiveListItem) && !empty($ActiveListItem) && $ActiveListItem == 'Forms' ? 'active' : '' ?>" href="<?php echo InterfaceAppUrl(); ?>/user/list/forms/<?php echo $ListInformation['ListID']; ?>/"><strong><?php InterfaceLanguage('Screen', '0969', false, '', false); ?></strong></a>
            <?php } ?> 
            (
            <?php if (InterfacePrivilegeCheck('Subscribers.Get', $UserInformation)): ?>
                <a class="btn default btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0970'); ?></a>
            <?php endif; ?>
            <?php if ($ListInformation['IsAutomationList'] == 'No') { ?>
                /
                <?php if (InterfacePrivilegeCheck('Subscribers.Import', $UserInformation) ): ?>
                    <a class="btn default btn-sm <?php echo $disabled_class?> <?php echo isset($ActiveListItem) && !empty($ActiveListItem) && $ActiveListItem == 'Add' ? 'active' : '' ?>" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0613'); ?></a>
                <?php endif; ?>

                /
                <?php if (InterfacePrivilegeCheck('Subscribers.Delete', $UserInformation)): ?>
                        <!--<a class="btn default btn-sm <?php echo isset($ActiveListItem) && !empty($ActiveListItem) && $ActiveListItem == 'Remove' ? 'active' : '' ?>" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/remove/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0507'); ?></a>-->
                    <a class="btn default btn-sm <?php echo isset($ActiveListItem) && !empty($ActiveListItem) && $ActiveListItem == 'Move' ? 'active' : '' ?>" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/move/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '9253'); ?></a>
                    /
                    <a class="btn default btn-sm <?php echo isset($ActiveListItem) && !empty($ActiveListItem) && $ActiveListItem == 'Copy' ? 'active' : '' ?>" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/copy/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '1153'); ?></a>
                <?php endif; ?>
            <?php } ?> 
            )
            <span class="font-purple-sharp bold" title="List ID:  5">
                <?php InterfaceLanguage('Screen', '9118'); ?>     
            </span>
            <?php if (InterfacePrivilegeCheck('Subscribers.Get', $UserInformation)): ?>
                                        <!--<a class="btn default btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/export/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '1126'); ?></a>-->
            <?php endif; ?>
            <!--            <div class="btn-group btn-group-solid">
                            <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-horizontal"></i> <?php InterfaceLanguage('Screen', '1440', false, '', false, true); ?>
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu">
            <?php if (InterfacePrivilegeCheck('Subscribers.Get', $UserInformation)): ?>
                                                                        <li><a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0568'); ?></a></li>
            <?php endif; ?>
            <?php if (InterfacePrivilegeCheck('Subscribers.Import', $UserInformation)): ?>
                                                                        <li><a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0967'); ?></a></li>
            <?php endif; ?>
            <?php if (InterfacePrivilegeCheck('Subscribers.Delete', $UserInformation)): ?>
                                                                        <li><a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/remove/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '1108'); ?></a></li>
            <?php endif; ?>
            <?php if (InterfacePrivilegeCheck('Subscribers.Get', $UserInformation)): ?>
                                                                        <li><a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/export/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '1126'); ?></a></li>
            <?php endif; ?>
                            </ul>
                        </div>-->
        </div>
    </div>
</div>