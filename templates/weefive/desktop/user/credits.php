<?php
include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php');
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/birdie.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" charset="utf-8">
    var TaxRate = <?php print(PAYMENT_TAX_PERCENT); ?>;
    var PurchaseURL = '<?php InterfaceAppURL(); ?>/user/credits/purchase/';
    var pay_as_you_go_array =<?php echo json_encode($PayAsYouGoPackages); ?>;
</script>
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php InterfaceLanguage('Screen', '1872', false, '', false, false); ?>
            </span>
        </div>
    </div>
</div>
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-transparent">
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="portlet light">
                            <div class="portlet-body">
                                <form id="purchase-credits-form" action="<?php InterfaceAppURL(); ?>/user/credits/purchase/" method="post">

                                    <div class="row">
                                        <div class="form-group" id="form-row-Credits">
                                            <label for="Credits" style="width:100px" class="control-label col-md-2"><?php InterfaceLanguage('Screen', '1494', false, '', false, true); ?>: *</label>
                                            <div class="col-md-3">
                                                <select name="PayAsYouGoPackage" id="PayAsYouGoPackage" class="form-control">
                                                    <?php
                                                    foreach ($PayAsYouGoPackages as $EachGroup):
                                                        ?>
                                                        <option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('package', $EachGroup['UserGroupID'], ($UserInformation['RelUserGroupID'] == $EachGroup['UserGroupID'] ? true : false)); ?>><?php print($EachGroup['LimitEmailSendPerPeriod']); ?> Emails</option>
                                                        <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                                <input type="hidden" name="Credits" value="5000" readonly id="Credits" class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <span class="help-block">
                                                    <strong id="CreditsAmount">0.00</strong> 
                                                    <strong><?php print(PAYMENT_CURRENCY); ?></strong> 
                                                    <?php if (PAYMENT_TAX_PERCENT > 0): ?>(<?php print(PAYMENT_TAX_PERCENT); ?>% <?php InterfaceLanguage('Screen', '1706', false, '', false, true); ?>)<?php endif; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-action-container container-custom">
                                                <a class="btn default" targetform="purchase-credits-form" id="form-button" href="#" >
                                                    <?php InterfaceLanguage('Screen', '1495', false, '', true); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 pull-right">
                        <?php if (isset($Balance) && $Balance > 0) { ?>
                            <div class="portlet sale-summary" style="padding: 10px;margin-left: -40px;background: #fff">
                                <div class="portlet-title">
                                    <div class="caption font-purple-sharp sbold">  <?php InterfaceLanguage('CheckOut', '0010', false); ?> </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class="sale-info bold" style="font-size: 18px;"> <?= $Balance ?> $<i class="fa fa-img-down"></i>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                        <script language="JavaScript" type="text/javascript" >
                            TrustLogo("https://flyinglist.com/comodo_secure_seal_100x85_transp.png", "CL1", "none");
                        </script>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<script>
    $(document).ready(function () {
//        creditWindowLoaded();

        payAsYouGoChanged($("#PayAsYouGoPackage").val());

        $('#PayAsYouGoPackage').on('change', function () {
            var groupId = this.value;
            payAsYouGoChanged(groupId);
        });

        function payAsYouGoChanged(groupId)
        {
            var result;

            for (var i = 0; i < pay_as_you_go_array.length; i++) {
                if (pay_as_you_go_array[i]['UserGroupID'] == groupId) {
                    result = pay_as_you_go_array[i];
                }
            }
            var price_range = result.PaymentPricingRange.split("|");
            $('#CreditsAmount').text(price_range[0] * price_range[1]);
            $('#Credits').val(price_range[0]);
        }
    });

</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>