<form id="account-update-form" action="<?php InterfaceAppURL() ?>/user/account/" method="post">
    <div class="portlet light ">
        <div class="portlet-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-1" data-toggle="tab"><?php InterfaceLanguage('Screen', '0034', false, '', false); ?></a>
                </li>
                <li>
                    <a href="#tab-2" data-toggle="tab"><?php InterfaceLanguage('Screen', '0035', false, '', false); ?></a>
                </li>
            </ul>
            <div class="tab-content">
                <br/>
                <br/>
                <br/>
                <div class="col-md-12">
                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <strong><?php print($PageErrorMessage); ?></strong>
                        </div>
                        <?php
                    elseif (isset($PageSuccessMessage) == true):
                        ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <strong><?php print($PageSuccessMessage); ?></strong>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <strong><?php InterfaceLanguage('Screen', '0275', false); ?></strong>
                        </div>
                        <?php
                    else :
                        ?>
                        <div class="alert alert-info alert-info-custom">
                            <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                        </div>
                    <?php
                    endif;
                    ?>
                </div>
                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('FirstName') != '' ? 'has-error' : '')); ?>" id="form-group-FirstName">
                                <label class="control-label col-md-2" for="FirstName"><?php InterfaceLanguage('Screen', '0021', false); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="FirstName" value="<?php echo set_value('FirstName', $UserInformation['FirstName']); ?>" id="FirstName" class="form-control" />
                                    <?php print(form_error('FirstName', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('LastName') != '' ? 'has-error' : '')); ?>" id="form-group-LastName">
                                <label class="control-label col-md-2" for="LastName"><?php InterfaceLanguage('Screen', '0022', false); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="LastName" value="<?php echo set_value('LastName', $UserInformation['LastName']); ?>" id="LastName" class="form-control" />
                                    <?php print(form_error('LastName', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Username') != '' ? 'has-error' : '')); ?>" id="form-group-Username">
                                <label class="control-label col-md-2" for="Username"><?php InterfaceLanguage('Screen', '0002', false); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="Username" value="<?php echo set_value('Username', $UserInformation['Username']); ?>" id="Username" class="form-control" />
                                    <?php print(form_error('Username', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('NewPassword') != '' ? 'has-error' : '')); ?>" id="form-group-NewPassword">
                                <label class="control-label col-md-2" for="NewPassword"><?php InterfaceLanguage('Screen', '0054', false); ?>:</label>

                                <div class="col-md-6"><input type="password" name="NewPassword" value="<?php echo set_value('NewPassword', ''); ?>" id="NewPassword" class="form-control" />
                                    <?php print(form_error('NewPassword', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('EmailAddress') != '' ? 'has-error' : '')); ?>" id="form-group-EmailAddress">
                                <label class="control-label col-md-2" for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', $UserInformation['EmailAddress']); ?>" id="EmailAddress" class="form-control" />
                                    <?php print(form_error('EmailAddress', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('TimeZone') != '' ? 'has-error' : '')); ?>" id="form-group-TimeZone">
                                <label class="control-label col-md-2" for="TimeZone"><?php InterfaceLanguage('Screen', '0016', false); ?>: *</label>
                                <div class="col-md-6">
                                    <select name="TimeZone" id="TimeZone" class="form-control">
                                        <?php
                                        foreach (Core::GetTimeZoneList() as $TimeZone):
                                            ?>
                                            <option value="<?php print($TimeZone); ?>" <?php echo set_select('TimeZone', html_entity_decode($TimeZone), ($UserInformation['TimeZone'] == html_entity_decode($TimeZone) ? true : false)) ?>><?php print($TimeZone); ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                    <?php print(form_error('TimeZone', '<span class="help-block">', '</span>')); ?>

                                </div>
                                <div class="col-md-3">
                                    <span  class="help-block"><?php InterfaceLanguage('Screen', '0033', false); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Language') != '' ? 'has-error' : '')); ?>" id="form-group-Language">
                                <label class="control-label col-md-2" for="Language"><?php InterfaceLanguage('Screen', '0018', false); ?>:</label>
                                <div class="col-md-6">
                                    <select name="Language" id="Language" class="form-control">
                                        <?php
                                        foreach (Core::DetectLanguages() as $EachLanguage):
                                            ?>
                                            <option value="<?php print($EachLanguage['Code']); ?>" <?php echo set_select('Language', $EachLanguage['Code'], ($UserInformation['Language'] == $EachLanguage['Code'] ? true : false)) ?>><?php print($EachLanguage['Name']); ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                    <?php print(form_error('Language', '<span class="help-block">', '</span>')); ?>

                                </div>
                                <div class="col-md-3">
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0032', false); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('CompanyName') != '' ? 'has-error' : '')); ?>" id="form-group-CompanyName">
                                <label class="control-label col-md-2" for="CompanyName"><?php InterfaceLanguage('Screen', '0023', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="CompanyName" value="<?php echo set_value('CompanyName', $UserInformation['CompanyName']); ?>" id="CompanyName" class="form-control" />
                                    <?php print(form_error('CompanyName', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Website') != '' ? 'has-error' : '')); ?>" id="form-group-Website">
                                <label class="control-label col-md-2" for="Website"><?php InterfaceLanguage('Screen', '0024', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="Website" value="<?php echo set_value('Website', $UserInformation['Website']); ?>" id="Website" class="form-control" />
                                    <?php print(form_error('Website', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Street') != '' ? 'has-error' : '')); ?>" id="form-group-Street">
                                <label class="control-label col-md-2" for="Street"><?php InterfaceLanguage('Screen', '0025', false); ?>:</label>
                                <div class="col-md-6">
                                    <textarea name="Street" id="Street" class="form-control"><?php echo set_value('Street', $UserInformation['Street']); ?></textarea>
                                    <?php print(form_error('Street', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('City') != '' ? 'has-error' : '')); ?>" id="form-group-City">
                                <label class="control-label col-md-2" for="City"><?php InterfaceLanguage('Screen', '0026', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="City" value="<?php echo set_value('City', $UserInformation['City']); ?>" id="City" class="form-control" />
                                    <?php print(form_error('City', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('State') != '' ? 'has-error' : '')); ?>" id="form-group-State">
                                <label class="control-label col-md-2" for="State"><?php InterfaceLanguage('Screen', '0027', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="State" value="<?php echo set_value('State', $UserInformation['State']); ?>" id="State" class="form-control" />
                                    <?php print(form_error('State', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Zip') != '' ? 'has-error' : '')); ?>" id="form-group-Zip">
                                <label class="control-label col-md-2" for="Zip"><?php InterfaceLanguage('Screen', '0028', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="Zip" value="<?php echo set_value('Zip', $UserInformation['Zip']); ?>" id="Zip" class="form-control" />
                                    <?php print(form_error('Zip', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Country') != '' ? 'has-error' : '')); ?>" id="form-group-Country">
                                <label class="control-label col-md-2" for="Country"><?php InterfaceLanguage('Screen', '0029', false); ?>:</label>
                                <div class="col-md-6">
                                    <select name="Country" id="Country" class="form-control">
                                        <option value="" <?php echo set_select('Country', '', ($UserInformation['Country'] == '' ? true : false)) ?>><?php InterfaceLanguage('Screen', '0017', false); ?></option>
                                        <?php
                                        $ArrayCountries = ApplicationHeader::$ArrayLanguageStrings['Screen']['0036'];
                                        asort($ArrayCountries);
                                        foreach ($ArrayCountries as $Code => $Name):
                                            ?>
                                            <option value="<?php print($Code); ?>" <?php echo set_select('Country', $Code, ($UserInformation['Country'] == $Code ? true : false)); ?>><?php print($Name); ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                    <?php print(form_error('Country', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Phone') != '' ? 'has-error' : '')); ?>" id="form-group-Phone">
                                <label class="control-label col-md-2" for="Phone"><?php InterfaceLanguage('Screen', '0030', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="Phone" value="<?php echo set_value('Phone', $UserInformation['Phone']); ?>" id="Phone" class="form-control" />
                                    <?php print(form_error('Phone', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Fax') != '' ? 'has-error' : '')); ?>" id="form-group-Fax">
                                <label class="control-label col-md-2" for="Fax"><?php InterfaceLanguage('Screen', '0031', false); ?>:</label>
                                <div class="col-md-6">
                                    <input type="text" name="Fax" value="<?php echo set_value('Fax', $UserInformation['Fax']); ?>" id="Fax" class="form-control" />
                                    <?php print(form_error('Fax', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-action-container container-custom">
                        <input type="submit" id="ButtonUpdateUserAccount" class="btn default" value="<?php InterfaceLanguage('Screen', '1107', false, '', true); ?>" />
                        <!--<a class="button" targetform="account-update-form" id="ButtonUpdateUserAccount"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1107', false, '', true); ?></strong></a>-->
                        <input type="hidden" name="Command" value="UpdateAccount" id="Command" />
                        <input type="hidden" name="UserID" value="<?php print($UserInformation['UserID']); ?>" id="UserID" />
                    </div>
                </div>
            </div>

        </div>

    </div>
</form>
