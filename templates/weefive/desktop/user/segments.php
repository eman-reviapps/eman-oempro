<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="segments-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="segments-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="segments-table-form"><?php InterfaceLanguage('Screen', '1291'); ?></a>

<!--                                    <i class="icon-microphone font-purple-sharp"></i>
<span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?></span>                    -->
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" >    
                                        <?php if (InterfacePrivilegeCheck('Segment.Create', $UserInformation)): ?>
                                            <a href="<?php InterfaceAppURL(); ?>/user/segment/create/<?php echo $ListID; ?>" class="btn default"><?php InterfaceLanguage('Screen', '1306'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="tab-content-browse" tabcollection="customfield-tabs">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            if (isset($PageSuccessMessage) == true):
                                                ?>
                                                <div class="alert alert-info alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <?php print($PageSuccessMessage); ?>
                                                </div>
                                                <?php
                                            elseif (isset($PageErrorMessage) == true):
                                                ?>
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <?php print($PageErrorMessage); ?>
                                                </div>
                                                <?php
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                    <form id="segments-table-form" action="<?php InterfaceAppURL(); ?>/user/segments/browse/<?php echo $ListID; ?>" method="post">
                                        <input type="hidden" name="Command" value="DeleteSegments" id="Command">
                                        <table class="grid table table-bordered" id="segments-table">
                                            <?php if ($Segments === false): ?>
                                                <tr>
                                                    <td><?php InterfaceLanguage('Screen', '1293', false, '', false, false); ?></td>
                                                </tr>
                                            <?php else: ?>
                                                <?php
                                                foreach ($Segments as $EachSegment):
                                                    ?>
                                                    <tr>
                                                        <td width="15" style="vertical-align:top">
                                                            <input class="grid-check-element" type="checkbox" name="SelectedSegments[]" value="<?php print($EachSegment['SegmentID']); ?>" id="SelectedSegments<?php print($EachSegment['SegmentID']); ?>">
                                                        </td>
                                                        <td width="485" >
                                                            <?php if (InterfacePrivilegeCheck('Segments.Get', $UserInformation)): ?>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/segment/edit/<?php echo $ListID; ?>/<?php print($EachSegment['SegmentID']); ?>"><?php print($EachSegment['SegmentName']); ?></a>
                                                            <?php else: ?>
                                                                <?php print($EachSegment['SegmentName']); ?>
                                                            <?php endif; ?>
                                                            <br />
                                                            <span class="data-label small"><?php InterfaceLanguage('Screen', '0104', false, '', false, false); ?></span> <span class="data small"><?php echo $EachSegment['SubscriberCount']; ?></span>
                                                            <span class="data-label small" style="margin-left:18px;"><a href="<?php InterfaceAppURL(); ?>/user/subscribers/browse/<?php echo $ListID; ?>/<?php echo $EachSegment['SegmentID']; ?>"><?php InterfaceLanguage('Screen', '0568', false); ?></a></span>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1295': '<?php InterfaceLanguage('Screen', '1295', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/segments.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>