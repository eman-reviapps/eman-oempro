<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold" >
                <?php Plugins::HookListener('Action', 'UI.Campaigns.Browse', array()); ?>
                <?php // InterfaceLanguage('Screen', '0714', false, '', false, true); ?>
                <?php InterfaceLanguage('Screen', '0140', false, '', false, true); ?>
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" > 
                <?php if (InterfacePrivilegeCheck('Campaign.Create', $UserInformation)): ?>
                    <a id="button-campaigns-create-campaign" class="btn default btn-transparen btn-sm <?php echo $disabled_class?>" href="<?php InterfaceAppURL(); ?>/user/campaign/create/"><strong><?php InterfaceLanguage('Screen', '0715', false, '', false); ?></strong></a>
                <?php endif; ?>
                <?php if (!$disable): ?>
                    <a id="button-campaigns-photo-editor" target="_blank" class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(false); ?>/user/photoeditor/"><strong><?php InterfaceLanguage('Screen', '9221', false, '', false); ?></strong></a>
                <?php endif; ?>
                <a id="button-campaigns-archives" class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/user/campaigns/archives/"><strong><?php InterfaceLanguage('Screen', '1061', false, '', false); ?></strong></a>                                                
                <a class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/user/tags/" class="action"><?php InterfaceLanguage('Screen', '0724', false, '', false, true); ?></a>

            </div>

        </div>
    </div>
</div>