<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php // include_once(TEMPLATE_PATH . 'desktop/user/campaigns_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-doc font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1062', false, '', false, true); ?> </span>                    
                </div>
                <div class="actions">
                    <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaigns/browse"><strong><?php InterfaceLanguage('Screen', '1133', false, '', FALSE); ?></strong></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="generate-url" action="<?php InterfaceAppURL(); ?>/user/campaigns/archives/" method="post">
                    <input type="hidden" name="Command" value="Generate" id="Command">
                    <?php if ($ArchiveURL != ''): ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1076', false); ?></h4>
                        <span class="help-block">
                            <p><?php InterfaceLanguage('Screen', '1075'); ?></p>
                        </span>
                        <div class="form-group">
                            <input type="text" name="some_name" value="<?php echo $ArchiveURL; ?>" id="some_name" class="form-control" readonly="readonly" />
                        </div>
                        <div class="form-row no-bg clearfix">
                            <a class="btn default" id="form-button" href="<?php InterfaceAppURL(); ?>/user/campaigns/archives/"><strong><?php InterfaceLanguage('Screen', '1077', false, '', true); ?></strong></a>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0037', false); ?></h4>
                        <span class="help-block">
                            <p><?php InterfaceLanguage('Screen', '1063'); ?></p>
                        </span>
                        <div class="form-group clearfix" id="form-row-Tag">
                            <label class="col-md-3 control-label" for="Tag"><?php InterfaceLanguage('Screen', '1064', false, '', false, true); ?></label>
                            <?php if ($Tags === false): ?>
                                <span class="help-block">
                                    <p><?php InterfaceLanguage('Screen', '1287'); ?></p>
                                </span>
                            <?php else: ?>
                                <div class="col-md-6">
                                    <select name="Tag" id="Tag" class="form-control">
                                        <?php foreach ($Tags as $Each): ?>
                                            <option value="<?php echo $Each['TagID']; ?>"><?php echo $Each['Tag']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <span class="help-block">
                                        <p><?php InterfaceLanguage('Screen', '1065'); ?></p>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group clearfix" id="form-row-TemplateURL">
                            <label class="col-md-3 control-label" for="TemplateURL"><?php InterfaceLanguage('Screen', '1066', false, '', false, true); ?></label>
                            <div class="col-md-6">
                                <input type="text" name="TemplateURL" value="" id="TemplateURL" class="form-control" />
                                <span class="help-block">
                                    <p><?php InterfaceLanguage('Screen', '1067'); ?></p>
                                </span>
                            </div>
                        </div>
                        <span class="help-block">
                            <p><?php InterfaceLanguage('Screen', '1068'); ?></p>
                            <ul>
                                <li><?php InterfaceLanguage('Screen', '1069'); ?></li>
                                <li><?php InterfaceLanguage('Screen', '1070'); ?></li>
                                <li><?php InterfaceLanguage('Screen', '1071'); ?></li>
                            </ul>
                            <p class="bold"><?php InterfaceLanguage('Screen', '1072'); ?></p>
                            <p><pre><code><?php InterfaceLanguage('Screen', '1073'); ?></code></pre></p>
                        </span>
                        <div class="help-column span-5 push-1 last">

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-2">   
                                    <?php if ($Tags !== false): ?>
                                        <a class="btn default" targetform="generate-url" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1074', false, '', true); ?></strong></a>                                
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_campaignpublicarchives.php'); ?>
    </div>
</div>


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>