<form id="campaign-create" action="<?php echo $FormURL; ?>" method="post">
	<input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
	<input type="hidden" name="ForceFieldFill" value="true" id="ForceFieldFill">
	<input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">

	<div class="container">

		<div class="span-18 last">
			<div id="page-shadow">
				<div id="page">
					<div class="page-bar">
						<h2><?php InterfaceLanguage('Screen', '0754', false, '', true, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?></h2>
					</div>
					<div class="white">
						<?php
						if (isset($PageErrorMessage) == true):
						?>
							<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
						<?php
						elseif (validation_errors()):
						?>
						<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php
						endif;
						?>

						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0991', false, '', false, true); ?></h3>
						<div class="form-row no-bg">
							<p><?php InterfaceLanguage('Screen', '0990', false); ?></p>
						</div>
						<div class="form-row <?php print((form_error('PreviousCampaign') != '' ? 'error' : '')); ?>" id="form-row-CampaignName">
							<select name="AnotherList" id="AnotherList" class="select">
								<?php foreach ($Lists as $Each): ?>
									<option value="<?php echo $Each['ListID']; ?>"><?php echo $Each['Name']; ?></option>
								<?php endforeach ?>
							</select>
							<?php print(form_error('AnotherList', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_emailcreatefromanotherlist.php'); ?>
		</div>

		<div class="span-18 last">
			<?php include_once(TEMPLATE_PATH.'desktop/user/email_create_wizard_steps.php'); ?>
		</div>

	</div>
</form>