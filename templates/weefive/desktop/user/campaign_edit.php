<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<link type="text/css" href="<?php InterfaceTemplateURL(); ?>styles/jqueryui_custom_theme/jquery-ui-1.7.2.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>js/jquery.datepicker.js"></script>
<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>js/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>js/jquery.slider.js"></script>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign_header.php'); ?>

<div class="page-content-inner">
    <div class="portlet light ">
        <div class="portlet-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-settings" data-toggle="tab">
                        <?php InterfaceLanguage('Screen', '0110', false, '', false, true); ?>
                    </a>
                </li>

                <?php if ($CampaignInformation['SplitTest'] != false): ?>
                    <li>
                        <a href="#tab-test" data-toggle="tab">
                            <?php InterfaceLanguage('Screen', '1352', false, '', false, true); ?>
                        </a>
                    </li>
                <?php endif; ?>

                <?php
                $ScheduleAllowedStatuses = array('Draft', 'Ready');
                if ((in_array($CampaignInformation['CampaignStatus'], $ScheduleAllowedStatuses) && $CampaignInformation['RelEmailID'] > 0) || ($CampaignInformation['SplitTest'] != false && count($CampaignInformation['Emails']) > 0)):
                    ?>
                    <li>
                        <a href="#tab-schedule" data-toggle="tab">
                            <?php InterfaceLanguage('Screen', '0751', false, '', false, true); ?>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="clearfix"></div>
            <form id="campaign-edit" class="form-inputs" action="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php echo $CampaignInformation['CampaignID']; ?>" method="post">
                <input type="hidden" name="Command" value="Save" id="Command" />
                <input type="hidden" name="RelEmailID" value="<?php echo $CampaignInformation['RelEmailID']; ?>" id="RelEmailID" />
                <input type="hidden" name="EmailMode" value="<?php echo $CampaignInformation['Email']['Mode']; ?>" id="EmailMode" />
                <input type="hidden" name="SendDate" value="<?php echo ($CampaignInformation['SendDate'] == '0000-00-00' ? date('Y-m-d') : $CampaignInformation['SendDate']); ?>" id="SendDate">
                <?php if ($CampaignInformation['SplitTest'] != false): ?>
                    <input type="hidden" name="TestSize" value="<?php echo $CampaignInformation['SplitTest']['TestSize']; ?>" id="TestSize">
                    <input type="hidden" name="TestDuration" value="<?php echo $CampaignInformation['SplitTest']['TestDuration']; ?>" id="TestDuration">
                <?php endif; ?>
                <div class="tab-content">
                    <?php
                    if (isset($PageSuccessMessage) == true):
                        ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageSuccessMessage); ?>
                        </div>
                        <?php
                    elseif (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    endif;
                    ?>

                    <div class="tab-pane active" id="tab-settings">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('CampaignName') != '' ? 'has-error' : '')); ?>" id="form-row-CampaignName">
                                    <label class="col-md-3 control-label" for="CampaignName"><?php InterfaceLanguage('Screen', '0756', false, '', false, true); ?>: *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="CampaignName" value="<?php echo set_value('CampaignName', $CampaignInformation['CampaignName']); ?>" id="CampaignName" class="form-control" style="width:450px;" />
                                        <?php print(form_error('CampaignName', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused' && $CampaignInformation['CampaignStatus'] != 'Sent'): ?>
                            <div class="row">
                                <div class="col-md-12">    
                                    <div class="form-group  <?php print((form_error('Recipients') != '' ? 'has-error' : '')); ?>" id="form-row-Recipients">
                                        <label class="col-md-3 control-label" for="Recipients"><?php InterfaceLanguage('Screen', '0092', false, '', false, true); ?>: *</label>
                                        <div class="col-md-5">
                                            <select name="Recipients[]" id="Recipients" multiple class="multiselect">
                                                <?php foreach ($Lists as $EachList): ?>
                                                    <optgroup label="<?php print($EachList['Name']); ?>">
                                                        <option value="<?php print($EachList['ListID'] . ':0'); ?>" <?php echo set_select('Recipients', $EachList['ListID'] . ':0', in_array($EachList['ListID'] . ':0', $CampaignRecipients)); ?>><?php InterfaceLanguage('Screen', '0777', false, '', false, false); ?></option>
                                                        <?php foreach ($EachList['Segments'] as $EachSegment): ?>
                                                            <option value="<?php print($EachList['ListID'] . ':' . $EachSegment['SegmentID']); ?>" <?php echo set_select('Recipients', $EachList['ListID'] . ':' . $EachSegment['SegmentID'], in_array($EachList['ListID'] . ':' . $EachSegment['SegmentID'], $CampaignRecipients)); ?>><?php print($EachSegment['SegmentName']); ?></option>
                                                        <?php endforeach ?>
                                                    </optgroup>
                                                <?php endforeach ?>
                                            </select>
                                            <?php print(form_error('Recipients', '<span class="help-block">', '</span>')); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="help-block"><?php InterfaceLanguage('Screen', '0766', false, '', false, false); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($CampaignInformation['CampaignStatus'] == 'Sent'): ?>
                            <div class="row">
                                <div class="col-md-12">  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="Recipients"><?php InterfaceLanguage('Screen', '0092', false, '', false, true); ?>:</label>
                                        <?php
                                        $ArrayRecipients = array();
                                        foreach ($Lists as $EachList):
                                            ?>
                                            <?php
                                            if (in_array($EachList['ListID'] . ':0', $CampaignRecipients)) {
                                                $ArrayRecipients[] = $EachList['Name'];
                                            }
                                            ?>
                                            <?php foreach ($EachList['Segments'] as $EachSegment): ?>
                                                <?php
                                                if (in_array($EachList['ListID'] . ':' . $EachSegment['SegmentID'], $CampaignRecipients)) {
                                                    $ArrayRecipients[] = $EachList['Name'] . ' > ' . $EachSegment['SegmentName'];
                                                }
                                                ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                        <div class="col-md-6">
                                            <textarea class="form-control" disabled="disabled"><?php echo implode("\n", $ArrayRecipients); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="form-group" id="form-row-PublishOnRSS">
                                    <div class="checkbox-container">
                                        <div class="checkbox-row" style="margin-left:0px;">
                                            <input type="checkbox" name="PublishOnRSS" value="Enabled" id="PublishOnRSS" <?php echo set_checkbox('PublishOnRSS', 'Enabled', ($CampaignInformation['PublishOnRSS'] == 'Enabled' ? true : false)); ?>>
                                            <label for="PublishOnRSS"><?php InterfaceLanguage('Screen', '0763', false, '', false, false); ?></label>
                                        </div>
                                    </div>
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0765', false, '', false, false); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused' && $CampaignInformation['CampaignStatus'] != 'Sent'): ?>
                            <?php if ($CampaignInformation['SplitTest'] == false): ?>
                                <div class="row">
                                    <div class="col-md-12">         
                                        <div class="form-group  clearfix">
                                            <?php if ($CampaignInformation['RelEmailID'] == 0): ?>
                                                <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/create/campaign/<?php echo $CampaignInformation['CampaignID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1254', false, '', false); ?></strong></a>
                                            <?php else: ?>
                                                <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/edit/campaign/<?php echo $CampaignInformation['CampaignID']; ?>/<?php echo $CampaignInformation['RelEmailID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1310', false, '', false); ?></strong></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused' && $CampaignInformation['CampaignStatus'] != 'Sent'): ?>
                            <div class="form-group no-bg">
                                <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0770', false, '', false, true); ?></h4>
                                <span class="help-block"><?php InterfaceLanguage('Screen', '0771', false, '', false, false, array(PRODUCT_NAME)); ?></span>
                                <div class="row">
                                    <div class="col-md-12">    
                                        <div class="form-group" id="form-row-EnableGoogleAnalytics">
                                            <div class="checkbox-container">
                                                <div class="checkbox-row no-margin">
                                                    <input type="checkbox" name="EnableGoogleAnalytics" value="true" id="EnableGoogleAnalytics" <?php echo set_checkbox('EnableGoogleAnalytics', 'true', $CampaignInformation['GoogleAnalyticsDomains'] != ''); ?>>
                                                    <label for="EnableGoogleAnalytics"><?php InterfaceLanguage('Screen', '0774', false, '', false, false); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">    
                                        <div class="form-group  <?php print((form_error('GoogleAnalyticsDomains') != '' ? 'has-error' : '')); ?>" id="form-row-GoogleAnalyticsDomains" <?php if ($CampaignInformation['GoogleAnalyticsDomains'] == ''): ?>style="display:none"<?php endif; ?>>
                                            <label class="col-md-3 control-label" for="GoogleAnalyticsDomains"><?php InterfaceLanguage('Screen', '0775', false, '', false, true); ?>: *</label>
                                            <div class="col-md-6">
                                                <textarea name="GoogleAnalyticsDomains" id="GoogleAnalyticsDomains" class="form-control" rows="5"><?php echo set_value('GoogleAnalyticsDomains', $CampaignInformation['GoogleAnalyticsDomains']); ?></textarea>
                                                <?php print(form_error('GoogleAnalyticsDomains', '<span class="help-block">', '</span>')); ?>
                                            </div>
                                            <span class="help-block"><?php InterfaceLanguage('Screen', '0776', false, '', false, false); ?></span>                                                              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php if ($CampaignInformation['SplitTest'] != false): ?>
                        <div class="tab-pane" id="tab-test">
                            <span class="help-block"><?php InterfaceLanguage('Screen', '1339', false, '', false, false); ?></span>
                            <div class="clearfix"></div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1340', false, '', false, true); ?>:</label>
                                        <div style="float:left;margin-top:6px;width:420px;position:relative">
                                            <div id="test-variations-container"></div>
                                            <div id="test-recipients-bar" style="white-space:nowrap;text-align:center;position:absolute;top:-25px;">
                                                <span class="data label small"><?php InterfaceLanguage('Screen', '1341', false, '', false, true); ?>: <span id="test-recipients-bar-ratio"></span></span>
                                                <div></div>
                                            </div>
                                            <div id="slider-range-min"></div>
                                            <div id="winning-recipients-bar" style="white-space:nowrap;text-align:center;position:absolute;bottom:-25px;right:0;">
                                                <div></div>
                                                <span class="data label small"><?php InterfaceLanguage('Screen', '1342', false, '', false, true); ?>: <span id="winning-recipients-bar-ratio"></span></span>
                                            </div>
                                        </div>
                                        <div class="form-group -note" style="clear:both;padding-top:9px;">
                                            <p><?php InterfaceLanguage('Screen', '1343', false, '', false, false, array('<span class="data" id="test-size">10%</span>', '<span class="data" id="winner-size">90%</span>')); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1345', false, '', false, true); ?>:</label>
                                        <div class="col-md-3">
                                            <select class="form-control" name="TestDurationMultiplier" id="TestDurationMultiplier">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" name="TestDurationBaseSeconds" id="TestDurationBaseSeconds">
                                                <option value="3600"><?php InterfaceLanguage('Screen', '1347'); ?></option>
                                                <option value="86400"><?php InterfaceLanguage('Screen', '1346'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="Winner"><?php InterfaceLanguage('Screen', '1348', false, '', false, true); ?>:</label>
                                        <div class="col-md-6">
                                            <select name="Winner" id="Winner" class="form-control">
                                                <option value="Highest Open Rate" <?php echo set_select('Winner', 'Highest Open Rate', $CampaignInformation['SplitTest']['Winner'] == 'Highest Open Rate'); ?>><?php InterfaceLanguage('Screen', '1349'); ?></option>
                                                <option value="Most Unique Clicks" <?php echo set_select('Winner', 'Most Unique Clicks', $CampaignInformation['SplitTest']['Winner'] == 'Most Unique Clicks'); ?>><?php InterfaceLanguage('Screen', '1350'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if (count($CampaignInformation['Emails']) > 0): ?>
                                <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1353', false, '', false, true); ?></h4>
                                <div class="form-group no-bg">
                                    <table class="small-grid" style="margin-top:3px;">
                                        <?php foreach ($CampaignInformation['Emails'] as $EachEmail): ?>
                                            <tr>
                                                <td>
                                                    <span style="font-size:14px;"><?php echo $EachEmail['EmailName']; ?></span>
                                                </td>
                                                <td style="text-align:right;">
                                                    <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused' && $CampaignInformation['CampaignStatus'] != 'Sent'): ?>
                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/duplicatetestemail/<?php echo $CampaignInformation['CampaignID']; ?>/<?php echo $CampaignInformation['SplitTest']['TestID']; ?>/<?php echo $EachEmail['EmailID']; ?>"><?php InterfaceLanguage('Screen', '0504'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a href="<?php InterfaceAppURL(); ?>/user/email/edit/campaign/<?php echo $CampaignInformation['CampaignID']; ?>/<?php echo $EachEmail['EmailID']; ?>"><?php InterfaceLanguage('Screen', '0508'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/removetestemail/<?php echo $CampaignInformation['CampaignID']; ?>/<?php echo $CampaignInformation['SplitTest']['TestID']; ?>/<?php echo $EachEmail['EmailID']; ?>#edit-tabs/tab-test"><?php InterfaceLanguage('Screen', '0507'); ?></a>
                                                    <?php endif; ?>
                                                    <a href="<?php InterfaceAppURL(); ?>/user/email/preview/<?php echo $EachEmail['EmailID']; ?>/html/campaign/<?php echo $CampaignInformation['CampaignID']; ?>" target="_blank"><?php InterfaceLanguage('Screen', '1355'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            <?php endif; ?>
                            <div class="form-group  no-bg">
                                <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/create/campaign/<?php echo $CampaignInformation['CampaignID']; ?>" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1354', false, '', true); ?></strong></a>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if ($CampaignInformation['CampaignStatus'] != 'Sending' && $CampaignInformation['CampaignStatus'] != 'Paused' && $CampaignInformation['CampaignStatus'] != 'Sent'): ?>
                        <div class="tab-pane" id="tab-schedule">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="ScheduleType"><?php InterfaceLanguage('Screen', '0828', false, '', false, true); ?></label>
                                        <div class="col-md-4">
                                            <select class="form-control" id="ScheduleType" name="ScheduleType">
                                                <option value="Immediate" <?php echo set_select('ScheduleType', 'Immediate', $CampaignInformation['ScheduleType'] == 'Immediate'); ?>><?php InterfaceLanguage('Screen', '0829', false, '', false, false); ?></option>
                                                <option value="Future" <?php echo set_select('ScheduleType', 'Future', $CampaignInformation['ScheduleType'] == 'Future'); ?>><?php InterfaceLanguage('Screen', '0830', false, '', false, false); ?></option>
                                                <option value="Recursive" <?php echo set_select('ScheduleType', 'Recursive', $CampaignInformation['ScheduleType'] == 'Recursive'); ?>><?php InterfaceLanguage('Screen', '0831', false, '', false, false); ?></option>
                                                <option value="Not Scheduled" <?php echo set_select('ScheduleType', 'Not Scheduled', $CampaignInformation['ScheduleType'] == 'Not Scheduled'); ?>><?php InterfaceLanguage('Screen', '0832', false, '', false, false); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="send-time-zone" class="form-group">
                                        <label class="col-md-3 control-label" for="SendTimeZone"><?php InterfaceLanguage('Screen', '0016', false, '', false, true); ?></label>
                                        <div class="col-md-4">
                                            <select name="SendTimeZone" id="SendTimeZone" class="form-control">
                                                <?php
                                                foreach (Core::GetTimeZoneList() as $TimeZone):
                                                    ?>
                                                    <option value="<?php print($TimeZone); ?>" <?php echo set_select('SendTimeZone', html_entity_decode($TimeZone), ($UserInformation['TimeZone'] == html_entity_decode($TimeZone) ? true : false)) ?>><?php print($TimeZone); ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="send-date-time">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group clearfix">
                                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0833', false, '', false, true); ?></label>
                                            <div class="col-md-5">
                                                <div id="datepicker"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php
                                            $SendTime = explode(':', $CampaignInformation['SendTime']);
                                            $SendTime = $SendTime[0];
                                            ?>
                                            <?php
                                            $SendMinute = explode(':', $CampaignInformation['SendTime']);
                                            $SendMinute = $SendMinute[1];
                                            ?>
                                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0834', false, '', false, true); ?></label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="SendTimeHour">
                                                    <option value="00" <?php echo set_select('SendTimeHour', '00', $SendTime == '00') ?>>00</option>
                                                    <option value="01" <?php echo set_select('SendTimeHour', '01', $SendTime == '01') ?>>01</option>
                                                    <option value="02" <?php echo set_select('SendTimeHour', '02', $SendTime == '02') ?>>02</option>
                                                    <option value="03" <?php echo set_select('SendTimeHour', '03', $SendTime == '03') ?>>03</option>
                                                    <option value="04" <?php echo set_select('SendTimeHour', '04', $SendTime == '04') ?>>04</option>
                                                    <option value="05" <?php echo set_select('SendTimeHour', '05', $SendTime == '05') ?>>05</option>
                                                    <option value="06" <?php echo set_select('SendTimeHour', '06', $SendTime == '06') ?>>06</option>
                                                    <option value="07" <?php echo set_select('SendTimeHour', '07', $SendTime == '07') ?>>07</option>
                                                    <option value="08" <?php echo set_select('SendTimeHour', '08', $SendTime == '08') ?>>08</option>
                                                    <option value="09" <?php echo set_select('SendTimeHour', '09', $SendTime == '09') ?>>09</option>
                                                    <option value="10" <?php echo set_select('SendTimeHour', '10', $SendTime == '10') ?>>10</option>
                                                    <option value="11" <?php echo set_select('SendTimeHour', '11', $SendTime == '11') ?>>11</option>
                                                    <option value="12" <?php echo set_select('SendTimeHour', '12', $SendTime == '12') ?>>12</option>
                                                    <option value="13" <?php echo set_select('SendTimeHour', '13', $SendTime == '13') ?>>13</option>
                                                    <option value="14" <?php echo set_select('SendTimeHour', '14', $SendTime == '14') ?>>14</option>
                                                    <option value="15" <?php echo set_select('SendTimeHour', '15', $SendTime == '15') ?>>15</option>
                                                    <option value="16" <?php echo set_select('SendTimeHour', '16', $SendTime == '16') ?>>16</option>
                                                    <option value="17" <?php echo set_select('SendTimeHour', '17', $SendTime == '17') ?>>17</option>
                                                    <option value="18" <?php echo set_select('SendTimeHour', '18', $SendTime == '18') ?>>18</option>
                                                    <option value="19" <?php echo set_select('SendTimeHour', '19', $SendTime == '19') ?>>19</option>
                                                    <option value="20" <?php echo set_select('SendTimeHour', '20', $SendTime == '20') ?>>20</option>
                                                    <option value="21" <?php echo set_select('SendTimeHour', '21', $SendTime == '21') ?>>21</option>
                                                    <option value="22" <?php echo set_select('SendTimeHour', '22', $SendTime == '22') ?>>22</option>
                                                    <option value="23" <?php echo set_select('SendTimeHour', '23', $SendTime == '23') ?>>23</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1 control-label" style="width : 20px"> 
                                                : 
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" name="SendTimeMinute">
                                                    <option value="00" <?php echo set_select('SendTimeMinute', '00', $SendMinute == '00') ?>>00</option>
                                                    <option value="15" <?php echo set_select('SendTimeMinute', '15', $SendMinute == '15') ?>>15</option>
                                                    <option value="30" <?php echo set_select('SendTimeMinute', '30', $SendMinute == '30') ?>>30</option>
                                                    <option value="45" <?php echo set_select('SendTimeMinute', '45', $SendMinute == '45') ?>>45</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="send-repeatedly">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0851', false, '', false, true); ?></label>

                                            <div class="col-md-5">
                                                <select class="form-control" name="SendRepeteadlyDayType" id="SendRepeteadlyDayType">
                                                    <option value="Every day" <?php echo set_select('SendRepeteadlyDayType', 'Every day', $SendRepeatedlyDayType == 'Every day'); ?>><?php InterfaceLanguage('Screen', '0852', false, '', false, true); ?></option>
                                                    <option value="Every weekday" <?php echo set_select('SendRepeteadlyDayType', 'Every weekday', $SendRepeatedlyDayType == 'Every weekday'); ?>><?php InterfaceLanguage('Screen', '0853', false, '', false, false); ?></option>
                                                    <option value="Monday, wednesday and friday" <?php echo set_select('SendRepeteadlyDayType', 'Monday, wednesday and friday', $SendRepeatedlyDayType == 'Monday, wednesday and friday'); ?>><?php InterfaceLanguage('Screen', '0854', false, '', false, false); ?></option>
                                                    <option value="Tuesday and thursday" <?php echo set_select('SendRepeteadlyDayType', 'Tuesday and thursday', $SendRepeatedlyDayType == 'Tuesday and thursday'); ?>><?php InterfaceLanguage('Screen', '0855', false, '', false, false); ?></option>
                                                    <option value="Days of week" <?php echo set_select('SendRepeteadlyDayType', 'Days of week', $SendRepeatedlyDayType == 'Days of week'); ?>><?php InterfaceLanguage('Screen', '0856', false, '', false, false); ?></option>
                                                    <option value="Days of month" <?php echo set_select('SendRepeteadlyDayType', 'Days of month', $SendRepeatedlyDayType == 'Days of month'); ?>><?php InterfaceLanguage('Screen', '0857', false, '', false, false); ?></option>
                                                </select>
                                                <div id="SendRepeteadlyMonthDaysContainter" style="margin-top: 10px">
                                                    <input type="text" name="SendRepeatedlyMonthDays" value="<?php echo $CampaignInformation['ScheduleRecDaysOfMonth']; ?>" id="SendRepeatedlyMonthDays" class="form-control">
                                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0858', false, '', false, false); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="checkbox-row" id="SendRepeatedlyWeekDaysContainter">
                                                    <select class="multiselect no-height" name="SendRepeatedlyWeekDays[]" id="SendRepeatedlyWeekDays" multiple="multiple" size="7" style="width:130px;">
                                                        <option value="1" <?php echo set_select('SendRepeatedlyWeekDays', '1', in_array('1', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 0, false, false); ?></option>
                                                        <option value="2" <?php echo set_select('SendRepeatedlyWeekDays', '2', in_array('2', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 1, false, false); ?></option>
                                                        <option value="3" <?php echo set_select('SendRepeatedlyWeekDays', '3', in_array('3', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 2, false, false); ?></option>
                                                        <option value="4" <?php echo set_select('SendRepeatedlyWeekDays', '4', in_array('4', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 3, false, false); ?></option>
                                                        <option value="5" <?php echo set_select('SendRepeatedlyWeekDays', '5', in_array('5', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 4, false, false); ?></option>
                                                        <option value="6" <?php echo set_select('SendRepeatedlyWeekDays', '6', in_array('6', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 5, false, false); ?></option>
                                                        <option value="0" <?php echo set_select('SendRepeatedlyWeekDays', '0', in_array('0', $ScheduleRecDaysOfWeek)); ?>><?php InterfaceLanguage('Screen', '0859', false, 6, false, false); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0860', false, '', false, true); ?></label>
                                            <div class="col-md-5">
                                                <select class="form-control" name="SendRepeatedlyMonthType" id="SendRepeatedlyMonthType">
                                                    <option value="Every month" <?php echo set_select('SendRepeatedlyMonthType', 'Every month', $SendRepeatedlyMonthType == 'Every month'); ?>><?php InterfaceLanguage('Screen', '0861', false, '', false, false); ?></option>
                                                    <option value="Months of year" <?php echo set_select('SendRepeatedlyMonthType', 'Months of year', $SendRepeatedlyMonthType == 'Months of year'); ?>><?php InterfaceLanguage('Screen', '0863', false, '', false, false); ?></option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="checkbox-row" id="SendRepeatedlyMonthsContainter">
                                                    <select name="SendRepeatedlyMonths[]" id="SendRepeatedlyMonths" multiple class="multiselect no-height" size="12" style="width:130px;">
                                                        <option value="1" <?php echo set_select('SendRepeatedlyMonths', 1, in_array(1, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 0, false, false); ?></option>
                                                        <option value="2" <?php echo set_select('SendRepeatedlyMonths', 2, in_array(2, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 1, false, false); ?></option>
                                                        <option value="3" <?php echo set_select('SendRepeatedlyMonths', 3, in_array(3, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 2, false, false); ?></option>
                                                        <option value="4" <?php echo set_select('SendRepeatedlyMonths', 4, in_array(4, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 3, false, false); ?></option>
                                                        <option value="5" <?php echo set_select('SendRepeatedlyMonths', 5, in_array(5, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 4, false, false); ?></option>
                                                        <option value="6" <?php echo set_select('SendRepeatedlyMonths', 6, in_array(6, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 5, false, false); ?></option>
                                                        <option value="7" <?php echo set_select('SendRepeatedlyMonths', 7, in_array(7, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 6, false, false); ?></option>
                                                        <option value="8" <?php echo set_select('SendRepeatedlyMonths', 8, in_array(8, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 7, false, false); ?></option>
                                                        <option value="9" <?php echo set_select('SendRepeatedlyMonths', 9, in_array(9, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 8, false, false); ?></option>
                                                        <option value="10" <?php echo set_select('SendRepeatedlyMonths', 10, in_array(10, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 9, false, false); ?></option>
                                                        <option value="11" <?php echo set_select('SendRepeatedlyMonths', 11, in_array(11, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 10, false, false); ?></option>
                                                        <option value="12" <?php echo set_select('SendRepeatedlyMonths', 12, in_array(12, $ScheduleRecMonths)); ?>><?php InterfaceLanguage('Screen', '0862', false, 11, false, false); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0834', false, '', false, true); ?></label>
                                            <div class="col-md-1">
                                                <select name="ScheduleRecHours[]" id="ScheduleRecHours" class="multiselect no-height" multiple="multiple" size="4" style="width:auto;">
                                                    <option value="00" <?php echo set_select('ScheduleRecHours', '00', in_array('00', $ScheduleRecHours)); ?>>00</option>
                                                    <option value="01" <?php echo set_select('ScheduleRecHours', '01', in_array('01', $ScheduleRecHours)); ?>>01</option>
                                                    <option value="02" <?php echo set_select('ScheduleRecHours', '02', in_array('02', $ScheduleRecHours)); ?>>02</option>
                                                    <option value="03" <?php echo set_select('ScheduleRecHours', '03', in_array('03', $ScheduleRecHours)); ?>>03</option>
                                                    <option value="04" <?php echo set_select('ScheduleRecHours', '04', in_array('04', $ScheduleRecHours)); ?>>04</option>
                                                    <option value="05" <?php echo set_select('ScheduleRecHours', '05', in_array('05', $ScheduleRecHours)); ?>>05</option>
                                                    <option value="06" <?php echo set_select('ScheduleRecHours', '06', in_array('06', $ScheduleRecHours)); ?>>06</option>
                                                    <option value="07" <?php echo set_select('ScheduleRecHours', '07', in_array('07', $ScheduleRecHours)); ?>>07</option>
                                                    <option value="08" <?php echo set_select('ScheduleRecHours', '08', in_array('08', $ScheduleRecHours)); ?>>08</option>
                                                    <option value="09" <?php echo set_select('ScheduleRecHours', '09', in_array('09', $ScheduleRecHours)); ?>>09</option>
                                                    <option value="10" <?php echo set_select('ScheduleRecHours', '10', in_array('10', $ScheduleRecHours)); ?>>10</option>
                                                    <option value="11" <?php echo set_select('ScheduleRecHours', '11', in_array('11', $ScheduleRecHours)); ?>>11</option>
                                                    <option value="12" <?php echo set_select('ScheduleRecHours', '12', in_array('12', $ScheduleRecHours)); ?>>12</option>
                                                    <option value="13" <?php echo set_select('ScheduleRecHours', '13', in_array('13', $ScheduleRecHours)); ?>>13</option>
                                                    <option value="14" <?php echo set_select('ScheduleRecHours', '14', in_array('14', $ScheduleRecHours)); ?>>14</option>
                                                    <option value="15" <?php echo set_select('ScheduleRecHours', '15', in_array('15', $ScheduleRecHours)); ?>>15</option>
                                                    <option value="16" <?php echo set_select('ScheduleRecHours', '16', in_array('16', $ScheduleRecHours)); ?>>16</option>
                                                    <option value="17" <?php echo set_select('ScheduleRecHours', '17', in_array('17', $ScheduleRecHours)); ?>>17</option>
                                                    <option value="18" <?php echo set_select('ScheduleRecHours', '18', in_array('18', $ScheduleRecHours)); ?>>18</option>
                                                    <option value="19" <?php echo set_select('ScheduleRecHours', '19', in_array('19', $ScheduleRecHours)); ?>>19</option>
                                                    <option value="20" <?php echo set_select('ScheduleRecHours', '20', in_array('20', $ScheduleRecHours)); ?>>20</option>
                                                    <option value="21" <?php echo set_select('ScheduleRecHours', '21', in_array('21', $ScheduleRecHours)); ?>>21</option>
                                                    <option value="22" <?php echo set_select('ScheduleRecHours', '22', in_array('22', $ScheduleRecHours)); ?>>22</option>
                                                    <option value="23" <?php echo set_select('ScheduleRecHours', '23', in_array('23', $ScheduleRecHours)); ?>>23</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1 control-label" style="width:20px">
                                                : 
                                            </div>
                                            <div class="col-md-1">
                                                <select name="ScheduleRecMinutes[]" id="ScheduleRecMinutes" class="multiselect no-height" multiple="multiple" size="4" style="width:auto;">
                                                    <option value="00" <?php echo set_select('ScheduleRecMinutes', '00', in_array('00', $ScheduleRecMinutes)); ?>>00</option>
                                                    <option value="15" <?php echo set_select('ScheduleRecMinutes', '15', in_array('15', $ScheduleRecMinutes)); ?>>15</option>
                                                    <option value="30" <?php echo set_select('ScheduleRecMinutes', '30', in_array('30', $ScheduleRecMinutes)); ?>>30</option>
                                                    <option value="45" <?php echo set_select('ScheduleRecMinutes', '45', in_array('45', $ScheduleRecMinutes)); ?>>45</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="ScheduleRecSendMaxInstance"><?php InterfaceLanguage('Screen', '0868', false, '', false, true); ?></label>
                                            <div class="col-md-2">
                                                <input type="text" name="ScheduleRecSendMaxInstance" value="<?php echo set_value('ScheduleRecSendMaxInstance', $CampaignInformation['ScheduleRecSendMaxInstance']); ?>" id="ScheduleRecSendMaxInstance" class="form-control"  />                                                                    
                                            </div>
                                            <div class="col-md-4">
                                                <span class="help-block"><?php InterfaceLanguage('Screen', '0869', false, '', false, false); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group-note">
                                                <strong  class="control-label" style="width:auto"><?php InterfaceLanguage('Screen', '0870', false, '', false, false); ?>:</strong> 
                                                <span id="in-words"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </form>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-2">
                        <?php if (InterfacePrivilegeCheck('Campaign.Update', $UserInformation)): ?>
                            <a class="btn default" targetform="campaign-edit" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_edit.js" type="text/javascript" charset="utf-8"></script>		
<?php if ($CampaignInformation['SplitTest'] != false): ?>
    <script src="<?php InterfaceTemplateURL(); ?>js/screens/user/split_test_settings.js" type="text/javascript" charset="utf-8"></script>		
<?php endif; ?>
<script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
<script>
    var Language = {
        '0304': '<?php InterfaceLanguage('Screen', '0304', false, '', true); ?>',
        '0761': '<?php InterfaceLanguage('Screen', '0761', false, '', true); ?>',
        '0751': '<?php InterfaceLanguage('Screen', '0751', false, '', true); ?>',
        '0811': '<?php InterfaceLanguage('Screen', '0811', false, '', true, false); ?>',
        '0812': '<?php InterfaceLanguage('Screen', '0812', false, '', true, false); ?>',
        '0828': '<?php InterfaceLanguage('Screen', '0828', false, '', true); ?>',
        '0851': '<?php echo strtolower(InterfaceLanguage('Screen', '0851', true, '', false)); ?>',
        '0852': '<?php InterfaceLanguage('Screen', '0852', false, '', false); ?>',
        '0853': '<?php InterfaceLanguage('Screen', '0853', false, '', false); ?>',
        '0854': '<?php InterfaceLanguage('Screen', '0854', false, '', false); ?>',
        '0855': '<?php InterfaceLanguage('Screen', '0855', false, '', false); ?>',
        '0859': ['<?php echo implode("','", InterfaceLanguage('Screen', '0859', true, '', false, false)); ?>'],
        '0861': '<?php echo strtolower(InterfaceLanguage('Screen', '0861', true, '', false)); ?>',
        '0862': ['<?php echo implode("','", InterfaceLanguage('Screen', '0862', true, '', false, false)); ?>'],
        '0864': '<?php InterfaceLanguage('Screen', '0864', false, '', false); ?>',
        '0865': '<?php InterfaceLanguage('Screen', '0865', false, '', false); ?>',
        '0866': '<?php InterfaceLanguage('Screen', '0866', false, '', false); ?>',
        '0867': '<?php echo strtolower(InterfaceLanguage('Screen', '0867', true, '', false)); ?>',
        '0871': '<?php echo strtolower(InterfaceLanguage('Screen', '0871', true, '', false)); ?>'
    };

    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
    var template_url = '<?php InterfaceInstallationURL(); ?>api.php';
    var CampaignID = <?php echo $CampaignInformation['CampaignID']; ?>;
    var screen_type = 'edit';
    var cf = '';
<?php if ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused' || $CampaignInformation['CampaignStatus'] == 'Sent'): ?>
        cf = 'no';
<?php endif; ?>

    $(document).ready(function () {
        all_library.init_handlers();
        settings_library.init_handlers();
        schedule_library.init_handlers();
    });
</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>