
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="tags-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="tags-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="tags-table-form"><?php InterfaceLanguage('Screen', '1085'); ?></a>
<!--<i class="icon-microphone font-purple-sharp"></i>-->
<!--<span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0723', false, '', false); ?> </span>-->                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >                        
                        <!--<a href="#" class="btn default btn-transparen btn-sm" id="create-tag-link"><?php InterfaceLanguage('Screen', '1086'); ?></a>-->
                        <a class="btn default btn-transparen btn-sm" data-toggle="modal" href="#basic"> <?php InterfaceLanguage('Screen', '1086'); ?> </a>
                        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title bold"><?php InterfaceLanguage('Screen', '1086'); ?></h4>
                                    </div>
                                    <form id="create-tag-form" action="<?php InterfaceAppURL(); ?>/user/tags/" method="post">
                                        <div class="modal-body"> 
                                            <input type="hidden" name="Command" value="CreateTag" id="Command">
                                            <!--<input type="hidden" name="Tag" value="" id="Tag">-->
                                            <input type="hidden" name="TagID" value="" id="TagID">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="Tag"><?php InterfaceLanguage('Screen', '1091', false, '', false, true); ?>:</label>                        
                                                    <div class="col-md-8">
                                                        <input type="text" name="Tag" required value="<?php echo set_value('Tag'); ?>" id="Tag" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn default"/>
                                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <form id="tags-table-form" action="<?php InterfaceAppURL(); ?>/user/tags/" method="post">
                        <input type="hidden" name="Command" value="DeleteTags" id="Command">
                        <input type="hidden" name="Tag" value="" id="Tag">
                        <input type="hidden" name="TagID" value="" id="TagID">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <?php
                                if (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                    <?php
                                elseif (isset($PageErrorMessage) == true):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                    <?php
                                endif;
                                ?>

                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="grid table table-bordered" id="tags-table">
                                <tr>
                                    <th colspan="2"><?php InterfaceLanguage('Screen', '0051', false, '', true); ?></th>
                                </tr>
                                <?php
                                if (count($Tags) < 1):
                                    ?>
                                    <tr>
                                        <td colspan="2"><?php InterfaceLanguage('Screen', '1087'); ?></td>
                                    </tr>
                                    <?php
                                endif;
                                foreach ($Tags as $EachTag):
                                    ?>
                                    <tr>
                                        <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedTags[]" value="<?php print($EachTag['TagID']); ?>" id="SelectedTags<?php print($EachTag['TagID']); ?>"></td>
                                        <td width="500" class="">
                                            <a href="#" tagid="<?php echo $EachTag['TagID']; ?>" class="update-link"><?php print($EachTag['Tag']); ?></a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1088': '<?php InterfaceLanguage('Screen', '1088', false, '', false); ?>',
            '1091': '<?php InterfaceLanguage('Screen', '1091', false, '', false, true); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/tags.js" type="text/javascript" charset="utf-8"></script>		
