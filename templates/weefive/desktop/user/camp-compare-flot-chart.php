<script>
    var ChartsFlotcharts = function () {

        return {
            //main function to initiate the module

            init: function () {

//                App.addResizeHandler(function () {
//                    Charts.initPieCharts();
//                });

            },
            initCharts: function () {

                if (!jQuery.plot) {
                    return;
                }
                var results = [];
                var campaign_ids

                function chart3(no_of_days) {
                    if ($('#chart_3').size() != 1) {
                        return;
                    }

                    var url_camps_compare = "<?php echo InterfaceAppURL() . '/user/campaigns/comparestatisticsCustom/' ?>";

                    var campaign_idsStr = "<?php print_r($CampaignIDs); ?>";
                    campaign_ids = campaign_idsStr.split("-").map(Number);
                    var len = campaign_ids.length;

                    if (len > 0)
                    {
                        draw_chart(no_of_days, 'opens');
                    }

                    function showTooltip(x, y, contents) {
                        $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 15,
                            border: '1px solid #333',
                            padding: '4px',
                            color: '#fff',
                            'border-radius': '3px',
                            'background-color': '#333',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                    }

                    var previousPoint = null;


                    $("#chart_3").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));

                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                        y = item.datapoint[1].toFixed(2);

                                var item_pos = Math.round(x);
                                var seriesIndex = item.seriesIndex;
                                var title = null;

                                console.log(item_pos)
                                console.log(seriesIndex)



                                title = "Day " + item_pos + " : ";
                                for (i = 0; i < campaign_ids.length; i++) {
                                    
                                    var cam_name = results[i][0].CampaignName;
                                    var value = results[i][item_pos - 1].Total;
                                    title += cam_name + " = " + value + " ";
                                    console.log(title)
                                }
                                console.log(title);

//                                if (seriesIndex == 0)
//                                {
//                                    //opens
//                                    title = opens[Math.round(item_pos - 1)].date;
//                                } else if (seriesIndex == 1)
//                                {
//                                    //clicks
//                                    title = clicks[Math.round(item_pos - 1)].date;
//                                } else if (seriesIndex == 2)
//                                {
//                                    //unsubscribtions
//                                    title = unsubscription[Math.round(item_pos - 1)].date;
//                                } else if (seriesIndex == 3)
//                                {
//                                    //forwards
//                                    title = forwards[Math.round(item_pos - 1)].date;
//                                }


                                showTooltip(item.pageX, item.pageY, title);
//                                showTooltip(item.pageX, item.pageY, item.series.label + " of " + title + " = " + y);
                            }

                        } else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });

                    $("#chart_3").bind("plotclick", function (event, pos, item) {

                        if (item) {

                            var item_pos = Math.round(pos.x);
                            var seriesIndex = item.seriesIndex;

//                            console.log(item_pos);
//                            console.log(item);

                            var title = null;
                            var date = null;

                            if (seriesIndex == 0)
                            {
                                //opens
                                title = 'opens';
                                date = opens[Math.round(item_pos - 1)].date;
                            } else if (seriesIndex == 1)
                            {
                                //clicks
                                title = 'clicks';
                                date = clicks[Math.round(item_pos - 1)].date;
                            } else if (seriesIndex == 2)
                            {
                                //unsubscribtions
                                title = 'unsubscriptions';
                                date = unsubscription[Math.round(item_pos - 1)].date;
                            } else if (seriesIndex == 3)
                            {
                                //forwards
                                title = 'forwards';
                                date = forwards[Math.round(item_pos - 1)].date;
                            }
//                            console.log(item.seriesIndex);
//                            console.log(clicks);
//                            console.log(title);
//                            console.log(date);
//                            console.log(clicks[Math.round(item_pos)]);

                            var link = "<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/" + title + "/" + date;
//                            window.open(link, '_blank');
//                            console.log(link);
                        }
                    });

                    function draw_chart(no_of_days, statistics)
                    {
                        $.ajax({
                            type: "POST",
                            data: {
                                'campaign_ids': campaign_idsStr,
                                'statistics': statistics,
                                'days': no_of_days
                            },
                            url: url_camps_compare,
                            success: function (data) {
                                results = JSON.parse(data);
                                console.log(results)
                            },
                            async: false // <- this turns it into synchronous
                        });

                        var all_data = [];

                        results.forEach(function (campaign) {
//                            console.log(campaign);
                            var chart_data = [];
                            var campaign_name = null;

                            campaign.forEach(function (entry) {
                                chart_data.push([entry.day, parseInt(entry.Total)]);
                                campaign_name = entry.CampaignName;
                            });
//                                console.log(chart_data);
                            all_data.push({
                                data: chart_data,
                                label: campaign_name,
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        });

                        plot = $.plot($("#chart_3"), all_data, {
                            series: {
                                lines: {
                                    show: true,
                                    lineWidth: 2,
                                    fill: true,
                                    fillColor: {
                                        colors: [{
                                                opacity: 0.05
                                            }, {
                                                opacity: 0.01
                                            }]
                                    }
                                },
                                points: {
                                    show: true,
                                    radius: 3,
                                    lineWidth: 1
                                },
                                shadowSize: 2
                            },
                            crosshair: {
                                mode: "x"
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                autoHighlight: false,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            },
                            colors: ["#D91E18", "#2AB4C0", "#8E44AD", "#FF8040", "#A52D69", "#FF00FF"],
                            yaxis: {
                                min: 0
                            },
                            xaxis: {
                                min: 1,
                                max: no_of_days,
                                ticks: 100,
                                tickDecimals: 0
                            }
                        });

                    }
                    $('input[name="CheckboxGraph"]').change(function () {
                        var statistics = null;
                        statistics = getStatisticsName();
                        draw_chart($('#DaysSelect').val(), statistics);
                    });

                }
                function getStatisticsName()
                {
                    return $('input[name=CheckboxGraph]:checked').val();
                }

                $('#DaysSelect').on('change', function () {
                    $("#CheckboxOpens").prop('checked', true);
                    var statistics = null;
                    statistics = getStatisticsName();
                    chart3($(this).val(), statistics);
                });

                chart3($('#DaysSelect').val());
            },
        };
    }();

    jQuery(document).ready(function () {
        ChartsFlotcharts.init();
        ChartsFlotcharts.initCharts();



    });
</script>

