<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>
<?php // include_once(TEMPLATE_PATH . 'desktop/user/home-flot-chart.php'); ?>

<?php
//print_r('<pre>');
//print_r($ListInformation);
//print_r('</pre>');
$BounceStatisticsArr = $ListInformation['BounceStatisticsArr'];
?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list-flot-chart.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/list-pie-chart.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/list-knob-dial.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list-geo-location.php'); ?>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />

<link href="<?php InterfaceTemplateURL(false); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL STYLES -->
<?php // print_r($ListInformation['BounceStatistics']) ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet">
                            <div class="portlet-body">
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if (isset($PageSuccessMessage) == true): ?>
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <?php print($PageSuccessMessage); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet light  portlet-fit">
                                            <div class="portlet-title">
                                                <div class="caption" style="padding: 0">
                                                    <div class="form-group" style="margin-bottom: 0">
                                                        <select class="form-control input-small" id="DaysSelect">
                                                            <option value="7" selected>7 days</option>
                                                            <option value="15">15 days</option>
                                                            <option value="30">30 days</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="actions" style="padding: 0">
                                                    <div class="form-group" style="margin-bottom: 0">
                                                        <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxSubscriptions" value="<?php InterfaceLanguage('Screen', '0141') ?>"> <?php InterfaceLanguage('Screen', '0141') ?>
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxUnsubscriptions" value="<?php InterfaceLanguage('Screen', '0841') ?>"> <?php InterfaceLanguage('Screen', '0841') ?>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="chart_3" class="chart" style="padding: 0px; position: relative;">
                                                    <canvas class="flot-base" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                    <canvas class="flot-overlay" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pull-left">
                                        <div class="portlet portlet-fit ">
                                            <div class="portlet-body">
                                                <div class="row" style="margin: 0">
                                                    <div class="col-md-6" style="padding: 0">
                                                        <div style="display:inline;width:200px;height:200px;"> 
                                                            <input class="knob" data-fgColor="#1ac6ff" data-skin="tron" style="border: 0px; -webkit-appearance: none; background: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="dashboard-stat2">
                                                                    <div class="display">
                                                                        <div class="number">
                                                                            <h3>
                                                                                <a href="<?php echo InterfaceAppUrl(); ?>/user/subscribers/browse/<?php echo $ListInformation['ListID']; ?>">
                                                                                    <span data-counter="counterup" data-value="<?php echo $ListInformation['SubscriberCount'] ?>"><?php echo $ListInformation['SubscriberCount'] ?></span>
                                                                                </a>
                                                                            </h3>
                                                                            <small><?php InterfaceLanguage('Screen', '0104', false); ?></small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="dashboard-stat2">
                                                                    <div class="display">
                                                                        <div class="number">
                                                                            <h3>
                                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/unsubscriptions">
                                                                                    <span data-counter="counterup" data-value="<?php echo $ListInformation['UnsubscribesStatistics']['TotalUnsubscriptions'] ?>"><?php echo $ListInformation['UnsubscribesStatistics']['TotalUnsubscriptions'] ?></span>
                                                                                </a>
                                                                            </h3>
                                                                            <small><?php InterfaceLanguage('Screen', '9205', false, '', false, false); ?></small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 40px">
                                                    <div class="col-md-3">
                                                        <div class="easy-pie-chart">
                                                            <div class="number transactions" data-percent="<?php echo $ListInformation['OverallOpenPerformance']; ?>">
                                                                <span><?php echo $ListInformation['OverallOpenPerformance']; ?></span>% <canvas height="75" width="75"></canvas></div>
                                                            <span class="title"> 
                                                                <?php InterfaceLanguage('Screen', '0975', false, '', false, true); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="margin-bottom-10 visible-sm"> </div>
                                                    <div class="col-md-3">
                                                        <div class="easy-pie-chart">
                                                            <div class="number visits" data-percent="<?php echo $ListInformation['OverallAccountOpenPerformance']; ?>">
                                                                <span><?php echo $ListInformation['OverallAccountOpenPerformance']; ?></span>% <canvas height="75" width="75"></canvas></div>
                                                            <span class="title"> 
                                                                <?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?>
                                                                <span class="data data-blue"><?php echo $ListInformation['OverallAccountOpenPerformance']; ?>%</span> <span class="data data-orange" style="color:#<?php if ($ListInformation['OpenPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['OpenPerformanceDifference']; ?>%)</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="margin-bottom-10 visible-sm"> </div>
                                                    <div class="col-md-3">
                                                        <div class="easy-pie-chart">
                                                            <div class="number visits" data-percent="<?php echo $ListInformation['OverallClickPerformance']; ?>">
                                                                <span><?php echo $ListInformation['OverallClickPerformance']; ?></span>% <canvas height="75" width="75"></canvas></div>
                                                            <span class="title"> 
                                                                <?php InterfaceLanguage('Screen', '0976', false, '', false, true); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="margin-bottom-10 visible-sm"> </div>
                                                    <div class="col-md-3">
                                                        <div class="easy-pie-chart">
                                                            <div class="number visits" data-percent="<?php echo $ListInformation['OverallAccountClickPerformance']; ?>">
                                                                <span><?php echo $ListInformation['OverallAccountClickPerformance']; ?></span>% <canvas height="75" width="75"></canvas></div>
                                                            <span class="title"> 
                                                                <?php InterfaceLanguage('Screen', '0888', false, '', false, true); ?>
                                                                <span class="data data-blue"><?php echo $ListInformation['OverallAccountClickPerformance']; ?>%</span> <span class="data data-orange" style="color:#<?php if ($ListInformation['ClickPerformanceDifference'] > 0): ?>259E01<?php else: ?>FF9701<?php endif; ?>;">(<?php echo $ListInformation['ClickPerformanceDifference']; ?>%)</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix" style="margin-left: 0">
                                    <div class="col-md-2" style="background: #fff">
                                        <div class="easy-pie-chart">
                                            <div class="number visits" data-percent="<?php echo $ListInformation['TotalSpamComplaints']['TotalSPAMPercent']; ?>">
                                                <span><?php echo $ListInformation['TotalSpamComplaints']['TotalSPAMPercent']; ?></span>% <canvas height="75" width="75"></canvas></div>
                                            <span class="title"> 
                                                <?php InterfaceLanguage('Screen', '0095', false, '', false, true); ?>
                                            </span>
                                        </div>
                                        <br/>
                                        <div class="dashboard-stat2">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/spams">
                                                            <span data-counter="counterup" data-value="<?php echo $ListInformation['TotalSpamComplaints']['TotalSPAMComplaints']; ?>"><?php echo $ListInformation['TotalSpamComplaints']['TotalSPAMComplaints']; ?></span>
                                                        </a>
                                                    </h3>
                                                    <small><?php InterfaceLanguage('Screen', '0095', false, '', false, true); ?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/opens">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['OpenStatistics']['TotalOpens']); ?>"><?php print($ListInformation['OpenStatistics']['TotalOpens']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '0837'); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/unique_opens">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['OpenStatistics']['UniqueOpens']); ?>"><?php print($ListInformation['OpenStatistics']['UniqueOpens']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '0094'); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/clicks">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['ClickStatistics']['TotalClicks']); ?>"><?php print($ListInformation['ClickStatistics']['TotalClicks']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '0838', false, '', false, false); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/unique_clicks">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['ClickStatistics']['UniqueClicks']); ?>"><?php print($ListInformation['ClickStatistics']['UniqueClicks']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '1390', false, '', false, false); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/forwards">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['ForwardStatistics']['TotalForwards']); ?>"><?php print($ListInformation['ForwardStatistics']['TotalForwards']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '0839', false); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                    <div class="display">
                                                        <div class="number">
                                                            <h3>
                                                                <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/unique_forwards">
                                                                    <span data-counter="counterup" data-value="<?php print($ListInformation['ForwardStatistics']['UniqueForwards']); ?>"><?php print($ListInformation['ForwardStatistics']['UniqueForwards']); ?></span>
                                                                </a>
                                                            </h3>
                                                            <small><?php InterfaceLanguage('Screen', '9125', false, '', false, false); ?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-left: 0">
                                        <div class="col-md-8">
                                            <div class="portlet">
                                                <div class="portlet-body">
                                                    <div id="donut" class="chart" style="height: 210px;padding: 0px; position: relative;margin-top: 30px">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row" style="margin: 0;margin-top: 10px">
                                                <div class="col-md-12">
                                                    <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                        <div class="display">
                                                            <div class="number">
                                                                <h3>
                                                                    <!--<a href="">-->
                                                                    <span data-counter="counterup" data-value="<?php echo $BounceStatisticsArr['Not Bounced']['count'] ?>"><?php echo $BounceStatisticsArr['Not Bounced']['count'] ?></span>
                                                                    <!--</a>-->
                                                                </h3>
                                                                <small><?php InterfaceLanguage('Screen', '1171', false); ?></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                        <div class="display">
                                                            <div class="number">
                                                                <h3>
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/soft_bounces">
                                                                        <span data-counter="counterup" data-value="<?php echo $BounceStatisticsArr['Soft']['count'] ?>"><?php echo $BounceStatisticsArr['Soft']['count'] ?></span>
                                                                    </a>
                                                                </h3>
                                                                <small><?php InterfaceLanguage('Screen', '9213', false, '', false, false); ?></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" >
                                                    <div class="dashboard-stat2 dashboard-stat2-vertical">
                                                        <div class="display">
                                                            <div class="number">
                                                                <h3>
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/list/reports/<?php echo $ListInformation['ListID']; ?>/hard_bounces">
                                                                        <span data-counter="counterup" data-value="<?php echo $BounceStatisticsArr['Hard']['count'] ?>"><?php echo $BounceStatisticsArr['Hard']['count'] ?></span>
                                                                    </a>
                                                                </h3>
                                                                <small><?php InterfaceLanguage('Screen', '9208', false); ?></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix" style="margin-top: 30px;">
                                    <div class="col-md-12">
                                        <div class="portlet light  portlet-fit">
                                            <div class="portlet-body">
                                                <div id="region_statistics_loading">
                                                    <img src="<?php InterfaceTemplateURL(); ?>assets/global/img/loading.gif" alt="loading" /> 
                                                </div>
                                                <div id="region_statistics_content" class="display-none">
                                                    <div class="btn-toolbar margin-bottom-10">
                                                        <div class="btn-group btn-group-circle" data-toggle="buttons">
                                                            <a href="" id="open_geo_home" class="btn grey-salsa btn-sm active"> Subscribers </a>
                                                        </div>
                                                        <div class="btn-group pull-right">
                                                            <a href="" class="btn btn-circle grey-salsa btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                                                <span id="option_selected" >All</span>
                                                                <span class="fa fa-angle-down"> </span>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li>
                                                                    <a href="javascript:;" id="view_geo_all"> All </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;" id="view_geo_last_3_month"> Last 3 month </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;" id="view_geo_last_6_month"> Last 6 month </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;" id="view_geo_last_year"> Last year  </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div id="vmap_world" class="vmaps display-none"> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>