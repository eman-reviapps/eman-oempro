<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<script src="<?php InterfaceTemplateURL(false); ?>js/swfobject.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign-flot-chart.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign-pie-chart.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign-knob-dial.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign-geo-location.php'); ?>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!--<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/charts-amcharts.js" type="text/javascript"></script>-->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />

<link href="<?php InterfaceTemplateURL(false); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

<?php include_once(TEMPLATE_PATH . 'desktop/user/campaign_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($PageSuccessMessage) == true):
                ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageSuccessMessage); ?>
                </div>
                <?php
            elseif (isset($PageErrorMessage) == true):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageErrorMessage); ?>
                </div>
                <?php
            endif;
            ?>
        </div>
    </div>
    <?php
    $SentRatio = ceil((100 * $CampaignInformation['TotalSent']) / $EstimatedRecipients);
    ?>
    <?php if ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused') { ?>
        <div class="row" style="margin-bottom: 0px;">
            <div class="col-md-12">
                <!--<div class="font-purple-sharp sbold"> <?php echo $CampaignInformation['CampaignStatus'] ?></div>-->

                <div class="status">
                    <div class="status-number"> <?php echo $CampaignInformation['TotalSent'] ?> / <?php echo $EstimatedRecipients; ?></div>
                </div>
                <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="padding-top: 5px;width: <?php echo $SentRatio ?>%">
                        <span class="bold" style="font-size: 15px"> <?php echo $SentRatio ?>% </span>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row" style="margin-bottom: 0px;">
        <div class="col-md-12">
            <div class="col-md-5 pull-left" style="background: #fff" >
                <div class="portlet sale-summary" style="">
                    <div class="portlet-title">
                        <div class="caption font-purple-sharp sbold"> Info </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="list-unstyled">
                            <li>
                                <span class="sale-info sale-info-custom bold"> Subject 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> <?php echo $CampaignInformation['CampaignName'] ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Sender 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <?php // print_r($CampaignInformation) ?>
                                <span class="sale-num sale-num-custom"> <?php echo $EmailInformation['FromName'] . ' &lt;' . $EmailInformation['FromEmail'] . '&gt;'; ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Sent on
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> <?php echo $CampaignInformation['SendProcessFinishedOn'] ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Recipients 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> 
                                    <?php
                                    $i = 0;
                                    $visible_count = 1;
                                    foreach ($ArrayRecipientLists as $EachList) {
                                        if (InterfacePrivilegeCheck('List.Get', $UserInformation)) {
                                            ?>
                                            <a class="bold" href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php print($EachList['ListID']); ?>"><?php print($EachList['Name']); ?></a>
                                            <?php
                                        } else {
                                            print($EachList['Name']);
                                        }
                                        //
                                        if ($i == $visible_count - 1 && $visible_count != count($ArrayRecipientLists)) {
                                            echo '<span id="more_lists" >and <a id="more_link" class="bold">' . (count($ArrayRecipientLists) - $visible_count ) . ' more.. </span> ' . '</a><span id="listIds" style="display:none">';
                                        }
                                        if ($i == count($ArrayRecipientLists) - 1 && count($ArrayRecipientLists) > $visible_count) {
                                            echo '</span>';
                                        }
                                        if ($i != count($ArrayRecipientLists) - 1) {
                                            echo " , ";
                                        } else {
                                            echo '<a id="showless" class="bold" style="display:none">show less</a>';
                                        }
                                        $i++;
                                    }
                                    ?>

                                </span>
                            </li>
                        </ul>
                        <div class="caption" style="padding-bottom: 10px;">
                            <!-- Pause - Start -->
                            <a id="campaign-pause-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Sending') ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/pause/<?php echo $CampaignInformation['CampaignID']; ?>"><strong><?php InterfaceLanguage('Screen', '0926', false, '', false, true); ?></strong></a>
                            <!-- Pause - End -->

                            <!-- Cancel schedule - Start -->
                            <a id="campaign-cancel-schedule-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Ready' && ($CampaignInformation['ScheduleType'] == 'Future' || $CampaignInformation['ScheduleType'] == 'Recursive')) ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/cancelschedule/<?php echo $CampaignInformation['CampaignID']; ?>">strong><?php InterfaceLanguage('Screen', '0928', false, '', false, true); ?></strong></a>
                            <!-- Cancel schedule - End -->

                            <!-- Cancel sending - Start -->
                            <a id="campaign-cancel-sending-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Sending' || ($CampaignInformation['CampaignStatus'] == 'Ready' && $CampaignInformation['ScheduleType'] == 'Immediate')) ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/cancelsending/<?php echo $CampaignInformation['CampaignID']; ?>"><strong><?php InterfaceLanguage('Screen', '0927', false, '', false, true); ?></strong></a>
                            <!-- Cancel sending - End -->

                            <!-- Resume - Start -->
                            <a id="campaign-resume-button" style="<?php echo ($CampaignInformation['CampaignStatus'] != 'Paused') ? 'display:none' : ''; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/resume/<?php echo $CampaignInformation['CampaignID']; ?>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0929', false, '', false, true); ?></strong></a>
                            <!-- Resume - End -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pull-right">
                <div class="portlet portlet-fit ">
                    <div class="portlet-body">
                        <div class="col-md-6" style="padding: 0">
                            <div style="display:inline;width:200px;height:200px;"> 
                                <input class="knob" data-fgColor="#1ac6ff" data-skin="tron" style="border: 0px; -webkit-appearance: none; background: none;">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet sale-summary">
                                <div class="portlet-title">
                                    <div class="caption font-purple-sharp sbold"> Deliverability </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0147', false); ?>
                                                <i class="fa fa-img-up"></i>
                                            </span>
                                            <span class="sale-num bold"> <?php echo $CampaignInformation['TotalSent']; ?></span> </span>
                                        </li>
                                        <?php if ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused'): ?>
                                            <li>
                                                <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0147', false, '', false, false); ?>
                                                    <i class="fa fa-img-down"></i>
                                                </span>
                                                <span class="sale-num bold"> <?php echo $CampaignInformation['TotalSent'] ?> </span>
                                            </li>
                                        <?php endif; ?>
                                        <li>
                                            <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0953', false, '', false, false); ?>
                                                <i class="fa fa-img-down"></i>
                                            </span>
                                            <span class="sale-num bold"> <?php echo $CampaignInformation['TotalRecipients'] ?> </span>
                                        </li>
                                        <li>
                                            <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0954', false, '', false, false); ?> </span>
                                            <span class="sale-num bold"> <?php echo $CampaignInformation['TotalFailed']; ?> </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/opens">
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalOpens']; ?>"><?php echo $CampaignInformation['TotalOpens']; ?></span>
                            </a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0837') ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $OpenedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom ">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/clicks">
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalClicks']; ?>"><?php echo $CampaignInformation['TotalClicks']; ?></span>
                            </a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0838') ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $ClickedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/forwards">
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalForwards']; ?>"><?php echo $CampaignInformation['TotalForwards']; ?></span>
                            </a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0839'); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $ForwardedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/unsubscriptions">
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalUnsubscriptions']; ?>"><?php echo $CampaignInformation['TotalUnsubscriptions']; ?></span>
                            </a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0097', false, '', false, true); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $UnsubscriptionRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <!--<a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/bounces">-->
                            
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalBounces']; ?>"><?php echo $CampaignInformation['TotalBounces']; ?></span>
                            
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0098', false, '', false, true); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $BounceRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <!--<a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/hard_bounces">-->
                           
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalHardBounces']; ?>"><?php echo $CampaignInformation['TotalHardBounces']; ?></span>
                          
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '1113', false, '', false, true); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $HardBouncedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-md-custom">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3>
                            <!--<a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $CampaignInformation['CampaignID']; ?>/spams">-->
                            
                                <span data-counter="counterup" data-value="<?php echo $CampaignInformation['TotalSpamReports']; ?>"><?php echo $CampaignInformation['TotalSpamReports']; ?></span>
                           
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0961', false, '', false, false); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $SpamRatio ?>% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light  portlet-fit">
                <div class="portlet-title">
                    <div class="caption" style="padding: 0">
                        <div class="form-group" style="margin-bottom: 0">
                            <select class="form-control input-small" id="DaysSelect">
                                <option value="7" selected>7 days</option>
                                <option value="15">15 days</option>
                                <option value="30">30 days</option>
                            </select>
                        </div>
                    </div>
                    <div class="actions" style="padding: 0">
                        <div class="form-group" style="margin-bottom: 0">
                            <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxOpens" value="<?php InterfaceLanguage('Screen', '0837') ?>"> <?php InterfaceLanguage('Screen', '0837') ?>
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxClicks" value="<?php InterfaceLanguage('Screen', '0838') ?>"> <?php InterfaceLanguage('Screen', '0838') ?>
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxUnsubscriptions" value="<?php InterfaceLanguage('Screen', '0841') ?>"> <?php InterfaceLanguage('Screen', '0841') ?>
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxForwards" value="<?php InterfaceLanguage('Screen', '0839') ?>"> <?php InterfaceLanguage('Screen', '0839') ?>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart_3" class="chart" style="padding: 0px; position: relative;">
                        <canvas class="flot-base" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                        <canvas class="flot-overlay" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light  portlet-fit">
                <div class="portlet-body">
                    <div id="region_statistics_loading">
                        <img src="<?php InterfaceTemplateURL(); ?>assets/global/img/loading.gif" alt="loading" /> 
                    </div>
                    <div id="region_statistics_content" class="display-none">
                        <div class="btn-toolbar margin-bottom-10">
                            <div class="btn-group btn-group-circle" data-toggle="buttons">
                                <a href="" id="open_geo_home" class="btn grey-salsa btn-sm active"> Open </a>
                                <!--<a href="" id="click_geo_home" class="btn grey-salsa btn-sm"> Click </a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a href="" class="btn btn-circle grey-salsa btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                    <span id="option_selected" >All</span>
                                    <span class="fa fa-angle-down"> </span>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" id="view_geo_all"> All </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_3_month"> Last 3 month </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_6_month"> Last 6 month </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_year"> Last year  </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="vmap_world" class="vmaps display-none" style="height: 330px"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet portlet-transparent">
                <div class="portlet-title">
                    <div class="caption font-purple-sharp sbold"> <?php InterfaceLanguage('Screen', '0849', false, '', false, true); ?> </div>
                </div>
                <div class="portlet-body">
                    <?php
                    if (count($MostClickedLinks) < 1):
                        ?>
                        <p style="margin-top: 10px" class="no-data-message"><?php InterfaceLanguage('Screen', '0895', false, '', false); ?></p>
                        <?php
                    else:
                        ?>
                        <table class="table table-no-bordered table-custom" style="width:50%">
                            <?php
                            foreach ($MostClickedLinks as $Index => $EachLink):
                                ?>
                                <?php $percentage = number_format((100 * $EachLink['TotalClicks']) / $CampaignInformation['TotalClicks']); ?>
                                <tr>
                                    <td width="80%" style="padding-left: 0">
                                        <div class="status">
                                            <div class="status-number"> <?php echo $percentage ?>% (<?php print(number_format($EachLink['TotalClicks'])); ?>)</div>
                                        </div>
                                        <!--progress-striped-->
                                        <!--style="border-radius: 10px !important;"-->
                                        <div class="progress" >
                                            <span style="width: <?php echo $percentage ?>%;" class="progress-bar progress-bar-success">
                                                <span class="sr-only"><?php echo $percentage ?>% percentage</span>
                                            </span>
                                        </div>
                                    </td>
                                    <td width="20%"><?php InterfaceTextCut($EachLink['LinkTitle'] != '' ? $EachLink['LinkTitle'] : $EachLink['LinkURL'], 80, '...', false); ?></td>
                                    <!--<td width="20%" class="right-align"><span class="data"><?php print(number_format($EachLink['TotalClicks'])); ?></span> <?php InterfaceLanguage('Screen', '0838', false, '', false); ?></td>-->
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </table>
                        <div class="clearfix"></div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet">
                <div class="portlet-body">
                    <!--                    <div id="chart_7" class="chart" style="height: 365px;width:515px overflow: hidden; text-align: left;">
                    
                                        </div>-->
                    <div id="donut" class="chart" style="height: 250px;padding: 0px; position: relative;margin-top: 50px">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($CampaignInformation['SplitTest'] != false): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet portlet-transparent">
                    <div class="portlet-title">
                        <div class="caption font-purple-sharp sbold"> <?php InterfaceLanguage('Screen', '1388', false, '', false, true); ?> </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-pane" id="most-test-statistics">
                            <div class="inner flash-chart-container" id="test-opens-chart-container">
                                <script type="text/javascript" charset="utf-8">
                                    SWFObjectContainer1 = new SWFObject("<?php InterfaceInstallationURL(); ?>assets/charts/amcolumn/amcolumn.swf", "activity-chart", "328", "220", "8", "#FFFFFF");
                                    SWFObjectContainer1.addVariable("path", "<?php InterfaceInstallationURL(); ?>assets/charts/amcolumn/");
                                    SWFObjectContainer1.addVariable("settings_file", escape("<?php InterfaceAppURL(); ?>/admin/chart/settings/bar/5"));
                                    SWFObjectContainer1.addVariable("data_file", escape("<?php InterfaceAppURL(); ?>/user/campaign/teststatistics/<?php print($CampaignInformation['CampaignID']) ?>/"));
                                    SWFObjectContainer1.addVariable("preloader_color", "#FFFFFF");
                                    SWFObjectContainer1.addParam("wmode", "opaque");
                                    SWFObjectContainer1.write("test-opens-chart-container");
                                </script>
                            </div>
                            <div class="alert alert-info-custom">
                                <span class="data-label small" style="margin:0px;"><?php InterfaceLanguage('Screen', '1340', false, '', false, false); ?></span>: <span class="data"><?php echo $CampaignInformation['SplitTest']['TestSize']; ?>%</span>
                                <?php if ($CampaignInformation['SplitTest']['RelWinnerEmailID'] != 0): ?>
                                    <br /><span class="data-label" style="margin:0px;"><?php InterfaceLanguage('Screen', '1348', false, '', false, false); ?>:</span> <span class="data"><?php echo $WinnerEmail; ?></span>
                                <?php endif; ?>
                            </div>
                            <table class="small-grid table table-bordered" style="margin-top:18px;">
                                <tr>
                                    <th width="60%"><?php InterfaceLanguage('Screen', '0008', false, '', true); ?></th>
                                    <th width="20%"><?php InterfaceLanguage('Screen', '0837', false, '', true); ?></th>
                                    <th width="20%"><?php InterfaceLanguage('Screen', '0838', false, '', true); ?></th>
                                </tr>
                                <?php foreach ($SplitTestEmails as $index => $EachTestEmail): ?>
                                    <tr>
                                        <td style="color:#<?php echo $ChartColors[$index] ?>;"><?php echo $EachTestEmail['EmailName']; ?></td>
                                        <td>
                                            <span class="data"><?php echo $EachTestEmail['UniqueOpens']; ?></span><br />
                                        </td>
                                        <td>
                                            <span class="data"><?php echo $EachTestEmail['UniqueClicks']; ?></span><br />
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<script>
    var APP_URL = '<?php InterfaceAppURL(); ?>';
    var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
    var CampaignID = <?php print($CampaignInformation['CampaignID']) ?>;

    $("#more_link").click(function () {
        $("#listIds").toggle();
        $("#more_lists").toggle();
        $("#showless").toggle();
    });

    $("#showless").click(function () {
        $("#listIds").toggle();
        $("#more_lists").toggle();
        $("#showless").toggle();
    });

</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_overview.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>