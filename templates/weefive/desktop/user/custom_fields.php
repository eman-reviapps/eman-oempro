<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <!--                                <div class="caption">
                                                                    <i class="icon-microphone font-purple-sharp"></i>
                                                                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0547', false, '', false, true); ?></span>                    
                                                                </div>-->
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" >    
                                        <?php if (InterfacePrivilegeCheck('CustomField.Create', $UserInformation)): ?>
                                            <a href="<?php InterfaceAppURL(); ?>/user/customfields/copy/<?php echo $ListID; ?>" class="btn default"><?php InterfaceLanguage('Screen', '1160'); ?></a>
                                            <a href="<?php InterfaceAppURL(); ?>/user/customfields/presets/<?php echo $ListID; ?>" class="btn default"><?php InterfaceLanguage('Screen', '1161'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li <?php echo!$IsCreateEvent && !$IsEditEvent ? 'class="active"' : '' ?> >
                                        <a href="#tab-content-browse" data-toggle="tab">
                                            <?php InterfaceLanguage('Screen', '1156', false, '', false, true); ?>
                                        </a>
                                    </li>
                                    <?php if (InterfacePrivilegeCheck('CustomField.Create', $UserInformation)): ?>
                                        <li <?php echo $IsCreateEvent ? 'class="active"' : '' ?> >
                                            <a href="#tab-content-create" data-toggle="tab">
                                                <?php InterfaceLanguage('Screen', '1157', false, '', false, true); ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ($IsEditEvent): ?>
                                        <li class="active">
                                            <a href="#tab-edit" data-toggle="tab">
                                                <?php InterfaceLanguage('Screen', '1158', false, '', false, true); ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab-content-browse" class="tab-pane <?php echo!$IsCreateEvent && !$IsEditEvent ? 'active' : '' ?>" >
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                if (isset($PageSuccessMessage) == true):
                                                    ?>
                                                    <div class="alert alert-info alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageSuccessMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                endif;
                                                ?>
                                                <div class="grid-operations">
                                                    <span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="custom-fields-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="custom-fields-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="custom-fields-table-form"><?php InterfaceLanguage('Screen', '1159'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <form id="custom-fields-table-form" action="<?php InterfaceAppURL(); ?>/user/customfields/browse/<?php echo $ListID; ?>" method="post">
                                            <input type="hidden" name="Command" value="DeleteCustomFields" id="Command">
                                            <table class="grid table table-bordered" id="custom-fields-table">
                                                <?php if ($CustomFields === false): ?>
                                                    <tr>
                                                        <td><?php InterfaceLanguage('Screen', '1162', false, '', false, false); ?></td>
                                                    </tr>
                                                <?php else: ?>
                                                    <?php
                                                    foreach ($CustomFields as $EachCustomField):
                                                        ?>
                                                        <tr>
                                                            <td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedCustomFields[]" value="<?php print($EachCustomField['CustomFieldID']); ?>" id="SelectedCustomFields<?php print($EachCustomField['CustomFieldID']); ?>"></td>
                                                            <td width="370" class="">
                                                                <?php if (InterfacePrivilegeCheck('CustomFields.Get', $UserInformation)): ?>
                                                                    <a href="<?php InterfaceAppURL(); ?>/user/customfields/edit/<?php echo $ListID; ?>/<?php print($EachCustomField['CustomFieldID']); ?>"><?php print($EachCustomField['FieldName']); ?></a>
                                                                <?php else: ?>
                                                                    <?php print($EachCustomField['FieldName']); ?>
                                                                <?php endif; ?>
                                                                <?php if ($EachCustomField['IsGlobal'] == 'Yes'): ?>
                                                                    <span class="label"><?php InterfaceLanguage('Screen', '1496'); ?></span>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td width="95">
                                                                <span class="data-label small"><?php InterfaceLanguage('Screen', '1163', false, '', false, false); ?></span> <span class="data small"><?php echo $EachCustomField['CustomFieldID']; ?></span>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </table>
                                        </form>
                                    </div>
                                    <?php if (InterfacePrivilegeCheck('CustomField.Create', $UserInformation)): ?>
                                        <div id="tab-content-create" class="tab-pane <?php echo $IsCreateEvent ? 'active' : '' ?>" class="clearfix">
                                            <br/>
                                            <br/>
                                            <br/>
                                            <form id="custom-field-create" action="<?php InterfaceAppURL(); ?>/user/customfields/browse/<?php echo $ListID; ?>" method="post">
                                                <input type="hidden" name="Command" value="Create" id="Command">
                                                <input type="hidden" name="Options" value="<?php echo set_value('Options'); ?>" id="Options">
                                                <input type="hidden" name="SelectedOptions" value="<?php echo set_value('SelectedOptions'); ?>" id="SelectedOptions">
                                                <div class="col-md-8">
                                                    <?php
                                                    if (isset($PageCreateErrorMessage) == true):
                                                        ?>
                                                        <div class="alert alert-danger alert-dismissable">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <?php print($PageCreateErrorMessage); ?>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="alert alert-info alert-info-custom">
                                                            <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                                                        </div>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group clearfix <?php print((form_error('FieldName') != '' ? 'has-error' : '')); ?>" id="form-row-FieldName">
                                                    <label class="col-md-3 control-label" for="FieldName"><?php InterfaceLanguage('Screen', '1216', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="FieldName" value="<?php echo set_value('FieldName'); ?>" id="FieldName" class="form-control" />
                                                        <?php print(form_error('FieldName', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix " id="form-row-FieldType">
                                                    <label class="col-md-3 control-label" for="FieldType"><?php InterfaceLanguage('Screen', '1217', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="FieldType" id="FieldType" class="FieldType">
                                                            <option value="Single line" <?php echo set_select('FieldType', 'Single line', $CustomField['']); ?>><?php InterfaceLanguage('Screen', '1218'); ?></option>
                                                            <option value="Paragraph text" <?php echo set_select('FieldType', 'Paragraph text'); ?>><?php InterfaceLanguage('Screen', '1219'); ?></option>
                                                            <option value="Multiple choice" <?php echo set_select('FieldType', 'Multiple choice'); ?>><?php InterfaceLanguage('Screen', '1220'); ?></option>
                                                            <option value="Drop down" <?php echo set_select('FieldType', 'Drop down'); ?>><?php InterfaceLanguage('Screen', '1221'); ?></option>
                                                            <option value="Checkboxes" <?php echo set_select('FieldType', 'Checkboxes'); ?>><?php InterfaceLanguage('Screen', '1222'); ?></option>
                                                            <option value="Date field" <?php echo set_select('FieldType', 'Date field'); ?>><?php InterfaceLanguage('Screen', '1366'); ?></option>
                                                            <option value="Time field" <?php echo set_select('FieldType', 'Time field'); ?>><?php InterfaceLanguage('Screen', '1367'); ?></option>
                                                            <option value="Hidden field" <?php echo set_select('FieldType', 'Hidden field'); ?>><?php InterfaceLanguage('Screen', '1223'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1234'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix " id="form-row-DateFieldYears" style="display:none;">
                                                    <label class="col-md-3 control-label" for="DateFieldYears"><?php InterfaceLanguage('Screen', '1368', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <?php InterfaceLanguage('Screen', '1369'); ?> 
                                                        <input type="text" name="DateFieldYears[]" value="<?php echo set_value('DateFieldYearsStart'); ?>" id="DateFieldYearsStart" class="form-control" style="width:40px;display: inline"> 
                                                        <?php InterfaceLanguage('Screen', '1370'); ?> 
                                                        <input type="text" name="DateFieldYears[]" value="<?php echo set_value('DateFieldYearsStart'); ?>" id="DateFieldYearsEnd" class="form-control" style="width:40px;display: inline">
                                                        <?php print(form_error('DateFieldYears', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-options" style="display:none;">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1247'); ?></label>
                                                    <table border="0" cellpadding="0" cellspacing="0" border="0" style="width:325px;margin:0px;">
                                                        <th style="color:#808080;padding-left:23px;width:140px;"><?php InterfaceLanguage('Screen', '1365'); ?></th>
                                                        <th style="color:#808080;padding-left:5px;"><?php InterfaceLanguage('Screen', '1364'); ?></th>
                                                    </table>
                                                    <div class="checkbox-container form-row-options-container"></div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('FieldDefaultValue') != '' ? 'has-error' : '')); ?>" id="form-row-FieldDefaultValue" stye="display:none">
                                                    <label class="col-md-3 control-label" for="FieldDefaultValue"><?php InterfaceLanguage('Screen', '1246', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="FieldDefaultValue" value="<?php echo set_value('FieldDefaultValue'); ?>" id="FieldDefaultValue" class="form-control" />
                                                        <?php print(form_error('FieldDefaultValue', '<span class="help-block">', '</span>')); ?>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1245'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-ValidationMethod">
                                                    <label class="col-md-3 control-label" for="ValidationMethod"><?php InterfaceLanguage('Screen', '1233', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="ValidationMethod" id="ValidationMethod">
                                                            <option value="Disabled" <?php echo set_select('ValidationMethod', 'Disabled'); ?>><?php InterfaceLanguage('Screen', '1224'); ?></option>
                                                            <option value="Numbers" <?php echo set_select('ValidationMethod', 'Numbers'); ?>><?php InterfaceLanguage('Screen', '1225'); ?></option>
                                                            <option value="Letters" <?php echo set_select('ValidationMethod', 'Letters'); ?>><?php InterfaceLanguage('Screen', '1226'); ?></option>
                                                            <option value="Numbers and letters" <?php echo set_select('ValidationMethod', 'Numbers and letters'); ?>><?php InterfaceLanguage('Screen', '1227'); ?></option>
                                                            <option value="Email address" <?php echo set_select('ValidationMethod', 'Email address'); ?>><?php InterfaceLanguage('Screen', '1228'); ?></option>
                                                            <option value="URL" <?php echo set_select('ValidationMethod', 'URL'); ?>><?php InterfaceLanguage('Screen', '1229'); ?></option>
                                                            <option value="Custom" <?php echo set_select('ValidationMethod', 'Custom'); ?>><?php InterfaceLanguage('Screen', '1232'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1235'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('ValidationRule') != '' ? 'has-error' : '')); ?>" id="form-row-ValidationRule" stye="display:none">
                                                    <label class="col-md-3 control-label" for="ValidationRule"><?php InterfaceLanguage('Screen', '1244', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="ValidationRule" value="<?php echo set_value('ValidationRule'); ?>" id="ValidationRule" class="form-control" />
                                                        <?php print(form_error('ValidationRule', '<span class="help-block">', '</span>')); ?>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1243'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-Visibility">
                                                    <label class="col-md-3 control-label" for="Visibility"><?php InterfaceLanguage('Screen', '1239', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="Visibility" id="Visibility">
                                                            <option value="User Only"><?php InterfaceLanguage('Screen', '1237'); ?></option>
                                                            <option value="Public"><?php InterfaceLanguage('Screen', '1238'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1236'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <div class="checkbox-container">
                                                        <div class="checkbox-row">
                                                            <input type="checkbox" name="IsRequired" value="Yes" id="IsRequired" <?php echo set_checkbox('IsRequired'); ?>>
                                                            <label for="IsRequired"><?php InterfaceLanguage('Screen', '1240', false, '', false, false); ?></label> <br />
                                                        </div>
                                                        <div class="checkbox-row">
                                                            <input type="checkbox" name="IsUnique" value="Yes" id="IsUnique" <?php echo set_checkbox('IsUnique'); ?>>
                                                            <label for="IsUnique"><?php InterfaceLanguage('Screen', '1241', false, '', false, false); ?></label> <br />
                                                        </div>
                                                        <div class="checkbox-row">
                                                            <input type="checkbox" name="IsGlobal" value="Yes" id="IsGlobal" <?php echo set_checkbox('IsGlobal'); ?>>
                                                            <label for="IsGlobal"><?php InterfaceLanguage('Screen', '1242', false, '', false, false); ?></label> <br />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn default" targetform="custom-field-create" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1214', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($IsEditEvent): ?>
                                    <div class="clearfix"></div>
                                        <div id="tab-edit" class="tab-pane active" class="clearfix">
                                            <form id="custom-field-edit" action="<?php InterfaceAppURL(); ?>/user/customfields/edit/<?php echo $ListID; ?>/<?php echo $CustomField['CustomFieldID']; ?>" method="post">
                                                <input type="hidden" name="Command" value="Edit" id="Command">
                                                <input type="hidden" name="CustomFieldID" value="<?php echo $CustomField['CustomFieldID']; ?>" id="CustomFieldID">
                                                <input type="hidden" name="Options" value="<?php echo $CustomField['Options']; ?>" id="Options">
                                                <input type="hidden" name="SelectedOptions" value="<?php echo set_value('SelectedOptions', $CustomField['SelectedOptions']); ?>" id="SelectedOptions">
                                                
                                                    <?php
                                                if (isset($PageEditErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageEditErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (isset($PageEditSuccessMessage) == true):
                                                    ?>
                                                    <div class="alert alert-info alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageEditSuccessMessage); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <div class="alert alert-info alert-info-custom">
                                                        <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                                                    </div>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('FieldName') != '' ? 'has-error' : '')); ?>" id="form-row-FieldName">
                                                    <label class="col-md-3 control-label" for="FieldName"><?php InterfaceLanguage('Screen', '1216', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="FieldName" value="<?php echo set_value('FieldName', $CustomField['FieldName']); ?>" id="FieldName" class="form-control" />
                                                        <?php print(form_error('FieldName', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-FieldType">
                                                    <label class="col-md-3 control-label" for="FieldType"><?php InterfaceLanguage('Screen', '1217', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="FieldType" id="FieldType" class="FieldType">
                                                            <option value="Single line" <?php echo set_select('FieldType', 'Single line', $CustomField['FieldType'] == 'Single line' ? true : false); ?>><?php InterfaceLanguage('Screen', '1218'); ?></option>
                                                            <option value="Paragraph text" <?php echo set_select('FieldType', 'Paragraph text', $CustomField['FieldType'] == 'Paragraph text' ? true : false); ?>><?php InterfaceLanguage('Screen', '1219'); ?></option>
                                                            <option value="Multiple choice" <?php echo set_select('FieldType', 'Multiple choice', $CustomField['FieldType'] == 'Multiple choice' ? true : false); ?>><?php InterfaceLanguage('Screen', '1220'); ?></option>
                                                            <option value="Drop down" <?php echo set_select('FieldType', 'Drop down', $CustomField['FieldType'] == 'Drop down' ? true : false); ?>><?php InterfaceLanguage('Screen', '1221'); ?></option>
                                                            <option value="Checkboxes" <?php echo set_select('FieldType', 'Checkboxes', $CustomField['FieldType'] == 'Checkboxes' ? true : false); ?>><?php InterfaceLanguage('Screen', '1222'); ?></option>
                                                            <option value="Date field" <?php echo set_select('FieldType', 'Date field', $CustomField['FieldType'] == 'Date field' ? true : false); ?>><?php InterfaceLanguage('Screen', '1366'); ?></option>
                                                            <option value="Time field" <?php echo set_select('FieldType', 'Time field', $CustomField['FieldType'] == 'Time field' ? true : false); ?>><?php InterfaceLanguage('Screen', '1367'); ?></option>
                                                            <option value="Hidden field" <?php echo set_select('FieldType', 'Hidden field', $CustomField['FieldType'] == 'Hidden field' ? true : false); ?>><?php InterfaceLanguage('Screen', '1223'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1234'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-DateFieldYears" style="display:none;">
                                                    <label class="col-md-3 control-label" for="DateFieldYears">
                                                        <?php InterfaceLanguage('Screen', '1368', false, '', false, true); ?>: *
                                                    </label>
                                                    <div class="col-md-6">
                                                        <?php InterfaceLanguage('Screen', '1369'); ?> 
                                                        <input type="text" name="DateFieldYears[]" value="<?php echo set_value('DateFieldYearsStart', $CustomField['DateFieldYearsStart']); ?>" id="DateFieldYearsStart" class="form-control" style="width:40px;display: inline"> 
                                                        <?php InterfaceLanguage('Screen', '1370'); ?> 
                                                        <input type="text" name="DateFieldYears[]" value="<?php echo set_value('DateFieldYearsStart', $CustomField['DateFieldYearsEnd']); ?>" id="DateFieldYearsEnd" class="form-control" style="width:40px;display: inline">
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-options" style="display:none;">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1247'); ?></label>
                                                    <table border="0" cellpadding="0" cellspacing="0" border="0" style="width:325px;margin:0px;">
                                                        <th style="color:#808080;padding-left:23px;width:140px;"><?php InterfaceLanguage('Screen', '1365'); ?></th>
                                                        <th style="color:#808080;padding-left:5px;"><?php InterfaceLanguage('Screen', '1364'); ?></th>
                                                    </table>
                                                    <div class="checkbox-container form-row-options-container"></div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('FieldDefaultValue') != '' ? 'has-error' : '')); ?>" id="form-row-FieldDefaultValue" stye="display:none">
                                                    <label class="col-md-3 control-label" for="FieldDefaultValue"><?php InterfaceLanguage('Screen', '1246', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="FieldDefaultValue" value="<?php echo set_value('FieldDefaultValue', $CustomField['FieldDefaultValue']); ?>" id="FieldDefaultValue" class="form-control" />
                                                        <?php print(form_error('FieldDefaultValue', '<span class="help-block">', '</span>')); ?>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1245'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-ValidationMethod">
                                                    <label class="col-md-3 control-label" for="ValidationMethod"><?php InterfaceLanguage('Screen', '1233', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="ValidationMethod" id="ValidationMethod">
                                                            <option value="Disabled" <?php echo set_select('ValidationMethod', 'Disabled', $CustomField['ValidationMethod'] == 'Disabled' ? true : false); ?>><?php InterfaceLanguage('Screen', '1224'); ?></option>
                                                            <option value="Numbers" <?php echo set_select('ValidationMethod', 'Numbers', $CustomField['ValidationMethod'] == 'Numbers' ? true : false); ?>><?php InterfaceLanguage('Screen', '1225'); ?></option>
                                                            <option value="Letters" <?php echo set_select('ValidationMethod', 'Letters', $CustomField['ValidationMethod'] == 'Letters' ? true : false); ?>><?php InterfaceLanguage('Screen', '1226'); ?></option>
                                                            <option value="Numbers and letters" <?php echo set_select('ValidationMethod', 'Numbers and letters', $CustomField['ValidationMethod'] == 'Numbers and letters' ? true : false); ?>><?php InterfaceLanguage('Screen', '1227'); ?></option>
                                                            <option value="Email address" <?php echo set_select('ValidationMethod', 'Email address', $CustomField['ValidationMethod'] == 'Email address' ? true : false); ?>><?php InterfaceLanguage('Screen', '1228'); ?></option>
                                                            <option value="URL" <?php echo set_select('ValidationMethod', 'URL', $CustomField['ValidationMethod'] == 'URL' ? true : false); ?>><?php InterfaceLanguage('Screen', '1229'); ?></option>
                                                            <option value="Date" <?php echo set_select('ValidationMethod', 'Date', $CustomField['ValidationMethod'] == 'Date' ? true : false); ?>><?php InterfaceLanguage('Screen', '1230'); ?></option>
                                                            <option value="Time" <?php echo set_select('ValidationMethod', 'Time', $CustomField['ValidationMethod'] == 'Time' ? true : false); ?>><?php InterfaceLanguage('Screen', '1231'); ?></option>
                                                            <option value="Custom" <?php echo set_select('ValidationMethod', 'Custom', $CustomField['ValidationMethod'] == 'Custom' ? true : false); ?>><?php InterfaceLanguage('Screen', '1232'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1235'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('ValidationRule') != '' ? 'has-error' : '')); ?>" id="form-row-ValidationRule" stye="display:none">
                                                    <label class="col-md-3 control-label" for="ValidationRule"><?php InterfaceLanguage('Screen', '1244', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="ValidationRule" value="<?php echo set_value('ValidationRule', $CustomField['ValidationRule']); ?>" id="ValidationRule" class="form-control" />
                                                        <?php print(form_error('ValidationRule', '<span class="help-block">', '</span>')); ?>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1243'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-Visibility">
                                                    <label class="col-md-3 control-label" for="Visibility"><?php InterfaceLanguage('Screen', '1239', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="Visibility" id="Visibility">
                                                            <option value="User Only" <?php echo set_select('Visibility', 'User Only', $CustomField['Visibility'] == 'User Only' ? true : false); ?>><?php InterfaceLanguage('Screen', '1237'); ?></option>
                                                            <option value="Public" <?php echo set_select('Visibility', 'Public', $CustomField['Visibility'] == 'Public' ? true : false); ?>><?php InterfaceLanguage('Screen', '1238'); ?></option>
                                                        </select>
                                                        <span class="help-block">
                                                            <?php InterfaceLanguage('Screen', '1236'); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <div class="checkbox-container">
                                                        <div class="checkbox-row">
                                                            <input type="checkbox" name="IsRequired" value="Yes" id="IsRequired" <?php echo set_checkbox('IsRequired', 'Yes', $CustomField['IsRequired'] == 'Yes' ? true : false); ?>>
                                                            <label for="IsRequired"><?php InterfaceLanguage('Screen', '1240', false, '', false, false); ?></label> <br />
                                                        </div>
                                                        <div class="checkbox-row">
                                                            <input type="checkbox" name="IsUnique" value="Yes" id="IsUnique" <?php echo set_checkbox('IsUnique', 'Yes', $CustomField['IsUnique'] == 'Yes' ? true : false); ?>>
                                                            <label for="IsUnique"><?php InterfaceLanguage('Screen', '1241', false, '', false, false); ?></label> <br />
                                                        </div>
                                                        <?php if ($CustomField['IsGlobal'] == 'No'): ?>
                                                            <div class="checkbox-row">
                                                                <input type="checkbox" name="IsGlobal" value="Yes" id="IsGlobal" <?php echo set_checkbox('IsGlobal'); ?>>
                                                                <label for="IsGlobal"><?php InterfaceLanguage('Screen', '1242', false, '', false, false); ?></label> <br />
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn default" targetform="custom-field-edit" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>

                                </div>
                                <div class="page-bar">

                                </div>
                                <div class="white" style="min-height:450px;">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="field-option-multiple-choice-template" style="display:none">
    <div class="field-option-container checkbox-row" optionid="#id#" style="padding:3px 0px;">
        <input type="radio" name="radiooption" class="option-default" optionid="#id#">
        <input type="text" class="option-label" optionid="#id#" value="#label#">
        <input type="text" class="option-value" optionid="#id#" value="#value#">
        <input type="button" value="+" class="add-button" optionid="#id#">
        <input type="button" value="-" class="remove-button" optionid="#id#">
    </div>
</div>

<div id="field-option-checkbox-template" style="display:none">
    <div class="field-option-container checkbox-row" optionid="#id#" style="padding:3px 0px;">
        <input type="checkbox" class="option-default" optionid="#id#">
        <input type="text" class="option-label" optionid="#id#" value="#label#">
        <input type="text" class="option-value" optionid="#id#" value="#value#">
        <input type="button" value="+" class="add-button" optionid="#id#">
        <input type="button" value="-" class="remove-button" optionid="#id#">
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1164': '<?php InterfaceLanguage('Screen', '1164', false, '', false); ?>'
        }
    };
    is_edit = <?php echo $IsEditEvent ? 'true' : 'false'; ?>;
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/customfields.js" type="text/javascript" charset="utf-8"></script>		


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>