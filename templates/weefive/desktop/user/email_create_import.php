<div>
    <div class="col-md-8" style="padding: 0;" >
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title() ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                if (isset($PageErrorMessage) == true):
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php print($PageErrorMessage); ?>
                    </div>
                    <?php
                elseif (validation_errors()):
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                    </div>
                    <?php
                endif;
                ?>
                <form id="campaign-create" class="form-inputs" action="<?php echo $FormURL; ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0781', false, '', false, true); ?></h4>

                    <?php if (IMAGE_EMBEDDING_ENABLED): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="form-row-ImageEmbedding">
                                    <div class="checkbox-container">
                                        <div class="checkbox-row" style="margin-left:0px;">
                                            <input type="checkbox" name="ImageEmbedding" value="Enabled" id="ImageEmbedding" <?php echo set_checkbox('ImageEmbedding', 'Enabled'); ?>>
                                            <label for="ImageEmbedding"><?php InterfaceLanguage('Screen', '0782', false, '', false, false); ?></label>
                                        </div>
                                    </div>
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0783', false, '', false, false); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="form-row-ContentType">
                                <label class="col-md-3 control-label" for="ContentType"><?php InterfaceLanguage('Screen', '0785', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <select name="ContentType" id="ContentType" class="form-control">
                                        <option value="HTML" <?php echo set_select('ContentType', 'HTML'); ?> <?php
                                        if (isset($ContentType) && $ContentType == 'HTML') {
                                            echo 'selected';
                                        }
                                        ?>><?php InterfaceLanguage('Screen', '0786', false, '', false, false); ?></option>
                                        <option value="Plain" <?php echo set_select('ContentType', 'Plain'); ?> <?php
                                        if (isset($ContentType) && $ContentType == 'Plain') {
                                            echo 'selected';
                                        }
                                        ?>><?php InterfaceLanguage('Screen', '0787', false, '', false, false); ?></option>
                                        <option value="Both" <?php echo set_select('ContentType', 'Both'); ?> <?php
                                        if (isset($ContentType) && $ContentType == 'Both') {
                                            echo 'selected';
                                        }
                                        ?>><?php InterfaceLanguage('Screen', '0788', false, '', false, false); ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0784', false, '', false, true); ?></h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Subject') != '' ? 'has-error' : '')); ?>" id="form-row-Subject">
                                <label class="col-md-3 control-label" for="Subject"><?php InterfaceLanguage('Screen', '0106', false, '', false, true); ?>: *</label>
                                <div class="col-md-8">
                                    <input type="text" name="Subject" value="<?php echo set_value('Subject', $FieldDefaultValues['Subject']); ?>" id="Subject" class="form-control personalized" />
                                    <div class="form-row-note personalization" id="personalization-Subject">
                                        <div class="well" style="margin: 0">
                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                            <select name="personalization-select-Subject" id="personalization-select-Subject" class="personalization-select">
                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                <?php foreach ($SubjectTags as $Label => $TagGroup): ?>
                                                    <optgroup label="<?php print($Label); ?>">
                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                    <?php print(form_error('Subject', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('FetchURL') != '' ? 'has-error' : '')); ?>" id="form-row-FetchURL">
                                <label class="col-md-3 control-label" for="FetchURL"><?php InterfaceLanguage('Screen', '1250', false, '', false, true); ?>: *</label>
                                <div class="col-md-8">
                                    <input type="text" name="FetchURL" value="<?php echo set_value('FetchURL', $FieldDefaultValues['FetchURL']); ?>" id="FetchURL" class="form-control personalized" />
                                    <div class="form-row-note personalization" id="personalization-FetchURL">
                                        <div class="well" style="margin: 0">
                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                            <select name="personalization-select-FetchURL" id="personalization-select-FetchURL" class="personalization-select">
                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                                    <optgroup label="<?php print($Label); ?>">
                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>&nbsp;&nbsp;
                                        </div>
                                    </div>

                                    <?php print(form_error('FetchURL', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('FetchPlainURL') != '' ? 'has-error' : '')); ?>" id="form-row-FetchPlainURL">
                                <label class="col-md-3 control-label" for="FetchPlainURL"><?php InterfaceLanguage('Screen', '1251', false, '', false, true); ?>: *</label>
                                <input type="text" name="FetchPlainURL" value="<?php echo set_value('FetchPlainURL', $FieldDefaultValues['FetchPlainURL']); ?>" id="FetchPlainURL" class="text personalized" style="width:500px;" />
                                <div class="form-group-note personalization" id="personalization-FetchPlainURL">
                                    <p>
                                        <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                        <select name="personalization-select-FetchPlainURL" id="personalization-select-FetchPlainURL" class="personalization-select">
                                            <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                            <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                                <optgroup label="<?php print($Label); ?>">
                                                    <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                        <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                    <?php endforeach; ?>
                                                </optgroup>
                                            <?php endforeach; ?>
                                        </select>&nbsp;&nbsp;
                                    </p>
                                </div>
                                <?php print(form_error('FetchPlainURL', '<span class="help-block">', '</span>')); ?>
                            </div>
                        </div>
                    </div>
                    <h4 style="display: none" class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0789', false, '', false, true); ?></h4>
                </form>

                <form  style="display: none" target="file-upload-frame" enctype="multipart/form-data" action="<?php InterfaceAppURL(); ?>/user/attachments/upload/" method="POST" name="file-upload-form" id="file-upload-form">
                    <div class="form-group">
                        <input type="file" name="AttachmentFile" id="AttachmentFile" /> <span id="uploading-indicator" style="display:none"><img id="search-loading-indicator" src="<?php InterfaceTemplateURL(); ?>/images/icon_load.gif" width="16" height="16" align="absmiddle" /> Uploading attachment...</span>
                        <div id="attachment-container" class="form-group-note" style="margin-left:0px;margin-top:9px;">
                            <p id="attachment-template" style="display:none"><input type="checkbox" name="attachment-checkbox[]" value="_AttachmentID_" class="attachment-checkbox" id="attachment-checkbox-_AttachmentID_" checked="checked" /> <a href="<?php InterfaceAppURL(); ?>/user/attachments/view/_AttachmentMD5ID_" target="_blank">_FileName_</a> <span class="data small" style="margin-left:18px;">_FileSize_</span></p>
                        </div>
                    </div>
                </form>
                <iframe name="file-upload-frame" id="file-upload-frame" style="height:1px;width:1px;display:none"></iframe>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreateimport.php'); ?>
    </div>

</div>
<div class="col-md-8"  style="padding: 0;">
    <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
</div>

<script>
    var wysiwyg_enabled = false;
    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
    var attachments = [
<?php
if ($Attachments != false) {
    for ($i = 0; $i < count($Attachments); $i++) {
        ?>
            { AttachmentID: '<?php print($Attachments[$i]['AttachmentID']); ?>', AttachmentMD5ID: '<?php print(md5($Attachments[$i]['AttachmentID'])); ?>', FileName: '<?php print(htmlspecialchars($Attachments[$i]['FileName'], ENT_QUOTES)); ?>', FileMimeType: '<?php print($Attachments[$i]['FileMimeType']); ?>', FileSize: '<?php print($Attachments[$i]['FileSize']); ?>' }<?php print($i < count($Attachments) - 1 ? ',' : ''); ?>
        <?php
    }
}
?>
    ];
            $(document).ready(function () {
        content_library.init_handlers();
    });
</script>