<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>

                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', ($IsEditEvent ? '1290' : '1306'), false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="segment-create" action="<?php InterfaceAppURL(); ?>/user/segment/<?php echo $IsEditEvent ? 'edit' : 'create'; ?>/<?php echo $ListInformation['ListID']; ?><?php echo $IsEditEvent ? '/' . $SegmentInformation['SegmentID'] : ''; ?>" method="post">
                                                <?php if ($IsEditEvent): ?>
                                                    <input type="hidden" name="Command" value="EditSegment" id="Command">
                                                    <input type="hidden" name="SegmentID" value="<?php echo $SegmentInformation['SegmentID']; ?>" id="Command">
                                                <?php else: ?>
                                                    <input type="hidden" name="Command" value="CreateSegment" id="Command">
                                                <?php endif; ?>
                                                <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">
                                                <input type="hidden" name="SegmentRules" value="<?php echo $SegmentInformation['SegmentRules']; ?>" id="SegmentRules">

                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                    <h4 class="form-section bold font-blue" style="margin-top: 10px !important"><?php InterfaceLanguage('Screen', '0037', false); ?></h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('SegmentName') != '' ? 'has-error' : '')); ?>" id="form-row-SegmentName">
                                                    <label class="col-md-3 control-label" for="SegmentName"><?php InterfaceLanguage('Screen', '0051', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <input name="SegmentName" id="SegmentName" type="text" value="<?php echo set_value('SegmentName', $SegmentInformation['SegmentName']); ?>" class="form-control" />
                                                        <?php print(form_error('SegmentName', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-SegmentOperator">
                                                    <label class="col-md-3 control-label" for="SegmentOperator"><?php InterfaceLanguage('Screen', '1298', false, '', false, true); ?>:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="SegmentOperator" id="SegmentOperator">
                                                            <option value="and" <?php echo set_select('SegmentOperator', 'and', $SegmentInformation['SegmentOperator'] == 'and'); ?>><?php InterfaceLanguage('Screen', '1299'); ?></option>
                                                            <option value="or" <?php echo set_select('SegmentOperator', 'or', $SegmentInformation['SegmentOperator'] == 'or'); ?>><?php InterfaceLanguage('Screen', '1300'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1399', false, '', false, true); ?>:</label>

                                                </div>
                                                <div class="form-group clearfix">
                                                    <div class="col-md-12">
                                                        <div class="rule-board rule-board-custom ">
                                                            <div class="rules" style="padding-top: 0">
                                                                <ul id="rule-list"></ul>
                                                            </div>
                                                            <div class="rule-board-footer">
                                                                <div class="rule-board-actions">
                                                                    <div class="main-action-with-menu add-new-rule-menu btn-group">
                                                                        <a href="#" class="main-action btn btn-default dropdown-toggle"><?php InterfaceLanguage('Screen', '1304'); ?></a>
                                                                        <div class="down-arrow" style="right:5px;"><span></span></div>
                                                                        <ul class="main-action-menu" style="width:200px">
                                                                            <li class="label"><?php InterfaceLanguage('Screen', '1398'); ?>:</li>
                                                                            <?php foreach ($DefaultFields as $Each): ?>
                                                                                <li><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" validationmethod="<?php echo $Each['ValidationMethod']; ?>" values="<?php echo $Each['Values']; ?>"><?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?></a></li>
                                                                            <?php endforeach; ?>
                                                                            <?php foreach ($CustomFields as $Each): ?>
                                                                                <li><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" fieldtype="<?php echo $Each['FieldType']; ?>" validationmethod="<?php echo $Each['ValidationMethod']; ?>" values="<?php
                                                                                    if ($Each['FieldType'] == 'Multiple choice' || $Each['FieldType'] == 'Drop down' || $Each['FieldType'] == 'Checkboxes') {
                                                                                        echo addslashes(htmlspecialchars($Each['FieldOptions']));
                                                                                    }
                                                                                    ?>"><?php echo $Each['FieldName'] ?></a></li>
                                                                                <?php endforeach; ?>
                                                                            <li class="activity label"><?php InterfaceLanguage('Screen', '1401'); ?>:</li>
                                                                            <?php foreach ($ActivityFields as $Each): ?>
                                                                                <li class="activity"><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" validationmethod=""><?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        </ul>
                                                                    </div>
                                                                    <a href="#" id="clear-board-link" class="btn default btn-transparen btn-sm"><?php InterfaceLanguage('Screen', '1400'); ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn default" targetform="segment-create" id="form-button" href="#" targetform="segment-create"><strong><?php InterfaceLanguage('Screen', ($IsEditEvent ? '1419' : '1306'), false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_segmentcreate.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-doc font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp">  </span>                    
                </div>
                <div class="actions">
                    <a class="btn default" href="<?php InterfaceAppURL(); ?>/user/segments/browse/<?php echo $ListInformation['ListID'] ?>"><strong><?php InterfaceLanguage('Screen', '1134', false, '', false); ?></strong></a>
                </div>
            </div>
            <div class="portlet-body form">

            </div>
        </div>
    </div>
</div>-->


<script type="text/javascript" charset="utf-8">
    var lang = {
    '1406' : '<?php InterfaceLanguage('Screen', '1406'); ?>',
            '1407' : '<?php InterfaceLanguage('Screen', '1407'); ?>',
            '1408' : '<?php InterfaceLanguage('Screen', '1408'); ?>',
            '1409' : '<?php InterfaceLanguage('Screen', '1409'); ?>',
            '1410' : '<?php InterfaceLanguage('Screen', '1410'); ?>',
            '1411' : '<?php InterfaceLanguage('Screen', '1411'); ?>',
            '1412' : '<?php InterfaceLanguage('Screen', '1412'); ?>',
            '1413' : '<?php InterfaceLanguage('Screen', '1413'); ?>',
            '1414' : '<?php InterfaceLanguage('Screen', '1414'); ?>',
            '1415' : '<?php InterfaceLanguage('Screen', '1415'); ?>',
            '1416' : '<?php InterfaceLanguage('Screen', '1416'); ?>'
    };
    var defaultFields = [<?php
                                        for ($i = 0; $i < count($DefaultFields); $i++) {
                                            echo '"' . $DefaultFields[$i]['CustomFieldID'] . '"';
                                            if ($i < count($DefaultFields) - 1) {
                                                echo ',';
                                            }
                                        }
                                        ?>];
    var ruleOperators = {
<?php for ($i = 0; $i < count($RuleOperators); $i++): ?>
        '<?php echo $RuleOperators[$i]['Name'] ?>' : [
    <?php for ($j = 0; $j < count($RuleOperators[$i]['Operators']); $j++): ?>
            {
            'Label':'<?php InterfaceLanguage('Screen', '1303', false, $RuleOperators[$i]['Operators'][$j]); ?>',
                    'Value':'<?php echo $RuleOperators[$i]['Operators'][$j]; ?>'
            }<?php
        if ($j < count($RuleOperators[$i]['Operators']) - 1) {
            echo ',';
        }
        ?>
    <?php endfor; ?>
        ]<?php
    if ($i < count($RuleOperators) - 1) {
        echo ',';
    }
    ?>
<?php endfor; ?>
    };
    var rules = [
<?php
$tmpArray1 = array();
$tmpArray5 = array();
foreach ($SegmentInformation['RulesArray'] as $EachRule) {
    if (!is_array($EachRule[0])) {
        $tmpArray2 = array();
        foreach ($EachRule as $Key => $Value) {
            $tmpArray2[] = $Key . ':"' . $Value . '"';
        }
        $tmpArray1[] = '{' . implode(',', $tmpArray2) . '}';
    } else {
        $tmpArray3 = array();
        foreach ($EachRule as $EachSubRule) {
            $tmpArray4 = array();
            foreach ($EachSubRule as $Key => $Value) {
                $tmpArray4[] = $Key . ':"' . $Value . '"';
            }
            $tmpArray3[] = '{' . implode(',', $tmpArray4) . '}';
        }
        $tmpArray5[] = '[' . implode(',', $tmpArray3) . ']';
    }
}
if (count($tmpArray1) > 0 || count($tmpArray5) > 0) {
    print implode(',', $tmpArray1) . (count($tmpArray1) > 0 ? ',' : '') . implode(',', $tmpArray5);
}
?>
    ];
    var fieldLabels = {
<?php foreach ($DefaultFields as $Each): ?>
        '<?php echo $Each['CustomFieldID']; ?>':{'fieldLabel':'<?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?>', 'validationMethod':'<?php echo $Each['ValidationMethod']; ?>', 'values':''},
<?php endforeach; ?>
<?php foreach ($ActivityFields as $Each): ?>
        '<?php echo $Each['CustomFieldID']; ?>':{'fieldLabel':'<?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?>', 'validationMethod':'', 'fieldType':'', 'values':''},
<?php endforeach; ?>
<?php for ($i = 0; $i < count($CustomFields); $i++): ?>
        '<?php echo $CustomFields[$i]['CustomFieldID']; ?>':{'fieldLabel':'<?php echo htmlspecialchars($CustomFields[$i]['FieldName'], ENT_QUOTES); ?>', 'validationMethod':'<?php echo $CustomFields[$i]['ValidationMethod']; ?>', 'fieldType':'<?php echo $CustomFields[$i]['FieldType']; ?>', 'values':'<?php
    if ($CustomFields[$i]['FieldType'] == 'Multiple choice' || $CustomFields[$i]['FieldType'] == 'Drop down' || $CustomFields[$i]['FieldType'] == 'Checkboxes') {
        echo addslashes(htmlspecialchars($CustomFields[$i]['FieldOptions']));
    }
    ?>'}<?php if ($i < count($CustomFields) - 1): ?>,<?php endif; ?>
<?php endfor; ?>
    };
    var campaigns = [
<?php for ($i = 0; $i < count($Campaigns); $i++): ?>
        {'CampaignID':<?php echo $Campaigns[$i]['CampaignID']; ?>, 'CampaignName':'<?php echo addslashes(htmlspecialchars($Campaigns[$i]['CampaignName'])); ?>'}<?php if ($i < count($Campaigns) - 1): ?>,<?php endif; ?>
<?php endfor; ?>
    ];</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/segment_create.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>