<form id="campaign-create" class="form-inputs"  action="<?php echo $FormURL; ?>" method="post">
    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">
    <div>
        <div class="col-md-8" style="padding: 0;" >
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-purple-sharp"> 
                            <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>    
                        </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if ($SplitTestCampaign): ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1357', false, '', false, true); ?></h4>
                        <div class="form-group no-bg">
                            <span class="help-block"><?php InterfaceLanguage('Screen', '1358', false, '', false, false); ?></span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('EmailName') != '' ? 'has-error' : '')); ?>" id="form-row-EmailName">
                                    <label class="col-md-3 control-label" for="EmailName"><?php InterfaceLanguage('Screen', '1359', false, '', false, true); ?>:</label>
                                    <div class="col-md-6">
                                        <input type="text" name="EmailName" value="<?php echo set_value('EmailName', $FieldDefaultValues['EmailName']); ?>" id="EmailName" class="form-control" />
                                        <?php print(form_error('EmailName', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0759', false, '', false, true); ?></h4>
                    <div class="form-group no-bg">
                        <span class="help-block"><?php InterfaceLanguage('Screen', '0753', false, '', false, false); ?></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('FromName') != '' ? 'has-error' : '')); ?>" id="form-row-FromName">
                                <label class="col-md-3 control-label" for="FromName"><?php InterfaceLanguage('Screen', '0757', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="FromName" value="<?php echo set_value('FromName', $FieldDefaultValues['FromName']); ?>" id="FromName" class="form-control" />
                                    <?php print(form_error('FromName', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('FromEmail') != '' ? 'has-error' : '')); ?>" id="form-row-FromEmail">
                                <label class="col-md-3 control-label" for="FromEmail"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <!--<input type="text" name="FromEmail" value="<?php echo set_value('FromEmail', $FieldDefaultValues['FromEmail']); ?>" id="FromEmail" class="form-control" />-->
                                    <input type="text" name="FromEmail" readonly value="mail@flyinglist.com" id="FromEmail" class="form-control" />
                                    <?php print(form_error('FromEmail', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0760', false, '', false, true); ?></h4>
                    <div class="form-group no-bg">
                        <span class="help-block"><?php InterfaceLanguage('Screen', '0764', false, '', false, false); ?></span>
                    </div>
<!--                    <div class="form-group" id="form-row-SameNameEmail">
                        <div class="checkbox-container">
                            <div class="checkbox-row no-margin">
                                <input type="checkbox" name="SameNameEmail" value="true" id="SameNameEmail" <?php echo set_checkbox('SameNameEmail', 'true', $FieldDefaultValues['SameNameEmail'] ? true : false); ?>>
                                <label for="SameNameEmail"><?php InterfaceLanguage('Screen', '0769', false, '', false, false); ?></label>
                            </div>
                        </div>
                    </div>-->
                    <!--<div id="replyto-name-email-container" <?php if (isset($_POST['SameNameEmail'])): ?>style="display:none" <?php endif; ?>>-->
                    <div id="replyto-name-email-container" <?php if (isset($_POST['SameNameEmail'])): ?>" <?php endif; ?>>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('ReplyToName') != '' ? 'has-error' : '')); ?>" id="form-row-ReplyToName">
                                    <label class="col-md-3 control-label" for="ReplyToName"><?php InterfaceLanguage('Screen', '0757', false, '', false, true); ?>: *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="ReplyToName" value="<?php echo set_value('ReplyToName', $FieldDefaultValues['ReplyToName']); ?>" id="ReplyToName" class="form-control" />
                                        <?php print(form_error('ReplyToName', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group <?php print((form_error('ReplyToEmail') != '' ? 'has-error' : '')); ?>" id="form-row-ReplyToEmail">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-md-3 control-label" for="ReplyToEmail"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="ReplyToEmail" value="<?php echo set_value('ReplyToEmail', $FieldDefaultValues['ReplyToEmail']); ?>" id="ReplyToEmail" class="form-control" />
                                        <?php print(form_error('ReplyToEmail', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreatesettings.php'); ?>
        </div>
    </div>
    <div class="col-md-8" style="padding: 0;" >
        <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
    </div>

</form>

<script>
    $(document).ready(function () {
        settings_library.init_handlers();
    });
</script>