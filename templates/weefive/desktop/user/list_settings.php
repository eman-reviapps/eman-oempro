<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>


<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-body">
                                <form id="list-settings" class="form-inputs" action="<?php InterfaceAppURL(); ?>/user/list/settings/<?php echo $ListInformation['ListID']; ?>" method="post">
                                    <input type="hidden" name="Command" value="Save" id="Command">
                                    <?php
                                    if (isset($PageSuccessMessage) == true):
                                        ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageSuccessMessage); ?>
                                        </div>
                                        <?php
                                    elseif (isset($PageErrorMessage) == true):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageErrorMessage); ?>
                                        </div>
                                        <?php
                                    elseif (validation_errors()):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="alert alert-info alert-info-custom">
                                            <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                                        </div>
                                    <?php
                                    endif;
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12">  
                                            <div class="form-group <?php print((form_error('Name') != '' ? 'has-error' : '')); ?>" id="form-row-Name">
                                                <label class="col-md-3 control-label" for="Name"><?php InterfaceLanguage('Screen', '0940', false, '', false, true); ?>: *</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="Name" value="<?php echo set_value('Name', $ListInformation['Name']); ?>" id="Name" class="form-control"  />
                                                    <?php print(form_error('Name', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix" id="form-row-OptInMode">
                                        <?php if ($UserInformation['GroupInformation']['ForceOptInList'] == 'Disabled'): ?>
                                            <label class="col-md-3 control-label" for="OptInMode"><?php InterfaceLanguage('Screen', '0941', false, '', false, true); ?>:</label>
                                            <div class="col-md-6">
                                                <select name="OptInMode" id="OptInMode" class="form-control">
                                                    <option value="Double" <?php echo set_select('OptInMode', 'Double', $ListInformation['OptInMode'] == 'Double' ? true : false); ?>><?php InterfaceLanguage('Screen', '0942', false, '', false, false); ?></option>
                                                    <option value="Single" <?php echo set_select('OptInMode', 'Single', $ListInformation['OptInMode'] == 'Single' ? true : false); ?>><?php InterfaceLanguage('Screen', '0943', false, '', false, false); ?></option>
                                                </select>
                                                <span class="help-block"><?php InterfaceLanguage('Screen', '0944', false, '', false, false); ?></span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="clearfix" style="margin-bottom: 10px"></div>
                                        <?php if ($ListInformation['OptInMode'] == 'Double'): ?>
                                            <label>&nbsp;</label>
                                            <?php if ($ListInformation['RelOptInConfirmationEmailID'] == 0): ?>

                                                <div class="col-md-3">
                                                    <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/create/confirmation/<?php echo $ListInformation['ListID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1170', false, '', false); ?></strong></a>
                                                </div>
                                            <?php else: ?>
                                                <div class="col-md-8" style="margin-left: 5px">
                                                    <a class="btn default" id="form-button" href="<?php echo InterfaceAppURL(); ?>/user/email/edit/confirmation/<?php echo $ListInformation['ListID']; ?>/<?php echo $ListInformation['RelOptInConfirmationEmailID']; ?>" ><strong><?php InterfaceLanguage('Screen', '1311', false, '', false); ?></strong></a>
                                                    <a class="btn red" href="<?php echo InterfaceAppURL(); ?>/user/list/deleteconfirmationemail/<?php echo $ListInformation['ListID']; ?>" onclick="return confirm('<?php InterfaceLanguage('Screen', '1862'); ?>');" ><?php InterfaceLanguage('Screen', '1859'); ?></a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group" id="form-row-HideInSubscriberArea">
                                        <div class="checkbox-container">
                                            <div class="checkbox-row" style="margin-left:0px;">
                                                <input type="checkbox" name="HideInSubscriberArea" value="true" id="HideInSubscriberArea" <?php echo set_checkbox('HideInSubscriberArea', 'true', $ListInformation['HideInSubscriberArea'] == 'true'); ?>>
                                                <label for="HideInSubscriberArea"><?php InterfaceLanguage('Screen', '0945', false, '', false, false); ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group-note checkbox-note">
                                            <p><?php InterfaceLanguage('Screen', '0946', false, '', false, false); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group" id="form-row-SendActivityNotification">
                                        <div class="checkbox-container">
                                            <div class="checkbox-row" style="margin-left:0px;">
                                                <input type="checkbox" name="SendActivityNotification" value="true" id="SendActivityNotification" <?php echo set_checkbox('SendActivityNotification', 'true', $ListInformation['SendActivityNotification'] == 'true'); ?>>
                                                <label for="SendActivityNotification"><?php InterfaceLanguage('Screen', '1392', false, '', false, false); ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group-note checkbox-note">
                                            <p><?php InterfaceLanguage('Screen', '1376', false, '', false, false, array($UserInformation['EmailAddress'])); ?></p>
                                        </div>
                                    </div>
                                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1036', false); ?></h4>
                                    <div class="form-group">
                                        <label class="control-label"><?php InterfaceLanguage('Screen', '1005', false, '', false, true); ?>:</label>
                                        <div class="form-group-note">
                                            <p><?php InterfaceLanguage('Screen', '1037', false); ?></p>
                                        </div>
                                        <div class="checkbox-container">
                                            <div class="checkbox-row clearfix">
                                                <div class="col-md-5">
                                                    <input type="checkbox" name="OptInSubscribeToEnabled" value="true" id="OptInSubscribeToEnabled" <?php echo set_checkbox('OptInSubscribeToEnabled', 'true', $ListInformation['OptInSubscribeTo'] != 0 ? true : false); ?>>
                                                    <label for="OptInSubscribeToEnabled"><?php InterfaceLanguage('Screen', '1038', false, '', false, false); ?>:</label> <br />

                                                    <select class="form-control" name="OptInSubscribeTo" id="OptInSubscribeTo" style="margin-bottom: 10px" >
                                                        <?php foreach ($Lists as $EachList): ?>
                                                            <option value="<?php echo $EachList['ListID']; ?>" <?php echo set_select('OptInSubscribeTo', $EachList['ListID'], $EachList['ListID'] == $ListInformation['OptInSubscribeTo']); ?>><?php echo $EachList['Name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="checkbox-row clearfix">
                                                <div class="col-md-5">
                                                    <input type="checkbox" name="OptInUnsubscribeFromEnabled" value="true" id="OptInUnsubscribeFromEnabled" <?php echo set_checkbox('OptInUnsubscribeFromEnabled', 'true', $ListInformation['OptInUnsubscribeFrom'] != 0 ? true : false); ?>>
                                                    <label for="OptInUnsubscribeFromEnabled"><?php InterfaceLanguage('Screen', '1039', false, '', false, false); ?>:</label> <br />

                                                    <select class="form-control" name="OptInUnsubscribeFrom" id="OptInUnsubscribeFrom" style="margin-bottom: 10px">
                                                        <?php foreach ($Lists as $EachList): ?>
                                                            <option value="<?php echo $EachList['ListID']; ?>" <?php echo set_select('OptInUnsubscribeFrom', $EachList['ListID'], $EachList['ListID'] == $ListInformation['OptInUnsubscribeFrom']); ?>><?php echo $EachList['Name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="checkbox-row clearfix">
                                                <div class="col-md-12">
                                                    <input type="checkbox" name="SubscriptionConfirmationPendingPageURLEnabled" value="true" id="SubscriptionConfirmationPendingPageURLEnabled" <?php echo set_checkbox('SubscriptionConfirmationPendingPageURLEnabled', 'true', $ListInformation['SubscriptionConfirmationPendingPageURL'] != '' ? true : false); ?>>
                                                    <label for="SubscriptionConfirmationPendingPageURLEnabled"><?php InterfaceLanguage('Screen', '1146', false, '', false, false); ?>:</label> <br />

                                                    <div id="SubscriptionConfirmationPendingPageURLContainer">
                                                        <div class="col-md-5">
                                                            <input type="text" style="margin-bottom: 10px" name="SubscriptionConfirmationPendingPageURL" value="<?php echo set_value('SubscriptionConfirmationPendingPageURL', $ListInformation['SubscriptionConfirmationPendingPageURL']); ?>" id="SubscriptionConfirmationPendingPageURL" class="form-control"  />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <span class="help-block">
                                                                <?php InterfaceLanguage('Screen', '1149'); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox-row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" name="SubscriptionConfirmedPageURLEnabled" value="true" id="SubscriptionConfirmedPageURLEnabled" <?php echo set_checkbox('SubscriptionConfirmedPageURLEnabled', 'true', $ListInformation['SubscriptionConfirmedPageURL'] != '' ? true : false); ?>>
                                                    <label for="SubscriptionConfirmedPageURLEnabled"><?php InterfaceLanguage('Screen', '1147', false, '', false, false); ?>:</label> <br />
                                                    <div id="SubscriptionConfirmedPageURLContainer">
                                                        <div class="col-md-5">
                                                            <input type="text" style="margin-bottom: 10px" name="SubscriptionConfirmedPageURL" value="<?php echo set_value('SubscriptionConfirmedPageURL', $ListInformation['SubscriptionConfirmedPageURL']); ?>" id="SubscriptionConfirmedPageURL" class="form-control" style="margin-left:28px;" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <span class="help-block">
                                                                <?php InterfaceLanguage('Screen', '1149'); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox-row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" name="SubscriptionErrorPageURLEnabled" value="true" id="SubscriptionErrorPageURLEnabled" <?php echo set_checkbox('SubscriptionErrorPageURLEnabled', 'true', $ListInformation['SubscriptionErrorPageURL'] != '' ? true : false); ?>>
                                                    <label for="SubscriptionErrorPageURLEnabled"><?php InterfaceLanguage('Screen', '1814', false, '', false, false); ?>:</label> <br />
                                                    <div id="SubscriptionErrorPageURLContainer">
                                                        <div class="col-md-5">
                                                            <input type="text" style="margin-bottom: 10px" name="SubscriptionErrorPageURL" value="<?php echo set_value('SubscriptionErrorPageURL', $ListInformation['SubscriptionErrorPageURL']); ?>" id="SubscriptionErrorPageURL" class="form-control" style="margin-left:28px;" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <span class="help-block">
                                                                <?php InterfaceLanguage('Screen', '1815'); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?php InterfaceLanguage('Screen', '1040', false, '', false, true); ?>:</label>
                                        <div class="form-group-note">
                                            <p><?php InterfaceLanguage('Screen', '1041', false); ?></p>
                                        </div>
                                        <div class="checkbox-container">
                                            <?php if (SUBSCRIBE_ON_UNSUBSCRIPTION_ENABLED): ?>
                                                <div class="checkbox-row clearfix" >
                                                    <div class="col-md-5">
                                                        <input type="checkbox" name="OptOutSubscribeToEnabled" value="true" id="OptOutSubscribeToEnabled" <?php echo set_checkbox('OptOutSubscribeToEnabled', 'true', $ListInformation['OptOutSubscribeTo'] != 0 ? true : false); ?>>
                                                        <label for="OptOutSubscribeToEnabled"><?php InterfaceLanguage('Screen', '1038', false, '', false, false); ?>:</label> <br />
                                                        <select class="form-control" name="OptOutSubscribeTo" id="OptOutSubscribeTo" style="margin-bottom: 10px">
                                                            <?php foreach ($Lists as $EachList): ?>
                                                                <option value="<?php echo $EachList['ListID']; ?>" <?php echo set_select('OptOutSubscribeTo', $EachList['ListID'], $EachList['ListID'] == $ListInformation['OptOutSubscribeTo']); ?>><?php echo $EachList['Name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="checkbox-row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" disabled hidden checked name="OptOutScope" value="All lists" id="OptOutScope" >
                                                    <label for="OptOutScope"  style="display: none"><?php InterfaceLanguage('Screen', '1042', false, '', false, false); ?></label>
                                                </div>
                                            </div>
                                            <div class="checkbox-row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" disabled hidden checked name="OptOutAddToGlobalSuppressionList" value="Yes" id="OptOutAddToGlobalSuppressionList" >
                                                    <label for="OptOutAddToGlobalSuppressionList" style="display: none"><?php InterfaceLanguage('Screen', '1439', false, '', false, false); ?></label>
                                                </div>
                                            </div>
                                            <?php if (REDIRECT_ON_UNSUBSCRIPTION_ENABLED): ?>
                                                <div class="checkbox-row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="UnsubscriptionConfirmedPageURLEnabled" value="true" id="UnsubscriptionConfirmedPageURLEnabled" <?php echo set_checkbox('UnsubscriptionConfirmedPageURLEnabled', 'true', $ListInformation['UnsubscriptionConfirmedPageURL'] != '' ? true : false); ?>>
                                                        <label for="UnsubscriptionConfirmedPageURLEnabled"><?php InterfaceLanguage('Screen', '1148', false, '', false, false); ?>:</label> <br />
                                                        <div id="UnsubscriptionConfirmedPageURLContainer">
                                                            <div class="col-md-5">
                                                                <input type="text" name="UnsubscriptionConfirmedPageURL" value="<?php echo set_value('UnsubscriptionConfirmedPageURL', $ListInformation['UnsubscriptionConfirmedPageURL']); ?>" id="UnsubscriptionConfirmedPageURL" class="form-control" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <span class="help-block">
                                                                    <?php InterfaceLanguage('Screen', '1149'); ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkbox-row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="UnsubscriptionErrorPageURLEnabled" value="true" id="UnsubscriptionErrorPageURLEnabled" <?php echo set_checkbox('UnsubscriptionErrorPageURLEnabled', 'true', $ListInformation['UnsubscriptionErrorPageURL'] != '' ? true : false); ?>>
                                                        <label for="UnsubscriptionErrorPageURLEnabled"><?php InterfaceLanguage('Screen', '1810', false, '', false, false); ?>:</label> <br />
                                                        <div id="UnsubscriptionErrorPageURLContainer">
                                                            <div class="col-md-5">
                                                                <input type="text" name="UnsubscriptionErrorPageURL" value="<?php echo set_value('UnsubscriptionErrorPageURL', $ListInformation['UnsubscriptionErrorPageURL']); ?>" id="UnsubscriptionErrorPageURL" class="form-control" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <span class="help-block">
                                                                    <?php InterfaceLanguage('Screen', '1811'); ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br/>
                                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1377', false); ?></h4>
                                    <a name="WebServiceIntegration"></a>
                                    <?php if ($ServiceURLs != false): ?>
                                        <div class="form-group no-bg">
                                            <table class="small-grid table table-bordered">
                                                <tr>
                                                    <th><?php InterfaceLanguage('Screen', '1393'); ?></th>
                                                </tr>
                                                <?php foreach ($ServiceURLs as $Each): ?>
                                                    <tr>
                                                        <td class="service-url" id="service-row-<?php echo $Each['WebServiceIntegrationID']; ?>">
                                                            <a class="service-url-link" href="<?php echo $Each['ServiceURL']; ?>" target="_blank"><?php echo htmlentities($Each['ServiceURL']); ?></a><br />
                                                            <span class="data-label small"><?php InterfaceLanguage('Screen', '1382', false, $Each['EventType']); ?></span>
                                                            <span class="data-label small service-url-status"></span>
                                                            <a urlid="<?php echo $Each['WebServiceIntegrationID']; ?>" href="#" class="small remove-service-url" style="margin-left:6px;"><?php InterfaceLanguage('Screen', '0507'); ?></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-12">  
                                            <div class="form-group <?php print((form_error('ServiceURL') != '' ? 'has-error' : '')); ?>" style="margin-bottom:0px;" id="form-row-ServiceURL">
                                                <label class="col-md-3 control-label" for="ServiceURL"><?php InterfaceLanguage('Screen', '1381', false, '', false, true); ?></label>
                                                <div class="col-md-4">
                                                    <input type="text" name="ServiceURL" value="" id="ServiceURL" class="form-control" >
                                                    <?php print(form_error('ServiceURL', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="help-block">
                                                        <?php InterfaceLanguage('Screen', '1387'); ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            <div class="form-group" style="margin-top:0px;" id="form-row-EventType">
                                                <label class="col-md-3 control-label" for="EventType"><?php InterfaceLanguage('Screen', '1380', false, '', false, true); ?></label>
                                                <div class="col-md-4">
                                                    <select class="form-control" name="EventType" id="EventType">
                                                        <option value="subscription"><?php InterfaceLanguage('Screen', '1378'); ?></option>
                                                        <option value="unsubscription"><?php InterfaceLanguage('Screen', '1379'); ?></option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">                                                
                                                    <a class="btn default" id="service-url-form-button" href="#" ><strong><?php InterfaceLanguage('Screen', '0613', false, '', true); ?></strong></a>                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <?php if (EMAIL_REQUESTS_ENABLED): ?>
                                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1044', false); ?></h4>
                                        <div class="form-group clearfix" id="form-row-ReqByEmailSearchToAddress">
                                            <label class="col-md-3 control-label" for="ReqByEmailSearchToAddress"><?php InterfaceLanguage('Screen', '0010', false); ?></label>
                                            <div class="col-md-5">
                                                <input type="text" name="ReqByEmailSearchToAddress" value="<?php echo set_value('ReqByEmailSearchToAddress', $ListInformation['ReqByEmailSearchToAddress']); ?>" id="ReqByEmailSearchToAddress" class="form-control">
                                                <span class="help-block">
                                                    <?php InterfaceLanguage('Screen', '1045', false); ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix" id="form-row-ReqByEmailSubscriptionCommand">
                                            <label class="col-md-3 control-label" for="ReqByEmailSubscriptionCommand"><?php InterfaceLanguage('Screen', '1046', false); ?></label>
                                            <div class="col-md-8">
                                                <input type="text" name="ReqByEmailSubscriptionCommand" value="<?php echo set_value('ReqByEmailSubscriptionCommand', $ListInformation['ReqByEmailSubscriptionCommand']); ?>" id="ReqByEmailSubscriptionCommand" class="form-control">
                                                <span class="help-block">
                                                    <?php InterfaceLanguage('Screen', '1048', false); ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix" id="form-row-ReqByEmailUnsubscriptionCommand">
                                            <label class="col-md-3 control-label" for="ReqByEmailUnsubscriptionCommand"><?php InterfaceLanguage('Screen', '1049', false); ?></label>
                                            <div class="col-md-8">
                                                <input type="text" name="ReqByEmailUnsubscriptionCommand" value="<?php echo set_value('ReqByEmailUnsubscriptionCommand', $ListInformation['ReqByEmailUnsubscriptionCommand']); ?>" id="ReqByEmailUnsubscriptionCommand" class="form-control">
                                                <span class="help-block">
                                                    <?php InterfaceLanguage('Screen', '1050', false); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </form>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a class="btn default" targetform="list-settings" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var API_URL = '<?php InterfaceInstallationURL(); ?>api.php';
    var ListID = <?php print($ListInformation['ListID']) ?>;
    var Language = {
        '1383': '<?php InterfaceLanguage('Screen', '1383'); ?>',
        '1384': '<?php InterfaceLanguage('Screen', '1384'); ?>',
        '1386': '<?php InterfaceLanguage('Screen', '1386'); ?>'
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/list_settings.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>