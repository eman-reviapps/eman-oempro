<?php
$AllLists = array();
$AllLists[] = array('value' => '', 'text' => ' -- Select List -- ');
foreach ($Lists as $EachList) {
    if ($EachList['IsAutomationList'] == 'No' && $EachList['ListID'] != $ListID)
        $AllLists[] = array('value' => $EachList['ListID'], 'text' => $EachList['Name']);
}
?>
<script>
    url_move_to_suppression = APP_URL + "/user/subscribers/move_to_suppression";
    url_copy_to_another_list = APP_URL + "/user/subscribers/copy_to_another_list";
    url_move_to_another_list = APP_URL + "/user/subscribers/move_to_another_list";
    url_unsubscribe = APP_URL + "/user/subscribers/unsubscribe";

    var lang_subscribers = {
        '1309': '<?php InterfaceLanguage('Screen', '1309'); ?>',
        '9242': '<?php InterfaceLanguage('Screen', '9242'); ?>',
        '9243': '<?php InterfaceLanguage('Screen', '9243'); ?>',
        '9244': '<?php InterfaceLanguage('Screen', '9244'); ?>',
        '9245': '<?php InterfaceLanguage('Screen', '9245'); ?>',
        '9246': '<?php InterfaceLanguage('Screen', '9246'); ?>',
        '9247': '<?php InterfaceLanguage('Screen', '9247'); ?>',
        '9248': '<?php InterfaceLanguage('Screen', '9248'); ?>',
        '9250': '<?php InterfaceLanguage('Screen', '9250'); ?>',
        '9251': '<?php InterfaceLanguage('Screen', '9251'); ?>',
        '9289': '<?php InterfaceLanguage('Screen', '9289'); ?>',
    };
    var ListsArray = JSON.parse('<?php print_r(json_encode($AllLists)) ?>');
//    var ListsArray = []
//    AllLists.forEach(function (entry) {
//        ListsArray.push([entry.day, parseInt(entry.Total)]);
//    });
</script>
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-subscribers.js" type="text/javascript"></script>

<div class="module-container" style="display:none;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?php
            if (isset($PageSuccessMessage) == true):
                ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageSuccessMessage); ?>
                </div>
                <?php
            elseif (isset($PageErrorMessage) == true):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php print($PageErrorMessage); ?>
                </div>
                <?php
            endif;
            ?>
            <div class="grid-operations">
                <div class="inner">

                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover dt-responsive dataTable no-footer dtr-inline" id="sample_1">
    <thead>
        <tr>
            <th></th>
            <th>
                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    <span></span>
                </label>
            </th>

            <?php
            foreach ($Subscribers[0] as $key => $value) {
                ?>
                <th> <?php echo $key ?> </th>
                <?php
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($Subscribers as $Subscriber) {
            ?>
            <tr class="subscriber-row" subscriberid = "<?php echo $Subscriber['SubscriberID'] ?>">
    <!--                <td>
                    <input class="grid-check-element" type="checkbox" name="SelectedSubscribers[]" value="<?php print($Subscriber['SubscriberID']); ?>" id="SelectedSubscriber<?php print($Subscriber['SubscriberID']); ?>">
                </td>-->
                <td></td>
                <td class="mt-checkbox-td">
                    <label style="margin-left: 8px" class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="<?php print($Subscriber['SubscriberID']); ?>" />
                        <span></span>
                    </label>
                </td>

                <?php
                foreach ($Subscriber as $key => $value) {
                    ?>
                    <td> <?php echo $value ?> </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>