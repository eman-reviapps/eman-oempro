<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-body">
                                <form id="list-synchrnoization" action="<?php InterfaceAppURL(); ?>/user/list/synchronization/<?php echo $ListInformation['ListID']; ?>" method="post">
                                    <input type="hidden" name="Command" value="Save" id="Command">
                                    <input type="hidden" name="SyncFieldMapping" value="<?php echo $ListInformation['SyncFieldMapping']; ?>" id="SyncFieldMapping">
                                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                                    <?php
                                    if (isset($PageSuccessMessage) == true):
                                        ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageSuccessMessage); ?>
                                        </div>
                                        <?php
                                    elseif (isset($PageErrorMessage) == true):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php print($PageErrorMessage); ?>
                                        </div>
                                        <?php
                                    elseif (validation_errors()):
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="alert alert-info alert-info-custom">
                                            <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                                        </div>
                                    <?php
                                    endif;
                                    ?>
                                    <div class="form-group clearfix" id="form-row-SyncStatus">
                                        <label class="col-md-3 control-label" for="SyncStatus"><?php InterfaceLanguage('Screen', '0053', false, '', false, true); ?>:</label>
                                        <div class="col-md-3">
                                            <select name="SyncStatus" id="SyncStatus" class="form-control">
                                                <option value="Enabled" <?php echo set_select('SyncStatus', 'Enabled', $ListInformation['SyncStatus'] == 'Enabled'); ?>><?php InterfaceLanguage('Screen', '0012', false, 'Enabled'); ?></option>
                                                <option value="Disabled" <?php echo set_select('SyncStatus', 'Disabled', $ListInformation['SyncStatus'] == 'Disabled'); ?>><?php InterfaceLanguage('Screen', '0012', false, 'Disabled'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix" id="form-row-SyncPeriod">
                                        <label class="col-md-3 control-label" for="SyncPeriod"><?php InterfaceLanguage('Screen', '1258', false, '', false, true); ?>:</label>
                                        <div class="col-md-3">
                                            <select name="SyncPeriod" id="SyncPeriod" class="form-control">
                                                <option value="Every Month" <?php echo set_select('SyncPeriod', 'Every Month', $ListInformation['SyncPeriod'] == 'Every Month'); ?>><?php InterfaceLanguage('Screen', '0861'); ?></option>
                                                <option value="Every Week" <?php echo set_select('SyncPeriod', 'Every Week', $ListInformation['SyncPeriod'] == 'Every Week'); ?>><?php InterfaceLanguage('Screen', '1259'); ?></option>
                                                <option value="Every Day" <?php echo set_select('SyncPeriod', 'Every Day', $ListInformation['SyncPeriod'] == 'Every Day'); ?>><?php InterfaceLanguage('Screen', '1260'); ?></option>
                                                <option value="Every Hour" <?php echo set_select('SyncPeriod', 'Every Hour', $ListInformation['SyncPeriod'] == 'Every Hour'); ?>><?php InterfaceLanguage('Screen', '1261'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="form-row-SyncSendReportEmail">
                                        <div class="checkbox-container">
                                            <div class="checkbox-row" style="margin-left:0px;">
                                                <input type="checkbox" name="SyncSendReportEmail" value="Yes" id="SyncSendReportEmail" <?php echo set_checkbox('SyncSendReportEmail', 'Yes', $ListInformation['SyncSendReportEmail'] == 'Yes'); ?>>
                                                <label for="SyncSendReportEmail"><?php InterfaceLanguage('Screen', '1262', false, '', false, false); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id ="sync-settings" style="display:none">
                                        <?php if (!$MapFields): ?>
                                            <?php if (($ListInformation['SyncStatus'] == 'Disabled' || $ListInformation['SyncStatus'] == 'Enabled') && $ListInformation['SyncFieldMapping'] == ''): ?>
                                                <input type="hidden" name="MapField" value="true" id="MapField">
                                                <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1264', false, '', false, true); ?></h4>
                                                <div class="form-group no-bg">
                                                    <p><?php InterfaceLanguage('Screen', '1263'); ?></p>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLHost') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLHost">
                                                    <label class="col-md-3 control-label" for="SyncMySQLHost"><?php InterfaceLanguage('Screen', '1200', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="SyncMySQLHost" value="<?php echo set_value('SyncMySQLHost', $ListInformation['SyncMySQLHost']); ?>" id="SyncMySQLHost" class="form-control" />
                                                        <?php print(form_error('SyncMySQLHost', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLPort') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLPort">
                                                    <label class="col-md-3 control-label" for="SyncMySQLPort"><?php InterfaceLanguage('Screen', '1201', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-2">
                                                        <input type="text" name="SyncMySQLPort" value="<?php echo set_value('SyncMySQLPort', $ListInformation['SyncMySQLPort']); ?>" id="SyncMySQLPort" class="form-control" />
                                                        <?php print(form_error('SyncMySQLPort', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLUsername') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLUsername">
                                                    <label class="col-md-3 control-label" for="SyncMySQLUsername"><?php InterfaceLanguage('Screen', '1202', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="SyncMySQLUsername" value="<?php echo set_value('SyncMySQLUsername', $ListInformation['SyncMySQLUsername']); ?>" id="SyncMySQLUsername" class="form-control" />
                                                        <?php print(form_error('SyncMySQLUsername', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLPassword') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLPassword">
                                                    <label class="col-md-3 control-label" for="SyncMySQLPassword"><?php InterfaceLanguage('Screen', '1203', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="password" name="SyncMySQLPassword" value="<?php echo set_value('SyncMySQLPassword', $ListInformation['SyncMySQLPassword']); ?>" id="SyncMySQLPassword" class="form-control" />
                                                        <?php print(form_error('SyncMySQLPassword', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLDBName') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLDBName">
                                                    <label class="col-md-3 control-label" for="SyncMySQLDBName"><?php InterfaceLanguage('Screen', '1204', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="SyncMySQLDBName" value="<?php echo set_value('SyncMySQLDBName', $ListInformation['SyncMySQLDBName']); ?>" id="SyncMySQLDBName" class="form-control" />
                                                        <?php print(form_error('SyncMySQLDBName', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix <?php print((form_error('SyncMySQLQuery') != '' ? 'has-error' : '')); ?>" id="form-row-SyncMySQLQuery">
                                                    <label class="col-md-3 control-label" for="SyncMySQLQuery"><?php InterfaceLanguage('Screen', '1205', false, '', false, true); ?>: *</label>
                                                    <div class="col-md-6">
                                                        <textarea name="SyncMySQLQuery" id="SyncMySQLQuery" class="form-control" rows="5"><?php echo set_value('SyncMySQLQuery', $ListInformation['SyncMySQLQuery']); ?></textarea>
                                                        <?php print(form_error('SyncMySQLQuery', '<span class="help-block">', '</span>')); ?>
                                                    </div>
                                                </div>
                                            <?php elseif (($ListInformation['SyncStatus'] == 'Enabled' || $ListInformation['SyncStatus'] == 'Disabled') && $ListInformation['SyncFieldMapping'] != ''): ?>
                                                <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1267', false, '', false, true); ?></h3>
                                                    <div class="form-group no-bg">
                                                        <p><?php InterfaceLanguage('Screen', '1266', false, '', false, false, array($ListInformation['SyncMySQLDBName'], $ListInformation['SyncMySQLHost'])); ?></p>
                                                    </div>
                                                    <div class="form-group no-bg">
                                                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                                                            <tr>
                                                                <th><?php InterfaceLanguage('Screen', '1268'); ?>
                                                                <th><?php InterfaceLanguage('Screen', '1269'); ?>
                                                            </tr>
                                                            <?php
                                                            $FieldMappings = explode(',', $ListInformation['SyncFieldMapping']);
                                                            foreach ($FieldMappings as $each) {
                                                                $mapping = explode('||||', $each);
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $mapping[0]; ?></td>
                                                                    <td><?php echo $mapping[1]; ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                    </div>
                                                    <div class="form-group no-bg">
                                                        <a class="button" id="reset-sync-form-button" href="#" style="float:left"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1270', false, '', true); ?></strong></a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <input type="hidden" name="SaveSyncDBSettings" value="true" id="SaveSyncDBSettings">
                                                <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1267', false, '', false, true); ?></h3>
                                                    <div class="form-group no-bg">
                                                        <p><?php InterfaceLanguage('Screen', '1282'); ?></p>
                                                    </div>
                                                    <div class="form-group no-bg">
                                                        <table class="small-grid" style="margin-top:18px;">
                                                            <?php
                                                            $i = 1;
                                                            foreach ($ImportFields as $EachField):
                                                                ?>
                                                                <tr>
                                                                    <td style="text-align:right;"><?php echo $EachField; ?></td>
                                                                    <td>
                                                                        <select name="MatchedFields<?php echo $i; ?>" id="MatchedFields<?php echo $i; ?>">
                                                                            <option value="0"><?php InterfaceLanguage('Screen', '1183'); ?></option>
                                                                            <option value="EmailAddress"><?php InterfaceLanguage('Screen', '0010'); ?></option>
                                                                            <?php foreach ($CustomFields as $EachCustomField): ?>
                                                                                <option value="CustomField<?php echo $EachCustomField['CustomFieldID']; ?>"><?php echo $EachCustomField['FieldName']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            endforeach;
                                                            ?>
                                                        </table>
                                                    </div>
                                                <?php endif; ?>
                                                </div>
                                                </form>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($ListInformation['SyncFieldMapping'] == ''): ?>
                                                                <a class="btn default" targetform="list-synchrnoization" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1265', false, '', false); ?></strong></a>
                                                            <?php else: ?>
                                                                <a class="btn default" targetform="list-synchrnoization" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '0304', false, '', false); ?></strong></a>
                                                                    <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>


                                                <script type="text/javascript" charset="utf-8">
                                                    function toggleSyncSettings() {
                                                        if ($('#SyncStatus').val() == 'Enabled') {
                                                            $('#sync-settings').show();
                                                        } else {
                                                            $('#sync-settings').hide();
                                                        }
                                                    }
                                                    $(document).ready(function () {
                                                        $('#SyncStatus').change(toggleSyncSettings);
                                                        toggleSyncSettings();
                                                        $('#reset-sync-form-button').click(function () {
                                                            $('#Command').val('resetSyncSettings');
                                                            $('#list-synchrnoization').submit();
                                                        });

                                                    });
                                                </script>

                                                <?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>