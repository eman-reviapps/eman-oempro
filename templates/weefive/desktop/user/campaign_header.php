<script type="text/javascript" charset="utf-8">
    var CampaignID = <?php echo $CampaignInformation['CampaignID']; ?>;
    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
</script>
 <!--light bg-inverse-->
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 15px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php print(ucwords($CampaignInformation['CampaignName'])); ?>
                <?php if ($CampaignInformation['SplitTest'] != false): ?>
                    <span class="label system"><?php InterfaceLanguage('Screen', '1363', false, '', false, false, array()); ?></span>
                <?php endif; ?>
                <?php
                if (count($CampaignInformation['Tags']) > 0):
                    foreach ($CampaignInformation['Tags'] as $EachTag):
                        ?>
                        <span class="tag-label label"><?php echo $EachTag['Tag']; ?> <a href="#" id="tag-id-<?php echo $EachTag['TagID']; ?>" class="remove-label-link">x</a></span>
                        <?php
                    endforeach;
                endif;
                ?>
            </span>
        </div>
        <div class="actions">
            <?php
            if (($CampaignInformation['SplitTest'] != false) && ($CampaignInformation['RelWinnerEmailID'] == 0)) {
                $TestEmailVersions = Emails::RetrieveEmailsOfTest($CampaignInformation['SplitTest']['TestID'], $CampaignInformation['CampaignID']);
            }

            $EmailID = ($CampaignInformation['SplitTest'] == false ? $CampaignInformation['RelEmailID'] : ($CampaignInformation['SplitTest']['RelWinnerEmailID'] == 0 ? $TestEmailVersions[0]['RelEmailID'] : $CampaignInformation['SplitTest']['RelWinnerEmailID']));

            // Encrypted query parameters - Start
            $ArrayQueryParameters = array(
                'CampaignID' => $CampaignInformation['CampaignID'],
                'EmailID' => $EmailID,
                'AutoResponderID' => 0,
                'fbrefresh' => uniqid(),
            );
            $EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
            // Encrypted query parameters - End

            $CampaignPublicLink = APP_URL . 'web_browser.php?p=' . $EncryptedQuery;

//            $Hash = Core::ShortenLink($CampaignPublicLink);
//            $CampaignPublicLink = APP_URL . 'link.php?p=' . rawurlencode($Hash);
            ?>

            <a class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/email/preview/<?php echo $EmailID; ?>/html/campaign/<?php echo $CampaignInformation['CampaignID']; ?>" target="_blank"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0850', false, '', false, true); ?></strong></a>

            <div class="btn-group btn-group-solid">
                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-horizontal"></i> <?php InterfaceLanguage('Screen', '9201'); //0846      ?>                    
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0847', false, '', false, true); ?>
                        </a>
                    </li>
                    <?php if (InterfacePrivilegeCheck('Campaign.Delete', $UserInformation)): ?>
                        <li>
                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/delete/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0848', false, '', false, true); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php
                    InterfacePluginMenuHook('Campaign.Navigation.Options', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
                    );
                    ?>
                </ul>
            </div>

<!--            <div class="btn-group btn-group-solid">
                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-horizontal"></i> <?php InterfaceLanguage('Screen', '0835'); ?>
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0836', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/opens"><?php InterfaceLanguage('Screen', '0837', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/clicks"><?php InterfaceLanguage('Screen', '0838', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/forwards"><?php InterfaceLanguage('Screen', '0839', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/browserviews"><?php InterfaceLanguage('Screen', '0840', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/unsubscriptions"><?php InterfaceLanguage('Screen', '0841', false, '', false, true); ?></a></li>
                    <?php
                    InterfacePluginMenuHook('Campaign.Navigation.Reports', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
                    );
                    ?>
                </ul>
            </div>-->

            <div class="btn-group btn-group-solid">
                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-horizontal"></i> <?php InterfaceLanguage('Screen', '9204'); ?>
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/<?php print strtolower($SubSection); ?>/csv"><?php InterfaceLanguage('Screen', '0844', false, '', false, true); ?></a></li>
                    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/<?php print strtolower($SubSection); ?>/xml"><?php InterfaceLanguage('Screen', '0845', false, '', false, true); ?></a></li>
                    <?php
                    InterfacePluginMenuHook('Campaign.Navigation.ReportOptions', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
                    );
                    ?>
                </ul>
            </div>

            <div class="btn-group btn-group-solid">
                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-horizontal"></i> <?php InterfaceLanguage('Screen', '1424'); ?>
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <?php 
                    $campaign_name = ucwords($CampaignInformation['CampaignName']);
                    $campaign_img_url = isset($CampaignInformation['Email']['ScreenshotImage']) && !empty($CampaignInformation['Email']['ScreenshotImage']) ? APP_URL . "data/screenshots/" . $CampaignInformation['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                    ?>
                    <li><a href="http://twitter.com/?status=<?php print(rawurlencode(InterfaceLanguage('Screen', '1635', true, '', false, false, array(rawurldecode($CampaignPublicLink))))); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1629', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-twitter-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1629', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1629', false, '', false); ?></a></li>
                    <li><a href="http://www.facebook.com/share.php?u=<?php print($CampaignPublicLink); ?>" target="_blank" title="<?php print(ucwords($CampaignInformation['CampaignName'])); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-facebook-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1630', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1630', false, '', false); ?></a></li>
                    <li><a href="http://www.myspace.com/Modules/PostTo/Pages/?u=<?php print($CampaignPublicLink); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1631', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-myspace-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1631', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1631', false, '', false); ?></a></li>
                    <li><a href="http://www.stumbleupon.com/submit?url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1632', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-stumbleupon-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1632', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1632', false, '', false); ?></a></li>
                    <li><a href="http://digg.com/submit?phase=2&url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1633', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-digg-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1633', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1633', false, '', false); ?></a></li>
                    <li><a href="http://del.icio.us/post?url=<?php print($CampaignPublicLink); ?>&title=<?php echo urlencode($CampaignInformation['CampaignName']); ?>" target="_blank" title="<?php InterfaceLanguage('Screen', '1634', false, '', false); ?>"><img src="<?php InterfaceTemplateURL(); ?>/images/icon-delicious-16x16.png" width="16" height="16" alt="<?php InterfaceLanguage('Screen', '1634', false, '', false); ?>"><?php InterfaceLanguage('Screen', '1634', false, '', false); ?></a></li>
                </ul>
            </div>

        </div>
    </div>
    <?php Plugins::HookListener('Action', 'UI.Campaign.Details', array($CampaignInformation)); ?>
</div>
