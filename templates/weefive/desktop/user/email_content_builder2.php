<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_content_builder_header2.php'); ?>

<div id="top" class="iguana">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-7">
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#">
                                <span class="left">&nbsp;</span>
                                <span class="right">&nbsp;</span>
                                <strong><?php InterfaceLanguage('Screen', '0801', false, '', true, false, array()); ?></strong>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5" style="margin-top: 4px">
                <?php if (!empty($Message)): ?>
                <span class="alert-success" style="padding: 4px;margin: 5px 5px 0">
                    <?php echo $Message; ?>
                </span>
                <?php endif; ?>                
                <a id="content-save-button" class="btn btn-sm default" href="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/" targetform="save-content-form"><strong><?php InterfaceLanguage('Screen', '0304', false, '', false, false); ?></strong></a>
                <a href="<?php echo $ReturnURL; ?>" class="btn btn-sm red" ><?php InterfaceLanguage('Screen', '0964', false, '', false, true); ?></a>
            </div>
        </div>
    </div>
</div>

<div id="middle" class="iguana"  style="padding-top:0;background-color:#fff;">
    <div class="container">
        <div class="span-24 last">
            <iframe name="email-source" id="email-source" width="100%" height="600" marginwidth="0" marginheight="0" frameborder="0"></iframe>
        </div>
    </div>
</div>

<div class="panel-container">
    <div class="panel-inner">

        <div id="single-editable-panel" style="display:none">
            <div class="properties-panel">
                <input type="text" name="single-editable-text-input" value="" id="single-editable-text-input" class="text personalized" style="width:960px" />
                <div class="form-row-note personalization" id="personalization-single-editable-text-input" style="width:976px;margin-left:0px;">
                    <p>
                        <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                        <select name="personalization-select-single-editable-text-input" id="personalization-select-single-editable-text-input" class="personalization-select">
                            <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                            <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                <optgroup label="<?php print($Label); ?>">
                                    <?php foreach ($TagGroup as $Tag => $Label): ?>
                                        <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>&nbsp;&nbsp;
                    </p>
                </div>
            </div>
        </div>
        <div id="plain-editable-panel" style="display:none">
            <div class="properties-panel">
                <textarea name="plain-editable-text-input" id="plain-editable-text-input" class="textarea personalized" style="width:960px"></textarea>
                <div class="form-row-note personalization" id="personalization-plain-editable-text-input" style="width:976px;margin-left:0px;">
                    <p>
                        <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                        <select name="personalization-select-plain-editable-text-input" id="personalization-select-plain-editable-text-input" class="personalization-select">
                            <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                            <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                <optgroup label="<?php print($Label); ?>">
                                    <?php foreach ($TagGroup as $Tag => $Label): ?>
                                        <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>&nbsp;&nbsp;
                    </p>
                </div>
            </div>
        </div>
        <div id="rich-editable-panel" style="display:none">
            <div class="properties-panel">
                <textarea name="rich-editable-text-input" id="rich-editable-text-input" class="textarea" style="width:960px;height:250px;"></textarea>
            </div>
        </div>
        <div class="element-panel" style="position:static">
            <p style="margin-top:9px;">
                <a href="#" id="hide-panel-button" class="last" style="margin-left:0px;"><?php InterfaceLanguage('Screen', '0500', false); ?></a>
            </p>
        </div>
    </div>

</div>

<form id="save-content-form" action="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/saveemail/" method="post">
    <input type="hidden" name="ReturnURL" value="<?php echo $ReturnURL; ?>" id="RedirectURL">
    <input type="hidden" name="Mode" value="<?php echo $Mode; ?>" id="Mode">
    <input type="hidden" name="EmailID" value="<?php echo $EmailID; ?>" id="EmailID">
    <input type="hidden" name="SaveURL" value="<?php echo InterfaceAppURL(true) . $SaveURL; ?>" id="SaveURL">
    <textarea id="frame-html-container-for-post" name="EmailHTMLContent" style="display:none"></textarea>
</form>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_email_content_builder_footer.php'); ?>
