<div class="row">
    <div class="col-md-8">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="subscriber-import" action="<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>/<?php echo $CurrentFlow->get_id(); ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1182', false); ?></h4>
                    <?php
                    endif;
                    ?>
                    <table class="small-grid" style="margin-top:18px;">
                        <?php
                        $i = 1;
                        foreach ($ImportFields as $EachField):
                            ?>
                            <tr>
                                <td style="text-align:right;"><?php echo $EachField; ?></td>
                                <td>
                                    <?php
                                    $IsEmailAddress = false;
                                    if (function_exists('filter_var') == true) {
                                        if (filter_var($EachField, FILTER_VALIDATE_EMAIL) !== false) {
                                            $IsEmailAddress = true;
                                        }
                                    }
                                    ?>
                                    <select name="MatchedFields<?php echo $i; ?>" class="form-control" id="MatchedFields<?php echo $i; ?>">
                                        <option value="0"><?php InterfaceLanguage('Screen', '1183'); ?></option>
                                        <option value="EmailAddress" <?php print(($IsEmailAddress == true ? 'selected="selected"' : '')); ?>><?php InterfaceLanguage('Screen', '0010'); ?></option>
                                        <?php foreach ($CustomFields as $EachCustomField): ?>
                                            <option value="<?php echo $EachCustomField['CustomFieldID']; ?>"><?php echo $EachCustomField['FieldName']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </table>

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1326', false); ?></h4>
                    <div class="form-group" id="suppression-list-radio-container">
                        <div class="checkbox-container">
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="radio" name="AddToSuppressionList" value="none" id="DontAddToSuppressionList" checked="checked">
                                <label for="DontAddToSuppressionList"><?php InterfaceLanguage('Screen', '1330', false); ?></label>
                            </div>
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="radio" name="AddToSuppressionList" value="global" id="AddToGlobalSuppressionList">
                                <label for="AddToGlobalSuppressionList"><?php InterfaceLanguage('Screen', '1331', false); ?></label>
                            </div>
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="radio" name="AddToSuppressionList" value="list" id="AddToListsSuppressionList">
                                <label for="AddToListsSuppressionList"><?php InterfaceLanguage('Screen', '1332', false); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="import-settings">
                        <div class="checkbox-container">
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="checkbox" name="UpdateDuplicates" value="true" id="UpdateDuplicates" checked="checked">
                                <label for="UpdateDuplicates"><?php InterfaceLanguage('Screen', '1329', false); ?></label>
                            </div>
                            <?php if ($ListInformation['OptInMode'] == 'Double'): ?>
                                <?php if ($UserInformation['GroupInformation']['ForceOptInList'] == 'Disabled'): ?>
                                    <div class="checkbox-row" style="margin-left:0px;">
                                        <input type="checkbox" name="DoNotSendOptInConfirmationEmail" value="true" id="DoNotSendOptInConfirmationEmail" checked="checked">
                                        <label for="DoNotSendOptInConfirmationEmail"><?php InterfaceLanguage('Screen', '1327', false); ?></label>
                                    </div>
                                <?php else: ?>
                                    <input type="hidden" name="DoNotSendOptInConfirmationEmail" value="false" id="DoNotSendOptInConfirmationEmail">
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" name="DoNotSendOptInConfirmationEmail" value="true" id="DoNotSendOptInConfirmationEmail">
                            <?php endif; ?>
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="checkbox" name="TriggerBehaviors" value="true" id="TriggerBehaviors">
                                <label for="TriggerBehaviors"><?php InterfaceLanguage('Screen', '1328', false); ?></label>
                                <div class="form-group-note">
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '1469'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersimportfieldmapping.php'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?php include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_wizard_steps.php'); ?>
    </div>
</div>


