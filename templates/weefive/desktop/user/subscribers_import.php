<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>
<?php 
//echo $ActiveListItem;
?>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', '0967', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php
                                if ($SubSection == 'flowlist') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_flow_list.php');
                                } elseif ($SubSection == 'Field mapping') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_field_mapping.php');
                                } elseif ($SubSection == 'Results') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_results.php');
                                } elseif ($SubSection == 'Data entry') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_data_entry.php');
                                } elseif ($SubSection == 'Form') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_form.php');
                                } elseif ($SubSection == 'File upload') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_file_upload.php');
                                } elseif ($SubSection == 'Mysql information') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_mysql_information.php');
                                } elseif ($SubSection == 'Highrise contact selection') {
                                    include_once(TEMPLATE_PATH . 'desktop/user/subscribers_import_highrise.php');
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Page - END -->
<script>
    var form_url = '<?php InterfaceAppURL(); ?>/user/subscribers/add/<?php echo $ListInformation['ListID']; ?>';
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers_import.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>