<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>


<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-close font-purple-sharp"></i>
                                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '9195', false, '', false, false); ?></span>                    
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="page-bar">
                                    <h2><?php InterfaceLanguage('Screen', '0136', false, '', false, false); ?></h2>
                                </div>
                                <form id="list-delete-form" method="post" action="<?php InterfaceAppURL(); ?>/user/list/delete/<?php echo $ListInformation['ListID']; ?>">
                                    <input type="hidden" name="ListDeleteConfirm" value="true" id="ListDeleteConfirm">
                                    <span class="help-block" style="margin-bottom: 20px"><?php InterfaceLanguage('Screen', '1136', false, '', false); ?></span>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <a class="btn default" href="#" targetform="list-delete-form"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1137', false, '', true); ?></strong></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>