<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-docs font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0552', false, '', false, true); ?> </span>                    
                </div>
            </div>
            <div class="portlet-body form">
                <div class="module-container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($Message != '' && $Message == 'upload_success'): ?>
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php InterfaceLanguage('Screen', '1876'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($Message != '' && $Message == 'folder_create'): ?>                       
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php InterfaceLanguage('Screen', '1881'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($Message != '' && $Message == 'folder_delete'): ?>
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php InterfaceLanguage('Screen', '1882'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($Message != '' && $Message == 'upload_error'): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php InterfaceLanguage('Screen', '1848'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="line-height:24px;padding:0 18px;">
                                <?php for ($i = count($Breadcrumb) - 1; $i >= 0; $i--): ?>
                                    <?php if ($i == 0): ?>
                                        / <?php echo $Breadcrumb[$i][1]; ?>
                                    <?php else: ?>
                                        / <a href="#" class="folder" folderid="<?php echo $Breadcrumb[$i][0]; ?>"><?php echo $Breadcrumb[$i][1]; ?></a>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="upload-form-container" class="clearfix">
                    <form target="media-upload-frame" id="media-upload-form" name="media-upload-form" action="<?php InterfaceAppURL(); ?>/user/medialibrary/upload" method="post" enctype="multipart/form-data" style="margin:0;padding:0;">
                        <input type="hidden" name="ParentFolderID" value="<?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getId(); ?>">
                        <div class="col-md-3" style="padding-left: 0">
                            <a href="#" class="btn btn-link" id="create-folder-link"><?php InterfaceLanguage('Screen', '1877'); ?></a>
                            <?php if ($CurrentFolder != false): ?>
                                <a href="#" class="btn btn-link" id="delete-folder-link" ><?php InterfaceLanguage('Screen', '1878'); ?></a>
                            <?php endif; ?>
                        </div>
                        <span class="col-md-2 control-label" style="width:auto">Upload file: </span>
                        <div class="col-md-6">
                            <input type="file" name="new-media-file" value="" id="new-media-file">
                        </div>

                    </form>
                    <p id="loading-indicator" style="display:none;margin:18px 0 0 18px;"><?php InterfaceLanguage('Screen', '1058'); ?></p>
                    <iframe name="media-upload-frame" id="media-upload-frame" style="height:1px;width:1px;display:none"></iframe>
                </div>
                <div class="email-template-gallery-container clearfix">
                    <?php if ($CurrentFolder !== false): ?>
                        <div class="email-template small folder" folderid="<?php echo $CurrentFolder->getFolderId(); ?>">
                            <div class="image">
                                <img src="<?php InterfaceTemplateURL(); ?>images/folder_parent.png" width="100" />
                            </div>
                            <div class="meta">
                                &nbsp;
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($Folders !== false && count($Folders) > 0): ?>
                        <?php foreach ($Folders as $Each): ?>
                            <div class="email-template small folder" folderid="<?php echo $Each->getId(); ?>">
                                <div class="image">
                                    <img src="<?php InterfaceTemplateURL(); ?>images/folder.png" width="100" />
                                </div>
                                <div class="meta">
                                    <?php echo $Each->getName(); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php if ($Files !== false && count($Files) > 0): ?>
                        <?php foreach ($Files as $Each): ?>
                            <div class="email-template small media" mediaurl="<?php echo $Each->getUrl(InterfaceAppURL(true)); ?>" id="file-<?php echo $Each->getId(); ?>">
                                <div class="image">
                                    <img src="<?php echo $Each->isImage() ? $Each->getUrl(InterfaceAppURL(true)) : InterfaceTemplateURL(true) . '/images/file.png'; ?>" width="100" />
                                </div>
                                <div class="meta">
                                    <?php echo $Each->getName(); ?>
                                </div>
                                <div class="meta-action">
                                    <a href="<?php echo $Each->getUrl(InterfaceAppURL(true)); ?>" target="_blank"><?php InterfaceLanguage('Screen', '1054'); ?></a>
                                    &nbsp;&nbsp;
                                    <a href="#" class="file-delete" fileid="<?php echo $Each->getId(); ?>"><?php InterfaceLanguage('Screen', '0042'); ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_editemailtemplate.php'); ?>
    </div>
</div>


<div class="container">
    <div class="span-18">
        <div id="page-shadow">
            <div id="page">
                <div class="white" style="min-height:420px;">

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var current_folder = <?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getId(); ?>;
    var parent_folder = <?php echo $CurrentFolder == false ? 0 : $CurrentFolder->getFolderId(); ?>;
    var language_object = {
        '1055': '<?php InterfaceLanguage('Screen', '1055'); ?>',
        '1880': '<?php InterfaceLanguage('Screen', '1880'); ?>',
        '1883': '<?php InterfaceLanguage('Screen', '1883'); ?>'
    };

    var media_library_url = '<?php InterfaceAppURL(); ?>/user/medialibrary/browse/_folderid_/page/';
</script>

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/media_library.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>