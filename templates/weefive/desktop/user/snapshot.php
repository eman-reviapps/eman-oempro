<div style="float:left;margin-right:18px;width:110px;">
	<h3><?php InterfaceLanguage('Screen', '1432', false, '', false, true, array()); ?></h3>
	<div style="margin-bottom:6px;">
		<span class="data-label small"><?php InterfaceLanguage('Screen', '0104'); ?>:</span><br />
		&nbsp;<span class="data small"><?php echo number_format($TotalSubscribers); ?></span> 
		<?php if ($UserInformation['GroupInformation']['LimitSubscribers'] > 0): ?><span class="data-label small">/ <?php echo number_format($UserInformation['GroupInformation']['LimitSubscribers']); ?></span><?php endif; ?>
	</div>
	<div style="margin-bottom:6px;">
		<span class="data-label small"><?php InterfaceLanguage('Screen', '0541'); ?>:</span><br />
		&nbsp;<span class="data small"><?php echo $TotalLists; ?></span> 
		<?php if ($UserInformation['GroupInformation']['LimitLists'] > 0): ?><span class="data-label small">/ <?php echo number_format($UserInformation['GroupInformation']['LimitLists']); ?></span><?php endif; ?>
	</div>
</div>
<div style="float:left;width:110px;">
	<h3><?php InterfaceLanguage('Screen', '1433', false, '', false, true, array()); ?></h3>
	<div style="margin-bottom:6px;">
		<span class="data-label small"><?php InterfaceLanguage('Screen', '1434'); ?>:</span><br />
		&nbsp;<span class="data small"><?php echo $CampaignsSentInPeriod; ?></span> 
		<?php if ($UserInformation['GroupInformation']['LimitCampaignSendPerPeriod'] > 0): ?><span class="data-label small">/ <?php echo number_format($UserInformation['GroupInformation']['LimitCampaignSendPerPeriod']); ?></span><?php endif; ?>
	</div>
	<div style="margin-bottom:6px;">
		<span class="data-label small"><?php InterfaceLanguage('Screen', '1431'); ?>:</span><br />
		&nbsp;<span class="data small"><?php echo number_format($EmailsSentInPeriod); ?></span> 
		<?php if ($UserInformation['GroupInformation']['LimitEmailSendPerPeriod'] > 0): ?><span class="data-label small">/ <?php echo number_format($UserInformation['GroupInformation']['LimitEmailSendPerPeriod']); ?></span><?php endif; ?>
	</div>
</div>
<div style="clear:both">
	<h3 style="margin-top:9px;"><?php InterfaceLanguage('Screen', '1394', false, '', false, true, array()); ?></h3>
	<img src="<?php InterfaceAppURL(); ?>/user/snapshot/emails_sent" />
</div>