<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>
<?php 
//echo $ActiveListItem;
?>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-purple-sharp sbold">
                                        <?php InterfaceLanguage('Screen', '1108', false, '', false, true); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form">
                                            <form id="subscribers-remove" action="<?php InterfaceAppURL(); ?>/user/subscribers/remove/<?php echo $ListInformation['ListID']; ?>" method="post">
                                                <input type="hidden" name="Command" value="RemoveSubscribers" id="Command">
                                                <input type="hidden" name="ListID" value="<?php echo $ListInformation['ListID']; ?>" id="ListID">

                                                <?php
                                                if (isset($PageErrorMessage) == true):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php print($PageErrorMessage); ?>
                                                    </div>
                                                    <?php
                                                elseif (validation_errors()):
                                                    ?>
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                                    </div>
                                                <?php else: ?>
                                                <h4 class="form-section bold font-blue" style="margin-top: 10px !important"><?php InterfaceLanguage('Screen', '0037', false); ?></h4>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="form-group clearfix <?php print((form_error('Name') != '' ? 'error' : '')); ?>" id="form-row-Name">
                                                    <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '0940', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <input type="text"value="<?php echo $ListInformation['Name']; ?>" class="form-control" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix" id="form-row-Subscribers">
                                                    <label class="col-md-3 control-label" for="Subscribers"><?php InterfaceLanguage('Screen', '0104', false, '', false, true); ?>:</label>
                                                    <div class="col-md-4">
                                                        <select name="Subscribers" id="Subscribers" class="form-control">
                                                            <option value="Active" <?php echo set_select('Subscribers', 'Active'); ?>><?php InterfaceLanguage('Screen', '1109'); ?></option>
                                                            <option value="Suppressed" <?php echo set_select('Subscribers', 'Suppressed'); ?>><?php InterfaceLanguage('Screen', '1110'); ?></option>
                                                            <option value="Not opted in for days" <?php echo set_select('Subscribers', 'Not opted in for days'); ?>><?php InterfaceLanguage('Screen', '1114'); ?></option>
                                                            <option value="Copy and paste" <?php echo set_select('Subscribers', 'Copy and paste'); ?>><?php InterfaceLanguage('Screen', '1115'); ?></option>
                                                            <optgroup label="<?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?>">
                                                                <?php foreach ($Segments as $EachSegment): ?>
                                                                    <option value="<?php echo $EachSegment['SegmentID']; ?>" <?php echo set_select('Subscribers', $EachSegment['SegmentID']); ?>><?php echo $EachSegment['SegmentName']; ?></option>
                                                                <?php endforeach; ?>
                                                            </optgroup>
                                                            <optgroup label="<?php InterfaceLanguage('Screen', '1111', false, '', false, true); ?>">
                                                                <option value="Soft bounced" <?php echo set_select('Subscribers', 'Soft bounced'); ?>><?php InterfaceLanguage('Screen', '1112'); ?></option>
                                                                <option value="Hard bounced" <?php echo set_select('Subscribers', 'Hard bounced'); ?>><?php InterfaceLanguage('Screen', '1113'); ?></option>
                                                            </optgroup>
                                                        </select> 
                                                    </div>

                                                    <span id="opt-in-days" style="display:none">
                                                        <div class="col-md-1">
                                                            <input type="text" name="NotOptedInForDays" id="NotOptedInForDays" class="form-control" />                                     
                                                            <?php InterfaceLanguage('Screen', '0851'); ?>
                                                        </div>
                                                    </span>
                                                    <div class="col-md-4">
                                                        <div class="form-group-note" id="combo-description">
                                                            <p style="display:none" id="message-Active"><?php InterfaceLanguage('Screen', '1116', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Suppressed"><?php InterfaceLanguage('Screen', '1117', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Not-opted-in-for-days"><?php InterfaceLanguage('Screen', '1118', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Copy-and-paste"><?php InterfaceLanguage('Screen', '1119', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Soft-bounced"><?php InterfaceLanguage('Screen', '1120', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Hard-bounced"><?php InterfaceLanguage('Screen', '1121', false, '', false, false); ?></p>
                                                            <p style="display:none" id="message-Segments"><?php InterfaceLanguage('Screen', '1122', false, '', false, false); ?></p>
                                                        </div>
                                                    </div>
                                                    <span id="copy-and-paste" style="display:none;" >
                                                        <div class="col-md-3">

                                                        </div>
                                                        <div class="col-md-6" style="padding-left: 0">
                                                            <textarea name="CopyAndPaste" id="CopyAndPaste" rows="5" class="form-control" ></textarea>
                                                        </div>
                                                    </span>

                                                </div>
                                                <div class="form-group" id="form-row-AddToSuppresionList">
                                                    <label>&nbsp;</label>
                                                    <div class="checkbox-container">
                                                        <div class="checkbox-row" style="margin-left:0px;">
                                                            <input type="checkbox" name="AddToSuppresionList" value="true" id="AddToSuppresionList" <?php echo set_checkbox('AddToSuppresionList', 'true'); ?>>
                                                            <label for="AddToSuppresionList"><?php InterfaceLanguage('Screen', '1123', false, '', false, false); ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-note checkbox-note">
                                                        <p><?php InterfaceLanguage('Screen', '1124', false, '', false, false); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <a class="btn default" targetform="subscribers-remove" id="form-button" href="#"><strong><?php InterfaceLanguage('Screen', '1108', false, '', true); ?></strong></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_subscribersremove.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Page - END -->

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers_remove.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>