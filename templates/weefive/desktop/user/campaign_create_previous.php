<form id="campaign-create" action="<?php InterfaceAppURL(); ?>/user/campaigns/create/<?php print($Flow); ?>" method="post">
	<input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
	<input type="hidden" name="FormStep" value="0" id="FormStep">
	<div class="container">

		<div class="span-18 last">
			<div id="page-shadow">
				<div id="page">
					<div class="page-bar">
						<h2><?php InterfaceLanguage('Screen', '0754', false, '', true, false, array($CurrentStep+1)); ?> / <?php print($Steps[$CurrentStep]['name']); ?></h2>
					</div>
					<div class="white">
						<?php
						if (isset($PageErrorMessage) == true):
						?>
							<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
						<?php
						elseif (validation_errors()):
						?>
						<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php
						endif;
						?>

						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0796', false, '', false, true); ?></h3>
						<div class="form-row no-bg">
							<p>Select one of your previously created campaigns below</p>
						</div>
						<div class="form-row <?php print((form_error('PreviousCampaign') != '' ? 'error' : '')); ?>" id="form-row-CampaignName">
							<select name="PreviousCampaign" id="PreviousCampaign">
								<?php foreach ($Campaigns as $Each): ?>
									<option value="<?php echo $Each['CampaignID']; ?>"><?php echo $Each['CampaignName']; ?></option>
								<?php endforeach ?>
							</select>
							<?php print(form_error('PreviousCampaign', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_campaigncreateprevious.php'); ?>
		</div>

		<div class="span-18 last">
			<div class="button-steps">
				<a class="button control-next" targetform="campaign-create" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0761', false, '', true); ?> &rarr;</strong></a>
				<div class="steps-container">
					<?php for ($i=0; $i<count($Steps); $i++): ?>
						<a class="step-button <?php if ($i > $LastStep): ?>disabled<?php else: ?><?php if ($CurrentStep == $i): ?>current-step<?php else: ?>previous-step<?php endif; ?><?php endif; ?>" href="<?php InterfaceAppURL(); ?>/user/campaigns/create/<?php print($Flow); ?>/<?php print($i+1); ?>"><?php print($i+1); ?> / <?php print($Steps[$i]['name']); ?></a>
					<?php endfor; ?>
				</div>
			</div>
		</div>

	</div>
</form>