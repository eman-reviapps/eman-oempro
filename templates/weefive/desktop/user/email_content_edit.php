<input type="hidden" name="EmailID" value="<?php echo $EmailInformation['EmailID']; ?>" id="EmailID">
<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0759', false, '', false, true); ?></h3>
<div class="form-row no-bg">
	<p><?php InterfaceLanguage('Screen', '0753', false, '', false, false); ?></p>
</div>
<div class="form-row <?php print((form_error('FromName') != '' ? 'error' : '')); ?>" id="form-row-FromName">
	<label for="FromName"><?php InterfaceLanguage('Screen', '0757', false, '', false, true); ?>: *</label>
	<input type="text" name="FromName" value="<?php echo set_value('FromName', $EmailInformation['FromName']); ?>" id="FromName" class="text" />
	<?php print(form_error('FromName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
<div class="form-row <?php print((form_error('FromEmail') != '' ? 'error' : '')); ?>" id="form-row-FromEmail">
	<label for="FromEmail"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
	<input type="text" name="FromEmail" value="<?php echo set_value('FromEmail', $EmailInformation['FromEmail']); ?>" id="FromEmail" class="text" />
	<?php print(form_error('FromEmail', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
<h3 class="form-legend"><?php InterfaceLanguage('Screen', '0760', false, '', false, true); ?></h3>
<div class="form-row no-bg">
	<p><?php InterfaceLanguage('Screen', '0764', false, '', false, false); ?></p>
</div>
<div class="form-row" id="form-row-SameNameEmail">
	<div class="checkbox-container">
		<div class="checkbox-row no-margin">
			<input type="checkbox" name="SameNameEmail" value="true" id="SameNameEmail" <?php echo set_checkbox('SameNameEmail', 'true', $UseSameNameAndEmail); ?>>
			<label for="SameNameEmail"><?php InterfaceLanguage('Screen', '0769', false, '', false, false); ?></label>
		</div>
	</div>
</div>
<div id="replyto-name-email-container" <?php if (isset($_POST['SameNameEmail'])): ?>style="display:none" <?php endif; ?>>
	<div class="form-row <?php print((form_error('ReplyToName') != '' ? 'error' : '')); ?>" id="form-row-ReplyToName">
		<label for="ReplyToName"><?php InterfaceLanguage('Screen', '0757', false, '', false, true); ?>: *</label>
		<input type="text" name="ReplyToName" value="<?php echo set_value('ReplyToName', $EmailInformation['ReplyToName']); ?>" id="ReplyToName" class="text" />
		<?php print(form_error('ReplyToName', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('ReplyToEmail') != '' ? 'error' : '')); ?>" id="form-row-ReplyToEmail">
		<label for="ReplyToEmail"><?php InterfaceLanguage('Screen', '0758', false, '', false, true); ?>: *</label>
		<input type="text" name="ReplyToEmail" value="<?php echo set_value('ReplyToEmail', $EmailInformation['ReplyToEmail']); ?>" id="ReplyToEmail" class="text" />
		<?php print(form_error('ReplyToEmail', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
</div>

<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1027', false); ?></h3>

<div class="form-row" id="form-row-ImageEmbedding">
	<div class="checkbox-container">
		<div class="checkbox-row" style="margin-left:0px;">
			<input type="checkbox" name="ImageEmbedding" value="Enabled" id="ImageEmbedding" <?php echo set_checkbox('ImageEmbedding', 'Enabled', $EmailInformation['ImageEmbedding'] == 'Enabled'); ?>>
			<label for="ImageEmbedding"><?php InterfaceLanguage('Screen', '0782', false, '', false, false); ?></label>
		</div>
	</div>
	<div class="form-row-note" style="margin-left:0px;">
		<p><?php InterfaceLanguage('Screen', '0783', false, '', false, false); ?></p>
	</div>
</div>
<div class="form-row" id="form-row-ContentType">
	<label for="ContentType"><?php InterfaceLanguage('Screen', '0785', false, '', false, true); ?>: *</label>
	<select name="ContentType" id="ContentType" class="select">
		<option value="HTML" <?php echo set_select('ContentType', 'HTML', $EmailInformation['ContentType'] == 'HTML'); ?>><?php InterfaceLanguage('Screen', '0786', false, '', false, false); ?></option>
		<option value="Plain" <?php echo set_select('ContentType', 'Plain', $EmailInformation['ContentType'] == 'Plain'); ?>><?php InterfaceLanguage('Screen', '0787', false, '', false, false); ?></option>
		<option value="Both" <?php echo set_select('ContentType', 'Both', $EmailInformation['ContentType'] == 'Both'); ?>><?php InterfaceLanguage('Screen', '0788', false, '', false, false); ?></option>
	</select>
</div>
<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1028', false, '', false, true); ?></h3>
<div class="form-row <?php print((form_error('Subject') != '' ? 'error' : '')); ?>" id="form-row-Subject">
	<label for="Subject"><?php InterfaceLanguage('Screen', '0106', false, '', false, true); ?>: *</label>
	<input type="text" name="Subject" value="<?php echo set_value('Subject', $EmailInformation['Subject']); ?>" id="Subject" class="text personalized" style="width:500px;" />
	<div class="form-row-note personalization" id="personalization-Subject">
		<p>
			<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
			<select name="personalization-select-Subject" id="personalization-select-Subject" class="personalization-select">
				<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
				<?php foreach($SubjectTags as $Label => $TagGroup): ?>
					<optgroup label="<?php print($Label); ?>">
						<?php foreach($TagGroup as $Tag => $Label): ?>
							<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
						<?php endforeach; ?>
					</optgroup>
				<?php endforeach; ?>
			</select>&nbsp;&nbsp;
		</p>
	</div>
	<?php print(form_error('Subject', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
<?php if ($EmailInformation['Mode'] == 'Empty'): ?>
	<div class="form-row <?php print((form_error('HTMLContent') != '' ? 'error' : '')); ?>" id="form-row-HTMLContent">
		<label for="HTMLContent"><?php InterfaceLanguage('Screen', '0175', false, '', false, true); ?>: *</label>
		<textarea name="HTMLContent" id="HTMLContent" class="textarea personalized" style="height:300px;width:500px;"><?php echo set_value('HTMLContent', $EmailInformation['HTMLContent']); ?></textarea>
		<div class="form-row-note personalization" id="personalization-HTMLContent">
			<p>
				<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
				<select name="personalization-select-HTMLContent" id="personalization-select-HTMLContent" class="personalization-select">
					<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
					<?php foreach($ContentTags as $Label => $TagGroup): ?>
						<optgroup label="<?php print($Label); ?>">
							<?php foreach($TagGroup as $Tag => $Label): ?>
								<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
							<?php endforeach; ?>
						</optgroup>
					<?php endforeach; ?>
				</select>&nbsp;&nbsp;
			</p>
		</div>
	<?php print(form_error('HTMLContent', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
<?php elseif ($EmailInformation['Mode'] == 'Template'): ?>
<div class="form-row clearfix" id="form-row-HTMLContent">
	<label for="HTMLContent"><?php InterfaceLanguage('Screen', '0175', false, '', false, true); ?>:</label>
	<?php if ($EmailEditMode == 'Confirmation'): ?>
		<a class="button" href="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/edit/confirmation/<?php echo base64_encode(urlencode($_SERVER['REQUEST_URI'])); ?>/<?php echo $ListInformation['ListID']; ?>/<?php echo $EmailInformation['EmailID']; ?>" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0799', false, '', true, false); ?></strong></a>
	<?php elseif ($EmailEditMode == 'AutoResponder'): ?>
		<a class="button" href="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/edit/autoresponder/<?php echo base64_encode(urlencode($_SERVER['REQUEST_URI'])); ?>/<?php echo $AutoResponder['AutoResponderID']; ?>/<?php echo $EmailInformation['EmailID']; ?>" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0799', false, '', true, false); ?></strong></a>
	<?php else: ?>
		<a class="button" href="<?php InterfaceAppURL() ?>/user/emailcontentbuilder/index/<?php echo $EmailInformation['CampaignID']; ?>" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0799', false, '', true, false); ?></strong></a>
	<?php endif; ?>
	<div class="form-row-note" style="clear:both">
		<p><?php InterfaceLanguage('Screen', '0800', false, '', false, false); ?></p>
	</div>
	<textarea name="HTMLContent" id="HTMLContent" class="" style="display:none"><?php echo $EmailInformation['HTMLContent']; ?></textarea>
</div>
<?php else: ?>
	<div class="form-row <?php print((form_error('FetchURL') != '' ? 'error' : '')); ?>" id="form-row-FetchURL">
		<label for="FetchURL"><?php InterfaceLanguage('Screen', '0797', false, '', false, true); ?>: *</label>
		<input type="text" name="FetchURL" value="<?php echo set_value('FetchURL', $EmailInformation['FetchURL']); ?>" id="FetchURL" class="text" />
		<?php print(form_error('FetchURL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
	<div class="form-row <?php print((form_error('FetchPlainURL') != '' ? 'error' : '')); ?>" id="form-row-FetchPlainURL">
		<label for="FetchPlainURL"><?php InterfaceLanguage('Screen', '0798', false, '', false, true); ?>: *</label>
		<input type="text" name="FetchPlainURL" value="<?php echo set_value('FetchPlainURL', $EmailInformation['FetchPlainURL']); ?>" id="FetchPlainURL" class="text" />
		<?php print(form_error('FetchPlainURL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
	</div>
<?php endif; ?>

<?php if ($EmailInformation['Mode'] != 'Import'): ?>
<div class="form-row <?php print((form_error('PlainContent') != '' ? 'error' : '')); ?>" id="form-row-PlainContent">
	<label for="PlainContent"><?php InterfaceLanguage('Screen', '0176', false, '', false, true); ?>: *</label>
	<textarea name="PlainContent" id="PlainContent" class="textarea personalized plain-text" style="height:300px;width:500px;"><?php echo set_value('PlainContent', $EmailInformation['PlainContent']); ?></textarea>
	<div class="form-row-note personalization" id="personalization-PlainContent">
		<p>
			<strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
			<select name="personalization-select-PlainContent" id="personalization-select-PlainContent" class="personalization-select">
				<option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
				<?php foreach($ContentTags as $Label => $TagGroup): ?>
					<optgroup label="<?php print($Label); ?>">
						<?php foreach($TagGroup as $Tag => $Label): ?>
							<option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
						<?php endforeach; ?>
					</optgroup>
				<?php endforeach; ?>
			</select>&nbsp;&nbsp;
		</p>
	</div>
	<?php print(form_error('PlainContent', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
<?php endif; ?>

<h3 style="display: none" class="form-legend"><?php InterfaceLanguage('Screen', '0789', false, '', false, true); ?></h3>
<form style="display: none"  target="file-upload-frame" enctype="multipart/form-data" action="<?php InterfaceAppURL(); ?>/user/attachments/upload/" method="POST" name="file-upload-form" id="file-upload-form">
	<input type="hidden" name="RelEmailID" value="<?php echo $EmailInformation['EmailID']; ?>" id="RelEmailID">
	<div class="form-row">
		<input type="file" name="AttachmentFile" id="AttachmentFile" /> <span id="uploading-indicator" style="display:none"><img id="search-loading-indicator" src="<?php InterfaceTemplateURL(); ?>/images/icon_load.gif" width="16" height="16" align="absmiddle" /> Uploading attachment...</span>
		<div id="attachment-container" class="form-row-note" style="margin-left:0px;margin-top:9px;">
			<p id="attachment-template" style="display:none"><input type="checkbox" name="attachment-checkbox[]" value="_AttachmentID_" class="attachment-checkbox" id="attachment-checkbox-_AttachmentID_" checked="checked" /> <a href="<?php InterfaceAppURL(); ?>/user/attachments/view/_AttachmentMD5ID_" target="_blank">_FileName_</a> <span class="data small" style="margin-left:18px;">_FileSize_</span></p>
		</div>
	</div>
</form>
<iframe name="file-upload-frame" id="file-upload-frame" style="height:1px;width:1px;display:none"></iframe>

<script type="text/javascript" charset="utf-8">
	var	tinymce_config = {
		// Location of tinymce script
		script_url		: '<?php print(InterfaceTemplateURL()); ?>js/tiny_mce_3432/tiny_mce.js',

		// General options
		theme						: 'advanced',
		plugins						: 'fullpage,safari,table,style,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,personalizer',
		auto_reset_designmode		: true,
		browsers					: 'msie,gecko,safari,opera',
		dialog_type					: 'modal',
		directionality				: "ltr",
		docs_language				: "en",
		language					: "en",
		nowrap						: false,
		apply_source_formatting		: true,
		entity_encoding				: 'named',
		cleanup						: true,
		cleanup_on_startup			: true,
		verify_html					: false,
		forced_root_block			: '',
		convert_newlines_to_brs		: false,
		element_format				: 'xhtml',
		fix_list_elements			: true,
		fix_table_elements			: true,
		fix_nesting					: true,
		force_p_newlines			: true,
		force_br_newlines			: false,
		force_hex_style_colors		: true,
		preformatted				: false,
		valid_child_elements		: 'table[tr|td]',
		paste_auto_cleanup_on_paste	: true,
		relative_urls				: false, 
		convert_urls				: false,

		// Theme options
		theme_advanced_buttons1				: "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor",
		theme_advanced_buttons2				: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,cleanup,fullscreen,|,code",
		theme_advanced_buttons3				: "personalizer,|,table,row_after,row_before,col_after,col_before,delete_col,delete_row,delete_table,|,link,unlink,image",
		theme_advanced_toolbar_location 	: "top",
		theme_advanced_toolbar_align		: "left",
		theme_advanced_statusbar_location	: "bottom",
		theme_advanced_resizing				: true,
		theme_advanced_resize_horizontal	: false,

		// Personalizer tags
		personalizer_tags					: [
		<?php foreach($ContentTags as $Label => $TagGroup): ?>
			{ label : true, title : '<?php print($Label); ?>' },
			<?php foreach($TagGroup as $Tag => $Label): ?>
				{ title : '<?php print($Label); ?>', tags : '<?php print($Tag); ?>' },
			<?php endforeach; ?>
		<?php endforeach; ?>
		]
	};

var wysiwyg_enabled = <?php print(WYSIWYG_ENABLED); ?>;
var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
var template_url = '<?php InterfaceInstallationURL(); ?>api.php';
var EmailID = <?php echo $EmailInformation['EmailID']; ?>;
var screen_type = 'edit';
var cf = '';
var attachments = [
	<?php if ($Attachments != false) { for ($i=0;$i < count($Attachments); $i++) { ?>
		{ AttachmentID: '<?php print($Attachments[$i]['AttachmentID']); ?>', AttachmentMD5ID: '<?php print(md5($Attachments[$i]['AttachmentID'])); ?>', FileName: '<?php print(htmlspecialchars($Attachments[$i]['FileName'], ENT_QUOTES)); ?>', FileMimeType: '<?php print($Attachments[$i]['FileMimeType']); ?>', FileSize: '<?php print($Attachments[$i]['FileSize']); ?>' }<?php print($i < count($Attachments)-1 ? ',' : ''); ?>
	<?php }} ?>
];

$(document).ready(function () {
	content_edit_library.init_handlers();
});
</script>

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/email_content_edit.js" type="text/javascript" charset="utf-8"></script>		
<script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce_3432/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
