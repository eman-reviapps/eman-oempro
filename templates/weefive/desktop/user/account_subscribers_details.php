<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />

<!--light bg-inverse-->
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '9273', false, '', false, true); ?></span>                    
        </div>
    </div>
</div>

<div class="page-content-inner">
    <div class="note note-info note-birdie">
        <span class="label label-success"><?php echo $TotalSubscribersOnTheAccount ?></span>
        <span class="bold"><?php InterfaceLanguage('Screen', '9274', false); ?> </span> <?php InterfaceLanguage('Screen', '9275', false); ?> 

        <?php
        if ($UserInformation['IsAdmin'] == 'No') {
            ?>
            <span class="bold"> <?php InterfaceLanguage('Screen', '0864', false); ?> </span> <?php echo $Limit ?>
            <?php
        }
        ?>
        . 
    </div>
    <?php
    if ($UserInformation['IsAdmin'] == 'No') {
        ?>
        <div class="status">
            <div class="status-number bold" style="width: <?php echo $Ratio ?>%"> <?php echo $Ratio ?>%</div>
        </div>
        <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $Ratio ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $Ratio ?>%">
                <span class="sr-only"> <?php echo $Ratio ?>% Complete (success) </span>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject font-purple-sharp sbold">Details</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover">
                            <thead>

                                <tr>
                                    <th> List </th>
                                    <th> Total Subscribers </th>
                                    <th> Unique Subscribers </th>
                                    <th> Unique Active Subscribers </th>
                                    <th> Unique Suppressed Subscribers </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($Details as $Row) {
                                    ?>
                                    <tr>
                                        <td>
                                            <a href="" class="primary-link"><?php echo $Row['ListInformation']['Name'] ?></a>
                                        <td>
                                            <span><?php echo $Row['ListTotalCount'] ?></span>
                                        </td>
                                        <td> 
                                            <span><?php echo $Row['ListUniqueCount'] ?> </span>
                                        </td>
                                        <td>
                                            <span><?php echo $Row['ListUniqueActiveCount'] ?> </span>
                                        </td>
                                        <td> 
                                            <span><?php echo $Row['ListUniqueSuppressedCount'] ?> </span>
                                        </td>
                                        <td>
                                            <span><?php echo round((($Row['ListUniqueCount'] / $TotalSubscribersOnTheAccount) * 100), 2) ?>%</span>
                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th colspan="4"> <?php echo $TotalSubscribersOnTheAccount ?> Subscriber on the account </th>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>