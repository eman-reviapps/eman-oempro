<script>
    var ChartsAmcharts = function () {


        var initChartSample7 = function () {
            var data = [];
            var series = Math.floor(Math.random() * 10) + 1;
            series = series < 5 ? 5 : series;
            data = [{
                    "label": "<?php InterfaceLanguage('Screen', '0885'); ?>" + " : " + <?php echo $NotOpenedRatio; ?> + "%",
                    "data": <?php echo $NotOpenedRatio; ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '0875') ?>" + " : " + <?php echo $OpenedRatio; ?> + "%",
                    "data": <?php echo $OpenedRatio; ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '0876') ?>" + " : " + <?php echo $ClickedRatio; ?> + "%",
                    "data": <?php echo $ClickedRatio; ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '0841') ?>" + " : " + <?php echo $UnsubscriptionRatio; ?> + "%",
                    "data": <?php echo $UnsubscriptionRatio ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '0961'); ?>" + " : " + <?php echo number_format((100 * $CampaignInformation['TotalSpamReports']) / $CampaignInformation['TotalSent']); ?> + "%",
                    "data": <?php echo number_format((100 * $CampaignInformation['TotalSpamReports']) / $CampaignInformation['TotalSent']); ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '0098'); ?>" + " : " + <?php echo $BounceRatio; ?> + "%",
                    "data": <?php echo $BounceRatio ?>
                }, {
                    "label": "<?php InterfaceLanguage('Screen', '1113'); ?>" + " : " + <?php echo $HardBouncedRatio; ?> + "%",
                    "data": <?php echo $HardBouncedRatio ?>
                }];
            $.plot($("#donut"), data, {
                title: {text: 'Legend Location'},
                series: {
                    pie: {
                        innerRadius: 0.6,
                        radius: 1,
                        show: true,
                        gradient: {
                            radial: true,
                            colors: [
                                {opacity: 0.5},
                                {opacity: 1.0}
                            ]
                        }
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                legend: {
                    show: true,
                },
                    colors: ["#A52D69", "#2AB4C0", "#D91E18", "#8E44AD", "#FF8040", "#FF00FF"]
            });
//            var chart = AmCharts.makeChart("chart_7", {
//                grid: {
//                    hoverable: true,
//                    clickable: true
//                },
//                "legend": {
//                    verticalAlign: "center",
//                    horizontalAlign: "right"
//                },
//                "type": "pie",
//                "theme": "light",
//                "showInLegend": true,
//                "fontFamily": 'Open Sans',
//                "color": '#888',
//                "dataProvider": [{
//                        "title": "<?php InterfaceLanguage('Screen', '0885'); ?>",
//                        "value": <?php echo 50; //$NotOpenedRatio;                    ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0875') ?>",
//                        "value": <?php echo 100; //$OpenedRatio;                    ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0876') ?>",
//                        "value": <?php echo 20; //$ClickedRatio;                  ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0841') ?>",
//                        "value": <?php echo $UnsubscriptionRatio ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0961'); ?>",
//                        "value": <?php echo number_format((100 * $CampaignInformation['TotalSpamReports']) / $CampaignInformation['TotalSent']); ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '0098'); ?>",
//                        "value": <?php echo $BounceRatio ?>
//                    }, {
//                        "title": "<?php InterfaceLanguage('Screen', '1113'); ?>",
//                        "value": <?php echo $HardBouncedRatio ?>
//                    }],
//                "valueField": "value",
//                "titleField": "title",
//                "outlineAlpha": 0.4,
//                "depth3D": 0,
//                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
//                "angle": 0,
//                "exportConfig": {
//                    menuItems: [{
//                            icon: '/lib/3/images/export.png',
//                            format: 'png'
//                        }]
//                }
//            });
//
//            jQuery('.chart_7_chart_input').off().on('input change', function () {
//                var property = jQuery(this).data('property');
//                var target = chart;
//                var value = Number(this.value);
//                chart.startDuration = 0;
//
//                if (property == 'innerRadius') {
//                    value += "%";
//                }
//
//                target[property] = value;
//                chart.validateNow();
//            });
//
//            $('#chart_7').closest('.portlet').find('.fullscreen').click(function () {
//                chart.invalidateSize();
//            });
        }

        return {
            //main function to initiate the module

            init: function () {

                initChartSample7();
            }

        };
    }();
    jQuery(document).ready(function () {
        ChartsAmcharts.init();
    });
</script>

