<div>
    <div class="col-md-8" style="padding: 0;" >
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="campaign-create" action="<?php echo $FormURL; ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="EmailTemplateID" value="" id="EmailTemplateID">
                    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">
                    <div class="email-template-gallery-container clearfix">
                        <?php foreach ($Templates as $Each): ?>
                            <div class="email-template <?php
                            if (isset($FieldDefaultValues['RelTemplateID']) && $FieldDefaultValues['RelTemplateID'] == $Each['TemplateID']) {
                                print 'selected';
                            }
                            ?>" id="email-template-<?php echo $Each['TemplateID']; ?>">
                                 <?php
                                 $img_url = ($Each['ScreenshotImage'] != '') ? APP_URL . "data/email_templates/" . $Each['ScreenshotImage'] : TEMPLATE_URL . "images/no_thumbnail.png";
                                 ?>
                                <div class="image">
                                    <img style="width: 135px;height: 160px" class="img-responsive"src="<?php echo $img_url ?>" />
                                </div>
                                <div class="meta">
                                    <?php echo $Each['TemplateName']; ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreategallery.php'); ?>
    </div>
</div>
<div class="col-md-8" style="padding: 0;" >
    <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
</div>

<script>
    $(document).ready(function () {
        template_library.init_handlers();
    });
</script>