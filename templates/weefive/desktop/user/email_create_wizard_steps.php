<div class="col-md-10" style="padding: 0">
    <nav>
        <ol class="cd-breadcrumb triangle">
            <?php for ($i = 0; $i < $CurrentFlow->get_step_count(); $i++): ?>

                <?php
                $class = "";
                if ($Wizard->get_current_step() == ($i + 1)) {
                    ?>
                    <li class="current">
                        <em><?php echo $i + 1; ?> / <?php echo $CurrentFlow->get_step($i)->get_title(); ?></em>
                    </li>                           
                    <?php
                } else {
                    if ($Wizard->get_last_step() < $i + 1) {
                        $class = 'disabled';
                    } else {
                        $class = 'previous-step';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $class ?>" href="<?php echo $FormURL; ?>/<?php echo $i + 1; ?>"><?php echo $i + 1; ?> / <?php echo $CurrentFlow->get_step($i)->get_title(); ?></a>
                    </li>
                    <?php
                }
                ?>
            <?php endfor; ?>
        </ol>
    </nav>
</div>
<div class="col-md-2" style="padding: 0;text-align: right">
    <ol class="cd-breadcrumb triangle cd-breadcrumb-custom" >
        <li class="current">
            <a targetform="campaign-create" href="#">
                <em>
                    <?php
                    if ($EditEvent) {
                        echo InterfaceLanguage('Screen', '0304', true, '') . ' &amp; ';
                    } InterfaceLanguage('Screen', '0761', false, '');
                    ?>
                </em>
            </a> 
        </li>
    </ol>

</div>
