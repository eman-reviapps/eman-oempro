<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/home-flot-chart.js" type="text/javascript"></script>-->

<?php include_once(TEMPLATE_PATH . 'desktop/user/home-flot-chart.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/user/home-geo-location.php'); ?>

<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.css" rel="stylesheet">
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {

        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    if (window.console) {
                        return console.log('onShown event fired');
                    }
                },
                onContentLoaded: function () {
                    if (window.console) {
                        return console.log('onContentLoaded event fired');
                    }
                },
                onNavigate: function (direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });

    });
</script>
<div class="page-content-inner">
    <div class="row widget-row">
        <!--        <div class="col-md-3">
                     BEGIN WIDGET THUMB 
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">
                            <img class="center-block" style="width:155px;height: 155px" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/page-builder.png" />
                        </h4>
                        <div class="widget-thumb-wrap">
                            <a href="" class="btn default btn-lg btn-block dropdown-toggle btn-custom"> Land page Builder <i class="fa fa-caret-down  fa-custom"></i></a>                   
                        </div>
                    </div>
                     END WIDGET THUMB 
                </div>-->
        <?php if (InterfacePrivilegeCheck('List.Create', $UserInformation)): ?>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">
                        <img class="center-block" style="height: 115px;width: 115px;" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/1-01.png" />
                    </h4>
                    <div class="widget-thumb-wrap">
                        <a id="button-overview-create-list" href="<?php InterfaceAppURL(); ?>/user/list/create" class="btn default btn-lg btn-block dropdown-toggle btn-custom <?php echo $disabled_class ?>"> <?php InterfaceLanguage('Screen', '9198', false, '', false); ?> </a>                   
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        <?php endif; ?>
        <?php if (InterfacePrivilegeCheck('Campaign.Create', $UserInformation)): ?>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">
                        <img class="center-block" style="height: 115px;width: 115px;" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/2-01.png" />
                    </h4>
                    <div class="widget-thumb-wrap">
                        <a id="button-overview-create-campaign" href="<?php InterfaceAppURL(); ?>/user/campaign/create/" class="btn default btn-lg btn-block dropdown-toggle btn-custom <?php echo $disabled_class ?>"> <?php InterfaceLanguage('Screen', '9199', false, '', false); ?> </a>                   
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        <?php endif; ?>
        <?php
//        InterfacePluginMenuHook('User.Overview.QuickAccess', 'NULL', ' <div class="col-md-3">
//            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
//                <h4 class="widget-thumb-heading">
//                    <img class="center-block" style="height: 165px" src="http://flyinglist.com/oempa/templates/weefive/assets/layouts/layout3/img/new_landing.png" />
//                </h4>
//                <div class="widget-thumb-wrap">
//                    <a href="_LINK_" class="btn default btn-lg btn-block dropdown-toggle btn-custom"> _TITLE_ </a>                   
//                </div>
//            </div>
//        </div>', ''
//        );
        ?>	

        <div class="col-md-3">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">
                    <img class="center-block" style="height: 115px;width: 115px;" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/3-01.png" />
                </h4>
                <div class="widget-thumb-wrap">
                    <a href="<?php InterfaceAppURL(); ?>/octlandingpage/templates" class="btn default btn-lg btn-block dropdown-toggle btn-custom <?php echo $disabled_class ?>"><?php InterfaceLanguage('Screen', '9211', false, '', false); ?></a>                   
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white margin-bottom-20 " style="padding-bottom: 15px">
                <h4 class="widget-thumb-heading">
                    <img class="center-block" style="height: 115px;width: 115px;" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/4-01.png" />
                </h4>
                <div class="btn-group" style="width:100%;">
                    <ul class="nav nav-tabs nav-custom">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <?php InterfaceLanguage('Screen', '9197', false, '', false); ?>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu" style="width:120%">
                                <li>
                                    <a href="#tab_automation" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9227', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a href="#tab_auto_responder2" id="auto_responder" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9228', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_landing_page" tabindex="-1" class="tab_help" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9229', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_subscription_behv" tabindex="-1" class="tab_help" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9230', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_segmentation" tabindex="-1" class="tab_help" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9231', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_3rd_party" tabindex="-1" class="tab_help" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9232', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_smart_subscription" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9233', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_ab_split_test" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9234', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_photo_editor" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9235', false, '', false); ?> </a>
                                </li>
                                <li>
                                    <a  href="#tab_api_keys" class="tab_help" tabindex="-1" data-toggle="tab" > <?php InterfaceLanguage('Screen', '9236', false, '', false); ?> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="tab-content tab-content-custom">
                <div class="tab-pane fade" id="tab_automation">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_automation.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_auto_responder2">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_autoresponder.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_landing_page">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_landingpage.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_subscription_behv">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_subscription_behv.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_segmentation">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_segmentation.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_3rd_party">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_3rd_party.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_smart_subscription">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_smart_subscription.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_ab_split_test">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_ab_split_test.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_photo_editor">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_photo_editor.php'); ?>
                </div>
                <div class="tab-pane fade" id="tab_api_keys">
                    <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_overview_api_keys.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="home_data">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                <!-- BEGIN STACK CHART CONTROLS PORTLET-->
                <div class="portlet light portlet-fit ">
                    <div class="portlet-title" style="margin-bottom: 0">
                        <div class="caption">
                            <i class=" icon-layers font-purple-sharp"></i>
                            <span class="caption-subject font-purple-sharp bold "><?php InterfaceLanguage('Screen', '1373', false, '', false, true) ?></span>
                        </div>
                    </div>
                    <div class="portlet-body" style="padding: 0">
                        <div class="portlet light portlet-fit ">
                            <div class="portlet-title" style="border-bottom: none">
                                <div class="caption" style="padding: 0">
                                    <div class="form-group" style="margin-bottom: 0">
                                        <select class="form-control input-small" id="MonthsSelect">
                                            <option value="3"><?php InterfaceLanguage('Screen', '9215') ?></option>
                                            <option value="6" selected><?php InterfaceLanguage('Screen', '9216') ?></option>
                                            <option value="12"><?php InterfaceLanguage('Screen', '9217') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="actions" style="padding: 0">
                                    <div class="form-group" style="margin-bottom: 0">
                                        <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxOpens" value="<?php InterfaceLanguage('Screen', '0837') ?>"> <?php InterfaceLanguage('Screen', '0837') ?>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxClicks" value="<?php InterfaceLanguage('Screen', '0838') ?>"> <?php InterfaceLanguage('Screen', '0838') ?>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                                <input type="checkbox" checked name="CheckboxGraph" id="CheckboxSubscriptions" value="<?php InterfaceLanguage('Screen', '9214') ?>"> <?php InterfaceLanguage('Screen', '9214') ?>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body" style="padding-right: 0;padding-top: 0">
                                <div id="chart_5" style="height:350px;"> </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END STACK CHART CONTROLS PORTLET-->
            </div>
            <div class="col-md-6 col-sm-6">
                <!-- BEGIN REGIONAL STATS PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-purple-sharp"></i>
                            <span class="caption-subject font-purple-sharp bold"><?php InterfaceLanguage('Screen', '9220') ?></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="region_statistics_loading">
                            <img src="<?php InterfaceTemplateURL(); ?>assets/global/img/loading.gif" alt="loading" /> 
                        </div>
                        <div id="region_statistics_content" class="display-none">
                            <div class="btn-toolbar margin-bottom-10">
                                <div class="btn-group btn-group-circle" data-toggle="buttons">
                                    <a href="" id="people_geo_home" class="btn grey-salsa btn-sm active"> <?php InterfaceLanguage('Screen', '9222') ?> </a>
                                    <!--<a href="" id="open_geo_home" class="btn grey-salsa btn-sm"> <?php InterfaceLanguage('Screen', '1402') ?> </a>-->
                                    <!--<a href="" id="click_geo_home" class="btn grey-salsa btn-sm"> <?php InterfaceLanguage('Screen', '0838') ?> </a>-->
                                </div>
                                <div class="btn-group pull-right">
                                    <a href="" class="btn btn-circle grey-salsa btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                        <span id="option_selected" >All</span>
                                        <span class="fa fa-angle-down"> </span>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="javascript:;" id="view_geo_all"> <?php InterfaceLanguage('Screen', '9001') ?> </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="view_geo_last_3_month"> <?php InterfaceLanguage('Screen', '9215') ?> </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="view_geo_last_6_month"> <?php InterfaceLanguage('Screen', '9216') ?> </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="view_geo_last_year"> <?php InterfaceLanguage('Screen', '9217') ?>  </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="vmap_world" class="vmaps display-none" style="height: 370px"> </div>
                        </div>
                    </div>
                </div>
                <!-- END REGIONAL STATS PORTLET-->
            </div>
        </div>
        <div class="row" style="margin-bottom: 40px">
            <div class="col-md-12">
                <div class="mt-element-list">
                    <div class="mt-list-head list-default custom_list">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="list-head-title-container" style="padding-top: 10px">
                                    <h3 class="list-title sbold font-purple-sharp"><?php InterfaceLanguage('Screen', '0099', false, '', false, true); ?> </h3>
                                </div>
                            </div>
                            <div class="col-xs-4" style="text-align: right">
                                <div class="list-head-summary-container">
                                    <div class="list-pending">
                                        <div class="list-label"><a href="<?php InterfaceAppURL(false); ?>/user/campaigns/browse/" class="btn default btn-transparen btn-sm"> View All </a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <?php
                        if (count($RecentCampaigns) < 1):
                            ?>
                            <p class="no-data-message"><?php InterfaceLanguage('Screen', '1374', false, '', false); ?></p>
                            <?php
                        else:
                            ?>
                            <ul class="items" >

                                <?php
                                $i = 0;
                                foreach ($RecentCampaigns as $Each):
                                    if ($i == 3) {
                                        break;
                                    }
                                    $i++;
                                    $img_url = isset($Each['Email']['ScreenshotImage']) && !empty($Each['Email']['ScreenshotImage']) ? AWS_END_POINT . S3_BUCKET . "/" . $Each['Email']['ScreenshotImage'] : TEMPLATE_URL . "assets/layouts/layout3/img/scrrenshot.png";
                                    ?>
                                    <li class="item item-custom ng-scope ng-isolate-scope">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-10">
                                                <div class="campaign-details">
                                                    <?php // print_r($Each['Email']['HTMLContent'])  ?>
                                                    <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                       data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                        <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                    </a>
                                                    <div class="column-block">
                                                        <h4><a class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php print($Each['CampaignID']); ?>/" dir="ltr"><?php print($Each['CampaignName']); ?></a></h4>
                                                        <span class="type ng-binding"><?php print($Each['CreateDateTime']); ?></span>
                                                        <div class="btn-group">
                                                            <a type="button" class="btn default" href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php print($Each['CampaignID']); ?>/">View report</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-7 col-sm-2 hidden-xs">

                                                <div class="stats visible-lg visible-md">
                                                    <ul>
                                                        <li>
                                                            <span class="number ng-binding bold"><?php print($Each['TotalSent']); ?></span>
                                                            <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9203', false); ?></span>
                                                        </li>

                                                        <li class="clicked">
                                                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $Each['CampaignID']; ?>/opens">
                                                                <span class="number ng-binding bold"><?php print($Each['TotalOpens']); ?></span>
                                                            </a>
                                                            <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0837'); ?></span>
                                                        </li>

                                                        <li class="clicked">
                                                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $Each['CampaignID']; ?>/clicks">
                                                                <span class="number ng-binding bold"><?php print($Each['TotalClicks']); ?></span>
                                                            </a>
                                                            <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '0838'); ?></span>
                                                        </li>

                                                        <li class="clicked">
                                                            <a href="<?php InterfaceAppURL(); ?>/user/campaign/reports/<?php echo $Each['CampaignID']; ?>/unsubscriptions">
                                                                <span class="number ng-binding bold"><?php print($Each['TotalUnsubscriptions']); ?></span>
                                                            </a>
                                                            <span class="description font-purple-sharp bold "><?php InterfaceLanguage('Screen', '9205'); ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <?php
                                endforeach;
                                ?>
                            </ul>
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hidden"></div>
<script>
    $(document).ready(function () {
//        $('#tab_auto_responder2').addClass('hidden');

        $(".tab_help").click(function () {
            $("#home_data").hide()
        });
    })

</script>

<?php $SecuredUserID = hash_hmac('sha256', $UserInformation['Username'], "e104a1ad06e2b4d917d78de81bf41dc8"); ?>
<!-- Email Automation Tracker - Start -->
<script type="text/javascript" charset="utf-8" id="js-email-automation">
    <?php if ($UserInformation['IsAdmin'] == 'No') {
        ?>
        window.email_automation_settings = {
            automation_user_id: "W11-1C/d37eb52d",
            automation_url: "https://app.flyinglist.com/cp/octautomation/track",
            user_id: '<?php echo $UserInformation['Username'] ?>',
            secured_user_id: '<?php echo $SecuredUserID ?>',
            email: '<?php echo $UserInformation['EmailAddress'] ?>',
            extra_info: {
                'name': '<?php echo $UserInformation['FirstName'] . " " . $UserInformation['LastName'] ?>',
                'phone': '<?php echo $UserInformation['Phone'] ?>'
            }
        };
    <?php } ?>
</script>
<script type="text/javascript" charset="utf-8">(function () {
        var w = window;
        var d = document;
        function l() {
            var s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'app.flyinglist.com/plugins/octautomation/js/tracker.js';
            var x = d.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (w.attachEvent) {
            w.attachEvent('onload', l);
        } else {
            w.addEventListener('load', l, false);
        }
    })();</script>
<!-- Email Automation Tracker - End -->

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>
