<script>
    var ComponentsKnobDials = function () {

        return {
            //main function to initiate the module

            init: function () {
                //knob does not support ie8 so skip it
                if (!jQuery().knob || App.isIE8()) {
                    return;
                }

                // general knob
                $('.knob').val(0).trigger('change').delay(90);
                $(".knob").knob({
                    'min': 0,
                    'max': <?php echo ($ListInformation['SubscriberCount'] - $ListInformation['UnsubscribesStatistics']['TotalUnsubscriptions']) ?>,
                    'dynamicDraw': true,
                    'thickness': 0.3,
                    'tickColorizeValues': true,
                    'skin': 'tron',
                    'readOnly': true,
                    'inputColor': '#333',
                    'fgColor ': '#1ac6ff',
                    'bgColor': 'rgba(26, 198, 255, 0.2)'
                });

                var tmr = self.setInterval(function () {
                    myDelay()
                }, 0.25);

                var m = 0;

                function myDelay() {
//echo $ListInformation['UnsubscribesStatistics']['TotalUnsubscriptions']
                    $('.knob').val(m).trigger('change');
                    var success_ratio = <?php echo ($ListInformation['SubscriberCount'] - $ListInformation['UnsubscribesStatistics']['TotalUnsubscriptions']) ?>;
                    var rate;
                    if (success_ratio % 1000 > 0)
                    {
                        rate = 8;
                    } else if (success_ratio % 100 > 0)
                    {
                        rate = 5;
                    } else if (success_ratio % 10 > 0)
                    {
                        rate = 1;
                    }
                    m += rate;
                    if (m == (success_ratio + rate) || success_ratio == 0) {
                        window.clearInterval(tmr);
                    }
                }
            }

        };

    }();

    jQuery(document).ready(function () {
        ComponentsKnobDials.init();
    });
</script>

