<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1029', false, '', false, true); ?></span>                    
                </div>
            </div>
            <div class="portlet-body form">
                <form id="suppression-table-form" action="<?php InterfaceAppURL(); ?>/user/suppressionlist/globallist/" method="post">
                    <?php if (InterfacePrivilegeCheck('List.Delete', $UserInformation)): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                    <?php
                                elseif (isset($PageErrorMessage) == true):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                    <?php
                                endif;
                                ?>
                                <div class="grid-operations">
                                    <span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="suppression-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="suppression-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                    <!-- <a href="#" class="grid-select-inverse" targetgrid="suppression-table"><?php InterfaceLanguage('Screen', '0041'); ?></a> -->
                                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="suppression-table-form"><?php InterfaceLanguage('Screen', '1030'); ?></a>
                                    <input type="hidden" name="Command" value="Delete" id="Command">
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="grid table table-bordered" id="suppression-table">
                                    <tr>
                                        <th width="570" colspan="2"><?php InterfaceLanguage('Screen', '0010', false, '', true); ?></th>
                                        <th><?php InterfaceLanguage('Screen', '1031', false, '', true); ?></th>
                                    </tr>
                                    <?php if ($Emails === false): ?>
                                        <tr>
                                            <td><?php InterfaceLanguage('Screen', '1034', false, '', false, false); ?></td>
                                        </tr>
                                    <?php else: ?>
                                        <?php
                                        foreach ($Emails as $EachEmail):
                                            ?>
                                            <tr>
                                                <td width="15" style="vertical-align:top">
                                                    <?php if ($EachEmail['SuppressionSource'] == 'User'): ?>
                                                        <input class="grid-check-element" type="checkbox" name="SelectedEmails[]" value="<?php print($EachEmail['SuppressionID']); ?>" id="SelectedEmails<?php print($EachEmail['SuppressionID']); ?>">
                                                    <?php endif; ?>
                                                </td>
                                                <td width="355" class="no-padding-left">
                                                    <?php print($EachEmail['EmailAddress']); ?>												
                                                </td>
                                                <td width="130" class="small-text"><?php echo $EachEmail['SuppressionSource'] ?></span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <?php if ($TotalPages > 1): ?>
                    <div class="span-1">
                        <ul class="pagination with-module">
                            <?php
                            if ($CurrentPage > 1):
                                ?>
                                <li class="first"><a href="<?php InterfaceAppURL(); ?>/user/suppressionlist/globallist/1/<?php print($RPP); ?>/">&nbsp;</a></li>
                                <?php
                            endif;
                            ?>
                            <?php
                            $CounterStart = ($CurrentPage < 3 ? 1 : $CurrentPage - 2);
                            $CounterFinish = ($CurrentPage > ($TotalPages - 2) ? $TotalPages : $CurrentPage + 2);
                            for ($PageCounter = $CounterStart; $PageCounter <= $CounterFinish; $PageCounter++):
                                ?>
                                <li <?php print(($CurrentPage == $PageCounter ? 'class="selected"' : '')); ?>><a href="<?php InterfaceAppURL(); ?>/user/suppressionlist/globallist/<?php print($PageCounter); ?>/<?php print($RPP); ?>/"><?php print(number_format($PageCounter)); ?></a></li>
                                <?php
                            endfor;
                            ?>
                            <?php
                            if ($CurrentPage < $TotalPages):
                                ?>
                                <li class="last"><a href="<?php InterfaceAppURL(); ?>/user/suppressionlist/globallist/<?php print($TotalPages); ?>/<?php print($RPP); ?>/">&nbsp;</a></li>
                                <?php
                            endif;
                            ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_globalsuppressionlist.php'); ?>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '1033': '<?php InterfaceLanguage('Screen', '1033', false, '', false); ?>'
        }
    };
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/suppressionlist.js" type="text/javascript" charset="utf-8"></script>		

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>