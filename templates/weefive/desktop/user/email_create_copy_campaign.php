<form id="campaign-create" action="<?php echo $FormURL; ?>" method="post">
    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
    <input type="hidden" name="ForceFieldFill" value="true" id="ForceFieldFill">
    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">

    <div>
        <div class="col-md-8" style="padding: 0;" >
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-purple-sharp"> 
                            <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                        </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                        <?php
                    endif;
                    ?>

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0796', false, '', false, true); ?></h4>
                    <div class="form-row no-bg">
                        <p><?php InterfaceLanguage('Screen', '1851', false); ?></p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('PreviousCampaign') != '' ? 'error' : '')); ?>" id="form-row-CampaignName">
                                <div class="col-md-6">
                                    <select name="PreviousCampaign" id="PreviousCampaign" class="form-control">
                                        <?php foreach ($Campaigns as $Each): ?>
                                            <?php if ($Each['CampaignID'] == $EntityID) continue; ?>
                                            <option value="<?php echo $Each['CampaignID']; ?>"><?php echo $Each['CampaignName']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <?php print(form_error('PreviousCampaign', '<span class="help-block">', '</span>')); ?>
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreatecopycampaign.php'); ?>
    </div>
</div>
<div class="col-md-8" style="padding: 0;" >
    <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
</div>
</form>