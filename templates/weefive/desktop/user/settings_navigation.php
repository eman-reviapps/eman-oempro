
<ul class="nav nav-tabs hidden-print">
    <?php if (InterfacePrivilegeCheck('User.Update', $UserInformation)) : ?>
        <li class="with-module<?php print($SubSection == 'Account' ? ' selected active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/account/"><?php InterfaceLanguage('Screen', '1106', false, '', false); ?></a></li>
        <li class="<?php print( ( $SubSection == 'APIKeys' || $ActiveSettingsItem == 'APIKeys') ? ' active' : ''); ?>"><a href="<?php InterfaceAppURL(); ?>/user/apikeys/"><?php InterfaceLanguage('Screen', '1940', false, '', false); ?></a></li>
    <?php endif; ?>
    <?php if (InterfacePrivilegeCheck('Clients.Get', $UserInformation)) : ?>
        <li class="<?php print( ($SubSection == 'Clients' || $ActiveSettingsItem == 'Clients') ? 'selected active' : ''); ?> <?php echo InterfacePrivilegeCheck('User.Update', $UserInformation) ? '' : 'with-module'; ?>"><a href="<?php InterfaceAppURL(); ?>/user/clients/"><?php InterfaceLanguage('Screen', '0572', false, '', false); ?></a></li>
    <?php endif; ?>
    <?php if (InterfacePrivilegeCheck('EmailTemplates.Manage', $UserInformation)) : ?>
        <li <?php print($SubSection == 'EmailTemplates' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/"><?php InterfaceLanguage('Screen', '0111', false, '', false); ?></a></li>
    <?php endif; ?>
    <?php if (PME_USAGE_TYPE == 'User' || WUFOO_INTEGRATION_ENABLED): ?>
        <li <?php print(substr($SubSection, 0, 11) == 'Integration' ? 'class="active selected"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/integration/"><?php InterfaceLanguage('Screen', '0132', false, '', false); ?></a></li>
    <?php endif; ?>
    <li <?php print($SubSection == 'Tags' ? 'class="selected active"' : ''); ?>><a href="<?php InterfaceAppURL(); ?>/user/tags/"><?php InterfaceLanguage('Screen', '0723', false, '', false); ?></a></li>
    <?php
    InterfacePluginMenuHook('User.Settings', $SubSection, '<li><a href="_LINK_"></i>_TITLE_</a></li>', '<li class="active selected "><a href="_LINK_">_TITLE_</a></li>'
    );
    ?>
</ul>