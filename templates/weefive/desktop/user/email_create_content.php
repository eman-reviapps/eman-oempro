<?php
//echo $CurrentFlow->get_id();
//echo $FormURL;
//print_r($FieldDefaultValues) ;
//echo $FieldDefaultValues['Mode'];
//echo $FieldDefaultValues['ContentType'];
//print_r($_SESSION[SESSION_NAME]['EmailInformation']);
//$email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
//$content_type = $email->get_content_type();
//echo $content_type;
//echo "ss" . $FieldDefaultValues['ContentType'];
?>
<div>
    <div class="col-md-8" style="padding: 0;" >
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> 
                        <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title() ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                if (isset($PageErrorMessage) == true):
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php print($PageErrorMessage); ?>
                    </div>
                    <?php
                elseif (validation_errors()):
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                    </div>
                    <?php
                endif;
                ?>
                <form id="campaign-create" class="form-inputs" action="<?php echo $FormURL; ?>" method="post">
                    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
                    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0781', false, '', false, true); ?></h4>

                    <?php if (IMAGE_EMBEDDING_ENABLED): ?>
                        <div class="form-group" id="form-row-ImageEmbedding">
                            <div class="checkbox-container">
                                <div class="checkbox-row" style="margin-left:0px;">
                                    <input type="checkbox" name="ImageEmbedding" value="Enabled" id="ImageEmbedding" <?php echo set_checkbox('ImageEmbedding', 'Enabled', $FieldDefaultValues['ImageEmbedding']); ?>>
                                    <label for="ImageEmbedding"><?php InterfaceLanguage('Screen', '0782', false, '', false, false); ?></label>
                                </div>
                            </div>
                            <span class="help-block" style="margin-left:0px;">
                                <p><?php InterfaceLanguage('Screen', '0783', false, '', false, false); ?></p>
                            </span>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="form-row-ContentType">
                                <label class="col-md-3 control-label"  for="ContentType"><?php InterfaceLanguage('Screen', '0785', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <select <?php echo $class ?> name="ContentType" id="ContentType" class="form-control">
                                        <option value="HTML" <?php echo set_select('ContentType', 'HTML', $FieldDefaultValues['ContentType'] == 'HTML' ? true : false); ?>><?php InterfaceLanguage('Screen', '0786', false, '', false, false); ?></option>
                                        <option value="Plain" <?php echo set_select('ContentType', 'Plain', $FieldDefaultValues['ContentType'] == 'Plain' ? true : false); ?>><?php InterfaceLanguage('Screen', '0787', false, '', false, false); ?></option>
                                        <option value="Both" <?php echo set_select('ContentType', 'Both', $FieldDefaultValues['ContentType'] == 'Both' ? true : false); ?>><?php InterfaceLanguage('Screen', '0788', false, '', false, false); ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0784', false, '', false, true); ?></h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Subject') != '' ? 'has-error' : '')); ?>" id="form-row-Subject">
                                <label class="col-md-3 control-label" for="Subject"><?php InterfaceLanguage('Screen', '0106', false, '', false, true); ?>: *</label>
                                <div class="col-md-8">
                                    <input type="text" name="Subject" value="<?php echo set_value('Subject', $FieldDefaultValues['Subject']); ?>" id="Subject" class="form-control personalized" />
                                    <div class="form-row-note personalization" id="personalization-Subject">
                                        <div class="well" style="margin: 0">
                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                            <select name="personalization-select-Subject" id="personalization-select-Subject" class="personalization-select">
                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                <?php foreach ($SubjectTags as $Label => $TagGroup): ?>
                                                    <optgroup label="<?php print(htmlspecialchars($Label, ENT_QUOTES)); ?>">
                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                            <option value="<?php print($Tag); ?>"><?php print(htmlspecialchars($Label, ENT_QUOTES)); ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                    <?php print(form_error('Subject', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php // if ($CurrentFlow->get_id() == 'fromscratch') { ?>
                    <!--                        <div class="row">
                                                <div class="col-md-12">    
                                                    <div class="form-group <?php print((form_error('HTMLContent') != '' ? 'has-error' : '')); ?>" id="form-row-HTMLContent">
                                                        <label class="col-md-3 control-label" for="HTMLContent"><?php InterfaceLanguage('Screen', '0175', false, '', false, true); ?>: *</label>
                                                        <div class="col-md-8">
                                                            <textarea name="HTMLContent" id="HTMLContent" class="form-control personalized" rows="10"><?php echo set_value('HTMLContent', $FieldDefaultValues['HTMLContent']); ?></textarea>
                                                            <div class="form-row-note personalization" id="personalization-HTMLContent">
                                                                <div class="well" style="margin: 0">
                                                                    <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                                                    <select name="personalization-select-HTMLContent" id="personalization-select-HTMLContent" class="personalization-select">
                                                                        <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                    <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                                                                    <optgroup label="<?php print($Label); ?>">
                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                                                                    <option value="<?php print($Tag); ?>"><?php print(htmlspecialchars($Label, ENT_QUOTES)); ?></option>
                        <?php endforeach; ?>
                                                                                    </optgroup>
                    <?php endforeach; ?>
                                                                    </select>&nbsp;&nbsp;
                                                                </div>
                                                            </div>
                    <?php print(form_error('HTMLContent', '<span class="help-block">', '</span>')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                    <?php
                    // }else
                    if ($CurrentFlow->get_id() == 'dragdropemail' || $CurrentFlow->get_id() == 'fromtemplate' || $CurrentFlow->get_id() == 'fromscratch' || ($CurrentFlow->get_id() == 'previouscampaign' && $FieldDefaultValues['Mode'] == 'Empty')) {
                        ?>
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="form-group clearfix  <?php print((form_error('HTMLContent') != '' ? 'has-error' : '')); ?>" id="form-row-HTMLContent">
                                    <label class="col-md-3 control-label" for="HTMLContent"><?php InterfaceLanguage('Screen', '0175', false, '', false, true); ?>:</label>
                                    <div class="col-md-8">
                                        <a id="open-content-builder" class="btn default btn-transparen btn-sm" href="#" style="float:left;"><strong><?php InterfaceLanguage('Screen', '9187', false, '', false, false); ?></strong></a>
                                        <input type="hidden" name="ContinueToBuilder" value="false" id="ContinueToBuilder">
                                        <input type="hidden" name="BuilderType" value="dragdrop" id="BuilderType">
                                        <span class="help-block" style="clear:both">
                                            <?php InterfaceLanguage('Screen', '9188', false, '', false, false); ?>
                                        </span>
                                        <?php print(form_error('HTMLContent', '<span class="help-block">', '</span>')); ?>
                                        <textarea name="HTMLContent" id="HTMLContent" class="form-control" rows="5" style="display:none"><?php echo $FieldDefaultValues['HTMLContent']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        ?>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="form-group clearfix  <?php print((form_error('HTMLContent') != '' ? 'has-error' : '')); ?>" id="form-row-HTMLContent">
                                    <label class="col-md-3 control-label" for="HTMLContent"><?php InterfaceLanguage('Screen', '0175', false, '', false, true); ?>:</label>
                                    <div class="col-md-8">
                                        <a id="open-content-builder" class="btn default btn-transparen btn-sm" href="#" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0799', false, '', true, false); ?></strong></a>
                                        <input type="hidden" name="ContinueToBuilder" value="false" id="ContinueToBuilder">
                                        <input type="hidden" name="BuilderType" value="old" id="BuilderType">
                                        <span class="help-block" style="clear:both">
                                            <p><?php InterfaceLanguage('Screen', '0800', false, '', false, false); ?></p>
                                        </span>
                                        <?php print(form_error('HTMLContent', '<span class="help-block">', '</span>')); ?>
                                        <textarea name="HTMLContent" id="HTMLContent" class="form-control" style="display:none"><?php echo $FieldDefaultValues['HTMLContent']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">    
                            <div class="form-group <?php print((form_error('PlainContent') != '' ? 'has-error' : '')); ?>" id="form-row-PlainContent">
                                <label class="col-md-3 control-label" for="PlainContent"><?php InterfaceLanguage('Screen', '0176', false, '', false, true); ?>: *</label>
                                <div class="col-md-8">
                                    <textarea name="PlainContent" id="PlainContent" class="form-control personalized plain-text" rows="10"><?php echo set_value('PlainContent', $FieldDefaultValues['PlainContent']); ?></textarea>
                                    <div class="form-row-note personalization" id="personalization-PlainContent">
                                        <div class="well" style="margin: 0">
                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                            <select name="personalization-select-PlainContent" id="personalization-select-PlainContent" class="personalization-select">
                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                <?php foreach ($ContentTags as $Label => $TagGroup): ?>
                                                    <optgroup label="<?php print($Label); ?>">
                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                    <?php print(form_error('PlainContent', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 style="display: none" class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0789', false, '', false, true); ?></h4>
                </form>

                <form style="display: none" target="file-upload-frame" enctype="multipart/form-data" action="<?php InterfaceAppURL(); ?>/user/attachments/upload/" method="POST" name="file-upload-form" id="file-upload-form">
                    <div class="form-group">
                        <input type="file" name="AttachmentFile" id="AttachmentFile" />
                        <span id="uploading-indicator" style="display:none"><img id="search-loading-indicator" src="<?php InterfaceTemplateURL(); ?>/images/icon_load.gif" width="16" height="16" align="absmiddle" /> Uploading attachment...</span>
                        <span id="uploading-error-indicator" style="display:none;color:#ff0000;">File couldn't be uploaded...</span>
                        <div id="attachment-container" class="form-row-note" style="margin-left:0px;margin-top:9px;">
                            <p id="attachment-template" style="display:none"><input type="checkbox" name="attachment-checkbox[]" value="_AttachmentID_" class="attachment-checkbox" id="attachment-checkbox-_AttachmentID_" checked="checked" /> <a href="<?php InterfaceAppURL(); ?>/user/attachments/view/_AttachmentMD5ID_" target="_blank">_FileName_</a> <span class="data small" style="margin-left:18px;">_FileSize_</span></p>
                        </div>
                    </div>
                </form>
                <iframe name="file-upload-frame" id="file-upload-frame" style="height:1px;width:1px;display:none"></iframe>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreatecontent.php'); ?>
    </div>
</div>
<div class="col-md-8" style="padding: 0;" >
    <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
</div>

<script src="<?php InterfaceTemplateURL(); ?>js/tiny_mce_3432/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
<script>
    var tinymce_config = {
        // Location of tinymce script
        script_url: '<?php print(InterfaceTemplateURL()); ?>js/tiny_mce_3432/tiny_mce.js',
        // General options
        convert_fonts_to_spans: false,
        valid_children: "+span[font],+font[p],+body[style]",
        theme: 'advanced',
        plugins: 'fullpage,safari,table,style,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,personalizer',
        browsers: 'msie,gecko,safari,opera',
        dialog_type: 'modal',
        directionality: "ltr",
        docs_language: "en",
        language: "en",
        nowrap: false,
        entity_encoding: 'named',
        verify_html: false,
        forced_root_block: '',
        convert_newlines_to_brs: false,
        element_format: 'xhtml',
        fix_list_elements: true,
        force_p_newlines: true,
        force_br_newlines: false,
        force_hex_style_colors: true,
        preformatted: true,
        paste_auto_cleanup_on_paste: true,
        relative_urls: false,
        convert_urls: false,
        // Theme options
        theme_advanced_buttons1: "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor",
        theme_advanced_buttons2: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,cleanup,fullscreen,|,code",
        theme_advanced_buttons3: "personalizer,|,table,row_after,row_before,col_after,col_before,delete_col,delete_row,delete_table,|,link,unlink,image",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        file_browser_callback: "oempro_media_library",
        // Personalizer tags
        personalizer_tags: [
<?php
$tags = array();
foreach ($ContentTags as $Label => $TagGroup) {
    $tags[] = "{ label : true, title : '" . htmlspecialchars($Label, ENT_QUOTES) . "' }";
    foreach ($TagGroup as $Tag => $Label) {
        if (strpos($Tag, '%Link:') !== false) {
            if ($Tag == '%Link:Confirm%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1659', true, '', false, false) . '</a>';
            } elseif ($Tag == '%Link:Reject%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1660', true, '', false, false) . '</a>';
            } elseif ($Tag == '%Link:Forward%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1661', true, '', false, false) . '</a>';
            } elseif ($Tag == '%Link:WebBrowser%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1662', true, '', false, false) . '</a>';
            } elseif ($Tag == '%Link:Unsubscribe%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1663', true, '', false, false) . '</a>';
            } elseif ($Tag == '%Link:SubscriberArea%') {
                $Tag = '<a href="' . $Tag . '">' . InterfaceLanguage('Screen', '1664', true, '', false, false) . '</a>';
            } else {
                $Tag = '<a href="' . $Tag . '">' . $Label . '</a>';
            }
        }
        $tags[] = "{ title : '" . htmlspecialchars($Label, ENT_QUOTES) . "', tags : '" . $Tag . "' }";
    }
}
echo implode(',', $tags);
?>
        ]
    };

    function oempro_media_library(field_name, url, type, win) {
        tinyMCE.activeEditor.windowManager.open({
            file: '<?php InterfaceAppURL(); ?>/user/medialibrary/browse/0/popup', // PHP session ID is now included if there is one at all
            width: 450,
            height: 500,
            resizable: "yes",
            close_previous: "no",
            scrollbars: "yes",
            popup_css: false,
            inline: true
        }, {
            window: win,
            input: field_name
        });
        return false;
    }


    var wysiwyg_enabled = <?php print(WYSIWYG_ENABLED); ?>;
    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
    var attachments = [
<?php
if ($Attachments != false) {
    for ($i = 0; $i < count($Attachments); $i++) {
        ?>
            { AttachmentID: '<?php print($Attachments[$i]['AttachmentID']); ?>', AttachmentMD5ID: '<?php print(md5($Attachments[$i]['AttachmentID'])); ?>', FileName: '<?php print(htmlspecialchars($Attachments[$i]['FileName'], ENT_QUOTES)); ?>', FileMimeType: '<?php print($Attachments[$i]['FileMimeType']); ?>', FileSize: '<?php print($Attachments[$i]['FileSize']); ?>' }<?php print($i < count($Attachments) - 1 ? ',' : ''); ?>
        <?php
    }
}
?>
    ];
            var languageObject = {
                '1854': "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1854', true, '', true), ENT_QUOTES); ?>",
                '1855': "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1855', true, '', true), ENT_QUOTES); ?>",
                '1856': "<?php echo htmlspecialchars(InterfaceLanguage('Screen', '1856', true), ENT_QUOTES); ?>"
            };

    $(document).ready(function () {
        content_library.init_handlers();
        $('#open-content-builder').click(function () {
            $('#ContinueToBuilder').val('true');
//            $('#BuilderType').val('old');
            $('#campaign-create').submit();
        });
//        $('#open-dragdrop-builder').click(function () {
//            $('#ContinueToDragDropBuilder').val('true');
//            $('#BuilderType').val('dragdrop');
//            $('#campaign-create').submit();
//        });
    });
</script>