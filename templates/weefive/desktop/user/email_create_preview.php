<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/lightbox.css" type="text/css" media="screen" title="no title" charset="utf-8">

<form id="campaign-create" class="form-inputs" action="<?php echo $FormURL; ?>" method="post">
    <input type="hidden" name="Command" value="" id="Command">
    <input type="hidden" name="FormSubmit" value="true" id="FormSubmit">
    <input type="hidden" name="EditEvent" value="<?php echo $EditEvent ? 'true' : 'false'; ?>" id="EditEvent">

    <div>
        <div class="col-md-8" style="padding: 0;" >
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-purple-sharp"> 
                            <?php InterfaceLanguage('Screen', '0754', false, '', false, false, array($Wizard->get_current_step())); ?> / <?php echo $CurrentStep->get_title(); ?>
                        </span>
                    </div>
                </div>
                <div class="portlet-body form">

                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (isset($PageSuccessMessage) == true):
                        ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageSuccessMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info alert-info-custom"">
                            <?php InterfaceLanguage('Screen', '0803', false, '', false, true); ?>
                        </div>
                    <?php
                    endif;
                    ?>

                    <div class="form-group">
                        <span class="help-block"><?php InterfaceLanguage('Screen', '0804', false, '', false, false); ?></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="form-group" id="form-row-PreviewType">
                                <label class="col-md-3 control-label" for="PreviewType"><?php InterfaceLanguage('Screen', '0805', false, '', false, true); ?>:</label>
                                <div class="col-md-8">
                                    <div class="checkbox-container">
                                        <div class="checkbox-row">
                                            <input type="radio" name="PreviewType" value="email" id="PreviewType-Email" checked="checked"> <label for="PreviewType-Email"><?php InterfaceLanguage('Screen', '0806', false, '', false, false); ?></label>
                                        </div>
                                        <div class="checkbox-row">
                                            <input type="radio" name="PreviewType" value="browser" id="PreviewType-Browser"> <label for="PreviewType-Browser"><?php InterfaceLanguage('Screen', '0807', false, '', false, false); ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php print((form_error('PreviewEmailAddress') != '' ? 'has-error' : '')); ?>" id="form-row-PreviewEmailAddress" style="display:none">
                        <label class="col-md-3 control-label" for="PreviewEmailAddress"><?php InterfaceLanguage('Screen', '0010', false, '', false, true); ?>: *</label>
                        <div class="col-md-8">
                            <input type="text" name="PreviewEmailAddress" value="<?php echo set_value('PreviewEmailAddress'); ?>" id="PreviewEmailAddress" class="form-control" />
                            <div class="help-block">
                                <p><?php InterfaceLanguage('Screen', '0808', false, '', false, false); ?></p>
                            </div>
                            <?php print(form_error('PreviewEmailAddress', '<span class="help-block">', '</span>')); ?>
                        </div>
                    </div>
                    <div class="form-group no-bg clearfix">
                        <a class="btn default btn-transparen btn-sm" href="#" style="float:left;display:none;" id="preview-button"><strong><?php InterfaceLanguage('Screen', '0750', false, '', true); ?></strong></a>
                    </div>

                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '1646', false, '', false, true); ?></h4>
                    <div class="form-group">
                        <span class="help-block"><?php InterfaceLanguage('Screen', '1647', false, '', false, false); ?></span>
                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid" id="activity-table" style="margin-top:9px;">
                            <?php foreach ($ImagesInEmail as $ImageType => $ImageURLs): ?>
                                <tr>
                                    <td width="40px" style="padding:9px 0 9px 9px;"><span class="data big"><?php print(number_format(count($ImageURLs))); ?></span></td>
                                    <td style="padding:9px 0;">
                                        <strong><?php print($ImageType) ?> <?php InterfaceLanguage('Screen', '1648', false, '', false, true); ?></strong>
                                        <?php foreach ($ImageURLs as $Index => $EachImageURL): ?>
                                            <?php
                                            $ImageInfo = pathinfo($EachImageURL);
                                            if (($ImageInfo['dirname'] == '.') || ($ImageInfo['dirname'] == '')):
                                                ?>
                                                <br><?php InterfaceLanguage('Screen', '1649', false, '', false, false); ?>
                                                <?php
                                                break;
                                            endif;
                                            ?>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php foreach ($CSSInEmail as $CSSType => $CSSStyles): ?>
                                <tr>
                                    <td width="40px" style="padding:9px 0 9px 9px;"><span class="data big"><?php print(number_format(count($CSSStyles))); ?></span></td>
                                    <td style="padding:9px 0;"><strong><?php InterfaceLanguage('Screen', '1650', false, $CSSType, false, false); ?></strong></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php foreach ($JavaScriptInEmail as $JSType => $JSTags): ?>
                                <tr>
                                    <td width="40px" style="padding:9px 0 9px 9px;"><span class="data big"><?php print(number_format(count($JSTags))); ?></span></td>
                                    <td style="padding:9px 0;">
                                        <strong><?php InterfaceLanguage('Screen', '1651', false, $JSType, false, false); ?></strong>
                                        <br><?php InterfaceLanguage('Screen', '1652', false, '', false, false); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>

                    <?php if (InterfacePrivilegeCheck('Email.SpamTest', $UserInformation)): ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0815', false, '', false, true); ?></h4>
                        <div class="form-group">
                            <span class="help-block"><?php InterfaceLanguage('Screen', '0816', false, '', false, false); ?></span>
                            <table class="small-grid no-zebra table table-bordered " style="width:50%;border:1px solid #c5c5c5;margin-top:18px;">
                                <tr>
                                    <th><?php InterfaceLanguage('Screen', '0817', false, '', false, false); ?></th>
                                    <th><?php InterfaceLanguage('Screen', '0818', false, '', false, false); ?></th>
                                </tr>
                                <?php foreach ($SpamReport['Scores'] as $Each): ?>
                                    <tr>
                                        <td><span class="data"><?php echo $Each['Point'] ?></span></td>
                                        <td><?php echo $Each['Description'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                                <tr>
                                    <td style="border-top:2px solid #c5c5c5;"><span class="data bold"><?php echo $SpamReport['Hits'] ?></span></td>
                                    <td style="border-top:2px solid #c5c5c5;"><span class="bold"><?php InterfaceLanguage('Screen', '0819', false, '', false, false); ?></span></td>
                                </tr>
                            </table>
                        </div>
                    <?php endif; ?>

                    <?php if (InterfacePrivilegeCheck('Email.DesignPreview', $UserInformation) && $IsPreviewMyEmailDisabled == FALSE): ?>
                        <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0820', false, '', false, true); ?></h4>
                        <?php if ($ShowPreviewMyEmailPromotion == TRUE): ?>
                            <div class="form-group">
                                <p><?php InterfaceLanguage('Screen', '0822', false, '', false, false); ?></p>
                            </div>
                        <?php else: ?>
                            <div class="form-group no-bg clearfix">
                                <a class="btn default btn-transparen btn-sm" href="#" style="float:left;" id="test-button"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0821', false, '', true); ?></strong></a>
                            </div>
                            <?php if (isset($DesignTests) && count($DesignTests) > 0): ?>
                                <div class="form-group">
                                    <label for="DesignTest"><?php InterfaceLanguage('Screen', '0823', false, '', false, true); ?></label>
                                    <select id="DesignTest" name="DesignTest">
                                        <?php foreach ($DesignTests as $Each): ?>
                                            <option value="<?php echo $Each['JobID']; ?>"><?php echo $Each['SubmitDate']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <div class="form-row-note">
                                        <p><?php InterfaceLanguage('Screen', '0824', false, '', false, false); ?></p>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="form-group no-bg clearfix">
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0825', false, '', false, false); ?></span>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>



                    <?php if (InterfacePrivilegeCheck('Email.DesignPreview', $UserInformation) && $IsPreviewMyEmailDisabled == FALSE): ?>
                        <?php if (isset($DesignTests) && count($DesignTests) > 0): ?>
                            <?php if ($ShowPreviewMyEmailPromotion == FALSE): ?>
                                <div class="page-bar">
                                    <h2><?php InterfaceLanguage('Screen', '0826', false, '', false, true); ?></h2>
                                </div>
                                <div class="blue">
                                    <div id="test-container">
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_emailcreatepreview.php'); ?>
        </div>
    </div>
    <div class="col-md-8" style="padding: 0;" >
        <?php include_once(TEMPLATE_PATH . 'desktop/user/email_create_wizard_steps.php'); ?>
    </div>

</form>

<script>
    var TemplateURL = '<?php InterfaceTemplateURL(); ?>';
</script>
<script src="<?php InterfaceTemplateURL(); ?>js/jquery.lightbox.js" type="text/javascript" charset="utf-8"></script>
<script>
    var Language = {
        '0811': '<?php InterfaceLanguage('Screen', '0811', false, '', true, false); ?>',
        '0812': '<?php InterfaceLanguage('Screen', '0812', false, '', true, false); ?>',
        '1425': '<?php InterfaceLanguage('Screen', '1425', false, '', false, false); ?>',
        '1426': '<?php InterfaceLanguage('Screen', '1426', false, '', false, false); ?>'
    };
    var TemplateURL = '<?php InterfaceTemplateURL(); ?>';
    var EmailID = <?php echo $EmailID; ?>;
    var preview_url = '<?php InterfaceAppURL(); ?>/user/email/preview/' + EmailID + '/html/<?php echo $Mode; ?>/<?php echo $EntityID; ?>';
    var api_url = '<?php InterfaceInstallationURL(); ?>api.php';
    var screen_type = 'create';
    $(document).ready(function () {
        preview_library.init_handlers();
    });
</script>