<ul class="nav nav-tabs ">
    <li<?php if ($SubSection == 'Statistics'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0836', false, '', false, true); ?></a></li>
    <?php if (InterfacePrivilegeCheck('CustomFields.Get', $UserInformation)): ?>
        <li<?php if ($SubSection == 'CustomFields' || $ActiveListItem == 'CustomFields'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/customfields/browse/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0547', false, '', false, true); ?></a></li>
    <?php endif; ?>
    <?php if (InterfacePrivilegeCheck('Segments.Get', $UserInformation)): ?>
        <li<?php if ($SubSection == 'Segments' || $ActiveListItem == 'Segments'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/segments/browse/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0563', false, '', false, true); ?></a></li>
    <?php endif; ?>
    <?php if (InterfacePrivilegeCheck('AutoResponders.Get', $UserInformation)): ?>
        <li<?php if ($SubSection == 'AutoResponders' || $ActiveListItem == 'AutoResponders'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/autoresponders/browse/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0557', false, '', false, true); ?></a></li>
    <?php endif; ?>
    <?php if (InterfacePrivilegeCheck('List.Update', $UserInformation) && InterfacePrivilegeCheck('Subscribers.Import', $UserInformation)): ?>
        <li<?php if ($SubSection == 'Synchronization'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/list/synchronization/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '1257', false, '', false, true); ?></a></li>
    <?php endif; ?>
    <li<?php if ($SubSection == 'Settings'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/list/settings/<?php echo $ListInformation['ListID']; ?>"><?php InterfaceLanguage('Screen', '0110', false, '', false, true); ?></a></li>

    <?php
    if ($ListInformation['IsAutomationList'] == 'No') {
        InterfacePluginMenuHook('User.List.Options', $SubSection, '<li><a href="_LINK_/' . $ListInformation['ListID'] . '">_TITLE_</a></li>', '<li class="active"><a href="_LINK_/' . $ListInformation['ListID'] . '">_TITLE_</a></li>'
        );
    }
    ?>
</ul>
