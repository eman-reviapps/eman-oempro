<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>



<div class="row">
    <div class="col-md-12">
        <!--light bg-inverse-->
        <div class="portlet " style="padding-bottom: 0px;margin-bottom: 0px" >
            <div class="portlet-title">
                <div class="caption">
                    <?php if (count($ListInformation) > 0 && $ListID != 0): ?>
                        <span class="caption-subject bold font-purple-sharp"><?php InterfaceLanguage('Screen', '9271', false, '', false, true, array($ListInformation['Name'])); ?></span>
                    <?php else: ?>
                        <span class="caption-subject bold font-purple-sharp"><?php InterfaceLanguage('Screen', '9138', false, '', false, true); ?></span>
                    <?php endif; ?>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" > 
                        <?php
                        if (count($ListInformation) > 0 && $ListID != 0) {
                            ?>
                            <!--<a class="btn default btn-transparen btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/list/statistics/<?php echo $ListInformation['ListID']; ?>"><strong><?php InterfaceLanguage('Screen', '9261', false, '', false, true); ?></strong></a>-->
                        <a class="btn default btn-transparen btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/lists/browse"><strong><?php InterfaceLanguage('Screen', '9282', false, '', false, true); ?></strong></a>
                            <?php
                        } else {
                            ?>
                            <a class="btn default btn-transparen btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/lists/browse"><strong><?php InterfaceLanguage('Screen', '9282', false, '', false, true); ?></strong></a>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="portlet light portlet-transparent">
            <div class="portlet-title btn_move" style="margin-bottom: 0">
                <div class="col-md-412" style="padding: 0">
                    <a href="#" class="btn default" id="move_back_to_list" ><?php InterfaceLanguage('Screen', '9261'); ?></a> 
                    <a href="#" class="btn default" id="move_to_another_list" ><?php InterfaceLanguage('Screen', '9250'); ?></a> 
                    <a href="#" class="btn default" id="move_to_global_supprression"><?php InterfaceLanguage('Screen', '9260'); ?></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="clearfix"></div>
                <div class="form-group clearfix" style="margin-bottom: 0" >
                    <label class="col-md-3 control-label" for="Subscribers"><?php InterfaceLanguage('Screen', '0541', false, '', false, true); ?>:</label>
                    <div class="col-md-4">
                        <select name="Lists" id="Lists" class="form-control">
                            <option value="0"> -- <?php InterfaceLanguage('Screen', '1029', false, '', false, true); ?> -- </option>
                            <?php
                            foreach ($Lists as $EachList):
                                if ($EachList['IsAutomationList'] == 'No') {
                                    ?>
                                    <option value="<?php echo $EachList['ListID']; ?>"><?php echo $EachList['Name']; ?></option>
                                    <?php
                                }
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <div id="suppression_div" style="margin-top: 50px">
                    <?php
//                            print_r('<pre>');
//                            print_r($Emails);
//                            print_r('</pre>');
                    ?>
                </div>
            </div>
        </div>  
    </div>
</div>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/suppressionlist.js" type="text/javascript" charset="utf-8"></script>		


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        listId = <?php echo $ListID ?>;
        $('#Lists option[value="' + listId + '"]').get(0).selected = true;
        applyFilters();
    })

</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>