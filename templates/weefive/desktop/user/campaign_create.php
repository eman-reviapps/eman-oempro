<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<link type="text/css" href="<?php InterfaceTemplateURL(); ?>styles/jqueryui_custom_theme/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<!--<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>js/jquery.ui.core.js"></script>-->
<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="<?php InterfaceTemplateURL(); ?>js/jquery.slider.js"></script>-->

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-doc font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0715', false, '', false, true); ?> </span>                    

                    <?php if (isset($WizardURL)): ?>
                        <div class="container">
                            <div class="notice-header campaign-notice">
                                <?php InterfaceLanguage('Screen', '1916', false, '', false, false, array($WizardCampaignName, InterfaceAppURL(true) . $WizardURL)); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="campaign-create" class="form-inputs" action="<?php InterfaceAppURL(); ?>/user/campaign/create/<?php print($Flow); ?>" method="post">
                    <input type="hidden" name="Command" value="Create" id="Command">
                    <input type="hidden" name="TestSize" value="20" id="TestSize">
                    <?php
                    if (isset($PageErrorMessage) == true):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php print($PageErrorMessage); ?>
                        </div>
                        <?php
                    elseif (validation_errors()):
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php InterfaceLanguage('Screen', '0275', false); ?>
                        </div>
                        <?php
                    endif;
                    ?>
                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0110', false, '', false, true); ?></h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('CampaignName') != '' ? 'has-error' : '')); ?>" id="form-row-CampaignName">
                                <label class="col-md-3 control-label" for="CampaignName"><?php InterfaceLanguage('Screen', '0756', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <input type="text" name="CampaignName" value="<?php echo set_value('CampaignName'); ?>" id="CampaignName" class="form-control" style="width:450px;" />
                                    <?php print(form_error('CampaignName', '<span class="help-block">', '</span>')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('Recipients') != '' ? 'has-error' : '')); ?>" id="form-row-Recipients">
                                <label class="col-md-3 control-label" for="Recipients"><?php InterfaceLanguage('Screen', '0092', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <?php if ($Lists === FALSE && InterfacePrivilegeCheck('List.Create', $UserInformation)): ?>
                                        <span class="help-block"><?php InterfaceLanguage('Screen', '0924', false, '', false, false, array(InterfaceAppURL(true) . '/user/list/create/')); ?>
                                        </span>
                                    <?php else: ?>
                                        <select name="Recipients[]" id="Recipients" multiple class="multiselect">
                                            <?php foreach ($Lists as $EachList): ?>
                                                <optgroup label="<?php print($EachList['Name']); ?>">
                                                    <option value="<?php print($EachList['ListID'] . ':0'); ?>" <?php echo set_select('Recipients', $EachList['ListID'] . ':0'); ?>><?php InterfaceLanguage('Screen', '0777', false, '', false, false); ?></option>
                                                    <?php foreach ($EachList['Segments'] as $EachSegment): ?>
                                                        <option value="<?php print($EachList['ListID'] . ':' . $EachSegment['SegmentID']); ?>" <?php echo set_select('Recipients', $EachList['ListID'] . ':' . $EachSegment['SegmentID']); ?>><?php print($EachSegment['SegmentName']); ?></option>
                                                    <?php endforeach ?>
                                                </optgroup>
                                            <?php endforeach ?>
                                        </select>
                                        <?php print(form_error('Recipients', '<span class="help-block">', '</span>')); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-3">
                                    <span class="help-block"><?php InterfaceLanguage('Screen', '0766', false, '', false, false); ?></span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox-container">
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="checkbox" name="SplitTesting" value="Enabled" id="SplitTesting" <?php echo set_checkbox('SplitTesting', 'Enabled', false); ?>>
                                <label for="SplitTesting"><?php InterfaceLanguage('Screen', '1338', false, '', false, false); ?></label>
                            </div>
                        </div>
                        <div class="help-block" style="margin-left:0px;">
                            <?php InterfaceLanguage('Screen', '1339', false, '', false, false); ?>
                        </div>
                        <div id="split-testing-options">
                            <div style="margin-top:54px;" class="clearfix">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1340', false, '', false, true); ?>:</label>
                                <div class="col-md-8">
                                    <div style="float:left;margin-top:6px;width:420px;position:relative">
                                        <div id="test-recipients-bar" style="white-space:nowrap;text-align:center;position:absolute;top:-25px;">
                                            <span class="data label small"><?php InterfaceLanguage('Screen', '1341', false, '', false, true); ?>: <span id="test-recipients-bar-ratio"></span></span>
                                            <div></div>
                                        </div>
                                        <div id="slider-range-min"></div>
                                        <div id="winning-recipients-bar" style="white-space:nowrap;text-align:center;position:absolute;bottom:-25px;right:0;">
                                            <div></div>
                                            <span class="data label small"><?php InterfaceLanguage('Screen', '1342', false, '', false, true); ?>: <span id="winning-recipients-bar-ratio"></span></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <div class="help-block" style="margin-top:9px;">
                                        <?php InterfaceLanguage('Screen', '1343', false, '', false, false, array('<span class="data" id="test-size">20%</span>', '<span class="data" id="winner-size">80%</span>')); ?>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-top:9px;">
                                <label class="col-md-3 control-label"><?php InterfaceLanguage('Screen', '1345', false, '', false, true); ?>:</label>
                                <select class="select" name="TestDurationMultiplier" id="TestDurationMultiplier">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                                <select class="select" name="TestDurationBaseSeconds" id="TestDurationBaseSeconds">
                                    <option value="3600"><?php InterfaceLanguage('Screen', '1347'); ?></option>
                                    <option value="86400"><?php InterfaceLanguage('Screen', '1346'); ?></option>
                                </select>
                            </div>
                            <div style="margin-top:9px;">
                                <label class="col-md-3 control-label" for="Winner"><?php InterfaceLanguage('Screen', '1348', false, '', false, true); ?>:</label>
                                <select name="Winner" id="Winner" class="select">
                                    <option value="Highest Open Rate"><?php InterfaceLanguage('Screen', '1349'); ?></option>
                                    <option value="Most Unique Clicks"><?php InterfaceLanguage('Screen', '1350'); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-row-PublishOnRSS">
                        <div class="checkbox-container">
                            <div class="checkbox-row" style="margin-left:0px;">
                                <input type="checkbox" name="PublishOnRSS" value="Enabled" id="PublishOnRSS" <?php echo set_checkbox('PublishOnRSS', 'Enabled', true); ?>>
                                <label for="PublishOnRSS"><?php InterfaceLanguage('Screen', '0763', false, '', false, false); ?></label>
                            </div>
                        </div>
                        <div class="help-block" style="margin-left:0px;">
                            <?php InterfaceLanguage('Screen', '0765', false, '', false, false); ?>
                        </div>
                    </div>
                    <h4 class="form-section bold font-blue"><?php InterfaceLanguage('Screen', '0770', false, '', false, true); ?></h4>
                    <div class="help-block">
                        <p><?php InterfaceLanguage('Screen', '0771', false, '', false, false, array(PRODUCT_NAME)); ?></p>
                    </div>
                    <div class="form-group" id="form-row-EnableGoogleAnalytics">
                        <div class="checkbox-container">
                            <div class="checkbox-row no-margin">
                                <input type="checkbox" name="EnableGoogleAnalytics" value="true" id="EnableGoogleAnalytics" <?php echo set_checkbox('EnableGoogleAnalytics', 'true'); ?>>
                                <label for="EnableGoogleAnalytics"><?php InterfaceLanguage('Screen', '0774', false, '', false, false); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group <?php print((form_error('GoogleAnalyticsDomains') != '' ? 'has-error' : '')); ?>" id="form-row-GoogleAnalyticsDomains" <?php if (!isset($_POST['EnableGoogleAnalytics'])): ?>style="display:none"<?php endif; ?>>
                                <label class="col-md-3 control-label" class="col-md-3 control-label" for="GoogleAnalyticsDomains"><?php InterfaceLanguage('Screen', '0775', false, '', false, true); ?>: *</label>
                                <div class="col-md-6">
                                    <textarea name="GoogleAnalyticsDomains" id="GoogleAnalyticsDomains" class="form-control" style="height:60px;"><?php echo set_value('GoogleAnalyticsDomains'); ?></textarea>
                                    <?php print(form_error('GoogleAnalyticsDomains', '<span class="help-block">', '</span>')); ?>
                                    <div class="help-block">
                                        <p><?php InterfaceLanguage('Screen', '0776', false, '', false, false); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span-18 last">
                        <div class="form-action-container">
                            
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <a class="btn default" targetform="campaign-create" href="#"><strong><?php InterfaceLanguage('Screen', '1255', false, '', true); ?></strong></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_campaigncreate.php'); ?>
    </div>
</div>

<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/campaign_create.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/split_test_settings.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>