<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>
<?php
//print_r($_SESSION['template_builder']);
//exit();
?>
<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '0311', false, '', false, true); ?> </span>                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >                                                
                        <a class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/user/emailtemplates/"><?php InterfaceLanguage('Screen', '1875', false); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="EmailTemplateEditForm" method="post" action="<?php InterfaceAppURL(); ?>/user/emailtemplates/edit/<?php print($TemplateInformation->TemplateID); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="TemplateID" value="<?php print($TemplateInformation->TemplateID); ?>" id="TemplateID" />
                    <input type="hidden" name="RelOwnerUserID" value="<?php print($UserInformation['UserID']); ?>" id="RelOwnerUserID" />
                    <div class="portlet light" style="padding: 0">
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab-1" data-toggle="tab"><?php InterfaceLanguage('Screen', '0135', false, '', false); ?></a>
                                </li>
                                <li>
                                    <a href="#tab-2" data-toggle="tab"><?php InterfaceLanguage('Screen', '0142', false, '', false); ?></a>
                                </li>
                            </ul>

                            <div class="tab-content" id="page-user-email-template-edit"> 
                                <br/>
                                <br/>
                                <br/>
                                <?php
                                if (isset($PageErrorMessage) == true):
                                    ?>
                                    <?php
                                elseif (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong><?php print($PageSuccessMessage); ?></strong>
                                    </div>
                                    <?php
                                elseif (validation_errors()):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong><?php InterfaceLanguage('Screen', '0275', false); ?></strong>
                                    </div>
                                    <?php
                                else :
                                    ?>
                                    <div class="alert alert-info alert-info-custom">
                                        <strong><?php InterfaceLanguage('Screen', '0037', false); ?></strong>
                                    </div>
                                <?php
                                endif;
                                ?>

                                <!-- Preferences Section - START -->
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php print((form_error('TemplateName') != '' ? 'has-error' : '')); ?>" id="form-row-TemplateName">
                                                <label class="control-label col-md-2" for="TemplateName"><?php InterfaceLanguage('Screen', '0051', false); ?>: *</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="TemplateName" value="<?php echo set_value('TemplateName', $TemplateInformation->TemplateName); ?>" id="TemplateName" class="form-control" />
                                                    <?php print(form_error('TemplateName', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php print((form_error('TemplateDescription') != '' ? 'has-error' : '')); ?>" id="form-row-TemplateDescription">
                                                <label class="control-label col-md-2" for="TemplateDescription"><?php InterfaceLanguage('Screen', '0170', false); ?>:</label>
                                                <div class="col-md-8">
                                                    <textarea name="TemplateDescription" id="TemplateDescription" rows="5" class="form-control"><?php echo set_value('TemplateDescription', $TemplateInformation->TemplateDescription); ?></textarea>
                                                    <?php print(form_error('TemplateDescription', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                    <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group <?php print((form_error('TemplateThumbnail') != '' ? 'has-error' : '')); ?>" id="form-row-TemplateThumbnail">
                                                                                    <label class="control-label col-md-2" for="TemplateThumbnail"><?php InterfaceLanguage('Screen', '0171', false); ?>:</label>
                                                                                    <div class="col-md-8">
                                                                                        <input type="file" name="TemplateThumbnail" value="" id="TemplateThumbnail">
                                                                                        <div class="form-row-note" style="margin-top:9px;">
                                                                                            <a href="<?php InterfaceAppURL(); ?>/user/emailtemplates/thumbnail/<?php print($TemplateInformation->TemplateID); ?>" target="_blank"><img src="<?php InterfaceAppURL(); ?>/user/emailtemplates/thumbnail/<?php print($TemplateInformation->TemplateID); ?>" width="50" border="0" /></a>
                                                                                        </div>
                                    <?php print(form_error('TemplateThumbnail', '<span class="help-block">', '</span>')); ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                </div>
                                <!-- Preferences Section - END -->

                                <!-- Email Contents Section - START -->
                                <div class="tab-pane" id="tab-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php print((form_error('TemplateSubject') != '' ? 'has-error' : '')); ?>" id="form-row-TemplateSubject">
                                                <label class="control-label col-md-2" for="TemplateSubject"><?php InterfaceLanguage('Screen', '0106', false); ?>: *</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="TemplateSubject" value="<?php echo set_value('TemplateSubject', $TemplateInformation->TemplateSubject); ?>" id="TemplateSubject" class="form-control personalized" rows="10" />
                                                    <div class="form-row-note personalization" id="personalization-TemplateSubject">
                                                        <div class="well" style="margin: 0">
                                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                                            <select name="personalization-select-TemplateSubject" id="personalization-select-TemplateSubject" class="personalization-select">
                                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                                <?php foreach ($Tags as $Label => $TagGroup): ?>
                                                                    <optgroup label="<?php print($Label); ?>">
                                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                                        <?php endforeach; ?>
                                                                    </optgroup>
                                                                <?php endforeach; ?>
                                                            </select>&nbsp;&nbsp;
                                                        </div>
                                                    </div>
                                                    <?php print(form_error('TemplateSubject', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php print((form_error('TemplateHTMLContent') != '' ? 'has-error' : '')); ?>" id="form-row-TemplateHTMLContent">
                                                <label class="control-label col-md-2" for="TemplateHTMLContent"><?php InterfaceLanguage('Screen', '0175', false); ?>:</label>
                                                <input type="hidden" name="HTMLPure" id="HTMLPure" value="<?php echo set_value('HTMLPure', $TemplateInformation->HTMLPure); ?>">
                                                <input type="hidden" name="ScreenshotImage" id="ScreenshotImage" value="<?php echo set_value('ScreenshotImage', $TemplateInformation->ScreenshotImage); ?>">
                                               
                                                <div class="col-md-8">
                                                    <textarea name="TemplateHTMLContent" id="TemplateHTMLContent" class="form-control personalized html-code"  rows="10"><?php echo set_value('TemplateHTMLContent', $TemplateInformation->TemplateHTMLContent); ?></textarea>
                                                    <div class="form-row-note personalization" id="personalization-TemplateHTMLContent">
                                                        <div class="well" style="margin: 0">
                                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                                            <select name="personalization-select-TemplateHTMLContent" id="personalization-select-TemplateHTMLContent" class="personalization-select">
                                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                                <?php foreach ($Tags as $Label => $TagGroup): ?>
                                                                    <optgroup label="<?php print($Label); ?>">
                                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                                        <?php endforeach; ?>
                                                                    </optgroup>
                                                                <?php endforeach; ?>
                                                            </select>&nbsp;&nbsp;
                                                        </div>
                                                    </div>
                                                    <?php print(form_error('TemplateHTMLContent', '<span class="help-block">', '</span>')); ?>
                                                    <div class="container-custom">
                                                        <a class="btn btn default" id="proceed-to-builder-link"><strong><?php InterfaceLanguage('Screen', '9237', false); ?></strong></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group <?php print((form_error('TemplatePlainContent') != '' ? 'has-error' : '')); ?>" id="form-row-TemplatePlainContent">
                                                <label class="control-label col-md-2" for="TemplatePlainContent"><?php InterfaceLanguage('Screen', '0176', false); ?>:</label>
                                                <div class="col-md-8">
                                                    <textarea name="TemplatePlainContent" id="TemplatePlainContent" class="form-control personalized plain-text" style="width:500px;height:200px;"><?php echo set_value('TemplatePlainContent', $TemplateInformation->TemplatePlainContent); ?></textarea>
                                                    <div class="form-row-note personalization" id="personalization-TemplatePlainContent">
                                                        <div class="well" style="margin: 0">
                                                            <strong><?php InterfaceLanguage('Screen', '0622', false, '', false, false); ?>:</strong>&nbsp;&nbsp;
                                                            <select name="personalization-select-TemplatePlainContent" id="personalization-select-TemplatePlainContent" class="personalization-select">
                                                                <option value=""><?php InterfaceLanguage('Screen', '0623', false, '', false, false); ?></option>
                                                                <?php foreach ($Tags as $Label => $TagGroup): ?>
                                                                    <optgroup label="<?php print($Label); ?>">
                                                                        <?php foreach ($TagGroup as $Tag => $Label): ?>
                                                                            <option value="<?php print($Tag); ?>"><?php print($Label); ?></option>
                                                                        <?php endforeach; ?>
                                                                    </optgroup>
                                                                <?php endforeach; ?>
                                                            </select>&nbsp;&nbsp;
                                                        </div>
                                                    </div>
                                                    <?php print(form_error('TemplatePlainContent', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email Contents Section - END -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-action-container container-custom">

                                <a class="btn btn default" id="save-link"><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
                                <input type="hidden" name="Command" value="ProceedToBuilder" id="Command" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include_once(TEMPLATE_PATH . 'desktop/help/help_user_editemailtemplate.php'); ?>
    </div>
</div>



<script src="<?php InterfaceTemplateURL(false); ?>js/screens/admin/create_email_template.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>