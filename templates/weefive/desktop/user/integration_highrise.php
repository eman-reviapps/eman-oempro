<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php InterfaceLanguage('Screen', '1721', false, '', false, true, array('Highrise')); ?> </span>                    
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" >                                                              
                        <a href="<?php InterfaceAppURL(); ?>/user/integration/" class="btn default btn-transparen btn-sm" ><?php InterfaceLanguage('Screen', '1722', false); ?></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <?php if ($PageErrorMessage != ''): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php print($PageErrorMessage); ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($PageSuccessMessage != ''): ?>
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php print($PageSuccessMessage); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <form id="Integration" method="post" action="<?php InterfaceAppURL(); ?>/user/integration/highrise/">
                        <div class="form-group no-bg">
                            <p><?php InterfaceLanguage('Screen', '1789', false); ?></p>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('HighriseAccount') != '' ? 'has-error' : '')); ?>" id="form-row-HighriseAccount">
                                    <label class="col-md-3 control-label" for="HighriseAccount" style="width:180px;"><?php InterfaceLanguage('Screen', '0207', false); ?> : *</label>
                                    <div class="col-md-3">
                                        <input type="text" name="HighriseAccount" value="<?php echo set_value('HighriseAccount', $IntegrationConfig ? $IntegrationConfig->getAccount() : ''); ?>" id="HighriseAccount" class="form-control" />                                        
                                    </div>
                                    <div class="col-md-3">
                                        <span class="help-block">
                                            http://<span id="account-url-preview"></span>.highrisehq.com
                                        </span>
                                    </div>
                                    <div class="col-md-3">
                                        <?php print(form_error('HighriseAccount', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('HighriseAPIKey') != '' ? 'has-error' : '')); ?>" id="form-row-HighriseAPIKey">
                                    <label class="col-md-3 control-label" for="HighriseAPIKey" style="width:180px;"><?php InterfaceLanguage('Screen', '0208', false); ?> : *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="HighriseAPIKey" value="<?php echo set_value('HighriseAPIKey', $IntegrationConfig ? $IntegrationConfig->getAPIKey() : ''); ?>" id="HighriseAPIKey" class="form-control" />
                                        <span class="help-block">
                                            <p>You can find API token in your Highrise account. Just go to  My Info  and click "API Token" button.</p>
                                        </span>
                                    </div>
                                    <div class="col-md-2">
                                        <?php print(form_error('HighriseAPIKey', '<span class="help-block">', '</span>')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php print((form_error('IsEnabled') != '' ? 'has-error' : '')); ?>" id="form-row-IsEnabled">
                                    <label class="col-md-3 control-label" for="IsEnabled" style="width:180px;"><?php InterfaceLanguage('Screen', '1737', false); ?> : *</label>
                                    <div class="col-md-6">
                                        <select name="IsEnabled" id="IsEnabled" class="form-control">
                                            <option value="1" <?php echo set_select('IsEnabled', '1', $IntegrationConfig ? ($IntegrationConfig->isEnabled() ? true : false) : false); ?>><?php InterfaceLanguage('Screen', '0049', false); ?></option>
                                            <option value="0" <?php echo set_select('IsEnabled', '0', $IntegrationConfig ? ($IntegrationConfig->isEnabled() ? false : true) : false); ?>><?php InterfaceLanguage('Screen', '0050', false); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="Command" value="EditHighriseSettings" id="Command"/>
                        <br/>
                        <br/>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-2">
                                    <a class="btn default" targetform="Integration" id="ButtonIntegration"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1736', false, '', true); ?></strong></a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceTemplateURL(false); ?>js/screens/user/settings_integration.js" type="text/javascript" charset="utf-8"></script>
