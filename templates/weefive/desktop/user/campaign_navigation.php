<!--<span class="btn blue-hoki compose-btn btn-block"><?php InterfaceLanguage('Screen', '0835', false, '', true, false); ?></span>                    
<ul class="inbox-nav">
    <li<?php if ($SubSection == 'Overview'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/overview/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0836', false, '', false, true); ?></a></li>
    <li<?php if ($SubSection == 'Opens'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/opens"><?php InterfaceLanguage('Screen', '0837', false, '', false, true); ?></a></li>
    <li<?php if ($SubSection == 'Clicks'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/clicks"><?php InterfaceLanguage('Screen', '0838', false, '', false, true); ?></a></li>
    <li<?php if ($SubSection == 'Forwards'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/forwards"><?php InterfaceLanguage('Screen', '0839', false, '', false, true); ?></a></li>
    <li<?php if ($SubSection == 'BrowserViews'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/browserviews"><?php InterfaceLanguage('Screen', '0840', false, '', false, true); ?></a></li>
    <li<?php if ($SubSection == 'Unsubscriptions'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/viewstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/unsubscriptions"><?php InterfaceLanguage('Screen', '0841', false, '', false, true); ?></a></li>
    <?php
    InterfacePluginMenuHook('Campaign.Navigation.Reports', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
    );
    ?>
</ul>-->
<span class="btn blue-hoki compose-btn btn-block"><?php InterfaceLanguage('Screen', '0843', false, '', true, false); ?></span>  
<ul class="inbox-nav">
    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/<?php print strtolower($SubSection); ?>/csv"><?php InterfaceLanguage('Screen', '0844', false, '', false, true); ?></a></li>
    <li><a href="<?php InterfaceAppURL(); ?>/user/campaign/exportstatistics/<?php echo $CampaignInformation['CampaignID']; ?>/<?php print strtolower($SubSection); ?>/xml"><?php InterfaceLanguage('Screen', '0845', false, '', false, true); ?></a></li>
    <?php
    InterfacePluginMenuHook('Campaign.Navigation.ReportOptions', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
    );
    ?>
</ul>
<span class="btn blue-hoki compose-btn btn-block"><?php InterfaceLanguage('Screen', '0846', false, '', true, false); ?></span>
<ul class="inbox-nav">
    <li<?php if ($SubSection == 'Edit'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/edit/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0847', false, '', false, true); ?></a></li>
    <?php if (InterfacePrivilegeCheck('Campaign.Delete', $UserInformation)): ?>
        <li<?php if ($SubSection == 'Delete'): ?> class="active"<?php endif; ?>><a href="<?php InterfaceAppURL(); ?>/user/campaign/delete/<?php echo $CampaignInformation['CampaignID']; ?>"><?php InterfaceLanguage('Screen', '0848', false, '', false, true); ?></a></li>
        <?php endif; ?>
        <?php
        InterfacePluginMenuHook('Campaign.Navigation.Options', $SubSection, '<li><a href="_LINK_">_TITLE_</a></li>', '<li class="active"><a href="_LINK_">_TITLE_</a></li>', array(null, $CampaignInformation)
        );
        ?>
</ul>
