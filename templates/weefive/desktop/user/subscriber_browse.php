<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>


<div class="note note-info">
    <span>( <a href="<?php InterfaceAppURL(); ?>/user/subscribers/details/"><strong><?php echo $TotalSubscribersOnTheAccount ?></strong> of <strong><?php echo $UserInformation['GroupInformation']['LimitSubscribers']; ?></strong> </a> <?php InterfaceLanguage('Screen', '9272'); ?> )</span>
</div>
<div class="row">
    <div class="col-md-12">
        <!--light bg-inverse-->
        <div class="portlet " style="padding-bottom: 0px;margin-bottom: 0px" >
            <div class="portlet-title">
                <div class="caption">
                    <!--<i class="icon-docs font-purple-sharp"></i>-->
                    <?php if (count($ListInformation) > 0): ?>
                        <span class="caption-subject bold font-purple-sharp"><?php InterfaceLanguage('Screen', '1316', false, '', false, true, array($ListInformation['Name'])); ?></span>
                    <?php else: ?>
                        <span class="caption-subject bold font-purple-sharp"><?php InterfaceLanguage('Screen', '0568', false, '', false, true, array($ListInformation['Name'])); ?></span>
                    <?php endif; ?>
                        <!--<span class="small">( <a href="<?php InterfaceAppURL(); ?>/user/subscribers/details/"><strong><?php echo $TotalSubscribersOnTheAccount ?></strong> of <strong><?php echo $UserInformation['GroupInformation']['LimitSubscribers']; ?></strong> </a> <?php InterfaceLanguage('Screen', '9272'); ?> )</span>-->
                </div>
                <?php if (count($ListInformation) > 0): ?>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" >  
                            <a class="btn default btn-transparen btn-sm" href="<?php echo InterfaceAppUrl(); ?>/user/list/statistics/<?php echo $ListInformation['ListID']; ?>"><strong><?php InterfaceLanguage('Screen', '1134', false, '', false); ?></strong></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <form id="subscribers-export-form" action="<?php InterfaceAppURL(); ?>/user/subscribers/export/0/true" method="post">
            <input type="hidden" name="Command" value="ExportSubscribers" id="Command">
            <input type="hidden" name="ExportRules" value="" id="ExportRules">
            <input type="hidden" name="ExportOperator" value="" id="ExportOperator">
            <input type="hidden" name="ExportListID" value="<?php echo (count($ListInformation) < 1 ? '' : $ListInformation['ListID']); ?>" id="ExportListID">
        </form>
        <div class="portlet light portlet-transparent">
            <?php if ($ListInformation['IsAutomationList'] == 'No') { ?>
                <div class="portlet-title" style="margin-bottom: 0">
                    <div class="col-md-412" style="padding: 0">
                        <a href="#" class="btn default" id="copy_to_another_list" ><?php InterfaceLanguage('Screen', '9289'); ?></a> 
                        <a href="#" class="btn default" id="move_to_another_list" ><?php InterfaceLanguage('Screen', '9250'); ?></a> 
                        <a href="#" class="btn default" id="move_to_supprression"><?php InterfaceLanguage('Screen', '9243'); ?></a>
                        <a href="#" class="btn default" id="unsubscribe"><?php InterfaceLanguage('Screen', '1309'); ?></a>
                    </div>
                </div>
            <?php } ?>
            <div class="portlet-body">
                <div class="clearfix"></div>
                <div id="page-shadow">
                    <div id="page">
                        <div class="page-overlay"><div class="page-overlay-label"><?php InterfaceLanguage('Screen', '1429'); ?></div></div>

                        <div class="portlet box purple-soft">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php InterfaceLanguage('Screen', '9200'); ?> </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="form-row no-bg clearfix" id="form-row-RuleBoard">
                                    <div class="rule-board" style="float:left;width:100%;">
                                        <div class="main-action-with-menu filter-operator-menu" style="float:left;">
                                            <a href="#" class="main-action"><?php InterfaceLanguage('Screen', '1452'); ?> 
                                                <span id="filter-operator"><?php InterfaceLanguage('Screen', '1299'); ?></span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="main-action-menu" style="min-width:150px;">
                                                <li operator="and"><a href="#"><?php InterfaceLanguage('Screen', '1299'); ?></a></li>
                                                <li operator="or"><a href="#"><?php InterfaceLanguage('Screen', '1300'); ?></a></li>
                                            </ul>
                                        </div>
                                        <div class="main-action-with-menu add-new-rule-menu"  style="width:200px">
                                            <a href="#" class="main-action">
                                                <span><?php InterfaceLanguage('Screen', '1304'); ?></span>
                                                <i class="fa fa-angle-down pull-right"></i>
                                            </a>
                                            <ul class="main-action-menu">
                                                <li class="label"><?php InterfaceLanguage('Screen', '1398'); ?>:</li>
                                                <?php foreach ($DefaultFields as $Each): ?>
                                                    <li class="default-fields"><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" validationmethod="<?php echo $Each['ValidationMethod']; ?>" values="<?php echo $Each['Values']; ?>"><?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?></a></li>
                                                <?php endforeach; ?>
                                                <?php foreach ($PluginFields as $Each): ?>
                                                    <li class="default-fields"><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" validationmethod="<?php echo $Each['ValidationMethod']; ?>" values="<?php echo $Each['Values']; ?>"><?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?></a></li>
                                                <?php endforeach; ?>
                                                <li class="activity label"><?php InterfaceLanguage('Screen', '1401'); ?>:</li>
                                                <?php foreach ($ActivityFields as $Each): ?>
                                                    <li class="activity"><a href="#" customfieldid="<?php echo $Each['CustomFieldID']; ?>" validationmethod=""><?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="main-action-with-menu add-segment-rules-menu">
                                            <a href="#" class="main-action">
                                                <?php InterfaceLanguage('Screen', '1451'); ?>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="main-action-menu"></ul>
                                        </div>
                                        <a href="#" id="remove-segment-rules-link" style="display:none;"><?php InterfaceLanguage('Screen', '1453'); ?></a>

                                        <div class="rules">
                                            <ul id="specific-list-rule" style="margin:0px;">
                                                <li class="rule" style="width:100%">
                                                    <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">
                                                        <label class="control-label" style="width: 50px"><?php InterfaceLanguage('Screen', '1444'); ?> : </label>
                                                        <div class="col-md-6">
                                                            <select name="SearchList" id="SearchList" class="form-control">
                                                                <?php foreach ($Lists as $EachList): ?>
                                                                    <option value="<?php echo $EachList['ListID']; ?>"><?php echo $EachList['Name']; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul id="rule-list"></ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-row no-bg form-action-container clearfix">
                                    <a class="btn default"  id="apply-filter-button" href="#"><strong><?php InterfaceLanguage('Screen', '1455', false, '', false); ?></strong></a>
                                    <a href="#" id="clear-board-link" class="btn default" ><?php InterfaceLanguage('Screen', '1400'); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">

                        </div>
                        <div id="subscribers_div" style="margin-top: 50px">

                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<script>

    $('.subscriber-row td:not(.mt-checkbox-td)').live('click', function () {
//        alert('Column:' + $('.dataTable tbody tr td').eq($(this).index()).html().trim());
//        alert($(this).index())
    if ($(this).index() != 0)
    {
    segmentId = <?php echo isset($SegmentInformation['SegmentID']) ? $SegmentInformation['SegmentID'] : 0 ?>;
    url = APP_URL + 'user/subscriber/edit/' + $('#SearchList').val() + '/' + $(this).parent().attr('subscriberid') + '/' + segmentId;
    window.location = url;
    }
    });</script>
<script type="text/javascript" charset="utf-8">
    var listId = <?php echo (count($ListInformation) < 1 ? 0 : $ListInformation['ListID']) ?>;
    var segmentId = <?php echo (count($SegmentInformation) < 1 ? 0 : $SegmentInformation['SegmentID']) ?>;
    var lang = {
    '1406' : '<?php InterfaceLanguage('Screen', '1406'); ?>',
            '1407' : '<?php InterfaceLanguage('Screen', '1407'); ?>',
            '1408' : '<?php InterfaceLanguage('Screen', '1408'); ?>',
            '1409' : '<?php InterfaceLanguage('Screen', '1409'); ?>',
            '1410' : '<?php InterfaceLanguage('Screen', '1410'); ?>',
            '1411' : '<?php InterfaceLanguage('Screen', '1411'); ?>',
            '1412' : '<?php InterfaceLanguage('Screen', '1412'); ?>',
            '1413' : '<?php InterfaceLanguage('Screen', '1413'); ?>',
            '1414' : '<?php InterfaceLanguage('Screen', '1414'); ?>',
            '1415' : '<?php InterfaceLanguage('Screen', '1415'); ?>',
            '1416' : '<?php InterfaceLanguage('Screen', '1416'); ?>',
            '1448' : '<?php InterfaceLanguage('Screen', '1448'); ?>',
            '1454' : '<?php InterfaceLanguage('Screen', '1454'); ?>',
            '1457' : '<?php InterfaceLanguage('Screen', '1457'); ?>',
            '1458' : '<?php InterfaceLanguage('Screen', '1458'); ?>'
    };
    var segments = {};
<?php foreach ($Lists as $EachList): ?>
        segments[<?php echo $EachList['ListID']; ?>] = [];
    <?php foreach ($EachList['Segments'] as $EachSegment): ?>
            segments[<?php echo $EachList['ListID']; ?>].push({id:<?php echo $EachSegment['SegmentID'] ?>, name:'<?php echo htmlspecialchars($EachSegment['SegmentName'], ENT_QUOTES); ?>', rules:'<?php echo addslashes($EachSegment['SegmentRules']); ?>', operator:'<?php echo $EachSegment['SegmentOperator']; ?>'});
    <?php endforeach; ?>
<?php endforeach; ?>
    var customFields = {};
<?php foreach ($Lists as $EachList): ?>
        customFields[<?php echo $EachList['ListID']; ?>] = [];
    <?php foreach ($EachList['CustomFields'] as $EachCustomField): ?>
            customFields[<?php echo $EachList['ListID']; ?>].push({id:<?php echo $EachCustomField['CustomFieldID'] ?>, name:'<?php echo htmlspecialchars($EachCustomField['FieldName'], ENT_QUOTES); ?>', fieldtype:'<?php echo $EachCustomField['FieldType']; ?>', validationmethod:'<?php echo $EachCustomField['ValidationMethod']; ?>', fieldoptions:'<?php echo addslashes(htmlspecialchars($EachCustomField['FieldOptions'])); ?>'});
    <?php endforeach; ?>
<?php endforeach; ?>
    var campaigns = [
<?php for ($i = 0; $i < count($Campaigns); $i++): ?>
        {'CampaignID':<?php echo $Campaigns[$i]['CampaignID']; ?>, 'CampaignName':'<?php echo htmlspecialchars($Campaigns[$i]['CampaignName'], ENT_QUOTES); ?>'}<?php if ($i < count($Campaigns) - 1): ?>,<?php endif; ?>
<?php endfor; ?>
    ];
    var fieldLabels = {
<?php foreach ($DefaultFields as $Each): ?>
        '<?php echo $Each['CustomFieldID']; ?>':{'fieldLabel':'<?php InterfaceLanguage('Screen', '0665', false, $Each['CustomFieldID']); ?>', 'validationMethod':'<?php echo $Each['ValidationMethod']; ?>', 'values':''},
<?php endforeach; ?>
<?php for ($i = 0; $i < count($ActivityFields); $i++): ?>
        '<?php echo $ActivityFields[$i]['CustomFieldID']; ?>':{'fieldLabel':'<?php InterfaceLanguage('Screen', '0665', false, $ActivityFields[$i]['CustomFieldID']); ?>', 'validationMethod':'', 'fieldType':'', 'values':''}<?php if ($i < count($ActivityFields) - 1): ?>,<?php endif; ?>
<?php endfor; ?>
    };
<?php foreach ($Lists as $EachList): ?>
    <?php foreach ($EachList['CustomFields'] as $EachCustomField): ?>
            fieldLabels[<?php echo $EachCustomField['CustomFieldID'] ?>] = {'fieldLabel':'<?php echo htmlspecialchars($EachCustomField['FieldName'], ENT_QUOTES); ?>', 'validationMethod':'<?php echo $EachCustomField['ValidationMethod']; ?>', 'fieldType':'<?php echo $EachCustomField['FieldType']; ?>', 'values':'<?php
        if ($EachCustomField['FieldType'] == 'Multiple choice' || $EachCustomField['FieldType'] == 'Drop down' || $EachCustomField['FieldType'] == 'Checkboxes') {
            echo addslashes(htmlspecialchars($EachCustomField['FieldOptions']));
        }
        ?>'};
    <?php endforeach; ?>
<?php endforeach; ?>
    var ruleOperators = {
<?php for ($i = 0; $i < count($RuleOperators); $i++): ?>
        '<?php echo $RuleOperators[$i]['Name'] ?>' : [
    <?php for ($j = 0; $j < count($RuleOperators[$i]['Operators']); $j++): ?>
            {
            'Label':'<?php InterfaceLanguage('Screen', '1303', false, $RuleOperators[$i]['Operators'][$j]); ?>',
                    'Value':'<?php echo $RuleOperators[$i]['Operators'][$j]; ?>'
            }<?php
        if ($j < count($RuleOperators[$i]['Operators']) - 1) {
            echo ',';
        }
        ?>
    <?php endfor; ?>
        ]<?php
    if ($i < count($RuleOperators) - 1) {
        echo ',';
    }
    ?>
<?php endfor; ?>
    };
    var activityFields = ['<?php echo implode("','", $ActivityFieldIDs); ?>'];
    var defaultFields = [<?php
for ($i = 0; $i < count($DefaultFields); $i++) {
    echo '"' . $DefaultFields[$i]['CustomFieldID'] . '"';
    if ($i < count($DefaultFields) - 1) {
        echo ',';
    }
}
?>];
    var pluginFields = [<?php
for ($i = 0; $i < count($PluginFields); $i++) {
    echo '"' . $PluginFields[$i]['CustomFieldID'] . '"';
    if ($i < count($PluginFields) - 1) {
        echo ',';
    }
}
?>];
    var statusFields = ['OptInPending', 'Subscribed', 'OptOutPending', 'Unsubscribed'];
    var languageObject = {
    '1335' : "<?php InterfaceLanguage('Screen', '1335'); ?>",
            '1750' : "<?php InterfaceLanguage('Screen', '1750'); ?>"
    };</script>
<script src="<?php InterfaceTemplateURL(); ?>js/screens/user/subscribers.js" type="text/javascript" charset="utf-8"></script>		


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>