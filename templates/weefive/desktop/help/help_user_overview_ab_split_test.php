<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9234', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            Split Test is a method for optimizing your email marketing campaigns. 
            Through Split Test, you can test two versions of your email message among a small group of subscribers, 
            determine the better performer, and then use the superior message for the remainder of your subscribers. 
            You can then use the superior message as the foundation for your next campaign, and continue to 
            test additional elements – thus refining and enhancing your email campaigns one test at a time.
        </p>
        <ul>
            <li>Click on the “Campaigns”</li>
            <li>Click on the “Create New Campaign”</li>
            <li>On the first page where you need to define the campaign settings you can enable A/B Split testing.</li>
            <li>After you enable the A/B split testing set the parameters as described below.</li>
        </ul>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/ab_split/split_0.png" />
        </p>
        <p>
            Test Size<br/>
            You can define the test size using the slider and define the how many percentage of the total 
            campaign users we should test the two versions of our email. For example if we select the 20% of total 
            campaign users then the test version will be sent in equal groups of the 20% users.<br/>
            <br/>
            Test Duration<br/>
            You can define the test duration so that you can record click through or email open data for the split 
            test within that time frame. You can set this duration to few hours or few days.<br/>
            <br/>
            Winner is<br/>
            You can choose whether the winner can be based on the email open ratio or based on the click through 
            ratios for both the version. Both the versions will be compared for the winner is parameter.<br/>
        </p>
    </div>
</div>