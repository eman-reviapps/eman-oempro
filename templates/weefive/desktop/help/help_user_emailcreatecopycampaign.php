<div class="note note-info">
    <h4 class="block bold">Copy Email Content</h4>
    <p>You can create your new campaign content from a previously created campaign content. Just select the one of your previous campaigns from the list and <?php print(PRODUCT_NAME); ?> will fill-in the fields for you in following steps.</p>
</div>