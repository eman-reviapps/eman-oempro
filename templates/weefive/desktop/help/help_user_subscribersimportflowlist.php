<div class="note note-info">
    <h4 class="block bold">Add Subscribers To Your List</h4>
    <p>Select one of the import methods on the left to add subscribers to your list.</p>
    <p>You can import by copying and pasting, uploading from your computer or directly from your MySQL database.</p>
</div>