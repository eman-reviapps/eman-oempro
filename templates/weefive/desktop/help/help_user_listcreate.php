<div class="note note-info">
    <h4 class="block bold">What's Subscriber List?</h4>
    <p>Briefly, subscriber list is the container of your recipient email addresses, auto responders, custom fields, etc.</p>
    <p>You can create more than one subscriber list to store different group of recipient email addresses.</p>
</div>