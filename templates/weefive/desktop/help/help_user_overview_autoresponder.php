<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9228', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p>
            Auto-responders play a very important role in Email Marketing. You can 
            send emails or updates to your subscribers automatically without the need 
            to login to the Flying List account. You can use autoresponders to send a 
            welcome email or a happy birthday email. You could even send an email when
            someone clicks on a particular link, forwards your newsletter.
        </p>
        <ul>
            <li>To create and manage the auto responders go to “Lists”</li>
            <li>Select the mailing list you wish to create auto-responder for</li>
            <li>Now click on the “Auto Responder” link from the List Options menu.</li>
            <li>You should see the auto responder browser – which will list any existing auto responders within the system and also allow you to create new ones.</li>
            <li>Click on the “Create new auto-responder” link</li>
        </ul>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_0.png" />
        </p>
        <p>
            This will take you to the auto responder creation screen – where you will
            need to define the auto responder name, trigger event and the schedule to 
            send the auto responder. You will also have to create your auto responder 
            email which will be sent to the subscriber.
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_1.png" />
        </p>
        <p>
            Auto Responder Fields Explained<br/>
            <br/>
            Name<br/>
            Name of your auto responder<br/>
            <br/>
            Triggers on<br/>
            Select the event that will trigger the auto responder. There are five triggers available<br/>
            <br/>
            Subscription<br/>
            If this trigger is selected then when the subscription trigger happens [a subscriber subscribes] that time the Auto Responder will be sent. This means the auto responder will be sent to the new subscribers for that particular list.<br/>
            <br/>
            Link click<br/>
            If this trigger is selected then in the event when the existing subscriber of the list clicks a link within the email campaign body – then an auto responder will be sent to the subscriber.<br/>
            <br/>
            Campaign Open<br/>
            This trigger will fire an auto responder when the subscriber opens the campaign email.<br/>
            <br/>
            Forward to friend<br/>
            This trigger will fire an auto responder when the subscriber clicks on the forward to friend link and actually forwards the email to someone<br/>
            <br/>
            Date Information<br/>
            This type of trigger will only work based on the date fields, such as subscription date, birth date etc.<br/>
            Send<br/>
            You can set the auto responder to be sent based on a specified time frame.<br/>
        </p>

        <ul>
            <li>Immediately</li>
            <li>Seconds later</li>
            <li>Minutes later</li>
            <li>Hours later</li>
            <li>Days later</li>
            <li>Weeks later</li>
            <li>Months later</li>
        </ul>
        <p>
            Click “Create Auto Responder & Edit Email” button to proceed to the next screen.<br/>
            On the next screen you will have to select one of the methods for creating the auto responder email content.
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_2.png" />
        </p>
        <p>
            Method to create auto responder email<br/>
            <br/>
            From Scratch<br/>
            This method will allow you to use your own custom html code for creating auto responder email<br/>
            <br/>
            Template Gallery<br/>
            Flying List has several pre-configured templates available within the system – you can select the pre-built template to be used as your auto responder.<br/>
            <br/>
            You can select this method if you wish to use the built in templates for your auto responders.<br/>
            <br/>
            Fetch URL<br/>
            This can be used to fetch the auto responder content from a third party website or your own website.<br/>
            <br/>
            For this user guide we are going to use the first method “From scratch”.<br/>
        </p>
        <p class="bold">
            Step 1 Sender Information [Settings]
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_3.png" />
        </p>
        <p>
            Name<br/>
            Your subscribers will see this name when they receive your auto responder.<br/>
            <br/>
            Email Address<br/>
            Your subscribers will see this email address when they receive your auto responder.<br/>
            <br/>
            Reply to Information<br/>
            By default this option is checked – if you wish to have the different reply to information you can un check this option and add the new reply to information to your auto responder.<br/>
        </p>
        <p class="bold">
            Step 2 Content[Settings]
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_4.png" />
        </p>
        <p>
            Embed images to email content<br/>
            If this option is checked every image within your content will be embedded to your email content, although this isn’t a good idea for the email content which can have large number of emails.<br/>
            <br/>
            Content Type<br/>
            Select the preferred content type whether you wish to send an HTML or text based email.<br/>
            <br/>
            Campaign Content<br/>
            Campaign content has 3 major elements – subject, actual email content and the attachments. You can add your preferred subject link for your auto responder and also can add attachments along with the email content.<br/>
        </p>
        <p>
            Once all the above options are populated hit the “Next” button to proceed to the preview stage of our
            auto responder email. You can use the preview screen to test your auto responder design by sending 
            a preview email or viewing it online on the browser.
        </p>
        <p>
            When you are satisfied with the preview email click on the “Next” button and save your auto-responder.
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/autoresponder/create_auto_5.png" />
        </p>
    </div>
</div>