<div class="note note-info">
    <h4 class="block bold">Import Process Completed</h4>
    <p>Your data has been successfully imported into your subscriber list.</p>
</div>