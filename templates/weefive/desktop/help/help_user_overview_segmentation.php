<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9231', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            This topic will cover everything related to the data segmentation from your mailing lists. 
            Learn how relevancy drives response and the various different ways you can split up your email lists 
            to ensure you hit the target more often.
        </p>
        <p>
            Segmentation of your address database is widely regarded as a proven way to improve responses and results.
        </p>
        <ul>
            <li>To create and manage segments go to <span class="bold">“Lists”</span></li>
            <li>Click on the mailing list you wish to create segments for</li>
            <li>Now click on the <span class="bold">“Segments”</span> link under the List Options</li>
        </ul>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/segmentation/1.jpg" />
        </p>
        <ul>
            <li>After you click <span class="bold">“Create Segment”</span> link you should see the right side.</li>
            <li>Enter the name of your segment [example: Subscribers having yahoo addresses]</li>
            <li>Select whether the all rules you create should match or any one of them</li>
            <li>The segment rules are the most important part. You can create a rule based on </li>
        </ul>
        <p>
            The segment rules are the most important part. You can create a rule based on subscribers information 
            or based on subscribers activity such as opening emails, clicking on the links etc.
        </p>
        <p style="background: yellow">
            You can add multiple rules to the segment such as the person should have yahoo.com email id and
            should be between 18-25 age group.
        </p>
        <p>
            For this user manual, we will use the rule for yahoo.com users. Once we populate the necessary 
            fields and also add the rule wherein the subscriber's email id contains yahoo.com we have
            to click on the “create segment” button. This will create a segment with the options we selected.
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/segmentation/2.jpg" />
        </p>
        <p>
            The screenshot above shows us our newly created segment which consists 32 subscribers having yahoo.com email ids.
        </p>
    </div>
</div>