<div class="note note-info">
    <h4 class="block bold">Define Your Email Settings</h4>
    <p>In this section, you can set the name and email address you wish to send email from and reply settings.</p>
    <p>You can set the reply settings same as the sender email address or define a different one by unchecking the checkbox on the bottom left side.</p>
</div>