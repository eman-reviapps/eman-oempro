<div class="note note-info">
    <h4 class="block bold">What's Global Suppression List?</h4>
    <p>In other words, Global Suppression List means 'Do Not Send List'. Your email campaigns will not be sent to email addresses which exist in your global suppression list.</p>
    <p>The difference between global and local suppression list is, global suppression list in affect for all your subscriber lists. Local suppression lists are in affect for the owner subscriber list only.</p>
    <p>You can add email addresses to global suppression list to avoid sending any email campaigns in the future even if you create a brand new subscriber list.</p>
</div>