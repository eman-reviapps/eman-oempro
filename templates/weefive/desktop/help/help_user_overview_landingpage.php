<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9229', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            Successful email marketing is not only sending emails that achieve high open rates. 
            Success email marketing consists of email content with high open and click rates and a landing page 
            that converts well, 
        </p>
        <p class="bold">
            In Flying List make Unlimited Landing pages with free hosting 
        </p>
        <p>
            Landing Page Builder will allow you and your users to build awesome landing pages without coding any single
            HTML line. By dragging and dropping content blocks, you will be able to create amazing landing pages
            that are also compatible with mobile devices (responsive).
        </p>
        <p class="bold">
            Creating landing pages
        </p>
        <p>
            Landing pages can be created and managed very easy and simply. After login, you can click 
            “Create Landing Pages” link inside the menu. 
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/1.png" />
        </p>
        <p>
            You will be redirected to the landing page list. If this is the first time you visit this page or if 
            you haven’t created any landing pages so far, you will see an empty list on the screen:
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/2.png" />
        </p>
        <p>
            Select one of them then click “Create a Landing Page” button to create your first page. You will be redirected 
            to the landing page template gallery:
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/3.png" />
        </p>
        <p>
            After selecting the landing page template, simply click “Continue to builder” button on the top right corner.
            You will be redirected to the landing page builder:
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/4.jpg" />
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/5.png" />
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/6.png" />
        </p>
        <p class="bold">
            Setting custom domains for landing pages
        </p>
        <p>
            Once you create your first landing page and return back to the page list (by clicking the Close button on 
            the builder), you will see your page on the list. Below each page you have created, you will see two links:
        </p>
        <ul>
            <li>Custom URL Settings</li>
            <li>Visit Page</li>
        </ul>
        <p>
            If you click “Visit Page” link, you will be redirected to your landing page on a new window. 
            This is useful for previewing your landing page and for sharing the page URL.
        </p>
        <p>
            If you click “Custom URL Settings” link, you will be redirected to the settings page where you can setup
            a custom URL for your landing page. 
        </p>
        <p class="bold">
            In Flying List make Unlimited Landing pages with free hosting 
        </p>
        <p>
            For example, instead of accessing your landing page through the default URL;<br/>
            http://Flyinglist.com/......./....../....../........../.......<br/>
            you can access through;<br/>
            http://yourdomain.com/buynow/<br/>
            On the custom URL settings page, simply enter the URL you want to set:<br/>
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/7.jpg" />
        </p>
        <p>
            Hit the “Save” button. Now, you will be shown the required .htaccess directive to tell your web server
            redirect users to the correct landing page URL:
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/8.png" />
        </p>
        <p class="bold">
            Using landing pages in emails
        </p>
        <p>
            When creating your emails, you can easily insert links to your landing pages.
            Simply click  the Link button, choose your Landing Page and insert this link in your email:
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/landing/9.png" />
        </p>
    </div>
</div>