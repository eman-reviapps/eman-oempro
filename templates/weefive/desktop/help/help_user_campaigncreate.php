<div class="note note-info">
    <h4 class="block bold">New Email Campaign</h4>
    <p>Set your settings for your new campaign on the left. You can create a regular email campaign or a/b split test campaign.</p>
    <p>You will define your contents and scheduling for your email campaign in next steps.</p>
</div>