<h3>What's Payment Reports?</h3>
<p>If you are using <?php print(PRODUCT_NAME); ?> to run ESP (Email Service Provider) business for your customers, you can track your users activity and generate service fee receipts for them.</p>

<h3>Where is Payment Preferences?</h3>
<p>For each user group, you can set different payment and pricing preferences. Also, you can set the overall payment preferences in &quot;Settings&quot; section.</p>

<h3>Payment Gateway Integration</h3>
<p><?php print(PRODUCT_NAME); ?> comes with PayPal integration however it can easily be integrated with any payment method and gateway with the help of its powerful, full featured API.</p>