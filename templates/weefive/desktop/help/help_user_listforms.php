<div class="note note-info">
    <h4 class="block bold">Website Forms</h4>
    <p><?php print(PRODUCT_NAME); ?> generates subscription and unsubscription forms for you.</p>
    <p>Select the form type (subscription/unsubscription) by clicking to corresponding tab on the left and generate your form based on your needs.</p>
    <p>Then copy the generated HTML code and paste it to your website page(s). You can customize the HTML code based on your needs.</p>
</div>