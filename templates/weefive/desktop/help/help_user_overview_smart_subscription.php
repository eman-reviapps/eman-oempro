<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9233', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            Subscription / Un-Subscription forms can be integrated with your website, microsite or landing pages. To create new subscription / unsubscription forms for your mailing list(s) follow the steps below.
        </p>
        <ul style="list-style: decimal">
            <li>Go to Lists</li>
            <li>This will show you the existing lists available within your system.</li>
            <li>Next click on the mailing list you want to integrate within your website</li>
            <li>This will bring up the list overview page</li>
            <li>Click on the “Create Subscription/Unsubscription Form” button found on the upper right corner of the page.</li>
            <li>This will take you to a wizard where you can create the subscription / unsubscription forms</li>
        </ul>
        <br/>
        <p class="bold">
            Creating a subscription Form
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/subsc_form/create_subs_form.png" />
        </p>
        <p>
            Layout & Preview<br/>
            This will show you how your form will look like – there are three built in visual styles which can be 
            used to style your form layout.<br/>
            <br/>
            Options<br/>
            The options tab will allow you to select more mailing lists so that when a subscriber 
            subscribes using this form they will also be subscribed to selected lists under options.<br/>
            <br/>
            You can also choose to include custom fields associated with the mailing list you are generating
            the subscription form.<br/>
            <br/>
            HTML Code<br/>
            This tab will provide you the generated HTML code which can be easily integrated within your website. 
            Just copy and paste the html code within your website and customize the html code as you want but you 
            must not change the action of the form or the name of form elements.<br/>
        </p>
        <p class="bold">
            Creating an unsubscription Form
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/subsc_form/un_subs_form.png" />
        </p>
        <p>
            Click on the “Unsubscription Form” tab and you should see the html code, copy and paste the html code 
            within your website and customize the html code as you want but you must not change the action of the 
            form or the name of form elements.
        </p>
    </div>
</div>