<div class="note note-info">
    <h4 class="block bold">Quick Subscribe Removal</h4>
    <p>You can use this tool to remove many subscribers at once. Select the removal criteria on the left and click the button. It may take some time to complete the mass removal process.</p>
</div>