<h3>Upgrade Your Software</h3>
<p>If there's a new version available, you can upgrade your current version by entering FTP details on the left.</p>
<p><?php print(PRODUCT_NAME); ?> will connect to your server via FTP protocol to upgrade files and database. If FTP connection fails, you will receive a notification message. In such a case, you can try the manual upgrade.</p>

<h3>IMPORTANT!</h3>
<p>In case of a problem, <span style="background-color:#FF6;">don't forget to take backup of your Oempro directory and database</span> before upgrading. For more information, please take a look at <a href="http://octeth.com/docs/oempro_v40x/backup_instructions/" target="_blank">our documentation</a>.</p>