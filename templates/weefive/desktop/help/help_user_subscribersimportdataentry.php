<div class="note note-info">
    <h4 class="block bold">Data Entry</h4>
    <p>Copy and paste your CSV data to the field on the left. Be sure that your data is in CSV (comma separated value) format. Then select your CSV properties such as the character used to separate fields and enclose them.</p>
    <p><?php print(PRODUCT_NAME); ?> prepare your CSV data for import process and you will be asked to map fields of the CSV data with your subscriber list fields in the next step.</p>
</div>