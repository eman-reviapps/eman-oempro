<div class="note note-info">
    <h4 class="block bold">What's Custom Field Preset?</h4>
    <p>We have defined a list of custom fields for different purposes for you. You can select one of them on the left and add to your subscriber list with a single click.</p>
</div>