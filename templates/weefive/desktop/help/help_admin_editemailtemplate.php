<h3>What's Email Template?</h3>
<p>Email templates let you to create and set an email design layout. In this way, your system users will only be able to edit editable regions and your email design layout will not be affected.</p>
<h3>Advanced Email Template Editor</h3>
<p><?php print(PRODUCT_NAME); ?> includes powerful, easy-to-use email template editor. Just create your email template on a third party HTML editor and then copy it to <?php print(PRODUCT_NAME); ?>. You will then define editable regions, repeating blocks, etc.</p>
