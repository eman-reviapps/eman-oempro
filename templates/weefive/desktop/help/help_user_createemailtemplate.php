<div class="note note-info">
    <h4 class="block bold">What's Email Template?</h4>
    <p>Email templates let you to create and set an email design layout. In this way, your system users will only be able to edit editable regions and your email design layout will not be affected.</p>
    <h4 class="block bold">Advanced Email Template Editor</h4>
    <p><?php print(PRODUCT_NAME); ?> includes powerful, easy-to-use email template editor. Just create your email template on a third party HTML editor and then copy it to <?php print(PRODUCT_NAME); ?>. You will then define editable regions, repeating blocks, etc.</p>
</div>