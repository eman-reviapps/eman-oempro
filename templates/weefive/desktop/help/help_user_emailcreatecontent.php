<div class="note note-info">
    <h4 class="block bold">Set Your Email Content</h4>
    <p>In this section, you need to set your email content, subject and attachments of your email campaign.</p>
    <p>You can personalize your email content by selecting one of the available personalization tags from the drop list which will appear when you edit the email subject or plain email content field.</p>
    <p>To personalize your HTML email content, simply click percentage icon (%) on the toolbar of HTML editor.</p>
</div>