<div class="note note-info">
    <h4 class="block bold">Import From MySQL Database</h4>
    <p><?php print(PRODUCT_NAME); ?> can connect to another MySQL database and get data directly from your own database. Simply set MySQL database connection properties on the left.</p>
    <p>This process requires basic MySQL knowledge to write the required SQL query to fetch your records.</p>
</div>