<div class="note note-info">
    <h4 class="block bold">Field Mapping</h4>
    <p>In the previous step, you have provided the import data. Now, please map your data fields with your subscriber list fields on the left.</p>
    <p>If you don't wish to import a specific field in your import data, simply select &quot;Ignore this field&quot; option from the list corresponding to that field.</p>
    <p><strong>Important:</strong> You need to map at least the email address field.</p>
</div>