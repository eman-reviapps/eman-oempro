<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9235', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            Are you not a graphic designer?Don't have Photoshop?  No problem, you can edit your images right in 
            flying list photo editor and use it in your email. Enhance your images by adding effects, stickers, 
            or even by adding a message with beautiful fonts over your photo.
        </p>
        <p class="bold">
            Start use Flying List photo editor
        </p>
        <p>
            After your login click on “ campaigns “ button on the main menu
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/photo_editor/1.png" />
        </p>
        <p>
            Then you can found “ photo editor “ button on top right
        </p>
        <p>
            <img style="width:60%"  src="<?php InterfaceTemplateURL(); ?>images/help/photo_editor/2.png" />
        </p>
        <p>
            If you forget edit you photo before starting “ Create New Campaign “ , 
            you can found another button in our Drag & drop Email editor on top left side 
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/photo_editor/3.png" />
        </p>
        <p>
            Open Flying list photo editor , upload your photo and enjoy
        </p>
        <p>
            <img style="width:60%" src="<?php InterfaceTemplateURL(); ?>images/help/photo_editor/4.png" />
        </p>
        <p class="bold">
            Features
        </p>
        <p>
            <span class="bold">Image manipulation</span> - Easily crop, rotate, resize and round image corners with pixies image manipulation tools.
        </p>
        <p>
            <span class="bold">Add Text</span> – Advanced yet easy to use text system with over 600 fonts and dozens of styles ranging from color to outline.
        </p>
        <p>
            <span class="bold">Free Drawing</span> – Fully-featured free drawing system with a number of different brushes and options.
        </p>
        <p>
            <span class="bold">Filters</span> – Over a dozen image filters with more to come in the future updates.
        </p>
        <p>
            <span class="bold">Layers</span> – With flying list photo editor fully featured layers system you can use drag and drop to move them around, change z-index and more.
        </p>
        <p>
            <span class="bold">History</span> – Every operation performed will be remembered in the history panel so can jump back or forward in time easily.
        </p>
    </div>
</div>