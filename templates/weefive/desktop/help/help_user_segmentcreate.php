<div class="note note-info">
    <h4 class="block bold">Subscriber List Segments</h4>
    <p>Segments are smart filters which create a sub-list based on your rules. You can use these segments to target your email campaigns later.</p>
    <p><?php print(PRODUCT_NAME); ?> provides you a user-friendly rule board to set your segment rules easily within minutes.</p>
    <p>You can set your segment rules based on subscriber information (including custom fields) or subscriber activity (campaign opens, link clicks, etc.).</p>
</div>