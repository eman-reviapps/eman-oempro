<div class="note note-info">
    <h4 class="block bold">Copy Custom Fields</h4>
    <p>Select the source and target subscriber lists on the left and let <?php print(PRODUCT_NAME); ?> to copy your custom fields to the target subscriber list.</p>
    <p><?php print(PRODUCT_NAME); ?> will create new custom fields in the target list with same preferences in the selected source subscriber list.</p>
</div>