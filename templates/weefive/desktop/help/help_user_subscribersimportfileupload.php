<div class="note note-info">
    <h4 class="block bold">CSV File Upload</h4>
    <p>If your CSV data file is a big sized one, you can upload it from your computer by using this section.</p>
    <p>Please be sure that your file is in CSV format which means fields are separated with a comma, semicolon or tab character.</p>
    <p>In the next step, we will ask you to map your fields with subscriber fields.</p>
</div>