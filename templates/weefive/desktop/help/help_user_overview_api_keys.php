<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9236', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            An application programming interface key (API key)
        </p>
        <p>
            All user accounts in Flying List can have multiple and Unlimited API keys. With these new API keys, 
            it is much simpler to authenticate and run API commands. 
        </p>
        <p>
            How can you start to make First API Key ?
        </p>
        <p>
            After login, you can click “Setting” Button in the top right.
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/api_keys/1.png" />
        </p>
        <p>
            From menu, you can select API Key
        </p>
        <p>
            Now you can see the Create new API Key button ….. Click and enjoy
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/api_keys/2.png" />
        </p>
    </div>
</div>