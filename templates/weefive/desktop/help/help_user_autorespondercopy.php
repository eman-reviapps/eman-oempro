<div class="note note-info">
    <h4 class="block bold">Auto Responders</h4>
    <p>Auto responders let you to send sequential follow-up emails after a specific event such as subscription or link click. You can create unlimited sequential auto responders for each event in each subscriber list.</p>
</div>