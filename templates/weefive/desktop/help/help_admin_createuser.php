<h3>What's User Account?</h3>
<p>Create user accounts have full access to their email campaigns, subscriber lists, auto responders, campaign fields, etc. They can send email campaigns, import email address, etc. You can limit your users and set privileges for them in user groups section.</p>
<h3>User Privileges and Limits</h3>
<p>Define what your users can do and can not in user groups. You can limit their mailing activity or access to specific parts of <?php print(PRODUCT_NAME); ?></p>
