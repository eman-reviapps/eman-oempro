<div class="note note-info">
    <h4 class="block bold">What's Public Archive?</h4>
    <p>You can display a list of campaigns you have sent on your website. Different public archives can be set and each of them can include different set of campaigns.</p>
    <p>Just assign tag(s) for your campaigns and then choose one of the tags to create a public archive on the left side.</p>
</div>