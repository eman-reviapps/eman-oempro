<div class="note note-info">
    <h4 class="block bold">Edit Your Subscriber</h4>
    <p>This screen shows you all the available information related to your subscriber. You can update subscriber information or just go back to your subscriber browse screen by clicking the button above.</p>
    <p>In order to delete or unsubscribe this subscriber, click one of the appropriate buttons at the bottom.</p>
</div>