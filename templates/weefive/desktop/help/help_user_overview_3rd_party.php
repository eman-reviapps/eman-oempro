<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9232', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            This chapter will deal with the data synchronization tools available within <span class="bold">Flying List</span>.
            This tool will help you to keep your data synchronized to a 3rd party MySQL database. If there are several systems within your corporate structure such as ERP or CRM solutions and you would want to keep your email marketing data in sync with these ERP or CRM systems then you can easily create a list synchronization procedures.
        </p>
        <ul>
            <li>To create a new list synchronization procedure go to “Lists”</li>
            <li>Click on the particular list which requires Synchronization</li>
            <li>Click on the “Synchronization” link under the List Options</li>
        </ul>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/3rd_party/synch_0.png" />
        </p>
        <p>
            List Synchronization Settings<br/>
            <br/>
            Status<br/>
            You can use this drop down to enable and disable the synchronization.<br/>
            <br/>
            Frequency<br/>
            You can define the frequency to sync your data to the third party database using the following options.<br/>
            <br/>
            1. Every hour<br/>
            2. Every month<br/>
            3. Every week<br/>
            4. Every day<br/>
            5. Host Address<br/>
            <br/>Host address of your third party database where you will be syncing your Flying List subscriber data.<br/>
            <br/>
            Port<br/>
            You can leave it 3306 which is a default MySQL port but if there is any custom port you can change it anytime.<br/>
            <br/>
            Username<br/>
            Enter the username you use to connect to this database<br/>
            <br/>
            Password<br/>
            Enter the corresponding password for the username you specified.<br/>
            <br/>
            Name of the database<br/>
            Name of the database you wish to create the list synchronization with.<br/>
            <br/>
            SQL Query<br/>
            SQL Query which can select the necessary database tables and columns for synchronization of the list data.<br/>
            <br/>
            Once all the options are populated the synchronization will be created – in case there is an error it will be displayed on the screen.<br/>
        </p>
    </div>
</div>