<div class="note note-info">
    <h4 class="block bold">Import From URL</h4>
    <p>If you have created your email content on a third party HTML editor and don't want to use web based HTML editor, you can upload your HTML file to an accessible location on your web site and then enter the URL here.</p>
</div>