<div class="note note-info">
    <h4 class="block bold">What's Client Account?</h4>
    <p>Client accounts are useful if you are sending email campaigns on behalf of your customers.</p>
    <p>You can give them access to a reporting-only authorized area in <?php print(PRODUCT_NAME); ?> and let them to view their campaign statistics in real time.</p>
</div>