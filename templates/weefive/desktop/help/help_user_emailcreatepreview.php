<div class="note note-info">
    <h4 class="block bold">Test Your Email Before Delivery</h4>
    <p><?php print(PRODUCT_NAME); ?> gives you different tools and options to test your email before the delivery.</p>
    <p>You can test your email content by sending a preview copy of it to your own email address.</p> 
    <p>You can just open your email on the web browser and check the content.</p>
    <p><?php print(PRODUCT_NAME); ?> will also pass your email through worlds popular spam filter SpamAssassin and display you the results. You can improve your email content and subject to minimize the chance of getting filtered by spam filters.</p>
</div>