<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9230', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p> 
            <span class="bold">Flying List</span> offers a unique way of handling the subscription and un-subscription behaviors 
            so that when the event of subscription or unsubscription happens the system should perform a certain action.
        </p>
        <p>
            These behaviors can be configured through the list settings option.
        </p>
        <ul>
            <li>Go to your “Lists”</li>
            <li>Select the mailing list you wish to configure the behaviors for</li>
            <li>Click on the “Settings” link under the List Options.</li>
        </ul>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/subsc_behv/settings.png" />
        </p>
        <p>
            Subscription Behaviors<br/>
            <br/>
            You can activate several behaviors when the subscription event occurs. The behaviors are listed below.<br/>
            <br/>
            Subscribe him/her to a specific list<br/>
            If you enable this option you can subscribe the subscriber to a specific list of your choice.<br/>
            <br/>
            Unsubscribe him/her from a specific list<br/>
            If you enable this option you can unsubscribe the subscriber from some previous mailing list he/she is subscribed to already.<br/>
            <br/>
            Display my own confirmation pending page<br/>
            You can choose this option to display your own subscription confirmation pending page.<br/>
            <br/>
            Display my own subscription confirmed page<br/>
            You can choose this option to display your own subscription confirmed page when the subscriber clicks on the confirmation link.<br/>
            <br/>
            Display my own subscription error page<br/>
            You can choose this option to display your own subscription error page.<br/>
            <br/>
            Unsubscription Behavior<br/>
            <br/>
            Subscribe him/her to a specific list<br/>
            Choose this option to subscribe the subscriber to a specific list after he unsubscribes from the current list.<br/>
            <br/>
            Unsubscribe him/her from a specific list<br/>
            choose this option to unsubscribe the subscriber from other lists when he processes the unsubscription for this particular list<br/>
            <br/>
            Unsubscribe him/her from all lists<br/>
            Use this option to unsubscribe the subscriber from all the lists across your account when the unsubscription behavior happens.<br/>
            <br/>
            Add unsubscribed email addresses into suppression list<br/>
            You can choose this option to add the unsubscribed email to the suppress list<br/>
            <br/>
            Add unsubscribed email addresses into global suppression list<br/>
            You can choose this option to add the subscribed email to the global suppression list – global suppression list will ensure that no further communication is sent to the unsubscribed email.<br/>
            <br/>
            Display my own unsubscription confirmed page<br/>
            you can choose this option to display your own customized unsubscription confirmation page.<br/>
            <br/>
            Display my own unsubscription error page<br/>
            choose this option to display your own customized unsubscription error page.<br/>
        </p>
    </div>
</div>