<div class="portlet light bg-inverse">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp bold "> <?php InterfaceLanguage('Screen', '9227', false, '', false); ?> </span>
        </div>
        <div class="tools">
            <a href="" class="collapse" data-original-title="" title=""> </a>
            <a href="" class="fullscreen" data-original-title="" title=""> </a>
            <a href="" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <p class="bold">
            Welcome to the next generation email marketing!<br/>
            Send the right message, to the right person at the right time.
        </p>
        <p> 
            Track unlimited user data with JavaScript code on your website or app<br/>
            Create unlimited smart emails which can be event triggered<br/>
            <br/>
            Detailed email and user reporting<br/>
            Increase your user retention and engagement now!<br/>
            Email is the most effective communication tool with you and your customers. And now, it's even more effective. Email Automation plugin allows you to set smart event-triggered emails.<br/>
            <br/>
            Track unlimited user and data<br/>
            No matter how many users on your website or app you have. You can track unlimited user accounts and unlimited data of each user. More data means more effective email targeting.<br/>
            <br/>
            Increase retention and engagement rates<br/>
            Automatically contact you're slipping away users or users who are having a problem with a specific feature on your app, or users who have just signed up and in the trial period. Send the right message, to the right person at the right time.<br/>
            <br/>
            Saves your valuable time!<br/>
            Create your message schedules and leave the job to the Email Automation plugin. It will do the rest for you. It will track your user activities and whenever a user fits one of your messages, it will send the message and track recipient activities.<br/>
            <br/>
            Detailed reports<br/>
            Get detailed reports such as email opens, link clicks, geo-location detections, unsubscriptions, spam complaints and much more.<br/>
            <br/>
            Provide as a service to your customers<br/>
            Are you running your own email marketing service company? Provide a new value-added service to your customers and boost your business.<br/>
        </p>
        <p class="bold">
            How to use Email Automation
        </p>
        <p>
            Once you login to you will notice that “Email Automation” menu item has been added to the top menu:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/1.png" />
        </p>
        <p>
            Click the “Email Automation” top menu item. Because this is the first time you are accessing 
            the email automation section, the plugin will show you the “Getting Started” page which guides you to 
            insert tracking JavaScript code on your website pages.
        </p>
        <p>
            Email Automation can gather any kind of person data from your website pages (pages that have user information 
            such as post-login pages).  Simply copy the JavaScript code and place it on your website pages. 
            It’s as easy as placing Google Analytics tracking JavaScript code to your website page. 
            On the getting started page, you will also find a detailed explanation of securing your code,
            tracking an unlimited amount of person information (such as age, name, current plan, etc.). 
            Do your best to pass as much info as you can about your users. In this way, you can setup “smarter” emails.
        </p>
        <p>
            Once the tracking JavaScript code is placed on your website, plugin will inform you about the detected data:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/2.png" />
        </p>
        <p>
            Now, you can click “People” link on the left side menu to browse detected people.
        </p>

        <p class="bold">
            Managing people   
        </p>
        <p>
            On the “People” section, you will see the list of detected people (users in your web app, website, 
            e-commerce website, etc.). By default, “email address” and “last seen date” columns will be shown. 
            But you can customize the list layout and filter results:
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/automation/3.jpg" />
        </p>
        <p class="bold">
            Managing filters
        </p>
        <p>
            The most powerful feature of Email Automation plugin is the flexibility on targeting. 
            Simply click “Show/Hide Filters” link on the people browse page to open filtering box:
        </p>
        <p>
            Now, you will see the filter box. By default, no rules have been set.
            You can easily set your own rules by selecting target info and corresponding values. 
            For example, if you are planning to target passive users who haven’t logged into your web app for more
            than 30 days, you can setup such a similar rule set:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/4.png" />
        </p>
        <p>
            You are limitless about how you can setup your rules. 
            Here’s another one targeting users who are from United States, Canada, Australia or United Kingdom:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/5.png" />
        </p>
        <p>
            Once you hit the “Apply filters” button, Email Automation plugin will list you all people matching your criteria.
        </p>

        <p class="bold">
            Managing people list columns
        </p>
        <p>
            By default, the plugin will show you the email address and last seen date info of all people you have 
            on your list. You can add/remove more columns to the list. Click “Show/Hide Columns” link:
        </p>
        <p>
            Choose columns you want to display on the list and uncheck columns you want to hide from the list.
            Then click “Update columns” button:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/6.png" />
        </p>
        <p>
            Now you will see all checked columns on the people list.
        </p>

        <p class="bold">
            Person profile page and person management
        </p>
        <p>
            You can take a more detailed look on every single person detected in your user account. 
            Simply click to any of the detected people and you will be redirected to person profile page. 
            This page shows you all information you have about the person. 
            In addition to the info you pass through tracking JavaScript code, 
            Email Automation plugin will also try to detect some additional info such as geolocation of the person, 
            web browser, and version, etc.
        </p>
        <p>
            You can delete or unsubscribe a person on this page.
        </p>

        <p class="bold">
            Creating your first automated email
        </p>
        <p>
            Okay, your tracking code has been placed, a few people have been tracked and now it’s time to setup your first
            automated message. Simply click “Create Message” button on the top right corner of the page in Email
            Automation section.
        </p>
        <p>
            The email creates a page is quite similar to popular email apps such as Outlook. 
            We did our best to keep it similar so that your users will be able to get used to much easier.
        </p>
        <p>
            The first thing when creating your message is setting up criteria for your message. 
            Just like in people browse page, set your criteria. In this example, 
            we will set up criteria set targeting your users who haven’t been logged into your web app for more 
            than 30 days which can be considered as “passive users”:
        </p>
        <p>
            Now, you will see the filter box. By default, no rules have been set. 
            You can easily set your own rules by selecting target info and corresponding values. 
            For example, if you are planning to target passive users who haven’t logged into your web app for more 
            than 30 days, you can set up such a similar rule set:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/7.png" />
        </p>
        <p>
            You are limitless about how you can setup your rules. 
            Here're another one targeting users who are from United States, Canada, Australia or the United Kingdom:
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/8.png" />
        </p>
        <p>
            Once you hit the “Apply filters” button, Email Automation plugin will list you all people matching your criteria.
        </p>
        <p>
            Once you start to setup your rules (1), the plugin will search your people list to calculate
            estimated the number of recipients for your message (2). In the above example, 
            there are 649 estimated recipients (detected so far) for your message with a rule set of all users
            who haven’t been seen for more than 30 days and less than 60 days.
        </p>
        <p>
            The next step is to enter your email preferences including from/reply-to name and email addresses,
            email subject and most importantly, the email content.
        </p>

        <p>
            The email editor toolbar is quite similar to your word processing software. 
            However, there are some useful tools for you which we have explained below:
        </p>

        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/9.png" />
        </p>
        <p>
            <img src="<?php InterfaceTemplateURL(); ?>images/help/automation/10.png" />
        </p>

        <p>
            Once you are done with editing your email content, you can either save your message as “draft” or “activate” 
            it immediately. If it’s saved as “draft”, it will not be sent to any people matching the criteria of 
            this message.
        </p>
        <p>
            If it’s saved and activated, people matching your message will start receiving your emails, 
            only once! Email Automation plugin will monitor your people list for any new matching person and 
            send your message immediately.
        </p>
        <p class="bold">
            The message reports screen
        </p>
        <p>
            Once your message gets sent to recipients, Email Automation will start collecting useful statistics 
            such as message opens, link clicks and much more. These statistics can be found under “Message Overview” 
            page which can be accessed from “Messages” section (by clicking “Messages” link on the left side menu).
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/automation/11.png" />
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/automation/12.png" />
        </p>
        <p>
            <img style="width:70%" src="<?php InterfaceTemplateURL(); ?>images/help/automation/13.png" />
        </p>
    </div>
</div>