<div class="note note-info">
    <h4 class="block bold">Export To Your Computer</h4>
    <p>Set your export preferences on the left and get your subscriber data in CSV (comma separated value) format within seconds. Once the export data is downloaded to your computer, you can import it to any application that supports CSV file format.</p>
    <p>Almost all applications support CSV file format including Microsoft Excel&reg; and Apple Numbers&reg;.</p>
</div>