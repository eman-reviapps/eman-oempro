<?php include_once(TEMPLATE_PATH.'desktop/layouts/subscriber_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '0933', false, '', false, true); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<div class="white" style="min-height:200px;">
					<form id="campaigns-table-form" action="<?php InterfaceAppURL(); ?>/user/lists/browse/" method="post">
						<table border="0" cellspacing="0" cellpadding="0" class="grid" id="users-table">
							<tr>
								<th width="570" colspan="2"><?php InterfaceLanguage('Screen', '1479', false, '', true); ?></th>
							</tr>
							<?php 
							foreach ($SubscribedLists as $EachList): 
							?>
								<tr>
									<td width="355">
										<a href="<?php InterfaceAppURL(); ?>/subscriber/info/<?php print($EachList['ListID']); ?>/<?php echo $EachList['SubscriberID']; ?>"><?php print($EachList['ListName']); ?></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="help-column span-5 push-1 last">
		<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_lists.php'); ?>
	</div>
</div>
<!-- Page - End -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/subscriber_footer.php'); ?>