<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_header.php'); ?>

<div class="span-11 push-1">
	<div class="login-box">
		<div class="inner">
			<form id="Form_Login" method="post" action="<?php InterfaceAppURL(); ?>/subscriber/login/<?php echo $Parameter; ?>">
				<input type="hidden" name="MSubscriberID" value="<?php echo $MSubscriberID; ?>" id="MSubscriberID">
				<input type="hidden" name="MEmailAddress" value="<?php echo $MEmailAddress; ?>" id="MEmailAddress">
				<input type="hidden" name="ListID" value="<?php echo $ListID; ?>" id="ListID">
				<?php
				if (isset($PageErrorMessage) == true):
				?>
					<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
				<?php
				elseif (isset($PageSuccessMessage) == true):
				?>
					<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
				<?php
				else:
				?>
					<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1475', false); ?></h3>
				<?php
				endif;
				?>
				<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?> no-background-color" id="form-row-EmailAddress">
					<label for="EmailAddress" style="width:113px;"><?php InterfaceLanguage('Screen', '0010', false); ?>:</label>
					<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress'); ?>" id="EmailAddress" class="text" style="width:240px;" />
					<?php print(form_error('EmailAddress', '<div class="form-row-note error" style="padding-left:25px;"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row no-background-color clearfix" style="padding-left:33px;">
					<label>&nbsp;</label>
					<a href="#" id="FormButton_Submit" class="button" style="float:left;" targetform="Form_Login"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0266', false, '', true); ?></strong></a>
					<input type="hidden" name="Command" value="Login" id="Command" />
				</div>
			</form>
		</div>
	</div>
</div>
<?php include_once(TEMPLATE_PATH.'desktop/layouts/login_footer.php'); ?>
