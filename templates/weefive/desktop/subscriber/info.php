<?php include_once(TEMPLATE_PATH.'desktop/layouts/subscriber_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php InterfaceLanguage('Screen', '1478', false, '', false, true); ?></h1>
			</div>
			<div class="actions">
				<a class="button" href="<?php echo InterfaceAppUrl(); ?>/subscriber/lists"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1477', false, '', true); ?></strong></a>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - START -->
<form id="subscriber-edit" action="<?php InterfaceAppURL(); ?>/subscriber/info/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>" method="post">
	<input type="hidden" name="Command" value="Save" id="Command">
	<div class="container">
		<div class="span-18">
			<div id="page-shadow">
				<div id="page">
					<div class="white">
						<?php
						if (isset($PageSuccessMessage) == true):
						?>
							<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
						<?php
						elseif (isset($PageErrorMessage) == true):
						?>
							<h3 class="form-legend success"><?php print($PageErrorMessage); ?></h3>
						<?php
						elseif (validation_errors()):
						?>
						<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
						<?php else: ?>
						<h3 class="form-legend"><?php InterfaceLanguage('Screen', '1336', false, '', false, false, array($ListInformation['Name'])); ?></h3>
						<?php
						endif;
						?>
						<div class="form-row <?php print((form_error('EmailAddress') != '' ? 'error' : '')); ?>" id="form-row-EmailAddress">
							<label for="EmailAddress"><?php InterfaceLanguage('Screen', '0010', false, '', false, true); ?>: *</label>
							<input type="text" name="EmailAddress" value="<?php echo set_value('EmailAddress', $SubscriberInformation['EmailAddress']); ?>" id="EmailAddress" class="text" style="width:450px;" readonly="readonly" />
							<?php print(form_error('EmailAddress', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<div class="form-row" id="form-row-BounceType">
							<label for="BounceType"><?php InterfaceLanguage('Screen', '1320', false, '', false, true); ?>:</label>
							<input disabled="disabled" readonly="readonly" type="text" name="BounceType" value="<?php echo set_value('EmailAddress', $SubscriberInformation['BounceType']); ?>" id="BounceType" class="text" />
						</div>
						<div class="form-row" id="form-row-SubscriptionDate">
							<label for="SubscriptionDate"><?php InterfaceLanguage('Screen', '1321', false, '', false, true); ?>:</label>
							<input disabled="disabled" readonly="readonly" type="text" name="SubscriptionDate" value="<?php echo set_value('EmailAddress', date('M d, Y', strtotime($SubscriberInformation['SubscriptionDate']))); ?>" id="SubscriptionDate" class="text" />
						</div>
						<?php foreach($CustomFields as $EachField): ?>
							<div class="form-row">
								<label><?php echo $EachField['FieldName']; ?></label>
								<?php echo $EachField['html']; ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="help-column span-5 push-1 last">
			<?php include_once(TEMPLATE_PATH.'desktop/help/help_user_subscriberedit.php'); ?>
		</div>
		
		<div class="span-18 last">
			<div class="form-action-container">
				<a class="button" targetform="subscriber-edit" id="form-button" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0304', false, '', true); ?></strong></a>
				<a class="button" id="form-button" href="<?php InterfaceAppURL(); ?>/user/subscriber/unsubscribe/<?php echo $ListInformation['ListID']; ?>/<?php echo $SubscriberInformation['SubscriberID']; ?>" style="float:left;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '1309', false, '', true); ?></strong></a>
			</div>
		</div>
		
	</div>
</form>
<!-- Page - END -->

<?php include_once(TEMPLATE_PATH.'desktop/layouts/subscriber_footer.php'); ?>