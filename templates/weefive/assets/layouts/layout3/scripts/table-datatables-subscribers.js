var TableDatatablesEditable = function () {

    var handleTable = function () {

        var selectedIDs = [];
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-full-width",
            "showDuration": "3000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        function show_alert(message)
        {
            bootbox.alert({
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn purple-sharp'
                    }
                },
                message: message,
            });
        }

        function getSelectedSubscribers(oTable)
        {
            var rowcollection = oTable.$(".checkboxes:checked", {"page": "all"});
            selectedIDs = [];

            rowcollection.each(function (index, elem) {
                var checkbox_value = $(elem).val();
                selectedIDs.push(checkbox_value);
            });
            selectedIDs = selectedIDs.join(',');
        }

        function UnSusbscribe(oTable)
        {
            getSelectedSubscribers(oTable);
            if (selectedIDs.length == 0)
            {
                show_alert(lang_subscribers['9244']);
                return;
            }
            bootbox.confirm({
                title: "<span class='bold' >" + lang_subscribers['1309'] + "</span>",
                message: lang_subscribers['9248'],
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn purple-sharp'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true)
                    {
                        $.ajax({
                            type: "POST",
                            data: {
                                'listid': $('#SearchList').val(),
                                'SelectedSubscribersIDs': selectedIDs
                            },
                            url: url_unsubscribe,
                            success: function (data) {
                                data_parsed = JSON.parse(data);
//                                show_alert(data_parsed[1]);
                                if (data_parsed[0] == true)
                                {
                                    toastr['info'](data_parsed[1], 'Notification');
                                    applyFilters();
                                } else
                                {
                                    toastr['error'](data_parsed[1], 'Notification');
                                }
                            },
                            async: false
                        });
                    }
                }
            });
        }

        function CopyToAnotherList(oTable)
        {
            getSelectedSubscribers(oTable);
            if (selectedIDs.length == 0)
            {
                show_alert(lang_subscribers['9244']);
                return;
            }
            bootbox.prompt({
                title: "<span class='bold' >" + lang_subscribers['9289'] + "</span>",
                inputType: 'select',
                inputOptions: ListsArray,
                buttons: {
                    confirm: {
                        label: 'Ok',
                        className: 'btn purple-sharp'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn btn-danger'
                    }
                },
                callback: function (result) {

                    if (result != null)
                    {
                        console.log(result);
                        $.ajax({
                            type: "POST",
                            data: {
                                'listid': $('#SearchList').val(),
                                'to_listid': result,
                                'SelectedSubscribersIDs': selectedIDs
                            },
                            url: url_copy_to_another_list,
                            success: function (data) {
                                console.log(data);
                                data_parsed = JSON.parse(data);
//                                show_alert(data_parsed[1]);
                                if (data_parsed[0] == true)
                                {
                                    toastr['info'](data_parsed[1], 'Notification');
                                    applyFilters();
                                } else
                                {
                                    toastr['error'](data_parsed[1], 'Notification');
                                }
                            },
                            async: false
                        });
                    }
                }
            });
        }
        function MoveToAnotherList(oTable)
        {
            getSelectedSubscribers(oTable);
            if (selectedIDs.length == 0)
            {
                show_alert(lang_subscribers['9244']);
                return;
            }
            bootbox.prompt({
                title: "<span class='bold' >" + lang_subscribers['9250'] + "</span>",
                inputType: 'select',
                inputOptions: ListsArray,
                buttons: {
                    confirm: {
                        label: 'Ok',
                        className: 'btn purple-sharp'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn btn-danger'
                    }
                },
                callback: function (result) {

                    if (result != null)
                    {
                        console.log(result);
                        $.ajax({
                            type: "POST",
                            data: {
                                'listid': $('#SearchList').val(),
                                'to_listid': result,
                                'SelectedSubscribersIDs': selectedIDs
                            },
                            url: url_move_to_another_list,
                            success: function (data) {
                                console.log(data);
                                data_parsed = JSON.parse(data);
//                                show_alert(data_parsed[1]);
                                if (data_parsed[0] == true)
                                {
                                    toastr['info'](data_parsed[1], 'Notification');
                                    applyFilters();
                                } else
                                {
                                    toastr['error'](data_parsed[1], 'Notification');
                                }
                            },
                            async: false
                        });
                    }
                }
            });
        }

        function MoveToSuppressionList(oTable)
        {
            getSelectedSubscribers(oTable);

            if (selectedIDs.length == 0)
            {
                show_alert(lang_subscribers['9244']);
                return;
            }

            bootbox.confirm({
                title: "<span class='bold' >" + lang_subscribers['9243'] + "</span>",
                message: lang_subscribers['9247'],
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn purple-sharp'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true)
                    {
                        $.ajax({
                            type: "POST",
                            data: {
                                'listid': $('#SearchList').val(),
                                'SelectedSubscribersIDs': selectedIDs
                            },
                            url: url_move_to_suppression,
                            success: function (data) {
                                console.log(data)
                                data_parsed = JSON.parse(data);
//                                show_alert(data_parsed[1]);
                                if (data_parsed[0] == true)
                                {
                                    toastr['info'](data_parsed[1], 'Notification');
                                    applyFilters();
                                } else
                                {
                                    toastr['error'](data_parsed[1], 'Notification');
                                }
                            },
                            async: false
                        });
                    }
                }
            });


        }
        var table = $('#sample_1');

        var oTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
//                {className: 'btn purple btn-outline', text: lang_subscribers['9250'],
//                    action: function (e, dt, node, config) {
//                        MoveToAnotherList(dt);
//                    }},
//                {className: 'btn dark btn-outline', text: lang_subscribers['9243'],
//                    action: function (e, dt, node, config) {
//                        MoveToSuppressionList(dt);
//                    }},
//                {className: 'btn red btn-outline unsubscribe-btn', text: lang_subscribers['1309'],
//                    action: function (e, dt, node, config) {
//                        UnSusbscribe(dt);
//                    }},
                {extend: 'print', className: 'btn dark btn-outline'},
                {extend: 'copy', className: 'btn red btn-outline'},
                {extend: 'pdf', className: 'btn green btn-outline'},
                {extend: 'excel', className: 'btn yellow btn-outline '},
                {extend: 'csv', className: 'btn purple btn-outline '},
                {extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
            ],
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,
            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        });

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });


        $("#copy_to_another_list").unbind('click').click(function () {
            CopyToAnotherList(oTable);
        });

        $("#move_to_another_list").unbind('click').click(function () {
            MoveToAnotherList(oTable);
        });
        $("#move_to_supprression").unbind('click').click(function () {
            MoveToSuppressionList(oTable);
        });
        $("#unsubscribe").unbind('click').click(function () {
            UnSusbscribe(oTable);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesEditable.init();
});