// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var path = require('path');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('less', function () {
    gulp.src(url + 'less/main.less')
            //.pipe(sourcemaps.init())
            .pipe(less())

            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false,
                remove: false,
            }))
            //.pipe(sourcemaps.write())
            .pipe(minifyCSS())
            .pipe(gulp.dest(url + 'css'))
            .pipe(browserSync.reload({stream: true}));
});

gulp.task('integrate-less', function () {
    gulp.src(url + 'less/integrate.less')
            .pipe(less())
            .on('error', function (err) {
                this.emit('end');
            })
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false,
                remove: false
            }))
            .pipe(minifyCSS())
            .pipe(gulp.dest(url + 'css'))
            .pipe(browserSync.reload({stream: true}));
});

// Concatenate & Minify JS
gulp.task('scripts', function () {
    return gulp.src([
        url + 'js/editor/resources/colors.js',
        url + 'js/editor/resources/gradients.js',
        url + 'js/vendor/jquery.js',
        url + 'js/vendor/jquery-ui.js',
        url + 'js/vendor/file-saver.js',
        url + 'js/vendor/pagination.js',
        url + 'js/vendor/spectrum.js',
        url + 'js/vendor/hammer.js',
        url + 'js/vendor/scrollbar.js',
        url + 'js/vendor/angular.min.js',
        url + 'js/vendor/angular-animate.js',
        url + 'js/vendor/angular-aria.js',
        url + 'js/vendor/angular-material.js',
        url + 'js/vendor/angular-sortable.js',
        url + 'js/vendor/fabric.js',
        url + 'js/editor/App.js',
        url + 'js/editor/LocalStorage.js',
        url + 'js/editor/Settings.js',
        url + 'js/editor/Keybinds.js',
        url + 'js/editor/Canvas.js',
        url + 'js/editor/crop/cropper.js',
        url + 'js/editor/crop/cropzone.js',
        url + 'js/editor/crop/cropController.js',
        url + 'js/editor/basics/RotateController.js',
        url + 'js/editor/basics/CanvasBackgroundController.js',
        url + 'js/editor/basics/ResizeController.js',
        url + 'js/editor/basics/RoundedCornersController.js',
        url + 'js/editor/zoomController.js',
        url + 'js/editor/TopPanelController.js',
        url + 'js/editor/directives/Tabs.js',
        url + 'js/editor/directives/PrettyScrollbar.js',
        url + 'js/editor/directives/ColorPicker.js',
        url + 'js/editor/directives/FileUploader.js',
        url + 'js/editor/directives/TogglePanelVisibility.js',
        url + 'js/editor/directives/ToggleSidebar.js',
        url + 'js/editor/text/Text.js',
        url + 'js/editor/text/TextController.js',
        url + 'js/editor/text/TextAlignButtons.js',
        url + 'js/editor/text/TextDecorationButtons.js',
        url + 'js/editor/text/Fonts.js',
        url + 'js/editor/drawing/Drawing.js',
        url + 'js/editor/drawing/DrawingController.js',
        url + 'js/editor/drawing/RenderBrushesDirective.js',
        url + 'js/editor/History.js',
        url + 'js/editor/Saver.js',
        url + 'js/editor/filters/FiltersController.js',
        url + 'js/editor/filters/Filters.js',
        url + 'js/editor/shapes/SimpleShapesController.js',
        url + 'js/editor/shapes/StickersController.js',
        url + 'js/editor/shapes/StickersCategories.js',
        url + 'js/editor/shapes/SimpleShapes.js',
        url + 'js/editor/shapes/Polygon.js',
        url + 'js/editor/objects/ObjectsPanelController.js',
    ])
            .pipe(concat('scripts.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(url + 'js'))
            .pipe(browserSync.reload({stream: true}));
});

// Watch Files For Changes
gulp.task('watch', function () {
    browserSync({
        proxy: "localhost/pixie/",
        //server: {
        //   baseDir: "./"
        //}
    });

    gulp.watch(url + 'js/**/*.js', ['scripts']);
    gulp.watch(url + 'less/**/*.less', ['less']);
    gulp.watch(url + 'less/**/integrate.less', ['integrate-less']);
});

// Default Task
gulp.task('default', ['less', 'scripts', 'watch']);