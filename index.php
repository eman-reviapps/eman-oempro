<?php

// Check if configuration file exists. If it does not exist, redirect to installation utility - Start
$FileToCheck = dirname(__FILE__) . '/data/config.inc.php';

if (file_exists($FileToCheck) == false) {
    header('Location: ./install/index.php');
    exit;
}
// Check if configuration file exists. If it does not exist, redirect to installation utility - End
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End

$TargetURL = '';

if (defined('HTACCESS_ENABLED') == true && HTACCESS_ENABLED == true) {
    $TargetURL = './' . APP_DIRNAME . '/user/';
} else {
    $TargetURL = './' . APP_DIRNAME . '/index.php?/user/';
}

header('Location: ' . $TargetURL);
exit;
?>