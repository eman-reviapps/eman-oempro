$(document).ready(function() {
	if ($('input[name=FormValue_LicenseType]').length > 0) {
		$('input[name=FormValue_LicenseType]').click(function() {
			var SelectedInstallType = $(this).val();
			SwitchInstallType(SelectedInstallType);
		});

		SwitchInstallType($('input[name=FormValue_LicenseType]:checked').val());
	} else {

	}


});





function SwitchInstallType(SelectedInstallType) {
	if (SelectedInstallType == 'Trial') {
		$('#TrialOptions').show();
		$('#RegisterOptions').hide();
		$('#PurchaseOptions').hide();
	} else if (SelectedInstallType == 'Register') {
		$('#TrialOptions').hide();
		$('#RegisterOptions').show();
		$('#PurchaseOptions').hide();
	} else if (SelectedInstallType == 'Purchase') {
		$('#TrialOptions').hide();
		$('#RegisterOptions').hide();
		$('#PurchaseOptions').show();
	}
}