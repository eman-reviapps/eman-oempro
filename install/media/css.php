<?php
/**
*
*
* @author Cem Hurturk
* @version $Id$
* @copyright Octeth, 11 November, 2007
* @package default
**/

/**
* CSS Theme Module
**/

// PHP Settings - Start
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);
set_time_limit(30);
// PHP Settings - End

// Set the install path and other constants - Start
$CurrentPath = dirname(__FILE__);
define('TEMPLATE_PATH',		$CurrentPath.'/../../templates/weefor/');
define('APP_PATH',			$CurrentPath.'/../..');
define('TEMPLATE_URL',		'../../templates/weefor/');
define('APP_URL',			'./');
define('TEMPLATE',			'weefor');
// Set the install path and other constants - End

// Include main module - Start {
include_once($CurrentPath.'/../../includes/classes/core.inc.php');
include_once($CurrentPath.'/../../includes/classes/themes.inc.php');
include_once($CurrentPath.'/../../includes/libraries/csscolor.inc.php');
// Include main module - End }

// Retrieve CSS style of the template - Start
$CSS = file_get_contents(TEMPLATE_PATH.'styles/ui.css');
// Retrieve CSS style of the template - End

// Retrieve CSS settings - Start
$ArrayCSSSettings = ThemeEngine::LoadCSSSettings(TEMPLATE);
// Retrieve CSS settings - End

// Show product logo if requested - Start
if ($_GET['ShowLogo'] != '')
	{
	header('Content-type: image/png');

	if (file_exists(TEMPLATE_PATH.'images/logo-oempro.png') == true)
		{
		$FileHandler = fopen(TEMPLATE_PATH.'images/logo-oempro.png', 'rb');
			fpassthru($FileHandler);
		fclose($FileHandler);
		exit;
		}
	exit;
	}
// Show product logo if requested - End

// Apply theme settings to the template CSS - Start
// Calculate tints for theme settings - Start
foreach ($ArrayThemeSettings as $ThemeKey1=>$Value1)
	{
	foreach ($ArrayThemeSettings as $ThemeKey2=>$Value2)
		{
		if (strpos($Value2, $ThemeKey1) !== false)
			{
			$difference = str_replace($ThemeKey1, '', $Value2);
			if ($difference == 'FGC')
				{
				$color = new CSS_Color($ArrayThemeSettings[$ThemeKey1]);
				$newcolor = $color->fg['0'];
				$ArrayThemeSettings[$ThemeKey2] = $newcolor;
				}
			else
				{
				$color = new CSS_Color($ArrayThemeSettings[$ThemeKey1]);
				$newcolor = $color->bg[$difference];
				$ArrayThemeSettings[$ThemeKey2] = $newcolor;
				}
			}
		}
	}
// Calculate tints for theme settings - End

// Calculate tints for css settings - Start
foreach ($ArrayCSSSettings as $Key=>$EachSettings1)
	{
	foreach ($ArrayCSSSettings as $EachSettings2)
		{
		if (strpos($EachSettings1['Default'], $EachSettings2['Tag']) !== false)
			{
			$difference = str_replace($EachSettings2['Tag'], '', $EachSettings1['Default']);
			if ($difference == 'FGC')
				{
				$color = new CSS_Color($EachSettings2['Default']);
				$newcolor = $color->fg['0'];
				$ArrayCSSSettings[$Key]['Default'] = $newcolor;
				}
			else
				{
				$color = new CSS_Color($EachSettings2['Default']);
				$newcolor = $color->bg[$difference];
				$ArrayCSSSettings[$Key]['Default'] = $newcolor;
				}
			}
		}
	}
// Calculate tints for css settings - End

foreach ($ArrayCSSSettings as $Key=>$ArrayEachSetting)
	{
	$Pattern = '/'.$ArrayEachSetting['Tag'].'/iU';
	$Replace = $ArrayEachSetting['Default'];
	$CSS = preg_replace($Pattern, $Replace, $CSS);
	}
// Apply theme settings to the template CSS - End

// Perform other replacements - Start
$Pattern = array('/_Template:URL_/iU', '/_CSSPHP:URL_/iU');
$Replace = array(TEMPLATE_URL, APP_URL.'css.php');
$CSS = preg_replace($Pattern, $Replace, $CSS);
// Perform other replacements - End

header('content-type: text/css');
print $CSS;

exit;
?>