<?php

/**
 * @author Cem Hurturk
 * @copyright Octeth.com
 * */
/**
 * API Module. Contains all functions and procedures.
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('api');
Core::LoadObject('user_auth');
Core::LoadObject('client_auth');
Core::LoadObject('admin_auth');
Core::LoadObject('subscriber_auth');
Core::LoadObject('users');
Core::LoadObject('clients');
Core::LoadObject('admins');
Core::LoadObject('subscribers');
Core::LoadObject('form');
Core::LoadObject('json');
// Load other modules - End
// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'login');
// Load language - End
// API procedures - Start
try {
    // Initialize variables - Start
    $ArrayOutput = array();
    // Initialize variables - End
    // Decide the connection method (POST, GET or XML) - Start
    $ArrayAPIData = array();

    if (count($_POST) > 0) {
        $ArrayAPIData = $_POST;
    } elseif (count($_GET) > 0) {
        $ArrayAPIData = $_GET;
    } else {
        throw new Exception('Invalid connection method');
    }
    // Decide the connection method (POST, GET or XML) - End
    // Lowercase all field names - Start
    $TMPArrayAPIData = $ArrayAPIData;
    unset($ArrayAPIData);
    foreach ($TMPArrayAPIData as $Key => $Value) {
        $ArrayAPIData[strtolower($Key)] = $Value;
    }
    unset($TMPArrayAPIData);
    // Lowercase all field names - End
    // APIKEY - Start
    if (isset($ArrayAPIData['apikey']) && $ArrayAPIData['apikey'] != '') {
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'user.login',
                    'protected' => false,
                    'parameters' => array(
                        'apikey' => $ArrayAPIData['apikey']
                    )
        ));
    }
    // APIKEY - End
    // If session ID provided, re-start the session with that session ID - Start
    if ($ArrayAPIData['sessionid'] != '') {
        // Destroy the previous session opened in config file - Start
        session_id('');
        session_write_close();
        // Destroy the previous session opened in config file - End

        session_id($ArrayAPIData['sessionid']);
        session_start();
    }
    // If session ID provided, re-start the session with that session ID - End
    // Plugin hook - Start
    Plugins::HookListener('Action', 'API.Before', array($ArrayAPIData));
    // Plugin hook - End
    // If passed in data is in XML format, parse it - Start
    if ($ArrayAPIData['mode'] == 'XML') {
        $ObjectXMLData = simplexml_load_string($ArrayAPIData['xmldata']);

        $ArrayAPIData['command'] = $ObjectXMLData->command;
    }
    // If passed in data is in XML format, parse it - End
    // If user is logged in, get user information - Start
    if (UserAuth::IsLoggedIn(false, false)) {
        $ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']));
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($ArrayUserInformation) == true) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 'USER ACCOUNT TRIAL PERIOD HAS EXPIRED'
            );
            throw new Exception('');
        }
        // Check if user account has expired - End }
    }
    // If user is logged in, get user information - End
    // If client is logged in, get client information - Start
    if (ClientAuth::IsLoggedIn(false, false)) {
        $ArrayClientInformation = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))' => $_SESSION[SESSION_NAME]['ClientLogin']));
    }
    // If client is logged in, get client information - End
    // If admin is logged in, get admin information - Start
    if (AdminAuth::IsLoggedIn(false, false)) {
        $ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));
    }
    // If admin is logged in, get admin information - End
    // If subscriber is logged in, get admin information - Start
    if (SubscriberAuth::IsLoggedIn(false, false)) {
        $ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('CONCAT(MD5(`SubscriberID`), MD5(`EmailAddress`))' => $_SESSION[SESSION_NAME]['SubscriberLogin']['Salt']), $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']);
    }
    // If subscriber is logged in, get admin information - End
    // Process the request and output the result - Start
    if ($ArrayAPIData['command'] == 'User.Login') {
        include_once(Core::GetAPIPath('user.login'));
    } elseif ($ArrayAPIData['command'] == 'User.PasswordRemind') {
        include_once(Core::GetAPIPath('user.passwordremind'));
    } elseif ($ArrayAPIData['command'] == 'User.PasswordReset') {
        include_once(Core::GetAPIPath('user.passwordreset'));
    } elseif ($ArrayAPIData['command'] == 'User.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('User.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('user.update'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.Create') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Approve') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('campaign.approve'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Users.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('users.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.Snapshot') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.snapshot'));
        } else if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.snapshot'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.AddCredits') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.addcredits'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Users.Delete') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('users.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.Switch') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.switch'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.PaymentPeriods') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.paymentperiods'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.PaymentPeriods.Update') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.paymentperiods_update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'User.PaymentPeriods.Update2') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('user.paymentperiods_update2'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    }elseif ($ArrayAPIData['command'] == 'Admin.Login') {
        include_once(Core::GetAPIPath('admin.login'));
    } elseif ($ArrayAPIData['command'] == 'Admin.PasswordRemind') {
        include_once(Core::GetAPIPath('admin.passwordremind'));
    } elseif ($ArrayAPIData['command'] == 'Admin.PasswordReset') {
        include_once(Core::GetAPIPath('admin.passwordreset'));
    } elseif ($ArrayAPIData['command'] == 'Admin.Update') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('admin.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Settings.Update') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('settings.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Settings.EmailSendingTest') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('settings.emailsendingtest'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Theme.Create') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('theme.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Theme.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('theme.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Theme.Update') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('theme.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Theme.Delete') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('theme.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Themes.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('themes.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroup.Create') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroup.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroup.Update') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroup.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroup.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroup.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroup.Delete') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroup.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroup.Duplicate') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroup.duplicate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'UserGroups.Get') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('usergroups.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Attachment.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('attachment.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Duplicate') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.duplicate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Emails.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('emails.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.PersonalizationTags') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.personalizationtags'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.EmailPreview') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.emailpreview'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.SpamTest') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Email.SpamTest'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.spamtest'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.DesignPreview.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Email.DesignPreview'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.designpreviewcreate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.DesignPreview.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Email.DesignPreview'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.designpreviewdelete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.DesignPreview.GetList') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Email.DesignPreview'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.designpreviewlist'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.DesignPreview.Details') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Email.DesignPreview'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.designpreviewdetails'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Template.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('EmailTemplates.Manage'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.templatecreate'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.templatecreate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Templates.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('EmailTemplates.Manage'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.templatesget'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.templatesget'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Template.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('EmailTemplates.Manage'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.templateget'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.templateget'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Template.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('EmailTemplates.Manage'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.templateupdate'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.templateupdate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Template.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('EmailTemplates.Manage'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('email.templatedelete'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.templatedelete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Email.Delivery.Test') {
        if (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('email.delivery.test'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaigns.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaigns.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaigns.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Pause') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.pause'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Resume') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.resume'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaign.Cancel') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaign.cancel'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaigns.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaign.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaigns.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Campaigns.Archive.GetURL') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaigns.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('campaigns.archive.geturl'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'List.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('list.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'List.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('list.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Lists.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('lists.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Lists.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Lists.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('lists.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'List.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('list.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'CustomField.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('CustomField.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('customfield.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'CustomField.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('CustomField.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('customfield.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'CustomFields.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('CustomFields.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('customfields.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'CustomFields.Copy') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('CustomField.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('customfields.copy'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'CustomFields.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('CustomFields.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('customfields.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.GetURLs') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.geturls'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.AddURL') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.addurl'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.DeleteURLs') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.deleteurls'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.TestURL') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.testurl'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.GenerateSubscriptionFormHTMLCode') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.generatesubscriptionformhtmlcode'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'ListIntegration.GenerateUnsubscriptionFormHTMLCode') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('List.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('listintegration.generateunsubscriptionformhtmlcode'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.Upload') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Media.Upload'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.upload'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.Browse') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Media.Browse'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.browse'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.Retrieve') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Media.Retrieve'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.retrieve'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.FolderCreate') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start
            if (Users::HasPermissions(array('Media.Upload'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.foldercreate'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.FolderDelete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start
            if (Users::HasPermissions(array('Media.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.folderdelete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Media.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Media.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('medialibrary.delete'));
        } elseif (AdminAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('medialibrary.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponders.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponders.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponders.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponder.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponder.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponder.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponders.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponders.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponders.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponders.Copy') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponder.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponders.copy'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponder.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponder.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponder.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'AutoResponder.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('AutoResponder.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('autoresponder.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Segments.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Segments.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('segments.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Segments.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Segments.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('segments.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Segments.Copy') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Segment.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('segments.copy'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Segment.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Segment.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('segment.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Segment.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Segment.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('segment.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Subscribers.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Subscribers.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('subscribers.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Subscriber.Update') {
        if (isset($ArrayAPIData['access']) && $ArrayAPIData['access'] == 'subscriber') {
            if (SubscriberAuth::IsLoggedIn(false, false)) {
                include_once(Core::GetAPIPath('subscriber.update'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        } else {
            if (UserAuth::IsLoggedIn(false, false)) {
                // User privilege check - Start			
                if (Users::HasPermissions(array('Subscriber.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                    $ArrayOutput = array('Success' => false,
                        'ErrorCode' => 99999
                    );
                    throw new Exception('');
                }
                // User privilege check - End

                include_once(Core::GetAPIPath('subscriber.update'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        }
    } elseif ($ArrayAPIData['command'] == 'Subscriber.GetLists') {
        if (SubscriberAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('subscriber.getlists'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Subscriber.Get') {
        if (isset($ArrayAPIData['access']) && $ArrayAPIData['access'] == 'subscriber') {
            if (SubscriberAuth::IsLoggedIn(false, false)) {
                include_once(Core::GetAPIPath('subscriber.get'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        } else {
            if (UserAuth::IsLoggedIn(false, false)) {
                // User privilege check - Start			
                if (Users::HasPermissions(array('Subscribers.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                    $ArrayOutput = array('Success' => false,
                        'ErrorCode' => 99999
                    );
                    throw new Exception('');
                }
                // User privilege check - End

                include_once(Core::GetAPIPath('subscriber.get'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        }
    } elseif ($ArrayAPIData['command'] == 'Subscriber.Login') {
        include_once(Core::GetAPIPath('subscriber.login'));
    } elseif ($ArrayAPIData['command'] == 'Subscriber.Subscribe') {
        include_once(Core::GetAPIPath('subscriber.subscribe'));
    } elseif ($ArrayAPIData['command'] == 'Subscriber.OptIn') {
        include_once(Core::GetAPIPath('subscriber.optin'));
    } elseif ($ArrayAPIData['command'] == 'Subscriber.Unsubscribe') {
        include_once(Core::GetAPIPath('subscriber.unsubscribe'));
    } elseif ($ArrayAPIData['command'] == 'Subscribers.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Subscribers.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('subscribers.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Subscribers.Search') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Subscribers.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('subscribers.search'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Subscribers.Import') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Subscribers.Import'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('subscribers.import'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Clients.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Clients.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('clients.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Clients.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Clients.Delete'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('clients.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Client.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('client.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.Update') {
        if (isset($ArrayAPIData['access']) && $ArrayAPIData['access'] == 'client') {
            if (ClientAuth::IsLoggedIn(false, false)) {
                include_once(Core::GetAPIPath('client.update'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        } else {
            if (UserAuth::IsLoggedIn(false, false)) {
                // User privilege check - Start			
                if (Users::HasPermissions(array('Client.Update'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                    $ArrayOutput = array('Success' => false,
                        'ErrorCode' => 99999
                    );
                    throw new Exception('');
                }
                // User privilege check - End

                include_once(Core::GetAPIPath('client.update'));
            } else {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99998
                );
                throw new Exception('');
            }
        }
    } elseif ($ArrayAPIData['command'] == 'Client.AssignSubscriberLists') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Client.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('client.assignsubscriberlists'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.AssignCampaigns') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Client.Create'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('client.assigncampaigns'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.Login') {
        include_once(Core::GetAPIPath('client.login'));
    } elseif ($ArrayAPIData['command'] == 'Client.PasswordRemind') {
        include_once(Core::GetAPIPath('client.passwordremind'));
    } elseif ($ArrayAPIData['command'] == 'Client.PasswordReset') {
        include_once(Core::GetAPIPath('client.passwordreset'));
    } elseif ($ArrayAPIData['command'] == 'Client.Lists.Get') {
        if (ClientAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('client.lists.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.List.Get') {
        if (ClientAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('client.list.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.Campaigns.Get') {
        if (ClientAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('client.campaigns.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Client.Campaign.Get') {
        if (ClientAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('client.campaign.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tags.Get') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('tags.get'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tag.Create') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('tag.create'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tag.Update') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('tag.update'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tags.Delete') {
        if (UserAuth::IsLoggedIn(false, false)) {
            include_once(Core::GetAPIPath('tags.delete'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tag.AssignToCampaigns') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaigns.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('tag.assigntocampaigns'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } elseif ($ArrayAPIData['command'] == 'Tag.UnassignFromCampaigns') {
        if (UserAuth::IsLoggedIn(false, false)) {
            // User privilege check - Start			
            if (Users::HasPermissions(array('Campaigns.Get'), $ArrayUserInformation['GroupInformation']['Permissions']) == false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 99999
                );
                throw new Exception('');
            }
            // User privilege check - End

            include_once(Core::GetAPIPath('tag.unassignfromcampaigns'));
        } else {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 99998
            );
            throw new Exception('');
        }
    } else {
        $APICallParameters = array();
        $APICallParameters['command'] = $ArrayAPIData['command'];
        $APICallParameters['parameters'] = $ArrayAPIData;

        unset($APICallParameters['parameters']['command']);
        unset($APICallParameters['parameters']['apikey']);
        unset($APICallParameters['parameters']['username']);
        unset($APICallParameters['parameters']['password']);
        unset($APICallParameters['parameters']['password']);
        unset($APICallParameters['parameters']['responseformat']);

        if (AdminAuth::IsLoggedIn(false, false)) {
            $APICallParameters['username'] = $ArrayAdminInformation['Username'];
            $APICallParameters['password'] = $ArrayAdminInformation['Password'];
            $APICallParameters['protected'] = true;
        } else if (UserAuth::IsLoggedIn(false, false)) {
            $APICallParameters['username'] = $ArrayUserInformation['Username'];
            $APICallParameters['password'] = $ArrayUserInformation['Password'];
            $APICallParameters['protected'] = true;
        }

        $ArrayOutput = API::call($APICallParameters);
        unset($APICallParameters);
    }
    // Process the request and output the result - End
} catch (Exception $e) {
    if ($e->GetMessage() != '') {
        die($e->GetMessage());
    }
}
// API procedures - End
// Display the request output on the screen - Start
if ($ArrayAPIData['responseformat'] == 'JSON') {
    // ResponseFormat = JSON
    header('Content-Type: text/javascript');
    $ObjectJSON = new Services_JSON();


    if (isset($ArrayAPIData['jsonpcallback']) && $ArrayAPIData['jsonpcallback'] != '') {
        print $ArrayAPIData['jsonpcallback'] . '(' . $ObjectJSON->encode($ArrayOutput) . ')';
    } else {
        print $ObjectJSON->encode($ArrayOutput);
    }
} else {
    // ResponseFormat = XML
    header('Content-Type: text/xml');
    $XML = '<?xml version="1.0" encoding="utf-8"?>';
    $XML .= '<response>';

    foreach ($ArrayOutput as $Key => $Value) {
        if (strtolower(gettype($Value)) == 'array') {
            if (is_numeric($Key) == true)
                $Key = 'node_' . $Key;
            $XML .= '<' . $Key . '>';
            foreach ($Value as $SubKey => $SubValue) {
                if (strtolower(gettype($SubValue)) == 'array') {
                    if (is_numeric($SubKey) == true)
                        $SubKey = 'subnode_' . $SubKey;
                    $XML .= '<' . $SubKey . '>';
                    foreach ($SubValue as $Sub2Key => $Sub2Value) {
                        if (strtolower(gettype($Sub2Value)) == 'array') {
                            if ((is_numeric($Sub2Key) == true) || (eregi('a-z', $Sub2Key) == false)) {
                                $XML .= '<subnode2 value="' . $Sub2Key . '">';
                                foreach ($Sub2Value as $Sub3Key => $Sub3Value) {
                                    if (is_numeric($Sub3Key) == true)
                                        $Sub3Key = 'subnode3_' . $Sub3Key;
                                    $XML .= '<' . $Sub3Key . '><![CDATA[' . $Sub3Value . ']]></' . $Sub3Key . '>';
                                }
                                $XML .= '</subnode2>';
                            }
                            else {
                                if (is_numeric($Sub2Key) == true)
                                    $Sub2Key = 'subnode2_' . $Sub2Key;
                                $XML .= '<' . $Sub2Key . '>';
                                foreach ($Sub2Value as $Sub3Key => $Sub3Value) {
                                    if (is_numeric($Sub3Key) == true)
                                        $Sub3Key = 'subnode3_' . $Sub3Key;
                                    $XML .= '<' . $Sub3Key . '><![CDATA[' . $Sub3Value . ']]></' . $Sub3Key . '>';
                                }
                                $XML .= '</' . $Sub2Key . '>';
                            }
                        }
                        else {
                            if ((is_numeric($Sub2Key) == true) || (eregi('a-z', $Sub2Key) == false)) {
                                $XML .= '<subnode2 value="' . $Sub2Key . '"><![CDATA[' . $Sub2Value . ']]></subnode2>';
                            } else {
                                $XML .= '<' . $Sub2Key . '><![CDATA[' . $Sub2Value . ']]></' . $Sub2Key . '>';
                            }
                        }
                    }
                    $XML .= '</' . $SubKey . '>';
                } else {
                    if (is_numeric($SubKey) == true)
                        $SubKey = 'subnode_' . $SubKey;
                    $XML .= '<' . $SubKey . '><![CDATA[' . $SubValue . ']]></' . $SubKey . '>';
                }
            }
            $XML .= '</' . $Key . '>';
        }
        else {
            if (is_numeric($Key) == true)
                $Key = 'node_' . $Key;
            $XML .= '<' . $Key . '><![CDATA[' . $Value . ']]></' . $Key . '>';
        }
    }
    $XML .= '</response>';
    print $XML;
}
// Display the request output on the screen - End

exit;
?>