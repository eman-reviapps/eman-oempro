<?php
// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('campaigns');
Core::LoadObject('statistics');
Core::LoadObject('media_library');
Core::LoadObject('users');
// Load other modules - End

// Decrypt URL parameters - Start
if ($_GET['p'] != '')
	{
	$ArrayParameters = Core::DecryptURL($_GET['p']);
	}
else
	{
	$ArrayParameters = $_GET;
	}
// Decrypt URL parameters - End

// Retrieve media - Start
$mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
$file = $mediaLibraryFileMapper->findById($ArrayParameters['MediaID']);
if ($file == false || $file->getUserId() != $ArrayParameters['UserID'])
	{
	print('Media not found');
	exit;
	}
// Retrieve media - End

// Retrieve user information - Start
if ($ArrayParameters['UserID'] > 0)
	{
	$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayParameters['UserID']));
	if ($ArrayUser == false)
		{
		print('User not found');
		exit;
		}		
	}
else
	{
	$ArrayUser = array('UserID' => 0);
	}
// Retrieve user information - End

// Display media - Start
if ($file->getData() == 'file')
	{
	// Media stored in file system
	// print DATA_PATH.'media/'.md5($ArrayMedia['MediaID']);
	// exit;
	header('Content-Type: '.$file->getType());
	$FileHandler = fopen(DATA_PATH.'media/'.md5($file->getId()), 'rb');
		fpassthru($FileHandler);
	fclose($FileHandler);
	exit;
	}
elseif ($file->getData() == 's3')
	{
	// Media stored in S3 servers	
	header('Location: '.S3_URL.'/'.S3_MEDIALIBRARY_PATH.'/'.md5($ArrayUser['UserID']).'/'.md5($file->getId()));
	exit;
	}
elseif ($file->getData() != '')
	{
	// Media stored in Sendloop database
	header('Content-Type: '.$file->getType());
	print (base64_decode($file->getData()));
	exit;
	}
else
	{
	exit;
	}
// Display media - End

?>