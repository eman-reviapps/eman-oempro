<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * Email open tracking module
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('campaigns');
Core::LoadObject('emails');
Core::LoadObject('auto_responders');
Core::LoadObject('statistics');
// Load other modules - End

// Decrypt URL parameters - Start
$TargetURL = Core::GetShortenLink($_GET['p']);
// Decrypt URL parameters - End

header('Location: '.$TargetURL);

exit;


?>