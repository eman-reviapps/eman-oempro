<?php
/**
 * Report Abuse screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Report Abuse Email';
$ArrayLanguageStrings['Screen']['0002']	= 'Report abuse email you have received to Abuse Department';
$ArrayLanguageStrings['Screen']['0003']	= 'If you believe that you have received an abuse email from one of our customers, please submit your abuse report by using the form below. We will need Message-ID code which is included in every email that is being sent by our customers. You can find it in the email header. We can remove your email address from all our user databases if you want. Also, if you want a response from our Abuse Department, please provide your name and email address in the following form. Your confidential information is important for us. We never sell or distribute your information with third parties.';
$ArrayLanguageStrings['Screen']['0004']	= 'Message ID';
$ArrayLanguageStrings['Screen']['0005']	= 'Reason For Report, Comments';
$ArrayLanguageStrings['Screen']['0006']	= 'Full Name';
$ArrayLanguageStrings['Screen']['0007']	= 'Email Address';
$ArrayLanguageStrings['Screen']['0008']	= 'Report Abuse';
$ArrayLanguageStrings['Screen']['0009']	= 'Missing message ID';
$ArrayLanguageStrings['Screen']['0010']	= 'Please share your comments with us';
$ArrayLanguageStrings['Screen']['0011']	= 'Please enter your name';
$ArrayLanguageStrings['Screen']['0012']	= 'Please enter your email address';
$ArrayLanguageStrings['Screen']['0013']	= 'Please fill in all fields';
$ArrayLanguageStrings['Screen']['0014']	= 'Email address is invalid';
$ArrayLanguageStrings['Screen']['0015']	= 'ABUSE REPORT - %s';
$ArrayLanguageStrings['Screen']['0016']	= "%s\n\n\n-----------------------\nReported By: %s (%s)\n-----------------------\nCampaign ID: %s\nSubscriber ID: %s\nEmail Address: %s\nList ID: %s\nUser ID: %s\n-----------------------\n";
$ArrayLanguageStrings['Screen']['0017']	= 'Thank You!';
$ArrayLanguageStrings['Screen']['0018']	= 'We received your abuse complaint. We will investigate the email you have received and take the correct action against the sender.';
$ArrayLanguageStrings['Screen']['0019']	= '';

?>