<?php
/**
 * Login screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Username';
$ArrayLanguageStrings['Screen']['0002']	= 'Password';
$ArrayLanguageStrings['Screen']['0003']	= 'Forgot password? <a href="./forgot_password.php">Click here</a>.';
$ArrayLanguageStrings['Screen']['0004']	= 'Remember me on next login';
$ArrayLanguageStrings['Screen']['0005']	= 'Login';
$ArrayLanguageStrings['Screen']['0006']	= 'Please Login';
$ArrayLanguageStrings['Screen']['0007']	= 'Username is missing';
$ArrayLanguageStrings['Screen']['0008']	= 'Password is missing';
$ArrayLanguageStrings['Screen']['0009']	= 'Invalid username or password';
$ArrayLanguageStrings['Screen']['0010']	= 'Verification';
$ArrayLanguageStrings['Screen']['0011']	= 'Please enter the text in image above';
$ArrayLanguageStrings['Screen']['0012']	= 'Please enter the text in image below';
$ArrayLanguageStrings['Screen']['0013']	= 'Text you entered and text in the image does not match. Try again.';
$ArrayLanguageStrings['Screen']['0014']	= 'Email Address';
$ArrayLanguageStrings['Screen']['0015']	= 'Email address is invalid';
$ArrayLanguageStrings['Screen']['0016']	= 'Email address is missing';
$ArrayLanguageStrings['Screen']['0017']	= 'Subscription validation failed';
$ArrayLanguageStrings['Screen']['0018']	= 'Login information is invalid';

?>