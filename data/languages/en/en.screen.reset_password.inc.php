<?php
/**
 * Password eset screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Password Reset';
$ArrayLanguageStrings['Screen']['0002']	= 'Go to login screen';
$ArrayLanguageStrings['Screen']['0003']	= 'User information is missing';
$ArrayLanguageStrings['Screen']['0004']	= 'Invalid user information';
$ArrayLanguageStrings['Screen']['0005']	= 'An email including your new password is sent to your email address.';

$ArrayLanguageStrings['Email']['0001']	= 'Password reset';
$ArrayLanguageStrings['Email']['0002']	= "We have created a new password for you upon your request.\n\nUsername:%s\nNew password:%s";
?>