<?php
/**
 * @author Cem Hurturk
 * @copyright Octeth.com
 **/

/**
 * Main language configuration file
 **/

// Important: Do not change this line! - Start
if (! isset($ArrayLanguageStrings)) $$ArrayLanguageStrings = array();
// Important: Do not change this line! - End

$ArrayLanguageStrings['Config']['Charset']				= 'utf-8';
$ArrayLanguageStrings['Config']['OriginalName']			= 'English';
$ArrayLanguageStrings['Config']['EnglishName']			= 'English';
$ArrayLanguageStrings['Config']['LongDateFormat']		= 'jS F, Y';
$ArrayLanguageStrings['Config']['ShortDateFormat']		= 'jS M, y';
$ArrayLanguageStrings['Config']['TimeFormat']			= 'g:ia';

$ArrayLanguageStrings['Screen']['9000']	= 'Select';
$ArrayLanguageStrings['Screen']['9001']	= 'All';
$ArrayLanguageStrings['Screen']['9002']	= 'None';
$ArrayLanguageStrings['Screen']['9003']	= 'Inverse';
$ArrayLanguageStrings['Screen']['9004']	= 'Auto responders';
$ArrayLanguageStrings['Screen']['9005']	= "Loading, please wait...";
$ArrayLanguageStrings['Screen']['9006']	= 'Create';
$ArrayLanguageStrings['Screen']['9007']	= 'Save';
$ArrayLanguageStrings['Screen']['9008']	= 'Copy';
$ArrayLanguageStrings['Screen']['9009']	= 'Send';
$ArrayLanguageStrings['Screen']['9010']	= 'Search';
$ArrayLanguageStrings['Screen']['9011']	= 'Order by';
$ArrayLanguageStrings['Screen']['9012']	= 'Browse';
$ArrayLanguageStrings['Screen']['9013']	= "Pages";
$ArrayLanguageStrings['Screen']['9014']	= "Active subscribers";
$ArrayLanguageStrings['Screen']['9015']	= "Suppression list";
$ArrayLanguageStrings['Screen']['9016']	= "Ascending";
$ArrayLanguageStrings['Screen']['9017']	= "Descending";
$ArrayLanguageStrings['Screen']['9018']	= 'Browse subscribers';
$ArrayLanguageStrings['Screen']['9019']	= 'An error occurred while trying to process your request. Please try again.';
$ArrayLanguageStrings['Screen']['9020']	= "Email address";
$ArrayLanguageStrings['Screen']['9021']	= 'Custom fields';
$ArrayLanguageStrings['Screen']['9022']	= 'Export subscribers';
$ArrayLanguageStrings['Screen']['9023']	= 'Remove subscribers';
$ArrayLanguageStrings['Screen']['9024']	= 'Segments';
$ArrayLanguageStrings['Screen']['9025']	= 'Settings';
$ArrayLanguageStrings['Screen']['9026']	= 'Web service integration';
$ArrayLanguageStrings['Screen']['9027']	= 'Save settings';
$ArrayLanguageStrings['Screen']['9028']	= 'Website integration';
$ArrayLanguageStrings['Screen']['9029']	= 'Edit subscriber information';
$ArrayLanguageStrings['Screen']['9030']	= 'Import subscribers';
$ArrayLanguageStrings['Screen']['9031']	= 'Campaign Settings';
$ArrayLanguageStrings['Screen']['9032']	= 'Campaign Email';
$ArrayLanguageStrings['Screen']['9033']	= 'Preview';
$ArrayLanguageStrings['Screen']['9034']	= 'Email Builder';
$ArrayLanguageStrings['Screen']['9035']	= 'Custom Field: ';
$ArrayLanguageStrings['Screen']['9036']	= array(
												'SubscriberID'			=> 'Subscriber ID',
												'EmailAddress'			=> 'Email Address',
												'BounceType'			=> 'Bounce Type',
												'SubscriptionStatus'	=> 'Subscription Status',
												'SubscriptionDate'		=> 'Subscription Date',
												'SubscriptionIP'		=> 'Subscription IP',
												'UnsubscriptionDate'	=> 'Unsubscription Date',
												'UnsubscriptionIP'		=> 'Unsubscription IP',
												'OptInDate'				=> 'Opt-In Date',
												);
$ArrayLanguageStrings['Screen']['9037']	= array(
												'%Link:Confirm%'		=> 'Opt-In Confirmation Link',
												'%Link:Reject%'			=> 'Opt-In Reject Link',
												'%Link:Unsubscribe%'	=> 'Unsubscription Link',
												'%Link:SubscriberArea%'	=> 'Subscriber Area Link',
												'%Link:Forward%'		=> 'Forward To Friend Link',
												'%Link:WebBrowser%'		=> 'View In Web Browser Link',
												'%RemoteContent%'		=> 'Remote Content'
												);
$ArrayLanguageStrings['Screen']['9038']	= array(
												'%User:FirstName%'		=> 'First Name',
												'%User:LastName%'		=> 'Last Name',
												'%User:EmailAddress%'	=> 'Email Address',
												'%User:CompanyName%'	=> 'Company Name',
												'%User:Website%'		=> 'Website URL',
												'%User:Street%'			=> 'Street',
												'%User:City%'			=> 'City',
												'%User:State%'			=> 'State',
												'%User:Zip%'			=> 'Zip',
												'%User:Country%'		=> 'Country',
												'%User:Phone%'			=> 'Phone',
												'%User:Fax%'			=> 'Fax',
												'%User:TimeZone%'		=> 'Time Zone',
												);
$ArrayLanguageStrings['Screen']['9039']	= 'FBL Message Received (FAILED - USER NOT FOUND)';
$ArrayLanguageStrings['Screen']['9040']	= "The following FBL message received from %s email address and but could NOT be processed because user information not found in the database:\n\nCampaign ID: %s\nSubscriber ID: %s\nEmail Address: %s\nList ID: %s\nUser ID: %s\n\n------------------\n\n%s";
$ArrayLanguageStrings['Screen']['9041']	= "FBL Message Received but couldn't be processed (FORMAT NOT COMPATIBLE)";
$ArrayLanguageStrings['Screen']['9042']	= "The following FBL message received but couldn't be processed:\n\n------------------\n\n%s";
$ArrayLanguageStrings['Screen']['9043']	= 'FBL Message Received (FAILED - CAMPAIGN, LIST OR SUBSCRIBER NOT FOUND)';
$ArrayLanguageStrings['Screen']['9044']	= "The following FBL message received from %s email address and but could NOT be processed because campaign, list or subscriber information not found in the database:\n\nCampaign ID: %s\nSubscriber ID: %s\nEmail Address: %s\nList ID: %s\nUser ID: %s\n\n------------------\n\n%s";
$ArrayLanguageStrings['Screen']['9045']	= "BOUNCE DETECTION FAILED";
$ArrayLanguageStrings['Screen']['9046']	= "The following bounce delivery report couldn't be processed:\n\n%s\n\n-----------------\nTo: %s\nFrom: %s\nBounced Email Address: %s\nX-Message ID: %s\nSubject: %s\nParsed X-Message ID: %s\n\n-----------------\nRaw Email Content:\n\n%s";
$ArrayLanguageStrings['Screen']['9047']	= "BOUNCED SUBSCRIBER DETECTION FAILED";
$ArrayLanguageStrings['Screen']['9048']	= "Subscriber, list, campaign or user information couldn't be found in the following bounce message body:\n\n%s\n\n-----------------\nTo: %s\nFrom: %s\nBounced Email Address: %s\nX-Message ID: %s\nSubject: %s\nParsed X-Message ID: %s\n\n-----------------\nRaw Email Content:\n\n%s\n\n-----------------\nParsed Email Content:\n\n%s";
$ArrayLanguageStrings['Screen']['9049']	= array(
												'Monday'		=> 'Monday',
												'Tuesday'		=> 'Tuesday',
												'Wednesday'		=> 'Wednesday',
												'Thursday'		=> 'Thursday',
												'Friday'		=> 'Friday',
												'Saturday'		=> 'Saturday',
												'Sunday'		=> 'Sunday'
												);
$ArrayLanguageStrings['Screen']['9050']	= "First Name";
$ArrayLanguageStrings['Screen']['9051']	= "Last Name";
$ArrayLanguageStrings['Screen']['9052']	= array(
												"AD"=>"Andorra",
												"AE"=>"United Arab Emirates",
												"AF"=>"Afghanistan",
												"AG"=>"Antigua & Barbuda",
												"AI"=>"Anguilla",
												"AL"=>"Albania",
												"AM"=>"Armenia",
												"AN"=>"Netherlands Antilles",
												"AO"=>"Angola",
												"AQ"=>"Antarctica",
												"AR"=>"Argentina",
												"AS"=>"American Samoa",
												"AT"=>"Austria",
												"AU"=>"Australia",
												"AW"=>"Aruba",
												"AZ"=>"Azerbaijan",
												"BA"=>"Bosnia and Herzegovina",
												"BB"=>"Barbados",
												"BD"=>"Bangladesh",
												"BE"=>"Belgium",
												"BF"=>"Burkina Faso",
												"BG"=>"Bulgaria",
												"BH"=>"Bahrain",
												"BI"=>"Burundi",
												"BJ"=>"Benin",
												"BM"=>"Bermuda",
												"BN"=>"Brunei Darussalam",
												"BO"=>"Bolivia",
												"BR"=>"Brazil",
												"BS"=>"Bahama",
												"BT"=>"Bhutan",
												"BV"=>"Bouvet Island",
												"BW"=>"Botswana",
												"BY"=>"Belarus",
												"BZ"=>"Belize",
												"CA"=>"Canada",
												"CC"=>"Cocos (Keeling) Islands",
												"CF"=>"Central African Republic",
												"CG"=>"Congo",
												"CH"=>"Switzerland",
												"CI"=>"Côte D'ivoire (Ivory Coast)",
												"CK"=>"Cook Iislands",
												"CL"=>"Chile",
												"CM"=>"Cameroon",
												"CN"=>"China",
												"CO"=>"Colombia",
												"CR"=>"Costa Rica",
												"CU"=>"Cuba",
												"CV"=>"Cape Verde",
												"CX"=>"Christmas Island",
												"CY"=>"Cyprus",
												"CZ"=>"Czech Republic",
												"DE"=>"Germany",
												"DJ"=>"Djibouti",
												"DK"=>"Denmark",
												"DM"=>"Dominica",
												"DO"=>"Dominican Republic",
												"DZ"=>"Algeria",
												"EC"=>"Ecuador",
												"EE"=>"Estonia",
												"EG"=>"Egypt",
												"EH"=>"Western Sahara",
												"ER"=>"Eritrea",
												"ES"=>"Spain",
												"ET"=>"Ethiopia",
												"FI"=>"Finland",
												"FJ"=>"Fiji",
												"FK"=>"Falkland Islands (Malvinas)",
												"FM"=>"Micronesia",
												"FO"=>"Faroe Islands",
												"FR"=>"France",
												"FX"=>"France, Metropolitan",
												"GA"=>"Gabon",
												"GB"=>"United Kingdom (Great Britain)",
												"GD"=>"Grenada",
												"GE"=>"Georgia",
												"GF"=>"French Guiana",
												"GH"=>"Ghana",
												"GI"=>"Gibraltar",
												"GL"=>"Greenland",
												"GM"=>"Gambia",
												"GN"=>"Guinea",
												"GP"=>"Guadeloupe",
												"GQ"=>"Equatorial Guinea",
												"GR"=>"Greece",
												"GS"=>"South Georgia and the South Sandwich Islands",
												"GT"=>"Guatemala",
												"GU"=>"Guam",
												"GW"=>"Guinea-Bissau",
												"GY"=>"Guyana",
												"HK"=>"Hong Kong",
												"HM"=>"Heard & McDonald Islands",
												"HN"=>"Honduras",
												"HR"=>"Croatia",
												"HT"=>"Haiti",
												"HU"=>"Hungary",
												"ID"=>"Indonesia",
												"IE"=>"Ireland",
												"IL"=>"Israel",
												"IN"=>"India",
												"IO"=>"British Indian Ocean Territory",
												"IQ"=>"Iraq",
												"IR"=>"Islamic Republic of Iran",
												"IS"=>"Iceland",
												"IT"=>"Italy",
												"JM"=>"Jamaica",
												"JO"=>"Jordan",
												"JP"=>"Japan",
												"KE"=>"Kenya",
												"KG"=>"Kyrgyzstan",
												"KH"=>"Cambodia",
												"KI"=>"Kiribati",
												"KM"=>"Comoros",
												"KN"=>"St. Kitts and Nevis",
												"KP"=>"Korea, Democratic People's Republic of",
												"KR"=>"Korea, Republic of",
												"KW"=>"Kuwait",
												"KY"=>"Cayman Islands",
												"KZ"=>"Kazakhstan",
												"LA"=>"Lao People's Democratic Republic",
												"LB"=>"Lebanon",
												"LC"=>"Saint Lucia",
												"LI"=>"Liechtenstein",
												"LK"=>"Sri Lanka",
												"LR"=>"Liberia",
												"LS"=>"Lesotho",
												"LT"=>"Lithuania",
												"LU"=>"Luxembourg",
												"LV"=>"Latvia",
												"LY"=>"Libyan Arab Jamahiriya",
												"MA"=>"Morocco",
												"MC"=>"Monaco",
												"MD"=>"Moldova, Republic of",
												"MG"=>"Madagascar",
												"MH"=>"Marshall Islands",
												"ML"=>"Mali",
												"MN"=>"Mongolia",
												"MM"=>"Myanmar",
												"MO"=>"Macau",
												"MP"=>"Northern Mariana Islands",
												"MQ"=>"Martinique",
												"MR"=>"Mauritania",
												"MS"=>"Monserrat",
												"MT"=>"Malta",
												"MU"=>"Mauritius",
												"MV"=>"Maldives",
												"MW"=>"Malawi",
												"MX"=>"Mexico",
												"MY"=>"Malaysia",
												"MZ"=>"Mozambique",
												"NA"=>"Namibia",
												"NC"=>"New Caledonia",
												"NE"=>"Niger",
												"NF"=>"Norfolk Island",
												"NG"=>"Nigeria",
												"NI"=>"Nicaragua",
												"NL"=>"Netherlands",
												"NO"=>"Norway",
												"NP"=>"Nepal",
												"NR"=>"Nauru",
												"NU"=>"Niue",
												"NZ"=>"New Zealand",
												"OM"=>"Oman",
												"PA"=>"Panama",
												"PE"=>"Peru",
												"PF"=>"French Polynesia",
												"PG"=>"Papua New Guinea",
												"PH"=>"Philippines",
												"PK"=>"Pakistan",
												"PL"=>"Poland",
												"PM"=>"St. Pierre & Miquelon",
												"PN"=>"Pitcairn",
												"PR"=>"Puerto Rico",
												"PT"=>"Portugal",
												"PW"=>"Palau",
												"PY"=>"Paraguay",
												"QA"=>"Qatar",
												"RE"=>"Réunion",
												"RO"=>"Romania",
												"RU"=>"Russian Federation",
												"RW"=>"Rwanda",
												"SA"=>"Saudi Arabia",
												"SB"=>"Solomon Islands",
												"SC"=>"Seychelles",
												"SD"=>"Sudan",
												"SE"=>"Sweden",
												"SG"=>"Singapore",
												"SH"=>"St. Helena",
												"SI"=>"Slovenia",
												"SJ"=>"Svalbard & Jan Mayen Islands",
												"SK"=>"Slovakia",
												"SL"=>"Sierra Leone",
												"SM"=>"San Marino",
												"SN"=>"Senegal",
												"SO"=>"Somalia",
												"SR"=>"Suriname",
												"ST"=>"Sao Tome & Principe",
												"SV"=>"El Salvador",
												"SY"=>"Syrian Arab Republic",
												"SZ"=>"Swaziland",
												"TC"=>"Turks & Caicos Islands",
												"TD"=>"Chad",
												"TF"=>"French Southern Territories",
												"TG"=>"Togo",
												"TH"=>"Thailand",
												"TJ"=>"Tajikistan",
												"TK"=>"Tokelau",
												"TM"=>"Turkmenistan",
												"TN"=>"Tunisia",
												"TO"=>"Tonga",
												"TP"=>"East Timor",
												"TR"=>"Turkey",
												"TT"=>"Trinidad & Tobago",
												"TV"=>"Tuvalu",
												"TW"=>"Taiwan, Province of China",
												"TZ"=>"Tanzania, United Republic of",
												"UA"=>"Ukraine",
												"UG"=>"Uganda",
												"UM"=>"United States Minor Outlying Islands",
												"US"=>"United States of America",
												"UY"=>"Uruguay",
												"UZ"=>"Uzbekistan",
												"VA"=>"Vatican City State (Holy See)",
												"VC"=>"St. Vincent & the Grenadines",
												"VE"=>"Venezuela",
												"VG"=>"British Virgin Islands",
												"VI"=>"United States Virgin Islands",
												"VN"=>"Viet Nam",
												"VU"=>"Vanuatu",
												"WF"=>"Wallis & Futuna Islands",
												"WS"=>"Samoa",
												"YE"=>"Yemen",
												"YT"=>"Mayotte",
												"YU"=>"Yugoslavia",
												"ZA"=>"South Africa",
												"ZM"=>"Zambia",
												"ZR"=>"Zaire",
												"ZW"=>"Zimbabwe"
												);
$ArrayLanguageStrings['Screen']['9053']	= "Synchronization";
$ArrayLanguageStrings['Screen']['9054']	= "Your subscriber list synchronized";
$ArrayLanguageStrings['Screen']['9055']	= "Your subscriber list %s is synchronized. %s subscribers imported/updated.";
$ArrayLanguageStrings['Screen']['9056']	= array(
												'January'		=> 'January',
												'February'		=> 'February',
												'March'			=> 'March',
												'April'			=> 'April',
												'May'			=> 'May',
												'June'			=> 'June',
												'July'			=> 'July',
												'August'		=> 'August',
												'September'		=> 'September',
												'October'		=> 'October',
												'November'		=> 'November',
												'December'		=> 'December'
												);
$ArrayLanguageStrings['Screen']['9057']	= "You don't have permission.";
$ArrayLanguageStrings['Screen']['9058']	= "Not Enough Privileges";
$ArrayLanguageStrings['Screen']['9059']	= "You don't have enough privileges to access this section.";
$ArrayLanguageStrings['Screen']['9060']	= "Subscriber Lists";
$ArrayLanguageStrings['Screen']['9061']	= "Campaigns";
$ArrayLanguageStrings['Screen']['9062']	= "Clients";
$ArrayLanguageStrings['Screen']['9063']	= "Settings";
$ArrayLanguageStrings['Screen']['9064']	= "Account Information";
$ArrayLanguageStrings['Screen']['9065']	= "Logout";
$ArrayLanguageStrings['Screen']['9066']	= "Media Library";
$ArrayLanguageStrings['Screen']['9067']	= "Suppression list";
$ArrayLanguageStrings['Screen']['9068']	= 'M j, Y g:i a';
$ArrayLanguageStrings['Screen']['9069']	= 'D, j M g:i a';
$ArrayLanguageStrings['Screen']['9070']	= 'g:i a';
$ArrayLanguageStrings['Screen']['9071']	= 'Today, ';
$ArrayLanguageStrings['Screen']['9072']	= 'Yesterday, ';
$ArrayLanguageStrings['Screen']['9073']	= array(
											'Email Open'		=> 'Opened email',
											'Link Click'		=> 'Clicked a link',
											'Browser View'		=> 'Viewed on web browser',
											'Email Forwarded'	=> 'Forwarded to a friend',
											'Unsubscribed'		=> 'Unsubscribed',
											'Hard Bounced'		=> 'Hard bounced - Email address invalid',
											'Soft Bounced'		=> 'Soft bounced - Email address temporarily offline',
											);
$ArrayLanguageStrings['Screen']['9074']	= 'Auto Responder';
$ArrayLanguageStrings['Screen']['9075']	= 'Users';
$ArrayLanguageStrings['Screen']['9076']	= 'User Groups';
$ArrayLanguageStrings['Screen']['9077']	= 'Overview';
$ArrayLanguageStrings['Screen']['9078']	= 'Newsletter Templates';
$ArrayLanguageStrings['Screen']['9079']	= 'Payment Reports';
$ArrayLanguageStrings['Screen']['9080']	= 'Campaign Emails Sent: ';
$ArrayLanguageStrings['Screen']['9081']	= 'Auto Responder Emails Sent: ';
$ArrayLanguageStrings['Screen']['9082']	= 'Total Campaigns Sent: ';
$ArrayLanguageStrings['Screen']['9083']	= 'Total Design Preview Requests: ';
$ArrayLanguageStrings['Screen']['9084']	= 'Auto Responder Monthly Fee: ';
$ArrayLanguageStrings['Screen']['9085']	= 'Design Preview Monthly Fee: ';
$ArrayLanguageStrings['Screen']['9086']	= 'System Monthly Fee: ';
$ArrayLanguageStrings['Screen']['9087']	= 'Discount: ';
$ArrayLanguageStrings['Screen']['9088']	= 'Tax: ';
$ArrayLanguageStrings['Screen']['9089']	= 'Total Amount: ';
$ArrayLanguageStrings['Screen']['9090']	= 'Email Settings';
$ArrayLanguageStrings['Screen']['9091']	= 'ESP Settings';
$ArrayLanguageStrings['Screen']['9092']	= 'Preferences';
$ArrayLanguageStrings['Screen']['9093']	= 'Rebranding';
$ArrayLanguageStrings['Screen']['9094']	= 'Your '.PRODUCT_NAME.' license does not support this functionality. ESP license is required.';
$ArrayLanguageStrings['Screen']['9095']	= 'Loading user interface, please wait...';
$ArrayLanguageStrings['Screen']['9096']	= 'Plug-in Manager';
$ArrayLanguageStrings['Screen']['9097']	= 'Target path for creating email files is incorrect or not writeable.';
$ArrayLanguageStrings['Screen']['9098']	= 'Account Information';
$ArrayLanguageStrings['Screen']['9099']	= 'Subscriber information';
$ArrayLanguageStrings['Screen']['9100']	= 'Campaign links';
$ArrayLanguageStrings['Screen']['9101']	= 'Opt-in Confirmation Links';
$ArrayLanguageStrings['Screen']['9102']	= 'Subscriber list links';
$ArrayLanguageStrings['Screen']['9103']	= 'Links';
$ArrayLanguageStrings['Screen']['9104']	= 'User information';
$ArrayLanguageStrings['Screen']['9105']	= 'About';
$ArrayLanguageStrings['Screen']['9106']	= 'Unlimited';
$ArrayLanguageStrings['Screen']['9107']	= 'Default Opt-In Email';
$ArrayLanguageStrings['Screen']['9108']	= 'Please confirm your subscription';
$ArrayLanguageStrings['Screen']['9109']	= "Thank you for subscribing to our list. Please confirm your subscription by clicking the following link:\n\n%Link:Confirm%\n\nIf you have subscribed by mistake, you can cancel your subscription request by clicking the following link:\n\n%Link:Reject%";
$ArrayLanguageStrings['Screen']['9110']	= "RSS feed for %s emails";
$ArrayLanguageStrings['Screen']['9111']	= "Not updated";
$ArrayLanguageStrings['Screen']['9112']	= "Email Templates";
$ArrayLanguageStrings['Screen']['9113']	= "Email Settings Test Email";
$ArrayLanguageStrings['Screen']['9114']	= "<p>This is a test email sent for testing your new ".PRODUCT_NAME." email settings</p><p>Please delete this message.</p>";
$ArrayLanguageStrings['Screen']['9115']	= "This is a test email sent for testing your new ".PRODUCT_NAME." email settings.\n\nPlease delete this message.";
$ArrayLanguageStrings['Screen']['9116']	= "Duplicate of %s";
$ArrayLanguageStrings['Screen']['9117']	= '&copy;Copyright Octeth Ltd. All rights reserved. For customer service or community forum, please visit <a href="http://octeth.com/clientarea/" target="_blank">http://octeth.com</a>';
$ArrayLanguageStrings['Screen']['9118']	= 'Subscribers';
$ArrayLanguageStrings['Screen']['9119']	= 'Unsubscriptions';
$ArrayLanguageStrings['Screen']['9120']	= 'Total Clicks';
$ArrayLanguageStrings['Screen']['9121']	= 'Unique Clicks';
$ArrayLanguageStrings['Screen']['9122']	= 'Total Opens';
$ArrayLanguageStrings['Screen']['9123']	= 'Unique Opens';
$ArrayLanguageStrings['Screen']['9124']	= 'Total Forwards';
$ArrayLanguageStrings['Screen']['9125']	= 'Unique Forwards';
$ArrayLanguageStrings['Screen']['9126']	= 'Total Browser Views';
$ArrayLanguageStrings['Screen']['9127']	= 'Unique Browser Views';
$ArrayLanguageStrings['Screen']['9128']	= 'Not Bounced';
$ArrayLanguageStrings['Screen']['9129']	= 'Total Income';
$ArrayLanguageStrings['Screen']['9130']	= 'Average Income';
$ArrayLanguageStrings['Screen']['9131']	= 'Redirecting to PayPal, please wait...';
$ArrayLanguageStrings['Screen']['9132']	= 'PayPal:';
$ArrayLanguageStrings['Screen']['9133']	= 'Click the following link to pay through your PayPal account';
$ArrayLanguageStrings['Screen']['9134']	= 'Browse all subscriber lists';
$ArrayLanguageStrings['Screen']['9135']	= 'Browse all campaigns';
$ArrayLanguageStrings['Screen']['9136']	= 'Manage';
$ArrayLanguageStrings['Screen']['9137']	= 'Tags';
$ArrayLanguageStrings['Screen']['9138']	= "Global Suppression List";
$ArrayLanguageStrings['Screen']['9139']	= "Emails";
$ArrayLanguageStrings['Screen']['9140']	= "Copy of %s";
$ArrayLanguageStrings['Screen']['9141']	= "Never";
$ArrayLanguageStrings['Screen']['9142']	= "Conditional Personalization";
$ArrayLanguageStrings['Screen']['9143']	= "Create a new email";
$ArrayLanguageStrings['Screen']['9144']	= "Duplicate";
$ArrayLanguageStrings['Screen']['9145']	= "Edit";
$ArrayLanguageStrings['Screen']['9146']	= "Delete";
$ArrayLanguageStrings['Screen']['9147']	= "Choose an email";
$ArrayLanguageStrings['Screen']['9148']	= "Upgrade Utility";
$ArrayLanguageStrings['Screen']['9149']	= array(
												'Date'			=> 'Email delivery date',
												);
$ArrayLanguageStrings['Screen']['9150']	= "Other Tags";
$ArrayLanguageStrings['Screen']['9151']	= array(
												'st'			=> 'st',
												'nd'			=> 'nd',
												'rd'			=> 'rd',
												'th'			=> 'th',
												);
$ArrayLanguageStrings['Screen']['9152']	= "day";
$ArrayLanguageStrings['Screen']['9153']	= "opens";
$ArrayLanguageStrings['Screen']['9154']	= "clicks";
$ArrayLanguageStrings['Screen']['9155']	= "unsubscriptions";
$ArrayLanguageStrings['Screen']['9156']	= "forwards";
$ArrayLanguageStrings['Screen']['9157']	= "browser views";
$ArrayLanguageStrings['Screen']['9158']	= "bounces";
$ArrayLanguageStrings['Screen']['9159']	= "Global Custom Field";
$ArrayLanguageStrings['Screen']['9160']	= "Update Available! Download ".PRODUCT_NAME." v%s!";
$ArrayLanguageStrings['Screen']['9161']	= 'You agree to our <a href="http://octeth.com/products/oempro/sla.php" target="_blank">End User License Agreement (EULA)</a> by purchasing, downloading, installing and using this software.';
$ArrayLanguageStrings['Screen']['9162']	= '(record removed)';
$ArrayLanguageStrings['Screen']['9163']	= 'User import threshold exceeded';
$ArrayLanguageStrings['Screen']['9164']	= "Following user has exceeded import threshold:\n\nUser ID: %s\nName: %s\nEmail Address: %s\nImport Threshold: %s subscribers\nImported Subscribers: %s";
$ArrayLanguageStrings['Screen']['9165']	= 'User total campaign recipient threshold exceeded';
$ArrayLanguageStrings['Screen']['9166']	= "Following user has exceeded total campaign recipient threshold:\n\nUser ID: %s\nName: %s\nEmail Address: %s\nCampaign Recipient Threshold: %s recipients\nTotal Recipients: %s\nCampaign ID: #%s";
$ArrayLanguageStrings['Screen']['9167']	= "Subscription not confirmed yet";
$ArrayLanguageStrings['Screen']['9168']	= "System Check";
$ArrayLanguageStrings['Screen']['9169']	= "System Check Failure! Click here.";
$ArrayLanguageStrings['Screen']['9170']	= 'Catch-all email address domain for bounce handling is not set. It can be changed from <a href="./email_settings.php">Settings &gt; Email Settings &gt; Bounce Settings</a> tab.';
$ArrayLanguageStrings['Screen']['9171']	= 'Catch-all email address domain has been entered incorrectly. It should be a domain (Ex: yourdomain.com) instead of email@yourdomain.com. It can be changed from <a href="./email_settings.php">Settings &gt; Email Settings &gt; Bounce Settings</a> tab.';
$ArrayLanguageStrings['Screen']['9172']	= 'Following errors found while running a system check:';
$ArrayLanguageStrings['Screen']['9173']	= 'No errors found. Oempro will run system check periodically to keep you updated about system alerts and errors.';
$ArrayLanguageStrings['Screen']['9174']	= 'Send Pending Campaigns';
$ArrayLanguageStrings['Screen']['9175']	= 'Browse Campaigns';
$ArrayLanguageStrings['Screen']['9176']	= 'Email piping test results - PASSED';
$ArrayLanguageStrings['Screen']['9177']	= "You have successfully set email piping. Here's the received email content:\n\n------\n\n%s\n\n------\n\n";





$ArrayLanguageStrings['Screen']['Subscribe']				= 'Subscribe';
$ArrayLanguageStrings['Screen']['Unsubscribe']				= 'Unsubscribe';
$ArrayLanguageStrings['Screen']['EmailAddress']				= 'Email Address';
$ArrayLanguageStrings['Screen']['Subscribers']				= 'Subscribers';
$ArrayLanguageStrings['Screen']['Statistics']				= 'Statistics';
$ArrayLanguageStrings['Screen']['SubscriberListSections']	= 'Subscriber List Sections';
$ArrayLanguageStrings['Screen']['CampaignSections']			= 'Campaign Sections';
$ArrayLanguageStrings['Screen']['SettingsSections']			= 'Settings Sections';
$ArrayLanguageStrings['Screen']['Section']					= 'Section';
$ArrayLanguageStrings['Screen']['Close']					= 'Close';
$ArrayLanguageStrings['Screen']['BounceType']				= "Bounce type";
$ArrayLanguageStrings['Screen']['OptInDate']				= "Opt-in date";
$ArrayLanguageStrings['Screen']['SubscriberID']				= "Subscriber ID";
$ArrayLanguageStrings['Screen']['SubscriptionIP']			= "Subscription ip";
$ArrayLanguageStrings['Screen']['SubscriptionDate']			= "Subscription date";
$ArrayLanguageStrings['Screen']['SubscriptionStatus']		= "Subscription status";
$ArrayLanguageStrings['Screen']['AccountInformation']		= "Account Information";
$ArrayLanguageStrings['Screen']['Pending Approval']			= "Pending approval";
$ArrayLanguageStrings['Screen']['Paused']					= "Paused";
$ArrayLanguageStrings['Screen']['Sending']					= "Sending";
$ArrayLanguageStrings['Screen']['Ready']					= "Ready";
$ArrayLanguageStrings['Screen']['Failed']					= "Failed";
$ArrayLanguageStrings['Screen']['Draft']					= "Draft";
$ArrayLanguageStrings['Screen']['Sent']						= "Sent";

$ArrayLanguageStrings['Screen']['EmailSelector_0001']	= 'Are you sure to delete selected email?';

$ArrayLanguageStrings['Screen']['EmailBuilder_0001']	= 'Name';
$ArrayLanguageStrings['Screen']['EmailBuilder_0002']	= 'Save';
$ArrayLanguageStrings['Screen']['EmailBuilder_0003']	= 'Discard';
$ArrayLanguageStrings['Screen']['EmailBuilder_0003']	= 'Discard';
$ArrayLanguageStrings['Screen']['EmailBuilder_0004']	= 'From';
$ArrayLanguageStrings['Screen']['EmailBuilder_0005']	= 'Name';
$ArrayLanguageStrings['Screen']['EmailBuilder_0006']	= 'Email address';
$ArrayLanguageStrings['Screen']['EmailBuilder_0007']	= 'Fetch content from a remote url';
$ArrayLanguageStrings['Screen']['EmailBuilder_0008']	= 'Embed images';
$ArrayLanguageStrings['Screen']['EmailBuilder_0009']	= 'Subject';
$ArrayLanguageStrings['Screen']['EmailBuilder_0010']	= 'Attachment file size exceeds your limit.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0011']	= 'Remove';
$ArrayLanguageStrings['Screen']['EmailBuilder_0012']	= 'Settings';
$ArrayLanguageStrings['Screen']['EmailBuilder_0013']	= 'Layout';
$ArrayLanguageStrings['Screen']['EmailBuilder_0014']	= 'Content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0015']	= 'Attachments';
$ArrayLanguageStrings['Screen']['EmailBuilder_0016']	= 'Preview';
$ArrayLanguageStrings['Screen']['EmailBuilder_0017']	= 'Empty';
$ArrayLanguageStrings['Screen']['EmailBuilder_0018']	= 'Blank template';
$ArrayLanguageStrings['Screen']['EmailBuilder_0019']	= 'Currently there are no attachments.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0020']	= 'Uploading attachment, please wait...';
$ArrayLanguageStrings['Screen']['EmailBuilder_0021']	= 'Removing attachment...';
$ArrayLanguageStrings['Screen']['EmailBuilder_0022']	= 'View plain content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0023']	= 'View html content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0024']	= 'Email saved';
$ArrayLanguageStrings['Screen']['EmailBuilder_0025']	= 'Please enter an import url for content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0026']	= 'Please enter a valid from email address';
$ArrayLanguageStrings['Screen']['EmailBuilder_0027']	= 'Please enter a reply-to from email address';
$ArrayLanguageStrings['Screen']['EmailBuilder_0028']	= 'Plain and HTML content can not be empty at the same time';
$ArrayLanguageStrings['Screen']['EmailBuilder_0029']	= 'Please enter a valid preview email address';
$ArrayLanguageStrings['Screen']['EmailBuilder_0030']	= 'Preview of the email is sent to your email address';
$ArrayLanguageStrings['Screen']['EmailBuilder_0031']	= 'When previewing via email and browser, a random recipient will be selected from your subscriber list and preview email will be personalized with this random recipient\'s data.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0032']	= 'Email Preview';
$ArrayLanguageStrings['Screen']['EmailBuilder_0033']	= 'Browser Preview';
$ArrayLanguageStrings['Screen']['EmailBuilder_0034']	= 'Spam Reports';
$ArrayLanguageStrings['Screen']['EmailBuilder_0035']	= 'Design Testing';
$ArrayLanguageStrings['Screen']['EmailBuilder_0036']	= '<a id="email-builder-browser-preview-link" href="" target="_blank">Click here</a> to preview on your browser (opens a new window)';
$ArrayLanguageStrings['Screen']['EmailBuilder_0037']	= 'Send preview';
$ArrayLanguageStrings['Screen']['EmailBuilder_0038']	= 'Genearate spam report for this email';
$ArrayLanguageStrings['Screen']['EmailBuilder_0039']	= 'Spam testing error. Please contact your administrator';
$ArrayLanguageStrings['Screen']['EmailBuilder_0040']	= 'Loading spam report, please wait...';
$ArrayLanguageStrings['Screen']['EmailBuilder_0041']	= 'Weight';
$ArrayLanguageStrings['Screen']['EmailBuilder_0042']	= 'Description';
$ArrayLanguageStrings['Screen']['EmailBuilder_0043']	= 'If this number is greater than 7.0, this email will possibly moved to junk folder';
$ArrayLanguageStrings['Screen']['EmailBuilder_0044']	= 'This email\'s content does not contain any spam related content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0045']	= 'An error occured while trying to fetch design preview information. Please contact your administrator.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0046']	= 'Design preview service is not available right now. Please contact your administrator.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0047']	= 'Available credits';
$ArrayLanguageStrings['Screen']['EmailBuilder_0048']	= 'Select a job to view';
$ArrayLanguageStrings['Screen']['EmailBuilder_0049']	= 'Create a new email design preview';
$ArrayLanguageStrings['Screen']['EmailBuilder_0050']	= array(
																'AOL'				=>	"America Online",
																'AOLDesktop10'		=>	"AOL Desktop 10",
																'GoogleStandard'	=>	"Google",
																'Hotmail'			=>	"Hotmail",
																'LotusNotes8'		=>	"Lotus Notes 8",
																'Thunderbird'		=>	"Mozilla Thunderbird",
																'Outlook2003'		=>	"Microsift Outlook 2003",
																'Outlook2007'		=>	"Microsift Outlook 2007",
																'OutlookExpress'	=>	"Microsift Outlook Express",
																'Yahoo'				=>	"Yahoo",
																'AppleDesktopMail'	=>	"Apple Mail",
																'applemail'			=>	"Apple Webmail (.mac / me)",
																'Entourage'			=>	"Microsoft Entourage (Mac OS X)",
																'Eudora'			=>	"Eudora (Mac OS X)",
																'Mailcom'			=>	"Mail.com",
																);
$ArrayLanguageStrings['Screen']['EmailBuilder_0051']	= 'Email Client';
$ArrayLanguageStrings['Screen']['EmailBuilder_0052']	= 'View';
$ArrayLanguageStrings['Screen']['EmailBuilder_0053']	= 'Please wait...';
$ArrayLanguageStrings['Screen']['EmailBuilder_0054']	= 'Images Off';
$ArrayLanguageStrings['Screen']['EmailBuilder_0055']	= 'Images On';
$ArrayLanguageStrings['Screen']['EmailBuilder_0056']	= 'Loading design previews, please wait...';
$ArrayLanguageStrings['Screen']['EmailBuilder_0057']	= 'Missing unsubscription link in HTML content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0058']	= 'Missing unsubscription link in plain content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0059']	= 'Missing opt-in confirmation link in HTML content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0060']	= 'Missing opt-in confirmation link in plain content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0061']	= 'Please select';
$ArrayLanguageStrings['Screen']['EmailBuilder_0062']	= 'Previous submitted jobs';
$ArrayLanguageStrings['Screen']['EmailBuilder_0063']	= 'Design preview process may take several minutes.';
$ArrayLanguageStrings['Screen']['EmailBuilder_0064']	= 'Missing opt-in reject link in HTML content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0065']	= 'Missing opt-in reject link in plain content';
$ArrayLanguageStrings['Screen']['EmailBuilder_0066']	= 'Refresh';
$ArrayLanguageStrings['Screen']['EmailBuilder_0067']	= 'Click to personalize subject';
$ArrayLanguageStrings['Screen']['EmailBuilder_0068']	= 'type % to personalize';
$ArrayLanguageStrings['Screen']['EmailBuilder_0069']	= 'Save email';
$ArrayLanguageStrings['Screen']['EmailBuilder_0070']	= 'Are you sure to cancel your changes and close email builder?';
$ArrayLanguageStrings['Screen']['EmailBuilder_0071']	= 'Please enter a name for your email';
$ArrayLanguageStrings['Screen']['EmailBuilder_0072']	= 'Save and Close';
$ArrayLanguageStrings['Screen']['EmailBuilder_0073']	= 'Are you sure to close email builder without saving changes?';
$ArrayLanguageStrings['Screen']['EmailBuilder_0074']	= 'Cancel';
$ArrayLanguageStrings['Screen']['EmailBuilder_0078']	= 'if you change your email template, you will lose your email content. Are you sure to continue?';



?>