<?php
/**
 * Public archives screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Tag';
$ArrayLanguageStrings['Screen']['0002']	= 'Template URL';
$ArrayLanguageStrings['Screen']['0003'] = 'This tool generates a unique url based on the settings below. You can use this link to show campaign archives to your website visitors.';
$ArrayLanguageStrings['Screen']['0004'] = 'Only campaigns with selected tag assigned will be showed in archive page';
$ArrayLanguageStrings['Screen']['0005'] = 'You can display archive page within your own page template. Leave the field blank if you want to view archive page in default template.';
$ArrayLanguageStrings['Screen']['0006'] = 'Please create <a href="./tags.php">Tags</a> first. Public archive pages require tags to show your campaigns.';
$ArrayLanguageStrings['Screen']['0007']	= 'Generate page url';
$ArrayLanguageStrings['Screen']['0008']	= 'Generating a unique url, please wait...';
$ArrayLanguageStrings['Screen']['0009']	= 'Now you can add the following url to your website. When visitors click on this link, they will be directed to an archive page.';
$ArrayLanguageStrings['Screen']['0010']	= 'Archive Error';
$ArrayLanguageStrings['Screen']['0011']	= 'Please return back and try again. If problem continues, contact service provider.';
$ArrayLanguageStrings['Screen']['0012']	= 'Campaign Archives';
$ArrayLanguageStrings['Screen']['0013']	= 'Currently there are no entries in archive. Please check back again later.';
$ArrayLanguageStrings['Screen']['0014']	= 'In order your template code to work, you need to do following:';
$ArrayLanguageStrings['Screen']['0015']	= 'Wrap the code that will display campaigns with &lt;LIST:Campaigns&gt; and &lt;/LIST:Campaigns&gt; tags';
$ArrayLanguageStrings['Screen']['0016']	= 'Insert _CampaignURL_ into href attribute of the campaign link';
$ArrayLanguageStrings['Screen']['0017']	= 'Insert _CampaignName_ to display campaign name';
$ArrayLanguageStrings['Screen']['0018']	= 'Example:';

?>