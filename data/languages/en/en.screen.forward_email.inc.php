<?php
/**
 * Forward To Friend screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Forward Email';
$ArrayLanguageStrings['Screen']['0002']	= 'Tell your friend about the email you have received';
$ArrayLanguageStrings['Screen']['0003']	= 'Please enter the following requested information and share the email you have received with your friend.';
$ArrayLanguageStrings['Screen']['0004']	= 'Friend Name';
$ArrayLanguageStrings['Screen']['0005']	= 'Friend Email';
$ArrayLanguageStrings['Screen']['0006']	= 'Your Name';
$ArrayLanguageStrings['Screen']['0007']	= 'Your Email';
$ArrayLanguageStrings['Screen']['0008']	= 'Do you want to include a note?';
$ArrayLanguageStrings['Screen']['0009']	= 'We never store or distribute the entered data to third parties. Submitted information will not be stored.';
$ArrayLanguageStrings['Screen']['0010']	= 'Email forwarded';
$ArrayLanguageStrings['Screen']['0011']	= 'Your email is forwarded to your friend\'s email address';
$ArrayLanguageStrings['Screen']['0012']	= '<a href="%s">Click here</a> to go back and forward email to another friend';
$ArrayLanguageStrings['Screen']['0013']	= 'Please enter friends name';
$ArrayLanguageStrings['Screen']['0014']	= 'Please enter friends email address';
$ArrayLanguageStrings['Screen']['0015']	= 'Please enter your name';
$ArrayLanguageStrings['Screen']['0016']	= 'Please enter your email address';
$ArrayLanguageStrings['Screen']['0017']	= 'Please correct the following errors and try again.';
$ArrayLanguageStrings['Screen']['0018']	= 'Invalid email address format';
$ArrayLanguageStrings['Screen']['0019']	= '%s is forwarding you this email';
$ArrayLanguageStrings['Screen']['0020']	= "%s also included a message for you:\n\n%s";
$ArrayLanguageStrings['Screen']['0021']	= "Dear %s,\n\n%s forwarded you this email. You can view it on your web browser by clicking the following link:\n\n%s\n\n%s";
$ArrayLanguageStrings['Screen']['0022']	= 'Forward Email';

?>