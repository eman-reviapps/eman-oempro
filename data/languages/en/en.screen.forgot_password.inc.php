<?php
/**
 * Forgot Password screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Send Reminder Email';
$ArrayLanguageStrings['Screen']['0002']	= 'Password Reminder';
$ArrayLanguageStrings['Screen']['0003']	= 'Go Back';
$ArrayLanguageStrings['Screen']['0004']	= 'Email address is missing';
$ArrayLanguageStrings['Screen']['0005']	= 'Invalid email address';
$ArrayLanguageStrings['Screen']['0006']	= 'An email including how to reset your password is sent to your email address.';
$ArrayLanguageStrings['Screen']['0007']	= 'Email address not found';

$ArrayLanguageStrings['Email']['0001']	= 'Password reset';
$ArrayLanguageStrings['Email']['0002']	= "We have received your password reset request. To reset your password, visit the link below.\n\nUsername:%s\nPassword reset link:%s";
?>