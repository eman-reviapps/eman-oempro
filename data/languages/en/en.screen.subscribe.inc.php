<?php
/**
 * Subscribe screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Missing Email Address';
$ArrayLanguageStrings['Screen']['0002']	= 'Please return back and enter your email address.';
$ArrayLanguageStrings['Screen']['0003']	= 'Error Occurred';
$ArrayLanguageStrings['Screen']['0004']	= 'Please return back and try again. If problem continues, contact service provider.';
$ArrayLanguageStrings['Screen']['0005']	= 'Subscriber IP address is missing';
$ArrayLanguageStrings['Screen']['0006']	= 'Invalid subscriber list ID';
$ArrayLanguageStrings['Screen']['0007']	= 'Invalid Email Address';
$ArrayLanguageStrings['Screen']['0008']	= 'Please return back and check your email address. It is invalid.';
$ArrayLanguageStrings['Screen']['0009']	= 'Missing Information';
$ArrayLanguageStrings['Screen']['0010']	= 'You forgot to enter information in %s field. Please return back and try again.';
$ArrayLanguageStrings['Screen']['0011']	= 'Information Already Exists';
$ArrayLanguageStrings['Screen']['0012']	= 'Information you have entered in subscription form (%s) already exists in the system. Please return back and try again with different information.';
$ArrayLanguageStrings['Screen']['0013']	= 'Invalid Information';
$ArrayLanguageStrings['Screen']['0014']	= 'Information you have entered in %s field is invalid. Please return back and try again.';
$ArrayLanguageStrings['Screen']['0015']	= 'Already Subscribed';
$ArrayLanguageStrings['Screen']['0016']	= 'Email address you have entered (%s) already subscribed into the list.';
$ArrayLanguageStrings['Screen']['0017']	= 'Thank You!';
$ArrayLanguageStrings['Screen']['0018']	= 'You are now subscribed to our mail list with your email address (%s). You can unsubscribe your email address at anytime by clicking the unsubscription link inside our newsletter.';
$ArrayLanguageStrings['Screen']['0019']	= 'You Are Almost Done';
$ArrayLanguageStrings['Screen']['0020']	= 'We just sent a confirmation email to your email address (%s). Please click the confirmation link inside our email and your subscription will be activated.';
$ArrayLanguageStrings['Screen']['0021']	= 'Already Confirmed';
$ArrayLanguageStrings['Screen']['0022']	= '%s email address has already been opt-in confirmed. Please unsubscribe by clicking unsubscribe link in the email you have received.';
$ArrayLanguageStrings['Screen']['0023']	= '%s email address is confirmed. You can unsubscribe at anytime by clicking unsubscription link in the email you will receive.';
$ArrayLanguageStrings['Screen']['0024']	= 'Subscription Cancelled';
$ArrayLanguageStrings['Screen']['0025']	= '%s email address is deleted and subscription cancelled';
$ArrayLanguageStrings['Screen']['0026']	= 'Invalid email address (%s) is entered. Please return back and try again.';
$ArrayLanguageStrings['Screen']['0027']	= 'No matching records found.';
$ArrayLanguageStrings['Screen']['0028']	= 'Already Unsubscribed';
$ArrayLanguageStrings['Screen']['0029']	= 'Email address is already unsubscribed in our list.';
$ArrayLanguageStrings['Screen']['0030']	= 'Unsubscription Completed';
$ArrayLanguageStrings['Screen']['0031']	= 'Your email address is unsubscribed from our mailing list.';

?>