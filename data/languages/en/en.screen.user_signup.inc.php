<?php
/**
 * User sign-up screen language strings
 **/

$ArrayLanguageStrings['Screen']['0001']	= 'Sign Up';
$ArrayLanguageStrings['Screen']['0002']	= 'Enjoy email marketing today!';
$ArrayLanguageStrings['Screen']['0003']	= 'Fill in all fields below to complete your sign-up.';
$ArrayLanguageStrings['Screen']['0004']	= 'Sign Up';
$ArrayLanguageStrings['Screen']['0005']	= 'We respect your privacy! Your email address or any other sensitive information is not shared with third parties.';
$ArrayLanguageStrings['Screen']['0006']	= 'Thank You!';
$ArrayLanguageStrings['Screen']['0007']	= 'Your account is ready';
$ArrayLanguageStrings['Screen']['0008']	= 'You can now login to your control panel and start creating lists and campaigns.';
$ArrayLanguageStrings['Screen']['0009']	= 'Email Address';
$ArrayLanguageStrings['Screen']['0010']	= 'Username';
$ArrayLanguageStrings['Screen']['0011']	= 'Password';
$ArrayLanguageStrings['Screen']['0012']	= array(
												'FirstName'			=> 'First Name',
												'LastName'			=> 'Last Name',
												'CompanyName'		=> 'Company Name',
												'Website'			=> 'Website URL',
												'Street'			=> 'Street',
												'City'				=> 'City',
												'State'				=> 'State',
												'Zip'				=> 'ZIP',
												'Country'			=> 'Country',
												'Phone'				=> 'Phone',
												'Fax'				=> 'Fax',
												'TimeZone'			=> 'Time Zone',
												'Language'			=> 'Interface Language',
												);
$ArrayLanguageStrings['Screen']['0013']	= 'Please enter your email address';
$ArrayLanguageStrings['Screen']['0014']	= 'Please enter your username';
$ArrayLanguageStrings['Screen']['0015']	= 'Please enter your password';
$ArrayLanguageStrings['Screen']['0016']	= 'Errors occurred. Please correct errors below and try again.';
$ArrayLanguageStrings['Screen']['0017']	= 'Please fill in this field';
$ArrayLanguageStrings['Screen']['0018']	= 'Invalid email address';
$ArrayLanguageStrings['Screen']['0019']	= 'Email address in use';
$ArrayLanguageStrings['Screen']['0020']	= 'Email address you have entered is already registered. Please try another one.';
$ArrayLanguageStrings['Screen']['0021']	= 'Click the following link below to login:';
$ArrayLanguageStrings['Screen']['0022']	= 'Username you have entered is already registered. Please try another one.';
$ArrayLanguageStrings['Screen']['0023']	= 'Select Plan';
$ArrayLanguageStrings['Screen']['0024']	= 'Please choose a valid plan';



?>