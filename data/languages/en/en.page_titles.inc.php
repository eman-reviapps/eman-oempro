<?php
/**
 * @author Cem Hurturk
 * @copyright Octeth.com
 **/

/**
 * Page Title languages
 **/

$ArrayLanguageStrings['PageTitle']['LoginPage']									= 'Please login | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['ForgotPassword']							= 'Password Reminder | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['OverviewPage']								= 'Overview | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsPage']						= 'Subscriber Lists | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsCustomFieldsPage']			= 'Subscriber List &gt; Custom Fields | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsAutoRespondersPage']			= 'Subscriber List &gt; Auto Responders | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsSegmentsPage']				= 'Subscriber List &gt; Segments | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsBrowseSubscribersPage']		= 'Subscriber List &gt; Browse Subscribers | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsEditSubscriberPage']			= 'Subscriber List &gt; Edit Subscriber | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsExportSubscribersPage']		= 'Subscriber List &gt; Export Subscribers | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsRemoveSubscribersPage']		= 'Subscriber List &gt; Remove Subscribers | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsImportSubscribersPage']		= 'Subscriber List &gt; Import Subscribers | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListSettingsPage']				= 'Subscriber List &gt; Settings | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsWebServiceIntegrationPage']	= 'Subscriber List &gt; Web Service Integration | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListsWebsiteIntegrationPage']		= 'Subscriber List &gt; Website Integration | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['ClientsPage']								= 'Clients | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['AccountInformationPage']					= 'Account Information | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignsPage']								= 'Campaigns | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriptionError']							= 'Subscription Error';
$ArrayLanguageStrings['PageTitle']['SubscriptionSuccess']						= 'Subscription Success';
$ArrayLanguageStrings['PageTitle']['UnsubscriptionSuccess']						= 'Unsubscription Completed';
$ArrayLanguageStrings['PageTitle']['UnsubscriptionError']						= 'Unsubscription Error';
$ArrayLanguageStrings['PageTitle']['EditClientPage']							= 'Edit Client Account Information | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListStatisticsPage']				= 'Subscriber List &gt; Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['ForwardToFriend']							= 'Forward Email To Your Friends';
$ArrayLanguageStrings['PageTitle']['ReportAbuse']								= 'Report Abuse';
$ArrayLanguageStrings['PageTitle']['MediaLibrary']								= 'Media Library';
$ArrayLanguageStrings['PageTitle']['EmailPreviewPage']							= 'Email Preview';
$ArrayLanguageStrings['PageTitle']['UserSignUpPage']							= 'Sign Up';
$ArrayLanguageStrings['PageTitle']['CampaignSettingsPage']						= 'Campaign &gt; Settings | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberSubscriptionsPage']				= 'Subscriptions | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SubscriberListSynchronizationPage']			= 'Subscriber List &gt; Synchronization | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['PermissionErrorPage']						= 'No Permission';
$ArrayLanguageStrings['PageTitle']['SuppressedSubscribersPage']					= 'Suppressed Subscribers | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignStatisticsPage']					= 'Campaign &gt; Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignLinkStatisticsPage']				= 'Campaign &gt; Link Click Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignBrowserViewStatisticsPage']			= 'Campaign &gt; Browser View Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignForwardStatisticsPage']				= 'Campaign &gt; Forward Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignOpenStatisticsPage']				= 'Campaign &gt; Open Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignUnsubscriptionStatisticsPage']		= 'Campaign &gt; Unsubscription Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['AutoResponderStatisticsPage']				= 'Auto Responder &gt; Statistics | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['UsersPage']									= 'Users | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CreateUserPage']							= 'Create a New User | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EditUserPage']								= 'Edit User | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['PaymentReportsPage']						= 'Payment Reports | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EmailSettingsPage']							= 'Email Settings | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['ESPSettingsPage']							= 'ESP Settings | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['RebrandingSettingsPage']					= 'Rebranding Settings | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['PreferencesPage']							= 'Preferences | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CreateUserGroupPage']						= 'Create User Group | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['UserGroupsPage']							= 'User Groups | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EditUserGroupPage']							= 'Edit User Group | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['PlugInManager']								= 'Plug-in Manager | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['About']										= 'About | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EmailTemplatesPage']						= 'Email Templates | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CreateEmailTemplatePage']					= 'Create Email Template | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EditEmailTemplatePage']						= 'Edit Email Template | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SendCampaigns']								= 'Send Campaigns | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['TagsPage']									= 'Tags | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['Upgrade']									= 'Upgrade Utility | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['ArchiveError']								= 'Archive Error | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignArchives']							= 'Campaign Archives | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['CampaignCompare']							= 'Compare Campaign Performance | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['EmailManagement']							= 'Email Management | '.PRODUCT_NAME;
$ArrayLanguageStrings['PageTitle']['SystemCheck']								= 'System Check | '.PRODUCT_NAME;

?>