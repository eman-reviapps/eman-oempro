<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Email open tracking module
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('payments');
// Load other modules - End
// Decrypt URL parameters - Start
if ($_GET['p'] != '') {
    $ArrayParameters = Core::DecryptURL($_GET['p']);
} else {
    $ArrayParameters = $_GET;
}
// Decrypt URL parameters - End
// Set varilables - Start

$PaymentLogID = $ArrayParameters['PaymentLogID'];
$UserID = $ArrayParameters['UserID'];
$PaymentGateway = $ArrayParameters['PaymentGateway'];
$IsCreditPurchase = isset($ArrayParameters['IsCreditPurchase']) && $ArrayParameters['IsCreditPurchase'] ? $ArrayParameters['IsCreditPurchase'] : false;
$ReputationLevel = isset($ArrayParameters['ReputationLevel']) && !empty($ArrayParameters['ReputationLevel']) ? $ArrayParameters['ReputationLevel'] : 'Trusted';

// Set varilables - End
// Retrieve user information - Start
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
if ($ArrayUser == false) {
    header('Location: ./user/');
    exit;
}
Payments::SetUser($ArrayUser);
// Retrieve user information - End
// Retrieve payment period log information - Start
$ArrayPaymentPeriod = Payments::GetPaymentPeriod($PaymentLogID);
if ($ArrayPaymentPeriod == false) {
    header('Location: ./user/');
    exit;
}
// Retrieve payment period log information - End
// Perform payment gateway redirection/process - Start
if ($PaymentGateway == 'PayPal_Express') {
    Core::LoadObject('gateway');
    Core::LoadObject('payment_gateways/gateway_paypal_express.inc.php');

    PaymentGateway::SetInterface('PayPal_Express');
    $ArrayParameters = array(
        'business' => PAYPALEXPRESSBUSINESSNAME,
        // 'amount'			=> number_format($ArrayPaymentPeriod['TotalAmount'], 2),
        'amount' => Core::FormatCurrency($ArrayPaymentPeriod['TotalAmount'], false),
        'item_name' => PAYPALEXPRESSPURCHASEDESCRIPTION,
        'currency_code' => PAYPALEXPRESSCURRENCY,
        'address1' => '',
        'address2' => '',
        'city' => '',
        'country' => '',
        'first_name' => '',
        'last_name' => '',
        'lc' => '',
        'night_phone_a' => '',
        'night_phone_b' => '',
        'night_phone_c' => '',
        'state' => '',
        'zip' => '',
    );
    PaymentGateway::$Interface->SetGatewayParameters($ArrayParameters, $ArrayUser, $PaymentLogID, $IsCreditPurchase, $ReputationLevel);

    PaymentGateway::$Interface->Redirect();
    exit;
} else if ($PaymentGateway == 'iyzipay') {
    
}
// Perform payment gateway redirection/process - End
?>