<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Email open tracking module
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
// Load other modules - End
// Behave based on the payment gateway type - Start

if (($_GET['Gateway'] == 'PayPal_Express') && ($_GET['Type'] == 'Ping')) {
// PayPal Express IPN/PDT	
    Core::LoadObject('users');
    Core::LoadObject('payments');
    Core::LoadObject('gateway');
    Core::LoadObject('payment_gateways/gateway_paypal_express.inc.php');

    PaymentGateway::SetInterface('PayPal_Express');
    PaymentGateway::$Interface->ProcessReturnedData($_POST);
} else if ($_GET['Gateway'] == 'Iyzipay') {

// PayPal Express IPN/PDT	
    Core::LoadObject('users');
    Core::LoadObject('payments');
    Core::LoadObject('gateway');
    Core::LoadObject('payment_gateways/gateway_iyzico.inc.php');
    PaymentGateway::SetInterface('Iyzipay');

    $result = PaymentGateway::$Interface->ProcessReturnedData($_POST);

    

    $url = APP_URL . APP_DIRNAME . (HTACCESS_ENABLED == false ? '/index.php?' : '');

    if ($result[0]) {
        //success
        $process = $result[1];
        if ($process == 'Upgrade') {
            $_SESSION[SESSION_NAME]['PAYMENTSUCCESS'] = $result[2];
            header('Location: ' . $url . '/process/upgrade');
        } else {
            header('Location: ' . $url . '/user/');
        }
    } else {
        header('Location: ' . $url . '/user/');
    }
}
// Behave based on the payment gateway type - End


exit;
?>