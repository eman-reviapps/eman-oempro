<?php

/**
 *
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Email open tracking module
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('campaigns');
Core::LoadObject('emails');
Core::LoadObject('auto_responders');
Core::LoadObject('statistics');
// Load other modules - End
// Set the POST and GET same - Start
if ((count($_POST) == 0) && (count($_GET) > 0)) {
    $_POST = $_GET;
}
// Set the POST and GET same - End
// Decrypt URL parameters - Start
if ($_POST['p'] != '') {
    $ArrayParameters = Core::DecryptURL($_POST['p']);
} else {
    $ArrayParameters = $_POST;
}
// Decrypt URL parameters - End
// Set varilables - Start
$CampaignID = $ArrayParameters['CampaignID'];
$EmailID = $ArrayParameters['EmailID'];
$AutoResponderID = $ArrayParameters['AutoResponderID'];
$SubscriberID = $ArrayParameters['SubscriberID'];
$ListID = $ArrayParameters['ListID'];
$LinkURL = $ArrayParameters['LinkURL'];
if (isset($ArrayParameters['ShorterLinks']) && $ArrayParameters['ShorterLinks'] === TRUE) {
    $LinkURL = rawurldecode($ArrayParameters['LinkURL']);
} else {
    $LinkURL = base64_decode(rawurldecode($ArrayParameters['LinkURL']));
}
$LinkTitle = $ArrayParameters['LinkTitle'];
$Preview = $ArrayParameters['Preview'];
$SubscriberIP = $_SERVER['REMOTE_ADDR'];

// Set varilables - End
// Validate parameters and retrieve required information - Start
if ($Preview == '') {
    try {
        $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
        $GeoTagInfo = geoip_record_by_addr($GeoTag, $SubscriberIP);

        $Open_City = isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : '';
        $Open_Country = isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : '';

        // Track link clicks - Start
        // Retrieve list information - Start
        $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ListID), false, false);
        if ($ArrayList == false) {
            throw new Exception('Subscriber list not found');
        }
        // Retrieve list information - End
        // Retrieve campaign information - Start
        $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));
        if ($ArrayCampaign == false) {
            $ArrayCampaign = array();
        }
        // Retrieve campaign information - End
        // Retrieve email information (if provided) - Start {
        $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $EmailID), false);
        if ($ArrayEmail == false) {
            $ArrayEmail = array();
        }
        // Retrieve email information (if provided) - End }
        // Retrieve auto-responder information - Start
        $ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), Array('AutoResponderID' => $AutoResponderID));
        if ($ArrayAutoResponder == false) {
            $ArrayAutoResponder = array();
        }
        // Retrieve auto-responder information - End

        if ((count($ArrayAutoResponder) == 0) && (count($ArrayCampaign) == 0)) {
            throw new Exception('Campaign or autoresponder not found');
        }

        // Retrieve subscriber information - Start
        $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
        if ($ArraySubscriber == false) {
            throw new Exception('Subscriber not found');
        }
        // Retrieve subscriber information - End
        // Check if this user has clicked any link on this campaign before - Start
        if (count($ArrayCampaign) > 0) {
            $TotalClicks = Statistics::RetrieveCampaignLinkStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayCampaign['CampaignID'], (isset($ArrayEmail['EmailID']) != false ? $ArrayEmail['EmailID'] : 0));
        } elseif (count($ArrayAutoResponder) > 0) {
            $TotalClicks = Statistics::RetrieveAutoResponderLinkStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayAutoResponder['AutoResponderID']);
        }
        // Check if this user has clicked any link on this campaign before - End
        // Track the open - Start
        $ArrayFieldnValues = array();

        if ($TotalClicks == 0) {
            // Update 'unique' link statistics - Start
            if (count($ArrayCampaign) > 0) {
                $ArrayFieldnValues['UniqueClicks'] = $ArrayCampaign['UniqueClicks'] + 1;
            } elseif (count($ArrayAutoResponder) > 0) {
                $ArrayFieldnValues['UniqueClicks'] = $ArrayAutoResponder['UniqueClicks'] + 1;
            }
            // Update 'unique' link statistics - End
        }

        // Update 'total' link statistics - Start
        if (count($ArrayCampaign) > 0) {
            $ArrayFieldnValues['TotalClicks'] = $ArrayCampaign['TotalClicks'] + 1;
            Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));
        } elseif (count($ArrayAutoResponder) > 0) {
            $ArrayFieldnValues['TotalClicks'] = $ArrayAutoResponder['TotalClicks'] + 1;
            AutoResponders::Update($ArrayFieldnValues, array('AutoResponderID' => $ArrayAutoResponder['AutoResponderID']));
        }
        // Update 'total' link statistics - End
        // Update link statistics table - Start
        Statistics::RegisterLinkClickTrackWithIP($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], ($ArrayCampaign['CampaignID'] != '' ? $ArrayCampaign['CampaignID'] : 0), ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), $LinkURL, $LinkTitle, ($ArrayAutoResponder['AutoResponderID'] != '' ? $ArrayAutoResponder['AutoResponderID'] : 0), (isset($ArrayEmail['EmailID']) != false ? $ArrayEmail['EmailID'] : 0)
                , $SubscriberIP, $Open_City, $Open_Country);
        // Update link statistics table - End
        // Trigger auto-responders if recipeint has clicked for the first time to this link - Start
        if (Statistics::SubscriberLinkClickTotal($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayCampaign['CampaignID'], ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), $LinkURL, $ArrayAutoResponder['AutoResponderID'], (isset($ArrayEmail['EmailID']) != false ? $ArrayEmail['EmailID'] : 0)) == 1) {
            Core::LoadObject('auto_responders');
            $TotalRegisteredAutoResponders = AutoResponders::RegisterAutoResponders($ArrayList['ListID'], $ArraySubscriber['SubscriberID'], ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), 'OnSubscriberLinkClick', $LinkURL);
        }
        // Trigger auto-responders if recipeint has clicked for the first time to this link - End
        // Important Notice: There's a duplicate code of below in track_link.php to track the email opens if the recipient has clicked a link
        // but email open is not detected/tracked before. If you make a change below, don't forget to check track_link.php
        // Check if recipient email open has not been tracked but recipient has clicked a link. If this is the case, track the open - Start {
        if (count($ArrayCampaign) > 0) {
            $TotalOpens = Statistics::RetrieveCampaignOpenStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayCampaign['CampaignID'], (isset($ArrayEmail['EmailID']) != false ? $ArrayEmail['EmailID'] : 0));
        } elseif (count($ArrayAutoResponder) > 0) {
            $TotalOpens = Statistics::RetrieveAutoResponderOpenStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayAutoResponder['AutoResponderID']);
        }

        if ($TotalOpens == 0) {
            // Track the open - Start
            $ArrayFieldnValues = array();

            // Update 'unique' open statistics - Start
            if (count($ArrayCampaign) > 0) {
                $ArrayFieldnValues['UniqueOpens'] = $ArrayCampaign['UniqueOpens'] + 1;
            } elseif (count($ArrayAutoResponder) > 0) {
                $ArrayFieldnValues['UniqueOpens'] = $ArrayAutoResponder['UniqueOpens'] + 1;
            }
            // Update 'unique' open statistics - End
            // Update 'total' open statistics - Start
            if (count($ArrayCampaign) > 0) {
                $ArrayFieldnValues['TotalOpens'] = $ArrayCampaign['TotalOpens'] + 1;
                Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));
            } elseif (count($ArrayAutoResponder) > 0) {
                $ArrayFieldnValues['TotalOpens'] = $ArrayAutoResponder['TotalOpens'] + 1;
                AutoResponders::Update($ArrayFieldnValues, array('AutoResponderID' => $ArrayAutoResponder['AutoResponderID']));
            }
            // Update 'total' open statistics - End
            // Update open statistics table - Start

            $ArrayFieldAndValues = array(
                'Subscriber_IP' => $SubscriberIP,
                'City' => $Open_City,
                'Country' => $Open_Country,
            );
            Subscribers::Update($ArrayList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $ArraySubscriber['SubscriberID']));

            Statistics::RegisterOpenTrackWithIP($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], ($ArrayCampaign['CampaignID'] != '' ? $ArrayCampaign['CampaignID'] : 0), ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), ($ArrayAutoResponder['AutoResponderID'] != '' ? $ArrayAutoResponder['AutoResponderID'] : 0), (isset($ArrayEmail['EmailID']) != false ? $ArrayEmail['EmailID'] : 0)
                    , $SubscriberIP, $Open_City, $Open_Country);
            // Update open statistics table - End
            // Track the open - End
        }
        // Check if recipient email open has not been tracked but recipient has clicked a link. If this is the case, track the open - End }
        // Track link clicks - End
    } catch (Exception $e) {
        // Nothing to do on catch
    }
}
// Validate parameters and retrieve required information - End
// Redirect to the target URL - Start
// Link fix - Start
// & and &amp; fix -> Unfortunately, browser innerHTML functionality brokes & and other similar
// special characters and changes them to their html entities. & is important for links this routine
// will fix it
$LinkURL = preg_replace('/&amp;amp;/i', 'OemproTempLinkAmpersand', $LinkURL);
$LinkURL = preg_replace('/&amp;/i', '&', $LinkURL);
$LinkURL = preg_replace('/OemproTempLinkAmpersand/i', '&amp;', $LinkURL);
// Link fix - End
// Parse the link URL - Start
$ArrayParsedURL = parse_url($LinkURL);
// Parse the link URL - End
// Check if Google Analytics integration enabled. If yes, add Google Analytics parameters to the link - Start
if (count($ArrayCampaign) > 0) {
    $AnalyticsLinkParameters = Core::GenerateGoogleAnalyticsLinkParameters($ArrayCampaign, $ArraySubscriber, $LinkURL, $LinkTitle, $ArrayParsedURL['host']);
    if (($AnalyticsLinkParameters != '') && ($ArrayParsedURL['query'] != '')) {
        $AnalyticsLinkParameters = '&' . $AnalyticsLinkParameters;
    } elseif (($AnalyticsLinkParameters != '') && ($ArrayParsedURL['query'] == '')) {
        $AnalyticsLinkParameters = '?' . $AnalyticsLinkParameters;
    }
}
// Check if Google Analytics integration enabled. If yes, add Google Analytics parameters to the link - End

header('Location: ' . $LinkURL . $AnalyticsLinkParameters);
// Redirect to the target URL - End

exit;
?>
