<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * Subscription module (public)
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('api');
// Load other modules - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'subscribe');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Set the POST and GET same - Start
if ((count($_POST) == 0) && (count($_GET) > 0))
	{
	$_POST = $_GET;
	}
// Set the POST and GET same - End

// Decrypt URL parameters - Start
if ($_POST['p'] != '')
	{
	$ArrayParameters = Core::DecryptURL($_POST['p']);
	}
else
	{
	$ArrayParameters = $_POST;
	}
// Decrypt URL parameters - End

// Retrieve list information - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayParameters['FormValue_ListID']));
// Retrieve list information - End

// Prepare and send the request to the API - Start
	$arrayAPIParameters = array();
	$arrayAPIParameters['listid']			= $ArraySubscriberList['ListID'];
	$arrayAPIParameters['autoresponderid']	= $ArrayParameters['FormValue_AutoResponderID'];
	$arrayAPIParameters['ipaddress']		= $_SERVER['REMOTE_ADDR'];
	$arrayAPIParameters['emailid']			= $ArrayParameters['FormValue_EmailID'];
	$arrayAPIParameters['campaignid']		= $ArrayParameters['FormValue_CampaignID'];
	$arrayAPIParameters['subscriberid']		= $ArrayParameters['FormValue_SubscriberID'];
	$arrayAPIParameters['emailaddress']		= $ArrayParameters['FormValue_EmailAddress'];
	if ($ArrayParameters['Preview'] == 1)
		{
		$arrayAPIParameters['preview'] = 1;
		}
	foreach ($_POST['FormValue_Fields'] as $Key=>$Value)
		{
		$arrayAPIParameters[strtolower($Key)] = $Value;
		}

	$arrayReturn = API::call(array(
		'format'	=> 'xml',
		'command'	=> 'subscriber.unsubscribe',
		'parameters'=> $arrayAPIParameters
	));
// Prepare and send the request to the API - End

// Retrieve the request result and process it - Start
$XML = $arrayReturn;
$ObjectXML = simplexml_load_string($XML);
// Retrieve the request result and process it - End

// Perform redirections - Start
if ($ObjectXML->Success == true)
	{
	if ($ObjectXML->RedirectURL != '')
		{
			$GetData = '';
			if (SEND_SUBSCRIBER_INFORMATION) {
				$GetData = (eregi('\?', $ObjectXML->RedirectURL) == false ? '?' : '&').'XMLReturn='.rawurlencode($XML);
			}
		header('Location: '.$ObjectXML->RedirectURL.$GetData);
		exit;
		}
	else
		{
		// Success page parsing - Start
		$ObjectTemplate 					= new TemplateEngine();
		$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1560'];
		$ObjectTemplate->ArrayFormFields 	= array(
													);

		$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/success.tpl', 'file');

		$ObjectTemplate->LoadTemplates();

		$ArrayReplaceList = array(
								'Insert:MessageTitle'			=> $ArrayLanguageStrings['Screen']['1560'],
								'Insert:MessageDesc'			=> $ArrayLanguageStrings['Screen']['1561'],
								);
		$ObjectTemplate->Replace($ArrayReplaceList);

		$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
		// Success page parsing - End
		}
	}
else
	{
	// If redirection URL is set, redirect to the target URL - Start {
	if ($ArraySubscriberList['UnsubscriptionErrorPageURL'] != '' && REDIRECT_ON_UNSUBSCRIPTION_ENABLED)
		{
			$GetData = '';
			if (SEND_SUBSCRIBER_INFORMATION) {
				$GetData = preg_match('/\?/', $ArraySubscriberList['UnsubscriptionErrorPageURL']) ? '&' : '?';
				$GetData .= 'e='.$ObjectXML->ErrorCode.'&l='.$arrayAPIParameters['listid'].'&s='.$arrayAPIParameters['subscriberid'].'&em='.rawurlencode($arrayAPIParameters['emailaddress']);
			}
		header('Location: '.$ArraySubscriberList['UnsubscriptionErrorPageURL'].$GetData);
		exit;
		}
	// If redirection URL is set, redirect to the target URL - End }

	// Set the error message and title based on the returned error code - Start
	if ($ObjectXML->ErrorCode == 1)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 2)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 3)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 4)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 5)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 6)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1550'];
		$ErrorDescription	= sprintf($ArrayLanguageStrings['Screen']['1562'], $ArrayParameters['FormValue_EmailAddress']);
		}
	elseif ($ObjectXML->ErrorCode == 7)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1550'];
		$ErrorDescription	= sprintf($ArrayLanguageStrings['Screen']['1563']);
		}
	elseif ($ObjectXML->ErrorCode == 8)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	elseif ($ObjectXML->ErrorCode == 9)
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1564'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1565'];
		}
	else
		{
		$ErrorTitle			= $ArrayLanguageStrings['Screen']['1538'];
		$ErrorDescription	= $ArrayLanguageStrings['Screen']['1539'];
		}
	// Set the error message and title based on the returned error code - End
		
	// Error page parsing - Start
	$ObjectTemplate 					= new TemplateEngine();
	$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1566'];
	$ObjectTemplate->ArrayFormFields 	= array(
												);

	$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/error.tpl', 'file');

	$ObjectTemplate->LoadTemplates();

	$ArrayReplaceList = array(
							'Insert:ErrorTitle'			=> $ErrorTitle,
							'Insert:ErrorDesc'			=> $ErrorDescription,
							);
	$ObjectTemplate->Replace($ArrayReplaceList);

	$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
	// Error page parsing - End
	}
// Perform redirections - End

exit;
?>