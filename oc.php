<?php
// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

$QueryParameters = Core::DecryptQueryStringAsArrayAdvanced($_GET['p']);

$_POST = array(
	'ListID'		=> $QueryParameters[0],
	'SubscriberID'	=> $QueryParameters[1],
	'Mode'			=> $QueryParameters[2] == 1 ? 'Confirm' : 'Reject',
	'Preview'		=> $QueryParameters[3] == 0 ? '' : $QueryParameters[3],
);

include_once 'opt_confirm.php';