<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Subscription module (public)
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('api');
// Load other modules - End
// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'subscribe');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');
// Load language - End
// Set the POST and GET same - Start
if ((count($_POST) == 0) && (count($_GET) > 0)) {
    $_POST = $_GET;
}
// Set the POST and GET same - End
$is_ajax = isset($_POST['is_ajax']) && !empty($_POST['is_ajax']) && $_POST['is_ajax'] ? true : false;
$callback = isset($_POST['callback']) && !empty($_POST['callback']) ? $_POST['callback'] : false;


$ArrayListIDs = explode(',', $_POST['FormValue_ListID']);

// Retrieve list information - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayListIDs[0]));
// Retrieve list information - End
// Prepare and send the request to the API - Start {
$arrayParameters = array();

$arrayParameters['listid'] = $_POST['FormValue_ListID'];
$arrayParameters['ipaddress'] = $_SERVER['REMOTE_ADDR'];
foreach ($_POST['FormValue_Fields'] as $Key => $Value) {
    if (strtolower(gettype($Value)) == 'array') {
        $arrayParameters[strtolower($Key)] = array();
        foreach ($Value as $EachValue) {
            $arrayParameters[strtolower($Key)][] = $EachValue;
        }
    } else {
        $arrayParameters[strtolower($Key)] = $Value;
    }
}

$arrayReturn = API::call(array(
            'format' => 'xml',
            'command' => 'subscriber.subscribe',
            'parameters' => $arrayParameters
        ));

// Prepare and send the request to the API - End }

$XML = $arrayReturn;
$ObjectXML = simplexml_load_string($arrayReturn);

// Perform redirections - Start
if ($ObjectXML->Success == true) {
    if ($is_ajax) {
        $output = array(
            "State" => true
        );
        if ($callback) {
            header('Content-Type: text/javascript, charset=UTF-8');
            echo $callback . '(' . json_encode($output) . ');';
        } else {
            header('Content-Type: application/x-json, charset=UTF-8');
            echo json_encode($output);
        }
        exit();
    }
    if ($ObjectXML->RedirectURL != '') {
        if (defined('POST_SUBSCRIPTION_DATA_TRANSFER_TYPE') == true && POST_SUBSCRIPTION_DATA_TRANSFER_TYPE == 'POST-JSON') {
            $SubscriberData = array();

            if (isset($ObjectXML->Subscriber) == true && count(get_object_vars($ObjectXML->Subscriber)) > 0) {
                foreach (get_object_vars($ObjectXML->Subscriber) as $Key => $Value) {
                    $SubscriberData[$Key] = (string) $Value;
                }
            }

            $JsonData = rawurlencode(json_encode($SubscriberData));

            $HTML = <<<EOF
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form id="frm" name="frm" action="{$ObjectXML->RedirectURL}" method="POST">
		<input type="hidden" name="SubscriberData" value="{$JsonData}">
	</form>
	<script>
		document.frm.submit();
	</script>
</body>
</html>
EOF;
            print $HTML;
            exit;
        } else {
            $GetData = '';
            if (SEND_SUBSCRIBER_INFORMATION) {
                $GetData = (eregi('\?', $ObjectXML->RedirectURL) == false ? '?' : '&') . 'XMLReturn=' . rawurlencode($ObjectXML->asXML());
            }
            header('Location: ' . $ObjectXML->RedirectURL . $GetData);
            exit;
        }
    } else {
        // Success page parsing - Start
        $ObjectTemplate = new TemplateEngine();
        $ObjectTemplate->PageTitle = $ArrayLanguageStrings['Screen']['1533'];
        $ObjectTemplate->ArrayFormFields = array(
        );

        $ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/success.tpl', 'file');

        $ObjectTemplate->LoadTemplates();

        $ArrayReplaceList = array(
            'Insert:MessageTitle' => ($ArraySubscriberList['OptInMode'] == 'Single' ? $ArrayLanguageStrings['Screen']['1534'] : $ArrayLanguageStrings['Screen']['1543']),
            'Insert:MessageDesc' => ($ArraySubscriberList['OptInMode'] == 'Single' ? sprintf($ArrayLanguageStrings['Screen']['1544'], $_POST['FormValue_Fields']['EmailAddress']) : sprintf($ArrayLanguageStrings['Screen']['1545'], $_POST['FormValue_Fields']['EmailAddress'])),
        );
        $ObjectTemplate->Replace($ArrayReplaceList);

        $ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
        // Success page parsing - End
    }
} else {
    if ($is_ajax) {
        if ($ObjectXML->ErrorCode == 1) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
        } elseif ($ObjectXML->ErrorCode == 2) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1546'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1547'];
        } elseif ($ObjectXML->ErrorCode == 3) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1548'];
        } elseif ($ObjectXML->ErrorCode == 4) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1549'];
        } elseif ($ObjectXML->ErrorCode == 5) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1550'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1551'];
        } elseif ($ObjectXML->ErrorCode == 6) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1552'];
            $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1553'], $ObjectXML->ErrorCustomFieldTitle);
        } elseif ($ObjectXML->ErrorCode == 7) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1554'];
            $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1555'], $ObjectXML->ErrorCustomFieldTitle);
        } elseif ($ObjectXML->ErrorCode == 8) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1556'];
            $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1557'], $ObjectXML->ErrorCustomFieldTitle);
        } elseif ($ObjectXML->ErrorCode == 9) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1558'];
            $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1559'], $_POST['FormValue_Fields']['EmailAddress']);
        } elseif ($ObjectXML->ErrorCode == 10) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
        } elseif ($ObjectXML->ErrorCode == 11) {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
        } else {
            $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
            $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
        }

        $output = array(
            "State" => false,
            "ErrorTitle" => $ErrorTitle,
            "ErrorDescription" => $ErrorDescription,
        );
        if ($callback) {
            header('Content-Type: text/javascript, charset=UTF-8');
            echo $callback . '(' . json_encode($output) . ');';
        } else {
            header('Content-Type: application/x-json, charset=UTF-8');
            echo json_encode($output);
        }
        exit();
    }
    // If redirection URL is set, redirect to the target URL - Start {
    if ($ArraySubscriberList['SubscriptionErrorPageURL'] != '') {
        if (defined('POST_SUBSCRIPTION_DATA_TRANSFER_TYPE') == true && POST_SUBSCRIPTION_DATA_TRANSFER_TYPE == 'POST-JSON') {
            $JsonData = rawurlencode(json_encode(array(
                'e' => $ObjectXML->ErrorCode,
                'l' => $arrayParameters['listid'],
                'em' => $arrayParameters['emailaddress'],
            )));

            $HTML = <<<EOF
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form id="frm" name="frm" action="{$ArraySubscriberList['SubscriptionErrorPageURL']}" method="POST">
		<input type="hidden" name="SubscriberData" value="{$JsonData}">
	</form>
	<script>
		document.frm.submit();
	</script>
</body>
</html>
EOF;
            print $HTML;
            exit;
        } else {
            $GetData = '';
            if (SEND_SUBSCRIBER_INFORMATION) {
                $GetData = preg_match('/\?/', $ArraySubscriberList['SubscriptionErrorPageURL']) ? '&' : '?';
                $GetData .= 'e=' . $ObjectXML->ErrorCode . '&l=' . $arrayParameters['listid'] . '&em=' . rawurlencode($arrayParameters['emailaddress']);
            }
            header('Location: ' . $ArraySubscriberList['SubscriptionErrorPageURL'] . $GetData);
            exit;
        }
    }
    // If redirection URL is set, redirect to the target URL - End }
    // Set the error message and title based on the returned error code - Start
    if ($ObjectXML->ErrorCode == 1) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
    } elseif ($ObjectXML->ErrorCode == 2) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1546'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1547'];
    } elseif ($ObjectXML->ErrorCode == 3) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1548'];
    } elseif ($ObjectXML->ErrorCode == 4) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1549'];
    } elseif ($ObjectXML->ErrorCode == 5) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1550'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1551'];
    } elseif ($ObjectXML->ErrorCode == 6) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1552'];
        $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1553'], $ObjectXML->ErrorCustomFieldTitle);
    } elseif ($ObjectXML->ErrorCode == 7) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1554'];
        $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1555'], $ObjectXML->ErrorCustomFieldTitle);
    } elseif ($ObjectXML->ErrorCode == 8) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1556'];
        $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1557'], $ObjectXML->ErrorCustomFieldTitle);
    } elseif ($ObjectXML->ErrorCode == 9) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1558'];
        $ErrorDescription = sprintf($ArrayLanguageStrings['Screen']['1559'], $_POST['FormValue_Fields']['EmailAddress']);
    } elseif ($ObjectXML->ErrorCode == 10) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
    } elseif ($ObjectXML->ErrorCode == 11) {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
    } else {
        $ErrorTitle = $ArrayLanguageStrings['Screen']['1538'];
        $ErrorDescription = $ArrayLanguageStrings['Screen']['1539'];
    }
    // Set the error message and title based on the returned error code - End
    // Error page parsing - Start

    $ObjectTemplate = new TemplateEngine();
    $ObjectTemplate->PageTitle = $ArrayLanguageStrings['Screen']['1542'];
    $ObjectTemplate->ArrayFormFields = array(
    );

    $ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/error.tpl', 'file');

    $ObjectTemplate->LoadTemplates();

    $ArrayReplaceList = array(
        'Insert:ErrorTitle' => $ErrorTitle,
        'Insert:ErrorDesc' => $ErrorDescription,
    );
    $ObjectTemplate->Replace($ArrayReplaceList);

    $ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
    // Error page parsing - End
}
// Perform redirections - End


exit;
?>