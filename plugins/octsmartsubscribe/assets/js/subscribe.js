window.octsmartsubscribe = {
	jq: null,
	els: {
		overlay: null,
		popup: null,
		button: null,
		formButton: null,
		input: null
	},
	isLoading: false,
	isPopupOpen: false,
	isPopupClosedBefore: false,
	config: {
		overlayOpacity: .3,
		inputBorder: '1px solid #999999',
		buttonLoading: {'background':'none','border':'none','color':'#000','text-shadow':'none','font-size':'24px','height':'26px','line-height':'26px'}
	},
	init: function($) {
		window.octsmartsubscribe.jq = $;
		window.octsmartsubscribe.applyButtonStyles();
		window.octsmartsubscribe.createPopup();
		window.octsmartsubscribe.attachEventListeners();
	},
	applyButtonStyles: function() {
		var $ = window.octsmartsubscribe.jq;
		var $button = $('#octsmartsubscribe-button'),
			options = window.octsmartsubscribeOptions;
		
		if ($button.length < 1) return;
		if (options.type !== 'button') return;

		var buttonCSSOptions = {
			'background-color': options.buttonBG,
			'border-radius': '5px',
			'color': options.buttonFG,
			'display': 'inline-block',
			'font-family': 'Arial, sans-serif',
			'font-size': options.buttonLabelSize,
			'font-weight': 'bold',
			'line-height': '1.5em',
			'padding': '.3em 1em',
			'text-decoration': 'none'
		};

		if (options.buttonStyle == 'gloss') {
			$button[0].style.backgroundImage = "-o-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
			$button[0].style.backgroundImage = "-ms-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
			$button[0].style.backgroundImage = "-moz-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
			$button[0].style.backgroundImage = "-webkit-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
			$button[0].style.backgroundImage = "linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";

			buttonCSSOptions['text-shadow'] = '0 2px 1px rgba(0, 0, 0, .3)';
			buttonCSSOptions['border'] = '2px solid rgba(0, 0, 0, .1)';
		}

		$button.text(options.buttonLabel);
		$button.css(buttonCSSOptions);

		window.octsmartsubscribe.els.button = $button;
	},
	createPopup: function() {
		var $ = window.octsmartsubscribe.jq;
		var $popupOverlay = $('<div id="octsmartsubscribe-popup-overlay"></div>'),
			options = window.octsmartsubscribeOptions;
		$popupOverlay.css({
			'background-color': options.popupOverlayColor,
			'display': 'none',
			'height': '100%',
			'left': '0px',
			'opacity': window.octsmartsubscribe.config.overlayOpacity,
			'position': 'fixed',
			'top': '0px',
			'width': '100%',
			'z-index': 99999
		});
		$('body').append($popupOverlay);

		var $popup = $('<div id="octsmartsubscribe-popup"></div>');
		$popup.css({
			'background': '#FFFFFF',
			'border-radius': '5px',
			'box-shadow': '0 0 20px ' + options.popupOverlayColor,
			'display': 'none',
			'margin': '0 auto',
			'padding': '18px',
			'position': 'fixed',
			'width': '250px',
			'z-index': 100000
		});
		$('body').append($popup);

		var $header = $('<h3></h3>').text(options.popupHeader);
		$header.css({
			'border': 'none',
			'color': '#333333',
			'display': 'block',
			'font-family': 'Arial, sans-serif',
			'font-size': '16px',
			'font-weight': 'bold',
			'line-height': '1.5em',
			'margin': '0 0 9px 0',
			'padding': '0',
			'text-align': 'center',
			'text-decoration': 'none'
		});
		$popup.append($header);

		if (options.type !== 'button') {
			var $popupClose = $('<div>x</div>');
			$popupClose.css({
				'background': '#000000',
				'border-radius': '50%',
				'color': '#FFFFFF',
				'cursor': 'pointer',
				'font-family': 'Arial, sans-serif',
				'font-size': '13px',
				'font-weight': 'bold',
				'height': '20px',
				'line-height': '20px',
				'position': 'absolute',
				'right': '-10px',
				'text-align': 'center',
				'top': '-10px',
				'width': '20px'
			});
			$popup.append($popupClose);
			window.octsmartsubscribe.els.popupClose = $popupClose;
		}

		var $text = $('<p></p>').text(options.popupText);
		$text.css({
			'border': 'none',
			'color': '#333333',
			'display': 'block',
			'font-family': 'Arial, sans-serif',
			'font-size': '13px',
			'font-weight': 'normal',
			'line-height': '1.5em',
			'margin': '9px 0',
			'padding': '0',
			'text-align': 'center',
			'text-decoration': 'none'
		});
		$popup.append($text);

		var $input = $('<input type="email" placeholder="' + options.placeholderText + '" required>').text(options.popupText);
		$input.css({
			'border': window.octsmartsubscribe.config.inputBorder,
			'border-radius': '4px',
			'box-shadow': 'inset 0 2px 2px #CCCCCC',
			'box-sizing': 'border-box',
			'color': '#333333',
			'display': 'block',
			'font-family': 'Arial, sans-serif',
			'font-size': '13px',
			'height': '2em',
			'line-height': '2em',
			'margin': '0 auto',
			'padding': '0 1em',
			'text-align': 'center',
			'width': '95%'
		});
		$popup.append($input);

		var $formButton = $('<a href="#"></a>').text(options.subscribeText);
		$formButton.css({
			'background-color': '#0077CC',
			'border-radius': '4px',
			'box-sizing': 'border-box',
			'color': '#FFFFFF',
			'display': 'block',
			'font-family': 'Arial, sans-serif',
			'font-size': '13px',
			'font-weight': 'bold',
			'height': '2em',
			'line-height': '2em',
			'margin': '5px auto 0 auto',
			'padding': '0 1em',
			'text-align': 'center',
			'text-decoration': 'none',
			'width': '95%'
		});
		$popup.append($formButton);

		if (options.type == 'button') {
			$formButton.css({
				'background-color': options.buttonBG,
				'color': options.buttonFG
			});
			if (options.buttonStyle == 'gloss') {
				$formButton[0].style.backgroundImage = "-o-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				$formButton[0].style.backgroundImage = "-ms-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				$formButton[0].style.backgroundImage = "-moz-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				$formButton[0].style.backgroundImage = "-webkit-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				$formButton[0].style.backgroundImage = "linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";

				$formButton.css('text-shadow', '0 2px 1px rgba(0, 0, 0, .3)');
			}
		}

		if (options.poweredBy) {
			var $poweredBy = $('<p></p>');
			$poweredBy.css({
				'border': 'none',
				'color': '#999999',
				'display': 'block',
				'font-family': 'Arial, sans-serif',
				'font-size': '10px',
				'font-weight': 'normal',
				'line-height': '1.5em',
				'margin': '9px 0 0 0',
				'padding': '0',
				'text-align': 'center',
				'text-decoration': 'none'
			});
			var $poweredByLink = $(options.poweredBy);
			$poweredByLink.css({
				'border': 'none',
				'color': '#999999',
				'display': 'block',
				'font-family': 'Arial, sans-serif',
				'font-size': '10px',
				'font-weight': 'normal',
				'line-height': '1.5em',
				'margin': '9px 0 0 0',
				'padding': '0',
				'text-align': 'center',
				'text-decoration': 'none'
			});
			$poweredBy.append($poweredByLink);
			$popup.append($poweredBy);
		}

		window.octsmartsubscribe.els.overlay = $popupOverlay;
		window.octsmartsubscribe.els.popup = $popup;
		window.octsmartsubscribe.els.input = $input;
		window.octsmartsubscribe.els.formButton = $formButton;

		window.octsmartsubscribe.positionPopupOnWindowResize();
	},
	attachEventListeners: function() {
		var $ = window.octsmartsubscribe.jq;
		var options = window.octsmartsubscribeOptions,
			els = window.octsmartsubscribe.els;
		if (options.type == 'button') {
			$(options.buttonSelector).on('click', window.octsmartsubscribe.showPopupWithClick);
			$(els.overlay).on('click', window.octsmartsubscribe.hidePopupWithClick);
			$(els.overlay).on('touchstart', window.octsmartsubscribe.hidePopupWithClick);
			$(els.input).on('keydown', function(ev) {
				if (ev.which === 27) {
					window.octsmartsubscribe.hidePopupWithClick(ev);
				}
			});
			$(window).on('resize', window.octsmartsubscribe.positionPopupOnWindowResize);
		} else {
			if (options.scrollWhen == 'element') {
				$(window).on('scroll', function(ev) {
					var $el = $(options.scrollSelector),
						scrollTop = $(window).scrollTop(),
						scrollBottom = scrollTop + $(window).height(),
						elTop = $el.offset().top,
						elBottom = elTop + ($el.outerHeight() / 5);
					
					if ((elBottom <= scrollBottom) && (elTop >= scrollTop)) {
						window.octsmartsubscribe.showPopupWhenScroll();
					}
				});
			} else {
				$(window).on('scroll', function(ev) {
					var windowHeight = $(window).height(),
						scrollTop = $(window).scrollTop(),
						percent = Math.round(scrollTop * 100 / windowHeight);

					if (percent >= options.scrollPercent) {
						window.octsmartsubscribe.showPopupWhenScroll();
					}
				});
			}
			$(els.popupClose).on('click', window.octsmartsubscribe.hidePopupWhenScroll);
		}

		$(els.formButton).on('click', window.octsmartsubscribe.submit);
		$(els.input).on('keydown', function(ev) {
			if (ev.which === 13) {
				window.octsmartsubscribe.submit(ev);
			}
		});
	},
	positionPopupOnWindowResize: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		var els = window.octsmartsubscribe.els,
			options = window.octsmartsubscribeOptions;

		if (options.type != 'button') return;

		els.popup.css({
			top: (($(window).height() - els.popup.outerHeight()) / 2) + 'px',
			left: (($(window).width() - els.popup.outerWidth()) / 2) + 'px'
		});
	},
	showPopupWhenScroll: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		if (window.octsmartsubscribe.isPopupOpen) return;
		if (window.octsmartsubscribe.isPopupClosedBefore) return;

		var options = window.octsmartsubscribeOptions,
			els = window.octsmartsubscribe.els;

		var popupLocation = {}, popupAnim = {};
		switch (options.popupLocation) {
			case "top":
				popupLocation = { top: '30px', 'left': (($(window).width() - els.popup.outerWidth()) / 2) + 'px' };
				popupAnim = { 'left': (($(window).width() - els.popup.outerWidth()) / 2) + 'px' };
				break;
			case "bottom":
				popupLocation = { bottom: '30px', 'left': (($(window).width() - els.popup.outerWidth()) / 2) + 'px' };
				popupAnim = { 'left': (($(window).width() - els.popup.outerWidth()) / 2) + 'px' };
				break;
			case "topright":
				popupLocation = { top: '30px', 'right': '30px' };
				popupAnim = { top: '30px' };
				break;
			case "right":
				popupLocation = { top: (($(window).height() - els.popup.outerHeight()) / 2) + 'px', 'right': '30px' };
				popupAnim = { top: (($(window).height() - els.popup.outerHeight()) / 2) + 'px' };
				break;
			case "bottomright":
				popupLocation = { bottom: '30px', 'right': '30px' };
				popupAnim = { bottom: '30px' };
				break;
			case "topleft":
				popupLocation = { top: '30px', 'left': '30px' };
				popupAnim = { top: '30px' };
				break;
			case "left":
				popupLocation = { top: (($(window).height() - els.popup.outerHeight()) / 2) + 'px', 'left': '30px' };
				popupAnim = { top: (($(window).height() - els.popup.outerHeight()) / 2) + 'px' };
				break;
			case "bottomleft":
				popupLocation = { bottom: '30px', 'left': '30px' };
				popupAnim = { bottom: '30px' };
				break;
		}

		if (options.popupAnimation == 'slidein' && (options.popupLocation == 'topright' || options.popupLocation == 'right' || options.popupLocation == 'bottomright')) {
			popupAnim.right = ((els.popup.outerWidth() + 50) * -1) + 'px';
		} else if (options.popupAnimation == 'slidein' && (options.popupLocation == 'topleft' || options.popupLocation == 'left' || options.popupLocation == 'bottomleft')) {
			popupAnim.left = ((els.popup.outerWidth() + 50) * -1) + 'px';
		} else if (options.popupAnimation == 'slidein' && (options.popupLocation == 'top')) {
			popupAnim.top = ((els.popup.outerHeight() + 50) * -1) + 'px';
		} else if (options.popupAnimation == 'slidein' && (options.popupLocation == 'bottom')) {
			popupAnim.bottom = ((els.popup.outerHeight() + 50) * -1) + 'px';
		} else {
			popupAnim = { opacity: 0 };
			els.popup.css(popupLocation);
			popupLocation = { opacity: 1 };
		}

		els.popup.css(popupAnim).show().animate(popupLocation);
		window.octsmartsubscribe.isPopupOpen = true;
	},
	hidePopupWhenScroll: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		var els = window.octsmartsubscribe.els;
		els.popup.hide();
		window.octsmartsubscribe.isPopupOpen = false;
		window.octsmartsubscribe.isPopupClosedBefore = true;
	},
	showPopupWithClick: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		ev.preventDefault();

		var options = window.octsmartsubscribeOptions,
			els = window.octsmartsubscribe.els;

		els.overlay.css('opacity', 0).show().animate({
			opacity: window.octsmartsubscribe.config.overlayOpacity
		}, {
			duration: 100,
			complete: function() {
				els.popup.show();
				els.input.focus();
			}
		});
	},
	hidePopupWithClick: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		if (window.octsmartsubscribe.isLoading) return;

		var options = window.octsmartsubscribeOptions,
			els = window.octsmartsubscribe.els;

		els.popup.hide();
		els.overlay.animate({
			opacity: 0
		}, {
			duration: 100,
			complete: function() {
				els.overlay.hide().css('opacity', window.octsmartsubscribe.config.overlayOpacity);
			}
		});

		els.input.css('border', window.octsmartsubscribe.config.inputBorder).val('').removeAttr('disabled');


		els.formButton.text(options.subscribeText);
		els.formButton.css({
			'background-color': '#0077CC',
			'color': '#FFFFFF',
			'font-size': '13px',
			'height': '2em',
			'line-height': '2em'
		});
		if (options.type == 'button') {
			els.formButton.css({
				'background-color': options.buttonBG,
				'color': options.buttonFG
			});
			if (options.buttonStyle == 'gloss') {
				els.formButton[0].style.backgroundImage = "-o-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				els.formButton[0].style.backgroundImage = "-ms-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				els.formButton[0].style.backgroundImage = "-moz-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				els.formButton[0].style.backgroundImage = "-webkit-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";
				els.formButton[0].style.backgroundImage = "linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)";

				els.formButton.css('text-shadow', '0 2px 1px rgba(0, 0, 0, .3)');
			}
		}
	},
	buttonLoadingAnimation: function() {
		var $ = window.octsmartsubscribe.jq;
		if (typeof window.octsmartsubscribe.animationCounter == 'undefined') {
			window.octsmartsubscribe.animationCounter = 0;
		}
		if (typeof window.octsmartsubscribe.animationDirection == 'undefined') {
			window.octsmartsubscribe.animationDirection = true;
		}
		var $button = window.octsmartsubscribe.els.formButton;

		var array = ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'];
		if ((window.octsmartsubscribe.animationDirection && window.octsmartsubscribe.animationCounter == array.length - 1) || (! window.octsmartsubscribe.animationDirection && window.octsmartsubscribe.animationCounter == 0)) {
			window.octsmartsubscribe.animationDirection = ! window.octsmartsubscribe.animationDirection;
		}
		array[window.octsmartsubscribe.animationCounter] = '&bull;';

		$button.html(array.join(''));

		if (window.octsmartsubscribe.animationDirection) {
			window.octsmartsubscribe.animationCounter++;
		} else {
			window.octsmartsubscribe.animationCounter--;
		}
		window.octsmartsubscribe.buttonAnimationTimeout = setTimeout(window.octsmartsubscribe.buttonLoadingAnimation, 20);
	},
	submit: function(ev) {
		var $ = window.octsmartsubscribe.jq;
		ev.preventDefault();
		var options = window.octsmartsubscribeOptions,
			els = window.octsmartsubscribe.els,
			emailRE = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		els.input.css('border', window.octsmartsubscribe.config.inputBorder);

		if (emailRE.test(els.input.val()) == false) {
			els.input.focus().select();
			var originalBorder = els.input.css('border');
			els.input.css('border-color', '#FF0000');
			els.input.animate({ borderWidth: '7px' }, { duration: 100, complete: function() { els.input.animate({ borderWidth: '2px' }, { duration: 100 }); } });
		} else {
			window.octsmartsubscribe.isLoading = true;
			els.input.attr('disabled', 'disabled');
			els.formButton.css(window.octsmartsubscribe.config.buttonLoading);
			window.octsmartsubscribe.buttonLoadingAnimation();

			$.ajax({
				url: options.submitURL,
				dataType: 'jsonp',
				data: {
					emailAddress: els.input.val(),
					lun: options.lun
				},
				success: function(response) {
					clearTimeout(window.octsmartsubscribe.buttonAnimationTimeout);
					els.formButton.html('&#10004');
					window.octsmartsubscribe.isLoading = false;
					setTimeout(function() {
						if (options.type == 'button') {
							window.octsmartsubscribe.hidePopupWithClick();
						} else {
							window.octsmartsubscribe.hidePopupWhenScroll();
						}
					}, 1000);
				}
			});
		}
	}
};

function onJQUERYload(makeConflictCall) { 
	if (makeConflictCall) {
		$.noConflict();
	}
	(function($) {
		window.octsmartsubscribe.init($);
	})(jQuery);
}

if ( ! window.octsmartsubscribeOptions) {

} else {
	if (! window.jQuery || jQuery.fn.jquery != '1.11.0') {
		var eH = document.getElementsByTagName('head')[0], eS = document.createElement('script');
		eS.type = 'text/javascript';
		eS.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js';
		eS.onload = function() { onJQUERYload(true); };
		eH.appendChild(eS);
	} else {
		onJQUERYload(false);
	}
}