function onTypeChange() {
	var value = $('#Type').val();
	if (value == 'button') {
		$('.button-options').show();
		$('.scroll-options').hide();

		$('#popup-preview').css({
			position: 'relative',
			top: 'auto',
			left: 'auto',
			right: 'auto',
			bottom: 'auto'
		});

	} else if (value == 'scroll') {
		$('.button-options').hide();
		$('.scroll-options').show();

		$('#popup-preview').css({ position: 'absolute' });
		updatePreviewLocation();
	}
	$('#popup-preview-box').height($('#popup-preview').outerHeight() + 50);
}

function updatePreviewLocation() {
	var positionProperties = {};
	switch ($('#PopupLocation').val()) {
		case "top":
			positionProperties.top = '10px';
			positionProperties.left = '50%';
			positionProperties.right = 'auto';
			positionProperties.bottom = 'auto';
			positionProperties.marginLeft = '-143px'
			break;
		case "topright":
			positionProperties.top = '10px';
			positionProperties.left = 'auto';
			positionProperties.right = '10px';
			positionProperties.bottom = 'auto';
			positionProperties.marginLeft = '0'
			break;
		case "right":
			positionProperties.top = 'auto';
			positionProperties.left = 'auto';
			positionProperties.right = '10px';
			positionProperties.bottom = 'auto';
			positionProperties.marginLeft = '0'
			break;
		case "bottomright":
			positionProperties.top = 'auto';
			positionProperties.left = 'auto';
			positionProperties.right = '10px';
			positionProperties.bottom = '10px';
			positionProperties.marginLeft = '0'
			break;
		case "bottom":
			positionProperties.top = 'auto';
			positionProperties.left = '50%';
			positionProperties.right = 'auto';
			positionProperties.bottom = '10px';
			positionProperties.marginLeft = '-143px'
			break;
		case "bottomleft":
			positionProperties.top = 'auto';
			positionProperties.left = '10px';
			positionProperties.right = 'auto';
			positionProperties.bottom = '10px';
			positionProperties.marginLeft = '0'
			break;
		case "left":
			positionProperties.top = 'auto';
			positionProperties.left = '10px';
			positionProperties.right = 'auto';
			positionProperties.bottom = 'auto';
			positionProperties.marginLeft = '0'
			break;
		case "topleft":
			positionProperties.top = '10px';
			positionProperties.left = '10px';
			positionProperties.right = 'auto';
			positionProperties.bottom = 'auto';
			positionProperties.marginLeft = '0'
			break;
	}

	$('#popup-preview').css(positionProperties);
}

function updatePreviewButton() {
	var $button = $('#button-preview'),
		$popupButton = $('#popup-button-preview');

	var buttonOptions = {
		label: $('#ButtonLabel').val() == '' ? Language['0007'] : $('#ButtonLabel').val()
	};

	var flatButtonOptions = {
		backgroundColor: $('#ButtonBackgroundColor').val() == '' ? '#0077CC' : $('#ButtonBackgroundColor').val(),
		color: $('#ButtonLabelColor').val() == '' ? '#FFFFFF' : $('#ButtonLabelColor').val(),
		fontSize: $('#ButtonLabelSize').val() == '' ? '18px' : $('#ButtonLabelSize').val(),
		backgroundImage: 'none',
		textShadow: 'none',
		border: 'none'
	};

	var glossButtonOptions = {
		backgroundColor: $('#ButtonBackgroundColor').val() == '' ? '#0077CC' : $('#ButtonBackgroundColor').val(),
		color: $('#ButtonLabelColor').val() == '' ? '#FFFFFF' : $('#ButtonLabelColor').val(),
		fontSize: $('#ButtonLabelSize').val() == '' ? '18px' : $('#ButtonLabelSize').val(),
		textShadow: '0 2px 1px rgba(0, 0, 0, .3)',
		border: '2px solid rgba(0, 0, 0, .1)'
	}


	$button.text(buttonOptions.label);
	if ($('#ButtonStyle').val() == 'flat') {
		$button.css(flatButtonOptions);

		popupButtonOptions = flatButtonOptions;
		popupButtonOptions.fontSize = '14px';
		
		$popupButton.css(flatButtonOptions);
	} else {
		$button.css(glossButtonOptions);
		$button.css('background-image', '-webkit-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', '-moz-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', '-ms-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', 'linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)');
		
		popupButtonOptions = glossButtonOptions;
		popupButtonOptions.fontSize = '14px';
		
		$popupButton.css(glossButtonOptions);
		$popupButton.css('background-image', '-webkit-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', '-moz-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', '-ms-linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)')
			.css('background-image', 'linear-gradient(top, rgba(255, 255, 255, .3), rgba(255, 255, 255, .3) 49%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, .3) 100%)');
	}
}

function updatePreviewPopup() {
	var $overlay = $('#popup-overlay-preview'),
		$popup = $('#popup-preview');

	var overlayColor = $('#PopupOverlayColor').val() == '' ? '#0077CC' : $('#PopupOverlayColor').val();

	$overlay.css({ backgroundColor: overlayColor });
	$popup.css({ boxShadow: '0 0 20px ' + overlayColor });

	$('h3', $popup).text($('#PopupHeader').val());

	var html = $('<div>' + $('#PopupText').val().replace(/src="[^"]*"/gi, "src=\"\"") + '</div>');
	html.children().not('a, span, strong, em, br').remove();
	$('#text-for-popup', $popup).html(html.html());

	$('#popup-preview-box').height($('#popup-preview').outerHeight() + 50);
}

function updateScrollWhen() {
	var value = $('#ScrollWhen').val();
	if (value == 'element') {
		$('#form-row-ScrollPercent').hide();
		$('#form-row-ScrollElement').show();
	} else {
		$('#form-row-ScrollPercent').show();
		$('#form-row-ScrollElement').hide();
	}
}

$(document).ready(function() {
	$('#Type').change(onTypeChange);

	$('#button-preview').click(function(ev) { ev.preventDefault(); });

	$('#ButtonLabel, #ButtonBackgroundColor, #ButtonLabelColor, #ButtonLabelSize').keyup(updatePreviewButton);
	$('#ButtonStyle').change(updatePreviewButton);
	$('#PopupHeader, #PopupText, #PopupOverlayColor').keyup(updatePreviewPopup);
	$('#ScrollWhen').change(updateScrollWhen);
	$('#PopupLocation').change(updatePreviewLocation);
	
	updatePreviewButton();
	updatePreviewPopup();
	updateScrollWhen();

	onTypeChange();
});