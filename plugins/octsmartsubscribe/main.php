<?php
include_once(PLUGIN_PATH . 'octsmartsubscribe/functions.php');

class octsmartsubscribe extends Plugins
{
	private static $PluginCode = 'octsmartsubscribe';
	private static $ArrayLanguage = array();
	public static $ObjectCI = null;
	private static $UserInformation = array();
	private static $AdminInformation = array();
	private static $Models = null;
	
	private static $PluginLicenseKey = null;
	private static $PluginLicenseStatus = true;
	private static $PluginLicenseInfo = null;
	
	private static $PluginVersion = "1.0.1";


	// -------------------------------------
	// Default Plugin Methods
	// -------------------------------------

	public function enable_octsmartsubscribe()
	{
		self::_LoadConfig();

		// License check
		self::_CheckLicenseStatus(true);
		if (self::$PluginLicenseStatus == false)
		{
			if (defined('octsmartsubscribe_LicenseStatusMessage') == true && octsmartsubscribe_LicenseStatusMessage == 'not_bound_to_this_oempro')
			{
				$_SESSION['PluginMessage'] = array(false, 'The plugin license does not belong to this Oempro license.<br>Please install the correct smart subscribe plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="'.InterfaceAppUrl(true).'/octsmartsubscribe/trial_request">request a trial license</a>.');
				header("location: ".InterfaceAppURL(true).'/admin/plugins/disable/octsmartsubscribe');
				exit;
			}
			else
			{
				$_SESSION['PluginMessage'] = array(false, 'Please install the smart subscribe plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="'.InterfaceAppUrl(true).'/octsmartsubscribe/trial_request">request a trial license</a>.');
				header("location: ".InterfaceAppURL(true).'/admin/plugins/disable/octsmartsubscribe');
				exit;
			}
		}
	}

	public function disable_octsmartsubscribe()
	{
		Database::$Interface->DeleteRows_Enhanced(array(
			'Table' => MYSQL_TABLE_PREFIX.'options',
			'Criteria' => array(
				array('Column' => 'OptionName', 'Operator' => 'LIKE', 'Value' => 'octsmartsubscribe_%')
			)
		));
	}

	public function load_octsmartsubscribe()
	{
		// Load language - Start
		$Language = Database::$Interface->GetOption(self::$PluginCode . '_Language');
		if (count($Language) == 0) {
			Database::$Interface->SaveOption(self::$PluginCode . '_Language', 'en');
			$Language = 'en';
		} else {
			$Language = $Language[0]['OptionValue'];
		}
		$ArrayPlugInLanguageStrings = array();
		if (file_exists(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php') == true) {
			include_once(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php');
		} else {
			include_once(PLUGIN_PATH . self::$PluginCode . '/languages/en/en.inc.php');
		}
		self::$ArrayLanguage = $ArrayPlugInLanguageStrings;
		unset($ArrayPlugInLanguageStrings);
		// Load language - End

		// License check
		self::_CheckLicenseStatus();

		if (self::$PluginLicenseStatus == false)
		{
			return false;
		}

		parent::RegisterEnableHook(self::$PluginCode);
		parent::RegisterDisableHook(self::$PluginCode);

		parent::RegisterMenuHook(self::$PluginCode, 'set_menu_items');

		self::_LoadConfig();

	}

	// -------------------------------------
	// Hooks, menu item setups, template tag setups
	// -------------------------------------

	public function set_menu_items()
	{
		$ArrayMenuItems = array(
			'MenuLocation' => 'User.List.Options',
			'MenuID' => 'Smart Subscribe',
			'MenuLink' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/settings',
			'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
		);

		return $ArrayMenuItems;
	}

	// -------------------------------------
	// Controllers (user interface methods)
	// -------------------------------------

	public function ui_trial_request()
	{
		if (file_exists(APP_PATH . '/data/license_octsmartsubscribe.dat') == true)
		{
			$_SESSION['PluginMessage'] = array(false, 'It seems that a license for this plugin has been already generated.');
			header("location: ".InterfaceAppURL(true).'/admin/plugins');
			exit;
		}

		include_once(PLUGIN_PATH . '/octsmartsubscribe/functions.php');

		$LicenseEngine = new octsmartsubscribe_license_engine();
		$LicenseExpireDate = $LicenseEngine->request_trial_license();

		if (is_array($LicenseExpireDate) == true && is_bool($LicenseExpireDate[0]) == true && $LicenseExpireDate[0] == false)
		{
			$_SESSION['PluginMessage'] = array(false, $LicenseExpireDate[1]);
			header("location: ".InterfaceAppURL(true).'/admin/plugins');
			exit;
		}

		$_SESSION['PluginMessage'] = array(true, 'Trial period has just started. It will expire on '.date('Y-m-d', strtotime($LicenseExpireDate)));
		header("location: ".InterfaceAppURL(true).'/admin/plugins/enable/octsmartsubscribe');
		exit;
	}



	public function ui_settings($ListID)
	{
		if (self::_PluginAppHeader('User') == false) return;

		Core::LoadObject('api');

		// Retrieve list information - Start {
		$APIResult = API::call(array(
			'format'	=>	'array',
			'command'	=>	'list.get',
			'protected'	=>	true,
			'username'	=>	self::$UserInformation['Username'],
			'password'	=>	self::$UserInformation['Password'],
			'parameters'=>	array('listid' => $ListID)
			));

		if ($APIResult['Success'] == false) {
			self::$ObjectCI->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}
		$ListInformation = $APIResult['List'];
		// Retrieve list information - End }

		$OptionsForList = Database::$Interface->GetOption(self::$PluginCode . '_OptionsForList_' . $ListID);
		if (count($OptionsForList) < 1) {
			$OptionsForList = new stdClass;
			$OptionsForList->type = 'button';
			$OptionsForList->buttonLabel = self::$ArrayLanguage['Screen']['0007'];
			$OptionsForList->buttonLabelSize = '18px';
			$OptionsForList->buttonStyle = 'gloss';
			$OptionsForList->buttonBG = '#0077CC';
			$OptionsForList->buttonFG = '#FFFFFF';
			$OptionsForList->popupHeader = self::$ArrayLanguage['Screen']['0045'];
			$OptionsForList->popupText = self::$ArrayLanguage['Screen']['0046'];
			$OptionsForList->popupOverlayColor = '#0077CC';
		} else {
			$OptionsForList = json_decode($OptionsForList[0]['OptionValue']);
		}


		// Load and display the view to the user. First, we set the information which will be transferred
		// to the view file for processing:
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0001'],
			'PluginLanguage' => self::$ArrayLanguage,
			'SubSection' => 'Smart Subscribe',
			'CurrentMenuItem' => 'Lists',
			'UserInformation' => self::$UserInformation,
			'ListInformation' => $ListInformation,
			'OptionsForList' => $OptionsForList
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		// Load the view file
		self::$ObjectCI->plugin_render(self::$PluginCode, 'user_settings', $ArrayViewData, true);
	}

	public function ui_code($ListID)
	{
		if (self::_PluginAppHeader('User') == false) return;

		if (! isset($_POST['Command']) && $_POST['Command'] !== 'GenerateEmbedCode') {
			self::$ObjectCI->display_user_message(self::$ArrayLanguage['Screen']['0043'], self::$ArrayLanguage['Screen']['0044']);
			return;
		}

		Core::LoadObject('api');

		// Retrieve list information - Start {
		$APIResult = API::call(array(
			'format'	=>	'array',
			'command'	=>	'list.get',
			'protected'	=>	true,
			'username'	=>	self::$UserInformation['Username'],
			'password'	=>	self::$UserInformation['Password'],
			'parameters'=>	array('listid' => $ListID)
			));

		if ($APIResult['Success'] == false) {
			self::$ObjectCI->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}
		$ListInformation = $APIResult['List'];
		// Retrieve list information - End }

		// Generate embed code - Start {
		$EmbedCodeProperties = new stdClass;
		$EmbedCodeProperties->lun = Core::EncryptNumberAdvanced($ListID) . '--' . Core::EncryptNumberAdvanced(self::$UserInformation['UserID']);
		$EmbedCodeProperties->submitURL = Core::InterfaceAppURL() . '/' . self::$PluginCode . '/subscribe';
		$EmbedCodeProperties->placeholderText = self::$ArrayLanguage['Screen']['0049'];
		$EmbedCodeProperties->subscribeText = self::$ArrayLanguage['Screen']['0007'];
		$EmbedCodeProperties->type = $_POST['Type'];

		if (OCTSMARTSUBSCRIBE_DISPLAY_WEBSITE_LINK_IN_POPUP) {
			$EmbedCodeProperties->poweredBy = '<a href="' . OCTSMARTSUBSCRIBE_POPUP_WEBSITE_LINK . '" target="_blank">' . sprintf(self::$ArrayLanguage['Screen']['0022'], empty(self::$UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : self::$UserInformation['GroupInformation']['ThemeInformation']['ProductName']) . '</a>';
		}

		$EmbedCodeProperties->buttonSelector = '.octsmartsubscribe-button';
		if ($EmbedCodeProperties->type == 'button') {
			$EmbedCodeProperties->buttonLabel = $_POST['ButtonLabel'];
			$EmbedCodeProperties->buttonLabelSize = $_POST['ButtonLabelSize'];
			$EmbedCodeProperties->buttonStyle = $_POST['ButtonStyle'];
			$EmbedCodeProperties->buttonBG = $_POST['ButtonBackgroundColor'];
			$EmbedCodeProperties->buttonFG = $_POST['ButtonLabelColor'];
		} else {
			$EmbedCodeProperties->scrollWhen = $_POST['ScrollWhen'];
			if ($EmbedCodeProperties->scrollWhen == 'element') {
				$EmbedCodeProperties->scrollSelector = $_POST['ScrollElement'];
			} else {
				$EmbedCodeProperties->scrollPercent = $_POST['ScrollPercent'];
			}
			$EmbedCodeProperties->popupLocation = $_POST['PopupLocation'];
			$EmbedCodeProperties->popupAnimation = $_POST['PopupAnimation'];
		}

		$EmbedCodeProperties->popupHeader = $_POST['PopupHeader'];
		$EmbedCodeProperties->popupText = $_POST['PopupText'];
		$EmbedCodeProperties->popupOverlayColor = $_POST['PopupOverlayColor'];

		Database::$Interface->SaveOption(self::$PluginCode . '_OptionsForList_' . $ListID, json_encode($EmbedCodeProperties));

		$EmbedSource = file_get_contents(PLUGIN_PATH . self::$PluginCode . '/assets/js/embed.js');
		$EmbedSource = str_replace('_HTTPS_URL_', OCTSMARTSUBSCRIBE_JS_HTTPS_URL, $EmbedSource);
		$EmbedSource = str_replace('_HTTP_URL_', OCTSMARTSUBSCRIBE_JS_HTTP_URL, $EmbedSource);

		$EmbedCodePropertiesEncoded = json_encode($EmbedCodeProperties);
		$EmbedCodePropertiesEncoded = str_replace('","', "\",\n\"", $EmbedCodePropertiesEncoded);
		$EmbedCodePropertiesEncoded = str_replace('{"', "{\n\"", $EmbedCodePropertiesEncoded);
		$EmbedCodePropertiesEncoded = str_replace('"}', "\"\n}", $EmbedCodePropertiesEncoded);
		$EmbedCode = '<script type="text/javascript">' . "\n" . 'window.octsmartsubscribeOptions = ' . $EmbedCodePropertiesEncoded . ";\n" . $EmbedSource . "\n</script>";

		if ($EmbedCodeProperties->type == 'button') {
			$EmbedCode .= "\n" . '<a href="#" class="octsmartsubscribe-button" id="octsmartsubscribe-button">' . $EmbedCodeProperties->buttonLabel . '</a>';
		}
		// Generate embed code - End }

		// Load and display the view to the user. First, we set the information which will be transferred
		// to the view file for processing:
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0001'],
			'PluginLanguage' => self::$ArrayLanguage,
			'SubSection' => 'Smart Subscribe',
			'CurrentMenuItem' => 'Lists',
			'UserInformation' => self::$UserInformation,
			'ListInformation' => $ListInformation,
			'EmbedCode' => $EmbedCode
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		// Load the view file
		self::$ObjectCI->plugin_render(self::$PluginCode, 'user_code', $ArrayViewData, true);
	}

	public function ui_subscribe()
	{
		if (! isset($_REQUEST['callback'])) self::return_OK_result();
		if (! isset($_REQUEST['emailAddress'])) self::return_OK_result();
		if (! isset($_REQUEST['lun'])) self::return_OK_result();

		list($ListID, $UserID) = explode('--', $_REQUEST['lun']);
		$ListID = Core::DecryptNumberAdvanced($ListID);
		$UserID = Core::DecryptNumberAdvanced($UserID);

		Core::LoadObject('users');
		$User = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
		if (! $User) self::return_OK_result();
		$Permissions = explode(',', $User['GroupInformation']['Permissions']);
		if (! in_array('Plugin.' . self::$PluginCode, $Permissions)) self::return_OK_result();
		
		Core::LoadObject('api');
		$result = API::call(array(
			'format'	=>	'array',
			'command'	=>	'subscriber.subscribe',
			'parameters'=>	array(
				'listid' => $ListID,
				'emailaddress' => $_REQUEST['emailAddress'],
				'ipaddress' => $_SERVER['REMOTE_ADDR']
			)
		));

		self::return_OK_result();
	}

	private function return_OK_result()
	{
		header("Content-type: application/x-javascript");
		echo $_REQUEST['callback'].'({"result":"OK"});';
		exit;
	}


	// -------------------------------------
	// Private methods
	// -------------------------------------

	// This method should be called in every user interface method (controller) as the
	// first line. You can force controller to be executed for "Admin" or "User" authorized
	// account owners.
	private function _PluginAppHeader($Section = 'Admin')
	{
		self::$ObjectCI =& get_instance();

		// License status check
		if (self::$PluginLicenseStatus == false)
		{
			$Message = sprintf(self::$ArrayLanguage['Screen']['0051'], (defined('octsmartsubscribe_LicenseStatusMessage') == true ? (octsmartsubscribe_LicenseStatusMessage == 'expired' ? self::$ArrayLanguage['Screen']['0050'] : '') : ''), self::$PluginLicenseKey);
			self::$ObjectCI->display_public_message('', $Message);
			return false;
		}

		if (Plugins::IsPlugInEnabled(self::$PluginCode) == false)
		{
			$Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
			self::$ObjectCI->display_public_message('', $Message);
			return false;
		}

		if ($Section == 'Admin')
		{
			self::_CheckAdminAuth();
		}
		elseif ($Section == 'User')
		{
			self::_CheckUserAuth();
		}

		return true;
	}

	// This is a private method for checking logged in user administrator privileges
	private function _CheckAdminAuth()
	{
		Core::LoadObject('admin_auth');
		Core::LoadObject('admins');

		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');

		self::$AdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));

		return;
	}

	// This is a private method for checking logged in user privileges
	private function _CheckUserAuth()
	{
		Core::LoadObject('user_auth');

		UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');

		self::$UserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);

		if (Users::IsAccountExpired(self::$UserInformation) == true)
		{
			$_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
			self::$ObjectCI->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
		}

		return;
	}

	private function _CheckLicenseStatus()
	{
		global $LicenseCheck;

		self::$PluginLicenseStatus = OctSmartSubscribe_PerformLicenseCheck();
		self::$PluginLicenseKey = $LicenseCheck->get_plugin_license_key();
		self::$PluginLicenseInfo = $LicenseCheck->get_plugin_license_info();
		if (self::$PluginLicenseStatus == false)
		{
			return false;
		}
		return true;
	}

	// Plugin config is loaded if exists
	private function _LoadConfig()
	{
		$ConfigFile = PLUGIN_PATH . self::$PluginCode . '/data/config.inc.php';
		if (file_exists($ConfigFile) == true) {
			include_once($ConfigFile);
		}
	}
}