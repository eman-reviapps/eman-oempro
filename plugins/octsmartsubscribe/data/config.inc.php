<?php
// Enable or disable "Powered by X" link in subscription popup. "X" is the name of the product setup in user group settings.
define('OCTSMARTSUBSCRIBE_DISPLAY_WEBSITE_LINK_IN_POPUP', false);
define('OCTSMARTSUBSCRIBE_POPUP_WEBSITE_LINK', 'http://');

// You can host smart subscribe javascript file anywhere. Default is your Oempro installation URL
define('OCTSMARTSUBSCRIBE_JS_HTTP_URL', PLUGIN_URL . 'octsmartsubscribe/assets/js/subscribe.min.js');
define('OCTSMARTSUBSCRIBE_JS_HTTPS_URL', PLUGIN_URL . 'octsmartsubscribe/assets/js/subscribe.min.js');