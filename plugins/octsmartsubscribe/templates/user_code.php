<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <!--                            <div class="portlet-title">
                            <?php // echo $PluginLanguage['Screen']['0001']; ?>
                                                        </div>-->
                            <div class="portlet-body">
                                <span class="help-block"><?php echo $PluginLanguage['Screen']['0047']; ?></span>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea onclick="$(this).select()" class="form-control" readonly="readonly" rows="12" style="font-size:12px;font-family:monospace;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;">
                                                <?php echo htmlspecialchars($EmbedCode); ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row-note no-bg" style="margin-left:18px;">
                                    <p><a href="<?php echo InterfaceAppURL(); ?>/octsmartsubscribe/settings/<?php echo $ListInformation['ListID']; ?>" class="btn default">&larr; <?php echo $PluginLanguage['Screen']['0048']; ?></a></p>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>