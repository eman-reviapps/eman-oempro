<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include_once(TEMPLATE_PATH . 'desktop/user/list_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/list_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <!--                            <div class="portlet-title">
                            <?php // echo $PluginLanguage['Screen']['0001']; ?>
                                                        </div>-->
                            <div class="portlet-body">
                                <form id="smart-subscribe-settings" action="<?php InterfaceAppURL(); ?>/octsmartsubscribe/code/<?php echo $ListInformation['ListID']; ?>" method="post">
                                    <input type="hidden" name="Command" value="GenerateEmbedCode" id="Command">

                                    <span class="help-block"><?php echo $PluginLanguage['Screen']['0021']; ?></span>
                                    <br/>
                                    <h4 class="form-section bold font-blue"><?php echo $PluginLanguage['Screen']['0002']; ?></h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" id="form-row-Type">
                                                <label class="col-md-3 control-label" for="Type"><?php echo $PluginLanguage['Screen']['0004']; ?>:</label>
                                                <div class="col-md-3">
                                                    <select name="Type" id="Type" class="form-control">
                                                        <option value="button" <?php echo $OptionsForList->type == 'button' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0005']; ?></option>
                                                        <option value="scroll" <?php echo $OptionsForList->type == 'scroll' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0006']; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="button-options" style="position:relative;">
                                        <h4 class="form-section bold font-blue"><?php echo $PluginLanguage['Screen']['0008']; ?></h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ButtonLabel">
                                                    <label class="col-md-3 control-label" for="ButtonLabel"><?php echo $PluginLanguage['Screen']['0009']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ButtonLabel" value="<?php echo isset($OptionsForList->buttonLabel) ? $OptionsForList->buttonLabel : $PluginLanguage['Screen']['0007']; ?>" id="ButtonLabel" class="form-control" style="width:250px;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ButtonLabelSize">
                                                    <label class="col-md-3 control-label" for="ButtonLabelSize"><?php echo $PluginLanguage['Screen']['0016']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ButtonLabelSize" value="<?php echo isset($OptionsForList->buttonLabelSize) ? $OptionsForList->buttonLabelSize : '18px'; ?>" id="ButtonLabelSize" class="form-control" style="width:50px;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ButtonStyle">
                                                    <label class="col-md-3 control-label" for="ButtonStyle"><?php echo $PluginLanguage['Screen']['0010']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <select name="ButtonStyle" id="ButtonStyle" class="form-control">
                                                            <option value="flat" <?php echo isset($OptionsForList->buttonStyle) && $OptionsForList->buttonStyle == 'flat' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0011']; ?></option>
                                                            <option value="gloss" <?php echo isset($OptionsForList->buttonStyle) && $OptionsForList->buttonStyle == 'gloss' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0012']; ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ButtonBackgroundColor">
                                                    <label class="col-md-3 control-label" for="ButtonBackgroundColor"><?php echo $PluginLanguage['Screen']['0013']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ButtonBackgroundColor" value="<?php echo isset($OptionsForList->buttonBG) ? $OptionsForList->buttonBG : '#0077CC'; ?>" id="ButtonBackgroundColor" class="form-control" style="width:150px;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ButtonLabelColor">
                                                    <label class="col-md-3 control-label" for="ButtonLabelColor"><?php echo $PluginLanguage['Screen']['0014']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ButtonLabelColor" value="<?php echo isset($OptionsForList->buttonFG) ? $OptionsForList->buttonFG : '#FFFFFF'; ?>" id="ButtonLabelColor" class="form-control" style="width:150px;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="box-shadow:0 0 20px rgba(0,0,0,.2);margin:18px 9px;text-align:center;border:1px dashed #616265;padding:72px 18px;position:relative;background:white;width:300px;position:absolute;top:90px;right:10px;height:50px;">
                                            <div style="background:#616265;color:#CCCCCC;font-size:10px;position:absolute;top:0;left:0;width:auto;padding:5px 8px;"><?php echo $PluginLanguage['Screen']['0015']; ?></div>
                                            <a href="#" id="button-preview" style="display:inline-block;font-weight:bold;font-size:14px;line-height:1.5em;text-decoration:none;border-radius:5px;padding:.3em 1em;"></a>
                                        </div>
                                    </div>

                                    <div class="scroll-options">
                                        <h4 class="form-section bold font-blue"><?php echo $PluginLanguage['Screen']['0024']; ?></h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ScrollWhen">
                                                    <label class="col-md-3 control-label" for="ScrollWhen"><?php echo $PluginLanguage['Screen']['0025']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <select name="ScrollWhen" id="ScrollWhen" class="form-control">
                                                            <option value="element" <?php echo isset($OptionsForList->scrollWhen) && $OptionsForList->scrollWhen == 'element' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0026']; ?></option>
                                                            <option value="scroll" <?php echo isset($OptionsForList->scrollWhen) && $OptionsForList->scrollWhen == 'scroll' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0027']; ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ScrollPercent" style="display:none">
                                                    <label class="col-md-3 control-label" for="ScrollPercent"><?php echo $PluginLanguage['Screen']['0028']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ScrollPercent" id="ScrollPercent" value="<?php echo isset($OptionsForList->scrollPercent) ? $OptionsForList->scrollPercent : '50'; ?>" class="form-control" style="width:25px;text-align:right;"> <strong style="font-family:arial;">%</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-ScrollElement" style="display:none">
                                                    <label class="col-md-3 control-label" for="ScrollElement"><?php echo $PluginLanguage['Screen']['0029']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" name="ScrollElement" id="ScrollElement" value="<?php echo isset($OptionsForList->scrollSelector) ? $OptionsForList->scrollSelector : '#element-id'; ?>" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br/>
                                    <h4 class="form-section bold font-blue"><?php echo $PluginLanguage['Screen']['0023']; ?></h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix" id="form-row-PopupHeader">
                                                <label class="col-md-3 control-label" for="PopupHeader"><?php echo $PluginLanguage['Screen']['0018']; ?>:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="PopupHeader" value="<?php echo isset($OptionsForList->popupHeader) ? $OptionsForList->popupHeader : $PluginLanguage['Screen']['0007']; ?>" id="PopupHeader" class="form-control" style="width:450px;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">       
                                            <div class="form-group clearfix" id="form-row-PopupText">
                                                <label class="col-md-3 control-label" for="PopupText"><?php echo $PluginLanguage['Screen']['0019']; ?>:</label>
                                                <div class="col-md-6">
                                                    <textarea name="PopupText" id="PopupText" rows="4" class="form-control"><?php echo isset($OptionsForList->popupText) ? $OptionsForList->popupText : $PluginLanguage['Screen']['0046']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix" id="form-row-PopupOverlayColor">
                                                <label class="col-md-3 control-label" for="PopupOverlayColor"><?php echo $PluginLanguage['Screen']['0020']; ?>:</label>
                                                <div class="col-md-3">
                                                    <input type="text" name="PopupOverlayColor" value="<?php echo isset($OptionsForList->popupOverlayColor) ? $OptionsForList->popupOverlayColor : '#0077CC'; ?>" id="PopupOverlayColor" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="scroll-options">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-PopupLocation">
                                                    <label class="col-md-3 control-label" for="PopupLocation"><?php echo $PluginLanguage['Screen']['0030']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <select name="PopupLocation" id="PopupLocation" class="form-control">
                                                            <option value="top" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'top' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0031']; ?></option>
                                                            <option value="topright" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'topright' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0032']; ?></option>
                                                            <option value="right" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'right' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0033']; ?></option>
                                                            <option value="bottomright" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'bottomright' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0034']; ?></option>
                                                            <option value="bottom" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'bottom' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0035']; ?></option>
                                                            <option value="bottomleft" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'bottomleft' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0036']; ?></option>
                                                            <option value="left" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'left' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0037']; ?></option>
                                                            <option value="topleft" <?php echo isset($OptionsForList->popupLocation) && $OptionsForList->popupLocation == 'topleft' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0038']; ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix" id="form-row-PopupAnimation">
                                                    <label class="col-md-3 control-label" for="PopupAnimation"><?php echo $PluginLanguage['Screen']['0039']; ?>:</label>
                                                    <div class="col-md-3">
                                                        <select name="PopupAnimation" id="PopupAnimation" class="form-control">
                                                            <option value="fadein" <?php echo isset($OptionsForList->popupAnimation) && $OptionsForList->popupAnimation == 'fadein' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0040']; ?></option>
                                                            <option value="slidein" <?php echo isset($OptionsForList->popupAnimation) && $OptionsForList->popupAnimation == 'slidein' ? 'selected' : ''; ?>><?php echo $PluginLanguage['Screen']['0041']; ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div style="box-shadow:0 0 20px rgba(0,0,0,.2);margin:18px auto;text-align:center;border:1px dashed #616265;padding:72px 18px;position:relative;background:white;width:500px;" id="popup-preview-box">
                                                <div style="background:#616265;color:#CCCCCC;font-size:10px;position:absolute;top:0;left:0;width:auto;padding:5px 8px;z-index:2;"><?php echo $PluginLanguage['Screen']['0017']; ?></div>
                                                <div id="popup-overlay-preview" style="opacity:.3;position:absolute;top:0;left:0;width:100%;height:100%;"></div>
                                                <div id="popup-preview" style="border-radius:5px;width:250px;background:white;padding:18px;margin:0 auto;position:relative;z-index:2;">
                                                    <h3 style="color:#333;font-size:16px;font-weight:bold;margin:0 0 9px 0;"></h3>
                                                    <p id="text-for-popup" style="color:#333;font-size:13px;margin:9px 0;"></p>
                                                    <div style="margin-top:9px;">
                                                        <input type="email" placeholder="<?php echo $PluginLanguage['Screen']['0049']; ?>" style="color:#333;box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-o-box-sizing:border-box;-webkit-box-sizing:border-box;border:1px solid #0077CC;width:95%;border-radius:4px;font-size:13px;line-height:2em;height:2em;padding:0 1em;text-align:center;box-shadow: inset 0 2px 2px #CCCCCC;display:inline-block;">
                                                        <a href="#" id="popup-button-preview" style="box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-o-box-sizing:border-box;-webkit-box-sizing:border-box;border:1px solid #0077CC;width:95%;border-radius:4px;font-size:13px;line-height:2em;padding:0 1em;text-align:center;background-color:#0077CC;display:inline-block;text-decoration:none;color:white;font-weight:bold;margin-top:5px;"><?php echo $PluginLanguage['Screen']['0007']; ?></a>
                                                    </div>
                                                    <?php if (OCTSMARTSUBSCRIBE_DISPLAY_WEBSITE_LINK_IN_POPUP): ?>
                                                        <p style="margin:9px 0 0 0;font-size:10px;color:#999999;"><a href="<?php echo OCTSMARTSUBSCRIBE_POPUP_WEBSITE_LINK; ?>" target="_blank" style="color:#999999;text-decoration:none;"><?php echo sprintf($PluginLanguage['Screen']['0022'], empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']); ?></a></p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a class="btn default" targetform="smart-subscribe-settings" id="form-button" href="#"><?php echo strtoupper($PluginLanguage['Screen']['0042']); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var Language = {
        '0007': '<?php echo $PluginLanguage['Screen']['0007']; ?>',
    };
</script>
<script src="<?php echo APP_URL; ?>/plugins/octsmartsubscribe/assets/js/settings.js" type="text/javascript" charset="utf-8"></script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>