<form action="<?php InterfaceAppURL(); ?>/octloginblocker/admin_settings/" method="POST" id="LoginBlockerOptions">

<div id="page-shadow">
	<div id="page">
		<div class="page-bar">
			<h2><?php print($PluginLanguage['Screen']['0016']); ?></h2>
		</div>
		<div class="white" style="min-height:430px;padding-top: 1px;">
			<?php if ($FormSuccess): ?>
				<h3 class="form-legend success"><?php echo $PluginLanguage['Screen']['0021']; ?></h3>
			<?php endif ?>

			<?php if ($BlocksRemoved): ?>
				<h3 class="form-legend success"><?php echo $PluginLanguage['Screen']['0022']; ?></h3>
			<?php endif ?>

			<div class="clearfix" style="background:#f7f7f7;margin:9px 9px 0 9px;padding:12px 9px;">
				<div style="background:white;border:1px solid #D0D0D0;border-bottom-width:5px;padding:0 10px 10px 10px;margin-right:15px;float:left; width:25%;">
					<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0017']); ?></h3>
					<div class="form-row no-bg">
						<div class="data-row">
							<span class="data big"><?php echo $BlockStatistics['AdminArea']; ?></span>
							<span class="data-label"><?php print($PluginLanguage['Screen']['0020']); ?></span>
							<span class="data"></span>
						</div>
					</div>
				</div>
				<div style="background:white;border:1px solid #D0D0D0;border-bottom-width:5px;padding:0 10px 10px 10px;margin-right:15px;float:left; width:25%;">
					<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0018']); ?></h3>
					<div class="form-row no-bg">
						<div class="data-row">
							<span class="data big"><?php echo $BlockStatistics['UserArea']; ?></span>
							<span class="data-label"><?php print($PluginLanguage['Screen']['0020']); ?></span>
							<span class="data"></span>
						</div>
					</div>
				</div>
				<div style="background:white;border:1px solid #D0D0D0;border-bottom-width:5px;padding:0 10px 10px 10px;float:left; width:25%;">
					<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0019']); ?></h3>
					<div class="form-row no-bg">
						<div class="data-row">
							<span class="data big"><?php echo $BlockStatistics['ClientArea']; ?></span>
							<span class="data-label"><?php print($PluginLanguage['Screen']['0020']); ?></span>
							<span class="data"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="form-row" style="margin-top:0;padding-bottom:12px;">
				<a href="<?php InterfaceAppURL(); ?>/octloginblocker/admin_remove_blocks/" class="button" style="display:inline-block;"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>REMOVE ALL BLOCKS</strong></a>
			</div>

			<h3 class="form-legend"><?php echo $PluginLanguage['Screen']['0006']; ?></h3>
			<div class="form-row" id="form-row-AdminAreaBlocking">
				<label for="AdminAreaBlocking"><?php echo $PluginLanguage['Screen']['0005']; ?>:</label>
				<select name="AdminAreaBlocking" id="AdminAreaBlocking" class="select area-blocking-enabler">
					<option value="enabled" <?php echo set_select('AdminAreaBlocking', 'enabled', $PluginOptions['AdminAreaBlocking'] == 'enabled'); ?>><?php echo $PluginLanguage['Screen']['0007']; ?></option>
					<option value="disabled" <?php echo set_select('AdminAreaBlocking', 'disabled', $PluginOptions['AdminAreaBlocking'] == 'disabled'); ?>><?php echo $PluginLanguage['Screen']['0008']; ?></option>
				</select>
			</div>
			<div style="display:none;">
				<div class="form-row <?php print((form_error('AdminAreaAllowedMaxTries') != '' ? 'error' : '')); ?>" id="form-row-AdminAreaAllowedMaxTries">
					<label for="AdminAreaAllowedMaxTries"><?php echo $PluginLanguage['Screen']['0009']; ?>: *</label>
					<input type="text" name="AdminAreaAllowedMaxTries" value="<?php echo set_value('AdminAreaAllowedMaxTries', $PluginOptions['AdminAreaAllowedMaxTries']); ?>" id="AdminAreaAllowedMaxTries" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0010']; ?></div>
					<?php print(form_error('AdminAreaAllowedMaxTries', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row <?php print((form_error('AdminAreaBlockDuration') != '' ? 'error' : '')); ?>" id="form-row-AdminAreaBlockDuration">
					<label for="AdminAreaBlockDuration"><?php echo $PluginLanguage['Screen']['0011']; ?>: *</label>
					<input type="text" name="AdminAreaBlockDuration" value="<?php echo set_value('AdminAreaBlockDuration', $PluginOptions['AdminAreaBlockDuration']); ?>" id="AdminAreaBlockDuration" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0012']; ?></div>
					<?php print(form_error('AdminAreaBlockDuration', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
			</div>

			<h3 class="form-legend"><?php echo $PluginLanguage['Screen']['0013']; ?></h3>
			<div class="form-row" id="form-row-UserAreaBlocking">
				<label for="UserAreaBlocking"><?php echo $PluginLanguage['Screen']['0005']; ?>:</label>
				<select name="UserAreaBlocking" id="UserAreaBlocking" class="select area-blocking-enabler">
					<option value="enabled" <?php echo set_select('UserAreaBlocking', 'enabled', $PluginOptions['UserAreaBlocking'] == 'enabled'); ?>><?php echo $PluginLanguage['Screen']['0007']; ?></option>
					<option value="disabled" <?php echo set_select('UserAreaBlocking', 'disabled', $PluginOptions['UserAreaBlocking'] == 'disabled'); ?>><?php echo $PluginLanguage['Screen']['0008']; ?></option>
				</select>
			</div>
			<div style="display:none;">
				<div class="form-row <?php print((form_error('UserAreaAllowedMaxTries') != '' ? 'error' : '')); ?>" id="form-row-UserAreaAllowedMaxTries">
					<label for="UserAreaAllowedMaxTries"><?php echo $PluginLanguage['Screen']['0009']; ?>: *</label>
					<input type="text" name="UserAreaAllowedMaxTries" value="<?php echo set_value('UserAreaAllowedMaxTries', $PluginOptions['UserAreaAllowedMaxTries']); ?>" id="UserAreaAllowedMaxTries" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0010']; ?></div>
					<?php print(form_error('UserAreaAllowedMaxTries', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row <?php print((form_error('UserAreaBlockDuration') != '' ? 'error' : '')); ?>" id="form-row-UserAreaBlockDuration">
					<label for="UserAreaBlockDuration"><?php echo $PluginLanguage['Screen']['0011']; ?>: *</label>
					<input type="text" name="UserAreaBlockDuration" value="<?php echo set_value('UserAreaBlockDuration', $PluginOptions['UserAreaBlockDuration']); ?>" id="UserAreaBlockDuration" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0012']; ?></div>
					<?php print(form_error('UserAreaBlockDuration', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
			</div>

			<h3 class="form-legend"><?php echo $PluginLanguage['Screen']['0014']; ?></h3>
			<div class="form-row" id="form-row-ClientAreaBlocking">
				<label for="ClientAreaBlocking"><?php echo $PluginLanguage['Screen']['0005']; ?>:</label>
				<select name="ClientAreaBlocking" id="ClientAreaBlocking" class="select area-blocking-enabler">
					<option value="enabled" <?php echo set_select('ClientAreaBlocking', 'enabled', $PluginOptions['ClientAreaBlocking'] == 'enabled'); ?>><?php echo $PluginLanguage['Screen']['0007']; ?></option>
					<option value="disabled" <?php echo set_select('ClientAreaBlocking', 'disabled', $PluginOptions['ClientAreaBlocking'] == 'disabled'); ?>><?php echo $PluginLanguage['Screen']['0008']; ?></option>
				</select>
			</div>
			<div style="display:none;">
			 	<div class="form-row <?php print((form_error('ClientAreaAllowedMaxTries') != '' ? 'error' : '')); ?>" id="form-row-ClientAreaAllowedMaxTries">
					<label for="ClientAreaAllowedMaxTries"><?php echo $PluginLanguage['Screen']['0009']; ?>: *</label>
					<input type="text" name="ClientAreaAllowedMaxTries" value="<?php echo set_value('ClientAreaAllowedMaxTries', $PluginOptions['ClientAreaAllowedMaxTries']); ?>" id="ClientAreaAllowedMaxTries" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0010']; ?></div>
					<?php print(form_error('ClientAreaAllowedMaxTries', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
				<div class="form-row <?php print((form_error('ClientAreaBlockDuration') != '' ? 'error' : '')); ?>" id="form-row-ClientAreaBlockDuration">
					<label for="ClientAreaBlockDuration"><?php echo $PluginLanguage['Screen']['0011']; ?>: *</label>
					<input type="text" name="ClientAreaBlockDuration" value="<?php echo set_value('ClientAreaBlockDuration', $PluginOptions['ClientAreaBlockDuration']); ?>" id="ClientAreaBlockDuration" class="text" style="width:150px;" />
					<div class="form-row-note"><?php echo $PluginLanguage['Screen']['0012']; ?></div>
					<?php print(form_error('ClientAreaBlockDuration', '<div class="form-row-note error"><p>', '</p></div>')); ?>
				</div>
			</div>

			<h3 class="form-legend"><?php echo $PluginLanguage['Screen']['0023']; ?></h3>
			<div class="form-row" id="form-row-BlockedCountries">
				<label for="BlockedCountries"><?php echo $PluginLanguage['Screen']['0024']; ?>:</label>
				<select name="BlockedCountries[]" id="BlockedCountries" multiple class="select" style="height:300px;">
					<option value="AF" <?php echo set_select('BlockedCountries', 'AF', in_array('AF', $BlockedCountries)); ?>>Afghanistan</option>
					<option value="AX" <?php echo set_select('BlockedCountries', 'AX', in_array('AX', $BlockedCountries)); ?>>Åland Islands</option>
					<option value="AL" <?php echo set_select('BlockedCountries', 'AL', in_array('AL', $BlockedCountries)); ?>>Albania</option>
					<option value="DZ" <?php echo set_select('BlockedCountries', 'DZ', in_array('DZ', $BlockedCountries)); ?>>Algeria</option>
					<option value="AS" <?php echo set_select('BlockedCountries', 'AS', in_array('AS', $BlockedCountries)); ?>>American Samoa</option>
					<option value="AD" <?php echo set_select('BlockedCountries', 'AD', in_array('AD', $BlockedCountries)); ?>>Andorra</option>
					<option value="AO" <?php echo set_select('BlockedCountries', 'AO', in_array('AO', $BlockedCountries)); ?>>Angola</option>
					<option value="AI" <?php echo set_select('BlockedCountries', 'AI', in_array('AI', $BlockedCountries)); ?>>Anguilla</option>
					<option value="AQ" <?php echo set_select('BlockedCountries', 'AQ', in_array('AQ', $BlockedCountries)); ?>>Antarctica</option>
					<option value="AG" <?php echo set_select('BlockedCountries', 'AG', in_array('AG', $BlockedCountries)); ?>>Antigua and Barbuda</option>
					<option value="AR" <?php echo set_select('BlockedCountries', 'AR', in_array('AR', $BlockedCountries)); ?>>Argentina</option>
					<option value="AM" <?php echo set_select('BlockedCountries', 'AM', in_array('AM', $BlockedCountries)); ?>>Armenia</option>
					<option value="AW" <?php echo set_select('BlockedCountries', 'AW', in_array('AW', $BlockedCountries)); ?>>Aruba</option>
					<option value="AU" <?php echo set_select('BlockedCountries', 'AU', in_array('AU', $BlockedCountries)); ?>>Australia</option>
					<option value="AT" <?php echo set_select('BlockedCountries', 'AT', in_array('AT', $BlockedCountries)); ?>>Austria</option>
					<option value="AZ" <?php echo set_select('BlockedCountries', 'AZ', in_array('AZ', $BlockedCountries)); ?>>Azerbaijan</option>
					<option value="BS" <?php echo set_select('BlockedCountries', 'BS', in_array('BS', $BlockedCountries)); ?>>Bahamas</option>
					<option value="BH" <?php echo set_select('BlockedCountries', 'BH', in_array('BH', $BlockedCountries)); ?>>Bahrain</option>
					<option value="BD" <?php echo set_select('BlockedCountries', 'BD', in_array('BD', $BlockedCountries)); ?>>Bangladesh</option>
					<option value="BB" <?php echo set_select('BlockedCountries', 'BB', in_array('BB', $BlockedCountries)); ?>>Barbados</option>
					<option value="BY" <?php echo set_select('BlockedCountries', 'BY', in_array('BY', $BlockedCountries)); ?>>Belarus</option>
					<option value="BE" <?php echo set_select('BlockedCountries', 'BE', in_array('BE', $BlockedCountries)); ?>>Belgium</option>
					<option value="BZ" <?php echo set_select('BlockedCountries', 'BZ', in_array('BZ', $BlockedCountries)); ?>>Belize</option>
					<option value="BJ" <?php echo set_select('BlockedCountries', 'BJ', in_array('BJ', $BlockedCountries)); ?>>Benin</option>
					<option value="BM" <?php echo set_select('BlockedCountries', 'BM', in_array('BM', $BlockedCountries)); ?>>Bermuda</option>
					<option value="BT" <?php echo set_select('BlockedCountries', 'BT', in_array('BT', $BlockedCountries)); ?>>Bhutan</option>
					<option value="BO" <?php echo set_select('BlockedCountries', 'BO', in_array('BO', $BlockedCountries)); ?>>Bolivia, Plurinational State of</option>
					<option value="BQ" <?php echo set_select('BlockedCountries', 'BQ', in_array('BQ', $BlockedCountries)); ?>>Bonaire, Sint Eustatius and Saba</option>
					<option value="BA" <?php echo set_select('BlockedCountries', 'BA', in_array('BA', $BlockedCountries)); ?>>Bosnia and Herzegovina</option>
					<option value="BW" <?php echo set_select('BlockedCountries', 'BW', in_array('BW', $BlockedCountries)); ?>>Botswana</option>
					<option value="BV" <?php echo set_select('BlockedCountries', 'BV', in_array('BV', $BlockedCountries)); ?>>Bouvet Island</option>
					<option value="BR" <?php echo set_select('BlockedCountries', 'BR', in_array('BR', $BlockedCountries)); ?>>Brazil</option>
					<option value="IO" <?php echo set_select('BlockedCountries', 'IO', in_array('IO', $BlockedCountries)); ?>>British Indian Ocean Territory</option>
					<option value="BN" <?php echo set_select('BlockedCountries', 'BN', in_array('BN', $BlockedCountries)); ?>>Brunei Darussalam</option>
					<option value="BG" <?php echo set_select('BlockedCountries', 'BG', in_array('BG', $BlockedCountries)); ?>>Bulgaria</option>
					<option value="BF" <?php echo set_select('BlockedCountries', 'BF', in_array('BF', $BlockedCountries)); ?>>Burkina Faso</option>
					<option value="BI" <?php echo set_select('BlockedCountries', 'BI', in_array('BI', $BlockedCountries)); ?>>Burundi</option>
					<option value="KH" <?php echo set_select('BlockedCountries', 'KH', in_array('KH', $BlockedCountries)); ?>>Cambodia</option>
					<option value="CM" <?php echo set_select('BlockedCountries', 'CM', in_array('CM', $BlockedCountries)); ?>>Cameroon</option>
					<option value="CA" <?php echo set_select('BlockedCountries', 'CA', in_array('CA', $BlockedCountries)); ?>>Canada</option>
					<option value="CV" <?php echo set_select('BlockedCountries', 'CV', in_array('CV', $BlockedCountries)); ?>>Cape Verde</option>
					<option value="KY" <?php echo set_select('BlockedCountries', 'KY', in_array('KY', $BlockedCountries)); ?>>Cayman Islands</option>
					<option value="CF" <?php echo set_select('BlockedCountries', 'CF', in_array('CF', $BlockedCountries)); ?>>Central African Republic</option>
					<option value="TD" <?php echo set_select('BlockedCountries', 'TD', in_array('TD', $BlockedCountries)); ?>>Chad</option>
					<option value="CL" <?php echo set_select('BlockedCountries', 'CL', in_array('CL', $BlockedCountries)); ?>>Chile</option>
					<option value="CN" <?php echo set_select('BlockedCountries', 'CN', in_array('CN', $BlockedCountries)); ?>>China</option>
					<option value="CX" <?php echo set_select('BlockedCountries', 'CX', in_array('CX', $BlockedCountries)); ?>>Christmas Island</option>
					<option value="CC" <?php echo set_select('BlockedCountries', 'CC', in_array('CC', $BlockedCountries)); ?>>Cocos (Keeling) Islands</option>
					<option value="CO" <?php echo set_select('BlockedCountries', 'CO', in_array('CO', $BlockedCountries)); ?>>Colombia</option>
					<option value="KM" <?php echo set_select('BlockedCountries', 'KM', in_array('KM', $BlockedCountries)); ?>>Comoros</option>
					<option value="CG" <?php echo set_select('BlockedCountries', 'CG', in_array('CG', $BlockedCountries)); ?>>Congo</option>
					<option value="CD" <?php echo set_select('BlockedCountries', 'CD', in_array('CD', $BlockedCountries)); ?>>Congo, the Democratic Republic of the</option>
					<option value="CK" <?php echo set_select('BlockedCountries', 'CK', in_array('CK', $BlockedCountries)); ?>>Cook Islands</option>
					<option value="CR" <?php echo set_select('BlockedCountries', 'CR', in_array('CR', $BlockedCountries)); ?>>Costa Rica</option>
					<option value="CI" <?php echo set_select('BlockedCountries', 'CI', in_array('CI', $BlockedCountries)); ?>>Côte d'Ivoire</option>
					<option value="HR" <?php echo set_select('BlockedCountries', 'HR', in_array('HR', $BlockedCountries)); ?>>Croatia</option>
					<option value="CU" <?php echo set_select('BlockedCountries', 'CU', in_array('CU', $BlockedCountries)); ?>>Cuba</option>
					<option value="CW" <?php echo set_select('BlockedCountries', 'CW', in_array('CW', $BlockedCountries)); ?>>Curaçao</option>
					<option value="CY" <?php echo set_select('BlockedCountries', 'CY', in_array('CY', $BlockedCountries)); ?>>Cyprus</option>
					<option value="CZ" <?php echo set_select('BlockedCountries', 'CZ', in_array('CZ', $BlockedCountries)); ?>>Czech Republic</option>
					<option value="DK" <?php echo set_select('BlockedCountries', 'DK', in_array('DK', $BlockedCountries)); ?>>Denmark</option>
					<option value="DJ" <?php echo set_select('BlockedCountries', 'DJ', in_array('DJ', $BlockedCountries)); ?>>Djibouti</option>
					<option value="DM" <?php echo set_select('BlockedCountries', 'DM', in_array('DM', $BlockedCountries)); ?>>Dominica</option>
					<option value="DO" <?php echo set_select('BlockedCountries', 'DO', in_array('DO', $BlockedCountries)); ?>>Dominican Republic</option>
					<option value="EC" <?php echo set_select('BlockedCountries', 'EC', in_array('EC', $BlockedCountries)); ?>>Ecuador</option>
					<option value="EG" <?php echo set_select('BlockedCountries', 'EG', in_array('EG', $BlockedCountries)); ?>>Egypt</option>
					<option value="SV" <?php echo set_select('BlockedCountries', 'SV', in_array('SV', $BlockedCountries)); ?>>El Salvador</option>
					<option value="GQ" <?php echo set_select('BlockedCountries', 'GQ', in_array('GQ', $BlockedCountries)); ?>>Equatorial Guinea</option>
					<option value="ER" <?php echo set_select('BlockedCountries', 'ER', in_array('ER', $BlockedCountries)); ?>>Eritrea</option>
					<option value="EE" <?php echo set_select('BlockedCountries', 'EE', in_array('EE', $BlockedCountries)); ?>>Estonia</option>
					<option value="ET" <?php echo set_select('BlockedCountries', 'ET', in_array('ET', $BlockedCountries)); ?>>Ethiopia</option>
					<option value="FK" <?php echo set_select('BlockedCountries', 'FK', in_array('FK', $BlockedCountries)); ?>>Falkland Islands (Malvinas)</option>
					<option value="FO" <?php echo set_select('BlockedCountries', 'FO', in_array('FO', $BlockedCountries)); ?>>Faroe Islands</option>
					<option value="FJ" <?php echo set_select('BlockedCountries', 'FJ', in_array('FJ', $BlockedCountries)); ?>>Fiji</option>
					<option value="FI" <?php echo set_select('BlockedCountries', 'FI', in_array('FI', $BlockedCountries)); ?>>Finland</option>
					<option value="FR" <?php echo set_select('BlockedCountries', 'FR', in_array('FR', $BlockedCountries)); ?>>France</option>
					<option value="GF" <?php echo set_select('BlockedCountries', 'GF', in_array('GF', $BlockedCountries)); ?>>French Guiana</option>
					<option value="PF" <?php echo set_select('BlockedCountries', 'PF', in_array('PF', $BlockedCountries)); ?>>French Polynesia</option>
					<option value="TF" <?php echo set_select('BlockedCountries', 'TF', in_array('TF', $BlockedCountries)); ?>>French Southern Territories</option>
					<option value="GA" <?php echo set_select('BlockedCountries', 'GA', in_array('GA', $BlockedCountries)); ?>>Gabon</option>
					<option value="GM" <?php echo set_select('BlockedCountries', 'GM', in_array('GM', $BlockedCountries)); ?>>Gambia</option>
					<option value="GE" <?php echo set_select('BlockedCountries', 'GE', in_array('GE', $BlockedCountries)); ?>>Georgia</option>
					<option value="DE" <?php echo set_select('BlockedCountries', 'DE', in_array('DE', $BlockedCountries)); ?>>Germany</option>
					<option value="GH" <?php echo set_select('BlockedCountries', 'GH', in_array('GH', $BlockedCountries)); ?>>Ghana</option>
					<option value="GI" <?php echo set_select('BlockedCountries', 'GI', in_array('GI', $BlockedCountries)); ?>>Gibraltar</option>
					<option value="GR" <?php echo set_select('BlockedCountries', 'GR', in_array('GR', $BlockedCountries)); ?>>Greece</option>
					<option value="GL" <?php echo set_select('BlockedCountries', 'GL', in_array('GL', $BlockedCountries)); ?>>Greenland</option>
					<option value="GD" <?php echo set_select('BlockedCountries', 'GD', in_array('GD', $BlockedCountries)); ?>>Grenada</option>
					<option value="GP" <?php echo set_select('BlockedCountries', 'GP', in_array('GP', $BlockedCountries)); ?>>Guadeloupe</option>
					<option value="GU" <?php echo set_select('BlockedCountries', 'GU', in_array('GU', $BlockedCountries)); ?>>Guam</option>
					<option value="GT" <?php echo set_select('BlockedCountries', 'GT', in_array('GT', $BlockedCountries)); ?>>Guatemala</option>
					<option value="GG" <?php echo set_select('BlockedCountries', 'GG', in_array('GG', $BlockedCountries)); ?>>Guernsey</option>
					<option value="GN" <?php echo set_select('BlockedCountries', 'GN', in_array('GN', $BlockedCountries)); ?>>Guinea</option>
					<option value="GW" <?php echo set_select('BlockedCountries', 'GW', in_array('GW', $BlockedCountries)); ?>>Guinea-Bissau</option>
					<option value="GY" <?php echo set_select('BlockedCountries', 'GY', in_array('GY', $BlockedCountries)); ?>>Guyana</option>
					<option value="HT" <?php echo set_select('BlockedCountries', 'HT', in_array('HT', $BlockedCountries)); ?>>Haiti</option>
					<option value="HM" <?php echo set_select('BlockedCountries', 'HM', in_array('HM', $BlockedCountries)); ?>>Heard Island and McDonald Islands</option>
					<option value="VA" <?php echo set_select('BlockedCountries', 'VA', in_array('VA', $BlockedCountries)); ?>>Holy See (Vatican City State)</option>
					<option value="HN" <?php echo set_select('BlockedCountries', 'HN', in_array('HN', $BlockedCountries)); ?>>Honduras</option>
					<option value="HK" <?php echo set_select('BlockedCountries', 'HK', in_array('HK', $BlockedCountries)); ?>>Hong Kong</option>
					<option value="HU" <?php echo set_select('BlockedCountries', 'HU', in_array('HU', $BlockedCountries)); ?>>Hungary</option>
					<option value="IS" <?php echo set_select('BlockedCountries', 'IS', in_array('IS', $BlockedCountries)); ?>>Iceland</option>
					<option value="IN" <?php echo set_select('BlockedCountries', 'IN', in_array('IN', $BlockedCountries)); ?>>India</option>
					<option value="ID" <?php echo set_select('BlockedCountries', 'ID', in_array('ID', $BlockedCountries)); ?>>Indonesia</option>
					<option value="IR" <?php echo set_select('BlockedCountries', 'IR', in_array('IR', $BlockedCountries)); ?>>Iran, Islamic Republic of</option>
					<option value="IQ" <?php echo set_select('BlockedCountries', 'IQ', in_array('IQ', $BlockedCountries)); ?>>Iraq</option>
					<option value="IE" <?php echo set_select('BlockedCountries', 'IE', in_array('IE', $BlockedCountries)); ?>>Ireland</option>
					<option value="IM" <?php echo set_select('BlockedCountries', 'IM', in_array('IM', $BlockedCountries)); ?>>Isle of Man</option>
					<option value="IL" <?php echo set_select('BlockedCountries', 'IL', in_array('IL', $BlockedCountries)); ?>>Israel</option>
					<option value="IT" <?php echo set_select('BlockedCountries', 'IT', in_array('IT', $BlockedCountries)); ?>>Italy</option>
					<option value="JM" <?php echo set_select('BlockedCountries', 'JM', in_array('JM', $BlockedCountries)); ?>>Jamaica</option>
					<option value="JP" <?php echo set_select('BlockedCountries', 'JP', in_array('JP', $BlockedCountries)); ?>>Japan</option>
					<option value="JE" <?php echo set_select('BlockedCountries', 'JE', in_array('JE', $BlockedCountries)); ?>>Jersey</option>
					<option value="JO" <?php echo set_select('BlockedCountries', 'JO', in_array('JO', $BlockedCountries)); ?>>Jordan</option>
					<option value="KZ" <?php echo set_select('BlockedCountries', 'KZ', in_array('KZ', $BlockedCountries)); ?>>Kazakhstan</option>
					<option value="KE" <?php echo set_select('BlockedCountries', 'KE', in_array('KE', $BlockedCountries)); ?>>Kenya</option>
					<option value="KI" <?php echo set_select('BlockedCountries', 'KI', in_array('KI', $BlockedCountries)); ?>>Kiribati</option>
					<option value="KP" <?php echo set_select('BlockedCountries', 'KP', in_array('KP', $BlockedCountries)); ?>>Korea, Democratic People's Republic of</option>
					<option value="KR" <?php echo set_select('BlockedCountries', 'KR', in_array('KR', $BlockedCountries)); ?>>Korea, Republic of</option>
					<option value="KW" <?php echo set_select('BlockedCountries', 'KW', in_array('KW', $BlockedCountries)); ?>>Kuwait</option>
					<option value="KG" <?php echo set_select('BlockedCountries', 'KG', in_array('KG', $BlockedCountries)); ?>>Kyrgyzstan</option>
					<option value="LA" <?php echo set_select('BlockedCountries', 'LA', in_array('LA', $BlockedCountries)); ?>>Lao People's Democratic Republic</option>
					<option value="LV" <?php echo set_select('BlockedCountries', 'LV', in_array('LV', $BlockedCountries)); ?>>Latvia</option>
					<option value="LB" <?php echo set_select('BlockedCountries', 'LB', in_array('LB', $BlockedCountries)); ?>>Lebanon</option>
					<option value="LS" <?php echo set_select('BlockedCountries', 'LS', in_array('LS', $BlockedCountries)); ?>>Lesotho</option>
					<option value="LR" <?php echo set_select('BlockedCountries', 'LR', in_array('LR', $BlockedCountries)); ?>>Liberia</option>
					<option value="LY" <?php echo set_select('BlockedCountries', 'LY', in_array('LY', $BlockedCountries)); ?>>Libya</option>
					<option value="LI" <?php echo set_select('BlockedCountries', 'LI', in_array('LI', $BlockedCountries)); ?>>Liechtenstein</option>
					<option value="LT" <?php echo set_select('BlockedCountries', 'LT', in_array('LT', $BlockedCountries)); ?>>Lithuania</option>
					<option value="LU" <?php echo set_select('BlockedCountries', 'LU', in_array('LU', $BlockedCountries)); ?>>Luxembourg</option>
					<option value="MO" <?php echo set_select('BlockedCountries', 'MO', in_array('MO', $BlockedCountries)); ?>>Macao</option>
					<option value="MK" <?php echo set_select('BlockedCountries', 'MK', in_array('MK', $BlockedCountries)); ?>>Macedonia, the former Yugoslav Republic of</option>
					<option value="MG" <?php echo set_select('BlockedCountries', 'MG', in_array('MG', $BlockedCountries)); ?>>Madagascar</option>
					<option value="MW" <?php echo set_select('BlockedCountries', 'MW', in_array('MW', $BlockedCountries)); ?>>Malawi</option>
					<option value="MY" <?php echo set_select('BlockedCountries', 'MY', in_array('MY', $BlockedCountries)); ?>>Malaysia</option>
					<option value="MV" <?php echo set_select('BlockedCountries', 'MV', in_array('MV', $BlockedCountries)); ?>>Maldives</option>
					<option value="ML" <?php echo set_select('BlockedCountries', 'ML', in_array('ML', $BlockedCountries)); ?>>Mali</option>
					<option value="MT" <?php echo set_select('BlockedCountries', 'MT', in_array('MT', $BlockedCountries)); ?>>Malta</option>
					<option value="MH" <?php echo set_select('BlockedCountries', 'MH', in_array('MH', $BlockedCountries)); ?>>Marshall Islands</option>
					<option value="MQ" <?php echo set_select('BlockedCountries', 'MQ', in_array('MQ', $BlockedCountries)); ?>>Martinique</option>
					<option value="MR" <?php echo set_select('BlockedCountries', 'MR', in_array('MR', $BlockedCountries)); ?>>Mauritania</option>
					<option value="MU" <?php echo set_select('BlockedCountries', 'MU', in_array('MU', $BlockedCountries)); ?>>Mauritius</option>
					<option value="YT" <?php echo set_select('BlockedCountries', 'YT', in_array('YT', $BlockedCountries)); ?>>Mayotte</option>
					<option value="MX" <?php echo set_select('BlockedCountries', 'MX', in_array('MX', $BlockedCountries)); ?>>Mexico</option>
					<option value="FM" <?php echo set_select('BlockedCountries', 'FM', in_array('FM', $BlockedCountries)); ?>>Micronesia, Federated States of</option>
					<option value="MD" <?php echo set_select('BlockedCountries', 'MD', in_array('MD', $BlockedCountries)); ?>>Moldova, Republic of</option>
					<option value="MC" <?php echo set_select('BlockedCountries', 'MC', in_array('MC', $BlockedCountries)); ?>>Monaco</option>
					<option value="MN" <?php echo set_select('BlockedCountries', 'MN', in_array('MN', $BlockedCountries)); ?>>Mongolia</option>
					<option value="ME" <?php echo set_select('BlockedCountries', 'ME', in_array('ME', $BlockedCountries)); ?>>Montenegro</option>
					<option value="MS" <?php echo set_select('BlockedCountries', 'MS', in_array('MS', $BlockedCountries)); ?>>Montserrat</option>
					<option value="MA" <?php echo set_select('BlockedCountries', 'MA', in_array('MA', $BlockedCountries)); ?>>Morocco</option>
					<option value="MZ" <?php echo set_select('BlockedCountries', 'MZ', in_array('MZ', $BlockedCountries)); ?>>Mozambique</option>
					<option value="MM" <?php echo set_select('BlockedCountries', 'MM', in_array('MM', $BlockedCountries)); ?>>Myanmar</option>
					<option value="NA" <?php echo set_select('BlockedCountries', 'NA', in_array('NA', $BlockedCountries)); ?>>Namibia</option>
					<option value="NR" <?php echo set_select('BlockedCountries', 'NR', in_array('NR', $BlockedCountries)); ?>>Nauru</option>
					<option value="NP" <?php echo set_select('BlockedCountries', 'NP', in_array('NP', $BlockedCountries)); ?>>Nepal</option>
					<option value="NL" <?php echo set_select('BlockedCountries', 'NL', in_array('NL', $BlockedCountries)); ?>>Netherlands</option>
					<option value="NC" <?php echo set_select('BlockedCountries', 'NC', in_array('NC', $BlockedCountries)); ?>>New Caledonia</option>
					<option value="NZ" <?php echo set_select('BlockedCountries', 'NZ', in_array('NZ', $BlockedCountries)); ?>>New Zealand</option>
					<option value="NI" <?php echo set_select('BlockedCountries', 'NI', in_array('NI', $BlockedCountries)); ?>>Nicaragua</option>
					<option value="NE" <?php echo set_select('BlockedCountries', 'NE', in_array('NE', $BlockedCountries)); ?>>Niger</option>
					<option value="NG" <?php echo set_select('BlockedCountries', 'NG', in_array('NG', $BlockedCountries)); ?>>Nigeria</option>
					<option value="NU" <?php echo set_select('BlockedCountries', 'NU', in_array('NU', $BlockedCountries)); ?>>Niue</option>
					<option value="NF" <?php echo set_select('BlockedCountries', 'NF', in_array('NF', $BlockedCountries)); ?>>Norfolk Island</option>
					<option value="MP" <?php echo set_select('BlockedCountries', 'MP', in_array('MP', $BlockedCountries)); ?>>Northern Mariana Islands</option>
					<option value="NO" <?php echo set_select('BlockedCountries', 'NO', in_array('NO', $BlockedCountries)); ?>>Norway</option>
					<option value="OM" <?php echo set_select('BlockedCountries', 'OM', in_array('OM', $BlockedCountries)); ?>>Oman</option>
					<option value="PK" <?php echo set_select('BlockedCountries', 'PK', in_array('PK', $BlockedCountries)); ?>>Pakistan</option>
					<option value="PW" <?php echo set_select('BlockedCountries', 'PW', in_array('PW', $BlockedCountries)); ?>>Palau</option>
					<option value="PS" <?php echo set_select('BlockedCountries', 'PS', in_array('PS', $BlockedCountries)); ?>>Palestinian Territory, Occupied</option>
					<option value="PA" <?php echo set_select('BlockedCountries', 'PA', in_array('PA', $BlockedCountries)); ?>>Panama</option>
					<option value="PG" <?php echo set_select('BlockedCountries', 'PG', in_array('PG', $BlockedCountries)); ?>>Papua New Guinea</option>
					<option value="PY" <?php echo set_select('BlockedCountries', 'PY', in_array('PY', $BlockedCountries)); ?>>Paraguay</option>
					<option value="PE" <?php echo set_select('BlockedCountries', 'PE', in_array('PE', $BlockedCountries)); ?>>Peru</option>
					<option value="PH" <?php echo set_select('BlockedCountries', 'PH', in_array('PH', $BlockedCountries)); ?>>Philippines</option>
					<option value="PN" <?php echo set_select('BlockedCountries', 'PN', in_array('PN', $BlockedCountries)); ?>>Pitcairn</option>
					<option value="PL" <?php echo set_select('BlockedCountries', 'PL', in_array('PL', $BlockedCountries)); ?>>Poland</option>
					<option value="PT" <?php echo set_select('BlockedCountries', 'PT', in_array('PT', $BlockedCountries)); ?>>Portugal</option>
					<option value="PR" <?php echo set_select('BlockedCountries', 'PR', in_array('PR', $BlockedCountries)); ?>>Puerto Rico</option>
					<option value="QA" <?php echo set_select('BlockedCountries', 'QA', in_array('QA', $BlockedCountries)); ?>>Qatar</option>
					<option value="RE" <?php echo set_select('BlockedCountries', 'RE', in_array('RE', $BlockedCountries)); ?>>Réunion</option>
					<option value="RO" <?php echo set_select('BlockedCountries', 'RO', in_array('RO', $BlockedCountries)); ?>>Romania</option>
					<option value="RU" <?php echo set_select('BlockedCountries', 'RU', in_array('RU', $BlockedCountries)); ?>>Russian Federation</option>
					<option value="RW" <?php echo set_select('BlockedCountries', 'RW', in_array('RW', $BlockedCountries)); ?>>Rwanda</option>
					<option value="BL" <?php echo set_select('BlockedCountries', 'BL', in_array('BL', $BlockedCountries)); ?>>Saint Barthélemy</option>
					<option value="SH" <?php echo set_select('BlockedCountries', 'SH', in_array('SH', $BlockedCountries)); ?>>Saint Helena, Ascension and Tristan da Cunha</option>
					<option value="KN" <?php echo set_select('BlockedCountries', 'KN', in_array('KN', $BlockedCountries)); ?>>Saint Kitts and Nevis</option>
					<option value="LC" <?php echo set_select('BlockedCountries', 'LC', in_array('LC', $BlockedCountries)); ?>>Saint Lucia</option>
					<option value="MF" <?php echo set_select('BlockedCountries', 'MF', in_array('MF', $BlockedCountries)); ?>>Saint Martin (French part)</option>
					<option value="PM" <?php echo set_select('BlockedCountries', 'PM', in_array('PM', $BlockedCountries)); ?>>Saint Pierre and Miquelon</option>
					<option value="VC" <?php echo set_select('BlockedCountries', 'VC', in_array('VC', $BlockedCountries)); ?>>Saint Vincent and the Grenadines</option>
					<option value="WS" <?php echo set_select('BlockedCountries', 'WS', in_array('WS', $BlockedCountries)); ?>>Samoa</option>
					<option value="SM" <?php echo set_select('BlockedCountries', 'SM', in_array('SM', $BlockedCountries)); ?>>San Marino</option>
					<option value="ST" <?php echo set_select('BlockedCountries', 'ST', in_array('ST', $BlockedCountries)); ?>>Sao Tome and Principe</option>
					<option value="SA" <?php echo set_select('BlockedCountries', 'SA', in_array('SA', $BlockedCountries)); ?>>Saudi Arabia</option>
					<option value="SN" <?php echo set_select('BlockedCountries', 'SN', in_array('SN', $BlockedCountries)); ?>>Senegal</option>
					<option value="RS" <?php echo set_select('BlockedCountries', 'RS', in_array('RS', $BlockedCountries)); ?>>Serbia</option>
					<option value="SC" <?php echo set_select('BlockedCountries', 'SC', in_array('SC', $BlockedCountries)); ?>>Seychelles</option>
					<option value="SL" <?php echo set_select('BlockedCountries', 'SL', in_array('SL', $BlockedCountries)); ?>>Sierra Leone</option>
					<option value="SG" <?php echo set_select('BlockedCountries', 'SG', in_array('SG', $BlockedCountries)); ?>>Singapore</option>
					<option value="SX" <?php echo set_select('BlockedCountries', 'SX', in_array('SX', $BlockedCountries)); ?>>Sint Maarten (Dutch part)</option>
					<option value="SK" <?php echo set_select('BlockedCountries', 'SK', in_array('SK', $BlockedCountries)); ?>>Slovakia</option>
					<option value="SI" <?php echo set_select('BlockedCountries', 'SI', in_array('SI', $BlockedCountries)); ?>>Slovenia</option>
					<option value="SB" <?php echo set_select('BlockedCountries', 'SB', in_array('SB', $BlockedCountries)); ?>>Solomon Islands</option>
					<option value="SO" <?php echo set_select('BlockedCountries', 'SO', in_array('SO', $BlockedCountries)); ?>>Somalia</option>
					<option value="ZA" <?php echo set_select('BlockedCountries', 'ZA', in_array('ZA', $BlockedCountries)); ?>>South Africa</option>
					<option value="GS" <?php echo set_select('BlockedCountries', 'GS', in_array('GS', $BlockedCountries)); ?>>South Georgia and the South Sandwich Islands</option>
					<option value="SS" <?php echo set_select('BlockedCountries', 'SS', in_array('SS', $BlockedCountries)); ?>>South Sudan</option>
					<option value="ES" <?php echo set_select('BlockedCountries', 'ES', in_array('ES', $BlockedCountries)); ?>>Spain</option>
					<option value="LK" <?php echo set_select('BlockedCountries', 'LK', in_array('LK', $BlockedCountries)); ?>>Sri Lanka</option>
					<option value="SD" <?php echo set_select('BlockedCountries', 'SD', in_array('SD', $BlockedCountries)); ?>>Sudan</option>
					<option value="SR" <?php echo set_select('BlockedCountries', 'SR', in_array('SR', $BlockedCountries)); ?>>Suriname</option>
					<option value="SJ" <?php echo set_select('BlockedCountries', 'SJ', in_array('SJ', $BlockedCountries)); ?>>Svalbard and Jan Mayen</option>
					<option value="SZ" <?php echo set_select('BlockedCountries', 'SZ', in_array('SZ', $BlockedCountries)); ?>>Swaziland</option>
					<option value="SE" <?php echo set_select('BlockedCountries', 'SE', in_array('SE', $BlockedCountries)); ?>>Sweden</option>
					<option value="CH" <?php echo set_select('BlockedCountries', 'CH', in_array('CH', $BlockedCountries)); ?>>Switzerland</option>
					<option value="SY" <?php echo set_select('BlockedCountries', 'SY', in_array('SY', $BlockedCountries)); ?>>Syrian Arab Republic</option>
					<option value="TW" <?php echo set_select('BlockedCountries', 'TW', in_array('TW', $BlockedCountries)); ?>>Taiwan, Province of China</option>
					<option value="TJ" <?php echo set_select('BlockedCountries', 'TJ', in_array('TJ', $BlockedCountries)); ?>>Tajikistan</option>
					<option value="TZ" <?php echo set_select('BlockedCountries', 'TZ', in_array('TZ', $BlockedCountries)); ?>>Tanzania, United Republic of</option>
					<option value="TH" <?php echo set_select('BlockedCountries', 'TH', in_array('TH', $BlockedCountries)); ?>>Thailand</option>
					<option value="TL" <?php echo set_select('BlockedCountries', 'TL', in_array('TL', $BlockedCountries)); ?>>Timor-Leste</option>
					<option value="TG" <?php echo set_select('BlockedCountries', 'TG', in_array('TG', $BlockedCountries)); ?>>Togo</option>
					<option value="TK" <?php echo set_select('BlockedCountries', 'TK', in_array('TK', $BlockedCountries)); ?>>Tokelau</option>
					<option value="TO" <?php echo set_select('BlockedCountries', 'TO', in_array('TO', $BlockedCountries)); ?>>Tonga</option>
					<option value="TT" <?php echo set_select('BlockedCountries', 'TT', in_array('TT', $BlockedCountries)); ?>>Trinidad and Tobago</option>
					<option value="TN" <?php echo set_select('BlockedCountries', 'TN', in_array('TN', $BlockedCountries)); ?>>Tunisia</option>
					<option value="TR" <?php echo set_select('BlockedCountries', 'TR', in_array('TR', $BlockedCountries)); ?>>Turkey</option>
					<option value="TM" <?php echo set_select('BlockedCountries', 'TM', in_array('TM', $BlockedCountries)); ?>>Turkmenistan</option>
					<option value="TC" <?php echo set_select('BlockedCountries', 'TC', in_array('TC', $BlockedCountries)); ?>>Turks and Caicos Islands</option>
					<option value="TV" <?php echo set_select('BlockedCountries', 'TV', in_array('TV', $BlockedCountries)); ?>>Tuvalu</option>
					<option value="UG" <?php echo set_select('BlockedCountries', 'UG', in_array('UG', $BlockedCountries)); ?>>Uganda</option>
					<option value="UA" <?php echo set_select('BlockedCountries', 'UA', in_array('UA', $BlockedCountries)); ?>>Ukraine</option>
					<option value="AE" <?php echo set_select('BlockedCountries', 'AE', in_array('AE', $BlockedCountries)); ?>>United Arab Emirates</option>
					<option value="GB" <?php echo set_select('BlockedCountries', 'GB', in_array('GB', $BlockedCountries)); ?>>United Kingdom</option>
					<option value="US" <?php echo set_select('BlockedCountries', 'US', in_array('US', $BlockedCountries)); ?>>United States</option>
					<option value="UM" <?php echo set_select('BlockedCountries', 'UM', in_array('UM', $BlockedCountries)); ?>>United States Minor Outlying Islands</option>
					<option value="UY" <?php echo set_select('BlockedCountries', 'UY', in_array('UY', $BlockedCountries)); ?>>Uruguay</option>
					<option value="UZ" <?php echo set_select('BlockedCountries', 'UZ', in_array('UZ', $BlockedCountries)); ?>>Uzbekistan</option>
					<option value="VU" <?php echo set_select('BlockedCountries', 'VU', in_array('VU', $BlockedCountries)); ?>>Vanuatu</option>
					<option value="VE" <?php echo set_select('BlockedCountries', 'VE', in_array('VE', $BlockedCountries)); ?>>Venezuela, Bolivarian Republic of</option>
					<option value="VN" <?php echo set_select('BlockedCountries', 'VN', in_array('VN', $BlockedCountries)); ?>>Viet Nam</option>
					<option value="VG" <?php echo set_select('BlockedCountries', 'VG', in_array('VG', $BlockedCountries)); ?>>Virgin Islands, British</option>
					<option value="VI" <?php echo set_select('BlockedCountries', 'VI', in_array('VI', $BlockedCountries)); ?>>Virgin Islands, U.S.</option>
					<option value="WF" <?php echo set_select('BlockedCountries', 'WF', in_array('WF', $BlockedCountries)); ?>>Wallis and Futuna</option>
					<option value="EH" <?php echo set_select('BlockedCountries', 'EH', in_array('EH', $BlockedCountries)); ?>>Western Sahara</option>
					<option value="YE" <?php echo set_select('BlockedCountries', 'YE', in_array('YE', $BlockedCountries)); ?>>Yemen</option>
					<option value="ZM" <?php echo set_select('BlockedCountries', 'ZM', in_array('ZM', $BlockedCountries)); ?>>Zambia</option>
					<option value="ZW" <?php echo set_select('BlockedCountries', 'ZW', in_array('ZW', $BlockedCountries)); ?>>Zimbabwe</option>
				</select>
				<div class="form-row-note" id="CountriesSelected">asfd</div>
			</div>
		</div>
	</div>
</div>

<div class="span-18 last">
	<div class="form-action-container">
		<a class="button" targetform="LoginBlockerOptions" id="ButtonPreferences"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo $PluginLanguage['Screen']['0015']; ?></strong></a>
		<input type="hidden" name="Command" value="UpdateOptions" id="Command" />
	</div>
</div>

</form>

<script>
	var Language = {
		'l0025': "<?php echo $PluginLanguage['Screen']['0025']; ?>"
	};
	$(document).ready(function() {
		function enableDisable() {
			var $container = $(this).parent().next();
			if ($(this).val() == 'disabled') {
				$container.hide();
			} else {
				$container.show();
			}
		}
		$('.area-blocking-enabler').change(enableDisable).each(function() {
			enableDisable.call($(this));
		});

		function updateCount() {
			var text = Language['l0025'].replace('%s', $('#BlockedCountries option:selected').length);
			$('#CountriesSelected').text(text);
		}
		$('#BlockedCountries').change(updateCount);
		updateCount();
	});
</script>