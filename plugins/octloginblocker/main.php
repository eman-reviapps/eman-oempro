<?php
class octloginblocker extends Plugins
{

	private static $PluginCode = 'octloginblocker';
	private static $ArrayLanguage = array();
	public static $ObjectCI = null;
	private static $AdminInformation = array();
	
	private static $LanguageCode = '';
	
	private static $PluginVersion = "1.0.0";

	public function enable_octloginblocker()
	{
		self::_LoadConfig();

		$DefaultLoginBlockerOptions = array(
			'is_enabled' => false,
			'number_of_tries' => 3,
			'block_duration' => 300 // 5 minutes
		);
		$DefaultLoginBlockerOptions = json_encode($DefaultLoginBlockerOptions, JSON_FORCE_OBJECT);
		$Areas = array('_userarea_options', '_adminarea_options', '_clientarea_options');
		foreach ($Areas as $EachArea) {
			Database::$Interface->SaveOption(self::$PluginCode . $EachArea, $DefaultLoginBlockerOptions);
		}

		Database::$Interface->SaveOption(self::$PluginCode . '_blocked_countries', json_encode(array('countries' => array())));
	}

	public function disable_octloginblocker()
	{
		Database::$Interface->DeleteRows_Enhanced(array(
			'Table' => MYSQL_TABLE_PREFIX.'options',
			'Criteria' => array(
				array('Column' => 'OptionName', 'Operator' => 'LIKE', 'Value' => self::$PluginCode . '_%')
			)
		));

		Database::$Interface->DeleteRows_Enhanced(array(
			'Table' => MYSQL_TABLE_PREFIX.'log_flood_detection',
			'Criteria' => array(
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'user_failed_login'),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'admin_failed_login', 'Link' => 'OR'),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'client_failed_login', 'Link' => 'OR')
			)
		));
	}

	public function load_octloginblocker()
	{
		// Load language - Start
		$Language = Database::$Interface->GetOption(self::$PluginCode . '_Language');
		if (count($Language) == 0) {
			Database::$Interface->SaveOption(self::$PluginCode . '_Language', 'en');
			$Language = 'en';
		} else {
			$Language = $Language[0]['OptionValue'];
		}
		self::$LanguageCode = $Language;

		$ArrayPlugInLanguageStrings = array();
		if (file_exists(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php') == true) {
			include_once(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php');
		} else {
			include_once(PLUGIN_PATH . self::$PluginCode . '/languages/en/en.inc.php');
		}
		self::$ArrayLanguage = $ArrayPlugInLanguageStrings;
		unset($ArrayPlugInLanguageStrings);
		// Load language - End

		parent::RegisterEnableHook(self::$PluginCode);
		parent::RegisterDisableHook(self::$PluginCode);

		parent::RegisterMenuHook(self::$PluginCode, 'set_menu_items');

		parent::RegisterHook('Action', 'User.Login.Before', self::$PluginCode, 'user_login_before', 10, 1);
		parent::RegisterHook('Action', 'User.Login.InvalidUser', self::$PluginCode, 'user_login_invalid', 10, 1);
		parent::RegisterHook('Action', 'Admin.Login.Before', self::$PluginCode, 'admin_login_before', 10, 1);
		parent::RegisterHook('Action', 'Admin.Login.InvalidUser', self::$PluginCode, 'admin_login_invalid', 10, 1);
		parent::RegisterHook('Action', 'Client.Login.Before', self::$PluginCode, 'client_login_before', 10, 1);
		parent::RegisterHook('Action', 'Client.Login.InvalidUser', self::$PluginCode, 'client_login_invalid', 10, 1);

		self::_LoadConfig();
	}

	public function user_login_invalid()
	{
		$Detector = self::_GetDetector('user');
		if (is_bool($Detector) && $Detector === false) return;
		$Detector->isFloodedByIP(self::_GetIP());
	}

	public function admin_login_invalid()
	{
		$Detector = self::_GetDetector('admin');
		if (is_bool($Detector) && $Detector === false) return;
		$Detector->isFloodedByIP(self::_GetIP());
	}

	public function client_login_invalid()
	{
		$Detector = self::_GetDetector('client');
		if (is_bool($Detector) && $Detector === false) return;
		$Detector->isFloodedByIP(self::_GetIP());
	}

	public function user_login_before()
	{
		self::_LoginBefore('user');
	}

	public function admin_login_before()
	{
		self::_LoginBefore('admin');
	}

	public function client_login_before()
	{
		self::_LoginBefore('client');
	}

	public function set_menu_items()
	{
		$ArrayMenuItems = array();
		
		$ArrayMenuItems[] = array(
			'MenuLocation' => 'Admin.Settings',
			'MenuID' => 'Login Blocker',
			'MenuLink' => Core::InterfaceAppURL() . '/octloginblocker/admin_settings/',
			'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
		);

		return $ArrayMenuItems;
	}

	public function ui_admin_settings()
	{
		self::_PluginAppHeader();

		if (self::$ObjectCI->input->post('Command') == 'UpdateOptions') {
			$FormSuccess = self::_EventSaveOptions();
		}

		$PluginOptions = array();

		$Options = self::_GetOptions('admin');
		$PluginOptions['AdminAreaBlocking'] = $Options->is_enabled == true ? 'enabled' : 'disabled';
		$PluginOptions['AdminAreaAllowedMaxTries'] = $Options->number_of_tries;
		$PluginOptions['AdminAreaBlockDuration'] = $Options->block_duration;

		$Options = self::_GetOptions('user');
		$PluginOptions['UserAreaBlocking'] = $Options->is_enabled == true ? 'enabled' : 'disabled';
		$PluginOptions['UserAreaAllowedMaxTries'] = $Options->number_of_tries;
		$PluginOptions['UserAreaBlockDuration'] = $Options->block_duration;

		$Options = self::_GetOptions('client');
		$PluginOptions['ClientAreaBlocking'] = $Options->is_enabled == true ? 'enabled' : 'disabled';
		$PluginOptions['ClientAreaAllowedMaxTries'] = $Options->number_of_tries;
		$PluginOptions['ClientAreaBlockDuration'] = $Options->block_duration;

		$BlockStatistics = self::_GetBlockStatistics();

		$BlockedCountries = self::_GetBlockedCountries();

		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0002'],
			'CurrentMenuItem' => 'Settings',
			'PluginView' => '../plugins/octloginblocker/templates/admin_settings.php',
			'SubSection' => 'Login Blocker',
			'PluginLanguage' => self::$ArrayLanguage,
			'SelectedTab' => $SelectedTab,
			'AdminInformation' => self::$AdminInformation,
			'PluginVersion' => self::$PluginVersion,
			'PluginOptions' => $PluginOptions,
			'BlockStatistics' => $BlockStatistics,
			'FormSuccess' => $FormSuccess,
			'BlocksRemoved' => isset($_REQUEST['m']) && $_REQUEST['m'] == 1,
			'BlockedCountries' => $BlockedCountries
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		self::$ObjectCI->render('admin/settings', $ArrayViewData);
	}

	public function ui_admin_remove_blocks()
	{
		self::_PluginAppHeader();

		Database::$Interface->DeleteRows_Enhanced(array(
			'Table' => 'oempro_log_flood_detection',
			'Criteria' => array(
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'admin_failed_login'),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'user_failed_login', 'Link' => 'OR'),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'client_failed_login', 'Link' => 'OR')
			)
		));

		self::$ObjectCI->load->helper('url');
		redirect(Core::InterfaceAppURL() . '/octloginblocker/admin_settings/?m=1');
	}

	private function _EventSaveOptions()
	{
		self::$ObjectCI->form_validation->set_rules("AdminAreaBlocking");
		self::$ObjectCI->form_validation->set_rules("AdminAreaAllowedMaxTries");
		self::$ObjectCI->form_validation->set_rules("AdminAreaBlockDuration");
		self::$ObjectCI->form_validation->set_rules("UserAreaBlocking");
		self::$ObjectCI->form_validation->set_rules("UserAreaAllowedMaxTries");
		self::$ObjectCI->form_validation->set_rules("UserAreaBlockDuration");
		self::$ObjectCI->form_validation->set_rules("ClientAreaBlocking");
		self::$ObjectCI->form_validation->set_rules("ClientAreaAllowedMaxTries");
		self::$ObjectCI->form_validation->set_rules("ClientAreaBlockDuration");
		self::$ObjectCI->form_validation->set_rules("BlockedCountries");

		if (self::$ObjectCI->form_validation->run() == false) return false;

		$Areas = array('_userarea_options' => 'UserArea', '_adminarea_options' => 'AdminArea', '_clientarea_options' => 'ClientArea');
		foreach (array_keys($Areas) as $EachArea) {
			$Options = array(
				'is_enabled' => self::$ObjectCI->input->post($Areas[$EachArea] . 'Blocking') == 'enabled' ? true : false,
				'number_of_tries' => self::$ObjectCI->input->post($Areas[$EachArea] . 'AllowedMaxTries') == '' ? 3 : self::$ObjectCI->input->post($Areas[$EachArea] . 'AllowedMaxTries'),
				'block_duration' => self::$ObjectCI->input->post($Areas[$EachArea] . 'BlockDuration') == '' ? 300 : self::$ObjectCI->input->post($Areas[$EachArea] . 'BlockDuration')
			);
			$Options = json_encode($Options, JSON_FORCE_OBJECT);
			Database::$Interface->SaveOption(self::$PluginCode . $EachArea, $Options);
		}

		$SelectedCountries = self::$ObjectCI->input->post('BlockedCountries');
		if (! is_array($SelectedCountries)) $SelectedCountries = array();
		Database::$Interface->SaveOption(self::$PluginCode . '_blocked_countries', json_encode(array('countries' => $SelectedCountries)));

		return true;
	}

	public function ui_blocked($For = 'u')
	{
		if (self::_PluginAppHeader('') == false) return;

		if ($For == 'g') {
			self::$ObjectCI->display_public_message(self::$ArrayLanguage['Screen']['0004'], self::$ArrayLanguage['Screen']['0026']);
		} else {
			$Areas = array(
				'u' => '_userarea_options',
				'a' => '_adminarea_options',
				'c' => '_clientarea_options',
			);
			$For = ! in_array($for, array_keys($Areas)) ? 'u' : $For;

			$Option = Database::$Interface->GetOption(self::$PluginCode . $Areas[$For]);
			$OptionValue = $Option[0]['OptionValue'];
			$UserAreaLoginBlockerOptions = json_decode($OptionValue);

			self::$ObjectCI->display_public_message(self::$ArrayLanguage['Screen']['0004'], sprintf(self::$ArrayLanguage['Screen']['0003'], $UserAreaLoginBlockerOptions->block_duration));
		}
	}

	private function _PluginAppHeader($Auth = 'Admin')
	{
		self::$ObjectCI =& get_instance();

		if (Plugins::IsPlugInEnabled(self::$PluginCode) == false)
		{
			$Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
			self::$ObjectCI->display_public_message('', $Message);
			return false;
		}

		if ($Auth == 'Admin') self::_CheckAdminAuth();

		return true;
	}

	private function _CheckAdminAuth()
	{
		Core::LoadObject('admin_auth');
		Core::LoadObject('admins');

		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
		self::$AdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));
	}

	private function _GetOptions($For = 'user')
	{
		$Keys = array(
			'user' => '_userarea_options',
			'admin' => '_adminarea_options',
			'client' => '_clientarea_options'
		);

		$Option = Database::$Interface->GetOption(self::$PluginCode . $Keys[$For]);
		$OptionValue = $Option[0]['OptionValue'];
		$LoginBlockerOptions = json_decode($OptionValue);

		return $LoginBlockerOptions;
	}

	private function _GetDetector($For = 'user')
	{
		$LoginBlockerOptions = self::_GetOptions($For);
		if ($LoginBlockerOptions->is_enabled === false) return false;

		$Zones = array(
			'user' => 'user_failed_login',
			'admin' => 'admin_failed_login',
			'client' => 'client_failed_login'
		);
		$Detector = new O_Security_Flood_Detector($Zones[$For]);
		$Detector->setMaxAccessForTimePeriod($LoginBlockerOptions->number_of_tries, OCTLOGINBLOCKER_MAX_ATTEMPTS_IN_SECONDS, $LoginBlockerOptions->block_duration);
		
		return $Detector;	
	}

	private function _GetIP()
	{
		return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}

	private function _GetBlockStatistics()
	{
		$AdminStatistics = Database::$Interface->GetRows_Enhanced(array(
			'Tables' => array('oempro_log_flood_detection'),
			'Fields' => array('COUNT(*) AS Total'),
			'Criteria' => array(
				array('Column' => 'IsBlocked', 'Operator' => '=', 'ValueWOQuote' => 1),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'admin_failed_login', 'Link' => 'AND')
			)
		));
		$AdminStatistics = $AdminStatistics[0]['Total'];

		$UserStatistics = Database::$Interface->GetRows_Enhanced(array(
			'Tables' => array('oempro_log_flood_detection'),
			'Fields' => array('COUNT(*) AS Total'),
			'Criteria' => array(
				array('Column' => 'IsBlocked', 'Operator' => '=', 'ValueWOQuote' => 1),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'user_failed_login', 'Link' => 'AND')
			)
		));
		$UserStatistics = $UserStatistics[0]['Total'];

		$ClientStatistics = Database::$Interface->GetRows_Enhanced(array(
			'Tables' => array('oempro_log_flood_detection'),
			'Fields' => array('COUNT(*) AS Total'),
			'Criteria' => array(
				array('Column' => 'IsBlocked', 'Operator' => '=', 'ValueWOQuote' => 1),
				array('Column' => 'Zone', 'Operator' => '=', 'Value' => 'client_failed_login', 'Link' => 'AND')
			)
		));
		$ClientStatistics = $ClientStatistics[0]['Total'];

		return array(
			'AdminArea' => $AdminStatistics,
			'UserArea' => $UserStatistics,
			'ClientArea' => $ClientStatistics
		);
	}

	private function _GetBlockedCountries()
	{
		$BlockedCountries = Database::$Interface->GetOption(self::$PluginCode . '_blocked_countries');
		$BlockedCountries = json_decode($BlockedCountries[0]['OptionValue']);
		$BlockedCountries = $BlockedCountries->countries;

		return $BlockedCountries;
	}

	private function _RedirectToBlocked($For = 'u')
	{
		self::$ObjectCI =& get_instance();
		self::$ObjectCI->load->helper('url');
		redirect(Core::InterfaceAppURL() . '/octloginblocker/blocked/' . $For);
	}

	private function _LoadConfig()
	{
		$ConfigFile = PLUGIN_PATH . self::$PluginCode . '/data/config.inc.php';
		if (file_exists($ConfigFile) == true) {
			include_once($ConfigFile);
		}
	}

	private function _LoginBefore($For = 'admin')
	{
		$IP = self::_GetIP();
		if (empty($IP)) return;

		$BlockedCountries = self::_GetBlockedCountries();
		if (count($BlockedCountries) > 0 && defined('GEO_LOCATION_DATA_PATH') && file_exists(GEO_LOCATION_DATA_PATH) == true) {
			$GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
			$CountryCode = geoip_country_code_by_addr($GeoTag, $IP);
			if (in_array($CountryCode, $BlockedCountries)) {
				self::_RedirectToBlocked('g');
			}
		}

		$Detector = self::_GetDetector($For);
		if (is_bool($Detector) && $Detector === false) return;
		if ($Detector->isFloodedByIP($IP, time(), false)) {
			self::_RedirectToBlocked($For[0]);
		}
	}
}