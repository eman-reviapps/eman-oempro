<?php
/**
 * Main language configuration file for the plug-in
 **/

// Important: Do not change this line! - Start
$ArrayPlugInLanguageStrings									= array();
// Important: Do not change this line! - End

$ArrayPlugInLanguageStrings['Screen']['0001']	= 'Login Blocker';
$ArrayPlugInLanguageStrings['Screen']['0002']	= 'Login Blocker Settings';
$ArrayPlugInLanguageStrings['Screen']['0003']	= 'You are blocked from logging in for %s seconds because you have exceeded maximum number of login attempts.';
$ArrayPlugInLanguageStrings['Screen']['0004']	= 'Login Blocked';
$ArrayPlugInLanguageStrings['Screen']['0005']	= 'Status';
$ArrayPlugInLanguageStrings['Screen']['0006']	= 'Admin Area Options';
$ArrayPlugInLanguageStrings['Screen']['0007']	= 'Enabled';
$ArrayPlugInLanguageStrings['Screen']['0008']	= 'Disabled';
$ArrayPlugInLanguageStrings['Screen']['0009']	= 'Invalid Logins';
$ArrayPlugInLanguageStrings['Screen']['0010']	= 'Number of maximum allowed successive invalid logins';
$ArrayPlugInLanguageStrings['Screen']['0011']	= 'Block Duration';
$ArrayPlugInLanguageStrings['Screen']['0012']	= 'Number of seconds to block any login attempts';
$ArrayPlugInLanguageStrings['Screen']['0013']	= 'User Area Options';
$ArrayPlugInLanguageStrings['Screen']['0014']	= 'Client Area Options';
$ArrayPlugInLanguageStrings['Screen']['0015']	= 'SAVE';
$ArrayPlugInLanguageStrings['Screen']['0016']	= 'Settings';
$ArrayPlugInLanguageStrings['Screen']['0017']	= 'Admin Area';
$ArrayPlugInLanguageStrings['Screen']['0018']	= 'User Area';
$ArrayPlugInLanguageStrings['Screen']['0019']	= 'Client Area';
$ArrayPlugInLanguageStrings['Screen']['0020']	= 'Blocked IPs';
$ArrayPlugInLanguageStrings['Screen']['0021']	= 'Settings are saved.';
$ArrayPlugInLanguageStrings['Screen']['0022']	= 'All blocks are removed.';
$ArrayPlugInLanguageStrings['Screen']['0023']	= 'Global Country Blocking';
$ArrayPlugInLanguageStrings['Screen']['0024']	= 'Blocked Countries';
$ArrayPlugInLanguageStrings['Screen']['0025']	= '%s countries selected';
$ArrayPlugInLanguageStrings['Screen']['0026']	= 'You are not allowed to access the system.';


?>