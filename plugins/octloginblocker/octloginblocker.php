<?php
/**
 * Login Blocker
 * Name: Login Blocker
 * Description: This plugin monitors admin, user and client area login attempts and logs failed logins. You can block certain IP addresses and/or countries.
 * Minimum Oempro Version: 4.7.0
 */

include_once(PLUGIN_PATH . 'octloginblocker/main.php');
