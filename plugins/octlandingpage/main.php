<?php

include_once PLUGIN_PATH . 'octlandingpage/functions.php';
include_once PLUGIN_PATH . 'octlandingpage/classes/lpb/autoloader.php';

class octlandingpage extends Plugins {

    private static $PluginCode = 'octlandingpage';
    private static $ArrayLanguage = array();
    public static $ObjectCI = null;
    private static $UserInformation = array();
    private static $AdminInformation = array();
    private static $PluginLicenseKey = null;
    private static $PluginLicenseStatus = true;
    private static $PluginLicenseInfo = null;
    private static $LanguageCode = '';
    private static $PluginVersion = "1.0.0";
    private static $MapperCache = array();

    public function getMapper($name) {
        $className = 'LPB_Mapper_' . $name;
        if (!isset($MapperCache[$className])) {
            $MapperCache[$className] = new $className(O_Registry::instance()->getDatabaseConnection());
        }
        return $MapperCache[$className];
    }

    public function enable_octlandingpage() {
        // License check
        self::_CheckLicenseStatus(true);
        if (self::$PluginLicenseStatus == false) {
            if (defined('octlandingpage_LicenseStatusMessage') == true && octlandingpage_LicenseStatusMessage == 'not_bound_to_this_oempro') {
                $_SESSION['PluginMessage'] = array(false, 'The plugin license does not belong to this Oempro license.<br>Please install the correct landing page builder plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="' . InterfaceAppUrl(true) . '/octlandingpage/trial_request">request a trial license</a>.');
                header("location: " . InterfaceAppURL(true) . '/admin/plugins/disable/octlandingpage');
                exit;
            } else {
                $_SESSION['PluginMessage'] = array(false, 'Please install the landing page builder plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="' . InterfaceAppUrl(true) . '/octlandingpage/trial_request">request a trial license</a>.');
                header("location: " . InterfaceAppURL(true) . '/admin/plugins/disable/octlandingpage');
                exit;
            }
        }

        self::_InstallDB();
    }

    public function disable_octlandingpage() {
        self::_UninstallDB();
        Database::$Interface->DeleteRows_Enhanced(array(
            'Table' => MYSQL_TABLE_PREFIX . 'options',
            'Criteria' => array(
                array('Column' => 'OptionName', 'Operator' => 'LIKE', 'Value' => 'octlandingpage_%')
            )
        ));
    }

    public function load_octlandingpage() {
        new O_AutoLoader(PLUGIN_PATH . 'octlandingpage/classes/');

        // Load language - Start
        $Language = Database::$Interface->GetOption(self::$PluginCode . '_Language');
        if (count($Language) == 0) {
            Database::$Interface->SaveOption(self::$PluginCode . '_Language', 'en');
            $Language = 'en';
        } else {
            $Language = $Language[0]['OptionValue'];
        }
        self::$LanguageCode = $Language;

        $ArrayPlugInLanguageStrings = array();
        if (file_exists(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php') == true) {
            include_once(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php');
        } else {
            include_once(PLUGIN_PATH . self::$PluginCode . '/languages/en/en.inc.php');
        }
        self::$ArrayLanguage = $ArrayPlugInLanguageStrings;
        unset($ArrayPlugInLanguageStrings);
        // Load language - End
        // License check
        self::_CheckLicenseStatus();
        if (self::$PluginLicenseStatus == false)
            return false;

        parent::RegisterEnableHook(self::$PluginCode);
        parent::RegisterDisableHook(self::$PluginCode);

        parent::RegisterHook('Action', 'User.Delete.Post', self::$PluginCode, 'user_delete', 10, 1000);

        parent::RegisterHook('Action', 'FormItem.AddTo.Admin.UserGroupLimitsForm', self::$PluginCode, 'set_user_group_form_items', 10, 1);
        parent::RegisterHook('Filter', 'UserGroup.Update.FieldValidator', self::$PluginCode, 'usergroup_update_fieldvalidator', 10, 1);
        parent::RegisterHook('Action', 'UserGroup.Update.Post', self::$PluginCode, 'usergroup_update', 10, 2);
        parent::RegisterHook('Action', 'UserGroup.Create.Post', self::$PluginCode, 'usergroup_update', 10, 2);
        parent::RegisterHook('Action', 'UserGroup.Delete.Post', self::$PluginCode, 'usergroup_delete', 10, 1);

        parent::RegisterHook('Filter', 'PersonalizationTags.Campaign.Content', self::$PluginCode, 'set_content_tags');
        parent::RegisterHook('Filter', 'Personalization.Content', self::$PluginCode, 'replace_page_links_in_campaign_content');

        parent::RegisterMenuHook(self::$PluginCode, 'set_menu_items');
    }

    public function set_user_group_form_items($UserGroup) {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        $UserGroupSettings = Database::$Interface->GetOption('octlandingpage_UserGroupSettings');
        $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

        $ArrayViewData = array(
            'PluginLanguage' => self::$ArrayLanguage,
            'UserGroup' => $UserGroup,
            'UserGroupSettings' => (isset($UserGroupSettings[$UserGroup['UserGroupID']]) == true ? $UserGroupSettings[$UserGroup['UserGroupID']] : false)
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $HTML = self::$ObjectCI->plugin_render(self::$PluginCode, 'inline_usergroup_formitems', $ArrayViewData, true, true);

        return array($HTML);
    }

    public function usergroup_update_fieldvalidator($ArrayFormRules) {
        $ArrayFormRules[] = array(
            'field' => 'OctLandingPage[PageLimit]',
            'label' => self::$ArrayLanguage['Screen']['0007'],
            'rules' => 'required|numeric',
        );
        return array($ArrayFormRules);
    }

    public function usergroup_update($UserGroup, $PostData) {
        if (isset($PostData['OctLandingPage']['PageLimit']) == true) {
            $UserGroupSettings = Database::$Interface->GetOption('octlandingpage_UserGroupSettings');
            $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

            $UserGroupSettings[$UserGroup['UserGroupID']]['PageLimit'] = $PostData['OctLandingPage']['PageLimit'];

            Database::$Interface->SaveOption('octlandingpage_UserGroupSettings', json_encode($UserGroupSettings));
        }
    }

    public function usergroup_delete($UserGroup) {
        $UserGroupSettings = Database::$Interface->GetOption('OctLandingPage_UserGroupSettings');
        $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

        if (isset($UserGroupSettings[$UserGroup['UserGroupID']]) == true) {
            unset($UserGroupSettings[$UserGroup['UserGroupID']]);
        }

        Database::$Interface->SaveOption('octlandingpage_UserGroupSettings', json_encode($UserGroupSettings));
    }

    public function replace_page_links_in_campaign_content($StringToPersonalize, $ArrayPersonalizationScope, $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, $ArrayAutoResponder, $IsPreview, $ArrayEmail, $DisablePersonalization) {
        $arrayPages = array();
        if (!function_exists('url_decode_landing_page_link')) {

            function url_decode_landing_page_link($matches) {
                $ids = Core::DecryptQueryStringAsArrayAdvanced($matches[1]);
                if (isset($arrayPages[$ids[1]])) {
                    $page = $arrayPages[$ids[1]];
                } else {
                    $page = octlandingpage::GetMapper('Page')->findByUserIdAndPageId($ids[0], $ids[1]);
                    if ($page === false)
                        return $matches[1];
                }
                return $page->getURL();
            }

        }
        $StringToPersonalize = preg_replace_callback("/%Link:Page:(.*)%/uiUm", url_decode_landing_page_link, $StringToPersonalize);

        return array($StringToPersonalize, $ArrayPersonalizationScope, $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, $ArrayAutoResponder, $IsPreview, $ArrayEmail, $DisablePersonalization);
    }

    public function set_menu_items() {
        $ArrayMenuItems = array();

        $ArrayMenuItems[] = array(
            'MenuLocation' => 'Admin.Settings',
            'MenuID' => 'Landing Page Builder',
            'MenuLink' => Core::InterfaceAppURL() . '/octlandingpage/admin_settings/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
        );
//TopDropMenu
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'User.TopMenu',
            'MenuID' => 'LandingPageBuilder',
            'MenuLink' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/pages',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0002'],
        );

        $ArrayMenuItems[] = array(
            'MenuLocation' => 'User.Overview.QuickAccess',
            'MenuID' => '',
            'MenuLink' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/templates',
            'MenuTitle' => strtoupper(self::$ArrayLanguage['Screen']['0072']),
        );

        return $ArrayMenuItems;
    }

    public function set_content_tags($Tags) {
        if (self::_PluginAppHeader('User') == false)
            return;

        $pages = self::getMapper('Page')->findAllByUserId(self::$UserInformation['UserID']);

        if (is_array($pages) && count($pages) > 0)
            $Tags[self::$ArrayLanguage['Screen']['0006']] = array();
        foreach ($pages as $eachPage) {
            // $Tags[self::$ArrayLanguage['Screen']['0006']]['%Link:Page:' . Core::EncryptNumberAdvanced($eachPage->getId()) . '%'] = $eachPage->getTitle();
            $Tags[self::$ArrayLanguage['Screen']['0006']]['%Link:Page:' . $eachPage->getEncryptedPageId() . '%'] = $eachPage->getTitle();
        }

        return array($Tags);
    }

    public function user_delete() {
        $SelectedUserIDs = func_get_args();
        if (is_array($SelectedUserIDs) == true && count($SelectedUserIDs) > 0) {
            $pageMapper = self::getMapper('Page');
            $visitorMapper = self::getMapper('Visitor');
            foreach ($SelectedUserIDs as $EachUserID) {
                $pageMapper->deleteByUserId($EachUserID);
                $visitorMapper->deleteByUserId($EachUserID);
            }
        }
    }

    public function ui_templates() {
        if (self::_PluginAppHeader('User') == false)
            return;

        if (AccountIsUnTrustedCheck(self::$UserInformation)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        
        if (self::$ObjectCI->input->post('TemplateID') !== false) {
            self::$ObjectCI->load->helper('url');
            self::_CreatePageSession(self::$ObjectCI->input->post('TemplateID'));
            redirect(InterfaceAppURL(true) . '/octlandingpage/builder/');
            exit;
        }

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0045'],
            'PluginLanguage' => self::$ArrayLanguage,
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => self::$ArrayLanguage['Screen']['0002'],
            'UserInformation' => self::$UserInformation,
            'Templates' => self::_GetTemplates(),
            'LanguageCode' => self::$LanguageCode
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Load the view file
        self::$ObjectCI->plugin_render(self::$PluginCode, 'user_templates', $ArrayViewData, true);
    }

    public function ui_builder_upload() {
        Core::LoadObject('api');
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'medialibrary.upload',
                    'protected' => true,
                    'username' => self::$UserInformation['Username'],
                    'password' => self::$UserInformation['Password'],
                    'parameters' => array(
                        'mediadata' => '',
                        'mediatype' => '',
                        'mediasize' => '',
                        'medianame' => ''
        )));
    }

    public function ui_builder_template($templateName = 'default', $content = '', $forAdmin = 0) {
        if ($forAdmin == 0) {
            if (self::_PluginAppHeader('User') == false)
                return;
        }
        else {
            if (self::_PluginAppHeader('') == false)
                return;
        }

        // check if template exists
        if (($templateName != '' && $content == '') || ($templateName != '' && $forAdmin == 1)) {
            $templatesPath = PLUGIN_PATH . 'octlandingpage/page_templates/';
            $requestedTemplatePath = $templatesPath . $templateName . '.html';
            if (!file_exists($requestedTemplatePath))
                return;
            $templateFileContents = file_get_contents($requestedTemplatePath);
            $templateFileContents = self::_ParseTemplateContents($templateFileContents);
        }

        if ($templateName == '' && $content != '') {
            $templateFileContents = $content;
        }

        $parsedTemplateFileContents = $templateFileContents;

        // INJECT STYLESHEETS
        $styles = array(
            PLUGIN_URL . 'octlandingpage/assets/editor/styles/iframe.css?' . time()
        );
        $styleInjection = "<!-- INJECTION START -->\n";
        foreach ($styles as $eachStyle) {
            $styleInjection .= '<link rel="stylesheet" href="' . $eachStyle . '">' . "\n";
        }
        $styleInjection .= "<!-- INJECTION FINISH -->\n";
        $parsedTemplateFileContents = str_replace('</head>', $styleInjection . "\n</head>", $parsedTemplateFileContents);

        // INJECT HEAD SCRIPTS
        $scripts = array(
            PLUGIN_URL . 'octlandingpage/assets/editor/scripts/jquery.js',
            PLUGIN_URL . 'octlandingpage/assets/editor/scripts/ckeditor/ckeditor.js',
            PLUGIN_URL . 'octlandingpage/assets/editor/scripts/ckeditor/adapters/jquery.js'
        );
        $scriptInjection = "<!-- INJECTION START -->\n";
        foreach ($scripts as $eachScript) {
            $scriptInjection .= '<script type="text/javascript" src="' . $eachScript . '"></script>' . "\n";
        }
        $scriptInjection .= "<!-- INJECTION FINISH -->\n";
        $parsedTemplateFileContents = str_replace('</head>', $scriptInjection . "\n</head>", $parsedTemplateFileContents);

        // INJEECT FOOTER SCRIPTS
        $scripts = array(
            'CKEDITOR.disableAutoInline = true;'
        );
        $scriptInjection = "<!-- INJECTION START -->\n" . '<script type="text/javascript">' . "\n";
        foreach ($scripts as $eachScript) {
            $scriptInjection .= $eachScript . "\n";
        }
        $scriptInjection .= "</script>\n<!-- INJECTION FINISH -->\n";
        $parsedTemplateFileContents = str_replace('</body>', $scriptInjection . "\n</body>", $parsedTemplateFileContents);

        header('Content-Type: text/html; charset=utf-8');
        echo $parsedTemplateFileContents;
        exit;
    }

    function ui_builder_parse_template_stylesheet() {
        if (self::_PluginAppHeader('') == false)
            return;

        if (self::$ObjectCI->input->post('template') !== false) {
            $template = self::$ObjectCI->input->post('template');
        } else {
            $templateName = self::$ObjectCI->input->post('templateName');
            $templatesPath = PLUGIN_PATH . 'octlandingpage/page_templates/';
            $requestedTemplatePath = $templatesPath . $templateName . '.html';
            $template = file_get_contents($requestedTemplatePath);
        }
        $editableAttributes = array();

        // GET ALL STYLE TAGS
        preg_match_all("/<style>.*<\/style>/uis", $template, $styleTagMatches);
        foreach ($styleTagMatches as $eachTagMatch) {
            // GET ALL STYLE DEFINITIONS WITHIN STYLE TAG BODY
            preg_match_all("/([^\{\t<\n]+)\{([^{]+)\}/uis", $eachTagMatch[0], $styleDefinitions, PREG_SET_ORDER);
            foreach ($styleDefinitions as $eachStyleDefinition) {
                // FIND ALL EDITABLE DEFINITIONS
                $selector = trim(str_replace("\t", '', $eachStyleDefinition[1]));
                $definition = explode("\n", trim(str_replace("\t", '', $eachStyleDefinition[2])));
                $editables = preg_grep("/\!editable/", $definition);
                foreach ($editables as $eachEditable) {
                    // PARSE EDITABLE ATTRIBUTE
                    $attribute = array();
                    $attribute['property'] = '';
                    $attribute['selector'] = $selector;

                    preg_match_all("/(\[@(?<key>[a-z]+)\s(?<value>[^[]+)\]).*/uiUs", $eachEditable, $optionMatches, PREG_SET_ORDER);
                    if (is_array($optionMatches) && count($optionMatches) > 0) {
                        foreach ($optionMatches as $each) {
                            $attribute[$each['key']] = $each['value'];
                        }
                    }

                    if (!isset($attribute['label']) && !isset($attribute['link']))
                        continue;

                    preg_match("/(^[^:]+)/", $eachEditable, $propertyMatch);
                    if (is_array($propertyMatch) && count($propertyMatch) > 1) {
                        $attribute['property'] = $propertyMatch[1];
                    }
                    $editableAttributes[] = $attribute;
                }
            }
        }

        header('Content-type: application/json');
        echo json_encode($editableAttributes);
    }

    public function ui_builder($pageId = 0) {
        if (self::_PluginAppHeader('User') == false)
            return;

        $pageMapper = self::GetMapper('Page');
        $page = null;
        if ($pageId != 0) {
            $page = $pageMapper->findByUserIdAndPageId(self::$UserInformation['UserID'], $pageId);
            if ($page === false) {
                self::$ObjectCI->display_user_message(self::$ArrayLanguage['Screen']['0012'], self::$ArrayLanguage['Screen']['0013']);
                return;
            }

            $pageSession = self::_CreatePageSession('');
            $pageSession->setId($page->getId());
            $pageSession->setContent($page->getContent());
            $pageSession->setTitle($page->getTitle());
            $pageSession->setTemplate($page->getTemplate());
            $pageSession->setScreenshot($page->getScreenshot());
//            print_r('<pre>');
//            print_r($page->getScreenshot());
//            print_r('</pre>');
//            exit;
            self::_UpdatePageSession($pageSession);
        }

        if (($pageSession = self::_BuilderHeader()) === false)
            return;

        if (self::$ObjectCI->input->post('Command') === 'Save') {
            $pageId = self::$ObjectCI->input->post('pageId');
            $title = self::$ObjectCI->input->post('title');
            $content = self::$ObjectCI->input->post('content');

            /* Eman */
            $img_val = self::$ObjectCI->input->post('img_val');
            $upload_dir = DATA_PATH . "screenshots/";
            $screenshot = 'landing_' . time() . '.png';

            //Get the base-64 string from data
            $filteredData = substr($img_val, strpos($img_val, ",") + 1);
            //Decode the string
            $unencodedData = base64_decode($filteredData);
            //Save the image
            file_put_contents($upload_dir . $screenshot, $unencodedData);

            /* Eman */

            if ($pageId !== '' || $pageId != 0)
                $pageSession->setId($pageId);

            $pageMapper = self::GetMapper('Page');
            $mode = '';

            if ($title === false || $title == '') {
                $returnData = array(
                    'result' => false,
                    'sections' => array('title' => self::$ArrayLanguage['Screen']['0074'])
                );
                header('Content-Type: application/json');
                echo json_encode($returnData);
                exit;
            }

            if ($pageSession->getId() != 0 && $pageSession->getId() != '') {
                $page = $pageMapper->findByUserIdAndPageId(self::$UserInformation['UserID'], $pageSession->getId());
                $mode = 'save';

                $old_screenshot = $page->getScreenshot();

                if (isset($old_screenshot) && !empty($old_screenshot)) {
                    $image_path = DATA_PATH . "screenshots/" . $old_screenshot;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            } else {
                $page = new LPB_Domain_Page();
                $page->setUserId(self::$UserInformation['UserID']);
                $page->setCustomURL('');
                $mode = 'insert';
            }

            $page->setTitle($title);
            $page->setContent(self::_CleanUpContent($content));
            $page->setScreenshot($screenshot); /* Eman */

            if ($mode == 'save') {
                $pageMapper->update($page);
            } else {
                $templatesPath = PLUGIN_PATH . 'octlandingpage/page_templates/';
                $requestedTemplatePath = $templatesPath . $pageSession->getTemplateId() . '.html';
                $templateFileContents = file_get_contents($requestedTemplatePath);
                $templateFileContents = self::_ParseTemplateContents($templateFileContents);
                $page->setTemplate($templateFileContents);
                $pageMapper->insert($page);
                $pageSession->setId($page->getId());
                $pageSession->setTemplate($templateFileContents);
            }

            $pageSession->setContent($page->getContent());
            $pageSession->setTitle($page->getTitle());
            $pageSession->setScreenshot($page->getScreenshot());

            self::_UpdatePageSession($pageSession);

            $returnData = array(
                'result' => true,
                'PageID' => $page->getId()
            );

            header('Content-Type: application/json');
            echo json_encode($returnData);
            exit;
        }

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0045'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'LanguageCode' => self::$LanguageCode,
            'PageSession' => $pageSession,
            'Page' => $page
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Load the view file
        self::$ObjectCI->plugin_render(self::$PluginCode, 'user_builder', $ArrayViewData, true);
    }

    public function ui_template_test($template) {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0045'],
            'PluginLanguage' => self::$ArrayLanguage,
            'LanguageCode' => self::$LanguageCode,
            'Template' => $template
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Load the view file
        self::$ObjectCI->plugin_render(self::$PluginCode, 'admin_tester', $ArrayViewData, true);
    }

    public function ui_builder_compiled_content($pageId) {
        if (self::_PluginAppHeader('User') == false)
            return;
        $pageMapper = self::GetMapper('Page');
        $page = $pageMapper->findByUserIdAndPageId(self::$UserInformation['UserID'], $pageId);

        self::ui_builder_template('', $page->getCompiledContent(false));
    }

    public function ui_builder_preview() {
        if (self::_PluginAppHeader('User') == false)
            return;

        if (self::$ObjectCI->input->post('content') !== false && self::$ObjectCI->input->post('template') !== false) {
            $template = self::$ObjectCI->input->post('template');
            $content = self::$ObjectCI->input->post('content');

            $page = new LPB_Domain_Page();
            $page->setContent($content);
            $page->setTemplate($template);
            $_SESSION['PageBuilderSessionPreview'] = $page->getCompiledContent();

            header('Content-type: application/json');
            echo json_encode(array('result' => true));
            exit;
        }

        if (!isset($_SESSION['PageBuilderSessionPreview']))
            return;

        header('Content-type: text/html');
        echo $_SESSION['PageBuilderSessionPreview'];
        exit;
    }

    public function ui_admin_settings($SelectedTab = 'settings') {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        Core::LoadObject('api');

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0026'],
            'CurrentMenuItem' => 'Settings',
            'PluginView' => '../plugins/octlandingpage/templates/admin_settings.php',
            'SubSection' => 'Landing Page Builder',
            'PluginLanguage' => self::$ArrayLanguage,
            'PluginLicenseKey' => self::$PluginLicenseKey,
            'SelectedTab' => $SelectedTab,
            'AdminInformation' => self::$AdminInformation,
            'PluginVersion' => self::$PluginVersion,
            'PluginLicenseInfo' => self::$PluginLicenseInfo,
            'TestTemplates' => self::_GetTemplates(true)
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->render('admin/settings', $ArrayViewData);
    }

    public function ui_trial_request() {
        if (file_exists(APP_PATH . '/data/license_octlandingpage.dat') == true) {
            $_SESSION['PluginMessage'] = array(false, 'It seems that a license for this plugin has been already generated.');
            header("location: " . InterfaceAppURL(true) . '/admin/plugins');
            exit;
        }

        include_once(PLUGIN_PATH . '/octlandingpage/functions.php');

        $LicenseEngine = new octlandingpage_license_engine();
        $LicenseExpireDate = $LicenseEngine->request_trial_license();

        if (is_array($LicenseExpireDate) == true && is_bool($LicenseExpireDate[0]) == true && $LicenseExpireDate[0] == false) {
            $_SESSION['PluginMessage'] = array(false, $LicenseExpireDate[1]);
            header("location: " . InterfaceAppURL(true) . '/admin/plugins');
            exit;
        }

        $_SESSION['PluginMessage'] = array(true, 'Trial period has just started. It will expire on ' . date('Y-m-d', strtotime($LicenseExpireDate)));
        header("location: " . InterfaceAppURL(true) . '/admin/plugins/enable/octlandingpage');
        exit;
    }

    public function ui_pages() {
        if (self::_PluginAppHeader('User') == false)
            return;

        if (self::$ObjectCI->input->post('Command') == 'DeletePages') {
            $selectedPages = self::$ObjectCI->input->post('SelectedPages');
            if (is_array($selectedPages) && count($selectedPages) > 0) {
                foreach ($selectedPages as $eachSelectedPage) {
                    self::getMapper('Page')->deleteByPageIdAndUserId($eachSelectedPage, self::$UserInformation['UserID']);
                    self::getMapper('Visitor')->deleteByPageIdAndUserId($eachSelectedPage, self::$UserInformation['UserID']);
                }
            }
        }

        $pages = self::getMapper('Page')->findAllByUserId(self::$UserInformation['UserID']);

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0002'],
            'PluginLanguage' => self::$ArrayLanguage,
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => self::$ArrayLanguage['Screen']['0002'],
            'UserInformation' => self::$UserInformation,
            'Pages' => $pages,
            'LanguageCode' => self::$LanguageCode
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Load the view file
        self::$ObjectCI->plugin_render(self::$PluginCode, 'user_pages', $ArrayViewData, true);
    }

    public function ui_customurl($pageId) {
        if (self::_PluginAppHeader('User') == false)
            return;

        $page = self::getMapper('Page')->findByUserIdAndPageId(self::$UserInformation['UserID'], $pageId);
        if (!$page) {
            self::$ObjectCI->display_user_message(self::$ArrayLanguage['Screen']['0012'], self::$ArrayLanguage['Screen']['0013']);
            return;
        }

        $pageSuccessMessage = '';
        if (self::$ObjectCI->input->post('Command') == 'SaveCustomURL') {
            $customURL = self::$ObjectCI->input->post('CustomURL');
            if ($customURL !== '') {
                self::$ObjectCI->form_validation->set_rules('CustomURL', 'CustomURL', 'prep_url');
                $validationResult = self::$ObjectCI->form_validation->run();
                self::$ObjectCI->form_validation->run();
                $customURL = self::$ObjectCI->input->post('CustomURL');
            }

            $page->setCustomURL($customURL);
            self::GetMapper('Page')->update($page);
            $pageSuccessMessage = self::$ArrayLanguage['Screen']['0020'];
        }
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0014'],
            'PluginLanguage' => self::$ArrayLanguage,
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => self::$ArrayLanguage['Screen']['0002'],
            'UserInformation' => self::$UserInformation,
            'Page' => $page,
            'PageSuccessMessage' => $pageSuccessMessage,
            'CustomURLInfo' => parse_url($page->getCustomURL()),
            'LanguageCode' => self::$LanguageCode
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Load the view file
        self::$ObjectCI->plugin_render(self::$PluginCode, 'user_page_options', $ArrayViewData, true);
    }

    public function ui_view($encUserId, $encPageId, $encListId = 0, $encSubscriberId = 0) {
        self::_PluginAppHeader('', true);

        self::$ObjectCI->load->helper('url');
        $uriString = str_replace('octlandingpage/view/', '', trim(uri_string(), '/'));
        if ($encListId != 0 && $encSubscriberId != 0) {
            list($userId, $pageId, $listId, $subscriberId) = Core::DecryptQueryStringAsArrayAdvanced($uriString);
        } else {
            list($userId, $pageId) = Core::DecryptQueryStringAsArrayAdvanced($uriString);
            $listId = 0;
            $subscriberId = 0;
        }

        $page = self::getMapper('Page')->findByUserIdAndPageId($userId, $pageId);

        if ($page === false)
            return;

        self::_TrackPageView($page, $listId, $subscriberId);

        echo $page->getCompiledContent();
    }

    private function _InstallDB() {
        $query = 'DROP TABLE IF EXISTS `oempro_landing_pages`;';
        Database::$Interface->ExecuteQuery($query);
        $query = 'CREATE TABLE `oempro_landing_pages` ( `PageID` int(11) unsigned NOT NULL AUTO_INCREMENT, `PageTitle` varchar(255) CHARACTER SET utf8 NOT NULL, `PageContent` text CHARACTER SET utf8 NOT NULL, `PageTemplate` text CHARACTER SET utf8 NOT NULL, `RelOwnerUserID` int(11) NOT NULL, `Options` TEXT NOT NULL, PRIMARY KEY (`PageID`), KEY `RelOwnerUserID` (`RelOwnerUserID`), KEY `IDX_PAGE_USER_ID` (`PageID`,`RelOwnerUserID`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        Database::$Interface->ExecuteQuery($query);

        $query = 'DROP TABLE IF EXISTS `oempro_landing_pages_visitor_tracking`;';
        Database::$Interface->ExecuteQuery($query);
        $query = 'CREATE TABLE `oempro_landing_pages_visitor_tracking` (`ID` int(11) unsigned NOT NULL AUTO_INCREMENT, `VisitorID` int(11) unsigned NOT NULL, `RelPageID` int(11) unsigned NOT NULL, `RelOwnerUserID` int(11) unsigned NOT NULL, `DateTime` datetime NOT NULL, `RelListID` int(11) unsigned NOT NULL, `RelSubscriberID` int(11) unsigned NOT NULL, `IPAddress` varchar(15) COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`ID`), UNIQUE KEY `IDX_UNIQUE_VISITOR` (`VisitorID`,`RelPageID`), KEY `IDX_PAGE` (`RelPageID`), KEY `IDX_USER` (`RelOwnerUserID`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
        Database::$Interface->ExecuteQuery($query);
    }

    private function _UninstallDB() {
        $query = 'DROP TABLE IF EXISTS oempro_landing_pages';
        Database::$Interface->ExecuteQuery($query);

        $query = 'DROP TABLE IF EXISTS oempro_landing_pages_visitor_tracking';
        Database::$Interface->ExecuteQuery($query);
    }

    private function _GetCookieName() {
        return COOKIE_NAME . '_octlandingpage';
    }

    private function _TrackPageView($page, $listId, $subscriberId) {
        $visitorMapper = self::getMapper('Visitor');
        $urlInfo = parse_url($page->getCustomURL() == '' ? APP_URL : $page->getCustomURL());

        if (!isset($_COOKIE[self::_GetCookieName()])) {
            $visitor = new LPB_Domain_Visitor();
            $visitor->setPageId($page->getId());
            $visitor->setUserId($page->getUserId());
            $visitor->setDateTime(date('Y-m-d H:i:s'));
            $visitor->setIPAddress($_SERVER['REMOTE_ADDR']);
            $visitor->setListId($listId);
            $visitor->setSubscriberId($subscriberId);
            $visitorMapper->insert($visitor);

            $visitorMapper->assignUniqueVisitorId($visitor);

            $cookieData = base64_encode(Core::EncryptArrayAsQueryStringAdvanced(array($page->getId(), $visitor->getVisitorId())));
            setcookie(self::_GetCookieName(), $cookieData, strtotime('+30 days'), '/', $urlInfo['host']);
        } else {
            $cookieData = base64_decode($_COOKIE[self::_GetCookieName()]);
            list($pageId, $visitorId) = Core::DecryptQueryStringAsArrayAdvanced($cookieData);
            if ($pageId != $page->getId()) {
                $visitor = new LPB_Domain_Visitor();
                $visitor->setPageId($page->getId());
                $visitor->setUserId($page->getUserId());
                $visitor->setDateTime(date('Y-m-d H:i:s'));
                $visitor->setIPAddress($_SERVER['REMOTE_ADDR']);
                $visitor->setListId($listId);
                $visitor->setSubscriberId($subscriberId);
                $visitor->setVisitorId($visitorId);
                $visitorMapper->insert($visitor);

                $cookieData = base64_encode(Core::EncryptArrayAsQueryStringAdvanced(array($page->getId(), $visitor->getVisitorId())));
                setcookie(self::_GetCookieName(), $cookieData, strtotime('+30 days'), '/', $urlInfo['host']);
            }
        }
    }

    private function _GetTemplates($getOnlyTestTemplates = false) {
        $templatePath = PLUGIN_PATH . 'octlandingpage/page_templates/';
        $templates = array();

        if ($handle = @opendir($templatePath)) {
            while (false !== ($file = @readdir($handle))) {
                if ('.' === $file)
                    continue;
                if ('..' === $file)
                    continue;
                if (false === strpos($file, '.html'))
                    continue;

                if ($getOnlyTestTemplates && strpos($file, '_') !== 0)
                    continue;
                if (!$getOnlyTestTemplates && strpos($file, '_') === 0)
                    continue;

                $templateID = str_replace('.html', '', $file);
                $templateTag = '';

                $templateThumbnail = str_replace('.html', '.png', $file);
                if (!file_exists(PLUGIN_PATH . '/octlandingpage/page_templates/' . $templateThumbnail))
                    $templateThumbnail = '';

                if (!is_bool(strpos($templateID, '__'))) {
                    preg_match("/__(.*)$/ui", $templateID, $matches);
                    if (count($matches) > 0) {
                        $templateTag = $matches[1];
                        $templateName = ucwords(str_replace('_', ' ', preg_replace("/(__.*)$/ui", "", $templateID)));
                    }
                } else {
                    $templateName = ucwords(str_replace('_', ' ', $templateID));
                }

                $templates[] = array(
                    'TemplateID' => $templateID,
                    'TemplateName' => $templateName,
                    'TemplateThumbnail' => $templateThumbnail,
                    'TemplateTag' => $templateTag
                );
            }
            closedir($handle);
        }

        return $templates;
    }

    private function _PluginAppHeader($Section = 'Admin', $IgnoreLicenseCheck = false) {
        self::$ObjectCI = & get_instance();

        // License status check
        if (self::$PluginLicenseStatus == false && $IgnoreLicenseCheck == false) {
            $Message = sprintf(self::$ArrayLanguage['Screen']['0025'], (defined('octlandingpage_LicenseStatusMessage') == true ? (octlandingpage_LicenseStatusMessage == 'expired' ? self::$ArrayLanguage['Screen']['0024'] : '') : ''), self::$PluginLicenseKey);
            self::$ObjectCI->display_public_message('', $Message);
            return false;
        }

        if (Plugins::IsPlugInEnabled(self::$PluginCode) == false) {
            $Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
            self::$ObjectCI->display_public_message('', $Message);
            return false;
        }

        if ($Section == 'Admin') {
            self::_CheckAdminAuth();
        } elseif ($Section == 'User') {
            self::_CheckUserAuth();
        }

        return true;
    }

    private function _CheckAdminAuth() {
        Core::LoadObject('admin_auth');
        Core::LoadObject('admins');

        AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');

        self::$AdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));

        return;
    }

    private function _CheckUserAuth() {
        Core::LoadObject('user_auth');

        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');

        self::$UserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);

        if (Users::IsAccountExpired(self::$UserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }

        return;
    }

    private function _CheckLicenseStatus() {
        global $LicenseCheck;

        self::$PluginLicenseStatus = octlandingpage_PerformLicenseCheck();
        self::$PluginLicenseKey = $LicenseCheck->get_plugin_license_key();
        self::$PluginLicenseInfo = $LicenseCheck->get_plugin_license_info();
        if (self::$PluginLicenseStatus == false) {
            return false;
        }
        return true;
    }

    private function _CreatePageSession($templateId) {
        $pageSession = new LPB_PageSession();
        $pageSession->setTemplateId($templateId);

        if ($templateId != '') {
            $templatesPath = PLUGIN_PATH . 'octlandingpage/page_templates/';
            $requestedTemplatePath = $templatesPath . $pageSession->getTemplateId() . '.html';
            $templateFileContents = file_get_contents($requestedTemplatePath);
            $templateFileContents = self::_ParseTemplateContents($templateFileContents);
            $pageSession->setTemplate($templateFileContents);
        }

        $_SESSION['PageBuilderSession'] = serialize($pageSession);
        return $pageSession;
    }

    private function _UpdatePageSession(LPB_PageSession $pageSession) {
        $_SESSION['PageBuilderSession'] = serialize($pageSession);
    }

    protected function _BuilderHeader() {
        if (!isset($_SESSION['PageBuilderSession']))
            return false;
        $pageSession = @unserialize($_SESSION['PageBuilderSession']);
        if (!$pageSession)
            return false;
        if (get_class($pageSession) !== 'LPB_PageSession')
            return false;

        return $pageSession;
    }

    protected function _CleanUpContent($content) {
        $pattern = "/<body[^>]*?>(.*)<\/body>/ui";
        return preg_replace($pattern, "$1", $content);
    }

    protected function _ParseTemplateContents($templateFileContents) {
        Core::LoadObject('api');
        $UserLists = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => self::$UserInformation['Username'],
                    'password' => self::$UserInformation['Password'],
                    'parameters' => array(
                    )
        ));
        $UserLists = $UserLists['Lists'];

        $BlockOptionsTextForLists = array();
        foreach ($UserLists as $EachList) {
            $BlockOptionsTextForLists[] = $EachList['ListID'] . '##!!##' . htmlentities($EachList['Name'], ENT_QUOTES | ENT_HTML401);
        }
        $BlockOptionsTextForLists = implode('!!##!!', $BlockOptionsTextForLists);
        $templateFileContents = str_replace('%UserListsAsBlockOption%', $BlockOptionsTextForLists, $templateFileContents);

        $templateFileContents = str_replace('%APP_URL%', APP_URL, $templateFileContents);
        return $templateFileContents;
    }

}
