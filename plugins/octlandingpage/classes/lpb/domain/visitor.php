<?php
class LPB_Domain_Visitor extends O_Domain_Abstract
{
	protected $_userId;
	protected $_visitorId = 0;
	protected $_pageId;
	protected $_dateTime;
	protected $_listId = 0;
	protected $_subscriberId = 0;
	protected $_ipAddress;

	protected function _init()
	{
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getVisitorId()
	{
		return $this->_visitorId;
	}

	public function setVisitorId($visitorId)
	{
		$this->_visitorId = $visitorId;
	}

	public function getPageId()
	{
		return $this->_pageId;
	}

	public function setPageId($pageId)
	{
		$this->_pageId = $pageId;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}

	public function getListId()
	{
		return $this->_listId;
	}

	public function setListId($listId)
	{
		$this->_listId = $listId;
	}

	public function getSubscriberId()
	{
		return $this->_subscriberId;
	}

	public function setSubscriberId($subscriberId)
	{
		$this->_subscriberId = $subscriberId;
	}

	public function getIPAddress()
	{
		return $this->_ipAddress;
	}

	public function setIPAddress($ipAddress)
	{
		$this->_ipAddress = $ipAddress;
	}


}