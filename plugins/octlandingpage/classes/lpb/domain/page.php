<?php
class LPB_Domain_Page extends O_Domain_Abstract
{
	protected $_userId;
	protected $_title;
	protected $_content;
	protected $_template;
	protected $_numOfUniqueVisits = 0;
	protected $_options;
	protected $_screenshot;/*Eman*/

	protected function _init()
	{
		$this->_options = new stdClass;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getTitle()
	{
		return $this->_title;
	}

	public function setTitle($title)
	{
		$this->_title = $title;
	}

	public function getContent()
	{
		return $this->_content;
	}

	public function getCompiledContent($removeContentEditable = true)
	{
		$content = preg_replace("/(.*<body[^>]*?>)(.*)(<\/body>.*)/uis", "$1" . $this->getContent() . "$3", $this->getTemplate());
		if ($removeContentEditable === true)
		{
			$content = preg_replace("/contenteditable=\"true\"/uim", "", $content);
		}
		$content = preg_replace("/<title>.*<\/title>/uim", "<title>" . $this->getTitle() . "</title>", $content);
		return $content;
	}

	public function setContent($content)
	{
		$this->_content = $content;
	}

	public function getTemplate()
	{
		return $this->_template;
	}

	public function setTemplate($template)
	{
		$this->_template = $template;
	}

	public function getUniqueVisitCount()
	{
		return $this->_numOfUniqueVisits;
	}

	public function setUniqueVisitCount($count)
	{
		$this->_numOfUniqueVisits = $count;
	}

	public function getOptions()
	{
		return $this->_options;
	}

	public function setOptions($options)
	{
		$this->_options = $options;
	}

	public function getEncryptedPageId()
	{
		return Core::EncryptArrayAsQueryStringAdvanced(array($this->getUserId(), $this->getId()));
	}

	public function getURL()
	{
		return $this->getCustomURL() == '' ? $this->getOriginalURL() : $this->getCustomURL();
	}

	public function getOriginalURL()
	{
		if (function_exists('InterfaceAppURL'))
		{
			return InterfaceAppURL(true) . '/octlandingpage/view/' . $this->getEncryptedPageId();
		}
		else
		{
			return APP_URL.APP_DIRNAME.(HTACCESS_ENABLED == false ? '/index.php?' : '').'/octlandingpage/view/' . $this->getEncryptedPageId();
		}
	}

	public function setCustomURL($url)
	{
		$this->_options->url = $url;
	}

	public function getCustomURL($url)
	{
		if (! isset($this->_options->url)) return '';
		return $this->_options->url;
	}
        
        /*Eman*/
        public function getScreenshot()
	{
		return $this->_screenshot;
	}

	public function setScreenshot($screenshot)
	{
		$this->_screenshot = $screenshot;
	}


}