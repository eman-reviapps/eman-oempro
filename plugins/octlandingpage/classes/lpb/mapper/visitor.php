<?php
class LPB_Mapper_Visitor extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_landing_pages_visitor_tracking');
		$query->setCondition(new O_Sql_Condition(
			'ID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$visitor = new LPB_Domain_Visitor($dbRowArray['ID']);
		$visitor->setRawDbColumns($dbRowArray);
		$visitor->setVisitorId($dbRowArray['VisitorID']);
		$visitor->setPageId($dbRowArray['RelPageID']);
		$visitor->setUserId($dbRowArray['RelOwnerUserID']);
		$visitor->setDateTime($dbRowArray['DateTime']);
		$visitor->setListId($dbRowArray['RelListID']);
		$visitor->setSubscriberId($dbRowArray['RelSubscriberID']);
		$visitor->setIPAddress($dbRowArray['IPAddress']);
		return $visitor;
	}

	public function assignUniqueVisitorId(LPB_Domain_Visitor $obj)
	{
		$query = new O_Sql_SelectQuery('oempro_landing_pages_visitor_tracking');
		$query->setFields(array('MAX(VisitorID)' => 'NextVisitorID'));
		$rs = $this->_db->query($query);

		$max = $rs->fetch_assoc();
		if ($rs->num_rows < 1)
		{
			$max = 0;
		}
		else
		{
			$max = (int) $max['NextVisitorID'];
		}

		$visitorID = $max + 1;

		$updateQuery = new O_Sql_UpdateQuery('oempro_landing_pages_visitor_tracking');
		$updateQuery->setFields(array(
			'VisitorID' => $visitorID
		));
		$updateQuery->setCondition(new O_Sql_Condition(
			'ID', '=', $obj->getId()
		));
		$this->_db->query($updateQuery);

		$obj->setVisitorId($visitorID);
	}

	public function findByVisitorIdAndPageId($visitorId, $pageId)
	{
		$query = new O_Sql_SelectQuery('oempro_landing_pages_visitor_tracking');
		$query->addComment('Find landing page visitor tracking data by visitor id and page id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition1 = new O_Sql_Condition('VisitorID', '=', $visitorId, FALSE);
		$condition2 = new O_Sql_Condition('RelPageID', '=', $pageId, FALSE);
		$condition3 = new O_Sql_ConditionGroup();
		$condition3->add($condition1);
		$condition3->add($condition2);
		$query->setCondition($condition3);

		$rs = $this->_db->query($query);

		$data = false;
		if ($rs->num_rows > 0) {
			while ($row = $rs->fetch_assoc()) {
				$data = $this->_constructObject($row);
			}
		}

		return $data;
	}

	public function getTotalUniqueVisitorsOfPageId($pageId)
	{
		$query = new O_Sql_SelectQuery('oempro_landing_pages_visitor_tracking');
		$query->setFields(array('COUNT(*)' => 'TotalVisitors'));

		$condition = new O_Sql_Condition('RelPageID', '=', $pageId, FALSE);
		$query->setCondition($condition);

		$rs = $this->_db->query($query);
		if ($rs->num_rows > 0) {
			$row = $rs->fetch_assoc();
			return (int) $row['TotalVisitors'];
		} else {
			return 0;
		}
	}

	public function insert(LPB_Domain_Visitor $obj)
	{
		if ($obj->getVisitorId() != 0)
		{
			$doesExist = $this->findByVisitorIdAndPageId($obj->getVisitorId(), $obj->getPageId());
			if ($doesExist !== false) return;
		}

		$query = new O_Sql_InsertQuery('oempro_landing_pages_visitor_tracking');
		$query->addComment('Landing page visitor tracking insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'VisitorID' => $this->_db->real_escape_string($obj->getVisitorId()),
										'RelPageID' => $this->_db->real_escape_string($obj->getPageId()),
										'RelOwnerUserID' => $this->_db->real_escape_string($obj->getUserId()),
										'DateTime' => $this->_db->real_escape_string($obj->getDateTime()),
										'RelListID' => $this->_db->real_escape_string($obj->getListId()),
										'RelSubscriberID' => $this->_db->real_escape_string($obj->getSubscriberId()),
										'IPAddress' => $this->_db->real_escape_string($obj->getIPAddress())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function deleteByUserId($userId)
	{
		$query = new O_Sql_DeleteQuery('oempro_landing_pages_visitor_tracking');
		$query->addComment('Delete landing pages visitor tracking by user id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
		$query->setCondition($condition);

		$rs = $this->_db->query($query);
	}

	public function deleteByPageIdAndUserId($pageId, $userId)
	{
		$query = new O_Sql_DeleteQuery('oempro_landing_pages_visitor_tracking');
		$query->addComment('Delete landing page visitor tracking by page id and user id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition1 = new O_Sql_Condition('RelPageID', '=', $pageId, FALSE);
		$condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
		$condition3 = new O_Sql_ConditionGroup();
		$condition3->add($condition1);
		$condition3->add($condition2);
		$query->setCondition($condition3);

		$rs = $this->_db->query($query);
	}
}
