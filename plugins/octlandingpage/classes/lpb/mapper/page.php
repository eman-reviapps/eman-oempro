<?php

class LPB_Mapper_Page extends O_Mapper_Abstract {

    protected function _getSqlQueryForFindById($id) {
        $query = new O_Sql_SelectQuery('oempro_landing_pages');
        $query->setCondition(new O_Sql_Condition(
                'PageID', '=', $id
        ));
        return $query;
    }

    /* Eman */

    protected function _constructObject($dbRowArray) {
        $page = new LPB_Domain_Page($dbRowArray['PageID']);
        $page->setRawDbColumns($dbRowArray);
        $page->setUserId($dbRowArray['RelOwnerUserID']);
        $page->setTitle($dbRowArray['PageTitle']);
        $page->setContent($dbRowArray['PageContent']);
        $page->setTemplate($dbRowArray['PageTemplate']);
        $page->setOptions(json_decode($dbRowArray['Options']));
        $page->setScreenshot($dbRowArray['ScreenshotImage']);
        return $page;
    }

    /* Eman */

    public function insert(LPB_Domain_Page $obj) {
        $query = new O_Sql_InsertQuery('oempro_landing_pages');
        $query->addComment('Landing page insert query');
        $query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

        $query->setFieldsAndValues(array(
            'PageTitle' => $this->_db->real_escape_string($obj->getTitle()),
            'RelOwnerUserID' => $this->_db->real_escape_string($obj->getUserID()),
            'PageContent' => $this->_db->real_escape_string($obj->getContent()),
            'PageTemplate' => $this->_db->real_escape_string($obj->getTemplate()),
            'Options' => $this->_db->real_escape_string(json_encode($obj->getOptions())),
            'ScreenshotImage' => $this->_db->real_escape_string($obj->getScreenshot())
        ));

        $id = $this->_db->query($query);
        $obj->setId($id);
    }

    /* Eman */

    public function update(LPB_Domain_Page $obj) {
        $query = new O_Sql_UpdateQuery('oempro_landing_pages');
        $query->setFields(array(
            'PageTitle' => $this->_db->real_escape_string($obj->getTitle()),
            'PageContent' => $this->_db->real_escape_string($obj->getContent()),
            'PageTemplate' => $this->_db->real_escape_string($obj->getTemplate()),
            'Options' => $this->_db->real_escape_string(json_encode($obj->getOptions())),
            'ScreenshotImage' => $this->_db->real_escape_string($obj->getScreenshot())
        ));

        $condition1 = new O_Sql_Condition('PageID', '=', $obj->getId(), FALSE);
        $condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $obj->getUserId(), FALSE);
        $condition3 = new O_Sql_ConditionGroup();
        $condition3->add($condition1);
        $condition3->add($condition2);
        $query->setCondition($condition3);

        $this->_db->query($query);
    }

    public function findAllByUserId($userId) {
        $query = new O_Sql_SelectQuery('oempro_landing_pages');
        $query->addComment('Landing pages find all pages by user id query');
        $query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
        $query->setCondition(new O_Sql_Condition(
                'RelOwnerUserID', '=', $userId
        ));

        $rs = $this->_db->query($query);

        $pages = array();
        if ($rs->num_rows > 0) {
            while ($row = $rs->fetch_assoc()) {
                $page = $this->_constructObject($row);
                $page->setUniqueVisitCount($this->getUniqueVisitCountForPage($page));
                $pages[] = $page;
            }
        }

        return $pages;
    }

    public function getUniqueVisitCountForPage(LPB_Domain_Page $page) {
        $visitorMapper = octlandingpage::getMapper('Visitor');
        return $visitorMapper->getTotalUniqueVisitorsOfPageId($page->getId());
    }

    public function findByUserIdAndPageId($userId, $pageId) {
        $query = new O_Sql_SelectQuery('oempro_landing_pages');
        $query->addComment('Find landing page by page id and user id query');
        $query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

        $condition1 = new O_Sql_Condition('PageID', '=', $pageId, FALSE);
        $condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
        $condition3 = new O_Sql_ConditionGroup();
        $condition3->add($condition1);
        $condition3->add($condition2);
        $query->setCondition($condition3);

        $rs = $this->_db->query($query);

        $page = false;
        if ($rs->num_rows > 0) {
            while ($row = $rs->fetch_assoc()) {
                $page = $this->_constructObject($row);
            }
        }

        return $page;
    }

    public function deleteByPageIdAndUserId($pageId, $userId) {
        $query = new O_Sql_DeleteQuery('oempro_landing_pages');
        $query->addComment('Delete landing page by page id and user id query');
        $query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

        $condition1 = new O_Sql_Condition('PageID', '=', $pageId, FALSE);
        $condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
        $condition3 = new O_Sql_ConditionGroup();
        $condition3->add($condition1);
        $condition3->add($condition2);
        $query->setCondition($condition3);

        $rs = $this->_db->query($query);
    }

    public function deleteByUserId($userId) {
        $query = new O_Sql_DeleteQuery('oempro_landing_pages');
        $query->addComment('Delete landing pages by user id query');
        $query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

        $condition = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
        $query->setCondition($condition);

        $rs = $this->_db->query($query);
    }

}
