<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.$PluginLanguage['Screen']['0001']; ?></title>
</head>
<body>

<div id="page-code-editor">
	<div id="page-code-editor-header">
<!-- 		<div class="page-editor-header-form-row">
			<label for="tite"><?php echo $PluginLanguage['Screen']['0045']; ?></label>
			<input id="page-editor-title-placeholder" type="text" placeholder="Enter your page title here" style="width:600px;font-weight:bold;" disabled="disabled">
			<span id="title-placeholder-error" class="page-editor-input-sidekick error"></span>
		</div>
 -->		<div class="button-container">
			<!-- <a href="#" class="primary" id="page-editor-button-save"><?php echo $PluginLanguage['Screen']['0048']; ?><span class="loading"><?php echo $PluginLanguage['Screen']['0049']; ?></span><span class="success"><?php echo $PluginLanguage['Screen']['0050']; ?></span><span class="fail"><?php echo $PluginLanguage['Screen']['0051']; ?></span></a> -->
			<a href="<?php InterfaceAppUrl(); ?>/octlandingpage/admin_settings/" class="secondary"><?php echo $PluginLanguage['Screen']['0052']; ?></a>
		</div>
		<div class="error-container" id="page-editor-error-container">
		</div>
	</div>
	<div id="page-code-editor-tab-container">
		<a href="page-code-editor-email-builder"><?php echo $PluginLanguage['Screen']['0053']; ?></a>
		<!-- <a href="page-code-editor-preview-content" style="color:#CCC;"><?php echo $PluginLanguage['Screen']['0054']; ?></a> -->
	</div>
	<div id="page-code-editor-email-builder" class="page-code-editor-tab-content">
		<div class="page-code-editor-tab-toolbar">
		</div>
		<div class="page-code-editor-tab-sidebar" id="page-code-editor-builder-blocks-sidebar">
			<div class="page-code-editor-builder-block-folder">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-abc"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-text">
				<h2><?php echo $PluginLanguage['Screen']['0055']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-media"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-media">
				<h2><?php echo $PluginLanguage['Screen']['0057']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-buttons"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-buttons">
				<h2><?php echo $PluginLanguage['Screen']['0058']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-other"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-other">
				<h2><?php echo $PluginLanguage['Screen']['0059']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
		</div>
		<iframe id="page-code-editor-builder-iframe" src="" frameborder="0"></iframe>
	</div>
	<div id="page-code-editor-preview-content" class="page-code-editor-tab-content">
		<div class="page-code-editor-tab-toolbar">
			<div class="toolbar-button-group">
				<a href="#" class="selected" data-preview-mode="desktop"><?php echo $PluginLanguage['Screen']['0060']; ?></a>
				<a href="#" data-preview-mode="tablet"><?php echo $PluginLanguage['Screen']['0061']; ?></a>
				<a href="#" data-preview-mode="phone"><?php echo $PluginLanguage['Screen']['0062']; ?></a>
			</div>
		</div>
		<iframe id="page-code-editor-preview-iframe" src="about:blank" frameborder="0"></iframe>
		<div id="page-code-editor-preview-iframe-frame"></div>
	</div>
</div>

<div id="dummy-image-editor-popup-trigger" data-form-popup="form-popup-image-editor"></div>
<div id="dummy-block-editor-popup-trigger" data-form-popup="form-popup-block-editor"></div>

<div id="popup-form-content-container" style="display:none">
	<div id="form-popup-block-editor">
		<div style="width:200px;">
			<div id="form-popup-block-editor-contents">
				
			</div>
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-block-options-save"><?php echo $PluginLanguage['Screen']['0063']; ?></button> <?php echo $PluginLanguage['Screen']['0064']; ?> <a href="#" class="form-popup-cancel"><?php echo $PluginLanguage['Screen']['0065']; ?></a>
			</div>
		</div>
	</div>
	<div id="form-popup-image-editor">
		<div style="width:400px;">
			<div class="left" style="width:200px;">
				<div class="form-row">
					<label class="input-label"><?php echo $PluginLanguage['Screen']['0066']; ?></label>
					<input id="email-builder-popup-image-link" type="text" style="width:180px;">
				</div>
			</div>
			<div class="left" style="width:200px;">
				<div class="form-row">
				<label class="input-label"><?php echo $PluginLanguage['Screen']['0067']; ?></label>
				<input id="email-builder-popup-image-alt" type="text" style="width:180px;">
				</div>
			</div>
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-image-editor-save"><?php echo $PluginLanguage['Screen']['0063']; ?></button> <?php echo $PluginLanguage['Screen']['0064']; ?> <a href="#" class="form-popup-cancel"><?php echo $PluginLanguage['Screen']['0065']; ?></a>
			</div>
		</div>
	</div>
	<div id="email-builder-popup-theme-options">
		<div style="width:500px;">
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-theme-options-save"><?php echo $PluginLanguage['Screen']['0064']; ?></button> <?php echo $PluginLanguage['Screen']['0065']; ?> <a href="#" class="form-popup-cancel"><?php echo $PluginLanguage['Screen']['0066']; ?></a>
			</div>
		</div>
	</div>
</div>

<div id="popup-media-library">
	<iframe src="<?php InterfaceAppURL(); ?>/user/medialibrary/browse/0/popup/a/1" frameborder="0"></iframe>
</div>

<form action="" id="editor-form">
	<input type="hidden" name="Command" value="Save">
	<input type="hidden" name="Title" value="">
	<input type="hidden" name="PageID" value="0">
	<input type="hidden" name="Template" value="<?php echo InterfaceAppURL(true) . '/octlandingpage/builder_template/' . $Template . '/a/1'; ?>">
	<textarea id="page-editor-hidden-html-content" style="display:none"></textarea>
	<textarea id="page-editor-hidden-template-html-content" style="display:none"></textarea>
</form>

<div id="page-editor-preloader"></div>
<style>
	@import "<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/styles/editor.css?<?php echo time(); ?>";
</style>
<script>
	var APP_URL = '<?php echo InterfaceAppURL(); ?>';
	var PAGE_ID = 0;
	var LANGUAGE = {
		'l0073': "<?php echo $PluginLanguage['Screen']['0073']; ?>"
	};
</script>
<script src="<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/scripts/jquery.js"></script>
<script src="<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/scripts/editor.js?<?php echo time(); ?>"></script>

</body>
</html>