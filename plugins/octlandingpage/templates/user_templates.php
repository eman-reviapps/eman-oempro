<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold" >
                <?php echo $PluginLanguage['Screen']['0003']; ?>        
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" > 
                <a class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/octlandingpage/pages/"><strong><?php echo ($PluginLanguage['Screen']['0015']); ?></strong></a>                    
                <a class="btn default btn-transparen btn-sm" targetform="page-create" id="form-button" href="#"><strong><?php echo $PluginLanguage['Screen']['0047']; ?> &rarr;</strong></a>
            </div>

        </div>
    </div>
</div>
<div class="page-content-inner">
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-6">
            <div class="note note-info note-custom" style="margin-bottom: 0">
                <h4>Select page and then continue to Page Builder</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-body form">

                    <form id="page-create" action="<?php InterfaceAppURL(); ?>/octlandingpage/templates" method="post">
                        <input type="hidden" name="TemplateID" value="">
                        <div class="email-template-gallery-container clearfix">
                            <?php foreach ($Templates as $Each): ?>
                                <div class="email-template <?php echo $Each['TemplateID']; ?>" id="page-template-<?php echo $Each['TemplateID']; ?>">
                                    <?php if ($Each['TemplateTag'] != ''): ?>
                                        <div style="font-weight:bold;position:absolute;background:white;padding:3px 5px;color:#0077cc;top:0;right:0;box-shadow:-2px 2px 8px rgba(0, 119, 204, .4);font-size:10px;line-height:10px;"><?php echo strtoupper($Each['TemplateTag']); ?></div>
                                    <?php endif; ?>
                                    <div class="image">
                                        <?php if ($Each['TemplateThumbnail'] != ''): ?>
                                            <img src="<?php echo InterfaceInstallationURL(true) . 'plugins/octlandingpage/page_templates/' . $Each['TemplateThumbnail']; ?>" />
                                        <?php else: ?>
                                            <img src="<?php InterfaceTemplateURL(); ?>/images/no_thumbnail.png" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="meta">
                                        <?php echo $Each['TemplateName']; ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-2">
                                    <!--<a class="btn default" targetform="page-create" id="form-button" href="#"><strong><?php echo $PluginLanguage['Screen']['0047']; ?> &rarr;</strong></a>-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--    <div class="col-md-4">
        <?php include PLUGIN_PATH . '/octlandingpage/templates/help_templates_' . $LanguageCode . '.php'; ?>
            </div>-->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.email-template').click(function () {
            $('.email-template-gallery-container .email-template.selected').removeClass('selected');
            $(this).addClass('selected');
            $('[name="TemplateID"]').val($(this).attr('id').replace('page-template-', ''));
        });
        if ($('.email-template-gallery-container .email-template.selected').length < 1) {
            $('.email-template:first-child').click();
        }
    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>