<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<div class="row">
    <div class="col-md-8">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-purple-sharp"></i>
                    <span class="caption-subject bold font-purple-sharp"> <?php echo $PluginLanguage['Screen']['0014']; ?> </span>                    
                </div>
                <div class="actions">
                    <a class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/octlandingpage/pages/"><strong><?php echo ($PluginLanguage['Screen']['0015']); ?></strong></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="page-options" action="<?php InterfaceAppURL(); ?>/octlandingpage/customurl/<?php echo $Page->getId(); ?>" method="post">
                    <input type="hidden" name="Command" value="SaveCustomURL" id="Command">

                    <?php if (!empty($PageSuccessMessage)): ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <strong><?php print($PageSuccessMessage); ?></strong>
                        </div>
                    <?php endif; ?>

                    <span class="help-block"><?php echo $PluginLanguage['Screen']['0016']; ?></span>
                    <br/>
                    <div class="form-group clearfix" id="form-row-OriginalURL">
                        <label class="col-md-3 control-label" for="OriginalURL"><?php echo $PluginLanguage['Screen']['0019']; ?>:</label>
                       <div class="col-md-9">
                            <input type="text" readonly value="<?php echo $Page->getOriginalURL(); ?>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group clearfix <?php print((form_error('CustomURL') != '' ? 'error' : '')); ?>" id="form-row-CustomURL">
                        <label class="col-md-3 control-label" for="CustomURL"><?php echo $PluginLanguage['Screen']['0018']; ?>:</label>
                        <div class="col-md-9">
                            <input name="CustomURL" id="CustomURL" type="text" value="<?php echo set_value('CustomURL', $Page->getCustomURL()); ?>" class="form-control" />
                            <?php print(form_error('CustomURL', '<span class="help-block">', '</span>')); ?>
                            <span class="help-block"><?php echo $PluginLanguage['Screen']['0022']; ?></span>                         
                        </div>
                    </div>
                    <?php if ($Page->getCustomURL() != ''): ?>
                        <h4 class="form-section bold font-blue"><?php echo $PluginLanguage['Screen']['0021']; ?></h4>
                        <div class="form-group clearfix">
                            <span class="help-block"><?php echo $PluginLanguage['Screen']['0023']; ?></span>
                            <textarea id="htaccess-snippet" rows="10" class="form-control" style="width:95%" readonly="readonly">RewriteEngine On
                                                        RewriteCond %{REQUEST_URI} ^<?php echo $CustomURLInfo['path']; ?> 
                                                        RewriteRule ^(.*)$ <?php echo $Page->getOriginalURL(); ?> [L,P]</textarea>
                        </div>
                    <?php endif; ?>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-2">
                                <a class="btn default" id="form-button" href="#" targetform="page-options"><strong><?php echo strtoupper($PluginLanguage['Screen']['0017']); ?></strong></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php include PLUGIN_PATH . '/octlandingpage/templates/help_htaccess_' . $LanguageCode . '.php'; ?>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#htaccess-snippet').click(function () {
            $(this).select();
        });
    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>