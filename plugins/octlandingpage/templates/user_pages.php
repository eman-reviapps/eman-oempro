<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.css" rel="stylesheet">
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/bootstrap-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {

        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    if (window.console) {
                        return console.log('onShown event fired');
                    }
                },
                onContentLoaded: function () {
                    if (window.console) {
                        return console.log('onContentLoaded event fired');
                    }
                },
                onNavigate: function (direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });

    });
</script>
<div class="portlet" style="padding-bottom: 0px;margin-bottom: 0px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold" >
                <?php echo $PluginLanguage['Screen']['0002']; ?>                  
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" > 
                <?php if (InterfacePrivilegeCheck('List.Create', $UserInformation)): ?>
                    <a id="button-pages-create-page" class="btn default btn-transparen btn-sm <?php echo $disabled_class?>" href="<?php InterfaceAppURL(); ?>/octlandingpage/templates/"><strong><?php echo ($PluginLanguage['Screen']['0003']); ?></strong></a>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>
<div class="page-content-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-transparent">
                <div class="portlet-body form">
                    <form id="pages-table-form" action="<?php InterfaceAppURL(); ?>/octlandingpage/pages/" method="post">
                        <input type="hidden" name="Command" value="DeletePages" id="Command">
                        <!--                    <div class="row">
                                                <div class="col-md-12">
                                                    <div class="note note-info note-custom" style="margin-bottom: 0">
                                                        <strong>Select page and go to Page Builder</strong>
                                                    </div>
                                                </div>
                                            </div>-->

                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                    <?php
                                elseif (isset($PageErrorMessage) == true):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                    <?php
                                endif;
                                ?>
                                <div class="operations-custom">
                                    <span class="caption-md small bold font-dark"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
                                    <a href="#" class="grid-select-all btn default btn-transparen btn-sm" targetgrid="pages-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                    <a href="#" class="grid-select-none btn default btn-transparen btn-sm" targetgrid="pages-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                    <a href="#" class="main-action btn default btn-transparen btn-sm" targetform="pages-table-form"><?php InterfaceLanguage('Screen', '0042'); ?></a>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="grid items items-custom table" id="pages-table">
    <!--                                    <tr>
                                            <th width="100%" colspan="3"><?php echo strtoupper($PluginLanguage['Screen']['0004']); ?></th>
                                        </tr>-->
                                        <?php
                                        if (count($Pages) < 1 || $Pages === false):
                                            ?>
                                            <tr class="item">
                                                <td colspan="4"><?php echo $PluginLanguage['Screen']['0005']; ?></td>
                                            </tr>
                                            <?php
                                        endif;
                                        foreach ($Pages as $EachPage):
                                            ?>
                                            <tr class="item">
                                                <td width="15" style="vertical-align:top;float:left"><input class="grid-check-element" type="checkbox" name="SelectedPages[]" value="<?php echo $EachPage->getId(); ?>" id="SelectedPages<?php echo $EachPage->getId(); ?>"></td>
                                                <td style="float: left">
                                                    <div class="campaign-details">
                                                        <?php
//                                                        print_r('<pre>');
//                                                        print_r($EachPage->getScreenshot());
//                                                        print_r('</pre>');
                                                        $img_url = ($EachPage->getScreenshot() != '') ? APP_URL . "data/screenshots/" . $EachPage->getScreenshot() : TEMPLATE_URL . "images/no_thumbnail.png";
//                                                       
                                                        ?>
                                                        <a class="preview email-preview" href="<?php echo $img_url ?>" style="float: left; width:120px; height: 105px; background:url(/images/newsletter_screenshot2.png) no-repeat top center"
                                                           data-toggle="lightbox" data-title="Email screenshot" data-footer="">
                                                            <img width="120" height="105" class="img-responsive" src="<?php echo $img_url ?>">
                                                        </a>
                                                        <div class="column-block">                                                        
                                                            <h4>
                                                                <a class="font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/octlandingpage/builder/<?php echo $EachPage->getId(); ?>"><?php echo $EachPage->getTitle(); ?></a>
                                                            </h4>
                                                        </div>
                                                        <!--</div>-->
                                                    </div>
                                                </td>
                                                <td style="float: right" >
                                                    <div class="stats visible-lg visible-md">
                                                        <ul>
                                                            <li style="width:60px">
                                                                <a href="<?php InterfaceAppURL(); ?>/octlandingpage/builder/<?php echo $EachPage->getId(); ?>" class="btn default"><?php echo $PluginLanguage['Screen']['0075']; ?></a>
                                                            </li>
                                                            <li style="width:170px">
                                                                <a href="<?php InterfaceAppURL(); ?>/octlandingpage/customurl/<?php echo $EachPage->getId(); ?>" class="btn default"><?php echo $PluginLanguage['Screen']['0011']; ?></a>
                                                            </li>
                                                            <li>
                                                                <span class="description font-purple-sharp bold ">
                                                                    <a href="<?php echo $EachPage->getURL(); ?>" class="btn default" target="_blank"><?php echo $PluginLanguage['Screen']['0010']; ?></a>
                                                                </span>
                                                            </li>
                                                            <li class="clicked">
                                                                <span class="number ng-binding bold"><?php echo number_format($EachPage->getUniqueVisitCount(), 0); ?></span>
                                                                <span class="description font-purple-sharp bold "><?php echo $PluginLanguage['Screen']['0044']; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--    <div class="col-md-4">
<?php include PLUGIN_PATH . '/octlandingpage/templates/help_landingpage_' . $LanguageCode . '.php'; ?>
            </div>-->
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    var language_object = {
        screen: {
            '0009': '<?php echo $PluginLanguage['Screen']['0009']; ?>'
        }
    };

    $(document).ready(function () {
        $('#pages-table-form').submit(function () {
            if (!confirm(language_object.screen['0009'])) {
                return false;
            }
        });
    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>