<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.$PluginLanguage['Screen']['0001']; ?></title>
</head>
<body>

<div id="page-code-editor">
	<div id="page-code-editor-header">
		<div class="page-editor-header-form-row">
			<label for="tite"><?php echo $PluginLanguage['Screen']['0045']; ?></label>
			<input id="page-editor-title-placeholder" type="text" placeholder="Enter your page title here" style="width:600px;font-weight:bold;">
			<span id="title-placeholder-error" class="page-editor-input-sidekick error"></span>
		</div>
		<div class="button-container">
                    <a href="https://app.flyinglist.com/cp/user/photoeditor/" target="_blank" class="primary"><?php echo $PluginLanguage['Screen']['0076']; ?><span class="loading"><?php echo $PluginLanguage['Screen']['0049']; ?></span><span class="success"><?php echo $PluginLanguage['Screen']['0050']; ?></span><span class="fail"><?php echo $PluginLanguage['Screen']['0051']; ?></span></a>
			<a href="#" class="primary" id="page-editor-button-save"><?php echo $PluginLanguage['Screen']['0048']; ?><span class="loading"><?php echo $PluginLanguage['Screen']['0049']; ?></span><span class="success"><?php echo $PluginLanguage['Screen']['0050']; ?></span><span class="fail"><?php echo $PluginLanguage['Screen']['0051']; ?></span></a>
			<a href="#" class="secondary js-quit-link"><?php echo $PluginLanguage['Screen']['0052']; ?></a>
		</div>
		<div class="error-container" id="page-editor-error-container">
		</div>
	</div>
	<div id="page-code-editor-tab-container">
		<a href="page-code-editor-email-builder"><?php echo $PluginLanguage['Screen']['0053']; ?></a>
		<a href="page-code-editor-preview-content"><?php echo $PluginLanguage['Screen']['0054']; ?></a>
	</div>
	<div id="page-code-editor-email-builder" class="page-code-editor-tab-content">
		<div class="page-code-editor-tab-toolbar">
		</div>
		<div class="page-code-editor-tab-sidebar" id="page-code-editor-builder-blocks-sidebar">
			<div class="page-code-editor-builder-block-folder folder-for-text">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-abc"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-text">
				<h2><?php echo $PluginLanguage['Screen']['0055']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder folder-for-media">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-media"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-media">
				<h2><?php echo $PluginLanguage['Screen']['0057']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder folder-for-buttons">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-buttons"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-buttons">
				<h2><?php echo $PluginLanguage['Screen']['0058']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
			<div class="page-code-editor-builder-block-folder folder-for-other">
				<div class="block-folder-icon block-folder-base"></div>
				<div class="block-folder-icon block-folder-other"></div>
			</div>
			<div class="block-folder-contents" id="page-code-editor-builder-blocks-other">
				<h2><?php echo $PluginLanguage['Screen']['0059']; ?></h2>
				<p><?php echo $PluginLanguage['Screen']['0056']; ?></p>
			</div>
		</div>
		<iframe id="page-code-editor-builder-iframe" src="" frameborder="0"></iframe>
	</div>
	<div id="page-code-editor-preview-content" class="page-code-editor-tab-content">
		<div class="page-code-editor-tab-toolbar">
			<div class="toolbar-button-group">
				<a href="#" class="selected" data-preview-mode="desktop"><?php echo $PluginLanguage['Screen']['0060']; ?></a>
				<a href="#" data-preview-mode="tablet"><?php echo $PluginLanguage['Screen']['0061']; ?></a>
				<a href="#" data-preview-mode="phone"><?php echo $PluginLanguage['Screen']['0062']; ?></a>
			</div>
		</div>
		<iframe id="page-code-editor-preview-iframe" src="about:blank" frameborder="0"></iframe>
		<div id="page-code-editor-preview-iframe-frame"></div>
	</div>
</div>

<div id="dummy-image-editor-popup-trigger" data-form-popup="form-popup-image-editor"></div>
<div id="dummy-block-editor-popup-trigger" data-form-popup="form-popup-block-editor"></div>

<div id="popup-form-content-container" style="display:none">
	<div id="form-popup-block-editor">
		<div style="width:200px;">
			<div id="form-popup-block-editor-contents">
				
			</div>
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-block-options-save"><?php echo $PluginLanguage['Screen']['0063']; ?></button> <?php echo $PluginLanguage['Screen']['0064']; ?> <a href="#" class="form-popup-cancel"><?php echo $PluginLanguage['Screen']['0065']; ?></a>
			</div>
		</div>
	</div>
	<div id="form-popup-image-editor">
		<div style="width:400px;">
			<div class="left" style="width:200px;">
				<div class="form-row">
					<label class="input-label"><?php echo $PluginLanguage['Screen']['0066']; ?></label>
					<input id="email-builder-popup-image-link" type="text" style="width:180px;">
				</div>
			</div>
			<div class="left" style="width:200px;">
				<div class="form-row">
				<label class="input-label"><?php echo $PluginLanguage['Screen']['0067']; ?></label>
				<input id="email-builder-popup-image-alt" type="text" style="width:180px;">
				</div>
			</div>
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-image-editor-save"><?php echo $PluginLanguage['Screen']['0063']; ?></button> <?php echo $PluginLanguage['Screen']['0064']; ?> <a href="#" class="form-popup-cancel"><?php echo $PluginLanguage['Screen']['0065']; ?></a>
			</div>
		</div>
	</div>
	<div id="email-builder-popup-theme-options">
		<div style="width:500px;">
			<div class="form-row no-border last" style="clear:both;">
				<button class="default" id="email-builder-popup-theme-options-close"><?php echo $PluginLanguage['Screen']['0073']; ?></button> <a href="#" class="form-popup-cancel" style="display:none;"><?php echo $PluginLanguage['Screen']['0066']; ?></a>
			</div>
		</div>
	</div>
</div>

<div id="popup-media-library">
	<!--<iframe src="<?php InterfaceAppURL(); ?>/user/medialibrary/browse/0/popup/a/1" frameborder="0"></iframe>-->
	<iframe src="<?php InterfaceAppURL(); ?>/user/medialibrary/browse_simple" frameborder="0"></iframe>
</div>

<form action="" id="editor-form">
	<input type="hidden" name="Command" value="Save">
	<input type="hidden" name="Title" value="<?php echo $PageSession->getTitle(); ?>">
	<input type="hidden" name="PageID" value="<?php echo $PageSession->getId(); ?>">
	<input type="hidden" name="Template" value="<?php echo $PageSession->getTemplateId() != '' ? InterfaceAppURL(true) . '/octlandingpage/builder_template/' . $PageSession->getTemplateId() : ''; ?>">
	<textarea id="page-editor-hidden-html-content" style="display:none"><?php echo is_null($Page) ? '' : htmlentities($Page->getCompiledContent(false)); ?></textarea>
	<textarea id="page-editor-hidden-template-html-content" style="display:none"><?php echo htmlentities($PageSession->getTemplate()); ?></textarea>
</form>

<div id="page-editor-preloader"></div>
<style>
	@import "<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/styles/editor.css?<?php echo time(); ?>";
</style>
<script>
	var APP_URL = '<?php echo InterfaceAppURL(); ?>';
	var PAGE_ID = <?php echo is_null($Page) ? '0' : $Page->getId(); ?>;
	var LANGUAGE = {
		'l0073': "<?php echo $PluginLanguage['Screen']['0073']; ?>",
		'l0074': "<?php echo $PluginLanguage['Screen']['0074']; ?>"
	};
</script>
<script src="<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/scripts/jquery.js"></script>
<script src="<?php echo PLUGIN_URL; ?>/octlandingpage/assets/editor/scripts/editor.js?<?php echo time(); ?>"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/html2canvas/html2canvas.js"></script>

</body>
</html>