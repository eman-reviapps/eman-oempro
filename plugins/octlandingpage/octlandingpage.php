<?php
/**
 * Landing Page Builder
 * Name: Landing Page Builder
 * Description: Build landing pages with drag-and-drop page builder
 * Minimum Oempro Version: 4.6.4
 */

include_once(PLUGIN_PATH . 'octlandingpage/main.php');
