var appStates = { IDLE: 0, ANIMATING: 1, ORDERING: 2, RESIZING: 3, EDITING: 4 };
var globalConfig = {
	animation: { duration: 200 },
	slDebug: false
};
var editorSource, editorPlain, currentEditorInstance,
	dropPlaceholderSelector = '.sl-drop-placeholder',
	dropPlaceholderActiveStateClassName = 'sl-drop-placeholder-active',
	contentBlockSelector = '.sl-content-block',
	$currentlyEditingImage = null, isAnimating = false,
	isResizingABlock = false,
	isDragging = false,
	isIFRAMEloaded = false,
	themeOptions = {},
	draggingBlocks = false,
	draggingBlockHTML = '',
	appState = appStates.IDLE,
	lastSaveTime = new Date().getTime();

window.blockOptionsUpdate = function() {};

/* -----------------------------------------------------------------------
 * --- UTILITY FUNCTIONS
 * ----------------------------------------------------------------------- */
function isExplorer() {
	return /*@cc_on!@*/0;
}
function makeChildrenDropZone($, parentSelector, childrenSelector, effect, options) {
	$(parentSelector).on('dragover', childrenSelector, function(ev) {
		var $dropzone = $(this);
		ev.preventDefault(); // ALLOW TO DROP
		if (options.dragover) {
			options.dragover.call($dropzone, $, ev);
		}
		ev.originalEvent.dataTransfer.dropEffect = effect;
		return false;
	}).on('dragleave', childrenSelector, function(ev) {
		var $dropzone = $(this);
		if (options.dragleave) {
			options.dragleave.call($dropzone, $, ev);
		}
	}).on('drop', childrenSelector, function(ev) {
		var $dropzone = $(this);
		ev.stopPropagation();
		if (options.drop) {
			options.drop.call($dropzone, $, ev);
		}
		ev.preventDefault();
	});

	/* TODO
	if (isExplorer()) {
		// TO GET IE TO WORK
		$(parentSelector).on('dragenter', childrenSelector, function(ev) {
			var $dropzone = $(this);
			$dropzone.addClass('sl-drop-placeholder-active');
		});
	}
	*/
}
function setDraggingBlockHTML(html) {
	draggingBlockHTML = html;
}
function getDraggingBlockHTML(html) {
	return draggingBlockHTML;
}
function setAppState(state) {
	appState = state;
}
function getAppState() {
	return appState;
}
function getBuilderFrameAndScripts() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		jQuery = $builderFrame[0].contentWindow.jQuery,
		ckEditor = $builderFrame[0].contentWindow.CKEDITOR;

	return {
		frame: $builderFrame,
		jQuery: jQuery,
		ckEditor: ckEditor
	};
}
function debugMessage(type, message) {
	if (! globalConfig.slDebug) return;
	console[type](message);
}
function getHTML() {
	// var head = $('#page-code-editor-builder-iframe').contents().find('head').html();
	// head = head.replace(/<!--\sINJECTION\sSTART\s-->(.|\n)*?<!--\sINJECTION\sFINISH\s-->/gi, "");
	// head = head.replace(/<script.*?ckeditor.*?\/script>/gi, "");
	// head = head.replace(/<link.*?ckeditor.*?>/gi, "");

	destroyCKEDITORinstances();

	var body = $('#page-code-editor-builder-iframe').contents().find('body').html();
	body = body.replace(/<!--\sINJECTION\sSTART\s-->(.|\n)*?<!--\sINJECTION\sFINISH\s-->/gi, "");
	// body = body.replace(/\scontenteditable="true"/gi, "");

	// return '<html><head>' + head + '</head><body>' + body + '</body></html>';
	return body;
}


/* -----------------------------------------------------------------------
 * --- FORM POPUP PLUGIN
 * ----------------------------------------------------------------------- */
(function($) {
	$.fn.formPopup = function() {
		var defaultSuccessText = 'SAVED';
		function createFormPopup() {
			var $formPopup = $('<div class="form-popup"><div class="form-popup-arrow"></div><div class="form-popup-contents"></div><div class="form-popup-loading"></div><div class="form-popup-success">' + defaultSuccessText + '</div></div>');
			$('body').append($formPopup);
			$formPopup.on('click', '.form-popup-cancel', function(ev) {
				ev.preventDefault();
				hidePopup();
			});
		}
		function createOverlay() {
			$('body').append('<div class="form-popup-overlay"></div>');
			$('.form-popup-overlay').on('click', function() {
				hidePopup();
				window.mediaLibraryHide();
			});
		}
		function showOverlay() {
			$('.form-popup-overlay').show();
			$('body').css('overflow', 'hidden');
		}
		function hideOverlay() {
			$('.form-popup-overlay').hide();
			$('body').css('overflow', 'auto');
		}
		function showMediaLibrary(callback) {
			if ($('.form-popup-overlay').length < 1) createOverlay();
			showOverlay();
			$('#popup-media-library').show();
			$('#popup-media-library iframe')[0].contentWindow.media_library.set_media = callback;
			$('#popup-media-library iframe').on('load', function() {
				$('#popup-media-library iframe')[0].contentWindow.media_library.set_media = callback;
			});
		}
		function hideMediaLibrary() {
			$('#popup-media-library').hide();
			hideOverlay();
		}
		window.mediaLibraryShow = showMediaLibrary;
		window.mediaLibraryHide = hideMediaLibrary;
		function showPopup(left, top, formContent, arrowOffset) {
			if ($('.form-popup-overlay').length < 1) createOverlay();
			showOverlay();
			$('.form-popup-arrow').show();
			if ($('.form-popup').length < 1) createFormPopup();
			$('.form-popup').css({
				top: top + 'px',
				left: left + 'px'
			}).children('.form-popup-contents').append($('#' + formContent)).end().show();
			$('.form-popup-loading').width($('.form-popup-contents').width());
			$('.form-popup-success').width($('.form-popup-contents').width());
			if (arrowOffset > 0) {
				$('.form-popup-arrow').css('left', (20 + arrowOffset) + 'px');
			} else {
				$('.form-popup-arrow').css('left', '20px');
			}
			var popupBottom = $('.form-popup').offset().top + $('.form-popup').outerHeight(),
				windowHeight = $(window).height();
			if (popupBottom > windowHeight) {
				$('.form-popup').css('top', $('.form-popup').offset().top - (popupBottom - windowHeight) - 20 + 'px');
				$('.form-popup-arrow').hide();
			}
			$('body').trigger('sendloop.formpopup.show', formContent);
		}
		function hidePopup() {
			var formContent = $('.form-popup .form-popup-contents').children().first().attr('id');
			$('body').trigger('sendloop.formpopup.hide', formContent);
			$('#popup-form-content-container').append($('.form-popup .form-popup-contents').children());
			$('.form-popup').hide().removeClass('success');
			hideOverlay();
		}

		function handleClick(ev) {
			ev.preventDefault();
			$(this).blur();
			var pos = $(this).offset(),
				popupContent = $(this).data('form-popup'),
				popupLeft = pos.left - 5,
				popupTop = pos.top + 30,
				popupWidth = $('#' + popupContent + ' > div').outerWidth(),
				popupHeight = $('#' + popupContent + ' > div').outerHeight(),
				arrowOffset = 0;

			if (popupLeft + popupWidth >= $(window).width() - 50) {
				var originalPopupLeft = popupLeft;
				popupLeft += $(window).width() - (popupLeft + popupWidth) - 50;
				arrowOffset = originalPopupLeft - popupLeft;
			}
			if ($(this).data('form-popup-success-text')) {
				$('.form-popup-success').text($(this).data('form-popup-success-text'));
			} else {
				$('.form-popup-success').text(defaultSuccessText);
			}
			showPopup(popupLeft, popupTop, popupContent, arrowOffset);
		}
		return this.each(function() {
			if (this.tagName.toLowerCase() == 'input') {
				$(this).on('focus', handleClick);
			} else {
				$(this).on('click', handleClick);
			}
		});
	};
}(jQuery));
function formPopupToggleLoading() {
	$('.form-popup').toggleClass('loading');
	if ($('.form-popup').hasClass('loading')) {
		$('input, select', $('.form-popup')).attr('disabled', 'disabled');
	} else {
		$('input, select', $('.form-popup')).removeAttr('disabled', 'disabled');
	}
}
function formPopupToggleSuccess() {
	$('.form-popup').addClass('success');
}

/* -----------------------------------------------------------------------
 * --- FORMS
 * ----------------------------------------------------------------------- */
function initTitle() {
	$('#page-editor-title-placeholder').on('change keyup blur', function() {
		$('[name="Title"]').val($(this).val());
	});
}
function initToolbarDropdowns() {
	$('.with-drop > a').on('click', function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		var $dropContainer = $(this).parent(), isAlreadyOpen = false;
		if ($dropContainer.hasClass('open')) isAlreadyOpen = true;
		$('.with-drop').removeClass('open');
		if (isAlreadyOpen) return;
		$dropContainer.toggleClass('open');
		if ($dropContainer.hasClass('open')) {
			$('body').one('click', function(ev) {
				// if ($dropContainer.has(ev.target).length > 0) return;
				$dropContainer.removeClass('open');
			});
		}
	});
}
function initToolbarButtonGroups() {
	$('.toolbar-button-group').on('click', 'a', function(ev) {
		ev.preventDefault();
		$(this).parent().children('.selected').removeClass('selected');
		$(this).addClass('selected');

		var previewMode = $(this).data('preview-mode'),
			$previewFrame = $('#page-code-editor-preview-iframe'),
			$previewFrameFrame = $('#page-code-editor-preview-iframe-frame');
		switch (previewMode) {
			case 'desktop':
				$previewFrame.css('width', '90%').css('left', '5%').css('margin-left', '0');
				$previewFrameFrame.css('width', '92%').css('left', '4%').css('margin-left', '0');
				break;
			case 'tablet':
				$previewFrame.css('width', '768px').css('left', '50%').css('margin-left', '-384px');
				$previewFrameFrame.css('width', '788px').css('left', '50%').css('margin-left', '-394px');
				break;
			case 'phone':
				$previewFrame.css('width', '320px').css('left', '50%').css('margin-left', '-160px');
				$previewFrameFrame.css('width', '342px').css('left', '50%').css('margin-left', '-170px');
				break;
		}
	});
	$('.toolbar-button-group a').first().click();
}
function initTabs() {
	var CKEDITORinstancesDestroyed = true;
	$('#page-code-editor-tab-container').on('click', 'a', function(ev) {
		ev.preventDefault();
		var targetTab = $(this).attr('href');
		$(this).parent().children('a').removeClass('selected');
		$('.page-code-editor-tab-content').hide();
		$(this).addClass('selected');
		$('#' + targetTab).show();

		function initCKEDITOR() {
			if (! CKEDITORinstancesDestroyed) return;
			var $builderFrame = $('#page-code-editor-builder-iframe'),
				builderFrameDoc = $builderFrame.contents();
			setTimeout(function() {
				$('#page-code-editor-builder-iframe')[0].contentWindow.jQuery('[contenteditable="true"]').not('a').ckeditor();
			}, 500);
			CKEDITORinstancesDestroyed = false;
		}

		currentEditorInstance = null;
		if (targetTab == 'page-code-editor-email-builder') {
			if (CKEDITORinstancesDestroyed) {
				if (isIFRAMEloaded) {
					initCKEDITOR();
				} else {
					$('#page-code-editor-builder-iframe').load(function() {
						initCKEDITOR();
					});
				}
			}
		} else if (targetTab == 'page-code-editor-preview-content') {
			$('#page-code-editor-preview-iframe-frame').addClass('loading');
			$.post(APP_URL + '/octlandingpage/builder_preview', {
				'content': getHTML(),
				'template': $('#page-editor-hidden-template-html-content').val()
			}, function(data) {
				if (data.result) {
					$('#page-code-editor-preview-iframe').attr('src', APP_URL + '/octlandingpage/builder_preview');
				}
			});
		}

		$(window).resize();
	});

	$('#page-code-editor-preview-iframe').on('load', function() {
		$('#page-code-editor-preview-iframe-frame').removeClass('loading');
	});
}
function initEditorResizing() {
	$(window).resize(function() {
		var $window = $(this),
			wHeight = $window.height(),
			wWidth = $window.width();
		$('.page-code-editor-tab-content').height(wHeight - 78);
		$('#page-code-editor-preview-iframe').height(wHeight - 154);
		$('#page-code-editor-preview-iframe-frame').height(wHeight - 140);
		$('#page-code-editor-builder-iframe').height(wHeight - 116).width(wWidth - 79);
		$('.page-code-editor-tab-sidebar').height(wHeight - 116);
		$('#popup-media-library').css({
			left: ((wWidth - $('#popup-media-library').outerWidth()) / 2) + 'px',
			top: ((wHeight - $('#popup-media-library').outerHeight()) / 2) + 'px'
		});
	});
}

function initPredefinedFormValues() {
	var title = $('[name="Title"]').val();
	if (title !== '') $('#page-editor-title-placeholder').val(title);
}
function save(ev, isFinal, callback, errorCallback) {
	if (ev) ev.preventDefault();
	var $this = $('#page-editor-button-save');
	$this.addClass('loading');
	var saveData = {
		pageId: $('[name="PageID"]').val(),
		title: $('[name="Title"]').val(),
		// content: $('#page-code-editor-builder-iframe')[0].contentDocument.body.innerHTML,
		content: getHTML(),
		Command: 'Save'
	};
	if (isFinal) {
		saveData.isFinal = true;
	}
//        $('#page-code-editor-builder-iframe')
        
	$('#title-placeholder-error').html('');
	$.post(APP_URL + '/octlandingpage/builder', saveData, function(data) {
		if (data.result) {
			if (data.PageID !== undefined) {
				$('[name="PageID"]').val(data.PageID);
			}
			$this.removeClass('loading').addClass('success');
			setTimeout(function() {
				$this.removeClass('success');
			}, 1000);
			lastSaveTime = new Date().getTime();
			if (callback) {
				callback(data.RedirectURL);
			}
		} else {
			$this.removeClass('loading').addClass('fail');
			if (data.sections) {
				for (var i in data.sections) {
					var error = data.sections[i];
					if (i == 'title') {
						var text = $('#title-placeholder-error').html() === '' ? [] : [$('#title-placeholder-error').html()];
						text.push(data.sections[i]);
						$('#title-placeholder-error').html(text.join(', '));
					}
				}
			} else {
				$('#page-editor-error-container').show().html(data.errorMessage);
			}
			if (errorCallback) {
				errorCallback();
			}
			setTimeout(function() {
				$this.removeClass('fail');
			}, 1000);
		}
	});
}
function initSaveButton() {
	$('#page-editor-button-save').on('click', save);
}

function initBuilderTemplateCssParser() {
	var $builderFrame = $('#page-code-editor-builder-iframe');
	if ($('[name="Template"]').val() === '') {
		$.post(APP_URL + '/octlandingpage/builder_parse_template_stylesheet', {
			template: $('#page-editor-hidden-template-html-content').val()
		}, function(data) {
			buildThemeOptionsForm(data);
		});
	} else {
		$.post(APP_URL + '/octlandingpage/builder_parse_template_stylesheet', {
			templateName: $builderFrame.attr('src').replace(APP_URL + '/octlandingpage/builder_template/', '').split('/')[0]
		}, function(data) {
			buildThemeOptionsForm(data);
		});
	}
}
function updateTemplateStyles() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		builderFrameDoc = $builderFrame.contents()[0];

	var currentStylesheet = $('body style[title="sl-theme-options"]', builderFrameDoc)[0];
	var sheet = currentStylesheet.sheet || currentStylesheet.styleSheet;
	var cssText = '';
	currentStylesheet.innerText = '';
	for (var selector in themeOptions) {
		cssText += selector + '{';
		for (var property in themeOptions[selector]) {
			cssText += property + ':' + themeOptions[selector][property] + ';';
		}
		cssText += '}';
	}
	currentStylesheet.appendChild(builderFrameDoc.createTextNode(cssText));
}
function buildThemeOptionsForm(data) {
	var $saveButton = $('#email-builder-popup-theme-options-close'),
		$saveButtonParent = $saveButton.parent();
		$builderFrame = $('#page-code-editor-builder-iframe'),
		builderFrameDoc = $builderFrame.contents(),
		propertyMap = {'background-color':'color', 'color':'color', 'width': 'size', 'max-width': 'size', 'height': 'size', 'font-family': 'font', 'font-size': 'size', 'line-height': 'size'},
		$container = null,
		parsedCSS = [];

	var $toolbar = $('#page-code-editor-email-builder .page-code-editor-tab-toolbar'),
		groups = {}, $popupMenuContainer = $('#popup-form-content-container');
	$.each(data, function() {
		if ((this.property in propertyMap) === false) return;
		parsedCSS.push(this);
	});
	// ADD TOOLBAR BUTTONS FOR GROUPS
	$.each(parsedCSS, function() {
		if ((this.property in propertyMap) === false) return;
		if (! this.group || this.group === '') this.group = 'Other';
		if (! groups[this.group]) {
			groups[this.group] = {safeLabel:'',items:[]};
			var safeLabel = this.group.toLowerCase().replace(' ', '-');
			groups[this.group].groupSafeLabel = safeLabel;
			var $toolbarButton = $('<a href="#" data-form-popup="email-builder-popup-theme-options-' + safeLabel + '">' + this.group + '</a>');
			$toolbar.append($toolbarButton);
			$toolbarButton.formPopup();
		}
		groups[this.group].items.push(this);
	});
	// ADD MENUS FOR GROUPS
	for (var group in groups) {
		var g = groups[group],
			popupID = 'email-builder-popup-theme-options-' + g.groupSafeLabel,
			$popup = null, $popupInner = null;

		if ($('#' + popupID).length > 0) { // USE THE POPUP
			$popup = $('#' + popupID);
			$popupInner = $('> div', $popup);
		} else { // CREATE POPUP
			$popup = $('<div class="theme-options" id="email-builder-popup-theme-options-' + g.groupSafeLabel + '"></div>');
			$popupInner = $('<div></div>');
			var $submitRow = $('<div class="form-row no-border last"><button class="default form-popup-cancel">' + LANGUAGE.l0073 + '</button>');

			$popup.append($popupInner);
			$popupInner.append($submitRow);
			$popupMenuContainer.append($popup);
		}

		$.each(g.items, function(i) {
			var $row = $('<div class="form-row"></div>'), $input = null,
				updateEvent = '', $rowContainer = null;
			$row.append('<label class="input-label">' + this.label + '</label>');

			var maxRowsInAColumn = g.items.length <= 3 ? 1 : Math.ceil(g.items.length / 3),
				createNewContainer = (i === 0 || i % maxRowsInAColumn === 0);

			if (createNewContainer) {
				$rowContainer = $('<div class="left"></div>');
				$('.form-row.last', $popupInner).before($rowContainer);
			} else {
				$rowContainer = $('.left', $popupInner).last();
			}

			switch (propertyMap[this.property]) {
				case 'file':
					$input = $('<input type="file">');
					updateEvent = 'change';
					break;
				case 'size':
					$input = $('<input type="text">');
					updateEvent = 'keyup';
					break;
				case 'color':
					$input = $('<input type="text" class="sl-color-picker">');
					updateEvent = 'keyup';
					break;
				case 'font':
					$input = $('<input type="text" class="sl-font-picker">');
					updateEvent = 'keyup';
					break;
			}
			if ($input !== null) {
				$input.data('sl-selector', this.selector).data('sl-property', this.property);
				if (propertyMap[this.property] != 'file') {
					$input.val($(this.selector, builderFrameDoc).css(this.property));
					if (! themeOptions[this.selector]) themeOptions[this.selector] = {};
					themeOptions[this.selector][this.property] = $input.val();
				}
				$row.append($input);
				$input.on(updateEvent, function() {
					themeOptions[$(this).data('sl-selector')][$(this).data('sl-property')] = $(this).val();
					updateTemplateStyles();
					// var styleSheet = sheet.add(builderFrameDoc[0]);
					// sheet.addRule(styleSheet, $(this).data('sl-selector'), $(this).data('sl-property') + ': ' + $(this).val() + ';', 1);
					// $($(this).data('sl-selector'), builderFrameDoc).css($(this).data('sl-property'), $(this).val());
				});
			}
			$rowContainer.append($row);
		});

		$popupInner.width($('.left', $popupInner).length * 175);
	}
	initColorPicker();
	initFontPicker();
}
function initFontPicker() {
	$('.sl-font-picker').each(function() {
		var $originalInput = $(this),
			$fontThumbnail = $('<div class="sl-font-picker-thumbnail">Lorem ipsum</div>'),
			$fontPickerWindow = $('<div class="sl-font-picker-window"></div>');

		$originalInput.after($fontThumbnail).hide();
		$fontThumbnail.css('font-family', $originalInput.val()).text($originalInput.val().match(/^[^,]+/i)[0].replace(/["'']/gi, ''));
		$fontThumbnail.after($fontPickerWindow);

		var fonts = [
			'Georgia, serif', '"Palatino Linotype", "Book Antiqua", Palatino, serif', '"Times New Roman", Times, serif',
			'Arial, Helvetica, sans-serif', '"Arial Black", Gadget, sans-serif', '"Comic Sans MS", cursive, sans-serif',
			'Impact, Charcoal, sans-serif', '"Lucida Sans Unicode", "Lucida Grande", sans-serif', 'Tahoma, Geneva, sans-serif',
			'"Trebuchet MS", Helvetica, sans-serif', 'Verdana, Geneva, sans-serif', '"Courier New", Courier, monospace',
			'"Lucida Console", Monaco, monospace'
		];
		$.each(fonts, function() {
			var font = this;
			var $fontOption = $('<div class="font-picker-option">Lorem ipsum</div>');
			$fontOption.css('font-family', font).text(font.match(/^[^,]+/i)[0].replace(/["']/gi, ''));
			$fontPickerWindow.append($fontOption);
		});
		var $fontButton = $('<button>Done</button>');
		$fontPickerWindow.append($fontButton).hide();
		$fontButton.on('click', function() {
			$('body').off('click.fontpickerwindow');
			$fontPickerWindow.hide();
		});
		$fontThumbnail.on('click', function(ev) {
			ev.stopPropagation();
			$('.sl-font-picker-window').hide();
			$fontPickerWindow.show();
			$('body').on('click.fontpickerwindow', function(ev) {
				if ($(ev.target).parents('.sl-font-picker-window').length > 0) return;
				$('body').off('click.fontpickerwindow');
				$fontPickerWindow.hide();
			});
		});
		$('.font-picker-option', $fontPickerWindow).on('click', function() {
			var font = $(this).css('font-family');
			$fontThumbnail.css('font-family', font).text(font.match(/^[^,]+/i)[0].replace(/["'']/gi, ''));
			$originalInput.val(font);
			$originalInput.keyup();
		});
	});
}
function initColorPicker() {
	$('.sl-color-picker').each(function() {
		var $originalInput = $(this),
			$colorPickerThumbnail = $('<div class="sl-color-picker-thumbnail"><div></div></div>'),
			$colorThumbnail = $('div', $colorPickerThumbnail),
			$colorPickerWindow = $('<div class="sl-color-picker-window"></div>');
		$(this).after($colorPickerThumbnail).hide();
		$colorThumbnail.css('background-color', $(this).val());
		$colorPickerThumbnail.after($colorPickerWindow);
		var basicColors = new Array('00', '33', '66', '99', 'CC', 'FF');
		for (i = 0; i < 6; i++) {
			for (j = 0; j < 6; j++) {
				for (k = 0; k < 6; k++) {
					var newColorHex = basicColors[i] + basicColors[j] + basicColors[k];
					$colorPickerWindow.append('<div style="background-color:#' + newColorHex + ';"></div>');
				}
			}
		}
		var $colorInput = $('<input type="text">').val(rgbToHex($(this).val()));
		var $colorButton = $('<button>Done</button>');
		$colorPickerWindow.append($colorInput).append($colorButton).hide();
		$colorButton.on('click', function() {
			$('body').off('click.colorpickerwindow');
			$colorPickerWindow.hide();
		});
		$('div', $colorPickerWindow).on('click', function() {
			var color = $(this).css('background-color');
			var hex = rgbToHex(color);
			$colorThumbnail.css('background-color', hex);
			$colorInput.val(hex);
			$originalInput.val(hex);
			$originalInput.keyup();
		});
		$colorInput.on('keyup', function() {
			$colorThumbnail.css('background-color', $(this).val());
			$originalInput.val($(this).val());
			$originalInput.keyup();
		});
		$colorPickerThumbnail.on('click', function(ev) {
			ev.stopPropagation();
			$('.sl-color-picker-window').hide();
			$colorPickerWindow.show();
			$('body').on('click.colorpickerwindow', function(ev) {
				if ($(ev.target).parents('.sl-color-picker-window').length > 0) return;
				$('body').off('click.colorpickerwindow');
				$colorPickerWindow.hide();
			});
		});
	});
}
function rgbToHex(rgb) {
	if (rgb.indexOf('#') >= 0) return rgb;
	if (! String.prototype.trim) {
		String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
	}
	rgb = rgb.replace('rgb(', '').replace(')', '').split(',');
	var r = rgb[0].trim();
	var g = rgb[1].trim();
	var b = rgb[2].trim();
	rgb = b | (g << 8) | (r << 16);
	return '#' + (0x1000000 + rgb).toString(16).slice(1);
}
function fetchBlocksFromContent() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		builderFrameDoc = $builderFrame.contents(),
		$sidebar = $('#page-code-editor-builder-blocks-sidebar'),
		fetchedBlocks = [];

	$('.page-code-editor-builder-block-folder').on('mouseenter', function() {
		$('.page-code-editor-builder-block-folder').removeClass('hover');
		var $contents = $(this).next();
		$(this).next().show();
		$('.block-folder-contents').not($contents).hide();
	}).on('mouseleave', function() {
		var $folder = $(this);
			$('.page-code-editor-builder-block-folder').removeClass('hover');
	});
	$('body').on('mouseover', function(ev) {
		var $target = $(ev.target);
		var hasFolderParent = $target.parents('.page-code-editor-builder-block-folder').length > 0;
		var hasFolderContent = $target.parents('.block-folder-contents').length > 0;
		var hasFolderClass = $target.hasClass('page-code-editor-builder-block-folder');
		var hasContentClass = $target.hasClass('block-folder-contents');
		if ((! hasFolderParent && ! hasFolderContent) && (! hasFolderClass && ! hasContentClass)) {
			$('.page-code-editor-builder-block-folder').removeClass('hover');
			$('.block-folder-contents').hide();
		}
	});
	$('.block-folder-contents').on('mouseenter', function() {
		$(this).prev().addClass('hover');
	}).on('mouseleave', function() {
		$('.page-code-editor-builder-block-folder').removeClass('hover');
	});

	if ($('[name="Template"]').val() !== '') {
		$('.sl-content-block-hidden', builderFrameDoc).hide();
	}

	if ($('#page-editor-hidden-template-html-content').val() !== '') {
		if ($('[name="Template"]').val() !== '') {
			$('.sl-content-block-hidden', builderFrameDoc).remove();
		}
		builderFrameDoc = $('#page-editor-hidden-template-html-content').val();
	}

	var blocksForText = false, blocksForMedia = false, blocksForButton = false, blocksForOther = false;
	$('.sl-content-block', builderFrameDoc).each(function() {
		var label = $(this).data('block-label'),
			group = $(this).data('block-group');
		if (! label) return;
		if ($.inArray(label, fetchedBlocks) > -1) {
			$(this).remove();
			return;
		}
		fetchedBlocks.push($(this).data('block-label'));
		var $block = $('<div><div class="label-container"></div></div>');
		if (isExplorer()) {
			$block.prepend('<a class="icon-container" href="#"></a>');
		} else {
			$block.prepend('<div class="icon-container"></div>');
		}
		$('.icon-container', $block).data('html', $(this).clone().wrap('<div>').parent().html());
		$('.icon-container', $block).addClass($(this).data('block-icon'));
		$('.label-container', $block).text($(this).data('block-label'));
		if (group == 'text') {
			blocksForText = true;
			$('#page-code-editor-builder-blocks-text').append($block);
		} else if (group == 'media') {
			blocksForMedia = true;
			$('#page-code-editor-builder-blocks-media').append($block);
		} else if (group == 'button') {
			blocksForButton = true;
			$('#page-code-editor-builder-blocks-buttons').append($block);
		} else {
			blocksForOther = true;
			$('#page-code-editor-builder-blocks-other').append($block);
		}

		if ($(this).hasClass('sl-content-block-hidden')) {
			$(this).remove();
		}
	});

	if (blocksForText === false) $('.page-code-editor-builder-block-folder.folder-for-text').hide();
	if (blocksForMedia === false) $('.page-code-editor-builder-block-folder.folder-for-media').hide();
	if (blocksForButton === false) $('.page-code-editor-builder-block-folder.folder-for-buttons').hide();
	if (blocksForOther === false) $('.page-code-editor-builder-block-folder.folder-for-other').hide();
}

function initBuilderDraggableBlocks() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		builderFrameDoc = $builderFrame.contents();

	function displayDropPlaceholders() {
		$(contentBlockSelector, builderFrameDoc).not('.sl-content-block-static').each(function() {
			var $placeholderBefore = $('<div class="sl-drop-placeholder"><div></div></div>', builderFrameDoc),
				$placeholderAfter = $('<div class="sl-drop-placeholder"><div></div></div>', builderFrameDoc);
			$(this).after($placeholderAfter).before($placeholderBefore);
		});
		$(dropPlaceholderSelector, builderFrameDoc).next(dropPlaceholderSelector).remove();
	}
	function hideDropPlaceholders() {
		$(dropPlaceholderSelector, builderFrameDoc).remove();
	}
	$('.page-code-editor-tab-sidebar .icon-container').each(function() {
		var $draggable = $(this);
		$draggable.attr('draggable', 'true');
		$draggable.on('dragstart', function(ev) {
			draggingBlocks = true;
			displayDropPlaceholders();
			ev.originalEvent.dataTransfer.effectAllowed = "copy";
			var html = $(ev.target).data('html');
			ev.originalEvent.dataTransfer.setData('Text', 'builder/block');
			setDraggingBlockHTML(html);
		}).on('dragend', function(ev) {
			draggingBlocks = false;
			hideDropPlaceholders();
		}).on('drag', function() {
			var $container = $(this).parents('.block-folder-contents');
			$container.removeClass('hover').hide().css('opacity', 1);
		});
	});
	document.onselectstart = function(e) { if (draggingBlocks) { e.preventDefault(); return false; } };
}
function initBuilderDroppableFrame(ev) {
	var frameAndScripts = getBuilderFrameAndScripts(),
		jQuery = frameAndScripts.jQuery,
		animSpeed = globalConfig.animation.duration;

	makeChildrenDropZone(jQuery, 'body', '.sl-drop-placeholder', 'copy', {
		dragover: function($) {
			this.addClass('sl-drop-placeholder-active');
		},
		dragleave: function($) {
			this.removeClass('sl-drop-placeholder-active');
		},
		drop: function($, ev) {
			setAppState(appStates.ANIMATING);
			var $html = jQuery(getDraggingBlockHTML()), animHeight = 0;
			if ($html.hasClass('sl-content-block-hidden')) {
				$html.removeClass('sl-content-block-hidden').show();
			}
			// CALCULATE HEIGHT OF THE DROPPED HTML - START
			var $dropbox = jQuery('<div></div>').css({
				position: 'absolute',
				top: '-10000px',
				left: '-10000px'
			});

			jQuery('body').append($dropbox);
			$dropbox.width(this.width()).append($html);
			animHeight = $dropbox.outerHeight();
			// CALCULATE HEIGHT OF THE DROPPED HTML - END
			// ANIMATE AND APPEND HTML - START
			var $animator = jQuery('<div></div>').height(0),
				currentScrollTop = jQuery('body').scrollTop();
			this.after($animator);
			$html.css('opacity', 0);
			$animator.animate({height: animHeight}, { complete: function() {
				jQuery(this).after($html).remove();
				jQuery('[contenteditable="true"]', $html).ckeditor();
				$dropbox.remove();
				$html.animate({opacity: 1}, { complete: function() {
					setAppState(appStates.IDLE);
				}});
			}, duration: animSpeed});
			jQuery('body').animate({scrollTop: currentScrollTop + (animHeight / 2)}, {duration: animSpeed});
			// ANIMATE AND APPEND HTML - END
		}
	});
}
function initBuilderImageDroppableFrame(ev) {
	return;
	/**
	var frameAndScripts = getBuilderFrameAndScripts(),
		jQuery = frameAndScripts.jQuery,
		dropzonePlaceholderClassName = 'sl-content-block-image-drop-zone-placeholder',
		dropzoneTimer = null;

	jQuery('body').data('sl-image-drop-placeholders-created', false);

	makeChildrenDropZone(jQuery, 'body', '', 'copy', {
		dragover: function($, ev) {
			if ($.inArray('Files', ev.originalEvent.dataTransfer.types) < 0) return;

			// CREATE PLACEHOLDERS
			clearTimeout(dropzoneTimer);
			if (jQuery('body').data('sl-image-drop-placeholders-created') === true) return;
			jQuery('img.sl-content-block-image').each(function() {
				var $image = jQuery(this),
					imagePos = $image.offset(),
					$placeholder = jQuery('<div class="' + dropzonePlaceholderClassName + '"></div>');

				$image.after($placeholder);
				$placeholder.css({ top: imagePos.top + 'px', left: imagePos.left + 'px' })
					.width($image.width())
					.height($image.height())
					.data('sl-attached-to', $image);
			});
			jQuery('body').data('sl-image-drop-placeholders-created', true);
		},
		dragleave: function($, ev) {
			if ($.inArray('Files', ev.originalEvent.dataTransfer.types) < 0) return;
			if (jQuery('body').data('sl-image-drop-placeholders-created') === false) return;
			dropzoneTimer = setTimeout(function() {
				// REMOVE PLACEHOLDERS
				if (jQuery('.active').length > 0) return;
				jQuery('.' + dropzonePlaceholderClassName).remove();
				jQuery('body').data('sl-image-drop-placeholders-created', false);
			}, 70);
		},
		drop: function($, ev) {
			if ($.inArray('Files', ev.originalEvent.dataTransfer.types) < 0) return;
			if (jQuery('body').data('sl-image-drop-placeholders-created') === false) return;
			var $target = jQuery(ev.toElement || ev.target);
			if($target.hasClass(dropzonePlaceholderClassName)) return;

			ev.preventDefault();
			ev.stopPropagation();
		}
	});

	makeChildrenDropZone(jQuery, 'body', '.' + dropzonePlaceholderClassName, 'copy', {
		dragover: function($, ev) {
			this.addClass('active');
		},
		dragleave: function($, ev) {
			this.removeClass('active');
		},
		drop: function($, ev) {
			if ($.inArray('Files', ev.originalEvent.dataTransfer.types) < 0) return;

			ev.preventDefault();
			ev.stopImmediatePropagation();
			setAppState(appStates.IDLE);

			var $image = jQuery(ev.toElement || ev.target).data('sl-attached-to');
			var formData = new FormData(),
				files = ev.originalEvent.dataTransfer.files,
				xhr = new XMLHttpRequest();

			formData.append('file', files[0]);
			xhr.upload = function(ev) {
				if (! ev.lengthComputable) return;
				var percent = (ev.loaded / ev.total * 100);
			};
			xhr.onload = function(ev) {
				$image.attr('src', ev.target.responseText);
				jQuery('.' + dropzonePlaceholderClassName).remove();
				jQuery('body').data('sl-image-drop-placeholders-created', false);
			};
			xhr.open('POST', APP_URL + '/octlandingpage/ui_builder_upload');
			xhr.send(formData);
		}
	});
**/
}

/* -----------------------------------------------------------------------
 * --- BLOCK OVERLAY AND BLOCK OVERLAY PLUGINS
 * ----------------------------------------------------------------------- */
function plg_BlockCKEditor(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		ck = frameAndScripts.ckEditor,
		editClassName = 'sl-content-block-edit';

	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight();


		jQuery('[contenteditable]', block).each(function() {
			if ($(this).parents('[contenteditable]').length > 0) return;
			var $edit = $('<div class="' + editClassName + '"></div>'),
				$editable = jQuery(this),
				pos = $editable.offset(),
				width = $editable.outerWidth(),
				height = $editable.outerHeight();
			
			$block.append($edit);

			$edit.css({ top: pos.top + 'px', left: pos.left + 'px' })
				.width(width).height(height);

			var blockOptionsElementArray = $block.data('sl-block-option-elements');
			blockOptionsElementArray.push($edit);
			$block.data('sl-block-option-elements', blockOptionsElementArray);

			$edit.on('click', function(ev) {
				// HIDE OPTIONS OF BLOCK - START
				var blockOptionsElementArray = $block.data('sl-block-option-elements');
				jQuery.map(blockOptionsElementArray, function(el) {
					// if (el.hasClass(overlayClassName)) return;
					// if (el.hasClass(dragHandleClassName)) return;
					// if (el.hasClass(resizeTooltipClassName)) return;
					jQuery(el).hide();
				});
				// HIDE OPTIONS OF BLOCK - END
				setAppState(appStates.EDITING);
				$editable.trigger('focus');
				$editable.ckeditor().editor.once('blur', function() {
					setAppState(appStates.IDLE);
					var evMouseLeave = jQuery.Event('mouseleave');
					evMouseLeave.pageX = 0;
					evMouseLeave.pageY = 0;
					$block.trigger(evMouseLeave);
				});
			});
		});

	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - REMOVE - mouseleave');
		jQuery('.' + editClassName, block).remove();
	});
}
function plg_BlockFrame(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery, className = 'sl-content-block-overlay';
	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight(),
			overlayPadding = 10;
		
		jQuery('.' + className).remove();
		var $blockOverlay = jQuery('<div class="' + className + '"></div>');
		$blockOverlay.width(blockWidth + (overlayPadding * 2))
			.height(blockHeight + (overlayPadding * 2))
			.css({top: blockPos.top - overlayPadding + 'px', left: blockPos.left - overlayPadding + 'px'});

		$block.append($blockOverlay);

		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		blockOptionsElementArray.push($blockOverlay);
		$block.data('sl-block-option-elements', blockOptionsElementArray);

		// if ($block.data('is-ckeditor-active') === true) return;
	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - FRAME - mouseleave');
		jQuery('.' + className, block).remove();
	});
}
function plg_BlockRemove(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		overlayClassName = 'sl-content-block-overlay',
		removeClassName = 'sl-content-block-remove',
		staticClassName = 'sl-content-block-static';
	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight(),
			$overlay = jQuery('.' + overlayClassName, block);

		if ($overlay.length < 1) return;
		if ($block.hasClass(staticClassName)) return;

		var $remove = $('<div class="' + removeClassName + '"></div>');
		$remove.css({top: blockPos.top - 20 + 'px', left: blockPos.left + blockWidth - 15 + 'px'});
		$block.append($remove);

		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		blockOptionsElementArray.push($remove);
		$block.data('sl-block-option-elements', blockOptionsElementArray);

		$remove.on('click', function(ev) {
			destroyCKEDITORinstancesInBlock(block);
			$block.trigger('mouseleave').animate(
				{ opacity: 0 },
				{ complete: function() {
					$(this).animate({ height: 0 }, { complete: function() { $(this).remove(); }});
				}}
			);
		});
	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - REMOVE - mouseleave');
		jQuery('.' + removeClassName, block).remove();
	});
}
function plg_BlockOrder(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		blockClassName = 'sl-content-block',
		staticClassName = 'sl-content-block-static',
		overlayClassName = 'sl-content-block-overlay',
		orderClassName = 'sl-content-block-order',
		placeholderClassName = 'sl-content-block-order-placeholder',
		ghostClassName = 'sl-content-block-order-ghost',
		dummyClassName = 'sl-content-block-order-dummy',
		$activeDummy = null,
		animDur = globalConfig.animation.duration;

	function mousemoveBody(ev) {
		var offsetData = this.data('sl-drag-offset');
		this.css({
			top: ev.pageY - offsetData.y + 'px'
		});
		$activeDummy = null;
		jQuery('.' + dummyClassName).each(function() {
			var $this = jQuery(this);
			$this.removeClass('active');
			var pos = $this.offset();
			if (ev.pageY > pos.top && ev.pageY < pos.top + $this.outerHeight()) $activeDummy = $this;
		});
		if ($activeDummy !== null) $activeDummy.addClass('active');
	}
	function mouseupBody($dragGhost, $overlay, blockWidth, blockHeight, ev) {
		var $block = this,
			animSpeed = animDur;

		function animBlockFadeout() {
			var $fadeoutDiv = jQuery('<div></div>').height($block.height());
			$block.after($fadeoutDiv).hide();
			$fadeoutDiv.animate({ height: 0 }, { duration: animSpeed, complete: function() { jQuery(this).remove(); animBlockFadeInNew(); } });
		}
		function animBlockFadeInNew() {
			$activeDummy.css({ position: 'relative' })
				.children('div').css({ height: 0 })
				.animate( { height: $dragGhost.outerHeight() }, { duration: animSpeed, complete: function() {
					$dragGhost.remove();
					jQuery(this).parent().animate({ opacity: 0 }, { duration: animSpeed, complete: function() {
						jQuery(this).after($block).remove();
						jQuery('.' + dummyClassName).remove();
						$block.show().animate({ opacity: 1}, { duration: animSpeed, complete: function() {
							setAppState(appStates.IDLE);
							var mouseleaveEvent = $.Event('mouseleave');
							mouseleaveEvent.pageX = 0;
							mouseleaveEvent.pageY = 0;
							$activeDummy = null;
							$block.trigger(mouseleaveEvent);
						}});
					}});
				}});
		}

		jQuery('body').off('mousemove.slblockorder');
		setAppState(appStates.ANIMATING);
		jQuery('.' + placeholderClassName).remove();
		jQuery('.' + dummyClassName).not($activeDummy).remove();
		if ($activeDummy !== null) {
			$block.animate({ opacity: 0 }, { duration: animSpeed, complete: animBlockFadeout });
		} else {
			jQuery('.' + ghostClassName).remove();
			setAppState(appStates.IDLE);
		}
	}
	function mousedownOrder($block, $overlay, blockPos, blockWidth, blockHeight, ev) {
		ev.preventDefault();
		setAppState(appStates.ORDERING);
		// CREATE PLACEHOLDER - START
		var $dragPlaceholder = jQuery('<div></div>').addClass(placeholderClassName);
		jQuery('body').append($dragPlaceholder);
		$dragPlaceholder.css({
			top: blockPos.top + 'px',
			left: blockPos.left + 'px'
		}).width(blockWidth).height(blockHeight).data('sl-drag-offset', {
			x: ev.pageX - blockPos.left,
			y: ev.pageY - blockPos.top
		});
		// CREATE PLACEHOLDER - END
		// CREATE GHOST - START
		var $dragGhost = jQuery('<div></div>').addClass(ghostClassName);
		jQuery('body').append($dragGhost);
		$dragGhost.css({
			top: blockPos.top + 'px',
			left: blockPos.left + 'px'
		}).width(blockWidth).height(blockHeight);
		// CREATE GHOST - END
		// HIDE OPTIONS OF BLOCK - START
		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		jQuery.map(blockOptionsElementArray, function(el) {
			jQuery(el).hide();
		});
		// HIDE OPTIONS OF BLOCK - END
		// CREATE ORDER ZONES - START
		jQuery('.' + blockClassName)
			.not('.' + staticClassName).not($block)
			.before('<div class="' + dummyClassName + '"><div></div></div>')
			.after('<div class="' + dummyClassName + '"><div></div></div>');
		$block.next('.' + dummyClassName).remove();
		$block.prev('.' + dummyClassName).remove();
		jQuery('.' + dummyClassName).width(blockWidth);
		// CREATE ORDER ZONES - END
		jQuery('body').on('mousemove.slblockorder', jQuery.proxy(mousemoveBody, $dragPlaceholder));
		jQuery('body').one('mouseup', jQuery.proxy(mouseupBody, $block, $dragGhost, $overlay, blockWidth, blockHeight));
	}
	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight(),
			$overlay = jQuery('.' + overlayClassName, block);

		if ($overlay.length < 1) return;
		if ($block.hasClass(staticClassName)) return;

		var $order = jQuery('<div class="' + orderClassName + '"></div>');
		$order.css({
			top: blockPos.top - 20 + 'px',
			left: blockPos.left + blockWidth - 50 + 'px'
		});
		$block.append($order);

		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		blockOptionsElementArray.push($order);
		$block.data('sl-block-option-elements', blockOptionsElementArray);

		$order.on('mousedown', jQuery.proxy(mousedownOrder, $order, $block, $overlay, blockPos, blockWidth, blockHeight));
	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - ORDER - mouseleave');
		jQuery('.' + orderClassName, block).remove();
	});
}
function plg_BlockOptions(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		staticClassName = 'sl-content-block-static',
		optionsClassName = 'sl-block-edit-button';

	function setupOptionsPopupForm(options, $block) {
		var $formContents = $('#form-popup-block-editor-contents'),
			$inputs = [];

		$formContents.html('').append('<h3>' + options.title + '</h3>');
		for (var i = 0; i < options.options.length; i++) {
			var option = options.options[i],
				$formRow = $('<div class="form-row"></div>'),
				$label = $('<label class="input-label"></label>').text(option.title),
				replaceChunks = option.replace.split('::'),
				$el = jQuery(replaceChunks[0], $block),
				optionsArray = [];
			if (option.type == 'select') {
				$input = $('<select></select>');
				if (option.selectoptions) {
					var optionsForSelect = option.selectoptions.split('!!##!!');
					for (var j = 0; j < optionsForSelect.length; j++) {
						var optionDetail = optionsForSelect[j].split('##!!##');
						$input.append('<option value="' + optionDetail[0] + '">' + optionDetail[1] + '</option>');
						optionsArray.push(optionDetail[0]);
					}
				}
			} else {
				$input = $('<input type="text">');
			}

			if (replaceChunks[1] == 'text') {
				$input.val($el.text());
			} else if (replaceChunks[1].indexOf('attr#') > -1) {
				var attr = replaceChunks[1].split('#')[1];
				if (attr == 'class') {
					for (var k = 0; k < optionsArray.length; k++) {
						if ($el.hasClass(optionsArray[k])) {
							$('option[value="' + optionsArray[k] + '"]', $input).attr('selected', 'selected');
							break;
						}
					}
				} else {
					if (option.template) {
						$input.val($el.attr(attr).replace(option.template.replace('%s', ''), ''));
					} else {
						$input.val($el.attr(attr));
					}
				}
			}
			$input.data('block-option', { block: $block, replace: option.replace, selectoptions: option.selectoptions ? option.selectoptions : false, template: option.template ? option.template : false });
			$formRow.append($label).append($input);
			$formContents.append($formRow);
			$inputs.push($input);
		}
		setupOptionsPopupFormCallback($inputs, $block);
	}
	function setupOptionsPopupFormCallback($inputs, $block) {
		window.blockOptionsUpdate = function() {
			for (var i = 0; i < $inputs.length; i++) {
				var $input = $inputs[i],
					option = $input.data('block-option'),
					replaceChunks = option.replace.split('::'),
					$el = jQuery(replaceChunks[0], $block);
				if (replaceChunks[1] == 'text') {
					$el.text($input.val());
				} else if (replaceChunks[1].indexOf('attr#') > -1) {
					var attr = replaceChunks[1].split('#')[1];
					if (attr == 'class') {
						var optionsForSelect = option.selectoptions.split('!!##!!');
						for (var j = 0; j < optionsForSelect.length; j++) {
							var optionDetail = optionsForSelect[j].split('##!!##');
							$el.removeClass(optionDetail[0]);
						}
						$el.addClass($input.val());
					} else {
						if (option.template !== false) {
							$el.attr(attr, option.template.replace('%s', $input.val()));
						} else {
							$el.attr(attr, $input.val());
						}
					}
				}
			}
			$('#email-builder-popup-block-options-save').next('a').trigger('click');
		};
	}
	function clickBlockEdit($block, ev) {
		ev.preventDefault();
		var $blockEdit = jQuery(this),
			$popupTrigger = $('#dummy-block-editor-popup-trigger'),
			blockEditPos = $blockEdit.offset(),
			framePos = frameAndScripts.frame.offset(),
			frameScrollTop = jQuery('body').scrollTop();

		var options = eval('(' + $block.data('block-options') + ')');
		setupOptionsPopupForm(options, $block);

		$popupTrigger.css({
			top: blockEditPos.top + framePos.top - frameScrollTop + 'px',
			left: blockEditPos.left + framePos.left + 'px'
		}).trigger('click').css({ top: -100, left: -100 });
	}

	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block);
		if (! $block.data('block-options')) return;

		var blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight();

		// if ($block.hasClass(staticClassName)) return;

		var $blockEdit = jQuery('<a class="' + optionsClassName + '"></a>');
		$block.append($blockEdit);
		$blockEdit.css({
			top: blockPos.top + ((blockHeight / 2) - 20) + 'px',
			left: blockPos.left + ((blockWidth / 2) - 20) + 'px'
		});

		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		blockOptionsElementArray.push($blockEdit);
		$block.data('sl-block-option-elements', blockOptionsElementArray);

		$blockEdit.on('click', jQuery.proxy(clickBlockEdit, $blockEdit, $block));
	}).on('blockmouseleave.sl', function(ev, block) {
		jQuery('.' + optionsClassName, block).remove();
	});
	$('#email-builder-popup-block-options-save').on('click', function() {
		debugMessage('log', 'PLG - OPTIONS - mouseleave');
		window.blockOptionsUpdate();
	});
}
function plg_BlockImageOptions(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		imageClass = 'sl-content-block-image';

	function clickButtonUpload(ev) {
		ev.preventDefault();
		var $image = this;
		window.mediaLibraryShow(function(url) {
			$image.attr('src', url);
			$image.parents('.sl-content-block').trigger('mouseleave');
			window.mediaLibraryHide();
		});
		/**
		filepicker.pickAndStore({
			mimetype: 'image/*',
			services: ['COMPUTER']
		}, {
			path: '/user_images/'
		}, function(fpfiles) {
			var fileKey = fpfiles[0].key.replace('//', '/');
			$image.attr('src', 'http://media.myimagerepo.com/' + fileKey);
			$image.parents('.sl-content-block').trigger('mouseleave');
		});
*/
	}
	function clickButtonEdit($button, ev) {
		ev.preventDefault();
		var $image = this,
			$popup = $('#form-popup-image-editor'),
			$popupTrigger = $('#dummy-image-editor-popup-trigger'),
			$inputAlt = $('#email-builder-popup-image-alt'),
			$inputLink = $('#email-builder-popup-image-link'),
			buttonPos = $button.offset(),
			framePos = frameAndScripts.frame.offset(),
			frameScrollTop = jQuery('body').scrollTop();

		$popup.data('sl-editing-image', $image);

		$popupTrigger.css({
			left: buttonPos.left + framePos.left + 'px',
			top: buttonPos.top + framePos.top - frameScrollTop + 'px'
		});

		$inputAlt.val($image.attr('alt'));
		if ($image.parents('a').length > 0) {
			$inputLink.val($image.parents('a').first().attr('href'));
		} else {
			$inputLink.val('');
		}

		$('#dummy-image-editor-popup-trigger').trigger('click');
	}
	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight(),
			$images = jQuery('.' + imageClass, $block);

		$images.each(function() {
			var $image = jQuery(this),
				imagePos = $image.offset(),
				width = $image.outerWidth(),
				height = $image.outerHeight(),
				isSmall = false;
			
			var $buttonEdit = jQuery('<a class="sl-image-edit-button"></a>'),
				$buttonUpload = jQuery('<a class="sl-image-upload-button"></a>'),
				$overlay = jQuery('<div class="sl-image-overlay"></div>');

			if (width <= 150) {
				$buttonEdit.addClass('small');
				$buttonUpload.addClass('small');
				$overlay.addClass('small');
				isSmall = true;
			}

			$block.append($buttonEdit).append($buttonUpload).append($overlay);

			var blockOptionsElementArray = $block.data('sl-block-option-elements');
			blockOptionsElementArray.push($buttonEdit);
			blockOptionsElementArray.push($buttonUpload);
			blockOptionsElementArray.push($overlay);
			$block.data('sl-block-option-elements', blockOptionsElementArray);

			$image.data('sl-image-buttons', [$buttonEdit, $buttonUpload, $overlay]);

			$buttonUpload.css({
				top: imagePos.top + (height / 2) - (isSmall ? 10 : 20) + 'px',
				left: imagePos.left + (width / 2) - (isSmall ? 20 : 40) + 'px'
			}).on('click', jQuery.proxy(clickButtonUpload, $image));

			$buttonEdit.css({
				top: imagePos.top + ((height) / 2) - (isSmall ? 10 : 20) + 'px',
				left: imagePos.left + ((width) / 2) + (isSmall ? 2 : 5) + 'px'
			}).on('click', jQuery.proxy(clickButtonEdit, $image, $buttonEdit));
			$overlay.css({
				top: imagePos.top + 'px',
				left: imagePos.left + 'px'
			}).width(width).height(height);
		});
	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - IMAGE OPTIONS - mouseleave');
		var $block = jQuery(block),
			$images = jQuery('.' + imageClass, $block);
		$images.each(function() {
			var elements = jQuery(this).data('sl-image-buttons');
			jQuery.each(elements, function() {
				jQuery(this).remove();
			});
		});
	});
}
function plg_BlockResize(frameAndScripts) {
	var jQuery = frameAndScripts.jQuery,
		blockClassName = 'sl-content-block',
		blockResizableColumnsClassName = 'sl-content-block-resizable-columns',
		staticClassName = 'sl-content-block-static',
		overlayClassName = 'sl-content-block-overlay',
		dragHandleClassName = 'sl-block-resize-handle',
		resizeTooltipClassName = 'sl-block-resize-tooltip',
		resizeTooltipLeftClassName = 'sl-block-resize-tooltip-left',
		resizeTooltipRightClassName = 'sl-block-resize-tooltip-right';

	function mousedownHandle(ev) {
		// PREVENT DEFAULT EVENT TO DISABLE TEXT SELECTION
		ev.preventDefault();
		ev.stopPropagation();

		// SET APP STATE
		setAppState(appStates.RESIZING);

		var $handle = jQuery(this),
			handlePos = $handle.offset(),
			handleData = $handle.data('sl-target-resizables'),
			usePercentage = $handle.data('sl-resizable-use-percentage'),
			$block = $handle.parents('.sl-content-block');

		// CREATE AND APPEND TOOLTIPS FOR DISPLAYING COLUMN SIZES
		var $sizeTooltipPrev = jQuery('<div></div>').addClass(resizeTooltipClassName).addClass(resizeTooltipLeftClassName),
			$sizeTooltipNext = jQuery('<div></div>').addClass(resizeTooltipClassName).addClass(resizeTooltipRightClassName);
		$block.append($sizeTooltipPrev).append($sizeTooltipNext);
		
		var blockOptionsElementArray = $block.data('sl-block-option-elements');
		blockOptionsElementArray.push($sizeTooltipPrev);
		blockOptionsElementArray.push($sizeTooltipNext);
		$block.data('sl-block-option-elements', blockOptionsElementArray);

		// SETUP HANDLE DATA
		handleData = {
			usePercentage: usePercentage,
			offset: {x: ev.pageX - handlePos.left},
			evX: ev.pageX,
			nextColumn: { el: handleData[1], width: handleData[1].width() },
			prevColumn: { el: handleData[0], width: handleData[0].width() },
			tooltips: { prev: $sizeTooltipPrev, next: $sizeTooltipNext }
		};
		$handle.data('sl-drag-data', handleData);

		// POSITION TOOLTIPS
		$sizeTooltipPrev.css({ left: handlePos.left - 35, top: handlePos.top + 50 }).text(handleData.prevColumn.width + 'px');
		$sizeTooltipNext.css({ left: handlePos.left + 25, top: handlePos.top + 50 }).text(handleData.nextColumn.width + 'px');

		// HIDE OPTIONS OF BLOCK - START
		blockOptionsElementArray = $block.data('sl-block-option-elements');
		jQuery.map(blockOptionsElementArray, function(el) {
			// if (el.hasClass(overlayClassName)) return;
			if (el.hasClass(dragHandleClassName)) return;
			if (el.hasClass(resizeTooltipClassName)) return;
			jQuery(el).hide();
		});
		// HIDE OPTIONS OF BLOCK - END

		// ATTACH MOUSEUP EVENT TO CLEAN UP
		jQuery('body').one('mouseup', jQuery.proxy(mouseupHandle, $handle));
		jQuery('body').on('mousemove.slcontentblockresize', jQuery.proxy(mousemoveHandle, $handle));
	}
	function mousemoveHandle(ev) {
		var $handle = jQuery(this),
			resizeData = $handle.data('sl-drag-data'),
			mouseX = ev.pageX,
			snap = 10;

		var nextNewWidth = resizeData.nextColumn.width - (mouseX - resizeData.evX),
			prevNewWidth = resizeData.prevColumn.width + (mouseX - resizeData.evX);

		if (ev.shiftKey) {
			var nextSnappedWidth = Math.floor(nextNewWidth / snap) * snap,
				snapDiff = nextNewWidth - nextSnappedWidth;

			nextNewWidth = nextSnappedWidth;
			prevNewWidth += snapDiff;
		}

		var stringNextNewWidth = nextNewWidth,
			stringPrevNewWidth = prevNewWidth;
		if (resizeData.usePercentage) {
			var $parentRow = resizeData.nextColumn.el.parents('tr').first();
			if ($parentRow.length < 1) {
				$parentRow = resizeData.nextColumn.el.parents('.column-container').first();
			}
			var parentRowWidth = $parentRow.width();

			var nextColumnPercentage = (nextNewWidth * 100) / parentRowWidth,
				prevColumnPercentage = (prevNewWidth * 100) / parentRowWidth;


			stringNextNewWidth = nextColumnPercentage + '%';
			stringPrevNewWidth = prevColumnPercentage + '%';
		}

		resizeData.nextColumn.el.width(stringNextNewWidth);
		resizeData.prevColumn.el.width(stringPrevNewWidth);

		$handle.css({ left: mouseX - resizeData.offset.x + 'px' });
		resizeData.tooltips.prev.css({ left: mouseX - resizeData.offset.x - 35 + 'px' }).text(prevNewWidth + 'px');
		resizeData.tooltips.next.css({ left: mouseX - resizeData.offset.x + 35 + 'px' }).text(nextNewWidth + 'px');
	}
	function mouseupHandle(ev) {
		var $handle = jQuery(this),
			handlePos = $handle.offset(),
			$block = $handle.parents('.sl-content-block');

		jQuery('body').off('mousemove.slcontentblockresize');

		jQuery('.' + resizeTooltipClassName).remove();

		setAppState(appStates.IDLE);
		var evMouseLeave = jQuery.Event('mouseleave');
		evMouseLeave.pageX = handlePos.left;
		evMouseLeave.pageY = handlePos.top + 50;
		$block.trigger(evMouseLeave);
	}

	jQuery('body').on('blockmouseenter.sl', function(ev, block) {
		var $block = jQuery(block),
			$overlay = jQuery('.' + overlayClassName, block);

		if ($overlay.length < 1) return;
		if ($block.hasClass(staticClassName)) return;
		if (! $block.hasClass(blockResizableColumnsClassName)) return;

		var blockPos = $block.offset(),
			blockWidth = $block.outerWidth(),
			blockHeight = $block.outerHeight(),
			$columns = jQuery('td, .column', $block),
			numberOfColumns = $columns.length,
			$parentRow = $columns.parents('tr').first(),
			rowWidth = $parentRow.width();

		for (var i = 1; i < numberOfColumns; i++) {
			var $currentColumn = jQuery($columns[i]),
				columnWidth = $currentColumn.width(),
				usePercentage = $currentColumn[0].style.width.indexOf('%') !== -1,
				$dragHandle = jQuery('<div class="' + dragHandleClassName + '"><div></div></div>'),
				columnPos = $currentColumn.offset();

			$block.append($dragHandle);
			$dragHandle.data('sl-target-resizables', [jQuery($columns[i - 1]), $currentColumn])
				.data('sl-resizable-use-percentage', usePercentage)
				.data('sl-is-dragging', false)
				.css({ top: columnPos.top - 30 + 'px', left: columnPos.left - 20 + 'px' })
				.children('div')
					.height($block.height() / 2);


			var blockOptionsElementArray = $block.data('sl-block-option-elements');
			blockOptionsElementArray.push($dragHandle);
			$block.data('sl-block-option-elements', blockOptionsElementArray);
		}
	}).on('blockmouseleave.sl', function(ev, block) {
		debugMessage('log', 'PLG - RESIZE - mouseleave');
		jQuery('.' + dragHandleClassName, block).remove();
	});

	jQuery('body').on('mousedown', '.' + dragHandleClassName, mousedownHandle);
}
function initContentBlockOverlayPlugins() {
	var frameAndScripts = getBuilderFrameAndScripts();
	plg_BlockFrame(frameAndScripts);
	plg_BlockRemove(frameAndScripts);
	plg_BlockOrder(frameAndScripts);
	plg_BlockOptions(frameAndScripts);
	plg_BlockImageOptions(frameAndScripts);
	plg_BlockResize(frameAndScripts);
	plg_BlockCKEditor(frameAndScripts);
}
function initContentBlockOverlay() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		jQuery = $builderFrame[0].contentWindow.jQuery,
		builderFrameDoc = $builderFrame.contents(),
		blockActiveClassName = 'sl-content-block-active';

	jQuery('body').on('mouseenter', '.sl-content-block', function() {
		var $block = jQuery(this);
		if (getAppState() !== appStates.IDLE) return;
		if ($block.hasClass(blockActiveClassName)) return;
		jQuery('.sl-content-block').data('sl-block-option-elements', []);
		$block.addClass(blockActiveClassName);
		jQuery('body').trigger('blockmouseenter.sl', [this]);
	}).on('mouseleave', '.sl-content-block', function() {
		var $block = jQuery(this);
		if (getAppState() !== appStates.IDLE) return;
		if (! $block.hasClass(blockActiveClassName)) return;
		jQuery('.sl-content-block').data('sl-block-option-elements', []);
		jQuery('body').trigger('blockmouseleave.sl', [this]);
		$block.removeClass(blockActiveClassName);
	});
}

function destroyCKEDITORinstances() {
	var frameAndScripts = getBuilderFrameAndScripts(),
		ckeditor = frameAndScripts.ckEditor;

	jQuery('[contenteditable="true"]', frameAndScripts.frame.contents().find('body')).each(function() {
		for (var instanceName in ckeditor.instances) {
			if (this === ckeditor.instances[instanceName].element.$) {
				ckeditor.instances[instanceName].destroy();
			}
		}
	});
}
function destroyCKEDITORinstancesInBlock(block) {
	var frameAndScripts = getBuilderFrameAndScripts(),
		ckeditor = frameAndScripts.ckEditor;
	jQuery('[contenteditable="true"]', block).each(function() {
		for (var instanceName in ckeditor.instances) {
			if (this === ckeditor.instances[instanceName].element.$) {
				ckeditor.instances[instanceName].destroy();
			}
		}
	});
}
function initImageEditorPopup() {
	var $popup = $('#form-popup-image-editor'),
		$inputAlt = $('#email-builder-popup-image-alt'),
		$inputLink = $('#email-builder-popup-image-link');

	$('#email-builder-popup-image-editor-save').on('click', function() {
		$(this).next('a').trigger('click');
	});

	$inputLink.on('keyup', function() {
		var val = $(this).val(),
			$currentlyEditingImage = $popup.data('sl-editing-image'),
			$link;
		if (val.length < 1 && $currentlyEditingImage.parents('a').length > 0) {
			$link = $currentlyEditingImage.parents('a').first();
			$link.replaceWith($currentlyEditingImage);
		} else {
			if ($currentlyEditingImage.parents('a').length < 1) {
				$link = $('<a href="#"></a>');
				$currentlyEditingImage.wrap($link);
			} else {
				$link = $currentlyEditingImage.parents('a').first();
			}
			$link.attr('href', val);
		}
	});
	$inputAlt.on('keyup', function() {
		var val = $(this).val(),
			$currentlyEditingImage = $popup.data('sl-editing-image');
		$currentlyEditingImage.attr('alt', val);
	});
}
function initBuilderFrame() {
	var $builderFrame = $('#page-code-editor-builder-iframe'),
		builderFrameDoc = $builderFrame.contents();

	function initAll() {
		fetchBlocksFromContent();
		initBuilderDraggableBlocks();
		initContentBlockOverlay();
		initContentBlockOverlayPlugins();
		initBuilderDroppableFrame();
		initBuilderTemplateCssParser();
		if (!!window.FileReader) {
			initBuilderImageDroppableFrame();
		}
		hidePreloader();
	}
	if ($('body', builderFrameDoc).children().length > 0) {
		initAll();
	} else {
		$('#page-code-editor-builder-iframe').load(initAll);
	}
}

function hidePreloader() {
	$('#page-editor-preloader').animate({ opacity: 0 }, { complete: function() {
		$(this).remove();
	}, duration: 700 });
}

$(document).ready(function() {
	var isTemplate = true;

	// filepicker.setKey('A6AU1uvZEQ1W061ijnexzz');
	$('[data-form-popup]').formPopup();
	initToolbarButtonGroups();
	initTabs();
	initToolbarDropdowns();
	initTitle();
	
	initEditorResizing();
	initSaveButton();
	initPredefinedFormValues();

	$('[href="page-code-editor-html-code"]').hide();
	initBuilderFrame();
	initImageEditorPopup();
	$('#page-code-editor-builder-iframe').load(function() { isIFRAMEloaded = true; });

	var template = $('[name="Template"]').val();
	if (template === '') {
		$('#page-code-editor-builder-iframe').attr('src', APP_URL + '/octlandingpage/builder_compiled_content/' + PAGE_ID);
	} else {
		$('#page-code-editor-builder-iframe').attr('src', $('[name="Template"]').val());
	}

	$('#page-code-editor-tab-container a:visible').first().click();

	$('.js-quit-link').click(function(ev) {
		ev.preventDefault();
		var currentTime = new Date().getTime(),
			diff = currentTime - lastSaveTime,
			result;

		if ((currentTime - lastSaveTime) > 5000) {
			result = confirm("Are you sure? If you haven't saved your work, it will be lost!");
		} else {
			result = true;
		}

		if (result === false) {
			return false;
		} else {
			window.onbeforeunload = function() {
				return null;
			};
			window.location.href = APP_URL + "/octlandingpage/pages";
		}
	});

	$('.editor-link-ignore').on('click', function(ev) { ev.preventDefault(); return false; });

	$(window).resize();

	/**
	window.onbeforeunload = function() {
		var currentTime = new Date().getTime(),
		diff = currentTime - lastSaveTime;

		if ((currentTime - lastSaveTime) > 5000) {
			return "If you haven't saved your work, you will loose all the changes!";
		}
	}
	*/
});