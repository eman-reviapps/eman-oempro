<?php

/**
 * Main language configuration file for the plug-in
 * */
// Important: Do not change this line! - Start
$ArrayPlugInLanguageStrings = array();
// Important: Do not change this line! - End

$ArrayPlugInLanguageStrings['Screen']['0001'] = 'User Register';
$ArrayPlugInLanguageStrings['Screen']['0002'] = 'Register';
$ArrayPlugInLanguageStrings['Screen']['0003'] = 'Upgrade';
$ArrayPlugInLanguageStrings['Screen']['0004'] = 'User was not found';
$ArrayPlugInLanguageStrings['Screen']['0005'] = 'Invoices';
$ArrayPlugInLanguageStrings['Screen']['0006'] = 'Invoice';
$ArrayPlugInLanguageStrings['Screen']['0007'] = 'Pay';
$ArrayPlugInLanguageStrings['Screen']['0008'] = 'Back to invoices';
$ArrayPlugInLanguageStrings['Screen']['0009'] = 'Choose a plan';
$ArrayPlugInLanguageStrings['Screen']['0010'] = 'Free';
$ArrayPlugInLanguageStrings['Screen']['0011'] = 'Pay As You Go';
$ArrayPlugInLanguageStrings['Screen']['0012'] = 'Monthly plan';
$ArrayPlugInLanguageStrings['Screen']['0013'] = 'Annually plan';
$ArrayPlugInLanguageStrings['Screen']['0014'] = 'Upgrade';
$ArrayPlugInLanguageStrings['Screen']['0015'] = 'Up to ';
$ArrayPlugInLanguageStrings['Screen']['0016'] = 'subscribers';
$ArrayPlugInLanguageStrings['Screen']['0017'] = 'emails';
$ArrayPlugInLanguageStrings['Screen']['0018'] = 'per month';
$ArrayPlugInLanguageStrings['Screen']['0019'] = 'per year';
$ArrayPlugInLanguageStrings['Screen']['0020'] = 'per email';
$ArrayPlugInLanguageStrings['Screen']['0021'] = 'Upgrade to this';
$ArrayPlugInLanguageStrings['Screen']['0022'] = 'Failed to upgrade';
$ArrayPlugInLanguageStrings['Screen']['0023'] = 'Failed. Total current subscribers is more than choosen package allowed sbscribers';

$ArrayPlugInLanguageStrings['Screen']['0024'] = 'Serial';
$ArrayPlugInLanguageStrings['Screen']['0025'] = 'Period fro';
$ArrayPlugInLanguageStrings['Screen']['0026'] = 'Period to';
$ArrayPlugInLanguageStrings['Screen']['0027'] = 'Amount';
$ArrayPlugInLanguageStrings['Screen']['0028'] = 'Status';

$ArrayPlugInLanguageStrings['Screen']['0029'] = 'Checkout';
$ArrayPlugInLanguageStrings['Screen']['0030'] = 'Checkout Information';
$ArrayPlugInLanguageStrings['Screen']['0031'] = 'Upgrade done successfully';
$ArrayPlugInLanguageStrings['Screen']['0032'] = 'Your current balance';

$ArrayPlugInLanguageStrings['Common']['0001'] = 'Required parameters are missing ';
$ArrayPlugInLanguageStrings['Common']['0002'] = 'User group updated successfully ';

$ArrayPlugInLanguageStrings['Common']['99997'] = 'This is not an ESP Oempro license';
$ArrayPlugInLanguageStrings['Common']['99998'] = 'Authentication failure or session expired';
$ArrayPlugInLanguageStrings['Common']['99999'] = 'Not enough privileges';
$ArrayPlugInLanguageStrings['Common']['10000'] = 'Internal error occurred. Failed to connect to system';

$ArrayPlugInLanguageStrings['Admin.Login']['1'] = 'Username is missing';
$ArrayPlugInLanguageStrings['Admin.Login']['2'] = 'Password is missing';
$ArrayPlugInLanguageStrings['Admin.Login']['3'] = 'Invalid login information';
$ArrayPlugInLanguageStrings['Admin.Login']['4'] = 'Invalid image verification';
$ArrayPlugInLanguageStrings['Admin.Login']['5'] = 'Image verification failed';


$ArrayPlugInLanguageStrings['User.Create']['1'] = 'Missing user group ID';
$ArrayPlugInLanguageStrings['User.Create']['2'] = 'Missing email address';
$ArrayPlugInLanguageStrings['User.Create']['3'] = 'Username is missing';
$ArrayPlugInLanguageStrings['User.Create']['4'] = 'Password is missing';
$ArrayPlugInLanguageStrings['User.Create']['5'] = 'Reputation level is missing';
$ArrayPlugInLanguageStrings['User.Create']['6'] = 'Missing first name';
$ArrayPlugInLanguageStrings['User.Create']['7'] = 'Missing last name';
$ArrayPlugInLanguageStrings['User.Create']['8'] = 'Missing time zone';
$ArrayPlugInLanguageStrings['User.Create']['9'] = 'Missing language';
$ArrayPlugInLanguageStrings['User.Create']['10'] = 'Invalid email address';
$ArrayPlugInLanguageStrings['User.Create']['11'] = 'Invalid user group ID';
$ArrayPlugInLanguageStrings['User.Create']['12'] = 'Username is already registered';
$ArrayPlugInLanguageStrings['User.Create']['13'] = 'Email address is already registered';
$ArrayPlugInLanguageStrings['User.Create']['14'] = 'Invalid language. Not supported.';
$ArrayPlugInLanguageStrings['User.Create']['15'] = 'Invalid reputation level';
$ArrayPlugInLanguageStrings['User.Create']['16'] = 'Maximum allowed user accounts in your license exceeded';


$ArrayPlugInLanguageStrings['User.Login']['1'] = 'Username is missing';
$ArrayPlugInLanguageStrings['User.Login']['2'] = 'Password is missing';
$ArrayPlugInLanguageStrings['User.Login']['3'] = 'Invalid login information';
$ArrayPlugInLanguageStrings['User.Login']['4'] = 'Invalid image verification';
$ArrayPlugInLanguageStrings['User.Login']['5'] = 'Image verification failed';

$ArrayPlugInLanguageStrings['user.paymentperiods.update']['1'] = 'User ID is missing';
$ArrayPlugInLanguageStrings['user.paymentperiods.update']['2'] = 'Invalid user information';
$ArrayPlugInLanguageStrings['user.paymentperiods.update']['3'] = 'Log ID is missing';
$ArrayPlugInLanguageStrings['user.paymentperiods.update']['4'] = 'Invalid payment period (log ID)';
$ArrayPlugInLanguageStrings['user.paymentperiods.update']['5'] = 'Invalid include tax parameter';
$ArrayPlugInLanguageStrings['user.paymentperiods.update']['6'] = 'Invalid payment status';
?>