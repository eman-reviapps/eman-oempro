var InstallDB = function () {

    var handleInstallDB = function () {

        jQuery("#installForm").validate({
            rules: {
                host: {
                    required: true
                },
                database_name: {
                    required: true
                },
                user_name: {
                    required: true
                }
            },
            messages: {
                host: {
                    required: "Host name is required."
                },
                database_name: {
                    required: "Database name is required."
                },
                user_name: {
                    required: "Username  is required."
                }
            }
        });
        jQuery('#btnInstallDb').click(function () {
            jQuery("#installForm").validate();
            var jQueryform = jQuery("#installForm");
            // check if the input is valid
            if (!jQueryform.valid())
            {
                return false;
            }
            jQuery('#loading').html(url);
            jQuery("#installForm").submit();
        });

    }


    return {
        //main function to initiate the module
        init: function () {

            handleInstallDB();

        }

    };

}();

jQuery(document).ready(function () {
    InstallDB.init();
});