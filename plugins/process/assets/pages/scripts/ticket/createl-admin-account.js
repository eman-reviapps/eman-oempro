var CreateAdminAccount = function () {

    var handleCreateAdminAccount = function () {

        jQuery("#installAdmin").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: "Email is required.",
                    email: "Email is not valid."
                },
                password: {
                    required: "Password is required.",
                    minlength: "Password must be at least 6 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    minlength: "Password confirmation must be at least 6 characters.",
                    equalTo: "Password confirmation didnt match password"
                }
            }
        });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleCreateAdminAccount();

        }

    };

}();

jQuery(document).ready(function () {
    CreateAdminAccount.init();
});