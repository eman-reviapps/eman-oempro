<?php

class Validation {

    private static $instance = null;

    public static function get() {

        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        
    }

    public function verifyRequiredParams($req_params, $req_method) {
        $error = false;
        $error_fields = "";
        foreach ($req_params as $param) {
            if (!isset($req_method[$param]) || strlen(trim($req_method[$param])) <= 0) {
                $error = true;
                $error_fields .= $param . ', ';
            }
        }
        if ($error) {
            return array(false, $error_fields);
        } else {
            return array(true);;
        }
    }

}
