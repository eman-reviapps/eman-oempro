<?php

class Curl {

    public function __construct() {
       
    }

    public function execute($URL, $ArrayPostParameters, $HTTPRequestType = 'POST', $HTTPAuth = false, $HTTPAuthUsername = '', $HTTPAuthPassword = '', $ConnectTimeOutSeconds = 60, $ReturnHeaders = false) {
        $PostParameters = implode('&', $ArrayPostParameters);

        $CurlHandler = curl_init();
        curl_setopt($CurlHandler, CURLOPT_URL, $URL);

        if ($HTTPRequestType == 'GET') {
            curl_setopt($CurlHandler, CURLOPT_HTTPGET, true);
        } elseif ($HTTPRequestType == 'PUT') {
            curl_setopt($CurlHandler, CURLOPT_PUT, true);
        } elseif ($HTTPRequestType == 'DELETE') {
            curl_setopt($CurlHandler, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($CurlHandler, CURLOPT_POST, true);
            curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $PostParameters);
        } else {
            curl_setopt($CurlHandler, CURLOPT_POST, true);
            curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $PostParameters);
        }

        curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CurlHandler, CURLOPT_CONNECTTIMEOUT, $ConnectTimeOutSeconds);
        curl_setopt($CurlHandler, CURLOPT_TIMEOUT, $ConnectTimeOutSeconds);
        curl_setopt($CurlHandler, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
        curl_setopt($CurlHandler, CURLOPT_SSL_VERIFYPEER, false);

        // The option doesn't work with safe mode or when open_basedir is set.
        if ((ini_get('safe_mode') != false) && (ini_get('open_basedir') != false)) {
            curl_setopt($CurlHandler, CURLOPT_FOLLOWLOCATION, true);
        }

        if ($ReturnHeaders == true) {
            curl_setopt($CurlHandler, CURLOPT_HEADER, true);
        } else {
            curl_setopt($CurlHandler, CURLOPT_HEADER, false);
        }

        if ($HTTPAuth == true) {
            curl_setopt($CurlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($CurlHandler, CURLOPT_USERPWD, $HTTPAuthUsername . ':' . $HTTPAuthPassword);
        }

        $RemoteContent = curl_exec($CurlHandler);

        if (curl_error($CurlHandler) != '') {
            return array(false, curl_error($CurlHandler));
        }

        curl_close($CurlHandler);

        return array(true, $RemoteContent);
    }

}
