<?php

class Output {

    public static function getOutputMessage($command, $code) {
        if ($code == 10000 || $code == 99999 || $code == 99998) {
            return trans("api-codes." . $code);
        } else {
            return trans("api-codes." . $command . "." . $code);
        }
    }

}
