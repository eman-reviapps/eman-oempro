<?php

class AdminAPI extends OemproAPI {

    public function __construct() {
        parent::__construct();
    }

    public function login() {

        $this->setCommand("Admin.Login");

        $parameters = array(
            'Command=Admin.Login',
            'ResponseFormat=JSON',
            'Username=' . Oempro::$ADMIN_USER_NAME,
            'Password=' . Oempro::$ADMIN_PASSWORD,
            'DisableCaptcha=true'
        );
        $response = $this->curl->execute(Oempro::$OEMPRO_URL . '/api.php', $parameters, 'POST', false, '', '', 30, false);

        if ($response[0] == false) {
            return array(false, 10000);
        }

        $response = json_decode($response[1]);

        if ($response->Success == false) {
            return array(false, $response->ErrorCode);
        }

        if (isset($response->SessionID) == false || $response->SessionID == '') {
            return array(false, 10000);
        }

        return array(true, $response->SessionID); //admin session id
    }

}
