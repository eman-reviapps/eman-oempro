<?php

class UsersAPI extends OemproAPI {

    public function __construct() {
        parent::__construct();
    }

    public function createUser($data) {

        $this->setCommand("User.Create");

        $parameters = array(
            'Command=User.Create',
            'ResponseFormat=JSON',
            'SessionID=' . $this->admin_session_id,
            'RelUserGroupID=' . $data['package'],
            'EmailAddress=' . $data['email'],
            'Username=' . $data['user_name'],
            'Password=' . $data['password'],
            'FirstName=' . $data['first_name'],
            'LastName=' . $data['last_name'],
            'TimeZone=(GMT) London',
            'Language=' . $data['language'],
            'ReputationLevel=' . $data['reputation_level'],
            'CompanyName=' . $data['organization_name'],
            'Website=',
            'Street=',
            'City=',
            'State=',
            'Zip=',
            'Country=',
            'Phone=' . $data['phone'],
            'Fax=',
            'PreviewMyEmailAccount=',
            'PreviewMyEmailAPIKey=',
            'AvailableCredits=0',
        );

        $response = $this->curl->execute(Oempro::$OEMPRO_URL . '/api.php', $parameters, 'POST', false, '', '', 30, false);

        if ($response[0] == false) {
            return array(false, 10000);
        }

        $response = json_decode($response[1]);

        if ($response->Success == false) {
            return array(false, $response->ErrorCode);
        }

        return array(true, $response->UserID);
    }

    public function loginUser($data) {

        $this->setCommand("User.Login");

        $parameters = array(
            'Command=User.Login',
            'ResponseFormat=JSON',
            'Username=' . $data['user_name'],
            'Password=' . $data['password'],
            'RememberMe=' . $data['remember_me'],
            'DisableCaptcha=' . $data['disable_captcha'],
        );

        $response = $this->curl->execute(Oempro::$OEMPRO_URL . '/api.php', $parameters, 'POST', false, '', '', 30, false);

        if ($response[0] == false) {
            return array(false, 10000);
        }

        $response = json_decode($response[1]);

        if ($response->Success == false) {
            return array(false, $response->ErrorCode);
        }

        return array(true, $response->UserInfo, $response->SessionID);
    }

}
