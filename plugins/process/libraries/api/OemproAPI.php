<?php

class OemproAPI {

    protected $curl;
    protected $command;
    protected $admin_session_id;

    function __construct() {
        $this->curl = new Curl();
    }

    public function getCommand() {
        return $this->command;
    }

    protected function setCommand($command) {
        $this->command = $command;
    }

    protected function getAdminSessionId() {
        return $this->admin_session_id;
    }

    public function setAdminSessionId($admin_session_id) {
        $this->admin_session_id = $admin_session_id;
    }

    protected function getCurl() {
        return $this->curl;
    }

    protected function setCurl($curl) {
        $this->curl = $curl;
    }

}
