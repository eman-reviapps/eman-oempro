<?php

class UserGroupsAPI extends OemproAPI {

    public function __construct() {
        parent::__construct();
    }

    public function getUserGroups() {

        $this->setCommand("UserGroups.Get");

        $parameters = array(
            'Command=UserGroups.Get',
            'ResponseFormat=JSON',
            'SessionID=' . $this->admin_session_id
        );

        $response = $this->curl->execute(Oempro::$OEMPRO_URL . '/api.php', $parameters, 'POST', false, '', '', 30, false);

        if ($response[0] == false) {
            return array(false, 10000);
        }

        $response = json_decode($response[1]);

        if ($response->Success == false) {
            return array(false, $response->ErrorCode);
        }
        return array(true, $response->UserGroups);
    }

}
