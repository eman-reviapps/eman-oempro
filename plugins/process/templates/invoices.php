<?php
include_once(PLUGIN_PATH . 'process/libraries/helpers/Input.php');
include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php');
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-purple-sharp"> <?php echo $PluginLanguage['Screen']['0005']; ?> </span>                    
                </div>
                 <div class="actions"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th><?php echo $PluginLanguage['Screen']['0024']; ?></th>
                            <th><?php echo $PluginLanguage['Screen']['0025']; ?></th>
                            <th><?php echo $PluginLanguage['Screen']['0026']; ?></th>
                            <th><?php echo $PluginLanguage['Screen']['0027']; ?></th>
                            <th><?php echo $PluginLanguage['Screen']['0028']; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($Periods as $EachPeriod):
                            ?>
                            <tr>
                                <td ><a href="<?php echo InterfaceAppURL(true) . "/" . $PluginCode . '/invoice/' . $UserInformation['UserID'] . '/' . $EachPeriod['LogID']; ?>"><?php print($EachPeriod['InvoiceSerial']); ?></a></td>
                                <td ><a href="<?php echo InterfaceAppURL(true) . "/" . $PluginCode . '/invoice/' . $UserInformation['UserID'] . '/' . $EachPeriod['LogID']; ?>"><?php print(date('jS, F Y', strtotime($EachPeriod['PeriodStartDate']))); ?></a></td>
                                <td ><a href="<?php echo InterfaceAppURL(true) . "/" . $PluginCode . '/invoice/' . $UserInformation['UserID'] . '/' . $EachPeriod['LogID']; ?>"><?php print(date('jS, F Y', strtotime($EachPeriod['PeriodEndDate']))); ?></a></td>
                                <td><span class="data"><?php print(Core::FormatCurrency($EachPeriod['TotalAmount'])); ?></span></td>
                                <td>                                    
                                    <span class="label label-sm <?php echo $EachPeriod['PaymentStatus'] == 'Paid' ? 'label-success' : 'label-danger' ?> "> <?php InterfaceLanguage('Screen', '0639', false, $EachPeriod['PaymentStatus'], false, false, array()); ?> </span>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
//include_once(PLUGIN_PATH . 'process/templates/layouts/scripts.php');

//page scripts here
include_once(PLUGIN_PATH . 'process/templates/javascript/global-vars.php');
//include_once(PLUGIN_PATH . 'process/templates/javascript/register.php');

