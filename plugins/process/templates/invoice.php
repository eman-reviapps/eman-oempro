<?php
//include_once(TEMPLATE_PATH . 'desktop/layouts/user_invoice_header.php');

include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php');
?>
<link rel="stylesheet" href="<?php InterfaceAppURL(); ?>/admin/css" type="text/css" media="screen, projection, print" />
<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/invoice.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="<?php InterfaceTemplateURL(); ?>styles/invoice_print.css" type="text/css" media="print"/>

<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/invoice.css" rel="stylesheet" type="text/css" />
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/html2canvas/html2canvas.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include_once(TEMPLATE_PATH . 'desktop/user/settings_navigation.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light">
                            <div class="portlet-body" >
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light">
                                            <div class="portlet-title hidden-print ">
                                                <div class="caption" style="padding: 0;margin-top: -40px;"> 
                                                    <script language="JavaScript" type="text/javascript" >
                                                        TrustLogo("https://flyinglist.com/comodo_secure_seal_100x85_transp.png", "CL1", "none");
                                                    </script>
                                                    <!--<a href="https://ssl.comodo.com" id="comodoTL">SSL Certificates</a>-->
                                                </div>
                                                <div class="actions">
                                                    <a id="print-button" class="btn default btn-transparen btn-sm hidden-print margin-bottom-5">
                                                        <strong>
                                                            <?php InterfaceLanguage('Screen', '0643', false, '', false); ?>
                                                        </strong>
                                                    </a>
                                                    <a id="print-pdf" class="btn default btn-transparen btn-sm hidden-print margin-bottom-5">
                                                        PDF
                                                    </a>
                                                    <?php
                                                    if ($Period['PaymentStatus'] != 'Paid') {
                                                        ?>
                                                        <a href="<?php echo $PayLink ?>" class="btn default btn-transparen btn-sm hidden-print margin-bottom-5">
                                                            <strong>
                                                                <?php echo $PluginLanguage['Screen']['0007']; ?>
                                                            </strong>
                                                        </a>
                                                        <?php
                                                    }
                                                    ?>
                                                    <a href="<?php echo InterfaceAppURL(true); ?>/<?php echo $PluginCode ?>/invoices" class="btn default btn-transparen btn-sm hidden-print margin-bottom-5">
                                                        <strong><?php echo $PluginLanguage['Screen']['0008']; ?></strong>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body  col-md-8" id="to_pdf">
                                                <div class="invoice">
                                                    <div class="row invoice-logo" style="padding-bottom: 10px">
                                                        <div class="col-xs-6 invoice-logo-space">
                                                            <img style="width: 170px;" src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/img/logo-flying.png" alt="logo" class="img-responsive" > </div>
                                                        <div class="col-xs-6" >
                                                            <?php // print_r($Period)  ?>
                                                            <h3 class="bold"><?php InterfaceLanguage('Screen', '0641', false, '', false); ?></h3>
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <strong>Invoice No:</strong> <?php echo $Period['InvoiceSerial'] ?> </li>
                                                                <li>
                                                                    <strong>Period:</strong> From <?php print(date('jS, F Y', strtotime($Period['PeriodStartDate'])) . ' To ' . date('jS, F Y', strtotime($Period['PeriodEndDate']))); ?> 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row" style="padding-top: 10px;padding-bottom: 10px">
                                                        <div class="col-xs-6">
                                                            <h3 class="bold">Seller</h3>
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    FLYING LIST PAZARLAMA IÇ VE DIŞ TIC. LTD. ŞTI.
                                                                <li>
                                                                    <strong>Address:</strong> 
                                                                    Mimar Sinan Mah. Orhangazi St.
                                                                    <br/>Yesil Belde Kent Sitesi A11 Blok 27 E 18 Cekmekoy /Istanbul, Turkey
                                                                </li>
                                                                <br/>
                                                                <li><strong>Email :</strong><a href="mailto:#"> info@flyinglist.com </a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <h3 class="bold">Buyer</h3>
                                                            <ul class="list-unstyled">
                                                                <li> <?php print($User['FirstName'] . ' ' . $User['LastName']); ?> 

                                                                    <?php
                                                                    if ($User['CompanyName'] != '') {
                                                                        ?>
                                                                        <?php echo '/ ' . $User['CompanyName'] ?> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </li>
                                                                <li> 
                                                                    <strong>Address:</strong> 
                                                                    <?php print($User['Street']); ?> 
                                                                    <br/>
                                                                    <?php
                                                                    echo '/' . $User['City'];
                                                                    if ($User['Country'] != '') {
                                                                        echo ', ';
                                                                        InterfaceLanguage('Screen', '0036', false, $User['Country'], false, false);
                                                                    }
                                                                    ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class=""> Description </th>
                                                                        <th class=""> </th>
                                                                        <th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if ($User['GroupInformation']['CreditSystem'] == 'Disabled'): ?>
                                                                        <tr>
                                                                            <td width="280"><?php InterfaceLanguage('Screen', '0648', false, '', false, true); ?></td>
                                                                            <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['CampaignsTotalRecipients'])); ?></span></td>
                                                                            <td width="260"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="280"><?php InterfaceLanguage('Screen', '0649', false, '', false, true); ?></td>
                                                                            <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['AutoRespondersSent'])); ?></span></td>
                                                                            <td width="260"></td>
                                                                        </tr>
                                                                    <?php endif; ?>

                                                                    <tr class="border-bottom">
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0650', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left"><span class="data"><?php print(number_format($Period['DesignPreviewRequests'])); ?></span></td>
                                                                        <td width="260"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0651', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left">&nbsp;</td>
                                                                        <td width="260"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0652', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left">&nbsp;</td>
                                                                        <td width="260"></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0653', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left">&nbsp;</td>
                                                                        <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['ChargeSystemPeriod'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                                                                    </tr>
                                                                    <tr id="discount-row">
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0654', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left">&nbsp;</td>
                                                                        <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['Discount'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>                            
                                                                    </tr>
                                                                    <tr id="tax-row" style="display:none">
                                                                        <td width="280"><?php InterfaceLanguage('Screen', '0655', false, '', false, true); ?></td>
                                                                        <td width="100" style="text-align:left"><span class="data"><?php print(PAYMENT_TAX_PERCENT); ?>%</span></td>
                                                                        <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['Tax'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                                                                    </tr>
                                                                    <tr class="border-top-dark">
                                                                        <td width="280"><strong><?php InterfaceLanguage('Screen', '0656', false, '', false, true); ?></strong></td>
                                                                        <td width="100" style="text-align:left">&nbsp;</td>
                                                                        <td width="260"><span class="data big"><?php print(Core::FormatCurrency($Period['TotalAmount'], false)); ?></span> <span class="data"><?php print(PAYMENT_CURRENCY); ?></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#print-button').click(function () {
        window.print();
    });
    $(document).ready(function () {

        $('#print-pdf').click(function () {
//            $('body').scrollTop(0);
            var elem = $('#to_pdf');

            html2canvas(elem, {
                imageTimeout: 2000,
                removeContainer: true,
                onrendered: function (canvas) {
                    var imgData = canvas.toDataURL(
                            'image/png');

                    var doc = new jsPDF({
                        unit: 'px',
                        format: 'a4'
                    });
//                    doc.addImage(imgData, 'PNG', 20, 20);
                    doc.addImage(imgData, 'JPEG', 20, 10);

//                    var doc = new jsPDF();
//                    doc.setFontSize(100);
//                    doc.addImage(imgData, 'PNG', 10, 10);

                    doc.save('invoice_' + '<?php echo date('Y-m-d', strtotime($Period['PeriodStartDate'])) ?>' + '.pdf');

                },
                logging: true,
                useCORS: true
            });

        });
    });

</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_invoice_footer.php'); ?>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>