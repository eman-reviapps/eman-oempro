<?php
include_once(PLUGIN_PATH . 'process/libraries/helpers/Input.php');
include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php');
//print_r($PluginLanguage);
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/css/birdie.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-transparent">
            <div class="portlet-body">
                <div class="row note note-info note-birdie">
                    <?php // print_r($CurrentUserGroup) ?>
                    <div class="col-md-9">
                        <p> Current plan
                            <a class="link-custom" target="_blank"> <strong><?php echo $CurrentUserGroup['GroupName'] ?></strong> </a>
                        </p>
                        <p><strong class="font-purple-sharp"><?php echo $CurrentUserGroup['LimitSubscribers'] ?></strong> <?php InterfaceLanguage('Screen', '0104', false); ?></p>
                        <p><strong class="font-purple-sharp"><?php echo $CurrentUserGroup['LimitEmailSendPerPeriod'] > 0 ? $CurrentUserGroup['LimitEmailSendPerPeriod'] : 'Unlimited' ?></strong> <?php InterfaceLanguage('Screen', '0528', false); ?>.</p>
                        <p><strong class="font-purple-sharp"><?php echo $TotalSubscribersOnTheAccount ?></strong> <?php InterfaceLanguage('Screen', '9272', false); ?></p>

                    </div>
                    <div class="col-md-3">
                        <script language="JavaScript" type="text/javascript" >
                            TrustLogo("https://flyinglist.com/comodo_secure_seal_100x85_transp.png", "CL1", "none");
                        </script>
                        <!--<a style="display: inline" href="https://ssl.comodo.com" id="comodoTL">SSL Certificates</a>-->
                    </div>
                </div>
                <div class="portlet portlet-fit">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-10">
                                <?php
                                if (isset($PageErrorMessage) == true):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable" style="width: 910px">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                    <?php
                                elseif (isset($PageSuccessMessage) == true):
                                    ?>
                                    <div class="alert alert-info alert-dismissable" style="width: 910px">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                    <?php
                                elseif (validation_errors()):
                                    ?>
                                    <div class="alert alert-danger alert-dismissable" style="width: 910px">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php InterfaceLanguage('Screen', '0275', false); ?>
                                    </div>               
                                    <?php
                                endif;
                                ?>
                                <form id="UpgradeForm" method="post" action="<?php InterfaceAppURL(); ?>/process/upgrade/">  
                                    <input type="hidden" id="upgradeType" name="upgradeType"/>
                                    <birdie-plan>
                                        <div class="birdie-plan birdie-clearfix">
                                            <div class="birdie-plan-progress"></div>
                                            <h2>No, Restrictions for any Feature<br/>
                                                Choose a Plan depends on your Subscriber number  

                                            </h2>
                                            <hr/>
                                            <div class="plan-container birdie-clearfix">
                                                <?php if ($PayAsYouGo) {
                                                    ?>
                                                    <div class="current-plan">
                                                        <?php // print_r($PayAsYouGo)  ?>
                                                        <h3><?php echo $PluginLanguage['Screen']['0011']; ?></h3>
                                                        <birdie-drop>
                                                            <div class="form-group <?php print((form_error('package') != '' ? 'error' : '')); ?>" id="form-row-package">
                                                                <select name="PayAsYouGoPackage" id="PayAsYouGoPackage" class="form-control">

                                                                    <?php
                                                                    foreach ($PayAsYouGo as $EachGroup):
                                                                        ?>
                                                                        <option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('package', $EachGroup['UserGroupID'], ($UserInformation['RelUserGroupID'] == $EachGroup['UserGroupID'] ? true : false)); ?>><?php print($EachGroup['GroupName']); ?></option>
                                                                        <?php
                                                                    endforeach;
                                                                    ?>
                                                                </select>
                                                                <?php print(form_error('PayAsYouGoPackage', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="birdie-drop ">
                                                                <p><span id="PayAsYouGoLimitEmails"></span> Emails</p>
                                                            </div>
                                                            <style scoped="scoped">  </style>
                                                        </birdie-drop>
                                                        <p>Pay only when a campaign is sent</p><br/>
                                                        <div class="price"><span class="sign">$</span><span class="price"><span id="PayAsYouGoPrice"></span></span></div>
                                                       
                                                        <!--<input type="hidden" id="PayAsYouGoUpgrade" name="PayAsYouGoUpgrade" value="<?php echo $PayAsYouGo['UserGroupID'] ?>"/>-->

                                                        <?php if (!$disable) { ?>
                                                            <birdie-button link="#" classnames="js-purchase-credits" label="Purchase credits">
                                                                <a class="birdie-button large inline js-subscribe-to-plan" onclick="toPayAsYouGo()" >Upgrade to this</a>
                                                            </birdie-button>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($PaidUserGroupsMonthly) {
                                                    ?>
                                                    <div class="monthly-plan">
                                                        <h3><?php echo $PluginLanguage['Screen']['0012']; ?></h3>
                                                        <birdie-drop>
                                                            <div class="form-group <?php print((form_error('package') != '' ? 'error' : '')); ?>" id="form-row-package">
                                                                <select name="MonthlyPackage" id="MonthlyPackage" class="form-control">

                                                                    <?php
                                                                    foreach ($PaidUserGroupsMonthly as $EachGroup):
                                                                        ?>
                                                                        <option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('package', $EachGroup['UserGroupID'], ($UserInformation['RelUserGroupID'] == $EachGroup['UserGroupID'] ? true : false)); ?>><?php print($EachGroup['GroupName']); ?></option>
                                                                        <?php
                                                                    endforeach;
                                                                    ?>
                                                                </select>
                                                                <?php print(form_error('MonthlyPackage', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="birdie-drop ">
                                                                <p><span id="MonthlyLimitSubscribers"></span> subscribers</p>
                                                            </div>
                                                            <style scoped="scoped">  </style>
                                                        </birdie-drop>
                                                        <p>Store up to <span id="MonthlyLimitSubscribers2"></span> subscribers and send <span id="MonthlyLimitEmails"></span> emails <span id="PeriodMonthly"></span>.</p>
                                                        <div class="price"><span class="sign">$</span><span class="price"><span id="MonthlyPrice"></span></span>each month</div>
                                                        <?php if (!$disable) { ?>
                                                            <birdie-button link="#" size="large" style="inline" classnames="js-subscribe-to-plan" type="" label="Upgrade to this">
                                                                <a class="birdie-button large inline js-subscribe-to-plan" onclick="toMonthly()" >Upgrade to this</a>
                                                                <!--<input class="btn birdie-button large inline js-subscribe-to-plan" type="button" value="Upgrade to this">-->
                                                            </birdie-button>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($PaidUserGroupsAnnually) {
                                                    ?>
                                                    <div class="future-plan">
                                                        <h3><?php echo $PluginLanguage['Screen']['0013']; ?></h3>
                                                        <birdie-drop>
                                                            <div class="form-group <?php print((form_error('package') != '' ? 'error' : '')); ?>" id="form-row-package">
                                                                <select name="AnnuallyPackage" id="AnnuallyPackage" class="form-control">
                                                                    <?php
                                                                    foreach ($PaidUserGroupsAnnually as $EachGroup):
                                                                        ?>
                                                                        <option value="<?php print($EachGroup['UserGroupID']); ?>" <?php echo set_select('package', $EachGroup['UserGroupID'], ($UserInformation['RelUserGroupID'] == $EachGroup['UserGroupID'] ? true : false)); ?>><?php print($EachGroup['GroupName']); ?></option>
                                                                        <?php
                                                                    endforeach;
                                                                    ?>
                                                                </select>
                                                                <?php print(form_error('AnnuallyPackage', '<div class="form-row-note error"><p>', '</p></div>')); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="birdie-drop ">
                                                                <p><span id="AnnuallyLimitSubscribers"></span> subscribers</p>
                                                            </div>
                                                        </birdie-drop>
                                                        <p>Store up to <span id="AnnuallyLimitSubscribers2"></span> subscribers and send <span id="AnnuallyLimitEmails"></span> emails <span id="PeriodAnnually"></span>.</p>
                                                        <div class="price"><span class="sign">$</span><span class="price"><span id="AnnuallyPrice"></span></span>each year</div>
                                                        <?php if (!$disable) { ?>
                                                            <birdie-button link="#" size="large" style="inline" classnames="js-subscribe-to-plan" type="" label="Upgrade to this">
                                                                <a class="birdie-button large inline js-subscribe-to-plan" onclick="toAnnually()" >Upgrade to this</a>
                                                            </birdie-button>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </birdie-plan>
                                </form>
                            </div>
                            <div class="col-md-2">
                                <?php if (isset($Balance) && $Balance > 0) { ?>
                                    <div class="portlet sale-summary" style="padding: 10px;margin-left: -40px;background: #fff">
                                        <div class="portlet-title">
                                            <div class="caption font-purple-sharp sbold">  <?php echo $PluginLanguage['Screen']['0032']; ?> </div>
                                        </div>
                                        <div class="portlet-body">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <span class="sale-info bold" style="font-size: 18px;"> <?= $Balance ?> $<i class="fa fa-img-down"></i>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>
                                <ul class="payment-icons">
                                    <li>
                                        <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/amex.png">
                                    </li>
                                    <li>
                                        <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/visa_electron.png">
                                    </li>
                                    <li>
                                        <img class="image-payment" src="https://flyinglist.com/wp-content/uploads/2015/04/visa.png">
                                    </li>
                                    <li>
                                        <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/maestro.png">
                                    </li>
                                    <li>
                                        <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/mastercard.png">
                                    </li>
                                    <li>
                                        <img class="image-payment master-card" src="https://flyinglist.com/wp-content/uploads/2015/04/iyzico.png">
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once(PLUGIN_PATH . 'process/templates/layouts/scripts.php');

//page scripts here
include_once(PLUGIN_PATH . 'process/templates/javascript/global-vars.php');
include_once(PLUGIN_PATH . 'process/templates/javascript/upgrade.php');

