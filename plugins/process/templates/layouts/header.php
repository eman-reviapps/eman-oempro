
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
$dir = 'ltr';
$lang = 'en';
?>
<html lang="en" dir="<?php echo $dir ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Register</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' ?>" rel="stylesheet" type="text/css" />

        <?php
        if ($dir == "rtl") {
            ?>
            <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css' ?>" rel="stylesheet" type="text/css" />
            <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css' ?>" rel="stylesheet" type="text/css" />

            <?php
        } else {
            ?>
            <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet" type="text/css" />
            <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' ?>" rel="stylesheet" type="text/css" />

            <?php
        }
        ?>

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/select2/css/select2.min.css' ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/plugins/select2/css/select2-bootstrap.min.css' ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/css/components.min.css' ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo PLUGIN_URL . '/process/assets/global/css/plugins.min.css' ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo PLUGIN_URL . '/process/assets/pages/css/login.min.css' ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo PLUGIN_URL . '/process/assets/layouts/layout4/css/flyinglist.css' ?>" rel="stylesheet" type="text/css" />


        <?php
        if ($dir == "rtl") {
            ?>
            <link href="<?php echo PLUGIN_URL . '/process/assets/layouts/layout4/css/custom-rtl.css' ?>" rel="stylesheet" type="text/css" />

            <?php
        } else {
            ?>
            <link href="<?php echo PLUGIN_URL . '/process/assets/layouts/layout4/css/custom.css' ?>" rel="stylesheet" type="text/css" />

            <?php
        }
        ?>
        <?php
        if ($lang == "ar") {
            ?>
            <link href="<?php echo PLUGIN_URL . '/process/assets/layouts/layout4/css/ar.css' ?>" rel="stylesheet" type="text/css" />
            <?php
        }
        ?>
        <script type="text/javascript"> //<![CDATA[ 
            var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
            document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
            //]]>
        </script>
    </head>
    <body class="page-actions register">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="{{URL::to('home')}}">
                <img style="width: 190px" src="<?php echo PLUGIN_URL . '/process/assets/layouts/layout4/img/logo-flying.png' ?>" alt="">

            </a>
            <span class="">
                <script language="JavaScript" type="text/javascript" >
                    TrustLogo("https://flyinglist.com/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                </script>
                <!--<a style="margin-left: 150px;margin-top: -25px;margin-bottom: 10px;" href="https://ssl.comodo.com" id="comodoTL">SSL Certificates</a>-->
            </span>
        </div>