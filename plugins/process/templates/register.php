<?php
include_once(PLUGIN_PATH . 'process/templates/layouts/header.php');
include_once(PLUGIN_PATH . 'process/libraries/helpers/Input.php');
?>

<div class="content">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption-custom center-block text-center">
                Sign up for free
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="POST" action="<?php InterfaceAppURL(); ?>/process/register/" id="RegisterForm" class="horizontal-form">  
                <div class="form-body">
                    <?php
                    $display = '';
                    $msg = '';
                    if (isset($PageErrorMessage) == true && $PageErrorMessage != '') {
                        $msg = $PageErrorMessage;
                        $display = '';
                        $class = 'danger';
                    } elseif (isset($PageSuccessMessage) == true && $PageSuccessMessage != '') {
                        $msg = $PageSuccessMessage;
                        $display = '';
                        $class = 'success';
                    } else {
                        $msg = '';
                        $display = 'display:none';
                        $class = 'danger';
                    }
                    ?>
                    <input type="hidden" name="time" value="<?php echo $time; ?>" />
                    <div style="<?php echo $display ?>" class = "alert alert-<?php echo $class ?> alert-dismissable">            
                        <button class="close" data-close="alert"></button>
                        <span><?php echo $msg ?></span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input type="text" id="first_name" value="<?php echo Input::old('first_name', $_POST) ?>" name="first_name" class="form-control" >
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Last Name</label>
                                <input type="text" id="last_name" value="<?php echo Input::old('last_name', $_POST) ?>" name="last_name" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Phone Number</label>
                                <input type="text" id="phone" value="<?php echo Input::old('phone', $_POST) ?>" name="phone" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Organization Name</label>
                                <input type="text" id="organization_name" value="<?php echo Input::old('organization_name', $_POST) ?>" name="organization_name" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email Address</label>
                                <input type="text" id="email" value="<?php echo Input::old('email', $_POST) ?>" name="email" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Package</label>
                                <select class="form-control" id="package" name="package" data-placeholder="Choose a package" tabindex="1">
                                    <?php
                                    if ($UserGroups) {
                                        foreach ($UserGroups as $user_group) {

                                            if ($user_group['UserGroupID'] != 1) {
                                                $selected = (Input::old('package', $_POST) == $user_group['UserGroupID'] || ($user_group['UserGroupID'] == $ChoosenGroup && $ChoosenGroup > 0)) ? 'selected' : '';
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $user_group['UserGroupID'] ?>"><?php echo $user_group['GroupName'] ?></option>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <option value="">Choose a package</option>
                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Username</label>
                                <input type="text" id="user_name" value="<?php echo Input::old('user_name', $_POST) ?>" name="user_name" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <input type="password" id="password" name="password" class="form-control" >

                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p class="text-center">I agree to the 
                                    <a class="font-purple-sharp bold" href="">Privacy Policy</a>, 
                                    <a class="font-purple-sharp bold" href="">Terms of Service</a>, 
                                    <a class="font-purple-sharp bold" href="">User Pledge</a> and
                                    <a class="font-purple-sharp bold" href="">Anti-Spam Policy</a>
                                </p>
                            </div>
                        </div>
                        <!--/span-->
                        
                    </div>
                </div>
                <div class="form-actions">
                    <input id="sign_up" class="btn btn-block blue" type="submit" value="Sign Up">
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php
include_once(PLUGIN_PATH . 'process/templates/layouts/scripts.php');

//page scripts here
include_once(PLUGIN_PATH . 'process/templates/javascript/global-vars.php');
include_once(PLUGIN_PATH . 'process/templates/javascript/register.php');

include_once(PLUGIN_PATH . 'process/templates/layouts/footer.php');
