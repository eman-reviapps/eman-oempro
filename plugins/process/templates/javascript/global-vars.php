
<script type="text/javascript">
    var url = "Processing... <img src=<?php echo PLUGIN_URL . '/backend/assets/layouts/layout4/img/Chasing_blocks.gif'?>>";

    $.validator.addMethod('phone', function (value, element, param) {
        return this.optional(element) ||
                value.match(typeof param == 'string' ? new RegExp(param) : param);
    },
            'Please enter a value in the correct format.');

</script>