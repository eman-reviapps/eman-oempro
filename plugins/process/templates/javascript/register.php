<script>
    var FormValidation = function () {

        // basic validation
        var handleValidation1 = function () {
            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#RegisterForm');

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    first_name: {
                        required: true,
                        minlength: 3
                    },
                    last_name: {
                        required: true,
                        minlength: 3
                    },
                    phone: {
                        required: true,
                        phone: "^[\\+]*[0-9]{8,13}$"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    package: {
                        required: true
                    },
                    user_name: {
                        required: true,
                        minlength: 3
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    first_name: {
                        required: "User Name is required",
                        minlength: "User Name at least 3 characters"
                    },
                    last_name: {
                        required: "Last Name is required",
                        minlength: "Last Name at least 3 characters"
                    },
                    phone: {
                        required: "Phone is required",
                        phone: "Only numbers allowed"
                    },
                    email: {
                        required: "Email is required",
                        email: "Invalid Email address"
                    },
                    package: {
                        required: "Package is required",
                    },
                    user_name: {
                        required: "User Name is required",
                        minlength: "User Name at least 3 characters"
                    },
                    password: {
                        required: "Password is required",
                        minlength: "Password Must Be at least 6 characters"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              

                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label
                            .closest('.form-group').removeClass('has-error'); // set success class to the control group
                }
            });


        }
        return {
            //main function to initiate the module
            init: function () {

                handleValidation1();

            }

        };
    }();
    jQuery(document).ready(function () {
        FormValidation.init();
    });
</script>



