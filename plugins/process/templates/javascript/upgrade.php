<script>
    var pay_as_you_go_array =<?php echo json_encode($PayAsYouGo); ?>;
    var paid_monthly_array =<?php echo json_encode($PaidUserGroupsMonthly); ?>;
    var paid_annually_array =<?php echo json_encode($PaidUserGroupsAnnually); ?>;

//    alert($("#MonthlyPackage").val());
//    alert($("#AnnuallyPackage").val());

    monthlyChanged($("#MonthlyPackage").val());
    annuallyChanged($("#AnnuallyPackage").val());
    payAsYouGoChanged($("#PayAsYouGoPackage").val());

    $('#MonthlyPackage').on('change', function () {
        var groupId = this.value;
        monthlyChanged(groupId);
    });

    $('#AnnuallyPackage').on('change', function () {
        var groupId = this.value;
        annuallyChanged(groupId);
    });
    $('#PayAsYouGoPackage').on('change', function () {
        var groupId = this.value;
        payAsYouGoChanged(groupId);
    });
    function monthlyChanged(groupId)
    {
        var result;

        for (var i = 0; i < paid_monthly_array.length; i++) {
            if (paid_monthly_array[i]['UserGroupID'] == groupId) {
                result = paid_monthly_array[i];
            }
        }
        $("#MonthlyLimitSubscribers").text(result.LimitSubscribers);
        $("#MonthlyLimitSubscribers2").text(result.LimitSubscribers);

        limit_emails = result.LimitEmailSendPerPeriod;
        if (limit_emails == 0)
        {
            $("#MonthlyLimitEmails").text('unlimited');
            $("#PeriodMonthly").text('');
        } else
        {
            $("#MonthlyLimitEmails").text(limit_emails);
            $("#PeriodMonthly").text('per month');
        }

        $("#MonthlyPrice").text(result.PaymentSystemChargeAmount);
//        console.log(result);
    }
    function annuallyChanged(groupId)
    {
        var result;

        for (var i = 0; i < paid_annually_array.length; i++) {
            if (paid_annually_array[i]['UserGroupID'] == groupId) {
                result = paid_annually_array[i];
            }
        }
        $("#AnnuallyLimitSubscribers").text(result.LimitSubscribers);
        $("#AnnuallyLimitSubscribers2").text(result.LimitSubscribers);

        limit_emails = result.LimitEmailSendPerPeriod;
        if (limit_emails == 0)
        {
            $("#AnnuallyLimitEmails").text('unlimited');
            $("#PeriodAnnually").text('');
        } else
        {
            $("#AnnuallyLimitEmails").text(limit_emails);
            $("#PeriodAnnually").text('per month');
        }

        $("#AnnuallyPrice").text(result.PaymentSystemChargeAmount);

//        console.log(result);
    }
    function payAsYouGoChanged(groupId)
    {
        var result;

        for (var i = 0; i < pay_as_you_go_array.length; i++) {
            if (pay_as_you_go_array[i]['UserGroupID'] == groupId) {
                result = pay_as_you_go_array[i];
            }
        }
        $("#PayAsYouGoLimitEmails").text(result ? result.LimitEmailSendPerPeriod : '');

        var price_range = result.PaymentPricingRange.split("|");
        $('#PayAsYouGoPrice').text(price_range[0]*price_range[1]);
    }

    function toPayAsYouGo()
    {
        $("#upgradeType").val(1);
        $("#UpgradeForm").submit()
    }
    function toMonthly()
    {
        $("#upgradeType").val(2);
        $("#UpgradeForm").submit()
    }
    function toAnnually()
    {
        $("#upgradeType").val(3);
        $("#UpgradeForm").submit()
    }

</script>



