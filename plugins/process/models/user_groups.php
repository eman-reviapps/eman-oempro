<?php

class model_user_groups extends model_base_reg
{
	public function __construct()
	{

	}

	public function GetUserGroups()
	{
		$SQLQuery = $this->add_parameters("SELECT * FROM oempro_user_groups WHERE UserGroupID<>1");

		$Query = $this->query_result($SQLQuery);
		if (is_bool($Query) == true && $Query == false) return false;

		return $Query;
	}

}