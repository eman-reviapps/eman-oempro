<?php

class model_user_balance extends model_base_reg {

    public function __construct() {
        
    }

    public function CreateUserBalance($data) {

        $params = array(
            "RelUserID" => $data['user_id'],
            "Balance" => $data['balance'],
            "CreationDate" => $data['create_date'],
        );

        $Query = $this->query_insert($params, "oempro_users_balance");

        if (is_bool($Query) == true && $Query == false)
            return false;

        return $Query;
    }

    public function updateUserBalance($data) {

        $ArrayFieldnValues = array(
            "Balance" => $data["balance"],
            "UpdateDate" => $data["update_date"]
        );
        $ArrayCriterias = array("RelUserID" => $data["user_id"]);

        $Query = $this->query_update($ArrayFieldnValues, $ArrayCriterias, "oempro_users_balance");

        if (is_bool($Query) == true && $Query == false)
            return false;

        return $Query;
    }

    public function getUserBalance($user_id) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_users_balance WHERE RelUserID=? order by LogID desc", array($user_id));

        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true && $Query == false)
            return 0;

        $result = $Query[0];
        $balance = $result->Balance;

        return $balance;
    }

    public function checkBalanceExists($user_id) {
        $SQLQuery = $this->add_parameters("SELECT count(*) as count FROM oempro_users_balance WHERE RelUserID=?", array($user_id));

        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true && $Query == false)
            return false;

        $result = $Query[0];
        $count = $result->count;

        $exists = $count == 0 ? false : true;

        return $exists;
    }

}
