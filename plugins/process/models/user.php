<?php

class model_user extends model_base_reg {

    public function __construct() {
        
    }

    public function CreateUser($data) {

        $params = array(
            "RelUserGroupID" => null,
            "EmailAddress" => $data['email'],
            "Username" => $data['user_name'],
            "Password" => $data['password'],
            "ReputationLevel" => "Trusted",
            "FirstName" => $data['first_name'],
            "LastName" => $data['last_name'],
            "CompanyName" => $data['organization_name'],
            "Phone" => $data['phone'],
            "Language" => $data['language'],
            "TimeZone" => "(GMT) London",
            "AvailableCredits" => 0,
        );

        $Query = $this->query_insert($params, "oempro_users");

        if (is_bool($Query) == true && $Query == false)
            return false;

        return $Query;
    }

    public function updateUser($data) {

        $ArrayFieldnValues = array("RelUserGroupID" => $data["package"]);
        $ArrayCriterias = array("UserID" => $data["user_id"]);

        $Query = $this->query_update($ArrayFieldnValues, $ArrayCriterias, "oempro_users");

//        if (is_bool($Query) == true && $Query == false)
//            return false;

        return $Query;
    }

}
