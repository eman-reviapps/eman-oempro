<?php
/**
 * User Process Plug-In For Oempro4
 * Name: User Process
 * Description: this provides user register, user upgrade and user invoices
 * Minimum Oempro Version: 4.6.2
 */

include_once(PLUGIN_PATH . 'process/main.php');
