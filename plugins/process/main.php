<?php

include_once(PLUGIN_PATH . 'process/libraries/helpers/Validation.php');
include_once(PLUGIN_PATH . 'process/libraries/helpers/Curl.php');
include_once(PLUGIN_PATH . 'process/libraries/helpers/Output.php');
include_once(PLUGIN_PATH . 'process/libraries/Oempro.php');
include_once(PLUGIN_PATH . 'process/libraries/api/OemproAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/AdminAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/UsersAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/PaymentAPI.php');

class process extends Plugins {

    // -------------------------------------
    // Plugin must-have variablies:
    // -------------------------------------
    // - Do not rename them!
    // - Do not remove them!
    // The value of this property should be the same as your class name
    private static $PluginCode = 'process';
    // Just leave this as an empty array. It will be populated later on
    private static $ArrayLanguage = array();
    // Leave this variable as null. It will store the CodeIgniter framework
    public static $ObjectCI = null;
    // Leave it empty. If the authorization level is "user", this array
    // will contain the user information
    private static $UserInformation = array();
    // Lave it empty. If the authorization level is "admin", this array
    // will contain the admin information
    private static $AdminInformation = array();
    // Leave this variable as it is. Do not change it.
    private static $Models = null;
    // These variables will be used in the near future, do not change or
    // remove them.
    private static $PluginLicenseKey = null;
    private static $PluginLicenseStatus = true;
    private static $PluginLicenseInfo = null;
    // This is the version of the plugin. It should be in x.x.x format.
    // Example: 1.0.5
    private static $PluginVersion = "1.0.0";
    // Does your plugin require models? Set them here as array.
    // Example: array('model1', 'model2', 'model3')
    private static $LoadedModels = array('user_groups');

    // -------------------------------------
    // Default Plugin Methods
    // -------------------------------------

    public function __construct() {
        // Do not use the constructor for any purpose. It will not work
        // Instead, use "enable", "disable" and "load" plugin functions
    }

    // This method is executed when the administrator clicks "Enable" link
    // next to this plugin in Plugin manager of the Oempro admin area
    public function enable_process() {
        // This private method will load models
        self::_LoadModels();

        // License check
        // TODO: License check engine will be implemented soon. This engine
        // 		 call will let you to sell your plugins on Octeth Plugin Store
        //		 and avoid piracy
        // If your plugin depends on new database tables, you should setup
        // these tables and data here.
//		self::$Models->db_setup->SetupDatabaseSchema();
    }

    // This method is executed when the administrator clicks "Disable" link
    // next to this plugin in Plugin management of the Oempro admin area
    public function disable_process() {
        // You can remove plugin database tables, remove plugin options
        // or perform anything related to uninstalling your plugin here
    }

    // This method is executed whenever an Oempro page (admin, user or any other pages)
    // is executed. The plugin must be enabled to get this method executed. It's somewhat
    // like the constructor method.
    // Menu item placements, hook registration and all on-load processes should be done
    // in this method.
    public function load_process() {
        // License check
        // TODO: License check engine will be implemented soon. This engine
        // 		 call will let you to sell your plugins on Octeth Plugin Store
        //		 and avoid piracy
        // Register enable and disable hooks. Do NOT change these two lines.
        parent::RegisterEnableHook(self::$PluginCode);
        parent::RegisterDisableHook(self::$PluginCode);

        // Setup menu items
        // "set_menu_items" method will be executed inside your plugin class
        // to register menu items. Simply check "set_menu_items" method to learn
        // how you should setup your menu items on the user interface.
        parent::RegisterMenuHook(self::$PluginCode, 'set_menu_items');
        // Hook registration. To learn more about available hook listeners
        // visit the plugin development manual. Below, we registered one
        // hook as an example.
//		parent::RegisterHook('Action', 'FormItem.AddTo.Admin.UserGroupLimitsForm', self::$PluginCode, 'set_user_group_form_items', 10, 1);
//		parent::RegisterHook('Filter', 'UserGroup.Update.FieldValidator', self::$PluginCode, 'usergroup_update_fieldvalidator', 10, 1);
//		parent::RegisterHook('Action', 'UserGroup.Update.Post', self::$PluginCode, 'usergroup_update', 10, 2);
//		parent::RegisterHook('Action', 'UserGroup.Create.Post', self::$PluginCode, 'usergroup_update', 10, 2);
//		parent::RegisterHook('Action', 'UserGroup.Delete.Post', self::$PluginCode, 'usergroup_delete', 10, 1);
//		parent::RegisterHook('Action', 'User.Delete.Post', self::$PluginCode, 'user_delete', 10, 1000);
        // Retrieve language setting. This setting will be used to set
        // the language of the plugin. First we will try to retrieve the
        // saved language preference from "options" table. If it doesn't exist,
        // we will set the default "en" (English) language. If it exists, we will
        // set the saved one in the options table.
        $Language = Database::$Interface->GetOption(self::$PluginCode . '_Language');
        if (count($Language) == 0) {
            Database::$Interface->SaveOption(self::$PluginCode . '_Language', 'en');
            $Language = 'en';
        } else {
            $Language = $Language[0]['OptionValue'];
        }

        // Load the language file
        // The selected language will be loaded here. If the language file doesn't exist,
        // plugin will load the default "en" language pack.
        $ArrayPlugInLanguageStrings = array();
        if (file_exists(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php') == true) {
            include_once(PLUGIN_PATH . self::$PluginCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php');
        } else {
            include_once(PLUGIN_PATH . self::$PluginCode . '/languages/en/en.inc.php');
        }
        self::$ArrayLanguage = $ArrayPlugInLanguageStrings;

        unset($ArrayPlugInLanguageStrings);

        // This private method will load models
        // The following line will load models defined in self::$LoadedModules array property.
        // All database processes should be done in model files to keep your plugin code
        // structure organized. (basic MVC approach)
        self::_LoadModels();

        // Do you need to load any third party classes, helpers, libraries? Load them here.
        // There's no standards/restrictions on loading third party classes, libraries or helpers.
        // They're standard PHP functions, classes or code blocks such as Mailgun wrapper, phpmailer
        // class, etc. The most important thing is, don't forget to set the path absolute from the root
        // To help you, we have a constant: PLUGIN_PATH . self::$PluginCode . Take a look at examples
        // listed below.
        // include_once(PLUGIN_PATH . self::$PluginCode . '/libraries/111.php');
        // include_once(PLUGIN_PATH . self::$PluginCode . '/libraries/222.php');
        // include_once(PLUGIN_PATH . self::$PluginCode . '/libraries/333.php');
        // Define constants if needed. Constants to use in your classes or plugin class? You can set
        // them here, just to keep the code organized. Or you can set them anywhere in your code. Just
        // be sure that they are prefixed with the self::$PluginCode to avoid conflicts with the main
        // Oempro or other plugin constants.
        // define(strtoupper(self::$PluginCode).'_SESSION_THRESHOLD', 5);
    }

    // -------------------------------------
    // Hooks, menu item setups, template tag setups
    // -------------------------------------
    // This method setups menu items on the user interface
    // For more info about available menu item locations,
    // please refer to our plugin development manual
    public function set_menu_items() {
        // On the Oempro user interface, you can insert content to specific areas. The list of
        // these specific areas can be found in our plugin development manual. This is the correct
        // method to set which menu item to show where. Below, you an find some examples.

        $ArrayMenuItems = array();
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'User.Settings',
            'MenuID' => 'user_upgrade_ug',
            'MenuLink' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/upgrade/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0003'],
        );
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'User.Settings',
            'MenuID' => 'user_invoices_ug',
            'MenuLink' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/invoices/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0005'],
        );

        return array($ArrayMenuItems);
    }

    // -------------------------------------
    // Controllers (user interface methods)
    // -------------------------------------

    public function ui_register($group_id = 0) {
        if (self::_PluginAppHeader(null) == false)
            return;

        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('payments');
        Core::LoadObject('user_groups');

        $EventResult = self::_EventRegister();
        if (is_array($EventResult) == true) {
            if ($EventResult[0] == false) {
                $PageErrorMessage = $EventResult[1];
            } else {
                $PageSuccessMessage = $EventResult[1];
            }
        }

        // Get lists - Start
//        $FreeUserGroupsMonthly = UserGroups::RetrieveUserGroups(array('*'), array("PaymentSystemChargeAmount" => "0", "SubscriptionType" => 'Monthly', "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
        $UserGroups = UserGroups::RetrieveUserGroups(array('*'), array("PaymentSystemChargeAmount" => "0", "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
//        $UserGroups = self::$Models->user_groups->getUserGroups();

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0002'],
            'PluginView' => '../plugins/process/templates/register.php',
            'PluginLanguage' => self::$ArrayLanguage,
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'UserGroups' => $UserGroups,
            'ChoosenGroup' => $group_id
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render(self::$PluginCode, 'register', $ArrayViewData, true);
    }

    public function ui_test() {
        if (self::_PluginAppHeader('User') == false)
            return;

        Core::LoadObject('api');
        Core::LoadObject('payments');
        Core::LoadObject('user_groups');
        Core::LoadObject('user_balance');

        Core::LoadObject('gateway');

        Core::LoadObject('payment_gateways/gateway_iyzico.inc.php');
        PaymentGateway::SetInterface('Iyzipay');

        $ArrayPaymentParameters = array(
            'UserID' => 1,
            'LogID' => 36,
            'IsCreditPurchase' => false,
            'ReputationLevel' => "Trusted",
            'Price' => "43.34",
            'PaidPrice' => "43.34",
            'CallbackURL' => APP_URL . PaymentGateway::$Interface->getCallBackURL()
        );
        $items = array();
        $items[] = array(
            "ID" => "32",
            "Name" => "15,001 - 20,000 Subscribers",
            "Category" => "Monthly",
            "Price" => "43.34",
        );

        $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
        $GeoTagInfo = geoip_record_by_addr($GeoTag, $_SERVER['REMOTE_ADDR']);
        $City = isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : '';
        $Country = isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : '';

        $ArrayUser = self::$UserInformation;
        $ArrayUser['IP'] = $_SERVER['REMOTE_ADDR'];
        $ArrayUser['uCity'] = $City;
        $ArrayUser['uCountry'] = 'Egypt';

        $checkoutFormInitialize = PaymentGateway::$Interface->initializeCheckoutForm($ArrayPaymentParameters, $ArrayUser, $items);
        $conversationId = $checkoutFormInitialize->getConversationId();
        $locale = $checkoutFormInitialize->getLocale();
        $status = $checkoutFormInitialize->getStatus();
        $errorCode = $checkoutFormInitialize->getErrorCode();
        $errorMessage = $checkoutFormInitialize->getErrorMessage();
        $errorGroup = $checkoutFormInitialize->getErrorGroup();
        $systemTime = $checkoutFormInitialize->getSystemTime();
        $token = $checkoutFormInitialize->getToken();
        $tokenExpireTime = $checkoutFormInitialize->getTokenExpireTime();
        $paymentPageUrl = $checkoutFormInitialize->getPaymentPageUrl();
        $checkoutFormContent = $checkoutFormInitialize->getCheckoutFormContent();

        if (strtolower($status) == 'success') {
            #update payment with token and conversation_id
            $ArrayFieldAndValues = array(
                'ConversationID' => $conversationId,
                'Token' => $token
            );
            $ArrayCriterias = array(
                'LogID' => 36
            );
            Payments::Update($ArrayFieldAndValues, $ArrayCriterias);
        }


        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0029'],
            'CurrentMenuItem' => 'Checkout',
            'PluginView' => '../plugins/process/templates/checkout.php',
            'SubSection' => 'user_checkout_ug',
            'PluginLanguage' => self::$ArrayLanguage,
            'PluginCode' => self::$PluginCode,
            'UserInformation' => $ArrayUser,
            'ErrorCode' => $errorCode,
            'PageErrorMessage' => $errorMessage,
            'CheckoutFormContent' => $checkoutFormContent,
            'Status' => $status,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render(self::$PluginCode, 'checkout', $ArrayViewData, true);
//        self::$ObjectCI->render('user/checkout', $ArrayViewData);
    }

    public function ui_upgrade() {
        if (self::_PluginAppHeader('User') == false)
            return;

        Core::LoadObject('api');
        Core::LoadObject('payments');
        Core::LoadObject('user_groups');
        Core::LoadObject('user_balance');
        Core::LoadObject('user_payment');

        $EventResult = self::_EventUpgrade();
        if (is_array($EventResult) == true) {
            if ($EventResult[0] == false) {
                $PageErrorMessage = $EventResult[1];
            } else {
                $PageSuccessMessage = $EventResult[1];
            }
        }

        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => self::$UserInformation['UserID']), true);
        self::$UserInformation = $ArrayUser;
//        $user = self::$UserInformation;
        $UserGroups = self::$Models->user_groups->getUserGroups();


        $FreeUserGroupsMonthly = UserGroups::RetrieveUserGroups(array('*'), array("PaymentSystemChargeAmount" => "0", "SubscriptionType" => 'Monthly', "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
//        $FreeUserGroupsAnnually = UserGroups::RetrieveUserGroups(array('*'), array("PaymentSystemChargeAmount" => "0","SubscriptionType"=>'Annually',"CreditSystem"=>"Disabled"));
        $UserGroupsMonthly = UserGroups::RetrieveUserGroups(array('*'), array("SubscriptionType" => 'Monthly', "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
        $UserGroupsAnnually = UserGroups::RetrieveUserGroups(array('*'), array("SubscriptionType" => "Annually", "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
        $PayAsYouGo = UserGroups::RetrieveUserGroups(array('*'), array("CreditSystem" => "Enabled", 'IsAdminGroup' => 'No'));

        $CurrentUserGroup = UserGroups::RetrieveUserGroups(array('*'), array("UserGroupID" => $ArrayUser['RelUserGroupID']));

        $PaidUserGroupsMonthly = array();
        foreach ($UserGroupsMonthly as $UserGroupMonthly) {
            if ($UserGroupMonthly[PaymentSystemChargeAmount] > 0) {
                $PaidUserGroupsMonthly[] = $UserGroupMonthly;
            }
        }

        $PaidUserGroupsAnnually = array();
        foreach ($UserGroupsAnnually as $UserGroupAnnually) {
            if ($UserGroupAnnually[PaymentSystemChargeAmount] > 0) {
                $PaidUserGroupsAnnually[] = $UserGroupAnnually;
            }
        }
        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($ArrayUser['UserID']);

        $Balance = UserBalance::getUserBalance($ArrayUser['UserID']); //self::$Models->user_balance->getUserBalance($user['UserID']);

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0002'],
            'CurrentMenuItem' => 'Settings',
            'PluginView' => '../plugins/process/templates/upgrade.php',
            'SubSection' => 'user_upgrade_ug',
            'PluginLanguage' => self::$ArrayLanguage,
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'UserGroups' => $UserGroups,
            'UserInformation' => $ArrayUser,
            'FreeUserGroupsMonthly' => $FreeUserGroupsMonthly[0],
            'PaidUserGroupsMonthly' => $PaidUserGroupsMonthly,
            'PaidUserGroupsAnnually' => $PaidUserGroupsAnnually,
            'PayAsYouGo' => $PayAsYouGo,
            'CurrentUserGroup' => $CurrentUserGroup[0],
            'TotalSubscribersOnTheAccount' => $TotalSubscribersOnTheAccount,
            'Balance' => $Balance,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

//        self::$ObjectCI->plugin_render(self::$PluginCode, 'upgrade', $ArrayViewData, true);
        self::$ObjectCI->render('user/settings', $ArrayViewData);
    }

    private function _EventUpgrade() {

        if (!empty($_POST)) {

            if (AccountIsUnTrustedCheck(self::$UserInformation)) {
                show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
                exit;
            }

            $valid = Validation::get()->verifyRequiredParams(
                    array(
                "upgradeType")
                    , $_POST
            );

            if (!$valid[0]) {
                return array(false, self::$ArrayLanguage['Common']['0001'] . $valid[1]);
            }

            $upgrade_type = $_POST['upgradeType'];
            if ($upgrade_type == 1) {
                //PayAsYouGo
                $new_package = $_POST['PayAsYouGoPackage'];
            } else if ($upgrade_type == 2) {
                //Monthly
                $new_package = $_POST['MonthlyPackage'];
            } else if ($upgrade_type == 3) {
                //Annually
                $new_package = $_POST['AnnuallyPackage'];
            }

            $user = self::$UserInformation;
            $old_package = $user["RelUserGroupID"];
            //check for payment if old package is not the same as new package
            if ($old_package == $new_package) {
                return array(true);
            }
            // Get selected usergroup info
            $ArrayOldUserGroup = UserGroups::RetrieveUserGroup($user["RelUserGroupID"]);
            $ArrayNewUserGroup = UserGroups::RetrieveUserGroup($new_package);

            $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced(self::$UserInformation['UserID']);
            if ($TotalSubscribersOnTheAccount > $ArrayNewUserGroup['LimitSubscribers'] && ($ArrayNewUserGroup['LimitSubscribers'] > 0)) {
                return array(false, self::$ArrayLanguage['Screen']['0023']);
            }

            // Check if payment period for this user exists for the current period
            UserPayment::setUser($user);
            UserPayment::setOldPackage($ArrayOldUserGroup);
            UserPayment::setNewPackage($ArrayNewUserGroup);

            $ArrayPaymentPeriod = '';
            #if current package is pay as u go
            if ($ArrayOldUserGroup['CreditSystem'] == 'Disabled') {
                Payments::SetUser($user);
                $old_logID = Payments::GetCurrentPaymentPeriodIfExists();
                $ArrayPaymentPeriod = isset($old_logID) ? Payments::GetLog($old_logID) : '';
            }
            UserPayment::setCuurentPaymentPeriod($ArrayPaymentPeriod);

            if ($ArrayOldUserGroup['CreditSystem'] == 'Enabled' && $ArrayNewUserGroup['CreditSystem'] == 'Enabled') {
                $CurrentBalance = UserBalance::getUserBalance($user['UserID']);
                $UpgradeInfo = UserPayment::processCreditDiscount($CurrentBalance, $ArrayNewUserGroup);
            } else {
                $UpgradeInfo = UserPayment::processDiscount();
            }

            $send_payment = $UpgradeInfo['SendPayment'];

            if ($send_payment) {
                $TotalAmount = $UpgradeInfo['TotalAmount'];
                $Discount = $UpgradeInfo['Discount'];
                $Credits = $UpgradeInfo['Credits'];

                //redirects user to payment page to pay fees
                if ($ArrayOldUserGroup['CreditSystem'] == 'Enabled' && $ArrayNewUserGroup['CreditSystem'] == 'Enabled') {
                    $ArrayPaymentParameters = array(
                        'Process' => 'CreditPurchase',
                        'Credits' => $Credits,
                        'NewPackage' => $new_package,
                        'OldPackage' => $old_package,
                        'ServiceFee' => $TotalAmount,
                        'Discount' => $Discount,
                        'UserID' => $user["UserID"],
                        'IsCreditPurchase' => true,
                        'ReputationLevel' => 'Trusted',
                        'ErrorURL' => InterfaceAppURL(true) . '/user/credits/',
                    );
                } else {
                    $new_service_fee = $UpgradeInfo['ServiceFee'];
                    $discount = $UpgradeInfo['Discount'];

                    $ArrayPaymentParameters = array(
                        'Process' => 'Upgrade',
                        'NewPackage' => $new_package,
                        'OldPackage' => $old_package,
                        'ArrayNewUserGroup' => $ArrayNewUserGroup,
                        'ServiceFee' => $new_service_fee,
                        'Discount' => $discount,
                        'UserID' => $user["UserID"],
                        'IsCreditPurchase' => false,
                        'ReputationLevel' => 'Trusted',
                        'ErrorURL' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/upgrade/',
                    );
                }
                $_SESSION[SESSION_NAME]['CheckoutInformation'] = array();
                unset($_SESSION[SESSION_NAME]['CheckoutInformation']);

                $_SESSION[SESSION_NAME]['CheckoutInformation'] = serialize((object) $ArrayPaymentParameters);

                $link = InterfaceAppURL(true) . '/user/' . 'payment/checkout/';
                header('Location: ' . $link);
            } else {
                $link = Core::InterfaceAppURL() . '/' . self::$PluginCode . '/upgrade/';

                $Result = UserPayment::doUpgrade($UpgradeInfo, $new_package);

                if ($Result[0] == false) {
                    $error_code = $Result[2];
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['CheckOut'][$error_code]);
                } else {
                    return array(true, ApplicationHeader::$ArrayLanguageStrings['CheckOut']['0003']);
                }
            }
        }
        return array(true);
    }

    public function ui_invoices() {
        if (self::_PluginAppHeader('User') == false)
            return;

        $PageErrorMessage = '';
        $PageSuccessMessage = '';

        Core::LoadObject('payments');
        $ArrayUser = self::$UserInformation;
        Payments::SetUser($ArrayUser);

        //Retrieve payments - Start {
        $ArrayUserPayments = Payments::GetPaymentPeriods();
//        $ArrayViewData['Periods'] = $ArrayUserPayments;
        // Retrieve payments - End }

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0005'],
            'CurrentMenuItem' => 'Settings',
            'PluginView' => '../plugins/process/templates/invoices.php',
            'SubSection' => 'user_invoices_ug',
            'PluginLanguage' => self::$ArrayLanguage,
            'PluginCode' => self::$PluginCode,
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'Periods' => $ArrayUserPayments,
            'UserInformation' => $ArrayUser
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->render('user/settings', $ArrayViewData);
    }

    public function ui_invoice($UserID, $LogID) {

        if (self::_PluginAppHeader('User') == false)
            return;

        // Load required modules - Start {
        Core::LoadObject('payments');
        // Load required modules - End }
        // Retrieve user account - Start {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);

        if ($ArrayUser == false) {
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user', 'location', '302');
        }
        Payments::SetUser($ArrayUser);
        // Retrieve user account - End }
        // Retrieve log details - Start {
        $ArrayLog = Payments::GetLog($LogID);
        // Retrieve log details - End }
        // Interface parsing - Start
//        $ArrayPaymentParameters = array(
//            'PaymentLogID' => $LogID,
//            'UserID' => $UserID,
//            'PaymentGateway' => 'PayPal_Express',
//        );
//        $pay_link = APP_URL . 'payment.php?p=' . Core::EncryptURL($ArrayPaymentParameters);

        $ArrayPaymentParameters = array(
            'PaymentLogID' => $LogID,
            'UserID' => $UserID
        );
//        $pay_link = APP_URL . 'payment.php?p=' . Core::EncryptURL($ArrayPaymentParameters);

        $pay_link = Core::InterfaceAppURL() . '/' . self::$PluginCode . '/pay/' . $LogID . "/" . $UserID;

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . self::$ArrayLanguage['Screen']['0006'],
            'CurrentMenuItem' => 'user_invoices_ug',
            'PluginView' => '../plugins/process/templates/invoice.php',
            'PluginLanguage' => self::$ArrayLanguage,
            'PluginCode' => self::$PluginCode,
            'User' => $ArrayUser,
            'Period' => $ArrayLog,
            'PayLink' => $pay_link,
            'UserInformation' => $ArrayUser,
            'SubSection' => 'user_invoices_ug'
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        self::$ObjectCI->plugin_render(self::$PluginCode, 'invoice', $ArrayViewData, true);
    }

    public function ui_pay($LogID, $UserID) {

        if (self::_PluginAppHeader('User') == false)
            return;

        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);

        if ($ArrayUser == false) {
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user', 'location', '302');
        }
        Core::LoadObject('payments');

        Payments::SetUser($ArrayUser);
        $ArrayLog = Payments::GetLog($LogID);

        if ($ArrayLog == false) {
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user', 'location', '302');
        }

        $ArrayPaymentParameters = array(
            'Process' => 'PayInvoice',
            'UserID' => $UserID,
            'PaymentLogID' => $LogID,
            'ServiceFee' => $ArrayLog['ChargeSystemPeriod'],
            'Discount' => $ArrayLog['Discount'],
            'IsCreditPurchase' => false,
            'ReputationLevel' => 'Trusted',
            'ErrorURL' => Core::InterfaceAppURL() . '/' . self::$PluginCode . '/invoice/' . $UserID . "/" . $LogID,
        );

        $_SESSION[SESSION_NAME]['CheckoutInformation'] = array();
        unset($_SESSION[SESSION_NAME]['CheckoutInformation']);

        $_SESSION[SESSION_NAME]['CheckoutInformation'] = serialize((object) $ArrayPaymentParameters);

        $link = InterfaceAppURL(true) . '/user/' . 'payment/checkout/';
        header('Location: ' . $link);
    }

    // -------------------------------------
    // Events (events of the user interface such as save, create, delete, etc.)
    // -------------------------------------
    // i will write my register code here // add user and login him
    private function _EventRegister() {

        if (!empty($_POST)) {
            $valid = Validation::get()->verifyRequiredParams(
                    array(
                "first_name", "last_name", "phone", "email", "package", "user_name", "password")
                    , $_POST
            );

            if (!$valid[0]) {
                return array(false, self::$ArrayLanguage['Common']['0001'] . $valid[1]);
            }

            $admin_api = new AdminAPI();
            $users_api = new UsersAPI();

            //login admin to get session id to use in all subsequent api's calls
            $login = $admin_api->login();
            if (!$login[0]) {

                return array(false, self::$ArrayLanguage["'" . $admin_api->getCommand() . "'"]["'" . $login[1] . "'"]);
            }

            //admin session id
            $admin_session_id = $login[1];

            $users_api->setAdminSessionId($admin_session_id);

            // Get selected usergroup info
            $ArrayUserGroup = UserGroups::RetrieveUserGroup($_POST['package']);
            $service_fee = empty($ArrayUserGroup['PaymentSystemChargeAmount']) ? 0 : $ArrayUserGroup['PaymentSystemChargeAmount'];
            $reputation_level = $service_fee == 0 ? 'Trusted' : 'Untrusted';

            //prepare create user data
            $data = array(
                "first_name" => htmlspecialchars($_POST['first_name']),
                "last_name" => htmlspecialchars($_POST['last_name']),
                "phone" => htmlspecialchars($_POST['phone']),
                "organization_name" => isset($_POST['organization_name']) ? htmlspecialchars($_POST['organization_name']) : '',
                "email" => htmlspecialchars($_POST['email']),
                "package" => htmlspecialchars($_POST['package']),
                "user_name" => htmlspecialchars($_POST['user_name']),
                "password" => htmlspecialchars($_POST['password']),
                "language" => 'en',
                "reputation_level" => $reputation_level,
            );
            //create user
            $create = $users_api->createUser($data);
            if (!$create[0]) {
                return array(false, self::$ArrayLanguage[$users_api->getCommand()][$create[1]]);
            }
            $UserID = $create[1];
            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
            //get all user data using UserID
            if ($ArrayUser == false) {
                return array(false, self::$ArrayLanguage['Screen']['0004']);
            }

            //send email to user
            $directory = PLUGIN_PATH . self::$PluginCode . '/templates/';
            $file = 'register.html';
            if (file_exists($directory . $file) && strstr($file, '.html')) {
                $message = file_get_contents($directory . $file);
                $message = str_replace('<span id="UserFullName"> Eman</span>', '<span id="UserFullName">' . htmlspecialchars($_POST['first_name']) . '</span>', $message);
                $message = str_replace('<span id="userName"></span>', '<span id="userName">' . htmlspecialchars($_POST['user_name']) . '</span>', $message);
                $message = str_replace('<span id="password"></span>', '<span id="password">' . htmlspecialchars($_POST['password']) . '</span>', $message);
                $message = str_replace('<span id="UserGroupName"></span>', '<span id="UserGroupName">' . $ArrayUser['GroupInformation']['GroupName'] . '</span>', $message);
                $message = str_replace('<span id="GroupName" style="font-weight:bold" >Free 500 Plan </span>', '<span id="GroupName"  style="font-weight:bold">' . $ArrayUser['GroupInformation']['GroupName'] . '</span> package ', $message);
                $message = str_replace('<span id="userEmail"></span>', '<span id="userEmail">' . htmlspecialchars($_POST['email']) . '</span>', $message);
                if ($service_fee > 0) {
                    $message = str_replace('<span id="FreeMoreText">Or Upgrade from Setting to other Specific Plan</span>', '<span id="FreeMoreText"></span>', $message);
                }
            }
            $to = htmlspecialchars($_POST['email']);
            $subject = "Welcome " . htmlspecialchars($_POST['first_name']);

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: FlyingList <support@flyinglist.com>' . "\r\n";

            $mail = mail($to, $subject, $message, $headers);

            if (Payments::IsPaymentSystemEnabled(true) == true && $service_fee > 0) {

                // Check if payment period for this user exists for the current period,if not,create invoice
                Payments::SetUser($ArrayUser);
                Payments::CheckIfPaymentPeriodExists();

                //Payment update invoice Start
                //get payment LogID unique identifier
                $ArrayPaymentPeriod = Payments::GetLog();
                $LogID = $ArrayPaymentPeriod["LogID"];

                //update invoice with service total if usergroup has service fees
                $ArrayReturn = API::call(array(
                            'format' => 'JSON',
                            'command' => 'user.paymentperiods.update2',
                            'protected' => true,
                            'username' => $ArrayUser['Username'],
                            'password' => $ArrayUser['Password'],
                            'parameters' => array(
                                'userid' => $ArrayUser["UserID"],
                                'logid' => $LogID,
                                'discount' => 0,
                                'includetax' => 'Exclude',
                                'paymentstatus' => "Unpaid",
                                'sendreceipt' => "No",
                            )
                ));
                if ($ArrayReturn['Success'] == false) {
//                    $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
                    return array(false, self::$ArrayLanguage['Screen']['0022']);
                }
//                $PaymentPeriod = $ArrayReturn['PaymentPeriod'];
            }
            //Payment update invoice End
            //login user Start
            $data_login = array(
                "user_name" => htmlspecialchars($_POST['user_name']),
                "password" => htmlspecialchars($_POST['password']),
                "remember_me" => 'yes',
                "disable_captcha" => true,
            );
            $login_user = $users_api->loginUser($data_login);
            if (!$login_user[0]) {
                return array(false, self::$ArrayLanguage[$users_api->getCommand()][$login_user[1]]);
            }
            $user = $login_user[1];

            UserAuth::Login($user->UserID, $user->Username, $user->Password);

            // Plug-in hook - Start
            Plugins::HookListener('Action', 'User.Login.Post', array($user->UserID));
            // Plug-in hook - End
            self::$UserInformation = (array) $user;
            //login user End
            //redirects user to payment page to pay fees
            $ArrayPaymentParameters = array(
                'PaymentLogID' => $LogID,
                'UserID' => $UserID,
                'PaymentGateway' => 'PayPal_Express',
                'IsCreditPurchase' => false,
                'ReputationLevel' => 'Trusted',
            );

            // Create Automation list - Start {
            $ArrayFieldAndValues = array(
                'Name' => 'Automation List',
                'RelOwnerUserID' => self::$UserInformation['UserID'],
                'OptInMode' => 'Single',
                'IsAutomationList' => 'Yes',
                'OptOutSubscribeTo' => 0,
                'OptOutUnsubscribeFrom' => 0,
                'OptOutScope' => 'All lists',
                'OptOutAddToSuppressionList' => 'No',
                'OptOutAddToGlobalSuppressionList' => 'Yes',
            );
            $NewSubscriberListID = Lists::Create($ArrayFieldAndValues);
            // Plug-in hook - Start
            Plugins::HookListener('Action', 'List.Create.Post', array($NewSubscriberListID));
            // Plug-in hook - End
            Subscribers::CreateCustomGeoColumns($NewSubscriberListID);
            // Create Automation list - End }

            if (Payments::IsPaymentSystemEnabled(false) == true && $service_fee > 0) {
                $link = APP_URL . 'payment.php?p=' . Core::EncryptURL($ArrayPaymentParameters);
                header('Location: ' . $link);
            } else {
                header('Location: ' . InterfaceAppURL(true) . '/user/');
            }
        }
        return array(true);
    }

    // -------------------------------------
    // Private methods
    // -------------------------------------
    // This method should be called in every user interface method (controller) as the
    // first line. You can force controller to be executed for "Admin" or "User" authorized
    // account owners.
    private function _PluginAppHeader($Section = 'Admin') {
        self::$ObjectCI = & get_instance();

        // License status check
        // TODO: License status check will be processed here in the future

        if (Plugins::IsPlugInEnabled(self::$PluginCode) == false) {
            $Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
            self::$ObjectCI->display_public_message('', $Message);
            return false;
        }

        if ($Section == 'Admin') {
            self::_CheckAdminAuth();
        } elseif ($Section == 'User') {
            self::_CheckUserAuth();
        }

        return true;
    }

    // This is a private method for checking logged in user administrator privileges
    private function _CheckAdminAuth() {
        Core::LoadObject('admin_auth');
        Core::LoadObject('admins');

        AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');

        self::$AdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));

        return;
    }

    // This is a private method for checking logged in user privileges
    private function _CheckUserAuth() {
        Core::LoadObject('user_auth');

        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');

        self::$UserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);

        if (Users::IsAccountExpired(self::$UserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }

        return;
    }

    // Models defined in self::$LoadedModels will be loaded here.
    private function _LoadModels() {
        include_once('models/base.php');
        self::$Models = new stdClass();

        self::$LoadedModels = array_merge(array('user', 'user_balance'), self::$LoadedModels);

        $InitiatedModels = array();

        if (count(self::$LoadedModels) > 0) {
            foreach (self::$LoadedModels as $EachModel) {
                if (file_exists(PLUGIN_PATH . self::$PluginCode . '/models/' . $EachModel . '.php') == false)
                    continue;
                if (isset($InitiatedModels[$EachModel]) == true)
                    continue;

                include_once('models/' . $EachModel . '.php');
                $ClassName = 'model_' . $EachModel;
                self::$Models->{$EachModel} = new $ClassName();
                self::$Models->{$EachModel}->Models = self::$Models;

                $InitiatedModels[$EachModel] = true;
            }
        }
    }

}
