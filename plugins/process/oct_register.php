<?php
/**
 * User Register Plug-In For Oempro4
 * Name: User Register
 * Description: this provides user register page
 * Minimum Oempro Version: 4.6.2
 */

include_once(PLUGIN_PATH . 'oct_register/main.php');
