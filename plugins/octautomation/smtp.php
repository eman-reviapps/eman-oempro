<?php
function SMTP_SendMessage($MailServerSettings, $UserID, $MessageID, $PersonID, $To, $FromName, $FromEmail, $ReplyToName, $ReplyToEmail, $Subject, $PlainContent, $HTMLContent)
{
	$Transport = Swift_SmtpTransport::newInstance();
	$Transport->setHost($MailServerSettings->Hostname);
	$Transport->setPort($MailServerSettings->Port);
	$Transport->setEncryption($MailServerSettings->Encryption);
	$Transport->setUsername($MailServerSettings->Username);
	$Transport->setPassword($MailServerSettings->Password);
	$Transport->setTimeout($MailServerSettings->Timeout);

	$Mailer = Swift_Mailer::newInstance($Transport);
	$Message = Swift_Message::newInstance();

	$Message->setFrom(array($FromEmail => $FromEmail));
	$Message->setReplyTo(array($ReplyToEmail => $ReplyToEmail));
	$Message->setCharset('UTF-8');
	$Message->setPriority(3);

	// Set other email headers
	$EncryptedUserID = sl_encrypt_number($UserID, 5);

	$Message->setSubject($Subject);

	$Message->setBody($HTMLContent, 'text/html', 'UTF-8');
	$Message->addPart($PlainContent, 'text/plain', 'UTF-8');

	$Message->setTo($To);
	try {
		$Result = $Mailer->send($Message);
	} catch (Exception $e) {
		return false;
	}

	if ($Result === false)
	{
		return false;
	}
	else
	{
		return $MessageID;
	}
}
