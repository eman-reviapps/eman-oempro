<?php

function FormatPeopleColumnValue($ColumnName = '', $ColumnValue = '', $CountryList = array())
{
	if ($ColumnValue == '')
	{
		return '<span style="color:#ccc;"><em>n/a</em></span>';
	}

	if (preg_match('/date$/i', $ColumnName) > 0)
	{
		$TimedValue = strtotime($ColumnValue);

		if ($TimedValue == false || $TimedValue < 0)
		{
			return $ColumnValue;
		}
		else
		{
			return date("jS M'y, g:ia", $TimedValue);
		}
	}
	elseif ($ColumnName == 'Country')
	{
		if (count($CountryList) > 0 && isset($CountryList[$ColumnValue]) == true)
		{
			return $CountryList[$ColumnValue];
		}
		else
		{
			return $ColumnValue;
		}
	}
	else
	{
		return $ColumnValue;
	}
}
