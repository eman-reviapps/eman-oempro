<?php
/**
 * Email Automation plugin for Oempro
 * Name: Email Automation Plugin
 * Description: Automate your customer lifecycle emailings with this plugin.
 * Minimum Oempro Version: 4.6.1
 */

include_once(PLUGIN_PATH . 'octautomation/main.php');
