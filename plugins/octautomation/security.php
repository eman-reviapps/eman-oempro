<?php

define('SECURE_PASSWORD_SALT','3^Th#*Ru@@X9K239823jkskjds!32983293822lkewlksdkjsd@!gdxg');
define('SECURE_ENCRYPTION_KEY','W9w4*y$NCXbd29382938230293lekskdjskdnsmd232938239*8H$');
define('SECURE_PASSWORD_LOOP_LIMIT',30);
define('HASH_CYCLE_LIMIT',20000);
define('XOR_KEY', 643545783209832); //  for EncryptNumber

function pseudoRandomKey($size) {

    if (function_exists('openssl_random_pseudo_bytes')) {
        $rnd = openssl_random_pseudo_bytes($size, $strong);
        if ($strong === TRUE) {
            return $rnd;
        }
    }
    $sha='';
    $rnd='';
    for ($i=0;$i<$size;$i++) {

        $sha= hash('sha256',$sha.mt_rand());
        $char= mt_rand(0,62);
        $rnd .= chr(hexdec($sha[$char].$sha[$char+1]));

    }
	return $rnd;
}

/**
 * Create password hash with SHA 512
 * @param string $password
 * @return string
 */
function  securePassword($password) {
	$hash='';
	$salt = substr(base64_encode(SECURE_PASSWORD_SALT),0,22);

	for ($i=0;$i<SECURE_PASSWORD_LOOP_LIMIT;$i++) {
		$hash = hash('sha512', $hash.$salt.$password);
	}

	return $hash;
}

/**
 * Check hash with password
 * @param string $password
 * @param string $hash
 * @return bool
 */
function validPassword($password, $hash) {
	$salt = substr(base64_encode(SECURE_PASSWORD_SALT),0,22);
	$newHash='';

	for ($i=0;$i<SECURE_PASSWORD_LOOP_LIMIT;$i++) {
		$newHash= hash('sha512',$newHash.$salt.$password);
	}
	return ($newHash==$hash);
}


/**
 * Encrypt with AES 256
 *
 * @param string $Text
 * @return string
 */
function EncryptText($Text) {

	$ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	$iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);

	$encrypted = mcrypt_encrypt(
		MCRYPT_RIJNDAEL_128,
		SECURE_ENCRYPTION_KEY,
		$Text,
		MCRYPT_MODE_CBC,
		$iv
	);

	$encrypted = base64_encode($encrypted).'[|]'.base64_encode($iv);

	return $encrypted;
}
/**
 * Decrypt with AES 256
 *
 * @param string $Text
 * @return string
 */
function DecryptText($Text) {

	$Text = explode('[|]',$Text);

	$Data	= base64_decode($Text[0]);
	$iv		= base64_decode($Text[1]);

	$decrypted = mcrypt_decrypt(
		MCRYPT_RIJNDAEL_128,
		SECURE_ENCRYPTION_KEY,
		$Data,
		MCRYPT_MODE_CBC,
		$iv
	);

	return trim($decrypted,"\0");
}



/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Number Encryption functions START - {
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
function bitxor($o1, $o2) {
	$xorWidth = PHP_INT_SIZE*8;
	$o1 = str_split($o1, $xorWidth);
	$o2 = str_split($o2, $xorWidth);
	$res = '';
	$runs = count($o1);
	for($i=0;$i<$runs;$i++)
		$res .= decbin(bindec($o1[$i]) ^ bindec($o2[$i]));
	return $res;
}
/**
 * Encrypt decimal number
 * @param $Number (max value 999.999.999)
 * @return string
 */
function _EncryptNumber($Number) {

	$Chksum = calcChecksum($Number);

	$Key	= str_pad(decbin(XOR_KEY),	32, "0", STR_PAD_LEFT);
	$Number	= str_pad(decbin($Number),	32, "0", STR_PAD_LEFT);

	$Bit = bitxor($Key, $Number);
	$Dec = bindec($Bit);

	$Base36 = base_convert($Dec,10,36);

	return strtoupper($Base36).$Chksum;
}
/**
 * Decrypt an encrypted decimal number
 * @param $String
 * @return bool|number|string
 */
function _DecryptNumber($String) {

	$Chksum = substr($String,-2);
	$String = substr($String,0,-2);

	$Dec = base_convert($String,36,10);

	$Key	= str_pad(decbin(XOR_KEY),	32, "0", STR_PAD_LEFT);
	$String	= str_pad(decbin($Dec),		32, "0", STR_PAD_LEFT);

	$Bit = bitxor($Key,$String);
	$Dec = bindec($Bit);

	$RealChksum = calcChecksum($Dec);

	if ($RealChksum != $Chksum) {
		return false;
	}

	return $Dec;
}


function calcChecksum($Dec) {

	// Calc Checksum - START {
	$ChkNumberString =(string)$Dec;
	$ChkNumberLength = strlen($ChkNumberString);
	$RoundCount = 0;
	do {

		$Total = 0;
		for ($s=0;$s<$ChkNumberLength;$s++) {
			$Total += (int)substr($ChkNumberString,$s,1);
		}

		$ChkNumberString = (string)$Total;
		$ChkNumberLength = strlen($ChkNumberString);
		$RoundCount++;

	} while ($Total > 9);
	$RoundCount32 = base_convert($RoundCount+9,10,36);
	$RealChksum = strtoupper($RoundCount32.$Total);
	// Calc Checksum - FINISH }

	return $RealChksum;
}


function EncryptNumber($Number) {

	$Encrypted = _EncryptNumber($Number);

	$Chksum = substr($Encrypted,-2);
	$ChksumArray = str_split($Chksum);

	$Data = substr($Encrypted,0,-2);

	$DataArray = str_split($Data);
	$NumericTotal = 0;

	foreach ($DataArray as $Key) {
		$NumericTotal = base_convert($Key,36,10);
	}

	$DataArray2 = $DataArray;

	$DataCount = count($DataArray);
	$Result = array();

	if ($NumericTotal %2) {

		$DataArray[0] = $DataArray2[2];
		$DataArray[1] = $DataArray2[3];
		$DataArray[2] = $DataArray2[0];
		$DataArray[3] = $DataArray2[1];

	} else {

		$DataArray[0] = $DataArray2[1];
		$DataArray[1] = $DataArray2[0];
		$DataArray[2] = $DataArray2[3];
		$DataArray[3] = $DataArray2[2];
		array_reverse($ChksumArray);
	}

	foreach($DataArray as $Key => $DataChar) {
		if ($Key == 1) {
			$Result[] = $ChksumArray[0];
		}
		if ($Key == ($DataCount-1)) {
			$Result[] = $ChksumArray[1];
		}
		$Result[] = $DataChar;
	}

	return implode('',$Result);
}

function DecryptNumber($String) {

	$StringArray = str_split($String);
	$StringCount = count($StringArray);
	$DataArray = array();
	$ChksumArray = array();
	$NumericTotal = 0;

	foreach ($StringArray as $Key => $Char) {

		if ($Key == 1) {

			$ChksumArray[] = $Char;

		} elseif ($Key == ($StringCount-2)) {

			$ChksumArray[] = $Char;

		} else {

			$DataArray[] = $Char;
			$NumericTotal = base_convert($Char,36,10);
		}
	}

	$DataArray2 = $DataArray;

	if ($NumericTotal %2) {

		$DataArray[2] = $DataArray2[0];
		$DataArray[3] = $DataArray2[1];
		$DataArray[0] = $DataArray2[2];
		$DataArray[1] = $DataArray2[3];

	} else {

		$DataArray[1] = $DataArray2[0];
		$DataArray[0] = $DataArray2[1];
		$DataArray[3] = $DataArray2[2];
		$DataArray[2] = $DataArray2[3];
		array_reverse($ChksumArray);
	}

	return _DecryptNumber(implode('',$DataArray).implode('',$ChksumArray));
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Number Encryption functions FINISH - }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


