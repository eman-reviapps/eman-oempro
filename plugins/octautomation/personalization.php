<?php
class Personalization
{
	function __construct()
	{
	}

	function Engage_PersonalizeWithSubscriberInfo($StringToPersonalize, $Recipient, $Columns)
	{
		$TMPSearchList = array();
		$TMPReplaceList = array();

		foreach ($Columns as $Field=>$Name)
		{
			$TMPSearchList[] = '/%' . $Name . '%/';
			$TMPReplaceList[] = (isset($Recipient->{$Field}) == true ? $Recipient->{$Field} : '');
		}

		$StringToPersonalize = preg_replace($TMPSearchList, $TMPReplaceList, $StringToPersonalize);

		return $StringToPersonalize;
	}

	function Engage_FindAllLinksInHTML($String)
	{
		// Initialize variables - Start
		$ArrayLinks			= array();
		// Initialize variables - End

		// Insert each <a> into new line - Start
		$NewString = str_replace("</a>", "</a>\n", $String);
		// Insert each <a> into new line - End

		// Find all links - Start
		$TMPPattern = "/<a(.*)>.*<\/a>/isU";
		preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);
		// Find all links - End

		// Find "href" and "title" parameters inside links - Start
		$TMPCounter = 0;

		foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
			{
			// Find the title of the link - Start
			$SearchOn = $ArrayEachMatch[1];
			$TMPPattern = "/title=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			$ArrayLinks[$TMPCounter]['Title']		= $ArraySubMatches[0][1];
			$ArrayLinks[$TMPCounter]['FullTitle']	= $ArraySubMatches[0][0];
			// Find the title of the link - End

			// Find the link of the link - Start
			$TMPPattern = "/href=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			$ArrayLinks[$TMPCounter]['Link']	= $ArraySubMatches[0][1];
			$ArrayLinks[$TMPCounter]['FullLink']= $ArraySubMatches[0][0];
			// Find the link of the link - End

			// Find the nochange parameter in the link - Start
			$TMPPattern = "/class=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			if (count($ArraySubMatches) > 0)
				{
				if (strpos('no-link-track', $ArraySubMatches[0][1]) !== false)
					{
					$ArrayLinks[$TMPCounter]['NoLinkTrack']		= true;
					}
				}
			// Find the nochange parameter in the link - End

			$ArrayLinks[$TMPCounter]['AllLink']	= $ArrayEachMatch[0];

			$TMPCounter++;
			}
		// Find "href" and "title" parameters inside links - End

		// Find map links - Start
		$TMPPattern = "/<area(.*)>/isU";
		preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);

		foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
			{
			// Find the title of the link - Start
			$SearchOn = $ArrayEachMatch[1];
			$TMPPattern = "/title=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			$ArrayLinks[$TMPCounter]['Title']	= $ArraySubMatches[0][1];
			$ArrayLinks[$TMPCounter]['FullTitle']= $ArraySubMatches[0][0];
			// Find the title of the link - End

			// Find the link of the link - Start
			$TMPPattern = "/href=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			$ArrayLinks[$TMPCounter]['Link']	= $ArraySubMatches[0][1];
			$ArrayLinks[$TMPCounter]['FullLink']= $ArraySubMatches[0][0];
			// Find the link of the link - End

			// Find the nochange parameter in the link - Start
			$TMPPattern = "/class=\"([^\r\n]*)\"/iU";
			preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

			if (count($ArraySubMatches) > 0)
				{
				if (strpos('no-link-track', $ArraySubMatches[0][1]) !== false)
					{
					$ArrayLinks[$TMPCounter]['NoLinkTrack']		= true;
					}
				}
			// Find the nochange parameter in the link - End

			$ArrayLinks[$TMPCounter]['AllLink']	= $ArrayEachMatch[0];

			$TMPCounter++;
			}
		// Find map links - End

		return $ArrayLinks;
	}	

	function Engage_TrackLinks($StringToPersonalize, $UserID, $MessageID, $PersonID)
	{
		$ArrayLinks = $this->Engage_FindAllLinksInHTML($StringToPersonalize);

		// Loop each detected link - Start
		foreach ($ArrayLinks as $EachIndex=>$ArrayEachLink)
		{
			// If link is not anchor (#) or mailto: or system link or set not to be tracked , proceed - Start
			if ((((isset($ArrayEachLink['NoLinkTrack']) == false) || ($ArrayEachLink['NoLinkTrack'] != true))) && (substr($ArrayEachLink['Link'], 0, 1) != '#') && (strtolower(substr($ArrayEachLink['Link'], 0, 7)) != 'mailto:'))
			{
				// Prepare detected links and their titles - Start
				$TMPLinkTitle	= $ArrayEachLink['Title'];
				$TMPLinkURL		= $ArrayEachLink['Link'];
				// Prepare detected links and their titles - End

				// Encrypted query parameters - Start
				$EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array($UserID, $MessageID, $PersonID));
				$EncryptedQuery .= '/'.rawurlencode(base64_encode($TMPLinkTitle)).'/'.rawurlencode(base64_encode($TMPLinkURL));
				// Encrypted query parameters - End

				// Register the link  - Start
				$ReplaceString = Core::InterfaceAppURL().'/octautomation/tl/'.$EncryptedQuery;
				// Register the link  - End

				// Replace link href with the new one in the link - Start
					$TMPPattern 	= $ArrayEachLink['FullLink'];
				$TMPAllLink = str_replace($TMPPattern, 'href="'.$ReplaceString.'"', $ArrayEachLink['AllLink']);
				// Replace link href with the new one in the link - End

				// Replace the link in the string - Start
					$TMPPattern 	= $ArrayEachLink['AllLink'];
					$TMPReplace 	= $TMPAllLink;
				$StringToPersonalize = str_replace($TMPPattern, $TMPReplace, $StringToPersonalize);
				// Replace the link in the string - End
				}
			// If link is not anchor (#) or mailto: or system link, proceed - End
			}
		// Loop each detected link - End

		return $StringToPersonalize;
	}

	function Engage_TrackOpens($StringToPersonalize, $UserID, $MessageID, $PersonID)
	{
		$EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array($UserID, $MessageID, $PersonID));
		$OpenTrackURL = '<img src="'.Core::InterfaceAppURL().'/octautomation/to/'.$EncryptedQuery.'" width="5" height="2" alt=".">';
		return preg_replace('/<\/body>/i', "\n".$OpenTrackURL."\n\n</body>", $StringToPersonalize);
	}

}
