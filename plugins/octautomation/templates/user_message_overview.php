<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>


<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/flot/excanvas.min.js"></script><![endif]-->

<?php include_once('message-flot-chart.php'); ?>
<?php include_once('message-pie-chart.php'); ?>
<?php include_once('message-knob-dial.php'); ?>
<?php include_once('message-geo-location.php'); ?>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(false); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/profile-2.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php // include('user_header.php'); ?>

<?php include('user_message_header.php'); ?>
<?php // print_r($Message)?>

<div class="page-content-inner">
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-12">
            <div class="col-md-5 pull-left" style="background: #fff" >
                <div class="portlet sale-summary" style="">
                    <div class="portlet-title">
                        <div class="caption font-purple-sharp sbold"> Info </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="list-unstyled">
                            <li>
                                <span class="sale-info sale-info-custom bold"> Subject 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> <?php print($Message->Subject); ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Sender 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <?php // print_r($CampaignInformation) ?>
                                <span class="sale-num sale-num-custom"> <?php print($Message->FromName); ?> &lt;<?php print($Message->FromEmail); ?>&gt; </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Status 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> 
                                    <?php print($PluginLanguage['Screen']['0075'][$Message->Status]); ?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pull-right">
                <div class="portlet portlet-fit ">
                    <div class="portlet-body">
                        <div class="col-md-6" style="padding: 0">
                            <div style="display:inline;width:200px;height:200px;"> 
                                <input class="knob" data-fgColor="#1ac6ff" data-skin="tron" style="border: 0px; -webkit-appearance: none; background: none;">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet sale-summary">
                                <div class="portlet-title">
                                    <div class="caption font-purple-sharp sbold"> Deliverability </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0147', false); ?>
                                                <i class="fa fa-img-up"></i>
                                            </span>
                                            <span class="sale-num bold"> <?php echo $Message->TotalSent; ?></span> </span>
                                        </li>
                                        <!--                                        <li>
                                                                                    <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0953', false, '', false, false); ?>
                                                                                        <i class="fa fa-img-down"></i>
                                                                                    </span>
                                                                                    <span class="sale-num bold"> <?php echo $Message->TotalSent; ?> </span>
                                                                                </li>-->
                                        <li>
                                            <span class="sale-info bold"> <?php InterfaceLanguage('Screen', '0954', false, '', false, false); ?> </span>
                                            <span class="sale-num bold"> <?php echo $Message->TotalHardBounced; ?> </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <!--<span data-counter="counterup" data-value="1450">1450</span>-->
                            <a href="<?php InterfaceAppURL(); ?>/octautomation/reports/<?php echo $Message->MessageID; ?>/opens"><span data-counter="counterup" data-value="<?php print(number_format($Message->TotalOpened, 0)); ?>"><?php print(number_format($Message->TotalOpened, 0)); ?></span></a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0837') ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $Message->OpenedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/octautomation/reports/<?php echo $Message->MessageID; ?>/clicks"<span data-counter="counterup" data-value="<?php print(number_format($Message->TotalClicked, 0)); ?>"><?php print(number_format($Message->TotalClicked, 0)); ?></span></a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0838') ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $Message->ClickedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat2 ">
                <div class="display separator">
                    <div class="number">
                        <h3>
                            <a href="<?php InterfaceAppURL(); ?>/octautomation/reports/<?php echo $Message->MessageID; ?>/unsubscriptions"<span data-counter="counterup" data-value="<?php print(number_format($Message->TotalUnsubscribed, 0)); ?>"><?php print(number_format($Message->TotalUnsubscribed, 0)); ?></span></a>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0097', false, '', false, true); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $Message->UnsubscriptionRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3>
                            <span data-counter="counterup" data-value="<?php print(number_format($Message->TotalHardBounced, 0)); ?>"><?php print(number_format($Message->TotalHardBounced, 0)); ?></span>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '1113', false, '', false, true); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $Message->HardBouncedRatio ?>% </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3>
                            <span data-counter="counterup" data-value="<?php print(number_format($Message->TotalSpamComplaint, 0)); ?>"><?php print(number_format($Message->TotalSpamComplaint, 0)); ?></span>
                        </h3>
                        <small><?php InterfaceLanguage('Screen', '0961', false, '', false, false); ?></small>
                    </div>
                </div>
                <div class="status">
                    <div class="status-title"> <?php echo $Message->SpamRatio ?>% </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet light  portlet-fit">
                <div class="portlet-title">
                    <div class="caption" style="padding: 0">
                        <div class="form-group" style="margin-bottom: 0">
                            <select class="form-control input-small" id="DaysSelect">
                                <option value="7" selected>7 days</option>
                                <option value="15">15 days</option>
                                <option value="30">30 days</option>
                            </select>
                        </div>
                    </div>
                    <div class="actions" style="padding: 0">
                        <div class="form-group" style="margin-bottom: 0">
                            <div class="mt-checkbox-inline" style="padding: 0;padding-top: 5px">
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxOpens" value="<?php InterfaceLanguage('Screen', '0837') ?>"> <?php InterfaceLanguage('Screen', '0837') ?>
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxClicks" value="<?php InterfaceLanguage('Screen', '0838') ?>"> <?php InterfaceLanguage('Screen', '0838') ?>
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-custom mt-checkbox-outline" style="margin-bottom: 0">
                                    <input type="checkbox" checked name="CheckboxGraph" id="CheckboxUnsubscriptions" value="<?php InterfaceLanguage('Screen', '0841') ?>"> <?php InterfaceLanguage('Screen', '0841') ?>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart_3" class="chart" style="padding: 0px; position: relative;">
                        <canvas class="flot-base" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                        <canvas class="flot-overlay" width="100%" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100%; height: 300px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light  portlet-fit">
                <div class="portlet-body">
                    <div id="region_statistics_loading">
                        <img src="<?php InterfaceTemplateURL(); ?>assets/global/img/loading.gif" alt="loading" /> 
                    </div>
                    <div id="region_statistics_content" class="display-none">
                        <div class="btn-toolbar margin-bottom-10">
                            <div class="btn-group btn-group-circle" data-toggle="buttons">
                                <a href="" id="open_geo_home" class="btn grey-salsa btn-sm active"> Open </a>
                                <!--<a href="" id="click_geo_home" class="btn grey-salsa btn-sm"> Click </a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a href="" class="btn btn-circle grey-salsa btn-sm" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                    <span id="option_selected" >All</span>
                                    <span class="fa fa-angle-down"> </span>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" id="view_geo_all"> All </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_3_month"> Last 3 month </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_6_month"> Last 6 month </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" id="view_geo_last_year"> Last year  </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="vmap_world" class="vmaps display-none"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

        </div>
        <div class="col-md-6">
            <div class="portlet">
                <div class="portlet-body">
                    <div id="donut" class="chart" style="height: 250px;padding: 0px; position: relative;margin-top: 50px">

                    </div>
                </div>
            </div>
        </div> 
    </div>
<!--    <div class="row">
        <div class="tabbable-line tab-custom" style="margin-top: 20px">
            <ul class="nav nav-tabs">
                <?php // if (defined(GEO_LOCATION_DATA_PATH) && file_exists(GEO_LOCATION_DATA_PATH) == true): ?>
                <li class="active">
                    <a href="#tab-locations" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0093']); ?> </a>
                </li>
                <?php // endif; ?>
                <li>
                    <a href="#tab-opens" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0094']); ?> </a>
                </li>
                <li>
                    <a href="#tab-clicks" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0095']); ?> </a>
                </li>
                <li>
                    <a href="#tab-optouts" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0096']); ?> </a>
                </li>
            </ul>
            <div class="tab-content" style="background-color: transparent">
                <?php // if (defined(GEO_LOCATION_DATA_PATH) && file_exists(GEO_LOCATION_DATA_PATH) == true): ?>
                <div class="tab-pane active" id="tab-locations">
                    <?php if (is_array($GeoOpenStats) == true && count($GeoOpenStats) > 0): ?>
                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                            <?php
                            foreach ($GeoOpenStats as $CountryCode => $EachCity):
                                $CountryOpens = 0;
                                foreach ($EachCity as $City => $Detections) {
                                    $CountryOpens += $Detections;
                                }
                                ?>
                                <tr>
                                    <td style="background-color:#DCE8F1;" width="200"><strong><?php print($CountryList[$CountryCode]); ?></strong></td>
                                    <td style="background-color:#DCE8F1;" width="100" class="right-align"><span class="data"><?php print(number_format($CountryOpens, 0)); ?></span> <?php print($PluginLanguage['Screen']['0097']); ?></td>
                                    <td style="background-color:#DCE8F1;">&nbsp;</td>
                                </tr>
                                <?php foreach ($EachCity as $City => $Detections): ?>
                                    <tr>
                                        <td><span style="padding-left:20px;"><?php print($City); ?></span></td>
                                        <td class="right-align"><span class="data"><?php print(number_format($Detections, 0)); ?></span> <?php print($PluginLanguage['Screen']['0097']); ?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <div class="white">
                            <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0098']); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
                <?php // endif; ?>
                <div class="tab-pane" id="tab-opens">
                    <?php if (is_array($Message_OpenLog) == true && count($Message_OpenLog) > 0): ?>
                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                            <?php
                            foreach ($Message_OpenLog as $Index => $EachEntry):
                                ?>
                                <tr>
                                    <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                    <td class="right-align"><?php print($PluginLanguage['Screen']['0099']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Open_Date))); ?></span></td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <div class="white">
                            <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0100']); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-pane" id="tab-clicks">
                    <?php if (is_array($Message_ClickLog) == true && count($Message_ClickLog) > 0): ?>
                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                            <?php
                            foreach ($Message_ClickLog as $Index => $EachEntry):
                                ?>
                                <tr>
                                    <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                    <td class="right-align"><?php print($PluginLanguage['Screen']['0101']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Click_Date))); ?></span></td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <div class="white">
                            <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0102']); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-pane" id="tab-optouts">
                    <?php if (is_array($Message_UnsubscriptionLog) == true && count($Message_UnsubscriptionLog) > 0): ?>
                        <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                            <?php
                            foreach ($Message_UnsubscriptionLog as $Index => $EachEntry):
                                ?>
                                <tr>
                                    <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                    <td class="right-align"><?php print($PluginLanguage['Screen']['0103']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Unsubscription_Date))); ?></span></td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        <div class="white">
                            <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0104']); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>-->
</div>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>