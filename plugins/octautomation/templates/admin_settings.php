<div id="page-shadow">
	<div id="page">
		<div class="page-bar">
			<ul class="livetabs" tabcollection="octautomation-settings" tabcallback="tab_switch">
				<li id="tab-settings"><a href="<?php InterfaceAppURL(); ?>/octautomation/admin_settings/settings/"><?php print($PluginLanguage['Screen']['0115']); ?></a></li>
				<li id="tab-license"><a href="#"><?php print($PluginLanguage['Screen']['0120']); ?></a></li>
				<li id="tab-support"><a href="#"><?php print($PluginLanguage['Screen']['0121']); ?></a></li>
				<?php if (preg_match('/^TRIAL/i', $PluginLicenseInfo['PluginLicenseKey']) > 0): ?>
					<li id="tab-trial"><a href="#"><?php print($PluginLanguage['Screen']['0169']); ?></a></li>
				<?php endif; ?>
			</ul>

		</div>
		<div class="white" style="min-height:430px;">
			<div tabcollection="octautomation-settings" id="tab-content-settings">
				<?php include(APP_PATH.'/plugins/octautomation/templates/admin_settings_include_settings.php'); ?>
			</div>
			<div tabcollection="octautomation-settings" id="tab-content-license">
				<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0122']); ?></h3>
				<div class="form-row no-background-color">
					<p><?php print($PluginLanguage['Screen']['0123']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0124']); ?></p>
					<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['0125']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print($PluginLicenseKey); ?> (v<?php print($PluginVersion); ?>)</span></p>
				</div>
			</div>
			<div tabcollection="octautomation-settings" id="tab-content-support">
				<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0126']); ?></h3>
				<div class="form-row no-background-color">
					<p><?php print($PluginLanguage['Screen']['0127']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0128']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0129']); ?></p>
					<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['0168']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print($PluginLicenseKey); ?> (v<?php print($PluginVersion); ?>)</span></p>
				</div>
			</div>
			<?php if (preg_match('/^TRIAL/i', $PluginLicenseInfo['PluginLicenseKey']) > 0): ?>
				<div tabcollection="octautomation-settings" id="tab-content-trial">
					<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0170']); ?></h3>
					<div class="form-row no-background-color">
						<p><?php print(sprintf($PluginLanguage['Screen']['0171'], date($PluginLanguage['Screen']['0172'], strtotime($PluginLicenseInfo['LicenseExpireDate'])))); ?></p>
						<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['0173']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print(date($PluginLanguage['Screen']['0172'], strtotime($PluginLicenseInfo['LicenseExpireDate']))); ?><br><a href="http://octeth.com/plugins/email-automation-plugin/purchase" target="_blank"><?php print($PluginLanguage['Screen']['0174']); ?></a></span></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php if (in_array($SelectedTab, array('settings')) == true): ?>
	<div class="span-18 last">
		<div class="form-action-container">
			<?php if ($SelectedTab == 'settings'): ?>
				<a class="button" targetform="octautomation-form-settings"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0366', false, '', true); ?></strong></a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

<script>
	var DefaultSelectedTab = '<?php print($SelectedTab); ?>';

	function tab_switch(tab_collection, clicked_tab_id) {
		if (clicked_tab_id == 'tab-settings') {
			return true;
		}
	}
	$(document).ready(function() {
		$('ul[tabcollection="octautomation-settings"] li').click(function(ev) {
			var SelectedTab = $(this).attr('id');
			var TargetURL = $('a', this).attr('href');
			if (SelectedTab == 'tab-settings') {
				window.location.href = TargetURL;
			}
		});

		if (DefaultSelectedTab == 'settings') {
			select_tab('#tab-settings', false);
		}
	});
</script>