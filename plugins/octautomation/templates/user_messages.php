<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include('user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light portlet-transparent">
                            <div class="portlet-title" style="margin-bottom: 0">
                                <div class="caption">
                                    <!--<i class="icon-microphone font-purple-sharp"></i>-->
                                    <?php // print(($PluginLanguage['Screen']['0049'])); ?>
                                    <?php if (is_bool($Messages) == true && $AllMessageCount == 0): ?>

                                        <div class="white" style="min-height:350px;width:706px;">
                                            <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0051']); ?></p>
                                        </div>

                                    <?php else: ?>
                                        <div class="">
                                            <span class="caption-md small bold font-dark">Select:</span>
                                            <a href="#" class="select-all grid-select-all btn default btn-transparen btn-sm" targetgrid="subscribers-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>
                                            <a href="#" class="unselect-all grid-select-none btn default btn-transparen btn-sm" targetgrid="subscribers-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                            <?php if ($ListWhat == 'active'): ?>
                                                <a href="#" class="js-delete-selected main-action btn default btn-transparen btn-sm" targetform="subscribers-table-form"><?php print($PluginLanguage['Screen']['0050']); ?></a>
                                            <?php elseif ($ListWhat == 'draft'): ?>
                                                <a href="#" class="js-delete-selected main-action btn default btn-transparen btn-sm" targetform="subscribers-table-form"><?php print($PluginLanguage['Screen']['0050']); ?></a>
                                            <?php elseif ($ListWhat == 'paused'): ?>
                                                <a href="#" class="js-delete-selected main-action btn default btn-transparen btn-sm" targetform="subscribers-table-form"><?php print($PluginLanguage['Screen']['0050']); ?></a>
                                            <?php elseif ($ListWhat == 'deleted'): ?>
                                                <a href="#" class="js-draft-selected main-action btn default btn-transparen btn-sm" targetform="subscribers-table-form"><?php print($PluginLanguage['Screen']['0053']); ?></a>
                                            <?php endif; ?>

                                            <div class="main-action-with-menu" id="show-message-menu">
                                                <?php if ($ListWhat == 'active'): ?>
                                                    <div class="main-action-with-menu" id="assign-tag-menu">
                                                        <button class="main-action btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> 
                                                            Show: Active Emails (<?php print(number_format($Active_TotalMessages, 0)); ?>)
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu main-action-menu" role="menu">
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/draft" id="">Draft Emails (<?php print(number_format($Draft_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/paused" id="">Paused Emails (<?php print(number_format($Paused_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/deleted" id="">Deleted Emails (<?php print(number_format($Deleted_TotalMessages, 0)); ?>)</a></li>
                                                        </ul>
                                                    </div>
                                                <?php elseif ($ListWhat == 'paused'): ?>
                                                    <div class="main-action-with-menu" id="assign-tag-menu">
                                                        <button class="main-action btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> 
                                                            Show: Paused Emails (<?php print(number_format($Paused_TotalMessages, 0)); ?>)
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu main-action-menu" role="menu">
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/active" id="">Active Emails (<?php print(number_format($Active_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/draft" id="">Draft Emails (<?php print(number_format($Draft_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/deleted" id="">Deleted Emails (<?php print(number_format($Deleted_TotalMessages, 0)); ?>)</a></li>
                                                        </ul>
                                                    </div>
                                                <?php elseif ($ListWhat == 'draft'): ?>
                                                    <div class="main-action-with-menu" id="assign-tag-menu">
                                                        <button class="main-action btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> 
                                                            Show: Draft Emails (<?php print(number_format($Draft_TotalMessages, 0)); ?>)
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu main-action-menu" role="menu">
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/active" id="">Active Emails (<?php print(number_format($Active_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/paused" id="">Paused Emails (<?php print(number_format($Paused_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/deleted" id="">Deleted Emails (<?php print(number_format($Deleted_TotalMessages, 0)); ?>)</a></li>
                                                        </ul>
                                                    </div>
                                                <?php elseif ($ListWhat == 'deleted'): ?>
                                                    <div class="main-action-with-menu" id="assign-tag-menu">
                                                        <button class="main-action btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> 
                                                            Show: Deleted Emails (<?php print(number_format($Deleted_TotalMessages, 0)); ?>)
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu main-action-menu" role="menu">
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/active" id="">Active Emails (<?php print(number_format($Active_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/paused" id="">Paused Emails (<?php print(number_format($Paused_TotalMessages, 0)); ?>)</a></li>
                                                            <li><a href="<?php InterfaceAppURL(); ?>/octautomation/messages/draft" id="">Draft Emails (<?php print(number_format($Draft_TotalMessages, 0)); ?>)</a></li>
                                                        </ul>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" > 
                                            <a id="" class="btn default btn-transparen btn-sm <?php echo $disabled_class?>" href="<?php InterfaceAppURL(); ?>/octautomation/create_message"><strong><?php print(($PluginLanguage['Screen']['0003'])); ?></strong></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form id="messages-list-form" action="<?php InterfaceAppURL(); ?>/octautomation/messages/<?php print($ListWhat); ?>" method="post">
                                        <div class="table-responsive">
                                            <table id="subscribers-table" border="0" class="small-grid items items-custom table">
                                                <?php foreach ($Messages as $Index => $EachMessage): ?>
                                                    <?php
                                                    $OpenRate = number_format(((100 * $EachMessage->TotalOpened) / $EachMessage->TotalSent), 2);
                                                    $ClickRate = number_format(((100 * $EachMessage->TotalClicked) / $EachMessage->TotalSent), 2);
                                                    ?>
                                                    <tr class="item">
                                                        <td width="15" style="vertical-align:top;float:left"><input class="grid-check-element" type="checkbox" name="SelectedMessages[]" value="<?php print($EachMessage->MessageID); ?>"></td>
                                                        <td style="float: left">
                                                            <div class="campaign-details campaign-details-custom">
                                                                <h4>
                                                                    <a class="ng-binding font-purple-sharp bold" href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($EachMessage->MessageID); ?>"><?php print(character_limiter($EachMessage->MessageName, 45)); ?></a>
                                                                </h4>

                                                                <?php if ($EachMessage->LatestEmailSentAt == '0000-00-00 00:00:00'): ?>
                                                                    <span class="clearfix"><?php print($PluginLanguage['Screen']['0176']); ?></span>
                                                                <?php else: ?>
                                                                    <span class="clearfix"><?php print(sprintf($PluginLanguage['Screen']['0177'], date($PluginLanguage['Screen']['0052'], strtotime($EachMessage->LatestEmailSentAt)))); ?></span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>
                                                        <td style="float: right" >
                                                            <div class="stats visible-lg visible-md">
                                                                <ul>
                                                                    <li>
                                                                        <span class="number ng-binding bold"><?php print(number_format($EachMessage->TotalSent)); ?></span>
                                                                        <span class="description font-purple-sharp bold ">Sent</span>
                                                                    </li>
                                                                    <li class="clicked">
                                                                        <span class="number ng-binding bold"><?php print(number_format($EachMessage->TotalOpened)); ?></span>
                                                                        <span class="description font-purple-sharp bold ">Opens</span>
                                                                        <span class="number number-small small"><?php print($OpenRate); ?>%</span>
                                                                    </li>

                                                                    <li class="clicked">
                                                                        <span class="number ng-binding bold"><?php print(number_format($EachMessage->TotalClicked)); ?></span>
                                                                        <span class="description font-purple-sharp bold ">Clicks</span>
                                                                        <span class="number number-small small"><?php print($ClickRate); ?>%</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>
                                        <input type="hidden" name="Command" id="Command" value="" />
                                    </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceInstallationURL(); ?>/plugins/octautomation/js/init.js" type="text/javascript" charset="utf-8"></script>

<script>
    $(document).ready(function () {
        $('.js-redirect-create-message').click(function (ev) {
            ev.preventDefault();
            window.location.href = "<?php InterfaceAppURL(); ?>/octautomation/create_message";
        });

        $('#show-message-menu').click(function () {
            $(this).addClass('open');
            return false;
        });
        $('#show-message-menu').mouseout(function (e) {
            if ($(e.relatedTarget).isChildOf('#show-message-menu') == false) {
                $('#show-message-menu').removeClass('open');
            }
        });
        $('#show-message-menu li a').click(function () {
            var target_link = $(this).attr('href');
            window.location.href = target_link;
        });

        $('.js-delete-selected').click(function () {
            var SelectedRows = $('table input[type="checkbox"]:checked');

            if (SelectedRows.length == 0)
                return false;

            var Response = confirm('Are you sure? Selected messages will be deleted.');
            if (Response == false)
                return;

            var selectedIDs = [];

            SelectedRows.each(function () {
                selectedIDs.push($(this).val());
            })

            selectedIDs = selectedIDs.join(',');

            $('#Command').val('Delete');

            $('#messages-list-form').submit();
        })

        $('.js-draft-selected').click(function () {
            var SelectedRows = $('table input[type="checkbox"]:checked');

            if (SelectedRows.length == 0)
                return false;

            var Response = confirm('Are you sure? Selected messages will be reverted back to draft mode.');
            if (Response == false)
                return;

            var selectedIDs = [];

            SelectedRows.each(function () {
                selectedIDs.push($(this).val());
            })

            selectedIDs = selectedIDs.join(',');

            $('#Command').val('Draft');

            $('#messages-list-form').submit();
        })

    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>
