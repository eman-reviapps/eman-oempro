<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.js"></script>
<script>
    var jQueryAutomation = $.noConflict(false);
</script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/flot/excanvas.min.js"></script><![endif]-->
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/flot/jquery.flot.js"></script>
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.flot.tooltip.js"></script>
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.sparkline.min.js"></script>

<?php
$ChartData1 = array();
$ChartData2 = array();
$ChartXAxis = array();

for ($DayCounter = 0; $DayCounter <= 30; $DayCounter++) {
    $Date = date("jS M'y", time() - (86400 * $DayCounter));
    $Date2 = date("Y-m-d", time() - (86400 * $DayCounter));
    $Value1 = (isset($Message_DeliveryTimeFrame[$Date2]['TotalSent']) == true && $Message_DeliveryTimeFrame[$Date2]['TotalSent'] > 0 ? $Message_DeliveryTimeFrame[$Date2]['TotalSent'] : 0);
    $Value2 = (isset($Message_DeliveryTimeFrame[$Date2]['TotalOpened']) == true && $Message_DeliveryTimeFrame[$Date2]['TotalOpened'] > 0 ? $Message_DeliveryTimeFrame[$Date2]['TotalOpened'] : 0);
    $ChartData1[] = '[' . $DayCounter . ', ' . $Value1 . ', "' . $Date . '"]';
    $ChartData2[] = '[' . $DayCounter . ', ' . $Value2 . ', "' . $Date . '"]';
    if ($DayCounter % 5 == 0 && $DayCounter != 0 && $DayCounter != 30) {
        $ChartXAxis[] = '[' . $DayCounter . ', "' . $Date . '"]';
    }
}
?>

<script>
    jQueryAutomation(function ($) {
        $.plot('#chartdiv', [
            {
                color: '#85B4F2',
                label: '<?php print($PluginLanguage['Screen']['0077']); ?>',
                shadowSize: 0,
                lines: {
                    show: true,
                    fill: false,
                    lineWidth: 2
                },
                points: {
                    show: true
                },
                data: [<?php print(implode(',', $ChartData1)); ?>]
            },
            {
                color: '#F8AB11',
                label: '<?php print($PluginLanguage['Screen']['0076']); ?>',
                shadowSize: 0,
                lines: {
                    show: true,
                    fill: false,
                    lineWidth: 2
                },
                points: {
                    show: true
                },
                data: [<?php print(implode(',', $ChartData2)); ?>]
            }
        ], {
            legend: {
                show: true,
                labelBoxBorderColor: '#FFFFFF',
                position: 'se',
                backgroundColor: null,
                backgroundOpacity: 0,
                sorted: true,
                noColumns: 2,
                container: $('#legend-container')
            },
            grid: {
                show: true,
                color: '#E7EFF5',
                borderWidth: {
                    top: 0,
                    right: 0,
                    bottom: 1,
                    left: 0
                },
                borderColor: '#666666',
                hoverable: true
            },
            xaxis: {
                ticks: [<?php print(implode(',', $ChartXAxis)); ?>]
            },
            yaxis: {
            },
            tooltip: true,
            tooltipOpts: {
                content: "<strong>%s</strong><br>%NameX<br>%y"
            }
        });




    });
</script>

<style>
    #legend-container table {
        padding-bottom:0;
        margin-bottom:0;
    }
    #legend-container table td.legendColorBox {
        width: 20px !important;
        padding-right:0;
        margin-right:0;
    }
    #legend-container table td.legendLabel {
        width:100px !important;
        font-weight: bold;
        color:#666666;
    }
</style>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php // include('user_header.php'); ?>

<?php include('user_message_header.php'); ?>

<div class="page-content-inner">
    <div class="row" style="margin-bottom: 0px;">
        <div class="col-md-12">
            <div class="col-md-5 pull-left" style="background: #fff" >
                <div class="portlet sale-summary" style="">
                    <div class="portlet-title">
                        <div class="caption font-purple-sharp sbold"> Info </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="list-unstyled">
                            <li>
                                <span class="sale-info sale-info-custom bold"> Subject 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> <?php echo $CampaignInformation['CampaignName'] ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Sender 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <?php // print_r($CampaignInformation) ?>
                                <span class="sale-num sale-num-custom"> <?php echo $EmailInformation['FromName'] . ' &lt;' . $EmailInformation['FromEmail'] . '&gt;'; ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Sent on
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> <?php echo $CampaignInformation['SendProcessFinishedOn'] ?> </span>
                            </li>
                            <li>
                                <span class="sale-info sale-info-custom bold"> Recipients 
                                    <i class="fa fa-img-up"></i>
                                </span>
                                <span class="sale-num sale-num-custom"> 
                                    <?php
                                    $i = 0;
                                    $visible_count = 1;
                                    foreach ($ArrayRecipientLists as $EachList) {
                                        if (InterfacePrivilegeCheck('List.Get', $UserInformation)) {
                                            ?>
                                            <a class="bold" href="<?php InterfaceAppURL(); ?>/user/list/statistics/<?php print($EachList['ListID']); ?>"><?php print($EachList['Name']); ?></a>
                                            <?php
                                        } else {
                                            print($EachList['Name']);
                                        }
                                        //
                                        if ($i == $visible_count - 1 && $visible_count != count($ArrayRecipientLists)) {
                                            echo '<span id="more_lists" >and <a id="more_link" class="bold">' . (count($ArrayRecipientLists) - $visible_count ) . ' more.. </span> ' . '</a><span id="listIds" style="display:none">';
                                        }
                                        if ($i == count($ArrayRecipientLists) - 1 && count($ArrayRecipientLists) > $visible_count) {
                                            echo '</span>';
                                        }
                                        if ($i != count($ArrayRecipientLists) - 1) {
                                            echo " , ";
                                        } else {
                                            echo '<a id="showless" class="bold" style="display:none">show less</a>';
                                        }
                                        $i++;
                                    }
                                    ?>

                                </span>
                            </li>
                        </ul>
                        <div class="caption" style="padding-bottom: 10px;">
                            <!-- Pause - Start -->
                            <a id="campaign-pause-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Sending') ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/pause/<?php echo $CampaignInformation['CampaignID']; ?>"><strong><?php InterfaceLanguage('Screen', '0926', false, '', false, true); ?></strong></a>
                            <!-- Pause - End -->

                            <!-- Cancel schedule - Start -->
                            <a id="campaign-cancel-schedule-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Ready' && ($CampaignInformation['ScheduleType'] == 'Future' || $CampaignInformation['ScheduleType'] == 'Recursive')) ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/cancelschedule/<?php echo $CampaignInformation['CampaignID']; ?>">strong><?php InterfaceLanguage('Screen', '0928', false, '', false, true); ?></strong></a>
                            <!-- Cancel schedule - End -->

                            <!-- Cancel sending - Start -->
                            <a id="campaign-cancel-sending-button" style="<?php echo ($CampaignInformation['CampaignStatus'] == 'Sending' || ($CampaignInformation['CampaignStatus'] == 'Ready' && $CampaignInformation['ScheduleType'] == 'Immediate')) ? '' : 'display:none'; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/cancelsending/<?php echo $CampaignInformation['CampaignID']; ?>"><strong><?php InterfaceLanguage('Screen', '0927', false, '', false, true); ?></strong></a>
                            <!-- Cancel sending - End -->

                            <!-- Resume - Start -->
                            <a id="campaign-resume-button" style="<?php echo ($CampaignInformation['CampaignStatus'] != 'Paused') ? 'display:none' : ''; ?>" class="btn default btn-sm" href="<?php InterfaceAppURL() ?>/user/campaign/resume/<?php echo $CampaignInformation['CampaignID']; ?>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php InterfaceLanguage('Screen', '0929', false, '', false, true); ?></strong></a>
                            <!-- Resume - End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php // include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light portlet-transparent">

                            <div class="portlet-body">
                                <div class="white">
                                    <div class="inner flash-chart-container" id="activity-chart-container">
                                        <div id="chartdiv" style="margin-left:10px;height:190px;width:690px; "></div>
                                        <div id="legend-container" style="height:30px;width:250px;margin:0 auto;margin-top: 20px "></div>
                                    </div>

                                    <div class="custom-column-container cols-2 clearfix">
                                        <div class="col">
                                            <span class="data big"><?php print(number_format($Message->TotalSent, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0079']); ?></span><br />

                                            <span class="data big"><?php print(number_format($Message->TotalOpened, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0080']); ?></span><br />
                                            <?php if ($Message->TotalSent > 0): ?>
                                                <span class="data-label small"><?php print($PluginLanguage['Screen']['0081']); ?> </span> <span class="data"><?php print(number_format(($Message->TotalOpened * 100) / $Message->TotalSent, 2)); ?>%</span><br />
                                            <?php endif; ?>

                                            <span class="data big"><?php print(number_format($Message->TotalClicked, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0082']); ?></span><br />
                                            <?php if ($Message->TotalSent > 0): ?>
                                                <span class="data-label small"><?php print($PluginLanguage['Screen']['0083']); ?> </span> <span class="data"><?php print(number_format(($Message->TotalClicked * 100) / $Message->TotalSent, 2)); ?>%</span><br />
                                            <?php endif; ?>

                                            <span class="data big"><?php print(number_format($Message->TotalUnsubscribed, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0084']); ?></span><br />
                                            <?php if ($Message->TotalSent > 0): ?>
                                                <span class="data-label small"><?php print($PluginLanguage['Screen']['0085']); ?> </span> <span class="data"><?php print(number_format(($Message->TotalUnsubscribed * 100) / $Message->TotalSent, 2)); ?>%</span><br />
                                            <?php endif; ?>

                                            <span class="data big"><?php print(number_format($Message->TotalHardBounced, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0086']); ?></span><br />
                                            <?php if ($Message->TotalSent > 0): ?>
                                                <span class="data-label small"><?php print($PluginLanguage['Screen']['0087']); ?> </span> <span class="data"><?php print(number_format(($Message->TotalHardBounced * 100) / $Message->TotalSent, 2)); ?>%</span><br />
                                            <?php endif; ?>

                                            <span class="data big"><?php print(number_format($Message->TotalSpamComplaint, 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0088']); ?></span><br />
                                            <?php if ($Message->TotalSent > 0): ?>
                                                <span class="data-label small"><?php print($PluginLanguage['Screen']['0089']); ?> </span> <span class="data"><?php print(number_format(($Message->TotalSpamComplaint * 100) / $Message->TotalSent, 2)); ?>%</span><br />
                                            <?php endif; ?>
                                        </div>
                                        <div class="col">
                                            <div style="background-color:#f2f2f2;border:1px solid #e1e1e1;margin-bottom:18px;">
                                                <div style="padding:9px 12px;" class="clearfix">
                                                    <span class="data-label" style="margin-left:0;"><?php print($PluginLanguage['Screen']['0090']); ?></span><br>
                                                    <span class="data" style="font-weight: normal;"><?php print($PluginLanguage['Screen']['0075'][$Message->Status]); ?></span><br />

                                                    <span class="data-label" style="margin-left:0;"><?php print($PluginLanguage['Screen']['0091']); ?></span><br>
                                                    <span class="data" style="font-weight: normal;"><?php print($Message->FromName); ?> &lt;<?php print($Message->FromEmail); ?>&gt;</span><br />

                                                    <span class="data-label" style="margin-left:0;"><?php print($PluginLanguage['Screen']['0092']); ?></span><br>
                                                    <span class="data" style="font-weight: normal;"><?php print($Message->Subject); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tabbable-custom nav-justified" style="margin-top: 20px">
                                    <ul class="nav nav-tabs nav-justified">
                                        <?php if (defined(GEO_LOCATION_DATA_PATH) && file_exists(GEO_LOCATION_DATA_PATH) == true): ?>
                                            <li>
                                                <a href="#tab-locations" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0093']); ?> </a>
                                            </li>
                                        <?php endif; ?>
                                        <li class="active">
                                            <a href="#tab-opens" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0094']); ?> </a>
                                        </li>
                                        <li>
                                            <a href="#tab-clicks" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0095']); ?> </a>
                                        </li>
                                        <li>
                                            <a href="#tab-optouts" data-toggle="tab"> <?php print($PluginLanguage['Screen']['0096']); ?> </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <?php if (defined(GEO_LOCATION_DATA_PATH) && file_exists(GEO_LOCATION_DATA_PATH) == true): ?>
                                            <div class="tab-pane" id="tab-locations">
                                                <?php if (is_array($GeoOpenStats) == true && count($GeoOpenStats) > 0): ?>
                                                    <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                                                        <?php
                                                        foreach ($GeoOpenStats as $CountryCode => $EachCity):
                                                            $CountryOpens = 0;
                                                            foreach ($EachCity as $City => $Detections) {
                                                                $CountryOpens += $Detections;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td style="background-color:#DCE8F1;" width="200"><strong><?php print($CountryList[$CountryCode]); ?></strong></td>
                                                                <td style="background-color:#DCE8F1;" width="100" class="right-align"><span class="data"><?php print(number_format($CountryOpens, 0)); ?></span> <?php print($PluginLanguage['Screen']['0097']); ?></td>
                                                                <td style="background-color:#DCE8F1;">&nbsp;</td>
                                                            </tr>
                                                            <?php foreach ($EachCity as $City => $Detections): ?>
                                                                <tr>
                                                                    <td><span style="padding-left:20px;"><?php print($City); ?></span></td>
                                                                    <td class="right-align"><span class="data"><?php print(number_format($Detections, 0)); ?></span> <?php print($PluginLanguage['Screen']['0097']); ?></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        <?php endforeach; ?>
                                                    </table>
                                                <?php else: ?>
                                                    <div class="white">
                                                        <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0098']); ?></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                        <div class="tab-pane active" id="tab-opens">
                                            <?php if (is_array($Message_OpenLog) == true && count($Message_OpenLog) > 0): ?>
                                                <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                                                    <?php
                                                    foreach ($Message_OpenLog as $Index => $EachEntry):
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                                            <td class="right-align"><?php print($PluginLanguage['Screen']['0099']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Open_Date))); ?></span></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            <?php else: ?>
                                                <div class="white">
                                                    <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0100']); ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="tab-pane" id="tab-clicks">
                                            <?php if (is_array($Message_ClickLog) == true && count($Message_ClickLog) > 0): ?>
                                                <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                                                    <?php
                                                    foreach ($Message_ClickLog as $Index => $EachEntry):
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                                            <td class="right-align"><?php print($PluginLanguage['Screen']['0101']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Click_Date))); ?></span></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            <?php else: ?>
                                                <div class="white">
                                                    <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0102']); ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="tab-pane" id="tab-optouts">
                                            <?php if (is_array($Message_UnsubscriptionLog) == true && count($Message_UnsubscriptionLog) > 0): ?>
                                                <table border="0" cellspacing="0" cellpadding="0" class="small-grid">
                                                    <?php
                                                    foreach ($Message_UnsubscriptionLog as $Index => $EachEntry):
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachEntry->RelPersonID); ?>"><?php print($EachEntry->Email); ?></a></td>
                                                            <td class="right-align"><?php print($PluginLanguage['Screen']['0103']); ?> <span class="data"><?php print(date('jS M Y', strtotime($EachEntry->Unsubscription_Date))); ?></span></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            <?php else: ?>
                                                <div class="white">
                                                    <p style="font-size:16px;text-align: center;padding-top:36px;"><?php print($PluginLanguage['Screen']['0104']); ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>