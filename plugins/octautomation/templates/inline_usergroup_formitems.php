<?php
if (is_null($UserGroupSettings) == true)
{
	$UserGroupSettings = array();
}
?>
<div class="form-row <?php print((form_error('OctAutomation[DeliveryLimit]') != '' ? 'error' : '')); ?>" id="form-row-OctAutomation_DeliveryLimit">
	<label for="OctAutomation_DeliveryLimit"><?php print($PluginLanguage['Screen']['0155']); ?>:</label>
	<input type="text" name="OctAutomation[DeliveryLimit]" value="<?php echo set_value('OctAutomation[DeliveryLimit]', (is_bool($UserGroupSettings['DeliveryLimit']) == true ? '' : $UserGroupSettings['DeliveryLimit'])); ?>" id="form-row-OctAutomation_DeliveryLimit" class="text" />
	<div class="form-row-note">
		<p><?php print($PluginLanguage['Screen']['0156']); ?></p>
	</div>
	<?php print(form_error('OctAutomation[DeliveryLimit]', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>
