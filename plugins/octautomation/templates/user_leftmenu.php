<!--<span class="btn blue-hoki compose-btn btn-block">
    <?php print(($PluginLanguage['Screen']['0001'])); ?>
</span>-->
<!--<ul class="inbox-nav">-->
<ul class="nav nav-tabs ">
    <?php if ($TotalPeople == 0): ?>

        <li class="<?php print(($Section == 'GettingStarted' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/email_automation"><?php print($PluginLanguage['Screen']['0004']); ?></a></li>
        <li class="<?php print(($Section == 'People' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/people"><?php print($PluginLanguage['Screen']['0005']); ?><span class="item-count">(<?php print(number_format($TotalPeople, 0)); ?>)</span></a></li>
        <li class="<?php print(($Section == 'Messages' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/messages"><?php print($PluginLanguage['Screen']['0006']); ?></a></li>
    <?php else: ?>

        <li class="<?php print(($Section == 'People' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/people"><?php print($PluginLanguage['Screen']['0005']); ?><span class="item-count">(<?php print(number_format($TotalPeople, 0)); ?>)</span></a></li>
        <li class="<?php print(($Section == 'Messages' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/messages"><?php print($PluginLanguage['Screen']['0006']); ?></a></li>
        <li class="<?php print(($Section == 'GettingStarted' ? 'active' : '')); ?>"><a href="<?php InterfaceAppURL(); ?>/octautomation/email_automation"><?php print($PluginLanguage['Screen']['0007']); ?></a></li>
    <?php endif; ?>
</ul>
