<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-editable.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption">

        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <form id="people-list-form" action="<?php InterfaceAppURL(); ?>/octautomation/people/" method="post">

            <table id="sample_1" class="table table-striped table-hover table-bordered dataTable" >
                <thead>
                    <tr>
                        <th>
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                <span></span>
                            </label>
                        </th>
                        <th></th>
                        <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                            <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                            <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                            <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                            <?php else: ?>
                                <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <th> Delete </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (is_array($People) == true && count($People) > 0): ?>
                        <?php foreach ($People as $Index => $EachPerson): ?>
                            <tr>
                                <td>
                                    <label style="margin-left: 8px" class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="<?php print($EachPerson->PersonID); ?>" />
                                        <span></span>
                                    </label>
                                </td>
                                <td style="padding-left:0;padding-right:0;white-space:nowrap; text-align: center;"><img src="http://www.gravatar.com/avatar/<?php print(md5(strtolower(trim($EachPerson->Email)))); ?>?s=16&d=identicon" width="16" height="16" alt="Photo"></td>
                                <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                                    <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                        <td style="text-align:center; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                    <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                        <td style="text-align:right; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                    <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                        <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                    <?php else: ?>
                                        <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                    <?php endif; ?>

                                <?php endforeach; ?>
                                <td>
                                    <a style="color:#337ab7" class="delete" href="javascript:;"> Delete </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="Command" id="Command" value="">
            <input type="hidden" name="SelectedPeopleIDs" id="SelectedPeopleIDs" value="">

        </form>
    </div>
</div>