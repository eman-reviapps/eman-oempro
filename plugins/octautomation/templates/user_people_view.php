<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include('user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#person-profile" data-toggle="tab"><?php print($PluginLanguage['Screen']['0036']); ?></a></li>
                                    <li ><a href="#person-activity" data-toggle="tab"><?php print($PluginLanguage['Screen']['0037']); ?></a></li>
                                </ul>
                                <div class="clearfix"></div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="person-profile">

                                        <?php if (isset($PageErrorMessage) == true): ?>
                                            <h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
                                        <?php elseif (isset($PageSuccessMessage) == true): ?>
                                            <h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
                                        <?php endif; ?>

                                        <div style="width: 130px;float:left;padding:10px 10px 10px 20px;">
                                            <img src="http://www.gravatar.com/avatar/<?php print(md5(strtolower(trim($Person->Email)))); ?>?s=128&d=identicon" width="128" height="128" alt="Photo">
                                        </div>
                                        <div style="float:right;width:546px;">
                                            <div class="form-row" id="form-row-BounceType">
                                                <label><?php print($PluginLanguage['Screen']['0038']); ?></label>
                                                <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['Email'], $Person->Email)); ?></p>
                                            </div>
                                            <div class="form-row" id="form-row-BounceType">
                                                <label><?php print($PluginLanguage['Screen']['0039']); ?></label>
                                                <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['EntryDate'], $Person->EntryDate)); ?></p>
                                            </div>
                                            <div class="form-row" id="form-row-BounceType">
                                                <label><?php print($PluginLanguage['Screen']['0040']); ?></label>
                                                <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['Identifier'], $Person->Identifier)); ?></p>
                                            </div>
                                        </div>

                                        <div class="form-row" id="form-row-BounceType">
                                            <label style="width:300px;"><?php print($PluginLanguage['Screen']['0041']); ?></label>
                                            <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['LastSeenDate'], $Person->LastSeenDate)); ?></p>
                                        </div>

                                        <div class="form-row" id="form-row-BounceType">
                                            <label style="width:300px;"><?php print($PluginLanguage['Screen']['0042']); ?></label>
                                            <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['IPAddress'], $Person->IPAddress)); ?></p>
                                        </div>

                                        <div class="form-row" id="form-row-BounceType">
                                            <label style="width:300px;"><?php print($PluginLanguage['Screen']['0043']); ?></label>
                                            <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['City'], $Person->City, $CountryList)); ?> / <?php print(FormatPeopleColumnValue($AvailableColumns['Country'], $Person->Country, $CountryList)); ?></p>
                                        </div>

                                        <div class="form-row" id="form-row-BounceType">
                                            <label style="width:300px;"><?php print($PluginLanguage['Screen']['0044']); ?></label>
                                            <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns['SessionCounter'], $Person->SessionCounter)); ?></p>
                                        </div>

                                        <?php if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0): ?>
                                            <?php foreach (get_object_vars($PeopleMetaData) as $Name => $Key): ?>
                                                <div class="form-row" id="form-row-BounceType">
                                                    <label style="width:300px;"><?php print(ucfirst(str_replace('_', ' ', $Name))); ?>:</label>
                                                    <p style="padding-top:7px;margin:0;position:relative;left:9px;"><?php print(FormatPeopleColumnValue($AvailableColumns[$Key], $Person->{$Key})); ?></p>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="tab-pane" id="person-activity">
                                        <?php if (is_bool($Activity) == true): ?>
                                            <h4 class="form-section bold font-blue"><?php print($PluginLanguage['Screen']['0048']); ?></h4>

                                            <div class="form-row no-bg">
                                                <span class="help-block"><?php print($PluginLanguage['Screen']['0047']); ?></span>
                                            </div>
                                        <?php else: ?>
                                            <?php foreach ($Activity as $Index => $EachActivity): ?>
                                                <div class="form-row no-bg">
                                                    <span style="display:inline-block;width:150px;font-weight:bold;"><?php echo date("jS M'y, g:ia", strtotime($EachActivity->ActivityDateTime)); ?></span>
                                                    <?php if (isset($EachActivity->MessageName) == true && is_bool($EachActivity->MessageName) == false && $EachActivity->MessageName != ''): ?>
                                                        <span style="display:inline-block;width:400px;"><strong><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/message/<?php print($EachActivity->RelMessageID); ?>"><?php print($EachActivity->MessageName); ?></a></strong></span>
                                                    <?php endif; ?>
                                                    <?php print($EachActivity->ActivityType); ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="btn default" id="form-button" href="<?php print(InterfaceAppURL(true)); ?>/octautomation/people" ><strong><?php print(strtoupper($PluginLanguage['Screen']['0035'])); ?></strong></a>
                                            <a class="btn default js-button-unsubscribe" id="form-button" href="#" ><strong><?php InterfaceLanguage('Screen', '1309', false, '', true); ?></strong></a>
                                            <a class="btn default js-button-delete" id="form-button-delete" href="#" ><strong><?php InterfaceLanguage('Screen', '0042', false, '', true); ?></strong></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php InterfaceInstallationURL(); ?>/plugins/octautomation/js/init.js" type="text/javascript" charset="utf-8"></script>

<script>
    $(document).ready(function () {
        $('.js-button-unsubscribe').click(function () {
            var result = confirm('<?php print(addslashes($PlugInLanguage['Screen']['0045'])); ?>');

            if (result == false)
                return false;

            window.location.href = "<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($Person->PersonID); ?>/unsubscribe/";
            return false;
        });

        $('.js-button-delete').click(function () {
            var result = confirm('<?php print(addslashes($PlugInLanguage['Screen']['0046'])); ?>');

            if (result == false)
                return false;

            window.location.href = "<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($Person->PersonID); ?>/delete/";
            return false;
        });
    });
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>
