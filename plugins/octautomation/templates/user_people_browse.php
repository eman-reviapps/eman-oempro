<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<script type="text/javascript">
    var delete_url = "<?php InterfaceAppURL(); ?>/octautomation/people_delete/";
</script>

<?php include('user_header.php'); ?>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-transparent">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>

                        <div class="portlet " style="margin-top: 20px">

                            <div class="portlet-body">
                                <div class="clearfix"></div>
                                <div class="portlet box purple-soft">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <?php InterfaceLanguage('Screen', '9200'); ?> </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="padding-bottom: 8px">
                                        <div id="message-rules" style="border-bottom:none;">
                                            <p>
                                                <select name="SegmentOperator" id="SegmentOperator" style="">
                                                    <option value="and">Match all rules</option>
                                                    <option value="or">Match any rules</option>
                                                </select>                   
                                            </p>
                                            <div class="rule-rows" style="margin-top:8px;"></div>
                                            <p style="margin-top:20px;">
                                                <a class="btn default add-rule-button"  id="add-rule-button" href="#">+ Add new rule</a>
                                                <a class="btn default apply-filter-button"  id="apply-filter-button" href="#">Apply filters</a>
                                                <a class="btn default reset-filter-button"  id="reset-filter-button" href="#">Reset</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div id="people_div" style="margin-top: 75px">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$FilterFields = array();

foreach ($AvailableColumns as $Key => $Name) {
    if ($PeopleDataStructure[$Key] == 'date') {
        $OperatorTypes = 'operatorTypes.date';
    } elseif ($PeopleDataStructure[$Key] == 'number') {
        $OperatorTypes = 'operatorTypes.number';
    } elseif ($PeopleDataStructure[$Key] == 'text') {
        $OperatorTypes = 'operatorTypes.string';
    } else {
        $OperatorTypes = 'operatorTypes.string';
    }
    $FilterFields[] = "'" . $Key . "': {label: '" . $Name . "', operators:" . $OperatorTypes . ", value:'', options:''}";
}
?>

<script type="text/javascript">

    var operatorTypes = {
        empty: [],
        string: [['is', 'is'], ['is not', 'is not'], ['starts with', 'starts with'], ['ends with', 'ends with'], ['contains', 'contains']],
        date: [['more than', 'more than'], ['less than', 'less than'], ['exactly', 'exactly'], ['after', 'after'], ['on', 'on'], ['before', 'before']],
        number: [['is', 'is'], ['is not', 'is not'], ['greater than', 'greater than'], ['smaller than', 'smaller than']]
    };

    var availableColumnsAndOptions = {
        Empty: {label: 'Choose a property...', operators: operatorTypes.empty, value: '', options: ''},
<?php print(implode(',', $FilterFields)); ?>
    };


    // START - DON'T TOUCH THE RULES BELOW - THIS IS THE DEFAULT RULE THAT WILL BE DISPLAYED ON A NEW SEGMENT
    var segmentRules = [];
    segmentRules.push({column: 'Empty', operator: '', value: ''});

    var setupRuleBasedOnColumn = function (el, operator, value) {
        var $columnSelect = $(el),
                $row = $columnSelect.parent(),
                columnOptions = availableColumnsAndOptions[$columnSelect.val()],
                $columnOperatorSelect = $('<select name="RuleOperator[]" class="column-operator" style="margin-left:3px;width:120px;"></select>'),
                $columnValueInput;

        $row.find('.column-operator, .column-value').remove();
        var columnOperatorOptions = [];
        $.each(columnOptions.operators, function () {
            columnOperatorOptions.push('<option value="' + this[1] + '" ' + (operator == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
        });
        $columnOperatorSelect.html(columnOperatorOptions.join(''));
        $columnSelect.after($columnOperatorSelect);

        if (!columnOptions.options) {
            $columnValueInput = $('<input type="text" name="RuleValue[]" class="column-value" style="margin-left:3px;width:300px;">');
            if (value !== undefined)
                $columnValueInput.val(value);
        } else {
            $columnValueInput = $('<select name="RuleValue[]" class="column-value" style="margin-left:3px;width:170px;"></select>');
            var columnValueOptions = [];
            $.each(columnOptions.options, function () {
                columnValueOptions.push('<option value="' + this[1] + '" ' + (value == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
            });
            $columnValueInput.html(columnValueOptions.join(''));
        }
        if ($columnSelect.val() == 'Empty') {
            $columnOperatorSelect.hide();
            $columnValueInput.hide();
        } else {
            $columnOperatorSelect.show();
            $columnValueInput.show();
        }
        $columnOperatorSelect.after($columnValueInput);
        setupOperatorLabel(el);
    };
    var setupOperatorLabel = function (el) {
        var $ruleRow = $(el).parent();
        var $operatorSelect = $ruleRow.children('.column-operator');
        var $span = $('span.column-operator-label', $ruleRow);
        var operator = $operatorSelect.val();

        if ($span.length < 1) {
            $span = $('<span class="column-operator-label"></span>').css('color', '#616265').css('margin-left', '5px');
            $ruleRow.append($span);
        }
        if (operator == 'more than') {
            $span.text('days');
        } else if (operator == 'less than') {
            $span.text('days');
        } else if (operator == 'exactly') {
            $span.text('days');
        } else if (operator == 'after') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'before') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'on') {
            $span.text('YYYY-MM-DD');
        } else {
            $('.column-operator-label', $ruleRow).remove();
        }
    };
    var drawSelectedFilterBorder = function () {
        var filterBoxHeight = $('#filter-box').outerHeight();
        var selectedFilterListItem = $('.filter-list li.selected');
        if (selectedFilterListItem.length < 1)
            return;
        var liPos = selectedFilterListItem.position().top;
        $('.filter-list li.selected .border').height(filterBoxHeight);
        $('.filter-list li.selected .border').css('top', '-' + liPos + 'px');
    };
    var removeRule = function (ruleRow) {
        var $ruleRow = $(ruleRow);
        if ($('.rule-row').length == 1)
            return;
        $ruleRow.remove();
        drawSelectedFilterBorder();
        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    };
    var addRule = function () {
        var $lastRuleRow = $('.rule-row').last(),
                $cloneRuleRow = $lastRuleRow.clone();

        $cloneRuleRow.find('.column-operator, .column-value').remove();
        $lastRuleRow.after($cloneRuleRow);
        setupRuleBasedOnColumn($cloneRuleRow.find('.rule-column'));
        drawSelectedFilterBorder();
        if ($('.rule-row').length > 1) {
            $('.remove-rule-button').show();
        }
    };
    var populateRules = function (rules) {

        $.each(rules, function () {
//            alert($("#SegmentOperator").val())
            var $ruleRow = $('<div class="rule-row"></div>'),
                    $ruleColumnSelect = $('<select name="RuleColumn[]" class="rule-column" style="width:180px;"></select>'),
                    ruleObject = this;

            var ruleColumnSelectOptions = [];
            $.each(availableColumnsAndOptions, function (columnId, columnOptions) {
                ruleColumnSelectOptions.push('<option value="' + columnId + '" ' + (ruleObject.column == columnId ? 'selected' : '') + '>' + columnOptions.label + '</option>');
            });
            $ruleColumnSelect.html(ruleColumnSelectOptions.join(''));
//            $ruleRow.append('<span class="rule-separator">' + $("#SegmentOperator").val().toUpperCase() + '</span>');
            $ruleRow.append($ruleColumnSelect);

            $ruleRow.append($('<a href="#" class="delete remove-rule-button link">x</a>'));
//            $ruleRow.append($('<span class="rule-separator">AND</span>'))

            $('.rule-rows').append($ruleRow);
            setupRuleBasedOnColumn($ruleColumnSelect, ruleObject.operator, ruleObject.value);
        });
    };

    var apply_filters = function ()
    {
        var RuleColumns = new Array();
        $('div.rule-row').find('.rule-column').each(function () {
            RuleColumns.push($(this).val());
        });

        var RuleOperators = new Array();
        $('div.rule-row').find('.column-operator').each(function () {
            RuleOperators.push($(this).val());
        });

        var RuleValues = new Array();
        $('div.rule-row').find('.column-value').each(function () {
            RuleValues.push($(this).val());
        });

        url = APP_URL + "/octautomation/search";

        $.ajax({
            type: "POST",
            data: {
                'MatchType': $("#SegmentOperator").val(),
                'Rule_Columns': RuleColumns,
                'Rule_Operators': RuleOperators,
                'Rule_Values': RuleValues
            },
            url: url,
            success: function (data) {
//                    console.log(data);
//                    alert(data)
                $("#people_div").html(data)
            },
            async: false
        });
    };
    window.estimatedRecipientsRequest = null;

    $(document).ready(function () {

        populateRules(segmentRules);
        apply_filters();
        $('.apply-filter-button').on('click', function (ev) {
            apply_filters();
            return false;
        });

        $('.reset-filter-button').on('click', function (ev) {
            $('.rule-rows').html('');
            populateRules(segmentRules);
            return false;
        });
        $('.rule-rows').on('change', '.rule-column', function (event) {
            setupRuleBasedOnColumn(this);
        });
        $('.rule-rows').on('change', '.column-operator', function (event) {
            setupOperatorLabel(this);
        });
        $('.rule-rows').on('change', '.column-value', function (event) {
        });
        $('.rule-rows').on('click', '.remove-rule-button', function (event) {
            event.preventDefault();
            removeRule($(this).parent());
            showNewFilterOption();
        });
        $('.add-rule-button').on('click', function (event) {
            event.preventDefault();
            addRule();
            showNewFilterOption();
        });
        $('.rule-column').each(function () {
            setupRuleBasedOnColumn(this);
        });
        function showNewFilterOption() {
            $('.filter-list li.selected').removeClass('selected');
            $('#new-filter-box').show();
            $('#browsing-filter-name').html('&nbsp;');
        }

        $('#filter-box input, #filter-box select').on('change', function () {
            showNewFilterOption();
        });
        drawSelectedFilterBorder();
        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    });
</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>