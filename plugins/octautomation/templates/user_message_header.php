<div class="portlet" style="padding-bottom: 0px;margin-bottom: 15px">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-purple-sharp sbold">
                <?php print((word_limiter($Message->MessageName, 5))); ?>
            </span>
        </div>
        <div class="actions">
            <a class="btn default btn-sm" href="<?php InterfaceAppURL(); ?>/octautomation/message/<?php print($Message->MessageID); ?>"><strong><?php print($PluginLanguage['Screen']['0105']); ?></strong></a>

            <!--<div class="main-action-with-menu" id="share-campaign-menu">-->
            <div class="btn-group btn-group-solid">
                <!--                <a href="#" class="main-action">Options</a>
                                <div class="down-arrow"><span></span></div>-->

                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-horizontal"></i> Options
                    <i class="fa fa-angle-down"></i>
                </button>

                <ul class="dropdown-menu">
                    <?php if ($Message->Status == 'Draft'): ?>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/delete" target="_blank">Delete Message</a></li>
                    <?php elseif ($Message->Status == 'Active'): ?>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/delete" target="_blank">Delete Message</a></li>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/pause" target="_blank">Pause Message</a></li>
                    <?php elseif ($Message->Status == 'Paused'): ?>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/activate" target="_blank">Activate Message</a></li>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/delete" target="_blank">Delete Message</a></li>
                    <?php elseif ($Message->Status == 'Deleted'): ?>
                        <li><a href="<?php InterfaceAppURL(); ?>/octautomation/report/<?php print($Message->MessageID); ?>/activate" target="_blank">Activate Message</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#share-campaign-menu').click(function () {
            $(this).addClass('open');
            return false;
        });
        $('#share-campaign-menu').mouseout(function (e) {
            if ($(e.relatedTarget).isChildOf('#share-campaign-menu') == false) {
                $('#share-campaign-menu').removeClass('open');
            }
            return false;
        });

        $('.tag-label a').click(function () {
            var a = $(this);
            var parent_span = $(this).parent('.tag-label');
            parent_span.fadeTo('fast', 0.33);
            $.post(api_url, {command: 'Tag.UnassignFromCampaigns', tagid: $(this).attr('id').replace('tag-id-', ''), campaignids: CampaignID}, function () {
                parent_span.remove();
            });
            a.click(function () {
                return false;
            });
        });
    });
</script>