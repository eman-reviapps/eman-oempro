<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<?php include('user_header.php'); ?>
<?php include('user_message_header.php'); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption font-purple-sharp sbold"><?= $Title ?></span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <?php
                            foreach ($ResultRows[0] as $key => $value) {
                                if (in_array($key, $Keys)) {
                                    ?>
                                    <th> <?php echo $key ?> </th>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                    </thead>
                    <?php
                    if (count($ResultRows) > 0) {
                        
                    } else {
                        echo $PluginLanguage['Screen']['0221'];
                    }
                    ?>
                    <tbody>
                        <?php
                        foreach ($ResultRows as $Rows) {
                            ?>
                            <tr>
                                <?php
                                foreach ($Rows as $key => $value) {
                                    if (in_array($key, $Keys)) {
                                        ?>
                                        <td> 
                                            <?php
                                            if ($key == 'Email') {
                                                ?>
                                                <a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php echo $Rows['RelPersonID']; ?>"><?php echo $value; ?></a>
                                                <?php
                                            } else {
                                                echo $value;
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>