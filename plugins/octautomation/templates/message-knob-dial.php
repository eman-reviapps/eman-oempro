<script>
    var ComponentsKnobDials = function () {

        return {
            //main function to initiate the module

            init: function () {
                //knob does not support ie8 so skip it
                if (!jQuery().knob || App.isIE8()) {
                    return;
                }

                // general knob
                $('.knob').val(0).trigger('change').delay(90);
                $(".knob").knob({
                    'min': 0,
                    'max': <?php echo ($Message->TotalSent - $Message->TotalHardBounced) ?>,
                    'dynamicDraw': true,
                    'thickness': 0.3,
                    'tickColorizeValues': true,
                    'skin': 'tron',
                    'readOnly': true,
                    'inputColor': '#333',
                    'fgColor ': '#1ac6ff',
                    'bgColor': 'rgba(26, 198, 255, 0.2)'
                });

                var tmr = self.setInterval(function () {
                    myDelay()
                }, 30);

                var m = 0;

                function myDelay() {

                    $('.knob').val(m).trigger('change');
                    var success_ratio = <?php echo ($Message->TotalSent - $Message->TotalHardBounced) ?>;
                    m += 1;
                    if (m == (success_ratio + 1) || success_ratio == 0) {
                        window.clearInterval(tmr);
                    }
                }
            }

        };

    }();

    jQuery(document).ready(function () {
        ComponentsKnobDials.init();
    });
</script>

