<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header_1.php'); ?>

<?php
$FilterEditMode = false;
if (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true && is_array($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && count($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) > 0) {
    $FilterEditMode = true;
    $MatchType = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'];
    $RuleColumns = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns'];
    $RuleOperators = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'];
    $RuleValues = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'];
}
?>

<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.js"></script>

<div class="container">
    <div class="span-23 last">
        <div id="section-bar">
            <div class="header">
                <h1 class="left-side-padding"><?php print(strtoupper($PluginLanguage['Screen']['0001'])); ?></h1>
            </div>
            <div class="actions">
                <a id="" class="button" href="<?php InterfaceAppURL(); ?>/octautomation/create_message"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php print(strtoupper($PluginLanguage['Screen']['0003'])); ?></strong></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="span-5 snap-5">
        <?php include('user_leftmenu.php'); ?>
    </div>
    <div class="span-18 last">
        <div id="page-shadow">
            <div id="page">
                <div class="page-bar"><h2><?php print(strtoupper($PluginLanguage['Screen']['0029'])); ?></h2></div>
                <div class="white" style="min-height:430px;">
                    <form id="people-list-form" action="<?php InterfaceAppURL(); ?>/octautomation/people/" method="post">
                        <?php if (isset($PageErrorMessage) == true): ?>
                            <h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
                        <?php elseif (isset($PageSuccessMessage) == true): ?>
                            <h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
                        <?php endif; ?>






                        <!-- Controls -->
                        <div style="position:relative;padding:9px 12px;">
                            <a href="#" class="icon dark js-showhide-columns" style="float:right;margin:0 5px 0 0;">Show/Hide Columns</a>
                            <a href="#" class="icon dark js-showhide-filters" style="margin:3px 0 0 5px; font-size:11px;">Show/Hide Filters</a>

                            <div id="column-setup-container" style="background:#DBE8F0;display:none;position:absolute;z-index:999;width:200px;right:0;top:-47px;border:1px solid #07C;border-right:none;">
                                <div class="form-box" style="background:#DBE8F0;padding:9px 12px;">
                                    <div class="inner">
                                        <h2 style="margin-top:0px;line-height: 20px;">Columns</h2>
                                        <h3 style="color:#616265;margin:5px 0 0 0;padding:0;font-size:12px;">Select columns to display</h3>
                                        <div style="background-color:#E9F0F6;border:1px solid #7f96a3;padding:5px;height:200px;overflow:auto;margin:9px 0;">
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="Identifier" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'Identifier', (in_array('Identifier', $ChosenColumns) == true ? true : false))); ?>> User ID</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="Email" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'Email', (in_array('Email', $ChosenColumns) == true ? true : false))); ?>> Email address</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="EntryDate" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'EntryDate', (in_array('EntryDate', $ChosenColumns) == true ? true : false))); ?>> Entry date</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="LastSeenDate" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'LastSeenDate', (in_array('LastSeenDate', $ChosenColumns) == true ? true : false))); ?>> Last seen date</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="IPAddress" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'IPAddress', (in_array('IPAddress', $ChosenColumns) == true ? true : false))); ?>> IP address</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="City" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'City', (in_array('City', $ChosenColumns) == true ? true : false))); ?>> City</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="Country" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'Country', (in_array('Country', $ChosenColumns) == true ? true : false))); ?>> Country</label>
                                            <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="SessionCounter" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', 'SessionCounter', (in_array('SessionCounter', $ChosenColumns) == true ? true : false))); ?>> Sessions</label>
                                            <?php if (count($PeopleMetaData) > 0): ?>
                                                <?php foreach (get_object_vars($PeopleMetaData) as $EachName => $EachRef): ?>
                                                    <label style="display:block;line-height:16px; font-size:11px;"><input type="checkbox" value="<?php print($EachRef); ?>" name="ShowColumns[]" <?php print(set_checkbox('ShowColumns[]', $EachRef, (in_array($EachRef, $ChosenColumns) == true ? true : false))); ?>> <?php print(ucfirst(str_replace('_', ' ', $EachName))); ?></label>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            <button class="default" type="submit" name="Command2" value="ChangeColumns">Update Columns</button>
                                            or <a href="#" id="hide-column-window-link">cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>







                        <!-- People filtering -->
                        <style>
                            .rule-row {
                                padding:5px;
                                background-color:#E9F0F6;
                                margin-bottom:2px;
                            }
                            .remove-rule-button {
                                float:right;
                                color:red !important;
                                font-weight:bold;
                                text-decoration: none;
                            }
                        </style>
                        <div class="people-list-filters" style="background-color:#07C;padding:10px;position:relative;display:none;">
                            <div style="background:white;border:1px solid #7f96a3;padding:10px;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;width:100%">
                                <div id="message-rules" style="border-bottom:none;">
                                    <p>
                                        <select name="SegmentOperator" id="SegmentOperator" style="width:auto;">
                                            <option value="and" <?php print(set_select('SegmentOperator', 'and', ($FilterEditMode == true ? ($MatchType == 'and' ? true : false) : true))); ?>>Match all rules</option>
                                            <option value="or" <?php print(set_select('SegmentOperator', 'or', ($FilterEditMode == true ? ($MatchType == 'or' ? true : false) : false))); ?>>Match any rules</option>
                                        </select>

                                        <button class="default add-rule-button" style="float:right;">+ Add new rule</button>
                                    </p>
                                    <div class="rule-rows" style="margin-top:8px;"></div>
                                    <p style="margin-top:7px;">
                                        <button class="default apply-filter-button"> Apply filters</button>
                                        <button class="default reset-filter-button"> Reset</button>
                                    </p>
                                </div>
                            </div>
                        </div>







                        <!-- People browse -->
                        <div class="grid-operations">
                            <div class="inner">
                                <div style="float:right">
                                    <span class="label" style="margin-right:9px;"><span class="data"><?php print(($TotalSearchResultPeople == 1 ? $TotalSearchResultPeople . ' person' : $TotalSearchResultPeople . ' people')); ?></span></span>
                                </div>
                                <span class="label">Select:</span>
                                <a href="#" class="select-all grid-select-all" targetgrid="subscribers-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>,
                                <a href="#" class="unselect-all grid-select-none" targetgrid="subscribers-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
                                -&nbsp;<a href="#" class="js-delete-selected main-action" targetform="subscribers-table-form"><?php print($PluginLanguage['Screen']['0050']); ?></a>
                            </div>
                        </div>

                        <div class="white" style="margin-top:24px;min-height:36px;width:706px;overflow:auto;border-top:1px solid #ccc;">
                            <table id="subscribers-table" border="0" cellspacing="0" cellpadding="0" class="small-grid" style="table-layout: fixed; width: auto; min-width: 100%;">
                                <tr id="subscribers-table-header">
                                    <th data-header="checkbox" width="25" style="padding-left:0;padding-right:0;"></th>
                                    <th data-header="gravatar" width="25" style="padding-left:0;padding-right:0;"></th>
                                    <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                                        <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                            <th style="padding-right:20px; white-space:nowrap;text-align: center;" data-header="<?php print($EachColumn); ?>"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/people/?sf=<?php print($EachColumn); ?>&st=<?php print($OrderByType == 'ASC' ? 'desc' : 'asc'); ?>" class="<?php print(($EachColumn == $OrderByField ? ($OrderByType == 'ASC' ? 'asc' : 'desc') : '')); ?>"><?php print($AvailableColumns[$EachColumn]); ?></a></th>
                                        <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                            <th style="padding-right:20px; white-space:nowrap;text-align: right;" data-header="<?php print($EachColumn); ?>"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/people/?sf=<?php print($EachColumn); ?>&st=<?php print($OrderByType == 'ASC' ? 'desc' : 'asc'); ?>" class="<?php print(($EachColumn == $OrderByField ? ($OrderByType == 'ASC' ? 'asc' : 'desc') : '')); ?>"><?php print($AvailableColumns[$EachColumn]); ?></a></th>
                                        <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                            <th style="padding-right:20px; white-space:nowrap;" data-header="<?php print($EachColumn); ?>"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/people/?sf=<?php print($EachColumn); ?>&st=<?php print($OrderByType == 'ASC' ? 'desc' : 'asc'); ?>" class="<?php print(($EachColumn == $OrderByField ? ($OrderByType == 'ASC' ? 'asc' : 'desc') : '')); ?>"><?php print($AvailableColumns[$EachColumn]); ?></a></th>
                                        <?php else: ?>
                                            <th style="padding-right:20px; white-space:nowrap;" data-header="<?php print($EachColumn); ?>"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/people/?sf=<?php print($EachColumn); ?>&st=<?php print($OrderByType == 'ASC' ? 'desc' : 'asc'); ?>" class="<?php print(($EachColumn == $OrderByField ? ($OrderByType == 'ASC' ? 'asc' : 'desc') : '')); ?>"><?php print($AvailableColumns[$EachColumn]); ?></a></th>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                                <?php if (is_array($People) == true && count($People) > 0): ?>
                                    <?php foreach ($People as $Index => $EachPerson): ?>
                                        <tr class="subscriber-row">
                                            <td style="padding-left:0;padding-right:0;white-space:nowrap; text-align: center;"><input class="grid-check-element" type="checkbox" name="SelectedPeople[]" value="<?php print($EachPerson->PersonID); ?>"></td>
                                            <td style="padding-left:0;padding-right:0;white-space:nowrap; text-align: center;"><img src="http://www.gravatar.com/avatar/<?php print(md5(strtolower(trim($EachPerson->Email)))); ?>?s=16&d=identicon" width="16" height="16" alt="Photo"></td>
                                            <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                                                <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                                    <td style="text-align:center; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                                    <td style="text-align:right; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                                    <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                <?php else: ?>
                                                    <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tr>
                            </table>
                        </div>



                        <div style="overflow: auto;">
                            <table class="subscribers-table" cellpadding="0" cellspacing="0" id="people-table" style="table-layout: fixed; width: auto; min-width: 100%;">
                            </table>
                        </div>









                        <input type="hidden" name="Command" id="Command" value="">
                        <input type="hidden" name="SelectedPeopleIDs" id="SelectedPeopleIDs" value="">

                        <?php print($OemproPagination->create_links()); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php InterfaceInstallationURL(); ?>/plugins/octautomation/js/init.js" type="text/javascript" charset="utf-8"></script>

<?php
$FilterFields = array();

foreach ($AvailableColumns as $Key => $Name) {
    if ($PeopleDataStructure[$Key] == 'date') {
        $OperatorTypes = 'operatorTypes.date';
    } elseif ($PeopleDataStructure[$Key] == 'number') {
        $OperatorTypes = 'operatorTypes.number';
    } elseif ($PeopleDataStructure[$Key] == 'text') {
        $OperatorTypes = 'operatorTypes.string';
    } else {
        $OperatorTypes = 'operatorTypes.string';
    }
    $FilterFields[] = "'" . $Key . "': {label: '" . $Name . "', operators:" . $OperatorTypes . ", value:'', options:''}";
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.js-showhide-filters').on('click', function (ev) {
            ev.preventDefault();
            $('.people-list-filters').toggle();
        });
        $('.js-showhide-columns').on('click', function (ev) {
            ev.preventDefault();
            if ($('#column-setup-container').is(':hidden') == true) {
                $('#column-setup-container').show();
                $('html').one('click', function () {
                    $('#column-setup-container').hide();
                });
                $('#column-setup-container *').one('click', function (ev) {
                    ev.stopPropagation();
                });
            } else {
                $('#column-setup-container').hide();
            }
            return false;
        });
        $('#hide-column-window-link').on('click', function (ev) {
            ev.preventDefault();
            $('#column-setup-container').hide();
        });

        $('.js-delete-selected').click(function () {
            var SelectedRows = $('table input[type="checkbox"]:checked');

            if (SelectedRows.length == 0)
                return false;

            var Response = confirm('Are you sure? Selected people will be deleted.');
            if (Response == false)
                return;

            var selectedIDs = [];

            SelectedRows.each(function () {
                selectedIDs.push($(this).val());
            })

            selectedIDs = selectedIDs.join(',');

            $('#SelectedPeopleIDs').val(selectedIDs);
            $('#Command').val('DeletePeople');

            $('#people-list-form').submit();
        })

        $('.select-all').on('click', function (ev) {
            ev.preventDefault();
            $('table input[type="checkbox"]').prop('checked', true);
        });
        $('.unselect-all').on('click', function (ev) {
            ev.preventDefault();
            $('table input[type="checkbox"]').removeAttr('checked');
        });

        $('#ListWhat').change(function () {
            var ListWhat = $(this).val();
            window.location.href = "<?php print(InterfaceAppURL(true)); ?>/octautomation/people/" + ListWhat;
        });
    });


    var operatorTypes = {
        empty: [],
        string: [['is', 'is'], ['is not', 'is not'], ['starts with', 'starts with'], ['ends with', 'ends with'], ['contains', 'contains']],
        date: [['more than', 'more than'], ['less than', 'less than'], ['exactly', 'exactly'], ['after', 'after'], ['on', 'on'], ['before', 'before']],
        number: [['is', 'is'], ['is not', 'is not'], ['greater than', 'greater than'], ['smaller than', 'smaller than']]
    };

    var availableColumnsAndOptions = {
        Empty: {label: 'Choose a property...', operators: operatorTypes.empty, value: '', options: ''},
<?php print(implode(',', $FilterFields)); ?>
    };


    // START - DON'T TOUCH THE RULES BELOW - THIS IS THE DEFAULT RULE THAT WILL BE DISPLAYED ON A NEW SEGMENT
    var segmentRules = [];

<?php
$SubmittedRuleColumns = $RuleColumns;
$SubmittedRuleOperators = $RuleOperators;
$SubmittedRuleValues = $RuleValues;

if (is_array($SubmittedRuleColumns) == true && count($SubmittedRuleColumns) > 0) {
    foreach ($SubmittedRuleColumns as $Index => $EachSubmittedRuleColumn):
        ?>
            segmentRules.push({column: '<?php print($EachSubmittedRuleColumn); ?>', operator: '<?php print($SubmittedRuleOperators[$Index]); ?>', value: '<?php print(str_replace(array("\\", "'"), array("", "\'"), $SubmittedRuleValues[$Index])); ?>'});
        <?php
    endforeach;
}
else {
    ?>
        segmentRules.push({column: 'Empty', operator: '', value: ''});
    <?php
}
?>

    var setupRuleBasedOnColumn = function (el, operator, value) {
        var $columnSelect = $(el),
                $row = $columnSelect.parent(),
                columnOptions = availableColumnsAndOptions[$columnSelect.val()],
                $columnOperatorSelect = $('<select name="RuleOperator[]" class="column-operator" style="margin-left:3px;width:120px;"></select>'),
                $columnValueInput;

        $row.find('.column-operator, .column-value').remove();

        var columnOperatorOptions = [];
        $.each(columnOptions.operators, function () {
            columnOperatorOptions.push('<option value="' + this[1] + '" ' + (operator == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
        });
        $columnOperatorSelect.html(columnOperatorOptions.join(''));
        $columnSelect.after($columnOperatorSelect);

        if (!columnOptions.options) {
            $columnValueInput = $('<input type="text" name="RuleValue[]" class="column-value" style="margin-left:3px;width:300px;">');
            if (value !== undefined)
                $columnValueInput.val(value);
        } else {
            $columnValueInput = $('<select name="RuleValue[]" class="column-value" style="margin-left:3px;width:170px;"></select>');
            var columnValueOptions = [];
            $.each(columnOptions.options, function () {
                columnValueOptions.push('<option value="' + this[1] + '" ' + (value == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
            });
            $columnValueInput.html(columnValueOptions.join(''));
        }
        if ($columnSelect.val() == 'Empty') {
            $columnOperatorSelect.hide();
            $columnValueInput.hide();
        } else {
            $columnOperatorSelect.show();
            $columnValueInput.show();
        }
        $columnOperatorSelect.after($columnValueInput);
        setupOperatorLabel(el);
    };
    var setupOperatorLabel = function (el) {
        var $ruleRow = $(el).parent();
        var $operatorSelect = $ruleRow.children('.column-operator');
        var $span = $('span.column-operator-label', $ruleRow);
        var operator = $operatorSelect.val();

        if ($span.length < 1) {
            $span = $('<span class="column-operator-label"></span>').css('color', '#616265').css('margin-left', '5px');
            $ruleRow.append($span);
        }
        if (operator == 'more than') {
            $span.text('days');
        } else if (operator == 'less than') {
            $span.text('days');
        } else if (operator == 'exactly') {
            $span.text('days');
        } else if (operator == 'after') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'before') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'on') {
            $span.text('YYYY-MM-DD');
        } else {
            $('.column-operator-label', $ruleRow).remove();
        }
    };
    var drawSelectedFilterBorder = function () {
        var filterBoxHeight = $('#filter-box').outerHeight();
        var selectedFilterListItem = $('.filter-list li.selected');
        if (selectedFilterListItem.length < 1)
            return;
        var liPos = selectedFilterListItem.position().top;
        $('.filter-list li.selected .border').height(filterBoxHeight);
        $('.filter-list li.selected .border').css('top', '-' + liPos + 'px');
    };
    var removeRule = function (ruleRow) {
        var $ruleRow = $(ruleRow);
        if ($('.rule-row').length == 1)
            return;
        $ruleRow.remove();
        drawSelectedFilterBorder();
        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    };
    var addRule = function () {
        var $lastRuleRow = $('.rule-row').last(),
                $cloneRuleRow = $lastRuleRow.clone();

        $cloneRuleRow.find('.column-operator, .column-value').remove();
        $lastRuleRow.after($cloneRuleRow);
        setupRuleBasedOnColumn($cloneRuleRow.find('.rule-column'));
        drawSelectedFilterBorder();
        if ($('.rule-row').length > 1) {
            $('.remove-rule-button').show();
        }
    };
    var populateRules = function (rules) {
        $.each(rules, function () {
            var $ruleRow = $('<div class="rule-row"></div>'),
                    $ruleColumnSelect = $('<select name="RuleColumn[]" class="rule-column" style="width:130px;"></select>'),
                    ruleObject = this;

            var ruleColumnSelectOptions = [];
            $.each(availableColumnsAndOptions, function (columnId, columnOptions) {
                ruleColumnSelectOptions.push('<option value="' + columnId + '" ' + (ruleObject.column == columnId ? 'selected' : '') + '>' + columnOptions.label + '</option>');
            });
            $ruleColumnSelect.html(ruleColumnSelectOptions.join(''));
            $ruleRow.append($ruleColumnSelect);

            $ruleRow.append($('<a href="#" class="remove-rule-button link">x</a>'));

            $('.rule-rows').append($ruleRow);
            setupRuleBasedOnColumn($ruleColumnSelect, ruleObject.operator, ruleObject.value);
        });
    };

    window.estimatedRecipientsRequest = null;

    $(document).ready(function () {
        $('.apply-filter-button').on('click', function (ev) {
            $('#Command').val('ApplyFilter');
        });

        $('.reset-filter-button').on('click', function (ev) {
            $('#Command').val('ResetFilter');
        });

        $('.rule-rows').on('change', '.rule-column', function (event) {
            setupRuleBasedOnColumn(this);
        });
        $('.rule-rows').on('change', '.column-operator', function (event) {
            setupOperatorLabel(this);
        });
        $('.rule-rows').on('change', '.column-value', function (event) {
        });
        $('.rule-rows').on('click', '.remove-rule-button', function (event) {
            event.preventDefault();
            removeRule($(this).parent());
            showNewFilterOption();
        });
        $('.add-rule-button').on('click', function (event) {
            event.preventDefault();
            addRule();
            showNewFilterOption();
        });
        $('.rule-column').each(function () {
            setupRuleBasedOnColumn(this);
        });
        populateRules(segmentRules);

        function showNewFilterOption() {
            $('.filter-list li.selected').removeClass('selected');
            $('#new-filter-box').show();
            $('#browsing-filter-name').html('&nbsp;');
        }

        $('#filter-box input, #filter-box select').on('change', function () {
            showNewFilterOption();
        });
        drawSelectedFilterBorder();

        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    });
</script>





<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer_1.php'); ?>
