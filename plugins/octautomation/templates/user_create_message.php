<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<style>
    #BuilderPlainText {
        position:absolute;
        left:-1000px;
        width:131px;
    }
</style>

<?php
if ($EditMode == true) {
    $MessageRules = json_decode($Message->Rules);

    $MatchType = $MessageRules->MatchType;
    $RuleColumns = $MessageRules->RuleColumn;
    $RuleOperators = $MessageRules->RuleOperator;
    $RuleValues = $MessageRules->RuleValue;
}

$FilterPreset = false;
if (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['CreateMessage']) == true && $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['CreateMessage'] == true && isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true && is_array($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && count($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) > 0) {
    $FilterPreset = true;
    $MatchType = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'];
    $RuleColumns = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns'];
    $RuleOperators = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'];
    $RuleValues = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'];
    unset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['CreateMessage']);
}

$MergeTags = array();
$MergeTags_Subject = array();

foreach ($AvailableColumns as $Key => $Name) {
    $MergeTag = '%' . $Name . '%';
    $MergeTags[] = "{label: '" . str_replace("'", "\\'", $Name) . "', value: '" . $MergeTag . "'}";
    $MergeTags_Subject[$Key] = $Name;
}
?>

<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.js"></script>
<script>
    var jQueryAutomation = $.noConflict(true);
</script>
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/ckeditor_builder/ckeditor.js"></script>

<?php include('user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light portlet-transparent" style="margin-top: 15px">
                            <div class="portlet-title" style="margin-bottom: 0;">
                                <div class="caption">
                                    <?php if ($EditMode == true): ?>
                                        <span class="caption-subject bold font-purple-sharp"><?php print(($PluginLanguage['Screen']['0109'])); ?></span> 
                                    <?php else: ?>
                                        <span class="caption-subject bold font-purple-sharp"><?php print(($PluginLanguage['Screen']['0054'])); ?></span> 
                                    <?php endif; ?>
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" > 
                                        <?php if ($EditMode == true): ?>
                                            &nbsp;
                                        <?php else: ?>
                                            &nbsp;
                                            <!--<a id="" class="btn default btn-transparen btn-sm" href="<?php InterfaceAppURL(); ?>/octautomation/create_message"><strong><?php print(($PluginLanguage['Screen']['0003'])); ?></strong></a>-->
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="white">
                                    <?php if ($EditMode == true): ?>
                                        <form id="form1" action="<?php InterfaceAppURL(); ?>/octautomation/message/<?php print($Message->MessageID); ?>" method="post">
                                        <?php else: ?>
                                            <form id="form1" action="<?php InterfaceAppURL(); ?>/octautomation/create_message/" method="post">
                                            <?php endif; ?>
                                            <?php if (isset($PageErrorMessage) == true && $PageErrorMessage != ''): ?>
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <?php print($PageErrorMessage); ?>
                                                </div>
                                            <?php elseif (isset($PageSuccessMessage) == true && $PageSuccessMessage != ''): ?>
                                                <div class="alert alert-info alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <strong><?php print($PageSuccessMessage); ?></strong>
                                                </div>
                                            <?php endif; ?>
                                                <div class="portlet box purple-soft" style="margin-bottom: 10px">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <?php InterfaceLanguage('Screen', '9200'); ?> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body" style="padding-bottom: 8px">
                                                    <div id="message-rules" style="border-bottom:none;">
                                                        <p>
                                                            <select name="SegmentOperator" id="SegmentOperator" style="width:auto;">
                                                                <option value="and" <?php print(set_select('SegmentOperator', 'and', ($EditMode == true ? ($MatchType == 'and' ? true : false) : ($FilterPreset == true ? ($MatchType == 'and' ? true : false) : true)))); ?>>Match all rules</option>
                                                                <option value="or" <?php print(set_select('SegmentOperator', 'or', ($EditMode == true ? ($MatchType == 'or' ? true : false) : ($FilterPreset == true ? ($MatchType == 'or' ? true : false) : false)))); ?>>Match any rules</option>
                                                            </select>
                                                        </p>
                                                        <div class="rule-rows" style="margin-top:8px;"></div>
                                                        <p style="margin-top:20px;">
                                                            <button class="btn default add-rule-button"><i class="fa fa-plus"></i> Add new rule</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Email parameters -->
                                            <div class="form-group clearfix">
                                                <label class="col-md-3 control-label" for="">Recipients:</label>
                                                <p style="padding-top:7px;margin:0;position:relative;left:9px;">~<span class="js-estimated-recipients"></span> (estimated)</p>
                                            </div>
                                            <div class="form-group clearfix <?php print((form_error('MessageName') != '' ? 'has-error' : '')); ?>">
                                                <label class="col-md-3 control-label" for="MessageName">Message Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="MessageName" value="<?php echo set_value('MessageName', ($EditMode == true ? $Message->MessageName : '')); ?>" id="MessageName" class="form-control" placeholder="For management purposes. Define the purpose of your message. Ex: Signup welcome email" />
                                                    <?php print(form_error('MessageName', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix <?php print((form_error('FromName') != '' ? 'has-error' : '')); ?> <?php print((form_error('FromEmail') != '' ? 'has-error' : '')); ?>">
                                                <label class="col-md-3 control-label" for="FromName">From:</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="FromName" value="<?php echo set_value('FromName', ($EditMode == true ? $Message->FromName : '')); ?>" id="FromName" class="form-control" placeholder="Name" />
                                                    <?php print(form_error('FromName', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" name="FromEmail" value="<?php echo set_value('FromEmail', ($EditMode == true ? $Message->FromEmail : '')); ?>" id="FromEmail" class="form-control" placeholder="Email address" />
                                                    <?php print(form_error('FromEmail', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix <?php print((form_error('ReplyToName') != '' ? 'has-error' : '')); ?> <?php print((form_error('ReplyToEmail') != '' ? 'has-error' : '')); ?>">
                                                <label class="col-md-3 control-label" for="ReplyToName">Reply To:</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="ReplyToName" value="<?php echo set_value('ReplyToName', ($EditMode == true ? $Message->ReplyToName : '')); ?>" id="ReplyToName" class="form-control" placeholder="Name" />
                                                    <?php print(form_error('ReplyToName', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" name="ReplyToEmail" value="<?php echo set_value('ReplyToEmail', ($EditMode == true ? $Message->ReplyToEmail : '')); ?>" id="ReplyToEmail" class="form-control" placeholder="Email address" />
                                                    <?php print(form_error('ReplyToEmail', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix <?php print((form_error('Subject') != '' ? 'has-error' : '')); ?>" id="form-row-Subject">
                                                <label class="col-md-3 control-label" for="Subject">Subject:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="Subject" id="Subject" value="<?php echo set_value('Subject', ($EditMode == true ? $Message->Subject : '')); ?>" class="form-control personalized" placeholder="For higher conversions, keep your email subject short and to the point" />
                                                    <?php print(form_error('Subject', '<span class="help-block">', '</span>')); ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="SubjectPersonalizer" id="SubjectPersonalizer" style="display:none;">
                                                        <option value=""><-- Personalize subject</option>
                                                        <?php foreach ($MergeTags_Subject as $Key => $Name): ?>
                                                            <option value="%<?php print($Name); ?>%"><?php print($Name); ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="form-group clearfix <?php print((form_error('PlainContent') != '' ? 'has-error' : '')); ?> <?php print((form_error('builder') != '' ? 'has-error' : '')); ?>">
                                                <textarea name="builder"></textarea>
                                                <textarea name="BuilderPlainText" id="BuilderPlainText"><?php print(set_value('BuilderPlainText', ($EditMode == true ? $Message->PlainContent : ''))); ?></textarea>
                                                <?php print(form_error('PlainContent', '<span class="help-block">', '</span>')); ?>
                                                <?php print(form_error('builder', '<span class="help-block">', '</span>')); ?>
                                            </div>
                                            <input type="hidden" name="Command" id="Command" value="" />
                                            <?php if ($EditMode == true): ?>
                                                <input type="hidden" name="MessageID" id="MessageID" value="<?php print($Message->MessageID); ?>" />
                                            <?php endif; ?>
                                        </form>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if ($EditMode == true): ?>
                                                <?php if ($Message->Status == 'Draft'): ?>
                                                    <a class="btn default js-activate-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0055'])); ?></strong></a>
                                                    <a class="btn default js-save-as-draft-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0056'])); ?></strong></a>
                                                    <a class="btn default js-delete-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0106'])); ?></strong></a>
                                                <?php elseif ($Message->Status == 'Active'): ?>
                                                    <a class="btn default js-activate-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0107'])); ?></strong></a>
                                                    <a class="btn default js-pause-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0108'])); ?></strong></a>
                                                    <a class="btn default js-delete-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0106'])); ?></strong></a>
                                                <?php elseif ($Message->Status == 'Paused'): ?>
                                                    <a class="btn default js-activate-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0055'])); ?></strong></a>
                                                    <a class="btn default js-delete-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0106'])); ?></strong></a>
                                                <?php elseif ($Message->Status == 'Deleted'): ?>
                                                    <a class="btn default js-activate-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0055'])); ?></strong></a>
                                                    <a class="btn default js-delete-button" id="form-button" href="#"><strong><?php print(strtoupper($PluginLanguage['Screen']['0107'])); ?></strong></a>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <a class="btn default js-activate-button" id="form-button" href="#" ><strong><?php print(strtoupper($PluginLanguage['Screen']['0055'])); ?></strong></a>
                                                <a class="btn default js-save-as-draft-button" id="form-button" href="#" ><strong><?php print(strtoupper($PluginLanguage['Screen']['0056'])); ?></strong></a>
                                                <a class="btn default js-cancel-button" id="form-button" href="#" ><strong><?php print(strtoupper($PluginLanguage['Screen']['0035'])); ?></strong></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php InterfaceInstallationURL(); ?>/plugins/octautomation/js/init.js" type="text/javascript" charset="utf-8"></script>

<?php
$DefaultHTMLContent = '';

if (isset($_REQUEST['builder']) == true && $_REQUEST['builder'] != '') {
    /*
     * IMPORTANT!!!! READ THIS FIRST BEFORE MAKING ANY CHANGES!
     * Use $_REQUEST['builder'] here to avoid CodeIgniter's form_prep html entity encryption.
     * Also, don't forget to replace \r, \n, \t and '
     * */
    $DefaultHTMLContent = str_replace(array("\n", "\r", "\t"), array("", "", ""), $_REQUEST['builder']);
    $DefaultHTMLContent = preg_replace("/(?<!\\\\)'/uim", "\\\\'", $DefaultHTMLContent);
} else {
    if ($EditMode == true) {
        $DefaultHTMLContent = str_replace(array("\n", "\r", "\t"), array("", "", ""), $Message->HTMLContent);
        $DefaultHTMLContent = preg_replace("/(?<!\\\\)'/uim", "\\\\'", $DefaultHTMLContent);
    } else {
        $DefaultHTMLContent = '<!doctype html><html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><meta charset="UTF-8"><title>Document</title></head><body></body></html>';
    }
}

$FilterFields = array();

foreach ($AvailableColumns as $Key => $Name) {
    if ($PeopleDataStructure[$Key] == 'date') {
        $OperatorTypes = 'operatorTypes.date';
    } elseif ($PeopleDataStructure[$Key] == 'number') {
        $OperatorTypes = 'operatorTypes.number';
    } elseif ($PeopleDataStructure[$Key] == 'text') {
        $OperatorTypes = 'operatorTypes.string';
    } else {
        $OperatorTypes = 'operatorTypes.string';
    }
    $FilterFields[] = "'" . $Key . "': {label: '" . $Name . "', operators:" . $OperatorTypes . ", value:'', options:''}";
}
?>

<script>
    var focusTimeOut = null;
    var plugInUrl = '<?php InterfaceAppURL(); ?>/octautomation/send_test_email';

    jQueryAutomation.fn.extend({
        insertAtCaret: function (myValue) {
            var obj;
            if (typeof this[0].name != 'undefined')
                obj = this[0];
            else
                obj = this;
            if ($.browser.msie) {
                obj.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                obj.focus();
            } else if ($.browser.mozilla || $.browser.webkit) {
                var startPos = obj.selectionStart;
                var endPos = obj.selectionEnd;
                var scrollTop = obj.scrollTop;
                obj.value = obj.value.substring(0, startPos) + myValue + obj.value.substring(endPos, obj.value.length);
                obj.focus();
                obj.selectionStart = startPos + myValue.length;
                obj.selectionEnd = startPos + myValue.length;
                obj.scrollTop = scrollTop;
            } else {
                obj.value += myValue;
                obj.focus();
            }
        }
    });

    function initSubjectPersonalizer() {
        jQueryAutomation('#SubjectPersonalizer').on('change', function () {
            jQueryAutomation('#Subject').focus().insertAtCaret(jQueryAutomation(this).val());
            jQueryAutomation('option', this).first().attr('selected', 'selected');
        });
    }

    jQueryAutomation(document).ready(function () {
        jQueryAutomation('#Subject').on('focus', function () {
            jQueryAutomation('#SubjectPersonalizer').fadeIn('fast');
        });
        jQueryAutomation('#Subject').on('blur', function () {
            focusTimeOut = setTimeout(function () {
                jQueryAutomation('#SubjectPersonalizer').fadeOut('fast');
            }, 500);
        });
        jQueryAutomation('#SubjectPersonalizer').on('focus', function (ev) {
            ev.preventDefault();
            ev.stopImmediatePropagation();
            clearTimeout(focusTimeOut);
        });
        jQueryAutomation('#SubjectPersonalizer').on('blur', function (ev) {
            jQueryAutomation('#SubjectPersonalizer').fadeOut('fast');
        });

        var builder = CKEDITOR.replace('builder', {
            customConfig: '<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/ckeditor_builder/config_engage.js',
            slpersonalizer: {
                tags: [
                    {
                        groupLabel: 'Subscriber tags',
                        tags: [
<?php print(implode(',', $MergeTags)); ?>
                        ]
                    }
                ]
            },
            slpreview_email: '<?php print($UserInformation['Email']); ?>'
        });

        CKEDITOR.instances.builder.on('pluginsLoaded', function () {
            CKEDITOR.instances.builder.setData('<?php print($DefaultHTMLContent); ?>');
        });
    });

    var operatorTypes = {
        empty: [],
        string: [['is', 'is'], ['is not', 'is not'], ['starts with', 'starts with'], ['ends with', 'ends with'], ['contains', 'contains']],
        date: [['more than', 'more than'], ['less than', 'less than'], ['exactly', 'exactly'], ['after', 'after'], ['on', 'on'], ['before', 'before']],
        number: [['is', 'is'], ['is not', 'is not'], ['greater than', 'greater than'], ['smaller than', 'smaller than']]
    };
    var availableColumnsAndOptions = {
        Empty: {label: 'Choose a property...', operators: operatorTypes.empty, value: '', options: ''},
<?php print(implode(',', $FilterFields)); ?>
    };

    // START - DON'T TOUCH THE RULES BELOW - THIS IS THE DEFAULT RULE THAT WILL BE DISPLAYED ON A NEW SEGMENT
    var segmentRules = [];

<?php
$SubmittedRuleColumns = $RuleColumns;
$SubmittedRuleOperators = $RuleOperators;
$SubmittedRuleValues = $RuleValues;

if (is_array($SubmittedRuleColumns) == true && count($SubmittedRuleColumns) > 0) {
    foreach ($SubmittedRuleColumns as $Index => $EachSubmittedRuleColumn):
        ?>
            segmentRules.push({column: '<?php print($EachSubmittedRuleColumn); ?>', operator: '<?php print($SubmittedRuleOperators[$Index]); ?>', value: '<?php print(str_replace(array("\\", "'"), array("", "\'"), $SubmittedRuleValues[$Index])); ?>'});
        <?php
    endforeach;
}
else {
    ?>
        segmentRules.push({column: 'Empty', operator: '', value: ''});
    <?php
}
?>

    var setupRuleBasedOnColumn = function (el, operator, value) {
        var $columnSelect = jQueryAutomation(el),
                $row = $columnSelect.parent(),
                columnOptions = availableColumnsAndOptions[$columnSelect.val()],
                $columnOperatorSelect = jQueryAutomation('<select name="RuleOperator[]" class="column-operator" style="margin-left:3px;width:120px;"></select>'),
                $columnValueInput;

        $row.find('.column-operator, .column-value').remove();

        var columnOperatorOptions = [];
        jQueryAutomation.each(columnOptions.operators, function () {
            columnOperatorOptions.push('<option value="' + this[1] + '" ' + (operator == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
        });
        $columnOperatorSelect.html(columnOperatorOptions.join(''));
        $columnSelect.after($columnOperatorSelect);

        if (!columnOptions.options) {
            $columnValueInput = jQueryAutomation('<input type="text" name="RuleValue[]" class="column-value" style="margin-left:3px;width:300px;">');
            if (value !== undefined)
                $columnValueInput.val(value);
        } else {
            $columnValueInput = jQueryAutomation('<select name="RuleValue[]" class="column-value" style="margin-left:3px;width:170px;"></select>');
            var columnValueOptions = [];
            jQueryAutomation.each(columnOptions.options, function () {
                columnValueOptions.push('<option value="' + this[1] + '" ' + (value == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
            });
            $columnValueInput.html(columnValueOptions.join(''));
        }
        if ($columnSelect.val() == 'Empty') {
            $columnOperatorSelect.hide();
            $columnValueInput.hide();
        } else {
            $columnOperatorSelect.show();
            $columnValueInput.show();
        }
        $columnOperatorSelect.after($columnValueInput);
        setupOperatorLabel(el);
    };
    var setupOperatorLabel = function (el) {
        var $ruleRow = jQueryAutomation(el).parent();
        var $operatorSelect = $ruleRow.children('.column-operator');
        var $span = jQueryAutomation('span.column-operator-label', $ruleRow);
        var operator = $operatorSelect.val();

        if ($span.length < 1) {
            $span = jQueryAutomation('<span class="column-operator-label"></span>').css('color', '#616265').css('margin-left', '5px');
            $ruleRow.append($span);
        }
        if (operator == 'more than') {
            $span.text('days');
        } else if (operator == 'less than') {
            $span.text('days');
        } else if (operator == 'exactly') {
            $span.text('days');
        } else if (operator == 'after') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'before') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'on') {
            $span.text('YYYY-MM-DD');
        } else {
            jQueryAutomation('.column-operator-label', $ruleRow).remove();
        }
    };
    var drawSelectedFilterBorder = function () {
        var filterBoxHeight = jQueryAutomation('#filter-box').outerHeight();
        var selectedFilterListItem = jQueryAutomation('.filter-list li.selected');
        if (selectedFilterListItem.length < 1)
            return;
        var liPos = selectedFilterListItem.position().top;
        jQueryAutomation('.filter-list li.selected .border').height(filterBoxHeight);
        jQueryAutomation('.filter-list li.selected .border').css('top', '-' + liPos + 'px');
    };
    var removeRule = function (ruleRow) {
        var $ruleRow = jQueryAutomation(ruleRow);
        if (jQueryAutomation('.rule-row').length == 1)
            return;
        $ruleRow.remove();
        drawSelectedFilterBorder();
        if (jQueryAutomation('.rule-row').length == 1) {
            jQueryAutomation('.remove-rule-button').hide();
        } else {
            jQueryAutomation('.remove-rule-button').show();
        }
    };
    var addRule = function () {
        var $lastRuleRow = jQueryAutomation('.rule-row').last(),
                $cloneRuleRow = $lastRuleRow.clone();

        $cloneRuleRow.find('.column-operator, .column-value').remove();
        $lastRuleRow.after($cloneRuleRow);
        setupRuleBasedOnColumn($cloneRuleRow.find('.rule-column'));
        drawSelectedFilterBorder();
        if (jQueryAutomation('.rule-row').length > 1) {
            jQueryAutomation('.remove-rule-button').show();
        }
    };
    var populateRules = function (rules) {
        jQueryAutomation.each(rules, function () {
            var $ruleRow = jQueryAutomation('<div class="rule-row"></div>'),
                    $ruleColumnSelect = jQueryAutomation('<select name="RuleColumn[]" class="rule-column" style="width:180px;"></select>'),
                    ruleObject = this;

            var ruleColumnSelectOptions = [];
            jQueryAutomation.each(availableColumnsAndOptions, function (columnId, columnOptions) {
                ruleColumnSelectOptions.push('<option value="' + columnId + '" ' + (ruleObject.column == columnId ? 'selected' : '') + '>' + columnOptions.label + '</option>');
            });
            $ruleColumnSelect.html(ruleColumnSelectOptions.join(''));
            $ruleRow.append($ruleColumnSelect);

            $ruleRow.append(jQueryAutomation('<a href="#" class="remove-rule-button link">x</a>'));

            jQueryAutomation('.rule-rows').append($ruleRow);
            setupRuleBasedOnColumn($ruleColumnSelect, ruleObject.operator, ruleObject.value);
        });
    };

    window.estimatedRecipientsRequest = null;
    var filterResults = function () {
        var ruleColumns = jQueryAutomation('.rule-column'),
                ruleOperators = jQueryAutomation('.column-operator'),
                ruleValues = jQueryAutomation('.column-value'),
                rules = [];

        ruleColumns.each(function (idx) {
            var rule = {};
            rule.column = jQueryAutomation(this).val();
            rule.operator = jQueryAutomation(ruleOperators[idx]).val();
            rule.val = jQueryAutomation(ruleValues[idx]).val();
            rules.push(rule);
        });

        if (window.estimatedRecipientsRequest && window.estimatedRecipientsRequest.readyState != 4) {
            window.estimatedRecipientsRequest.abort();
        }
        window.estimatedRecipientsRequest = jQueryAutomation.post('<?php InterfaceAppURL(); ?>/octautomation/estimate_recipients/new', {rules: rules, match_type: jQueryAutomation('#SegmentOperator').val()}, function (data) {
            jQueryAutomation('.js-estimated-recipients').html(data.recipients);
        }, 'json');
    }

    jQueryAutomation(document).ready(function () {
        jQueryAutomation('#SegmentOperator').on('change', function () {
            filterResults();
        });

        initSubjectPersonalizer();

        jQueryAutomation('.js-cancel-button').on('click', function (ev) {
            window.location.href = '<?php print(InterfaceAppURL(true) . "/octautomation/messages"); ?>';
            return false;
        });
        jQueryAutomation('.js-activate-button').on('click', function (ev) {
            ev.preventDefault();
            jQueryAutomation('#Command').val('CreateMessage');
            CKEDITOR.instances.builder.setMode('wysiwyg');
            setTimeout(function () {
                var docu = jQueryAutomation(CKEDITOR.instances.builder.window.getFrame().$).contents();
                jQueryAutomation('#slblocktool', docu).remove();
                jQueryAutomation('#form1').submit();
            }, 200);
        });
        jQueryAutomation('.js-save-as-draft-button').on('click', function (ev) {
            ev.preventDefault();
            jQueryAutomation('#Command').val('CreateDraftMessage');
            CKEDITOR.instances.builder.setMode('wysiwyg');
            setTimeout(function () {
                var docu = jQueryAutomation(CKEDITOR.instances.builder.window.getFrame().$).contents();
                jQueryAutomation('#slblocktool', docu).remove();
                jQueryAutomation('#form1').submit();
            }, 200);
        });
        jQueryAutomation('.js-delete-button').on('click', function (ev) {
            ev.preventDefault();
            jQueryAutomation('#Command').val('DeleteMessage');
            CKEDITOR.instances.builder.setMode('wysiwyg');
            setTimeout(function () {
                var docu = jQueryAutomation(CKEDITOR.instances.builder.window.getFrame().$).contents();
                jQueryAutomation('#slblocktool', docu).remove();
                jQueryAutomation('#form1').submit();
            }, 200);
        });
        jQueryAutomation('.js-pause-button').on('click', function (ev) {
            ev.preventDefault();
            jQueryAutomation('#Command').val('PauseMessage');
            CKEDITOR.instances.builder.setMode('wysiwyg');
            setTimeout(function () {
                var docu = jQueryAutomation(CKEDITOR.instances.builder.window.getFrame().$).contents();
                jQueryAutomation('#slblocktool', docu).remove();
                jQueryAutomation('#form1').submit();
            }, 200);
        });
        jQueryAutomation('.rule-rows').on('change', '.rule-column', function (event) {
            setupRuleBasedOnColumn(this);
            filterResults();
        });
        jQueryAutomation('.rule-rows').on('change', '.column-operator', function (event) {
            setupOperatorLabel(this);
            filterResults();
        });
        jQueryAutomation('.rule-rows').on('change', '.column-value', function (event) {
            filterResults();
        });

        jQueryAutomation('.rule-rows').on('click', '.remove-rule-button', function (event) {
            event.preventDefault();
            removeRule(jQueryAutomation(this).parent());
            showNewFilterOption();
            filterResults();
        });
        jQueryAutomation('.add-rule-button').on('click', function (event) {
            event.preventDefault();
            addRule();
            showNewFilterOption();
            filterResults();
        });
        jQueryAutomation('.rule-column').each(function () {
            setupRuleBasedOnColumn(this);
        });
        populateRules(segmentRules);

        function showNewFilterOption() {
            jQueryAutomation('.filter-list li.selected').removeClass('selected');
            jQueryAutomation('#new-filter-box').show();
            jQueryAutomation('#browsing-filter-name').html('&nbsp;');
        }

        jQueryAutomation('#filter-box input, #filter-box select').on('change', function () {
            showNewFilterOption();
        });
        drawSelectedFilterBorder();

        if (jQueryAutomation('.rule-row').length == 1) {
            jQueryAutomation('.remove-rule-button').hide();
        } else {
            jQueryAutomation('.remove-rule-button').show();
        }

        filterResults();
    });

</script>

<style>
    .cke_combo__slpersonalizer .cke_combo_text {
        width:65px !important;
    }
    .cke_combo__sltesterengage .cke_combo_text {
        width:90px !important;
    }
    .cke_button__sltester_icon,
    .cke_button__sltesterengage_icon,
    .cke_button__slplaintext_icon,
    .cke_button__sendloopmedialibrary_icon {
        display: none;
    }
    .cke_button__sltester_label,
    .cke_button__sltesterengage_label,
    .cke_button__slplaintext_label,
    .cke_button__sendloopmedialibrary_label {
        display: inline;
    }
    .cke_button_disabled .cke_button_label {
        color:#DDD;
    }
</style>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>