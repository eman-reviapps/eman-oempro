<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_header.php'); ?>

<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.js"></script>
<script>
	var jQueryAutomation = $.noConflict(false);
</script>
<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/flot/excanvas.min.js"></script><![endif]-->
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/flot/jquery.flot.js"></script>
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.flot.tooltip.js"></script>
<script src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/js/jquery.sparkline.min.js"></script>

<?php
$ChartData1 = array();
$ChartXAxis = array();

for ($DayCounter = 0; $DayCounter<=30; $DayCounter++)
{
	$Date = date("jS M'y", time() - (86400 * $DayCounter));
	$Date2 = date("Y-m-d", time() - (86400 * $DayCounter));
	$Value1 = (isset($MessageActivityLog[$Date2]->TotalMessagesSent) == true ? $MessageActivityLog[$Date2]->TotalMessagesSent : 0);
	$ChartData1[] = '['.$DayCounter.', '.$Value1.', "'.$Date.'"]';
	if ($DayCounter % 5 == 0 && $DayCounter != 0 && $DayCounter != 30)
	{
		$ChartXAxis[] = '['.$DayCounter.', "'.$Date.'"]';
	}
}
?>

<script>
	jQueryAutomation(function ($) {
		$.plot('#chartdiv', [
			{
				color:'#85B4F2',
				label:'<?php print($PluginLanguage['Screen']['0077']); ?>',
				shadowSize:0,
				lines: {
					show:true,
					fill:true,
					lineWidth:2
				},
				points: {
					show:true
				},
				data: [<?php print(implode(',', $ChartData1)); ?>]
			}
		], {
			legend: {
				show: true,
				labelBoxBorderColor:'#FFFFFF',
				position:'se',
				backgroundColor:null,
				backgroundOpacity:0,
				sorted:true,
				noColumns:2,
				container: $('#legend-container')
			},
			grid: {
				show:true,
				color:'#E7EFF5',
				borderWidth: {
					top:0,
					right:0,
					bottom:1,
					left:0
				},
				borderColor:'#666666',
				hoverable: true
			},
			xaxis: {
				ticks: [<?php print(implode(',', $ChartXAxis)); ?>]
			},
			yaxis: {
			},
			tooltip: true,
			tooltipOpts: {
				content: "<strong>%s</strong><br>%NameX<br>%y"
			}
		});




	});
</script>

<style>
	#legend-container table {
		padding-bottom: 0;
		margin-bottom: 0;
	}

	#legend-container table td.legendColorBox {
		width: 20px !important;
		padding-right: 0;
		margin-right: 0;
	}

	#legend-container table td.legendLabel {
		width: 100px !important;
		font-weight: bold;
		color: #666666;
	}
</style>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php print(strtoupper($PluginLanguage['Screen']['0001'])); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - Start -->
<div class="container">
	<form id="form1" method="post" action="">
		<div class="span-23">
			<div id="page-shadow">
				<div id="page">
					<div class="page-bar">
						<div class="actions">
							<a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/admin_settings/"><?php print($PluginLanguage['Screen']['0115']); ?></a>
						</div>
						<ul class="livetabs" tabcollection="octautomation-reports">
							<li id="tab-overview">
								<a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/admin_reports/overview"><?php print($PluginLanguage['Screen']['0132']); ?></a>
							</li>
						</ul>
					</div>
					<div class="white">
						<div tabcollection="octautomation-reports" id="tab-content-overview">

							<div class="inner flash-chart-container" id="activity-chart-container" style="padding-top:18px;">
								<div id="chartdiv" style="margin-left:10px;height:190px;width:900px; "></div>
								<div id="legend-container" style="height:30px;width:170px;margin:0 auto; "></div>
							</div>

							<div class="custom-column-container cols-2 clearfix">
								<div class="col">
									<span class="data big"><?php print((isset($MessageStats['Today']->TotalMessagesSent) == true ? number_format($MessageStats['Today']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0157']); ?></span><br />
									<span class="data"><?php print((isset($MessageStats['Yesterday']->TotalMessagesSent) == true ? number_format($MessageStats['Yesterday']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label small"><?php print($PluginLanguage['Screen']['0158']); ?> </span><br />


									<span class="data big"><?php print((isset($MessageStats['ThisMonth']->TotalMessagesSent) == true ? number_format($MessageStats['ThisMonth']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0159']); ?></span><br />
									<span class="data"><?php print((isset($MessageStats['LastMonth']->TotalMessagesSent) == true ? number_format($MessageStats['LastMonth']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label small"><?php print($PluginLanguage['Screen']['0160']); ?> </span><br />

									<span class="data big"><?php print((isset($MessageStats['ThisYear']->TotalMessagesSent) == true ? number_format($MessageStats['ThisYear']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label big"><?php print($PluginLanguage['Screen']['0161']); ?></span><br />
									<span class="data"><?php print((isset($MessageStats['LastYear']->TotalMessagesSent) == true ? number_format($MessageStats['LastYear']->TotalMessagesSent, 0) : 0)); ?></span> <span class="data-label small"><?php print($PluginLanguage['Screen']['0162']); ?> </span><br />
								</div>
								<div class="col">
									<div style="background-color:#f2f2f2;border:1px solid #e1e1e1;margin-bottom:18px;">
										<div style="padding:9px 12px;" class="clearfix">
											<span class="data-label" style="margin-left:0;"><?php print($PluginLanguage['Screen']['0163']); ?></span><br>
											<?php if ((is_bool($TopSenders) == true && $TopSenders == false) || (is_array($TopSenders) == true && count($TopSenders) == 0)): ?>
												<span class="data" style="margin-left:0;font-weight: normal;"><?php print($PluginLanguage['Screen']['0164']); ?></span><br>
											<?php else: ?>
												<table>
													<tr>
														<th style="padding-left:0;text-align: left;color:#595959;"><?php print($PluginLanguage['Screen']['0165']); ?></th>
														<th style="text-align: right;color:#595959;"><?php print($PluginLanguage['Screen']['0166']); ?></th>
														<th style="text-align: right;color:#595959;"><?php print($PluginLanguage['Screen']['0167']); ?></th>
													</tr>
													<?php foreach ($TopSenders as $Index=>$EachSender): ?>
														<tr>
															<td style="padding-left:0;"><a href="<?php InterfaceAppUrl(); ?>/admin/users/edit/<?php print($EachSender->UserInfo['UserID']); ?>"><?php print($EachSender->UserInfo['FirstName'].' '.$EachSender->UserInfo['LastName']); ?></a></td>
															<td style="margin:0;color:#595959;text-align:right;"><?php print(number_format($EachSender->TotalMessagesSent, 0)); ?></td>
															<td style="margin:0;color:#595959;text-align:right;">
																<?php print(number_format($EachSender->TotalMessagesOpened, 0)); ?>
																<span style="font-size:0.85em;">(<?php print(number_format(($EachSender->TotalMessagesSent > 0 ? ((100 * $EachSender->TotalMessagesOpened) / $EachSender->TotalMessagesSent) : 0), 2)); ?>%)</span>
															</td>
														</tr>
													<?php endforeach; ?>
												</table>
											<?php endif; ?>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<!-- Page - End -->

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/admin_footer.php'); ?>
