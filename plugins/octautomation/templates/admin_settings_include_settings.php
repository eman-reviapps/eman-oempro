<form id="octautomation-form-settings" method="post" action="<?php InterfaceAppURL(); ?>/octautomation/admin_settings/settings/">
	<?php if (isset($PageErrorMessage) == true && $PageErrorMessage != ''): ?>
		<h3 class="form-legend error"><?php print($PageErrorMessage); ?></h3>
	<?php elseif (isset($PageSuccessMessage) == true && $PageSuccessMessage != ''): ?>
		<h3 class="form-legend success"><?php print($PageSuccessMessage); ?></h3>
	<?php elseif (validation_errors()): ?>
		<h3 class="form-legend error"><?php InterfaceLanguage('Screen', '0275', false); ?></h3>
	<?php else: ?>
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0186']); ?></h3>
	<?php endif; ?>

	<div class="form-row" id="form-row-DeliveryMethod">
		<label for="DeliveryMethod"><?php print($PluginLanguage['Screen']['0180']); ?>: *</label>
		<select name="DeliveryMethod" id="DeliveryMethod" class="select">
			<optgroup label="<?php print($PluginLanguage['Screen']['0183']); ?>">
				<option value="smtp" <?php echo $DeliveryMethodToDisplay == 'smtp' ? 'selected' : ''; ?>><?php print($PluginLanguage['Screen']['0199']); ?></option>
			</optgroup>
			<optgroup label="<?php print($PluginLanguage['Screen']['0184']); ?>">
				<option value="mailgun" <?php echo $DeliveryMethodToDisplay == 'mailgun' ? 'selected' : ''; ?>>Mailgun</option>
				<option value="mandrill" <?php echo $DeliveryMethodToDisplay == 'mandrill' ? 'selected' : ''; ?>>Mandrill</option>
				<option value="smtpcom" <?php echo $DeliveryMethodToDisplay == 'smtpcom' ? 'selected' : ''; ?>>SMTP.com (octeth.smtp.com)</option>
			</optgroup>
			<optgroup label="<?php print($PluginLanguage['Screen']['0185']); ?>">
				<option value="savetodirectory" <?php echo $DeliveryMethodToDisplay == 'savetodirectory' ? 'selected' : ''; ?>><?php print($PluginLanguage['Screen']['0182']); ?></option>
				<option value="powermta" <?php echo $DeliveryMethodToDisplay == 'powermta' ? 'selected' : ''; ?>><?php print($PluginLanguage['Screen']['0201']); ?></option>
			</optgroup>
		</select>
		<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0181']); ?></p></div>
	</div>


	<div id="settings-for-mailgun" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0133']); ?></h3>
		<div class="form-row no-bg">
			<p><?php print($PluginLanguage['Screen']['0134']); ?></p>
		</div>
		<div class="form-row" id="form-row-Mailgun_WebHookURL">
			<label for="Mailgun_WebHookURL"><?php print($PluginLanguage['Screen']['0178']); ?>: *</label>
			<input type="text" name="Mailgun_WebHookURL" value="<?php echo set_value('Mailgun_WebHookURL', $Mailgun_WebHookURL); ?>" id="Mailgun_WebHookURL" class="text" placeholder="" readonly="readonly" onclick="this.select();" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0179']); ?></p></div>
		</div>
		<div class="form-row <?php print((form_error('Mailgun_APIKey') != '' ? 'error' : '')); ?>" id="form-row-Mailgun_APIKey">
			<label for="Mailgun_APIKey"><?php print($PluginLanguage['Screen']['0135']); ?>: *</label>
			<input type="text" name="Mailgun_APIKey" value="<?php echo set_value('Mailgun_APIKey', (isset($MailServerSettings->APIKey) == true ? $MailServerSettings->APIKey : '')); ?>" id="Mailgun_APIKey" class="text" placeholder="key-23dsd0sd9ds23029sds0293029" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0136']); ?></p></div>
			<?php print(form_error('Mailgun_APIKey', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('Mailgun_Domain') != '' ? 'error' : '')); ?>" id="form-row-Mailgun_Domain">
			<label for="Mailgun_Domain"><?php print($PluginLanguage['Screen']['0137']); ?>: *</label>
			<input type="text" name="Mailgun_Domain" value="<?php echo set_value('Mailgun_Domain', (isset($MailServerSettings->Domain) == true ? $MailServerSettings->Domain : '')); ?>" id="Mailgun_Domain" class="text" placeholder="mydomain.com" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0138']); ?></p></div>
			<?php print(form_error('Mailgun_Domain', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row no-bg">
			<p><?php print($PluginLanguage['Screen']['0148']); ?></p>
		</div>
	</div>

	<div id="settings-for-mandrill" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0187']); ?></h3>
		<div class="form-row no-bg">
			<p><?php print($PluginLanguage['Screen']['0188']); ?></p>
		</div>
		<div class="form-row" id="form-row-Mandrill_WebHookURL">
			<label for="Mandrill_WebHookURL"><?php print($PluginLanguage['Screen']['0189']); ?>: *</label>
			<input type="text" name="Mandrill_WebHookURL" value="<?php echo set_value('Mandrill_WebHookURL', $Mandrill_WebHookURL); ?>" id="Mandrill_WebHookURL" class="text" placeholder="" readonly="readonly" onclick="this.select();" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0190']); ?></p></div>
		</div>
		<div class="form-row <?php print((form_error('Mandrill_Username') != '' ? 'error' : '')); ?>" id="form-row-Mandrill_Username">
			<label for="Mandrill_Username"><?php print($PluginLanguage['Screen']['0191']); ?>: *</label>
			<input type="text" name="Mandrill_Username" value="<?php echo set_value('Mandrill_Username', (isset($MailServerSettings->Username) == true ? $MailServerSettings->Username : '')); ?>" id="Mandrill_Username" class="text" placeholder="" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0192']); ?></p></div>
			<?php print(form_error('Mandrill_Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('Mandrill_Password') != '' ? 'error' : '')); ?>" id="form-row-Mandrill_Password">
			<label for="Mandrill_Password"><?php print($PluginLanguage['Screen']['0193']); ?>: *</label>
			<input type="text" name="Mandrill_Password" value="<?php echo set_value('Mandrill_Password', (isset($MailServerSettings->Password) == true ? $MailServerSettings->Password : '')); ?>" id="Mandrill_Password" class="text" placeholder="" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0192']); ?></p></div>
			<?php print(form_error('Mandrill_Password', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row no-bg">
			<p><?php print($PluginLanguage['Screen']['0194']); ?></p>
		</div>
	</div>
	
	<div id="settings-for-smtp" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0200']); ?></h3>
		<div class="form-row <?php print((form_error('SMTP_Host') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Host">
			<label for="SMTP_Host"><?php InterfaceLanguage('Screen', '0379', false); ?>: *</label>
			<input type="text" name="SMTP_Host" value="<?php echo set_value('SMTP_Host', (isset($MailServerSettings->Hostname) == true ? $MailServerSettings->Hostname : '')); ?>" id="SMTP_Host" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0380', false, '', false); ?></p></div>
			<?php print(form_error('SMTP_Host', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTP_Port') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Port">
			<label for="SMTP_Port"><?php InterfaceLanguage('Screen', '0381', false); ?>: *</label>
			<input type="text" name="SMTP_Port" value="<?php echo set_value('SMTP_Port', (isset($MailServerSettings->Port) == true ? $MailServerSettings->Port : '')); ?>" id="SMTP_Port" class="text" />
			<?php print(form_error('SMTP_Port', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTP_Encryption') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Encryption">
			<label for="SMTP_Encryption"><?php InterfaceLanguage('Screen', '0382', false); ?>:</label>
			<select name="SMTP_Encryption" id="SMTP_Encryption" class="select">
				<option value="" <?php echo set_select('SMTP_Encryption', '', (! isset($MailServerSettings->Encryption) || $MailServerSettings->Encryption == '' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0384', false, '', false); ?></option>
				<option value="ssl" <?php echo set_select('SMTP_Encryption', 'ssl', (isset($MailServerSettings->Encryption) && $MailServerSettings->Encryption == 'ssl' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0385', false, '', false); ?></option>
				<option value="tls" <?php echo set_select('SMTP_Encryption', 'tls', (isset($MailServerSettings->Encryption) && $MailServerSettings->Encryption == 'tls' ? true : false)); ?>><?php InterfaceLanguage('Screen', '0386', false, '', false); ?></option>
			</select>
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0383', false, '', false); ?></p></div>
			<?php print(form_error('SMTP_Encryption', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTP_Timeout') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Timeout">
			<label for="SMTP_Timeout"><?php InterfaceLanguage('Screen', '0387', false); ?>: *</label>
			<input type="text" name="SMTP_Timeout" value="<?php echo set_value('SMTP_Timeout', isset($MailServerSettings->Timeout) ? $MailServerSettings->Timeout : ''); ?>" id="SMTP_Timeout" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0388', false, '', false); ?></p></div>
			<?php print(form_error('SMTP_Timeout', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTP_Username') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Username">
			<label for="SMTP_Username"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SMTP_Username" value="<?php echo set_value('SMTP_Username', isset($MailServerSettings->Username) ? $MailServerSettings->Username : ''); ?>" id="SMTP_Username" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0392', false, '', false); ?></p></div>
			<?php print(form_error('SMTP_Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTP_Password') != '' ? 'error' : '')); ?>" id="form-row-SMTP_Password">
			<label for="SMTP_Password"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SMTP_Password" value="<?php echo set_value('SMTP_Password', isset($MailServerSettings->Password) ? $MailServerSettings->Password : ''); ?>" id="SMTP_Password" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0393', false, '', false); ?></p></div>
			<?php print(form_error('SMTP_Password', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>
	
	<div id="settings-for-savetodirectory" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0195']); ?></h3>
		<div class="form-row <?php print((form_error('SaveToDirectory_Directory') != '' ? 'error' : '')); ?>" id="form-row-SaveToDirectory_Directory">
			<label for="SaveToDirectory_Directory"><?php print($PluginLanguage['Screen']['0196']); ?>: *</label>
			<input type="text" name="SaveToDirectory_Directory" value="<?php echo set_value('SaveToDirectory_Directory', (isset($MailServerSettings->Directory) == true ? $MailServerSettings->Directory : '')); ?>" id="SaveToDirectory_Directory" class="text" placeholder="" />
			<div class="form-row-note"><p><?php print($PluginLanguage['Screen']['0197']); ?></p></div>
			<?php print(form_error('SaveToDirectory_Directory', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row no-bg">
			<p><?php print($PluginLanguage['Screen']['0198']); ?></p>
		</div>
	</div>

	<div id="settings-for-powermta" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0202']); ?></h3>
		<div class="form-row <?php print((form_error('PowerMTA_VMTA') != '' ? 'error' : '')); ?>" id="form-row-PowerMTA_VMTA">
			<label for="PowerMTA_VMTA"><?php InterfaceLanguage('Screen', '0320', false); ?>: *</label>
			<input type="text" name="PowerMTA_VMTA" value="<?php echo set_value('PowerMTA_VMTA', isset($MailServerSettings->VMTA) ? $MailServerSettings->VMTA : ''); ?>" id="PowerMTA_VMTA" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0369', false, '', false); ?></p></div>
			<?php print(form_error('PowerMTA_VMTA', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('PowerMTA_Directory') != '' ? 'error' : '')); ?>" id="form-row-PowerMTA_Directory">
			<label for="PowerMTA_Directory"><?php InterfaceLanguage('Screen', '0371', false); ?>: *</label>
			<input type="text" name="PowerMTA_Directory" value="<?php echo set_value('PowerMTA_Directory', isset($MailServerSettings->Directory) ? $MailServerSettings->Directory : ''); ?>" id="PowerMTA_Directory" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0372', false, '', false); ?></p></div>
			<?php print(form_error('PowerMTA_Directory', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>

	<div id="settings-for-smtpcom" style="display:none" class="octautomation-settings-container">
		<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0217']); ?></h3>
		<div class="form-row <?php print((form_error('SMTPCOM_Username') != '' ? 'error' : '')); ?>" id="form-row-SMTPCOM_Username">
			<label for="SMTPCOM_Username"><?php InterfaceLanguage('Screen', '0375', false); ?>: *</label>
			<input type="text" name="SMTPCOM_Username" value="<?php echo set_value('SMTPCOM_Username', isset($MailServerSettings->Username) ? $MailServerSettings->Username : ''); ?>" id="SMTPCOM_Username" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0377', false, '', false); ?></p></div>
			<?php print(form_error('SMTPCOM_Username', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
		<div class="form-row <?php print((form_error('SMTPCOM_Password') != '' ? 'error' : '')); ?>" id="form-row-SMTPCOM_Password">
			<label for="SMTPCOM_Password"><?php InterfaceLanguage('Screen', '0376', false); ?>: *</label>
			<input type="password" name="SMTPCOM_Password" value="<?php echo set_value('SMTPCOM_Password', isset($MailServerSettings->Password) ? $MailServerSettings->Password : ''); ?>" id="SMTPCOM_Password" class="text" />
			<div class="form-row-note"><p><?php InterfaceLanguage('Screen', '0378', false, '', false); ?></p></div>
			<?php print(form_error('SMTPCOM_Password', '<div class="form-row-note error"><p>', '</p></div>')); ?>
		</div>
	</div>



	<input type="hidden" name="Command" value="UpdateSettings" id="Command" />
</form>

<script type="text/javascript">
	function toggleSettings() {
		var value = $('#DeliveryMethod').val();
		$('.octautomation-settings-container').hide();
		$('#settings-for-' + value).show();
	}

	$(document).ready(function() {
		$('#DeliveryMethod').change(toggleSettings);
		toggleSettings();
	});
</script>
