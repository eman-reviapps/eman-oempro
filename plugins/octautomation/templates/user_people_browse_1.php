<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<link href="<?php InterfaceTemplateURL(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php InterfaceTemplateURL(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php InterfaceTemplateURL(); ?>assets/layouts/layout3/scripts/table-datatables-editable.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    var delete_url = "<?php InterfaceAppURL(); ?>/octautomation/people_delete/";
</script>

<?php
$FilterEditMode = false;
if (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true && is_array($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && count($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) > 0) {
    $FilterEditMode = true;
    $MatchType = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'];
    $RuleColumns = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns'];
    $RuleOperators = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'];
    $RuleValues = $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'];
}
//print_r('<pre>');
//print_r($MatchType);
//print_r($RuleColumns);
//print_r($RuleOperators);
//print_r($RuleValues);
//print_r('</pre>');
?>

<div class="people-list-filters" style="background-color:#07C;padding:10px;position:relative;">
    <div style="background:white;border:1px solid #7f96a3;padding:10px;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;width:100%">
        <div id="message-rules" style="border-bottom:none;">
            <p>
                <select name="SegmentOperator" id="SegmentOperator" style="width:auto;">
                    <option value="and" <?php print(set_select('SegmentOperator', 'and', ($FilterEditMode == true ? ($MatchType == 'and' ? true : false) : true))); ?>>Match all rules</option>
                    <option value="or" <?php print(set_select('SegmentOperator', 'or', ($FilterEditMode == true ? ($MatchType == 'or' ? true : false) : false))); ?>>Match any rules</option>
                </select>

                <button class="default add-rule-button" style="float:right;">+ Add new rule</button>
            </p>
            <div class="rule-rows" style="margin-top:8px;"></div>
            <p style="margin-top:7px;">
                <button class="default apply-filter-button"> Apply filters</button>
                <button class="default reset-filter-button"> Reset</button>
            </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php if (isset($PageErrorMessage) == true): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php print($PageErrorMessage); ?>
            </div>
        <?php elseif (isset($PageSuccessMessage) == true): ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php print($PageSuccessMessage); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">

                                </div>
                                <div class="actions">
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form id="people-list-form" action="<?php InterfaceAppURL(); ?>/octautomation/people/" method="post">

                                    <table id="sample_1" class="table table-striped table-hover table-bordered dataTable" >
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th></th>
                                                <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                                                    <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                                        <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                                                    <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                                        <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                                                    <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                                        <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                                                    <?php else: ?>
                                                        <th><?php print($AvailableColumns[$EachColumn]); ?></th>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <th> Delete </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (is_array($People) == true && count($People) > 0): ?>
                                                <?php foreach ($People as $Index => $EachPerson): ?>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left: 8px" class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                <input type="checkbox" class="checkboxes" value="<?php print($EachPerson->PersonID); ?>" />
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td style="padding-left:0;padding-right:0;white-space:nowrap; text-align: center;"><img src="http://www.gravatar.com/avatar/<?php print(md5(strtolower(trim($EachPerson->Email)))); ?>?s=16&d=identicon" width="16" height="16" alt="Photo"></td>
                                                        <?php foreach ($ChosenColumns as $Index => $EachColumn): ?>
                                                            <?php if ($PeopleDataStructure[$EachColumn] == 'date'): ?>
                                                                <td style="text-align:center; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                            <?php elseif ($PeopleDataStructure[$EachColumn] == 'number'): ?>
                                                                <td style="text-align:right; white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                            <?php elseif ($PeopleDataStructure[$EachColumn] == 'text'): ?>
                                                                <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                            <?php else: ?>
                                                                <td style="white-space:nowrap;"><a href="<?php print(InterfaceAppURL(true)); ?>/octautomation/person/<?php print($EachPerson->PersonID); ?>/"><?php print(FormatPeopleColumnValue($AvailableColumns[$EachColumn], $EachPerson->{$EachColumn}, $CountryList)); ?></a></td>
                                                            <?php endif; ?>

                                                        <?php endforeach; ?>
                                                        <td>
                                                            <a style="color:#337ab7" class="delete" href="javascript:;"> Delete </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="Command" id="Command" value="">
                                    <input type="hidden" name="SelectedPeopleIDs" id="SelectedPeopleIDs" value="">

                                    <?php print($OemproPagination->create_links()); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$FilterFields = array();

foreach ($AvailableColumns as $Key=>$Name)
{
	if ($PeopleDataStructure[$Key] == 'date')
	{
		$OperatorTypes = 'operatorTypes.date';
	}
	elseif ($PeopleDataStructure[$Key] == 'number')
	{
		$OperatorTypes = 'operatorTypes.number';
	}
	elseif ($PeopleDataStructure[$Key] == 'text')
	{
		$OperatorTypes = 'operatorTypes.string';
	}
	else
	{
		$OperatorTypes = 'operatorTypes.string';
	}
	$FilterFields[] = "'".$Key."': {label: '".$Name."', operators:".$OperatorTypes.", value:'', options:''}";
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.js-showhide-filters').on('click', function (ev) {
            ev.preventDefault();
            $('.people-list-filters').toggle();
        });
        $('#ListWhat').change(function () {
            var ListWhat = $(this).val();
            window.location.href = "<?php print(InterfaceAppURL(true)); ?>/octautomation/people/" + ListWhat;
        });
    });


    var operatorTypes = {
        empty: [],
        string: [['is', 'is'], ['is not', 'is not'], ['starts with', 'starts with'], ['ends with', 'ends with'], ['contains', 'contains']],
        date: [['more than', 'more than'], ['less than', 'less than'], ['exactly', 'exactly'], ['after', 'after'], ['on', 'on'], ['before', 'before']],
        number: [['is', 'is'], ['is not', 'is not'], ['greater than', 'greater than'], ['smaller than', 'smaller than']]
    };

    var availableColumnsAndOptions = {
        Empty: {label: 'Choose a property...', operators: operatorTypes.empty, value: '', options: ''},
<?php print(implode(',', $FilterFields)); ?>
    };


    // START - DON'T TOUCH THE RULES BELOW - THIS IS THE DEFAULT RULE THAT WILL BE DISPLAYED ON A NEW SEGMENT
    var segmentRules = [];

<?php
$SubmittedRuleColumns = $RuleColumns;
$SubmittedRuleOperators = $RuleOperators;
$SubmittedRuleValues = $RuleValues;

if (is_array($SubmittedRuleColumns) == true && count($SubmittedRuleColumns) > 0) {
    foreach ($SubmittedRuleColumns as $Index => $EachSubmittedRuleColumn):
        ?>
            segmentRules.push({column: '<?php print($EachSubmittedRuleColumn); ?>', operator: '<?php print($SubmittedRuleOperators[$Index]); ?>', value: '<?php print(str_replace(array("\\", "'"), array("", "\'"), $SubmittedRuleValues[$Index])); ?>'});
        <?php
    endforeach;
}
else {
    ?>
        segmentRules.push({column: 'Empty', operator: '', value: ''});
    <?php
}
?>
    console.log(segmentRules)
    var setupRuleBasedOnColumn = function (el, operator, value) {
        var $columnSelect = $(el),
                $row = $columnSelect.parent(),
                columnOptions = availableColumnsAndOptions[$columnSelect.val()],
                $columnOperatorSelect = $('<select name="RuleOperator[]" class="column-operator" style="margin-left:3px;width:120px;"></select>'),
                $columnValueInput;

        $row.find('.column-operator, .column-value').remove();

        var columnOperatorOptions = [];
        $.each(columnOptions.operators, function () {
            columnOperatorOptions.push('<option value="' + this[1] + '" ' + (operator == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
        });
        $columnOperatorSelect.html(columnOperatorOptions.join(''));
        $columnSelect.after($columnOperatorSelect);

        if (!columnOptions.options) {
            $columnValueInput = $('<input type="text" name="RuleValue[]" class="column-value" style="margin-left:3px;width:300px;">');
            if (value !== undefined)
                $columnValueInput.val(value);
        } else {
            $columnValueInput = $('<select name="RuleValue[]" class="column-value" style="margin-left:3px;width:170px;"></select>');
            var columnValueOptions = [];
            $.each(columnOptions.options, function () {
                columnValueOptions.push('<option value="' + this[1] + '" ' + (value == this[1] ? 'selected' : '') + '>' + this[0] + '</option>');
            });
            $columnValueInput.html(columnValueOptions.join(''));
        }
        if ($columnSelect.val() == 'Empty') {
            $columnOperatorSelect.hide();
            $columnValueInput.hide();
        } else {
            $columnOperatorSelect.show();
            $columnValueInput.show();
        }
        $columnOperatorSelect.after($columnValueInput);
        setupOperatorLabel(el);
    };
    var setupOperatorLabel = function (el) {
        var $ruleRow = $(el).parent();
        var $operatorSelect = $ruleRow.children('.column-operator');
        var $span = $('span.column-operator-label', $ruleRow);
        var operator = $operatorSelect.val();

        if ($span.length < 1) {
            $span = $('<span class="column-operator-label"></span>').css('color', '#616265').css('margin-left', '5px');
            $ruleRow.append($span);
        }
        if (operator == 'more than') {
            $span.text('days');
        } else if (operator == 'less than') {
            $span.text('days');
        } else if (operator == 'exactly') {
            $span.text('days');
        } else if (operator == 'after') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'before') {
            $span.text('YYYY-MM-DD');
        } else if (operator == 'on') {
            $span.text('YYYY-MM-DD');
        } else {
            $('.column-operator-label', $ruleRow).remove();
        }
    };
    var drawSelectedFilterBorder = function () {
        var filterBoxHeight = $('#filter-box').outerHeight();
        var selectedFilterListItem = $('.filter-list li.selected');
        if (selectedFilterListItem.length < 1)
            return;
        var liPos = selectedFilterListItem.position().top;
        $('.filter-list li.selected .border').height(filterBoxHeight);
        $('.filter-list li.selected .border').css('top', '-' + liPos + 'px');
    };
    var removeRule = function (ruleRow) {
        var $ruleRow = $(ruleRow);
        if ($('.rule-row').length == 1)
            return;
        $ruleRow.remove();
        drawSelectedFilterBorder();
        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    };
    var addRule = function () {
        var $lastRuleRow = $('.rule-row').last(),
                $cloneRuleRow = $lastRuleRow.clone();

        $cloneRuleRow.find('.column-operator, .column-value').remove();
        $lastRuleRow.after($cloneRuleRow);
        setupRuleBasedOnColumn($cloneRuleRow.find('.rule-column'));
        drawSelectedFilterBorder();
        if ($('.rule-row').length > 1) {
            $('.remove-rule-button').show();
        }
    };
    var populateRules = function (rules) {
        $.each(rules, function () {
            var $ruleRow = $('<div class="rule-row"></div>'),
                    $ruleColumnSelect = $('<select name="RuleColumn[]" class="rule-column" style="width:130px;"></select>'),
                    ruleObject = this;

            var ruleColumnSelectOptions = [];
            $.each(availableColumnsAndOptions, function (columnId, columnOptions) {
                ruleColumnSelectOptions.push('<option value="' + columnId + '" ' + (ruleObject.column == columnId ? 'selected' : '') + '>' + columnOptions.label + '</option>');
            });
            $ruleColumnSelect.html(ruleColumnSelectOptions.join(''));
            $ruleRow.append($ruleColumnSelect);

            $ruleRow.append($('<a href="#" class="remove-rule-button link">x</a>'));

            $('.rule-rows').append($ruleRow);
            setupRuleBasedOnColumn($ruleColumnSelect, ruleObject.operator, ruleObject.value);
        });
    };

    window.estimatedRecipientsRequest = null;

    $(document).ready(function () {
        $('.apply-filter-button').on('click', function (ev) {
            $('#Command').val('ApplyFilter');
        });

        $('.reset-filter-button').on('click', function (ev) {
            $('#Command').val('ResetFilter');
        });

        $('.rule-rows').on('change', '.rule-column', function (event) {
            setupRuleBasedOnColumn(this);
        });
        $('.rule-rows').on('change', '.column-operator', function (event) {
            setupOperatorLabel(this);
        });
        $('.rule-rows').on('change', '.column-value', function (event) {
        });
        $('.rule-rows').on('click', '.remove-rule-button', function (event) {
            event.preventDefault();
            removeRule($(this).parent());
            showNewFilterOption();
        });
        $('.add-rule-button').on('click', function (event) {
            event.preventDefault();
            addRule();
            showNewFilterOption();
        });
        $('.rule-column').each(function () {
            setupRuleBasedOnColumn(this);
        });
        populateRules(segmentRules);

        function showNewFilterOption() {
            $('.filter-list li.selected').removeClass('selected');
            $('#new-filter-box').show();
            $('#browsing-filter-name').html('&nbsp;');
        }

        $('#filter-box input, #filter-box select').on('change', function () {
            showNewFilterOption();
        });
        drawSelectedFilterBorder();

        if ($('.rule-row').length == 1) {
            $('.remove-rule-button').hide();
        } else {
            $('.remove-rule-button').show();
        }
    });
</script>
<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>