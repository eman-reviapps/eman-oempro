<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_header.php'); ?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php InterfaceTemplateURL(); ?>assets/apps/css/inbox.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<?php include('user_header.php'); ?>

<div class="page-content-inner">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet">
                <div class="portlet-body">
                    <div class="tabbable-line tab-custom">
                        <?php include('user_leftmenu.php'); ?>
                        <div class="clearfix"></div>
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php print(($PluginLanguage['Screen']['0004'])); ?>
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" > 
                                        <a id="" class="btn default btn-transparen btn-sm <?php echo $disabled_class?>" href="<?php InterfaceAppURL(); ?>/octautomation/create_message"><strong><?php print(($PluginLanguage['Screen']['0003'])); ?></strong></a>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (isset($PageErrorMessage) == true): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageErrorMessage); ?>
                                    </div>
                                <?php elseif (isset($PageSuccessMessage) == true): ?>
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <?php print($PageSuccessMessage); ?>
                                    </div>
                                <?php endif; ?>
                                <h4 class="form-section bold font-blue"><?php print($PluginLanguage['Screen']['0009']); ?></h4>

                                <div class="form-group no-bg">
                                    <span class="help-block"><?php print($PluginLanguage['Screen']['0010']); ?></span>
                                    <span class="help-block" style="margin-top:9px;"><?php print($PluginLanguage['Screen']['0011']); ?></span>
                                </div>

                                <h4 class="form-section bold font-blue"><?php print($PluginLanguage['Screen']['0013']); ?></h4>

                                <div class="form-group no-bg">
                                    <span class="help-block"><?php print($PluginLanguage['Screen']['0014']); ?></span>

                                    <textarea name="" id="" class="form-control" style="margin-top:9px;border:3px dotted #616265;padding:10px;height:230px;font-family: Courier, 'Courier New', monospace;font-size:12px;background-color:#DDE8EF;" onclick="this.select();" readonly="readonly">
<!-- Email Automation Tracker - Start -->
<script type="text/javascript" charset="utf-8" id="js-email-automation">
                                            window.email_automation_settings = {
                                                automation_user_id: "<?php print($EncryptedUserID); ?>",
                                                automation_url: "<?php print(InterfaceAppURL(true)); ?>/octautomation/track",
                                                user_id: "user id here",
                                                secured_user_id: "encrypted user id here",
                                                email: "test@test.com",
                                                extra_info: {
                                                }
                                            };
                                                                                                                                                                                                        </script>
<script type="text/javascript" charset="utf-8">(function () {
                                                var w = window;
                                                var d = document;
                                                function l() {
                                                    var s = d.createElement('script');
                                                    s.type = 'text/javascript';
                                                    s.async = true;
                                                    s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php print(str_replace(array('http://', 'https://'), array('', ''), InterfaceInstallationURL(true))); ?>plugins/octautomation/js/tracker.js';
                                                    var x = d.getElementsByTagName('script')[0];
                                                    x.parentNode.insertBefore(s, x);
                                                }
                                                if (w.attachEvent) {
                                                    w.attachEvent('onload', l);
                                                } else {
                                                    w.addEventListener('load', l, false);
                                                }
                                            })();</script>
<!-- Email Automation Tracker - End -->
                                    </textarea>
                                    <p style="background-color: #616265; text-align:center;padding:5px;letter-spacing:1.2px;color:#DDE8EF; font-weight: bold;"><?php print($PluginLanguage['Screen']['0012']); ?></p>
                                </div>

                                <div class="form-group no-bg js-data-waiting-container" style="background-color:#DDE8EF;border-radius:5px;text-align: center;border:1px solid #d0d8dd;">
                                    <p style="font-weight: bold;">
                                        <img src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/images/loader.gif" alt="" style="vertical-align: middle;" />
                                        <?php print($PluginLanguage['Screen']['0027']); ?>
                                    </p>
                                </div>
                                <div class="alert alert-success">
                                    <p style="font-weight: bold;">
                                        <?php print($PluginLanguage['Screen']['0028']); ?>
                                    </p>
                                </div>

                                <h4 class="form-section bold font-blue"><?php print($PluginLanguage['Screen']['0015']); ?></h4>

                                <div class="form-group no-bg">
                                    <style scoped="scoped">
                                        dl {
                                            display: block !important;
                                            border: 1px dotted #616265 !important;
                                            -webkit-margin-before: 1em !important;
                                            -webkit-margin-after: 1em !important;
                                            -webkit-margin-start: 0px !important;
                                            -webkit-margin-end: 0px !important;
                                        }

                                        dt {
                                            background-color: #F2F2F2 !important;
                                            font-weight: bold !important;
                                            padding: 5px 10px !important;
                                            display: block !important;
                                        }

                                        dd {
                                            padding: 5px 10px 5px 20px !important;
                                            margin: 0 !important;
                                            display: block !important;
                                        }

                                        dd code {
                                            display:inline-block;
                                            background-color: #FBF9C8 !important;
                                            color: #616265 !important;
                                            padding: 6px 12px !important;
                                            margin-top:6px;
                                            margin-bottom:6px;
                                            white-space: pre-wrap !important;
                                            font-family: monospace !important;
                                            font-size:12px;
                                        }
                                    </style>

                                    <p><?php print($PluginLanguage['Screen']['0016']); ?></p>

                                    <dl>
                                        <dt>user_id</dt>
                                        <dd><?php print($PluginLanguage['Screen']['0019']); ?></dd>
                                        <dt>secured_user_id</dt>
                                        <dd><?php print($PluginLanguage['Screen']['0020']); ?><br><code>&lt;?php $SecuredUserID = hash_hmac(&#x27;sha256&#x27;, &#x27;user_id_here&#x27;, &quot;<?php print($UserIDHash); ?>&quot;); ?&gt;</code></dd>
                                        <dt>email</dt>
                                        <dd><?php print($PluginLanguage['Screen']['0021']); ?></dd>
                                        <dt>extra_info</dt>
                                        <dd>
                                            <?php print($PluginLanguage['Screen']['0021']); ?><br>
                                            <code>extra_info:{
                                                'name':'Michael John',
                                                'age':24,
                                                'total_products':12,
                                                'birth_date':'1973-12-03 11:00:00'
                                                }</code>
                                            <br><?php print($PluginLanguage['Screen']['0021']); ?>
                                            <dl>
                                                <dt>string</dt>
                                                <dd><?php print($PluginLanguage['Screen']['0024']); ?><br><code>'name':'Michael John'</code></dd>
                                                <dt>number</dt>
                                                <dd><?php print($PluginLanguage['Screen']['0025']); ?><br><code>'age':24</code></dd>
                                                <dt>date</dt>
                                                <dd><?php print($PluginLanguage['Screen']['0026']); ?><br><code>'birth_date':'1973-12-03 11:00:00'</code></dd>
                                            </dl>
                                        </dd>
                                    </dl>
                                </div>

                                <h4 class="form-section bold font-blue"><?php print($PluginLanguage['Screen']['0017']); ?></h4>

                                <div class="form-group no-bg">
                                    <p><?php print($PluginLanguage['Screen']['0018']); ?></p>
                                </div>

                                <div class="form-group no-bg js-data-waiting-container" style="background-color:#DDE8EF;border-radius:5px;text-align: center;border:1px solid #d0d8dd;">
                                    <p style="font-weight: bold;">
                                        <img src="<?php InterfaceInstallationURL(); ?>plugins/octautomation/images/loader.gif" alt="" style="vertical-align: middle;" />
                                        <?php print($PluginLanguage['Screen']['0027']); ?>
                                    </p>
                                </div>
                                <div class="alert alert-success">
                                    <p style="font-weight: bold;">
                                        <?php print($PluginLanguage['Screen']['0028']); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php InterfaceInstallationURL(); ?>/plugins/octautomation/js/init.js" type="text/javascript" charset="utf-8"></script>

<script>
    function CheckForData() {
        $.getJSON('<?php print(Core::InterfaceAppURL() . '/octautomation/user_get_people_count/'); ?>', function (data) {
            if (data.PeopleCount == 0) {
                setTimeout(CheckForData, 2000);
            } else {
                $('.js-data-waiting-container').hide();
                $('.js-data-received-container').show();
            }
        });
    }

    $(document).ready(function () {
        $('.js-data-waiting-container').show();
        $('.js-data-received-container').hide();

        $('.js-data-received-continue').click(function (ev) {
            ev.preventDefault();
            window.location.href = '<?php print(Core::InterfaceAppURL() . '/octautomation/people/'); ?>';
        });

        CheckForData();
    })
</script>

<?php include_once(TEMPLATE_PATH . 'desktop/layouts/user_footer.php'); ?>
