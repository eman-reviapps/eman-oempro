<?php

function ChangeMetaCharset($HTMLContent)
{
	$IsHTMLBlockExists = false;
	$IsHeadBlockExists = false;
	$IsMetaCharsetExists = false;

	// Check if <html> block exists
	if (preg_match('/(<html.*>)/i', $HTMLContent) > 0)
	{
		$IsHTMLBlockExists = true;
	}

	// Check if <head> block exists
	if (preg_match('/(<head.*>)/i', $HTMLContent) > 0)
	{
		$IsHeadBlockExists = true;
	}

	// Check if meta charset directiv exists
	if (preg_match("/<meta.*charset.*\/>/i", $HTMLContent) > 0)
	{
		$IsMetaCharsetExists = true;
	}

	if ($IsHTMLBlockExists == false)
	{
		$HTMLContent = '<html>'."\n".$HTMLContent."\n\n</html>";
	}
	if ($IsHeadBlockExists == false)
	{
		$HTMLContent = preg_replace('/<html>/i', "<html>\n<head></head>\n", $HTMLContent);
	}

	// For some unkown shitty reason, if the title tag comes before the meta content-type tag, Turkish
	// characters are shown corrupted. Therefore, we first remove the meta tag from head block and
	// add our own utf8 meta tag as the first tag inside head block. Do not change this, understood?

	if ($IsMetaCharsetExists == true)
	{
		$HTMLContent = preg_replace("/<meta.*charset.*\/>/i", '', $HTMLContent);
	}

	$MetaCharset = '<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />';
	$HTMLContent = preg_replace('/<head>/i', "<head>\n".$MetaCharset."\n", $HTMLContent);

	return $HTMLContent;
}

function RemoveIguanaHTMLCodes($HTMLContent)
{
	$HTMLContent = preg_replace('/<div.*data-sl-cleanup="true".*>.*<\/div>/uiUm', '', $HTMLContent);
	return $HTMLContent;
}