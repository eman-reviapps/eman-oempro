<?php

include_once(PLUGIN_PATH . 'octautomation/functions.php');
include_once(PLUGIN_PATH . "/octautomation/swiftmailer_430/swift_required.php");

class octautomation extends Plugins {

    private static $ArrayLanguage = array();
    public static $ObjectCI = null;
    private static $UserInformation = array();
    private static $AdminInformation = array();
    private static $Models = null;
    private static $SecurityHash = '23023odskjd!03292';
    private static $SessionThreshold = 1800;
    private static $PluginLicenseKey = null;
    private static $PluginLicenseStatus = true;
    private static $PluginVersion = "1.1.0";
    private static $PluginLicenseInfo = null;

//    private $email_html;

    /* Default Plugin Methods */

    public function __construct() {
        
    }

    public function enable_octautomation() {
        self::_LoadModels();

        // License check
        self::_CheckLicenseStatus(true);
        if (self::$PluginLicenseStatus == false) {
            if (defined('octautomation_LicenseStatusMessage') == true && octautomation_LicenseStatusMessage == 'not_bound_to_this_oempro') {
                $_SESSION['PluginMessage'] = array(false, 'The plugin license does not belong to this Oempro license.<br>Please install the correct email automation plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="' . InterfaceAppUrl(true) . '/octautomation/trial_request">request a trial license</a>.');
                header("location: " . InterfaceAppURL(true) . '/admin/plugins/disable/octautomation');
                exit;
            } else {
                $_SESSION['PluginMessage'] = array(false, 'Please install the email automation plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="' . InterfaceAppUrl(true) . '/octautomation/trial_request">request a trial license</a>.');
                header("location: " . InterfaceAppURL(true) . '/admin/plugins/disable/octautomation');
                exit;
            }
        }

        self::setup_plugin_databaser_schema();
    }

    public function disable_octautomation() {
        Database::$Interface->DeleteRows_Enhanced(array(
            'Table' => MYSQL_TABLE_PREFIX . 'options',
            'Criteria' => array(
                array('Column' => 'OptionName', 'Operator' => 'LIKE', 'Value' => 'OctAutomation%')
            )
        ));
    }

    public function load_octautomation() {
        // License check
        self::_CheckLicenseStatus();

        if (self::$PluginLicenseStatus == false) {
            return false;
        }

        parent::RegisterEnableHook('octautomation');
        parent::RegisterDisableHook('octautomation');

        parent::RegisterMenuHook('octautomation', 'set_menu_items');

        parent::RegisterHook('Action', 'FormItem.AddTo.Admin.UserGroupLimitsForm', 'octautomation', 'set_user_group_form_items', 10, 1);
        parent::RegisterHook('Filter', 'UserGroup.Update.FieldValidator', 'octautomation', 'usergroup_update_fieldvalidator', 10, 1);
        parent::RegisterHook('Action', 'UserGroup.Update.Post', 'octautomation', 'usergroup_update', 10, 2);
        parent::RegisterHook('Action', 'UserGroup.Create.Post', 'octautomation', 'usergroup_update', 10, 2);
        parent::RegisterHook('Action', 'UserGroup.Delete.Post', 'octautomation', 'usergroup_delete', 10, 1);
        parent::RegisterHook('Action', 'User.Delete.Post', 'octautomation', 'user_delete', 10, 1000);

        // Retrieve language setting - Start {
        $Language = Database::$Interface->GetOption('OctAutomation_Language');
        if (count($Language) == 0) {
            Database::$Interface->SaveOption('OctAutomation_Language', 'en');
            $Language = 'en';
        } else {
            $Language = $Language[0]['OptionValue'];
        }
        // Retrieve language setting - End }
        // Language file - Start {
        $ArrayPlugInLanguageStrings = array();
        if (file_exists(PLUGIN_PATH . 'octautomation/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php') == true) {
            include_once(PLUGIN_PATH . 'octautomation/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php');
        } else {
            include_once(PLUGIN_PATH . 'octautomation/languages/en/en.inc.php');
        }
        self::$ArrayLanguage = $ArrayPlugInLanguageStrings;

        unset($ArrayPlugInLanguageStrings);
        // Language file - End }

        self::_LoadModels();

        include_once('encrypt.php');
        include_once('security.php');
        include_once('mailgun.php');
        include_once('savetodirectory.php');
        include_once('smtp.php');
        include_once('mandrill.php');

        // Define constants
        define('OCTAUTOMATION_SESSION_THRESHOLD', self::$SessionThreshold);
    }

    /* Hooks */

    public function set_menu_items() {
        $ArrayMenuItems = array();
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'Admin.Settings',
            'MenuID' => 'Email Automation',
            'MenuLink' => Core::InterfaceAppURL() . '/octautomation/admin_settings/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
        );
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'Admin.TopDropMenu',
            'MenuID' => 'Email Automation',
            'MenuLink' => Core::InterfaceAppURL() . '/octautomation/admin_reports/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
        );
        $ArrayMenuItems[] = array(
            'MenuLocation' => 'User.TopMenu',
            'MenuID' => 'Email Automation',
            'MenuLink' => Core::InterfaceAppURL() . '/octautomation/people/',
            'MenuTitle' => self::$ArrayLanguage['Screen']['0001'],
        );

        return array($ArrayMenuItems);
    }

    public function set_user_group_form_items($UserGroup) {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        $UserGroupSettings = Database::$Interface->GetOption('OctAutomation_UserGroupSettings');
        $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

        $ArrayViewData = array(
            'PluginLanguage' => self::$ArrayLanguage,
            'UserGroup' => $UserGroup,
            'UserGroupSettings' => (isset($UserGroupSettings[$UserGroup['UserGroupID']]) == true ? $UserGroupSettings[$UserGroup['UserGroupID']] : false)
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $HTML = self::$ObjectCI->plugin_render('octautomation', 'inline_usergroup_formitems', $ArrayViewData, true, true);

        return array($HTML);
    }

    public function usergroup_update_fieldvalidator($ArrayFormRules) {
        $ArrayFormRules[] = array(
            'field' => 'OctAutomation[DeliveryLimit]',
            'label' => self::$ArrayLanguage['Screen']['0155'],
            'rules' => 'required|numeric',
        );
        return array($ArrayFormRules);
    }

    public function usergroup_update($UserGroup, $PostData) {
        if (isset($PostData['OctAutomation']['DeliveryLimit']) == true) {
            $UserGroupSettings = Database::$Interface->GetOption('OctAutomation_UserGroupSettings');
            $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

            $UserGroupSettings[$UserGroup['UserGroupID']]['DeliveryLimit'] = $PostData['OctAutomation']['DeliveryLimit'];

            Database::$Interface->SaveOption('OctAutomation_UserGroupSettings', json_encode($UserGroupSettings));
        }
    }

    public function usergroup_delete($UserGroup) {
        $UserGroupSettings = Database::$Interface->GetOption('OctAutomation_UserGroupSettings');
        $UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);

        if (isset($UserGroupSettings[$UserGroup['UserGroupID']]) == true) {
            unset($UserGroupSettings[$UserGroup['UserGroupID']]);
        }

        Database::$Interface->SaveOption('OctAutomation_UserGroupSettings', json_encode($UserGroupSettings));
    }

    public function user_delete() {
        $SelectedUserIDs = func_get_args();

        if (is_array($SelectedUserIDs) == true && count($SelectedUserIDs) > 0) {
            foreach ($SelectedUserIDs as $EachUserID) {
                self::$Models->messages->DeleteUserRecords($EachUserID);
            }
        }
    }

    private function setup_plugin_databaser_schema() {
        self::$Models->messages->SetupDatabaseSchema();
    }

    /* User Interface Methods */

    public function ui_tl($UserID, $MessageID, $PersonID, $LinkTitle, $LinkURL) {
        self::_PluginAppHeader(null);

        $Decoded = Core::DecryptQueryStringAsArrayAdvanced($UserID . '/' . $MessageID . '/' . $PersonID);
        list($UserID, $MessageID, $PersonID) = $Decoded;

        $LinkTitle = urldecode(base64_decode($LinkTitle));
        $LinkURL = urldecode(base64_decode($LinkURL));

        if ($LinkURL == '')
            $LinkURL = $LinkTitle;
        if ($LinkURL == '')
            return;

        $UserInformation = Users::RetrieveUser(array('*'), array('UserID' => $UserID), false);
        if (is_bool($UserInformation) == true && $UserInformation == false) {
            return false;
        }
        $MessageStatus = self::$Models->people->GetMessageSentLogForPerson($UserInformation, $MessageID, $PersonID);

        if (is_bool($MessageStatus) == true || $MessageStatus->IsClicked == 'Yes') {
            header('Location: ' . $LinkURL);
            exit;
        }

        self::$Models->people->UpdatePersonDeliveryLog($UserInformation, $MessageID, $PersonID, array(
            'IsClicked' => 'Yes',
            'Click_Date' => date('Y-m-d H:i:s'),
        ));

        self::$Models->people->RegisterActivityToPerson($UserInformation, $PersonID, 'Clicked link', $MessageID);
        self::$Models->messages->UpdateMessage($UserInformation, $MessageID, array(
            'TotalClicked' => array('TotalClicked + 1'),
        ));

        self::$Models->messages->RegisterMessageLog($UserInformation, $MessageID, array(
            'TotalClicked' => array('TotalClicked + 1')
        ));

        header('Location: ' . $LinkURL);
        exit;
    }

//    public function ui_eman_send() {
//
//        self::_PluginAppHeader(null);
//        $UserID = 1;
//        $MessageID = 2;
//        $PersonID = 82;
//
//        $message = "";
//        $file = PLUGIN_PATH . '/octautomation/email.html';
//        
//        if (file_exists($file) && strstr($file, '.html')) {
//            $message = file_get_contents($file);
//            $message = str_replace('<span id="loginUrl"></span>', '<span id="loginUrl"><a href=""> Login </a></span>', $message);
//            $message = str_replace('<span id="userName"></span>', '<span id="userName"></span>', $message);
//        }
//
//        $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array($UserID, $MessageID, $PersonID));
//        $OpenTrackURL = '<img src="' . Core::InterfaceAppURL() . '/octautomation/eman_to/' . $EncryptedQuery . '" width="5" height="2" alt=".">';
//        $message = preg_replace('/<\/body>/i', "\n" . $OpenTrackURL . "\n\n</body>", $message);
//
//        $to = htmlspecialchars('eman.cse2008@gmail.com');
//        $subject = "Welcome";
//
//        // Always set content-type when sending HTML email
//        $headers = "MIME-Version: 1.0" . "\r\n";
//        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//        $headers .= 'From: <support@flyinglist.com>' . "\r\n";
//
//        $mail = mail($to, $subject, $message, $headers);
//    }
//
//    public function ui_eman_to($UserID, $MessageID, $PersonID) {
//
//        $ip = $_SERVER['REMOTE_ADDR'];
//        $myfile = fopen(PLUGIN_PATH . "/octautomation/newfile.txt", "w");
//        $txt = $ip;
//        fwrite($myfile, $txt);
//        fclose($myfile);
//    }

    public function ui_to($UserID, $MessageID, $PersonID) {
        self::_PluginAppHeader(null);

        $Decoded = Core::DecryptQueryStringAsArrayAdvanced($UserID . '/' . $MessageID . '/' . $PersonID);
        list($UserID, $MessageID, $PersonID) = $Decoded;

        $UserInformation = Users::RetrieveUser(array('*'), array('UserID' => $UserID), false);
        if (is_bool($UserInformation) == true && $UserInformation == false) {
            return false;
        }
        $MessageStatus = self::$Models->people->GetMessageSentLogForPerson($UserInformation, $MessageID, $PersonID);

        if (is_bool($MessageStatus) == true || $MessageStatus->IsOpened == 'Yes') {
            Core::DisplayTransImage(true);
        }

        // Geo-location detection
//        if (defined(GEO_LOCATION_DATA_PATH) && isset($_SERVER['REMOTE_ADDR']) == true && file_exists(GEO_LOCATION_DATA_PATH) == true) {
        if (isset($_SERVER['REMOTE_ADDR']) == true && file_exists(GEO_LOCATION_DATA_PATH) == true) {
            $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
            $GeoTagInfo = geoip_record_by_addr($GeoTag, $_SERVER['REMOTE_ADDR']);
        }

        self::$Models->people->UpdatePersonDeliveryLog($UserInformation, $MessageID, $PersonID, array(
            'IsOpened' => 'Yes',
            'Open_City' => (isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : ''),
            'Open_Country' => (isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : ''),
            'Open_Date' => date('Y-m-d H:i:s'),
        ));

        self::$Models->people->RegisterActivityToPerson($UserInformation, $PersonID, 'Opened message', $MessageID);
        self::$Models->messages->UpdateMessage($UserInformation, $MessageID, array(
            'TotalOpened' => array('TotalOpened + 1'),
        ));

        self::$Models->messages->RegisterMessageLog($UserInformation, $MessageID, array(
            'TotalOpened' => array('TotalOpened + 1')
        ));

        Core::DisplayTransImage(true);
    }

    public function ui_mailgun() {
        self::_PluginAppHeader(null);

        $AvailableWebhooks = array('opened', 'clicked', 'unsubscribed', 'complained', 'bounced');
        if (in_array(self::$ObjectCI->input->post('event'), $AvailableWebhooks) == false)
            return;

        if (self::$ObjectCI->input->post('Automation-Email-ID') == false)
            return;
        if (self::$ObjectCI->input->post('event') == false)
            return;
        if (self::$ObjectCI->input->post('timestamp') == false)
            return;
        if (self::$ObjectCI->input->post('token') == false)
            return;
        if (self::$ObjectCI->input->post('signature') == false)
            return;

        $MailServerSettings = Database::$Interface->GetOption('OctAutomation_MailServer');
        $MailServerSettings = json_decode($MailServerSettings[0]['OptionValue']);
        if (isset($MailServerSettings->APIKey) == false || isset($MailServerSettings->Domain) == false) {
            return false;
        }

        if (self::$ObjectCI->input->post('signature') != hash_hmac('sha256', self::$ObjectCI->input->post('timestamp') . self::$ObjectCI->input->post('token'), $MailServerSettings->APIKey)) {
            return false;
        }

        list($EncryptedUserID, $EncryptedMessageID, $EncryptedPersonID, $Checksum) = explode('/', self::$ObjectCI->input->post('Automation-Email-ID'));
        list($UserID, $MessageID, $PersonID) = sl_decrypt_encrypted_parameters(array($EncryptedUserID, $EncryptedMessageID, $EncryptedPersonID), $Checksum, 5);

        self::$UserInformation = Users::RetrieveUser(array('*'), array('UserID' => $UserID), false);
        if (is_bool(self::$UserInformation) == true && self::$UserInformation == false) {
            return false;
        }

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);
        if (is_bool($Message) == true) {
            return false;
        }

        $Person = self::$Models->people->GetPerson(self::$UserInformation, $PersonID, true);
        if (is_bool($Person) == true) {
            return false;
        }

        if (self::$ObjectCI->input->post('event') == 'opened') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsOpened == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsOpened' => 'Yes',
                'Open_City' => self::$ObjectCI->input->post('city'),
                'Open_Country' => self::$ObjectCI->input->post('country'),
                'Open_Date' => date('Y-m-d H:i:s'),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Opened message', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalOpened' => array('TotalOpened + 1'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalOpened' => array('TotalOpened + 1')
            ));
        }
        elseif ($this->input->post('event') == 'clicked') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsClicked == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsClicked' => 'Yes',
                'Click_Date' => date('Y-m-d H:i:s'),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Clicked link', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalClicked' => array('TotalClicked + 1'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalClicked' => array('TotalClicked + 1')
            ));
        }
        elseif ($this->input->post('event') == 'unsubscribed') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsUnsubscribed == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'Unsubscription_Date' => date('Y-m-d H:i:s'),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Unsubscribed', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalUnsubscribed' => array('TotalUnsubscribed + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s'),
                'UnsubscriptionIP' => self::$ObjectCI->input->post('ip'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalUnsubscribed' => array('TotalUnsubscribed + 1')
            ));
        }
        elseif ($this->input->post('event') == 'complained') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsSpamComplaint == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsSpamComplaint' => 'Yes',
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'SPAM Complained', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalSpamComplaint' => array('TotalSpamComplaint + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s'),
                'UnsubscriptionIP' => self::$ObjectCI->input->post('ip'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalSpamComplaint' => array('TotalSpamComplaint + 1')
            ));
        }
        elseif ($this->input->post('event') == 'bounced') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsHardBounced == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsHardBounced' => 'Yes',
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Hard bounced', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalHardBounced' => array('TotalHardBounced + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'IsBounced' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s'),
                'UnsubscriptionIP' => self::$ObjectCI->input->post('ip'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalHardBounced' => array('TotalHardBounced + 1')
            ));
        }
    }

    public function ui_mandrill() {
        $PostDummy = urldecode('%5B%7B%22event%22%3A%22open%22%2C%22ts%22%3A1394089338%2C%22user_agent%22%3A%22Mozilla%5C%2F5.0+%28Windows%3B+U%3B+Windows+NT+5.1%3B+de%3B+rv%3A1.9.0.7%29+Gecko%5C%2F2009021910+Firefox%5C%2F3.0.7+%28via+ggpht.com+GoogleImageProxy%29%22%2C%22user_agent_parsed%22%3A%7B%22type%22%3A%22Browser%22%2C%22ua_family%22%3A%22Firefox%22%2C%22ua_name%22%3A%22Firefox+3.0.7%22%2C%22ua_version%22%3A%223.0.7%22%2C%22ua_url%22%3A%22http%3A%5C%2F%5C%2Fwww.firefox.com%5C%2F%22%2C%22ua_company%22%3A%22Mozilla+Foundation%22%2C%22ua_company_url%22%3A%22http%3A%5C%2F%5C%2Fwww.mozilla.org%5C%2F%22%2C%22ua_icon%22%3A%22http%3A%5C%2F%5C%2Fcdn.mandrill.com%5C%2Fimg%5C%2Femail-client-icons%5C%2Ffirefox.png%22%2C%22os_family%22%3A%22Windows%22%2C%22os_name%22%3A%22Windows+XP%22%2C%22os_url%22%3A%22http%3A%5C%2F%5C%2Fen.wikipedia.org%5C%2Fwiki%5C%2FWindows_XP%22%2C%22os_company%22%3A%22Microsoft+Corporation.%22%2C%22os_company_url%22%3A%22http%3A%5C%2F%5C%2Fwww.microsoft.com%5C%2F%22%2C%22os_icon%22%3A%22http%3A%5C%2F%5C%2Fcdn.mandrill.com%5C%2Fimg%5C%2Femail-client-icons%5C%2Fwindowsxp.png%22%2C%22mobile%22%3Afalse%7D%2C%22ip%22%3A%2266.249.81.148%22%2C%22location%22%3A%7B%22country_short%22%3A%22US%22%2C%22country%22%3A%22United+States%22%2C%22region%22%3A%22California%22%2C%22city%22%3A%22Mountain+View%22%2C%22latitude%22%3A37.386051178%2C%22longitude%22%3A-122.083847046%2C%22postal_code%22%3A%2294043%22%2C%22timezone%22%3A%22-07%3A00%22%7D%2C%22_id%22%3A%22af7be1b353174fe6851a386df45b087e%22%2C%22msg%22%3A%7B%22ts%22%3A1394089298%2C%22_id%22%3A%22af7be1b353174fe6851a386df45b087e%22%2C%22state%22%3A%22sent%22%2C%22subject%22%3A%22This+is+a+testing+message%22%2C%22email%22%3A%22mert.hurturk%40gmail.com%22%2C%22tags%22%3A%5B%5D%2C%22opens%22%3A%5B%7B%22ts%22%3A1394089338%2C%22ip%22%3A%2266.249.81.148%22%2C%22location%22%3A%22California%2C+US%22%2C%22ua%22%3A%22Windows%5C%2FWindows+XP%5C%2FFirefox%5C%2FFirefox+3.0.7%22%7D%5D%2C%22clicks%22%3A%5B%5D%2C%22smtp_events%22%3A%5B%7B%22ts%22%3A1394089298%2C%22type%22%3A%22sent%22%2C%22diag%22%3A%22250+2.0.0+OK+1394089298+l12si8469722yhq.121+-+gsmtp%22%2C%22source_ip%22%3A%22205.201.131.134%22%2C%22destination_ip%22%3A%2274.125.137.26%22%2C%22size%22%3A3244%7D%5D%2C%22resends%22%3A%5B%5D%2C%22_version%22%3A%229g6VfKQvFBi5eho4I-iwyg%22%2C%22metadata%22%3A%7B%22AutomationEmailID%22%3A%22C11-1C%5C%2FC11-1C%5C%2FB11-1C%5C%2F58021848%22%7D%2C%22sender%22%3A%22mert.hurturk%40gmail.com%22%2C%22template%22%3Anull%7D%7D%5D');
        $_POST['mandrill_events'] = $PostDummy;
        self::_PluginAppHeader(null);

        if (($MandrillData = self::$ObjectCI->input->post('mandrill_events')) == false)
            return;

        $MandrillData = json_decode($MandrillData);

        if (!$MandrillData)
            return;
        if (!is_array($MandrillData))
            return;

        foreach ($MandrillData as $EachMandrillData) {
            self::_ProcessMandrillData($EachMandrillData);
        }
    }

    public function ui_trial_request() {
        if (file_exists(APP_PATH . '/data/license_octautomation.dat') == true) {
            $_SESSION['PluginMessage'] = array(false, 'It seems that a license for this plugin has been already generated.');
            header("location: " . InterfaceAppURL(true) . '/admin/plugins');
            exit;
        }

        include_once(PLUGIN_PATH . '/octautomation/functions.php');

        $LicenseEngine = new octautomation_license_engine();
        $LicenseExpireDate = $LicenseEngine->request_trial_license();

        if (is_array($LicenseExpireDate) == true && is_bool($LicenseExpireDate[0]) == true && $LicenseExpireDate[0] == false) {
            $_SESSION['PluginMessage'] = array(false, $LicenseExpireDate[1]);
            header("location: " . InterfaceAppURL(true) . '/admin/plugins');
            exit;
        }

        $_SESSION['PluginMessage'] = array(true, 'Trial period has just started. It will expire on ' . date('Y-m-d', strtotime($LicenseExpireDate)));
        header("location: " . InterfaceAppURL(true) . '/admin/plugins/enable/octautomation');
        exit;
    }

    public function ui_track() {
        self::_PluginAppHeader(null);

        // The below line is a quick hack for codeigniter url query string problem with jsonp callbacks
        parse_str($_SERVER['QUERY_STRING'], $_GET);

        // Verify the Oempro user account
        list($EncryptedUserID, $Checksum) = explode("/", $_GET['automation_user_id']);
        list($Oempro_UserID) = sl_decrypt_encrypted_parameters(array($EncryptedUserID), $Checksum, 5);
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "111" . PHP_EOL, FILE_APPEND | LOCK_EX);
        self::$UserInformation = Users::RetrieveUser(array('*'), array('UserID' => $Oempro_UserID), false);
        if (is_bool(self::$UserInformation) == true && self::$UserInformation == false) {
            // Oempro user not found. Stop tracking.
            self::_ReturnJSONPResponse($_GET['callback'], array('status' => false, 'message' => 'user not found'));
            return;
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "222" . PHP_EOL, FILE_APPEND | LOCK_EX);

        if (Users::IsAccountExpired(self::$UserInformation) == true) {
            // Oempro user account is expired. Stop tracking.
            self::_ReturnJSONPResponse($_GET['callback'], array('status' => false, 'message' => 'user not active'));
            return;
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "3333" . PHP_EOL, FILE_APPEND | LOCK_EX);
        if (AccountIsUnTrustedCheck(self::$UserInformation)) {
            self::_ReturnJSONPResponse($_GET['callback'], array('status' => false, 'message' => 'user not trusted'));
            return;
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "444" . PHP_EOL, FILE_APPEND | LOCK_EX);
        // Verify the person user id and secured user id
        if (self::$ObjectCI->input->get('user_id') === false) {
            self::_ReturnJSONPResponse($_GET['callback'], array('status' => false, 'message' => 'missing user_id'));
            return;
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "555" . PHP_EOL, FILE_APPEND | LOCK_EX);
        $UserIDHash = md5(self::$SecurityHash . self::$SecurityHash . ' ' . self::$UserInformation['UserID']);
        $SecuredUserID = hash_hmac('sha256', self::$ObjectCI->input->get('user_id'), $UserIDHash);

        if ($SecuredUserID != self::$ObjectCI->input->get('secured_user_id')) {
            self::_ReturnJSONPResponse($_GET['callback'], array('status' => false, 'message' => 'user_id does not match secured_user_id'));
            return;
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "666" . PHP_EOL, FILE_APPEND | LOCK_EX);
        // Create the person data
        $PersonData = new stdClass();
        $PersonData->id = self::$ObjectCI->input->get('user_id');
        $PersonData->hash = self::$ObjectCI->input->get('secured_user_id');
        $PersonData->email = self::$ObjectCI->input->get('email');

        foreach ($_GET as $Key => $Value) {
            if ($Key == 'automation_url' || $Key == '/octautomation/track' || $Key == '_' || $Key == 'callback' || $Key == 'automation_user_id' || $Key == 'user_id' || $Key == 'secured_user_id' || $Key == 'email')
                continue;

            $Key = preg_replace("/[^A-Za-z0-9_]/", '_', $Key);

            $PersonData->{$Key} = (is_numeric($Value) == true ? (int) $Value : (string) $Value);
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "777" . PHP_EOL, FILE_APPEND | LOCK_EX);
        $Result = false;
        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced(self::$UserInformation['UserID']);

        $limit = self::$UserInformation['GroupInformation']['LimitSubscribers'] ? self::$UserInformation['GroupInformation']['LimitSubscribers'] : -1;
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "111 Limit $limit" . PHP_EOL, FILE_APPEND | LOCK_EX);
        if (
                ($TotalSubscribersOnTheAccount < $limit && $limit > 0) ||
                ($limit == -1)
        ) {
            $Result = self::$Models->people->RegisterPerson(self::$UserInformation, $PersonData, true, self::$SecurityHash);
            file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "666 yes here add $TotalSubscribersOnTheAccount" . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", print_r($b, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

        //add subscriber to automation list - Start
        if ($Result == true) {
            self::_addSubscirberToAutomationList(self::$UserInformation, $PersonData);
        }
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", "Result $Result" . PHP_EOL, FILE_APPEND | LOCK_EX);

        //add subscriber to automation list - End
        // Return response

        self::_ReturnJSONPResponse($_GET['callback'], array('status' => true));
        return;
    }

    private function _addSubscirberToAutomationList($User, $PersonData = array()) {

        if (isset($_SERVER['REMOTE_ADDR']) == true && file_exists(GEO_LOCATION_DATA_PATH) == true) {
            $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
            $GeoTagInfo = geoip_record_by_addr($GeoTag, $_SERVER['REMOTE_ADDR']);
        }
        Core::LoadObject('lists');
        Core::LoadObject('subscribers');

        $array_list_information = Lists::RetrieveListCustom(array('*'), array('IsAutomationList' => 'Yes', 'RelOwnerUserID' => $User['UserID']), false, false);
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", print_r($array_list_information, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", print_r($User, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", print_r($PersonData, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

        $SubscriberParameters = array(
            'CheckSubscriberLimit' => false,
            'UserInformation' => $User,
            'ListInformation' => $array_list_information,
            'EmailAddress' => (is_object($PersonData) == true ? $PersonData->email : $PersonData['email']),
            'IPAddress' => $_SERVER['REMOTE_ADDR'],
            'SubscriptionStatus' => 'Subscribed',
            'OtherFields' => array(),
            'SendConfirmationEmail' => false,
            'UpdateIfDuplicate' => true,
            'UpdateIfUnsubscribed' => true,
            'ApplyBehaviors' => false,
            'UpdateStatistics' => true,
            'TriggerWebServices' => false,
            'TriggerAutoResponders' => false
        );

        $ret = Subscribers::AddSubscriber_Enhanced($SubscriberParameters);
        file_put_contents(PLUGIN_PATH . "/octautomation/add.txt", print_r($ret, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

        $ArrayFieldAndValues = array(
            'Subscriber_IP' => (isset($_SERVER['REMOTE_ADDR']) == true ? $_SERVER['REMOTE_ADDR'] : ''),
            'City' => (isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : ''),
            'Country' => (isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : ''),
        );
        Subscribers::Update($array_list_information['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $ret[1]));
    }

    public function ui_admin_settings($SelectedTab = 'settings') {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        Core::LoadObject('api');

        $PageErrorMessage = '';
        $PageSuccessMessage = '';

        $MailServerSettings = Database::$Interface->GetOption('OctAutomation_MailServer');
        $MailServerSettings = json_decode($MailServerSettings[0]['OptionValue']);

        $APIResponse = API::call(array(
                    'format' => 'object',
                    'command' => 'users.get',
                    'protected' => true,
                    'username' => self::$AdminInformation['Username'],
                    'password' => self::$AdminInformation['Password'],
                    'parameters' => array(
                        'OrderField' => 'FirstName|LastName',
                        'OrderType' => 'ASC|ASC',
                        'RelUserGroupID' => 'Enabled',
                        'RecordsPerRequest' => 10000000,
                        'RecordsFrom' => 0,
                    )
        ));

        if (isset($APIResponse->Success) == true && $APIResponse->Success == true && isset($APIResponse->Users) == true && is_object($APIResponse->Users) == true && count($APIResponse->Users) > 0) {
            $Users = array();

            foreach ($APIResponse->Users as $Index => $EachUser) {
                $Users[] = $EachUser;
            }
        } else {
            $Users = false;
        }

        if ($SelectedTab == 'settings') {
            $EventResult = self::_EventSaveAdminSettings();
            if (is_array($EventResult) == true && is_bool($EventResult[0]) == true && $EventResult[0] == false) {
                $PageErrorMessage = $EventResult[1];
            } elseif (is_array($EventResult) == true && is_bool($EventResult[0]) == true && $EventResult[0] == true) {
                $PageSuccessMessage = $EventResult[1];
            }
            $MailServerSettings = Database::$Interface->GetOption('OctAutomation_MailServer');
            $MailServerSettings = json_decode($MailServerSettings[0]['OptionValue']);
        }

        $Mailgun_WebHookURL = InterfaceAppUrl(true) . '/octautomation/mailgun/';
        $Mandrill_WebHookURL = InterfaceAppUrl(true) . '/octautomation/mandrill/';

        $DeliveryMethodToDisplay = $MailServerSettings->Engine;
        if ($DeliveryMethodToDisplay == 'smtp' && $MailServerSettings->Hostname == 'octeth.smtp.com') {
            $DeliveryMethodToDisplay = 'smtpcom';
        }
        if ($SelectedTab == 'settings' && self::$ObjectCI->input->post('Command') !== false) {
            $DeliveryMethodToDisplay = self::$ObjectCI->input->post('DeliveryMethod');
        }

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0002'],
            'CurrentMenuItem' => 'Settings',
            'PluginView' => '../plugins/octautomation/templates/admin_settings.php',
            'SubSection' => 'Email Automation',
            'PluginLanguage' => self::$ArrayLanguage,
            'PluginLicenseKey' => self::$PluginLicenseKey,
            'SelectedTab' => $SelectedTab,
            'AdminInformation' => self::$AdminInformation,
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'MailServerSettings' => $MailServerSettings,
            'Users' => $Users,
            'PluginVersion' => self::$PluginVersion,
            'PluginLicenseInfo' => self::$PluginLicenseInfo,
            'Mailgun_WebHookURL' => $Mailgun_WebHookURL,
            'Mandrill_WebHookURL' => $Mandrill_WebHookURL,
            'DeliveryMethodToDisplay' => $DeliveryMethodToDisplay
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->render('admin/settings', $ArrayViewData);
    }

    public function ui_admin_reports() {
        if (self::_PluginAppHeader('Admin') == false)
            return;

        $MessageActivityLog = self::$Models->messages->GetLastXDayMessageActivities(0, 30);
        $MessageStats = self::$Models->messages->GetMessageStats(0);
        $TopSenders = self::$Models->messages->GetTopSenders(date('Y-m'), 5);

        $TMPTopSenders = $TopSenders;
        $TopSenders = array();
        foreach ($TMPTopSenders as $Index => $EachSender) {
            $User = users::RetrieveUser(array('*'), array('UserID' => $EachSender->UserID), true);
            if (is_bool($User) == true)
                continue;
            $EachSender->UserInfo = $User;
            $TopSenders[] = $EachSender;
        }

        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . self::$ArrayLanguage['Screen']['0114'],
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => self::$ArrayLanguage['Screen']['0001'],
            'PluginLanguage' => self::$ArrayLanguage,
            'MessageActivityLog' => $MessageActivityLog,
            'MessageStats' => $MessageStats,
            'TopSenders' => $TopSenders,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'admin_reports', $ArrayViewData, true);
    }

    public function ui_email_automation() {

        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        $UserIDHash = md5(self::$SecurityHash . self::$SecurityHash . ' ' . self::$UserInformation['UserID']);
        $EncryptedUserID = implode('/', sl_generate_encrypted_parameter(array(self::$UserInformation['UserID']), 5));

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0001'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => null,
            'PageSuccessMessage' => null,
            'PeopleCount' => $PeopleCount,
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'UserIDHash' => $UserIDHash,
            'EncryptedUserID' => $EncryptedUserID,
            'Section' => 'GettingStarted',
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_overview', $ArrayViewData, true);
    }

    public function ui_search() {
        if (self::_PluginAppHeader('User') == false)
            return;

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => self::$ArrayLanguage['Screen']['0030']['Identifier'],
            'Email' => self::$ArrayLanguage['Screen']['0030']['Email'],
            'EntryDate' => self::$ArrayLanguage['Screen']['0030']['EntryDate'],
            'LastSeenDate' => self::$ArrayLanguage['Screen']['0030']['LastSeenDate'],
            'IPAddress' => self::$ArrayLanguage['Screen']['0030']['IPAddress'],
            'City' => self::$ArrayLanguage['Screen']['0030']['City'],
            'Country' => self::$ArrayLanguage['Screen']['0030']['Country'],
            'SessionCounter' => self::$ArrayLanguage['Screen']['0030']['SessionCounter'],
        );

        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }
//        print_r($_POST['SegmentOperator']);
//        print_r($_POST['RuleColumn']);
//        print_r($_POST['RuleOperator']);
//        print_r($_POST['RuleValue']);
//        exit();
        $MatchType = $_POST['MatchType'];
        $Rules = array();
        if (isset($_POST['Rule_Columns']) == true && is_array($_POST['Rule_Columns']) == true && count($_POST['Rule_Columns']) > 0) {
            foreach ($_POST['Rule_Columns'] as $Index => $EachRuleColumn) {
                $Rules[] = array(
                    'column' => $EachRuleColumn,
                    'operator' => (isset($_POST['Rule_Operators'][$Index]) == true ? $_POST['Rule_Operators'][$Index] : ''),
                    'val' => (isset($_POST['Rule_Values'][$Index]) == true ? $_POST['Rule_Values'][$Index] : ''),
                );
            }
        }

        $People = self::$Models->people->BrowsePeopleCustom(self::$UserInformation, false, $Rules, $MatchType);

        include('helpers.php');

        $ChosenColumns = array();

        foreach ($AvailableColumns as $key => $value) {
            $ChosenColumns[] = $key;
        }

        $CountryList = self::_GetCountryList();
        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $ArrayViewData = array(
            'People' => $People,
            'PeopleMetaData' => $PeopleMetaData,
            'PeopleDataStructure' => $PeopleDataStructure,
            'ChosenColumns' => $ChosenColumns,
            'AvailableColumns' => $AvailableColumns,
            'CountryList' => $CountryList,
        );

        self::$ObjectCI->load->helper('url');

        self::$ObjectCI->plugin_render('octautomation', 'user_people_browse_filter', $ArrayViewData, true);
    }

    public function ui_people($SubCommand = '') {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }
        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => self::$ArrayLanguage['Screen']['0030']['Identifier'],
            'Email' => self::$ArrayLanguage['Screen']['0030']['Email'],
            'EntryDate' => self::$ArrayLanguage['Screen']['0030']['EntryDate'],
            'LastSeenDate' => self::$ArrayLanguage['Screen']['0030']['LastSeenDate'],
            'IPAddress' => self::$ArrayLanguage['Screen']['0030']['IPAddress'],
            'City' => self::$ArrayLanguage['Screen']['0030']['City'],
            'Country' => self::$ArrayLanguage['Screen']['0030']['Country'],
            'SessionCounter' => self::$ArrayLanguage['Screen']['0030']['SessionCounter'],
        );

        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        // Events go here
        $EventResult = self::_EventDeletePeople();
        if (count($EventResult) == 2) {
            $PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
        }

        $People = self::$Models->people->BrowsePeopleCustom(self::$UserInformation);

        include('helpers.php');

        $PageMessage = array();
        $ChosenColumns = array();

        foreach ($AvailableColumns as $key => $value) {
            $ChosenColumns[] = $key;
        }

        $CountryList = self::_GetCountryList();
        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0029'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => null,
            'PageSuccessMessage' => null,
            'Section' => 'People',
            'People' => $People,
            'PeopleMetaData' => $PeopleMetaData,
            'PeopleDataStructure' => $PeopleDataStructure,
            'ChosenColumns' => $ChosenColumns,
            'AvailableColumns' => $AvailableColumns,
            'CountryList' => $CountryList,
            'TotalPeople' => $PeopleCount,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_people_browse', $ArrayViewData, true);
    }

    public function ui_people_old($SubCommand = '') {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $UserCookieSuffix = sha1(substr_replace(self::$SecurityHash, self::$UserInformation['UserID'], 5, 1));
        $CookieNamePeopleSorting = 'oempro_octautomation_people_sorting' . $UserCookieSuffix;
        $CookieNamePeopleColumns = 'oempro_octautomation_people_columns' . $UserCookieSuffix;

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => self::$ArrayLanguage['Screen']['0030']['Identifier'],
            'Email' => self::$ArrayLanguage['Screen']['0030']['Email'],
            'EntryDate' => self::$ArrayLanguage['Screen']['0030']['EntryDate'],
            'LastSeenDate' => self::$ArrayLanguage['Screen']['0030']['LastSeenDate'],
            'IPAddress' => self::$ArrayLanguage['Screen']['0030']['IPAddress'],
            'City' => self::$ArrayLanguage['Screen']['0030']['City'],
            'Country' => self::$ArrayLanguage['Screen']['0030']['Country'],
            'SessionCounter' => self::$ArrayLanguage['Screen']['0030']['SessionCounter'],
        );

        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        // Events go here
        $EventResult = self::_EventDeletePeople();
        if (count($EventResult) == 2) {
            $PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
        }

        $EventResult = self::_EventApplyFilters();
        if (count($EventResult) == 2) {
            $PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
        }

        $EventResult = self::_EventResetFilters();
        if (count($EventResult) == 2) {
            $PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
        }

//		$EventResult = $this->_EventCreateMessageFromRules();
//		if (count($EventResult) == 2)
//		{
//			$PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
//		}

        $RPP = 100000;
        $StartFrom = (self::$ObjectCI->input->get('p') > 0 ? (self::$ObjectCI->input->get('p') - 1) * $RPP : 0);

        if (isset($_COOKIE[$CookieNamePeopleSorting]) == true && $_COOKIE[$CookieNamePeopleSorting] != '') {
            $SortSettings = unserialize(DecryptText($_COOKIE[$CookieNamePeopleSorting]));

            $OrderByField = $SortSettings['OrderField'];
            $OrderByType = $SortSettings['OrderType'];
        } else {
            $OrderByField = 'LastSeenDate';
            $OrderByType = 'DESC';
        }

        if (self::$ObjectCI->input->get('sf') != false && self::$ObjectCI->get('st') != false) {
            if (in_array(self::$ObjectCI->get('sf'), array_keys($AvailableColumns)) == false) {
                $OrderByField = 'LastSeenDate';
                $OrderByType = 'DESC';
            } else {
                $OrderByField = self::$ObjectCI->get('sf');
                $OrderByType = (self::$ObjectCI->get('st') == 'asc' ? 'ASC' : 'DESC');

                $CookieValue = EncryptText(serialize(array('OrderField' => $OrderByField, 'OrderType' => $OrderByType)));
                setcookie($CookieNamePeopleSorting, $CookieValue, time() + (86400 * 365), '/', '.sendloop.com');
            }
        }

        $Rules = array();
        if (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && is_array($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && count($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) > 0) {
            foreach ($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns'] as $Index => $EachRuleColumn) {
                $Rules[] = array(
                    'column' => $EachRuleColumn,
                    'operator' => (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'][$Index]) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'][$Index] : ''),
                    'val' => (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'][$Index]) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'][$Index] : ''),
                );
            }
        }

        $TotalSearchResultPeople = self::$Models->people->BrowsePeople(self::$UserInformation, $StartFrom, $RPP, true, $OrderByField, $OrderByType, $Rules, (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'] : null));
        $People = self::$Models->people->BrowsePeople(self::$UserInformation, $StartFrom, $RPP, false, $OrderByField, $OrderByType, $Rules, (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'] : null));

        include('helpers.php');
        include_once('pagination.php');

        // Pagination - Start
        $OemproPagination = new Oempro_Pagination();

        // Important!!! Pagination is currently not working, therefore we set the RPP to 100,000
        $config['base_url'] = InterfaceAppUrl(true) . '/octautomation/people/&sf=' . $OrderByField . '&st=' . $OrderByType;
        $config['total_rows'] = $TotalSearchResultPeople;
        $config['per_page'] = $RPP;
        $config['uri_segment'] = 5;
        $config['num_links'] = 2;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'p';
        $config['display_pages'] = true;

        $config['full_tag_open'] = '<div class="table-pagination"><div class="container">';
        $config['full_tag_close'] = '</div></div>';

        $config['first_link'] = '';
        $config['first_tag_open'] = '<div class="first-page">';
        $config['first_tag_close'] = '</div>';

        $config['last_link'] = '';
        $config['last_tag_open'] = '<div class="last-page">';
        $config['last_tag_close'] = '</div>';

        $config['next_link'] = '';
        $config['next_tag_open'] = '<div class="next-page">';
        $config['next_tag_close'] = '</div>';

        $config['prev_link'] = '';
        $config['prev_tag_open'] = '<div class="previous-page">';
        $config['prev_tag_close'] = '</div>';

        $config['cur_tag_open'] = '<div class="page current">';
        $config['cur_tag_close'] = '</div>';

        $config['num_tag_open'] = '<div class="page">';
        $config['num_tag_close'] = '</div>';

        $OemproPagination->initialize($config);
        // Pagination - End

        $PageMessage = array();

        $EventResult = self::_EventUpdateColumns(array_keys($AvailableColumns), $CookieNamePeopleColumns);
        if (count($EventResult) == 2) {
            $PageMessage = array(($EventResult[0] == false ? 'error' : 'success'), $EventResult[1]);
        }
        $ChosenColumns = array();
        if (isset($_COOKIE[$CookieNamePeopleColumns]) == true && $_COOKIE[$CookieNamePeopleColumns] != '') {
            $ChosenColumns = unserialize(DecryptText($_COOKIE[$CookieNamePeopleColumns]));
        } else {
//            $ChosenColumns = array('Email', 'LastSeenDate','IPAddress','City','Country','SessionCounter');
        }

        foreach ($AvailableColumns as $key => $value) {
            $ChosenColumns[] = $key;
        }
        if ($SubCommand == 'save_column_order') {
            self::_EventSaveListColumns($CookieNamePeopleColumns);
            return;
        }

        $CountryList = self::_GetCountryList();
        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0029'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => null,
            'PageSuccessMessage' => null,
            'Section' => 'People',
            'People' => $People,
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'TotalSearchResultPeople' => $TotalSearchResultPeople,
            'PeopleMetaData' => $PeopleMetaData,
            'PeopleDataStructure' => $PeopleDataStructure,
            'ChosenColumns' => $ChosenColumns,
            'AvailableColumns' => $AvailableColumns,
            'OrderByField' => $OrderByField,
            'OrderByType' => $OrderByType,
            'CountryList' => $CountryList,
            'OemproPagination' => $OemproPagination,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_people_browse', $ArrayViewData, true);
    }

    public function ui_person($PersonID = 0, $SubCommand = '') {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => 'User ID',
            'Email' => 'Email Address',
            'EntryDate' => 'Entry Date',
            'LastSeenDate' => 'Last Seen Date',
            'IPAddress' => 'IP Address',
            'City' => 'City',
            'Country' => 'Country',
            'SessionCounter' => 'Sessions'
        );
        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        $Person = self::$Models->people->GetPerson(self::$UserInformation, $PersonID, true);
        if (is_bool($Person) == true) {
            redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
            return;
        }

        $Activity = self::$Models->people->GetActivityOfPerson(self::$UserInformation, $Person->PersonID, true);

        $CountryList = self::_GetCountryList();

        include('helpers.php');

        // Events go here...
        if ($SubCommand == 'delete') {
            self::_EventDeletePerson($Person->PersonID);
        }
        if ($SubCommand == 'unsubscribe') {
            self::_EventUnsubscribePerson($Person->PersonID);
        }

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0034'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => null,
            'PageSuccessMessage' => null,
            'Section' => 'People',
            'Person' => $Person,
            'Activity' => $Activity,
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'AvailableColumns' => $AvailableColumns,
            'PeopleMetaData' => $PeopleMetaData,
            'CountryList' => $CountryList,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_people_view', $ArrayViewData, true);
    }

    public function ui_user_get_people_count() {
        if (self::_PluginAppHeader('User') == false)
            return;

        header('Content-Type: application/json');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);

        print json_encode(array('PeopleCount' => $PeopleCount));
        return;
    }

    public function ui_estimate_recipients($MessageID = 0) {
        if (self::_PluginAppHeader('User') == false)
            return;

        $MatchType = self::$ObjectCI->input->post('match_type');
        $Rules = self::$ObjectCI->input->post('rules');

        $EstimatedResults = self::$Models->people->GetFilteredPeopleCount(self::$UserInformation, $Rules, $MatchType);

        header('content-type: application/json');
        print json_encode(array('recipients' => number_format($EstimatedResults)));
        return;
    }

    public function ui_messages($ListWhat = 'active', $SubCommand = '', $SubValue = '') {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');
        self::$ObjectCI->load->helper('text');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $AvailableListWhat = array('all', 'draft', 'active', 'paused', 'deleted');
        if (in_array($ListWhat, $AvailableListWhat) == false) {
            $ListWhat = 'active';
        }

        if ((self::$ObjectCI->input->post('Command') == 'Draft' || self::$ObjectCI->input->post('Command') == 'Delete' || self::$ObjectCI->input->post('Command') == 'Pause' || self::$ObjectCI->input->post('Command') == 'Activate') && count(self::$ObjectCI->input->post('SelectedMessages') > 0)) {
            foreach (self::$ObjectCI->input->post('SelectedMessages') as $Index => $EachMessageID) {
                $Message = self::$Models->messages->GetMessage(self::$UserInformation, $EachMessageID);
                if (is_bool($Message) == true)
                    continue;
                if (self::$ObjectCI->input->post('Command') == 'Delete') {
                    self::$Models->messages->UpdateMessage(self::$UserInformation, $EachMessageID, array(
                        'Status' => 'Deleted',
                    ));
                } elseif (self::$ObjectCI->input->post('Command') == 'Pause') {
                    self::$Models->messages->UpdateMessage(self::$UserInformation, $EachMessageID, array(
                        'Status' => 'Paused',
                    ));
                } elseif (self::$ObjectCI->input->post('Command') == 'Activate') {
                    self::$Models->messages->UpdateMessage(self::$UserInformation, $EachMessageID, array(
                        'Status' => 'Active',
                    ));
                } elseif (self::$ObjectCI->input->post('Command') == 'Draft') {
                    self::$Models->messages->UpdateMessage(self::$UserInformation, $EachMessageID, array(
                        'Status' => 'Draft',
                    ));
                }
            }

            redirect(InterfaceAppURL(true) . "/octautomation/messages/" . $ListWhat, 'redirect');
            return;
        }

        $Messages = self::$Models->messages->GetMessages(self::$UserInformation, $ListWhat);

        $AllMessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, false);

        $Draft_TotalMessages = self::$Models->messages->GetMessageTotal(self::$UserInformation, false, 'Draft');
        $Active_TotalMessages = self::$Models->messages->GetMessageTotal(self::$UserInformation, false, 'Active');
        $Paused_TotalMessages = self::$Models->messages->GetMessageTotal(self::$UserInformation, false, 'Paused');
        $Deleted_TotalMessages = self::$Models->messages->GetMessageTotal(self::$UserInformation, false, 'Deleted');

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0049'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => null,
            'PageSuccessMessage' => null,
            'Section' => 'Messages',
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'Messages' => $Messages,
            'AllMessageCount' => $AllMessageCount,
            'CurrentMessageType' => $ListWhat,
            'Draft_TotalMessages' => $Draft_TotalMessages,
            'Active_TotalMessages' => $Active_TotalMessages,
            'Paused_TotalMessages' => $Paused_TotalMessages,
            'Deleted_TotalMessages' => $Deleted_TotalMessages,
            'ListWhat' => $ListWhat,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_messages', $ArrayViewData, true);
    }

    public function ui_create_message() {
        if (self::_PluginAppHeader('User') == false)
            return;

        if (AccountIsUnTrustedCheck(self::$UserInformation)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => self::$ArrayLanguage['Screen']['0030']['Identifier'],
            'Email' => self::$ArrayLanguage['Screen']['0030']['Email'],
            'EntryDate' => self::$ArrayLanguage['Screen']['0030']['EntryDate'],
            'LastSeenDate' => self::$ArrayLanguage['Screen']['0030']['LastSeenDate'],
            'IPAddress' => self::$ArrayLanguage['Screen']['0030']['IPAddress'],
            'City' => self::$ArrayLanguage['Screen']['0030']['City'],
            'Country' => self::$ArrayLanguage['Screen']['0030']['Country'],
            'SessionCounter' => self::$ArrayLanguage['Screen']['0030']['SessionCounter'],
        );

        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        $Rules = array();
        if (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && is_array($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) == true && count($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns']) > 0) {
            foreach ($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Columns'] as $Index => $EachRuleColumn) {
                $Rules[] = array(
                    'column' => $EachRuleColumn,
                    'operator' => (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'][$Index]) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Operators'][$Index] : ''),
                    'val' => (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'][$Index]) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['Rule_Values'][$Index] : ''),
                );
            }
        }

        $TotalSearchResultPeople = self::$Models->people->BrowsePeople(self::$UserInformation, 0, 1000000000, true, 'LastSeenDate', 'DESC', $Rules, (isset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType']) == true ? $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']['MatchType'] : null));

        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $EventResult = self::_EventCreateMessage(false, false);
        if (count($EventResult) == 2) {
            if ($EventResult[0] == false) {
                $PageErrorMessage = $EventResult[1];
            } else {
                $PageSuccessMessage = $EventResult[1];
            }
        }

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0054'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'Section' => 'Messages',
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'TotalSearchResultPeople' => $TotalSearchResultPeople,
            'PeopleMetaData' => $PeopleMetaData,
            'PeopleDataStructure' => $PeopleDataStructure,
            'AvailableColumns' => $AvailableColumns,
            'EditMode' => false,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_create_message', $ArrayViewData, true);
    }

    public function ui_message($MessageID = 0) {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);

        if (is_bool($Message) == true && $Message == false) {
            redirect(InterfaceAppURL(true) . "/octautomation/messages/", 'redirect');
            return;
        }

        $PageErrorMessage = '';
        $PageSuccessMessage = '';

        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => 'User ID',
            'Email' => 'Email Address',
            'EntryDate' => 'Entry Date',
            'LastSeenDate' => 'Last Seen Date',
            'IPAddress' => 'IP Address',
            'City' => 'City',
            'Country' => 'Country',
            'SessionCounter' => 'Sessions'
        );
        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        $EventResult = self::_EventCreateMessage(true, $Message);
        if (count($EventResult) == 2) {
            if ($EventResult[0] == false) {
                $PageErrorMessage = $EventResult[1];
            } else {
                $PageSuccessMessage = $EventResult[1];
            }
        }

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0105'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'Section' => 'Messages',
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'Message' => $Message,
            'AvailableColumns' => $AvailableColumns,
            'PeopleDataStructure' => $PeopleDataStructure,
            'EditMode' => true,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_create_message', $ArrayViewData, true);
    }

    public function ui_report($MessageID = 0, $SubCommand = '') {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);
        $MessageCount = self::$Models->messages->GetMessageTotal(self::$UserInformation, true, 'Active');

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);

        if (is_bool($Message) == true && $Message == false) {
            redirect(InterfaceAppURL(true) . "/octautomation/messages/", 'redirect');
            return;
        }

        $Message->OpenedRatio = ceil((100 * $Message->TotalOpened) / $Message->TotalSent);
        $Message->ClickedRatio = ceil((100 * $Message->TotalClicked) / $Message->TotalSent);
        $Message->UnsubscriptionRatio = ceil((100 * $Message->TotalUnsubscribed) / $Message->TotalSent);
        $Message->HardBouncedRatio = ceil((100 * $Message->TotalHardBounced) / $Message->TotalSent);
        $Message->SpamRatio = ceil((100 * $Message->TotalSpamComplaint) / $Message->TotalSent);

        $PageErrorMessage = '';
        $PageSuccessMessage = '';

        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation);

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => 'User ID',
            'Email' => 'Email Address',
            'EntryDate' => 'Entry Date',
            'LastSeenDate' => 'Last Seen Date',
            'IPAddress' => 'IP Address',
            'City' => 'City',
            'Country' => 'Country',
            'SessionCounter' => 'Sessions'
        );
        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        if ($SubCommand == 'delete' || $SubCommand == 'pause' || $SubCommand == 'activate') {
            if ($SubCommand == 'delete') {
                self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                    'Status' => 'Deleted',
                ));
            } elseif ($SubCommand == 'pause') {
                self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                    'Status' => 'Paused',
                ));
            } elseif ($SubCommand == 'activate') {
                self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                    'Status' => 'Active',
                ));
            }

            redirect(InterfaceAppURL(true) . "/octautomation/report/" . $Message->MessageID, 'redirect');
            return;
        }

        $Message_OpenLog = self::$Models->messages->GetOpenLog(self::$UserInformation, $Message->MessageID);
        $Message_ClickLog = self::$Models->messages->GetClickLog(self::$UserInformation, $Message->MessageID);
        $Message_UnsubscriptionLog = self::$Models->messages->GetUnsubscriptionLog(self::$UserInformation, $Message->MessageID);
        $Message_DeliveryTimeFrame = self::$Models->messages->GetTimeFrame(self::$UserInformation, $Message->MessageID, 30);
        $GeoOpenStats = array();

        if (is_array($Message_OpenLog) == true && count($Message_OpenLog) > 0) {
            foreach ($Message_OpenLog as $Index => $EachLog) {
                $GeoOpenStats[$EachLog->Open_Country][$EachLog->Open_City] = (isset($GeoOpenStats[$EachLog->Open_Country][$EachLog->Open_City]) == true && $GeoOpenStats[$EachLog->Open_Country][$EachLog->Open_City] > 0 ? $GeoOpenStats[$EachLog->Open_Country][$EachLog->Open_City] + 1 : 1);
            }
        }

        $CountryList = self::_GetCountryList();

        $ArrayViewData = array(
            'PageTitle' => self::$ArrayLanguage['Screen']['0054'],
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'PageErrorMessage' => $PageErrorMessage,
            'PageSuccessMessage' => $PageSuccessMessage,
            'Section' => 'Messages',
            'TotalPeople' => $PeopleCount,
            'TotalMessages' => $MessageCount,
            'Message' => $Message,
            'AvailableColumns' => $AvailableColumns,
            'PeopleDataStructure' => $PeopleDataStructure,
            'Message_OpenLog' => $Message_OpenLog,
            'Message_ClickLog' => $Message_ClickLog,
            'Message_UnsubscriptionLog' => $Message_UnsubscriptionLog,
            'GeoOpenStats' => $GeoOpenStats,
            'CountryList' => $CountryList,
            'Message_DeliveryTimeFrame' => $Message_DeliveryTimeFrame,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        self::$ObjectCI->plugin_render('octautomation', 'user_message_overview', $ArrayViewData, true);
    }

    public function ui_send_test_email() {
        if (self::_PluginAppHeader('User') == false)
            return;

        header('content-type:application/json');

        $PeopleMetaData = self::$Models->people->GetPeopleMetaData(self::$UserInformation);
        if (is_bool($PeopleMetaData) == false) {
            $PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
        }

        $AvailableColumns = array(
            'Identifier' => 'User ID',
            'Email' => 'Email Address',
            'EntryDate' => 'Entry Date',
            'LastSeenDate' => 'Last Seen Date',
            'IPAddress' => 'IP Address',
            'City' => 'City',
            'Country' => 'Country',
            'SessionCounter' => 'Sessions'
        );
        if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0) {
            foreach (get_object_vars($PeopleMetaData) as $Name => $Key) {
                $AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
            }
        }

        // Email verification
        $IsToEmailValid = true;
        $ToEmails = array();

        if (strpos(self::$ObjectCI->input->post('ToEmail'), ',') !== false) {
            $ToEmails = explode(',', self::$ObjectCI->input->post('ToEmail'));
        } else {
            $ToEmails[] = self::$ObjectCI->input->post('ToEmail');
        }

        foreach ($ToEmails as $EachToEmail) {
            if (filter_var($EachToEmail, FILTER_VALIDATE_EMAIL) == false) {
                $IsToEmailValid = false;
                break;
            }
        }

        if (!$IsToEmailValid) {
            print(json_encode(array(
                        'type' => 'error',
                        'message' => self::$ArrayLanguage['Screen']['0110'],
            )));
            return;
        }

        include_once(PLUGIN_PATH . "/octautomation/personalization.php");
        include_once(PLUGIN_PATH . "/octautomation/htmlcodetools.php");

        // Select a random recipient based on the matching rules
        $MatchType = self::$ObjectCI->input->post('MatchType');
        $Rules = self::$ObjectCI->input->post('Rules');
        $RandomRecipient = self::$Models->people->SelectRandomRecipient(self::$UserInformation, $Rules, $MatchType, true);

        $EncryptedUserID = sl_encrypt_number(self::$UserInformation['UserID'], 5);

        $Subject = self::$ObjectCI->input->post('Subject');
        $HTMLContent = $_REQUEST['HTMLContent'];
        $PlainContent = self::$ObjectCI->input->post('PlainContent');

        $Personalization = new Personalization();

        $ContentType = '';
        $Subject = $Personalization->Engage_PersonalizeWithSubscriberInfo($Subject, $RandomRecipient, $AvailableColumns);
        $PlainContent = $Personalization->Engage_PersonalizeWithSubscriberInfo($PlainContent, $RandomRecipient, $AvailableColumns);
        $HTMLContent = $Personalization->Engage_PersonalizeWithSubscriberInfo($HTMLContent, $RandomRecipient, $AvailableColumns);

        preg_match('/<body(.*)>(.*)<\/body>/is', $HTMLContent, $Matches);
        if (isset($Matches[0]) == true) {
            $StrippedHTML = preg_replace('/\s+/', '', strip_tags($Matches[0]));
        }

        $MailServerSettings = Database::$Interface->GetOption('OctAutomation_MailServer');
        $MailServerSettings = json_decode($MailServerSettings[0]['OptionValue']);

        // Send the preview email
        foreach ($ToEmails as $EachToEmail) {
            $PreviewResult = Mailgun_SendMessage($MailServerSettings->Domain, $MailServerSettings->APIKey, self::$UserInformation['UserID'], 0, 0, $EachToEmail, self::$ObjectCI->input->post('FromName'), self::$ObjectCI->input->post('FromEmail'), self::$ObjectCI->input->post('ReplyToName'), self::$ObjectCI->input->post('ReplyToEmail'), self::$ArrayLanguage['Screen']['0112'] . ' ' . $Subject, $PlainContent, $HTMLContent);
            if (is_bool($PreviewResult) == true && $PreviewResult == false) {
                $DeliveryResult = false;
            } else {
                $DeliveryResult = true;
            }
        }

        if ($DeliveryResult == false) {
            print(json_encode(array(
                        'type' => 'error',
                        'message' => self::$ArrayLanguage['Screen']['0113'],
            )));
            return;
        }

        // Response
        print(json_encode(array(
                    'type' => 'success', // error, success
                    'message' => self::$ArrayLanguage['Screen']['0111'],
        )));
        return;
    }

    public function ui_people_delete() {
        // Events go here
        if (self::_PluginAppHeader('User') == false)
            return;

        if ($_POST['SelectedPeopleIDs'] != false) {
            $SelectedPeople = explode(',', $_POST['SelectedPeopleIDs']);
        } else {
            $SelectedPeople = array();
        }
        if (count($SelectedPeople) > 0) {
            foreach ($SelectedPeople as $EachPersonID) {
                self::$Models->people->DeletePerson(self::$UserInformation, $EachPersonID);
            }
            if (count($SelectedPeople) == 1) {
                print_r((json_encode(array(true, self::$ArrayLanguage['Screen']['0219']))));
            } else {
                print_r((json_encode(array(true, self::$ArrayLanguage['Screen']['0032']))));
            }
        } else {
            print_r((json_encode(array(false, self::$ArrayLanguage['Screen']['0033']))));
        }
        return;
    }

    /* Events */

    private function _EventTestDeliverySettings($APIKey = '', $CustomDomain = '', $ToEmail = '') {
        $TestResult = Mailgun_SendMessage($CustomDomain, $APIKey, self::$AdminInformation['AdminID'], 0, 0, self::$AdminInformation['EmailAddress'], self::$AdminInformation['Name'], self::$AdminInformation['EmailAddress'], self::$AdminInformation['Name'], self::$AdminInformation['EmailAddress'], self::$ArrayLanguage['Screen']['0142'], '', self::$ArrayLanguage['Screen']['0143']);

        if (is_bool($TestResult) == true && $TestResult == false)
            return false;

        return true;
    }

    private function _EventSaveAdminSettings() {
        if (self::$ObjectCI->input->post('Command') != 'UpdateSettings')
            return false;

        self::$ObjectCI->load->helper('url');

        if (self::$ObjectCI->input->post('DeliveryMethod') == 'mailgun') {
            self::$ObjectCI->form_validation->set_rules('Mailgun_APIKey', self::$ArrayLanguage['Screen']['0135'], 'required');
            self::$ObjectCI->form_validation->set_rules('Mailgun_Domain', self::$ArrayLanguage['Screen']['0137'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestResult = self::_EventTestDeliverySettings(self::$ObjectCI->input->post('Mailgun_APIKey'), self::$ObjectCI->input->post('Mailgun_Domain'), self::$AdminInformation['EmailAddress']);
            if ($TestResult == false) {
                self::$ObjectCI->form_validation->set_error_to_field('Mailgun_APIKey', self::$ArrayLanguage['Screen']['0145']);
                self::$ObjectCI->form_validation->set_error_to_field('Mailgun_Domain', self::$ArrayLanguage['Screen']['0146']);
                return array(false, self::$ArrayLanguage['Screen']['0144']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'mailgun',
                'APIKey' => self::$ObjectCI->input->post('Mailgun_APIKey'),
                'Domain' => self::$ObjectCI->input->post('Mailgun_Domain'),
            )));
        } else if (self::$ObjectCI->input->post('DeliveryMethod') == 'mandrill') {
            self::$ObjectCI->form_validation->set_rules('Mandrill_Username', self::$ArrayLanguage['Screen']['0135'], 'required');
            self::$ObjectCI->form_validation->set_rules('Mandrill_Password', self::$ArrayLanguage['Screen']['0137'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestServerSettings = new stdClass;
            $TestServerSettings->Hostname = 'smtp.mandrillapp.com';
            $TestServerSettings->Port = '587';
            $TestServerSettings->Encryption = '';
            $TestServerSettings->Username = self::$ObjectCI->input->post('Mandrill_Username');
            $TestServerSettings->Password = self::$ObjectCI->input->post('Mandrill_Password');
            $TestServerSettings->Timeout = '30';
            $TestFromEmailAddress = self::$AdminInformation['EmailAddress'];
            $TestFromName = self::$AdminInformation['Name'];
            $TestResult = Mandrill_SendMessage($TestServerSettings, 0, 0, 0, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, self::$ArrayLanguage['Screen']['0218'], '.', '.');
            if ($TestResult === false) {
                self::$ObjectCI->form_validation->set_error_to_field('Mandrill_Username', self::$ArrayLanguage['Screen']['0203']);
                self::$ObjectCI->form_validation->set_error_to_field('Mandrill_Password', self::$ArrayLanguage['Screen']['0204']);
                return array(false, self::$ArrayLanguage['Screen']['0205']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'mandrill',
                'Hostname' => 'smtp.mandrillapp.com',
                'Port' => '587',
                'Encryption' => '',
                'Timeout' => '30',
                'Username' => self::$ObjectCI->input->post('Mandrill_Username'),
                'Password' => self::$ObjectCI->input->post('Mandrill_Password')
            )));
        } else if (self::$ObjectCI->input->post('DeliveryMethod') == 'savetodirectory') {
            self::$ObjectCI->form_validation->set_rules('SaveToDirectory_Directory', self::$ArrayLanguage['Screen']['0206'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestServerSettings = new stdClass;
            $TestServerSettings->Directory = self::$ObjectCI->input->post('SaveToDirectory_Directory');
            $TestFromEmailAddress = self::$AdminInformation['EmailAddress'];
            $TestFromName = self::$AdminInformation['Name'];
            $TestResult = SaveToDirectory_SendMessage($TestServerSettings, 0, 0, 0, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, self::$ArrayLanguage['Screen']['0218'], '.', '.');
            if ($TestResult === false) {
                self::$ObjectCI->form_validation->set_error_to_field('SaveToDirectory_Directory', self::$ArrayLanguage['Screen']['0207']);
                return array(false, self::$ArrayLanguage['Screen']['0208']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'savetodirectory',
                'Directory' => self::$ObjectCI->input->post('SaveToDirectory_Directory')
            )));
        } else if (self::$ObjectCI->input->post('DeliveryMethod') == 'powermta') {
            self::$ObjectCI->form_validation->set_rules('PowerMTA_VMTA', self::$ArrayLanguage['Screen']['0209'], 'required');
            self::$ObjectCI->form_validation->set_rules('PowerMTA_Directory', self::$ArrayLanguage['Screen']['0210'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestServerSettings = new stdClass;
            $TestServerSettings->Directory = self::$ObjectCI->input->post('PowerMTA_Directory');
            $TestServerSettings->VMTA = self::$ObjectCI->input->post('PowerMTA_VMTA');
            $TestFromEmailAddress = self::$AdminInformation['EmailAddress'];
            $TestFromName = self::$AdminInformation['Name'];
            $TestResult = SaveToDirectory_SendMessage($TestServerSettings, 0, 0, 0, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, self::$ArrayLanguage['Screen']['0218'], '.', '.');
            if ($TestResult == false) {
                self::$ObjectCI->form_validation->set_error_to_field('PowerMTA_Directory', self::$ArrayLanguage['Screen']['0211']);
                return array(false, self::$ArrayLanguage['Screen']['0208']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'powermta',
                'Directory' => self::$ObjectCI->input->post('PowerMTA_Directory'),
                'VMTA' => self::$ObjectCI->input->post('PowerMTA_VMTA')
            )));
        } else if (self::$ObjectCI->input->post('DeliveryMethod') == 'smtp') {
            self::$ObjectCI->form_validation->set_rules('SMTP_Host', self::$ArrayLanguage['Screen']['0212'], 'required');
            self::$ObjectCI->form_validation->set_rules('SMTP_Port', self::$ArrayLanguage['Screen']['0213'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestServerSettings = new stdClass;
            $TestServerSettings->Hostname = self::$ObjectCI->input->post('SMTP_Host');
            $TestServerSettings->Port = self::$ObjectCI->input->post('SMTP_Port');
            $TestServerSettings->Encryption = self::$ObjectCI->input->post('SMTP_Encryption');
            $TestServerSettings->Username = self::$ObjectCI->input->post('SMTP_Username');
            $TestServerSettings->Password = self::$ObjectCI->input->post('SMTP_Password');
            $TestServerSettings->Timeout = self::$ObjectCI->input->post('SMTP_Timeout');
            $TestFromEmailAddress = self::$AdminInformation['EmailAddress'];
            $TestFromName = self::$AdminInformation['Name'];
            $TestResult = SMTP_SendMessage($TestServerSettings, 0, 0, 0, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, self::$ArrayLanguage['Screen']['0218'], '.', '.');
            if ($TestResult === false) {
                return array(false, self::$ArrayLanguage['Screen']['0208']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'smtp',
                'Hostname' => self::$ObjectCI->input->post('SMTP_Host'),
                'Port' => self::$ObjectCI->input->post('SMTP_Port'),
                'Encryption' => self::$ObjectCI->input->post('SMTP_Encryption'),
                'Username' => self::$ObjectCI->input->post('SMTP_Username'),
                'Password' => self::$ObjectCI->input->post('SMTP_Password'),
                'Timeout' => self::$ObjectCI->input->post('SMTP_Timeout')
            )));
        } else if (self::$ObjectCI->input->post('DeliveryMethod') == 'smtpcom') {
            self::$ObjectCI->form_validation->set_rules('SMTPCOM_Username', self::$ArrayLanguage['Screen']['0214'], 'required');
            self::$ObjectCI->form_validation->set_rules('SMTPCOM_Password', self::$ArrayLanguage['Screen']['0215'], 'required');

            if (self::$ObjectCI->form_validation->run() === false) {
                return array(false, self::$ArrayLanguage['Screen']['0057']);
            }

            $TestServerSettings = new stdClass;
            $TestServerSettings->Hostname = 'octeth.smtp.com';
            $TestServerSettings->Port = '25';
            $TestServerSettings->Encryption = '';
            $TestServerSettings->Username = self::$ObjectCI->input->post('SMTPCOM_Username');
            $TestServerSettings->Password = self::$ObjectCI->input->post('SMTPCOM_Password');
            $TestServerSettings->Timeout = '15';
            $TestFromEmailAddress = self::$AdminInformation['EmailAddress'];
            $TestFromName = self::$AdminInformation['Name'];
            $TestResult = SMTP_SendMessage($TestServerSettings, 0, 0, 0, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, $TestFromName, $TestFromEmailAddress, self::$ArrayLanguage['Screen']['0218'], '.', '.');
            if ($TestResult == false) {
                return array(false, self::$ArrayLanguage['Screen']['0216']);
            }

            Database::$Interface->SaveOption('OctAutomation_MailServer', json_encode(array(
                'Engine' => 'smtp',
                'Hostname' => 'octeth.smtp.com',
                'Port' => '25',
                'Encryption' => '',
                'Username' => self::$ObjectCI->input->post('SMTPCOM_Username'),
                'Password' => self::$ObjectCI->input->post('SMTPCOM_Password'),
                'Timeout' => '15'
            )));
        }

        return array(true, self::$ArrayLanguage['Screen']['0147']);
    }

    private function _EventCreateMessage($IsEditMode = false, $Message = false) {
        if (self::$ObjectCI->input->post('Command') != 'CreateMessage' && self::$ObjectCI->input->post('Command') != 'CreateDraftMessage' && self::$ObjectCI->input->post('Command') != 'DeleteMessage' && self::$ObjectCI->input->post('Command') != 'PauseMessage')
            return false;

        self::$ObjectCI->load->helper('url');

        self::$ObjectCI->form_validation->set_rules('MessageName', self::$ArrayLanguage['Screen']['0060'], 'required');
        self::$ObjectCI->form_validation->set_rules('FromName', self::$ArrayLanguage['Screen']['0061'], 'required');
        self::$ObjectCI->form_validation->set_rules('FromEmail', self::$ArrayLanguage['Screen']['0062'], 'required|valid_email');
        self::$ObjectCI->form_validation->set_rules('ReplyToName', self::$ArrayLanguage['Screen']['0063'], 'required');
        self::$ObjectCI->form_validation->set_rules('ReplyToEmail', self::$ArrayLanguage['Screen']['0064'], 'required|valid_email');
        self::$ObjectCI->form_validation->set_rules('Subject', self::$ArrayLanguage['Screen']['0065'], 'required');
        self::$ObjectCI->form_validation->set_rules('builder', self::$ArrayLanguage['Screen']['0066'], '');
        self::$ObjectCI->form_validation->set_rules('BuilderPlainText', self::$ArrayLanguage['Screen']['0067'], '');
        self::$ObjectCI->form_validation->set_rules('Rules', self::$ArrayLanguage['Screen']['0068'], '');
        self::$ObjectCI->form_validation->set_rules('SegmentOperator', self::$ArrayLanguage['Screen']['0069'], 'required');
        self::$ObjectCI->form_validation->set_rules('PlainContent', self::$ArrayLanguage['Screen']['0070'], '');

        if (self::$ObjectCI->form_validation->run() === false) {
            return array(false, self::$ArrayLanguage['Screen']['0057']);
        }

        $Subject = self::$ObjectCI->input->post('Subject');

        /*
         * IMPORTANT!!!! READ THIS FIRST BEFORE MAKING ANY CHANGES!
         * Use $_REQUEST['HTMLContent'] here to avoid CodeIgniter's form_prep html entity encryption.
         * */
        $HTMLContent = $_REQUEST['builder'];
        $PlainContent = self::$ObjectCI->input->post('BuilderPlainText');

        preg_match('/<body(.*)>(.*)<\/body>/is', $HTMLContent, $Matches);
        if (isset($Matches[0]) == true) {
            $StrippedHTML = preg_replace('/\s+/', '', strip_tags($Matches[0]));
        } else {
            $StrippedHTML = '';
        }

        if ($PlainContent != '' && $StrippedHTML == '') {
            $EmailContentType = 'plain';
        } elseif ($StrippedHTML != '' && $PlainContent == '') {
            $EmailContentType = 'html';
        } elseif ($StrippedHTML != '' && $PlainContent != '') {
            $EmailContentType = 'multipart';
        } else {
            $EmailContentType = 'plain';
        }

        if ($StrippedHTML == '' && $PlainContent == '') {
            self::$ObjectCI->form_validation->set_error_to_field('builder', self::$ArrayLanguage['Screen']['0058']);
            return array(false, self::$ArrayLanguage['Screen']['0059']);
        }

        // Check for required tags in email contents
        self::$ObjectCI->load->library('EmailContentValidator');
        if ($EmailContentType == 'html' || $EmailContentType == 'multipart') {
            if ($StrippedHTML == '') {
                self::$ObjectCI->form_validation->set_error_to_field('builder', self::$ArrayLanguage['Screen']['0071']);
                $FormValidationErrorExists = true;
            }

//			$Status = $this->emailcontentvalidator->DetectTagInContent($HTMLContent, '%Link:Unsubscribe%', true);
//			if ($Status == false)
//			{
//				$this->form_validation->set_error_to_field('builder', $this->lang->line('00309'));
//				$FormValidationErrorExists = true;
//			}

            $HTMLContent = self::$ObjectCI->emailcontentvalidator->RemoveUnwantedCodes($HTMLContent);
        }

        if ($EmailContentType == 'plain' || $EmailContentType == 'multipart') {
            if ($PlainContent == '') {
                self::$ObjectCI->form_validation->set_error_to_field('PlainContent', self::$ArrayLanguage['Screen']['0072']);
                $FormValidationErrorExists = true;
            }

//			$Status = $this->emailcontentvalidator->DetectTagInContent($PlainContent, '%Link:Unsubscribe%', false);
//			if ($Status == false)
//			{
//				$this->form_validation->set_error_to_field('PlainContent', $this->lang->line('00310'));
//				$FormValidationErrorExists = true;
//			}

            $PlainContent = self::$ObjectCI->emailcontentvalidator->RemoveUnwantedCodes($PlainContent);
        }

        // Validate email campaign HTML content
        if ($EmailContentType == 'html' || $EmailContentType == 'multipart') {
            $Status = self::$ObjectCI->emailcontentvalidator->CheckImageURLFormat($HTMLContent);
            if ($Status == false) {
                self::$ObjectCI->form_validation->set_error_to_field('builder', self::$ArrayLanguage['Screen']['0073']);
                $FormValidationErrorExists = true;
            }

            $Status = self::$ObjectCI->emailcontentvalidator->DetectUnwantedCodes($HTMLContent);
            if ($Status == false) {
                self::$ObjectCI->form_validation->set_error_to_field('builder', self::$ArrayLanguage['Screen']['0074']);
                return;
            }
        }

        if ($FormValidationErrorExists == true) {
            return array(false, self::$ArrayLanguage['Screen']['0059']);
        }

        self::$ObjectCI->load->helper('htmlcodetools');
        $HTMLContent = ChangeMetaCharset($HTMLContent);
        $HTMLContent = RemoveIguanaHTMLCodes($HTMLContent);

        if ($IsEditMode == false) {
            $NewMessageID = self::$Models->messages->CreateMessage(self::$UserInformation, array(
                'MessageID' => '',
                'RelUserID' => self::$UserInformation['UserID'],
                'MessageName' => self::$ObjectCI->input->post('MessageName'),
                'MessageType' => 'Email',
                'CreatedAt' => date('Y-m-d H:i:s'),
                'Status' => (self::$ObjectCI->input->post('Command') == 'CreateDraftMessage' ? 'Draft' : 'Active'),
                'FromName' => self::$ObjectCI->input->post('FromName'),
                'FromEmail' => self::$ObjectCI->input->post('FromEmail'),
                'ReplyToName' => self::$ObjectCI->input->post('ReplyToName'),
                'ReplyToEmail' => self::$ObjectCI->input->post('ReplyToEmail'),
                'Subject' => self::$ObjectCI->input->post('Subject'),
                'HTMLContent' => $HTMLContent,
                'PlainContent' => $PlainContent,
                'TotalSent' => 0,
                'TotalOpened' => 0,
                'TotalClicked' => 0,
                'TotalUnsubscribed' => 0,
                'TotalHardBounced' => 0,
                'TotalSpamComplaint' => 0,
                'LatestEmailSentAt' => 0,
                'Rules' => json_encode(array('MatchType' => self::$ObjectCI->input->post('SegmentOperator'), 'RuleColumn' => self::$ObjectCI->input->post('RuleColumn'), 'RuleOperator' => self::$ObjectCI->input->post('RuleOperator'), 'RuleValue' => self::$ObjectCI->input->post('RuleValue'))),
            ));
            redirect(InterfaceAppURL(true) . "/octautomation/messages", 'redirect');
            return;
        } else {
            $MessageStatus = 'Draft';
            if (self::$ObjectCI->input->post('Command') == 'CreateMessage') {
                $MessageStatus = 'Active';
            } elseif (self::$ObjectCI->input->post('Command') == 'CreateDraftMessage') {
                $MessageStatus = 'Draft';
            } elseif (self::$ObjectCI->input->post('Command') == 'DeleteMessage') {
                $MessageStatus = 'Deleted';
            } elseif (self::$ObjectCI->input->post('Command') == 'PauseMessage') {
                $MessageStatus = 'Paused';
            }

            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'MessageName' => self::$ObjectCI->input->post('MessageName'),
                'Status' => $MessageStatus,
                'FromName' => self::$ObjectCI->input->post('FromName'),
                'FromEmail' => self::$ObjectCI->input->post('FromEmail'),
                'ReplyToName' => self::$ObjectCI->input->post('ReplyToName'),
                'ReplyToEmail' => self::$ObjectCI->input->post('ReplyToEmail'),
                'Subject' => self::$ObjectCI->input->post('Subject'),
                'HTMLContent' => $HTMLContent,
                'PlainContent' => $PlainContent,
                'Rules' => json_encode(array('MatchType' => self::$ObjectCI->input->post('SegmentOperator'), 'RuleColumn' => self::$ObjectCI->input->post('RuleColumn'), 'RuleOperator' => self::$ObjectCI->input->post('RuleOperator'), 'RuleValue' => self::$ObjectCI->input->post('RuleValue'))),
            ));
            redirect(InterfaceAppURL(true) . "/octautomation/report/" . $Message->MessageID, 'redirect');
            return;
        }
    }

    private function _EventDeletePerson($PersonID = 0) {
        self::$Models->people->DeletePerson(self::$UserInformation, $PersonID);

        redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
        return;
    }

    private function _EventUnsubscribePerson($PersonID = 0) {
        self::$Models->people->UnsubscribePerson(self::$UserInformation, $PersonID);

        redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
        return;
    }

    private function _EventDeletePeople() {

        if (self::$ObjectCI->input->post('Command') != 'DeletePeople')
            return;

        if (self::$ObjectCI->input->post('SelectedPeopleIDs') != false) {
            $SelectedPeople = explode(',', self::$ObjectCI->input->post('SelectedPeopleIDs'));
        } else {
            $SelectedPeople = array();
        }

        if (count($SelectedPeople) > 0) {
            foreach ($SelectedPeople as $Index => $EachPersonID) {
                self::$Models->people->DeletePerson(self::$UserInformation, $EachPersonID);
            }
            return array(true, self::$ArrayLanguage['Screen']['0032']);
        } else {
            return array(false, self::$ArrayLanguage['Screen']['0033']);
        }
    }

    private function _EventApplyFilters() {
        if (self::$ObjectCI->input->post('Command') != 'ApplyFilter')
            return false;

        $PeopleDataStructure = self::$Models->people->GetPeopleDataStructure(self::$UserInformation['UserID']);

        $MatchType = self::$ObjectCI->input->post('SegmentOperator');
        $Rules = array();
        $Rules_Columns = self::$ObjectCI->input->post('RuleColumn');
        $Rules_Operators = self::$ObjectCI->input->post('RuleOperator');
        $Rules_Values = self::$ObjectCI->input->post('RuleValue');

        if (is_array($Rules_Columns) == true && count($Rules_Columns) > 0) {
            foreach ($Rules_Columns as $Index => $EachColumn) {
                if (isset($PeopleDataStructure[$EachColumn]) == false)
                    continue;

                $Rules[] = array(
                    'column' => $EachColumn,
                    'operator' => $Rules_Operators[$Index],
                    'val' => $Rules_Values[$Index],
                );
            }
        }

        $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering'] = array(
            'MatchType' => $MatchType,
            'Rule_Columns' => $Rules_Columns,
            'Rule_Operators' => $Rules_Operators,
            'Rule_Values' => $Rules_Values,
        );

        redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
        return;
    }

    private function _EventSaveListColumns($CookieName) {
        if (self::$ObjectCI->input->post('columns') == false)
            return false;

        $Columns = explode(',', self::$ObjectCI->input->post('columns'));

        $ChosenColumns = array();
        foreach ($Columns as $Index => $EachColumn) {
            if ($EachColumn == 'checkbox' || $EachColumn == 'gravatar')
                continue;

            $ChosenColumns[] = $EachColumn;
        }

        $CookieValue = EncryptText(serialize($ChosenColumns));
        setcookie($CookieName, $CookieValue, time() + (86400 * 365), '/');

        return;
    }

    private function _EventUpdateColumns($AvailableColumns, $CookieName) {
        if (self::$ObjectCI->input->post('Command2') != 'ChangeColumns')
            return;

        self::$ObjectCI->form_validation->set_rules('ShowColumns[]', 'Column', 'required');

        if (self::$ObjectCI->form_validation->run() === false) {
            return array(false, self::$ArrayLanguage['Screen']['0031']);
        }

        $SelectedColumns = self::$ObjectCI->input->post('ShowColumns');

        $ChosenColumns = array();
        foreach ($SelectedColumns as $Index => $EachSelectedColumn) {
            if (in_array($EachSelectedColumn, $AvailableColumns) !== false) {
                $ChosenColumns[] = $EachSelectedColumn;
            }
        }

        $CookieValue = EncryptText(serialize($ChosenColumns));
        setcookie($CookieName, $CookieValue, time() + (86400 * 365), '/');

        redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
        return;
    }

    private function _EventResetFilters() {
        if (self::$ObjectCI->input->post('Command') != 'ResetFilter')
            return false;

        $_SESSION['OemproOctAutomation']['PeopleBrowseFiltering'] = array();
        unset($_SESSION['OemproOctAutomation']['PeopleBrowseFiltering']);

        redirect(InterfaceAppURL(true) . "/octautomation/people", 'redirect');
        return;
    }

    /* Private Methods */

    private function _PluginAppHeader($Section = 'Admin') {
        self::$ObjectCI = & get_instance();

        if (self::$PluginLicenseStatus == false) {
            $Message = sprintf(self::$ArrayLanguage['Screen']['0130'], (defined('octautomation_LicenseStatusMessage') == true ? (octautomation_LicenseStatusMessage == 'expired' ? self::$ArrayLanguage['Screen']['0175'] : '') : ''), self::$PluginLicenseKey);
            self::$ObjectCI->display_public_message('', $Message);
            return false;
        }

        if (Plugins::IsPlugInEnabled('octautomation') == false) {
            $Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
            self::$ObjectCI->display_public_message('', $Message);
            return false;
        }

        if ($Section == 'Admin') {
            self::_CheckAdminAuth();
        } elseif ($Section == 'User') {
            self::_CheckUserAuth();
        }

        self::$ObjectCI->load->helper('text');

        return true;
    }

    private function _CheckAdminAuth() {
        Core::LoadObject('admin_auth');
        Core::LoadObject('admins');

        AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');

        self::$AdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));

        return;
    }

    private function _CheckUserAuth() {
        Core::LoadObject('user_auth');

        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');

        self::$UserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);

        if (Users::IsAccountExpired(self::$UserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            self::$ObjectCI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }

        return;
    }

    private function _ReturnJSONPResponse($Callback = '', $Response = array()) {
        header('content-type: application/json; charset=utf-8');
        print($Callback . '(' . (count($Response) > 0 ? json_encode($Response) : '') . ')');
        return;
    }

    private function _LoadModels() {
        include_once('models/base.php');
        self::$Models = new stdClass();

        include_once('models/messages.php');
        self::$Models->messages = new model_messages();
        self::$Models->messages->Models = self::$Models;

        include_once('models/people.php');
        self::$Models->people = new model_people();
        self::$Models->people->Models = self::$Models;
    }

    private function _GetCountryList() {
        $ArrayCountryList = array(
            'AD' => "Andorra",
            'AE' => "United Arab Emirates",
            'AF' => "Afghanistan",
            'AG' => "Antigua & Barbuda",
            'AI' => "Anguilla",
            'AL' => "Albania",
            'AM' => "Armenia",
            'AN' => "Netherlands Antilles",
            'AO' => "Angola",
            'AQ' => "Antarctica",
            'AR' => "Argentina",
            'AS' => "American Samoa",
            'AT' => "Austria",
            'AU' => "Australia",
            'AW' => "Aruba",
            'AZ' => "Azerbaijan",
            'BA' => "Bosnia and Herzegovina",
            'BB' => "Barbados",
            'BD' => "Bangladesh",
            'BE' => "Belgium",
            'BF' => "Burkina Faso",
            'BG' => "Bulgaria",
            'BH' => "Bahrain",
            'BI' => "Burundi",
            'BJ' => "Benin",
            'BM' => "Bermuda",
            'BN' => "Brunei Darussalam",
            'BO' => "Bolivia",
            'BR' => "Brazil",
            'BS' => "Bahama",
            'BT' => "Bhutan",
            'BV' => "Bouvet Island",
            'BW' => "Botswana",
            'BY' => "Belarus",
            'BZ' => "Belize",
            'CA' => "Canada",
            'CC' => "Cocos (Keeling) Islands",
            'CF' => "Central African Republic",
            'CG' => "Congo",
            'CH' => "Switzerland",
            'CI' => "Côte D'ivoire (Ivory Coast)",
            'CK' => "Cook Iislands",
            'CL' => "Chile",
            'CM' => "Cameroon",
            'CN' => "China",
            'CO' => "Colombia",
            'CR' => "Costa Rica",
            'CU' => "Cuba",
            'CV' => "Cape Verde",
            'CX' => "Christmas Island",
            'CY' => "Cyprus",
            'CZ' => "Czech Republic",
            'DE' => "Germany",
            'DJ' => "Djibouti",
            'DK' => "Denmark",
            'DM' => "Dominica",
            'DO' => "Dominican Republic",
            'DZ' => "Algeria",
            'EC' => "Ecuador",
            'EE' => "Estonia",
            'EG' => "Egypt",
            'EH' => "Western Sahara",
            'ER' => "Eritrea",
            'ES' => "Spain",
            'ET' => "Ethiopia",
            'FI' => "Finland",
            'FJ' => "Fiji",
            'FK' => "Falkland Islands (Malvinas)",
            'FM' => "Micronesia",
            'FO' => "Faroe Islands",
            'FR' => "France",
            'FX' => "France, Metropolitan",
            'GA' => "Gabon",
            'GB' => "United Kingdom (Great Britain)",
            'GD' => "Grenada",
            'GE' => "Georgia",
            'GF' => "French Guiana",
            'GH' => "Ghana",
            'GI' => "Gibraltar",
            'GL' => "Greenland",
            'GM' => "Gambia",
            'GN' => "Guinea",
            'GP' => "Guadeloupe",
            'GQ' => "Equatorial Guinea",
            'GR' => "Greece",
            'GS' => "South Georgia and the South Sandwich Islands",
            'GT' => "Guatemala",
            'GU' => "Guam",
            'GW' => "Guinea-Bissau",
            'GY' => "Guyana",
            'HK' => "Hong Kong",
            'HM' => "Heard & McDonald Islands",
            'HN' => "Honduras",
            'HR' => "Croatia",
            'HT' => "Haiti",
            'HU' => "Hungary",
            'ID' => "Indonesia",
            'IE' => "Ireland",
            'IL' => "Israel",
            'IN' => "India",
            'IO' => "British Indian Ocean Territory",
            'IQ' => "Iraq",
            'IR' => "Islamic Republic of Iran",
            'IS' => "Iceland",
            'IT' => "Italy",
            'JM' => "Jamaica",
            'JO' => "Jordan",
            'JP' => "Japan",
            'KE' => "Kenya",
            'KG' => "Kyrgyzstan",
            'KH' => "Cambodia",
            'KI' => "Kiribati",
            'KM' => "Comoros",
            'KN' => "St. Kitts and Nevis",
            'KP' => "Korea, Democratic People's Republic of",
            'KR' => "Korea, Republic of",
            'KW' => "Kuwait",
            'KY' => "Cayman Islands",
            'KZ' => "Kazakhstan",
            'LA' => "Lao People's Democratic Republic",
            'LB' => "Lebanon",
            'LC' => "Saint Lucia",
            'LI' => "Liechtenstein",
            'LK' => "Sri Lanka",
            'LR' => "Liberia",
            'LS' => "Lesotho",
            'LT' => "Lithuania",
            'LU' => "Luxembourg",
            'LV' => "Latvia",
            'LY' => "Libyan Arab Jamahiriya",
            'MA' => "Morocco",
            'MC' => "Monaco",
            'MD' => "Moldova, Republic of",
            'MG' => "Madagascar",
            'MH' => "Marshall Islands",
            'ML' => "Mali",
            'MN' => "Mongolia",
            'MM' => "Myanmar",
            'MO' => "Macau",
            'MP' => "Northern Mariana Islands",
            'MQ' => "Martinique",
            'MR' => "Mauritania",
            'MS' => "Monserrat",
            'MT' => "Malta",
            'MU' => "Mauritius",
            'MV' => "Maldives",
            'MW' => "Malawi",
            'MX' => "Mexico",
            'MY' => "Malaysia",
            'MZ' => "Mozambique",
            'NA' => "Namibia",
            'NC' => "New Caledonia",
            'NE' => "Niger",
            'NF' => "Norfolk Island",
            'NG' => "Nigeria",
            'NI' => "Nicaragua",
            'NL' => "Netherlands",
            'NO' => "Norway",
            'NP' => "Nepal",
            'NR' => "Nauru",
            'NU' => "Niue",
            'NZ' => "New Zealand",
            'OM' => "Oman",
            'PA' => "Panama",
            'PE' => "Peru",
            'PF' => "French Polynesia",
            'PG' => "Papua New Guinea",
            'PH' => "Philippines",
            'PK' => "Pakistan",
            'PL' => "Poland",
            'PM' => "St. Pierre & Miquelon",
            'PN' => "Pitcairn",
            'PR' => "Puerto Rico",
            'PT' => "Portugal",
            'PW' => "Palau",
            'PY' => "Paraguay",
            'QA' => "Qatar",
            'RE' => "Réunion",
            'RO' => "Romania",
            'RU' => "Russian Federation",
            'RW' => "Rwanda",
            'SA' => "Saudi Arabia",
            'SB' => "Solomon Islands",
            'SC' => "Seychelles",
            'SD' => "Sudan",
            'SE' => "Sweden",
            'SG' => "Singapore",
            'SH' => "St. Helena",
            'SI' => "Slovenia",
            'SJ' => "Svalbard & Jan Mayen Islands",
            'SK' => "Slovakia",
            'SL' => "Sierra Leone",
            'SM' => "San Marino",
            'SN' => "Senegal",
            'SO' => "Somalia",
            'SR' => "Suriname",
            'ST' => "Sao Tome & Principe",
            'SV' => "El Salvador",
            'SY' => "Syrian Arab Republic",
            'SZ' => "Swaziland",
            'TC' => "Turks & Caicos Islands",
            'TD' => "Chad",
            'TF' => "French Southern Territories",
            'TG' => "Togo",
            'TH' => "Thailand",
            'TJ' => "Tajikistan",
            'TK' => "Tokelau",
            'TM' => "Turkmenistan",
            'TN' => "Tunisia",
            'TO' => "Tonga",
            'TP' => "East Timor",
            'TR' => "Turkey",
            'TT' => "Trinidad & Tobago",
            'TV' => "Tuvalu",
            'TW' => "Taiwan, Province of China",
            'TZ' => "Tanzania, United Republic of",
            'UA' => "Ukraine",
            'UG' => "Uganda",
            'UM' => "United States Minor Outlying Islands",
            'US' => "United States of America",
            'UY' => "Uruguay",
            'UZ' => "Uzbekistan",
            'VA' => "Vatican City State (Holy See)",
            'VC' => "St. Vincent & the Grenadines",
            'VE' => "Venezuela",
            'VG' => "British Virgin Islands",
            'VI' => "United States Virgin Islands",
            'VN' => "Viet Nam",
            'VU' => "Vanuatu",
            'WF' => "Wallis & Futuna Islands",
            'WS' => "Samoa",
            'YE' => "Yemen",
            'YT' => "Mayotte",
            'YU' => "Yugoslavia",
            'ZA' => "South Africa",
            'ZM' => "Zambia",
            'ZR' => "Zaire",
            'ZW' => "Zimbabwe",
        );

        return $ArrayCountryList;
    }

    private function _CheckLicenseStatus() {
        global $LicenseCheck;

        self::$PluginLicenseStatus = OctAutomation_PerformLicenseCheck();
        self::$PluginLicenseKey = $LicenseCheck->get_plugin_license_key();
        self::$PluginLicenseInfo = $LicenseCheck->get_plugin_license_info();
        if (self::$PluginLicenseStatus == false) {
            return false;
        }
        return true;
    }

    private function _ProcessMandrillData($MandrillData) {
        $AvailableWebhooks = array('open', 'click', 'unsub', 'spam', 'hard_bounce');
        if (!in_array($MandrillData->event, $AvailableWebhooks))
            return;

        if (!isset($MandrillData->msg))
            return;
        if (!isset($MandrillData->msg->metadata) || !isset($MandrillData->msg->metadata->AutomationEmailID))
            return;

        list($EncryptedUserID, $EncryptedMessageID, $EncryptedPersonID, $Checksum) = explode('/', $MandrillData->msg->metadata->AutomationEmailID);
        list($UserID, $MessageID, $PersonID) = sl_decrypt_encrypted_parameters(array($EncryptedUserID, $EncryptedMessageID, $EncryptedPersonID), $Checksum, 5);

        self::$UserInformation = Users::RetrieveUser(array('*'), array('UserID' => $UserID), false);
        if (is_bool(self::$UserInformation) == true && self::$UserInformation == false) {
            return false;
        }

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);
        if (is_bool($Message) == true) {
            return false;
        }

        $Person = self::$Models->people->GetPerson(self::$UserInformation, $PersonID, true);
        if (is_bool($Person) == true) {
            return false;
        }

        $Event = $MandrillData->event;
        if ($Event == 'open') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsOpened == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsOpened' => 'Yes',
                'Open_City' => $MandrillData->location == null ? '' : $MandrillData->location->region . ' - ' . $MandrillData->location->city,
                'Open_Country' => $MandrillData->location == null ? '' : $MandrillData->location->country,
                'Open_Date' => date('Y-m-d H:i:s', $MandrillData->ts),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Opened message', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalOpened' => array('TotalOpened + 1'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalOpened' => array('TotalOpened + 1')
            ));
        }
        elseif ($Event == 'click') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsClicked == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsClicked' => 'Yes',
                'Click_Date' => date('Y-m-d H:i:s', $MandrillData->ts),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Clicked link', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalClicked' => array('TotalClicked + 1'),
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalClicked' => array('TotalClicked + 1')
            ));
        }
        elseif ($Event == 'unsub') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsUnsubscribed == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'Unsubscription_Date' => date('Y-m-d H:i:s', $MandrillData->ts),
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Unsubscribed', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalUnsubscribed' => array('TotalUnsubscribed + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s', $MandrillData->ts),
                'UnsubscriptionIP' => $MandrillData->ip,
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalUnsubscribed' => array('TotalUnsubscribed + 1')
            ));
        }
        elseif ($Event == 'spam') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsSpamComplaint == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsSpamComplaint' => 'Yes',
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'SPAM Complained', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalSpamComplaint' => array('TotalSpamComplaint + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s', $MandrillData->ts),
                'UnsubscriptionIP' => $MandrillData->ip,
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalSpamComplaint' => array('TotalSpamComplaint + 1')
            ));
        }
        elseif ($Event == 'hard_bounce') {
            $MessageStatus = self::$Models->people->GetMessageSentLogForPerson(self::$UserInformation, $Message->MessageID, $Person->PersonID);

            if (is_bool($MessageStatus) == true)
                return;
            if ($MessageStatus->IsHardBounced == 'Yes')
                return;

            self::$Models->people->UpdatePersonDeliveryLog(self::$UserInformation, $Message->MessageID, $Person->PersonID, array(
                'IsHardBounced' => 'Yes',
            ));

            self::$Models->people->RegisterActivityToPerson(self::$UserInformation, $Person->PersonID, 'Hard bounced', $Message->MessageID);
            self::$Models->messages->UpdateMessage(self::$UserInformation, $Message->MessageID, array(
                'TotalHardBounced' => array('TotalHardBounced + 1'),
            ));

            self::$Models->people->UpdatePerson(self::$UserInformation, $Person->PersonID, array(
                'IsUnsubscribed' => 'Yes',
                'IsBounced' => 'Yes',
                'UnsubscriptionDate' => date('Y-m-d H:i:s', $MandrillData->ts),
                'UnsubscriptionIP' => $MandrillData->ip,
            ));

            self::$Models->messages->RegisterMessageLog(self::$UserInformation, $MessageID->MessageID, array(
                'TotalHardBounced' => array('TotalHardBounced + 1')
            ));
        }
    }

    /*
     * Eman
     */

    public function ui_reports($MessageID, $Statistic, $Date = null) {
        if (self::_PluginAppHeader('User') == false)
            return;

        self::$ObjectCI->load->helper('url');

        $PeopleCount = self::$Models->people->GetPeopleCount(self::$UserInformation['UserID']);

        if ($PeopleCount == 0) {
            redirect(InterfaceAppURL(true) . "/octautomation/email_automation", 'redirect');
            return;
        }

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);

        if (is_bool($Message) == true && $Message == false) {
            redirect(InterfaceAppURL(true) . "/octautomation/messages/", 'redirect');
            return;
        }
//        if ($Statistic == 'opens') {
//            $view = 'message_report_opens';
//        } elseif ($Statistic == 'clicks') {
//            $view = 'message_report_clicks';
//        } elseif ($Statistic == 'unsubscriptions') {
//            $view = 'message_report_unsubscriptions';
//        }

        $ArrayDataRows = null;
        $ResultRows = array();
        if ($Statistic == 'opens') {
            $ArrayDataRows = self::$Models->messages->GetOpenLog_Enhanced(self::$UserInformation, $Message->MessageID, $Date);
//            $keys = array("Email", "Open_Date", "Total_Opens", "City", "Country");
            $keys = array("Email", "Open_Date", "City", "Country");
        } elseif ($Statistic == 'clicks') {
            $ArrayDataRows = self::$Models->messages->GetClickLog_Enhanced(self::$UserInformation, $Message->MessageID, $Date);
//            $keys = array("Email", "Click_Date", "Total_Clicks");
            $keys = array("Email", "Click_Date");
        } elseif ($Statistic == 'unsubscriptions') {
            $ArrayDataRows = self::$Models->messages->GetUnsubscriptionLog_Enhanced(self::$UserInformation, $Message->MessageID, $Date);
//            $keys = array("Email", "Unsubscription_Date", "Total_Unsubscriptions");
            $keys = array("Email", "Unsubscription_Date");
        }

        foreach ($ArrayDataRows as $Row) {

            $ResultRows[] = array(
                "LogID" => $Row->LogID,
                "RelMessageID" => $Row->RelMessageID,
                "RelPersonID" => $Row->RelPersonID,
                "RelUserID" => $Row->RelUserID,
                "Email" => $Row->Email,
                "Total_Opens" => isset($Row->Total_Opens) ? $Row->Total_Opens : 0,
                "Total_Clicks" => isset($Row->Total_Clicks) ? $Row->Total_Clicks : 0,
                "Total_Unsubscriptions" => isset($Row->Total_Unsubscriptions) ? $Row->Total_Unsubscriptions : 0,
                "Open_Date" => date('Y-m-d', strtotime($Row->Open_Date)) ,
                "Click_Date" => date('Y-m-d', strtotime($Row->Click_Date)),
                "Unsubscription_Date" => date('Y-m-d', strtotime($Row->Unsubscription_Date)),
                "City" => $Row->Open_City,
                "Country" => isset($Row->Open_Country) ? self::_GetCountryList()[strtoupper($Row->Open_Country)] : '',
                "SentAt" => $Row->SentAt,
                "IsOpened" => $Row->IsOpened,
                "IsClicked" => $Row->IsClicked,
                "IsUnsubscribed" => $Row->IsUnsubscribed,
                "IsHardBounced" => $Row->IsHardBounced,
                "IsSpamComplaint" => $Row->IsSpamComplaint,
            );
        }

        if ($Statistic == 'opens') {
            $StatisticViewData = array(
                'Section' => self::$ArrayLanguage['Screen']['0222'],
                'Title' => self::$ArrayLanguage['Screen']['0222'],
                'PageTitle' => self::$ArrayLanguage['Screen']['0222'] . self::$ArrayLanguage['Screen']['0220'],
            );
        } elseif ($Statistic == 'clicks') {
            $StatisticViewData = array(
                'Section' => self::$ArrayLanguage['Screen']['0223'],
                'Title' => self::$ArrayLanguage['Screen']['0223'],
                'PageTitle' => self::$ArrayLanguage['Screen']['0223'] . self::$ArrayLanguage['Screen']['0220'],
            );
        } elseif ($Statistic == 'unsubscriptions') {
            $StatisticViewData = array(
                'Section' => self::$ArrayLanguage['Screen']['0224'],
                'Title' => self::$ArrayLanguage['Screen']['0224'],
                'PageTitle' => self::$ArrayLanguage['Screen']['0224'] . self::$ArrayLanguage['Screen']['0220'],
            );
        }

        $ArrayViewData = array(
            'PluginLanguage' => self::$ArrayLanguage,
            'UserInformation' => self::$UserInformation,
            'CurrentMenuItem' => 'Email Automation',
            'Keys' => $keys,
            'Message' => $Message,
            'ResultRows' => $ResultRows,
        );

        $ArrayViewData = array_merge($ArrayViewData, $StatisticViewData);
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
//        print_r('<pre>');
//        print_r($ArrayDataRows);
//        print_r('</pre>');
//        exit();
        self::$ObjectCI->plugin_render('octautomation', "message_report_details", $ArrayViewData, true);
    }

    /*
     * Eman
     */

    public function ui_message_statistics($type) {

        if (self::_PluginAppHeader('User') == false) {
            return;
        }

        header('Content-Type: application/json');

        $MessageID = $_POST['message_id'];

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);
        if (is_bool($Message) == true && $Message == false) {
            return;
        }

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
        $StartFromDate = date('Y-m-d', strtotime($Message->CreatedAt));
        $ToDate = date('Y-m-d', strtotime(SentAt . ' +' . ($LastXDays - 1) . ' days'));

//        $Message_DeliveryTimeFrame = self::$Models->messages->GetTimeFrame(self::$UserInformation, $MessageID, $Days);
        $Message_DeliveryTimeFrame = self::$Models->messages->GetTimeFrameCustom(self::$UserInformation, $MessageID, $StartFromDate, $ToDate);
        $output = array();

        for ($DayCounter = 0; $DayCounter < $Days; $DayCounter++) {
//            $Date = date("jS M'y", time() - (86400 * $DayCounter));
            $Date2 = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartFromDate)) . ' +' . $DayCounter . ' days 00:00:00'));

            if ($type == 'opens') {
                $Value = (isset($Message_DeliveryTimeFrame[$Date2]['TotalOpened']) == true && $Message_DeliveryTimeFrame[$Date2]['TotalOpened'] > 0 ? $Message_DeliveryTimeFrame[$Date2]['TotalOpened'] : 0);
            } else if ($type == 'clicks') {
                $Value = (isset($Message_DeliveryTimeFrame[$Date2]['TotalClicked']) == true && $Message_DeliveryTimeFrame[$Date2]['TotalClicked'] > 0 ? $Message_DeliveryTimeFrame[$Date2]['TotalClicked'] : 0);
            } else if ($type == 'unsubscriptions') {
                $Value = (isset($Message_DeliveryTimeFrame[$Date2]['TotalUnsubscribed']) == true && $Message_DeliveryTimeFrame[$Date2]['TotalUnsubscribed'] > 0 ? $Message_DeliveryTimeFrame[$Date2]['TotalUnsubscribed'] : 0);
            }

            $new_row['day'] = ($DayCounter + 1);
            $new_row['Total'] = $Value;
            $new_row['date'] = $Date2;

            $output[] = $new_row;
        }
        print_r((json_encode($output)));
        return;
    }

    /*
     * Eman
     */

    public function ui_message_geo_statistics($type) {

        if (self::_PluginAppHeader('User') == false) {
            return;
        }

        header('Content-Type: application/json');

        //get message
        $MessageID = $_POST['message_id'];

        $Message = self::$Models->messages->GetMessage(self::$UserInformation, $MessageID);

        if (is_bool($Message) == true && $Message == false) {
            return;
        }

        //get period
        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }
        try {
            if ($type == 'opens') {
                $GeoData = self::$Models->messages->GetMessageOpenGeo(self::$UserInformation['UserID'], $Message->MessageID, $StartDate, $CurrentDate);
            } else if ($type == 'clicks') {
                $GeoData = self::$Models->messages->GetMessageClickGeo(self::$UserInformation['UserID'], $Message->MessageID, $StartDate, $CurrentDate);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            return;
        }

        $data = array();
        $countries = array();
        foreach ($GeoData as $Row) {
            $data[$Row['Country']] = $Row['Count'];
//            $countries[$Row['Country']] = $Row['Country_Name'];
            $countries[$Row['Country']] = self::_GetCountryList()[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

}
