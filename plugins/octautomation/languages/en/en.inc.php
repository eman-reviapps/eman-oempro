<?php
/**
 * @author Cem Hurturk
 * @copyright Octeth.com
 **/

/**
 * Main language configuration file for the plug-in
 **/

// Important: Do not change this line! - Start
$ArrayPlugInLanguageStrings									= array();
// Important: Do not change this line! - End

$ArrayPlugInLanguageStrings['Screen']['0001']	= 'Email Automation';
$ArrayPlugInLanguageStrings['Screen']['0002']	= 'Email Automation Settings';
$ArrayPlugInLanguageStrings['Screen']['0003']	= 'Create Message';
$ArrayPlugInLanguageStrings['Screen']['0004']	= 'Getting Started';
$ArrayPlugInLanguageStrings['Screen']['0005']	= 'People';
$ArrayPlugInLanguageStrings['Screen']['0006']	= 'Messages';
$ArrayPlugInLanguageStrings['Screen']['0007']	= 'Setup Instructions';
$ArrayPlugInLanguageStrings['Screen']['0008']	= 'API Reference &amp; Help';
$ArrayPlugInLanguageStrings['Screen']['0009']	= 'Installation of email automation is as easy as installing Google Analytics to your website';
$ArrayPlugInLanguageStrings['Screen']['0010']	= 'Within a few minutes, you will be tracking user activities on your website, application or ecommerce website and start sending them targeted, relevant emails, just on time.';
$ArrayPlugInLanguageStrings['Screen']['0011']	= 'Simply copy the JavaScript displayed below and add it to the footer of every page on your website where user information exists. You can pass unlimited amount of user data to the system with the JavaScript below.';
$ArrayPlugInLanguageStrings['Screen']['0012']	= 'Click on the above textarea to select javascript code snippet';
$ArrayPlugInLanguageStrings['Screen']['0013']	= 'Step 1: Copy JavaScript code and place it to your website pages';
$ArrayPlugInLanguageStrings['Screen']['0014']	= 'This JavaScript code should be added to all website pages where you have the user data of the visitor (such as pages after visitor logged in). It is the same procedure as adding Google Analycs JavaScript code.';
$ArrayPlugInLanguageStrings['Screen']['0015']	= 'Step 2: Customize the JavaScript code';
$ArrayPlugInLanguageStrings['Screen']['0016']	= 'You need to define some required fields in the JavaScript code:';
$ArrayPlugInLanguageStrings['Screen']['0017']	= 'Step 3: Add JavaScript to your website page';
$ArrayPlugInLanguageStrings['Screen']['0018']	= 'Okay, your JavaScript code is now ready. Simply add it to your every website page before the <strong>&lt;/body&gt;</strong> tag.';
$ArrayPlugInLanguageStrings['Screen']['0019']	= 'Set the value of this parameter to the ID of the user in your app';
$ArrayPlugInLanguageStrings['Screen']['0020']	= 'In order to prevent abuse, you need to create a hash which matches the user_id value. For this, here is an example algorithm written in PHP:';
$ArrayPlugInLanguageStrings['Screen']['0021']	= 'Set the value of this parameter to the email address of the user';
$ArrayPlugInLanguageStrings['Screen']['0022']	= 'The best part of Sendloop Engage is handling unlimited amount of user data. In this way, you can send very targeted messages to the right user on the right time. For this, you simply need to add any extra information inside extra_info section in the JavaScript code. Here is an example:';
$ArrayPlugInLanguageStrings['Screen']['0023']	= 'Sendloop Engage supports three different data types: text, number and date. To set the right data type, follow these rules:';
$ArrayPlugInLanguageStrings['Screen']['0024']	= 'Enclose with quotes. Here is an example:';
$ArrayPlugInLanguageStrings['Screen']['0025']	= 'Do not enclose with quotes and set a number only';
$ArrayPlugInLanguageStrings['Screen']['0026']	= 'Add \'_date\' suffix to the field name and set the date in YYYY-MM-DD HH:MM:SS format. Here is an example:';
$ArrayPlugInLanguageStrings['Screen']['0027']	= 'Waiting for the first data to arrive in...';
$ArrayPlugInLanguageStrings['Screen']['0028']	= 'We have received the first person data! Congratulations! <a href="#" class="alert-link js-data-received-continue">Click here to continue</a>...';
$ArrayPlugInLanguageStrings['Screen']['0029']	= 'People';
$ArrayPlugInLanguageStrings['Screen']['0030']	= array(
	'Identifier' => 'User ID',
	'Email' => 'Email Address',
	'EntryDate' => 'Entry Date',
	'LastSeenDate' => 'Last Seen Date',
	'IPAddress' => 'IP Address',
	'City' => 'City',
	'Country' => 'Country',
	'SessionCounter' => 'Sessions'
);
$ArrayPlugInLanguageStrings['Screen']['0031']	= 'Please select at least one column to show on the list';
$ArrayPlugInLanguageStrings['Screen']['0032']	= 'Selected people have been deleted';
$ArrayPlugInLanguageStrings['Screen']['0033']	= 'There is nothing to delete. Please select at least one person to delete.';
$ArrayPlugInLanguageStrings['Screen']['0034']	= 'Person Detail';
$ArrayPlugInLanguageStrings['Screen']['0035']	= 'Return Back';
$ArrayPlugInLanguageStrings['Screen']['0036']	= 'Person Profile';
$ArrayPlugInLanguageStrings['Screen']['0037']	= 'Activity';
$ArrayPlugInLanguageStrings['Screen']['0038']	= 'Email Address:';
$ArrayPlugInLanguageStrings['Screen']['0039']	= 'Entry Date:';
$ArrayPlugInLanguageStrings['Screen']['0040']	= 'User ID:';
$ArrayPlugInLanguageStrings['Screen']['0041']	= 'Last Seen Date:';
$ArrayPlugInLanguageStrings['Screen']['0042']	= 'IP Address:';
$ArrayPlugInLanguageStrings['Screen']['0043']	= 'City / Country:';
$ArrayPlugInLanguageStrings['Screen']['0044']	= 'Sessions:';
$ArrayPlugInLanguageStrings['Screen']['0045']	= 'Are you sure? This person will be unsubscribed from your all future mailings';
$ArrayPlugInLanguageStrings['Screen']['0046']	= 'Are you sure? This person will be deleted from your account';
$ArrayPlugInLanguageStrings['Screen']['0047']	= 'No activity has been detected for this person yet...';
$ArrayPlugInLanguageStrings['Screen']['0048']	= 'No activity';
$ArrayPlugInLanguageStrings['Screen']['0049']	= 'Messages';
$ArrayPlugInLanguageStrings['Screen']['0050']	= 'Delete selected';
$ArrayPlugInLanguageStrings['Screen']['0051']	= 'You haven\'t created any email so far...<br><a href="#" class="js-redirect-create-message">Click here to create your first smart email...</a>';
$ArrayPlugInLanguageStrings['Screen']['0052']	= "jS M'y, g:ia";
$ArrayPlugInLanguageStrings['Screen']['0053']	= 'Revert to draft';
$ArrayPlugInLanguageStrings['Screen']['0054']	= 'Create Message';
$ArrayPlugInLanguageStrings['Screen']['0055']	= 'Activate Message';
$ArrayPlugInLanguageStrings['Screen']['0056']	= 'Save As Draft';
$ArrayPlugInLanguageStrings['Screen']['0057']	= 'Please fill in all required fields';
$ArrayPlugInLanguageStrings['Screen']['0058']	= 'You need to set an email content, in HTML, in plain text or in both types... An empty email can not be set.';
$ArrayPlugInLanguageStrings['Screen']['0059']	= 'There is a problem with the information you have entered. See below for error messages.';
$ArrayPlugInLanguageStrings['Screen']['0060']	= 'Message Name';
$ArrayPlugInLanguageStrings['Screen']['0061']	= 'From Name';
$ArrayPlugInLanguageStrings['Screen']['0062']	= 'From Email';
$ArrayPlugInLanguageStrings['Screen']['0063']	= 'Reply-To Name';
$ArrayPlugInLanguageStrings['Screen']['0064']	= 'Reply-To Email';
$ArrayPlugInLanguageStrings['Screen']['0065']	= 'Subject';
$ArrayPlugInLanguageStrings['Screen']['0066']	= 'HTML Content';
$ArrayPlugInLanguageStrings['Screen']['0067']	= 'Plain Content';
$ArrayPlugInLanguageStrings['Screen']['0068']	= 'Rules';
$ArrayPlugInLanguageStrings['Screen']['0069']	= 'Segment Operator';
$ArrayPlugInLanguageStrings['Screen']['0070']	= 'Plain Content';
$ArrayPlugInLanguageStrings['Screen']['0071']	= 'HTML email content is empty';
$ArrayPlugInLanguageStrings['Screen']['0072']	= 'Plain text email content is empty';
$ArrayPlugInLanguageStrings['Screen']['0073']	= 'The HTML content of your email has invalid image urls';
$ArrayPlugInLanguageStrings['Screen']['0074']	= 'The HTML content of your email campaign contains script, flash or java';
$ArrayPlugInLanguageStrings['Screen']['0075']	= array(
	'Draft' => 'Draft',
	'Active' => 'Active',
	'Paused' => 'Paused',
	'Deleted' => 'Deleted'
);
$ArrayPlugInLanguageStrings['Screen']['0076']	= 'Recipients opened';
$ArrayPlugInLanguageStrings['Screen']['0077']	= 'Emails sent';
$ArrayPlugInLanguageStrings['Screen']['0078']	= 'Message Statistics';
$ArrayPlugInLanguageStrings['Screen']['0079']	= 'Emails have been sent';
$ArrayPlugInLanguageStrings['Screen']['0080']	= 'People opened your email';
$ArrayPlugInLanguageStrings['Screen']['0081']	= 'Open rate is';
$ArrayPlugInLanguageStrings['Screen']['0082']	= 'People clicked link in the email';
$ArrayPlugInLanguageStrings['Screen']['0083']	= 'Link click rate is';
$ArrayPlugInLanguageStrings['Screen']['0084']	= 'People unsubscribed from your email';
$ArrayPlugInLanguageStrings['Screen']['0085']	= 'Unsubscription rate is';
$ArrayPlugInLanguageStrings['Screen']['0086']	= 'Invalid email addresses detected';
$ArrayPlugInLanguageStrings['Screen']['0087']	= 'Invalid email address detection rate is';
$ArrayPlugInLanguageStrings['Screen']['0088']	= 'Spam complaints filed by people';
$ArrayPlugInLanguageStrings['Screen']['0089']	= 'Spam complaint rate is';
$ArrayPlugInLanguageStrings['Screen']['0090']	= 'Status';
$ArrayPlugInLanguageStrings['Screen']['0091']	= 'From';
$ArrayPlugInLanguageStrings['Screen']['0092']	= 'Subject';
$ArrayPlugInLanguageStrings['Screen']['0093']	= 'Locations';
$ArrayPlugInLanguageStrings['Screen']['0094']	= 'Email Opens';
$ArrayPlugInLanguageStrings['Screen']['0095']	= 'Link Clicks';
$ArrayPlugInLanguageStrings['Screen']['0096']	= 'Unsubscriptions';
$ArrayPlugInLanguageStrings['Screen']['0097']	= 'opens';
$ArrayPlugInLanguageStrings['Screen']['0098']	= 'No email open location has been detected so far...';
$ArrayPlugInLanguageStrings['Screen']['0099']	= 'opened at';
$ArrayPlugInLanguageStrings['Screen']['0100']	= 'No email open detections so far...';
$ArrayPlugInLanguageStrings['Screen']['0101']	= 'clicked at';
$ArrayPlugInLanguageStrings['Screen']['0102']	= 'No link click detections so far...';
$ArrayPlugInLanguageStrings['Screen']['0103']	= 'unsubscribed at';
$ArrayPlugInLanguageStrings['Screen']['0104']	= 'No unsubscriptions so far... Yay!';
$ArrayPlugInLanguageStrings['Screen']['0105']	= 'Edit Message';
$ArrayPlugInLanguageStrings['Screen']['0106']	= 'Delete';
$ArrayPlugInLanguageStrings['Screen']['0107']	= 'Save Changes';
$ArrayPlugInLanguageStrings['Screen']['0108']	= 'Pause Message';
$ArrayPlugInLanguageStrings['Screen']['0109']	= 'Edit Message';
$ArrayPlugInLanguageStrings['Screen']['0110']	= 'Email address is invalid';
$ArrayPlugInLanguageStrings['Screen']['0111']	= 'Preview email has been sent to the email address';
$ArrayPlugInLanguageStrings['Screen']['0112']	= '[PREVIEW]';
$ArrayPlugInLanguageStrings['Screen']['0113']	= 'Email delivery failed :(';
$ArrayPlugInLanguageStrings['Screen']['0114']	= 'Email Automation Reports';
$ArrayPlugInLanguageStrings['Screen']['0115']	= 'Settings';
$ArrayPlugInLanguageStrings['Screen']['0116']	= 'Overview';
$ArrayPlugInLanguageStrings['Screen']['0117']	= 'User Activities';
$ArrayPlugInLanguageStrings['Screen']['0118']	= 'Message Delivery';
$ArrayPlugInLanguageStrings['Screen']['0119']	= 'User Limits';
$ArrayPlugInLanguageStrings['Screen']['0120']	= 'License';
$ArrayPlugInLanguageStrings['Screen']['0121']	= 'Support';
$ArrayPlugInLanguageStrings['Screen']['0122']	= 'End User License Agreement';
$ArrayPlugInLanguageStrings['Screen']['0123']	= 'By using this plugin, you agree our End User License Agreement and Terms of Use which can be read on our website at <a href="http://octeth.com/terms/">http://octeth.com/terms/</a>';
$ArrayPlugInLanguageStrings['Screen']['0124']	= '&copy;Copyright Octeth, all rights reserved.';
$ArrayPlugInLanguageStrings['Screen']['0125']	= 'License Key: ';
$ArrayPlugInLanguageStrings['Screen']['0126']	= 'Customer Service and Technical Support';
$ArrayPlugInLanguageStrings['Screen']['0127']	= 'For billing and customer service, please contact <a href="mailto:hello@octeth.com">hello@octeth.com</a>';
$ArrayPlugInLanguageStrings['Screen']['0128']	= 'For technical support and assistance requests, please contact <a href="mailto:support@octeth.com">support@octeth.com</a>';
$ArrayPlugInLanguageStrings['Screen']['0129']	= 'Plugin documentation and help articles can be found on our <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">help portal</a>';
$ArrayPlugInLanguageStrings['Screen']['0130']	= 'Invalid plugin license key. Access to the plugin disabled.<br>%s<br><br>Plugin License Key:<br></strong>%s</strong><br><br><a href="#" onclick="window.history.go(-1); return false;">Click here to go back</a>';
$ArrayPlugInLanguageStrings['Screen']['0131']	= 'Billing';
$ArrayPlugInLanguageStrings['Screen']['0132']	= 'Dashboard';
$ArrayPlugInLanguageStrings['Screen']['0133']	= 'Please enter your Mailgun integration settings';
$ArrayPlugInLanguageStrings['Screen']['0134']	= 'In order to let your users send automated emails, you need to enter your Mailgun credentials below. If you don\'t have a Mailgun account, visit <a href="http://mailgun.com/" target="_blank">their website</a> to get yours now.';
$ArrayPlugInLanguageStrings['Screen']['0135']	= 'Mailgun API Key';
$ArrayPlugInLanguageStrings['Screen']['0136']	= 'Your Mailgun API key will be displayed to you right after you login to your <a href="http://mailgun.com" target="_blank">Mailgun account</a>';
$ArrayPlugInLanguageStrings['Screen']['0137']	= 'Custom Domain';
$ArrayPlugInLanguageStrings['Screen']['0138']	= 'Enter the domain name you have authorized in your Mailgun account.';
$ArrayPlugInLanguageStrings['Screen']['0139']	= 'Test Mailgun Connection';
$ArrayPlugInLanguageStrings['Screen']['0140']	= 'Hit the button below to try connecting to your Mailgun account and send a test email to %s';
$ArrayPlugInLanguageStrings['Screen']['0141']	= 'Test Mailgun Credentials';
$ArrayPlugInLanguageStrings['Screen']['0142']	= 'Testing Mailgun credentials';
$ArrayPlugInLanguageStrings['Screen']['0143']	= '<p>This email is the result of Mailgun credentials test. If you are reading this email, it means that test was successful. You can delete this email.</p>';
$ArrayPlugInLanguageStrings['Screen']['0144']	= 'Mailgun credentials failed. Mailgun rejected the test email. Please check your Mailgun API key and custom domain below.';
$ArrayPlugInLanguageStrings['Screen']['0145']	= 'The Mailgun API key might be wrong';
$ArrayPlugInLanguageStrings['Screen']['0146']	= 'The Mailgun custom domain might be wrong';
$ArrayPlugInLanguageStrings['Screen']['0147']	= 'Settings have been saved';
$ArrayPlugInLanguageStrings['Screen']['0148']	= 'For more details about Mailgun integration such as enabling email open reporting, link click detections, geo-location detections, spam complaints, hard bounce and optout tracking, take a look at our <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">help article</a>.';
$ArrayPlugInLanguageStrings['Screen']['0149']	= 'Update User Limits';
$ArrayPlugInLanguageStrings['Screen']['0150']	= 'Set limits for each user';
$ArrayPlugInLanguageStrings['Screen']['0151']	= 'You can setup usage limits for each user below. To authorize user to send unlimited emails, simply set -1 for that user.';
$ArrayPlugInLanguageStrings['Screen']['0152']	= 'No active user exists on the system.<br>First <a href="%s">create a user</a> and then check here to set usage limits for the user.';
$ArrayPlugInLanguageStrings['Screen']['0153']	= 'Name';
$ArrayPlugInLanguageStrings['Screen']['0154']	= array(
	'Trusted' => 'Trusted user',
	'Untrusted' => 'Untrusted user'
);
$ArrayPlugInLanguageStrings['Screen']['0155']	= 'Email Automation Delivery Limit';
$ArrayPlugInLanguageStrings['Screen']['0156']	= 'Set the maximum number of emails can be sent through email automation plugin in a single month. To set unlimited email delivery, type "-1" above.';
$ArrayPlugInLanguageStrings['Screen']['0157']	= 'Messages sent today';
$ArrayPlugInLanguageStrings['Screen']['0158']	= 'Messages sent yesterday';
$ArrayPlugInLanguageStrings['Screen']['0159']	= 'Messages sent this month';
$ArrayPlugInLanguageStrings['Screen']['0160']	= 'Messages sent last month';
$ArrayPlugInLanguageStrings['Screen']['0161']	= 'Messages sent this year';
$ArrayPlugInLanguageStrings['Screen']['0162']	= 'Messages sent last year';
$ArrayPlugInLanguageStrings['Screen']['0163']	= 'Top Senders This Month';
$ArrayPlugInLanguageStrings['Screen']['0164']	= 'No data to list yet. Check back later...';
$ArrayPlugInLanguageStrings['Screen']['0165']	= 'User';
$ArrayPlugInLanguageStrings['Screen']['0166']	= 'Messages Sent';
$ArrayPlugInLanguageStrings['Screen']['0167']	= 'Messages Opened';
$ArrayPlugInLanguageStrings['Screen']['0168']	= 'Reference License Key: ';
$ArrayPlugInLanguageStrings['Screen']['0169']	= 'Purchase License';
$ArrayPlugInLanguageStrings['Screen']['0170']	= 'Unlock your plugin and use it without any limits';
$ArrayPlugInLanguageStrings['Screen']['0171']	= 'You are currently using a trial license for this plugin which is going to expire on %s. You can purchase a plugin license and unlock this plugin.';
$ArrayPlugInLanguageStrings['Screen']['0172']	= "jS M'y";
$ArrayPlugInLanguageStrings['Screen']['0173']	= 'Trial expires at:';
$ArrayPlugInLanguageStrings['Screen']['0174']	= 'Purchase Now';
$ArrayPlugInLanguageStrings['Screen']['0175']	= 'Your license has expired.';
$ArrayPlugInLanguageStrings['Screen']['0176']	= 'Not sent yet';
$ArrayPlugInLanguageStrings['Screen']['0177']	= 'Last sent at %s';
$ArrayPlugInLanguageStrings['Screen']['0178']	= 'Mailgun Webhook URL';
$ArrayPlugInLanguageStrings['Screen']['0179']	= 'This is the URL which should be set in your Mailgun webhook settings. In this way, email opens, link clicks, spam complaints and many other events can be tracked. For more information, please visit <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">our help article</a>';
$ArrayPlugInLanguageStrings['Screen']['0180']	= 'Delivery Method';
$ArrayPlugInLanguageStrings['Screen']['0181']	= 'Please select the email delivery method for your email automation users';
$ArrayPlugInLanguageStrings['Screen']['0182']	= 'Save to directory';
$ArrayPlugInLanguageStrings['Screen']['0183']	= 'Remote Mail Servers';
$ArrayPlugInLanguageStrings['Screen']['0184']	= '3rd Party Mail Servers';
$ArrayPlugInLanguageStrings['Screen']['0185']	= 'Local Mail Servers';
$ArrayPlugInLanguageStrings['Screen']['0186']	= 'Please fill in all the required fields below';
$ArrayPlugInLanguageStrings['Screen']['0187']	= 'Please enter your Mandrill integration settings';
$ArrayPlugInLanguageStrings['Screen']['0188']	= 'In order to let your users send automated emails, you need to enter your Mandrill credentials below. If you don\'t have a Mandrill account, visit <a href="http://mandrill.com/" target="_blank">their website</a> to get yours now.';
$ArrayPlugInLanguageStrings['Screen']['0189']	= 'Mandrill Webhook URL';
$ArrayPlugInLanguageStrings['Screen']['0190']	= 'This is the URL which should be set in your Mandrill webhook settings. In this way, email opens, link clicks, spam complaints and many other events can be tracked. For more information, please visit <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">our help article</a>';
$ArrayPlugInLanguageStrings['Screen']['0191']	= 'Username';
$ArrayPlugInLanguageStrings['Screen']['0192']	= 'You can find your Mandrill SMTP username and password on <a href="https://mandrillapp.com/settings/index" target="_blank">Mandrill SMTP &amp; API Credentials</a> page';
$ArrayPlugInLanguageStrings['Screen']['0193']	= 'Password';
$ArrayPlugInLanguageStrings['Screen']['0194']	= 'For more details about Mandrill integration such as enabling email open reporting, link click detections, geo-location detections, spam complaints, hard bounce and optout tracking, take a look at our <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">help article</a>.';
$ArrayPlugInLanguageStrings['Screen']['0195']	= 'Please enter your "Save to directory" settings';
$ArrayPlugInLanguageStrings['Screen']['0196']	= 'Target Directory';
$ArrayPlugInLanguageStrings['Screen']['0197']	= 'Enter the path of the target directory where emails will be saved.<br>Ex: /var/www/vhosts/domain.com/httpdocs/mail_queue';
$ArrayPlugInLanguageStrings['Screen']['0198']	= 'For more details about "Save to directory" delivery method, take a look at our <a href="http://octeth.com/help/email-automation-plugin/" target="_blank">help article</a>.';
$ArrayPlugInLanguageStrings['Screen']['0199']	= 'SMTP connection';
$ArrayPlugInLanguageStrings['Screen']['0200']	= 'Please enter SMTP connection settings';
$ArrayPlugInLanguageStrings['Screen']['0201']	= 'PowerMTA';
$ArrayPlugInLanguageStrings['Screen']['0202']	= 'Please enter PowerMTA settings';
$ArrayPlugInLanguageStrings['Screen']['0203']	= 'The Mandrill username might be wrong';
$ArrayPlugInLanguageStrings['Screen']['0204']	= 'The Mandrill password might be wrong';
$ArrayPlugInLanguageStrings['Screen']['0205']	= 'Mandrill credentials failed. Mandrill rejected the test email. Please check your Mandrill settings below.';
$ArrayPlugInLanguageStrings['Screen']['0206']	= 'Target directory';
$ArrayPlugInLanguageStrings['Screen']['0207']	= 'Target directory is either not accessible or does not exist';
$ArrayPlugInLanguageStrings['Screen']['0208']	= 'Settings couldn\'t be saved. Please check your delivery settings below.';
$ArrayPlugInLanguageStrings['Screen']['0209']	= 'Virtual MTA name';
$ArrayPlugInLanguageStrings['Screen']['0210']	= 'Pick-up directory';
$ArrayPlugInLanguageStrings['Screen']['0211']	= 'Pick-up directory is either not accessible or does not exist';
$ArrayPlugInLanguageStrings['Screen']['0212']	= 'Host address';
$ArrayPlugInLanguageStrings['Screen']['0213']	= 'Port';
$ArrayPlugInLanguageStrings['Screen']['0214']	= 'SMTP.com username';
$ArrayPlugInLanguageStrings['Screen']['0215']	= 'SMTP.com password';
$ArrayPlugInLanguageStrings['Screen']['0216']	= 'SMTP.com credentials failed.Please check your SMTP.com settings below.';
$ArrayPlugInLanguageStrings['Screen']['0217']	= 'Please enter SMTP.com settings';
$ArrayPlugInLanguageStrings['Screen']['0218']	= 'Email automation test email';
$ArrayPlugInLanguageStrings['Screen']['0219']	= 'Person has been deleted';

$ArrayPlugInLanguageStrings['Screen']['0220']	= 'Details';
$ArrayPlugInLanguageStrings['Screen']['0221']	= 'No records found.';

$ArrayPlugInLanguageStrings['Screen']['0222']	= 'Opens';
$ArrayPlugInLanguageStrings['Screen']['0223']	= 'Clicks';
$ArrayPlugInLanguageStrings['Screen']['0224']	= 'Unsubscriptions';
?>