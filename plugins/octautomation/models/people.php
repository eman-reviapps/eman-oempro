<?php

class model_people extends model_base {

    private $PeopleSystemColumns = array('PersonID', 'Identifier', 'IsUnsubscribed', 'IsBounced', 'UnsubscriptionDate', 'UnsubscriptionIP', 'Email', 'EntryDate', 'LastSeenDate', 'IPAddress', 'City', 'Country', 'SessionCounter', 'id', 'hash', 'email');

    public function __construct() {
        
    }

    public function GetTableColumns($User, $TableName) {
        $SQLQuery = "SHOW COLUMNS IN " . $TableName;
        $Result = $this->query_result($SQLQuery);

        $TableColumns = array();
        if (count($Result) > 0) {
            foreach ($Result as $Index => $EachColumn) {
                $TableColumns[] = $EachColumn->Field;
            }
        }

        return $TableColumns;
    }

    public function GetPeopleDataStructure($User) {
        $Fields = $this->field_data('oempro_octautomation_people_' . $User['UserID']);

        $TableStructure = array();

        foreach ($Fields as $Index => $EachField) {
            $DataType = 'text';
            if ($EachField->type == 'datetime') {
                $DataType = 'date';
            } elseif ($EachField->type == 'int' || $EachField->type == 'real') {
                $DataType = 'number';
            } elseif ($EachField->type == 'string') {
                $DataType = 'text';
            } else {
                $DataType = 'text';
            }
            $TableStructure[$EachField->name] = $DataType;
        }

        return $TableStructure;
    }

    public function GetPeopleMetaData($User) {
        $SQLQuery = "SELECT * FROM oempro_octautomation_people_meta_" . $User['UserID'] . " WHERE EntryID=1";
        $Result = $this->query_result($SQLQuery);

        if (is_bool($Result) == true && $Result == false) {
            return false;
        } else {
            $MetaData = $Result;
            $MetaData = $MetaData[0];
            return $MetaData;
        }
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function GenerateFilterSQLQueryPart($User, $Rules, $MatchType) {
        $AvailableOperators = array('is', 'is not', 'starts with', 'ends with', 'contains', 'more than', 'less than', 'exactly', 'after', 'on', 'before', 'greater than', 'less than');

        $TableStructure = $this->GetPeopleDataStructure($User);

        if ($MatchType != 'and')
            $MatchType = 'or';

        $SQLFilter = array();

        if (count($Rules) > 0) {
            foreach ($Rules as $Index => $EachRule) {
                if (isset($TableStructure[$EachRule['column']]) == false)
                    continue;
                if (in_array($EachRule['operator'], $AvailableOperators) == false)
                    $EachRule['operator'] = 'is';

                if ($TableStructure[$EachRule['column']] == 'date') {
                    if ($EachRule['val'] == '')
                        continue;

                    if ($EachRule['operator'] == 'more than') {
                        if (is_numeric($EachRule['val']) == false)
                            continue;
                        $SQLFilter[] = "DATEDIFF(NOW(), `" . $EachRule['column'] . "`)>=" . $this->escape_str($EachRule['val']);
                    }
                    elseif ($EachRule['operator'] == 'less than') {
                        if (is_numeric($EachRule['val']) == false)
                            continue;
                        $SQLFilter[] = "DATEDIFF(NOW(), `" . $EachRule['column'] . "`)<=" . $this->escape_str($EachRule['val']);
                    }
                    elseif ($EachRule['operator'] == 'exactly') {
                        if (is_numeric($EachRule['val']) == false)
                            continue;
                        $SQLFilter[] = "DATEDIFF(NOW(), `" . $EachRule['column'] . "`)=" . $this->escape_str($EachRule['val']);
                    }
                    elseif ($EachRule['operator'] == 'after') {
                        if (strtotime($EachRule['val']) === false || strtotime($EachRule['val']) == -1)
                            continue;
                        $TargetDate = date('Y-m-d 00:00:00', strtotime($EachRule['val']));
                        $SQLFilter[] = "`" . $EachRule['column'] . "`>=" . "'" . $this->escape_str($TargetDate) . "'";
                    }
                    elseif ($EachRule['operator'] == 'on') {
                        if (strtotime($EachRule['val']) === false || strtotime($EachRule['val']) == -1)
                            continue;
                        $TargetDate_Start = date('Y-m-d 00:00:00', strtotime($EachRule['val']));
                        $TargetDate_End = date('Y-m-d 23:59:59', strtotime($EachRule['val']));
                        $SQLFilter[] = "(`" . $EachRule['column'] . "`>=" . "'" . $this->escape_str($TargetDate_Start) . "' AND `" . $EachRule['column'] . "`<=" . "'" . $this->escape_str($TargetDate_End) . "')";
                    }
                    elseif ($EachRule['operator'] == 'before') {
                        if (strtotime($EachRule['val']) === false || strtotime($EachRule['val']) == -1)
                            continue;
                        $TargetDate = date('Y-m-d 23:59:59', strtotime($EachRule['val']));
                        $SQLFilter[] = "`" . $EachRule['column'] . "`<=" . "'" . $this->escape_str($TargetDate) . "'";
                    }
                    else {
                        continue;
                    }
                } elseif ($TableStructure[$EachRule['column']] == 'number') {
                    if (is_numeric($EachRule['val']) == false)
                        continue;

                    if ($EachRule['operator'] == 'is') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`=" . $this->escape_str($EachRule['val']);
                    } elseif ($EachRule['operator'] == 'is not') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`!=" . $this->escape_str($EachRule['val']);
                    } elseif ($EachRule['operator'] == 'greater than') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`>=" . $this->escape_str($EachRule['val']);
                    } elseif ($EachRule['operator'] == 'smaller than') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`<=" . $this->escape_str($EachRule['val']);
                    }
                } else {
                    if ($EachRule['operator'] == 'is') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`='" . $this->escape_str($EachRule['val']) . "'";
                    } elseif ($EachRule['operator'] == 'is not') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "`!='" . $this->escape_str($EachRule['val']) . "'";
                    } elseif ($EachRule['operator'] == 'starts with') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "` LIKE '" . $this->escape_str($EachRule['val']) . "%'";
                    } elseif ($EachRule['operator'] == 'ends with') {
                        $SQLFilter[] = "`" . $EachRule['column'] . "` LIKE '%" . $this->escape_str($EachRule['val']) . "'";
                    } elseif ($EachRule['operator'] == 'contains') {
                        if (preg_match('/\|/i', $EachRule['val']) > 0) {
                            $TMP_SQL = array();

                            foreach (explode('|', $EachRule['val']) as $TMP_EachRule_Val) {
                                $TMP_SQL[] = "`" . $EachRule['column'] . "` LIKE '%" . $this->escape_str($TMP_EachRule_Val) . "%'";
                            }
                            $SQLFilter[] = "(" . implode(' OR ', $TMP_SQL) . ")";
                        } else {
                            $SQLFilter[] = "`" . $EachRule['column'] . "` LIKE '%" . $this->escape_str($EachRule['val']) . "%'";
                        }
                    }
                }
            }
        }

        $SQLFilter = implode(($MatchType == 'and' ? ' AND ' : ' OR '), $SQLFilter);

        return $SQLFilter;
    }

    public function GetMessageSentPeople($User, $MessageID) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=?", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0 || is_bool($Query) == true)
            return false;

        return $Query;
    }

    public function GetMessageSentLogForPerson($User, $MessageID, $PersonID) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelPersonID=?", array($MessageID, $PersonID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0 || is_bool($Query) == true)
            return false;

        return $Query[0];
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function RetrieveRecipientsOfMessage($User, $MessageID, $Rules, $MatchType) {
        // Retrieve sent people of the message
        $TMP_SentPeople = $this->GetMessageSentPeople($User, $MessageID);

        $SentPeople = array();
        if (is_bool($TMP_SentPeople) == false) {
            foreach ($TMP_SentPeople as $Index => $EachSentPerson) {
                $SentPeople[$EachSentPerson->RelPersonID] = true;
            }
        }

        // Retrieve recipients of the message
        $SQLFilter = $this->GenerateFilterSQLQueryPart($User, $Rules, $MatchType);

        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : ""));
        $Query = $this->query_result($SQLQuery);

        $Recipients = array();

        if (count($Query) > 0) {
            foreach ($Query as $Index => $EachPerson) {
                if (isset($SentPeople[$EachPerson->PersonID]) == true && $SentPeople[$EachPerson->PersonID] == true)
                    continue;

                $Recipients[$EachPerson->PersonID] = $EachPerson;
            }
        }
        else {
            return false;
        }

        if (count($Recipients) == 0)
            return false;

        return $Recipients;
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function SelectRandomRecipient($User, $Rules, $MatchType, $CreateIfNotExists = false) {
        $SQLFilter = $this->GenerateFilterSQLQueryPart($User, $Rules, $MatchType);

        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : "") . " ORDER BY RAND() LIMIT 0,1");
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0 || is_bool($Query) == true) {
            $Recipient = new stdClass();
            $Recipient->PersonID = 0;
            $Recipient->Identifier = 'test' . rand(1000, 9000);
            $Recipient->IsUnsubscribed = 'No';
            $Recipient->IsBounced = 'No';
            $Recipient->UnsubscriptionDate = '0000-00-00 00:00:00';
            $Recipient->UnsubscriptionIP = '0.0.0.0';
            $Recipient->Email = 'test@test.com';
            $Recipient->EntryDate = date('Y-m-d H:i:s', time() - (86400 * 23));
            $Recipient->LastSeenDate = date('Y-m-d H:i:s', time() - (86400 * 2));
            $Recipient->IPAddress = '127.0.0.1';
            $Recipient->City = 'Los Angeles';
            $Recipient->Country = 'United States';
            $Recipient->SessionCounter = 42;
        } else {
            $Recipient = $Query[0];
        }

        return $Recipient;
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function GetFilteredPeopleCount($User, $Rules, $MatchType) {
        $SQLFilter = $this->GenerateFilterSQLQueryPart($User, $Rules, $MatchType);

        $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS EstimatedRecipients FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : ""));
        $Query = $this->query_result($SQLQuery);

        $EstimatedRecipients = $Query[0]->EstimatedRecipients;

        return $EstimatedRecipients;
    }

    public function CreatePeopleMetaData($User, $FieldMapping = array()) {
        $SQLQuery = "REPLACE INTO oempro_octautomation_people_meta_" . $User['UserID'] . " (`EntryID`, `RelUserID`, `CustomDataMapping`) VALUES (1, " . $User['UserID'] . ", " . $this->escape(json_encode($FieldMapping)) . ")";
        $Result = $this->query_result($SQLQuery);

        return;
    }

    public function RegisterPerson($User, $PersonData = array(), $ReturnMsgOnError = false, $SkipHashCheck = false, $SecurityHash = '') {
        $this->CheckIfPeopleTablesExist($User['UserID'], true);

        // Verify the person ID
        if (isset($PersonData->id) == false || $PersonData->id == '' || ($SkipHashCheck == false && (isset($PersonData->hash) == false || $PersonData->hash == ''))) {
            if ($ReturnMsgOnError == false) {
                print("Invalid person ID");
                return false;
            } else {
                return array(false, 'Invalid person ID');
            }
        }

        // Verify the person email address
        if (isset($PersonData->email) == false || $PersonData->email == '' || filter_var($PersonData->email, FILTER_VALIDATE_EMAIL) === false) {
            if ($ReturnMsgOnError == false) {
                print("Email address is missing or invalid");
                return false;
            } else {
                return array(false, "Email address is missing or invalid");
            }
        }

        // Verify the security hash
        if ($SkipHashCheck == false) {
            $SecretKey = md5($SecurityHash . $SecurityHash . ' ' . $User['UserID']);
            $EncryptedPersonID = hash_hmac("sha256", $PersonData->id, $SecretKey);

            if ($EncryptedPersonID != $PersonData->hash) {
                if ($ReturnMsgOnError == false) {
                    print("Invalid hash");
                    return false;
                } else {
                    return array(false, 'Invalid hash');
                }
            }
        }

        // Geo-location detection
//        if (defined(GEO_LOCATION_DATA_PATH) && isset($_SERVER['REMOTE_ADDR']) == true && file_exists(GEO_LOCATION_DATA_PATH) == true) {
        if (isset($_SERVER['REMOTE_ADDR']) == true && file_exists(GEO_LOCATION_DATA_PATH) == true) {
            $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
            $GeoTagInfo = geoip_record_by_addr($GeoTag, $_SERVER['REMOTE_ADDR']);
        }

        // Data mapping
        $CustomDataMapping = array();
        $DBParameters = array();

        $TableColumns = $this->GetTableColumns($User, 'oempro_octautomation_people_' . $User['UserID']);

        if (is_object($PersonData) == true) {
            $Vars = get_object_vars($PersonData);
        } else {
            $Vars = $PersonData;
        }

        if (count($Vars) > 0) {
            foreach ($Vars as $Key => $Value) {
                if (in_array($Key, $this->PeopleSystemColumns) == true)
                    continue;
                if (is_double($Value) == false && is_numeric($Value) == false && is_string($Value) == false)
                    continue;

                $FieldName = md5($Key);
                $FieldType = (preg_match('/_date$/i', $Key) > 0 ? 'date' : (is_numeric($Value) == true ? 'number' : 'text'));
                $CustomDataMapping[$Key] = md5($Key);
                $DBParameters[md5($Key)] = (is_numeric($Value) == true ? $this->escape_str($Value) : $this->escape_str($Value));

                if (in_array($FieldName, $TableColumns) == false) {
                    if ($FieldType == 'number') {
                        $FieldTypeQuery = "DOUBLE NULL DEFAULT NULL";
                    } elseif ($FieldType == 'date') {
                        $FieldTypeQuery = "DATETIME NULL DEFAULT NULL";
                    } else {
                        $FieldTypeQuery = "VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL";
                    }


                    $SQLQuery = "ALTER TABLE  `oempro_octautomation_people_" . $User['UserID'] . "` ADD  `" . $FieldName . "` " . $FieldTypeQuery . " ;";
                    $Result = $this->query_result($SQLQuery);
                }
            }
        }

        $FieldList = (count(array_keys($DBParameters)) > 0 ? ", `" . implode('`, `', array_keys($DBParameters)) . "`" : '');
        $ValueList = (count(array_values($DBParameters)) > 0 ? ", '" . implode("', '", array_values($DBParameters)) . "'" : '');

        $DBUpdateParameters = array();
        foreach ($DBParameters as $Field => $Value) {
            $DBUpdateParameters[] = "`" . $Field . "`" . "=" . (is_numeric($Value) == true ? $Value : "'" . $Value . "'");
        }

        $PeopleFieldMapping = $this->GetPeopleMetaData($User);
        if (is_bool($PeopleFieldMapping) == true && $PeopleFieldMapping == false) {
            $this->CreatePeopleMetaData($User, $CustomDataMapping);
        } else {
            $TMP = json_decode($PeopleFieldMapping->CustomDataMapping);
            $TMP = array_merge((array) $TMP, (array) $CustomDataMapping);
            $this->CreatePeopleMetaData($User, $TMP);
        }

        $Parameters = array(
            'Identifier' => (is_object($PersonData) == true ? $PersonData->id : $PersonData['id']),
            'Email' => (is_object($PersonData) == true ? $PersonData->email : $PersonData['email']),
            'EntryDate' => date('Y-m-d H:i:s'),
            'LastSeenDate' => date('Y-m-d H:i:s'),
            'IPAddress' => (isset($_SERVER['REMOTE_ADDR']) == true ? $_SERVER['REMOTE_ADDR'] : ''),
            'City' => (isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : ''),
            'Country' => (isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : ''),
            'SessionCounter' => 1,
        );

        $DBUpdateParameters = array_merge($DBUpdateParameters, array(
            "Email='" . $this->escape_str((is_object($PersonData) == true ? $PersonData->email : $PersonData['email'])) . "'",
            "LastSeenDate='" . date('Y-m-d H:i:s') . "'",
            "IPAddress='" . $this->escape_str((isset($_SERVER['REMOTE_ADDR']) == true ? $_SERVER['REMOTE_ADDR'] : '')) . "'",
            "City='" . $this->escape_str((isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : '')) . "'",
            "Country='" . $this->escape_str((isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : '')) . "'",
        ));

        $PersonID = $this->CreatePerson($User, $Parameters, $FieldList, $ValueList, $DBUpdateParameters);

        return true;
    }

    public function CreatePerson($User, $Parameters, $FieldList = array(), $ValueList = array(), $DBUpdateParameters = '') {
        foreach ($Parameters as $Key => $Value) {
            $Parameters[$Key] = $this->escape_str($Value);
        }

        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE `Identifier`=?", array($Parameters['Identifier']));
        $Result = $this->query_result($SQLQuery);

        $SessionCounter = 0;

        if (is_bool($Result) == true && $Result == false) {
            $SessionCounter = 1;
        } else {
            $Person = $Result[0];

            if (time() - strtotime($Person->LastSeenDate) >= OCTAUTOMATION_SESSION_THRESHOLD) {
                $SessionCounter = $Person->SessionCounter + 1;
                $DBUpdateParameters[] = "SessionCounter=" . $SessionCounter;
            }
        }

        $DBUpdateParameters = implode(', ', $DBUpdateParameters);

        $Parameters['RelUserID'] = $User['UserID'];

        $SQLQuery = "INSERT INTO oempro_octautomation_people_" . $User['UserID'] . " (`" . implode('`, `', array_keys($Parameters)) . "` " . $FieldList . ") VALUES ('" . implode("', '", array_values($Parameters)) . "' " . $ValueList . ") ON DUPLICATE KEY UPDATE " . $DBUpdateParameters;
        $Result = $this->query_result($SQLQuery);

        $PersonID = $this->insert_id();

        return $PersonID;
    }

    public function DeletePerson($User, $PersonID) {
        $SQLQuery = $this->add_parameters("DELETE FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE PersonID=?", array($PersonID));
        $Query = $this->query_result($SQLQuery);

        /*
         * We set the RelPersonID to zero so that the stats and relevancy
         * doesn't get affected. RelPersonID=0 means a deleted person
         */
        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_people_delivery_log_" . $User['UserID'] . " SET RelPersonID=0 WHERE RelPersonID=?", array($PersonID));
        $Query = $this->query_result($SQLQuery);

        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_people_activity_log_" . $User['UserID'] . " SET RelPersonID=0 WHERE RelPersonID=?", array($PersonID));
        $Query = $this->query_result($SQLQuery);

        return;
    }

    public function UpdatePerson($User, $PersonID, $UpdatedFields) {
        $TMP = array();
        $Parameters = array();
        foreach ($UpdatedFields as $Field => $Value) {
            if (is_array($Value)) {
                $TMP[] = $Field . " = " . $Value[0];
            } else {
                $Parameters[] = $Value;
                $TMP[] = $Field . '=?';
            }
        }
        $UpdatedFields = implode(', ', $TMP);
        unset($TMP);

        $Parameters[] = $PersonID;
        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_people_" . $User['UserID'] . " SET " . $UpdatedFields . " WHERE PersonID=?", $Parameters);
        $Query = $this->query_result($SQLQuery);
    }

    public function UpdatePersonDeliveryLog($User, $MessageID, $PersonID, $UpdatedFields) {
        $TMP = array();
        $Parameters = array();
        foreach ($UpdatedFields as $Field => $Value) {
            if (is_array($Value)) {
                $TMP[] = $Field . " = " . $Value[0];
            } else {
                $Parameters[] = $Value;
                $TMP[] = $Field . '=?';
            }
        }
        $UpdatedFields = implode(', ', $TMP);
        unset($TMP);

        $Parameters[] = $PersonID;
        $Parameters[] = $MessageID;
        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_people_delivery_log_" . $User['UserID'] . " SET " . $UpdatedFields . " WHERE RelPersonID=? AND RelMessageID=?", $Parameters);
        $Query = $this->query_result($SQLQuery);
    }

    public function UnsubscribePerson($User, $PersonID) {

        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_people_" . $User['UserID'] . " SET IsUnsubscribed=?, UnsubscriptionDate=?, UnsubscriptionIP=? WHERE PersonID=?", array('Yes', date('Y-m-d H:i:s'), $_SERVER['REMOTE_ADDR'], $PersonID));
        $Query = $this->query_result($SQLQuery);

        $this->RegisterActivityToPerson($User, $PersonID, 'Unsubscription');

        return;
    }

    public function GetPeopleCount($UserID) {
        $this->CheckIfPeopleTablesExist($UserID, true);

        $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalPeople FROM oempro_octautomation_people_" . $UserID . " WHERE RelUserID=? AND IsUnsubscribed='No' AND IsBounced='No'", array($UserID));

        $Result = $this->query_result($SQLQuery);

        $TotalPeople = $Result[0]->TotalPeople;

        return $TotalPeople;
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function BrowsePeopleCustom($User, $ReturnTotal = false, $Rules = array(), $MatchType = null) {
        
        if (count($Rules) == 0 || is_null($MatchType) == true) {
            $SQLFilter = '';
        } else {
            $SQLFilter = $this->GenerateFilterSQLQueryPart($User, $Rules, $MatchType);
        }

        if ($ReturnTotal == true) {
            $SQLQuery = "SELECT COUNT(*) AS TotalPeople FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : "") . " ";
        } else {
            $SQLQuery = "SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : "") . " ";
        }

        $QueryResult = $this->query_result($SQLQuery);

        if ($ReturnTotal == true) {
            $TotalPeople = $QueryResult[0]->TotalPeople;

            return $TotalPeople;
        } else {
            if (is_bool($QueryResult) == true && $QueryResult == false) {
                return false;
            }

            return $QueryResult;
        }
    }

    /*
     * $Rules = array(arrray('column' => 'field_name', 'operator' => 'operator', 'val' => 'value'), array(...));
     * $MatchType can be 'and' or 'or'
     * */

    public function BrowsePeople($User, $StartFrom, $RPP, $ReturnTotal = false, $OrderByField = 'LastSeenDate', $OrderByType = 'DESC', $Rules = array(), $MatchType = null) {
        $TableFields = $this->GetPeopleDataStructure($User);

        if (isset($TableFields[$OrderByField]) == false)
            $OrderByField = 'LastSeenDate';

        if (count($Rules) == 0 || is_null($MatchType) == true) {
            $SQLFilter = '';
        } else {
            $SQLFilter = $this->GenerateFilterSQLQueryPart($User, $Rules, $MatchType);
        }

        if ($ReturnTotal == true) {
            $SQLQuery = "SELECT COUNT(*) AS TotalPeople FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : "") . " ";
            $SQLQueryLimit = '';
        } else {
            $SQLQuery = "SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE IsUnsubscribed='No' AND IsBounced='No' " . ($SQLFilter != '' ? " AND " . $SQLFilter : "") . " ";
            $SQLQueryLimit = " LIMIT " . $StartFrom . ", " . $RPP;
        }

        $QueryResult = $this->query_result($SQLQuery . " ORDER BY `" . $this->escape_str($OrderByField) . "` " . $this->escape_str($OrderByType) . $SQLQueryLimit);

        if ($ReturnTotal == true) {
            $TotalPeople = $QueryResult[0]->TotalPeople;

            return $TotalPeople;
        } else {
            if (is_bool($QueryResult) == true && $QueryResult == false) {
                return false;
            }

            return $QueryResult;
        }
    }

    public function GetPerson($User, $PersonID, $ReturnActiveOnly = true) {
        if ($ReturnActiveOnly == true) {
            $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE PersonID=? AND IsUnsubscribed=? AND IsBounced=?", array($PersonID, 'No', 'No'));
        } else {
            $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE PersonID=?", array($PersonID));
        }

        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true && $Query == false)
            return false;
        if (count($Query) == 0)
            return false;

        return $Query[0];
    }

    public function GetPersonByIdentifier($User, $Identifier, $ReturnActiveOnly = true) {
        if ($ReturnActiveOnly == true) {
            $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE Identifier=? AND IsUnsubscribed=? AND IsBounced=?", array($Identifier, 'No', 'No'));
            $Query = $this->query_result($SQLQuery);
        } else {
            $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE Identifier=?", array($Identifier));
            $Query = $this->query_result($SQLQuery);
        }

        if (count($Query) == 0 || is_bool($Query) == true)
            return false;

        return $Query[0];
    }

    public function RegisterActivityToPerson($User, $PersonID, $ActivityType, $MessageID = 0) {
        $SQLQuery = $this->add_parameters("INSERT INTO oempro_octautomation_people_activity_log_" . $User['UserID'] . " (`LogID`, `RelMessageID`, `RelPersonID`, `RelUserID`, `ActivityDateTime`, `ActivityType`) VALUES (?, ?, ?, ?, ?, ?)", array(
            '', $MessageID, $PersonID, $User['UserID'], date('Y-m-d H:i:s'), $ActivityType
        ));

        $Query = $this->query_result($SQLQuery);
    }

    public function RegisterDeliveryLog($User, $MessageID, $PersonID, $IsOpened = false, $IsClicked = false, $IsUnsubscribed = false, $IsHardBounced = false, $IsSpamComplaint = false) {
        $SQLQuery = $this->add_parameters("INSERT INTO oempro_octautomation_people_delivery_log_" . $User['UserID'] . " (`LogID`, `RelMessageID`, `RelPersonID`, `RelUserID`, `SentAt`, `IsOpened`, `IsClicked`, `IsUnsubscribed`, `IsHardBounced`, `IsSpamComplaint`)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", array(
            '', $MessageID, $PersonID, $User['UserID'], date('Y-m-d H:i:s'), ($IsOpened == true ? 'Yes' : 'No'), ($IsClicked == true ? 'Yes' : 'No'), ($IsUnsubscribed == true ? 'Yes' : 'No'), ($IsHardBounced == true ? 'Yes' : 'No'), ($IsSpamComplaint == true ? 'Yes' : 'No')
        ));
        $Query = $this->query_result($SQLQuery);
    }

    public function GetActivityOfPerson($User, $PersonID, $GetMessageName = false) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_activity_log_" . $User['UserID'] . " WHERE RelPersonID=? ORDER BY ActivityDateTime DESC", array($PersonID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        $Activities = $Query;

        if ($GetMessageName == true) {

            foreach ($Activities as $Index => $EachActivity) {
                if ($EachActivity->RelMessageID > 0) {
                    $Message = $this->Models->messages->GetMessage($User, $EachActivity->RelMessageID);
                } else {
                    $Message = false;
                }

                $EachActivity->MessageName = (is_bool($Message) == true ? false : $Message->MessageName);

                $Activities[$Index] = $EachActivity;
            }
        }

        return $Activities;
    }

    public function CheckIfPeopleTablesExist($UserID, $CreateTables = false) {
        $SQLQuery = "SHOW TABLES LIKE 'oempro_octautomation_people_" . $UserID . "'";
        $Result = $this->query_result($SQLQuery);

        if (is_bool($Result) == true && $Result == false && $CreateTables == false)
            return false;

        if (is_bool($Result) == true && $Result == false && $CreateTables == true) {
            $this->CreatePeopleTablesForUser($UserID);
        }

        return true;
    }

    public function CreatePeopleTablesForUser($UserID) {
        if ($UserID == '')
            return;

        $SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_octautomation_people_" . $UserID . "` (
		  `PersonID` int(11) NOT NULL AUTO_INCREMENT,
		  `RelUserID` int(11) NOT NULL,
		  `Identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `IsUnsubscribed` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `IsBounced` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `UnsubscriptionDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `UnsubscriptionIP` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.0.0.0',
		  `Email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `EntryDate` datetime NOT NULL,
		  `LastSeenDate` datetime NOT NULL,
		  `IPAddress` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `City` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `Country` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `SessionCounter` int(11) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`PersonID`),
		  UNIQUE KEY `Identifier` (`Identifier`),
		  KEY `Identifier_2` (`Identifier`,`IsUnsubscribed`,`IsBounced`),
		  KEY `IsUnsubscribed` (`IsUnsubscribed`,`IsBounced`),
		  KEY `RelUserID` (`RelUserID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
		";
        $Result = $this->query_result($SQLQuery);

        $SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_octautomation_people_activity_log_" . $UserID . "` (
		  `LogID` int(11) NOT NULL AUTO_INCREMENT,
		  `RelMessageID` int(11) NOT NULL,
		  `RelPersonID` int(11) NOT NULL,
		  `RelUserID` int(11) NOT NULL,
		  `ActivityDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `ActivityType` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  PRIMARY KEY (`LogID`),
		  KEY `RelMessageID` (`RelMessageID`,`ActivityDateTime`),
		  KEY `RelPersonID` (`RelPersonID`,`ActivityDateTime`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
		";
        $Result = $this->query_result($SQLQuery);

        $SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_octautomation_people_delivery_log_" . $UserID . "` (
		  `LogID` int(11) NOT NULL AUTO_INCREMENT,
		  `RelMessageID` int(11) NOT NULL,
		  `RelPersonID` int(11) NOT NULL,
		  `RelUserID` int(11) NOT NULL,
		  `SentAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `IsOpened` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `IsClicked` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `IsUnsubscribed` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `IsHardBounced` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `IsSpamComplaint` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
		  `Open_City` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `Open_Country` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
		  `Open_Date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `Click_Date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `Unsubscription_Date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`LogID`),
		  KEY `RelMessageID` (`RelMessageID`,`RelPersonID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
		";
        $Result = $this->query_result($SQLQuery);

        $SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_octautomation_people_meta_" . $UserID . "` (
		  `EntryID` int(11) NOT NULL AUTO_INCREMENT,
		  `RelUserID` int(11) NOT NULL,
		  `CustomDataMapping` text COLLATE utf8_unicode_ci NOT NULL,
		  PRIMARY KEY (`EntryID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
		";
        $Result = $this->query_result($SQLQuery);
    }

}
