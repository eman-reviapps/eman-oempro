<?php

class model_messages extends model_base {

    public function __construct() {
        
    }

    public function test_message_log_fillin($Days = 365, $Users = 1000) {
        $Date = new DateTime(date('Y-m-d'));

        for ($DayCounter = $Days; $DayCounter >= 0; $DayCounter--) {
            $Date->sub(new DateInterval('P1D'));
            $NewDate = $Date->format('Y-m-d 00:00:00');

            for ($UserCounter = 1; $UserCounter <= $Users; $UserCounter++) {
                $SQLQuery = $this->add_parameters("INSERT INTO oempro_octautomation_messages_log (LogID, RelUserID, Period, MessagesSent, TotalOpened, TotalClicked, TotalUnsubscribed, TotalHardBounced, TotalSpamComplaint) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", array(
                    '', $UserCounter, $NewDate, rand(1000, 2500), rand(250, 850), rand(50, 150), rand(10, 90), rand(0, 50), rand(0, 20)
                ));
                $Query = $this->query_result($SQLQuery);
            }
        }
    }

    public function DeleteUserRecords($UserID) {
        if (is_numeric($UserID) == false)
            return false;

        $SQLQueries = array();
        $SQLQueries[] = $this->add_parameters("DELETE FROM oempro_octautomation_messages WHERE RelUserID=?", array($UserID));
        $SQLQueries[] = $this->add_parameters("DELETE FROM oempro_octautomation_messages_log WHERE RelUserID=?", array($UserID));
        $SQLQueries[] = $this->add_parameters("DROP TABLE IF EXISTS oempro_octautomation_people_" . $UserID);
        $SQLQueries[] = $this->add_parameters("DROP TABLE IF EXISTS oempro_octautomation_people_activity_log_" . $UserID);
        $SQLQueries[] = $this->add_parameters("DROP TABLE IF EXISTS oempro_octautomation_people_meta_" . $UserID);

        foreach ($SQLQueries as $EachSQLQuery) {
            $Query = $this->query_result($EachSQLQuery);
        }
    }

    public function SetupDatabaseSchema() {
        $SQLQueries = array();

        $SQLQueries[] = $this->add_parameters("DROP TABLE IF EXISTS `oempro_octautomation_messages`;");
        $SQLQueries[] = $this->add_parameters("DROP TABLE IF EXISTS `oempro_octautomation_messages_log`;");
        $SQLQueries[] = $this->add_parameters("CREATE TABLE IF NOT EXISTS `oempro_octautomation_messages` (
			`MessageID` int(11) NOT NULL AUTO_INCREMENT,
			`RelUserID` int(11) NOT NULL,
			`MessageName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`MessageType` enum('Email') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Email',
			`CreatedAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			`Status` enum('Draft','Active','Paused','Deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Draft',
			`FromName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`FromEmail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`ReplyToName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`ReplyToEmail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`Subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
			`HTMLContent` longtext COLLATE utf8_unicode_ci NOT NULL,
			`PlainContent` longtext COLLATE utf8_unicode_ci NOT NULL,
			`TotalSent` int(11) NOT NULL DEFAULT '0',
			`TotalOpened` int(11) NOT NULL DEFAULT '0',
			`TotalClicked` int(11) NOT NULL DEFAULT '0',
			`TotalUnsubscribed` int(11) NOT NULL DEFAULT '0',
			`TotalHardBounced` int(11) NOT NULL DEFAULT '0',
			`TotalSpamComplaint` int(11) NOT NULL DEFAULT '0',
			`LatestEmailSentAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			`Rules` text COLLATE utf8_unicode_ci NOT NULL,
			PRIMARY KEY (`MessageID`),
			KEY `RelUserID` (`RelUserID`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
        $SQLQueries[] = $this->add_parameters("CREATE TABLE IF NOT EXISTS `oempro_octautomation_messages_log` (
			`LogID` int(11) NOT NULL AUTO_INCREMENT,
			`RelUserID` int(11) NOT NULL,
			`Period` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			`MessagesSent` int(11) NOT NULL DEFAULT '0',
			`TotalOpened` int(11) NOT NULL DEFAULT '0',
			`TotalClicked` int(11) NOT NULL DEFAULT '0',
			`TotalUnsubscribed` int(11) NOT NULL DEFAULT '0',
			`TotalHardBounced` int(11) NOT NULL DEFAULT '0',
			`TotalSpamComplaint` int(11) NOT NULL DEFAULT '0',
			PRIMARY KEY (`LogID`),
			UNIQUE KEY `RelUserID_2` (`RelUserID`,`Period`),
			KEY `RelUserID` (`RelUserID`),
			KEY `Period` (`Period`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

        foreach ($SQLQueries as $EachSQLQuery) {
            $Query = $this->query_result($EachSQLQuery);
        }
    }

    public function GetTopSenders($Period, $TopX = 10) {
        $SQLQuery = $this->add_parameters("SELECT RelUserID AS UserID, SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalMessagesOpened FROM oempro_octautomation_messages_log WHERE Period>=? AND Period<=? GROUP BY RelUserID ORDER BY TotalMessagesSent DESC LIMIT 0, ?", array(
            $Period . '-01 00:00:00', $Period . '-31 23:59:59', $TopX
        ));

        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            return false;

        return $Query;
    }

    public function GetMessageStats($UserID = 0) {
        $Today_Start = date('Y-m-d 00:00:00');
        $Today_End = date('Y-m-d 23:59:59');

        $Yesterday_Start = date('Y-m-d 00:00:00', time() - 86400);
        $Yesterday_End = date('Y-m-d 23:59:59', time() - 86400);

        $ThisMonth_Start = date('Y-m-01 00:00:00');
        $ThisMonth_End = date('Y-m-' . date('t', time()) . ' 23:59:59');

        $LastMonth_Start = date('Y-m-01 00:00:00', time() - (86400 * 30));
        $LastMonth_End = date('Y-m-' . date('t', time() - (86400 * 30)) . ' 23:59:59', time() - (86400 * 30));

        $ThisYear_Start = date('Y-01-01 00:00:00');
        $ThisYear_End = date('Y-12-31 23:59:59');

        $LastYear = date('Y') - 1;
        $LastYear_Start = date($LastYear . '-01-01 00:00:00');
        $LastYear_End = date($LastYear . '-12-31 23:59:59');

        $Stats = array();

        // Today
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $Today_Start, $Today_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['Today'] = false;

        $Stats['Today'] = $Query[0];

        // Yesterday
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $Yesterday_Start, $Yesterday_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['Yesterday'] = false;

        $Stats['Yesterday'] = $Query[0];

        // This Month
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $ThisMonth_Start, $ThisMonth_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['ThisMonth'] = false;

        $Stats['ThisMonth'] = $Query[0];

        // Last Month
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $LastMonth_Start, $LastMonth_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['LastMonth'] = false;

        $Stats['LastMonth'] = $Query[0];

        // This Year
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $ThisYear_Start, $ThisYear_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['ThisYear'] = false;

        $Stats['ThisYear'] = $Query[0];

        // Last Year
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM oempro_octautomation_messages_log WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? AND Period>=? AND Period<=?", array(
            $UserID, $LastYear_Start, $LastYear_End
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            $Stats['LastYear'] = false;

        $Stats['LastYear'] = $Query[0];

        return $Stats;
    }

    public function GetLastXDayMessageActivities($UserID = 0, $LastXDays = 30) {
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Period, '%Y-%m-%d') AS TargetPeriod, SUM(MessagesSent) AS TotalMessagesSent, SUM(TotalOpened) AS TotalOpened, SUM(TotalClicked) AS TotalClicked, SUM(TotalUnsubscribed) AS TotalUnsubscribed, SUM(TotalHardBounced) AS TotalHardBounced, SUM(TotalSpamComplaint) AS TotalSpamComplaint FROM `oempro_octautomation_messages_log` WHERE RelUserID" . ($UserID == 0 ? '!=' : '=') . "? GROUP BY DATE_FORMAT(Period, '%Y-%m-%d') ORDER BY TargetPeriod DESC LIMIT 0, ?", array(
            $UserID, $LastXDays
        ));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            return false;

        $Stats = array();

        foreach ($Query as $Index => $EachEntry) {
            $Stats[$EachEntry->TargetPeriod] = $EachEntry;
        }

        return $Stats;
    }

    public function GetTimeFrame($User, $MessageID, $LastXDays = 30) {
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(SentAt, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalSent FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? GROUP BY DATE_FORMAT(SentAt, '%Y-%m-%d') ORDER BY TargetDate DESC LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID']));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            return false;

        $Data = array();

        foreach ($Query as $Index => $EachResult) {
            $Data[$EachResult->TargetDate]['TotalSent'] = $EachResult->TotalSent;
        }

        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Open_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalOpened FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Open_Date!=? GROUP BY DATE_FORMAT(Open_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalOpened'] = $EachResult->TotalOpened;
            }
        }

        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Click_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalClicked FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Click_Date!=? GROUP BY DATE_FORMAT(Click_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalClicked'] = $EachResult->TotalClicked;
            }
        }

        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalUnsubscribed FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Unsubscription_Date!=? GROUP BY DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalUnsubscribed'] = $EachResult->TotalUnsubscribed;
            }
        }

        return $Data;
    }

    /*
     * Eman
     */

    public function GetTimeFrameCustom($User, $MessageID, $StartDate, $FinishDate) {
//        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(SentAt, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalSent FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? GROUP BY DATE_FORMAT(SentAt, '%Y-%m-%d') ORDER BY TargetDate DESC LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID']));
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(SentAt, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalSent FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND DATE_FORMAT(SentAt, '%Y-%m-%d %H:%i:%s')>=? AND DATE_FORMAT(SentAt, '%Y-%m-%d %H:%i:%s')<=? GROUP BY crc32(DATE_FORMAT(SentAt, '%Y-%m-%d')) ", array($MessageID, $User['UserID'], "'" . $StartDate . "'", "'" . $FinishDate . "'"));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            return false;

        $Data = array();

        foreach ($Query as $Index => $EachResult) {
            $Data[$EachResult->TargetDate]['TotalSent'] = $EachResult->TotalSent;
        }

//        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Open_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalOpened FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Open_Date!=? GROUP BY DATE_FORMAT(Open_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Open_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalOpened FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Open_Date!=? AND DATE_FORMAT(Open_Date, '%Y-%m-%d %H:%i:%s')>=? AND DATE_FORMAT(Open_Date, '%Y-%m-%d %H:%i:%s')<=? GROUP BY crc32(DATE_FORMAT(Open_Date, '%Y-%m-%d')) ", array($MessageID, $User['UserID'], '0000-00-00 00:00:00', "'" . $StartDate . "'", "'" . $FinishDate . "'"));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalOpened'] = $EachResult->TotalOpened;
            }
        }

//        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Click_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalClicked FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Click_Date!=? GROUP BY DATE_FORMAT(Click_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Click_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalClicked FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Click_Date!=? AND DATE_FORMAT(Click_Date, '%Y-%m-%d %H:%i:%s')>=? AND DATE_FORMAT(Click_Date, '%Y-%m-%d %H:%i:%s')<=? GROUP BY crc32(DATE_FORMAT(Click_Date, '%Y-%m-%d')) ", array($MessageID, $User['UserID'], '0000-00-00 00:00:00', "'" . $StartDate . "'", "'" . $FinishDate . "'"));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalClicked'] = $EachResult->TotalClicked;
            }
        }

//        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalUnsubscribed FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Unsubscription_Date!=? GROUP BY DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d') ORDER BY TargetDate LIMIT 0, " . $LastXDays, array($MessageID, $User['UserID'], '0000-00-00 00:00:00'));
        $SQLQuery = $this->add_parameters("SELECT DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d') AS TargetDate, COUNT(*) AS TotalUnsubscribed FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " WHERE RelMessageID=? AND RelUserID=? AND Unsubscription_Date!=? AND DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d %H:%i:%s')>=? AND DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d %H:%i:%s')<=? GROUP BY crc32(DATE_FORMAT(Unsubscription_Date, '%Y-%m-%d')) ", array($MessageID, $User['UserID'], '0000-00-00 00:00:00', "'" . $StartDate . "'", "'" . $FinishDate . "'"));
        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $Data[$EachResult->TargetDate]['TotalUnsubscribed'] = $EachResult->TotalUnsubscribed;
            }
        }

        return $Data;
    }

    /*
     * Eman
     */

    public function GetMessageOpenGeo($UserID, $MessageID, $StartDate = null, $FinishDate = null) {

        $ArrayOpens = array();
        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Open_Count"
//                . " (select  country_name from ip2location_db9 where country_code = Open_Country limit 1) Country_Name"
                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
                . "  WHERE     IsOpened='Yes'"
                . "  AND       RelUserID = $UserID"
                . "  AND       RelMessageID = $MessageID";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(Open_Date, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(Open_Date, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Open_Country"
                . "    ORDER BY  COUNT(*) desc";

        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $row = array(
                    "Open_Country" => $EachResult->Open_Country,
                    "Count" => $EachResult->Open_Count,
//                    "Country_Name" => $EachResult->Country_Name,
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    /*
     * Eman
     */

    public function GetMessageClickGeo($UserID, $MessageID, $StartDate = null, $FinishDate = null) {

        $ArrayClicks = array();

        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Click_Count"
//                . " (select  country_name from ip2location_db9 where country_code = Open_Country limit 1) Country_Name"
                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
                . "  WHERE     IsClicked='Yes'"
                . "  AND       RelUserID = $UserID"
                . "  AND       RelMessageID = $MessageID";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(Click_Date, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(Click_Date, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Open_Country"
                . "    ORDER BY  COUNT(*) desc";

        $Query = $this->query_result($SQLQuery);

        if (is_array($Query) == true && count($Query) > 0) {
            foreach ($Query as $Index => $EachResult) {
                $row = array(
                    "Open_Country" => $EachResult->Open_Country,
                    "Count" => $EachResult->Click_Count,
//                    "Country_Name" => $EachResult->Country_Name,
                );
                $ArrayClicks[] = $row;
            }
        }
        return $ArrayClicks;
    }

    /*
     * Eman
     */

    public function GetOpenLog_Enhanced($User, $MessageID, $Date) {

        $SQLQuery = "SELECT tblA.*, tblB.Email,count(Open_Date) Total_Opens FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA "
                . " INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB "
                . " ON tblA.RelPersonID=tblB.PersonID "
                . " WHERE tblA.RelMessageID= $MessageID"
                . " AND tblA.IsOpened='Yes' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(tblA.Open_Date, '%Y-%m-%d')='" . $Date . "' ";
        }
        $SQLQuery .= " GROUP BY tblA.Open_Date,RelPersonID ORDER BY tblA.Open_Date DESC,RelPersonID ";

//        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsOpened='Yes' ORDER BY tblA.Open_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    /*
     * Eman
     */

    public function GetClickLog_Enhanced($User, $MessageID, $Date) {
        $SQLQuery = "SELECT tblA.*, tblB.Email,count(Click_Date) Total_Clicks FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA "
                . " INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB"
                . " ON tblA.RelPersonID=tblB.PersonID"
                . " WHERE tblA.RelMessageID=$MessageID"
                . " AND tblA.IsClicked='Yes' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(tblA.Click_Date, '%Y-%m-%d')='" . $Date . "' ";
        }
        
        $SQLQuery .= " GROUP BY tblA.Click_Date,RelPersonID ORDER BY tblA.Click_Date DESC,RelPersonID ";
//        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsClicked='Yes' ORDER BY tblA.Click_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    /*
     * Eman
     */

    public function GetUnsubscriptionLog_Enhanced($User, $MessageID, $Date) {
        $SQLQuery = "SELECT tblA.*, tblB.Email,count(Unsubscription_Date) Total_Unsubscriptions FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA"
                . " INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB"
                . " ON tblA.RelPersonID=tblB.PersonID"
                . " WHERE tblA.RelMessageID=$MessageID"
                . " AND tblA.IsUnsubscribed='Yes'";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(tblA.Unsubscription_Date, '%Y-%m-%d')='" . $Date . "' ";
        }
        
        $SQLQuery .= " GROUP BY tblA.Unsubscription_Date,RelPersonID ORDER BY tblA.Unsubscription_Date DESC,RelPersonID ";
//        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsUnsubscribed='Yes' ORDER BY tblA.Unsubscription_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    public function GetOpenLog($User, $MessageID) {
        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsOpened='Yes' ORDER BY tblA.Open_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    public function GetClickLog($User, $MessageID) {
        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsClicked='Yes' ORDER BY tblA.Click_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    public function GetUnsubscriptionLog($User, $MessageID) {
        $SQLQuery = $this->add_parameters("SELECT tblA.*, tblB.Email FROM oempro_octautomation_people_delivery_log_" . $User['UserID'] . " AS tblA INNER JOIN oempro_octautomation_people_" . $User['UserID'] . " AS tblB ON tblA.RelPersonID=tblB.PersonID WHERE tblA.RelMessageID=? AND tblA.IsUnsubscribed='Yes' ORDER BY tblA.Unsubscription_Date DESC", array($MessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query;
    }

    public function CreateMessage($User, $Parameters) {
        foreach ($Parameters as $Key => $Value) {
            $Parameters[$Key] = $this->escape_str($Value);
        }
        $SQLQuery = "INSERT INTO oempro_octautomation_messages (`" . implode('`, `', array_keys($Parameters)) . "`) VALUES ('" . implode("', '", array_values($Parameters)) . "')";
        $Query = $this->query_result($SQLQuery);

        return $this->insert_id();
    }

    public function UpdateMessage($User, $MessageID, $UpdatedFields) {
        $TMP = array();
        $Parameters = array();
        foreach ($UpdatedFields as $Field => $Value) {
            if (is_array($Value)) {
                $TMP[] = $Field . " = " . $Value[0];
            } else {
                $Parameters[] = $Value;
                $TMP[] = $Field . '=?';
            }
        }

        $UpdatedFields = implode(', ', $TMP);
        unset($TMP);

        $Parameters[] = $MessageID;
        $SQLQuery = $this->add_parameters("UPDATE oempro_octautomation_messages SET " . $UpdatedFields . " WHERE MessageID=?", $Parameters);
        $Query = $this->query_result($SQLQuery);
    }

    public function DeleteMessage($User, $MessageID) {
        $SQLQuery = $this->add_parameters("DELETE FROM oempro_octautomation_messages WHERE MessageID=?", array($MessageID));
        $Query = $this->query_result($SQLQuery);
    }

    public function GetMessageTotal($User, $CountActiveOnly = true, $FilterByStatus = false) {
        if ($CountActiveOnly == true) {
            $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=? AND (Status='Draft' OR Status='Active' OR Status='Paused')", array($User['UserID']));
        } else {
            if (is_bool($FilterByStatus) == true) {
                $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=?", array($User['UserID']));
            } elseif ($FilterByStatus == 'Draft') {
                $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=? AND Status='Draft'", array($User['UserID']));
            } elseif ($FilterByStatus == 'Active') {
                $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=? AND Status='Active'", array($User['UserID']));
            } elseif ($FilterByStatus == 'Paused') {
                $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=? AND Status='Paused'", array($User['UserID']));
            } elseif ($FilterByStatus == 'Deleted') {
                $SQLQuery = $this->add_parameters("SELECT COUNT(*) AS TotalMessages FROM oempro_octautomation_messages WHERE RelUserID=? AND Status='Deleted'", array($User['UserID']));
            }
        }
        $Query = $this->query_result($SQLQuery);

        $TotalMessages = $Query[0]->TotalMessages;

        return $TotalMessages;
    }

    public function GetMessages($User = null, $ListWhat = 'active') {
        $Mapping = array(
            'all' => '',
            'draft' => 'Draft',
            'active' => 'Active',
            'paused' => 'Paused',
            'deleted' => 'Deleted',
        );

        if (is_null($User) == true) {
            if (isset($Mapping[$ListWhat]) == false || $Mapping[$ListWhat] == '') {
                $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages");
                $Query = $this->query_result($SQLQuery);
            } else {
                $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages WHERE Status=?", array($Mapping[$ListWhat]));
                $Query = $this->query_result($SQLQuery);
            }
        } else {
            if (isset($Mapping[$ListWhat]) == false || $Mapping[$ListWhat] == '') {
                $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages WHERE RelUserID=?", array($User['UserID']));
                $Query = $this->query_result($SQLQuery);
            } else {
                $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages WHERE RelUserID=? AND Status=?", array($User['UserID'], $Mapping[$ListWhat]));
                $Query = $this->query_result($SQLQuery);
            }
        }

        if (is_bool($Query) == true)
            return false;

        return $Query;
    }

    public function GetMessage($User, $MessageID) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages WHERE RelUserID=? AND MessageID=?", array($User['UserID'], $MessageID));
        $Query = $this->query_result($SQLQuery);

        if (is_bool($Query) == true)
            return false;

        return $Query[0];
    }

    public function GetMessageByEncryptedMessageID($User, $EncryptedMessageID, $Salt) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_messages WHERE RelUserID=? AND MD5(CONCAT(?, MessageID))=?", array($User['UserID'], $Salt, $EncryptedMessageID));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        return $Query[0];
    }

    public function GetMessageActivityFeed($User, $MessageID, $RPP = 100) {
        $SQLQuery = $this->add_parameters("SELECT * FROM oempro_octautomation_people_activity_log_" . $User['UserID'] . " WHERE RelMessageID=? ORDER BY ActivityDateTime DESC LIMIT 0, ?", array($MessageID, $RPP));
        $Query = $this->query_result($SQLQuery);

        if (count($Query) == 0)
            return false;

        $Activity = array();
        $People = array();

        $Counter = 0;
        foreach ($Query as $Index => $EachActivity) {
            $Activity[$Counter]['ActivityLog'] = $EachActivity;

            if ($EachActivity->RelPersonID == 0) {
                $Activity[$Counter]['Person'] = null;
            } else {
                if (isset($People[$EachActivity->RelPersonID]) == true && is_object($People[$EachActivity->RelPersonID]) == true) {
                    $Activity[$Counter]['Person'] = $People[$EachActivity->RelPersonID];
                } else {
                    $SQLQuery2 = $this->add_parameters("SELECT * FROM oempro_octautomation_people_" . $User['UserID'] . " WHERE PersonID=?", array($EachActivity->RelPersonID));
                    $Query2 = $this->query_result($SQLQuery2);
                    if (count($Query2) == 0) {
                        $Activity[$Counter]['Person'] = null;
                    } else {
                        $Person = $Query2[0];

                        $People[$Person->PersonID] = $Person;

                        $Activity[$Counter]['Person'] = $People[$Person->PersonID];
                    }
                }
            }

            $Counter++;
        }

        return $Activity;
    }

    public function IsMessageLocked($User, $MessageID, $MessageLockID) {
        $SQLQuery = $this->add_parameters("SELECT GET_LOCK('" . $MessageLockID . "', 0) AS LockStatus");
        $Query = $this->query_result($SQLQuery);

        $LockResult = $Query[0];

        if ($LockResult->LockStatus == 1) {
            return false;
        } else {
            return true;
        }
    }

    public function ReleaseMessageLock($User, $MessageID, $MessageLockID) {
        $SQLQuery = $this->add_parameters("SELECT RELEASE_LOCK('" . $MessageLockID . "')");
        $Query = $this->query_result($SQLQuery);

        return;
    }

    public function GetUserPeriodEmailDeliveryTotal($User, $Period) {
        $SQLQuery = $this->add_parameters("SELECT SUM(MessagesSent) AS TotalMessagesSent FROM oempro_octautomation_messages_log WHERE RelUserID=? AND DATE_FORMAT(Period, '%Y-%m')=?", array(
            $User['UserID'], $Period
        ));
        $Query = $this->query_result($SQLQuery);

        if (isset($Query[0]->TotalMessagesSent) == false)
            return 0;

        return $Query[0]->TotalMessagesSent;
    }

    public function RegisterMessageLog($User, $MessageID, $UpdatedCounters = array()) {
        $TMP = array();
        foreach ($UpdatedCounters as $Field => $Value) {
            $TMP[$Field] = (is_array($Value) == true ? $Value[0] : "'" . $this->escape_str($Value) . "'");
        }
        $Counters = $TMP;
        unset($TMP);

        $TMP = array();
        $UpdatedFields = array();
        foreach ($UpdatedCounters as $Field => $Value) {
            $TMP[] = $Field . (is_array($Value) == true ? " = " . $Value[0] : "='" . $this->escape_str($Value) . "'");
        }

        $UpdatedFields = implode(', ', $TMP);
        unset($TMP);

        $QueryValues = array(
            "''", $User['UserID'], $this->escape(date('Y-m-d H:00:00'))
        );

        $QueryValues = array_merge($QueryValues, array_values($Counters));

        $SQLQuery = $this->add_parameters("INSERT INTO oempro_octautomation_messages_log (`LogID`, `RelUserID`, `Period`, `" . implode('`, `', array_keys($Counters)) . "`) VALUES (" . implode(',', array_values($QueryValues)) . ") ON DUPLICATE KEY UPDATE " . $UpdatedFields, array());
        $Query = $this->query_result($SQLQuery);

        return;
    }

}
