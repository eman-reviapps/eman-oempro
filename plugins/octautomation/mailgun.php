<?php
function Mailgun_SendMessage($MailgunDomain, $MailgunAPIKey, $UserID, $MessageID, $PersonID, $To, $FromName, $FromEmail, $ReplyToName, $ReplyToEmail, $Subject, $PlainContent, $HTMLContent)
{
	$Parameters = array(
		'from' => $FromName . ' <' . $FromEmail . '>',
		'h:Reply-To' => ($ReplyToEmail != '' ? $ReplyToName . ' <' . $ReplyToEmail . '>' : $FromName . ' <' . $FromEmail . '>'),
		'to' => $To,
		'subject' => $Subject,
		'text' => $PlainContent,
		'html' => $HTMLContent,
		'o:tracking' => true,
		'o:tracking-clicks' => true,
		'o:tracking-opens' => true,
		'h:X-Mailgun-Variables' => json_encode(array('Automation-Email-ID' => implode('/', sl_generate_encrypted_parameter(array($UserID, $MessageID, $PersonID), 5))))
	);

	$Response = Mailgun_TalkToServer('https://api.mailgun.net/v2/'.$MailgunDomain.'/messages', $Parameters, 'POST', true, 'api', $MailgunAPIKey, 20, false);

	if ($Response[0] != true)
	{
		return false;
	}
	else
	{
		$Response = json_decode($Response[1]);

		if (isset($Response->id) == false) return false;

		return $Response->id;
	}
}

function Mailgun_TalkToServer($URL, $Parameters, $HTTPRequestType = 'POST', $HTTPAuth = false, $HTTPAuthUsername = '', $HTTPAuthPassword = '', $ConnectTimeOutSeconds = 60, $ReturnHeaders = false)
{
	$Parameters = http_build_query($Parameters);

	$CurlHandler = curl_init();
	curl_setopt($CurlHandler, CURLOPT_URL, $URL);

	if ($HTTPRequestType == 'GET')
	{
		curl_setopt($CurlHandler, CURLOPT_HTTPGET, true);
	}
	elseif ($HTTPRequestType == 'PUT')
	{
		curl_setopt($CurlHandler, CURLOPT_PUT, true);
	}
	elseif ($HTTPRequestType == 'DELETE')
	{
		curl_setopt($CurlHandler, CURLOPT_CUSTOMREQUEST, 'DELETE');
	}
	else
	{
		curl_setopt($CurlHandler, CURLOPT_POST, true);
		curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $Parameters);
	}

	curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($CurlHandler, CURLOPT_CONNECTTIMEOUT, $ConnectTimeOutSeconds);
	curl_setopt($CurlHandler, CURLOPT_TIMEOUT, $ConnectTimeOutSeconds);
	curl_setopt($CurlHandler, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
	curl_setopt($CurlHandler, CURLOPT_SSL_VERIFYPEER, false);

	// The option doesn't work with safe mode or when open_basedir is set.
	// if ((ini_get('safe_mode') != false) && (ini_get('open_basedir') != false))
	// 	{
	@curl_setopt($CurlHandler, CURLOPT_FOLLOWLOCATION, true);
	@curl_setopt($CurlHandler, CURLOPT_MAXREDIRS, 5);
	// }

	if ($ReturnHeaders == true)
	{
		curl_setopt($CurlHandler, CURLOPT_HEADER, true);
	}
	else
	{
		curl_setopt($CurlHandler, CURLOPT_HEADER, false);
	}

	if ($HTTPAuth == true)
	{
		curl_setopt($CurlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($CurlHandler, CURLOPT_USERPWD, $HTTPAuthUsername . ':' . $HTTPAuthPassword);
	}

	$RemoteContent = curl_exec($CurlHandler);

	if (curl_error($CurlHandler) != '')
	{
		return array(false, curl_error($CurlHandler));
	}

	curl_close($CurlHandler);

	return array(true, $RemoteContent);
}