/*
* To minimize and obfuscate, use Google's service:
* http://closure-compiler.appspot.com/home
* NOTE: Choose "simple" optimization method
*
* */
(function () {
	function loadScript(url, callback) {
		var script = document.createElement("script")
		script.type = "text/javascript";

		if (script.readyState) { //IE
			script.onreadystatechange = function () {
				if (script.readyState == "loaded" || script.readyState == "complete") {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else { //Others
			script.onload = function () {
				callback();
			};
		}

		script.src = url;
		document.getElementsByTagName("head")[0].appendChild(script);
	}

	function loadjQuery(callback) {
		loadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js", function () {
			$.noConflict();
			(function ($) {
				callback($);
			})(jQuery);
		});
	}

	function initialize($) {
		var widget = {};
		var $body = $('body');

		widget.showTime = function() {
			var parameters = {};

			$.each(window.email_automation_settings, function (key, value) {
				if (key == 'extra_info') {
					$.each(value, function(extra_info_key, extra_info_value) {
						if (typeof(extra_info_value) == 'string' || typeof(extra_info_value) == 'number') {
							parameters[extra_info_key] = extra_info_value;
						}
					});
				} else {
					if (typeof(value) == 'string' || typeof(value) == 'number') {
						parameters[key] = value;
					}
				}
			});

			parameters['web_browser'] = BrowserDetect.browser;
			parameters['web_browser_version'] = BrowserDetect.version;
			parameters['operating_system'] = BrowserDetect.OS;

			if (typeof(window.email_automation_settings.automation_user_id) != 'undefined') {
				var url = url = window.email_automation_settings.automation_url;

				$.ajax({
					url: url,
					type: 'GET',
					contentType: 'jsonp',
					data:parameters,
					dataType: 'jsonp'
				}).done(function(response) {
//						console.log(response);
				});
			}
		}

		widget.showTime();
	}

	if (typeof jQuery == 'function' && (jQuery.fn.jquery.match(/^1\.9\.[\d]$/im) !== null)) {
		initialize(jQuery);
	} else {
		loadjQuery(initialize);
	}
})();

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();