/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.fullPage = true;
	config.extraPlugins = 'autogrow,slpersonalizer,slplaintext,sltester_engage,codemirror,sharedspace,tableresize';
	config.codemirror_theme = 'blackboard';
	config.autoGrow_onStartup = true;
	config.removePlugins =  'templates,magicline';
	config.sltemplates_replaceContent = false;
	config.allowedContent = true;
	// config.extraAllowedContent = '*[*](*);div;table;tr;td;th';
	config.format_tags = 'p;h1;h2;h3';
	config.sltemplates_files = [];
	config.entities = false;
	config.sharedSpaces = {
		top: 'CkeditorToolbar'
	};
	config.toolbar = [
		{ name: 'document',    items : [ 'Source', '-', 'slplaintext' ] },
		{ name: 'clipboard',   items : [ 'Undo','Redo' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'links',       items : [ 'Link','Unlink' ] },
		{ name: 'insert',      items : [ 'Image','SpecialChar','Table' ] },
		{ name: 'insert',      items : [ 'SendloopMediaLibrary' ] },
		'/',
		{ name: 'styles',      items : [ 'Format','Font','FontSize' ] },
		{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
		{ name: 'content',		items: [ 'slpersonalizer' ] },
		{ name: 'insert',		items: [ 'SlTesterEngage' ] }
	];
};
