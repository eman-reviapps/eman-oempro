/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.fullPage = true;
	config.extraPlugins = 'autogrow,slpersonalizer,slplaintext,sllinks,slrss,slrater,slplainconverter,sltester,slfilepickerio,slblocks,codemirror,sharedspace,sltemplates,tableresize';
	config.codemirror_theme = 'blackboard';
	config.autoGrow_onStartup = true;
	config.removePlugins =  'templates,magicline';
	config.sltemplates_replaceContent = false;
	config.allowedContent = true;
	// config.extraAllowedContent = '*[*](*);div;table;tr;td;th';
	config.format_tags = 'p;h1;h2;h3';
	config.sltemplates_files = [];
	config.entities = false;
	config.sharedSpaces = {
		top: 'CkeditorToolbar'
	};
	config.toolbar = [
		// { name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		// { name: 'document',    items : [ 'Source','Maximize' ] },
		{ name: 'document',    items : [ 'Source', '-', 'slplaintext', '-', 'slplainconverter' ] },
		// { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'clipboard',   items : [ 'Undo','Redo' ] },
		// { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		// { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		// '/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
		// { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		// { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'links',       items : [ 'Link','Unlink' ] },
		// { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
		{ name: 'insert',      items : [ 'Image','SpecialChar','Table' ] },
		{ name: 'insert',      items : [ 'SendloopMediaLibrary' ] },
		'/',
		// { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'styles',      items : [ 'Format','Font','FontSize' ] },
		{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
		{ name: 'content',		items: [ 'slpersonalizer','slrss','sllinks','slrater' ] },
		{ name: 'content',		items: [ 'sltester' ] },
		{ name: 'content',		items: [ 'slinbox' ] }
		// { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
		// { name: 'tools',       items : [ 'Maximize' ] }
	];
};
