var slreaterTimer = null;

function setupSlRaterTimer(editor) {
	slreaterTimer = setTimeout(function() {
		if (editor.checkDirty()) {
			editor.resetDirty();

			$.post('/campaigns/create/ajax_spam_score', {
					subject:$('#Subject').val(),
					plainContent:$('#BuilderPlainText').val(),
					htmlContent:editor.getData()
				},
				function(data) {
					$('.cke_button__slrater').removeClass('status_normal')
						.removeClass('status_warning')
						.removeClass('status_critical')
						.addClass('status_' + data.status);

					setupSlRaterTimer(editor);
			});
		} else {
			setupSlRaterTimer(editor);
		}
	}, 1000);
}

CKEDITOR.plugins.add('slrater', {
	init: function(editor) {
		editor.addCommand('spamScoreDialog', new CKEDITOR.dialogCommand('spamScoreDialog', {modes: { wysiwyg:1,source:0,plaintext:1 }}));
		CKEDITOR.dialog.add('spamScoreDialog', function(editor) {
			return {
				title: 'SpamScore',
				resizable: CKEDITOR.DIALOG_RESIZE_HEIGHT,
				width: 400,
				minHeight: 200,
				buttons: [CKEDITOR.dialog.okButton],
				onShow: function() {
					var that = this;
					$('.cke_dialog_ui_vbox_child', this.parts.contents.$).html('<p>Loading SPAM Score, please wait...');
					$.post('/campaigns/create/spam_test', {
										subject:$('#Subject').val(),
										plainContent:$('#BuilderPlainText').val(),
										htmlContent:editor.getData()
									}, function(html) {
						$('.cke_dialog_ui_vbox_child', that.parts.contents.$).html(html);
					});
				},
				contents: [
					{
						id: 'tab-basic',
						label: 'Basic information',
						elements: [
							{
								type: 'html',
								html: '&nbsp;'
							}
						]
					}
				]
			}
		});
		
		var button = new CKEDITOR.ui.button({
			label: 'SPAM Score',
			command: 'spamScoreDialog'
		});
		editor.ui.addButton('slrater', button);
		setupSlRaterTimer(editor);
	}
});