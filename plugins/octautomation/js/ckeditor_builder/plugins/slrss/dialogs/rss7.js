CKEDITOR.dialog.add('slrssDialog', function (editor) {
	return {
		title: 'Enter the RSS feed URL to fetch',
		minWidth: 300,
		minHeight: 50,
		contents: [
			{
				id: 'tab-basic',
				label: '',
				elements: [
					{
						type: 'text',
						id: 'text',
						label: '',
						validate: CKEDITOR.dialog.validate.notEmpty( "RSS feed URL cannot be empty" )
					}
				]
			}
		],
		onOk: function() {
			var dialog = this;

			if (editor.mode == 'wysiwyg') {
				editor.insertHtml("%RSS:" + dialog.getValueOf('tab-basic', 'text') + ":20%<br>\n");
				editor.insertHtml('<strong><a href="%RSS:POST-LINK%" target="_blank">%RSS:TITLE:400%</strong></a><br>' + "\n");
				editor.insertHtml("<small>Published on %RSS:PUBLISH-DATE% by %RSS:AUTHOR%</small><br>\n");
				editor.insertHtml("%RSS:CONTENT:1000%<br>\n");
				editor.insertHtml("%RSS:END%\n");
			} else if (editor.mode == 'plaintext') {

			}
		}
	};
});