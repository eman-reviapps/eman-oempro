CKEDITOR.plugins.add('slrss', {
	init: function(editor) {
		editor.addCommand( 'slrssDialog', new CKEDITOR.dialogCommand( 'slrssDialog', {modes: { wysiwyg:1,source:0,plaintext:1 }} ) );
		CKEDITOR.dialog.add( 'slrssDialog', this.path + 'dialogs/rss7.js' );

		editor.addCommand('slrss', {
			modes: { wysiwyg:1,source:0,plaintext:1 },
			exec: function() {
				editor.execCommand('slrssDialog');
			}
		});
		var button = new CKEDITOR.ui.button({
			label: 'RSS',
			command: 'slrss'
		});
		editor.ui.addButton('slrss', button);
	}
});
