
CKEDITOR.plugins.add('slblocks', {
	onLoad: function() {
	},
	init: function(editor) {
		var config = editor.config,
			templateRead = false,
			templateDoc = null,
			elementFromMouse = (function() {
				function elementFromPoint( doc, mouse ) {
					return new CKEDITOR.dom.element( doc.$.elementFromPoint( mouse.x, mouse.y ) );
				}

				return function( doc, mouse, ignoreBox, forceMouse ) {

					var element = elementFromPoint( doc, mouse );

					// Return nothing if:
					//	\-> Element is not HTML.
					if ( !( element && element.type == CKEDITOR.NODE_ELEMENT && element.$ ) ) {
						return null;
					}

					return element;
				};
			})();

		editor.addCommand('optionsDialog', new CKEDITOR.dialogCommand('optionsDialog'));
		CKEDITOR.dialog.add('optionsDialog', function ( api ) {
			return {
				title: 'Options',
				minWidth: 400,
				minHeight: 200,
				contents: [
					{
						id: 'tab1',
						label: 'Gallery Options',
						title: 'Gallery Options',
						expand: true,
						padding: 0,
						elements: [
							{
								type: 'select',
								id: 'count',
								label: 'Number of images',
								items: [['1'], ['2'], ['3']],
								onChange: function(api) {}
							}
						]
					}
				],
				buttons: [ CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton ],
				onOk: function() {
					var countSelect = this.getContentElement('tab1', 'count'),
						countValue = countSelect.getValue(),
						countable = $(CKEDITOR.config.slblocks_optionsBlock.$),
						selector = countable.attr('countable-selector'),
						selectorParent = countable.attr('countable-parent-selector'),
						targetParent = $(selectorParent, countable),
						currentCountables = $(selector, countable),
						uid = countable.attr('countable-uid');

					var cacheElements = CKEDITOR.config.slblocks_countableCache[uid];

					if (currentCountables.length == countValue) {
						return;
					} else if (currentCountables.length < countValue) {
						for (var i=0;i<countValue;i++) {
							var elIdx = $(selector + ':eq('+i+')', countable);
							if (elIdx.length > 0) continue;
							targetParent.append(cacheElements[i]);
						}
					} else {
						for (var i=currentCountables.length;i>countValue-1;i--) {
							$(currentCountables[i]).remove();
						}
					}
				}
			};
		});

		editor.on('contentDom', addListeners, this);

		CKEDITOR.addCss(
			'.cke_show_border {border:none !important;} .cke_show_border td{border:none !important;}' +
			'#slblocktool {' +
			'cursor:pointer;' +
			'border:1px solid #a6a6a6 !important;border-radius:3px;' +
			'background-color:#e4e4e4;' +
			'height:24px;' +
			'padding:0;' +
			'font-family:Arial,sans-serif;' +
			'font-size:12px;' +
			'position:absolute;top:10px;right:10px;' +
			'box-shadow:0 1px 0 rgba(255,255,255,.5),0 0 2px rgba(255,255,255,.15) inset,0 1px 0 rgba(255,255,255,.15) inset;' +
			'background-image:linear-gradient(top,#fff,#e4e4e4);' +
			'background-image:-webkit-linear-gradient(top,#fff,#e4e4e4);' +
			'background-image:-moz-linear-gradient(top,#fff,#e4e4e4);' +
			'resize:none;' +
			'}' +
			'#slblocktool span {' +
			'color:#333;' +
			'border-right:1px solid #ddd;' +
			'text-decoration:none;' +
			'display:inline-block;' +
			'line-height:24px;' +
			'padding:0 10px;' +
			'}' +
			'#slblocktool span:hover {' +
			'background-color:#ccc !important;' +
			'background-image:linear-gradient(top,#e4e4e4,#ddd);' +
			'background-image:-webkit-linear-gradient(top,#e4e4e4,#ddd);' +
			'background-image:-moz-linear-gradient(top,#e4e4e4,#ddd);' +
			'}' +
			'[layout] { ' +
			'position:relative;' +
			'}'
		);

		function isTemplateSet() {
			if (!config.slblocks_template || config.slblocks_template == '')
				return false;

			return true;
		}

		function readTemplate() {
			if (! isTemplateSet()) return;

			var doc = $(config.slblocks_template),
				repeaters = $('[repeater]', doc),
				countables = $('[countable-selector]', doc);

			$.each(countables, function(i) {
				var selector = $(this).attr('countable-selector'),
					els = $(selector, this),
					uid = $(this).attr('countable-uid');

				var temp = [];
				els.each(function() {
					temp.push(this);
				});
				CKEDITOR.config.slblocks_countableCache[uid] = temp;
			});

			$.each(repeaters, function(i) {
				var layouts = $('[layout]', this), templates = [];
				$.each(layouts, function(j) {
					$(this).attr('repeater-id', i);
					var wrapped = $('<div>').append($(this).clone());
					templates.push({
						title: $(this).attr('layout-label'),
						image: '',
						description: '',
						html: $(wrapped).html().replace('\n', '')
					});
				});
				CKEDITOR.sladdTemplates(i, {
					imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),
					templates: templates
				});
			});

			templateRead = true;
		}

		function addTemplateSourceAndInit() {
			if (! isTemplateSet() && ! templateRead) return;

			var docu = $(CKEDITOR.instances.builder.window.getFrame().$).contents();
			// var docu = $(config.slblocks_template);
			var repeaters = docu.find('[repeater]');
			$.each(repeaters, function(i) {
				var layouts = $('[layout]:not([repeater-id])', this), templates = [];
				$.each(layouts, function(j) {
					$(this).attr('repeater-id', i);
					if (j > 0) $(this).remove();
				});
			});
		}

		function addListeners() {
			if (! templateRead && isTemplateSet()) {
				readTemplate();
				addTemplateSourceAndInit();
			}

			var editable = editor.editable(),
				doc = editor.document,
				win = editor.window,
				checkMouseTimer, tool, currentBlock;

			tool = CKEDITOR.dom.element.createFromHtml( '<div contenteditable="false" data-sl-cleanup="true" id="slblocktool"><span id="slblocktool-add" contenteditable="false">+ Add content</span><span contenteditable="false" id="slblocktool-remove">- Remove</span><span contenteditable="false" id="slblocktool-options">Options</span></div>', doc);
			$(tool.$).on('click', 'span', function(ev) {
				if (currentBlock != null) {
					var id = $(this).attr('id');
					if (id == 'slblocktool-add') {
						var range = editor.createRange();
						range.setStartAfter(currentBlock);
						range.select();
						editor.execCommand('sltemplates');
						tool.remove();
					} else if (id =='slblocktool-remove') {
						var parent = currentBlock.getParent();
						var layouts = $('[layout]', parent.$);
						if (layouts.length < 2) return;
						tool.remove();
						currentBlock.remove();
					} else if (id =='slblocktool-options') {
						CKEDITOR.config.slblocks_optionsBlock = currentBlock;
						editor.execCommand('optionsDialog');
						tool.remove();
					}
				}
			});

			editable.attachListener(doc, 'mousemove', function(event) {
				checkMouseTimeoutPending = true;
				if ( editor.mode != 'wysiwyg' || editor.readOnly || checkMouseTimer )
					return;

				var mouse = {
					x: event.data.$.clientX,
					y: event.data.$.clientY
				};

				checkMouseTimer = setTimeout( function() {
					checkMouse(mouse);
				}, 30 );

				function findParentWithClass(el, className) {
					var parent = el.getParent();
					if (parent.getName() == 'body') return false;
					if (parent.hasClass(className)) return parent;
					return findParentWithClass(parent, className);
				}

				function findParentWithAttribute(el, attrName) {
					var parent = el.getParent();
					if (! parent || ! parent.getName || parent.getName() == 'body' || parent.getName() == 'window') return false;
					if (parent.hasAttribute(attrName)) {
						return parent;
					}
					return findParentWithAttribute(parent, attrName);
				}

				function checkMouse(mouse) {
					checkMouseTimer = null;

					if (checkMouseTimeoutPending 								//	-> There must be an event pending.
						&& editor.focusManager.hasFocus 						// 	-> Editor must have focus.
						&& (element = elementFromMouse(doc, mouse, true)))	 	// 	-> There must be valid element.
					{
						var parent = null;
						if (element.hasAttribute('layout') == true) {
							parent = element;
						} else if ((parent = findParentWithAttribute(element, 'layout')) == false) {
							parent = null;
						}
						if (parent != null) {
							if (currentBlock == null || currentBlock.$ != parent.$) {
								var repeaterId = parent.getAttribute('repeater-id');
								tool.remove();
								tool.appendTo(parent);

								var parentParent = $(parent.getParent().$);
								var layouts = $('[layout]', parentParent);
								if (layouts.length < 2)  {
									$('#slblocktool-remove', tool.$).hide();
								} else {
									$('#slblocktool-remove', tool.$).show();
								}

								if (! parent.hasAttribute('countable-selector')) {
									$('#slblocktool-options', tool.$).hide();
								} else {
									$('#slblocktool-options', tool.$).show();
								}

								currentBlock = parent;
								editor.config.sltemplates = repeaterId;
							}
						} else {
							tool.remove();
							currentBlock = null;
						}
						checkMouseTimeoutPending = false;
					}
				}
			});			
		}
	}
});

CKEDITOR.config.slblocks_template = '';
CKEDITOR.config.slblocks_optionsBlock = null;
CKEDITOR.config.slblocks_countableCache = {};
