CKEDITOR.plugins.add('slpersonalizer', {
	requires: 'richcombo',
	onLoad: function() {
	},
	beforeInit: function(editor) {
		CKEDITOR.addCss('span[data-sl-noneditable] {border:1px dotted #000;padding:1px;}');
	},
	init: function(editor) {
		var config = editor.config;
		var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter,
				htmlFilter = dataProcessor && dataProcessor.htmlFilter;

		if ( dataFilter ) {
			dataFilter.addRules({
				text: function( text ) {
					return text.replace(/%[^%]+%/g, function( match ) {
						return CKEDITOR.plugins.slpersonalizer.createMergeTag(editor, null, match, 1);
					});
				}
			});
		}
		if ( htmlFilter ) {
			htmlFilter.addRules({
				elements: {
					'span': function( element ) {
						if ( element.attributes && element.attributes[ 'data-sl-noneditable' ] )
							delete element.name;
					}
				}
			});
		}

		editor.ui.addRichCombo('slpersonalizer', {
			modes: { wysiwyg:1,source:0,plaintext:1 },
			label: 'Merge Tags',
			title: 'Merge Tags',
			panel: {
				css: [CKEDITOR.skin.getPath( 'editor' )].concat(config.contentsCss),
				multiSelect: false,
				attributes: {'aria-label': 'Personalize', 'width': '200px'}
			},
			init: function() {
				for (eachTag in config.slpersonalizer.tags) {
					var currentTag = config.slpersonalizer.tags[eachTag];
					if (currentTag.groupLabel !== undefined) {
						this.startGroup(currentTag.groupLabel);
					}
					if (currentTag.tags !== undefined) {
						for (tag1 in currentTag.tags) {
							this.add(currentTag.tags[tag1].value, currentTag.tags[tag1].label, currentTag.tags[tag1].label);
						}
					} else {
						this.add(config.slpersonalizer.tags[eachTag].value, config.slpersonalizer.tags[eachTag].label, config.slpersonalizer.tags[eachTag].label);
					}
				}
			},
			onClick: function(value) {
				CKEDITOR.plugins.slpersonalizer.createMergeTag(editor, null, value);
			}
		});
	}
});

CKEDITOR.plugins.slpersonalizer = {
	createMergeTag: function(editor, oldElement, text, isGet) {
		if (editor.mode !== 'plaintext') {
			var element = new CKEDITOR.dom.element( 'span', editor.document );
			element.setAttributes({
				contenteditable: 'false',
				'data-sl-noneditable': 1
			});
			element.setText(text);

			if (isGet) return element.getOuterHtml();

			if ( oldElement ) {
				if ( CKEDITOR.env.ie ) {
					element.insertAfter( oldElement );
					// Some time is required for IE before the element is removed.
					setTimeout( function() {
						oldElement.remove();
						element.focus();
					}, 10 );
				} else {
					element.replace( oldElement );
				}
			} else {
				editor.insertElement( element );
			}
		} else {
			editor.insertText(text);
		}
	}
}