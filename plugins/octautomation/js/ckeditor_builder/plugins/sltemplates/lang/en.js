﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang( 'sltemplates', 'en', {
	button: 'Blocks',
	emptyListMsg: '(No blocks defined)',
	insertOption: 'Replace actual contents',
	options: 'Block Options',
	selectPromptMsg: 'Please select the block to insert into the content',
	title: 'Content blocks'
});
