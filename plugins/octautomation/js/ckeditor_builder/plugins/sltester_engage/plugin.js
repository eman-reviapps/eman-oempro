CKEDITOR.plugins.add('sltester_engage', {
		init: function(editor) {
			editor.addCommand('testerDialog', new CKEDITOR.dialogCommand('testerDialog', {
				modes: { wysiwyg:1,source:0,plaintext:1 }
			}));
			CKEDITOR.dialog.add('testerDialog', function(editor) {
				return {
					title: 'Test your email',
					resizable: CKEDITOR.DIALOG_RESIZE_NONE,
					width: 200,
					height: 80,
					onCancel: function() {
						jQueryAutomation('#sltesterEmailTestMessage').text('').removeClass('success').removeClass('error').hide();
					},
					buttons: [CKEDITOR.dialog.cancelButton,
							{
								type: 'button',
								label: 'Send email',
								id: 'popupTestEmailButton',
								'class': 'cke_dialog_ui_button_ok',
								onClick: function(evt) {
									var ruleColumns = jQueryAutomation('.rule-column'),
										ruleOperators = jQueryAutomation('.column-operator'),
										ruleValues = jQueryAutomation('.column-value'),
										rules = [];
									ruleColumns.each(function(idx) {
										var rule = {};
										rule.column = jQueryAutomation(this).val();
										rule.operator = jQueryAutomation(ruleOperators[idx]).val();
										rule.val = jQueryAutomation(ruleValues[idx]).val();
										rules.push(rule);
									});

									var dialog = evt.data.dialog;
									jQueryAutomation('#sltesterEmailTestMessage').text('Sending preview email, please wait...').removeClass('success').removeClass('error').show();
									jQueryAutomation.post(plugInUrl, {
										'FromName':jQueryAutomation('#FromName').val(),
										'FromEmail':jQueryAutomation('#FromEmail').val(),
										'ReplyToName':jQueryAutomation('#ReplyToName').val(),
										'ReplyToEmail':jQueryAutomation('#ReplyToEmail').val(),
										'Subject':jQueryAutomation('#Subject').val(),
										'PlainContent':jQueryAutomation('#BuilderPlainText').val(),
										'HTMLContent':editor.getData(),
										'ToEmail':dialog.getValueOf('tab-basic', 'testEmailAddress'),
										'Rules':rules,
										'MatchType':jQueryAutomation('#SegmentOperator').val()
									}, function(data) {
										jQueryAutomation('#sltesterEmailTestMessage').html(data.message).addClass(data.type).show();
									});
								}
							}
						],
					contents: [
						{
							id: 'tab-basic',
							label: 'Basic information',
							elements: [
								{
									type: 'text',
									id: 'testEmailAddress',
									label: 'Where to send the test email?',
									validate: CKEDITOR.dialog.validate.regex( /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, "Please enter a valid email address."),
									'default': editor.config.slpreview_email
								},
								{
									type: 'html',
									html: '<p id="sltesterEmailTestMessage"></p>'
								}
							]
						}
					]
				}
			});

			editor.ui.addButton('SlTesterEngage', {
				label: 'Send Test Email',
				command: 'testerDialog'
			});
		}
});