CKEDITOR.plugins.add('sltester', {
	init: function(editor) {
		var config = editor.config;
		editor.ui.addRichCombo('sltester', {
			modes: { wysiwyg:1,source:0,plaintext:1 },
			label: 'Test your email',
			title: 'Test your email',
			panel: {
				css: [CKEDITOR.skin.getPath( 'editor' )].concat(config.contentsCss),
				multiSelect: false
			},
			init: function() {
				this.add('email', 'Send a test email', 'Send a test email');
				this.add('browser', 'Test on your browser', 'Test on your browser');
			},
			onClick: function(value) {
				if (value == 'browser') {
					autoSaveEmail(function() {
						window.open('/campaigns/create/preview_on_browser');
					});
				} else {
					editor.execCommand('testerDialog');
				}
			},
			onOpen: function() {
			},
			onClose: function() {
			}
		});


		editor.addCommand('testerDialog', new CKEDITOR.dialogCommand('testerDialog', {
			modes: { wysiwyg:1,source:0,plaintext:1 }			
		}));
		CKEDITOR.dialog.add('testerDialog', function(editor) {
			return {
				title: 'Test your email',
				resizable: CKEDITOR.DIALOG_RESIZE_NONE,
				width: 200,
				height: 80,
				onCancel: function() {
					$('#sltesterEmailTestMessage').text('').removeClass('success').removeClass('error').hide();
				},
				buttons: [CKEDITOR.dialog.cancelButton, 
						{
							type: 'button',
							label: 'Send email',
							id: 'popupTestEmailButton',
							'class': 'cke_dialog_ui_button_ok',
							onClick: function(evt) {
								var dialog = evt.data.dialog;
								$('#sltesterEmailTestMessage').text('Sending preview email, please wait...').removeClass('success').removeClass('error').show();
								$.post('/campaigns/create/ajax_preview_email', {
									toEmail:dialog.getValueOf('tab-basic', 'testEmailAddress'),
									subject:$('#Subject').val(),
									plainContent:$('#BuilderPlainText').val(),
									htmlContent:editor.getData()
								}, function(data) {
									$('#sltesterEmailTestMessage').html(data.message).addClass(data.type).show();
									// var dialog = evt.data.dialog;
									// if ( dialog.fire( 'ok', { hide: true } ).hide !== false ) dialog.hide();
								});
							}
						}
					],
				contents: [
					{
						id: 'tab-basic',
						label: 'Basic information',
						elements: [
							{
								type: 'text',
								id: 'testEmailAddress',
								label: 'Where to send the test email?',
								validate: CKEDITOR.dialog.validate.regex( /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, "Please enter a valid email address."),
								'default': editor.config.slpreview_email
							},
							{
								type: 'html',
								html: '<p>To send previews to multiple email addresses,<br>seperate email addresses with a comma.</p>'
							},
							{
								type: 'html',
								html: '<p id="sltesterEmailTestMessage"></p>'
							}
						]
					}
				]
			}
		});
		
		var button = new CKEDITOR.ui.button({
			label: 'Test your email',
			command: 'testerDialog'
		});
		// editor.ui.addButton('sltester', button);
	}
});