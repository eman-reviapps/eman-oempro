CKEDITOR.plugins.add('slplainconverter', {
	init: function(editor) {
		editor.addCommand('slplainconverter', {
			modes: { wysiwyg:1,source:0,plaintext:0 },
			exec: function() {
				autoSaveEmail(function() {
					window.location.href="/campaigns/create/builder/convert_to_plain_text";
				});

			}
		});
		var button = new CKEDITOR.ui.button({
			label: 'HTML to plain text',
			command: 'slplainconverter'
		});
		editor.ui.addButton('slplainconverter', button);
	}
});
