CKEDITOR.plugins.add('sllinks', {
	requires: 'richcombo',
	onLoad: function() {
	},
	init: function(editor) {
		var config = editor.config;
		var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter,
				htmlFilter = dataProcessor && dataProcessor.htmlFilter;

		editor.addCommand( 'sllinksDialog', new CKEDITOR.dialogCommand( 'sllinksDialog', {modes: { wysiwyg:1,source:0,plaintext:1 }} ) );
		CKEDITOR.dialog.add( 'sllinksDialog', this.path + 'dialogs/link.js' );

		editor.ui.addRichCombo('sllinks', {
			modes: { wysiwyg:1,source:0,plaintext:1 },
			label: 'Links',
			title: 'Links',
			panel: {
				css: [CKEDITOR.skin.getPath( 'editor' )].concat(config.contentsCss),
				multiSelect: false,
				attributes: {'aria-label': 'Links', 'width': '200px'}
			},
			init: function() {
				for (eachLink in config.sllinks.links) {
					var currentLink = config.sllinks.links[eachLink];
					if (currentLink.groupLabel !== undefined) {
						this.startGroup(currentLink.groupLabel);
					}
					if (currentLink.links !== undefined) {
						for (link1 in currentLink.links) {
							this.add(currentLink.links[link1].value, currentLink.links[link1].label, currentLink.links[link1].label);
						}
					} else {
						this.add(config.sllinks.links[eachLink].value, config.sllinks.links[eachLink].label, config.sllinks.links[eachLink].label);
					}
				}
			},
			onClick: function(value) {
				CKEDITOR.plugins.sllinks.addLink(editor, value);
			}
		});
	}
});

CKEDITOR.plugins.sllinks = {
	selectedLink: undefined,
	addLink: function(editor, linkValue) {
		if (editor.mode == 'wysiwyg') {
			var selection = editor.getSelection(),
					range = selection.getRanges( 1 )[ 0 ];
			
			if (range.collapsed) {
				CKEDITOR.plugins.sllinks.selectedLink = linkValue;
				editor.execCommand('sllinksDialog');
			} else {
				var text = selection.getSelectedText();
						link = editor.document.createElement('a');

				link.setText(text);
				link.setAttribute('href', linkValue);

				editor.insertElement(link);
				// range.insertNode( link );
				// range.selectNodeContents( link );
			}
		} else if (editor.mode == 'plaintext') {
			if (linkValue == '%Link:Unsubscribe%') {
				var linkText = "\n\nUse the following link to unsubscribe:\n" + linkValue + "\n\n" ;
				editor.insertText(linkText);
			} else {
				editor.insertText(linkValue);
			}
		}
	}
}