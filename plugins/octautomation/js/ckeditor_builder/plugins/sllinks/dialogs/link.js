CKEDITOR.dialog.add('sllinksDialog', function (editor) {
	return {
		title: 'Link Text',
		minWidth: 300,
		minHeight: 50,
		contents: [
			{
				id: 'tab-basic',
				label: '',
				elements: [
					{
						type: 'text',
						id: 'text',
						label: '',
						validate: CKEDITOR.dialog.validate.notEmpty( "Link text field cannot be empty" )
					}
				]
			}
		],
		onOk: function() {
			var dialog = this;
			if (editor.mode == 'wysiwyg') {
				link = editor.document.createElement('a');
				link.setAttribute('href', CKEDITOR.plugins.sllinks.selectedLink);
				link.setText(dialog.getValueOf('tab-basic', 'text'));
				editor.insertElement(link);
			}
		}
	};
});