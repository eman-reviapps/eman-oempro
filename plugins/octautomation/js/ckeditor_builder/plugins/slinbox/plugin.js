CKEDITOR.plugins.add('slinbox', {
	init: function(editor) {
		editor.addCommand('previewcommand', {
			modes: { wysiwyg:1,source:0,plaintext:1 },
			exec: function() {
				autoSaveEmail(function() {
					window.open('/campaigns/create/design_test');
				});
			}
		});
		var button = new CKEDITOR.ui.button({
			label: 'Inbox Preview',
			command: 'previewcommand'
		});
		editor.ui.addButton('slinbox', button);
		setupSlRaterTimer(editor);

	}
});