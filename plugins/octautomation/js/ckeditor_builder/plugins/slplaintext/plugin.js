var plainEditable, plainEditableTextarea;
	CKEDITOR.plugins.add( 'slplaintext', {
		init: function( editor ) {
			editor.addMode( 'plaintext', function( callback ) {
				var contentsSpace = editor.ui.space( 'contents' );
				plainEditableTextarea = contentsSpace.getDocument().createElement( 'textarea' );

				plainEditableTextarea.setStyles(
					CKEDITOR.tools.extend({
						// IE7 has overflow the <textarea> from wrapping table cell.
						width: CKEDITOR.env.ie7Compat ? '99%' : '100%',
						height: '100%',
						resize: 'none',
						outline: 'none',
						'text-align': 'left',
						'font-family': 'monospace',
						'font-size': '14px',
						'line-height': '21px',
						'padding': '18px'
//						'background': 'url(/media/images/bg-plain-text.png) repeat-y top left'
					},
					CKEDITOR.tools.cssVendorPrefix( 'tab-size', 4 ) ) );

				// Make sure that source code is always displayed LTR,
				// regardless of editor language (#10105).
				plainEditableTextarea.setAttribute( 'dir', 'ltr' );

				plainEditableTextarea.addClass( 'cke_reset' );
				editor.ui.space( 'contents' ).append( plainEditableTextarea );

				var plainEditable = editor.editable(new plainTextEditable( editor, plainEditableTextarea ));

				// Fill the textarea with the current editor data.
				// plainEditable.setData('asdf');

				// Having to make <textarea> fixed sized to conquer the following bugs:
				// 1. The textarea height/width='100%' doesn't constraint to the 'td' in IE6/7.
				// 2. Unexpected vertical-scrolling behavior happens whenever focus is moving out of editor
				// if text content within it has overflowed. (#4762)
				if ( CKEDITOR.env.ie ) {
					plainEditable.attachListener( editor, 'resize', onResize, plainEditable );
					plainEditable.attachListener( CKEDITOR.document.getWindow(), 'resize', onResize, plainEditable );
					CKEDITOR.tools.setTimeout( onResize, 0, plainEditable );
				}

				jQueryAutomation(plainEditable.$).on('keyup', function() {
					var value = plainEditableTextarea.getValue();
					$('#BuilderPlainText').text(value);
				});

				CKEDITOR.instances.builder.on('insertText', function() {
					if (CKEDITOR.instances.builder.mode == 'plaintext') {
						var value = plainEditableTextarea.getValue();
						$('#BuilderPlainText').text(value);
					}
				});

				editor.fire( 'ariaWidget', this );
				callback();
			});

			editor.addCommand( 'plaintext', {
				modes: { wysiwyg:1,source:0,plaintext:1 },
				editorFocus: false,
				readOnly: 1,
				exec: function( editor ) {
					if (editor.mode == 'plaintext') {
						var value = plainEditableTextarea.getValue();
						$('#BuilderPlainText').text(value);
					}
					editor.getCommand( 'plaintext' ).setState( CKEDITOR.TRISTATE_DISABLED );
					editor.setMode( editor.mode == 'plaintext' ? 'wysiwyg' : 'plaintext' );
					if (editor.mode == 'plaintext') {
						plainEditableTextarea.setValue($('#BuilderPlainText').text());
						$(plainEditableTextarea.$).focus();
					}
				},
				canUndo: false				
			});

			if ( editor.ui.addButton ) {
				editor.ui.addButton( 'slplaintext', {
					label: 'Plain text',
					command: 'plaintext',
					toolbar: 'mode,10'
				});
			}

			editor.on( 'mode', function() {
				editor.getCommand( 'plaintext' ).setState( editor.mode == 'plaintext' ? CKEDITOR.TRISTATE_ON : CKEDITOR.TRISTATE_OFF );
			});

			function onResize() {
				// Holder rectange size is stretched by textarea,
				// so hide it just for a moment.
				this.hide();
				this.setStyle( 'height', this.getParent().$.clientHeight + 'px' );
				this.setStyle( 'width', this.getParent().$.clientWidth + 'px' );
				// When we have proper holder size, show textarea again.
				this.show();
			}


		}
	});

	var plainTextEditable = CKEDITOR.tools.createClass({
		base: CKEDITOR.editable,
		proto: {
			setData: function( data ) {
				this.setValue( data );
				// this.editor.fire( 'dataReady' );
			},

			getData: function() {
				return CKEDITOR.instances.builder.getData(1);
			},

			// Insertions are not supported in source plainEditable.
			insertHtml: function() {},
			insertElement: function() {},
			insertText: function(text) {
				$(this.$).insertAtCaret(text);
			},

			// Read-only support for textarea.
			setReadOnly: function( isReadOnly ) {
				this[ ( isReadOnly ? 'set' : 'remove' ) + 'Attribute' ]( 'readOnly', 'readonly' );
			},

			detach: function() {
				plainTextEditable.baseProto.detach.call( this );
				this.clearCustomData();
				this.remove();
			}
		}
	});

