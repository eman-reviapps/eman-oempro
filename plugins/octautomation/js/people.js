var timesSelect = '';
var campaignSelect = '';

Array.prototype.inArray = function (value) {
    var i;
    for (i = 0; i < this.length; i++) {
        if (this[i] === value) {
            return true;
        }
    }
    return false;
};

Array.prototype.removeFromArray = function (value) {
    var i, index;
    index = -1;
    for (i = 0; i < this.length; i++) {
        if (this[i] === value) {
            index = i;
            break;
        }
    }
    if (index === -1)
        return false;
    this.splice(index, 1);
    return true;
};

function isArray(input) {
    return typeof (input) == 'object' && (input instanceof Array);
}

var RuleBoard = {
    campaignSelect: '',
    timesSelect: '<select class="times-select"><option value="At most">' + lang['1408'] + '</option><option value="At least">' + lang['1409'] + '</option><option value="Only">' + lang['1410'] + '</option></select>',
    filterOperator: 'and',
    addSegmentRules: function (selectedSegmentId) {
        $('.add-segment-rules-menu').removeClass('open');
        var listId = $('#SearchList').val();
        var selectedSegmentObject = {};
        $.each(segments[listId], function (index, data) {
            if (data.id == selectedSegmentId) {
                selectedSegmentObject = data;
            }
        });
        var ruleArray = RuleBoard.convertSegmentRulesStringToArray(selectedSegmentObject.rules);
        RuleBoard.convertRuleArrayToRuleBoard(ruleArray, selectedSegmentObject);
        segmentId = selectedSegmentId;
    },
    init: function () {
        $('.main-action-with-menu').live('click', function () {
            $(this).addClass('open');
            return false;
        });
        $('.main-action-with-menu').live('mouseout', function (e) {
            if ($(e.relatedTarget).isChildOf('.main-action-with-menu') == false) {
                $(this).removeClass('open');
            }
        });
        $('.add-new-rule-menu ul a').live('click', function () {
            var parent = null;
            if ($(this).parent().isChildOf('li')) {
                var parentLi = $(this).parent().parent().parent().parent().parent();
                parent = $('ul.sub-group', parentLi);
            } else {
                parent = $('ul#rule-list');
            }
            RuleBoard.addRule($(this).attr('customfieldid'), $(this).text(), $(this).attr('fieldtype'), $(this).attr('validationmethod'), $(this).attr('values'), parent);
            $('.add-new-rule-menu').removeClass('open');
            return false;
        });
        $('.filter-operator-menu ul a').live('click', function () {
            RuleBoard.filterOperator = $(this).parent().attr('operator');
            RuleBoard.updateRuleSeparators();
            $('#filter-operator').text($(this).text());
            $('.filter-operator-menu').removeClass('open');
            return false;
        });
        $('#rule-list .delete').live('click', function () {
            var parentSubGroup = $(this).parent().parent('.sub-group');
            if ($(this).parent().next().hasClass('rule-separator')) {
                $(this).parent().next().remove();
            } else {
                if ($(this).parent().prev().hasClass('rule-separator')) {
                    $(this).parent().prev().remove();
                } else {
                    $('.specific-list-rule-separator').remove();
                }
            }
            $(this).parent().remove();
            if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length == 1) {
                var item = $('li.rule', parentSubGroup);
                parentSubGroup.parent().replaceWith(item);
            } else if (parentSubGroup.length > 0 && $('li.rule', parentSubGroup).length < 1) {
                parentSubGroup.parent().remove();
            }
            $('.rule-separator + .rule-separator').remove();
        });
        $('#rule-list .add').live('click', function () {
            RuleBoard.addRuleGroup($(this).parent(), true);
        });
        $('#clear-board-link').click(RuleBoard.clearBoard);
        $('.add-specific-link').live('click', function () {
            var parentLi = $(this).parent().parent();
            $('.specific-link', parentLi).show();
            $('.remove-specific-link-container', parentLi).show();
            $('.specific-link', parentLi).focus();
            $(this).hide();
            return false;
        });
        $('.remove-specific-link').live('click', function () {
            var parentLi = $(this).parent().parent().parent();
            $('.specific-link', parentLi).val('').hide();
            $('.add-specific-link', parentLi).show();
            $(this).parent().hide();
            return false;
        });
        $('#rule-list .rule-operator').live('change', function () {
            if ($(this).val() == 'Is set' || $(this).val() == 'Is not set') {
                $(this).next().hide();
            } else {
                $(this).next().show();
            }
        });
    },
    clearBoard: function () {
        $('#rule-list').html('');
        $('.specific-list-rule-separator').remove();
        return false;
    },
    convertRuleBoardToString: function (parentId, lookForSegmentRules) {
        var ruleString = '';
        var ruleArray = [];
        var subGroupRuleArray = [];
        var ruleBoardId = parentId;
        var activityRules = $(ruleBoardId + ' > li.rule.activity');
        $.each(activityRules, function (index, data) {
            ruleArray.push(RuleBoard.convertActivityRuleLiToString(data));
        });
        var informationRules = $(ruleBoardId + ' > li.rule.information');
        $.each(informationRules, function (index, data) {
            ruleArray.push(RuleBoard.convertInformationRuleLiToString(data));
        });
        var subGroupRules = $(ruleBoardId + ' > li.sub-group-container');
        $.each(subGroupRules, function (index, eachSubGroup) {
            var subGroupInformationRules = $('ul > li.rule.information', eachSubGroup);
            var tmpGroupArray = [];
            $.each(subGroupInformationRules, function (index, data) {
                tmpGroupArray.push(RuleBoard.convertInformationRuleLiToString(data));
            });
            subGroupRuleArray.push('((!' + tmpGroupArray.join(',,,') + '!))');
        });

        ruleString = ruleArray.join(',,,');
        if (subGroupRuleArray.length > 0) {
            ruleString += (ruleString != '' ? ',::,' : '') + subGroupRuleArray.join(',#,');
        }

        var ruleString2 = '';
        if (lookForSegmentRules && $('#segment-rule-list').length > 0) {
            ruleString2 = RuleBoard.convertRuleBoardToString('#segment-rule-list', false);
        }

        if (lookForSegmentRules && ruleString2 != '') {
            ruleString = RuleBoard.joinRuleStrings(ruleString2, ruleString);
        }

        return ruleString;
    },
    joinRuleStrings: function (rule1, rule2) {
        ruleAll = '';
        ruleString = '';
        ruleGroupString = '';

        rule1 = rule1.split(',::,');
        rule2 = rule2.split(',::,');

        rule1Rules = rule1[0];
        rule2Rules = rule2[0];
        rule1Groups = (rule1.length > 1 ? rule1[1] : '');
        rule2Groups = (rule2.length > 1 ? rule2[1] : '');

        ruleString += rule1Rules;
        if (rule2Rules != '') {
            ruleString += (ruleString != '' ? ',,,' : '') + rule2Rules;
        }

        ruleGroupString += rule1Groups;
        if (rule2Groups != '') {
            ruleGroupString += (ruleGroupString != '' ? ',#,' : '') + rule2Groups;
        }

        ruleAll += ruleString;
        if (ruleGroupString != '') {
            ruleAll += (ruleAll != '' ? ',::,' : '') + ruleGroupString;
        }
        return ruleAll;
    },
    convertInformationRuleLiToString: function (liElement) {
        var field = $(liElement).attr('fieldid');
        var operator = $('.rule-operator', liElement).val();
        var filter = $('.filter', liElement).val();
        if (!defaultFields.inArray(field) && !pluginFields.inArray(field)) {
            field = 'CustomField' + field;
        }
        return '[[' + field + ']||[' + operator + ']||[' + filter + ']]';
    },
    convertActivityRuleLiToString: function (liElement) {
        var field = $(liElement).attr('fieldid');
        var campaignFilter = $('.campaign-select', liElement).val();
        var campaignID = (campaignFilter != 'Total' && campaignFilter != 'Any') ? $('.campaign-select', liElement).val() : 0;
        if (campaignID != 0) {
            campaignFilter = 'Specific';
        }
        var timesFilter = $('.times-select', liElement).val();
        var timesValue = $('.times-input', liElement).val();
        if (field == 'Clicks') {
            var specificLink = $('.specific-link', liElement).val();
        }
        return '[[' + field + ']||[' + campaignFilter + ']||[' + campaignID + ']||[' + timesFilter + ']||[' + timesValue + ']' + (field == 'Clicks' ? '||[' + specificLink + ']' : '') + ']';
    },
    convertSegmentRulesStringToArray: function (ruleString) {
        returnArraySegmentRules = [];
        arrayRules = ruleString.split(',::,');
        arrayRuleGroups = (arrayRules.length > 1 ? arrayRules[1] : []);
        arrayRules = arrayRules[0];
        arrayRules = arrayRules.split(',,,');
        $.each(arrayRules, function (index, rule) {
            returnArraySegmentRules.push(RuleBoard.convertRuleStringToArray(rule));
        });
        if (arrayRuleGroups.length > 0) {
            arrayRuleGroupStrings = arrayRuleGroups.split(',#,');
            $.each(arrayRuleGroupStrings, function (index, eachRuleGroupString) {
                tmpArrayGroup = [];
                eachRuleArray = eachRuleGroupString.split(',,,');
                $.each(eachRuleArray, function (index, rule) {
                    tmpArrayGroup.push(RuleBoard.convertRuleStringToArray(rule));
                });
                returnArraySegmentRules.push(tmpArrayGroup);
            });
        }
        return returnArraySegmentRules;
    },
    convertRuleStringToArray: function (ruleString) {
        returnRuleObject = {};
        ruleString = ruleString.replace('((!', '').replace('[[', '').replace(']]', '').replace('!))', '');

        arrayEachRule = ruleString.split(']||[');
        isActivityRule = false;

        $.each(activityFields, function (index, field) {
            if (field == arrayEachRule[0]) {
                isActivityRule = true;
            }
        });

        if (!isActivityRule) {
            returnRuleObject = {'field': arrayEachRule[0], 'operator': arrayEachRule[1], 'filter': arrayEachRule[2]};
        } else {
            returnRuleObject = {'field': arrayEachRule[0], 'campaignFilter': arrayEachRule[1], 'campaignID': arrayEachRule[2], 'timesFilter': arrayEachRule[3], 'timesValue': arrayEachRule[4]};
            if (arrayEachRule.length == 6) {
                returnRuleObject.specificLink = arrayEachRule[5];
            }
        }
        return returnRuleObject;
    },
    convertRuleArrayToRuleBoard: function (rules, segmentInformation) {
        var parent = $('ul#rule-list');
        if (segmentInformation != false) {
            RuleBoard.addSegmentRulesContainer(segmentInformation);
            var parent = $('ul#segment-rule-list');
        }
        $.each(rules, function (index, data) {
            if (!isArray(data)) {
                data.field = data.field.replace('CustomField', '');
                if (data.field == '0' || data.field == '') {
                    return;
                }
                data.field = data.field.replace('CustomField', '');
                var newRule = RuleBoard.addRule(data.field, fieldLabels[data.field].fieldLabel, fieldLabels[data.field].fieldType, fieldLabels[data.field].validationMethod, fieldLabels[data.field].values, parent);
                if (data.field == 'Opens' || data.field == 'Clicks' || data.field == 'BrowserViews' || data.field == 'Forwards') {
                    RuleBoard.applyRuleActivityData(newRule, data.campaignFilter, data.campaignID, data.timesFilter, data.timesValue, data.field, data.field == 'Clicks' ? data.specificLink : '');
                } else {
                    RuleBoard.applyRuleData(newRule, data.operator, data.filter);
                }
            } else {
                var newRuleGroup;
                $.each(data, function (index, subData) {
                    subData.field = subData.field.replace('CustomField', '');
                    if (subData.field == '0' || subData.field == '') {
                        return;
                    }
                    if (index < 1) {
                        var newRule = RuleBoard.addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, parent);
                        var array = RuleBoard.addRuleGroup(newRule, segmentInformation != false ? false : true);
                        newRuleGroup = array[0];
                        RuleBoard.applyRuleData(array[1], subData.operator, subData.filter);
                    } else {
                        var newRule = RuleBoard.addRule(subData.field, fieldLabels[subData.field].fieldLabel, fieldLabels[subData.field].fieldType, fieldLabels[subData.field].validationMethod, fieldLabels[subData.field].values, newRuleGroup);
                        RuleBoard.applyRuleData(newRule, subData.operator, subData.filter);
                    }
                });
            }
        });
    },
    updateRuleSeparators: function () {
        $('#rule-list li.rule-separator').each(function () {
            if ($(this).text() == 'AND') {
                $(this).text('OR');
            } else {
                $(this).text('AND');
            }
        });
        $('.segment-rules .rule-separator').each(function () {
            if ($(this).text() == 'AND') {
                $(this).text('OR');
            } else {
                $(this).text('AND');
            }
        });
        $('.specific-list-rule-separator').text($('.specific-list-rule-separator').text() == 'AND' ? 'OR' : 'AND');

        return false;
    },
    applyRuleActivityData: function (ruleElement, campaignFilter, campaignID, timesFilter, timesValue, field, specificLink) {
        $('.specific-link', ruleElement).val(specificLink);
        if (specificLink != '') {
            $('.add-specific-link', ruleElement).hide();
            $('.specific-link', ruleElement).show();
            $('.remove-specific-link-container', ruleElement).show();
        }
        $('.times-select option[value="' + timesFilter + '"]', ruleElement).attr('selected', 'true');
        $('.times-input', ruleElement).val(timesValue);
    },
    applyRuleData: function (ruleElement, operator, filter) {
        $('.rule-operator > option[value="' + operator + '"]', ruleElement).attr('selected', 'true');
        if ($('select.filter', ruleElement).length > 0) {
            $('select.filter > option[value="' + filter.replace('"', '\\"') + '"]', ruleElement).attr('selected', 'true');
        } else {
            $('input.filter', ruleElement).val(filter);
        }
        if ($('.rule-operator', ruleElement).val() == 'Is set' || $('.rule-operator', ruleElement).val() == 'Is not set') {
            $('.rule-operator', ruleElement).next().hide();
        } else {
            $('.rule-operator', ruleElement).next().show();
        }
    },
    addRuleSeparator: function (parent, first) {
        var separatorLabel = '';
        if (parent.attr('id') != 'rule-list' && parent.attr('id') != 'segment-rule-list') {
            separatorLabel = (RuleBoard.filterOperator.toUpperCase() == 'AND' ? 'OR' : 'AND');
        } else {
            separatorLabel = RuleBoard.filterOperator.toUpperCase();
        }
        var html = $('<li class="rule-separator">' + separatorLabel + '</li>');
        if (first) {
            if ($('#rule-list li:first-child').hasClass('sub-group-container')) {
                $('> li.sub-group-container:first', parent).before(html);
            } else {
                $('> li.rule:first', parent).before(html);
            }
        } else {
            $('> li.rule:last', parent).before(html);
        }
    },
    addRuleGroup: function (item, displayAddMenu) {
        var selectValues = [];
        $('select', item).each(function () {
            selectValues.push($(this).val());
        });
        var ruleItem = item.clone();
        $('select', ruleItem).each(function (index) {
            $('option[value="' + selectValues[index] + '"]', this).attr('selected', 'true');
        });
        var ruleGroupLI = $('<li class="sub-group-container"></li>');
        var ruleGroupUL = $('<ul class="sub-group"></ul>');
        ruleGroupLI.append(ruleGroupUL);

        if (displayAddMenu == true) {
            var ruleActionsDiv = $('<div class="rule-board-actions"></div>');
            ruleGroupLI.append(ruleActionsDiv);
            var addRuleMenu = $('.add-new-rule-menu:last').clone();
            $('.main-action', addRuleMenu).text(lang['1407']);
            $('li.activity', addRuleMenu).remove();
            ruleActionsDiv.append(addRuleMenu);
        }


        item.replaceWith(ruleGroupLI);

        ruleGroupUL.append(ruleItem);
        return [ruleGroupUL, ruleItem];
    },
    addRule: function (fieldId, fieldLabel, fieldType, validationMethod, values, parent) {
        if (fieldId == 'Opens' || fieldId == 'Clicks' || fieldId == 'BrowserViews' || fieldId == 'Forwards') {
            var description = '';
            if (fieldId == 'Clicks') {
                description = '<div class="rule-description"><a href="#" class="add-specific-link">' + lang['1414'] + '</a><span class="remove-specific-link-container" style="display:none">' + lang['1416'] + '<br /><a href="#" class="remove-specific-link">' + lang['1415'] + '</a></span></div>';
            }
            var html = $('<li fieldid="' + fieldId + '" class="rule activity"><div class="delete">X</div><span class="field">' + fieldLabel + '</span> ' + '<input type="text" value="" class="specific-link" style="width:250px;display:none" />' + RuleBoard.campaignSelect + ' ' + RuleBoard.timesSelect + '<input class="times-input" type="text" value="0" style="width:14px" /> ' + languageObject['1750'] + description + '</li>');
        } else {
            var operatorOptions = '';
            if (validationMethod != '') {
                operatorOptions += '<select class="rule-operator">';
                if (fieldType == 'Date field') {
                    validationMethod = 'Date';
                } else if (fieldType == 'Time field') {
                    validationMethod = 'Time';
                }
                for (i = 0; i < ruleOperators[validationMethod].length; i++) {
                    data = ruleOperators[validationMethod][i];
                    if (values != '' && (data.Value == 'Contains' || data.Value == 'Does not contain' || data.Value == 'Begins with' || data.Value == 'Ends with' || data.Value == 'Equals to' || data.Value == 'Is greated than' || data.Value == 'Is smaller than' || data.Value == 'Is before' || data.Value == 'Is after')) {
                        continue;
                    }
                    ;
                    operatorOptions += '<option value="' + data.Value + '">' + data.Label + '</option>';
                }
                operatorOptions += '</select>';
            }
            var description = '';
            if (validationMethod == 'Date') {
                description = '<div class="rule-description">' + lang['1406'] + '</div>';
            }
            var field = '';
            if (values != '') {
                field += '<select class="filter">';
                var tmpArray1 = values.split(',,,');
                for (i = 0; i < tmpArray1.length; i++) {
                    tmpArray1[i] = tmpArray1[i].replace(']]*', '').replace(']]', '').replace('[[', '');
                    tmpArray1[i] = tmpArray1[i].split(']||[');
                    field += '<option value="' + tmpArray1[i][1] + '">' + tmpArray1[i][0] + '</option>';
                }
                field += '</select>';
            } else {
                field = '<input type="text" class="filter" />';
            }
            var html = $('<li fieldid="' + fieldId + '" class="rule information"><div class="delete">X</div><div class="add">+</div><span class="field">' + fieldLabel + '</span> ' + operatorOptions + ' ' + field + description + '</li>');
        }
        $(parent).append(html);
        if ($('li.rule', parent).length == 1 && $('.segment-rules').length == 1 && $(parent).attr('id') != 'segment-rule-list') {
            RuleBoard.addRuleSeparator(parent, false);
        }
        if ($('li.rule', parent).length > 1) {
            RuleBoard.addRuleSeparator(parent, false);
        }
        if ($('.specific-list-rule-separator').length < 1) {
            $('#specific-list-rule').append('<li class="specific-list-rule-separator rule-separator">' + RuleBoard.filterOperator.toUpperCase() + '</li>');
        }
        return html;
    }
};

function onSearchListChange() {
    var listId = $('#SearchList').val();
    if (segments[listId].length > 0) {
        $('.add-segment-rules-menu .main-action-menu li').remove();
        $.each(segments[listId], function (index, data) {
            $('.add-segment-rules-menu .main-action-menu').append('<li><a href="#" segmentid="' + data.id + '">' + data.name + '</a></li>');
        });
        $('.add-segment-rules-menu').show();
    } else {
        $('.add-segment-rules-menu .main-action-menu li').remove();
        $('.add-segment-rules-menu').hide();
    }
    $('.custom-fields-li').remove();
    $('#display-fields li.customfieldli').remove();
    $('#sort-fields li.customfieldli').remove();
    if (customFields[listId].length > 0) {
        $.each(customFields[listId], function (index, data) {
            var customFieldLi = $('<li class="custom-fields-li"><a href="#" customfieldid="' + data.id + '" fieldtype="' + data.fieldtype + '" validationmethod="' + data.validationmethod + '" values="' + data.fieldoptions + '">' + data.name + '</a></li>');
            if ($('.custom-fields-li').length < 1) {
                $('.default-fields:last').after(customFieldLi);
            } else {
                $('.custom-fields-li:last').after(customFieldLi);
            }
            var option = $('<li class="customfieldli" customfieldid="CustomField' + data.id + '"><a href="#">' + data.name + '</a></li>');
            $('#display-fields').append(option);
            $('#sort-fields').append(option.clone());
        });
    }
    RuleBoard.clearBoard();
    $('#form-row-RuleBoard').show();
}

function applyFilters() {
    var ruleString = '';
    ruleString = RuleBoard.convertRuleBoardToString('#rule-list', true);
    console.log(ruleString);
    url = APP_URL + "/user/subscribers/search";//'http://local-server.com/Oempro2/app/user/subscribers/search';

    $.ajax({
        type: "POST",
        data: {
            'listid': $('#SearchList').val(),
            'command': 'Subscribers.Search',
            'operator': RuleBoard.filterOperator,
            'rules': ruleString,
        },
        url: url,
        success: function (data) {
//            alert("hi")
//            alert($('#SearchList').val())
//            console.log($('#SearchList').val());
//            console.log(data);
            $("#subscribers_div").html(data)
        },
        async: false // <- this turns it into synchronous
    });

}

$(document).ready(function () {
    $('#page.collapsible div.page-collapsed-bar, #page.collapsible div.page-bar').click(function () {
        $(this).parent().toggleClass('collapsed');
    });
    RuleBoard.init();
    $('#apply-filter-button').click(function () {
        applyFilters();
        return false;
    });
    $('#people-list-form').submit(function () {
        if (!confirm(languageObject['1335'])) {
            return false;
        }
    });
});
