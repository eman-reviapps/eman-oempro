(function($) {
    $.fn.extend({
        isChildOf: function( filter_string ) {

          var parents = $(this).parents().get();

          for ( j = 0; j < parents.length; j++ ) {
           if ( $(parents[j]).is(filter_string) ) {
      return true;
           }
          }

          return false;
        }
    });
})(jQuery);

$(document).ready(function () {
	$('#assign-tag-menu').click(function() {
		$(this).addClass('open');
		return false;
	});
	$('#assign-tag-menu').mouseout(function(e) {
		if ($(e.relatedTarget).isChildOf('#assign-tag-menu') == false) {
			$('#assign-tag-menu').removeClass('open');
		}
	});
	$('#assign-tag-menu li a').click(function() {
		$('#Command').val('AssignTag');
		$('#TagID').val($(this).attr('id').replace('tag-id-', ''));
		$('#campaigns-table-form').submit();
	});
});