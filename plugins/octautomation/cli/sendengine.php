#!/usr/bin/php
<?php
// Avoid execution from the web browser
if (isset($_SERVER['REMOTE_ADDR']) == true) die('This script can not be executed from the web browser. It should be executed in CLI mode.');

// Setup some initial "required" parameters
$DS = DIRECTORY_SEPARATOR;
$APP_PATH = rtrim(str_replace($DS . 'plugins/octautomation/cli' . $DS, '', dirname(__FILE__) . $DS), $DS) . $DS;
$DATA_PATH = $APP_PATH . 'data' . $DS;

// Include config file
$IsCLI = true;
include_once $DATA_PATH . 'config.inc.php';

// If not ran from CLI, display error and exit
if (Core::RunningFromCLI() == false)
{
	Core::DisplayCLIBrowserError('EMAIL_PIPE');
	exit;
}

// Load initial required modules
Core::LoadObject('octethcli');

// If demo mode is enabled, exit
if (DEMO_MODE_ENABLED == true)
{
	echo 'Not available in demo mode.';
	exit;
}

// Load the Oempro language
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');

// Verbose level. 0 -> disabled, 1, 2
$VerboseLevel = 0;

$ObjectCLI = new Octeth_CLI();

// CLI options processing
$CLI_Options = getopt('hu:v:');

if (isset($CLI_Options['h']) == true)
{
	$ObjectCLI->writeln('');
	$ObjectCLI->writeln($ObjectCLI->green('---------------------------------------------'));
	$ObjectCLI->writeln($ObjectCLI->yellow('Email Automation Plugin For Oempro'));
	$ObjectCLI->writeln($ObjectCLI->yellow('(c)Copyright 1999-' . date('Y') . ' Octeth. All rights reserved.'));
	$ObjectCLI->writeln('');
	$ObjectCLI->writeln($ObjectCLI->blue('Usage:'));
	$ObjectCLI->writeln($ObjectCLI->blue('php ' . APP_PATH . '/plugins/octautomation/cli/sendengine.php'));
	$ObjectCLI->writeln($ObjectCLI->blue('Options:'));
	$ObjectCLI->writeln($ObjectCLI->blue("-h\t\tDisplay the help"));
	$ObjectCLI->writeln($ObjectCLI->blue("-u <user_id>\tProcess pending messags of the given user ID"));
	$ObjectCLI->writeln($ObjectCLI->blue("-v <value>\tVerbose level. Default 0, disabled. Available: 0, 1, 2"));
	$ObjectCLI->writeln('');
	$ObjectCLI->writeln($ObjectCLI->yellow('For more information:'));
	$ObjectCLI->writeln($ObjectCLI->yellow('http://octeth.com/plugins/email-automation-plugin/'));
	$ObjectCLI->writeln($ObjectCLI->green('---------------------------------------------'));
	$ObjectCLI->writeln('');
	$ObjectCLI->writeln('');
	exit;
}

// Verbose level
if (isset($CLI_Options['v']) == true && $CLI_Options['v'] != '')
{
	$AvailableVerboseLevels = array(0, 1, 2);
	if (in_array($CLI_Options['v'], $AvailableVerboseLevels) == true)
	{
		$VerboseLevel = $CLI_Options['v'];
	}
}

// Target user
$TargetUserID = false;
if (isset($CLI_Options['u']) == true && $CLI_Options['u'] != '')
{
	if (is_numeric($CLI_Options['u']) == true)
	{
		$TargetUserID = $CLI_Options['u'];
	}
}

// Log the process as started
$ProcessCode = 'octautomation_sendengine';
$ProcessLogID = Core::RegisterToProcessLog($ProcessCode);

// Environment settings
set_time_limit(0);
ignore_user_abort(true);
@ini_set('memory_limit', '512M');
@ini_set('max_execution_time', '0');

// Benchmarking
$BenchmarkStartTime = microtime(true);
$BenchmarkStartTimeOverall = microtime(true);

// Variables
$ErrorExists = false;
$ErrorMessage = '';
$SuccessMessage = '';

OctAutomation_SendEngine();

function OctAutomation_SendEngine()
{
	global $BenchmarkStartTime, $BenchmarkStartTimeOverall, $ErrorExists, $ErrorMessage, $SuccessMessage, $VerboseLevel, $ObjectCLI, $TargetUserID;

	// Check if send engine is in progress right now
	$LockFilePath = APP_PATH.'/data/tmp/octautomation_sendengine.pid';
	if (file_exists($LockFilePath) == true)
	{
		$ObjectCLI->writeln($ObjectCLI->red("ERROR: Send engine seems to be running. Can not continue. "));
		$ObjectCLI->writeln("");
		$ObjectCLI->writeln($ObjectCLI->green("Solution:"));
		$ObjectCLI->writeln($ObjectCLI->green("(1) Check if the send engine is running: ps aux | grep sendengine\\.php"));
		$ObjectCLI->writeln($ObjectCLI->green("    If it is running, you may kill the process by: kill -9 <pid>"));
		$ObjectCLI->writeln($ObjectCLI->green("    Please don't forget that send engine may take a while to complete the email delivery process"));
		$ObjectCLI->writeln($ObjectCLI->green("(2) Delete the PID file which is located at ".$LockFilePath));
		$ObjectCLI->writeln($ObjectCLI->green("(3) Run the send engine again or wait until cron runs it automatically (if cron job has been set)"));
		$ObjectCLI->writeln("");
		$ObjectCLI->writeln($ObjectCLI->green("If you are still having problems, please contact us via support@octeth.com"));

		return;
	}
	else
	{
		if (file_put_contents($LockFilePath, time() === false))
		{
			$ObjectCLI->writeln($ObjectCLI->red("ERROR: Failed to write the lock file on ".$LockFilePath.". Check for directory/file permissions"));
		}
	}

	// Load models
	include_once('../models/base.php');
	$Models = new stdClass();

	include_once('../models/messages.php');
	$Models->messages = new model_messages();
	$Models->messages->Models = $Models;

	include_once('../models/people.php');
	$Models->people = new model_people();
	$Models->people->Models = $Models;

	// Retrieve active messages
	$ActiveMessages = $Models->messages->GetMessages(null, 'active');
	if ($VerboseLevel >= 1)
	{
		OctAutomation_Benchmark($BenchmarkStartTime, "Active messages retrieved: " . (is_array($ActiveMessages) == true ? number_format(count($ActiveMessages), 0) : 0) . " messages");
		$BenchmarkStartTime = microtime(true);
	}

	if (is_bool($ActiveMessages) == true)
	{
		$ErrorMessage = "There are no active messages";
		$ErrorExists = true;

		if (unlink($LockFilePath) === false)
		{
			$ObjectCLI->writeln($ObjectCLI->red("ERROR: Failed to delete the lock file on ".$LockFilePath.". Check for directory/file permissions"));
		}
		return false;
	}

	$MailServerSettings = Database::$Interface->GetOption('OctAutomation_MailServer');
	$MailServerSettings = json_decode($MailServerSettings[0]['OptionValue']);
	if (isset($MailServerSettings->Engine) == false)
	{
		$ErrorMessage = "Email server settings have not been configured in admin area";
		$ErrorExists = true;

		if (unlink($LockFilePath) === false)
		{
			$ObjectCLI->writeln($ObjectCLI->red("ERROR: Failed to delete the lock file on ".$LockFilePath.". Check for directory/file permissions"));
		}
		return false;
	}

	include_once('../personalization.php');

	foreach ($ActiveMessages as $Index => $EachActiveMessage)
	{
		if (is_bool($TargetUserID) == false && $TargetUserID > 0)
		{
			$ObjectCLI->writeln($ObjectCLI->yellow("I will be processing active messages of user #".$TargetUserID." only."));
			if ($EachActiveMessage->RelUserID != $TargetUserID)
			{
				$ObjectCLI->writeln($ObjectCLI->red("This message doesn't belong to user #".$TargetUserID));
				continue;
			}
		}

		$MessageLockID = 'MessageID_' . $EachActiveMessage->MessageID;

		$BenchmarkStartTimeOverall = microtime(true);

		$ObjectCLI->writeln('');
		$ObjectCLI->writeln($ObjectCLI->yellow("Message Loop - Start (Message ID #" . $EachActiveMessage->MessageID . ")"));

		// Retrieve owner user
		$User = users::RetrieveUser(array('*'), array('UserID' => $EachActiveMessage->RelUserID), true);
		if (is_bool($User) == true)
		{
			$ObjectCLI->writeln($ObjectCLI->red("Owner user #" . $EachActiveMessage->RelUserID . " of the message not found"));
			continue;
		}

		OctAutomation_Benchmark($BenchmarkStartTime, "Owner user retrieved");
		$BenchmarkStartTime = microtime(true);

		// Check if user has used all available quota for his account this period. If yes, do not continue
		$UserGroupSettings = Database::$Interface->GetOption('OctAutomation_UserGroupSettings');
		$UserGroupSettings = json_decode($UserGroupSettings[0]['OptionValue'], true);
		if (isset($UserGroupSettings[$User['GroupInformation']['UserGroupID']]['DeliveryLimit']) == true)
		{
			$PeriodLimit = $UserGroupSettings[$User['GroupInformation']['UserGroupID']]['DeliveryLimit'];
			$ThisPeriodSentEmails = 0;
			if ($PeriodLimit >= 0)
			{
				$ThisPeriodSentEmails = $Models->messages->GetUserPeriodEmailDeliveryTotal($User, date('Y-m'));

				if ($ThisPeriodSentEmails >= $PeriodLimit)
				{
					// User period delivery limit exceeded. Do not continue with this message
					Plugins::HookListener('Action', 'OctAutomation.MessageDelivery.QuotaExceeded', array($User, $ThisPeriodSentEmails, $PeriodLimit));

					$ObjectCLI->writeln($ObjectCLI->red("ERROR: It seems that this user #".$User['UserID']." has reached the period message delivery limit (".number_format($ThisPeriodSentEmails, 0)."/".($PeriodLimit == -1 ? 'Unlimited' : $PeriodLimit)." messages). I will not continue with this message."));
					continue;
				}
			}
		}
		else
		{
			$PeriodLimit = -1;
			$ThisPeriodSentEmails = '?';
		}

		OctAutomation_Benchmark($BenchmarkStartTime, "Usage quota check completed: ".$ThisPeriodSentEmails."/".$PeriodLimit." messages");
		$BenchmarkStartTime = microtime(true);

		// Try to lock the message. If it is already locked, skip this message
		$IsLocked = $Models->messages->IsMessageLocked($User, $EachActiveMessage->MessageID, $MessageLockID);
		if ($IsLocked == true)
		{
			$ObjectCLI->writeln($ObjectCLI->green("Message locked. Skipping this message"));
			continue;
		}

		OctAutomation_Benchmark($BenchmarkStartTime, "Check if message is locked");
		$BenchmarkStartTime = microtime(true);

		OctAutomation_Benchmark($BenchmarkStartTime, "Time zone set based on user settings");
		$BenchmarkStartTime = microtime(true);

		// Prepare people columns
		$PeopleDataStructure = $Models->people->GetPeopleDataStructure($User);

		$PeopleMetaData = $Models->people->GetPeopleMetaData($User);
		if (is_bool($PeopleMetaData) == false)
		{
			$PeopleMetaData = json_decode($PeopleMetaData->CustomDataMapping);
		}

		$AvailableColumns = array(
			'Identifier' => 'User ID',
			'Email' => 'Email Address',
			'EntryDate' => 'Entry Date',
			'LastSeenDate' => 'Last Seen Date',
			'IPAddress' => 'IP Address',
			'City' => 'City',
			'Country' => 'Country',
			'SessionCounter' => 'Sessions'
		);
		if (is_bool($PeopleMetaData) == false && count(get_object_vars($PeopleMetaData)) > 0)
		{
			foreach (get_object_vars($PeopleMetaData) as $Name => $Key)
			{
				$AvailableColumns[$Key] = ucfirst(str_replace('_', ' ', $Name));
			}
		}

		OctAutomation_Benchmark($BenchmarkStartTime, "Get people columns and meta data");
		$BenchmarkStartTime = microtime(true);

		// Prepare message rules
		$TMP_MessageRules = json_decode($EachActiveMessage->Rules);
		$MessageMatchType = $TMP_MessageRules->MatchType;

		$MessageRules = array();
		if (count($TMP_MessageRules->RuleColumn) > 0)
		{
			foreach ($TMP_MessageRules->RuleColumn as $Index => $EachRuleColumn)
			{
				$MessageRules[] = array(
					'column' => $EachRuleColumn,
					'operator' => $TMP_MessageRules->RuleOperator[$Index],
					'val' => $TMP_MessageRules->RuleValue[$Index],
				);
			}
		}
		unset($TMP_MessageRules);

		OctAutomation_Benchmark($BenchmarkStartTime, "Prepare message rules");
		$BenchmarkStartTime = microtime(true);

		// Get people matching message rules. These people will be recipients after filtering out already sent ones
		$RecipientList = $Models->people->RetrieveRecipientsOfMessage($User, $EachActiveMessage->MessageID, $MessageRules, $MessageMatchType);

		if (is_bool($RecipientList) == true)
		{
			$Models->messages->ReleaseMessageLock($User, $EachActiveMessage->MessageID, $MessageLockID);
			$ObjectCLI->writeln($ObjectCLI->red("No pending recipients"));
			continue;
		}

		OctAutomation_Benchmark($BenchmarkStartTime, "Recipients retrieved");
		$BenchmarkStartTime = microtime(true);

		$TotalSent = 0;

		// Loop recipients and send message to them
		foreach ($RecipientList as $EachPersonID => $EachPerson)
		{
			$ObjectCLI->writeln($ObjectCLI->yellow("Recipient Loop - Start (Message ID #" . $EachActiveMessage->MessageID . " - Person ID #" . $EachPerson->PersonID . ")"));

			$BenchmarkStartTimePerRecipient = microtime(true);

			$Subject = $EachActiveMessage->Subject;
			$PlainContent = $EachActiveMessage->PlainContent;
			$HTMLContent = $EachActiveMessage->HTMLContent;

			// Perform personalization
			$Personalization = new Personalization();

			$Subject = $Personalization->Engage_PersonalizeWithSubscriberInfo($Subject, $EachPerson, $AvailableColumns);
			$PlainContent = $Personalization->Engage_PersonalizeWithSubscriberInfo($PlainContent, $EachPerson, $AvailableColumns);
			$HTMLContent = $Personalization->Engage_PersonalizeWithSubscriberInfo($HTMLContent, $EachPerson, $AvailableColumns);

			OctAutomation_Benchmark($BenchmarkStartTime, "Personalization completed");
			$BenchmarkStartTime = microtime(true);

			if ($MailServerSettings->Engine == 'powermta' || $MailServerSettings->Engine == 'smtp' || $MailServerSettings->Engine == 'savetodirectory')
			{
				// Link and open tracking
				$HTMLContent = $Personalization->Engage_TrackLinks($HTMLContent, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID);
				$HTMLContent = $Personalization->Engage_TrackOpens($HTMLContent, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID);
			}

			// Send message
			switch ($MailServerSettings->Engine)
			{
				case "mailgun":
					$Result = Mailgun_SendMessage($MailServerSettings->Domain, $MailServerSettings->APIKey, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID, $EachPerson->Email, $EachActiveMessage->FromName, $EachActiveMessage->FromEmail, $EachActiveMessage->ReplyToName, $EachActiveMessage->ReplyToEmail, $Subject, $PlainContent, $HTMLContent);
					break;
				case "savetodirectory":
					$Result = SaveToDirectory_SendMessage($MailServerSettings, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID, $EachPerson->Email, $EachActiveMessage->FromName, $EachActiveMessage->FromEmail, $EachActiveMessage->ReplyToName, $EachActiveMessage->ReplyToEmail, $Subject, $PlainContent, $HTMLContent);
					break;
				case "powermta":
					$Result = SaveToDirectory_SendMessage($MailServerSettings, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID, $EachPerson->Email, $EachActiveMessage->FromName, $EachActiveMessage->FromEmail, $EachActiveMessage->ReplyToName, $EachActiveMessage->ReplyToEmail, $Subject, $PlainContent, $HTMLContent);
					break;
				case "smtp":
					$Result = SMTP_SendMessage($MailServerSettings, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID, $EachPerson->Email, $EachActiveMessage->FromName, $EachActiveMessage->FromEmail, $EachActiveMessage->ReplyToName, $EachActiveMessage->ReplyToEmail, $Subject, $PlainContent, $HTMLContent);
					break;
				case "mandrill":
					$Result = Mandrill_SendMessage($MailServerSettings, $User['UserID'], $EachActiveMessage->MessageID, $EachPerson->PersonID, $EachPerson->Email, $EachActiveMessage->FromName, $EachActiveMessage->FromEmail, $EachActiveMessage->ReplyToName, $EachActiveMessage->ReplyToEmail, $Subject, $PlainContent, $HTMLContent);
					break;
			}
			if (is_bool($Result) == true)
			{
				$ObjectCLI->writeln($ObjectCLI->red("ERROR: Failed to send the message for this recipient. Continue with the next recipient..."));
				continue;
			}

			$TotalSent++;

			OctAutomation_Benchmark($BenchmarkStartTime, "Message sent");
			$BenchmarkStartTime = microtime(true);

			$Models->messages->UpdateMessage($User, $EachActiveMessage->MessageID, array(
				'TotalSent' => array('TotalSent + 1'),
				'LatestEmailSentAt' => date('Y-m-d H:i:s'),
			));

			OctAutomation_Benchmark($BenchmarkStartTime, "Message stats updated");
			$BenchmarkStartTime = microtime(true);

			$Models->people->RegisterActivityToPerson($User, $EachPersonID, 'Message Sent', $EachActiveMessage->MessageID);

			OctAutomation_Benchmark($BenchmarkStartTime, "Person activity log updated");
			$BenchmarkStartTime = microtime(true);

			$Models->people->RegisterDeliveryLog($User, $EachActiveMessage->MessageID, $EachPersonID, false, false, false, false, false);

			OctAutomation_Benchmark($BenchmarkStartTime, "Person delivery log updated");
			$BenchmarkStartTime = microtime(true);

			$Models->messages->RegisterMessageLog($User, $EachActiveMessage->MessageID, array(
				'MessagesSent' => array('MessagesSent + 1')
			));

			$ObjectCLI->writeln($ObjectCLI->yellow("Recipient Loop - End (" . number_format((microtime(true) - $BenchmarkStartTimePerRecipient), 5) . " seconds)"));
		}

		// Release the lock
		$Models->messages->ReleaseMessageLock($User, $EachActiveMessage->MessageID, $MessageLockID);

		OctAutomation_Benchmark($BenchmarkStartTime, "Message lock released");
		$BenchmarkStartTime = microtime(true);

		$ObjectCLI->writeln($ObjectCLI->yellow("Message Loop - End (" . number_format((microtime(true) - $BenchmarkStartTimeOverall), 5) . " seconds)"));
		$ObjectCLI->writeln("");
	}

	// Delete the lock file
	if (unlink($LockFilePath) === false)
	{
		$ObjectCLI->writeln($ObjectCLI->red("ERROR: Failed to delete the lock file on ".$LockFilePath.". Check for directory/file permissions"));
	}

	$SuccessMessage = 'Active messages have been processed';

}

function OctAutomation_Benchmark($BenchmarkStartTime, $Message = '')
{
	global $ObjectCLI, $VerboseLevel;

	if ($VerboseLevel >= 2)
	{
		$ObjectCLI->writeln($ObjectCLI->blue("[Benchmark] - " . number_format((microtime(true) - $BenchmarkStartTime), 5) . "s - " . number_format((memory_get_usage() / 1024), 0) . "KB - " . $Message));
	}

	return;
}

// Log the process as completed
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);

// Output and exit
if ($ErrorExists == true)
{
	$ObjectCLI->writeln('');
	$ObjectCLI->writeln($ObjectCLI->red($ErrorMessage));
	$ObjectCLI->writeln('');
}
else
{

	$ObjectCLI->writeln('');
	$ObjectCLI->writeln($ObjectCLI->green($SuccessMessage));
	$ObjectCLI->writeln('');
}

exit;
