<?php
function SaveToDirectory_SendMessage($MailServerSettings, $UserID, $MessageID, $PersonID, $To, $FromName, $FromEmail, $ReplyToName, $ReplyToEmail, $Subject, $PlainContent, $HTMLContent)
{

	$Transport = Swift_NullTransport::newInstance();
	$Mailer = Swift_Mailer::newInstance($Transport);
	$Message = Swift_Message::newInstance();

	$Message->setFrom(array($FromEmail => $FromEmail));
	$Message->setReplyTo(array($ReplyToEmail => $ReplyToEmail));
	$Message->setCharset('UTF-8');
	$Message->setPriority(3);

	if ($MailServerSettings->Engine == 'powermta')
	{
		$Headers = $Message->getHeaders();
		$Headers->addTextHeader('x-virtual-mta', $MailServerSettings->VMTA);
	}

	// Set other email headers
	$EncryptedUserID = sl_encrypt_number($UserID, 5);

	$Message->setSubject($Subject);

	$Message->setBody($HTMLContent, 'text/html', 'UTF-8');
	$Message->addPart($PlainContent, 'text/plain', 'UTF-8');

	$Message->setTo($To);

	$Message = $Message->toString();

	$FileName = md5($MessageID . $PersonID) . '.eml';
	$Result = @file_put_contents($MailServerSettings->Directory . DIRECTORY_SEPARATOR . $FileName, $Message);

	if ($Result === false)
	{
		return false;
	}
	else
	{
		return $MessageID;
	}
}
