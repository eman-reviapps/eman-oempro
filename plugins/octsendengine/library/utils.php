<?php
function octsendengine_checkUserCredits($user, $campaignToBeSent)
{
	if ($user['GroupInformation']['PaymentSystem'] == 'Enabled' && $user['GroupInformation']['CreditSystem'] == 'Enabled') {
		return octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'userCredits')) > 0;
	} else {
		return true;
	}
}

function octsendengine_checkEmailStopPluginHook()
{
	$pluginVars = Plugins::HookListener('Action', 'Email.Send.Stop', array('Campaign'));
	 return count($pluginVars) > 0 && $pluginVars[0] == 'pause';
}

function octsendengine_generateQueue($CampaignID, $UserID, $numberOfThreads, $campaignToBeSent)
{
	$TotalAddedRecipients	= 0;
	$TotalRecipients		= 0;

	// Check if the queue table for target campaign has been created before. If no, create it - Start {
	$SQLQuery = "SHOW TABLES LIKE 'oempro_queue_c_".mysql_real_escape_string($CampaignID)."'";

	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
	if (mysql_num_rows($ResultSet) == 0)
		{
		// Doesn't exist. Create it.
		$SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_queue_c_".mysql_real_escape_string($CampaignID)."` (
					  `QueueID` int(11) NOT NULL AUTO_INCREMENT,
					  `RelEmailID` int(11) NOT NULL,
					  `RelListID` int(11) NOT NULL,
					  `RelSegmentID` int(11) NOT NULL,
					  `RelSubscriberID` int(11) NOT NULL,
					  `EmailAddress` varchar(250) COLLATE UTF8_UNICODE_CI NOT NULL,
					  `Status` enum('Pending','Sending','Sent','Failed') COLLATE UTF8_UNICODE_CI NOT NULL,
					  `StatusMessage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
					  `ThreadID` int(11) NOT NULL,
					  PRIMARY KEY (`QueueID`),
					  UNIQUE KEY `EmailAddress` (`EmailAddress`),
					  KEY `Status` (`Status`),
					  KEY `ThreadID` (`ThreadID`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
		$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
		}
	// Check if the queue table for target campaign has been created before. If no, create it - End }

	// Retrieve recipient subscriber lists and segments - Start
	$ArrayRecipientLists = EmailQueue::GetRecipientLists($CampaignID, true);
	// Retrieve recipient subscriber lists and segments - End

	// Loop subscribers of recipient lists - Start
	foreach ($ArrayRecipientLists as $Key=>$ArrayRecipientList)
		{
		$ListID				= $ArrayRecipientList[0];
		$SegmentID			= $ArrayRecipientList[1];
		$ArraySegmentJoins	= Segments::GetSegmentJoinQuery($SegmentID);

		$SQLQuery  = "INSERT IGNORE INTO ".MYSQL_TABLE_PREFIX."queue_c_".mysql_real_escape_string($CampaignID)." (`RelListID`, `RelSegmentID`, `RelSubscriberID`, `EmailAddress`, `Status`, `StatusMessage`) ";
		$SQLQuery .= "SELECT ".$ListID.", ".$SegmentID.", tblSubscribers.SubscriberID, tblSubscribers.EmailAddress, 'Pending', '' ";
		$SQLQuery .= "FROM ".MYSQL_TABLE_PREFIX."subscribers_".$ListID." AS tblSubscribers ";
		if (count($ArraySegmentJoins) > 0)
			{
			$SQLQuery .= Database::$Interface->GetJoinString(Segments::GetSegmentJoinQuery($SegmentID, false, 'tblSubscribers'))." ";
			}
		$SQLQuery .= "WHERE tblSubscribers.BounceType!='Hard' AND tblSubscribers.SubscriptionStatus='Subscribed' ";

		if ($SegmentID > 0)
			{
			// $ArraySegment = Segments::GetSegmentSQLQuery($SegmentID);
			// $SQLQuery .= ' AND ('.Database::$Interface->GetRows(array('*'), array(), $ArraySegment['SegmentRules'], array(), 0, 0, $ArraySegment['SegmentOperator'], false, true).')';

			$ArraySegment = Segments::GetSegmentSQLQuery_Enhanced($SegmentID, false, '', 'and',  0, 'tblSubscribers');
			$SQLQuery .= ' AND (';
			$SQLQuery .= Database::$Interface->GetRows_Enhanced(array(
				'Tables'		=>	array('dummy'), // We pass a dummy table name because we need only WHERE sql part
				'Fields'		=>	array('*'),
				'ReturnSQLWHEREQuery'=>	true,
				'RowOrder'		=>	array('Column'=>'SegmentName','Type'=>'ASC'),
				'Criteria'		=>	$ArraySegment,
				'Joins'			=>	$ArraySegmentJoins
				));
			$SQLQuery .= ')';
			}

		$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery, true);

		// Clean the queue against suppressed emails - Start
		EmailQueue::RemoveSuppressedSubscribers($CampaignID, $ListID, $UserID);
		// Clean the queue against suppressed emails - End
		}
	// Loop subscribers of recipient lists - End

	// Optimize the outbox table to speed-up the process - Start
	EmailQueue::OptimizeQueueTable($CampaignID);
	// Optimize the outbox table to speed-up the process - End

	// Calculate the total recipients in the queue - Start
	$TotalRecipients = EmailQueue::CalculateTotalQueueItems($CampaignID, true);
	// Calculate the total recipients in the queue - End

	// Add thread ids
	$batch = floor($TotalRecipients / $numberOfThreads);
	for ($i = 1; $i < $numberOfThreads + 1; $i++) {
		$sql = 'UPDATE `oempro_queue_c_'.mysql_real_escape_string($CampaignID).'` SET `ThreadID` = '.$i.' WHERE QueueID > '.(($i-1) * $batch).' ORDER BY QueueID ASC LIMIT '.($i == $numberOfThreads ? $TotalRecipients : $batch);
		$ResultSet = Database::$Interface->ExecuteQuery($sql);
	}

	return $TotalRecipients;
}

function octsendengine_selectCampaignFromPendingQueue($pendingQueue)
{
	$selectedCampaign = false;

	foreach ($pendingQueue as $campaignID) {
		if ($selectedCampaign !== false) break;
		
		$campaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $campaignID));
		if (! $campaign) continue;

		// Campaign should have an email if it is not a split test campaign
		if ($campaign['RelEmailID'] == 0 && octsendengine_campaignHasSplitTestingEnabled($campaign) == false) continue;
		// Campaign status should be ready
		if ($campaign['CampaignStatus'] != 'Ready') continue;
		// Campaign owner user should be valid
		if (octsendengine_canUserSendACampaign($campaign['RelOwnerUserID']) == false) continue;

		$selectedCampaign = $campaign;
	}

	return $selectedCampaign;
}

function octsendengine_campaignHasSplitTestingEnabled($campaign)
{
	return $campaign['RelEmailID'] == 0 && SplitTests::RetrieveTestOfACampaign($campaign['CampaignID'], $campaign['RelOwnerUserID']) != false;
}

function octsendengine_canUserSendACampaign($userID)
{
	$user = Users::RetrieveUser(array('*'), array('UserID' => $userID));

	// User must exist
	if ($user == false) return false;
	// User account status must be enabled
	if ($user['AccountStatus'] != 'Enabled') return false;
	// If user has a monthly campaign limit, he/she should not exceed that
	if ($user['GroupInformation']['LimitCampaignSendPerPeriod'] > 0) {
		Payments::SetUser($user);
		Payments::CheckIfPaymentPeriodExists();
		$paymentPeriod = Payments::GetLog();
		if ($paymentPeriod['CampaignsSent'] >= $user['GroupInformation']['LimitCampaignSendPerPeriod']) return false;
	}
	// If user account has credit system enabled, he/she should have credits
	$creditsCheck = Payments::CheckAvailableCredits($user);
	if ($creditsCheck[0] == false) return false;

	return true;
}

function octsendengine_notifyAdminIfCampaignUserExceedsActivityThreshold($campaign, $totalRecipients)
{
	$user = Users::RetrieveUser(array('*'), array('UserID' => $campaign['RelOwnerUserID']));

	if (($user['GroupInformation']['ThresholdEmailSend'] > 0) && ($totalRecipients > $user['GroupInformation']['ThresholdEmailSend'])) {
		O_Email_Sender_ForAdmin::send(
			O_Email_Factory::userExceededEmailSendThreshold(
				$user, array('CampaignID' => $campaign['CampaignID'], 'CampaignName' => $campaign['CampaignName'], 'TotalRecipients' => $totalRecipients)
			)
		);
	}
}

function octsendengine_calculateSendingLimitForUser($userID)
{
	$user = Users::RetrieveUser(array('*'), array('UserID' => $userID));
	$sendingLimit = 1000000000;
	if ($user['GroupInformation']['LimitEmailSendPerPeriod'] > 0) {
		$totalSentInThisPeriod = Statistics::RetrieveEmailSendingAmountFromActivityLog($userID, date('Y-m-01 00:00:00'), date('Y-m-31 23:59:59'));
		$sendingLimit = $user['GroupInformation']['LimitEmailSendPerPeriod'] - $totalSentInThisPeriod;
	}
	return $sendingLimit;
}

function octsendengine_prepareEmail($campaign, $engine)
{
	$email = Emails::RetrieveEmail(array('*'), array('EmailID' => $campaign['RelEmailID']));
	if ($email == false) return false;

	$user = Users::RetrieveUser(array('*'), array('UserID' => $campaign['RelOwnerUserID']));

	// REMOTE CONTENT - START
	if ($email['FetchURL'] != '') {
		$email['HTMLContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($email['FetchURL'], array('User'), array(), $user, array(), $campaign, array(), false, $email));
		$email['ContentType'] = 'HTML';
		if ($email['HTMLContent'] === false) return false;
	}

	if ($email['FetchPlainURL'] != '') {
		$email['PlainContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($email['FetchPlainURL'], array('User'), array(), $user, array(), $campaign, array(), false, $email));
		$email['ContentType'] = ($email['ContentType'] == 'HTML' ? 'Both' : 'Plain');
		if ($email['PlainContent'] === false) return false;
	}
	// REMOTE CONTENT - END

	// USER GROUP HEADER FOOTER - START
	$headerAndFooter = Personalization::AddEmailHeaderFooter($email['PlainContent'], $email['HTMLContent'], $user['GroupInformation']);
	$email['PlainContent'] = $headerAndFooter[0];
	$email['HTMLContent'] = $headerAndFooter[1];
	// USER GROUP HEADER FOOTER - END

	// ATTACHMENTS AND IMAGE EMBEDDING - START
	if ($email['ContentType'] == 'HTML') {
		$emailContentType = 'HTML';
	} elseif ($email['ContentType'] == 'Plain') {
		$emailContentType = 'Plain';
	} elseif ($email['ContentType'] == 'Both') {
		$emailContentType = 'MultiPart';
	}

	/*
	$email['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
		$emailContentType,
		$email['HTMLContent'],
		$campaign['CampaignID'],
		$email['EmailID'],
		$user['UserID'],
		0,
		0,
		Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $email['EmailID']), array('AttachmentID' => 'ASC')),
		($email['ImageEmbedding'] == 'Enabled' ? true : false)
	);


	$engine->ClearAttachments();

	// Image embedding
	if ($email['ImageEmbedding'] == 'Enabled' && ($emailContentType == 'HTML' || $emailContentType == 'MultiPart')) {
		$images = Emails::DetectImages($email['HTMLContent'], array(APP_URL.'track_open.php?'), true);

		$counter = 0;
		foreach ($images[1] as $imageURL) {
			$CID = md5($campaign['CampaignID'] . $imageURL);
			$imageSource = Campaigns::FetchRemoteContent($imageURL);
			$imageFile = DATA_PATH . 'tmp/' . $CID;
			file_put_contents($imageFile, $imageSource);
			$imageInfo = getimagesize($imageFile);

			self::$ArrayTemporaryImageEmbeddingFiles[] = $imageFile;

			$engine->AddEmbeddedImage($imageFile, $CID, '', 'base64', $imageInfo['mime']);
			unset($imageSource);

			$email['HTMLContent'] = str_replace(array('"'.$images[1][$counter].'"', "'".$images[1][$counter]."'"), "cid:".$CID, $email['HTMLContent']);

			$counter++;
		}
	}

	// Attachments
	$attachments = Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $email['EmailID']), array('AttachmentID' => 'ASC'));
	if ($attachments != false) {
		foreach ($attachments as $eachAttachment) {
			$engine->AddAttachment(DATA_PATH.'attachments/'.md5($eachAttachment['AttachmentID']), $eachAttachment['FileName'], 'base64', 'application/octet-stream');
		}
	}
	**/
	// ATTACHMENTS AND IMAGE EMBEDDING - END

	// PLUGIN HOOKS - START
	$pluginVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($email['Subject'], $email['HTMLContent'], $email['PlainContent'], array()));
	$email['Subject'] = $pluginVars[0];
	$email['HTMLContent'] = $pluginVars[1];
	$email['PlainContent'] = $pluginVars[2];

	$pluginVars = Plugins::HookListener('Filter', 'Campaign.Email.Send.Before', array($email['Subject'], $email['HTMLContent'], $email['PlainContent'], array(), $campaign['CampaignID']));
	$email['Subject'] = $pluginVars[0];
	$email['HTMLContent'] = $pluginVars[1];
	$email['PlainContent'] = $pluginVars[2];
	// PLUGIN HOOKS - END

	return array($engine, $email);
}

function octsendengine_getQueueRows($campaignID, $threadID, $count)
{
	$ArrayFields		= array('*');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'queue_c_'.mysql_real_escape_string($campaignID));
	$ArrayCriterias		= array('Status' => 'Pending', 'ThreadID' => $threadID);
	$ArrayQueue = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array(), $count);

	if (count($ArrayQueue) < 1) { return false; }

	return $ArrayQueue;
}

function octsendengine_pauseCampaign($campaignID)
{
	$campaignFieldsForUpdating = array('CampaignStatus' => 'Paused', 'LastActivityDateTime' => date('Y-m-d H:i:s'));
	Campaigns::Update($campaignFieldsForUpdating, array('CampaignID' => $campaignID));
	octsendengine_set(octsendengine_vnForCampaign($campaignID, 'isPaused'), true);
}

function octsendengine_send($campaignID)
{

}

function octsendengine_inc($key)
{
	apc_inc($key);
}

function octsendengine_dec($key)
{
	apc_dec($key);
}

function octsendengine_set($key, $value)
{
	apc_store($key, $value);
}

function octsendengine_get($key)
{
	return apc_fetch($key);
}

function octsendengine_vnForCampaign($campaignID, $key)
{
	return 'c' . $campaignID . '.' . $key;
}

function createSendEngine($user)
{
	Core::LoadObject('phpmailer/class.phpmailer.php');
	$isSystemSettings = $user['GroupInformation']['SendMethod'] == 'System';
	$sendMethod = $isSystemSettings ? SEND_METHOD : $user['GroupInformation']['SendMethod'];
	if ($sendMethod == 'SMTP') Core::LoadObject('phpmailer/class.smtp.php');
	$sendEngine = new PHPMailer();

	$powerMtaVmta = $isSystemSettings ? SEND_METHOD_POWERMTA_VMTA : $user['GroupInformation']['SendMethodPowerMTAVMTA'];
	$powerMtaDir = rtrim($isSystemSettings ? SEND_METHOD_POWERMTA_DIR : $user['GroupInformation']['SendMethodPowerMTADir'], '/') . '/';
	$saveToDiskDir = rtrim($isSystemSettings ? SEND_METHOD_SAVETODISK_DIR : $user['GroupInformation']['SendMethodSaveToDiskDir'], '/') . '/';

	$xMailer = $user['GroupInformation']['XMailer'] != '' ? $user['GroupInformation']['XMailer'] : X_MAILER;

	if ($sendMethod == 'SMTP') {
		$sendEngine->IsSMTP();
		$sendEngine->Host = $isSystemSettings ? SEND_METHOD_SMTP_HOST : $user['GroupInformation']['SendMethodSMTPHost'];
		$sendEngine->Port = $isSystemSettings ? SEND_METHOD_SMTP_PORT : $user['GroupInformation']['SendMethodSMTPPort'];
		$sendEngine->SMTPSecure = $isSystemSettings ? SEND_METHOD_SMTP_SECURE : $user['GroupInformation']['SendMethodSMTPSecure'];
		$sendEngine->SMTPAuth = $isSystemSettings ? SEND_METHOD_SMTP_AUTH : $user['GroupInformation']['SendMethodSMTPAuth'];
		$sendEngine->Username = $isSystemSettings ? SEND_METHOD_SMTP_USERNAME : $user['GroupInformation']['SendMethodSMTPUsername'];
		$sendEngine->Password = $isSystemSettings ? SEND_METHOD_SMTP_PASSWORD : $user['GroupInformation']['SendMethodSMTPPassword'];
		$sendEngine->Timeout = $isSystemSettings ? SEND_METHOD_SMTP_TIMEOUT : $user['GroupInformation']['SendMethodSMTPTimeOut'];
		$sendEngine->SMTPDebug = $isSystemSettings ? SEND_METHOD_SMTP_DEBUG : $user['GroupInformation']['SendMethodSMTPDebug'];
		$sendEngine->SMTPKeepAlive = $isSystemSettings ? SEND_METHOD_SMTP_KEEPALIVE : $user['GroupInformation']['SendMethodSMTPKeepAlive'];
	} else if ($sendMethod == 'LocalMTA') {
		$sendEngine->IsQmail();
		$sendEngine->Sendmail = $isSystemSettings ? SEND_METHOD_LOCALMTA_PATH : $user['GroupInformation']['SendMethodLocalMTAPath'];
	} else if ($sendMethod == 'PHPMail') {
		$sendEngine->IsMail();
	} else if ($sendMethod == 'PowerMTA') {
		$sendEngine->Mailer	= 'save_to_disk';
	} else if ($sendMethod == 'SaveToDisk') {
		$sendEngine->Mailer	= 'save_to_disk';
	}
	$sendEngine->Hostname = (isset($_SERVER['SERVER_NAME']) == false ? EMAIL_DELIVERY_HOSTNAME : $_SERVER['SERVER_NAME']);

	if (CENTRALIZED_SENDER_DOMAIN != '')
		$sendEngine->AddCustomHeader('Sender: <user-'.Core::EncryptNumberAdvanced($user['UserID']).'@'.CENTRALIZED_SENDER_DOMAIN.'>');

	$sendEngine->AddCustomHeader('X-Mailer:' . $xMailer);
	$sendEngine->AddCustomHeader('X-Complaints-To:' . X_COMPLAINTS_TO);

	$sendEngineEncoding = '';
	if (! in_array(EMAIL_SOURCE_ENCODING, array('8bit', '7bit', 'binary', 'base64', 'quoted-printable'))) {
		$sendEngineEncoding = 'quoted-printable';
	} else {
		$sendEngineEncoding = EMAIL_SOURCE_ENCODING;
	}
	$sendEngine->Encoding = $sendEngineEncoding;

	return $sendEngine;
}

function octsendengine_updateCampaignTotals($campaignID, $totalSent, $totalFailed)
{
	$campaignFieldsForUpdating = array('TotalSent' => $totalSent, 'TotalFailed' => $totalFailed, 'LastActivityDateTime' => date('Y-m-d H:i:s'));
	Campaigns::Update($campaignFieldsForUpdating, array('CampaignID' => $campaignID));
}

function octsendengine_cleanupProcessVariables($variables)
{
	foreach ($variables as $each) {
		apc_delete($each);
	}
}