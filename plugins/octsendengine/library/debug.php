<?php
function octsendengine_debug($message, $color = 'yellow', $indentLevel = 0)
{
	global $debugMode;
	global $cliWriter;

	if (! $debugMode) return;

	if ($indentLevel > 0) {
		for ($i = 1; $i <= $indentLevel; $i++) {
			$message = $cliWriter->indent($message);
		}
	}
	$message = $cliWriter->{$color}($message);

	$cliWriter->writeln($message);
}