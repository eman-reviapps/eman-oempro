<?php
class mtse_engine
{
	protected $_numOfThreads = 0;

	protected $_start = 0;
	protected $_campaignID = 0;
	protected $_userID = 0;
	protected $_engineStart = 0;

	protected $_sendingLimit = 0;
	protected $_queueSize = 0;

	public function __construct($numberOfThreads)
	{
		$this->_start = time();
		$this->_numOfThreads = $numberOfThreads;
		$this->_engineStart = time();
	}

	public function setCampaignAndUserID($campaignID, $userID)
	{
		$this->_campaignID = $campaignID;
		$this->_userID = $userID;
		$this->_createTable();
		$this->_updateTable();
	}

	public function setQueueSizeAndSendingLimit($size, $limit)
	{
		$this->_queueSize = $size;
		$this->_sendingLimit = $limit;
		$this->_updateTable();
	}

	public function updateThread($threadID, $queueSize, $sent, $failed)
	{
		$sql = "UPDATE `oempro_octmtse_engine_{$this->_campaignID}` SET ";
		$sql .= "t{$threadID}queueSize = {$queueSize}";
		$sql .= ", t{$threadID}sent = {$sent}";
		$sql .= ", t{$threadID}failed = {$failed}";
		$sql .= " LIMIT 1";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);
	}

	public function updateEngine($sent)
	{
		$sql = "UPDATE `oempro_octmtse_engine_{$this->_campaignID}` SET ";
		$sql .= "sent = {$sent}";
		$sql .= " LIMIT 1";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);
	}

	public function shutdown()
	{
		$this->_dropTable();
	}

	public static function getInstances()
	{
		$sql = "SHOW TABLES LIKE 'oempro_octmtse_engine_%';";

		/** @noinspection PhpUndefinedMethodInspection */
		$queryResult = Database::$Interface->ExecuteQuery($sql);

		if (mysql_num_rows($queryResult) < 1) return array();

		$instances = array();
		while ($eachInstance = mysql_fetch_row($queryResult)) {
			$sql2 = "SELECT * FROM `" . $eachInstance[0] . "`;";
			/** @noinspection PhpUndefinedMethodInspection */
			$queryResult2 = Database::$Interface->ExecuteQuery($sql2);
			if (mysql_num_rows($queryResult2) < 1) continue;
			$queryResultRow = mysql_fetch_assoc($queryResult2);
			$instances[] = $queryResultRow;
		}

		return $instances;
	}

	protected function _createTable()
	{
		$sql = "CREATE TABLE `oempro_octmtse_engine_{$this->_campaignID}` ("
				." `start` int(11) NOT NULL,"
				." `campaignID` int(11) NOT NULL,"
				." `userID` int(11) NOT NULL,"
				." `queueSize` int(11) NOT NULL,"
				." `sendingLimit` int(11) NOT NULL,";
		for ($i = 1; $i <= $this->_numOfThreads; $i++) {
			$sql .= " `t{$i}queueSize` int(11) NOT NULL,";
			$sql .= " `t{$i}sent` int(11) NOT NULL,";
			$sql .= " `t{$i}failed` int(11) NOT NULL,";
		}
		$sql .= " `sent` int(11) NOT NULL";
		$sql .= ") ENGINE=MEMORY;";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);

		$sql = "INSERT INTO `oempro_octmtse_engine_{$this->_campaignID}` (campaignID, userID) VALUES ({$this->_campaignID}, {$this->_userID});";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);
	}

	protected function _updateTable()
	{
		$sql = "UPDATE `oempro_octmtse_engine_{$this->_campaignID}` SET ";
		$sql .= "start = {$this->_start}";
		$sql .= ", campaignID = {$this->_campaignID}";
		$sql .= ", userID = {$this->_userID}";
		$sql .= ", queueSize = {$this->_queueSize}";
		$sql .= ", sendingLimit = {$this->_sendingLimit}";
		$sql .= " LIMIT 1";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);
	}

	protected function _dropTable()
	{
		$sql = "DROP TABLE IF EXISTS `oempro_octmtse_engine_{$this->_campaignID}`;";

		/** @noinspection PhpUndefinedMethodInspection */
		Database::$Interface->ExecuteQuery($sql);
	}
}