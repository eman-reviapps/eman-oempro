<?php
if (version_compare(PHP_VERSION, '5.3.0') < 0) die('PHP VERSION MUST BE >= 5.3.0');
if (function_exists('apc_add') == false) die('PHP-APC EXTENSION IS REQUIRED');
if (ini_get('apc.enable_cli') != 1) die('APC EXTENSION IS NOT ENABLED IN CLI MODE => apc.enable_cli');

define('OSE_EMAIL_NOT_FOUND', 1);
define('OSE_NO_CREDITS', 2);
define('OSE_PLUGIN', 3);
define('OSE_NOT_SENDING', 4);

$DS = DIRECTORY_SEPARATOR;
$CLI_PATH = __DIR__ . "{$DS}..{$DS}cli{$DS}";
$LIB_PATH = __DIR__ . "{$DS}..{$DS}library{$DS}";
$PLUGIN_DATA_PATH = __DIR__ . "{$DS}..{$DS}data{$DS}";
$APP_PATH = __DIR__ . "{$DS}..{$DS}..{$DS}..{$DS}";
$DATA_PATH = $APP_PATH . "data{$DS}";
$IsCLI = true;
$debugMode = false;

if (! is_writable($PLUGIN_DATA_PATH)) die('PLUGIN DATA PATH IS NOT WRITABLE');

if ($argc > 1 && in_array('-v', $argv)) $debugMode = true;
if ($argc > 1 && in_array('-t', $argv)) {
	$index = array_search('-t', $argv);
	if ($index !== false && isset($argv[$index + 1])) {
		$numberOfThreads = $argv[$index + 1] * 1;
	}
}

date_default_timezone_set('UTC');
set_time_limit(0);
ignore_user_abort(true);

include_once $PLUGIN_DATA_PATH . "config.php";

$numberOfThreads = OCTMTSE_MAX_THREADS;
//$numberOfThreads = $numberOfThreads > OCTMTSE_MAX_THREADS ? OCTMTSE_MAX_THREADS : $numberOfThreads;
$numberOfCampaings = OCTMTSE_MAX_CAMPAIGNS;

include_once $LIB_PATH . "debug.php";
include_once $LIB_PATH . "utils.php";
include_once $LIB_PATH . "benchmark.php";
include_once $LIB_PATH . "multi.php";
include_once $LIB_PATH . "mtse.php";
include_once $DATA_PATH . "config.inc.php";

if (! Core::RunningFromCLI()) die('ACCESS DENIED');
if (DEMO_MODE_ENABLED) die('NOT AVAILABLE IN DEMO MODE');

Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('users');
Core::LoadObject('campaigns');
Core::LoadObject('octethcli');
Core::LoadObject('queue');
Core::LoadObject('segments');
Core::LoadObject('personalization');
Core::LoadObject('attachments');
Core::LoadObject('payments');
Core::LoadObject('split_tests');
Core::LoadObject('benchmark');
Core::LoadObject('send_engine');

$cliWriter = new Octeth_CLI();

include_once $LIB_PATH . 'swift/swift_required.php';
