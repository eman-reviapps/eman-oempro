<?php
if (! isset($threadID)) die('This script can not be run standalone.');

define('OCTMTSE_THREAD_RUNNING', true);

octsendengine_bm('engine-thread'.$threadID);

// CONNECT TO DATABASE
Core::ConnectDatabase(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_TABLE_PREFIX);

// INIT TRANSPORT
$transport = null;
$sendMethod = '';
$savedTransportOptions = array();
if ($ArrayUser['GroupInformation']['SendMethod'] == 'System') {
	$sendMethod = $ArrayUser['GroupInformation']['SendMethod'];
	$savedTransportOptions = array(
		$ArrayUser['GroupInformation']['SendMethodSMTPHost'], $ArrayUser['GroupInformation']['SendMethodSMTPPort'],
		$ArrayUser['GroupInformation']['SendMethodSMTPSecure'], $ArrayUser['GroupInformation']['SendMethodSMTPAuth'],
		$ArrayUser['GroupInformation']['SendMethodSMTPUsername'], $ArrayUser['GroupInformation']['SendMethodSMTPPassword'],
		$ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'], $ArrayUser['GroupInformation']['SendMethodLocalMTAPath'],
		$ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'], $ArrayUser['GroupInformation']['SendMethodPowerMTADir'],
		$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir']
	);
} else {
	$sendMethod = SEND_METHOD;
	$savedTransportOptions = array(
		SEND_METHOD_SMTP_HOST, SEND_METHOD_SMTP_PORT,
		SEND_METHOD_SMTP_SECURE, SEND_METHOD_SMTP_AUTH,
		SEND_METHOD_SMTP_USERNAME, SEND_METHOD_SMTP_PASSWORD,
		SEND_METHOD_SMTP_TIMEOUT, SEND_METHOD_LOCALMTA_PATH,
		SEND_METHOD_POWERMTA_VMTA, SEND_METHOD_POWERMTA_DIR,
		SEND_METHOD_SAVETODISK_DIR
	);
}
if ($sendMethod == 'SMTP') {
	$transport = Swift_SmtpTransport::newInstance($savedTransportOptions[0], $savedTransportOptions[1]);
	if ($savedTransportOptions[2] != '')
		$transport->setEncryption($savedTransportOptions[2]);
	if ($savedTransportOptions[3] == true) {
		$transport->setUsername($savedTransportOptions[4]);
		$transport->setPassword($savedTransportOptions[5]);
	}
} else if ($sendMethod == 'LocalMTA') {
	$transport = Swift_SendmailTransport::newInstance(SEND_METHOD_LOCALMTA_PATH);
} else if ($sendMethod == 'PHPMail') {
	$transport = Swift_MailTransport::newInstance();
} else if ($sendMethod == 'PowerMTA' || $sendMethod == 'SaveToDisk') {
	$transport = Swift_NullTransport::newInstance();
}
$mailer = Swift_Mailer::newInstance($transport);

$pauseCheckInterval = 25;
$threadLoopCounter = 0;

// Create email (SWIFT)
$swiftEmail = Swift_Message::newInstance();
$swiftEmail->setFrom(array($email['FromEmail'] => $email['FromName']));
$swiftEmail->setReplyTo(array($email['ReplyToEmail'] => $email['ReplyToName']));
$swiftEmail->setCharset(CHARSET);

// CAMPAIGN EMAIL LEVEL 1 PERSONALIZATION  (All personalization except subscriber personalization) - START
	// REMOTE CONTENT - START
	if ($email['FetchURL'] != '') {
		$email['HTMLContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($email['FetchURL'], array('User'), array(), $user, array(), $campaignToBeSent, array(), false, $email));
		$email['ContentType'] = 'HTML';
		if ($email['HTMLContent'] === false) exit;
	}
	if ($email['FetchPlainURL'] != '') {
		$email['PlainContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($email['FetchPlainURL'], array('User'), array(), $user, array(), $campaignToBeSent, array(), false, $email));
		$email['ContentType'] = ($email['ContentType'] == 'HTML' ? 'Both' : 'Plain');
		if ($email['PlainContent'] === false) exit;
	}
	// REMOTE CONTENT -  END

	// USER GROUP HEADER FOOTER - START
	$headerAndFooter = Personalization::AddEmailHeaderFooter($email['PlainContent'], $email['HTMLContent'], $user['GroupInformation']);
	$email['PlainContent'] = $headerAndFooter[0];
	$email['HTMLContent'] = $headerAndFooter[1];
	// USER GROUP HEADER FOOTER - END

	// ATTACHMENTS - START
	$attachments = Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $email['EmailID']), array('AttachmentID' => 'ASC'));
	if ($attachments != false) {
		foreach ($attachments as $eachAttachment) {
			$attachmentPath = DATA_PATH.'attachments/'.md5($eachAttachment['AttachmentID']);
			$attachmentFileName = $eachAttachment['FileName'];
			$swiftEmail->attach(
				Swift_Attachment::fromPath($attachmentPath)->setFilename($attachmentFileName)
			);
		}
	}
	// ATTACHMENTS - END

	// IMAGE EMBEDDING - START
	if ($email['ImageEmbedding'] == 'Enabled' && $email['HTMLContent'] != '') {
		$images = Emails::DetectImages($email['HTMLContent'], array(APP_URL.'track_open.php?'), true);
		$counter = 0;
		foreach ($images[1] as $imageURL) {
			$cid = $swiftEmail->embed(Swift_Image::fromPath($imageURL));
			$email['HTMLContent'] = str_replace(array('"'.$images[1][$counter].'"', "'".$images[1][$counter]."'"), "cid:".$cid, $email['HTMLContent']);
			$counter++;
		}
	}
	// IMAGE EMBEDDING - END

	// PLUGIN HOOKS - START
	$pluginVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($email['Subject'], $email['HTMLContent'], $email['PlainContent'], array()));
	$email['Subject'] = $pluginVars[0];
	$email['HTMLContent'] = $pluginVars[1];
	$email['PlainContent'] = $pluginVars[2];

	$pluginVars = Plugins::HookListener('Filter', 'Campaign.Email.Send.Before', array($email['Subject'], $email['HTMLContent'], $email['PlainContent'], array(), $campaign['CampaignID']));
	$email['Subject'] = $pluginVars[0];
	$email['HTMLContent'] = $pluginVars[1];
	$email['PlainContent'] = $pluginVars[2];
	// PLUGIN HOOKS - END
// CAMPAIGN EMAIL LEVEL 1 PERSONALIZATION  (All personalization except subscriber personalization) - END

$totalSent = 0;
$totalFailed = 0;
while ($queueRows = octsendengine_getQueueRows($campaignToBeSent['CampaignID'], $threadID, 10)) {
	// PAUSE CHECK
	octsendengine_bm('pause-check-'.'thread'.$threadID);
	$campaignPauseCheck = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $campaignToBeSent['CampaignID']));
	if ($campaignPauseCheck['CampaignStatus'] != 'Sending') break;
	// if (octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'isPaused'))) break;
	octsendengine_bm('pause-check-'.'thread'.$threadID);

	foreach ($queueRows as $queue) {
		octsendengine_bm('retrieve-list-info-'.'thread'.$threadID);
		// RETRIEVE LIST INFO
		if (($list = octsendengine_get('listCache' . $queue['RelListID'])) == false) {
			$list = Lists::RetrieveList(array('*'), array('ListID' => $queue['RelListID']), false, false);
			octsendengine_set('listCache' . $queue['RelListID'], $list);
		}
		octsendengine_bm('retrieve-list-info-'.'thread'.$threadID);

		octsendengine_bm('retrieve-subscriber-info-'.'thread'.$threadID);
		// RETRIEVE SUBSCRIBER INFO
		$subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $queue['RelSubscriberID']), $queue['RelListID']);
		octsendengine_bm('retrieve-subscriber-info-'.'thread'.$threadID);

		octsendengine_bm('personalize-email-'.'thread'.$threadID);

		// PERSONALIZE EMAIL (SUBJECT, HTML, PLAIN)
		$emailSubject = Personalization::Personalize($email['Subject'], array('Subscriber', 'User'), $subscriber, $user, $list, $campaignToBeSent, array(), false, $email);
		if ($email['HTMLContent'] != '') $emailHTMLContent = Personalization::Personalize($email['HTMLContent'], array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $subscriber, $user, $list, $campaignToBeSent, array(), false, $email);
		if ($email['PlainContent'] != '') $emailPlainContent = Personalization::Personalize($email['PlainContent'], array('Subscriber', 'Links', 'User', 'RemoteContent'), $subscriber, $user, $list, $campaignToBeSent, array(), false, $email);
		// TODO: Personalize plugin hooks (queue.inc.php:1014)

			// Generate abuse id
			$abuseMessageID = EmailQueue::GenerateAbuseMessageID($campaignToBeSent['CampaignID'], $subscriber['SubscriberID'], $subscriber['EmailAddress'], $list['ListID'], $campaignToBeSent['RelOwnerUserID'], 0);
			$retunPathCode = Core::CryptNumber($campaignToBeSent['CampaignID']).'-'.Core::CryptNumber($subscriber['SubscriberID']).'-'.Core::CryptNumber($list['ListID']).'-'.Core::CryptNumber($user['UserID']);
			$emailHeaderSender = str_replace(array('_INSERT:EMAILADDRESS_', '_INSERT:MAILSERVERDOMAIN_'), array($retunPathCode, BOUNCE_CATCHALL_DOMAIN), BOUNCE_EMAILADDRESS);

		$swiftEmail->setTo(array($subscriber['EmailAddress']));
		$swiftEmail->setReturnPath($emailHeaderSender);
		$swiftEmail->setSubject($emailSubject);

		if ($email['HTMLContent'] != '' && $email['PlainContent'] != '') {
			$swiftEmail->setBody($emailHTMLContent, 'text/html');
			$swiftEmail->addPart($emailPlainContent, 'text/plain');
		} else if ($email['HTMLContent'] == '' && $email['PlainContent'] != '') {
			$swiftEmail->setBody($emailPlainContent, 'text/plain');
		} else if ($email['HTMLContent'] != '' && $email['PlainContent'] == '') {
			$swiftEmail->setBody($emailHTMLContent, 'text/html');
		}

		$encryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
			$campaignToBeSent['CampaignID'], // Campaign ID
			'', // Autoresponder ID
			$subscriber['SubscriberID'], // Subscriber ID
			$list['ListID'], // List ID
			$email['EmailID'], // Email ID
			0 // Is preview
		));

		$headers = $swiftEmail->getHeaders();

		$headers->removeAll('List-Unsubscribe');
		$headers->removeAll('X-MessageID');
		$headers->removeAll('X-Report-Abuse');
		$headers->removeAll('X-SMTPAPI');

		$listUnsubscriberHeaderValue = '<' . APP_URL . 'u.php?p=' . $encryptedQuery . '>';
		if (FBL_INCOMING_EMAILADDRESS != '') {
			$listUnsubscriberHeaderValue .= ', <mailto:'.FBL_INCOMING_EMAILADDRESS.'?subject=unsubscribe:'.$abuseMessageID.'>';
		}
		$headers->addTextHeader('List-Unsubscribe', $listUnsubscriberHeaderValue);
		$headers->addTextHeader('X-MessageID', $abuseMessageID);
		$headers->addTextHeader('X-Report-Abuse', '<' . X_REPORT_ABUSE_URL . $abuseMessageID . '>');

		// Sendgrid email header
		if (SENDGRID_EMAIL_HEADER == true) {
			$headers->addTextHeader('X-SMTPAPI', '{"unique_args":{"abuse-id":"'.$abuseMessageID.'"}, "category":"campaign"}');
		}

		$campaignEmailHeaderReplacements = array(
			'%User:ID%' => $user['UserID'],
			'%User:UserID%' => $user['UserID'],
			'%User:EmailAddress%' => $user['EmailAddress'],
			'%UserGroup:ID%' => $user['RelUserGroupID'],
			'%UserGroup:UserGroupID%' => $user['RelUserGroupID'],
			'%Campaign:ID%' => $campaignToBeSent['CampaignID'],
			'%Campaign:CampaignID%' => $campaignToBeSent['CampaignID'],
			'%List:ID%' => $list['ListID'],
			'%List:ListID%' => $list['ListID'],
			'%Subscriber:ID%' => $subscriber['SubscriberID'],
			'%Subscriber:SubscriberID%' => $subscriber['SubscriberID'],
			'%Subscriber:EmailAddress%' => $subscriber['EmailAddress'],
			'%Subscriber:SubscriptionIP%' => $subscriber['SubscriptionIP'],
			'%Subscriber:SubscriptionDate%' => $subscriber['SubscriptionDate']
		);
		foreach ($campaignEmailHeaders as $eachHeader) {
			$headerName = $eachHeader->getName();
			$headerValue = $eachHeader->getValue();
			$headerValue = str_replace(array_keys($campaignEmailHeaderReplacements), array_values($campaignEmailHeaderReplacements), $headerValue);

			if ($headers->has($headerName)) {
				$swiftHeader = $headers->get($headerName);
				$swiftHeader->setValue($headerValue);
			} else {
				$headers->addTextHeader($headerName, $headerValue);
			}
		}

		octsendengine_bm('personalize-email-'.'thread'.$threadID);

		// SEND EMAIL
		octsendengine_bm('send-email-'.'thread'.$threadID);
		try {
			if ($sendMethod == 'SaveToDisk' || $sendMethod == 'PowerMTA') {
				if ($sendMethod == 'PowerMTA') {
					$powerMTAHeaders = array(
						'X-Sender' => $emailHeaderSender, 
						'X-Receiver' => $subscriber['EmailAddress'], 
						'X-Virtual-MTA' => $savedTransportOptions[8]
					);
					foreach ($powerMTAHeaders as $eachPMTAHeader => $eachPMTAValue) {
						if ($headers->has($eachPMTAHeader)) {
							$swiftHeader = $headers->get($eachPMTAHeader);
							$swiftHeader->setValue($eachPMTAValue);
						} else {
							$headers->addTextHeader($eachPMTAHeader, $eachPMTAValue);
						}
					}
				}

				$emailRendered = $swiftEmail->toString();
				$file = rtrim($sendMethod == 'PowerMTA' ? $savedTransportOptions[9] : $savedTransportOptions[10], '/') . '/';
				$file .= rand(100,999).rand(100,999).rand(100,999).rand(1000,9999).time();
				$result = @file_put_contents($file, $emailRendered);
				if (is_bool($result)) $result = 0;
			} else {
				$result = $mailer->send($swiftEmail);
			}
		} catch (Exception $e) {
			$result = 0;
		}
		octsendengine_bm('send-email-'.'thread'.$threadID);

		octsendengine_bm('after-send-email-'.'thread'.$threadID);
		if ($result > 0) {
			// SENT
			$queueFieldsForUpdating = array('Status' => 'Sent');
			EmailQueue::Update($campaignToBeSent['CampaignID'], $queueFieldsForUpdating, array('QueueID' => $queue['QueueID']));

			octsendengine_inc(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent'));
			octsendengine_dec(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'sendingLimit'));

			Payments::CampaignRecipientSent($campaignToBeSent['CampaignID'], 'Sent');

			$totalSent++;
		} else {
			// FAILED
			octsendengine_inc(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'));

			$queueFieldsForUpdating = array('Status' => 'Failed', 'StatusMessage' => '');
			EmailQueue::Update($campaignToBeSent['CampaignID'], $queueFieldsForUpdating, array('QueueID' => $queue['QueueID']));

			$totalFailed++;
		}
		octsendengine_bm('after-send-email-'.'thread'.$threadID);

		$threadLoopCounter++;
		octsendengine_inc(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalLooped'));

		$mtseEngine->updateThread($threadID, 0, $totalSent, $totalFailed);
	}
}

octsendengine_bm('engine-thread'.$threadID);
exit;