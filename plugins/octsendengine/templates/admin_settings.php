<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_header.php'); ?>

<style>
	.send-engine-container {
		padding: 0 18px;
		position: relative;
	}
	.send-engine-instance {
		padding: 18px 0;
		position: relative;
		z-index: 2;
	}
	.send-engine-instance-progress {
		background: rgba(0, 155, 255, 0.22);
		position: absolute;
		z-index: 1;
	}
	.send-engine-instance h3 {
		font-size: 14px;
		font-weight: bold;
		line-height: 28px;
		margin: 0;
		padding: 0;
	}
	.send-engine-instance p {
		color: #666666;
		font-size: 11px;
		line-height: 14px;
		margin: 0;
		padding: 0;
	}
	.send-engine-instance .progress {
		font-size: 28px;
		font-weight: bold;
		line-height: 28px;
		position: absolute;
		right: 0;
		top: 28px;
	}
	.send-engine-instance .performance {
		font-size: 14px;
		font-weight: normal;
		line-height: 14px;
		position: absolute;
		right: 80px;
		top: 31px;
	}
	.send-engine-instance .progress span {
		font-size: 18px;
		position: relative;
		top: -7px;
	}
	.send-engine-instance + .send-engine-instance {
		border-top: 1px solid #999999;
	}
	.total-performance {
		float: right;
		font-size: 18px;
		line-height: 45px;
		margin-right: 18px;
	}
</style>

<div class="container" style="margin-top:18px;">
	<div class="span-23">
		<div id="page-shadow">
			<div id="page">
				<div class="page-bar">
					<h2>Multi-Threaded Send Engine</h2>
					<div class="total-performance"></div>
				</div>
				<div class="white" style="min-height:430px;padding-top: 1px;">
					<div class="send-engine-container">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var APP_URL = "<?php InterfaceAppURL(); ?>/octsendengine/admin_status";
	var USER_URL = "<?php InterfaceAppURL(); ?>/admin/users/edit/";
	var USERGROUP_URL = "<?php InterfaceAppURL(); ?>/admin/usergroups/edit/";
	var totalPerformance = 0;

	function getData(callback) {
		totalPerformance = 0;
		$.getJSON(APP_URL, function(data) {
			if (callback)
				callback(data);
		});
	}
	function render(data) {
		var $container = $('.send-engine-container');
		$container.html('');
		if (data.length < 1) {
			$container.append('<p style="text-align:center;padding: 10px 20px; background:#EEFFCC;margin-top:170px;">' +
				'Currently there no campaigns are being sent.' +
				'</p>');
			$('.total-performance').text('');
		} else {
			$.each(data, function() {
				var $instance = $('<div class="send-engine-instance"><h3></h3><p><strong>CID:</strong> #<span class="campaign-id"></span>, <strong>UID:</strong> #<span class="user-id"></span>, <strong>U:</strong> <a href="" class="user-name"></a>, <strong>UG:</strong> <a href="" class="user-group-name"></a></p><div class="progress"></div><div class="performance"></div></div>');
				var $progressBar = $('<div class="send-engine-instance-progress"></div>');
				$('h3', $instance).text(this[1].CampaignName);
				var progress = Math.round(this[1].TotalSent * 100 / this[1].TotalRecipients);
				$('div.progress', $instance).html(progress + '<span>%</span>');
				$('div.performance', $instance).html(this[3] + '/s');
				$('span.campaign-id', $instance).text(this[1].CampaignID);
				$('span.user-id', $instance).text(this[1].RelOwnerUserID);
				$('a.user-name', $instance).text(this[2].FirstName + ' ' + this[2].LastName).attr("href", USER_URL + this[1].RelOwnerUserID);
				$('a.user-group-name', $instance).text(this[2].GroupInformation.GroupName).attr("href", USERGROUP_URL + this[2].GroupInformation.UserGroupID);
				$container.append($instance);
				$container.append($progressBar);
				var fullWidth = $instance.width();
				$progressBar.css({
					top: $instance.position().top + 18,
					left: $instance.position().left,
					width: fullWidth * progress / 100,
					height: 3
				});
				totalPerformance += this[3];
			});
			$('.total-performance').text(totalPerformance + '/s');
		}
		setTimeout(function() {
			getData(render);
		}, 2000);
	}
	$(document).ready(function() {
		getData(render);
	});
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/admin_footer.php'); ?>
