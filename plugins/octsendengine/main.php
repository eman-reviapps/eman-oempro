<?php
class octsendengine extends Plugins
{
	private static $_pluginCode = 'octsendengine';
	private static $_adminInformation;

	private static $PluginLicenseKey = null;
	private static $PluginLicenseStatus = true;
	private static $PluginLicenseInfo = null;

	public function enable_octsendengine()
	{
		// License check
		self::_CheckLicenseStatus(true);
		if (self::$PluginLicenseStatus == false)
		{
			if (defined('octsendengine_LicenseStatusMessage') == true && octsendengine_LicenseStatusMessage == 'not_bound_to_this_oempro')
			{
				$_SESSION['PluginMessage'] = array(false, 'The plugin license does not belong to this Oempro license.<br>Please install the correct multi-threaded send engine plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="'.InterfaceAppUrl(true).'/octsendengine/trial_request">request a trial license</a>.');
				header("location: ".InterfaceAppURL(true).'/admin/plugins/disable/octsendengine');
				exit;
			}
			else
			{
				$_SESSION['PluginMessage'] = array(false, 'Please install the multi-threaded send engine plugin license.dat file inside data directory before enabling this plugin.<br>If you do not have a license for this plugin, you can <a href="http://octeth.com/plugins/" target="_blank">purchase on our website</a> or you can <a href="'.InterfaceAppUrl(true).'/octsendengine/trial_request">request a trial license</a>.');
				header("location: ".InterfaceAppURL(true).'/admin/plugins/disable/octsendengine');
				exit;
			}
		}

	}
	public function disable_octsendengine() {}

	private function _CheckLicenseStatus()
	{
		global $LicenseCheck;

		self::$PluginLicenseStatus = octsendengine_PerformLicenseCheck();
		self::$PluginLicenseKey = $LicenseCheck->get_plugin_license_key();
		self::$PluginLicenseInfo = $LicenseCheck->get_plugin_license_info();
		if (self::$PluginLicenseStatus == false)
		{
			return false;
		}
		return true;
	}


	public function load_octsendengine()
	{
		self::_CheckLicenseStatus();
		if (self::$PluginLicenseStatus == false) return false;

		parent::RegisterMenuHook(self::$_pluginCode, 'set_menu_items');
	}

	public function set_menu_items()
	{
		$ArrayMenuItems = array();
		
		$ArrayMenuItems[] = array(
			'MenuLocation' => 'Admin.Settings',
			'MenuID' => 'Send Engine',
			'MenuLink' => Core::InterfaceAppURL() . '/octsendengine/admin_settings/',
			'MenuTitle' => 'Send Engine',
		);

		$ArrayMenuItems[] = array(
			'MenuLocation' => 'Admin.TopDropMenu',
			'MenuID' => 'Send Engine',
			'MenuLink' => Core::InterfaceAppURL() . '/octsendengine/admin_settings/',
			'MenuTitle' => 'Send Engine'
		);

		return $ArrayMenuItems;
	}

	public function ui_admin_settings()
	{
		$ObjectCI =& get_instance();

		if (Plugins::IsPlugInEnabled(self::$_pluginCode) == false)
		{
			$Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
			$ObjectCI->display_public_message('', $Message);
			return false;
		}

		self::_CheckAdminAuth();

		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . 'Send Engine',
			'CurrentMenuItem' => 'Drop',
			'CurrentDropMenuItem' => 'Send Engine',
			'PluginView' => '../plugins/octsendengine/templates/admin_settings.php',
			'SubSection' => 'Send Engine',
			'AdminInformation' => self::$_adminInformation
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());


		$ObjectCI->plugin_render('octsendengine', 'admin_settings', $ArrayViewData, true);
	}

	public function ui_admin_status()
	{
		Core::LoadObject("campaigns");
		Core::LoadObject("users");
		Core::LoadObject("user_groups");

		include_once PLUGIN_PATH . "/octsendengine/library/mtse.php";

		if (Plugins::IsPlugInEnabled(self::$_pluginCode) == false)
		{
			$Message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1707'];
			self::$ObjectCI->display_public_message('', $Message);
			return false;
		}

		self::_CheckAdminAuth();

		$runningInstances = mtse_engine::getInstances();
		$runningInstancesDetails = array();
		if (count($runningInstances) > 0) {
			foreach ($runningInstances as $eachInstance) {
				$campaignInformation = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $eachInstance['campaignID']), array(), false);
				$userInformation = Users::RetrieveUser(array('*'), array('UserID' => $eachInstance['userID']), true);

				if ($campaignInformation == false || $userInformation == false) continue;

				$runningInstancesDetails[] = array(
					$eachInstance, $campaignInformation, $userInformation, round($eachInstance['sent'] / (time() - $eachInstance['start']))
				);
			}
		}

		header('Content-Type: application/json');
		echo json_encode($runningInstancesDetails);

	}

	public function ui_trial_request()
	{
		if (file_exists(APP_PATH . '/data/license_octsendengine.dat') == true)
		{
			$_SESSION['PluginMessage'] = array(false, 'It seems that a license for this plugin has been already generated.');
			header("location: ".InterfaceAppURL(true).'/admin/plugins');
			exit;
		}

		$LicenseEngine = new octsendengine_license_engine();
		$LicenseExpireDate = $LicenseEngine->request_trial_license();

		if (is_array($LicenseExpireDate) == true && is_bool($LicenseExpireDate[0]) == true && $LicenseExpireDate[0] == false)
		{
			$_SESSION['PluginMessage'] = array(false, $LicenseExpireDate[1]);
			header("location: ".InterfaceAppURL(true).'/admin/plugins');
			exit;
		}

		$_SESSION['PluginMessage'] = array(true, 'Trial period has just started. It will expire on '.date('Y-m-d', strtotime($LicenseExpireDate)));
		header("location: ".InterfaceAppURL(true).'/admin/plugins/enable/octsendengine');
		exit;
	}

	private function _CheckAdminAuth()
	{
		Core::LoadObject('admin_auth');
		Core::LoadObject('admins');

		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
		self::$_adminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));
	}

}