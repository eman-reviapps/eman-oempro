#!/usr/bin/php
<?php
include_once 'init.php';

declare(ticks=1);

function siginShutdown($signal) {
	if ($signal === SIGINT || $signal === SIGTERM) {
		shutdownEngine();
	}
}

function shutdownEngine() {
	// Don't do anything if this is a child thread
	if (defined('OCTMTSE_THREAD_RUNNING') && OCTMTSE_THREAD_RUNNING) return;

	global $mtseEngine;

	octsendengine_dec('octmtse_instances');
	octsendengine_cleanupProcessVariables(array('bm_data'));


//	$mtseEngine->shutdown();

	exit();
}

register_shutdown_function('shutdownEngine');  
pcntl_signal(SIGTERM, 'sigintShutdown');  
pcntl_signal(SIGINT, 'siginShutdown');  

if (octsendengine_get('octmtse_instances') > $numberOfCampaigns) {
	octsendengine_debug('Multi-Threaded Send Engine is already running', 'red', 1);
	octsendengine_debug('');
	exit;
}

octsendengine_inc('octmtse_instances');

$engineStart = time();

octsendengine_bm('engine');

octsendengine_debug('');
octsendengine_debug('OEMPRO MULTI-THREADED SEND ENGINE');
octsendengine_debug('Engine start: ' . date('M d, Y H:i:s', $engineStart), 'blue');
octsendengine_debug('');

// GLOBAL VARIABLES - START
/**
 * Number of threads to be opened
 * @var array
 */
$maxThreads = $numberOfThreads;

/**
 * Campaign that will be sent
 * @var array
 */
$campaignToBeSent = array();

/**
 * Campaign email
 * @var array
 */
$email = array();

/**
 * Owner user of the campaign that will be sent
 * @var array
 */
$user = array();

/**
 * All custom email headers for the campaign
 * @var array
 */
$campaignEmailHeaders = array();
// GLOBAL VARIABLES - END

// FIND A CAMPAIGN TO SEND - START
octsendengine_debug('Searching for pending campaigns...', 'green');
$pendingCampaigns = Campaigns::GetPendingCampaigns();
if (count($pendingCampaigns) < 1) {
	octsendengine_debug('There are no pending campaigns', 'green', 1);
	octsendengine_debug('');
	shutdownEngine();
}
octsendengine_debug('Found ' . count($pendingCampaigns) . ' pending campaign(s)', 'green', 1);
// FIND A CAMPAIGN TO SEND - END

// CHOOSE ONE CAMPAIGN FROM THE PENDING QUEUE - START
octsendengine_debug('Deciding which campaign to be sent from the pending queue...', 'green');
$campaignToBeSent = octsendengine_selectCampaignFromPendingQueue($pendingCampaigns);
if (! $campaignToBeSent) {
	octsendengine_debug('Couldn\'t find a campaign to send from the pending queue', 'green', 1);
	octsendengine_debug('');
	shutdownEngine();
}
octsendengine_debug('Selected campaign #' . $campaignToBeSent['CampaignID'] . ' from user #' . $campaignToBeSent['RelOwnerUserID'], 'green', 1);
// CHOOSE ONE CAMPAIGN FROM THE PENDING QUEUE - END

if (octsendengine_campaignHasSplitTestingEnabled($campaignToBeSent)) {
	include $CLI_PATH . "send_include_split.php";
	exit;
}

// PREPARE CAMPAIGN - START
octsendengine_debug('Preparing campaign for sending...', 'green');
$campaignFieldsForUpdating = array();
if ($campaignToBeSent['SendProcessStartedOn'] == '0000-00-00 00:00:00')
	$campaignFieldsForUpdating['SendProcessStartedOn'] = date('Y-m-d H:i:s');
$campaignFieldsForUpdating['CampaignStatus'] = 'Sending';
$campaignFieldsForUpdating['LastActivityDateTime'] = date('Y-m-d H:i:s');
Campaigns::Update($campaignFieldsForUpdating, array('CampaignID' => $campaignToBeSent['CampaignID']));
// Also update campaign array
if (isset($campaignFieldsForUpdating['SendProcessStartedOn']))
	$campaignToBeSent['SendProcessStartedOn'] = $campaignFieldsForUpdating['SendProcessStartedOn'];
$campaignToBeSent['CampaignStatus'] = $campaignFieldsForUpdating['CampaignStatus'];
$campaignToBeSent['LastActivityDateTime'] = $campaignFieldsForUpdating['LastActivityDateTime'];
octsendengine_debug('Campaign status set to "SENDING" and campaign is ready for sending', 'green', 1);
// PREPARE CAMPAIGN - END

// Retrieve user info
$user = Users::RetrieveUser(array('*'), array('UserID' => $campaignToBeSent['RelOwnerUserID']));

$mtseEngine = new mtse_engine($numberOfThreads);
$mtseEngine->setCampaignAndUserID($campaignToBeSent['CampaignID'], $user['UserID']);

// Init campaign process variables (shared across processes)
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent'), 0);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'), 0);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalLooped'), 0);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'sendingLimit'), 0);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'userCredits'), 0);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'isPaused'), false);

// GENERATE QUEUE - START
octsendengine_debug('Generating recipient queue...', 'green');
$totalRecipients = octsendengine_generateQueue($campaignToBeSent['CampaignID'], $campaignToBeSent['RelOwnerUserID'], $maxThreads, $campaignToBeSent);
	// Update campaign total recipients on database
	$campaignFieldsForUpdating = array('TotalRecipients' => $totalRecipients, 'LastActivityDateTime' => date('Y-m-d H:i:s'));
	Campaigns::Update($campaignFieldsForUpdating, array('CampaignID' => $campaignToBeSent['CampaignID']));
	// Also update campaign array
	$campaignToBeSent['TotalRecipients'] = $campaignFieldsForUpdating['TotalRecipients'];
	$campaignToBeSent['LastActivityDateTime'] = $campaignFieldsForUpdating['LastActivityDateTime'];
octsendengine_debug('Campaign queue is generated. There are total ' . $totalRecipients . ' recipients in queue', 'green', 1);
// GENERATE QUEUE - END

// Notify administrator if user exceeds user group threshold
octsendengine_notifyAdminIfCampaignUserExceedsActivityThreshold($campaignToBeSent, $totalRecipients);

// Load custom campaign email headers
$emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');
$campaignEmailHeaders = $emailHeaderMapper->findByEmailType(array('all', 'campaign'));

// Calculate sending limit
$sendingLimit = octsendengine_calculateSendingLimitForUser($campaignToBeSent['RelOwnerUserID']);
octsendengine_set(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'sendingLimit'), $sendingLimit);
if ($sendingLimit < 1000000000)
	octsendengine_debug('User #' . $campaignToBeSent['RelOwnerUserID'] . ' can only send ' . number_format($sendingLimit, 0) . ' this month.', 'green', 1);

$mtseEngine->setQueueSizeAndSendingLimit($totalRecipients, $sendingLimit);

// Retrieve email info
$email = Emails::RetrieveEmail(array('*'), array('EmailID' => $campaignToBeSent['RelEmailID']));

$activeThreads = 0;
for ($threadCounter = 1; $threadCounter <= $maxThreads; $threadCounter++) {
	$pid = pcntl_fork();
	if ($pid == -1) {
		throw new Exception('Can not open new thread');
	} else if ($pid == 0) {
		$threadID = $threadCounter;
		include $CLI_PATH . 'send_include.php';
	} else {
		$activeThreads++;
	}
}
// CONNECT TO DATABASE
Core::ConnectDatabase(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_TABLE_PREFIX);

// WAIT UNTIL ALL THREADS QUIT - CHECK FOR CAMPAIGN STATUS ALSO - START
while ($activeThreads > 0) {
	$pid = pcntl_wait($status, WNOHANG);
	if ($pid > 0) $activeThreads--;

	// PLUGIN HOOK (Email.Send.Stop)
	if (octsendengine_checkEmailStopPluginHook()) {
		octsendengine_pauseCampaign($campaignToBeSent['CampaignID']);
	}

	// SENDING LIMIT CHECK
	if (octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'sendingLimit')) < 0) {
		octsendengine_pauseCampaign($campaignToBeSent['CampaignID']);
	}

	// CREDITS CHECK
	if (! octsendengine_checkUserCredits($user, $campaignToBeSent)) {
		octsendengine_pauseCampaign($campaignToBeSent['CampaignID']);
	}

	octsendengine_updateCampaignTotals(
		$campaignToBeSent['CampaignID'], 
		$campaignToBeSent['TotalSent'] + octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent')),
		$campaignToBeSent['TotalFailed'] + octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'))
	);

	$mtseEngine->updateEngine(octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent')));

	usleep(500);
}
// WAIT UNTIL ALL THREADS QUIT - CHECK FOR CAMPAIGN STATUS ALSO - END

octsendengine_updateCampaignTotals(
	$campaignToBeSent['CampaignID'], 
	$campaignToBeSent['TotalSent'] + octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent')),
	$campaignToBeSent['TotalFailed'] + octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'))
);

$totalEmailsSent = octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent'));
$totalEmailsFailed = octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'));
$totalLooped = octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalLooped'));
$userCredits = octsendengine_get(octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'userCredits'));

$engineTotalSeconds = time() - $engineStart;
$engineTotalSeconds = $engineTotalSeconds == 0 ? 1 : $engineTotalSeconds;
$emailsPerSecond = $totalEmailsSent / $engineTotalSeconds;

octsendengine_cleanupProcessVariables(array(
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalSent'),
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalFailed'),
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'totalLooped'),
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'sendingLimit'),
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'userCredits'),
	octsendengine_vnForCampaign($campaignToBeSent['CampaignID'], 'isPaused')
));

// UPDATE CAMPAIGN STATUS AND ITS TOTALS - START
Core::AddToActivityLog($campaignToBeSent['RelOwnerUserID'], 'Campaign Sent', 'Campaign #' . $campaignToBeSent['CampaignID'] . ' sent (' . number_format($totalEmailsSent) . ' recipients)');
Statistics::UpdateListActivityStatistics(0, $campaignToBeSent['RelOwnerUserID'], array('TotalSentEmail' => $totalEmailsSent));

$campaignToBeSent = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $campaignToBeSent['CampaignID']));
$campaignFieldsForUpdating = array();

if ($campaignToBeSent['CampaignStatus'] == 'Sending') {
	// Campaign is not paused. Proceed with normal procedure
	$TMPArrayReturn = EmailQueue::PurgeQueue($campaignToBeSent['CampaignID']);

	$campaignFieldsForUpdating = array(
		'CampaignStatus' => ($campaignToBeSent['ScheduleType'] == 'Recursive' ? 'Ready' : 'Sent'),
		'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
		'BenchmarkEmailsPerSecond' => number_format($emailsPerSecond, 4),
	);

	Payments::CampaignSent($campaignToBeSent['CampaignID']);
}
else
{
	// Campaign has been paused by user.
	$campaignFieldsForUpdating = array(
		'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
		'BenchmarkEmailsPerSecond' => number_format($emailsPerSecond, 4),
	);
}

Campaigns::Update($campaignFieldsForUpdating, array('CampaignID' => $campaignToBeSent['CampaignID']));
// UPDATE CAMPAIGN STATUS AND ITS TOTALS - END

octsendengine_bm('engine');

//$bm = octsendengine_get('bm_data');
//ksort($bm);

//foreach ($bm as $key => $value) {
//	echo str_pad($key, 40) . ' => ' . 'T: '.str_pad($value['total'], 20).' - AVG: '.$value['avg']."\n";
//}

exit;
