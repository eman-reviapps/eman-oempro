<?php
// Retrieve campaign information - Start
$ArrayCampaign = $campaignToBeSent;

// If is being processed or processed already, do not proceed with this loop
if ($ArrayCampaign['CampaignStatus'] != 'Ready') exit;

// Check if the campaign is an a/b split testing campaign - Start {
$IsAorBSplitTestCampaign = false;
if (($ArrayCampaign['RelEmailID'] == 0) && (SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']) != false)) $IsAorBSplitTestCampaign = true;

// If email ID is 0, do not proceed with this campaign
if (($ArrayCampaign['RelEmailID'] == 0) && ($IsAorBSplitTestCampaign == false)) exit;

// Retrieve user information
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayCampaign['RelOwnerUserID']));
if ($ArrayUser == false) exit;

// Check if user account status is enabled. If not, ignore this campaign
if ($ArrayUser['AccountStatus'] != 'Enabled') exit;

// Check if payment period for this user exists for the current period
Payments::SetUser($ArrayUser);
Payments::CheckIfPaymentPeriodExists();

// Check if user has exceeded monthly campaign sending limit
$ArrayPaymentPeriod = Payments::GetLog();
if (($ArrayPaymentPeriod['CampaignsSent'] >= $ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod']) && ($ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod'] > 0)) exit;

// Start benchmarking
EmailQueue::StartBenchmarking();

// Check if there's enough credits for the delivery
$CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);
if ($CreditsCheckReturn[0] == false) exit;

// Change campaigns status to 'sending'
if ($ArrayCampaign['SendProcessStartedOn'] != '0000-00-00 00:00:00')
{
	$ArrayFieldnValues = array(
		'CampaignStatus' => 'Sending',
	);
}
else
{
	$ArrayFieldnValues = array(
		'CampaignStatus' => 'Sending',
		'SendProcessStartedOn' => date('Y-m-d H:i:s'),
	);
}
$ArrayFieldnValues['LastActivityDateTime'] = date('Y-m-d H:i:s');
Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));

// If campaign is recursive campaign, record the process to recursive log and increase the recursive sending instance counter - Start
if ($ArrayCampaign['ScheduleType'] == 'Recursive')
{
	// Calculate current day of month, day of week, month, hour and minute - Start {
	$CurrentDayOfMonth = date('j');
	$CurrentDayOfWeek = date('w');
	$CurrentMonth = date('n');
	$CurrentHour = date('G');
	$CurrentMinute = date('i');

	// Strip leading zero from the minute - {
	if (substr($CurrentMinute, 0, 1) == '0')
	{
		$CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
	}
	// Strip leading zero from the minute - }

	// Round current minute to 0, 15, 30 or 45 - {
	if (($CurrentMinute >= 0) && ($CurrentMinute <= 14))
	{
		$CurrentMinute = 0;
	}
	elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29))
	{
		$CurrentMinute = 15;
	}
	elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44))
	{
		$CurrentMinute = 30;
	}
	elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59))
	{
		$CurrentMinute = 45;
	}
	// Round current minute to 0, 15, 30 or 45 - }
	// Calculate current day of month, day of week, month, hour and minute - End }
	Campaigns::LogToCampaignRecursiveLog($ArrayCampaign['CampaignID'], $CurrentDayOfWeek, $CurrentDayOfMonth, $CurrentMonth, $CurrentHour, $CurrentMinute);
	Campaigns::Update(array('ScheduleRecSentInstances' => $ArrayCampaign['ScheduleRecSentInstances'] + 1), array('CampaignID' => $ArrayCampaign['CampaignID']));
}
// If campaign is recursive campaign, record the process to recursive log and increase the recursive sending instance counter - End

Campaigns::Update(array('LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));

// Generate the queue of the campaign
$TotalRecipients = EmailQueue::GenerateQueue($EachCampaignID, $ArrayCampaign['RelOwnerUserID']);

// Update total recipients of the campaign
Campaigns::Update(array('TotalRecipients' => $TotalRecipients, 'LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));

// Check if activity exceeds user group threshold - Start
if (($ArrayUser['GroupInformation']['ThresholdEmailSend'] > 0) && ($TotalRecipients > $ArrayUser['GroupInformation']['ThresholdEmailSend']))
{
	O_Email_Sender_ForAdmin::send(
		O_Email_Factory::userExceededEmailSendThreshold(
			$ArrayUser, array('CampaignID' => $ArrayCampaign['CampaignID'], 'CampaignName' => $ArrayCampaign['CampaignName'], 'TotalRecipients' => $TotalRecipients)
		)
	);

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Threshold.CampaignRecipients', array($ArrayUser, $TotalRecipients, $EachCampaignID));
	// Plug-in hook - End
}
// Check if activity exceeds user group threshold - End

// Prepare and send emails to the queue
$TotalEmailsSent = EmailQueue::SendEmails($EachCampaignID, $ArrayUser, 10, $IsAorBSplitTestCampaign);

// If send emails method returns false, set campaign status to paused - Start
if ($TotalEmailsSent === false)
{
	Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $EachCampaignID));
}

// Log the activity
Core::AddToActivityLog($ArrayCampaign['RelOwnerUserID'], 'Campaign Sent', 'Campaign #' . $EachCampaignID . ' sent (' . number_format($TotalEmailsSent) . ' recipients)');

// Log the number of sent email in activity statistics
$ArrayActivities = array(
	'TotalSentEmail' => $TotalEmailsSent,
);
Statistics::UpdateListActivityStatistics(0, $ArrayCampaign['RelOwnerUserID'], $ArrayActivities);

// Stop benchmarking
EmailQueue::StopBenchmarking();

// Calculate benchmarking
$TotalEmailsPerSecond = EmailQueue::CalculateBenchmark($TotalEmailsSent);

// Purge queue records and update campaign - Start
$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

if ($ArrayCampaign['CampaignStatus'] == 'Sending')
{
	// Campaign is not paused. Proceed with normal procedure
	$TMPArrayReturn = EmailQueue::PurgeQueue($EachCampaignID);

	$ArrayFieldnValues = array(
		'CampaignStatus' => ($ArrayCampaign['ScheduleType'] == 'Recursive' ? 'Ready' : 'Sent'),
		'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
		'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
	);

	Payments::CampaignSent($ArrayCampaign['CampaignID']);
}
else
{
	// Campaign has been paused by user.
	$ArrayFieldnValues = array(
		'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
		'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
	);
}

Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
// Purge queue records and update campaign - End

exit;