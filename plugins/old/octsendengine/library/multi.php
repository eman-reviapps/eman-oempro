<?php
class Multi
{
	private $_max = 1;
	private $_active = 0;

	public function __construct($max) {
		$this->_max = $max;
	}

	public function run($callback) {
		$pid = pcntl_fork();
		if ($pid == -1) {
			throw new Exception('out of memory');
		} else if ($pid == 0) {
			call_user_func($callback, $pid);
			exit(0);
		} else {
			$this->_active++;
			// echo "Active {$this->_active}\n";
		}
		$this->waitForActive();
	}

	public function waitForActive() {
		while ($this->_active >= $this->_max) {
			$pid = pcntl_wait($status, WNOHANG);
			if ($pid > 0) $this->_active--;
		}
	}
}