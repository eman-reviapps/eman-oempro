<?php
$benchmarkPoints = array();
octsendengine_set('bm_data', $benchmarkPoints);

function octsendengine_bm($key) {
	$benchmarkPoints = octsendengine_get('bm_data');

	$mode = '';
	if (! isset($benchmarkPoints[$key])) {
		$benchmarkPoints[$key] = array();
		$mode = 'new';
	}

	if ($mode != 'new') {
		if ($benchmarkPoints[$key]['tmp'] != 0) {
			$duration = microtime(true) - $benchmarkPoints[$key]['tmp'];
			if ($benchmarkPoints[$key]['avg'] == 0) {
				$benchmarkPoints[$key]['avg'] = $duration;
			} else {
				$benchmarkPoints[$key]['avg'] = ($benchmarkPoints[$key]['avg'] + $duration) / 2;
			}
			$benchmarkPoints[$key]['total'] = $benchmarkPoints[$key]['total'] + $duration;
			$benchmarkPoints[$key]['tmp'] = 0;
		} else {
			$benchmarkPoints[$key]['tmp'] = microtime(true);
		}
	} else {
		$benchmarkPoints[$key]['total'] = 0;
		$benchmarkPoints[$key]['avg'] = 0;
		$benchmarkPoints[$key]['tmp'] = microtime(true);
	}

	octsendengine_set('bm_data', $benchmarkPoints);
}