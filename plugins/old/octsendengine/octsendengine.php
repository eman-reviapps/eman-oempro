<?php
/**
 * Multi-Threaded Send Engine
 * Name: Multi-Threaded Send Engine
 * Description: A brand new send engine for Oempro with increased send performance
 * Minimum Oempro Version: 4.6.4
 */

include_once(PLUGIN_PATH . 'octsendengine/functions.php');
include_once(PLUGIN_PATH . 'octsendengine/main.php');