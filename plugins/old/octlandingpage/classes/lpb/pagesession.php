<?php
class LPB_PageSession
{
	protected $_pageId = 0;
	protected $_templateId = '';
	protected $_content = '';
	protected $_title = '';
	protected $_template = '';


	public function __construct()
	{
	}

	public function setTemplateId($templateId) { $this->_templateId = $templateId; }
	public function getTemplateId() { return $this->_templateId; }

	public function setTemplate($template) { $this->_template = $template; }
	public function getTemplate() { return $this->_template; }

	public function setId($id) { $this->_pageId = $id; }
	public function getId() { return $this->_pageId; }

	public function setContent($content) { $this->_content = $content; }
	public function getContent() { return $this->_content; }

	public function setTitle($title) { $this->_title = $title; }
	public function getTitle() { return $this->_title; }
}