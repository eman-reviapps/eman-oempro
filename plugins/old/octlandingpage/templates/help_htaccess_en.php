<h3>About APACHE .htaccess Files</h3>
<p><code>.htaccess</code> files provide a way to make configuration changes on a per-directory basis on your server.</p>
<p>You can learn more about <code>.htaccess</code> files on this <a href="http://httpd.apache.org/docs/2.2/howto/htaccess.html" target="_blank">APACHE tutorial</a>.</p>