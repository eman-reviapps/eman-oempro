<div id="page-shadow">
	<div id="page">
		<div class="page-bar">
			<ul class="livetabs" tabcollection="octlandingpage-settings">
				<li id="tab-template-test"><a href="#"><?php print($PluginLanguage['Screen']['0068']); ?></a></li>
				<li id="tab-license"><a href="#"><?php print($PluginLanguage['Screen']['0027']); ?></a></li>
				<li id="tab-support"><a href="#"><?php print($PluginLanguage['Screen']['0028']); ?></a></li>
				<?php if (preg_match('/^TRIAL/i', $PluginLicenseInfo['PluginLicenseKey']) > 0): ?>
					<li id="tab-trial"><a href="#"><?php print($PluginLanguage['Screen']['0037']); ?></a></li>
				<?php endif; ?>
			</ul>

		</div>
		<div class="white" style="min-height:430px;">
			<div tabcollection="octlandingpage-settings" id="tab-content-template-test">
				<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0069']); ?></h3>
				<div class="form-row no-background-color">
					<p><?php echo $PluginLanguage['Screen']['0071']; ?></p>
				</div>
				<?php if (count($TestTemplates) < 1): ?>
				<div class="form-row no-background-color">
					<p style="color:red;">You don't have any test templates.</p>
				</div>
				<?php else: ?>
						<div class="email-template-gallery-container clearfix">
							<?php foreach ($TestTemplates as $Each): ?>
								<div class="email-template <?php echo $Each['TemplateID']; ?>" id="page-template-<?php echo $Each['TemplateID']; ?>">
									<?php if ($Each['TemplateTag'] != ''): ?>
										<div style="font-weight:bold;position:absolute;background:white;padding:3px 5px;color:#0077cc;top:0;right:0;box-shadow:-2px 2px 8px rgba(0, 119, 204, .4);font-size:10px;line-height:10px;"><?php echo strtoupper($Each['TemplateTag']); ?></div>
									<?php endif; ?>
									<div class="image">
										<?php if ($Each['TemplateThumbnail'] != ''): ?>
										<img src="<?php echo InterfaceInstallationURL(true) . 'plugins/octlandingpage/page_templates/' . $Each['TemplateThumbnail']; ?>" />
										<?php else: ?>
										<img src="<?php InterfaceTemplateURL(); ?>/images/no_thumbnail.png" />
										<?php endif; ?>
									</div>
									<div class="meta">
										<?php echo $Each['TemplateName']; ?>
									</div>
								</div>
							<?php endforeach ?>
						</div>
						<div class="form-row no-background-color">
							<a class="button" id="template-test-button" style="display:inline-block;" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo $PluginLanguage['Screen']['0070']; ?> &rarr;</strong></a>
						</div>
				<?php endif; ?>
			</div>
			<div tabcollection="octlandingpage-settings" id="tab-content-license">
				<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0029']); ?></h3>
				<div class="form-row no-background-color">
					<p><?php print($PluginLanguage['Screen']['0030']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0031']); ?></p>
					<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['0032']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print($PluginLicenseKey); ?> (v<?php print($PluginVersion); ?>)</span></p>
				</div>
			</div>
			<div tabcollection="octlandingpage-settings" id="tab-content-support">
				<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0033']); ?></h3>
				<div class="form-row no-background-color">
					<p><?php print($PluginLanguage['Screen']['0034']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0035']); ?></p>
					<p><?php print($PluginLanguage['Screen']['0036']); ?></p>
					<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['0043']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print($PluginLicenseKey); ?> (v<?php print($PluginVersion); ?>)</span></p>
				</div>
			</div>
			<?php if (preg_match('/^TRIAL/i', $PluginLicenseInfo['PluginLicenseKey']) > 0): ?>
				<div tabcollection="octlandingpage-settings" id="tab-content-trial">
					<h3 class="form-legend"><?php print($PluginLanguage['Screen']['0038']); ?></h3>
					<div class="form-row no-background-color">
						<p><?php print(sprintf($PluginLanguage['Screen']['0039'], date($PluginLanguage['Screen']['0040'], strtotime($PluginLicenseInfo['LicenseExpireDate'])))); ?></p>
						<p style="font-size:1.25em;font-weight:bold;text-align:center;padding:5px;border:1px solid #AFBBC3;"><?php print($PluginLanguage['Screen']['00041']); ?><br><span style="font-family: courier new, courier, monospace;font-size:1.35em;"><?php print(date($PluginLanguage['Screen']['0040'], strtotime($PluginLicenseInfo['LicenseExpireDate']))); ?><br><a href="http://octeth.com/plugins/landing-page-builder-plugin/purchase" target="_blank"><?php print($PluginLanguage['Screen']['0042']); ?></a></span></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<script>
$(document).ready(function () {
	$('.email-template').click(function() {
		$('.email-template-gallery-container .email-template.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	if ($('.email-template-gallery-container .email-template.selected').length < 1) {
		$('.email-template:first-child').click();
	}
	$('#template-test-button').click(function(ev) {
		ev.preventDefault();
		var template = $('.email-template.selected').attr('id').replace('page-template-', '');
		window.location.href = '<?php InterfaceAppURL(); ?>/octlandingpage/template_test/' + template;
	});
});
</script>