<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php echo $PluginLanguage['Screen']['0003']; ?></h1>
			</div>
			<div class="actions">
				<a class="button" href="<?php InterfaceAppURL(); ?>/octlandingpage/pages/"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo strtoupper($PluginLanguage['Screen']['0015']); ?></strong></a>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<div class="container">
	<div class="span-18 last">
		<div id="page-shadow">
			<div id="page">
				<div class="page-bar">
					<h2><?php echo $PluginLanguage['Screen']['0046']; ?></h2>
				</div>
				<div class="blue">
					<form id="page-create" action="<?php InterfaceAppURL(); ?>/octlandingpage/templates" method="post">
						<input type="hidden" name="TemplateID" value="">
						<div class="email-template-gallery-container clearfix">
							<?php foreach ($Templates as $Each): ?>
								<div class="email-template <?php echo $Each['TemplateID']; ?>" id="page-template-<?php echo $Each['TemplateID']; ?>">
									<?php if ($Each['TemplateTag'] != ''): ?>
										<div style="font-weight:bold;position:absolute;background:white;padding:3px 5px;color:#0077cc;top:0;right:0;box-shadow:-2px 2px 8px rgba(0, 119, 204, .4);font-size:10px;line-height:10px;"><?php echo strtoupper($Each['TemplateTag']); ?></div>
									<?php endif; ?>
									<div class="image">
										<?php if ($Each['TemplateThumbnail'] != ''): ?>
										<img src="<?php echo InterfaceInstallationURL(true) . 'plugins/octlandingpage/page_templates/' . $Each['TemplateThumbnail']; ?>" />
										<?php else: ?>
										<img src="<?php InterfaceTemplateURL(); ?>/images/no_thumbnail.png" />
										<?php endif; ?>
									</div>
									<div class="meta">
										<?php echo $Each['TemplateName']; ?>
									</div>
								</div>
							<?php endforeach ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="help-column span-5 push-1 last">
		<?php include PLUGIN_PATH . '/octlandingpage/templates/help_templates_' . $LanguageCode . '.php'; ?>
	</div>

	<div class="span-18 last">
		<div class="form-action-container">
			<a class="button" targetform="page-create" id="form-button" href="#"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo $PluginLanguage['Screen']['0047']; ?> &rarr;</strong></a>
		</div>
	</div>

</div>

<script>
$(document).ready(function () {
	$('.email-template').click(function() {
		$('.email-template-gallery-container .email-template.selected').removeClass('selected');
		$(this).addClass('selected');
		$('[name="TemplateID"]').val($(this).attr('id').replace('page-template-', ''));
	});
	if ($('.email-template-gallery-container .email-template.selected').length < 1) {
		$('.email-template:first-child').click();
	}
});
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>