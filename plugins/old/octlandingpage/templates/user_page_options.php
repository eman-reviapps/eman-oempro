<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>

<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php echo $PluginLanguage['Screen']['0014']; ?></h1>
			</div>
			<div class="actions">
				<a class="button" href="<?php InterfaceAppURL(); ?>/octlandingpage/pages/"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo strtoupper($PluginLanguage['Screen']['0015']); ?></strong></a>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->

<!-- Page - START -->
<form id="page-options" action="<?php InterfaceAppURL(); ?>/octlandingpage/customurl/<?php echo $Page->getId(); ?>" method="post">
	<input type="hidden" name="Command" value="SaveCustomURL" id="Command">
	<div class="container">
		<div class="span-18">
			<div id="page-shadow">
				<div id="page">
					<div class="white" style="min-height:350px;">
						<?php if (! empty($PageSuccessMessage)): ?>
							<h3 class="form-legend success"><?php echo $PageSuccessMessage; ?></h3>
						<?php endif; ?>

						<p style="padding:18px;margin-bottom:0;"><?php echo $PluginLanguage['Screen']['0016']; ?></p>
						<div class="form-row" id="form-row-OriginalURL">
							<label for="OriginalURL"><?php echo $PluginLanguage['Screen']['0019']; ?>:</label>
							<p style="margin:0;line-height:32px;"><?php echo $Page->getOriginalURL(); ?></p>
						</div>
						<div class="form-row <?php print((form_error('CustomURL') != '' ? 'error' : '')); ?>" id="form-row-CustomURL">
							<label for="CustomURL"><?php echo $PluginLanguage['Screen']['0018']; ?>:</label>
							<input name="CustomURL" id="CustomURL" type="text" value="<?php echo set_value('CustomURL', $Page->getCustomURL()); ?>" class="text" />
							<div class="form-row-note"><p><?php echo $PluginLanguage['Screen']['0022']; ?></p></div>
							<?php print(form_error('CustomURL', '<div class="form-row-note error"><p>', '</p></div>')); ?>
						</div>
						<?php if ($Page->getCustomURL() != ''): ?>
							<h3 class="form-legend"><?php echo $PluginLanguage['Screen']['0021']; ?></h3>
							<div class="form-row">
								<p><?php echo $PluginLanguage['Screen']['0023']; ?></p>
								<textarea id="htaccess-snippet" rows="10" class="textarea" style="width:95%" readonly="readonly">RewriteEngine On
RewriteCond %{REQUEST_URI} ^<?php echo $CustomURLInfo['path']; ?> 
RewriteRule ^(.*)$ <?php echo $Page->getOriginalURL(); ?> [L,P]</textarea>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="help-column span-5 push-1 last">
			<?php include PLUGIN_PATH . '/octlandingpage/templates/help_htaccess_' . $LanguageCode . '.php'; ?>
		</div>

		<div class="span-18 last">
			<div class="form-action-container">
				<a class="button" id="form-button" href="#" targetform="page-options"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo strtoupper($PluginLanguage['Screen']['0017']); ?></strong></a>
			</div>
		</div>

	</div>
</form>
<!-- Page - END -->

<script>
	$(document).ready(function() {
		$('#htaccess-snippet').click(function() {
			$(this).select();
		});
	});
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>