<h3>What's a Landing Page?</h3>
<p>In online marketing a landing page, sometimes known as a "lead capture page" or a "lander", is a single web page that appears in response to clicking on a search engine optimized search result or an online advertisement. The landing page will usually display directed sales copy that is a logical extension of the advertisement, search result or link.</p>
<p>You can find more information on <a href="http://en.wikipedia.org/wiki/Landing_page" target="_blank">Wikipedia article</a>.</p>
