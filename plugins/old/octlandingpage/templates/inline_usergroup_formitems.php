<?php
if (is_null($UserGroupSettings) == true)
{
	$UserGroupSettings = array();
}
?>
<div class="form-row <?php print((form_error('OctLandingPage[PageLimit]') != '' ? 'error' : '')); ?>" id="form-row-OctLandingPage_PageLimit">
	<label for="OctLandingPage_PageLimit"><?php print($PluginLanguage['Screen']['0007']); ?>:</label>
	<input type="text" name="OctLandingPage[PageLimit]" value="<?php echo set_value('OctLandingPage[PageLimit]', (is_bool($UserGroupSettings['PageLimit']) == true ? '' : $UserGroupSettings['PageLimit'])); ?>" id="form-row-OctLandingPage_PageLimit" class="text" />
	<div class="form-row-note">
		<p><?php print($PluginLanguage['Screen']['0008']); ?></p>
	</div>
	<?php print(form_error('OctLandingPage[PageLimit]', '<div class="form-row-note error"><p>', '</p></div>')); ?>
</div>