<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_header.php'); ?>


<!-- Section bar - Start -->
<div class="container">
	<div class="span-23 last">
		<div id="section-bar">
			<div class="header">
				<h1 class="left-side-padding"><?php echo $PluginLanguage['Screen']['0002']; ?></h1>
			</div>
			<div class="actions">
				<a id="button-pages-create-page" class="button" href="<?php InterfaceAppURL(); ?>/octlandingpage/templates/"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong><?php echo strtoupper($PluginLanguage['Screen']['0003']); ?></strong></a>
			</div>
		</div>
	</div>
</div>
<!-- Section bar - End -->


<div class="container">
	<div class="span-18">
		<div id="page-shadow">
			<div id="page">
				<div class="white" style="min-height:420px;" id="page-user-email-templates">
					<form id="pages-table-form" action="<?php InterfaceAppURL(); ?>/octlandingpage/pages/" method="post">
						<input type="hidden" name="Command" value="DeletePages" id="Command">
						<div class="module-container">
							<?php
							if (isset($PageSuccessMessage) == true):
							?>
							<div class="page-message success">
								<div class="inner">
									<span class="close">X</span>
									<?php echo $PageSuccessMessage; ?>
								</div>
							</div>
							<?php
							elseif (isset($PageErrorMessage) == true):
							?>
							<div class="page-message error">
								<div class="inner">
									<span class="close">X</span>
									<?php echo $PageErrorMessage; ?>
								</div>
							</div>
							<?php
							endif;
							?>
							<div class="grid-operations">
								<div class="inner">
									<span class="label"><?php InterfaceLanguage('Screen', '0038'); ?>:</span>
									<a href="#" class="grid-select-all" targetgrid="pages-table"><?php InterfaceLanguage('Screen', '0039'); ?></a>,
									<a href="#" class="grid-select-none" targetgrid="pages-table"><?php InterfaceLanguage('Screen', '0040'); ?></a>
									&nbsp;&nbsp;-&nbsp;&nbsp;
									<a href="#" class="main-action" targetform="pages-table-form"><?php InterfaceLanguage('Screen', '0042'); ?></a>
								</div>
							</div>
						</div>
						<table border="0" cellspacing="0" cellpadding="0" class="grid" id="pages-table">
							<tr>
								<th width="100%" colspan="3"><?php echo strtoupper($PluginLanguage['Screen']['0004']); ?></th>
							</tr>
							<?php
							if (count($Pages) < 1 || $Pages === false):
							?>
							<tr>
								<td colspan="3"><?php echo $PluginLanguage['Screen']['0005']; ?></td>
							</tr>
							<?php
							endif;
							foreach ($Pages as $EachPage):
							?>
								<tr>
									<td width="15" style="vertical-align:top"><input class="grid-check-element" type="checkbox" name="SelectedPages[]" value="<?php echo $EachPage->getId(); ?>" id="SelectedPages<?php echo $EachPage->getId(); ?>"></td>
									<td width="600" class="no-padding-left" style="position:relative;">
										<a href="<?php InterfaceAppURL(); ?>/octlandingpage/builder/<?php echo $EachPage->getId(); ?>"><?php echo $EachPage->getTitle(); ?></a>
										<br>
										<a href="<?php InterfaceAppURL(); ?>/octlandingpage/customurl/<?php echo $EachPage->getId(); ?>" class="grid-row-action"><?php echo $PluginLanguage['Screen']['0011']; ?></a>
										&nbsp;&nbsp;<a href="<?php echo $EachPage->getURL(); ?>" class="grid-row-action" target="_blank"><?php echo $PluginLanguage['Screen']['0010']; ?></a>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small"><?php echo $PluginLanguage['Screen']['0044']; ?>: <strong><?php echo number_format($EachPage->getUniqueVisitCount(), 0); ?></strong></span>
									</td>
								</tr>
							<?php
							endforeach;
							?>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="help-column span-5 push-1 last">
		<?php include PLUGIN_PATH . '/octlandingpage/templates/help_landingpage_' . $LanguageCode . '.php'; ?>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	var language_object = {
		screen : {
			'0009'	: '<?php echo $PluginLanguage['Screen']['0009']; ?>'
		}
	};

	$(document).ready(function () {
		$('#pages-table-form').submit(function() {
			if (! confirm(language_object.screen['0009'])) {
				return false;
			}
		});
	});
</script>

<?php include_once(TEMPLATE_PATH.'desktop/layouts/user_footer.php'); ?>