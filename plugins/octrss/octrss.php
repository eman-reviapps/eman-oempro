<?php
/**
* RSS Plug-In For Oempro4
* Name: RSS Plug-In
* Description: This fetches any RSS feed that is defined and embeds into outgoing emails. (c) Octeth Ltd. All rights reserved. octeth.com - support@octeth.com
* Minimum Oempro Version: 4.1.0
*/
class octrss extends Plugins
{
	public static $arrayLanguage = array();
	
	private static $arrayParsedRSS = array();
	private static $subject = '';
	private static $htmlBody = '';
	private static $plainBody = '';
	private static $tags = array();
	
	function __construct() {}

	function enable_octrss() {
		Database::$Interface->SaveOption('OctRSS_Language', 'en');
	}

	function disable_octrss() {
		Database::$Interface->RemoveOption('OctRSS_Language');
	}

	function load_octrss() {
		parent::RegisterEnableHook('octrss');
		parent::RegisterDisableHook('octrss');

		parent::RegisterHook('Filter', 'Email.Send.Before', 'octrss', 'detectRSSTags', 10, 3);
		parent::RegisterHook('Filter', 'PersonalizationTags.Campaign.Content', 'octrss', 'getPersonalizationTags', 10, 0);
		parent::RegisterHook('Filter', 'PersonalizationTags.Autoresponder.Content', 'octrss', 'getPersonalizationTags', 10, 0);

		$languageOption = Database::$Interface->GetOption('OctRSS_Language');

		$language = $LanguageOptions[0]['OptionValue'];

		if (file_exists(PLUGIN_PATH.'octrss/languages/'.strtolower($language).'/'.strtolower($language).'.inc.php') == true) {
			include_once(PLUGIN_PATH.'octrss/languages/'.strtolower($language).'/'.strtolower($language).'.inc.php');
		} else {
			include_once(PLUGIN_PATH.'octrss/languages/en/en.inc.php');
		}

		self::$arrayLanguage = $ArrayPlugInLanguageStrings;
		unset($ArrayPlugInLanguageStrings);
	}

	function getPersonalizationTags($arrayPersonalizationTags) {
		$arrayAvailableTags = array(
									'%RSS:[url]:[no. of items to fetch]%'			=>	self::$arrayLanguage['0001'],
									'%RSS:TITLE:[length]%'							=>	self::$arrayLanguage['0002'],
									'%RSS:CONTENT:[length]%'						=>	self::$arrayLanguage['0003'],
									'%RSS:POST-LINK%'								=>	self::$arrayLanguage['0004'],
									'%RSS:COMMENTS-LINK%'							=>	self::$arrayLanguage['0005'],
									'%RSS:PUBLISH-DATE%'							=>	self::$arrayLanguage['0006'],
									'%RSS:AUTHOR%'									=>	self::$arrayLanguage['0007'],
									'%RSS:END%'										=>	self::$arrayLanguage['0008'],
									);

		foreach ($arrayAvailableTags as $key=>$value) {
			$arrayPersonalizationTags['RSS'][$key] = $value;
		}

		return array($arrayPersonalizationTags);
	}

	function detectRSSTags($subject, $htmlBody, $plainBody) {
		self::$subject		= $subject;
		self::$htmlBody		= $htmlBody;
		self::$plainBody	= $plainBody;
	
		// HTML Body - Start	
		if ($htmlBody != '') {
			$htmlBody = self::findRSSBlocks($htmlBody);
		}
		// HTML Body - End

		// Plain Body - Start
		if ($plainBody != '') {
			$plainBody = self::findRSSBlocks($plainBody);
		}
		// Plain Body - End

		// Subject - Start
		// Nothing to do 
		// Subject - End

		return array($subject, $htmlBody, $plainBody);
	}

	function findRSSBlocks($content) {
		// load RSSParser library.
		include_once(PLUGIN_PATH."octrss/includes/libraries/rssparser.inc.php");
		
		preg_match_all('/%RSS:(.*)%(.*)%RSS:END%/iUs', $content, $ArrayMatches, PREG_SET_ORDER);

		// Loop each RSS feed - Start
		foreach ($ArrayMatches as $Key=>$ArrayFound) {
			self::$arrayParsedRSS = array();

			$RSSUrl = $ArrayFound[1];
			preg_match('/(http|https):\/\/(.*?):(.*)/i', $RSSUrl, $ArrayURLParams);

			if (count($ArrayURLParams) == 0) {
				continue;
			}
			$NumberOfItemsToList	= trim($ArrayURLParams[3], '[]');
			$RSSUrl					= trim($ArrayURLParams[1], '[]').'://'.trim($ArrayURLParams[2], '[]');

			$RSSTemplate	= $ArrayFound[2];

			self::$arrayParsedRSS = RSSParser::parse($RSSUrl, $NumberOfItemsToList, RSSParser::URL);
		
			$ReplaceString = '';
	
			// Loop each RSS item - Start
			foreach (self::$arrayParsedRSS as $RSSItem) {
				$TMPBuffer = $RSSTemplate;

				preg_match_all('/%RSS:TITLE:?(.*?)%/i', $TMPBuffer, $ArrayFoundTag, PREG_SET_ORDER);
				if (count($ArrayFoundTag) > 0) {
					$Title = $RSSItem->Title();
					$Length = trim($ArrayFoundTag[0][1], '[]');
					if (is_numeric($Length) == true) {
						if (strlen($Title) > $Length) {
							$TMPBuffer = str_replace($ArrayFoundTag[0][0], substr($Title, 0, $Length).'...', $TMPBuffer);
						} else {
							$TMPBuffer = str_replace($ArrayFoundTag[0][0], $Title, $TMPBuffer);
						}
					} else {
						$TMPBuffer = str_replace('%RSS:TITLE%', $Title, $TMPBuffer);
					}
				}

				preg_match_all('/%RSS:CONTENT:?(.*?)%/i', $TMPBuffer, $ArrayFoundTag, PREG_SET_ORDER);
				if (count($ArrayFoundTag) > 0) {
					$Length = trim($ArrayFoundTag[0][1], '[]');
					if (is_numeric($Length) == true) {
						if(strlen($RSSItem->Content()) > $Length){
							$TMPBuffer = str_replace($ArrayFoundTag[0][0], substr($RSSItem->Content(), 0, $Length).'...', $TMPBuffer);
						}
						else{
							$TMPBuffer = str_replace($ArrayFoundTag[0][0], $RSSItem->Content(), $TMPBuffer);
						}
					} 
					else {
						$TMPBuffer = str_replace('%RSS:CONTENT%', $RSSItem->Content(), $TMPBuffer);
					}
				}

				$TMPBuffer = str_replace('%RSS:POST-LINK%', $RSSItem->Link(), $TMPBuffer);
				$TMPBuffer = str_replace('%RSS:COMMENTS-LINK%', $RSSItem->CommentLink(), $TMPBuffer);
				$TMPBuffer = str_replace('%RSS:PUBLISH-DATE%', $RSSItem->Date(), $TMPBuffer);
				$TMPBuffer = str_replace('%RSS:AUTHOR%', $RSSItem->Author(), $TMPBuffer);

				$ReplaceString .= $TMPBuffer;
			}
			// Loop each RSS item - End

			$content = str_replace($ArrayFound[0], $ReplaceString, $content);
		}
		// Loop each RSS feed - End

		return $content;
	}

	function getLanguage() {
		return self::$arrayLanguage;	
	}

}


?>