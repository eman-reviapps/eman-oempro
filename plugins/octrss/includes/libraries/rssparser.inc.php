<?php
	class RSSParser{
		
		const URL = 1;
		const STR = 2;
				
		public static function parse($RSSFeed, $ItemCount=10, $FeedType = self::URL){
			// constructs an XML element from url or string by checking

			$XmlElement = new SimpleXMLElement($RSSFeed, null, ($FeedType == self::URL ? true : false));

			// if xml element loaded, this means we have given a valid URL/String as XML. 
			// Process XML file, if success load ParsedArray
			if($XmlElement){
				// Create RSSFeed object that holds RSS Items
				$RSSFeed = array();
				
				// define the namespaces that we are interested in
				$ns = array
				(
				        'content' => 'http://purl.org/rss/1.0/modules/content/',
				        'wfw' => 'http://wellformedweb.org/CommentAPI/',
				        'dc' => 'http://purl.org/dc/elements/1.1/'
				);
				
				if($XmlElement->channel->item){
					foreach($XmlElement->channel->item as $item){
						if($ItemCount==0)
							break;
						// add a new RSSItem object to the RSSFeed
						$RSSItem = new RSSItem();
						$RSSItem->Title((string) $item->title);
						$RSSItem->Link((string) $item->link);
						$RSSItem->CommentLink((string) $item->comments);
						$RSSItem->Date((string) $item->pubDate);

						// get data held in namespaces
				        $content = $item->children($ns['content']);
				        $dc      = $item->children($ns['dc']);
				        $wfw     = $item->children($ns['wfw']);

				        $RSSItem->Author((string) $dc->creator);
						$RSSItem->Content((string) trim($content->encoded));	
					
						if($RSSItem->Content()==""){
							$RSSItem->Content($item->description);
						}

				        if($RSSItem->CommentLink()=="")
							$RSSItem->CommentLink($wfw->commentRss);

						$RSSFeed[] = $RSSItem;
						
						$ItemCount--;
					}
				}
				else{
					foreach($XmlElement->entry as $entry){
						if($ItemCount==0)
							break;
						// add a new RSSItem object to the RSSFeed
						$RSSItem = new RSSItem();
						$RSSItem->Title((string) $entry->title);
						$RSSItem->Date((string) $entry->published);
				        $RSSItem->Author((string) $entry->author->name);
						$RSSItem->Content((string) trim($entry->content));	
					
						foreach($entry->link as $link){
							if($link['rel']=="replies" && $link['type']=="text/html"){
								$RSSItem->CommentLink((string) $link['href']);
							}
							if($link['rel']=="alternate" && $link['type']=="text/html"){
								$RSSItem->Link((string) $link['href']);
							}
						}	
						//add RSSItem to the RSS Feed object.
						$RSSFeed[] = $RSSItem;
						//Decrease Item count
						$ItemCount--;
					}
				}
				return $RSSFeed;
			}			
			// Process XML file, if success load ParsedArray
			return false;
		}
	}
	
	class RSSItem{
		private $Title;
		private $Link;
		private $CommentLink;
		private $Date;
		private $Author;
		private $Content;
		
		function RSSItem(){}
		function Title($Title=""){
			if($Title == "")
				return $this->Title;
			$this->Title = $Title;
		}
		function Link($Link=""){
			if($Link == "")
				return $this->Link;
			$this->Link = $Link;
		}
		function CommentLink($CommentLink=""){
			if($CommentLink == "")
				return $this->CommentLink;
			$this->CommentLink = $CommentLink;
		}
		function Date($Date=""){
			if($Date == "")
				return $this->Date;
			$this->Date = $Date;
		}
		function Author($Author=""){
			if($Author == "")
				return $this->Author;
			$this->Author = $Author;
		}
		function Content($Content=""){
			if($Content == "")
				return $this->Content;
			$this->Content = $Content;
		}
	}

?>