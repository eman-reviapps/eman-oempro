<?php
/**
 * THIS FILE SHOULD NOT BE RUN DIRECTLY. THIS IS JUST HELPER FILE FOR ALL OTHER
 * CRON MODULES.
 */

/**
 * Directory separator
 */
$DS = DIRECTORY_SEPARATOR;

/**
 * Path of Oempro installation directory
 */
$APP_PATH = rtrim(str_replace($DS.'extras'.$DS, '', dirname(__FILE__).$DS), $DS) . $DS;

/**
 * Path of Oempro data directory
 */
$DATA_PATH = $APP_PATH . 'data' . $DS;
