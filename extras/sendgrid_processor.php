<?php
/*
 * Oempro
 * (c)Copyright Octeth. All rights reserved.
 * http://octeth.com
 *
 * SendGrid.com - Oempro Integration Module
 *
 *
 */
include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End

if (DEMO_MODE_ENABLED == true)
	{
	print('NOT AVAILABLE IN DEMO MODE.');
	exit;
	}

// Log the process - Start {
$ProcessCode	= 'sendgrid_event_hook';
$ProcessLogID	= Core::RegisterToProcessLog($ProcessCode);
// Log the process - End }

// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

$ShellMessage = '';

try
	{
	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Cron.Bounce', array());
	Plugins::HookListener('Action', 'Extras.Sendgrid.Event.Pre', array());
	// Plug-in hook - End

	$HTTP_RAW_POST_DATA = <<<EOD
	[
    {
        "email": "john.doe@sendgrid.com",
        "sg_event_id": "VzcPxPv7SdWvUugt-xKymw",
        "sg_message_id": "142d9f3f351.7618.254f56.filter-147.22649.52A663508.0",
        "timestamp": 1386636112,
        "smtp-id": "<142d9f3f351.7618.254f56@sendgrid.com>",
        "event": "processed",
        "category":["category1","category2","category3"],
        "id": "001",
        "purchase":"PO1452297845",
        "uid": "123456"
    },
    {
        "email": "not an email address",
        "smtp-id": "<4FB29F5D.5080404@sendgrid.com>",
        "timestamp": 1386636115,
        "reason": "Invalid",
        "event": "dropped",
        "category":["category1","category2","category3"],
        "id": "001",
        "purchase":"PO1452297845",
        "uid": "123456"
    },
    {
        "email": "john.doe@sendgrid.com",
        "sg_event_id": "vZL1Dhx34srS-HkO-gTXBLg",
        "sg_message_id": "142d9f3f351.7618.254f56.filter-147.22649.52A663508.0",
        "timestamp": 1386636113,
        "smtp-id": "<142d9f3f351.7618.254f56@sendgrid.com>",
        "event": "delivered",
        "category":["category1","category2","category3"],
        "id": "001",
        "purchase":"PO1452297845",
        "uid": "123456"
    },
    {
        "email": "john.smith@sendgrid.com",
        "timestamp": 1386636127,
        "uid": "123456",
        "ip": "174.127.33.234",
        "purchase": "PO1452297845",
        "useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36",
        "id": "001",
        "category": ["category1","category2","category3"],
        "event": "open"
    },
    {
        "uid": "123456",
        "ip": "174.56.33.234",
        "purchase":"PO1452297845",
        "useragent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36",
        "event":"click",
        "email": "john.doe@sendgrid.com",
        "timestamp":1386637216,
        "url":"http://www.google.com/",
        "category":["category1","category2","category3"],
        "id":"001"
    },
    {
        "uid": "123456",
        "status": "5.1.1",
        "sg_event_id": "X_C_clhwSIi4EStEpol-SQ",
        "reason": "550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces. Learn more at http: //support.google.com/mail/bin/answer.py?answer=6596 do3si8775385pbc.262 - gsmtp ",
        "purchase": "PO1452297845",
        "event": "bounce",
        "email": "asdfasdflksjfe@sendgrid.com",
        "timestamp": 1386637483,
        "smtp-id": "<142da08cd6e.5e4a.310b89@localhost.localdomain>",
        "type": "bounce",
        "category": ["category1","category2","category3"],
        "id": "001"
    },
    {
        "email":"john.doe@gmail.com",
        "timestamp":1386638248,
        "uid":"123456",
        "purchase":"PO1452297845",
        "id":"001",
        "category":["category1","category2","category3"],
        "event":"unsubscribe"
    }
]
EOD;

	$ShellMessage = '';
	$RawSendgridData = json_decode($HTTP_RAW_POST_DATA);
	if ($RawSendgridData == null) throw new Exception('Invalid hook data');

	foreach ($RawSendgridData as $EachData) {
		$SendgridData = array();
		$SendgridData['Status']			= (isset($EachData->status) == false ? false : $EachData->status);
		$SendgridData['Reason']			= (isset($EachData->reason) == false ? false : $EachData->reason);
		$SendgridData['Event']			= (isset($EachData->event) == false ? false : $EachData->event);
		$SendgridData['AbuseID']		= (isset($EachData->abused) == false ? false : $EachData->abused);
		$SendgridData['EmailAddress']	= (isset($EachData->email) == false ? false : $EachData->email);
		$SendgridData['SMTPID']			= (isset($EachData->{'smtp-id'}) == false ? false : $EachData->{'smtp-id'});
		$SendgridData['TimeStamp']		= (isset($EachData->timestamp) == false ? false : $EachData->timestamp);
		$SendgridData['Category']		= (isset($EachData->category) == false ? false : $EachData->category);
		$SendgridData['Type']			= (isset($EachData->type) == false ? false : $EachData->type);
		$SendgridData['Response']		= (isset($EachData->response) == false ? false : $EachData->response);

		if ($SendgridData['EmailAddress'] != false && $SendgridData['AbuseID'] != false)
			{
			$AbuseIDParameters = explode('-', $SendgridData['AbuseID']);
			foreach ($AbuseIDParameters as $Index=>$Parameter)
				{
				$AbuseIDParameters[$Index] = Core::DecryptNumberAdvanced($Parameter);
				}

			$SendgridData['CampaignID']		= $AbuseIDParameters[0];
			$SendgridData['SubscriberID']	= $AbuseIDParameters[1];
			$SendgridData['EmailAddress']	= $AbuseIDParameters[2];
			$SendgridData['ListID']			= $AbuseIDParameters[3];
			$SendgridData['UserID']			= $AbuseIDParameters[4];
			$SendgridData['AutoResponderID']= $AbuseIDParameters[5];

			if (strtolower($SendgridData['Event']) == 'dropped')
				{
				// Sendgrid does not send Status parameter with dropped event
				$SendgridData['Status'] = '500';
				}

			// Detect Sendgrid event type
			if (strtolower($SendgridData['Event']) == 'bounce' || strtolower($SendgridData['Event']) == 'dropped')
				{
				// Bounce or Dropped Event

				O_Bounce_Processor::process_sendgrid($SendgridData, THRESHOLD_SOFT_BOUNCE_DETECTION);

				$ShellMessage = 'Bounce event for '.$SendgridData['EmailAddress'].' ('.$SendgridData['AbuseID'].') is processed';
				}
			elseif (strtolower($SendgridData['Event']) == 'spamreport')
				{
				// Spam Complaint Event

				O_FBL_Processor::process_sendgrid($SendgridData);

				$ShellMessage = 'Spam complaint event for '.$SendgridData['EmailAddress'].' ('.$SendgridData['AbuseID'].') is processed';
				}
			elseif (strtolower($SendgridData['Event']) == 'unsubscribe')
				{
				// Unsubscription Event

				$_POST = array(
					'FormValue_CampaignID'		=> $SendgridData['CampaignID'],
					'FormValue_AutoResponderID'	=> $SendgridData['AutoResponderID'],
					'FormValue_SubscriberID'	=> $SendgridData['SubscriberID'],
					'FormValue_ListID'			=> $SendgridData['ListID'],
					'FormValue_EmailID'			=> 0,
					'Preview'					=> 0,
				);

				include_once '../unsubscribe.php';

				$ShellMessage = 'Unsubscription event for '.$SendgridData['EmailAddress'].' ('.$SendgridData['AbuseID'].') is processed';
				}
			}
		else
			{
			$ShellMessage .= "\n" . 'Process failed. Email address or Abuse ID not found. '.var_export($SendgridData, true);
			}

		// Plug-in hook - Start
		Plugins::HookListener('Action', 'Extras.Sendgrid.Event.Post', array());
		// Plug-in hook - End
	}

	}
catch (Exception $e)
	{
	if ($e->GetMessage() != '')
		{
		$ShellMessage = $e->GetMessage();
		}
	}

// Log the process - Start {
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);
// Log the process - End }

// Display success message - Start
print($ShellMessage);
// Display success message - End
