<?php if (! is_cli()) exit('This script can only be ran from CLI.');

// COMMENT OUT THE FOLLOWING LINE TO ENABLE THIS SCRIPT
exit("\n\nThis script is disabled...\n\n");


// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================
// ============================================================================================================================


set_time_limit(0);
ignore_user_abort(true);

$IsCLI = true;

include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

Core::LoadObject('api');
Core::LoadObject('users');

echo "\n\nRetrieving all users...";
$users = get_all_users();
echo "\n".count($users)." users retrieved. Starting to send them password reminder email one by one...\n";
foreach ($users as $each_user) {
	echo "\nSending reminder email for user id #".$each_user['UserID']."...";
	API::call(array(
		'format'	=>	'array',
		'command'	=>	'user.passwordremind',
		'parameters'=>	array(
			'emailaddress'		=> $each_user['EmailAddress'],
			'customresetlink'	=> rawurlencode(base64_encode(APP_URL.APP_DIRNAME.(HTACCESS_ENABLED == false ? '/index.php?' : '').'/user/passwordreminder/reset/%s'))
			)
		)
	);
	echo "sent";
}
echo "\n\nDONE\n\n";
exit;

function get_all_users() {
	$users = Users::RetrieveUsers(array('UserID', 'EmailAddress'), array());
	if (is_bool($users) && $users === false) exit("\nNo users on the system.\n\n");
	return $users;
}

function is_cli() {
	return (! isset($_SERVER['SERVER_SOFTWARE']) && (php_sapi_name() == 'cli' || (is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)));
}