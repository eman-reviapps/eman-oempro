<?php
/*
 * Oempro
 * (c)Copyright Octeth. All rights reserved.
 * http://octeth.com
 *
 * Mailgun.com - Oempro Integration Module
 *
 *
 *
 * IMPORTANT: Mailgun webhook integration has not been done yet.
 * This integration will be included in coming Oempro update releases.
 *
 */

include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End

if (DEMO_MODE_ENABLED == true)
	{
	print('NOT AVAILABLE IN DEMO MODE.');
	exit;
	}

// Log the process - Start {
$ProcessCode	= 'mailgun_event_hook';
$ProcessLogID	= Core::RegisterToProcessLog($ProcessCode);
// Log the process - End }

// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

$ShellMessage = '';

try
	{
	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Cron.Bounce', array());
	Plugins::HookListener('Action', 'Extras.Mailgun.Event.Pre', array());
	// Plug-in hook - End

	$MailgunEvent = (isset($_POST['event']) == true ? $_POST['event'] : false);

	if (is_bool($MailgunEvent) == true && $MailgunEvent == false)
	{
		throw new Exception('Invalid Mailgun event');
	}

	if (strtolower($MailgunEvent) == 'opened')
	{
		// Oempro already handles this event
	}
	elseif (strtolower($MailgunEvent) == 'clicked')
	{
		// Oempro already handles this event
	}
	elseif (strtolower($MailgunEvent) == 'bounced')
	{
		O_Bounce_Processor::process_mailgun(array(

		), THRESHOLD_SOFT_BOUNCE_DETECTION);

		$ShellMessage = 'Bounce event is processed';
	}
	elseif (strtolower($MailgunEvent) == 'unsubscribed')
	{
		$_POST = array(
			'FormValue_CampaignID'		=> '',
			'FormValue_AutoResponderID'	=> '',
			'FormValue_SubscriberID'	=> '',
			'FormValue_ListID'			=> '',
			'FormValue_EmailID'			=> 0,
			'Preview'					=> 0,
		);

		include_once '../unsubscribe.php';

		$ShellMessage = 'Unsubscription event is processed';
	}
	elseif (strtolower($MailgunEvent) == 'complained')
	{
		O_FBL_Processor::process_mailgun(array(

		));

		$ShellMessage = 'Spam complaint event is processed';
	}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Extras.Mailgun.Event.Post', array());
	// Plug-in hook - End
	}
catch (Exception $e)
	{
	if ($e->GetMessage() != '')
		{
		$ShellMessage = $e->GetMessage();
		}
	}

// Log the process - Start {
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);
// Log the process - End }

// Display success message - Start
print($ShellMessage);
// Display success message - End
