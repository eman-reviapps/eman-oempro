<?php
/*
 * Oempro
 * (c)Copyright Octeth. All rights reserved.
 * http://octeth.com
 *
 * Mailjet.com - Oempro Integration Module
 *
 *
 *
 * IMPORTANT: Mailjet webhook integration has not been done yet.
 * This integration will be included in coming Oempro update releases.
 *
 */

include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End

if (DEMO_MODE_ENABLED == true)
	{
	print('NOT AVAILABLE IN DEMO MODE.');
	exit;
	}

// Log the process - Start {
$ProcessCode	= 'mailjet_event_hook';
$ProcessLogID	= Core::RegisterToProcessLog($ProcessCode);
// Log the process - End }

// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

$ShellMessage = '';

try
	{
	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Cron.Bounce', array());
	Plugins::HookListener('Action', 'Extras.Mailjet.Event.Pre', array());
	// Plug-in hook - End

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Extras.Mailjet.Event.Post', array());
	// Plug-in hook - End
	}
catch (Exception $e)
	{
	if ($e->GetMessage() != '')
		{
		$ShellMessage = $e->GetMessage();
		}
	}

// Log the process - Start {
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);
// Log the process - End }

// Display success message - Start
print($ShellMessage);
// Display success message - End
