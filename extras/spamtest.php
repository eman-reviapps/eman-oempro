<?php
/*
 * (c)Copyright Octeth. All rights reserved.
 * http://octeth.com
 *
 *
 * Installing SpamAssasin test tool on Linux systems
 *
 * If you are using RedHat/CentOS/Fedora distributions, you can
 * easily install SpamAssassin with the yum package manager:
 *
 * $ yum install spamassassin
 *
 * For more information about installing SpamAssassin, please
 * contact your server administrator. This is beyond Octeth Team
 * service limits.
 *
 * Notice: This PHP script will execute the SpamAssassin executable
 * make sure PHP script has privilege to execute SpamAssassin.
 *
 * This script will be executed with HTTP request. Therefore make sure
 * it's accessible through the web. Do not forget to set SPAM_FILTER_URL
 * constant in data/config.inc.php file
 *
 */

// -------------=[ CONFIGURATION ]=----------------
$SpamAssassinPath = '/usr/bin/spamassassin';





// -------------=[ PROCESS ]=----------------
// Initialize variables - STARTED
$NewsletterContent  = (isset($_POST['FormValue_NewsletterContent']) == true ? $_POST['FormValue_NewsletterContent'] : null);
if ($NewsletterContent == null)
	{
	exit;
	}
// Initialize variables - FINISHED

// Perform SPAM rating - STARTED
$Return = @shell_exec('echo '.escapeshellarg($NewsletterContent).' | '.$SpamAssassinPath.' -Lt');

preg_match_all('/X-Spam-Status: (.*)tests/i', $Return, $ArrayMatches);

print("<Return>");
print("<Header>".$ArrayMatches[0][0]."</Header>");

preg_match_all('/Content analysis details(.*)$/is', $Return, $ArrayMatches);

print("<Scores>".$ArrayMatches[0][0]."</Scores>");
print("</Return>");
// Perform SPAM rating - FINISHED

?>
