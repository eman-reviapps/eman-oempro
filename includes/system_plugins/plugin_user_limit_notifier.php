<?php

class plugin_user_limit_notifier extends Plugins {

    function __construct() {
        
    }

    function load_plugin_user_limit_notifier() {
        parent::RegisterHook('Action', 'UI.All', 'plugin_user_limit_notifier', 'notify', 10, 0);
    }

    function notify() {
        $ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);

        if (DISPLAY_LIMIT_NOTICE) {
            if ($ArrayUserInformation['GroupInformation']['LimitCampaignSendPerPeriod'] > 0) {
                Core::LoadObject('payments');
                // Check if payment period for this user exists for the current period - Start
                Payments::SetUser($ArrayUserInformation);
                Payments::CheckIfPaymentPeriodExists();
                // Check if payment period for this user exists for the current period - End
                // Check if user has exceeded monthly campaign sending limit - Start
                $ArrayPaymentPeriod = Payments::GetLog();
                if (($ArrayPaymentPeriod['CampaignsSent'] >= $ArrayUserInformation['GroupInformation']['LimitCampaignSendPerPeriod']) && ($ArrayUserInformation['GroupInformation']['LimitCampaignSendPerPeriod'] > 0)) {
                    print('<div class="note note-info"><span>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1500'] . '</span></div>');
                }
                // Check if user has exceeded monthly campaign sending limit - End
            }

            if ($ArrayUserInformation['GroupInformation']['LimitEmailSendPerPeriod'] > 0) {
                Core::LoadObject('statistics');
                $TotalSentInThisPeriod = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUserInformation['UserID'], date('Y-m-01 00:00:00'), date('Y-m-31 23:59:59'));

                $RemainingSendingLimit = $ArrayUserInformation['GroupInformation']['LimitEmailSendPerPeriod'] - $TotalSentInThisPeriod;
                if ($RemainingSendingLimit < 1) {
                    print('<div class="note note-info"><span>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1501'] . '</span></div>');
                }
            }
        }

        if (DISPLAY_CREDIT_NOTICE && $ArrayUserInformation['GroupInformation']['CreditSystem'] == 'Enabled' && $ArrayUserInformation['GroupInformation']['PaymentSystem'] == 'Enabled' && $ArrayUserInformation['AvailableCredits'] <= CREDIT_NOTICE_THRESHOLD) {
            print('<div class="note note-info"><span class="alert-heading bold font-red">'.ApplicationHeader::$ArrayLanguageStrings['Screen']['9279'].'! </span><span>' . sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1499'], $ArrayUserInformation['AvailableCredits']) . '</span></div>');
        }
    }

}
