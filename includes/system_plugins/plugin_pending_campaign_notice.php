<?php
class plugin_pending_campaign_notice extends Plugins
{
	function __construct()
	{

	}

	function load_plugin_pending_campaign_notice()
	{
		 parent::RegisterHook('Action', 'UI.Campaign.Details', 'plugin_pending_campaign_notice', 'displayNotice', 10, 1);
	}

	function displayNotice($CampaignInformation)
	{
		if ($CampaignInformation['CampaignStatus'] == 'Pending Approval') {
			echo '<div class="container"><div class="notice-header campaign-notice">'.ApplicationHeader::$ArrayLanguageStrings['Screen']['1799'].'</div></div>';
		}
	}


}