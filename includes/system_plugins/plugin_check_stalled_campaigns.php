<?php
/**
 * Checks for stalled email campaigns and resumes them
 *
 * @package default
 * @author Cem Hurturk
 */
class plugin_check_stalled_campaigns extends Plugins
{
private static $ProcessCode				= 'plugin_check_stalled_campaigns';
private static $ProcessRunInterval		= 1800; // every 30 minutes
private static $ProcessLogID			= 0;
private static $CampaignStallThreshold	= 3600; // One hour

function __construct()
	{

	}

function load_plugin_check_stalled_campaigns()
	{
	parent::RegisterHook('Action', 'Cron.General', 'plugin_check_stalled_campaigns', 'CheckForStalledCampaigns', 10, 0);
	}

function CheckForStalledCampaigns()
	{
	if (Core::CheckProcessLog(self::$ProcessCode, self::$ProcessRunInterval) == false)
		{
		return false;
		}

	Core::LoadObject('campaigns');
	
	self::$ProcessLogID = Core::RegisterToProcessLog(self::$ProcessCode);

	$RecoveredCampaigns = 0;

	$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."campaigns WHERE LastActivityDateTime < '".date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - self::$CampaignStallThreshold)."' AND CampaignStatus='Sending'";
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);

	if (mysql_num_rows($ResultSet) > 0)
		{
		while ($EachCampaign = mysql_fetch_assoc($ResultSet))
			{
			$ArrayFieldnValues = array
									(
									'CampaignStatus'		=> 'Paused',
									'LastActivityDateTime'	=> date('Y-m-d H:i:s'),
									);
			Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaign['CampaignID']));
			
			$RecoveredCampaigns++;
			}			
		}

	Core::RegisterToProcessLog(self::$ProcessCode, 'Completed', $RecoveredCampaigns.' campaigns have been recovered', self::$ProcessLogID);
	}
}
?>