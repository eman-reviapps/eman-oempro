<?php
/**
 * Cleans the data directory and keeps it clean
 *
 * @package default
 * @author Cem Hurturk
 */
class plugin_clean_datadir extends Plugins
{
private static $ProcessCode			= 'plugin_clean_datadir';
private static $ProcessRunInterval	= 86400; // every 1 day
private static $ProcessLogID		= 0;
private static $DirsToCheck			= array('imports', 'tmp');
private static $FileExpireSeconds	= 259200; // 3 days

function __construct()
	{
	
	}

function load_plugin_clean_datadir()
	{
	parent::RegisterHook('Action', 'Cron.General', 'plugin_clean_datadir', 'CleanDataDir', 10, 0);
	}

function CleanDataDir()
	{
	if (Core::CheckProcessLog(self::$ProcessCode, self::$ProcessRunInterval) == false)
		{
		return false;
		}
	
	self::$ProcessLogID = Core::RegisterToProcessLog(self::$ProcessCode);

	$DeletedFileCounter = 0;
	
	foreach (self::$DirsToCheck as $EachDir)
		{
		if ($DirHandler = opendir(DATA_PATH.$EachDir))	
			{
			while (($EachFile = readdir($DirHandler)) !== false)
				{
				if (($EachFile != '.') && ($EachFile != '..') && (preg_match('/^\./i', $EachFile) == 0) && (preg_match('/\.php$/i', $EachFile) == 0) && (filetype(DATA_PATH.$EachDir.'/'.$EachFile) == 'file'))
					{
					$FileModificationTime = filemtime(DATA_PATH.$EachDir.'/'.$EachFile);
					
					if ($FileModificationTime + self::$FileExpireSeconds <= strtotime(date('Y-m-d H:i:s')))
						{
						unlink(DATA_PATH.$EachDir.'/'.$EachFile);
						$DeletedFileCounter++;
						}
					}
				}
			closedir($DirHandler);
			}
		}
	
	Core::RegisterToProcessLog(self::$ProcessCode, 'Completed', $DeletedFileCounter.' temporary files are removed', self::$ProcessLogID);
	}
}
?>