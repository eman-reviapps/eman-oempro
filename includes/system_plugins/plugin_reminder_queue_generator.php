<?php

/**
 * Adds emails to the transactional queue for autoresponder reminders
 *
 * @package default
 * @author Mert Hurturk
 */
class plugin_reminder_queue_generator extends Plugins {

    private static $ProcessCode = 'plugin_reminder_queue_generator';
    private static $ProcessRunInterval = 86400; // once in a day
    private static $ProcessLogID = 0;

    function __construct() {
        
    }

    function load_plugin_reminder_queue_generator() {
        parent::RegisterHook('Action', 'Cron.General', 'plugin_reminder_queue_generator', 'CheckForRemindersToBeSent', 10, 0);
    }

    function CheckForRemindersToBeSent() {
        if (Core::CheckProcessLog(self::$ProcessCode, self::$ProcessRunInterval) == false) {
            return false;
        }

        Core::LoadObject('auto_responders');
        Core::LoadObject('users');
        Core::LoadObject('subscribers');
        Core::LoadObject('api');

        self::$ProcessLogID = Core::RegisterToProcessLog(self::$ProcessCode);

        $today = date('Y-m-d');
        $userCache = array();
        $queuedReminders = 0;
        $queuedSubscribers = 0;

        $autoResponders = AutoResponders::RetrieveResponders(
                        array('*'), array('AutoResponderTriggerType' => 'OnSubscriberDate'), $ArrayOrder = array('AutoResponderName' => 'ASC'), false
        );

        if ($autoResponders != FALSE) {
            foreach ($autoResponders as $eachAutoResponder) {
                $queuedReminders++;

                // Retrieve user information - Start {
                if (!isset($userCache[$eachAutoResponder['RelOwnerUserID']])) {
                    $arrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $eachAutoResponder['RelOwnerUserID']), false);
                    $userCache[$eachAutoResponder['RelOwnerUserID']] = $arrayUserInformation;
                } else {
                    $arrayUserInformation = $userCache[$eachAutoResponder['RelOwnerUserID']];
                }
                // Retrieve user information - End }
                // Calculate time to send - Start {
                switch ($eachAutoResponder['TriggerTimeType']) {
                    case 'Immediately':
                        $time = strtotime($today);
                        break;
                    case 'Seconds later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' seconds', strtotime($today));
                        break;
                    case 'Minutes later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' minutes', strtotime($today));
                        break;
                    case 'Hours later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' hours', strtotime($today));
                        break;
                    case 'Days later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' days', strtotime($today));
                        break;
                    case 'Weeks later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' weeks', strtotime($today));
                        break;
                    case 'Months later':
                        $time = strtotime('-' . $eachAutoResponder['TriggerTime'] . ' months', strtotime($today));
                        break;
                }
                $eachAutoResponder['AutoResponderTriggerValue2'] = $eachAutoResponder['AutoResponderTriggerValue2'] == '' ? 'Every month' : $eachAutoResponder['AutoResponderTriggerValue2'];
                $timeToSend = $eachAutoResponder['AutoResponderTriggerValue2'] == 'Every month' ? date('d', $time) : date('d-m', $time);
                $timeToSendFormat = $eachAutoResponder['AutoResponderTriggerValue2'] == 'Every month' ? '%d' : '%d-%m';
                // Calculate time to send - End }
                // Retrieve subscribers - Start {
                $field = strpos($eachAutoResponder['AutoResponderTriggerValue'], 'SubscriptionDate') ? 'SubscriptionDate' : 'CustomField' . $eachAutoResponder['AutoResponderTriggerValue'];
                $field = 'DATE_FORMAT(' . $field . ', "' . $timeToSendFormat . '")';

                $arraySubscribers = API::call(array(
                            'format' => 'array',
                            'command' => 'subscribers.search',
                            'protected' => true,
                            'username' => $arrayUserInformation['Username'],
                            'password' => $arrayUserInformation['Password'],
                            'parameters' => array(
                                'listid' => $eachAutoResponder['RelListID'],
                                'operator' => 'and',
                                'rules' => '[[SubscriptionStatus]||[Is]||[Subscribed]],,,[[BounceType]||[Is not]||[Hard]],,,[[' . $field . ']||[Is]||[' . $timeToSend . ']],,,[[' . $field . ']||[Is not]||[0000-00-00]]',
                                'recordsfrom' => 0,
                                'recordsperrequest' => 999000000,
                                'orderfield' => 'SubscriberID',
                                'ordertype' => 'ASC'
                            )
                ));
                $arraySubscribers = $arraySubscribers['Subscribers'];
                // Retrieve subscribers - End }

                if (!is_array($arraySubscribers) || count($arraySubscribers) < 1)
                    continue;

                foreach ($arraySubscribers as $eachSubscriber) {
                    EmailQueue::RegisterIntoQueue(
                            $eachAutoResponder['RelOwnerUserID'], $eachAutoResponder['RelListID'], $eachSubscriber['SubscriberID'], $today . ' 00:00:00', $eachAutoResponder['AutoResponderID'], $eachAutoResponder['RelEmailID'], 'Auto Responder'
                    );
                    $queuedSubscribers++;
                }
            }
        }

        Core::RegisterToProcessLog(self::$ProcessCode, 'Completed', 'Emails have been queued for ' . $queuedReminders . ' reminders with the total of ' . $queuedSubscribers . ' subscribers', self::$ProcessLogID);
    }

}
