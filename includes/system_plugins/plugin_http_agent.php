<?php
class plugin_http_agent extends Plugins
{
	function __construct()
	{

	}

	function load_plugin_http_agent()
	{
		// THIS PLUGIN IS DISABLED DUE TO HIGH LOAD ON DATABASE
		// parent::RegisterHook('Action', 'Track.Open', 'plugin_http_agent', 'track', 10, 4);
	}

	function track($list, $subscriber, $campaign, $autoResponder)
	{
		$agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

		$mapper = O_Registry::instance()->getMapper('LogUserAgents');

		$log = new O_Domain_LogUserAgent();
		$log->setUserAgentString($_SERVER['HTTP_USER_AGENT']);
		$log->setReferrer($_SERVER['HTTP_REFERER']);
		$log->setRelListID($list['ListID']);
		$log->setRelSubscriberID($subscriber['SubscriberID']);
		if ($campaign != FALSE && count($campaign) > 0) {
			$log->setRelCampaignID($campaign['CampaignID']);
		} else {
			$log->setRelCampaignID(0);
		}
		if ($autoResponder != FALSE && count($autoResponder) > 0) {
			$log->setRelAutoResponderID($autoResponder['AutoResponderID']);
		} else {
			$log->setRelAutoResponderID(0);
		}
		$log->setDateTime(date('Y-m-d H:i:s'));

		$mapper->insert($log);

		unset($log);
		unset($mapper);
	}
}
