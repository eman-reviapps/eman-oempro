<?php

class plugin_user_invoices_checker extends Plugins {

    function __construct() {
        
    }

    function load_plugin_user_invoices_checker() {
        parent::RegisterHook('Action', 'UI.AccountChecker', 'plugin_user_invoices_checker', 'check', 10, 0);
    }

    function check() {

        $ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
       
        $IsUnTrusted = AccountIsUnTrustedCheck($ArrayUserInformation);
        return $IsUnTrusted;
    }

}
