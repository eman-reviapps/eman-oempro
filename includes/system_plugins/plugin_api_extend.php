<?php
class plugin_api_extend extends Plugins
{
	public static $Extensions = array();

function load_plugin_api_extend()
	{
	parent::RegisterHook('Action', 'Api.Plugin.Extend', 'plugin_api_extend', 'api_command', 10, 1);
	}

function api_command($CommandName)
	{

	if (! isset(self::$Extensions[$CommandName])) return array();
	
	return array(
		'PluginCode'	=> self::$Extensions[$CommandName]['PluginCode'],
		'Method'		=> self::$Extensions[$CommandName]['Method'],
		'For'			=> implode(',', self::$Extensions[$CommandName]['For'])
	);

	}


}
?>