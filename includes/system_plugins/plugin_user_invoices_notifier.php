<?php

class plugin_user_invoices_notifier extends Plugins {

    function __construct() {
        
    }

    function load_plugin_user_invoices_notifier() {
        parent::RegisterHook('Action', 'UI.Invoices', 'plugin_user_invoices_notifier', 'check', 10, 0);
    }

    function check() {


        $ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        Core::LoadObject('payments');
        Core::LoadObject('subscribers');
        Payments::SetUser($ArrayUserInformation);

        //display invoices for non-credit accounts
        if ($ArrayUserInformation['GroupInformation']['CreditSystem'] == 'Disabled' && $ArrayUserInformation['GroupInformation']['PaymentSystem'] == 'Enabled') {

            $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

            if ($LogExists) {

                $ArrayPaymentPeriod = Payments::GetLog();
                // if user is untrusted or has upaid invoices.  
                if ($ArrayPaymentPeriod['PaymentStatus'] != 'Paid' || $ArrayUserInformation['ReputationLevel'] != 'Trusted') {
                    $url = InterfaceAppURL(true) . "/process" . '/invoices';
                    $message = ApplicationHeader::$ArrayLanguageStrings['Screen']['9277'];
                    $btn_text = ApplicationHeader::$ArrayLanguageStrings['Screen']['9278'];
                    $header = ApplicationHeader::$ArrayLanguageStrings['Screen']['9279'];

                    $text = <<<EOD
<div class="note note-info">
    <span class="alert-heading bold font-red">$header! </span>
    <span>$message</span>
    <br/>
    <br/>
    <p>
        <a class="btn red" href="$url" > $btn_text </a>
    </p>
</div>
EOD;
                    print($text);
                } else {
                    $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($ArrayUserInformation['UserID']);

                    if (($TotalSubscribersOnTheAccount) >= $ArrayUserInformation['GroupInformation']['LimitSubscribers'] && ($ArrayUserInformation['GroupInformation']['LimitSubscribers'] > 0)) {

                        $url = InterfaceAppURL(true) . "/process" . '/upgrade';
                        $message = ApplicationHeader::$ArrayLanguageStrings['Screen']['9280'];
                        $btn_text = ApplicationHeader::$ArrayLanguageStrings['Screen']['9281'];
                        $header = ApplicationHeader::$ArrayLanguageStrings['Screen']['9279'];
                        $Limit = $ArrayUserInformation['GroupInformation']['LimitSubscribers'];

                        $text = <<<EOD
<div class="note note-info">
    <span class="alert-heading bold font-red">$header! </span>
    <span>$message ( <span class="bold" >$Limit</span> ) .</span>
    <br/>
    <br/>
    <p>
        <a class="btn red" href="$url" > $btn_text </a>
    </p>
</div>
EOD;
                        print($text);
                    }
                }
            }
        }
    }

}
