<?php
class plugin_subscriber_filter_rule_suppression extends Plugins
{
	function load_plugin_subscriber_filter_rule_suppression()
		{
		parent::RegisterHook('Filter', 'SubscriberRuleFields', 'plugin_subscriber_filter_rule_suppression', 'addRule', 10, 1);
		parent::RegisterHook('Filter', 'MysqlCriteriaFilter', 'plugin_subscriber_filter_rule_suppression', 'parseCriteria', 10, 1);
		parent::RegisterHook('Filter', 'MysqlQueryGetRowsFilter', 'plugin_subscriber_filter_rule_suppression', 'parseGetRowsQuery', 10, 1);
		}

	function addRule($Rules)
		{
		$Rules[] = array('CustomFieldID'=>'SuppressionStatus', 'ValidationMethod'=>'Enum', 'Values'=>'[['.htmlspecialchars(ApplicationHeader::$ArrayLanguageStrings['Screen']['1747'], ENT_QUOTES).']||[Both]],,,[['.htmlspecialchars(ApplicationHeader::$ArrayLanguageStrings['Screen']['1748'], ENT_QUOTES).']||[Global]],,,[['.htmlspecialchars(ApplicationHeader::$ArrayLanguageStrings['Screen']['1749'], ENT_QUOTES).']||[Local]]');
		return array($Rules);
		}

	function parseCriteria($Criteria)
		{
		if ($Criteria['Column'] == 'SuppressionStatus')
			{
			if ($Criteria['Operator'] === '=')
				{
				$Criteria['Operator'] = 'IN';
				}
			else
				{
				$Criteria['Operator'] = 'NOT IN';
				}

			if ($Criteria['Value'] === 'Local')
				{
				$Criteria['ValueWOQuote'] = '(SELECT EmailAddress FROM oempro_suppression_list WHERE RelListID = %subscriber_list_id%)';
				}
			else if ($Criteria['Value'] === 'Global')
				{
				$Criteria['ValueWOQuote'] = '(SELECT EmailAddress FROM oempro_suppression_list WHERE RelListID = 0)';
				}
			else if ($Criteria['Value'] === 'Both')
				{
				$Criteria['ValueWOQuote'] = '(SELECT EmailAddress FROM oempro_suppression_list WHERE RelListID = 0 OR RelListID = %subscriber_list_id%)';
				}
			$Criteria['Column'] = 'EmailAddress';
			}

		return array($Criteria);
		}
		
	function parseGetRowsQuery($Query)
		{
		preg_match_all('/FROM oempro_subscribers_([\d]+)\s/', $Query, $Matches, PREG_SET_ORDER);
		if (count($Matches) > 0)
			{
			$Query = str_replace('%subscriber_list_id%', $Matches[0][1], $Query);
			}
		return array($Query);
		}

}