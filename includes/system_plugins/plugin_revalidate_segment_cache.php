<?php
class plugin_revalidate_segment_cache extends Plugins
{
	function __construct()
	{

	}

	function load_plugin_revalidate_segment_cache()
	{
		parent::RegisterHook('Action', 'Delete.Subscriber', 'plugin_revalidate_segment_cache', 'revalidate', 10, 1);
	}

	function revalidate($ListID)
	{
		Core::LoadObject('segments');
		$UpdatedFields = array(
			'SubscriberCountLastCalculatedOn' => date('Y-m-d H:i:s', strtotime('-2 days'))
		);
		Segments::Update($UpdatedFields, array('RelListID' => $ListID));
	}


}