<?php
/**
 * Cleans process log
 *
 * @package default
 * @author Mert Hurturk
 */
class plugin_clean_process_log extends Plugins
{
private static $ProcessCode				= 'plugin_clean_process_log';
private static $ProcessRunInterval		= 86400; // every day
private static $ProcessLogID			= 0;

function __construct()
	{
	}

function load_plugin_clean_process_log()
	{
	parent::RegisterHook('Action', 'Cron.General', 'plugin_clean_process_log', 'CleanLogs', 10, 0);
	}

function CleanLogs()
	{
	if (Core::CheckProcessLog(self::$ProcessCode, self::$ProcessRunInterval) == false)
		return false;

	self::$ProcessLogID = Core::RegisterToProcessLog(self::$ProcessCode);

	$SQLQuery = 'DELETE FROM oempro_processlog WHERE ProcessStatus = "Completed" AND ProcessFinishTime <= DATE_SUB(CURDATE(), INTERVAL 30 DAY) LIMIT 10000';
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);

	Core::RegisterToProcessLog(self::$ProcessCode, 'Completed', 'Process logs older than 30 days have been removed (10,000 at max)', self::$ProcessLogID);
	}
}