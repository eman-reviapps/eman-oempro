<?php
/**
 * These functions are defined if the PHP version is older than the expected
 *
 * @author Cem Hurturk
*/

// For PHP < 5.2.0
if (!function_exists('json_encode'))
	{
	function json_encode($String)
		{
		Core::LoadObject('json');

		$ObjectJSON = new Services_JSON();
		return $ObjectJSON->encodeUnsafe($String);
		}
	}

if (!function_exists('json_decode'))
	{
	function json_decode($String)
		{
		Core::LoadObject('json');

		$ObjectJSON = new Services_JSON();
		return $ObjectJSON->decode($String);
		}
	}

?>