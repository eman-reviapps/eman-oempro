<?php

include_once '../../cli/init.php';
include_once $DATA_PATH . 'config.inc.php';

$Users = Users::RetrieveUsers(array('*'));

foreach ($Users as $User) {
    // Retrieve user information
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $User['UserID']));

    if ($ArrayUser == false)
        continue;

    // Check if user account status is enabled. If not, ignore
    if ($ArrayUser['AccountStatus'] != 'Enabled')
        continue;

    Payments::SetUser($ArrayUser);
    //added by Eman - check if is this a renew for package or current period is not finished yet
    $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

    // Check if payment period for this user exists for the current period
    Payments::SetUser($ArrayUser);
    Payments::CheckIfPaymentPeriodExists();

    $ArrayPaymentPeriod = Payments::GetLog();

    //If user is in credit system and current credit is zero
    $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);
    if ($CreditsCheckReturn[0] == false) {
        Users::UpdateUser(array('ReputationLevel' => 'Untrusted'), array('UserID' => $ArrayUser["UserID"]));
        continue;
    }
    if (($ArrayUser['GroupInformation']['CreditSystem'] == 'Enabled') && ($ArrayUser['AvailableCredits'] <= CREDIT_NOTICE_THRESHOLD)) {

        if ($ArrayUser['CreditNoticeSent'] == 'No') {
            O_Email_Sender_ForAdmin::send(
                    O_Email_Factory::sendCreditLimitNotice($ArrayUser)
            );
            Users::UpdateUser(array('CreditNoticeSent' => 'Yes'), array('UserID' => $ArrayUser["UserID"]));

        }
    }

    //no period exists. so system did renew for user subscription, Here we will update invoice user total 
    //and send an email to user to pay. make user also untrusted untill he pays
    if ((!$LogExists || $ArrayPaymentPeriod['PaymentStatus'] == 'NA')) {
        //get current payment log . if it is just added
        $LogID = $ArrayPaymentPeriod["LogID"];
        UserPayment::setUser($ArrayUser);
        $ProcessBalance = UserPayment::processUserRenew($ArrayPaymentPeriod, false);
    }
}

