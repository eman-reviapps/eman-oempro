<?php

include_once '../../cli/init.php';
include_once $DATA_PATH . 'config.inc.php';

//$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => 70));
//Payments::SetUser($ArrayUser);
////added by Eman - check if is this a renew for package or current period is not finished yet
//$LogExists = Payments::GetCurrentPaymentPeriodIfExists();
//
//// Check if user account status is enabled. If not, ignore this campaign - End
//// Check if payment period for this user exists for the current period - Start
//Payments::SetUser($ArrayUser);
//Payments::CheckIfPaymentPeriodExists();
//print_r($LogExists);
////no period exists. so system did renew for user subscription, Here we will update invoice user total 
////and send an email to user to pay. make user also untrusted untill he pays
//if (!$LogExists) {
//    //get current payment log . if it is just added
//    $ArrayPaymentPeriod = Payments::GetLog();
//    $LogID = $ArrayPaymentPeriod["LogID"];
//
////        $ProcessBalance = UserBalance::processUserBalance($ArrayUser, $ArrayPaymentPeriod, false);
//    UserPayment::setUser($ArrayUser);
//    $ProcessBalance = UserPayment::processUserRenew($ArrayPaymentPeriod, false);
//    //file_put_contents(APP_PATH . "/web_send.txt", print_r($ProcessBalance, true));
//}
/**
 *
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
// Send queued campaign emails
$TotalProcessedCampaigns = 0;

$ArrayPendingCampaigns = Campaigns::GetPendingCampaigns();

// Generate the queue for each campaign - Start
$TotalProcessedCampaigns = 0;

foreach ($ArrayPendingCampaigns as $Key => $EachCampaignID) {
    // Do not allow to process more than 1 campaign per execution - Start
    if ($TotalProcessedCampaigns > 0) {
        break;
    }
    // Do not allow to process more than 1 campaign per execution - End
    // Retrieve campaign information - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    if ($ArrayCampaign['CampaignStatus'] != 'Ready') {
        // If is being processed or processed already - Start
        continue;
        // If is being processed or processed already - End
    }
    // Retrieve campaign information - End
    // Check if the campaign is an a/b split testing campaign - Start {
    $IsAorBSplitTestCampaign = false;
    if (($ArrayCampaign['RelEmailID'] == 0) && (SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']) != false)) {
        $IsAorBSplitTestCampaign = true;
    }
    // Check if the campaign is an a/b split testing campaign - End }
    // If email ID is 0, do not proceed with this campaign - Start
    if (($ArrayCampaign['RelEmailID'] == 0) && ($IsAorBSplitTestCampaign == false)) {
        // If is being processed or processed already - Start
        continue;
        // If is being processed or processed already - End
    }
    // If email ID is 0, do not proceed with this campaign - End
    // Retrieve user information - Start
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayCampaign['RelOwnerUserID']));

    if ($ArrayUser == false) {
        continue;
    }
    // Retrieve user information - End
    // Check if user account status is enabled. If not, ignore this campaign - Start
    if ($ArrayUser['AccountStatus'] != 'Enabled') {
        continue;
    }
    //Check if user is untrusted and didnt pay invoices. If not, ignore this campaign
    //lw untrusted 3ady hyb3t bs lma el admin ywafe2
//     if ($ArrayUser['ReputationLevel'] != 'Trusted')
//        continue;

    Payments::SetUser($ArrayUser);
    //added by Eman - check if is this a renew for package or current period is not finished yet
    $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

    // Check if user account status is enabled. If not, ignore this campaign - End
    // Check if payment period for this user exists for the current period - Start
    Payments::SetUser($ArrayUser);
    Payments::CheckIfPaymentPeriodExists();

    //no period exists. so system did renew for user subscription, Here we will update invoice user total 
    //and send an email to user to pay. make user also untrusted untill he pays
    if (!$LogExists) {
        //get current payment log . if it is just added
        $ArrayPaymentPeriod = Payments::GetLog();
        $LogID = $ArrayPaymentPeriod["LogID"];

//        $ProcessBalance = UserBalance::processUserBalance($ArrayUser, $ArrayPaymentPeriod, false);
        UserPayment::setUser($ArrayUser);
        $ProcessBalance = UserPayment::processUserRenew($ArrayPaymentPeriod, false);
        //file_put_contents(APP_PATH . "/web_send.txt", print_r($ProcessBalance, true));
    }
    // Check if payment period for this user exists for the current period - End
    // Check if user has exceeded monthly campaign sending limit - Start
    $ArrayPaymentPeriod = Payments::GetLog();
    if (($ArrayPaymentPeriod['CampaignsSent'] >= $ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod']) && ($ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod'] > 0)) {
        continue;
    }
    // Check if user has exceeded monthly campaign sending limit - End
    // Start benchmarking - Start
    EmailQueue::StartBenchmarking();
    // Start benchmarking - End
    // Check if there's enough credits for the delivery - Start {
    $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);

    if ($CreditsCheckReturn[0] == false) {
        continue;
    }
    // Check if there's enough credits for the delivery - End }
    // Change campaigns status to 'sending' - Start
    if ($ArrayCampaign['SendProcessStartedOn'] != '0000-00-00 00:00:00') {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
        );
    } else {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
            'SendProcessStartedOn' => date('Y-m-d H:i:s'),
        );
    }
    $ArrayFieldnValues['LastActivityDateTime'] = date('Y-m-d H:i:s');
    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
    // Change campaigns status to 'sending' - End
    // Generate the queue of the campaign - Start
    $TotalRecipients = EmailQueue::GenerateQueue($EachCampaignID, $ArrayCampaign['RelOwnerUserID']);
    // Generate the queue of the campaign - End
    // Update total recipients of the campaign - Start
    Campaigns::Update(array('TotalRecipients' => $TotalRecipients, 'LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));
    // Update total recipients of the campaign - End
    // Check if activity exceeds user group threshold - Start {
    if (($ArrayUser['GroupInformation']['ThresholdEmailSend'] > 0) && ($TotalRecipients > $ArrayUser['GroupInformation']['ThresholdEmailSend'])) {
        O_Email_Sender_ForAdmin::send(
                O_Email_Factory::userExceededEmailSendThreshold(
                        $ArrayUser, array('CampaignID' => $ArrayCampaign['CampaignID'], 'CampaignName' => $ArrayCampaign['CampaignName'], 'TotalRecipients' => $TotalRecipients)
                )
        );

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Threshold.CampaignRecipients', array($ArrayUser, $TotalRecipients, $EachCampaignID));
        // Plug-in hook - End
    }
    // Check if activity exceeds user group threshold - End }
    // Prepare and send emails to the queue - Start
    $TotalEmailsSent = EmailQueue::SendEmails($EachCampaignID, $ArrayUser, 10, $IsAorBSplitTestCampaign);
    // Prepare and send emails to the queue - End
    // Log the activity - Start
    Core::AddToActivityLog($ArrayCampaign['RelOwnerUserID'], 'Campaign Sent', 'Campaign #' . $EachCampaignID . ' sent (' . number_format($TotalEmailsSent) . ' recipients)');
    // Log the activity - End
    // Log the number of sent email in activity statistics  - Start {
    $ArrayActivities = array(
        'TotalSentEmail' => $TotalEmailsSent,
    );
    Statistics::UpdateListActivityStatistics(0, $ArrayCampaign['RelOwnerUserID'], $ArrayActivities);
    // Log the number of sent email in activity statistics  - End }
    // Stop benchmarking - Start
    EmailQueue::StopBenchmarking();
    // Stop benchmarking - End
    // Calculate benchmarking - Start
    $TotalEmailsPerSecond = EmailQueue::CalculateBenchmark($TotalEmailsSent);
    // Calculate benchmarking - End
    // Purge queue records and update campaign - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    if ($ArrayCampaign['CampaignStatus'] == 'Sending') {
        // Campaign is not paused. Proceed with normal procedure
        $TMPArrayReturn = EmailQueue::PurgeQueue($EachCampaignID);

        $ArrayFieldnValues = array(
            'CampaignStatus' => ($ArrayCampaign['ScheduleType'] == 'Recursive' ? 'Ready' : 'Sent'),
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );

        // Campaign is sent. Track the payment activity - Start
        Payments::CampaignSent($ArrayCampaign['CampaignID']);
        // Campaign is sent. Track the payment activity - End
    } else {
        // Campaign has been paused by user.
        $ArrayFieldnValues = array(
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );
    }

    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
    // Purge queue records and update campaign - End

    $TotalProcessedCampaigns++;
}
// Generate the queue for each campaign - End
?>