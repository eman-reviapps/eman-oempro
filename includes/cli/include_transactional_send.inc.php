<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
// Send queued transaction email emails
$TotalProcessedTransactionEmails = 0;

// Retrieve pending X amount of transaction emails - Start
$ArrayPendingEmails = TransactionEmails::RetrieveTransactionEmails(array('*'), array('Status' => 'Pending', array('field' => 'TimeToSend', 'operator' => '<=', 'value' => date('Y-m-d H:i:s'))), array('TimeToSend' => 'ASC'), 0, 500);
// Retrieve pending X amount of transaction emails - End
// Loop each pending transaction email, process and deliver it - Start
foreach ($ArrayPendingEmails as $Index => $ArrayEachEmail) {
    // Check the status of the pending email. It it is not 'pending', ignore it - Start
    $ArrayEachEmail = TransactionEmails::RetrieveTransactionEmail(array('*'), array('Status' => 'Pending', 'TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
    if ($ArrayEachEmail == false || $ArrayEachEmail['RelEmailID'] == 0) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Email information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    } else {
        // Change the status of the queue item to "Sending" - Start
        TransactionEmails::Update(array('Status' => 'Sending'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        // Change the status of the queue item to "Sending" - End
    }
    // Check the status of the pending email. It it is not 'pending', ignore it - End
    // Retrieve user information - Start
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayEachEmail['RelOwnerUserID']));
    if ($ArrayUser == false) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Owner user information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    // Retrieve user information - End
    // Check if there's enough credits for the delivery - Start {
    if (DEDUCT_CREDITS_FROM_AR == true) {
        $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);

        if ($CreditsCheckReturn[0] == false) {
            continue;
        }
    }
    // Check if there's enough credits for the delivery - End }
    // Check if payment period for this user exists for the current period - Start
    Payments::SetUser($ArrayUser);
    Payments::CheckIfPaymentPeriodExists();
    // Check if payment period for this user exists for the current period - End
    // Retrieve list information - Start
    $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayEachEmail['RelListID']), false);
    if ($ArrayList == false) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'List information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    // Retrieve list information - End
    // Retrieve subscriber information - Start
    $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $ArrayEachEmail['RelSubscriberID']), $ArrayEachEmail['RelListID']);
    if ($ArraySubscriber == false) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Subscriber information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }

    if ($ArraySubscriber['SubscriptionStatus'] != 'Subscribed') {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Subscription status is not subscribed'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    // Retrieve subscriber information - End
    // Retrieve email information - Start
    $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayEachEmail['RelEmailID']));
    if ($ArrayEmail == false) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Email information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    // Retrieve email information - End
    // Retrieve auto-responder information - Start
    $ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), array('AutoResponderID' => $ArrayEachEmail['RelAutoResponderID']));
    if ($ArrayAutoResponder == false) {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Auto-Responder information not found'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    
    /* Eman */
    // Retrieve auto-responder information - End
    // Check if user account status is enabled. If not, ignore this autoresponder
    if ($ArrayUser['AccountStatus'] != 'Enabled') {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Owner user is disabled'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    //Check if user is untrusted and didnt pay invoices. If not, ignore this autoresponder
    //lw untrusted 3ady hyb3t bs lma el admin ywafe2
    if ($ArrayUser['ReputationLevel'] != 'Trusted') {
        TransactionEmails::Update(array('Status' => 'Failed', 'StatusMessage' => 'Owner user is untrusted'), array('TransactionalQueueID' => $ArrayEachEmail['TransactionalQueueID']));
        continue;
    }
    /* Eman */
    
    // Retrieve auto-responder information - End
    // Prepare and send the email - Start
    TransactionEmails::SendEmail($ArrayEachEmail, $ArrayUser, $ArrayList, $ArraySubscriber, $ArrayAutoResponder, $ArrayEmail);
    // Prepare and send the email - End
    // Deduct email credits - Start {
    if (DEDUCT_CREDITS_FROM_AR == true) {
        $ArrayUser = Payments::DeductCredits($ArrayUser, true);
    }
    // Deduct email credits - End }
    // Plug-in hook - Start
    Plugins::HookListener('Action', 'Cron.TransactionalSend.AfterDelivery', array($ArrayEachEmail, $ArrayUser, $ArrayList, $ArraySubscriber, $ArrayAutoResponder));
    // Plug-in hook - End

    $TotalProcessedTransactionEmails++;
}
// Loop each pending transaction email, process and deliver it - End
?>