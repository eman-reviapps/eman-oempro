<?php

include_once '../../cli/init.php';
include_once $DATA_PATH . 'config.inc.php';
include_once(PLUGIN_PATH . 'process/libraries/helpers/Validation.php');
include_once(PLUGIN_PATH . 'process/libraries/helpers/Curl.php');
include_once(PLUGIN_PATH . 'process/libraries/helpers/Output.php');
include_once(PLUGIN_PATH . 'process/libraries/Oempro.php');
include_once(PLUGIN_PATH . 'process/libraries/api/OemproAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/AdminAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/UsersAPI.php');
include_once(PLUGIN_PATH . 'process/libraries/api/PaymentAPI.php');

/**
 *
 *
 * @author    Cem Hurturk
 * @version   $Id$
 * @copyright Octeth, 11 November, 2007
 * @package   default
 * */
// Send queued campaign emails
$TotalProcessedCampaigns = 0;

$ArrayPendingCampaigns = Campaigns::GetPendingCampaigns();

// Generate the queue for each campaign - Start
$TotalProcessedCampaigns = 0;

foreach ($ArrayPendingCampaigns as $Key => $EachCampaignID) {
    // Do not allow to process more than 1 campaign per execution
    if ($TotalProcessedCampaigns > 0)
        break;

    // Retrieve campaign information - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    // If is being processed or processed already, do not proceed with this loop
    if ($ArrayCampaign['CampaignStatus'] != 'Ready')
        continue;

    // Check if the campaign is an a/b split testing campaign - Start {
    $IsAorBSplitTestCampaign = false;
    if (($ArrayCampaign['RelEmailID'] == 0) && (SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']) != false))
        $IsAorBSplitTestCampaign = true;

    // If email ID is 0, do not proceed with this campaign
    if (($ArrayCampaign['RelEmailID'] == 0) && ($IsAorBSplitTestCampaign == false))
        continue;

    // Retrieve user information
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayCampaign['RelOwnerUserID']));
    if ($ArrayUser == false)
        continue;

    // Check if user account status is enabled. If not, ignore this campaign
    if ($ArrayUser['AccountStatus'] != 'Enabled')
        continue;

    //Check if user is untrusted and didnt pay invoices. If not, ignore this campaign
    //lw untrusted 3ady hyb3t bs lma el admin ywafe2
//     if ($ArrayUser['ReputationLevel'] != 'Trusted')
//        continue;

    Payments::SetUser($ArrayUser);
    //added by Eman - check if is this a renew for package or current period is not finished yet
    $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

    // Check if payment period for this user exists for the current period
    Payments::SetUser($ArrayUser);
    Payments::CheckIfPaymentPeriodExists();

    //no period exists. so system did renew for user subscription, Here we will update invoice user total 
    //and send an email to user to pay. make user also untrusted untill he pays
    if (!$LogExists) {

        //get current payment log . if it is just added
        $ArrayPaymentPeriod = Payments::GetLog();
        $LogID = $ArrayPaymentPeriodNew["LogID"];

        //here update invoice and check balance
        //login admin to get session id to use in all subsequent api's calls
        $login = $admin_api->login();
        if (!$login[0]) {
            return array(false, self::$ArrayLanguage["'" . $admin_api->getCommand() . "'"]["'" . $login[1] . "'"]);
        }
        //admin session id
        $admin_session_id = $login[1];

        //get current user package service fee
        $service_fee = empty($ArrayUserGroup['PaymentSystemChargeAmount']) ? 0 : $ArrayUserGroup['PaymentSystemChargeAmount'];
        $current_balance = UserBalance::getUserBalance($ArrayUser['UserID']);
        $discount = 0;
        $new_balance = 0;
        $send_payment = true;

        if ($current_balance > $service_fee) {
            $discount = $service_fee;
            $new_balance = $current_balance - $discount;
            $send_payment = false;
        } elseif ($current_balance == $service_fee) {
            $discount = $service_fee;
            $new_balance = 0;
            $send_payment = false;
        } else {
            $discount = $current_balance;
            $new_balance = 0;
            $send_payment = true;
        }

        $check_bal_exists = UserBalance::checkBalanceExists($ArrayUser['UserID']);

        if ($check_bal_exists) {
            // if he has balance records,then update current balance
            $ArrayFieldnValues = array(
                "Balance" => $new_balance,
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => $ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFieldnValues, $ArrayCriterias);
        } else {
            // if user has no balance records. then create new record
            $ArrayFieldAndValues = array(
                "RelUserID" => $ArrayUser['UserID'],
                "Balance" => $new_balance,
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFieldAndValues);
        }

        $ReputationLevel = $send_payment ? 'Untrusted' : 'Trusted';
        //update user to be un-trusted untill he pays
        Users::UpdateUser(array('ReputationLevel' => $ReputationLevel), array('UserID' => $ArrayUser["UserID"]));

        $payment_api = new PaymentAPI();
        $payment_api->setAdminSessionId($admin_session_id);

        $PaymentStatus = $send_payment ? 'Unpaid' : 'Paid';

        //prepare payment data array
        $data_payment = array(
            "UserID" => $ArrayUser["UserID"],
            "LogID" => $LogID,
            "Discount" => $discount,
            "IncludeTax" => 'Exclude',
            "PaymentStatus" => $PaymentStatus,
            "SendReceipt" => "Yes",
        );
        
        //update invoice with service total if usergroup has service fees
        $payment_api->updateInvoice($data_payment);
    }


    // Check if user has exceeded monthly campaign sending limit
    $ArrayPaymentPeriod = Payments::GetLog();
    if (($ArrayPaymentPeriod['CampaignsSent'] >= $ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod']) && ($ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod'] > 0))
        continue;

    // Start benchmarking
    EmailQueue::StartBenchmarking();

    // Check if there's enough credits for the delivery
    $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);
    if ($CreditsCheckReturn[0] == false)
        continue;

    // Change campaigns status to 'sending'
    if ($ArrayCampaign['SendProcessStartedOn'] != '0000-00-00 00:00:00') {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
        );
    } else {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
            'SendProcessStartedOn' => date('Y-m-d H:i:s'),
        );
    }
    $ArrayFieldnValues['LastActivityDateTime'] = date('Y-m-d H:i:s');
    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));

    // If campaign is recursive campaign, record the process to recursive log and increase the recursive sending instance counter - Start
    if ($ArrayCampaign['ScheduleType'] == 'Recursive') {
        // Calculate current day of month, day of week, month, hour and minute - Start {
        $CurrentDayOfMonth = date('j');
        $CurrentDayOfWeek = date('w');
        $CurrentMonth = date('n');
        $CurrentHour = date('G');
        $CurrentMinute = date('i');

        // Strip leading zero from the minute - {
        if (substr($CurrentMinute, 0, 1) == '0') {
            $CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
        }
        // Strip leading zero from the minute - }
        // Round current minute to 0, 15, 30 or 45 - {
        if (($CurrentMinute >= 0) && ($CurrentMinute <= 14)) {
            $CurrentMinute = 0;
        } elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29)) {
            $CurrentMinute = 15;
        } elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44)) {
            $CurrentMinute = 30;
        } elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59)) {
            $CurrentMinute = 45;
        }
        // Round current minute to 0, 15, 30 or 45 - }
        // Calculate current day of month, day of week, month, hour and minute - End }
        Campaigns::LogToCampaignRecursiveLog($ArrayCampaign['CampaignID'], $CurrentDayOfWeek, $CurrentDayOfMonth, $CurrentMonth, $CurrentHour, $CurrentMinute);
        Campaigns::Update(array('ScheduleRecSentInstances' => $ArrayCampaign['ScheduleRecSentInstances'] + 1), array('CampaignID' => $ArrayCampaign['CampaignID']));
    }
    // If campaign is recursive campaign, record the process to recursive log and increase the recursive sending instance counter - End

    Campaigns::Update(array('LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));

    // Generate the queue of the campaign
    $TotalRecipients = EmailQueue::GenerateQueue($EachCampaignID, $ArrayCampaign['RelOwnerUserID']);

    // Update total recipients of the campaign
    Campaigns::Update(array('TotalRecipients' => $TotalRecipients, 'LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));

    // Check if activity exceeds user group threshold - Start
    if (($ArrayUser['GroupInformation']['ThresholdEmailSend'] > 0) && ($TotalRecipients > $ArrayUser['GroupInformation']['ThresholdEmailSend'])) {
        O_Email_Sender_ForAdmin::send(
                O_Email_Factory::userExceededEmailSendThreshold(
                        $ArrayUser, array('CampaignID' => $ArrayCampaign['CampaignID'], 'CampaignName' => $ArrayCampaign['CampaignName'], 'TotalRecipients' => $TotalRecipients)
                )
        );

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Threshold.CampaignRecipients', array($ArrayUser, $TotalRecipients, $EachCampaignID));
        // Plug-in hook - End
    }
    // Check if activity exceeds user group threshold - End
    // Prepare and send emails to the queue
    $TotalEmailsSent = EmailQueue::SendEmails($EachCampaignID, $ArrayUser, 10, $IsAorBSplitTestCampaign);

    // If send emails method returns false, set campaign status to paused - Start
    if ($TotalEmailsSent === false) {
        Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $EachCampaignID));
    }

    // Log the activity
    Core::AddToActivityLog($ArrayCampaign['RelOwnerUserID'], 'Campaign Sent', 'Campaign #' . $EachCampaignID . ' sent (' . number_format($TotalEmailsSent) . ' recipients)');

    // Log the number of sent email in activity statistics
    $ArrayActivities = array(
        'TotalSentEmail' => $TotalEmailsSent,
    );
    Statistics::UpdateListActivityStatistics(0, $ArrayCampaign['RelOwnerUserID'], $ArrayActivities);

    // Stop benchmarking
    EmailQueue::StopBenchmarking();

    // Calculate benchmarking
    $TotalEmailsPerSecond = EmailQueue::CalculateBenchmark($TotalEmailsSent);

    // Purge queue records and update campaign - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    if ($ArrayCampaign['CampaignStatus'] == 'Sending') {
        // Campaign is not paused. Proceed with normal procedure
        $TMPArrayReturn = EmailQueue::PurgeQueue($EachCampaignID);

        $ArrayFieldnValues = array(
            'CampaignStatus' => ($ArrayCampaign['ScheduleType'] == 'Recursive' ? 'Ready' : 'Sent'),
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );

        Payments::CampaignSent($ArrayCampaign['CampaignID']);
    } else {
        // Campaign has been paused by user.
        $ArrayFieldnValues = array(
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );
    }

    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
    // Purge queue records and update campaign - End

    $TotalProcessedCampaigns++;
}
// Generate the queue for each campaign - End
?>