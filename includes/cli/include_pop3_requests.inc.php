<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

// Check if POP3 access is enabled - Start
if (POP3_REQUESTS_STATUS != 'Enabled')
	{
	throw new Exception('POP3 acccess is disabled for subscriber request handling. You can change settings in Administration Area &gt; Settings &gt; Email Settings section.');
	}
// Check if POP3 access is enabled - End

// Connect to POP3 server - Start
POP3Engine::SetParameters(POP3_REQUESTS_HOST, POP3_REQUESTS_PORT, POP3_REQUESTS_USERNAME, POP3_REQUESTS_PASSWORD, (POP3_REQUESTS_SSL == 'Yes' ? true : false));
$Return = POP3Engine::Connect();
if (strtolower(gettype($Return)) == 'array')
	{
	throw new Exception('POP3 connection failed. Received error message: '.$Return[0]);
	}
// Connect to POP3 server - End

// Learn the total number of messages in the POP3 server - Start
$TotalEmails = POP3Engine::GetTotalMessages();
// Learn the total number of messages in the POP3 server - End

// Set variables - Start
$TotalProcessedRequests	= 0;
$TotalLooped			= 0;
// Set variables - End

// Loop each message and process it - Start
for ($EmailCounter = 1; $EmailCounter <= $TotalEmails; $EmailCounter++)
	{
	// Do not process more than a specific emails at a time - Start
	if ($EmailCounter >= 1000) break;
	// Do not process more than a specific emails at a time - End

	$TotalLooped++;

	$RawEmailContent = POP3Engine::GetRawEmail($EmailCounter);

	print('Processing email #'.$EmailCounter."<br>");

	// Process the email message - Start
	if ($RawEmailContent != "")
		{
		// Convert line endings to \n - Start
		$RawEmailContent = preg_replace("#(\r\n|\r)#s", "\n", $RawEmailContent);
			$ArrayRawEmailContent = explode("\n", $RawEmailContent);
		// Convert line endings to \n - End

		// Generate MIME parser object - Start
		$ObjectMIME = new mime_parser_class;
		// Generate MIME parser object - End

		// MIME parser settings - Start
		$ArrayMIMEParserParameters				= array(
														'Data'		=> $RawEmailContent,
														);
		// MIME parser settings - End

		// Decode message - Start
		if ($ObjectMIME->Decode($ArrayMIMEParserParameters, $Decoded) == false)
			{
			// MIME decoding failure ($ObjectMIME->error and $ObjectMIME->error_position)
			print("Mail couldn't be MIME decoded. Skipped<br>");
			}
		else
			{
			// MIME decoding success

			print("Mail is now MIME encoded.<br>");

			// Gather email parts - Start
			$ToEmailAddress		= $Decoded[0]['ExtractedAddresses']['to:'][0]['address'];
			$FromEmailAddress	= $Decoded[0]['ExtractedAddresses']['from:'][0]['address'];
			$MessageID			= $Decoded[0]['Headers']['message-id:'];
			$Subject			= $Decoded[0]['Headers']['subject:'];
			$PartCount			= count($Decoded[0]['Parts']);
			// Gather email parts - End

			// Decide which body part to analyze - Start
			if ($Decoded[0]['Body'] == '')
				{
				$BodyPartToAnalyze			= $Decoded[0]['Parts'][1]['Body'];
				$BodyPartToSearchXMessageID	= $Decoded[0]['Parts'][2]['Body'];
				}
			else
				{
				$BodyPartToAnalyze			= $Decoded[0]['Body'];
				$BodyPartToSearchXMessageID	= $Decoded[0]['Body'];
				}
			// Decide which body part to analyze - End

			// Check if this email is a request for a list - Start
			$ArrayLists = RequestByEmail::CheckIfAnyListAssigned($Subject, $ToEmailAddress);
			
			if ($ArrayLists == false)
				{
				print("Request not recognized<br>");
				continue;
				}
			else
				{
				foreach ($ArrayLists as $Index=>$ArrayEachList)
					{
					if (strtolower($ArrayEachList['ReqByEmailSubscriptionCommand']) == strtolower($Subject))
						{
						// subscribe

						// Prepare and send the request to the API - Start
							$URL					= APP_URL.'api.php';
							$ArrayPostParameters	= array(
															'command=Subscriber.Subscribe',
															'responseformat=XML',
															'listid='.$ArrayEachList['ListID'],
															'ipaddress=0.0.0.0 - By Email Request',
															'emailaddress='.$FromEmailAddress,
															);

						$ArrayReturn = Core::DataPostToRemoteURL($URL, $ArrayPostParameters, 'POST', false, '', '', 60, false);
						// Prepare and send the request to the API - End

						// Delete the bounce delivery report from the POP3 server - Start
						POP3Engine::DeleteEmail($EmailCounter, true);
						// Delete the bounce delivery report from the POP3 server - End
						
						$TotalProcessedRequests++;
						}
					elseif (strtolower($ArrayEachList['ReqByEmailUnsubscriptionCommand']) == strtolower($Subject))
						{
						// unsubscribe

						// Prepare and send the request to the API - Start
							$URL					= APP_URL.'api.php';
							$ArrayPostParameters	= array(
															'command=Subscriber.Unsubscribe',
															'responseformat=XML',
															'emailaddress='.$FromEmailAddress,
															'subscriberid=',
															'listid='.$ArrayEachList['ListID'],
															'ipaddress=0.0.0.0 - By Email Request',
															);

						$ArrayReturn = Core::DataPostToRemoteURL($URL, $ArrayPostParameters, 'POST', false, '', '', 60, false);
						// Prepare and send the request to the API - End

						// Delete the bounce delivery report from the POP3 server - Start
						POP3Engine::DeleteEmail($EmailCounter, true);
						// Delete the bounce delivery report from the POP3 server - End

						$TotalProcessedRequests++;
						}
					}
				}
			// Check if this email is a request for a list - End
			}
		// Decode message - End
		}
	// Process the email message - End

	print('<hr />');
	}
// Loop each message and process it - End

// Disconnect from POP3 Server - Start
POP3Engine::Disconnect();
// Disconnect from POP3 Server - End


?>