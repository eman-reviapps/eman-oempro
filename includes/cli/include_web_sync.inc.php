<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

// Select subscriber lists which have enabled synchronization settings - Start
$ArraySubscriberLists = Lists::RetrieveLists(array('*'), array('SyncStatus' => 'Enabled'), array('ListID' => 'ASC'));
$TotalSubscriberListsToLoop = count($ArraySubscriberLists);
// Select subscriber lists which have enabled synchronization settings - End

// Loop every subscriber list and import data into the list - Start
$TotalLoopedSubscriberLists = 0;
$TotalImportedSubscribers	= 0;
$TotalUpdatedSubscribers	= 0;
$TotalProcessDuration		= 0;

foreach ($ArraySubscriberLists as $ArrayEachList)
	{
	// Check if sync is being processed for this list, do not continue - Start
	if (Lists::CheckIfAnyActiveSyncLogExists($ArrayEachList['ListID']) > 0)
		{
		print('Sync is active for list #'.$ArrayEachList['ListID'].'<br>');
		continue;
		}
	// Check if sync is being processed for this list, do not continue - End

	// Check if last execution time is before the set period - Start
	$Now			= time();
	$LastSyncTime	= strtotime($ArrayEachList['SyncLastDateTime']);
	$TimeDifference	= $Now - $LastSyncTime;

	if ($ArrayEachList['SyncPeriod'] == 'Every Hour')
		{
		$Threshold = 3600;
		}
	elseif ($ArrayEachList['SyncPeriod'] == 'Every Day')
		{
		$Threshold = 86400;
		}
	elseif ($ArrayEachList['SyncPeriod'] == 'Every Week')
		{
		$Threshold = 604800;
		}
	elseif ($ArrayEachList['SyncPeriod'] == 'Every Month')
		{
		$Threshold = 2419200;
		}

	if ($TimeDifference < $Threshold)
		{
		continue;
		}
	// Check if last execution time is before the set period - End

	// Retrieve owner user information - Start
	$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayEachList['RelOwnerUserID']));
	// Retrieve owner user information - End

	// Log the process - Start
	$LogID = Lists::LogListSync($ArrayEachList['ListID'], 'Processing', '', 0, 0);
	// Log the process - End

	// Benchmarking - Start
	Core::PerformanceBenchmark('Start', true);
	// Benchmarking - End

	// Connect to target MySQL server and database - Start
	$ObjectMySQLTest	= new MySqlDatabaseInterface();
	$ConnectionResult	= $ObjectMySQLTest->Connect($ArrayEachList['SyncMySQLHost'].':'.$ArrayEachList['SyncMySQLPort'], $ArrayEachList['SyncMySQLUsername'], $ArrayEachList['SyncMySQLPassword'], $ArrayEachList['SyncMySQLDBName'], '');
	if ($ConnectionResult == false)
		{
		// MySQL error occured
		// Benchmarking - Start
		$TotalProcessDuration += Core::PerformanceBenchmark('End', false);
		// Benchmarking - End

		Lists::LogListSync($ArrayEachList['ListID'], 'Failed', 'List ID: '.$ArrayEachList['ListID'].' - '.$ObjectMySQLTest->ReturnError(), $TotalProcessDuration, $LogID);
		print('List ID: '.$ArrayEachList['ListID'].' - '.$ObjectMySQLTest->ReturnError());
		continue;
		}

	$ObjectMySQLTest->ExecuteQuery('SET collation_connection = utf8_unicode_ci');
	$ObjectMySQLTest->ExecuteQuery('SET NAMES utf8');
	$ResultSet = $ObjectMySQLTest->ExecuteQuery($ArrayEachList['SyncMySQLQuery']);

	if ($ResultSet == false)
		{
		// MySQL error occured
		// Benchmarking - Start
		$TotalProcessDuration += Core::PerformanceBenchmark('End', false);
		// Benchmarking - End

		Lists::LogListSync($ArrayEachList['ListID'], 'Failed', 'List ID: '.$ArrayEachList['ListID'].' - '.$ObjectMySQLTest->ReturnError(), $TotalProcessDuration, $LogID);
		print('List ID: '.$ArrayEachList['ListID'].' - '.$ObjectMySQLTest->ReturnError());
		continue;
		}
	// Connect to target MySQL server and database - End

	// Prepare mapped fields - Start
	$ArrayMappedFields = Lists::ParseSyncMappedFields($ArrayEachList['SyncFieldMapping']);
	// Prepare mapped fields - End

	// Check if email address field is mapped - Start
	$TotalProcessedSubscribers	= 0;
	$TotalDuplicates			= 0;
	$TotalFailed				= 0;
	$TotalData					= mysql_num_rows($ResultSet);

	if (($ArrayMappedFields['EmailAddress'] == '') || (isset($ArrayMappedFields['EmailAddress']) == false))
		{
		// Benchmarking - Start
		$TotalProcessDuration += Core::PerformanceBenchmark('End', false);
		// Benchmarking - End

		Lists::LogListSync($ArrayEachList['ListID'], 'Failed', 'List ID: '.$ArrayEachList['ListID'].' - Email address is not mapped.', $TotalProcessDuration, $LogID);
		print('List ID: '.$ArrayEachList['ListID'].' - Email address is not mapped.<br>');
		continue;
		}
	// Check if email address field is mapped - End

	// Log the process - Start
	Lists::LogListSync($ArrayEachList['ListID'], 'Processing', '', 0, $LogID);
	// Log the process - End

	// Loop the result set - Start
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Generate subscriber fields to be imported (except email address) - Start
		$ArrayOtherFields 	= array();
		$GlobalCustomFields = array();
		
		foreach ($ArrayMappedFields as $OemproField=>$ThirdPartyField)
			{
			if (substr($OemproField, 0, 11) == 'CustomField')
				{
				$ArrayOtherFields[$OemproField] = $EachRow[$ThirdPartyField];
				}
			}
		$EmailAddress = $EachRow[$ArrayMappedFields['EmailAddress']];
		// Generate subscriber fields to be imported (except email address) - End

		$SubscriptionResult = Subscribers::AddSubscriber_Enhanced(array(
			'CheckSubscriberLimit'	=> false,
			'UserInformation'		=> $ArrayUser,
			'ListInformation'		=> $ArrayEachList,
			'EmailAddress'			=> $EmailAddress,
			'IPAddress'				=> '0.0.0.0 - Synchronized',
			'OtherFields'			=> $ArrayOtherFields,
			'UpdateIfDuplicate'		=> true,
			'UpdateIfUnsubscribed'	=> true,
			'ApplyBehaviors'		=> false,
			'SendConfirmationEmail'	=> false,
			'UpdateStatistics'		=> false,
			'TriggerWebServices'	=> false,
			'TriggerAutoResponders'	=> false
			));

		if ($SubscriptionResult[0] == false)
			{
			switch ($SubscriptionResult[1])
				{
				case 3:
					// Duplicate email address
					$TotalDuplicates++;
					break;
				default:
					// Invalid
					$TotalFailed++;
					break;
				}
			}
		else
			{
			// Import Successful
			$TotalProcessedSubscribers++;
			}
		}
	// Loop the result set - End

	// Close the connection to target MySQL server - Start
	$ObjectMySQLTest->CloseConnection();
	// Close the connection to target MySQL server - End

	// Benchmarking - Start
	$TotalProcessDuration += Core::PerformanceBenchmark('End', false);
	// Benchmarking - End

	$TotalLoopedSubscriberLists++;

	// Log the process - Start
	Lists::LogListSync($ArrayEachList['ListID'], 'Success', sprintf('%s subscribers processed, %s seconds', number_format($TotalProcessedSubscribers), number_format($TotalProcessDuration, 5)), $TotalProcessDuration, $LogID);
	// Log the process - End
	
	// Update list last sync date/time - Start
	Lists::Update(array('SyncLastDateTime' => date('Y-m-d H:i:s')), array('ListID' => $ArrayEachList['ListID']));
	// Update list last sync date/time - End
	
	// If reporting is enabled, send a notification email to the list owner - Start
	if ($ArrayEachList['SyncSendReportEmail'] == 'Yes')
		{
		if ($ArrayUser != false)
			{
			O_Email_Sender_ForAdmin::send(
				O_Email_Factory::listSyncNotification($ArrayUser, $ArrayEachList, number_format($TotalProcessedSubscribers))
			);
			}
		}
	// If reporting is enabled, send a notification email to the list owner - End
	}
// Loop every subscriber list and import data into the list - End

?>