<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

// Read from STDIN - Start
$RawEmailContent = "";

$FileHandler = fopen("php://stdin", "r");

if ($FileHandler > 0)
{ 
	while(!feof($FileHandler))
	{ 
		$RawEmailContent .= fgets($FileHandler, 120);
	}
}
fclose($FileHandler);
// Read from STDIN - End

// STDIN: Data received, processing it - Start
if (trim($RawEmailContent) != "")
	{
	// Convert line endings to \n - Start
	$RawEmailContent = preg_replace("#(\r\n|\r)#s", "\n", $RawEmailContent);
		$ArrayRawEmailContent = explode("\n", $RawEmailContent);
	// Convert line endings to \n - End

	// Generate MIME parser object - Start
	$ObjectMIME = new mime_parser_class;
	// Generate MIME parser object - End

	// MIME parser settings - Start
	$ArrayMIMEParserParameters				= array(
													'Data'		=> $RawEmailContent,
													);
	// MIME parser settings - End

	// Decode message - Start
	if ($ObjectMIME->Decode($ArrayMIMEParserParameters, $Decoded) == false)
		{
		// MIME decoding failure ($ObjectMIME->error and $ObjectMIME->error_position)
		}
	else
		{
		// MIME decoding success

		// Gather email parts - Start
		$ToEmailAddress		= $Decoded[0]['ExtractedAddresses']['to:'][0]['address'];
		$FromEmailAddress	= $Decoded[0]['ExtractedAddresses']['from:'][0]['address'];
		$MessageID			= $Decoded[0]['Headers']['message-id:'];
		$Subject			= $Decoded[0]['Headers']['subject:'];
		$PartCount			= count($Decoded[0]['Parts']);
		// Gather email parts - End

		// Decide which body part to analyze - Start
		if ($Decoded[0]['Body'] == '')
			{
			$BodyPartToAnalyze			= $Decoded[0]['Parts'][1]['Body'];
			$BodyPartToSearchXMessageID	= $Decoded[0]['Parts'][2]['Body'];
			}
		else
			{
			$BodyPartToAnalyze			= $Decoded[0]['Body'];
			$BodyPartToSearchXMessageID	= $Decoded[0]['Body'];
			}
		// Decide which body part to analyze - End

		// Check if this email is a request for a list - Start
		$ArrayLists = RequestByEmail::CheckIfAnyListAssigned($Subject, $ToEmailAddress);
		
		if ($ArrayLists == false)
			{
			throw new Exception('Request not recognized');
			}
		else
			{
			foreach ($ArrayLists as $Index=>$ArrayEachList)
				{
				if (strtolower($ArrayEachList['ReqByEmailSubscriptionCommand']) == strtolower($Subject))
					{
					// subscribe

					// Prepare and send the request to the API - Start
						$URL					= APP_URL.'api.php';
						$ArrayPostParameters	= array(
														'command=Subscriber.Subscribe',
														'responseformat=XML',
														'listid='.$ArrayEachList['ListID'],
														'ipaddress=0.0.0.0 - By Email Request',
														'emailaddress='.$FromEmailAddress,
														);

					$ArrayReturn = Core::DataPostToRemoteURL($URL, $ArrayPostParameters, 'POST', false, '', '', 60, false);
					// Prepare and send the request to the API - End
					}
				elseif (strtolower($ArrayEachList['ReqByEmailUnsubscriptionCommand']) == strtolower($Subject))
					{
					// unsubscribe

					// Prepare and send the request to the API - Start
						$URL					= APP_URL.'api.php';
						$ArrayPostParameters	= array(
														'command=Subscriber.Unsubscribe',
														'responseformat=XML',
														'emailaddress='.$FromEmailAddress,
														'subscriberid=',
														'listid='.$ArrayEachList['ListID'],
														'ipaddress=0.0.0.0 - By Email Request',
														);

					$ArrayReturn = Core::DataPostToRemoteURL($URL, $ArrayPostParameters, 'POST', false, '', '', 60, false);
					// Prepare and send the request to the API - End
					}
				}
			}
		// Check if this email is a request for a list - End
		}
	// Decode message - End
	
	}
// STDIN: Data received, processing it - End


?>