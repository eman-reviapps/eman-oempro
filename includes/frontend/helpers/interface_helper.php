<?php

/**
 * Displays the menu
 *
 * @package default
 * @author Cem Hurturk
 */
function InterfaceDisplayMenu($Section, $CurrentMenuItem, $NormalTemplate = '<li><a href="_Replace:Link_"><strong>_Replace:Name_</strong></a></li>', $SelectedTemplate = '<li class="selected"><a href="_Replace:Link_"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><strong>_Replace:Name_</strong></a></li>') {
    if ($Section == 'AdminTopMain') {
        $ArrayMenuItems = array(
            'Overview' => array(
                'Link' => InterfaceAppURL(true) . '/admin/overview/',
                'Name' => 'Overview',
            ),
            'Users' => array(
                'Link' => InterfaceAppURL(true) . '/admin/users/browse/',
                'Name' => 'Users',
            ),
            'PaymentReports' => array(
                'Link' => InterfaceAppURL(true) . '/admin/paymentreports/',
                'Name' => 'Payment Reports',
            )
        );
    } else if ($Section == 'AdminTopRightMain') {
        $ArrayMenuItems = array(
            'Settings' => array(
                'Link' => InterfaceAppURL(true) . '/admin/about/',
                'Name' => 'Settings',
            ),
            'Logut' => array(
                'Link' => InterfaceAppURL(true) . '/admin/logout/',
                'Name' => 'Logout',
            )
        );
    }

    $String = '';
    foreach ($ArrayMenuItems as $Index => $ArrayValues) {
        if ($Index == $CurrentMenuItem) {
            $String .= str_replace(array('_Replace:Link_', '_Replace:Name_'), array($ArrayValues['Link'], $ArrayValues['Name']), $SelectedTemplate) . "\n";
        } else {
            $String .= str_replace(array('_Replace:Link_', '_Replace:Name_'), array($ArrayValues['Link'], $ArrayValues['Name']), $NormalTemplate) . "\n";
        }
    }

    return $String;
}

/**
 * Returns the array of default interface variables
 *
 * @return void
 * @author Cem Hurturk
 */
function InterfaceDefaultValues() {
    Core::LoadObject('install');
    Install::GetLicenseInformation();

    $ArrayValues = array(
        'LicenseRegistrantName' => Install::$ArrayLicenseProperties['RegistrantName']['value'],
        'LicenseMaxUsers' => (Install::$ArrayLicenseProperties['MaxUsers']['value'] == '-1' ? ApplicationHeader::$ArrayLanguageStrings['Screen']['0164'] : Install::$ArrayLicenseProperties['MaxUsers']['value']),
        'LicenseAuthorizedDomain' => Install::$ArrayLicenseProperties['Domain']['value'],
        'LicenseKey' => Install::$ArrayLicenseProperties['LicenseKey']['value'],
            // 'LicenseLatestAvailableVersionLink'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['9160'], self::GetLatestVersionUpdateLink(true, 'HIDE:IfThereIsNoUpdate')),
    );

    return $ArrayValues;
}

function InterfaceNumber($number, $return = false) {
    $number = number_format(
            $number, ApplicationHeader::$ArrayLanguageStrings['Config']['NumberFormat'][0], ApplicationHeader::$ArrayLanguageStrings['Config']['NumberFormat'][1], ApplicationHeader::$ArrayLanguageStrings['Config']['NumberFormat'][2]
    );

    if ($return)
        return $number;

    echo $number;
}

function InterfacePercentage($percent, $return = false) {
    $percent = number_format(
            $percent, ApplicationHeader::$ArrayLanguageStrings['Config']['PercentFormat'][0], ApplicationHeader::$ArrayLanguageStrings['Config']['PercentFormat'][1], ApplicationHeader::$ArrayLanguageStrings['Config']['PercentFormat'][2]
    );

    $symbolLocation = ApplicationHeader::$ArrayLanguageStrings['Config']['PercentFormat'][3];

    $percent = ($symbolLocation == 0 ? '%' : '') . $percent . ($symbolLocation == 1 ? '%' : '');

    if ($return)
        return $percent;

    echo $percent;
}

/**
 * Displays the language
 *
 * @param string $Return 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceLanguage($LanguageGroup, $LanguageCode, $Return = false, $SubLanguageCode = '', $StrToUpper = false, $UCWords = false, $ArrayReplaceList = array()) {
    $LanguageString = ApplicationHeader::$ArrayLanguageStrings[$LanguageGroup][$LanguageCode];

    if ($SubLanguageCode !== '') {
        $LanguageString = $LanguageString[$SubLanguageCode];
    }

    if (count($ArrayReplaceList) > 0) {
        array_unshift($ArrayReplaceList, $LanguageString);
        $LanguageString = call_user_func_array('sprintf', $ArrayReplaceList);
    }

    if ($StrToUpper == true) {
        if (function_exists('mb_convert_case')) {
            $LanguageString = mb_convert_case($LanguageString, MB_CASE_UPPER, 'UTF-8');
        } else {
            $LanguageString = strtoupper($LanguageString);
        }
    }

    if ($UCWords == true) {
        if (function_exists('mb_convert_case')) {
            $LanguageString = mb_convert_case($LanguageString, MB_CASE_TITLE, 'UTF-8');
        } else {
            $LanguageString = ucwords($LanguageString);
        }
    }

    if ($Return == false) {
        print($LanguageString);
    } else {
        return ($LanguageString);
    }
}

/**
 * This function overrides CodeIgniter's default form validation error messages
 *
 * @return void
 * @author Cem Hurturk
 */
function InterfaceOverrideFormMessages() {
    $ObjectCodeIgniter = & get_instance();

    $ObjectCodeIgniter->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0348']);
    $ObjectCodeIgniter->form_validation->set_message('isset', ApplicationHeader::$ArrayLanguageStrings['Screen']['0349']);
    $ObjectCodeIgniter->form_validation->set_message('valid_email', ApplicationHeader::$ArrayLanguageStrings['Screen']['0350']);
    $ObjectCodeIgniter->form_validation->set_message('valid_emails', ApplicationHeader::$ArrayLanguageStrings['Screen']['0351']);
    $ObjectCodeIgniter->form_validation->set_message('valid_url', ApplicationHeader::$ArrayLanguageStrings['Screen']['0352']);
    $ObjectCodeIgniter->form_validation->set_message('valid_ip', ApplicationHeader::$ArrayLanguageStrings['Screen']['0353']);
    $ObjectCodeIgniter->form_validation->set_message('min_length', ApplicationHeader::$ArrayLanguageStrings['Screen']['0354']);
    $ObjectCodeIgniter->form_validation->set_message('max_length', ApplicationHeader::$ArrayLanguageStrings['Screen']['0355']);
    $ObjectCodeIgniter->form_validation->set_message('exact_length', ApplicationHeader::$ArrayLanguageStrings['Screen']['0356']);
    $ObjectCodeIgniter->form_validation->set_message('alpha', ApplicationHeader::$ArrayLanguageStrings['Screen']['0357']);
    $ObjectCodeIgniter->form_validation->set_message('alpha_numeric', ApplicationHeader::$ArrayLanguageStrings['Screen']['0358']);
    $ObjectCodeIgniter->form_validation->set_message('alpha_dash', ApplicationHeader::$ArrayLanguageStrings['Screen']['0359']);
    $ObjectCodeIgniter->form_validation->set_message('numeric', ApplicationHeader::$ArrayLanguageStrings['Screen']['0360']);
    $ObjectCodeIgniter->form_validation->set_message('is_numeric', ApplicationHeader::$ArrayLanguageStrings['Screen']['0361']);
    $ObjectCodeIgniter->form_validation->set_message('integer', ApplicationHeader::$ArrayLanguageStrings['Screen']['0362']);
    $ObjectCodeIgniter->form_validation->set_message('matches', ApplicationHeader::$ArrayLanguageStrings['Screen']['0363']);
    $ObjectCodeIgniter->form_validation->set_message('is_natural', ApplicationHeader::$ArrayLanguageStrings['Screen']['0364']);
    $ObjectCodeIgniter->form_validation->set_message('is_natural_no_zero', ApplicationHeader::$ArrayLanguageStrings['Screen']['0365']);
}

/**
 * Displays the App URL
 *
 * @param string $Return 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceAppURL($Return = false) {
    $url = APP_URL . APP_DIRNAME . (HTACCESS_ENABLED == false ? '/index.php?' : '');
    if ($Return == false) {
        print $url;
    } else {
        return $url;
    }
}

/**
 * Displays the Installation URL
 *
 * @param string $Return 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceInstallationURL($Return = false) {
    if ($Return == false) {
        print(APP_URL);
    } else {
        return APP_URL;
    }
}

/**
 * Displays the template (theme) URL
 *
 * @param string $Return 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceTemplateURL($Return = false) {
    if ($Return == false) {
        print(TEMPLATE_URL);
    } else {
        return TEMPLATE_URL;
    }
}

/**
 * Cuts the string with the provided suffix
 *
 * @param string $MaxChar 
 * @param string $Suffix 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceTextCut($String, $MaxChar = 30, $Suffix = '...', $Return = false) {
    if (strlen($String) > $MaxChar) {
        $String = substr($String, 0, $MaxChar) . $Suffix;
        if ($Return == false) {
            print($String);
        } else {
            return $String;
        }
    } else {
        if ($Return == false) {
            print($String);
        } else {
            return $String;
        }
    }
}

/**
 * Checks for the latest version and returns the response
 *
 * @param string $LanguagePreset 
 * @return void
 * @author Cem Hurturk
 */
function InterfaceCheckTheLatestVersion($Prefix = '', $Suffix = '', $LanguagePreset = '', $Return = false) {
    $Return = Core::CheckTheLatestVersion();

    if ($Return == false) {
        if ($Return == false) {
            print('');
        } else {
            return false;
        }
    } else {
        if ($Return == false) {
            printf($Prefix . $LanguagePreset . $Suffix, $Return);
            return;
        } else {
            return(sprintf($Prefix . $LanguagePreset . $Suffix, $Return));
        }
    }
}

/**
 * Prints out the administrator footer
 *
 * @return void
 * @author Cem Hurturk
 */
function InterfaceAdminFooter($Return = false, $FooterTemplate = '') {
    $ArrayLicenseProperties = Core::GetLicenseProperties(true, true);
    $ArraySystemCheckResults = Core::SystemCheck();
    $LatestVersion = Core::CheckTheLatestVersion();

    if (isset($ArrayLicenseProperties['LicenseExpireDate']) == true) {
        $LicenseExpiresAt = $ArrayLicenseProperties['LicenseExpireDate']['value'];
        $DaysRemaining = strtotime($LicenseExpiresAt) - time();

        $DaysRemaining = ($DaysRemaining < 0 ? '-1' : ($DaysRemaining < 86400 ? 0 : ceil($DaysRemaining / 86400)));
    }

    if (preg_match('/^TRIAL/i', $ArrayLicenseProperties['LicenseKey']['value']) == 0) {
        $FooterTemplate = '
		<div class="container">
			<div class="span-23 last">
				<div class="custom-column-container cols-2 clearfix" style="margin-top:18px;">
					<div class="col">
						<p>
							&copy;1999-_Replace:CopyYearNow_, Octeth. All rights reserved.<br />
							<span class="dimmed">Registered for _Replace:LicenseOwner_<br />
							_Replace:LicenseDomain_, _Replace:LicenseUsers_ (<a href="' . InterfaceAppURL(true) . '/admin/license/">' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1838'] . '</a>)<br />
							License Key: _Replace:OemproLicense_ (<a href="' . InterfaceAppURL(true) . '/admin/license/">' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1839'] . '</a>)<br />
							_Replace:OemproVersion_<br />
							_Replace:SystemCheckResults_</span>
						</p>
					</div>
					<div class="col">
						<p class="dimmed">
							' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1841'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1842'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1843'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0090'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0091'] . '
						</p>
					</div>
				</div>
			</div>
		</div>';
    } else {
        $FooterTemplate = '
			<style>
			.oempro-trial-c2a { padding:10px !important;margin:0;position:fixed; right:20px; top:50px; border:2px solid #2F7BC7; background-color: #fff; padding:20px; border-radius:5px; transform:rotate(20deg); -ms-transform:rotate(20deg); -webkit-transform:rotate(20deg);box-shadow: 0px 0px 10px #616265; opacity:0.2; }
			.oempro-trial-c2a:hover { opacity:1; }
			</style>
			<div class="oempro-trial-c2a">
				<p style="margin:0;padding:0;text-align:center; color:#000;font-weight:bold;">5-DAY TRIAL LICENSE</p>
				<p style="margin:0;padding:0;text-align:center; color:#000;font-weight:normal;">' . ($DaysRemaining == 0 ? 'Expires today' : ($DaysRemaining == -1 ? 'Expired' : 'Expires in ' . ($DaysRemaining > 1 ? $DaysRemaining . ' days' : $DaysRemaining . ' day'))) . '</p>
				<p style="margin:5px 0 0 0;padding:0;text-align: center;color:#000;"><a style="color:green;font-weight:bold;" href="http://octeth.com/oempro/pricing/" target="_blank">&gt; Purchase Oempro Now &lt;</a></p>
			</div>

			<div class="container">
				<div class="span-23 last">
					<div class="custom-column-container cols-2 clearfix" style="margin-top:18px;">
						<div class="col">
							<p>
								&copy;1999-_Replace:CopyYearNow_, Octeth. All rights reserved.<br />
								<strong>TIME LIMITED TRIAL LICENSE</strong><br />
								<span class="dimmed">_Replace:TrialExpiresAt_</span><br>
								<span class="dimmed">Registered for _Replace:LicenseOwner_<br />
								<a href="http://octeth.com/oempro/purchase/" target="_blank">Purchase Oempro Now</a><br />
								_Replace:OemproVersion_<br />
								_Replace:SystemCheckResults_</span>
							</p>
						</div>
						<div class="col">
							<p class="dimmed">
								' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1841'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1842'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1843'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0090'] . ' | ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0091'] . '
							</p>
							<p style="margin-top:-10px;" class="dimmed">Is it too technical to host your own software?<br>Try our hosted solution; <a href="http://sendloop.com/oempro/?utm_source=oempro_in_app" target="_blank">Sendloop</a> for free</p>
						</div>
					</div>
				</div>
			</div>';
    }


    $ArrayReplaceList = array(
        '_Replace:CopyYearNow_' => date('Y'),
        '_Replace:LicenseOwner_' => $ArrayLicenseProperties['RegistrantName']['value'],
        '_Replace:LicenseKey_' => $ArrayLicenseProperties['LicenseKey']['value'],
        '_Replace:LicenseEdition_' => $ArrayLicenseProperties['Edition']['value'] . ' ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0085'],
        '_Replace:LicenseDomain_' => $ArrayLicenseProperties['Domain']['value'],
        '_Replace:LicenseUsers_' => $ArrayLicenseProperties['MaxUsers']['value'] . ' ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0086'],
        '_Replace:OemproVersion_' => PRODUCT_NAME . ' v' . PRODUCT_VERSION . InterfaceCheckTheLatestVersion(', ', '', ApplicationHeader::$ArrayLanguageStrings['Screen']['0084'], true),
        '_Replace:SystemCheckResults_' => ($ArraySystemCheckResults[0] == false ? ApplicationHeader::$ArrayLanguageStrings['Screen']['0087'] : ''),
        '_Replace:OemproLicense_' => $ArrayLicenseProperties['LicenseKey']['value'],
        '_Replace:TrialExpiresAt_' => 'License ' . ($DaysRemaining == 0 ? 'expires today' : ($DaysRemaining == -1 ? 'expired' : 'expires in ' . ($DaysRemaining > 1 ? $DaysRemaining . ' days' : $DaysRemaining . ' day'))),
    );

    $FooterTemplate = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $FooterTemplate);

    if ($Return == false) {
        print $FooterTemplate;
        return;
    } else {
        return $FooterTemplate;
    }
}

/**
 * Prints out the user footer
 *
 * @return void
 * @author Cem Hurturk
 */
function InterfaceUserFooter($Return = false, $FooterTemplate = '') {
    $ArrayLicenseProperties = Core::GetLicenseProperties(true, true);
    $ArraySystemCheckResults = Core::SystemCheck();

    $ArrayReplaceList = array(
        '_Replace:CopyYearNow_' => date('Y'),
        '_Replace:LicenseOwner_' => $ArrayLicenseProperties['RegistrantName']['value'],
        '_Replace:LicenseKey_' => $ArrayLicenseProperties['LicenseKey']['value'],
        '_Replace:LicenseEdition_' => $ArrayLicenseProperties['Edition']['value'] . ' ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0085'],
        '_Replace:LicenseDomain_' => $ArrayLicenseProperties['Domain']['value'],
        '_Replace:LicenseUsers_' => $ArrayLicenseProperties['MaxUsers']['value'] . ' ' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0086'],
        '_Replace:OemproVersion_' => PRODUCT_NAME . ' v' . PRODUCT_VERSION . InterfaceCheckTheLatestVersion(', ', '', ApplicationHeader::$ArrayLanguageStrings['Screen']['0084'], true),
        '_Replace:SystemCheckResults_' => ($ArraySystemCheckResults[0] == false ? ApplicationHeader::$ArrayLanguageStrings['Screen']['0087'] : ''),
    );

    $FooterTemplate = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $FooterTemplate);

    if ($Return == false) {
        print $FooterTemplate;
        return;
    } else {
        return $FooterTemplate;
    }
}

/**
 * Runs plugin menu functions for hook
 *
 * @return void
 * @author Mert Hurturk
 * */
function InterfacePluginMenuHook($Location, $SelectedMenuItem, $Template, $SelectedTemplate, $Parameters = array()) {
    // Plug-in hook - Start
    $ArrayMenuItems = Plugins::HookListener('Action', 'System.Menu.Add', $Parameters);
    // Plug-in hook - End
    // Parse returned menu items  - Start
    if (count($ArrayMenuItems) > 0) {
        foreach ($ArrayMenuItems as $Key => $ArrayEachMenuItem) {
            $MenuItem = GetNestedArrayMenuItem($ArrayEachMenuItem);
            if (is_array($MenuItem) && count($MenuItem) > 0 && !isset($MenuItem['MenuLocation'])) {
                foreach ($MenuItem as $EachMenuItem) {
                    if ($Location == $EachMenuItem['MenuLocation']) {
                        print str_replace(array('_ID_', '_LINK_', '_TITLE_'), array($EachMenuItem['MenuID'], $EachMenuItem['MenuLink'], $EachMenuItem['MenuTitle']), ($EachMenuItem['MenuID'] == $SelectedMenuItem ? $SelectedTemplate : $Template));
                    }
                }
            } else {
                if ($Location == $MenuItem['MenuLocation']) {
                    print str_replace(array('_ID_', '_LINK_', '_TITLE_'), array($MenuItem['MenuID'], $MenuItem['MenuLink'], $MenuItem['MenuTitle']), ($MenuItem['MenuID'] == $SelectedMenuItem ? $SelectedTemplate : $Template));
                }
            }
        }
    }
    // Parse returned menu items  - End
}

function InterfacePluginIntegrationMenuHook($Template, $Parameters = array()) {
    $Location = 'User.Settings.Integrations';

    // Plug-in hook - Start
    $ArrayMenuItems = Plugins::HookListener('Action', 'System.Menu.Add', $Parameters);
    // Plug-in hook - End
    // Parse returned menu items  - Start
    if (count($ArrayMenuItems) > 0) {
        foreach ($ArrayMenuItems as $Key => $ArrayEachMenuItem) {
            $MenuItem = GetNestedArrayMenuItem($ArrayEachMenuItem);
            if (is_array($MenuItem) && count($MenuItem) > 0 && !isset($MenuItem['MenuLocation'])) {
                foreach ($MenuItem as $EachMenuItem) {
                    if ($Location == $EachMenuItem['MenuLocation']) {
                        print str_replace(array('_ICON_', '_LINK_', '_TITLE_', '_DESCRIPTION_'), array($EachMenuItem['MenuIcon'], $EachMenuItem['MenuLink'], $EachMenuItem['MenuTitle'], $EachMenuItem['MenuDescription']), $Template);
                    }
                }
            } else {
                if ($Location == $MenuItem['MenuLocation']) {
                    print str_replace(array('_ICON_', '_LINK_', '_TITLE_', '_DESCRIPTION_'), array($EachMenuItem['MenuIcon'], $EachMenuItem['MenuLink'], $EachMenuItem['MenuTitle'], $EachMenuItem['MenuDescription']), $Template);
                }
            }
        }
    }
    // Parse returned menu items  - End
}

function GetNestedArrayMenuItem($Array) {
    if (count($Array) > 0 && isset($Array['MenuLocation']))
        return $Array;
    if (count($Array) == 1)
        return GetNestedArrayMenuItem($Array[0]);
    return $Array;
}

/**
 * Runs plugin form items functions for hook
 *
 * @return void
 * @author Cem Hurturk
 * */
function InterfacePluginFormItemsHook($Location, $Parameters) {
    // Plug-in hook - Start
    $ArrayReturn = Plugins::HookListener('Action', $Location, $Parameters);
    // Plug-in hook - End

    if (count($ArrayReturn) > 0) {
        foreach ($ArrayReturn as $Index => $Each) {
            print is_array($Each) ? $Each[0] : $Each;
        }
    }
    return;
}

/**
 * Checks if administrator is logged in. 
 * This is needed in user are for displaying administrator control bar.
 *
 * @return void
 * @author Mert Hurturk
 * */
function InterfaceAdministratorControlBar() {
    Core::LoadObject('admin_auth');
    Core::LoadObject('users');

    if (AdminAuth::IsLoggedIn(false, false) == true) {
        $ArrayUsers = Users::RetrieveUsers(array('*'), array(), array('FirstName' => 'ASC', 'LastName' => 'ASC'), 0, 0, false, false);
        return array(true, $ArrayUsers);
    }
    return array(false);
}

/**
 * Checks if required privilege is available for user
 *
 * @param string $Privilege If privilege seperated by %, this means OR; if seperated by &, this means AND
 * @param string $ArrayUserInformation User information should also include group information
 * @return void
 * @author Mert Hurturk
 */
function InterfacePrivilegeCheck($Privilege, $ArrayUserInformation) {
    if (!isset($ArrayUserInformation['GroupInformation']) || !isset($ArrayUserInformation['GroupInformation']['Permissions'])) {
        return false;
    }

    $UserPrivileges = $ArrayUserInformation['GroupInformation']['Permissions'];
    $ArrayUserPrivileges = explode(',', trim($UserPrivileges));

    if (count($ArrayUserPrivileges) == 1 && $ArrayUserPrivileges[0] == '*') {
        return true;
    }

    $ArrayPrivileges = array();
    $Mode = 'DEFAULT';

    // Check if any operators given and setup privileges array - Start {
    if (preg_match("/\%/i", $Privilege)) {
        $ArrayPrivileges = explode('%', $Privilege);
        $Mode = 'OR';
    } else if (preg_match("/\&/i", $Privilege)) {
        $ArrayPrivileges = explode('&', $Privilege);
        $Mode = 'AND';
    } else {
        $ArrayPrivileges = array($Privilege);
        $Mode = 'DEFAULT';
    }
    // Check if any operators given and setup privileges array - End }
    // Loop through each privilege and check if user has privilege - Start {
    $TMPCounterPrivCheck = 0;
    foreach ($ArrayPrivileges as $EachPrivilege) {
        if (!in_array($EachPrivilege, $ArrayUserPrivileges)) {
            if ($Mode != 'OR') {
                return false;
            }
            $TMPCounterPrivCheck++;
        }
    }
    if ($Mode == 'OR' && $TMPCounterPrivCheck == count($ArrayPrivileges)) {
        return false;
    }
    // Loop through each privilege and check if user has privilege - End }

    return true;
}

/**
 * Checks if the user group has trial mode and if it's going to expire or not
 *
 * @return void
 * @author Cem Hurturk
 */
function AccountIsUnTrustedCheck($ArrayUserInformation) {

    Core::LoadObject('payments');
    Core::LoadObject('subscribers');
    Core::LoadObject('payments');
    Core::LoadObject('user_balance');

    Payments::SetUser($ArrayUserInformation);
    $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

    if ($LogExists) {
        $ArrayPaymentPeriod = Payments::GetLog();
        // if user is untrusted or has upaid invoices.  
        if ($ArrayPaymentPeriod['PaymentStatus'] != 'Paid' || $ArrayUserInformation['ReputationLevel'] != 'Trusted') {
            //here we will prevent creation of anything
            return true; //yes untrusted
        } else {
            return false;
        }
    }
    return $ArrayUserInformation['ReputationLevel'] != 'Trusted' ? true : false;
//    else {
//        //create period
//        Payments::SetUser($ArrayUserInformation);
//        Payments::CheckIfPaymentPeriodExists();
//        $ArrayPaymentPeriod = Payments::GetLog();
//
//        $PaymentStatus = UserBalance::processUserBalance($ArrayUserInformation, $ArrayPaymentPeriod);
//        return $PaymentStatus == 'Unpaid' ? true : false;
//    }
}

/**
 * Checks if the user group has trial mode and if it's going to expire or not
 *
 * @return void
 * @author Cem Hurturk
 */
function InterfaceUserTrialNotice($ArrayUserInformation, $Return = true, $LangExpireNotice = '') {

    if ($ArrayUserInformation['GroupInformation']['TrialGroup'] == 'Yes') {
        $DaysRemaining = (strtotime($ArrayUserInformation['UserSince']) + $ArrayUserInformation['GroupInformation']['TrialExpireSeconds']) - time();
        $DaysRemaining = ceil($DaysRemaining / 86400);

        if ($Return == true) {
            return sprintf($LangExpireNotice, $DaysRemaining);
        } else {
            printf($LangExpireNotice, $DaysRemaining);
        }
    }
}

function GetCountryList() {
    $ArrayCountryList = array(
        'AD' => "Andorra",
        'AE' => "United Arab Emirates",
        'AF' => "Afghanistan",
        'AG' => "Antigua & Barbuda",
        'AI' => "Anguilla",
        'AL' => "Albania",
        'AM' => "Armenia",
        'AN' => "Netherlands Antilles",
        'AO' => "Angola",
        'AQ' => "Antarctica",
        'AR' => "Argentina",
        'AS' => "American Samoa",
        'AT' => "Austria",
        'AU' => "Australia",
        'AW' => "Aruba",
        'AZ' => "Azerbaijan",
        'BA' => "Bosnia and Herzegovina",
        'BB' => "Barbados",
        'BD' => "Bangladesh",
        'BE' => "Belgium",
        'BF' => "Burkina Faso",
        'BG' => "Bulgaria",
        'BH' => "Bahrain",
        'BI' => "Burundi",
        'BJ' => "Benin",
        'BM' => "Bermuda",
        'BN' => "Brunei Darussalam",
        'BO' => "Bolivia",
        'BR' => "Brazil",
        'BS' => "Bahama",
        'BT' => "Bhutan",
        'BV' => "Bouvet Island",
        'BW' => "Botswana",
        'BY' => "Belarus",
        'BZ' => "Belize",
        'CA' => "Canada",
        'CC' => "Cocos (Keeling) Islands",
        'CF' => "Central African Republic",
        'CG' => "Congo",
        'CH' => "Switzerland",
        'CI' => "Côte D'ivoire (Ivory Coast)",
        'CK' => "Cook Iislands",
        'CL' => "Chile",
        'CM' => "Cameroon",
        'CN' => "China",
        'CO' => "Colombia",
        'CR' => "Costa Rica",
        'CU' => "Cuba",
        'CV' => "Cape Verde",
        'CX' => "Christmas Island",
        'CY' => "Cyprus",
        'CZ' => "Czech Republic",
        'DE' => "Germany",
        'DJ' => "Djibouti",
        'DK' => "Denmark",
        'DM' => "Dominica",
        'DO' => "Dominican Republic",
        'DZ' => "Algeria",
        'EC' => "Ecuador",
        'EE' => "Estonia",
        'EG' => "Egypt",
        'EH' => "Western Sahara",
        'ER' => "Eritrea",
        'ES' => "Spain",
        'ET' => "Ethiopia",
        'FI' => "Finland",
        'FJ' => "Fiji",
        'FK' => "Falkland Islands (Malvinas)",
        'FM' => "Micronesia",
        'FO' => "Faroe Islands",
        'FR' => "France",
        'FX' => "France, Metropolitan",
        'GA' => "Gabon",
        'GB' => "United Kingdom (Great Britain)",
        'GD' => "Grenada",
        'GE' => "Georgia",
        'GF' => "French Guiana",
        'GH' => "Ghana",
        'GI' => "Gibraltar",
        'GL' => "Greenland",
        'GM' => "Gambia",
        'GN' => "Guinea",
        'GP' => "Guadeloupe",
        'GQ' => "Equatorial Guinea",
        'GR' => "Greece",
        'GS' => "South Georgia and the South Sandwich Islands",
        'GT' => "Guatemala",
        'GU' => "Guam",
        'GW' => "Guinea-Bissau",
        'GY' => "Guyana",
        'HK' => "Hong Kong",
        'HM' => "Heard & McDonald Islands",
        'HN' => "Honduras",
        'HR' => "Croatia",
        'HT' => "Haiti",
        'HU' => "Hungary",
        'ID' => "Indonesia",
        'IE' => "Ireland",
        'IL' => "Israel",
        'IN' => "India",
        'IO' => "British Indian Ocean Territory",
        'IQ' => "Iraq",
        'IR' => "Islamic Republic of Iran",
        'IS' => "Iceland",
        'IT' => "Italy",
        'JM' => "Jamaica",
        'JO' => "Jordan",
        'JP' => "Japan",
        'KE' => "Kenya",
        'KG' => "Kyrgyzstan",
        'KH' => "Cambodia",
        'KI' => "Kiribati",
        'KM' => "Comoros",
        'KN' => "St. Kitts and Nevis",
        'KP' => "Korea, Democratic People's Republic of",
        'KR' => "Korea, Republic of",
        'KW' => "Kuwait",
        'KY' => "Cayman Islands",
        'KZ' => "Kazakhstan",
        'LA' => "Lao People's Democratic Republic",
        'LB' => "Lebanon",
        'LC' => "Saint Lucia",
        'LI' => "Liechtenstein",
        'LK' => "Sri Lanka",
        'LR' => "Liberia",
        'LS' => "Lesotho",
        'LT' => "Lithuania",
        'LU' => "Luxembourg",
        'LV' => "Latvia",
        'LY' => "Libyan Arab Jamahiriya",
        'MA' => "Morocco",
        'MC' => "Monaco",
        'MD' => "Moldova, Republic of",
        'MG' => "Madagascar",
        'MH' => "Marshall Islands",
        'ML' => "Mali",
        'MN' => "Mongolia",
        'MM' => "Myanmar",
        'MO' => "Macau",
        'MP' => "Northern Mariana Islands",
        'MQ' => "Martinique",
        'MR' => "Mauritania",
        'MS' => "Monserrat",
        'MT' => "Malta",
        'MU' => "Mauritius",
        'MV' => "Maldives",
        'MW' => "Malawi",
        'MX' => "Mexico",
        'MY' => "Malaysia",
        'MZ' => "Mozambique",
        'NA' => "Namibia",
        'NC' => "New Caledonia",
        'NE' => "Niger",
        'NF' => "Norfolk Island",
        'NG' => "Nigeria",
        'NI' => "Nicaragua",
        'NL' => "Netherlands",
        'NO' => "Norway",
        'NP' => "Nepal",
        'NR' => "Nauru",
        'NU' => "Niue",
        'NZ' => "New Zealand",
        'OM' => "Oman",
        'PA' => "Panama",
        'PE' => "Peru",
        'PF' => "French Polynesia",
        'PG' => "Papua New Guinea",
        'PH' => "Philippines",
        'PK' => "Pakistan",
        'PL' => "Poland",
        'PM' => "St. Pierre & Miquelon",
        'PN' => "Pitcairn",
        'PR' => "Puerto Rico",
        'PT' => "Portugal",
        'PW' => "Palau",
        'PY' => "Paraguay",
        'QA' => "Qatar",
        'RE' => "Réunion",
        'RO' => "Romania",
        'RU' => "Russian Federation",
        'RW' => "Rwanda",
        'SA' => "Saudi Arabia",
        'SB' => "Solomon Islands",
        'SC' => "Seychelles",
        'SD' => "Sudan",
        'SE' => "Sweden",
        'SG' => "Singapore",
        'SH' => "St. Helena",
        'SI' => "Slovenia",
        'SJ' => "Svalbard & Jan Mayen Islands",
        'SK' => "Slovakia",
        'SL' => "Sierra Leone",
        'SM' => "San Marino",
        'SN' => "Senegal",
        'SO' => "Somalia",
        'SR' => "Suriname",
        'ST' => "Sao Tome & Principe",
        'SV' => "El Salvador",
        'SY' => "Syrian Arab Republic",
        'SZ' => "Swaziland",
        'TC' => "Turks & Caicos Islands",
        'TD' => "Chad",
        'TF' => "French Southern Territories",
        'TG' => "Togo",
        'TH' => "Thailand",
        'TJ' => "Tajikistan",
        'TK' => "Tokelau",
        'TM' => "Turkmenistan",
        'TN' => "Tunisia",
        'TO' => "Tonga",
        'TP' => "East Timor",
        'TR' => "Turkey",
        'TT' => "Trinidad & Tobago",
        'TV' => "Tuvalu",
        'TW' => "Taiwan, Province of China",
        'TZ' => "Tanzania, United Republic of",
        'UA' => "Ukraine",
        'UG' => "Uganda",
        'UM' => "United States Minor Outlying Islands",
        'US' => "United States of America",
        'UY' => "Uruguay",
        'UZ' => "Uzbekistan",
        'VA' => "Vatican City State (Holy See)",
        'VC' => "St. Vincent & the Grenadines",
        'VE' => "Venezuela",
        'VG' => "British Virgin Islands",
        'VI' => "United States Virgin Islands",
        'VN' => "Viet Nam",
        'VU' => "Vanuatu",
        'WF' => "Wallis & Futuna Islands",
        'WS' => "Samoa",
        'YE' => "Yemen",
        'YT' => "Mayotte",
        'YU' => "Yugoslavia",
        'ZA' => "South Africa",
        'ZM' => "Zambia",
        'ZR' => "Zaire",
        'ZW' => "Zimbabwe",
    );

    return $ArrayCountryList;
}

function getCountryName($country_code) {
    $countryList = GetCountryList();
    return $countryList[strtoupper($country_code)];
}

?>