<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved 
| routes must come before any wildcard or regular expression routes.
|
*/

$route['default_controller']		= "user/controller_index";
$route['scaffolding_trigger']		= "";

/*
| -------------------------------------------------------------------------
| OEMPRO ROUTES
| -------------------------------------------------------------------------
*/

$route['admin']						= "admin/controller_index";
$route['user']						= "user/controller_index";
$route['client']					= "client/controller_index";
$route['public']					= "public/controller_index";
$route['subscriber']				= "subscriber/controller_area";

$route['admin/(:any)']				= "admin/controller_$1";
$route['user/(:any)']				= "user/controller_$1";
$route['client/(:any)']				= "client/controller_$1";
$route['public/(:any)']				= "public/controller_$1";
$route['subscriber/(:any)']			= "subscriber/controller_area/$1";
$route['(:any)']					= "controller_plugin/index/$1"; // this line should be at the end of all routes !!! IMPORTANT !!!

/* End of file routes.php */
/* Location: ./system/application/config/routes.php */