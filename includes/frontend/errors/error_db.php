<html>
<head>
<?php if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'): ?>
	<title>Database Error</title>
<?php else: ?>
	<title>Unexpected Error Occurred</title>
<?php endif; ?>
<style type="text/css">
	body { background-color: #e1e1e1; margin: 100px; font-family: Helvetica, Arial; font-size: 14px; color: #4C4C4C; }
	#content  { width: 300px; margin: 0 auto; border: #999 1px solid; background-color: #fff; padding: 20px 20px 12px 20px; }
	h1 { font-weight: normal; font-size: 18px; color: #4C4C4C; margin: 0 0 4px 0; }
</style>
</head>
<body>
	<div id="content">
		<?php if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'): ?>
			<h1><?php echo $heading; ?></h1>
			<?php echo $message; ?>
		<?php else: ?>
			<h1>Unexpected error occurred</h1>
			We have encountered unexpected error. Please contact system administrator.
		<?php endif; ?>
	</div>
</body>
</html>
