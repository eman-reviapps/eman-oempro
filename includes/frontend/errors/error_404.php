<?php header('HTTP/1.1 404 Not found') ?>
<html>
<head>
<title>404 Not Found</title>
<style type="text/css">
	body { background-color: #e1e1e1; margin: 100px; font-family: Helvetica, Arial; font-size: 14px; color: #4C4C4C; }
	#content  { width: 300px; margin: 0 auto; border: #999 1px solid; background-color: #fff; padding: 20px 20px 12px 20px; }
	h1 { font-weight: normal; font-size: 18px; color: #4C4C4C; margin: 0 0 4px 0; }
</style>
</head>
<body>
	<div id="content">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
</body>
</html>