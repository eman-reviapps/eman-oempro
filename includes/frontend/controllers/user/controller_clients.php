<?php

/**
 * Clients controller
 *
 * @author Mert Hurturk
 */
class Controller_Clients extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('clients');
        Core::LoadObject('api');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Mert Hurturk
     */
    function index() {
        // Privilege check - Start {
        if (Users::HasPermissions(array('Clients.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // Privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteClients') {
            $array_event_return = $this->_event_delete_clients($this->input->post('SelectedClients'));
        }
        // Events - End }
        // Retrieve clients - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'clients.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'ClientName',
                        'ordertype' => 'ASC'
                    )
        ));

        if ($array_return['Success'] == false) {
            $array_clients = array();
        } else {
            $array_clients = $array_return['Clients'] === false ? array() : $array_return['Clients'];
        }
        // Retrieve clients - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseClients'],
            'CurrentMenuItem' => 'Settings',
            'SubSection' => 'Clients',
            'UserInformation' => $this->array_user_information,
            'Clients' => $array_clients,
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/settings', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Create controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function create() {
        // Privilege check - Start {
        if (Users::HasPermissions(array('Client.Create'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // Privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'CreateClient') {
            $array_event_return = $this->_event_create_client();
        }
        // Events - End }
        // Retrieve campaigns - Start {
        $array_campaigns = array();
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'campaigns.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'retrievestatistics' => 'false'
                    )
        ));
        $array_campaigns = $array_return['Campaigns'];
        // Retrieve campaigns - End }
        // Retrieve lists - Start {
        $array_lists = array();
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        $array_lists = $array_return['Lists'];
        unset($array_return);
        // Retrieve lists - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateClient'],
            'CurrentMenuItem' => 'Settings',
            'ActiveSettingsItem' => 'Clients',
            'UserInformation' => $this->array_user_information,
            'Campaigns' => $array_campaigns,
            'Lists' => $array_lists,
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/client_create', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Edit controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function edit($client_id) {
        // Privilege check - Start {
        if (Users::HasPermissions(array('Client.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // Privilege check - End }
        // Retrieve client information - Start {
        $array_client = Clients::RetrieveClient(array('*'), array('ClientID' => $client_id, 'RelOwnerUserID' => $this->array_user_information['UserID']));
        $array_client_campaigns_temp = Clients::RetrieveAssignedCampaigns($client_id);
        $array_client_lists_temp = Clients::RetrieveAssignedSubscriberLists($client_id);

        $array_client_campaigns = array();
        foreach ($array_client_campaigns_temp as $each) {
            $array_client_campaigns[] = $each['CampaignID'];
        }
        unset($array_client_campaigns_temp);

        $array_client_lists = array();
        foreach ($array_client_lists_temp as $each) {
            $array_client_lists[] = $each['SubscriberListID'];
        }
        unset($array_client_lists_temp);
        // Retrieve client information - End }
        // Events - Start {
        if ($this->input->post('Command') == 'UpdateClient') {
            $array_event_return = $this->_event_update_client();
        }
        // Events - End }
        // Retrieve campaigns - Start {
        $array_campaigns = array();
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'campaigns.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'recordsperrequest' => 1000000
                    )
        ));
        $array_campaigns = $array_return['Campaigns'];
        // Retrieve campaigns - End }
        // Retrieve lists - Start {
        $array_lists = array();
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        $array_lists = $array_return['Lists'];
        unset($array_return);
        // Retrieve lists - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserUpdateClient'],
            'CurrentMenuItem' => 'Settings',
            'ActiveSettingsItem' => 'Clients',
            'UserInformation' => $this->array_user_information,
            'ClientInformation' => $array_client,
            'Campaigns' => $array_campaigns,
            'ClientCampaigns' => $array_client_campaigns,
            'Lists' => $array_lists,
            'ClientLists' => $array_client_lists,
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/client_edit', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Create client event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_create_client() {
        // Field validations - Start {
        $this->form_validation->set_rules("ClientName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1097'], 'required');
        $this->form_validation->set_rules("ClientEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0758'], 'required|valid_email');
        $this->form_validation->set_rules("ClientUsername", ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'], 'required');
        $this->form_validation->set_rules("ClientPassword", ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create client - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'client.create',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'clientname' => $this->input->post('ClientName'),
                        'clientemailaddress' => $this->input->post('ClientEmailAddress'),
                        'clientusername' => $this->input->post('ClientUsername'),
                        'clientpassword' => $this->input->post('ClientPassword')
                    )
        ));

        if ($array_return['Success'] == false) {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            switch ($error_code) {
                case '6':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1100']);
                    break;
                case '7':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1101']);
                    break;
                default:
                    break;
            }
        } else {
            $new_client_id = $array_return['ClientID'];
            if (count($this->input->post('ClientCampaigns')) > 0) {
                $array_return = API::call(array(
                            'format' => 'array',
                            'command' => 'client.assigncampaigns',
                            'protected' => true,
                            'username' => $this->array_user_information['Username'],
                            'password' => $this->array_user_information['Password'],
                            'parameters' => array(
                                'clientid' => $new_client_id,
                                'campaignids' => implode(',', $this->input->post('ClientCampaigns'))
                            )
                ));
            }

            if (count($this->input->post('ClientLists')) > 0) {
                $array_return = API::call(array(
                            'format' => 'array',
                            'command' => 'client.assignsubscriberlists',
                            'protected' => true,
                            'username' => $this->array_user_information['Username'],
                            'password' => $this->array_user_information['Password'],
                            'parameters' => array(
                                'clientid' => $new_client_id,
                                'subscriberlistids' => implode(',', $this->input->post('ClientLists'))
                            )
                ));
            }

            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1102']);
            redirect(InterfaceAppURL(true) . '/user/clients/', 'location', '302');
        }
    }

    /**
     * Update client event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_update_client() {
        // Field validations - Start {
        $this->form_validation->set_rules("ClientName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1097'], 'required');
        $this->form_validation->set_rules("ClientEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0758'], 'required|valid_email');
        $this->form_validation->set_rules("ClientUsername", ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'], 'required');
        // $this->form_validation->set_rules("ClientPassword", ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Update client - Start {
        $api_parameters = array(
            'clientid' => $this->input->post('ClientID'),
            'clientname' => $this->input->post('ClientName'),
            'clientemailaddress' => $this->input->post('ClientEmailAddress'),
            'clientusername' => $this->input->post('ClientUsername'),
        );
        if ($this->input->post('ClientPassword') != false || strlen($this->input->post('ClientPassword')) > 0) {
            $api_parameters['clientpassword'] = $this->input->post('ClientPassword');
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'client.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => $api_parameters
        ));

        if ($array_return['Success'] == false) {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            switch ($error_code) {
                case '9':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1100']);
                    break;
                case '10':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1101']);
                    break;
                default:
                    break;
            }
        } else {
            if (count($this->input->post('ClientCampaigns')) > 0) {
                $array_return = API::call(array(
                            'format' => 'array',
                            'command' => 'client.assigncampaigns',
                            'protected' => true,
                            'username' => $this->array_user_information['Username'],
                            'password' => $this->array_user_information['Password'],
                            'parameters' => array(
                                'clientid' => $this->input->post('ClientID'),
                                'campaignids' => implode(',', $this->input->post('ClientCampaigns'))
                            )
                ));
            }

            if (count($this->input->post('ClientLists')) > 0) {
                $array_return = API::call(array(
                            'format' => 'array',
                            'command' => 'client.assignsubscriberlists',
                            'protected' => true,
                            'username' => $this->array_user_information['Username'],
                            'password' => $this->array_user_information['Password'],
                            'parameters' => array(
                                'clientid' => $this->input->post('ClientID'),
                                'subscriberlistids' => implode(',', $this->input->post('ClientLists'))
                            )
                ));
            }

            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1104']);
            redirect(InterfaceAppURL(true) . '/user/clients/', 'location', '302');
        }
    }

    /**
     * Delete clients event
     *
     * @return void
     * @author Mert Hurturk
     */
    function _event_delete_clients($array_client_ids) {
        // Field validations - Start {
        $array_form_rules = array(
            array
                (
                'field' => 'SelectedClients[]',
                'label' => 'clients',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($array_form_rules);
        $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1083']);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, validation_errors());
        }
        // Run validation - End }
        // Delete clients - Start {
        $array_api_vars = array(
            'clients' => implode(',', $array_client_ids),
        );
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'clients.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => $array_api_vars
        ));

        if ($array_return['Success'] == false) {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_Return['ErrorCode'][0] : $array_return['ErrorCode']);
            switch ($error_code) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1083']);
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1084']);
            return true;
        }
        // Delete email templates - End }
    }

}

?>