<?php

/**
 * Email content builder controller
 *
 * @author Mert Hurturk
 */
class Controller_EmailDragDropBuilder extends MY_Controller {

    public $email_html;

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('campaigns');
        Core::LoadObject('api');
        Core::LoadObject('email_data');
        Core::LoadObject('personalization');
        // Load other modules - End	

        $this->load->helper('url');

        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, APP_URL . APP_DIRNAME . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        $this->email_html = <<<EOD
<!DOCTYPE html>
<html lang="en">

    <head>
    <meta name="viewport" content="width=device-width" initial-scale="1.0" user-scalable="yes" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>

body {
  margin: 0;
  padding: 0;
}

body, table, td, p, a, li {
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
}

a {
  word-wrap: break-word;
}

table td {
  border-collapse: collapse;
}

table {
  border-spacing: 0;
  border-collapse: collapse;
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

table, td {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

.ReadMsgBody {
	width:100%;
	background-color: #eeeeee;
}

.ExternalClass {
	width: 100%;
	background-color: #eeeeee;
}

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}

.ExternalClass * {
	line-height: 100%;
}

@media only screen and (max-width: 640px) {

  table[class="main"],td[class="main"] { width:100% !important; min-width: 200px !important; }

  table[class="logo-img"] { width:100% !important; float: none; margin-bottom: 15px;}
  table[class="logo-img"] td { text-align: center !important;}

  table[class="logo-title"] { width:100% !important; float: none;}
  table[class="logo-title"] td { text-align: center; height: auto}
  table[class="logo-title"] h1 { font-size: 24px !important; }
  table[class="logo-title"] h2 { font-size: 18px !important; }

  td[class="header-img"] img { width:100% !important; height:auto !important; }

  td[class="title"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="title"] h1 { font-size: 24px !important; }

  td.block-text { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="block-text"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="block-text"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="block-text"] li { font-size: 16px !important; line-height: 170% !important; }

  td[class="two-columns"] { padding-left: 25px !important; padding-right: 25px !important; }
  table[class="text-column"] { width:100% !important; float: none; margin-bottom: 15px;}

  td[class="image-caption"] { padding-left: 25px !important; padding-right: 25px !important; }
  table[class="image-caption-container"] { width:100% !important;}
  table[class="image-caption-column"] { width:100% !important; float: none;}
  td[class="image-caption-content"] img { width:100% !important; height:auto !important; }
  td[class="image-caption-content"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="image-caption-content"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="image-caption-top-gap"] { height: 15px !important; }
  td[class="image-caption-bottom-gap"] { height: 5px !important; }

  td[class="text"] { width:100% !important; }
  td[class="text"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="text"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="gap"] { display:none; }

  td[class="header"] { padding: 25px 25px 25px 25px !important; }
  td[class="header"] h1 { font-size: 24px !important; }
  td[class="header"] h2 { font-size: 20px !important; }

  td[class="footer"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="footer"] p { font-size: 13px !important; }
  table[class="footer-side"] { width: 100% !important; float: none !important; }
  td[class="footer-side"] { text-align: center !important; }
  td[class="social-links"] { text-align: center !important; }
  table[class="footer-social-icons"] { float: none !important; margin: 0px auto !important; }
  td[class="social-icon-link"] { padding: 0px 5px !important; }

  td[class="image"] img { width:100% !important; height:auto !important; }
  td[class="image"] { padding-left: 25px !important; padding-right: 25px !important; }

  td[class="image-full"] img { width:100% !important; height:auto !important; }
  td[class="image-full"] { padding-left: 0px !important; padding-right: 0px !important; }

  td[class="image-group"] img { width:100% !important; height:auto !important; margin: 15px 0px 15px 0px !important; }
  td[class="image-group"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="image-in-table"] { width:100% !important; float: none; margin-bottom: 15px;}
  table[class="image-in-table"] td { width:100% !important;}
  table[class="image-in-table"] img { width:100% !important; height:auto !important; }


  td[class="image-text"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="image-text"] p { font-size: 16px !important; line-height: 170% !important; }

  td[class="divider-simple"] { padding-left: 25px !important; padding-right: 25px !important; }

  td[class="divider-full"] { padding-left: 0px !important; padding-right: 0px !important; }

  td[class="social"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="preheader"] { display:none; }
  td[class="preheader-gap"] { display:none; }
  td[class="preheader-link"] { display:none; }
  td[class="preheader-text"] { width:100%; }

  td[class="buttons"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="button"] { width:100% !important; float: none; }

  td[class="content-buttons"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="buttons-full-width"] { padding-left: 0px !important; padding-right: 0px !important; }
  td[class="buttons-full-width"] a { width:100% !important; padding-left: 0px !important; padding-right: 0px !important; }
  td[class="buttons-full-width"] span { width:100% !important; padding-left: 0px !important; padding-right: 0px !important; }

  table[class="content"] { width:100% !important; float: none !important;}
  td[class="gallery-image"] { width:100% !important; padding: 0px !important;}

  table[class="social"] { width: 100%!important; text-align: center!important; }
  table[class="links"] { width: 100%!important; }
  table[class="links"] td { text-align: center!important; }
  table[class="footer-btn"] { text-align: center!important; width: 100%!important; margin-bottom: 10px; }
  table[class="footer-btn-wrap"] { margin-bottom: 0px; width: 100%!important; }

  td[class="head-social"]  { width: 100%!important; text-align: center!important; padding-top: 20px; }
  td[class="head-logo"]  { width: 100%!important; text-align: center!important; }
  tr[class="header-nav"] { display: none; }

}

</style>
</head>

<body>
    {body}
</body>
</html>
EOD;
        // Check if user account has expired - End }
    }

    /**
     * Displays iguana content editor.
     *
     * @return void
     * @author Mert Hurturk
     */
    function edit($mode, $session_key, $entity_id, $email_id = 0) {
        // Retrieve personalization tags - Start {
        if ($mode == 'campaign') {
            // Get recipient list ids - Start {
            $array_campaign = API::call(array(
                        'format' => 'array',
                        'command' => 'campaign.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'campaignid' => $entity_id,
                            'retrievestatistics' => false
                        )
            ));
            $array_campaign = $array_campaign['Campaign'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('campaign', $this->array_user_information['UserID'], $array_campaign['RecipientLists'][0], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($mode == 'confirmation') {
            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('confirmation', $this->array_user_information['UserID'], $entity_id, ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($mode == 'autoresponder') {
            // Get recipient list ids - Start {
            $array_auto_responder = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $entity_id,
                        )
            ));
            $array_auto_responder = $array_auto_responder['AutoResponder'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('autoresponder', $this->array_user_information['UserID'], $array_auto_responder['RelListID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        }
        // Retrieve personalization tags - End }

        $ScreenMessage = isset($_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage']) ? $_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage'] : '';
        unset($_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage']);

        Core::LoadObject('wizard');
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $flow = $wizard->get_extra_parameter('flow');

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailDragDropBuilder'],
            'Mode' => $mode,
            'Flow' => $flow,
            'EmailID' => $email_id,
            'ReturnURL' => $_SESSION[SESSION_NAME][$session_key],
            'SubjectTags' => $array_subject_tags,
            'ContentTags' => $array_content_tags,
            'SaveURL' => '/user/emaildragdropbuilder/edit/' . $mode . '/' . $session_key . '/' . $entity_id . '/' . $email_id,
            'Message' => $ScreenMessage
        );


        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/email_content_dragdrop_builder', $array_view_data);
    }

    /**
     * Saves email content to the session (or directly saves in database) and redirects to the submitted url
     *
     * @return void
     * @author Mert Hurturk
     */
    function saveemail() {
        if ($this->input->post('EmailID') == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $email->set_html_content($this->input->post('EmailHTMLContent'));

            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        } else {
            if ($this->input->post('Mode') == 'confirmation') {
                $validation_scope = 'OptIn';
            } elseif ($this->input->post('Mode') == 'autoresponder') {
                $validation_scope = 'AutoResponder';
            } elseif ($this->input->post('Mode') == 'campaign') {
                $validation_scope = 'Campaign';
            }

            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $this->input->post('EmailID'),
                            'htmlcontent' => $this->input->post('EmailHTMLContent'),
                            'validatescope' => $validation_scope
                        )
            ));

            if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
                $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
                if ($email instanceof Email_data) {
                    $email->set_html_content($this->input->post('EmailHTMLContent'));
                    $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
                }
            }
        }

        $_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1857'];
        redirect($this->input->post('SaveURL'));
    }

    function uploadImageS3($MediaData, $ArrayInfo, $PATH) {

        Core::LoadObject('aws_s3');
        $uri = $PATH . '/' . ($this->array_user_information['UserID']) . '/' . uniqid();

        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
        $result = $ObjectS3->putObjectString($MediaData, S3_BUCKET, $uri, S3::ACL_PUBLIC_READ, array(), $ArrayInfo['type']);
        if ($result) {
            return $uri;
        }
        return "";
    }

    function saveajaxemail() {

        Core::LoadObject('aws_s3');

        $pure_html = $this->input->post('pure_html');
        $html = $this->input->post('html');
        $img_val = $this->input->post('img_val');

        //Get the base-64 string from data
        $filteredData = substr($img_val, strpos($img_val, ",") + 1);
        $MediaData = base64_decode($filteredData);

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $MediaData, FILEINFO_MIME_TYPE);

        $ArrayInfo = array(
            "type" => $mime_type,
            "size" => getimagesizefromstring($MediaData),
            "tmp_name" => 'dragdrop_' . time() . '.png',
        );
        $image_uri = $this->uploadImageS3($MediaData, $ArrayInfo, S3_SCREENSHOTS_PATH);


        $_SESSION[SESSION_NAME]['screenshot_name'] = $image_uri;

        $html = ( get_magic_quotes_gpc() ) ? stripslashes($html) : $html;

        $email_html = $this->email_html;
        $email_html = str_replace('{body}', $html, $email_html);

        if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $old_screenshot = $email->getScreenshot();
            //check if old screenshot exists,then delete it
            if ($this->input->post('Flow') != 'previouscampaign') {
                if (isset($old_screenshot) && !empty($old_screenshot)) {
                    $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
                    $ObjectS3->deleteObject(S3_BUCKET, $old_screenshot);
                }
            }
        }
        if ($this->input->post('EmailID') == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);

            $email->set_html_content($email_html);
            $email->setScreenshot($image_uri);
            $email->setHtml_pure($pure_html);

            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        } else {
            if ($this->input->post('Mode') == 'confirmation') {
                $validation_scope = 'OptIn';
            } elseif ($this->input->post('Mode') == 'autoresponder') {
                $validation_scope = 'AutoResponder';
            } elseif ($this->input->post('Mode') == 'campaign') {
                $validation_scope = 'Campaign';
            }
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $this->input->post('EmailID'),
                            'htmlcontent' => $email_html,
                            'validatescope' => $validation_scope
                        )
            ));
            if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
                $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
                $email->set_html_content($email_html);
                $email->setScreenshot($image_uri);
                $email->setHtml_pure($pure_html);

                $ArrayCriterias = array('EmailID' => $this->input->post('EmailID'));
                $ArrayEmailInformation = array('ScreenshotImage' => $image_uri, 'HTMLPure' => $pure_html);
                Emails::Update($ArrayEmailInformation, $ArrayCriterias);

                $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
            }
        }

        $_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1857'];

        print $email_html;
        exit;
    }

    function saveajaxemail_old() {

        $pure_html = $this->input->post('pure_html');
        $html = $this->input->post('html');
        $img_val = $this->input->post('img_val');
        $upload_dir = DATA_PATH . "screenshots/";
        $file_name = 'dragdrop_' . time() . '.png';

        //Get the base-64 string from data
        $filteredData = substr($img_val, strpos($img_val, ",") + 1);
        //Decode the string
        $unencodedData = base64_decode($filteredData);
        //Save the image
        file_put_contents($upload_dir . $file_name, $unencodedData);

        $_SESSION[SESSION_NAME]['screenshot_name'] = $file_name;

        $html = ( get_magic_quotes_gpc() ) ? stripslashes($html) : $html;

        $email_html = $this->email_html;
        $email_html = str_replace('{body}', $html, $email_html);

        if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $old_screenshot = $email->getScreenshot();
            //check if old screenshot exists,then delete it
            if ($this->input->post('Flow') != 'previouscampaign') {
                if (isset($old_screenshot) && !empty($old_screenshot)) {
                    $image_path = DATA_PATH . "screenshots/" . $old_screenshot;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            }
        }
        if ($this->input->post('EmailID') == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);

            $email->set_html_content($email_html);
            $email->setScreenshot($file_name);
            $email->setHtml_pure($pure_html);

            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        } else {
            if ($this->input->post('Mode') == 'confirmation') {
                $validation_scope = 'OptIn';
            } elseif ($this->input->post('Mode') == 'autoresponder') {
                $validation_scope = 'AutoResponder';
            } elseif ($this->input->post('Mode') == 'campaign') {
                $validation_scope = 'Campaign';
            }
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $this->input->post('EmailID'),
                            'htmlcontent' => $email_html,
                            'validatescope' => $validation_scope
                        )
            ));
            if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
                $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
                $email->set_html_content($email_html);
                $email->setScreenshot($file_name);
                $email->setHtml_pure($pure_html);

                $ArrayCriterias = array('EmailID' => $this->input->post('EmailID'));
                $ArrayEmailInformation = array('ScreenshotImage' => $file_name, 'HTMLPure' => $pure_html);
                Emails::Update($ArrayEmailInformation, $ArrayCriterias);

                $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
            }
        }

        $_SESSION[SESSION_NAME]['ContentDragDropBuilderMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1857'];

        print $email_html;
        exit;
    }

    /**
     * Displays email content in iguana content editor.
     *
     * @return void
     * @author Mert Hurturk
     */
    function content($email_id = 0) {
        if ($email_id == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $email_content = $email->get_html_content();
        } else {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $email_id
                        )
            ));
            $email_content = $array_return['EmailInformation']['HTMLContent'];
        }

        print $email_content;
        exit;
    }

    /**
     * Test drives email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function index($CampaignID = '') {
        // Check if template builder session exists - Start {
        if ((!isset($_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent']) || $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] == '') && $CampaignID == '') {
            exit;
        }
        // Check if template builder session exists - End }
        // Get recipient list ids - Start {
        $ArrayRecipientListIDs = array();
        foreach ($_SESSION[SESSION_NAME]['CampaignInformation']['Recipients'] as $Each) {
            $TempArray = explode(':', $Each);
            $ArrayRecipientListIDs[] = $TempArray[0];
        }
        $ArrayRecipientListIDs = array_unique($ArrayRecipientListIDs);
        // Get recipient list ids - End }
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($this->ArrayUserInformation['UserID'], $ArrayRecipientListIDs, ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_campaign_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'Campaign');
        $array_list_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'List');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
        $array_content_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0779'] => $array_campaign_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0780'] => $array_list_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        $array_subject_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        // Retrieve personalization tags - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailContentBuilder'],
            'Flow' => $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'],
            'CampaignID' => $CampaignID,
            'SubjectTags' => $array_subject_tags,
            'ContentTags' => $array_content_tags
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('user/email_content_builder', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Save controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function save($CampaignID = '') {
        $this->load->helper('url');
        if ($CampaignID == '') {
            $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] = $this->input->post('EmailHTMLContent');
            redirect(InterfaceAPPUrl(true) . '/user/campaigns/create/' . $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'] . '/' . ($_SESSION[SESSION_NAME]['CampaignInformation']['Step'] + 1));
        } else {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Content" => true
            ));
            $CampaignInformation = $CampaignInformation[0];
            $ArrayAPIVars = array(
                'emailid' => $CampaignInformation['RelEmailID'],
                'fromname' => $CampaignInformation['Email']['FromName'],
                'fromemail' => $CampaignInformation['Email']['FromEmail'],
                'replytoname' => $CampaignInformation['Email']['ReplyToName'],
                'replytoemail' => $CampaignInformation['Email']['ReplyToEmail'],
                'subject' => $CampaignInformation['Email']['Subject'],
                'plaincontent' => $CampaignInformation['Email']['PlainContent'],
                'htmlcontent' => $this->input->post('EmailHTMLContent'),
                'validatescope' => 'Campaign'
            );
            $ArrayReturn = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => $ArrayAPIVars
            ));

            redirect(InterfaceAPPUrl(true) . '/user/campaign/edit/' . $CampaignID);
        }
    }

    /**
     * Displays HTMLContent in the session
     *
     * @return void
     * @author Mert Hurturk
     * */
    function displayHTMLContent($CampaignID = '') {
        // Check if template builder session exists - Start {
        if ((!isset($_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent']) || $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] == '') && $CampaignID == '') {
            exit;
        }
        // Check if template builder session exists - End }
        // If campaign id is given, retrieve campaign informatino - Start {
        if ($CampaignID != '') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Content" => true
            ));
            $CampaignInformation = $CampaignInformation[0];
            $Content = $CampaignInformation['Email']['HTMLContent'];
        }
        // If campaign id is given, retrieve campaign informatino - End }
        else {
            $Content = $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'];
        }


        print $Content;
        exit;
    }

    /**
     * Test drives email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventTestDriveEmailTemplate() {
        $_SESSION['template_builder']['TemplateHTMLContent'] = $_POST['TemplateHTMLContent'];
        $this->load->helper('url');
        redirect(APP_URL . APP_DIRNAME . '/admin/emailtemplates/testdrive');
    }

    /*
     * Eman
     */

    function select() {
        $group = $_POST['group'];
        $path = $_POST['path'];
        $directory = TEMPLATE_PATH . 'dragdrop/elements/' . $group . "/";
        $scanned_files = array_diff(scandir($directory), array('..', '.'));

        $buffer = '';
        $ContentTags = $_SESSION[SESSION_NAME]['ContentTags'];

        $personalize_html;
        foreach ($scanned_files as $n => $file) {
            if (file_exists($directory . $file) && strstr($file, '.html')) {
                $buffer .= file_get_contents($directory . $file);
                $buffer = str_replace("{PATH}", TEMPLATE_URL, $buffer);
                $buffer = str_replace('src="elements/', 'src="' . TEMPLATE_URL . "dragdrop/elements/", $buffer);
            }
        }
        if ($group == 'personalize') {
            if (file_exists($directory . $group . '.php') && strstr($file, '.php')) {

                foreach ($ContentTags as $Label => $TagGroup) {
                    foreach ($TagGroup as $Tag => $Label) {
                        $buffer .= file_get_contents($directory . $file);
//                        $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px">' . $Tag . '</div>';
                        if (strstr($Label, 'Link')) {
                            $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px"><a href="' . $Tag . '" >' . $Label . '</a></div>';
                        } else {
                            $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px">' . $Tag . '</div>';
                        }
                        $label_html = '<div class="preview">' . $Label . '</div>';

                        $buffer = str_replace('<div style="margin:0px 0px 10px 0px;line-height:22px"></div>', $tag_html, $buffer);
                        $buffer = str_replace('<div class="preview"></div>', $label_html, $buffer);
                    }
                }
            }
        }

        print $buffer;
    }

    function saveDropImg() {
        $raw_data = $_POST['data'];
        $nomefile = $_POST['filename'];
        $uploaddir = DATA_PATH . "dragdrop/images/";
        $url = "images/";

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $raw_data));

        file_put_contents(
                $uploaddir . $nomefile, $data
        );

        $response = array('percorso' => $url . $nomefile);
        print(json_encode($response));
        exit();
    }

    function videoToImg() {
        $youtubevideo = $_REQUEST['youtubevideo'];
        $youtube = str_replace("https://www.youtube.com/watch?v=", "", $youtubevideo);
        $youtube = str_replace("https://www.youtube.com/watch?v=/", "", $youtube);
        $youtube = str_replace("https://youtube/", "", $youtube);
        $img = "http://img.youtube.com/vi/" . $youtube . "/0.jpg";

        $MediaData = file_get_contents($img);
        $image = base64_encode($MediaData);

        print_r($img);
        exit();
    }

    function grab_image($url, $saveto) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }

    function uploadImage() {

        $uploaded_image = "";
        Core::LoadObject('aws_s3');
        if (!empty($_FILES)) {
            $MediaData = file_get_contents($_FILES['nomefile']['tmp_name']);

            $ArrayInfo = array(
                "type" => $_FILES['nomefile']['type'],
                "size" => $_FILES['nomefile']['size'],
                "tmp_name" => $_FILES['nomefile']['tmp_name'],
            );
            $uri = $this->uploadImageS3($MediaData, $ArrayInfo, S3_DRAGDROP_PATH);
            $uploaded_image = AWS_END_POINT . S3_BUCKET . "/" . $uri;
        }
        print_r($uploaded_image);
        exit();
    }

    function loadimages() {

        Core::LoadObject('aws_s3');
        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
        $files = $ObjectS3->getBucket(S3_BUCKET, S3_DRAGDROP_PATH . "/" . $this->array_user_information['UserID'] . "/", null, null, "/");

        $html = "<table class=\"table table-bordered\" width=\"100%\">\n";

        $html .= "<tr>\n";
        $i = 1;

        foreach ($files as $key => $file) {
            if ($file['size'] > 0) {
                $uri = AWS_END_POINT . S3_BUCKET . "/" . $file['name'];

                $html .= "
         <td style=\"background:#ffffff;margin:5px\">
            <center>
            <img crossOrigin=\"anonymous\"  src=\"$uri\" width=\"100\" height=\"100\" border=0 >
            </center>
            <a href=\"javascript:void(0);\" onclick=\"inserisci(this,false);\" class=\"insert-image\" data-image=\"$uri\"><span class=\"glyphicon glyphicon-download\"></span></a>
            <br>

         </td>\n
     ";
                if ($i == 3) {
                    $i = 0;
                    $html .= "</tr><tr>";
                }
                $i++;
            }
        }
        $html .= "</tr>";
        $html .= "</table>";

        print $html;
        exit();
    }

    function loadimages_old() {

        $folder = DATA_PATH . 'dragdrop' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
        $url = DATA_URL . 'dragdrop/images/';
        $i = 0;
        $handle = opendir($folder);
        while ($file = readdir($handle)) {
            if ($file != "." && $file != ".." && $file != ".DS_Store") {

                if (strlen($nome) > 0) {
                    if (trovaStringa($file, $nome)) {
                        $files[$i] = $file;
                        $i++;
                    }
                } else {
                    $files[$i] = $file;
                    $i++;
                }
            }
        }
        closedir($handle);

        $folder2 = str_replace("../../", "", $folder);
        $html = "<table class=\"table table-bordered\" width=\"100%\">\n";

        $html .= "<tr>\n";
        $i = 1;
        for ($s = 0; $s < count($files); $s++) {

            $html .= "
         <td style=\"background:#ffffff;margin:5px\">
            <center>
            <img src=\"$url/" . $files[$s] . "\" width=\"100\" height=\"100\" border=0 >
            </center>

            <span style=\"font-size:11px\">" . substr($files[$s], 0, 25) . "</span>
            <br>
            <a href=\"javascript:void(0);\" onclick=\"inserisci(this);\" class=\"insert-image\" data-image=\"{$url}/{$files[$s]}\"><span class=\"glyphicon glyphicon-download\"></span></a>
            <br>

         </td>\n
     ";
            if ($i == 3) {
                $i = 0;
                $html .= "</tr><tr>";
            }
            $i++;
        }
        $html .= "</tr>";
        $html .= "</table>";

        print $html;
        exit();
    }

}

// end of class User
?>