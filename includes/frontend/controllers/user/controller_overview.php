<?php

/**
 * Overview controller
 *
 * @author Mert Hurturk
 */
class Controller_Overview extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('statistics');
        Core::LoadObject('campaigns');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Create controller
     *
     * @author Mert Hurturk
     * */
    function index() {
        // Retrieve recent campaigns of user - Start {
        $campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignID', 'Type' => 'DESC'),
                    'Pagination' => array('Offset' => 0, 'Rows' => 10),
                    'Criteria' => array(
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID']),
                        array(
                            'Link' => 'AND',
                            array('Column' => '%c%.CampaignStatus', 'Operator' => '!=', 'Value' => 'Draft'),
                            array('Column' => '%c%.CampaignStatus', 'Operator' => '!=', 'Value' => 'Failed', 'Link' => 'AND'),
                            array('Column' => '%c%.CampaignStatus', 'Operator' => '!=', 'Value' => 'Pending Approval', 'Link' => 'AND')
                        )
                    ),
                    'RecipientLists' => FALSE,
                    'RecipientSegments' => FALSE,
                    'Tags' => FALSE,
                    'SplitTests' => FALSE,
                    'Content' => TRUE
        ));
        // Retrieve recent campaigns of user - End }

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserOverview'],
            'UserInformation' => $this->array_user_information,
            'RecentCampaigns' => $campaigns,
            'CurrentMenuItem' => 'Overview'
        );

        $this->render('user/overview', $array_view_data);
    }

    /**
     * Statistics data
     *
     * @return void
     * @author Mert Hurturk
     * */
    function statistics() {
        $statistics = Statistics::RetrieveOverallListActivityOfUser($this->array_user_information['UserID']);

        $ArrayData = array();
        $ArrayData['Series'] = array();
        $ArrayData['Graphs'] = array(
            0 => array('title' => '', 'axis' => 'left'),
            1 => array('title' => '', 'axis' => 'left'),
        );
        $ArrayData['GraphData'] = array();

        for ($TMPCounter = 30; $TMPCounter >= 0; $TMPCounter--) {
            $ArrayData['Series'][] = '<b>' . date('M j', strtotime('-' . $TMPCounter . ' days', time())) . '</b>';
            $ArrayData['GraphData'][0][] = array(
                'Value' => (isset($statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalSubscriptions']) == true ? $statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalSubscriptions'] : 0),
                'Parameters' => array(
                    'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0149'], number_format($statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalSubscriptions'])),
                ),
            );
            $ArrayData['GraphData'][1][] = array(
                'Value' => (isset($statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalUnsubscriptions']) == true ? $statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalUnsubscriptions'] : 0),
                'Parameters' => array(
                    'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($statistics[date('Y-m-d', strtotime('-' . $TMPCounter . ' days', time()))]['TotalUnsubscriptions'])),
                ),
            );
        }

        Core::LoadObject('chart_data_generator');
        $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
        header('Content-type: text/xml');
        print($XML);
        exit;
    }

    //By Eman
    public static function OpenStatistics() {
        //open,click,subscribe
        //1 - open
        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $CurrentDate = date('Y-m-d');
        $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));

        $output = array();

        $UserID = $_POST['user_id'];

        $statistics = Statistics::RetrieveUserOpenStatistics($UserID, $StartDate, $CurrentDate);

        foreach ($statistics as $Row) {

            $exp = explode("-", $Row['StatDate']);
            $dateObj = DateTime::createFromFormat('!m', $exp[1]);
            $monthName = $dateObj->format('M'); // March

            $Row['MonthName'] = $monthName;
            $Row['Month'] = $exp[1];
            $Row['Year'] = $exp[0];

            $output[$Row['StatDate']] = $Row;
        }

        for ($i = 0; $i < $_POST['no_of_months']; $i++) {

            $periodToAdd = " -$i month";
            $dateValue = date('Y-m-d', strtotime($CurrentDate . $periodToAdd));
            $time = strtotime($dateValue);
            $dateString = date('Y', $time) . "-" . date('m', $time);

            $dateObj = DateTime::createFromFormat('!m', date('m', $time));
            $monthName = $dateObj->format('M'); // March

            if (!isset($output[$dateString])) {
                $output[$dateString] = array(
                    'StatDate' => $dateString,
                    'TotalOpens' => 0,
                    'MonthName' => $monthName,
                    'Month' => date('m', $time),
                    'Year' => date('Y', $time),
                );
            }
        }

        // Obtain a list of columns
        foreach ($output as $key => $row) {
            $returnYear[$key] = $output['Year'];
            $one_wayMonth[$key] = $output['Month'];
        }

        // Sort the data with volume descending, edition ascending
        array_multisort($returnYear, SORT_ASC, $one_wayMonth, SORT_ASC, $output);

        $Result = array();
        foreach ($output as $key => $row) {
            $Result[] = $row;
        }

        print_r(json_encode($Result));
        return;
    }

    //By Eman
    public static function SubscribeStatistics() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $CurrentDate = date('Y-m-d');
        $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));

        $output = array();

        $UserID = $_POST['user_id'];

        $statistics = Statistics::RetrieveUserSubscribeStatistics($UserID, $StartDate, $CurrentDate);

        foreach ($statistics as $Row) {

            $exp = explode("-", $Row['StatisticsDate']);
            $dateObj = DateTime::createFromFormat('!m', $exp[1]);
            $monthName = $dateObj->format('M'); // March

            $Row['MonthName'] = $monthName;
            $Row['Month'] = $exp[1];
            $Row['Year'] = $exp[0];

            $output[$Row['StatisticsDate']] = $Row;
        }

        for ($i = 0; $i < $_POST['no_of_months']; $i++) {

            $periodToAdd = " -$i month";
            $dateValue = date('Y-m-d', strtotime($CurrentDate . $periodToAdd));
            $time = strtotime($dateValue);
            $dateString = date('Y', $time) . "-" . date('m', $time);

            $dateObj = DateTime::createFromFormat('!m', date('m', $time));
            $monthName = $dateObj->format('M'); // March

            if (!isset($output[$dateString])) {
                $output[$dateString] = array(
                    'StatisticsDate' => $dateString,
                    'TotalSubscriptions' => 0,
                    'MonthName' => $monthName,
                    'Month' => date('m', $time),
                    'Year' => date('Y', $time),
                );
            }
        }

        // Obtain a list of columns
        foreach ($output as $key => $row) {
            $returnYear[$key] = $output['Year'];
            $one_wayMonth[$key] = $output['Month'];
        }

        // Sort the data with volume descending, edition ascending
        array_multisort($returnYear, SORT_ASC, $one_wayMonth, SORT_ASC, $output);

        $Result = array();
        foreach ($output as $key => $row) {
            $Result[] = $row;
        }

        print_r(json_encode($Result));
        return;
    }

    //By Eman
    public static function ClickStatistics() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $CurrentDate = date('Y-m-d');
        $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));

        $output = array();

        $UserID = $_POST['user_id'];

        $statistics = Statistics::RetrieveUserClickStatistics($UserID, $StartDate, $CurrentDate);

        foreach ($statistics as $Row) {

            $exp = explode("-", $Row['StatDate']);
            $dateObj = DateTime::createFromFormat('!m', $exp[1]);
            $monthName = $dateObj->format('M'); // March

            $Row['MonthName'] = $monthName;
            $Row['Month'] = $exp[1];
            $Row['Year'] = $exp[0];

            $output[$Row['StatDate']] = $Row;
        }

        for ($i = 0; $i < $_POST['no_of_months']; $i++) {

            $periodToAdd = " -$i month";
            $dateValue = date('Y-m-d', strtotime($CurrentDate . $periodToAdd));
            $time = strtotime($dateValue);
            $dateString = date('Y', $time) . "-" . date('m', $time);

            $dateObj = DateTime::createFromFormat('!m', date('m', $time));
            $monthName = $dateObj->format('M'); // March

            if (!isset($output[$dateString])) {
                $output[$dateString] = array(
                    'StatDate' => $dateString,
                    'TotalClicks' => 0,
                    'MonthName' => $monthName,
                    'Month' => date('m', $time),
                    'Year' => date('Y', $time),
                );
            }
        }

        // Obtain a list of columns
        foreach ($output as $key => $row) {
            $returnYear[$key] = $output['Year'];
            $one_wayMonth[$key] = $output['Month'];
        }

        // Sort the data with volume descending, edition ascending
        array_multisort($returnYear, SORT_ASC, $one_wayMonth, SORT_ASC, $output);

        $Result = array();
        foreach ($output as $key => $row) {
            $Result[] = $row;
        }

        print_r(json_encode($Result));
        return;
    }

    //By Eman
    public static function PoepleGeoStatistics() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }

        $UserID = $_POST['user_id'];
        $GetCountryList = GetCountryList();

        $statistics = Statistics::RetrieveUserGeoPeopleStatistics($UserID, $StartDate, $CurrentDate);

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Poeple_Country']] = $Row['People_Count'];
//            $countries[$Row['Poeple_Country']] = $Row['Country_Name'];
            $countries[$Row['Poeple_Country']] = $GetCountryList[strtoupper($Row['Poeple_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

    //By Eman
    public static function OpenGeoStatistics() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }

        $UserID = $_POST['user_id'];
        $GetCountryList = GetCountryList();

        $statistics = Statistics::RetrieveUserGeoOpenStatistics($UserID, $StartDate, $CurrentDate);

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Open_Country']] = $Row['Open_Count'];
//            $countries[$Row['Open_Country']] = $Row['Country_Name'];
            $countries[$Row['Open_Country']] = $GetCountryList[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

    //By Eman
    public static function ClickGeoStatistics() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }
        $output = array();

        $UserID = $_POST['user_id'];
        $GetCountryList = GetCountryList();

        $statistics = Statistics::RetrieveUserGeoClickStatistics($UserID, $StartDate, $CurrentDate);

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Open_Country']] = $Row['Click_Count'];
//            $countries[$Row['Open_Country']] = $Row['Country_Name'];
            $countries[$Row['Open_Country']] = $GetCountryList[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

    //By Eman
    public static function OpenGeoStatisticsNormal() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $CampaignID = $_POST['campaign_id'];
        $CampaignID = isset($CampaignID) && !empty($CampaignID) ? $CampaignID : -1;

        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }

        $UserID = $_POST['user_id'];
        $GetCountryList = GetCountryList();

        $statistics = Statistics::RetrieveUserGeoOpenNormal($CampaignID, $UserID, $StartDate, $CurrentDate);

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Open_Country']] = $Row['Open_Count'];
//            $countries[$Row['Open_Country']] = $Row['Country_Name'];
            $countries[$Row['Open_Country']] = $GetCountryList[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

    //By Eman
    public static function ClickGeoStatisticsNormal() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $CampaignID = $_POST['campaign_id'];
        $CampaignID = isset($CampaignID) && !empty($CampaignID) ? $CampaignID : -1;

        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }
        $output = array();

        $UserID = $_POST['user_id'];
        $GetCountryList = GetCountryList();
        $statistics = Statistics::RetrieveUserGeoClickNormal($CampaignID, $UserID, $StartDate, $CurrentDate);

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Open_Country']] = $Row['Click_Count'];
//            $countries[$Row['Open_Country']] = $Row['Country_Name'];
            $countries[$Row['Open_Country']] = $GetCountryList[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

}
