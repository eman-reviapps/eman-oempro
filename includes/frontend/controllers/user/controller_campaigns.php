<?php

/**
 * Campaigns controller
 *
 * @author Mert Hurturk
 */
class Controller_Campaigns extends MY_Controller {

    /**
     * Constructor
     *
     * @author Cem Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->ArrayUserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Cem Hurturk
     */
    function index() {
        // Decide to go with campaign browse, create or blank state - Start {
        $this->browse();
        // Decide to go with campaign browse, create or blank state - End }	
    }

    /**
     * Archives controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function archives() {
        // Load other modules - Start
        Core::LoadObject('campaigns');
        Core::LoadObject('tags');
        Core::LoadObject('public_archives');
        // Load other modules - End	
        // Events - Start {
        $archive_url = '';
        if ($this->input->post('Command') == 'Generate') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'campaigns.archive.geturl',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => array(
                            'tagid' => $this->input->post('Tag'),
                            'templateurl' => $this->input->post('TemplateURL')
                        )
            ));
            $archive_url = $array_return['URL'];
        }
        // Events - End }
        // Retrieve tags - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'tags.get',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array()
        ));

        if ($array_return['Success'] == false) {
            $tags = array();
        } else {
            $tags = $array_return['Tags'];
        }
        // Retrieve tags - End }
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignPublicArchives'],
            'UserInformation' => $this->ArrayUserInformation,
            'CurrentMenuItem' => 'Campaigns',
            'ArchiveURL' => $archive_url,
            'Tags' => $tags
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/campaigns_public_archives', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Campaign performance comparison controller
     *
     * @author Mert Hurturk
     * */
    function compare($CampaignIDs) {
        // Load other modules - Start
        Core::LoadObject('campaigns');
        // Load other modules - End	
        // User privilege check - Start {
        if (Users::HasPermissions(array('Campaign.Get'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Load chart graph colors from the config file - Start {
        $ArrayChartGraphColors = explode(',', CHART_COLORS);
        // Load chart graph colors from the config file - End }
        // Retrieve campaigns - Start {
        $array_campaign_ids = explode('-', $CampaignIDs);
        $aray_campaigns = array();
        foreach ($array_campaign_ids as $each_campaign_id) {
            $campaign_information = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $each_campaign_id
                            ),
                            array(
                                "Link" => "AND",
                                "Column" => "%c%.CampaignStatus",
                                "Operator" => "=",
                                "Value" => "Sent"
                            ),
                            array(
                                "Link" => "AND",
                                "Column" => "%c%.RelOwnerUserID",
                                "Operator" => "=",
                                "Value" => $this->ArrayUserInformation['UserID']
                            )
                        )
            ));
            if (count($campaign_information) < 1)
                continue;
            $campaign_information = $campaign_information[0];

            $campaign_information['TotalSpamReports'] = Statistics::RetrieveSPAMComplaintCountOfCampaign($campaign_information['CampaignID']);

            $campaign_information['OpenRatio'] = number_format((100 * $campaign_information['UniqueOpens']) / $campaign_information['TotalSent'], 2);
            $campaign_information['ClickRatio'] = number_format((100 * $campaign_information['UniqueClicks']) / $campaign_information['TotalSent'], 2);
            $campaign_information['ForwardRatio'] = number_format((100 * $campaign_information['UniqueForwards']) / $campaign_information['TotalSent'], 2);
            $campaign_information['ViewRatio'] = number_format((100 * $campaign_information['UniqueViewsOnBrowser']) / $campaign_information['TotalSent'], 2);
            $campaign_information['UnsubscriptionRatio'] = number_format((100 * $campaign_information['TotalUnsubscriptions']) / $campaign_information['TotalSent'], 2);
            $campaign_information['BounceRatio'] = number_format((100 * ($campaign_information['TotalHardBounces'] + $campaign_information['TotalSoftBounces'])) / $campaign_information['TotalSent'], 2);
            $campaign_information['SpamRatio'] = ceil((100 * $campaign_information['TotalSpamReports']) / $campaign_information['TotalSent']);
            $array_campaigns[] = $campaign_information;
//            print_r($campaign_information);
//            exit();
        }
        // Retrieve campaigns - End }
        // Filters - Start {
        $Filter_ByCategory_Sent = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Sent'
                ),
                array(
                    'Link' => 'OR',
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Failed'
                )
            ),
        );

        $Filter_ByCategory_Outbox = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Sending'
                ),
                array(
                    'Link' => 'OR',
                    array(
                        'Column' => '%c%.CampaignStatus',
                        'Operator' => '=',
                        'Value' => 'Ready'
                    ),
                    array(
                        'Link' => 'AND',
                        'Column' => '%c%.ScheduleType',
                        'Operator' => '=',
                        'Value' => 'Immediate'
                    ),
                )
            )
        );

        $Filter_ByCategory_Draft = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Draft'
                ),
                array(
                    'Link' => 'OR',
                    array(
                        'Column' => '%c%.CampaignStatus',
                        'Operator' => '=',
                        'Value' => 'Ready'
                    ),
                    array(
                        'Link' => 'AND',
                        'Column' => '%c%.ScheduleType',
                        'Operator' => '=',
                        'Value' => 'Not Scheduled'
                    ),
                )
            ),
        );

        $Filter_ByCategory_Scheduled = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Ready'
                ),
                array(
                    'Link' => 'AND',
                    'Column' => '%c%.ScheduleType',
                    'Operator' => '!=',
                    'Value' => 'Not Scheduled'
                ),
                array(
                    'Link' => 'AND',
                    'Column' => '%c%.ScheduleType',
                    'Operator' => '!=',
                    'Value' => 'Immediate'
                )
            ),
        );

        $Filter_ByCategory_Paused = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                'Column' => '%c%.CampaignStatus',
                'Operator' => '=',
                'Value' => 'Paused'
            ),
        );

        $Filter_ByCategory_PendingApproval = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                'Column' => '%c%.CampaignStatus',
                'Operator' => '=',
                'Value' => 'Pending Approval'
            ),
        );
        // Filters - End }
        // Retrieve total number of campaigns - Start {
        $TotalSent = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Sent
        ));

        $TotalOutbox = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Outbox
        ));

        $TotalDraft = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Draft
        ));

        $TotalScheduled = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Scheduled
        ));

        $TotalPaused = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Paused
        ));

        $TotalPendingApproval = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_PendingApproval
        ));
        // Retrieve total number of campaigns - End }
        // Calculate total pages (pagination) - Start {
        $TotalPages = ceil($TotalBrowsedCampaigns / $RPP);
        // Calculate total pages (pagination) - End }
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignPerformanceComparison'],
            'UserInformation' => $this->ArrayUserInformation,
            'CurrentMenuItem' => 'Campaigns',
            'Campaigns' => $array_campaigns,
            'CampaignIDs' => $CampaignIDs,
            'ChartColors' => $ArrayChartGraphColors,
            'TotalSentCampaigns' => $TotalSent,
            'TotalOutboxCampaigns' => $TotalOutbox,
            'TotalDraftCampaigns' => $TotalDraft,
            'TotalPausedCampaigns' => $TotalPaused,
            'TotalScheduledCampaigns' => $TotalScheduled,
            'TotalPendingApprovalCampaigns' => $TotalPendingApproval,
            'RPP' => 25,
            'ActiveCampaignItem' => 'Compare',
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/campaign_compare', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Campaign statisitcs compare controller
     *
     * @author Mert Hurturk
     * */
    function comparestatisticsCustom() {
        // Load other modules - Start
        Core::LoadObject('campaigns');
        Core::LoadObject('statistics');
        // Load other modules - End	
        // Retrieve campaigns - Start {
        $CampaignIDs = $_POST['campaign_ids'];
        $Statistics = $_POST['statistics'];
        $Days = $_POST['days'];

        $array_campaign_ids = explode('-', $CampaignIDs);
        $aray_campaigns = array();
        foreach ($array_campaign_ids as $each_campaign_id) {
            $campaign_information = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $each_campaign_id
                            )
                        )
            ));
            $campaign_information = $campaign_information[0];
            $array_campaigns[] = $campaign_information;
        }
        // Retrieve campaigns - End }

        $ArrayData = array();
        $LastXDays = ($Days < 7 ? 7 : $Days);

        if ($Statistics == 'opens') {
            foreach ($array_campaigns as $index => $each_campaign) {

                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignOpenStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                            "Criteria" => array(
                                array(
                                    "Column" => "%c%.CampaignID",
                                    "Operator" => "=",
                                    "Value" => $each_campaign['CampaignID']
                                ),
                                array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                            )
                ));

                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
//                    $new_row['Total'] = $row['Total'] ;
                    $new_row['Total'] = (( $row['Total'] / $CampaignInformation['TotalSent'] ) * 100);
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'clicks') {
            foreach ($array_campaigns as $index => $each_campaign) {

                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignClickStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                            "Criteria" => array(
                                array(
                                    "Column" => "%c%.CampaignID",
                                    "Operator" => "=",
                                    "Value" => $each_campaign['CampaignID']
                                ),
                                array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                            )
                ));

                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
//                    $new_row['Total'] = $row['Total'];
                    $new_row['Total'] = (( $row['Total'] / $CampaignInformation['TotalSent'] ) * 100);
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'forwards') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignForwardStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
                    $new_row['Total'] = $row['Total'];
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'views') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignBrowserViewStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
                    $new_row['Total'] = $row['Total'];
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'unsubscriptions') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                            "Criteria" => array(
                                array(
                                    "Column" => "%c%.CampaignID",
                                    "Operator" => "=",
                                    "Value" => $each_campaign['CampaignID']
                                ),
                                array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                            )
                ));

                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
//                    $new_row['Total'] = $row['Unique'];
                    $new_row['Total'] = (( $row['Unique'] / $CampaignInformation['TotalSent'] ) * 100);
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'bounces') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignBounceStatisticsTimeFrame($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
                    $new_row['Total'] = $row['Total'];
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        } elseif ($Statistics == 'spams') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

                $Statistics = Statistics::RetrieveCampaignSpamStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $i = 1;
                foreach ($Statistics as $key => $row) {
                    $exp = explode("-", $key);
                    $day = $exp[2];

                    $new_row['day'] = $i; //$day;
                    $new_row['Total'] = $row['Total'];
                    $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));
                    $new_row['CampaignName'] = $each_campaign['CampaignName'];

                    $ArrayData[$index][] = $new_row;
                    $i++;
                }
            }
        }

        print_r((json_encode($ArrayData)));
        return;
    }

    /**
     * Campaign statisitcs compare controller
     *
     * @author Mert Hurturk
     * */
    function comparestatistics($CampaignIDs, $Statistics, $Days = 7) {
        // Load other modules - Start
        Core::LoadObject('campaigns');
        Core::LoadObject('chart_data_generator');
        Core::LoadObject('statistics');
        // Load other modules - End	
        // User privilege check - Start {
        if (Users::HasPermissions(array('Campaign.Get'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Load chart graph colors from the config file - Start {
        $ArrayChartGraphColors = explode(',', CHART_COLORS);
        // Load chart graph colors from the config file - End }
        // Retrieve campaigns - Start {
        $array_campaign_ids = explode('-', $CampaignIDs);
        $aray_campaigns = array();
        foreach ($array_campaign_ids as $each_campaign_id) {
            $campaign_information = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $each_campaign_id
                            )
                        )
            ));
            $campaign_information = $campaign_information[0];
            $array_campaigns[] = $campaign_information;
        }
        // Retrieve campaigns - End }

        $ArrayData = array();
        $ArrayData['Series'] = array();
        $ArrayData['Graphs'] = array();
        $ArrayData['GraphData'] = array();

        for ($i = 1; $i <= $Days; $i++) {
            $ArrayData['Series'][] = '<b>' . $i . ($i == 1 ? 'st' : ($i == 2 ? 'nd' : ($i == 3 ? 'rd' : 'th'))) . ' day</b>';
        }

        if ($Statistics == 'opens') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignOpenStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0892'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        } elseif ($Statistics == 'clicks') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignClickStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        } elseif ($Statistics == 'forwards') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignForwardStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0900'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        } elseif ($Statistics == 'views') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignBrowserViewStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0915'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        } elseif ($Statistics == 'unsubscriptions') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0917'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        } elseif ($Statistics == 'bounces') {
            foreach ($array_campaigns as $index => $each_campaign) {
                $StartFromDate = date('Y-m-d', strtotime($each_campaign['SendProcessStartedOn']));
                $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($Days - 1) . ' days'));
                $Statistics = Statistics::RetrieveCampaignBounceStatisticsTimeFrame($each_campaign['CampaignID'], $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
                $ArrayData['Graphs'][$index] = array('title' => '', 'axis' => 'left');
                for ($TMPCounter = $Days - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][$index][] = array(
                        'Value' => (isset($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0960'], number_format($Statistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }
        }

        $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
        header('Content-type: text/xml');
        print($XML);
        exit;
    }

    /**
     * Browse campaigns
     *
     * @return void
     * @author Mert Hurturk
     */
    function browse($StartFrom = 1, $RPP = 25, $FilterType = 'ByCategory', $FilterData = 'Sent', $FilterTag = '', $SortField = '', $Order = '') {

        
        // Load other modules - Start
        Core::LoadObject('campaigns');
        // Load other modules - End	
        // User privilege check - Start {
        if (Users::HasPermissions(array('Campaigns.Get'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteCampaign') {
            if (Users::HasPermissions(array('Campaign.Delete'), $this->ArrayUserInformation['GroupInformation']['Permissions'])) {
                $ArrayEventReturn = $this->_EventDeleteCampaign($this->input->post('SelectedCampaigns'), true, true, $StartFrom . '/' . $RPP . '/' . ($FilterType != '' ? $FilterType . '/' . $FilterData : ''));
            }
        } elseif ($this->input->post('Command') == 'AssignTag') {
            $ArrayEventReturn = $this->_EventAssignTag($this->input->post('SelectedCampaigns'), $this->input->post('TagID'));
        }
        // Events - End }
        // Filters - Start {
        $Filter_ByCategory_Sent = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Sent'
                ),
                array(
                    'Link' => 'OR',
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Failed'
                )
            ),
        );

        $Filter_ByCategory_Outbox = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Sending'
                ),
                array(
                    'Link' => 'OR',
                    array(
                        'Column' => '%c%.CampaignStatus',
                        'Operator' => '=',
                        'Value' => 'Ready'
                    ),
                    array(
                        'Link' => 'AND',
                        'Column' => '%c%.ScheduleType',
                        'Operator' => '=',
                        'Value' => 'Immediate'
                    ),
                )
            )
        );

        $Filter_ByCategory_Draft = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Draft'
                ),
                array(
                    'Link' => 'OR',
                    array(
                        'Column' => '%c%.CampaignStatus',
                        'Operator' => '=',
                        'Value' => 'Ready'
                    ),
                    array(
                        'Link' => 'AND',
                        'Column' => '%c%.ScheduleType',
                        'Operator' => '=',
                        'Value' => 'Not Scheduled'
                    ),
                )
            ),
        );

        $Filter_ByCategory_Scheduled = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                array(
                    'Column' => '%c%.CampaignStatus',
                    'Operator' => '=',
                    'Value' => 'Ready'
                ),
                array(
                    'Link' => 'AND',
                    'Column' => '%c%.ScheduleType',
                    'Operator' => '!=',
                    'Value' => 'Not Scheduled'
                ),
                array(
                    'Link' => 'AND',
                    'Column' => '%c%.ScheduleType',
                    'Operator' => '!=',
                    'Value' => 'Immediate'
                )
            ),
        );

        $Filter_ByCategory_Paused = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                'Column' => '%c%.CampaignStatus',
                'Operator' => '=',
                'Value' => 'Paused'
            ),
        );

        $Filter_ByCategory_PendingApproval = array(
            array(
                'Column' => '%c%.RelOwnerUserID',
                'Operator' => '=',
                'Value' => $this->ArrayUserInformation['UserID']
            ),
            array(
                'Link' => 'AND',
                'Column' => '%c%.CampaignStatus',
                'Operator' => '=',
                'Value' => 'Pending Approval'
            ),
        );
        // Filters - End }
        // Apply filtering and row ordering - Start {
        $CampaignStatusCriteria = $Filter_ByCategory_Sent;
        $CampaignRowOrder = array(
            'Column' => $SortField == '' ? 'SendProcessFinishedOn' : $SortField,
            'Type' => $Order == '' ? 'DESC' : $Order,
        );
        if (($FilterType == 'ByCategory') && ($FilterData == 'Outbox')) {
            $CampaignStatusCriteria = $Filter_ByCategory_Outbox;
            $CampaignRowOrder = array(
                'Column' => $SortField == '' ? 'CampaignName' : $SortField,
                'Type' => $Order == '' ? 'ASC' : $Order,
            );
        } elseif (($FilterType == 'ByCategory') && ($FilterData == 'Draft')) {
            $CampaignStatusCriteria = $Filter_ByCategory_Draft;
            $CampaignRowOrder = array(
                'Column' => $SortField == '' ? 'CreateDateTime' : $SortField,
                'Type' => $Order == '' ? 'DESC' : $Order,
            );
        } elseif (($FilterType == 'ByCategory') && ($FilterData == 'Scheduled')) {
            $CampaignStatusCriteria = $Filter_ByCategory_Scheduled;
            $CampaignRowOrder = array(
                'Column' => $SortField == '' ? 'SendDate' : $SortField,
                'Type' => $Order == '' ? 'ASC' : $Order,
            );
        } elseif (($FilterType == 'ByCategory') && ($FilterData == 'Paused')) {
            $CampaignStatusCriteria = $Filter_ByCategory_Paused;
            $CampaignRowOrder = array(
                'Column' => $SortField == '' ? 'CampaignName' : $SortField,
                'Type' => $Order == '' ? 'ASC' : $Order,
            );
        } elseif (($FilterType == 'ByCategory') && ($FilterData == 'PendingApproval')) {
            $CampaignStatusCriteria = $Filter_ByCategory_PendingApproval;
            $CampaignRowOrder = array(
                'Column' => $SortField == '' ? 'CampaignName' : $SortField,
                'Type' => $Order == '' ? 'ASC' : $Order,
            );
        }

        $ArrayFilterTags = array();
        if ($FilterTag !== '' && $FilterTag != -1) {
            $ArrayFilterTags = explode(':', $FilterTag);
        }
        // Apply filtering and row ordering - End }
        // Retrieve campaigns - Start {
        $Campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Criteria' => $CampaignStatusCriteria,
                    'TagFilter' => $ArrayFilterTags,
                    'RowOrder' => $CampaignRowOrder,
                    'SplitTest' => true,
                    'Content' => TRUE,
                    'Pagination' => array(
                        'Offset' => ($StartFrom <= 0 ? 0 : ($StartFrom - 1) * $RPP),
                        'Rows' => $RPP
                    )
        ));
        $TotalBrowsedCampaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $CampaignStatusCriteria,
                    'TagFilter' => $ArrayFilterTags,
                    'RowOrder' => $CampaignRowOrder
        ));
        // Retrieve campaigns - End }
        // Retrieve tags - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'tags.get',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array()
        ));

        if ($ArrayReturn->Success == false) {
            // Error occurred
        } else {
            $Tags = $ArrayReturn->Tags;
        }
        // Retrieve tags - End }
        // Retrieve total number of campaigns - Start {
        $TotalSent = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Sent
        ));

        $TotalOutbox = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Outbox
        ));

        $TotalDraft = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Draft
        ));

        $TotalScheduled = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Scheduled
        ));

        $TotalPaused = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_Paused
        ));

        $TotalPendingApproval = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'ReturnTotalRows' => true,
                    'Criteria' => $Filter_ByCategory_PendingApproval
        ));
        // Retrieve total number of campaigns - End }
        // Calculate total pages (pagination) - Start {
        $TotalPages = ceil($TotalBrowsedCampaigns / $RPP);
        // Calculate total pages (pagination) - End }
        // Interface parsing - Start {
        $CampaignsArr = array();
        foreach ($Campaigns as $Campaign) {
            $Campaign['opened_ratio'] = round((100 * $Campaign['TotalOpens']) / $Campaign['TotalSent'], 1);
            $Campaign['clicked_ratio'] = round((100 * $Campaign['TotalClicks']) / $Campaign['TotalSent'], 1);
            $Campaign['forwarded_ratio'] = round((100 * $Campaign['TotalForwards']) / $Campaign['TotalSent'], 1);
            $Campaign['browser_views_ratio'] = round((100 * $Campaign['TotalViewsOnBrowser']) / $Campaign['TotalSent'], 1);
            $Campaign['unsubscription_ratio'] = round((100 * $Campaign['TotalUnsubscriptions']) / $Campaign['TotalSent'], 1);
            $Campaign['spam_ratio'] = round((100 * $Campaign['TotalSpamReports']) / $Campaign['TotalSent'], 1);
            $Campaign['bounce_ratio'] = round((100 * $Campaign['TotalBounces']) / $Campaign['TotalSent'], 1);
            $Campaign['hard_bounce_ratio'] = round((100 * $Campaign['TotalHardBounces']) / $Campaign['TotalSent'], 1);
            $Campaign['not_opened_ratio'] = 100 - ($Campaign['opened_ratio'] + $Campaign['hard_bounce_ratio']);
            $CampaignsArr[] = $Campaign;
        }
        
//        Core::LoadObject('spark');
//        $CampaignsMetrics = array();
//        $result = Spark::deliverabilityByCampaign('2017-02-01T00:00', null);
//
//        if($result[0])
//        {
//            $CampaignsMetrics = $result[1];
//        }
        
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseCampaigns'],
            'UserInformation' => $this->ArrayUserInformation,
            'Tags' => $Tags,
            'Campaigns' => $CampaignsArr,
            'TotalPages' => $TotalPages,
            'TotalSentCampaigns' => $TotalSent,
            'TotalOutboxCampaigns' => $TotalOutbox,
            'TotalDraftCampaigns' => $TotalDraft,
            'TotalPausedCampaigns' => $TotalPaused,
            'TotalScheduledCampaigns' => $TotalScheduled,
            'TotalPendingApprovalCampaigns' => $TotalPendingApproval,
            'CurrentMenuItem' => 'Campaigns',
            'CurrentPage' => $StartFrom,
            'RPP' => $RPP,
            'FilterType' => $FilterType,
            'FilterData' => $FilterData,
            'FilterTags' => count($ArrayFilterTags) < 1 ? array() : $ArrayFilterTags,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        if (isset($ArrayEventReturn)) {
            foreach ($ArrayEventReturn as $Key => $Value) {
                $ArrayViewData[$Key] = $Value;
            }
        }

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($ArrayEventReturn) == true) {
            $ArrayViewData[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
        }

        $this->render('user/campaigns', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Preview campaign controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function preview($CampaignID, $Type = 'html', $Command = '') {
        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('subscribers');
        Core::LoadObject('lists');
        Core::LoadObject('emails');
        Core::LoadObject('campaigns');
        Core::LoadObject('personalization');
        // Load other modules - End
        // Retrieve campaign information - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.get',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'campaignid' => $CampaignID
                    )
        ));
        $CampaignInformation = $ArrayReturn['Campaign'];
        unset($ArrayReturn);
        // Retrieve campaign information - End }

        if ($Command != '' && $Command == 'source') {
            $ArrayReturn = API::call(array(
                        'format' => 'array',
                        'command' => 'email.get',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => array(
                            'emailid' => $CampaignInformation['RelEmailID']
                        )
            ));
            $EmailInformation = $ArrayReturn['EmailInformation'];
            unset($ArrayReturn);

            // Select one of the recipient lists from the campaign - Start
            $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $CampaignInformation['RecipientLists'][0]), false);
            // Select one of the recipient lists from the campaign - End
            // Retrieve a random subscriber from the target list - Start
            $ArrayRandomSubscriber = Subscribers::SelectRandomSubscriber($ArrayList['ListID'], true);
            // Retrieve a random subscriber from the target list - End

            if ($Type == 'html') {
                if ($EmailInformation['FetchURL'] != '') {
                    $ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($EmailInformation['FetchURL'], array('Subscriber', 'User'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), false));
                } else {
                    $ShowContent = $EmailInformation['HTMLContent'];
                }

                // Plug-in hook - Start
                $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $ShowContent, '', $ArrayRandomSubscriber));
                $ShowContent = $ArrayPlugInReturnVars[1];
                // Plug-in hook - End
                // Plug-in hook - Start
                $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $ShowContent, '', $ArrayRandomSubscriber, 'Browser Preview'));
                $ShowContent = $ArrayPlugInReturnVars[1];
                // Plug-in hook - End
            } else {
                if ($EmailInformation['FetchPlainURL'] != '') {
                    $ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($EmailInformation['FetchPlainURL'], array('Subscriber', 'User'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), false));
                } else {
                    $ShowContent = $EmailInformation['PlainContent'];
                }

                // Plug-in hook - Start
                $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', '', $ShowContent, $ArrayRandomSubscriber));
                $ShowContent = $ArrayPlugInReturnVars[2];
                // Plug-in hook - End
                // Plug-in hook - Start
                $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', '', $ShowContent, $ArrayRandomSubscriber, 'Browser Preview'));
                $ShowContent = $ArrayPlugInReturnVars[2];
                // Plug-in hook - End
            }

            // Add header/footer to the email (if exists in the user group) - Start {
            $ArrayReturn = Personalization::AddEmailHeaderFooter(($Type == 'plain' ? $ShowContent : ''), ($Type == 'html' ? $ShowContent : ''), $this->ArrayUserInformation['GroupInformation']);
            if ($Type == 'plain') {
                $ShowContent = $ArrayReturn[0];
            } else {
                $ShowContent = $ArrayReturn[1];
            }
            // Add header/footer to the email (if exists in the user group) - End }
            // Perform personalization (disable personalization and links if subscriber information not provided) - Start
            $ShowContent = Personalization::Personalize($ShowContent, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), true);
            // Perform personalization (disable personalization and links if subscriber information not provided) - End

            if ($Type == 'plain') {
                $ShowContent = '<pre>' . $ShowContent . '</pre>';
            }

            print $ShowContent;
            exit;
        }

        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseCampaigns'],
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'Mode' => $Type,
            'Flow' => $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'],
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/campaign_preview', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Delete campaign event
     *
     * @return void
     * @author Mert Hurturk
     */
    function _EventDeleteCampaign($ArrayCampaignIDs, $RedirectOnSuccess = true, $PerformFormValidation = true, $RedirectURI = '') {
        if ($PerformFormValidation == true) {
            // Field validations - Start {
            $ArrayFormRules = array(
                array
                    (
                    'field' => 'SelectedCampaigns[]',
                    'label' => 'campaigns',
                    'rules' => 'required',
                ),
            );

            $this->form_validation->set_rules($ArrayFormRules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0738']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }

        // Delete campaigns - Start {
        $ArrayAPIVars = array(
            'campaigns' => implode(',', $ArrayCampaignIDs),
        );
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'campaigns.delete',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0738']);
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            if ($RedirectOnSuccess == true) {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0739']);

                $this->load->helper('url');
                redirect(InterfaceAppURL(true) . '/user/campaigns/browse/' . $RedirectURI, 'location', '302');
            } else {
                return true;
            }
        }
        // Delete campaigns - End }
    }

    /**
     * Assign tag event
     *
     * @return void
     * @author Mert Hurturk
     */
    function _EventAssignTag($ArrayCampaignIDs, $TagID) {
        if ($PerformFormValidation == true) {
            // Field validations - Start {
            $ArrayFormRules = array(
                array
                    (
                    'field' => 'SelectedCampaigns[]',
                    'label' => 'campaigns',
                    'rules' => 'required',
                ),
            );

            $this->form_validation->set_rules($ArrayFormRules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0738']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }

        // Assign tags - Start {
        $ArrayAPIVars = array(
            'campaignids' => implode(',', $ArrayCampaignIDs),
            'tagid' => $TagID,
        );
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'tag.assigntocampaigns',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            return array(false, '');
        } else {
            // API Success
            if ($RedirectOnSuccess == true) {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0955']);

                $this->load->helper('url');
                redirect(InterfaceAppURL(true) . '/user/campaigns/browse/', 'location', '302');
            } else {
                return true;
            }
        }
        // Assign tags - End }
    }

}

// end of class User
?>