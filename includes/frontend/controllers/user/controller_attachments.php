<?php
/**
 * Attachments controller
 *
 * @author Mert Hurturk
 */

class Controller_Attachments extends MY_Controller
{
/**
 * Constructor
 *
 * @author Mert Hurturk
 */
function __construct()
	{
	parent::__construct();

	// Load other modules - Start
	Core::LoadObject('user_auth');
	Core::LoadObject('attachments');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
	// Check the login session, redirect based on the login session status - End
	}

/**
 * View controller
 *
 * @return void
 * @author Mert Hurturk
 **/
function view($AttachmentID)
	{
	if (!isset($AttachmentID) || $AttachmentID == '') exit;
	
	// Retrieve user information - Start {
	$ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
	// Retrieve user information - End }

	// Retrieve attachment information - Start {
	$ArrayAttachmentInformation = Attachments::RetrieveAttachment(array('*'), array('MD5(`AttachmentID`)'=>$AttachmentID));
	// Retrieve attachment information - End }
	
	if ($ArrayAttachmentInformation == false) exit;
	
	$FilePath = DATA_PATH."attachments/".md5($ArrayAttachmentInformation['AttachmentID']);
	Core::SendDownloadHeaders($ArrayAttachmentInformation['FileName'], filesize($FilePath), $ArrayAttachmentInformation['FileMimeType']);
	if (file_exists($FilePath))
		{
		$FileHandler = fopen($FilePath, "rb");
		while (!feof($FileHandler))
			{
			print fread($FileHandler, 8192);
			}
		fclose($FileHandler);
		}
	exit;
	}

/**
 * Upload controller
 *
 * @return void
 * @author Mert Hurturk
 */
function upload()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		// This feature is disabled in demo version
		$Attachment = false;
		$Script = "<script>parent.content_library.add_attachment_error();</script>";
		print $Script;
		exit;
		}

	if (count($_FILES) < 1)
		{
		$Script = "<script>parent.content_library.add_attachment_error();</script>";
		print $Script;
		exit;
		}

	// Retrieve user information - Start {
	$ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
	// Retrieve user information - End }

	$MemoryLimit = ini_get('memory_limit');
	$MemoryLimit_type = strtolower(substr($MemoryLimit, strlen($MemoryLimit)-1));
	$MemoryLimit = substr($MemoryLimit, 0, strlen($MemoryLimit)-1);
	$MemoryLimit = ($MemoryLimit_type == "m" ? $MemoryLimit*(1024*1024) : ($MemoryLimit_type == "k" ? $MemoryLimit*1024 : $MemoryLimit ));

	$PostMaxSize = ini_get('post_max_size');
	$PostMaxSize_type = strtolower(substr($PostMaxSize, strlen($PostMaxSize)-1));
	$PostMaxSize = substr($PostMaxSize, 0, strlen($PostMaxSize)-1);
	$PostMaxSize = ($PostMaxSize_type == "m" ? $PostMaxSize*(1024*1024) : ($PostMaxSize_type == "k" ? $PostMaxSize*1024 : $PostMaxSize ));

	$MinimumSize = min($PostMaxSize, $MemoryLimit);

	if ($_FILES['AttachmentFile']['size'] > $MinimumSize)
		{
		$Script = "<script>parent.content_library.add_attachment_error();</script>";
		}
	else
		{
		$ArrayAttachment = Attachments::Upload(($this->input->post('RelEmailID') !== FALSE ? $this->input->post('RelEmailID') : 0), $ArrayUserInformation['UserID'], $_FILES['AttachmentFile']);
		}

	if ($ArrayAttachment === false)
		{
		$Script = "<script>parent.content_library.add_attachment_error();</script>";
		}
	else
		{
		$Script = "<script>parent.content_library.add_attachment_element({ AttachmentID: '".$ArrayAttachment[0]."', AttachmentMD5ID: '".$ArrayAttachment[1]."', FileName: '".$_FILES['AttachmentFile']['name']."', FileMimeType: '".$_FILES['AttachmentFile']['type']."', FileSize: '".$_FILES['AttachmentFile']['size']."' });</script>";	
		}

	print $Script;
	}

} // end of class User

?>