<?php

/**
 * Search controller
 *
 * @author Mert Hurturk
 */
class Controller_Search extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('clients');
        Core::LoadObject('api');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Mert Hurturk
     */
    function index() {
        // Events - Start {
        // Events - End }
        // Search lists - Start {
//        print_r($_POST);
//        print_r($this->input->post('search-keyword-input'));
//        exit();
        $array_lists = Lists::RetrieveLists(array('*'), 'RelOwnerUserID = ' . $this->array_user_information['UserID'] . ' AND Name LIKE "%' . $this->input->post('search-keyword-input') . '%"', array('Name' => 'ASC'), array(), 5);
        // Search lists - End }
        // Search campaigns - Start {
        $array_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC'),
                    'Criteria' => array(
                        array('Column' => '%c%.CampaignName', 'Operator' => 'LIKE', 'Value' => '%' . $this->input->post('search-keyword-input') . '%'),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID'], 'Link' => 'AND')
                    )
        ));
        // Search campaigns - End }
        // Search subscribers - Start {
        $array_all_lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'));

        $array_subscribers = array();
        foreach ($array_all_lists as $each_list) {
            $array_found_subscribers = Subscribers::Search($this->input->post('search-keyword-input'), 'EmailAddress', $each_list['ListID'], 'Active', array('EmailAddress' => 'ASC'), 3);

            if (count($array_found_subscribers) > 0) {
                foreach ($array_found_subscribers as &$each) {
                    $each['ListID'] = $each_list['ListID'];
                    $each['ListName'] = $each_list['Name'];
                    $array_subscribers[] = $each;
                }
            }
        }
        // Search subscribers - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'Lists' => $array_lists == false ? array() : $array_lists,
            'Campaigns' => $array_campaigns == false ? array() : $array_campaigns,
            'Subscribers' => $array_subscribers
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/search', $ArrayViewData);
        // Interface parsing - End
    }

}

// end of class User
?>