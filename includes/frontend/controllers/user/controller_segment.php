<?php

/**
 * Customfields Controller
 */
class Controller_Segment extends MY_Controller {

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('segments');
        Core::LoadObject('custom_fields');
        Core::LoadObject('lists');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->arrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
    }

    /**
     * Create segment controller method
     * @author Mert Hurturk
     * */
    function create($listId) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Segment.Create'), $this->arrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $arrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->arrayUserInformation['Username'],
                    'password' => $this->arrayUserInformation['Password'],
                    'parameters' => array(
                        'listid' => $listId
                    )
        ));

        if ($arrayReturn['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arrayListInformation = $arrayReturn['List'];
        // Retrieve list information - End }
        // Retrieve custom fields - Start {
        $arrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->arrayUserInformation['Username'],
                    'password' => $this->arrayUserInformation['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $listId,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($arrayReturn['Success'] == false) {
            // Error occurred
        } else {
            $arrayCustomFields = $arrayReturn['CustomFields'];
            $totalCustomFieldCount = $arrayReturn['TotalCustomFields'] == NULL ? 0 : $arrayReturn['TotalCustomFields'];
            unset($arrayReturn);
        }
        // Retrieve custom fields - End }
        // Retrieve campaigns - Start {
        Core::LoadObject('campaigns');
        $arrayCampaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC'),
                    'Criteria' => array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->arrayUserInformation['UserID']),
                    'Content' => false,
                    'SplitTests' => false
        ));
        // Retrieve campaigns - End }
        // Events - Start {
        if ($this->input->post('Command') == 'CreateSegment') {
            $this->_eventCreateSegment($listId);
        }
        
        // Events - End }
        // Interface parsing - Start {
        $arrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateSegment'],
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Segments',
            'UserInformation' => $this->arrayUserInformation,
            'ListInformation' => $arrayListInformation,
            'SegmentInformation' => array('SegmentName' => '', 'SegmentOperator' => 'and', 'SegmentRules' => ''),
            'DefaultFields' => Segments::$RuleDefaultFields,
            'RuleOperators' => Segments::$RuleOperators,
            'ActivityFields' => Segments::$RuleActivityFields,
            'CustomFields' => $arrayCustomFields,
            'Campaigns' => $arrayCampaigns,
            'IsEditEvent' => false
        );

        $arrayViewData = array_merge($arrayViewData, InterfaceDefaultValues());

        $this->render('user/segment_create', $arrayViewData);
        // Interface parsing - End }
    }

    /**
     * Edit segment controller method
     * @author Mert Hurturk
     * */
    function edit($listId, $segmentId) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Segment.Update'), $this->arrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $arrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->arrayUserInformation['Username'],
                    'password' => $this->arrayUserInformation['Password'],
                    'parameters' => array(
                        'listid' => $listId
                    )
        ));

        if ($arrayReturn['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arrayListInformation = $arrayReturn['List'];
        // Retrieve list information - End }
        // Retrieve segment information - Start {
        $arraySegmentInformation = Segments::RetrieveSegment(array('*'), array('RelOwnerUserID' => $this->arrayUserInformation['UserID'], 'RelListID' => $listId, 'SegmentID' => $segmentId));
        if (!$arraySegmentInformation) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1417'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1418']);
            return;
        }
        // Retrieve segment information - End }
        // Retrieve custom fields - Start {
        $arrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->arrayUserInformation['Username'],
                    'password' => $this->arrayUserInformation['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $listId,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($arrayReturn['Success'] == false) {
            // Error occurred
        } else {
            $arrayCustomFields = $arrayReturn['CustomFields'] ? $arrayReturn['CustomFields'] : array();
            $totalCustomFieldCount = $arrayReturn['TotalCustomFields'] == NULL ? 0 : $arrayReturn['TotalCustomFields'];
            unset($arrayReturn);
        }
        // Retrieve custom fields - End }
        // Retrieve campaigns - Start {
        Core::LoadObject('campaigns');
        $arrayCampaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC'),
                    'Criteria' => array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->arrayUserInformation['UserID']),
                    'Content' => false,
                    'SplitTests' => false
        ));
        // Retrieve campaigns - End }
        // Events - Start {
        if ($this->input->post('Command') == 'EditSegment') {
            $this->_eventEditSegment($listId, $segmentId);
        }
        // Events - End }
        // Interface parsing - Start {
        $arrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEditSegment'],
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Segments',
            'UserInformation' => $this->arrayUserInformation,
            'ListInformation' => $arrayListInformation,
            'SegmentInformation' => $arraySegmentInformation,
            'DefaultFields' => Segments::$RuleDefaultFields,
            'RuleOperators' => Segments::$RuleOperators,
            'ActivityFields' => Segments::$RuleActivityFields,
            'CustomFields' => $arrayCustomFields,
            'Campaigns' => $arrayCampaigns,
            'IsEditEvent' => true
        );

        $arrayViewData = array_merge($arrayViewData, InterfaceDefaultValues());

        $this->render('user/segment_create', $arrayViewData);
        // Interface parsing - End }
    }

    /**
     * Create segment function
     * @author Mert Hurturk
     * */
    function _eventCreateSegment($listId) {
        // Field validations - Start {
        $this->form_validation->set_rules("SegmentName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0757'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create segment - Start {
        Core::LoadObject('segments');

        Segments::Create(array(
            'RelOwnerUserID' => $this->arrayUserInformation['UserID'],
            'RelListID' => $listId,
            'SegmentName' => $this->input->post('SegmentName'),
            'SegmentOperator' => $this->input->post('SegmentOperator'),
            'SegmentRules' => urldecode($this->input->post('SegmentRules'))
        ));
        // Create segment - End }

        $this->load->helper('url');
        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1305']);
        redirect(InterfaceAppURL(true) . '/user/segments/browse/' . $listId, 'location', '302');
    }

    /**
     * Create segment function
     * @author Mert Hurturk
     * */
    function _eventEditSegment($listId, $segmentId) {
        // Field validations - Start {
        $this->form_validation->set_rules("SegmentName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0757'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create segment - Start {
        Segments::Update(array(
            'SegmentName' => $this->input->post('SegmentName'),
            'SegmentOperator' => $this->input->post('SegmentOperator'),
            'SegmentRules' => $this->input->post('SegmentRules'),
            'SubscriberCountLastCalculatedOn' => date('Y-m-d H:i:s', strtotime('-3 days'))
                ), array(
            'SegmentID' => $segmentId,
            'RelOwnerUserID' => $this->arrayUserInformation['UserID']
        ));
        // Create segment - End }

        $this->load->helper('url');
        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1420']);
        redirect(InterfaceAppURL(true) . '/user/segments/browse/' . $listId, 'location', '302');
    }

}
