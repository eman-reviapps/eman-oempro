<?php

/**
 * Lists controller
 *
 * @author Mert Hurturk
 */
class Controller_Lists extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('statistics');
        Core::LoadObject('suppression_list');
        Core::LoadObject('lists');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    function index() {
        $this->browse();
    }

    /**
     * Browse controller
     *
     * @author Mert Hurturk
     * */
    function browse() {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Lists.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
//        Core::LoadObject('aws_s3');
//        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
//        $bucket1 = $ObjectS3->getBucket(S3_BUCKET);
//        $bucket2 = $ObjectS3->getBucket(S3_BUCKET, "screenshots");
//        $bucket3 = $ObjectS3->getBucket(S3_BUCKET, "screenshots/1/", null, null, "/");
//        $bucket4 = $ObjectS3->getBucket(S3_BUCKET, "screenshots/2/", null, null, "/");
//
//        print_r("<pre>");
//        print_r("hi1 <br/>");
//        print_r($bucket1);
//        print_r("hi22 <br/>");
//        print_r($bucket2);
//        print_r("hi3 <br/>");
//        print_r($bucket3);
//        print_r("hi4 <br/>");
//        print_r($bucket4);
//        print_r("</pre>");
//
//        exit();
//
//        $MediaData = file_get_contents("https://support.flyinglist.com/img/home.png");
//        $file = new O_Domain_MediaLibraryFile();
//        $file->setUserId($this->array_user_information['UserID']);
//        $file->setFolderId($this->input->post('ParentFolderID'));
//        $file->setType('image/png');
//
//        $mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
//        $mediaLibraryFileMapper->insert($file);
//
//        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
////        $ObjectS3->putObjectString($MediaData, S3_BUCKET, S3_MEDIALIBRARY_PATH . '/' . ($file->getUserId()) . '/' . (uniqid()), S3::ACL_PUBLIC_READ, array(), $file->getType());
//        print_r(AWS_END_POINT . "/" . S3_BUCKET . "/" . S3_MEDIALIBRARY_PATH . '/' . ($file->getUserId()) . "/40");
//        exit();
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteLists') {
            if (Users::HasPermissions(array('List.Delete'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_delete_lists($this->input->post('SelectedLists'));
            }
        }
        // Events - End }

        $global_suppression_total = SuppressionList::GetTotal($this->array_user_information['UserID'], 0);

        // Retrieve lists - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            // Error occurred
        } else {
            $all_lists = $array_return['Lists'];
            $array_lists = array();
            foreach ($all_lists as $list) {
                $array_list_information = Lists::RetrieveListCustom(array('*'), array('ListID' => $list['ListID'], 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('bounce', 'unsubscribes', 'open', 'click', 'forward', 'spam'));

                $list['list_information'] = $array_list_information;
                $array_lists[] = $list;
            }
            $total_list_count = $array_return['TotalListCount'];
            unset($array_return);
        }
        // Retrieve lists - End }
//        print_r('<pre>');
//        print_r($array_lists);
//        print_r('</pre>');
//        exit();
        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($this->array_user_information['UserID']);

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseLists'],
            'UserInformation' => $this->array_user_information,
            'Lists' => $array_lists,
            'CurrentMenuItem' => 'Lists',
            'GlobalSuppressionTotal' => $global_suppression_total,
            'TotalSubscribersOnTheAccount' => $TotalSubscribersOnTheAccount,
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        if (isset($array_event_return)) {
            foreach ($array_event_return as $key => $value) {
                $array_view_data[$key] = $value;
            }
        }

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        $this->render('user/lists', $array_view_data);
    }

    /**
     * Delete list event
     *
     * @author Mert Hurturk
     */
    function _event_delete_lists($array_list_ids, $redirect_on_success = true, $perform_form_validation = true) {
        if ($perform_form_validation == true) {
            // Field validations - Start {
            $array_form_rules = array(
                array
                    (
                    'field' => 'SelectedLists[]',
                    'label' => 'lists',
                    'rules' => 'required',
                ),
            );

            $this->form_validation->set_rules($array_form_rules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0936']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }

        $array_deleted = array();
        $array_failed = array();

//        echo count($array_list_ids);
//        exit();
        foreach ($array_list_ids as $list_id) {

            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'list.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'listid' => $list_id
                        )
            ));
            if ($array_return['Success'] == false) {
                $array_failed[] = array("ListID" => $list_id, "Name" => '', "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['9283']);
                continue;
            }
            $array_list_information = $array_return['List'];

            if ($array_list_information['IsAutomationList'] == 'Yes') {
                $array_failed[] = array("ListID" => $list_id, "Name" => $array_list_information['Name'], "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['9284']);
                continue;
            }

            if ($array_list_information['SubscriberCount'] > 0) {
                $array_failed[] = array("ListID" => $list_id, "Name" => $array_list_information['Name'], "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['9285']);
                continue;
            }

            $list_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                        'RelOwnerUserID' => $this->array_user_information['UserID'],
                        'RelListID' => $list_id
                            ), 5, 0);

            if ($list_suppress) {
                $array_failed[] = array("ListID" => $list_id, "Name" => $array_list_information['Name'], "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['9286']);
                continue;
            }

            $array_return = API::call(array(
                        'format' => 'object',
                        'command' => 'lists.delete',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'lists' => $list_id
                        )
            ));

            if ($array_return->Success == false) {
                // API Error occurred
                $ErrorCode = (strtolower(gettype($array_return->ErrorCode)) == 'array' ? $array_return->ErrorCode[0] : $array_return->ErrorCode);
                if ($ErrorCode == '1') {
                    $array_failed[] = array("ListID" => $list_id, "Name" => $array_list_information['Name'], "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['0936']);
                } else {
                    $array_failed[] = array("ListID" => $list_id, "Name" => $array_list_information['Name'], "Message" => ApplicationHeader::$ArrayLanguageStrings['Screen']['9287']);
                }
                continue;
            }
            $array_deleted[] = array("ListID" => $list_id, "Name" => $array_list_information['Name']);
        }
        echo count($array_list_ids);
        //if faild to delete list - one list
        if (count($array_list_ids) == 1 && count($array_failed) == 1 && count($array_deleted) == 0) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9287'] . " <span class='bold'>" . $array_failed[0]['Name'] . "</span> " . $array_failed[0]['Message']);
        }

        //prepare msg text to be shown to user
        $msg = "";
        if (count($array_failed) > 0) {
            $msg .= ApplicationHeader::$ArrayLanguageStrings['Screen']['0096'] . " <br/>";
            foreach ($array_failed as $row) {
                $msg .= "<span style='padding-left:20px'>List : <span class='bold'>" . $row['Name'] . "</span> , " . $row['Message'] . "</span><br/>";
            }
        }
        if (count($array_deleted) > 0) {
            $msg .= ApplicationHeader::$ArrayLanguageStrings['Screen']['9288'] . " <br/>";
            foreach ($array_deleted as $row) {
                $msg .= "<span style='padding-left:20px'>List : <span class='bold'>" . $row['Name'] . "</span>  " . "</span><br/>";
            }
        }

        if (count($array_list_ids) == count($array_failed)) {
            return array(false, $msg);
        }

        if ($redirect_on_success == true) {
            $_SESSION['PageMessageCache'] = array('Success', $msg);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/lists/browse/', 'location', '302');
        } else {
            return true;
        }
    }

}

?>