<?php

/**
 * Subscribers Controller
 *
 * @package default
 * @author Mert Hurturk
 * */
class Controller_Subscribers extends MY_Controller {

    /**
     * Available flows for creating email
     *
     * @var array
     */
    private $import_flows = array('form', 'copyandpaste', 'fromfile', 'fromdb', 'highrise');

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('lists');
        Core::LoadObject('subscribers');
        Core::LoadObject('segments');
        Core::LoadObject('custom_fields');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Add controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function add($list_id, $flow = '', $step_from_url = 0) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Import'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        if (AccountIsUnTrustedCheck($this->array_user_information)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        // User privilege check - End }
        // Argument validation - Start {
        if ($list_id == '' || $list_id == 0)
            throw new Exception('You need to pass list id');

        $List = Lists::RetrieveList(array('*'), array('ListID' => $list_id));
        if ($List['IsAutomationList'] == 'Yes') {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9057']);
            exit;
        }

        if ($flow != '' && in_array($flow, $this->import_flows) == false)
            throw new Exception('Not a valid flow');
        // Argument validation - End }

        Core::LoadObject('wizard');

        // Declare steps - Start {
        $step_field_mapping = new Step('Field mapping', array('Subscriber_import_wizard_functions', 'step_pre_field_mapping'), array('Subscriber_import_wizard_functions', 'step_post_field_mapping'));
        $step_field_mapping->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1277']);

        $step_results = new Step('Results', array('Subscriber_import_wizard_functions', 'step_pre_result'), array('Subscriber_import_wizard_functions', 'step_post_result'));
        $step_results->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1278']);

        $step_data_entry = new Step('Data entry', array('Subscriber_import_wizard_functions', 'step_pre_data_entry'), array('Subscriber_import_wizard_functions', 'step_post_data_entry'));
        $step_data_entry->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1276']);

        $step_file_upload = new Step('File upload', array('Subscriber_import_wizard_functions', 'step_pre_file_upload'), array('Subscriber_import_wizard_functions', 'step_post_file_upload'));
        $step_file_upload->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1279']);

        $step_mysql_settings = new Step('Mysql information', array('Subscriber_import_wizard_functions', 'step_pre_mysql'), array('Subscriber_import_wizard_functions', 'step_post_mysql'));
        $step_mysql_settings->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1280']);

        $step_form = new Step('Form', array('Subscriber_import_wizard_functions', 'step_pre_form'), array('Subscriber_import_wizard_functions', 'step_post_form'));
        $step_form->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1792']);

        $step_highrise_settings = new Step('Highrise contact selection', array('Subscriber_import_wizard_functions', 'step_pre_highrise'), array('Subscriber_import_wizard_functions', 'step_post_highrise'));
        $step_highrise_settings->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1743']);
        // Declare steps - End }
        // Declare flows - Start {
        $current_flow_object = null;

        $flow_object_copyandpaste = new Flow(array($step_data_entry, $step_field_mapping, $step_results));
        $flow_object_copyandpaste->set_id('copyandpaste');
        $flow_object_copyandpaste->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1172']);
        $flow_object_copyandpaste->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['1272']);

        $flow_object_form = new Flow(array($step_form, $step_results));
        $flow_object_form->set_id('form');
        $flow_object_form->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1792']);
        $flow_object_form->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['1793']);

        $flow_object_file = new Flow(array($step_file_upload, $step_field_mapping, $step_results));
        $flow_object_file->set_id('fromfile');
        $flow_object_file->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0220']);
        $flow_object_file->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['1273']);

        $flow_object_mysql = new Flow(array($step_mysql_settings, $step_field_mapping, $step_results));
        $flow_object_mysql->set_id('fromdb');
        $flow_object_mysql->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1274']);
        $flow_object_mysql->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['1275']);

        $flow_object_highrise = new Flow(array($step_highrise_settings, $step_field_mapping, $step_results));
        $flow_object_highrise->set_id('highrise');
        $flow_object_highrise->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1741']);
        $flow_object_highrise->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['1742']);

        if ($flow == 'copyandpaste') {
            $current_flow_object = $flow_object_copyandpaste;
        } else if ($flow == 'fromfile') {
            $current_flow_object = $flow_object_file;
        } else if ($flow == 'fromdb') {
            $current_flow_object = $flow_object_mysql;
        } else if ($flow == 'highrise') {
            $current_flow_object = $flow_object_highrise;
        } else if ($flow == 'form') {
            $current_flow_object = $flow_object_form;
        }
        // Declare flows - End }
        // If a flow is set, start wizard - Start {
        if ($flow != '') {
            $wizard = WizardFactory::get('Subscriber_import_wizard', $current_flow_object);

            $wizard->set_extra_parameter('flow', $flow);
            $wizard->set_extra_parameter('list_id', $list_id);
            $wizard->set_extra_parameter('user_information', $this->array_user_information);

            if ($step_from_url !== 0)
                $wizard->set_current_step($step_from_url);

            WizardFactory::save('Subscriber_import_wizard', $wizard);

            if ($this->input->post('FormSubmit') === false) {
                $wizard->run_pre();
            } else {
                $wizard->run_post();
            }
        }
        // If a flow is set, start wizard - End }
        // If flow is not set, clear all session variables - Start {
        if ($flow == '') {
            WizardFactory::delete('Subscriber_import_wizard');
            $_SESSION[SESSION_NAME]['SubscriberImportInformation'] = array();
            unset($_SESSION[SESSION_NAME]['SubscriberImportInformation']);
            unset($_SESSION[SESSION_NAME]['StepMessage']);
        }
        // If flow is not set, clear all session variables - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arr_list_information = $array_return['List'];

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'orderby' => 'ASC'
                    )
        ));
        $array_custom_fields = $array_return['CustomFields'];
        // Retrieve list information - End }
        // Interface parsing - Start {
        $array_view_data = array();
        $sub_section = ($flow == '' ? 'flowlist' : $wizard->get_current_step_object()->get_id());

        if ($sub_section == 'flowlist') {
            $array_view_data['AvailableFlows'] = array($flow_object_copyandpaste, $flow_object_form, $flow_object_file, $flow_object_mysql);

            if ($this->_isHighriseIntegrationEnabled()) {
                $array_view_data['AvailableFlows'][] = $flow_object_highrise;
            }
        } else {
            $array_view_data['CurrentFlow'] = $current_flow_object;
            $array_view_data['CurrentStep'] = $wizard->get_current_step_object();
            $array_view_data['Wizard'] = $wizard;
        }

        if (isset($_SESSION[SESSION_NAME]['StepMessage'])) {
            $message = unserialize($_SESSION[SESSION_NAME]['StepMessage']);
            unset($_SESSION[SESSION_NAME]['StepMessage']);

            $array_view_data = array_merge($array_view_data, $message->get_all_parameters_as_array());
            unset($message);
        }


        $array_view_data = array_merge($array_view_data, array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscribersImport'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'CurrentMenuItem' => 'Lists',
            'CustomFields' => $array_custom_fields,
            'SubSection' => $sub_section,
            'ActiveListItem' => 'Add',
        ));

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/subscribers_import', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Remove controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function remove($list_id) {
        show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9057']);
        exit;
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'RemoveSubscribers') {
            $arr_event_return = $this->_event_remove_subscribers($list_id);
        }
        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arr_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve segments - Start {
        $arr_segments = array();
        $arr_return = API::call(array(
                    'format' => 'array',
                    'command' => 'segments.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'SegmentName',
                        'ordertype' => 'ASC'
                    )
        ));
        if (count($arr_return['Segments']) > 0) {
            $arr_segments = $arr_return['Segments'];
        }
        // Retrieve segments - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscribersRemove'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Remove',
            'Segments' => $arr_segments,
        );
        $this->render('user/subscribers_remove', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Get Total subscribers details controller method
     *
     * @return void
     * @author Eman Mohammed
     * */
    function details() {
        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($this->array_user_information['UserID']);
        $Details = Subscribers::getSubscribersOnTheAccountDetails($this->array_user_information['UserID']);

//        print_r('<pre>');
//        print_r($Details);
//        print_r('</pre>');
//        exit();
        $Limit = $this->array_user_information['GroupInformation']['LimitSubscribers'];
        $Ratio = ceil((100 * $TotalSubscribersOnTheAccount) / $Limit);

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'],
            'UserInformation' => $this->array_user_information,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Details',
            'Limit' => $Limit,
            'Ratio' => $Ratio,
            'TotalSubscribersOnTheAccount' => $TotalSubscribersOnTheAccount,
            'Details' => $Details,
        );
        $this->render('user/account_subscribers_details', $array_view_data);
    }

    /**
     * Move controller method
     *
     * @return void
     * @author Eman Mohammed
     * */
    function move($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }

//        if (AccountIsUnTrustedCheck($this->array_user_information)) {
//            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
//            exit;
//        }

        $List = Lists::RetrieveList(array('*'), array('ListID' => $list_id));
        if ($List['IsAutomationList'] == 'Yes') {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9057']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'MoveSubscribers') {
            $arr_event_return = $this->_event_move_subscribers($list_id);
        }
        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arr_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve segments - Start {
        $arr_segments = array();
        $arr_return = API::call(array(
                    'format' => 'array',
                    'command' => 'segments.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'SegmentName',
                        'ordertype' => 'ASC'
                    )
        ));
        if (count($arr_return['Segments']) > 0) {
            $arr_segments = $arr_return['Segments'];
        }
        // Retrieve segments - End }

        Core::LoadObject('lists');
        $Lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);


        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscribersRemove'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'Lists' => $Lists,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Move',
            'Segments' => $arr_segments,
        );
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        $this->render('user/subscribers_move', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Copy controller method
     *
     * @return void
     * @author Eman Mohammed
     * */
    function copy($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }

        $List = Lists::RetrieveList(array('*'), array('ListID' => $list_id));
        if ($List['IsAutomationList'] == 'Yes') {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9057']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'CopySubscribers') {
            $arr_event_return = $this->_event_copy_subscribers($list_id);
        }
        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arr_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve segments - Start {
        $arr_segments = array();
        $arr_return = API::call(array(
                    'format' => 'array',
                    'command' => 'segments.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'SegmentName',
                        'ordertype' => 'ASC'
                    )
        ));
        if (count($arr_return['Segments']) > 0) {
            $arr_segments = $arr_return['Segments'];
        }
        // Retrieve segments - End }

        Core::LoadObject('lists');
        $Lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);

        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscribersCopy'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'Lists' => $Lists,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Copy',
            'Segments' => $arr_segments,
        );
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        $this->render('user/subscribers_copy', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Export controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function export($list_id = 0, $with_rules = 'false') {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'ExportSubscribers') {
            if ($with_rules === 'true') {
                $list_id = $this->input->post('ExportListID');
                $arr_event_return = $this->_event_export_subscribers_with_rules($list_id, $this->input->post('ExportRules'), $this->input->post('ExportOperator'));
            } else {
                $arr_event_return = $this->_event_export_subscribers($list_id);
            }
        }

        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $arr_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve segments - Start {
        $arr_segments = array();
        $arr_return = API::call(array(
                    'format' => 'array',
                    'command' => 'segments.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'SegmentName',
                        'ordertype' => 'ASC'
                    )
        ));
        if (count($arr_return['Segments']) > 0) {
            $arr_segments = $arr_return['Segments'];
        }
        // Retrieve segments - End }
        // Retrieve segments - Start {
        $arr_custom_fields = array();
        $arr_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));
        if (count($arr_return['CustomFields']) > 0) {
            $arr_custom_fields = $arr_return['CustomFields'];
        }
        // Retrieve segments - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscribersExport'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'CurrentMenuItem' => 'Lists',
            'Segments' => $arr_segments,
            'DefaultFields' => Segments::$RuleDefaultFields,
            'CustomFields' => $arr_custom_fields,
        );
        $this->render('user/subscribers_export', $array_view_data);
        // Interface parsingb - End }
    }

    /**
     * Un subscribe controller
     * @author Eman
     * */
    function unsubscribe() {
        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];

        Core::LoadObject('suppression_list');

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachSuscriberID), $ListID);
                $ArrSubscribers[] = $Subscriber;
                //EmailAddress
                $ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ListID));

                //mfrood a3ml update oempro_stats_activity 
                //add new field TotalUnsubscribedManually
                //Increment it for this day 
                //If there are record get and update if not create one

                Subscribers::Unsubscribe_Enhnaced(
                        $ArraySubscriberList, $this->array_user_information['UserID'], 0, 0, $Subscriber['EmailAddress'], $EachSuscriberID, $_SERVER['REMOTE_ADDR'] . ' - Manual UnSubscribe', '', 0
                );

                $ArrayActivities = array(
                    'TotalUnsubscribedManually' => 1,
                );
                Subscribers::UpdateListActivityStatistics($ListID, $this->array_user_information['UserID'], $ArrayActivities);
            }
            if (is_array($ArrSubscribers)) {
                foreach ($ArrSubscribers as $Subscriber) {
//                    $Record = SuppressionList::GetRecordForAnEmailAddress($Subscriber['EmailAddress'], $ListID);
                    SuppressionList::Add(array(
                        'RelListID' => $ListID,
                        'RelOwnerUserID' => $this->array_user_information['UserID'],
                        'SuppressionSource' => 'User',
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'PrevSubscriptionStatus' => 'Unsubscribed',
                        'PrevBounceType' => $Subscriber['BounceType'],
                        'SourceList' => $ListID,
                    ));
                }
            }
            print_r((json_encode(array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['9249']))));
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    /**
     * Move to another list controller
     * @author Eman
     * */
    function move_to_another_list() {
        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];
        $ToListID = $_POST['to_listid'];

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));
        $List = Lists::RetrieveList(array('*'), array('ListID' => $ListID));

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();
            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachSuscriberID), $ListID);
                $ArrSubscribers[] = $Subscriber;
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }

            // i have to remove this and replace that by removing one by one
            //Subscribers::RemoveSubscribersByEmailAddresses($ListID, $ArrSubscribersEmails);

            if (is_array($ArrSubscribers)) {
                $TotalFailed = 0;
                $TotalMoved = 0;
                $TotalDuplicate = 0;

                //move thm to another list
                foreach ($ArrSubscribers as $Subscriber) {
                    $result = $this->move_subscriber($Subscriber, $ListID, $ToListID, $this->array_user_information);

                    if ($result[0]) {
                        if ($result[1]) {
                            $TotalDuplicate++;
                        }
                        $TotalMoved++;
                    } else {
                        $TotalFailed++;
                    }
                }
            }
            //mfrood a3ml update oempro_stats_activity 
            //add new field TotalMoved to source list
            //add new field TotalFromMove to destination list
            //Increment it for this day with $TotalMoved
            //If there are record get and update if not create one

            $ArrayActivities = array(
                'TotalMoved' => $TotalMoved,
            );
            Subscribers::UpdateListActivityStatistics($ListID, $this->array_user_information['UserID'], $ArrayActivities);

            $ArrayToActivities = array(
                'TotalFromMove' => ($TotalMoved - $TotalDuplicate),
            );
            Subscribers::UpdateListActivityStatistics($ToListID, $this->array_user_information['UserID'], $ArrayToActivities);

            if ($TotalMoved > 0) {
                $msg = $TotalMoved . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9262'] . " , " . $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9263'];
                print_r((json_encode(array(true, $msg))));
            } else {
                $msg = $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9270'];
                print_r((json_encode(array(false, $msg))));
            }
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    /**
     * Copy to another list controller
     * @author Eman
     * */
    function copy_to_another_list() {
        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];
        $ToListID = $_POST['to_listid'];

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();
            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachSuscriberID), $ListID);
                $ArrSubscribers[] = $Subscriber;
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }

            if (is_array($ArrSubscribers)) {
                $TotalCopied = 0;
                $TotalDuplicates = 0;
                $TotalFailed = 0;
                $TotalInValidEmails = 0;
                $TotalGlobalSuppressed = 0;
                $TotalListSupressed = 0;

                //move thm to another list
                foreach ($ArrSubscribers as $Subscriber) {
                    $result = $this->copy_subscriber($Subscriber, $ListID, $ToListID, $this->array_user_information);
                    if (!$result[0]) {
                        $TotalFailed++;
                        switch ($result[1]) {
                            case 1:
                                $TotalGlobalSuppressed++;
                                break;
                            case 2:
                                $TotalListSupressed++;
                                break;
                            case 3:
                                $TotalDuplicates++;
                                break;
                            case 4:
                                $TotalInValidEmails++;
                                break;
                            default:
                                break;
                        }
                    } else {
                        $TotalCopied++;
                    }
                }
            }
            //mfrood a3ml update oempro_stats_activity 
            //add new field TotalFromCopy to destination list
            //Increment it for this day with $TotalCopied
            //If there are record get and update if not create one
            $ArrayToActivities = array(
                'TotalFromCopy' => $TotalCopied,
            );
            Subscribers::UpdateListActivityStatistics($ToListID, $this->array_user_information['UserID'], $ArrayToActivities);

            $msg = $this->get_copy_subscribers_msg($TotalCopied, $TotalFailed, $TotalDuplicates, $TotalGlobalSuppressed, $TotalListSupressed, $TotalInValidEmails);

            if ($TotalCopied > 0) {
                print_r((json_encode(array(true, $msg))));
            } else {
                print_r((json_encode(array(false, $msg))));
            }
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9300']))));
        }
        exit();
    }

    private function get_copy_subscribers_msg($TotalCopied, $TotalFailed, $TotalDuplicates, $TotalGlobalSuppressed, $TotalListSupressed, $TotalInValidEmails) {
        $msg = ($TotalCopied > 0 ? "<span class='bold'>" . $TotalCopied . "</span> " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9301'] . " <br/> " : "")
                . "<span class='bold'>" . $TotalFailed . "</span> " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9299'] . " <br/> "
                . ($TotalDuplicates > 0 ? " <span style='padding-left:20px'><span class='bold'>" . ApplicationHeader::$ArrayLanguageStrings['Screen']['9302'] . "</span> : " . $TotalDuplicates . "</span><br/> " : "")
                . ($TotalGlobalSuppressed > 0 ? " <span style='padding-left:20px'><span class='bold'>" . ApplicationHeader::$ArrayLanguageStrings['Screen']['9303'] . "</span> : " . $TotalGlobalSuppressed . "</span><br/> " : "")
                . ($TotalListSupressed > 0 ? " <span style='padding-left:20px'><span class='bold'>" . ApplicationHeader::$ArrayLanguageStrings['Screen']['9304'] . "</span> : " . $TotalListSupressed . "</span><br/> " : "")
                . ($TotalInValidEmails > 0 ? " <span style='padding-left:20px'><span class='bold'>" . ApplicationHeader::$ArrayLanguageStrings['Screen']['9305'] . "</span> : " . $TotalInValidEmails . "</span><br/> " : "");

        return $msg;
    }

    private function move_subscriber($Subscriber, $ListID, $ToListID, $User) {

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));

        $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'RelListID' => 0
                        ), 5, 0);
        //wont be added to this list
        $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'PrevSubscriptionStatus' => 'Unsubscribed',
                    'RelListID' => $ToListID
                        ), 5, 0);
        // can be added 3ady
        $list_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'RelListID' => $ToListID
                        ), 5, 0);

        if ($list_suppress_unsubscribe || $global_suppress) {
            //cant be moved to this list 
            return array(false);
        } else {
            if ($list_suppress) {
                SuppressionList::DeleteEmail($Subscriber['EmailAddress'], $ToListID, $User['UserID']);
            }
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'subscribers.delete',
                        'protected' => true,
                        'username' => $User['Username'],
                        'password' => $User['Password'],
                        'parameters' => array(
                            'subscriberlistid' => $ListID,
                            'subscribers' => $Subscriber['SubscriberID'],
                        )
            ));

            $Ret = Subscribers::AddSubscriber_Enhanced(array(
                        'UserInformation' => $User,
                        'ListInformation' => $ToList,
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Move',
                        'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                        'UpdateIfDuplicate' => false, //this
                        'UpdateIfUnsubscribed' => false, //this
                        'ApplyBehaviors' => true,
                        'SendConfirmationEmail' => false,
                        'UpdateStatistics' => true, //true
                        'TriggerWebServices' => false,
                        'TriggerAutoResponders' => false
            ));
            $duplicate = false;
            if ($Ret[0] == false) {
                switch ($Ret[1]) {
                    case 3:
                        $duplicate = true;
                        break;
                    default:
                        return array(false, 4);
                        break;
                }
            }
            $ArrayFieldAndValues = array(
                'Subscriber_IP' => $Subscriber['Subscriber_IP'],
                'City' => $Subscriber['City'],
                'Country' => $Subscriber['Country'],
                'SubscriptionStatus' => $Subscriber['SubscriptionStatus'],
                'BounceType' => $Subscriber['BounceType'],
            );
            Subscribers::Update($ToList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $Ret[1]));
            return array(true, $duplicate);
        }
    }

    private function copy_subscriber($Subscriber, $ListID, $ToListID, $User) {

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));

        $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'RelListID' => 0
                        ), 5, 0);
        //wont be added to this list
        $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'PrevSubscriptionStatus' => 'Unsubscribed',
                    'RelListID' => $ToListID
                        ), 5, 0);
        // can be added 3ady
        $list_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'RelListID' => $ToListID
                        ), 5, 0);

        if ($global_suppress) {
            //cant be copied to that list -- email exists in global supression list
            return array(false, 1); //$TotalFailedExitsInGlobalSuppList++;
        } else if ($list_suppress_unsubscribe || $list_suppress) {
            //cant be copied to that list -- email exists in supression list for that list
            return array(false, 2); //$TotalFailedExitsInSuppList++;
        } else {

            $Ret = Subscribers::AddSubscriber_Enhanced(array(
                        'UserInformation' => $User,
                        'ListInformation' => $ToList,
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Move',
                        'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                        'UpdateIfDuplicate' => false,
                        'UpdateIfUnsubscribed' => false,
                        'ApplyBehaviors' => true,
                        'SendConfirmationEmail' => false,
                        'UpdateStatistics' => true,
                        'TriggerWebServices' => false,
                        'TriggerAutoResponders' => false
            ));

            if ($Ret[0] == false) {
                switch ($Ret[1]) {
                    case 3:
                        return array(false, 3); //$TotalDuplicates++;
                    default:
                        // Invalid email address
                        return array(false, 4); //$TotalInValidEmails++;
                }
            }

            $ArrayFieldAndValues = array(
                'Subscriber_IP' => $Subscriber['Subscriber_IP'],
                'City' => $Subscriber['City'],
                'Country' => $Subscriber['Country'],
                'SubscriptionStatus' => $Subscriber['SubscriptionStatus'],
                'BounceType' => $Subscriber['BounceType'],
            );
            Subscribers::Update($ToList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $Ret[1]));
            return array(true); //$TotalSuccess++;
        }
    }

    /**
     * Move to suppression list controller
     * @author Eman
     * */
    function move_to_suppression() {

        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];

        Core::LoadObject('suppression_list');

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();
            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachSuscriberID), $ListID);
                $ArrSubscribers[] = $Subscriber;
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }

            //mfrood a3ml update oempro_stats_activity 
            //add new field TotalMovedToSuppressionList
            //Increment it for this day 
            //If there are record get and update if not create one
            Subscribers::RemoveSubscribersByEmailAddresses($ListID, $ArrSubscribersEmails);

            if (is_array($ArrSubscribers)) {
                foreach ($ArrSubscribers as $Subscriber) {
                    SuppressionList::Add(array(
                        'RelListID' => $ListID,
                        'RelOwnerUserID' => $this->array_user_information['UserID'],
                        'SuppressionSource' => 'User',
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'PrevSubscriptionStatus' => 'Subscribed',
                        'PrevBounceType' => $Subscriber['BounceType'],
                        'SourceList' => $ListID,
                    ));
                    $ArrayActivities = array(
                        'TotalSuppressed' => 1,
                    );
                    Subscribers::UpdateListActivityStatistics($ListID, $this->array_user_information['UserID'], $ArrayActivities);
                }
            }

            print_r((json_encode(array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['9245']))));
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    /**
     * Browse controller method
     *
     * @return void
     * @author Eman
     * */
    function search() {

        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }

        Core::LoadObject('lists');
        Core::LoadObject('subscribers');
        Core::LoadObject('segments');
        Core::LoadObject('custom_fields');

        $ArrayAPIData = $_POST;

        // Lowercase all field names - Start
        $TMPArrayAPIData = $ArrayAPIData;
        unset($ArrayAPIData);
        foreach ($TMPArrayAPIData as $Key => $Value) {
            $ArrayAPIData[strtolower($Key)] = $Value;
        }
        unset($TMPArrayAPIData);

        $ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));
        $ArrayGlobalCustomFieldSelects = array();

        if ($ArrayGlobalCustomFields != false) {
            foreach ($ArrayGlobalCustomFields as $EachField) {
                $FieldName = 'ValueText';
                $NullDefault = '""';
                if ($EachField['FieldType'] == 'Date field' || $EachField['ValidationMethod'] == 'Date') {
                    $FieldName = 'ValueDate';
                    $NullDefault = '"0000-00-00"';
                } else if ($EachField['FieldType'] == 'Time field' || $EachField['ValidationMethod'] == 'Time') {
                    $FieldName = 'ValueTime';
                    $NullDefault = '"00:00:00"';
                } else if ($EachField['ValidationMethod'] == 'Numbers') {
                    $FieldName = 'ValueDouble';
                    $NullDefault = '0';
                }

                $ArrayGlobalCustomFieldSelects[] = 'IFNULL((SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $EachField['CustomFieldID'] . ' AND EmailAddress = ' . MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid'] . '.EmailAddress LIMIT 1), ' . $NullDefault . ') AS CustomField' . $EachField['CustomFieldID'];
            }
        }
        // Add global custom field values to subscribers - End }

        $SQLQuery = Segments::GetSegmentSQLQuery_Enhanced(0, false, $ArrayAPIData['rules'], $ArrayAPIData['operator'], $ArrayAPIData['listid']);
        $SQLJoin = Segments::GetSegmentJoinQuery(0, false, '', $ArrayAPIData['rules'], $ArrayAPIData['listid']);

//        $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
//                    'ReturnFields' => array('*'),
//                    'SubscriberListID' => 13
//        ));

        $arr_subscribers = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array_merge($ArrayGlobalCustomFieldSelects, array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid'] . '.*')),
            'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid']),
            'Joins' => $SQLJoin,
            'Criteria' => $SQLQuery,
        ));
        // Retrieve custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $ArrayAPIData['listid'],
                        'orderfield' => 'FieldName',
                        'orderby' => 'ASC'
                    )
        ));
        $custom_fields = $array_return['CustomFields'];
        // Retrieve custom fields - End }
        $Subscribers = array();
        foreach ($arr_subscribers as $ArraySubscriber) {
            foreach ($ArraySubscriber as $key => $value) {
                if (strpos($key, 'CustomField') !== false) {

                    $id = substr($key, 11);
                    foreach ($custom_fields as $custom_field) {
                        $custom_field_ID = $custom_field['CustomFieldID'];
                        $field_name = $custom_field['FieldName'];
                        if ($id == $custom_field_ID) {
                            $ArraySubscriber[$custom_field['FieldName']] = $ArraySubscriber['CustomField' . $id];
                            unset($ArraySubscriber['CustomField' . $id]);
                        }
                    }
                }
            }
            $Subscribers[] = $ArraySubscriber;
        }


        $SubscriberCount = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array('COUNT(*) AS TotalSubscribers'),
            'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid']),
            'Joins' => $SQLJoin,
            'Criteria' => $SQLQuery
        ));
// Retrieve subscribers - End }

        $Lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);

// Return results - Start
        $ArrayOutput = array('Success' => true,
            'ErrorCode' => 0,
            'ErrorText' => '',
            'Lists' => $Lists,
            'Subscribers' => $Subscribers,
            'TotalSubscribers' => $SubscriberCount[0]['TotalSubscribers']
        );
//        print_r(json_encode($ArrayOutput));
        $this->render('user/subscriber_browse-filter', array(
            'Subscribers' => $Subscribers,
            'Lists' => $Lists,
            'ListID' => $ArrayAPIData['listid'],
        ));
    }

    /**
     * Browse controller method
     *
     * @return void
     * @author Eman
     * */
    function browse($list_id = 0, $segment_id = 0, $start_from = 1, $rpp = 50) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteSubscriber') {

            if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $this->_event_delete_subscribers($this->input->post('SelectedSubscribers'));
            }
        }
        // Events - End }

        $arr_list_information = array();
        $arr_subscribers = array();
        $arr_list_segments = array();

        if ($list_id > 0) {
            // Retrieve list information - Start {
            $arr_list_information = Lists::RetrieveList(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'ListID' => $list_id));

            if ($arr_list_information == false) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
                return;
            }
            // Retrieve list information - End }
            // Retrieve segment information - Start {
            $arr_segment_information = array();

            $arr_list_segments = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $list_id), array('SegmentName' => 'ASC'), false);

            if ($segment_id > 0) {
                $arr_segment_information = Segments::RetrieveSegment(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $arr_list_information['ListID'], 'SegmentID' => $segment_id), false);

                if ($arr_segment_information == false) {
                    $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1417'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1418']);
                    return;
                }
                $arr_subscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
                            'ReturnFields' => array('*'),
                            'SubscriberListID' => $list_id,
                            'SegmentID' => $segment_id
                ));
            } else {
                $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
                            'ReturnFields' => array('*'),
                            'SubscriberListID' => $list_id
                ));
            }
            // Retrieve custom fields - Start {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'customfields.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'subscriberlistid' => $list_id,
                            'orderfield' => 'FieldName',
                            'orderby' => 'ASC'
                        )
            ));
            $arr_custom_fields = $array_return['CustomFields'];
            // Retrieve custom fields - End }

            $custom_fields = $arr_custom_fields; //CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $list_id));
            // Retrieve segment information - End }
            $Subscribers = array();
            foreach ($arr_subscribers as $ArraySubscriber) {
                foreach ($ArraySubscriber as $key => $value) {
                    if (strpos($key, 'CustomField') !== false) {

                        $id = substr($key, 11);
                        foreach ($custom_fields as $custom_field) {
                            $custom_field_ID = $custom_field['CustomFieldID'];
                            $field_name = $custom_field['FieldName'];
                            if ($id == $custom_field_ID) {
                                $ArraySubscriber[$custom_field['FieldName']] = $ArraySubscriber['CustomField' . $id];
                                unset($ArraySubscriber['CustomField' . $id]);
                            }
                        }
                    }
                }
                $Subscribers[] = $ArraySubscriber;
            }
        }


//        if ($global_custom_fields != false) {
//            $custom_fields = array_merge($custom_fields ? $custom_fields : array(), $global_custom_fields);
//        }
        // Retrieve lists and segments - Start {
        $arr_lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);
        if (!$arr_lists) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1486'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1487']);
            return;
        }
        $global_custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));

        foreach ($arr_lists as &$each_list) {
            $each_list['Segments'] = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $each_list['ListID']), array('SegmentName' => 'ASC'), false);
            $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $each_list['ListID']));
            if ($global_custom_fields != false) {
                $custom_fields = array_merge($custom_fields ? $custom_fields : array(), $global_custom_fields);
            }
            $each_list['CustomFields'] = $custom_fields;
        }
        // Retrieve lists and segments - End }
        // Retrieve campaigns - Start {
        Core::LoadObject('campaigns');
        $arr_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC'),
                    'Criteria' => array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID']),
                    'Content' => false,
                    'SplitTests' => false,
                    'RecipientLists' => false,
                    'RecipientSegments' => false,
                    'Tags' => false
        ));
        // Retrieve campaigns - End }

        $arr_activity_field_ids = array();
        foreach (Segments::$RuleActivityFields as $each) {
            $arr_activity_field_ids[] = $each['CustomFieldID'];
        }

        // Interface parsing - Start {
        $array_view_data = array();

        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($this->array_user_information['UserID']);

        $array_view_data = array_merge($array_view_data, array(
            'CurrentMenuItem' => 'Subscribers',
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseSubscribers'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'ListSegments' => $arr_list_segments,
            'SegmentInformation' => $arr_segment_information,
            'Subscribers' => $Subscribers,
            'Lists' => $arr_lists,
            'CustomFields' => $arr_custom_fields,
            'Campaigns' => $arr_campaigns,
            'DefaultFields' => Segments::$RuleDefaultFields,
            'PluginFields' => Segments::$RulePluginFields,
            'RuleOperators' => Segments::$RuleOperators,
            'ActivityFieldIDs' => $arr_activity_field_ids,
            'ActivityFields' => Segments::$RuleActivityFields,
            'TotalSubscribersOnTheAccount' => $TotalSubscribersOnTheAccount,
        ));
        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/subscriber_browse', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Browse controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function browse_old($list_id = 0, $segment_id = 0, $start_from = 1, $rpp = 50) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteSubscriber') {

            if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $this->_event_delete_subscribers($this->input->post('SelectedSubscribers'));
            }
        }
        // Events - End }

        $arr_list_information = array();

        if ($list_id != 0) {
            // Retrieve list information - Start {
            $arr_list_information = Lists::RetrieveList(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'ListID' => $list_id));

            if ($arr_list_information == false) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
                return;
            }
            // Retrieve list information - End }
            // Retrieve segment information - Start {
            $arr_segment_information = array();
            if ($segment_id > 0) {
                $arr_segment_information = Segments::RetrieveSegment(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $arr_list_information['ListID'], 'SegmentID' => $segment_id), false);

                if ($arr_segment_information == false) {
                    $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1417'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1418']);
                    return;
                }
            }
            // Retrieve segment information - End }
            // Retrieve custom fields - Start {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'customfields.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'subscriberlistid' => $list_id,
                            'orderfield' => 'FieldName',
                            'orderby' => 'ASC'
                        )
            ));
            $arr_custom_fields = $array_return['CustomFields'];
            // Retrieve custom fields - End }
            // Retrieve subscribers - Start {
            // Retrieve subscribers - End }
        }

        // Retrieve lists and segments - Start {
        $arr_lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);
        if (!$arr_lists) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1486'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1487']);
            return;
        }

        $global_custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));
        foreach ($arr_lists as &$each_list) {
            $each_list['Segments'] = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $each_list['ListID']), array('SegmentName' => 'ASC'), false);
            $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $each_list['ListID']));
            if ($global_custom_fields != false) {
                $custom_fields = array_merge($custom_fields ? $custom_fields : array(), $global_custom_fields);
            }
            $each_list['CustomFields'] = $custom_fields;
        }
        // Retrieve lists and segments - End }
        // Retrieve campaigns - Start {
        Core::LoadObject('campaigns');
        $arr_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC'),
                    'Criteria' => array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID']),
                    'Content' => false,
                    'SplitTests' => false,
                    'RecipientLists' => false,
                    'RecipientSegments' => false,
                    'Tags' => false
        ));
        // Retrieve campaigns - End }

        $arr_activity_field_ids = array();
        foreach (Segments::$RuleActivityFields as $each) {
            $arr_activity_field_ids[] = $each['CustomFieldID'];
        }

        // Interface parsing - Start {
        $array_view_data = array();

        $array_view_data = array_merge($array_view_data, array(
            'CurrentMenuItem' => 'Subscribers',
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseSubscribers'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'SegmentInformation' => $arr_segment_information,
            'Lists' => $arr_lists,
            'CustomFields' => $arr_custom_fields,
            'Campaigns' => $arr_campaigns,
            'DefaultFields' => Segments::$RuleDefaultFields,
            'PluginFields' => Segments::$RulePluginFields,
            'RuleOperators' => Segments::$RuleOperators,
            'ActivityFieldIDs' => $arr_activity_field_ids,
            'ActivityFields' => Segments::$RuleActivityFields,
        ));
        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/subscriber_browse_old', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Downloads duplicate or failed data of given import id
     *
     * @return void
     * @author Mert Hurturk
     * */
    function download($type, $import_id) {
        $import_information = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array('*'),
            'Tables' => array(MYSQL_TABLE_PREFIX . 'subscriber_imports'),
            'Criteria' => array(
                array(
                    'Column' => 'ImportID',
                    'Operator' => '=',
                    'Value' => $import_id
                ),
                array(
                    'Link' => 'AND',
                    'Column' => 'RelUserID',
                    'Operator' => '=',
                    'Value' => $this->array_user_information['UserID']
                ),
            )
        ));
        $import_information = $import_information[0];

        if ($type == 'duplicateimportdata') {
            $data = $import_information['DuplicateData'];
        } else if ($type == 'failedimportdata') {
            $data = $import_information['FailedData'];
        }

        $data = trim($data, "\n");

        // Make CSV data file downloadable - Start 
        header("Content-type: application/octetstream");
        header("Content-Disposition: attachment; filename=\"importdata.csv\"");
        header('Content-length: ' . strlen($data));
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: public");
        // Make CSV data file downloadable - End
        print $data;
        exit;
    }

    /**
     * Delete subscribers event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_delete_subscribers($subscribers) {

        if (!$subscribers || count($subscribers) < 1) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1333']);
            return;
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $this->input->post('SearchList'),
                        'subscribers' => implode(',', $this->input->post('SelectedSubscribers'))
                    )
        ));
        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1334']);
    }

    /**
     * Export subscribers event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_export_subscribers($list_id) {
        $this->form_validation->set_rules('Fields', ApplicationHeader::$ArrayLanguageStrings['Screen']['1131'], 'required');
        if ($this->form_validation->run() == false) {
            return false;
        }

        $arr_subscribers = array();
        switch ($this->input->post('Subscribers')) {
            case 'Active': // All subscribers
                // $arr_subscribers = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Not Bounced', 'SubscriptionStatus'=>'Subscribed'), $list_id, array('EmailAddress'=>'ASC'), 0, 0);
                $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
                            'ReturnFields' => $this->input->post('Fields'),
                            'SubscriberListID' => $list_id,
                            'Criteria' => array(
                                array('Column' => 'BounceType', 'Operator' => '=', 'Value' => 'Not Bounced'),
                                array('Column' => 'SubscriptionStatus', 'Operator' => '=', 'Value' => 'Subscribed', 'Link' => 'AND')
                            )
                ));
                break;
            case 'Suppressed': // Suppressed subscribers
                $arr_subscribers = Subscribers::RetrieveSuppressedSubscribers(array('*'), array(), $list_id, array('EmailAddress' => 'ASC'), 0, 0, false, $this->array_user_information['UserID']);
                break;
            case 'Unsubscribed': // Unsubscribed subscribers
                // $arr_subscribers = Subscribers::RetrieveSubscribers(array('*'), array('SubscriptionStatus'=>'Unsubscribed'), $list_id, array('EmailAddress'=>'ASC'), 0, 0);
                $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
                            'ReturnFields' => $this->input->post('Fields'),
                            'SubscriberListID' => $list_id,
                            'Criteria' => array(
                                array('Column' => 'SubscriptionStatus', 'Operator' => '=', 'Value' => 'Unsubscribed')
                            )
                ));
                break;
            case 'Soft bounced': // Soft bounced subscribers
                // $arr_subscribers = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Soft'), $list_id, array('EmailAddress'=>'ASC'), 0, 0);
                $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
                            'ReturnFields' => $this->input->post('Fields'),
                            'SubscriberListID' => $list_id,
                            'Criteria' => array(
                                array('Column' => 'BounceType', 'Operator' => '=', 'Value' => 'Soft')
                            )
                ));
                break;
            case 'Hard bounced': // Hard bounced subscribers
                // $arr_subscribers = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Hard'), $list_id, array('EmailAddress'=>'ASC'), 0, 0);
                $arr_subscribers = Subscribers::RetrieveSubscribers_Enhanced(array(
                            'ReturnFields' => $this->input->post('Fields'),
                            'SubscriberListID' => $list_id,
                            'Criteria' => array(
                                array('Column' => 'BounceType', 'Operator' => '=', 'Value' => 'Hard')
                            )
                ));
                break;
            default: // Segment subscribers
                // $arr_subscribers = Subscribers::RetrieveSegmentSubscribers(array('*'), array(), $list_id, $this->input->post('Subscribers'), array('EmailAddress'=>'ASC'));
                $arr_subscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
                            'ReturnFields' => array('*'),
                            'SubscriberListID' => $list_id,
                            'SegmentID' => $this->input->post('Subscribers'),
                            'RowOrder' => array('Column' => 'EmailAddress', 'Type' => 'ASC')
                ));
                break;
        }

        // Get data string, print file and exit - Start {
        switch ($this->input->post('FileFormat')) {
            case 'csv':
                $file_data = Subscribers::GetCSVData($arr_subscribers, $this->input->post('Fields'), ApplicationHeader::$ArrayLanguageStrings['Screen']['0665']);
                // Make CSV data file downloadable - Start {
                header("Content-type: application/octetstream");
                header("Content-Disposition: attachment; filename=\"subscribers.csv\"");
                header('Content-length: ' . strlen($file_data));
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                // Make CSV data file downloadable - End }
                break;
            case 'xml':
                $file_data = Subscribers::GetXMLData($arr_subscribers, $this->input->post('Fields'));
                // Make XML data file downloadable - Start {
                header("Content-type: application/octetstream");
                header("Content-Disposition: attachment; filename=\"subscribers.xml\"");
                header('Content-length: ' . strlen($file_data));
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                // Make XML data file downloadable - End }
                break;
            case 'tab':
                $file_data = Subscribers::GetTABData($arr_subscribers, $this->input->post('Fields'));
                // Make TAB data file downloadable - Start {
                header("Content-type: application/octetstream");
                header("Content-Disposition: attachment; filename=\"subscribers.txt\"");
                header('Content-length: ' . strlen($file_data));
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                // Make TAB data file downloadable - End } 
                break;
        }
        // Get data string, print file and exit - End }

        print $file_data;
        exit;
    }

    /**
     * Export subscribers with rules event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_export_subscribers_with_rules($list_id, $rules, $operator) {
        // Add global custom field values to subscribers - Start {
        Core::LoadObject('lists');
        Core::LoadObject('custom_fields');

        $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => $list_id, 'IsGlobal' => 'No'));
        $ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));
        $ArrayGlobalCustomFieldSelects = array();
        if ($ArrayGlobalCustomFields != false) {
            foreach ($ArrayGlobalCustomFields as $EachField) {
                $FieldName = 'ValueText';
                if ($EachField['FieldType'] == 'Date field' || $EachField['ValidationMethod'] == 'Date') {
                    $FieldName = 'ValueDate';
                } else if ($EachField['FieldType'] == 'Time field' || $EachField['ValidationMethod'] == 'Time') {
                    $FieldName = 'ValueTime';
                } else if ($EachField['ValidationMethod'] == 'Numbers') {
                    $FieldName = 'ValueDouble';
                }

                $ArrayGlobalCustomFieldSelects[] = 'IFNULL((SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $EachField['CustomFieldID'] . ' AND EmailAddress = ' . MYSQL_TABLE_PREFIX . 'subscribers_' . $list_id . '.EmailAddress LIMIT 1), "") AS CustomField' . $EachField['CustomFieldID'];
            }
        }
        // Add global custom field values to subscribers - End }

        $SQLQuery = Segments::GetSegmentSQLQuery_Enhanced(0, false, $rules, $operator, $list_id);
        $SQLJoin = Segments::GetSegmentJoinQuery(0, false, '', $rules, $list_id);

        $mysql_result = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array_merge($ArrayGlobalCustomFieldSelects, array(MYSQL_TABLE_PREFIX . 'subscribers_' . $list_id . '.*')),
            'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $list_id),
            'Joins' => $SQLJoin,
            'Criteria' => $SQLQuery,
            'RowOrder' => array('Column' => 'EmailAddress', 'Type' => 'ASC'),
            'ReturnSQLResult' => TRUE
        ));

        $fields_to_export = array(
            'SubscriberID', 'EmailAddress', 'BounceType', 'SubscriptionStatus', 'SubscriptionIP', 'SubscriptionDate', 'OptInDate'
        );


        foreach ($ArrayCustomFields as $Each) {
            $fields_to_export[] = 'CustomField' . $Each['CustomFieldID'];
        }

        foreach ($ArrayGlobalCustomFields as $Each) {
            $fields_to_export[] = 'CustomField' . $Each['CustomFieldID'];
        }

        header("Content-type: application/octetstream");
        header("Content-Disposition: attachment; filename=\"subscribers.csv\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: public");

        print Subscribers::GetCSVDataHeader($fields_to_export, ApplicationHeader::$ArrayLanguageStrings['Screen']['0665']);
        print "\n";
        while ($EachRow = mysql_fetch_assoc($mysql_result)) {
            print Subscribers::GetCSVDataForASubscriber($fields_to_export, $EachRow) . "\n";
        }
        exit;
    }

    /**
     * Remove subscribers event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_move_subscribers($list_id) {

        $move_type = $this->input->post('Subscribers');
        $not_opt_for_days = $this->input->post('NotOptedInForDays');
        $not_opt_for_days = isset($not_opt_for_days) && !empty($not_opt_for_days) ? $not_opt_for_days : 0;

        $copy_paste = explode("\n", $this->input->post('CopyAndPaste'));

        $move_to_option = $this->input->post('move_to_option');
        $to_list_id = $this->input->post('Lists');
        $to_list_id = isset($to_list_id) && !empty($to_list_id) ? $to_list_id : 0;

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $to_list_id));
        $List = Lists::RetrieveList(array('*'), array('ListID' => $list_id));

        if ($move_type == 'Not opted in for days' && $not_opt_for_days == 0) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9258']);
            return false;
        }

        if ($move_to_option == 2 && $to_list_id == 0) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9256']);
            return false;
        }

        $arr_affected_subscribers = array();

        if ($this->input->post('Subscribers') == 'Active') {
            $arr_affected_subscribers = Subscribers::moveActiveSubscribers_Enhanced($list_id);
        } else if ($this->input->post('Subscribers') == 'Not opted in for days') {
            $arr_affected_subscribers = Subscribers::moveNotOptedInSubscribers_Enhanced($list_id, $this->input->post('NotOptedInForDays'));
        } else if ($this->input->post('Subscribers') == 'Copy and paste') {
            $arr_subscribers = explode("\n", $this->input->post('CopyAndPaste'));
            $arr_affected_subscribers = Subscribers::moveSubscribersByEmailAddresses_Enhhanced($list_id, $arr_subscribers);
        } else if ($this->input->post('Subscribers') == 'Soft bounced') {
            $arr_affected_subscribers = Subscribers::moveSoftBouncedSubscribers_Enhanced($list_id);
        } else {
            $arr_affected_subscribers = Subscribers::moveSubscribersOfSegment_Enhanced($list_id, $this->input->post('Subscribers'));
        }
        // If checked, add removed email addresses to suppression list - Start {

        $TotalMoved = 0;
        $TotalDuplicate = 0;
        $TotalFailed = 0;

        if (is_array($arr_affected_subscribers) && count($arr_affected_subscribers) > 0) {
            if ($move_to_option == 1) {
                //move to suppression list
                foreach ($arr_affected_subscribers as $Subscriber) {

                    $array_return = API::call(array(
                                'format' => 'array',
                                'command' => 'subscribers.delete',
                                'protected' => true,
                                'username' => $this->array_user_information['Username'],
                                'password' => $this->array_user_information['Password'],
                                'parameters' => array(
                                    'subscriberlistid' => $list_id,
                                    'subscribers' => $Subscriber['SubscriberID'],
                                )
                    ));

                    SuppressionList::Add(array(
                        'RelListID' => $list_id,
                        'RelOwnerUserID' => $this->array_user_information['UserID'],
                        'SuppressionSource' => 'User',
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'PrevSubscriptionStatus' => $Subscriber['SubscriptionStatus'],
                        'PrevBounceType' => $Subscriber['BounceType'],
                        'SourceList' => $list_id
                    ));
                    $TotalMoved++;
                    //mfrood a3ml update oempro_stats_activity 
                    //add new field TotalMovedToSuppressionList
                    //Increment it for this day 
                    //If there are record get and update if not create one
                    $ArrayActivities = array(
                        'TotalSuppressed' => 1,
                    );
                    Subscribers::UpdateListActivityStatistics($list_id, $this->array_user_information['UserID'], $ArrayActivities);
                }
            } else if ($move_to_option == 2) {
                //move to another list

                foreach ($arr_affected_subscribers as $Subscriber) {

                    $result = $this->move_subscriber($Subscriber, $list_id, $to_list_id, $this->array_user_information);
                    if ($result[0]) {
                        if ($result[1]) {
                            $TotalDuplicate++;
                        }
                        $TotalMoved++;
                    } else {
                        $TotalFailed++;
                    }
//                    $Ret = Subscribers::AddSubscriber_Enhanced(array(
//                                'UserInformation' => $this->array_user_information,
//                                'ListInformation' => $ToList,
//                                'EmailAddress' => $Subscriber['EmailAddress'],
//                                'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Move',
//                                'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
//                                'UpdateIfDuplicate' => true,
//                                'UpdateIfUnsubscribed' => true,
//                                'ApplyBehaviors' => true,
//                                'SendConfirmationEmail' => false,
//                                'UpdateStatistics' => true,
//                                'TriggerWebServices' => false,
//                                'TriggerAutoResponders' => false
//                    ));
//                    $ArrayFieldAndValues = array(
//                        'Subscriber_IP' => $Subscriber['Subscriber_IP'],
//                        'City' => $Subscriber['City'],
//                        'Country' => $Subscriber['Country'],
//                        'SubscriptionStatus' => $Subscriber['SubscriptionStatus'],
//                        'BounceType' => $Subscriber['BounceType'],
//                    );
//                    Subscribers::Update($ToList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $Ret[1]));
                }

                //mfrood a3ml update oempro_stats_activity 
                //add new field TotalMoved to source list
                //add new field TotalFromMove to destination list
                //Increment it for this day with $TotalMoved
                //If there are record get and update if not create one

                $ArrayActivities = array(
                    'TotalMoved' => $TotalMoved,
                );
                Subscribers::UpdateListActivityStatistics($list_id, $this->array_user_information['UserID'], $ArrayActivities);

                $ArrayToActivities = array(
                    'TotalFromMove' => ($TotalMoved - $TotalDuplicate),
                );
                Subscribers::UpdateListActivityStatistics($to_list_id, $this->array_user_information['UserID'], $ArrayToActivities);

                if ($TotalMoved <= 0) {
                    $msg = $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9264'];
                    $_SESSION['PageMessageCache'] = array('Error', $msg);
                    return false;
                }
            }
        } else {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9257']);
            return false;
        }
        // If checked, add removed email addresses to suppression list - End }

        $this->load->helper('url');
        $msg = $TotalMoved . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9262'] . " , " . $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9263'];
        $_SESSION['PageMessageCache'] = array('Success', $msg);
        redirect(InterfaceAppURL(true) . '/user/list/statistics/' . $list_id, 'location', '302');
    }

    /**
     * Copy subscribers event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_copy_subscribers($list_id) {

        $move_type = $this->input->post('Subscribers');
        $not_opt_for_days = $this->input->post('NotOptedInForDays');
        $not_opt_for_days = isset($not_opt_for_days) && !empty($not_opt_for_days) ? $not_opt_for_days : 0;

        $copy_paste = explode("\n", $this->input->post('CopyAndPaste'));

//        $move_to_option = $this->input->post('move_to_option');
        $to_list_id = $this->input->post('Lists');
        $to_list_id = isset($to_list_id) && !empty($to_list_id) ? $to_list_id : 0;

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $to_list_id));

        if ($move_type == 'Not opted in for days' && $not_opt_for_days == 0) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9258']);
            return false;
        }

        if ($to_list_id == 0) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9256']);
            return false;
        }

        $arr_affected_subscribers = array();

        if ($this->input->post('Subscribers') == 'Active') {
            $arr_affected_subscribers = Subscribers::moveActiveSubscribers_Enhanced($list_id);
        } else if ($this->input->post('Subscribers') == 'Not opted in for days') {
            $arr_affected_subscribers = Subscribers::moveNotOptedInSubscribers_Enhanced($list_id, $this->input->post('NotOptedInForDays'));
        } else if ($this->input->post('Subscribers') == 'Copy and paste') {
            $arr_subscribers = explode("\n", $this->input->post('CopyAndPaste'));
            $arr_affected_subscribers = Subscribers::moveSubscribersByEmailAddresses_Enhhanced($list_id, $arr_subscribers);
        } else if ($this->input->post('Subscribers') == 'Soft bounced') {
            $arr_affected_subscribers = Subscribers::moveSoftBouncedSubscribers_Enhanced($list_id);
        } else {
            $arr_affected_subscribers = Subscribers::moveSubscribersOfSegment_Enhanced($list_id, $this->input->post('Subscribers'));
        }
        // If checked, add removed email addresses to suppression list - Start {

        $TotalCopied = 0;
        $TotalDuplicates = 0;
        $TotalFailed = 0;
        $TotalInValidEmails = 0;
        $TotalGlobalSuppressed = 0;
        $TotalListSupressed = 0;

        if (is_array($arr_affected_subscribers) && count($arr_affected_subscribers) > 0) {

            foreach ($arr_affected_subscribers as $Subscriber) {

                $result = $this->copy_subscriber($Subscriber, $list_id, $to_list_id, $this->array_user_information);
                if (!$result[0]) {
                    $TotalFailed++;
                    switch ($result[1]) {
                        case 1:
                            $TotalGlobalSuppressed++;
                            break;
                        case 2:
                            $TotalListSupressed++;
                            break;
                        case 3:
                            $TotalDuplicates++;
                            break;
                        case 4:
                            $TotalInValidEmails++;
                            break;
                        default:
                            break;
                    }
                } else {
                    $TotalCopied++;
                }
            }
            //mfrood a3ml update oempro_stats_activity 
            //add new field TotalFromCopy to destination list
            //Increment it for this day with $TotalCopied
            //If there are record get and update if not create one

            $ArrayToActivities = array(
                'TotalFromCopy' => $TotalCopied,
            );
            Subscribers::UpdateListActivityStatistics($to_list_id, $this->array_user_information['UserID'], $ArrayToActivities);


            if ($TotalCopied <= 0) {
                $msg = $this->get_copy_subscribers_msg($TotalCopied, $TotalFailed, $TotalDuplicates, $TotalGlobalSuppressed, $TotalListSupressed, $TotalInValidEmails);
                $_SESSION['PageMessageCache'] = array('Error', $msg);
                return false;
            }
        } else {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['9300']);
            return false;
        }
        // If checked, add removed email addresses to suppression list - End }

        $this->load->helper('url');
        $msg = $this->get_copy_subscribers_msg($TotalCopied, $TotalFailed, $TotalDuplicates, $TotalGlobalSuppressed, $TotalListSupressed, $TotalInValidEmails);
        $_SESSION['PageMessageCache'] = array('Success', $msg);
        redirect(InterfaceAppURL(true) . '/user/list/statistics/' . $list_id, 'location', '302');
    }

    /**
     * Remove subscribers event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_remove_subscribers($list_id) {
        $returnRemovedSubscribers = $this->input->post('AddToSuppresionList') == 'true' && $this->input->post('Subscribers') != 'Suppressed';

        $arr_removed_subscribers = array();
        if ($this->input->post('Subscribers') == 'Active') {
            $arr_removed_subscribers = Subscribers::RemoveActiveSubscribers($list_id, $returnRemovedSubscribers);
        } else if ($this->input->post('Subscribers') == 'Suppressed') {
            $arr_removed_subscribers = Subscribers::RemoveSuppressedSubscribers($list_id, $this->array_user_information['UserID']);
        } else if ($this->input->post('Subscribers') == 'Not opted in for days') {
            $arr_removed_subscribers = Subscribers::RemoveNotOptedInSubscribers($list_id, $this->input->post('NotOptedInForDays'), $returnRemovedSubscribers);
        } else if ($this->input->post('Subscribers') == 'Copy and paste') {
            $arr_removed_subscribers = explode("\n", $this->input->post('CopyAndPaste'));
            Subscribers::RemoveSubscribersByEmailAddresses($list_id, $arr_removed_subscribers);
        } else if ($this->input->post('Subscribers') == 'Soft bounced') {
            $arr_removed_subscribers = Subscribers::RemoveSoftBouncedSubscribers($list_id, $returnRemovedSubscribers);
        } else if ($this->input->post('Subscribers') == 'Hard bounced') {
            $arr_removed_subscribers = Subscribers::RemoveHardBouncedSubscribers($list_id, $returnRemovedSubscribers);
        } else {
            $arr_removed_subscribers = Subscribers::RemoveSubscribersOfSegment($list_id, $this->input->post('Subscribers'), $returnRemovedSubscribers);
        }

        // If checked, add removed email addresses to suppression list - Start {
        if ($returnRemovedSubscribers) {
            if (is_array($arr_removed_subscribers)) {
                foreach ($arr_removed_subscribers as $each) {
                    SuppressionList::Add(array(
                        'RelListID' => $list_id,
                        'RelOwnerUserID' => $this->array_user_information['UserID'],
                        'SuppressionSource' => 'User',
                        'EmailAddress' => $each
                    ));
                }
            }
        }
        // If checked, add removed email addresses to suppression list - End }

        $this->load->helper('url');
        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1125']);
        redirect(InterfaceAppURL(true) . '/user/list/statistics/' . $list_id, 'location', '302');
    }

    protected function _isHighriseIntegrationEnabled() {
        $highriseConfigMapper = O_Registry::instance()->getMapper('HighriseIntegrationConfig');
        $highriseIntegrationConfig = $highriseConfigMapper->findByUserId($this->array_user_information['UserID']);

        if (HIGHRISE_INTEGRATION_ENABLED && $highriseIntegrationConfig != FALSE && $highriseIntegrationConfig->isEnabled()) {
            return true;
        }

        return false;
    }

}

class Subscriber_import_wizard_functions {

    public static function step_pre_highrise() {
        $CI = & get_instance();

        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $userInformation = $wizard->get_extra_parameter('user_information');
        $listId = $wizard->get_extra_parameter('list_id');

        $highriseConfigMapper = O_Registry::instance()->getMapper('HighriseIntegrationConfig');
        $highriseIntegrationConfig = $highriseConfigMapper->findByUserId($userInformation['UserID']);

        if (!HIGHRISE_INTEGRATION_ENABLED || $highriseIntegrationConfig == FALSE || $highriseIntegrationConfig->isEnabled() == FALSE) {
            $CI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/subscribers/add/' . $listId, 'location', '302');
        }

        $connector = new O_Integration_Highrise_Connector(
                $highriseIntegrationConfig->getApiKey(), $highriseIntegrationConfig->getAccount()
        );

        $companies = $connector->getCompanies();
        $tags = $connector->getTags();

        $message = new Session_message();
        $message->set_parameter('Companies', $companies);
        $message->set_parameter('Tags', $tags);
        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    public static function step_pre_form() {
        $CI = & get_instance();

        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $userInformation = $wizard->get_extra_parameter('user_information');
        $listId = $wizard->get_extra_parameter('list_id');

        Core::LoadObject('custom_fields');

        $array_custom_fields = CustomFields::RetrieveFields(
                        array('*'), array(
                    'RelOwnerUserID' => $userInformation['UserID'],
                    array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $listId . ')', 'auto-quote' => false)
                        ), array('CustomFieldID' => 'ASC')
        );

        if (!$array_custom_fields)
            $array_custom_fields = array();

        foreach ($array_custom_fields as &$each_field) {
            $each_field['html'] = CustomFields::GenerateCustomFieldHTMLCode($each_field);
        }

        $message = new Session_message();
        $message->set_parameter('CustomFieldsHtml', $array_custom_fields);
        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    public static function step_pre_data_entry() {
        $CI = & get_instance();

        if ($CI->input->post('FormSubmit') == false) {
            $CI->form_validation->set_rules("CopyAndPasteData");

            $CI->form_validation->run();
        }
    }

    public static function step_post_data_entry() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI = & get_instance();

        // Field Validations - Start {
        $CI->form_validation->set_rules("CopyAndPasteData", ApplicationHeader::$ArrayLanguageStrings['Screen']['1181'], 'required');
        // Field Validations - End }
        // Run validation - Start {
        if ($CI->form_validation->run() == false) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];
            return false;
        }
        // Run validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.import',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => array(
                        'listid' => $CI->input->post('ListID'),
                        'importstep' => 1,
                        'importtype' => 'Copy',
                        'importdata' => $CI->input->post('CopyAndPasteData'),
                        'fieldterminator' => $CI->input->post('FieldTerminator'),
                        'fieldencloser' => $CI->input->post('FieldEncloser')
                    )
        ));

        if (!isset($_SESSION[SESSION_NAME]['ImportVariables']))
            $_SESSION[SESSION_NAME]['ImportVariables'] = array();

        $_SESSION[SESSION_NAME]['ImportVariables']['ImportID'] = $array_return['ImportID'];
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportType'] = 'Copy';
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportFields'] = $array_return['ImportFields'];
        return true;
    }

    public static function step_post_form() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $user_information = $wizard->get_extra_parameter('user_information');
        $list_id = $wizard->get_extra_parameter('list_id');

        $CI = & get_instance();

        // Field Validations - Start {
        $CI->form_validation->set_rules("EmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'], 'required');
        // Field Validations - End }
        // Run validation - Start {
        if ($CI->form_validation->run() == false) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];
            return false;
        }
        // Run validation - End }

        Core::LoadObject('subscribers');
        Core::LoadObject('lists');

        $list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id), false, false, array());

        $other_fields = array();

        $array_custom_fields = CustomFields::RetrieveFields(
                        array('*'), array(
                    'RelOwnerUserID' => $user_information['UserID'],
                    array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $list_id . ')', 'auto-quote' => false)
                        )
        );

        if (!$array_custom_fields)
            $array_custom_fields = array();

        foreach ($array_custom_fields as $each) {
            if (isset($_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']])) {
                if (strtolower(gettype($_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']])) == 'array') {
                    if ($each['FieldType'] == 'Time field') {
                        $NewValue = implode(':', $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']]) . ':00';
                    } else if ($each['FieldType'] == 'Date field') {
                        $NewValue = $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']][2] . '-' . $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']][1] . '-' . $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']][0];
                    } else {
                        $NewValue = implode('||||', $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']]);
                    }
                } else {
                    $NewValue = $_POST['FormValue_Fields']['CustomField' . $each['CustomFieldID']];
                }
                $other_fields['CustomField' . $each['CustomFieldID']] = $NewValue;
            }
        }

        $subscriptionOptions = array(
            'UserInformation' => $user_information,
            'ListInformation' => $list_information,
            'EmailAddress' => $CI->input->post('EmailAddress'),
            'OtherFields' => $other_fields,
            'UpdateIfDuplicate' => $CI->input->post('UpdateDuplicates') == false ? false : true,
            'ApplyBehaviors' => $CI->input->post('TriggerBehaviors') == false ? false : true,
            'SendConfirmationEmail' => $CI->input->post('DoNotSendOptInConfirmationEmail') == 'true' ? false : true,
            'SubscriptionStatus' => $CI->input->post('DoNotSendOptInConfirmationEmail') == 'true' ? 'Subscribed' : '',
            'UpdateStatistics' => $CI->input->post('TriggerBehaviors') == false ? false : true,
            'TriggerWebServices' => $CI->input->post('TriggerBehaviors') == false ? false : true,
            'TriggerAutoResponders' => $CI->input->post('TriggerBehaviors') == false ? false : true
        );

        $result = Subscribers::AddSubscriber_Enhanced($subscriptionOptions);

        $totalImported = 1;
        $totalData = 1;
        $totalDuplicates = 0;
        $totalFailed = 0;
        if ($result[0] == FALSE) {
            $error = $result[1];
            if ($error == 1 || $error == 2) {
                $totalFailed = 1;
                $totalImported = 0;
            }

            if ($error == 3 || $error == 4) {
                $totalDuplicates = 1;
                $totalImported = 0;
            }
        }

        $_SESSION[SESSION_NAME]['ImportResult'] = array($totalData, $totalImported, $totalDuplicates, $totalFailed);
        $_SESSION[SESSION_NAME]['ImportID'] = 0;
        return true;
    }

    public static function step_post_highrise() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $userInformation = $wizard->get_extra_parameter('user_information');
        $listId = $wizard->get_extra_parameter('list_id');

        $CI = & get_instance();

        $highriseConfigMapper = O_Registry::instance()->getMapper('HighriseIntegrationConfig');
        $highriseIntegrationConfig = $highriseConfigMapper->findByUserId($userInformation['UserID']);

        if (!HIGHRISE_INTEGRATION_ENABLED || $highriseIntegrationConfig == FALSE || $highriseIntegrationConfig->isEnabled() == FALSE) {
            $CI->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/subscribers/add/' . $listId, 'location', '302');
        }

        $connector = new O_Integration_Highrise_Connector(
                $highriseIntegrationConfig->getApiKey(), $highriseIntegrationConfig->getAccount()
        );

        $selection = $CI->input->post('ContactSelection');
        $selectionType = substr($selection, 0, 1);
        $selection = substr($selection, 1);
        if ($selectionType == 'c') {
            $contacts = $connector->getContactsByCompany($selection);
        } else if ($selectionType == 't') {
            $contacts = $connector->getContactsByTag($selection);
        } else {
            $contacts = $connector->getContacts();
        }

        $fileName = md5($userInformation['UserID'] . date('YmdHis'));

        $csvData = '';

        $fp = fopen(DATA_PATH . '/imports/' . $fileName, 'w');
        foreach ($contacts as $eachContact) {
            fputcsv($fp, $eachContact, ',', '"');
        }
        fclose($fp);

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.import',
                    'protected' => true,
                    'username' => $userInformation['Username'],
                    'password' => $userInformation['Password'],
                    'parameters' => array(
                        'listid' => $listId,
                        'importstep' => 1,
                        'importtype' => 'File',
                        'importfilename' => $fileName,
                        'fieldterminator' => ',',
                        'fieldencloser' => '"'
                    )
        ));

        $_SESSION[SESSION_NAME]['ImportVariables'] = array();
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportID'] = $array_return['ImportID'];
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportType'] = 'File';
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportFields'] = $array_return['ImportFields'];
        return true;
    }

    public static function step_pre_file_upload() {
        
    }

    public static function step_post_file_upload() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI = & get_instance();

        if ($_FILES['userfile']['name'] == '') {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1196'];

            return false;
        }
        $config = array();
        $config['upload_path'] = DATA_PATH . 'imports/';
        $config['allowed_types'] = 'csv|txt';
        $config['encrypt_name'] = true;
        $config['max_size'] = IMPORT_MAX_FILESIZE;

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload()) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['119'], round(IMPORT_MAX_FILESIZE / 1024));

            return false;
        } else {
            $file_data = $CI->upload->data();
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'subscribers.import',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'listid' => $CI->input->post('ListID'),
                            'importstep' => 1,
                            'importtype' => 'File',
                            'importfilename' => $file_data['file_name'],
                            'fieldterminator' => $CI->input->post('FieldTerminator'),
                            'fieldencloser' => $CI->input->post('FieldEncloser')
                        )
            ));

            $_SESSION[SESSION_NAME]['ImportVariables'] = array();
            $_SESSION[SESSION_NAME]['ImportVariables']['ImportID'] = $array_return['ImportID'];
            $_SESSION[SESSION_NAME]['ImportVariables']['ImportType'] = 'File';
            $_SESSION[SESSION_NAME]['ImportVariables']['ImportFields'] = $array_return['ImportFields'];
            return true;
        }
    }

    function step_pre_mysql() {
        
    }

    function step_post_mysql() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI = & get_instance();

        // Field validations - Start {
        $CI->form_validation->set_rules("Host", ApplicationHeader::$ArrayLanguageStrings['Screen']['1200'], 'required');
        $CI->form_validation->set_rules("Port", ApplicationHeader::$ArrayLanguageStrings['Screen']['1201'], 'required');
        $CI->form_validation->set_rules("Username", ApplicationHeader::$ArrayLanguageStrings['Screen']['1202'], 'required');
        $CI->form_validation->set_rules("Password");
        $CI->form_validation->set_rules("Database", ApplicationHeader::$ArrayLanguageStrings['Screen']['1204'], 'required');
        $CI->form_validation->set_rules("SQLQuery", ApplicationHeader::$ArrayLanguageStrings['Screen']['1205'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($CI->form_validation->run() == false) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];

            return false;
        }

        $blockedHosts = explode(',', BLOCKED_MYSQL_HOSTS);
        if (in_array($CI->input->post('Host'), $blockedHosts)) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1820'];

            return false;
        }
        // Run validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.import',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => array(
                        'listid' => $CI->input->post('ListID'),
                        'importstep' => 1,
                        'importtype' => 'MySQL',
                        'importmysqlhost' => $CI->input->post('Host'),
                        'importmysqlport' => $CI->input->post('Port'),
                        'importmysqlusername' => $CI->input->post('Username'),
                        'importmysqlpassword' => $CI->input->post('Password'),
                        'importmysqldatabase' => $CI->input->post('Database'),
                        'importmysqlquery' => $CI->input->post('SQLQuery'),
                    )
        ));

        if ($array_return['Success'] == false) {
            if ($array_return['ErrorCode'] == 15) {
                $_SESSION['PageMessageCache'][0] = 'Error';
                $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1206'];

                return false;
            } else if ($array_return['ErrorCode'] == 16) {
                $_SESSION['PageMessageCache'][0] = 'Error';
                $_SESSION['PageMessageCache'][1] = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1207'], $array_return['MySQLError']);

                return false;
            }
        }

        $_SESSION[SESSION_NAME]['ImportVariables'] = array();
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportID'] = $array_return['ImportID'];
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportType'] = 'MySQL';
        $_SESSION[SESSION_NAME]['ImportVariables']['ImportFields'] = $array_return['ImportFields'];
        return true;
    }

    public static function step_pre_field_mapping() {
        $message = new Session_message();

        $message->set_parameter('ImportFields', $_SESSION[SESSION_NAME]['ImportVariables']['ImportFields']);

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);

        unset($message);
    }

    public static function step_post_field_mapping() {
        $wizard = WizardFactory::get('Subscriber_import_wizard');
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI = & get_instance();

        $api_parameters = array(
            'listid' => $CI->input->post('ListID'),
            'importstep' => 2,
            'importid' => $_SESSION[SESSION_NAME]['ImportVariables']['ImportID'],
            'addtoglobalsuppressionlist' => $CI->input->post('AddToSuppressionList') == 'global' ? 'true' : 'false',
            'addtosuppressionlist' => $CI->input->post('AddToSuppressionList') == 'list' ? 'true' : 'false',
            'sendconfirmationemail' => $CI->input->post('DoNotSendOptInConfirmationEmail') == 'true' ? 'false' : 'true',
            'triggerbehaviors' => $CI->input->post('TriggerBehaviors') == false ? 'false' : 'true',
            'updateduplicates' => $CI->input->post('UpdateDuplicates') == false ? 'false' : 'true',
        );

        if (strtolower(gettype($_SESSION[SESSION_NAME]['ImportVariables']['ImportFields'])) === 'array') {
            for ($i = 1; $i < count($_SESSION[SESSION_NAME]['ImportVariables']['ImportFields']) + 1; $i++) {
                $api_parameters['mappedfields']['FIELD' . $i] = $CI->input->post('MatchedFields' . $i);
            }
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.import',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => $api_parameters
        ));

        if ($array_return['Success'] == false) {
            switch ($array_return['ErrorCode']) {
                case '6':
                    $CI->load->helper('url');
                    unset($_SESSION[SESSION_NAME]['ImportVariables']);
                    unset($_SESSION[SESSION_NAME]['ImportWizard']);
                    redirect(InterfaceAppURL(true) . '/user/subscribers/add/' . $CI->input->post('ListID'), 'location', '302');
                    break;
                case '7':
                    $_SESSION['PageMessageCache'][0] = 'Error';
                    $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1184'];
                    break;
                case '8':
                    $_SESSION['PageMessageCache'][0] = 'Error';
                    $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1184'];
                    break;
            }
            return false;
        }

        unset($_SESSION[SESSION_NAME]['ImportVariables']);
        unset($_SESSION[SESSION_NAME]['ImportWizard']);

        $_SESSION[SESSION_NAME]['ImportResult'] = array($array_return['TotalData'], $array_return['TotalImported'], $array_return['TotalDuplicates'], $array_return['TotalFailed'], $array_return['TotalLimited']);
        $_SESSION[SESSION_NAME]['ImportID'] = $array_return['ImportID'];
        return true;
    }

    public static function step_pre_result() {
        WizardFactory::delete('Subscriber_import_wizard');

        $message = new Session_message();

        $message->set_parameter('ImportResult', $_SESSION[SESSION_NAME]['ImportResult']);
        $message->set_parameter('ImportID', $_SESSION[SESSION_NAME]['ImportID']);
        $_SESSION[SESSION_NAME]['ImportResult'] = array();
        $_SESSION[SESSION_NAME]['ImportID'] = array();
        unset($_SESSION[SESSION_NAME]['ImportResult']);
        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    public static function step_post_result() {
        return true;
    }

}
