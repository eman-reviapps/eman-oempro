<?php
/**
 * Account controller
 *
 * @author Mert Hurturk
 */

class Controller_Account extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();
	
		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('api');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
		// Retrieve user information - End }
		
		// Check if user account has expired - Start {
		if (Users::IsAccountExpired($this->array_user_information) == true)
			{
			$_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/user/logout/', 'location', '302');
			}
		// Check if user account has expired - End }
	}

	/**
	 * Index controller
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function index()
	{
		// Privilege check - Start {
		if (Users::HasPermissions(array('User.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false)
		{
			header('Location: '.InterfaceAppURL(true) . '/user/integration/');
			exit;
		}
		// Privilege check - End }
		
		// Events - Start {
		if ($this->input->post('Command') == 'UpdateAccount')
		{
			$array_event_return = $this->_event_update_account();
		}
		// Events - End }

		// Interface parsing - Start {
		$array_view_data 	= array(
								'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseClients'],
								'CurrentMenuItem'		=> 'Settings',
								'SubSection'			=> 'Account',
								'UserInformation'		=> $this->array_user_information
								);
		$array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
		foreach ($array_event_return as $key=>$value)
		{
			$array_view_data[$key] = $value;
		}

		if (isset($array_event_return) == true)
		{
			$array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
		}

		// Check if there is any message in the message buffer - Start {
		if ($_SESSION['PageMessageCache'][1] != '')
		{
			$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('user/settings', $array_view_data);
		// Interface parsing - End }
	}

	/**
	 * Edit user account event
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function _event_update_account()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array(false, 'This feature is disabled in demo version.');
		}

		// Field validations - Start {
		$array_form_rules = array(
								array
									(
									'field'		=> 'Username',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
									'rules'		=> 'required',
									),
								array
									(
									'field'		=> 'EmailAddress',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
									'rules'		=> 'required|valid_email',
									),
								array
									(
									'field'		=> 'TimeZone',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0016'],
									'rules'		=> 'required',
									),
								array
									(
									'field'		=> 'Language',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0018'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'FirstName',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0021'],
									'rules'		=> 'required',
									),
								array
									(
									'field'		=> 'LastName',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0022'],
									'rules'		=> 'required',
									),
								array
									(
									'field'		=> 'CompanyName',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0023'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Website',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0299'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Street',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0025'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'City',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0026'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Zip',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0028'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'State',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0027'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Country',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0029'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Phone',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0030'],
									'rules'		=> '',
									),
								array
									(
									'field'		=> 'Fax',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0031'],
									'rules'		=> '',
									),
								);

		$this->form_validation->set_rules($array_form_rules);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false);
		}
		// Run validation - End }

		// Update user account - Start
		$array_api_vars = array(
							'UserID'						=> $this->input->post('UserID'),
							'EmailAddress'					=> $this->input->post('EmailAddress'),
							'Username'						=> $this->input->post('Username'),
							'FirstName'						=> $this->input->post('FirstName'),
							'LastName'						=> $this->input->post('LastName'),
							'TimeZone'						=> $this->input->post('TimeZone'),
							'Language'						=> $this->input->post('Language'),
							'ReputationLevel'				=> 'Trusted',
							'CompanyName'					=> $this->input->post('CompanyName'),
							'Website'						=> $this->input->post('Website'),
							'Street'						=> $this->input->post('Street'),
							'City'							=> $this->input->post('City'),
							'State'							=> $this->input->post('State'),
							'Zip'							=> $this->input->post('Zip'),
							'Country'						=> $this->input->post('Country'),
							'Phone'							=> $this->input->post('Phone'),
							'Fax'							=> $this->input->post('Fax'),
							'PreviewMyEmailAccount'			=> '',
							'PreviewMyEmailAPIKey'			=> ''
							);
		if ($this->input->post('NewPassword') != '')
		{
			$array_api_vars['Password'] = $this->input->post('NewPassword');
		}

		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'user.update',
			'protected'	=>	true,
			'access'	=>	'user',
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$array_api_vars
			));

		if ($array_return['Success'] == false)
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return["ErrorCode"]);
			switch ($error_code)
			{
				case '1':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $error_code),
								);
					break;
				case '2':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $error_code),
								);
					break;
				case '3':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $error_code),
								);
					break;
				case '4':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $error_code),
								);
					break;
				case '5':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $error_code),
								);
					break;
				case '6':
					return array(
								false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1791'], 'User.Update', $error_code),
								);
					break;
				default:
					break;
			}
		}
		else
		{
			// API Success
			return array(
						true, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0328'], $this->array_user_information['FirstName'].' '.$this->array_user_information['LastName']),
						);
		}
		// Update user account - End		
	}
}
?>