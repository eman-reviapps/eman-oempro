<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Integration extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Cem Hurturk
	 */
	public function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('admins');
		Core::LoadObject('user_auth');
		Core::LoadObject('api');
		// Load other modules - End

		// Check the login session, redirect based on the login session status - Start
		UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve administrator information - Start {
		$this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
		// Retrieve administrator information - End }
	}

	/**
	 * Index controller
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	public function index()
	{
		// Interface parsing - Start
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserIntegration'],
			'CurrentMenuItem' => 'Settings',
			'SubSection' => 'Integration',
			'UserInformation' => $this->ArrayUserInformation
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		$this->render('user/settings', $ArrayViewData);
		// Interface parsing - End
	}

	public function wufoo($command = '', $integrationId = 0)
	{
		if (!WUFOO_INTEGRATION_ENABLED) {
			$this->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/integration');
			exit;
		}

		include_once(LIBRARY_PATH . 'wufoo/WufooApiWrapper.php');

		$step = 1;
		$forms = array();
		$wufooFields = array();
		$oemproFields = array();
		$integrations = array();
		$lastIntegration = false;
		$errorMessage = false;
		$successMessage = false;

		Core::LoadObject('lists');
		$lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID']));

		// Events - Start {
		if ($command == '') {
			$integrationMapper = O_Registry::instance()->getMapper('WufooIntegration');
			$integrations = $integrationMapper->findAllByUserId($this->ArrayUserInformation['UserID']);
		} else if ($command == 'delete' && $integrationId != 0) {
			$integrationMapper = O_Registry::instance()->getMapper('WufooIntegration');
			$integration = $integrationMapper->findById($integrationId);
			try {
				$wrapper = new WufooApiWrapper($integration->getApiKey(), $integration->getWufooSubDomain());
				$wrapper->webHookDelete($integration->getFormHash(), $integration->getWebhookHash());
			} catch (Exception $e) {
				// An exception might be thrown if user deletes wufoo form or
				// removes webhook connection through wufoo interface.
			}
			$integrationMapper->delete($integrationId, $this->ArrayUserInformation['UserID']);
			$this->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/integration/wufoo/');
			exit;
		} else if ($command == 'form' && !$this->input->post('Command')) {
			$integrationMapper = O_Registry::instance()->getMapper('WufooIntegration');
			unset($_SESSION[SESSION_NAME]['WufooIntegration']);
			$_SESSION[SESSION_NAME]['WufooIntegration'] = array(
				'WufooAPIKey' => '',
				'WufooSubdomain' => '',
				'WufooForm' => '',
				'OemproList' => ''
			);
			$lastIntegration = $integrationMapper->getLastIntegration($this->ArrayUserInformation['UserID']);
		} else if ($command == 'form' && $this->input->post('Command') == 'RetrieveForms') {
			$arrayFormRules = array();
			$arrayFormRules[] = array(
				'field' => 'WufooAPIKey',
				'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0208'],
				'rules' => 'required',
			);
			$arrayFormRules[] = array(
				'field' => 'WufooSubdomain',
				'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1725'],
				'rules' => 'required',
			);
			$this->form_validation->set_rules($arrayFormRules);

			if ($this->form_validation->run() != FALSE) {
				try {
					$wrapper = new WufooApiWrapper($this->input->post('WufooAPIKey'), $this->input->post('WufooSubdomain'));
					$forms = $wrapper->getForms();
					$_SESSION[SESSION_NAME]['WufooIntegration']['WufooAPIKey'] = $this->input->post('WufooAPIKey');
					$_SESSION[SESSION_NAME]['WufooIntegration']['WufooSubdomain'] = $this->input->post('WufooSubdomain');
					$step = 2;
				} catch (WufooException $e) {
					if ($e->getCode() == 6) {
						$errorMessage = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1733'], $this->input->post('WufooSubdomain'));
					} else {
						$errorMessage = $e->getMessage();
					}
				} catch (Exception $e) {
					$errorMessage = $e->getMessage();
				}
			}

		} else if ($command == 'form' && $this->input->post('Command') == 'RetrieveFields') {
			list($formHash, $formName) = explode('-', $this->input->post('WufooForm'));
			$_SESSION[SESSION_NAME]['WufooIntegration']['WufooFormHash'] = $formHash;
			$_SESSION[SESSION_NAME]['WufooIntegration']['WufooFormName'] = $formName;
			$_SESSION[SESSION_NAME]['WufooIntegration']['OemproList'] = $this->input->post('OemproList');
			$wrapper = new WufooApiWrapper($_SESSION[SESSION_NAME]['WufooIntegration']['WufooAPIKey'], $_SESSION[SESSION_NAME]['WufooIntegration']['WufooSubdomain']);
			Core::LoadObject('custom_fields');
			$oemproFields = CustomFields::RetrieveFields(array('*'),
				array('RelOwnerUserID' => $this->ArrayUserInformation['UserID']));
			$fields = $wrapper->getFields($formHash);

			$wufooFields = array();
			foreach ($fields->Fields as $id => $field) {
				if (!preg_match('/^Field[\d]+$/', $id)) continue;

				if ($field->Type == 'address') {
					foreach ($field->SubFields as $eachSubField) {
						$wufooFields[] = array(
							'id' => $eachSubField->ID,
							'type' => $field->Type,
							'title' => $eachSubField->Label,
							'value' => $eachSubField->ID
						);
					}
				} else {
					$value = $field->ID;
					if (count($field->SubFields) > 0) {
						$value = array();
						foreach ($field->SubFields as $eachSubField) {
							$value[] = $eachSubField->ID;
						}
						$value = implode(',', $value);
					}
					$wufooFields[] = array(
						'id' => $field->ID,
						'type' => $field->Type,
						'title' => $field->Title,
						'value' => $value
					);
				}
			}

			$step = 3;
		} else if ($command == 'form' && $this->input->post('Command') == 'MapFields') {
			$integration = new O_Domain_WufooIntegration();
			$integration->setApiKey($_SESSION[SESSION_NAME]['WufooIntegration']['WufooAPIKey']);
			$integration->setWufooSubDomain($_SESSION[SESSION_NAME]['WufooIntegration']['WufooSubdomain']);
			$integration->setFormHash($_SESSION[SESSION_NAME]['WufooIntegration']['WufooFormHash']);
			$integration->setFormName($_SESSION[SESSION_NAME]['WufooIntegration']['WufooFormName']);
			$integration->setListId($_SESSION[SESSION_NAME]['WufooIntegration']['OemproList']);
			$integration->setUserId($this->ArrayUserInformation['UserID']);

			$fieldMapping = array();
			foreach ($this->input->post('WufooFieldMapping') as $key => $value) {
				if ($value == '') continue;
				$fieldMapping[] = '[((' . $key . '=>' . $value . '))]';
			}

			$integration->setFieldMapping(implode(',*,*,', $fieldMapping));

			$integrationMapper = O_Registry::instance()->getMapper('WufooIntegration');
			$integrationMapper->insert($integration);

			$wrapper = new WufooApiWrapper($integration->getApiKey(), $integration->getWufooSubDomain());
			$response = $wrapper->webHookPut($integration->getFormHash(), APP_URL . 'wufoo_webhook.php', (string) $integration->getHandshakeKey(), true);

			$integration->setWebhookHash($response->Hash);
			$integrationMapper->updateWebhookHash($integration);

			$_SESSION[SESSION_NAME]['WufooIntegration'] = array();
			unset($_SESSION[SESSION_NAME]['WufooIntegration']);

			$this->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/integration/wufoo/');
			exit;
		}
		// Events - End }

		// Interface parsing - Start
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserIntegration'] . ' - Wufoo',
			'CurrentMenuItem' => 'Settings',
			'SubSection' => $command == '' ? 'IntegrationWufoo' : 'IntegrationWufooForm',
			'UserInformation' => $this->ArrayUserInformation,
			'Step' => $step,
			'Forms' => $forms,
			'WufooFields' => $wufooFields,
			'OemproFields' => $oemproFields,
			'Lists' => $lists,
			'Integrations' => $integrations,
			'LastIntegration' => $lastIntegration,
			'ErrorMessage' => $errorMessage
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		$this->render('user/settings', $ArrayViewData);
		// Interface parsing - End
	}

	public function highrise()
	{
		if (!HIGHRISE_INTEGRATION_ENABLED) {
			$this->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/integration');
			exit;
		}

		$configMapper = O_Registry::instance()->getMapper('HighriseIntegrationConfig');
		$integrationConfig = $configMapper->findByUserId($this->ArrayUserInformation['UserID']);

		// Events - Start {
		$pageSuccessMessage = '';
		$pageErrorMessage = '';
		if ($this->input->post('Command') == 'EditHighriseSettings') {
			if (!$this->input->post('HighriseAPIKey')
					|| !$this->input->post('HighriseAccount')) {
				$pageErrorMessage = ApplicationHeader::$ArrayLanguageStrings['Screen']['1740'];
			} else {
				$connector = new O_Integration_Highrise_Connector(
					$this->input->post('HighriseAPIKey'),
					$this->input->post('HighriseAccount')
				);
				if (! $connector->validateAccount()) {
					$pageErrorMessage = ApplicationHeader::$ArrayLanguageStrings['Screen']['1739'];
				} else {
					if (! $integrationConfig) {
						$integrationConfig = new O_Domain_HighriseIntegrationConfig();
						$integrationConfig->setAccount($this->input->post('HighriseAccount'));
						$integrationConfig->setAPIKey($this->input->post('HighriseAPIKey'));
						$integrationConfig->setUserId($this->ArrayUserInformation['UserID']);
						$configMapper->insert($integrationConfig);

						$pageSuccessMessage = ApplicationHeader::$ArrayLanguageStrings['Screen']['1738'];
					} else {
						$integrationConfig->setAccount($this->input->post('HighriseAccount'));
						$integrationConfig->setAPIKey($this->input->post('HighriseAPIKey'));
						$integrationConfig->isEnabled($this->input->post('IsEnabled') == '1');
						$configMapper->update($integrationConfig);
						$pageSuccessMessage = ApplicationHeader::$ArrayLanguageStrings['Screen']['1738'];
					}
				}
			}


		}
		// Events - End }


		// Interface parsing - Start
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserIntegration'] . ' - Highrise',
			'CurrentMenuItem' => 'Settings',
			'SubSection' => 'IntegrationHighrise',
			'UserInformation' => $this->ArrayUserInformation,
			'OemproFields' => $oemproFields,
			'Lists' => $lists,
			'PageSuccessMessage' => $pageSuccessMessage,
			'PageErrorMessage' => $pageErrorMessage,
			'IntegrationConfig' => $integrationConfig
			
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		$this->render('user/settings', $ArrayViewData);
		// Interface parsing - End
	}

	public function pme()
	{
		if (PME_USAGE_TYPE != 'User') {
			$this->load->helper('url');
			redirect(InterfaceAppURL(true) . '/user/integration');
			exit;
		}

		// Events - Start {
		if ($this->input->post('Command') == 'EditPMESettings') {
			$ArrayEventReturn = $this->_EventEditPMEIntegrationSettings();
		}
		// Events - End }

		// Interface parsing - Start
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserIntegration'] . ' - PreviewMyEmail',
			'CurrentMenuItem' => 'Settings',
			'SubSection' => 'IntegrationPreviewMyEmail',
			'UserInformation' => $this->ArrayUserInformation
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		foreach ($ArrayEventReturn as $Key => $Value)
		{
			$ArrayViewData[$Key] = $Value;
		}

		$this->render('user/settings', $ArrayViewData);
		// Interface parsing - End
	}

	/**
	 * Integration settings update event
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	protected function _EventEditPMEIntegrationSettings()
	{
		if (DEMO_MODE_ENABLED == true) {
			return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}

		// Field validations - Start {
		$ArrayFormRules = array();
		$ArrayFormRules[] = array
		(
			'field' => 'PreviewMyEmailAPIKey',
			'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0208'],
			'rules' => 'required',
		);


		$this->form_validation->set_rules($ArrayFormRules);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false) {
			return array(false);
		}
		// Run validation - End }

		// Update settings - Start {
		$ArrayAPIVars = array(
			'PreviewMyEmailAPIKey' => $this->input->post('PreviewMyEmailAPIKey'),
			'UserID' => $this->ArrayUserInformation['UserID']
		);

		$ArrayReturn = API::call(array(
									  'format' => 'array',
									  'command' => 'user.update',
									  'access' => 'user',
									  'protected' => true,
									  'username' => $this->ArrayUserInformation['Username'],
									  'password' => $this->ArrayUserInformation['Password'],
									  'parameters' => $ArrayAPIVars
								 ));

		if ($ArrayReturn['Success'] == false) {
			// API Error occurred
			$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn['ErrorCode'][0] : $ArrayReturn['ErrorCode']);

			switch ($ErrorCode)
			{
				case '4':
					$this->form_validation->_field_data['PreviewMyEmailAPIKey']['error'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0629'];
					return array(
						'PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0341'],
					);
					break;
				default:
					return array(
						'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
					);
					break;
			}
		}
		else
		{
			// API Success
			return array(
				'PageSuccessMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0339'],
			);
		}
		// Update settings - End }
	}
} // end of class User
?>