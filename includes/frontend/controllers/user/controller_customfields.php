<?php

/**
 * Customfields Controller
 */
class Controller_Customfields extends MY_Controller {

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('custom_fields');
        Core::LoadObject('lists');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Browse custom fields function
     * @author Mert Hurturk
     * */
    function browse($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('CustomFields.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }

        $is_create_event = false;

        // Events - Start {
        if ($this->input->post('Command') == 'DeleteCustomFields') {
            if (Users::HasPermissions(array('CustomFields.Delete'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_delete_customfields($list_id);
            }
        } else if ($this->input->post('Command') == 'Create') {
            $is_create_event = true;
            if (Users::HasPermissions(array('CustomField.Create'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_create_event_return = $this->_event_create_customfields($list_id);
            }
        }
        // Events - End }
        // Retrieve custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            // Error occurred
        } else {
            $array_customfields = $array_return['CustomFields'];
            $total_customfield_count = $array_return['TotalCustomFields'] == NULL ? 0 : $array_return['TotalCustomFields'];
            unset($array_return);
        }
        // Retrieve custom fields - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseCustomFields'],
            'UserInformation' => $this->array_user_information,
            'CustomFields' => $array_customfields,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'CustomFields',
            'IsEditEvent' => false,
            'IsCreateEvent' => $is_create_event,
            'ListID' => $list_id
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        if (isset($array_event_return)) {
            foreach ($array_event_return as $key => $value) {
                $array_view_data[$key] = $value;
            }
        }

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        if (isset($array_create_event_return) == true) {
            $array_view_data[($array_create_event_return[0] == false ? 'PageCreateErrorMessage' : 'PageCreateSuccessMessage')] = $array_create_event_return[1];
        }

        $this->render('user/custom_fields', $array_view_data);
    }

    /**
     * Copy custom fields function
     * @author Mert Hurturk
     * */
    function copy($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('CustomField.Create'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        if ($this->input->post('Command') == 'Copy') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'customfields.copy',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'sourcelistid' => $this->input->post('SourceListID'),
                            'targetlistid' => $array_list_information['ListID']
                        )
            ));

            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1209']);
            redirect(InterfaceAppURL(true) . '/user/customfields/browse/' . $list_id, 'location', '302');
        }
        // Events - End }
        // Retrieve lists - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            $array_lists = array();
        } else {
            $array_lists = $array_return['Lists'];
            unset($array_return);
        }

        // Remove self from lists array - Start {
        if (count($array_lists) > 0) {
            foreach ($array_lists as $key => $value) {
                if ($array_lists[$key]['ListID'] == $array_list_information['ListID'])
                    unset($array_lists[$key]);
            }
        }
        // Remove self from lists array - End }
        // Retrieve lists - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCopyCustomFields'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'Lists' => $array_lists,
            'ActiveListItem' => 'CustomFields'
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/custom_fields_copy', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Edit custom field function
     * @author Mert Hurturk
     * */
    function edit($list_id, $custom_field_id) {
        // Get custom field information - Start {
        $custom_field = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $custom_field_id, 'RelOwnerUserID' => $this->array_user_information['UserID']));
        $custom_field['Options'] = str_replace('*', '', $custom_field['FieldOptions']);
        foreach ($custom_field['ArrayOptions'] as $key => $each) {
            if ($each['is_selected'] == 'true')
                $array_selected_options[] = $key;
        }
        $custom_field['SelectedOptions'] = implode(',', $array_selected_options);
        unset($array_selected_options);
        // Get $custom_field information - End }
        // Events - Start {
        $array_event_data = array();
        if ($this->input->post('Command') == 'Edit') {
            if (Users::HasPermissions(array('CustomField.Update'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_edit_customfield($list_id, $custom_field_id);
                if (isset($array_event_return) == true) {
                    $array_event_data[($array_event_return[0] == false ? 'PageEditErrorMessage' : 'PageEditSuccessMessage')] = $array_event_return[1];
                }
            }
        }
        // Events - End }
        // Retrieve custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            // Error occurred
        } else {
            $array_customfields = $array_return['CustomFields'];
            $total_customfield_count = $array_return['TotalCustomFields'] == NULL ? 0 : $array_return['TotalCustomFields'];
            unset($array_return);
        }
        // Retrieve custom fields - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseCustomFields'],
            'UserInformation' => $this->array_user_information,
            'CustomFields' => $array_customfields,
            'CustomField' => $custom_field,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'CustomFields',
            'IsEditEvent' => true,
            'ListID' => $list_id
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        $array_view_data = array_merge($array_view_data, $array_event_data);

        if (isset($array_event_data)) {
            foreach ($array_event_data as $key => $value) {
                $array_view_data[$key] = $value;
            }
        }

        $this->render('user/custom_fields', $array_view_data);
        // Parse interface - End }
    }

    /**
     * Copy custom fields function
     * @author Mert Hurturk
     * */
    function presets($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('CustomField.Create'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        if ($this->input->post('Command') == 'Create') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'customfield.create',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'subscriberlistid' => $array_list_information['ListID'],
                            'presetname' => $this->input->post('Preset')
                        )
            ));

            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1213']);
            redirect(InterfaceAppURL(true) . '/user/customfields/browse/' . $list_id, 'location', '302');
        }
        // Events - End }
        global $ArrayCustomFieldPresets;

        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCopyCustomFields'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'CustomFieldPresets' => $ArrayCustomFieldPresets,
            'ActiveListItem' => 'CustomFields'
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/custom_fields_presets', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Create auto responder function
     * @author Mert Hurturk
     * */
    function _event_create_customfields($list_id) {
        // Field validations - Start {
        $this->form_validation->set_rules("Options");
        $this->form_validation->set_rules("SelectedOptions");
        $this->form_validation->set_rules("FieldType");
        $this->form_validation->set_rules("ValidationMethod");
        $this->form_validation->set_rules("FieldName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1216'], 'required');
        if ($this->input->post('ValidationMethod') == 'Date' || $this->input->post('ValidationMethod') == 'Time' || $this->input->post('ValidationMethod') == 'Custom') {
            $this->form_validation->set_rules("ValidationRule", ApplicationHeader::$ArrayLanguageStrings['Screen']['1244'], 'required');
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create custom field - Start {
        $api_parameters = array(
            'subscriberlistid' => $list_id,
            'fieldname' => $this->input->post('FieldName'),
            'fieldtype' => $this->input->post('FieldType'),
            'validationmethod' => $this->input->post('FieldType') == 'Date field' || $this->input->post('FieldType') == 'Time field' ? 'Disabled' : $this->input->post('ValidationMethod'),
            'Visibility' => $this->input->post('Visibility'),
            'IsRequired' => $this->input->post('IsRequired') == false ? 'No' : 'Yes',
            'IsUnique' => $this->input->post('IsUnique') == false ? 'No' : 'Yes',
            'IsGlobal' => $this->input->post('IsGlobal') == false ? 'No' : 'Yes'
        );

        if ($this->input->post('FieldType') == 'Single line' || $this->input->post('FieldType') == 'Paragraph text' || $this->input->post('FieldType') == 'Hidden field') {
            $api_parameters['defaultvalue'] = $this->input->post('FieldDefaultValue');
        } else if ($this->input->post('FieldType') == 'Date field') {
            $api_parameters['years'] = implode('-', $this->input->post('DateFieldYears'));
        } else {
            $tmp_options = explode(',,,', $this->input->post('Options'));
            $tmp_counter = 0;
            foreach ($tmp_options as $each_option) {
                $tmp_option = explode(']||[', $each_option);
                $api_parameters['OptionLabel'][$tmp_counter] = trim($tmp_option[0], '[');
                $api_parameters['OptionValue'][$tmp_counter] = trim($tmp_option[1], ']');
                $tmp_counter++;
            }
            if ($this->input->post('SelectedOptions') == '') {
                $api_parameters['OptionSelected'] = '';
            } else {
                $api_parameters['OptionSelected'] = explode(',', $this->input->post('SelectedOptions'));
            }
        }

        if ($this->input->post('ValidationMethod') == 'Custom') {
            $api_parameters['validationrule'] = $this->input->post('ValidationRule');
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfield.create',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => $api_parameters
        ));
        // Create custom field - End }

        if ($array_return['Success'] == true) {
            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1248']);
            redirect(InterfaceAppURL(true) . '/user/customfields/browse/' . $list_id, 'location', '302');
        } else {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
        }
    }

    /**
     * Create auto responder function
     * @author Mert Hurturk
     * */
    function _event_edit_customfield($list_id) {
        // Field validations - Start {
        $this->form_validation->set_rules("Options");
        $this->form_validation->set_rules("SelectedOptions");
        $this->form_validation->set_rules("FieldType");
        $this->form_validation->set_rules("ValidationMethod");
        $this->form_validation->set_rules("FieldName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1216'], 'required');
        if ($this->input->post('ValidationMethod') == 'Custom') {
            $this->form_validation->set_rules("ValidationRule", ApplicationHeader::$ArrayLanguageStrings['Screen']['1244'], 'required');
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Save custom field - Start {
        $api_parameters = array(
            'subscriberlistid' => $list_id,
            'customfieldid' => $this->input->post('CustomFieldID'),
            'fieldname' => $this->input->post('FieldName'),
            'fieldtype' => $this->input->post('FieldType'),
            'validationmethod' => $this->input->post('ValidationMethod'),
            'Visibility' => $this->input->post('Visibility'),
            'IsRequired' => $this->input->post('IsRequired') == false ? 'No' : 'Yes',
            'IsUnique' => $this->input->post('IsUnique') == false ? 'No' : 'Yes',
            'IsGlobal' => $this->input->post('IsGlobal') == false ? 'No' : 'Yes'
        );

        if ($this->input->post('FieldType') == 'Single line' || $this->input->post('FieldType') == 'Paragraph text' || $this->input->post('FieldType') == 'Hidden field') {
            $api_parameters['defaultvalue'] = $this->input->post('FieldDefaultValue');
        } else if ($this->input->post('FieldType') == 'Date field') {
            $api_parameters['years'] = implode('-', $this->input->post('DateFieldYears'));
        } else {
            $tmp_options = explode(',,,', $this->input->post('Options'));
            $tmp_counter = 0;
            foreach ($tmp_options as $each_option) {
                $tmp_option = explode(']||[', $each_option);
                $api_parameters['OptionLabel'][$tmp_counter] = trim($tmp_option[0], '[');
                $api_parameters['OptionValue'][$tmp_counter] = trim($tmp_option[1], ']');
                $tmp_counter++;
            }

            $api_parameters['OptionSelected'] = explode(',', $this->input->post('SelectedOptions'));
        }

        if ($this->input->post('ValidationMethod') == 'Custom') {
            $api_parameters['validationrule'] = $this->input->post('ValidationRule');
        }
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfield.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => $api_parameters
        ));
        // Save custom field - End }

        if ($array_return['Success'] == true) {
            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1249']);
            redirect(InterfaceAppURL(true) . '/user/customfields/browse/' . $list_id, 'location', '302');
        } else {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
        }
    }

    /**
     * Delete custom fields function
     * @author Mert Hurturk
     * */
    function _event_delete_customfields($list_id) {
        // Field validations - Start {
        $array_form_rules = array(
            array
                (
                'field' => 'SelectedCustomFields[]',
                'label' => 'customfields',
                'rules' => 'required',
            ),
        );

        $this->form_validation->set_rules($array_form_rules);
        $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1165']);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, validation_errors());
        }
        // Run validation - End }
        // Delete custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'object',
                    'command' => 'customfields.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'customfields' => implode(',', $this->input->post('SelectedCustomFields'))
                    )
        ));

        if ($array_return->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($array_return->ErrorCode)) == 'array' ? $array_return->ErrorCode[0] : $array_return->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1165']);
                    break;
                default:
                    break;
            }
        } else {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1166']);
            return true;
        }
        // Delete custom fields - End }
    }

}

?>