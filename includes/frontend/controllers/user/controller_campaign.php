<?php

/**
 * Campaigns controller
 *
 * @author Mert Hurturk
 */
class Controller_Campaign extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('statistics');
        Core::LoadObject('split_tests');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->ArrayUserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Duplicates an already sent campaign and resends it
     *
     */
    function resend($CampaignID) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Campaign.Create'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        if (AccountIsUnTrustedCheck($this->ArrayUserInformation)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        // User privilege check - End }
        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('subscribers');
        Core::LoadObject('lists');
        Core::LoadObject('emails');
        Core::LoadObject('campaigns');
        Core::LoadObject('personalization');
        // Load other modules - End
        // Retrieve campaign information - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.get',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'campaignid' => $CampaignID
                    )
        ));
        $CampaignInformation = $ArrayReturn['Campaign'];
        unset($ArrayReturn);
        // Retrieve campaign information - End }

        if (count($CampaignInformation) == 0) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }

        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.copy',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'campaignid' => $CampaignID
                    )
        ));

        $NewCampaignID = $ArrayReturn['NewCampaignID'];

        unset($ArrayReturn);

        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/campaign/edit/' . $NewCampaignID, 'location', '302');
        return;
    }

    /**
     * Overview controller
     *
     * @author Mert Hurturk
     */
    function overview($CampaignID) {
        if (Users::HasPermissions(array('Campaign.Get'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }


        $EstimatedRecipients = Campaigns::EstimatedCampaignRecipients($CampaignID);
        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }


        Core::LoadObject('spark');

        $CampaignMetrics = array();
        $result = Spark::deliverabilityByCampaign('2017-01-01T00:00', null, $CampaignID);
        if ($result[0]) {
            $CampaignMetrics = (array) $result[1][$CampaignID];
        }

        $CampaignInformation = $CampaignInformation[0];

        $TotalBounces = isset($CampaignMetrics) && isset($CampaignMetrics['count_bounce']) ? $CampaignMetrics['count_bounce'] : ($CampaignInformation['TotalHardBounces'] + $CampaignInformation['TotalSoftBounces']);
        $TotalHardBounces = isset($CampaignMetrics) && isset($CampaignMetrics['count_hard_bounce']) ? $CampaignMetrics['count_hard_bounce'] : $CampaignInformation['TotalHardBounces'];
        $TotalSpams = isset($CampaignMetrics) && isset($CampaignMetrics['count_spam_complaint']) ? $CampaignMetrics['count_spam_complaint'] : (Statistics::RetrieveSPAMComplaintCountOfCampaign($CampaignInformation['CampaignID']));

        $TotalSent = isset($CampaignMetrics) && isset($CampaignMetrics['count_sent']) ? $CampaignMetrics['count_sent'] : $CampaignInformation['TotalSent'];
        $TotalRecipients = isset($CampaignMetrics) && isset($CampaignMetrics['count_delivered']) ? $CampaignMetrics['count_delivered'] : $CampaignInformation['TotalRecipients'];
        $TotalFailed = $TotalSent - $TotalRecipients;

        $MostClickedLinks = array();
        $MostClickedLinksResultSet = Statistics::RetrieveCampaignLinkClicks($CampaignID, $this->ArrayUserInformation['UserID']);
        while ($ClickedLink = mysql_fetch_assoc($MostClickedLinksResultSet)) {
            $MostClickedLinks[] = $ClickedLink;
        }
        $HighestOpens = Statistics::RetrieveOpenPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
        $HighestClicks = Statistics::RetrieveClickPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);

        $opened_ratio = round(((100 * $CampaignInformation['TotalOpens']) / $TotalSent), 1);
        $clicked_ratio = round(((100 * $CampaignInformation['TotalClicks']) / $TotalSent), 1);
        $forwarded_ratio = round(((100 * $CampaignInformation['TotalForwards']) / $TotalSent), 1);
        $browser_views_ratio = round(((100 * $CampaignInformation['TotalViewsOnBrowser']) / $TotalSent), 1);
        $unsubscription_ratio = round(((100 * $CampaignInformation['TotalUnsubscriptions']) / $TotalSent), 1);

        $bounce_ratio = round(((100 * $TotalBounces) / $TotalSent), 1);
        $hard_bounce_ratio = round(((100 * $TotalHardBounces) / $TotalSent), 1);
        $spam_ratio = round(((100 * $TotalSpams) / $TotalSent), 1);

        $not_opened_ratio = 100 - ($opened_ratio + $hard_bounce_ratio);
        $CampaignInformation['TotalBounces'] = $TotalBounces;
        $CampaignInformation['TotalHardBounces'] = $TotalHardBounces;

        $CampaignInformation['HardBounceRatio'] = $hard_bounce_ratio;
        $CampaignInformation['SoftBounceRatio'] = number_format(($CampaignInformation['TotalSoftBounces'] * 100) / $CampaignInformation['TotalBounces']);
        $CampaignInformation['TotalSpamReports'] = $TotalSpams;

        $CampaignInformation['TotalSent'] = $TotalSent;
        $CampaignInformation['TotalRecipients'] = $TotalRecipients;
        $CampaignInformation['TotalFailed'] = $TotalFailed;

        $SplitTestEmails = array();
        if ($CampaignInformation['SplitTest'] != false) {
            $SplitTestEmails = Emails::RetrieveEmailsOfTest($CampaignInformation['SplitTest']['TestID'], $CampaignInformation['CampaignID'], true);

            $WinnerEmail = false;
            if ($CampaignInformation['SplitTest']['RelWinnerEmailID'] != 0) {
                $WinnerEmail = Emails::RetrieveEmail(array('EmailName'), array('EmailID' => $CampaignInformation['SplitTest']['RelWinnerEmailID']), false);
                $WinnerEmail = $WinnerEmail['EmailName'];
            }
        }
        Core::LoadObject('user_auth');
        Core::LoadObject('emails');
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.get',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'emailid' => $CampaignInformation['RelEmailID']
                    )
        ));
        $email_information = $array_return['EmailInformation'];

        // Retrieve lists and segments - Start {
        // Load other modules - Start
        Core::LoadObject('lists');
        Core::LoadObject('segments');
        // Load other modules - End

        $CampaignRecipients = array();
        foreach ($CampaignInformation['RecipientSegments'] as $Segment) {
            $CampaignRecipients[] = $Segment[0] . ':' . $Segment[1];
        }
        // Retrieve lists and segments - End }
        // Get recipient list ids - Start {
        $ArrayRecipientListIDs = array();
        foreach ($CampaignRecipients as $Each) {
            $TempArray = explode(':', $Each);

            $ArrayRecipientListIDs[] = $TempArray[0];
        }
        $ArrayRecipientListIDs = array_unique($ArrayRecipientListIDs);

        foreach ($ArrayRecipientListIDs as $ListID) {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'list.get',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => array(
                            'listid' => $ListID
                        )
            ));
            $list_name = '';
            if ($array_return['Success'] != false) {
                $list_name = $array_return['List']['Name'];
            }

            $ArrayRecipientLists[] = array("ListID" => $ListID, "Name" => $list_name);
        }
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignOverview'],
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'CampaignMetrics' => $CampaignMetrics,
            'EmailInformation' => $email_information,
            'ArrayRecipientLists' => $ArrayRecipientLists,
            'WinnerEmail' => $WinnerEmail,
            'SplitTestEmails' => $SplitTestEmails,
            'ChartColors' => explode(',', CHART_COLORS),
            'MostClickedLinks' => $MostClickedLinks,
            'HighestOpens' => $HighestOpens,
            'HighestClicks' => $HighestClicks,
            'CurrentMenuItem' => 'Campaigns',
            'SubSection' => 'Overview',
            'OpenedRatio' => $opened_ratio,
            'NotOpenedRatio' => $not_opened_ratio,
            'HardBouncedRatio' => $hard_bounce_ratio,
            'BounceRatio' => $bounce_ratio,
            'ClickedRatio' => $clicked_ratio,
            'ForwardedRatio' => $forwarded_ratio,
            'BrowserViewsRatio' => $browser_views_ratio,
            'UnsubscriptionRatio' => $unsubscription_ratio,
            'SpamRatio' => $spam_ratio,
            'EstimatedRecipients' => $EstimatedRecipients,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/campaign_overview', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Create controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function create() {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Campaign.Create'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        if (AccountIsUnTrustedCheck($this->ArrayUserInformation)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'Create') {
            $this->_event_create();
        }
        // Events - End }
        // Retrieve lists and segments - Start {
        // Load other modules - Start
        Core::LoadObject('lists');
        Core::LoadObject('segments');
        // Load other modules - End

        $ArraySubscriberLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID']));
        foreach ($ArraySubscriberLists as &$EachList) {
            $EachList['Segments'] = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachList['ListID']), array('SegmentName' => 'ASC'), false);
        }
        // Retrieve lists and segments - End }
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateCampaign'],
            'UserInformation' => $this->ArrayUserInformation,
            'CurrentMenuItem' => 'Campaigns',
            'Lists' => $ArraySubscriberLists
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($_SESSION[SESSION_NAME]['WizardSessionName']) && !empty($_SESSION[SESSION_NAME]['WizardSessionName'])) {
            Core::LoadObject('wizard');
            $WizardSessionName = $_SESSION[SESSION_NAME]['WizardSessionName'];
            $Wizard = WizardFactory::get($WizardSessionName, '');
            if ($Wizard->get_extra_parameter('mode') == 'campaign') {
                $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                            'Content' => false,
                            "Criteria" => array(
                                array(
                                    "Column" => "%c%.CampaignID",
                                    "Operator" => "=",
                                    "Value" => $Wizard->get_extra_parameter('entity_id')
                                ),
                                array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                            )
                ));
                if (count($CampaignInformation) > 0) {
                    $WizardURL = '/user/email/create/' . $Wizard->get_extra_parameter('mode') . '/' . $Wizard->get_extra_parameter('entity_id') . '/' . $Wizard->get_extra_parameter('flow') . '/' . $Wizard->get_last_step();
                    $ArrayViewData['WizardURL'] = $WizardURL;
                    $ArrayViewData['WizardCampaignName'] = $CampaignInformation[0]['CampaignName'];
                }
            }
        }

        $this->render('user/campaign_create', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Campaign create function
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_create() {
        // Field Validations - Start {
        $this->form_validation->set_rules("PublishOnRSS", "");
        $this->form_validation->set_rules("SameNameEmail", "");
        $this->form_validation->set_rules("EnableGoogleAnalytics", "");

        $this->form_validation->set_rules("CampaignName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0756'], 'required');
        $this->form_validation->set_rules("Recipients", ApplicationHeader::$ArrayLanguageStrings['Screen']['0092'], 'required');

        if ($this->input->post('EnableGoogleAnalytics') !== false) {
            $this->form_validation->set_rules("GoogleAnalyticsDomains", ApplicationHeader::$ArrayLanguageStrings['Screen']['0775'], 'required');
        }
        // Field Validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return false;
        }
        // Run validation - End }
        // Create campaign - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.create',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'campaignname' => $this->input->post('CampaignName')
                    )
        ));

        $CampaignID = $ArrayReturn['CampaignID'];

        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.update',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'campaignid' => $CampaignID,
                        'googleanalyticsdomains' => $this->input->post('GoogleAnalyticsDomains'),
                        'publishonrss' => $this->input->post('PublishOnRSS') == false ? 'Disabled' : 'Enabled',
                        'recipientlistsandsegments' => implode(',', $this->input->post('Recipients')),
                        'campaignstatus' => 'Draft',
                        'scheduletype' => 'Not Scheduled'
                    )
        ));
        // Create campaign - End }
        // Create test - Start {
        if ($this->input->post('SplitTesting') == 'Enabled') {
            $ArrayReturn = API::call(array(
                        'format' => 'array',
                        'command' => 'splittest.create',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => array(
                            'campaignid' => $CampaignID,
                            'testsize' => $this->input->post('TestSize'),
                            'testduration' => $this->input->post('TestDurationMultiplier') * $this->input->post('TestDurationBaseSeconds'),
                            'winner' => $this->input->post('Winner')
                        )
            ));
        }
        // Create test - End }
        // Redirect to email creator - Start {
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/email/create/campaign/' . $CampaignID);
        // Redirect to email creator - End }
    }

    /**
     * Edit controller
     *
     * @author Mert Hurturk
     * */
    function edit($CampaignID) {
        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Content' => true,
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }

        $CampaignInformation = $CampaignInformation[0];
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End
        // Events - Start {
        if ($this->input->post('Command') == 'Save') {
            if (Users::HasPermissions(array('Campaign.Update'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == true) {
                $this->_event_save($CampaignID, $CampaignInformation);
            }
        } elseif ($this->input->post('Command') == 'Preview') {
            $this->_event_preview($CampaignID);
        } elseif ($this->input->post('Command') == 'Test') {
            $this->_event_test($CampaignID);
        }
        // Events - End }

        $OldCampaignInformation = $CampaignInformation;

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Content' => true,
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    )
        ));
        $CampaignInformation = $CampaignInformation[0];
        // Retrieve campaign information - End }

        if ($this->input->post('Command') == 'Save' && $CampaignInformation['ScheduleType'] == 'Immediate' && $OldCampaignInformation['ScheduleType'] != 'Immediate') {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/campaign/overview/' . $CampaignInformation['CampaignID']);
        }

        unset($OldCampaignInformation);

        $SendRepeatedlyDayType = 'Every day';
        $SendRepeatedlyMonthType = 'Every month';
        $ScheduleRecDaysOfWeek = array();
        $ScheduleRecMonths = array();
        $ScheduleRecHours = array();
        $ScheduleRecMinutes = array();
        if ($CampaignInformation['ScheduleType'] == 'Recursive') {
            if ($CampaignInformation['ScheduleRecDaysOfWeek'] == '*') {
                $SendRepeatedlyDayType = 'Every day';
            } else if ($CampaignInformation['ScheduleRecDaysOfWeek'] == '1,2,3,4,5') {
                $SendRepeatedlyDayType = 'Every weekday';
            } else if ($CampaignInformation['ScheduleRecDaysOfWeek'] == '1,3,5') {
                $SendRepeatedlyDayType = 'Monday, wednesday and friday';
            } else if ($CampaignInformation['ScheduleRecDaysOfWeek'] == '2,4') {
                $SendRepeatedlyDayType = 'Tuesday and thursday';
            } else {
                if ($CampaignInformation['ScheduleRecDaysOfMonth'] == '') {
                    $SendRepeatedlyDayType = 'Days of week';
                    $ScheduleRecDaysOfWeek = explode(',', $CampaignInformation['ScheduleRecDaysOfWeek']);
                } else {
                    $SendRepeatedlyDayType = 'Days of month';
                }
            }

            if ($CampaignInformation['ScheduleRecMonths'] == '*') {
                $SendRepeatedlyMonthType = 'Every month';
            } else {
                $SendRepeatedlyMonthType = 'Months of year';
                $ScheduleRecMonths = explode(',', $CampaignInformation['ScheduleRecMonths']);
            }
            $ScheduleRecHours = explode(',', $CampaignInformation['ScheduleRecHours']);
            $ScheduleRecMinutes = explode(',', $CampaignInformation['ScheduleRecMinutes']);
        }



        // Retrieve lists and segments - Start {
        // Load other modules - Start
        Core::LoadObject('lists');
        Core::LoadObject('segments');
        // Load other modules - End

        $ArraySubscriberLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID']));
        foreach ($ArraySubscriberLists as &$EachList) {
            $EachList['Segments'] = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachList['ListID']), array('SegmentName' => 'ASC'), false);
        }

        $CampaignRecipients = array();
        foreach ($CampaignInformation['RecipientSegments'] as $Segment) {
            $CampaignRecipients[] = $Segment[0] . ':' . $Segment[1];
        }
        // Retrieve lists and segments - End }
        // Get recipient list ids - Start {
        $ArrayRecipientListIDs = array();
        foreach ($_SESSION[SESSION_NAME]['CampaignInformation']['Recipients'] as $Each) {
            $TempArray = explode(':', $Each);
            $ArrayRecipientListIDs[] = $TempArray[0];
        }
        $ArrayRecipientListIDs = array_unique($ArrayRecipientListIDs);
        // Get recipient list ids - End }
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignEdit'],
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'CurrentMenuItem' => 'Campaigns',
            'SubSection' => 'Edit',
            'Lists' => $ArraySubscriberLists,
            'CampaignRecipients' => $CampaignRecipients,
            'SendRepeatedlyDayType' => $SendRepeatedlyDayType,
            'ScheduleRecDaysOfWeek' => $ScheduleRecDaysOfWeek,
            'SendRepeatedlyMonthType' => $SendRepeatedlyMonthType,
            'ScheduleRecMonths' => $ScheduleRecMonths,
            'ScheduleRecHours' => $ScheduleRecHours,
            'ScheduleRecMinutes' => $ScheduleRecMinutes,
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/campaign_edit', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Campaign save information function
     *
     * @author Mert Hurturk
     * */
    function _event_save($CampaignID, $CampaignInformation = false) {
        $isEditingASentCampaign = (($CampaignInformation != false) && ($CampaignInformation['CampaignStatus'] == 'Sending' || $CampaignInformation['CampaignStatus'] == 'Paused' || $CampaignInformation['CampaignStatus'] == 'Sent'));

        // Field validations - Start {
        if (!$isEditingASentCampaign) {
            $this->form_validation->set_rules("SendRepeteadlyDayType", "");
            $this->form_validation->set_rules("SendRepeatedlyWeekDays", "");
            $this->form_validation->set_rules("SendRepeatedlyMonthDays", "");
            $this->form_validation->set_rules("SendRepeatedlyMonthType", "");
            $this->form_validation->set_rules("SendRepeatedlyMonths", "");
            $this->form_validation->set_rules("ScheduleRecHours", "");
            $this->form_validation->set_rules("ScheduleRecMinutes", "");
            $this->form_validation->set_rules("ScheduleRecSendMaxInstance", "");
            $this->form_validation->set_rules("SendTimeZone", "");
            $this->form_validation->set_rules("SendTimeHour", "");
            $this->form_validation->set_rules("SendTimeMinute", "");
            $this->form_validation->set_rules("SendDate", "");
            $this->form_validation->set_rules("EnableGoogleAnalytics", "");
            $this->form_validation->set_rules("ScheduleType", "");
            $this->form_validation->set_rules("Recipients", ApplicationHeader::$ArrayLanguageStrings['Screen']['0092'], 'required');
            $this->form_validation->set_rules("GoogleAnalyticsDomains", "");
            $this->form_validation->set_rules("Winner", "");
        }
        $this->form_validation->set_rules("PublishOnRSS", "");
        $this->form_validation->set_rules("CampaignName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0756'], 'required');

        if (!$isEditingASentCampaign) {
            if ($this->input->post('EnableGoogleAnalytics') !== false) {
                $this->form_validation->set_rules("GoogleAnalyticsDomains", ApplicationHeader::$ArrayLanguageStrings['Screen']['0775'], 'required');
            }
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return false;
        }
        // Run validation - End }
        // Campaign update - Start {
        $ArrayAPIVars = array(
            'campaignid' => $CampaignID,
            'campaignname' => $this->input->post('CampaignName'),
            'publishonrss' => $this->input->post('PublishOnRSS') == 'Enabled' ? 'Enabled' : 'Disabled'
        );

        if (!$isEditingASentCampaign) {
            $ArrayAPIVars['googleanalyticsdomains'] = $this->input->post('EnableGoogleAnalytics') == 'true' ? $this->input->post('GoogleAnalyticsDomains') : '';
            $ArrayAPIVars['recipientlistsandsegments'] = implode(',', $this->input->post('Recipients'));

            if ($this->input->post('ScheduleType') == 'Immediate') {
                $ArrayAPIVars['CampaignStatus'] = 'Ready';
                $ArrayAPIVars['ScheduleType'] = 'Immediate';
            } elseif ($this->input->post('ScheduleType') == 'Not Scheduled') {
                $ArrayAPIVars['CampaignStatus'] = 'Draft';
                $ArrayAPIVars['ScheduleType'] = 'Not Scheduled';
            } elseif ($this->input->post('ScheduleType') == 'Future') {
                $ArrayAPIVars['CampaignStatus'] = 'Ready';
                $ArrayAPIVars['ScheduleType'] = 'Future';
                $ArrayAPIVars['SendDate'] = $this->input->post('SendDate');
                $ArrayAPIVars['SendTime'] = $this->input->post('SendTimeHour') . ':' . $this->input->post('SendTimeMinute') . ':00';
                $ArrayAPIVars['SendTimeZone'] = $this->input->post('SendTimeZone');
            } elseif ($this->input->post('ScheduleType') == 'Recursive') {
                $ScheduleRecDaysOfWeek = '';
                $ScheduleRecDaysOfMonth = '';
                if ($this->input->post('SendRepeteadlyDayType') == 'Every day') {
                    $ScheduleRecDaysOfWeek = '*';
                } else if ($this->input->post('SendRepeteadlyDayType') == 'Every weekday') {
                    $ScheduleRecDaysOfWeek = '1,2,3,4,5';
                } else if ($this->input->post('SendRepeteadlyDayType') == 'Monday, wednesday and friday') {
                    $ScheduleRecDaysOfWeek = '1,3,5';
                } else if ($this->input->post('SendRepeteadlyDayType') == 'Tuesday and thursday') {
                    $ScheduleRecDaysOfWeek = '2,4';
                } else if ($this->input->post('SendRepeteadlyDayType') == 'Days of week') {
                    $ScheduleRecDaysOfWeek = implode(',', $this->input->post('SendRepeatedlyWeekDays'));
                    $ScheduleRecDaysOfMonth = '';
                } else if ($this->input->post('SendRepeteadlyDayType') == 'Days of month') {
                    $ScheduleRecDaysOfWeek = '';
                    $ScheduleRecDaysOfMonth = $this->input->post('SendRepeatedlyMonthDays');
                }

                $ScheduleRecMonths = '';
                if ($this->input->post('SendRepeatedlyMonthType') == 'Every month') {
                    $ScheduleRecMonths = '*';
                } else {
                    $ScheduleRecMonths = implode(',', $this->input->post('SendRepeatedlyMonths'));
                }

                $ArrayAPIVars['CampaignStatus'] = 'Ready';
                $ArrayAPIVars['ScheduleType'] = 'Recursive';
                $ArrayAPIVars['ScheduleRecDaysOfWeek'] = $ScheduleRecDaysOfWeek;
                $ArrayAPIVars['ScheduleRecDaysOfMonth'] = $ScheduleRecDaysOfMonth;
                $ArrayAPIVars['ScheduleRecMonths'] = $ScheduleRecMonths;
                $ArrayAPIVars['ScheduleRecHours'] = strtolower(gettype($this->input->post('ScheduleRecHours'))) == 'array' ? implode(',', $this->input->post('ScheduleRecHours')) : $this->input->post('ScheduleRecHours');
                $ArrayAPIVars['ScheduleRecMinutes'] = strtolower(gettype($this->input->post('ScheduleRecMinutes'))) == 'array' ? implode(',', $this->input->post('ScheduleRecMinutes')) : $this->input->post('ScheduleRecMinutes');
                $ArrayAPIVars['ScheduleRecSendMaxInstance'] = $this->input->post('ScheduleRecSendMaxInstance');
                $ArrayAPIVars['SendTimeZone'] = $this->input->post('SendTimeZone');
            }
        }
        if ($ArrayAPIVars['CampaignStatus'] == 'Ready' && $this->ArrayUserInformation['ReputationLevel'] == 'Untrusted') {
            $ArrayAPIVars['CampaignStatus'] = 'Pending Approval';
            O_Email_Sender_ForAdmin::send(
                    O_Email_Factory::campaignApprovalPendingNotification()
            );
        }


        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.update',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));
        // Campaign update - End }
        // Split test update - Start {
        if (!$isEditingASentCampaign) {
            // Retrieve campaign information - Start {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        )
            ));
            $CampaignInformation = $CampaignInformation[0];
            // Retrieve campaign information - End }

            if ($CampaignInformation['SplitTest'] != false) {
                SplitTests::Update($CampaignInformation['SplitTest']['TestID'], array(
                    'TestSize' => $this->input->post('TestSize'),
                    'TestDuration' => $this->input->post('TestDurationMultiplier') * $this->input->post('TestDurationBaseSeconds'),
                    'Winner' => $this->input->post('Winner')
                ));
            }
        }
        // Split test update - End }

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0963']);
    }

    /**
     * Duplicates test email for a campaign
     *
     * @return void
     * @author Mert Hurturk
     * */
    function duplicatetestemail($CampaignID, $TestID, $EmailID) {
        $SQLQuery = 'INSERT INTO ' . MYSQL_TABLE_PREFIX . 'emails (RelUserID, EmailName, FromName, FromEmail, ReplyToName, ReplyToEmail, ContentType, Mode, FetchURL, FetchPlainURL, Subject, PlainContent, HTMLContent, ImageEmbedding, RelTemplateID) ';
        $SQLQuery .= 'SELECT RelUserID, CONCAT("' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1371'] . '", EmailName) AS EmailName, FromName, FromEmail, ReplyToName, ReplyToEmail, ContentType, Mode, FetchURL, FetchPlainURL, Subject, PlainContent, HTMLContent, ImageEmbedding, RelTemplateID FROM ' . MYSQL_TABLE_PREFIX . 'emails WHERE EmailID = ' . $EmailID . ' AND RelUserID = ' . $this->ArrayUserInformation['UserID'];
        Database::$Interface->ExecuteQuery($SQLQuery);

        $NewEmailID = Database::$Interface->GetLastInsertID();

        SplitTests::AddTestEmailForCampaign($TestID, $CampaignID, $NewEmailID, $this->ArrayUserInformation['UserID']);

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1372']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/campaign/edit/' . $CampaignID . '#edit-tabs/tab-test');
    }

    /**
     * Removes test email
     *
     * @return void
     * @author Mert Hurturk
     * */
    function removetestemail($CampaignID, $TestID, $EmailID) {
        SplitTests::RemoveTestEmail($CampaignID, $TestID, $EmailID, $this->ArrayUserInformation['UserID']);

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1356']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/campaign/edit/' . $CampaignID . '#edit-tabs/tab-test');
    }

    /**
     * Campaign preview function
     *
     * @author Mert Hurturk
     */
    function _event_preview($CampaignID) {
        // Field validations - Start {
        $this->form_validation->set_rules("PreviewEmailAddress", '');
        $this->form_validation->set_rules("PreviewType", '');
        if ($this->input->post('PreviewType') == 'email') {
            $this->form_validation->set_rules("PreviewEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0809'], 'required|valid_email');
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return false;
        }
        // Run validation - End }


        if ($this->input->post('PreviewType') == 'email') {
            $ArrayReturn = API::call(array(
                        'format' => 'array',
                        'command' => 'email.emailpreview',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => array(
                            'campaignid' => $CampaignID,
                            'emailid' => $this->input->post('RelEmailID'),
                            'emailaddress' => $this->input->post('PreviewEmailAddress'),
                        )
            ));
            return array(
                'PageSuccessMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0810'],
            );
        } elseif ($this->input->post('PreviewType') == 'browser') {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/campaigns/preview/' . $CampaignID, '302');
        }
    }

    /**
     * Campaign design test function
     *
     * @author Mert Hurturk
     */
    function _event_test($Campaign) {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'email.designpreview.create',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array(
                        'emailid' => $this->input->post('RelEmailID')
                    )
        ));
    }

    /**
     * Delete controller
     *
     * @author Mert Hurturk
     * */
    function delete($CampaignID) {
        if (Users::HasPermissions(array('Campaign.Delete'), $this->ArrayUserInformation['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }

        $CampaignInformation = $CampaignInformation[0];
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End
        // Events - Start {
        if (isset($_POST['CampaignDeleteConfirm']) && $_POST['CampaignDeleteConfirm'] == 'true') {
            // Delete campaign - Start {
            $ArrayAPIVars = array(
                'campaigns' => $CampaignInformation['CampaignID']
            );
            $ArrayReturn = API::call(array(
                        'format' => 'object',
                        'command' => 'campaigns.delete',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => $ArrayAPIVars
            ));

            if ($ArrayReturn->Success == true) {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0914']);

                $this->load->helper('url');
                redirect(InterfaceAppURL(true) . '/user/campaigns/browse/', 'location', '302');
            }
            // Delete campaign - End }
        }
        // Events - End }
        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignDelete'],
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'CurrentMenuItem' => 'Campaigns',
            'SubSection' => 'Delete',
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/campaign_delete', $ArrayViewData);
        // Interface parsing - End }
    }

    /* By Eman */

    function campaignOpenStatistics() {

        $CampaignID = $_POST['campaign_id'];

        Core::LoadObject('statistics');
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignID));
        // Plug-in hook - End

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    'RecipientLists' => FALSE,
                    'RecipientSegments' => FALSE,
                    'Tags' => FALSE,
                    'SplitTests' => FALSE
        ));
        $CampaignInformation = $CampaignInformation[0];

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
        $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
        $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

        $OpenStatistics = Statistics::RetrieveCampaignOpenStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

        $output = array();
        $i = 1;
        foreach ($OpenStatistics as $key => $row) {
            $exp = explode("-", $key);
            $day = $exp[2];

            $new_row['day'] = $i; //$day;
            $new_row['Total'] = $row['Total'];
            $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));

            $output[] = $new_row;
            $i++;
        }
        print_r((json_encode($output)));
        return;
    }

    /* By Eman */

    function campaignClickStatistics() {

        $CampaignID = $_POST['campaign_id'];

        Core::LoadObject('statistics');
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignID));
        // Plug-in hook - End

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    'RecipientLists' => FALSE,
                    'RecipientSegments' => FALSE,
                    'Tags' => FALSE,
                    'SplitTests' => FALSE
        ));
        $CampaignInformation = $CampaignInformation[0];

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
        $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
        $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

        $ClickStatistics = Statistics::RetrieveCampaignClickStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

        $output = array();

        $i = 1;
        foreach ($ClickStatistics as $key => $row) {
            $exp = explode("-", $key);
            $day = $exp[2];

            $new_row['day'] = $i; //$day;
            $new_row['Total'] = $row['Total'];
            $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));

            $output[] = $new_row;
            $i++;
        }
        print_r((json_encode($output)));
        return;
    }

    /* By Eman */

    function campaignUnsubscriptionStatistics() {

        $CampaignID = $_POST['campaign_id'];

        Core::LoadObject('statistics');
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignID));
        // Plug-in hook - End

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    'RecipientLists' => FALSE,
                    'RecipientSegments' => FALSE,
                    'Tags' => FALSE,
                    'SplitTests' => FALSE
        ));
        $CampaignInformation = $CampaignInformation[0];

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
        $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
        $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

        $UnsubscriptionStatistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

        $output = array();

        $i = 1;
        foreach ($UnsubscriptionStatistics as $key => $row) {
            $exp = explode("-", $key);
            $day = $exp[2];

            $new_row['day'] = $i; //$day;
            $new_row['Unique'] = $row['Unique'];
            $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));

            $output[] = $new_row;
            $i++;
        }
        print_r((json_encode($output)));
        return;
    }

    /* By Eman */

    function campaignForwardStatistics() {

        $CampaignID = $_POST['campaign_id'];

        Core::LoadObject('statistics');
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignID));
        // Plug-in hook - End

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    'RecipientLists' => FALSE,
                    'RecipientSegments' => FALSE,
                    'Tags' => FALSE,
                    'SplitTests' => FALSE
        ));
        $CampaignInformation = $CampaignInformation[0];

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
        $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
        $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

        $ForwardStatistics = Statistics::RetrieveCampaignForwardStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

        $output = array();

        $i = 1;
        foreach ($ForwardStatistics as $key => $row) {
            $exp = explode("-", $key);
            $day = $exp[2];

            $new_row['day'] = $i; //$day;
            $new_row['Total'] = $row['Total'];
            $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));

            $output[] = $new_row;
            $i++;
        }
        print_r((json_encode($output)));
        return;
    }

    /* By Eman */

    function reports($CampaignID, $Statistic, $Date = null) {
        Core::LoadObject('statistics');

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End

        if ($Statistic == 'opens') {
            $view = 'user/campaign_report_opens';
        } elseif ($Statistic == 'clicks') {
            $view = 'user/campaign_report_clicks';
        } elseif ($Statistic == 'forwards') {
            $view = 'user/campaign_report_forwards';
        } elseif ($Statistic == 'unsubscriptions') {
            $view = 'user/campaign_report_unsubscriptions';
        } elseif ($Statistic == 'bounces') {
            $view = 'user/campaign_report_bounces';
        } elseif ($Statistic == 'hard_bounces') {
            $view = 'user/campaign_report_hard_bounces';
        } elseif ($Statistic == 'spams') {
            $view = 'user/campaign_report_spams';
        }
        $CampaignInformation = $CampaignInformation[0];

        $ResultColumns['SubscriberID'] = 'SubscriberID';
        $ResultColumns['EmailAddress'] = 'EmailAddress';
        $ResultColumns['Subscriber_IP'] = 'Subscriber_IP';
        $ResultColumns['City'] = 'City';
        $ResultColumns['Country'] = 'Country';

        $ResultSet = null;
        if ($Statistic == 'opens') {
            $ResultSet = Statistics::RetrieveCampaignDateOpens($CampaignInformation['CampaignID'], $Date);
            $ResultColumns['TotalOpens'] = 'TotalOpens';
        } elseif ($Statistic == 'clicks') {
            $ResultSet = Statistics::RetrieveCampaignDateClicks($CampaignInformation['CampaignID'], $Date);
            $ResultColumns['TotalClicks'] = 'TotalClicks';
        } elseif ($Statistic == 'forwards') {
            $ResultSet = Statistics::RetrieveCampaignDateForwards($CampaignInformation['CampaignID'], $Date);
            $ResultColumns['TotalForwards'] = 'TotalForwards';
        } elseif ($Statistic == 'unsubscriptions') {
            $ResultSet = Statistics::RetrieveCampaignDateUnsubscriptions($CampaignInformation['CampaignID'], $Date);
        } elseif ($Statistic == 'bounces') {
            $ResultSet = Statistics::RetrieveSubscriberBounceDate($CampaignInformation['CampaignID'], $Date, null);
        } elseif ($Statistic == 'hard_bounces') {
            $ResultSet = Statistics::RetrieveSubscriberBounceDate($CampaignInformation['CampaignID'], $Date, 'Hard');
        } elseif ($Statistic == 'spams') {
            $ResultSet = Statistics::RetrieveCampaignDateSPAM($CampaignInformation['CampaignID'], $Date);
            $ResultColumns['TotalSPAMComplaints'] = 'TotalSPAMComplaints';
        }

        $ArrayDataRows = array();
        $global_custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            if ($Statistic == 'spams') {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['SubscriberID']), $EachRow['ListID']);
                $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachRow['ListID']));
            } else {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachRow['RelListID']));
            }
            if ($global_custom_fields != false) {
                $custom_fields = array_merge($custom_fields ? $custom_fields : array(), $global_custom_fields);
            }
            if ($Statistic == 'forwards')
                $ArraySubscriber['TotalForwards'] = number_format($EachRow['TotalForwards']);
            elseif ($Statistic == 'clicks')
                $ArraySubscriber['TotalClicks'] = number_format($EachRow['TotalClicks']);
            elseif ($Statistic == 'opens')
                $ArraySubscriber['TotalOpens'] = number_format($EachRow['TotalOpens']);
            elseif ($Statistic == 'spams')
                $ArraySubscriber['TotalSPAMComplaints'] = number_format($EachRow['TotalSPAMComplaints']);

            foreach ($ArraySubscriber as $key => $value) {
                if (strpos($key, 'CustomField') !== false) {
                    $id = substr($key, 11);
                    foreach ($custom_fields as $custom_field) {
                        $custom_field_ID = $custom_field['CustomFieldID'];
                        $field_name = $custom_field['FieldName'];
                        if ($id == $custom_field_ID) {
                            $ArraySubscriber[$custom_field['FieldName']] = $ArraySubscriber['CustomField' . $id];
                            unset($ArraySubscriber['CustomField' . $id]);
                        }
                    }
                }
            }
            unset($ArraySubscriber['SubscriptionStatus']);
            unset($ArraySubscriber['UnsubscriptionDate']);
            unset($ArraySubscriber['UnsubscriptionIP']);
            unset($ArraySubscriber['BounceType']);
            unset($ArraySubscriber['SubscriptionDate']);
            unset($ArraySubscriber['SubscriptionIP']);
            unset($ArraySubscriber['OptInDate']);

            $ArrayDataRows[] = $ArraySubscriber;
        }
        if ($Statistic == 'opens') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '0837', true),
                'SubSection' => InterfaceLanguage('Screen', '0837', true),
                'Title' => InterfaceLanguage('Screen', '0837', true),
            );
        } elseif ($Statistic == 'clicks') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '0838', true),
                'SubSection' => InterfaceLanguage('Screen', '0838', true),
                'Title' => InterfaceLanguage('Screen', '0838', true),
            );
        } elseif ($Statistic == 'forwards') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '0839', true),
                'SubSection' => InterfaceLanguage('Screen', '0839', true),
                'Title' => InterfaceLanguage('Screen', '0839', true),
            );
        } elseif ($Statistic == 'unsubscriptions') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '0097', true),
                'SubSection' => InterfaceLanguage('Screen', '0097', true),
                'Title' => InterfaceLanguage('Screen', '0097', true),
            );
        } elseif ($Statistic == 'bounces') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '0098', true),
                'SubSection' => InterfaceLanguage('Screen', '0098', true),
                'Title' => InterfaceLanguage('Screen', '0098', true),
            );
        } elseif ($Statistic == 'hard_bounces') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '9208', true),
                'SubSection' => InterfaceLanguage('Screen', '9208', true),
                'Title' => InterfaceLanguage('Screen', '9208', true),
            );
        } elseif ($Statistic == 'spams') {
            $StatisticViewData = array(
                'PageTitle' => " - " . ucwords($CampaignInformation['CampaignName']) . " - " . InterfaceLanguage('Screen', '9206', true),
                'SubSection' => InterfaceLanguage('Screen', '9206', true),
                'Title' => InterfaceLanguage('Screen', '9206', true),
            );
        }

        // Interface parsing - Start {
        $ArrayViewData = array(
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'CurrentMenuItem' => 'Campaigns',
            'ResultColumns' => $ResultColumns,
            'ResultRows' => $ArrayDataRows,
        );


        $ArrayViewData = array_merge($ArrayViewData, $StatisticViewData);
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        $this->render('user/campaign_report_details', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Statistics data controller
     *
     * @author Mert Hurturk
     * */
    function statistics($CampaignID, $Statistics, $Days = 5) {
        Core::LoadObject('chart_data_generator');
        Core::LoadObject('statistics');

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignID));
        // Plug-in hook - End
        // Load chart graph colors from the config file - Start {
        $ArrayChartGraphColors = explode(',', CHART_COLORS);
        // Load chart graph colors from the config file - End }

        if ($Statistics == 'opens-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('OpenStatistics')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }

            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['OpenStatistics'] as $Each) {
                $SparklineData[] = $Each['Total'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'clicks-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('ClickStatistics')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }

            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['ClickStatistics'] as $Each) {
                $SparklineData[] = $Each['Total'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'forwards-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('ForwardStatistics')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }

            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['ForwardStatistics'] as $Each) {
                $SparklineData[] = $Each['Total'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'views-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('BrowserViewStatistics')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }
            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['BrowserViewStatistics'] as $Each) {
                $SparklineData[] = $Each['Total'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'unsubscriptions-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('UnsubscriptionStatistics')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }

            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['UnsubscriptionStatistics'] as $Each) {
                $SparklineData[] = $Each['Unique'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'bounces-sparkline') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Reports" => array(
                            "Days" => $Days,
                            "Statistics" => array('BounceStatisticsTimeFrame')
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }

            $CampaignInformation = $CampaignInformation[0];
            $SparklineData = array();

            foreach ($CampaignInformation['BounceStatisticsTimeFrame'] as $Each) {
                $SparklineData[] = $Each['Total'];
            }
            $SparklineData = implode('x', $SparklineData);

            $this->load->library('OemproSparkLine', array());
            $this->oemprosparkline->draw(array('data' => $SparklineData));
        } elseif ($Statistics == 'overview') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));

            if (count($CampaignInformation) < 1) {
                exit;
            }
            $CampaignInformation = $CampaignInformation[0];

            $opened_ratio = number_format((100 * $CampaignInformation['UniqueOpens']) / $CampaignInformation['TotalSent']);
            $bounced_ratio = number_format((100 * $CampaignInformation['TotalHardBounces']) / $CampaignInformation['TotalSent']);
            $not_opened_ratio = 100 - ($opened_ratio + $bounced_ratio);
            $not_opened = $CampaignInformation['TotalSent'] - ($CampaignInformation['UniqueOpens'] + $CampaignInformation['TotalHardBounces']);

            $XML = '<pie><slice color="' . $ArrayChartGraphColors[2] . '" title="' . $not_opened . '">' . $not_opened_ratio . '</slice><slice color="' . $ArrayChartGraphColors[1] . '" title="' . $CampaignInformation['UniqueOpens'] . '">' . $opened_ratio . '</slice><slice color="' . $ArrayChartGraphColors[0] . '" title="' . $CampaignInformation['TotalHardBounces'] . '">' . $bounced_ratio . '</slice></pie>';
            header('Content-type: text/xml');
            print($XML);
            exit;
        } else {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        'RecipientLists' => FALSE,
                        'RecipientSegments' => FALSE,
                        'Tags' => FALSE,
                        'SplitTests' => FALSE
            ));
            $CampaignInformation = $CampaignInformation[0];
            $LastXDays = ($Days < 7 ? 7 : $Days);
            $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
            $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));
            $OpenStatistics = Statistics::RetrieveCampaignOpenStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'left'),
                1 => array('title' => '', 'axis' => 'left'),
            );
            $ArrayData['GraphData'] = array();

            if ($Statistics == 'opens-clicks') {
                $ClickStatistics = Statistics::RetrieveCampaignClickStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'opens-unsubscriptions') {
                $UnsubscriptionStatistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'opens-all') {
                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total']) == true ? $OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0891'], number_format($OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'clicks-all') {
                $ClickStatistics = Statistics::RetrieveCampaignClickStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                    $ArrayData['GraphData'][1][] = array(
                        'Value' => (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0893'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'forwards-all') {
                $ForwardStatistics = Statistics::RetrieveCampaignForwardStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0900'], number_format($ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                    $ArrayData['GraphData'][1][] = array(
                        'Value' => (isset($ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total']) == true ? $ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0901'], number_format($ForwardStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'browserviews-all') {
                $ViewStatistics = Statistics::RetrieveCampaignBrowserViewStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0915'], number_format($ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                    $ArrayData['GraphData'][1][] = array(
                        'Value' => (isset($ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total']) == true ? $ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0915'], number_format($ViewStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Total'])),
                        ),
                    );
                }
            } elseif ($Statistics == 'unsubscriptions-all') {
                $UnsubscriptionStatistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => (isset($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0917'], number_format($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }

            if ($Statistics != 'clicks-all' && $Statistics != 'forwards-all' && $Statistics != 'browserviews-all' && $Statistics != 'unsubscriptions-all') {
                for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                    $ArrayData['GraphData'][1][] = array(
                        'Value' => (isset($OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique']) == true ? $OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'] : 0),
                        'Parameters' => array(
                            'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0892'], number_format($OpenStatistics[date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['Unique'])),
                        ),
                    );
                }
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        }
    }

    /**
     * Test statistics data controller
     *
     * @author Mert Hurturk
     * */
    function teststatistics($CampaignID) {
        Core::LoadObject('chart_data_generator');
        Core::LoadObject('statistics');
        Core::LoadObject('emails');
        Core::LoadObject('split_tests');

        // Load chart graph colors from the config file - Start {
        $ArrayChartGraphColors = explode(',', CHART_COLORS);
        // Load chart graph colors from the config file - End }
        // Retrieve test information and emails - Start {
        $TestInformation = SplitTests::RetrieveTestOfACampaign($CampaignID, $this->ArrayUserInformation['UserID']);
        $TestInformation = $TestInformation[0];

        $TestEmails = Emails::RetrieveEmailsOfTest($TestInformation['TestID'], $CampaignID);
        // Retrieve test information and emails - End }

        $ArrayData['Series'] = array(
            0 => ApplicationHeader::$ArrayLanguageStrings['Screen']['0094'],
            1 => ApplicationHeader::$ArrayLanguageStrings['Screen']['1390']
        );

        $ArrayData['Graphs'] = array();

        foreach ($TestEmails as $index => $EachEmail) {
            $TotalUniqueClicksOfEmail = Statistics::TotalUniqueClicksOfCampaign($CampaignID, $this->ArrayUserInformation['UserID'], $EachEmail['EmailID']);
            $TotalUniqueClicksOfEmail = $TotalUniqueClicksOfEmail ? $TotalUniqueClicksOfEmail : 0;

            $TotalUniqueOpensOfEmail = Statistics::TotalUniqueOpensOfCampaign($CampaignID, $this->ArrayUserInformation['UserID'], $EachEmail['EmailID']);
            $TotalUniqueOpensOfEmail = $TotalUniqueOpensOfEmail ? $TotalUniqueOpensOfEmail : 0;

            $ArrayData['Graphs'][] = array();
            $TempArray = array();
            $TempArray[0] = array(
                'Value' => $TotalUniqueOpensOfEmail,
                'Parameters' => array(
                    'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0892'], $TotalUniqueOpensOfEmail)
                )
            );
            $TempArray[1] = array(
                'Value' => $TotalUniqueClicksOfEmail,
                'Parameters' => array(
                    'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], $TotalUniqueClicksOfEmail)
                )
            );

            $ArrayData['GraphData'][] = $TempArray;
        }

        $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
        header('Content-type: text/xml');
        print($XML);
        exit;
    }

    function snippetsubscriberactivity($campaign_id, $statistic, $start_from = 0) {
        Core::LoadObject('statistics');

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $campaign_id
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            exit;
        }
        // Retrieve campaign information - End }

        if ($statistic == 'opens') {
            $result_set = Statistics::RetrieveCampaignOpens($campaign_id, $this->ArrayUserInformation['UserID'], $start_from);
            $array_subscribers = array();
            while ($each_row = mysql_fetch_assoc($result_set)) {
                $array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $each_row['RelSubscriberID']), $each_row['RelListID']);
                $tmp_array = array(
                    'SubscriberID' => $each_row['RelSubscriberID'],
                    'ListID' => $each_row['RelListID'],
                    'Email' => ($array_subscriber['EmailAddress'] == '' ? ApplicationHeader::$ArrayLanguageStrings['Screen']['0889'] : $array_subscriber['EmailAddress']),
                    'TotalOpens' => number_format($each_row['TotalOpens']),
                    'Activities' => array()
                );
                $array_subscriber_activity = Statistics::RetrieveSubscriberCampaignActivity($campaign_id, $array_subscriber['SubscriberID'], $each_row['RelListID']);
                foreach ($array_subscriber_activity as $timestamp => $array_activity) {
                    $activity_time = Core::ReFormatDate('', '', 'M j, Y g:i a', 'D, j M g:i a', 'g:i a', 'Today, ', 'Yesterday, ', $array_activity[0], true);
                    $activity_name = ($array_activity[1] == 'Email Open' ? '<strong>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]] . '</strong>' : ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]]);
                    $tmp_array['Activities'][] = array('time' => $activity_time, 'activity' => $activity_name);
                }
                $array_subscribers[] = $tmp_array;
            }
            $view = 'user/campaign_snippet_subscriber_activity_opens';
            $view_data = array(
                'Subscribers' => $array_subscribers
            );
        } elseif ($statistic == 'clicks') {
            $result_set = Statistics::RetrieveCampaignLinkClicks($campaign_id, $this->ArrayUserInformation['UserID'], 'Subscribers', $start_from);
            $array_subscribers = array();
            while ($each_row = mysql_fetch_assoc($result_set)) {
                $array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $each_row['RelSubscriberID']), $each_row['RelListID']);
                $tmp_array = array(
                    'SubscriberID' => $array_subscriber['SubscriberID'],
                    'ListID' => $each_row['RelListID'],
                    'Email' => $array_subscriber['EmailAddress'],
                    'TotalClicks' => number_format($each_row['TotalClicks']),
                );
                $array_subscriber_activity = Statistics::RetrieveSubscriberCampaignActivity($campaign_id, $array_subscriber['SubscriberID'], $each_row['RelListID']);
                foreach ($array_subscriber_activity as $timestamp => $array_activity) {
                    $activity_time = Core::ReFormatDate('', '', 'M j, Y g:i a', 'D, j M g:i a', 'g:i a', 'Today, ', 'Yesterday, ', $array_activity[0], true);
                    $activity_name = ($array_activity[1] == 'Link Click' ? '<strong>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]] . '</strong>' : ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]]);
                    $tmp_array['Activities'][] = array('time' => $activity_time, 'activity' => $activity_name);
                }
                $array_subscribers[] = $tmp_array;
            }
            $view = 'user/campaign_snippet_subscriber_activity_clicks';
            $view_data = array(
                'Subscribers' => $array_subscribers
            );
        } elseif ($statistic == 'unsubscriptions') {
            $result_set = Statistics::RetrieveCampaignUnsubscriptions($campaign_id, $start_from);
            $array_subscribers = array();
            while ($each_row = mysql_fetch_assoc($result_set)) {
                $array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $each_row['RelSubscriberID']), $each_row['RelListID']);
                $tmp_array = array(
                    'SubscriberID' => $array_subscriber['SubscriberID'],
                    'ListID' => $each_row['RelListID'],
                    'Email' => $array_subscriber['EmailAddress'],
                    'UnsubscriptionDate' => $each_row['UnsubscriptionDate'],
                );
                $array_subscriber_activity = Statistics::RetrieveSubscriberCampaignActivity($campaign_id, $array_subscriber['SubscriberID'], $each_row['RelListID']);
                foreach ($array_subscriber_activity as $timestamp => $array_activity) {
                    $activity_time = Core::ReFormatDate('', '', 'M j, Y g:i a', 'D, j M g:i a', 'g:i a', 'Today, ', 'Yesterday, ', $array_activity[0], true);
                    $activity_name = ($array_activity[1] == 'Link Click' ? '<strong>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]] . '</strong>' : ApplicationHeader::$ArrayLanguageStrings['Screen']['1485'][$array_activity[1]]);
                    $tmp_array['Activities'][] = array('time' => $activity_time, 'activity' => $activity_name);
                }
                $array_subscribers[] = $tmp_array;
            }
            $view = 'user/campaign_snippet_subscriber_activity_unsubscriptions';
            $view_data = array(
                'Subscribers' => $array_subscribers
            );
        }

        $this->render($view, $view_data);
    }

    /**
     * Statistics controller
     *
     * @author Mert Hurturk
     * */
    function viewstatistics($CampaignID, $Statistic, $Days = 7) {
        Core::LoadObject('statistics');

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
            return;
        }
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End

        if ($Statistic == 'opens') {
            $ReportsArray = array(
                "Days" => $Days,
                "Statistics" => array('OpenStatistics', 'OpenPerformance')
            );

            $view = 'user/campaign_statisticsview_opens';
        } elseif ($Statistic == 'clicks') {
            $ReportsArray = array(
                "Days" => $Days,
                "Statistics" => array('ClickStatistics', 'ClickPerformance')
            );

            $view = 'user/campaign_statisticsview_clicks';
        } elseif ($Statistic == 'forwards') {
            $ReportsArray = array(
                "Days" => $Days,
                "Statistics" => array('ForwardStatistics')
            );

            $view = 'user/campaign_statisticsview_forwards';
        } elseif ($Statistic == 'browserviews') {
            $ReportsArray = array(
                "Days" => $Days,
                "Statistics" => array('BrowserViewStatistics')
            );

            $view = 'user/campaign_statisticsview_browserviews';
        } elseif ($Statistic == 'unsubscriptions') {
            $ReportsArray = array(
                "Days" => $Days,
                "Statistics" => array('UnsubscriptionStatistics')
            );

            $view = 'user/campaign_statisticsview_unsubscriptions';
        }

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    "Reports" => $ReportsArray
        ));
        $CampaignInformation = $CampaignInformation[0];

        if ($Statistic == 'opens') {
            $TotalSubscribers = Statistics::RetrieveCampaignOpens($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], 0, true);

            $StatisticViewData = array(
                'OpenPerformance' => number_format((100 * $CampaignInformation['UniqueOpens']) / $CampaignInformation['TotalSent']),
                'AccountPerformance' => number_format($CampaignInformation['OverallAccountOpenPerformance']),
                'PerformanceDifference' => number_format((100 * $CampaignInformation['UniqueOpens']) / $CampaignInformation['TotalSent']) - number_format($CampaignInformation['OverallAccountOpenPerformance']),
                'HighestLowestOpens' => Statistics::RetrieveOpenPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']),
                'TotalSubscribers' => $TotalSubscribers,
                'SubSection' => 'Opens'
            );
        } elseif ($Statistic == 'clicks') {
            $ResultSet = Statistics::RetrieveCampaignLinkClicks($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], 'Links', 0);
            $ArrayClickedLinks = array();
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayClickedLinks[] = array(
                    'LinkURL' => $EachRow['LinkURL'],
                    'LinkTitle' => $EachRow['LinkTitle'],
                    'TotalClicks' => number_format($EachRow['TotalClicks']),
                );
            }

            $TotalSubscribers = Statistics::RetrieveCampaignLinkClicks($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], 'Subscribers', 0, true);

            $StatisticViewData = array(
                'ClickPerformance' => number_format((100 * $CampaignInformation['UniqueClicks']) / $CampaignInformation['TotalSent']),
                'AccountPerformance' => number_format($CampaignInformation['OverallAccountClickPerformance']),
                'PerformanceDifference' => number_format((100 * $CampaignInformation['UniqueClicks']) / $CampaignInformation['TotalSent']) - number_format($CampaignInformation['OverallAccountClickPerformance']),
                'HighestLowestClicks' => Statistics::RetrieveClickPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']),
                'ClickedLinks' => $ArrayClickedLinks,
                'TotalSubscribers' => $TotalSubscribers,
                'SubSection' => 'Clicks'
            );
        } elseif ($Statistic == 'forwards') {
            $ResultSet = Statistics::RetrieveCampaignForwards($CampaignInformation['CampaignID'], 0);
            $ArrayForwards = array();
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArrayForwards[] = array(
                    'SubscriberID' => $ArraySubscriber['SubscriberID'],
                    'Email' => $ArraySubscriber['EmailAddress'],
                    'TotalForwards' => number_format($EachRow['TotalForwards']),
                );
            }

            $StatisticViewData = array(
                'Forwards' => $ArrayForwards,
                'SubSection' => 'Forwards',
                'ForwardPerformance' => number_format((100 * $CampaignInformation['UniqueForwards']) / $CampaignInformation['TotalSent']),
            );
        } elseif ($Statistic == 'browserviews') {
            $ResultSet = Statistics::RetrieveCampaignBrowserViews($CampaignInformation['CampaignID'], 0);
            $ArrayViews = array();
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArrayViews[] = array(
                    'SubscriberID' => $ArraySubscriber['SubscriberID'],
                    'Email' => $ArraySubscriber['EmailAddress'],
                    'TotalViews' => number_format($EachRow['TotalViews']),
                );
            }

            $StatisticViewData = array(
                'Views' => $ArrayViews,
                'SubSection' => 'BrowserViews'
            );
        } elseif ($Statistic == 'unsubscriptions') {
            $TotalSubscribers = Statistics::RetrieveCampaignUnsubscriptions($CampaignInformation['CampaignID'], 0, true);

            $StatisticViewData = array(
                'TotalSubscribers' => $TotalSubscribers,
                'SubSection' => 'Unsubscriptions'
            );
        }

        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCampaignStatistics'],
            'UserInformation' => $this->ArrayUserInformation,
            'CampaignInformation' => $CampaignInformation,
            'CurrentMenuItem' => 'Campaigns',
        );

        $ArrayViewData = array_merge($ArrayViewData, $StatisticViewData);
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render($view, $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Statistic export controller
     *
     * @author Mert Hurturk
     * */
    function exportstatistics($CampaignID, $Statistic = 'overview', $Format = 'csv') {
        Core::LoadObject('statistics');

        // Retrieve campaign information - Start {
        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        ),
                        array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'Value' => $this->ArrayUserInformation['UserID'], 'Link' => 'AND')
                    )
        ));

        if (count($CampaignInformation) < 1) {
            exit;
        }
        // Retrieve campaign information - End }
        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Campaign.OnView', array($CampaignInformation));
        // Plug-in hook - End

        $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                    "Criteria" => array(
                        array(
                            "Column" => "%c%.CampaignID",
                            "Operator" => "=",
                            "Value" => $CampaignID
                        )
                    ),
                    "Reports" => array(
                        "Days" => 30,
                        "Statistics" => array('OpenStatistics', 'ClickStatistics', 'UnsubscriptionStatistics', 'ForwardStatistics', 'BrowserViewStatistics', 'BounceStatisticsTimeFrame')
                    )
        ));
        $CampaignInformation = $CampaignInformation[0];

        if ($Statistic == 'overview') {
            if ($Format == 'csv') {
                $CSVString = ApplicationHeader::$ArrayLanguageStrings['Screen']['0918'] . "\n\n" . Statistics::ConvertToCSV($CampaignInformation['UnsubscriptionStatistics'], 'Date');
                $CSVString .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0919'] . "\n\n" . Statistics::ConvertToCSV($CampaignInformation['OpenStatistics'], 'Date');
                $CSVString .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0920'] . "\n\n" . Statistics::ConvertToCSV($CampaignInformation['ClickStatistics'], 'Date');
                $CSVString .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0921'] . "\n\n" . Statistics::ConvertToCSV($CampaignInformation['BrowserViewStatistics'], 'Date');
                $CSVString .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0922'] . "\n\n" . Statistics::ConvertToCSV($CampaignInformation['ForwardStatistics'], 'Date');
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<unsubscription_statistics>' . Statistics::ConvertToXML($CampaignInformation['UnsubscriptionStatistics'], 'Date') . '</unsubscription_statistics>';
                $XMLString .= '<open_statistics>' . Statistics::ConvertToXML($CampaignInformation['OpenStatistics'], 'Date') . '</open_statistics>';
                $XMLString .= '<click_statistics>' . Statistics::ConvertToXML($CampaignInformation['ClickStatistics'], 'Date') . '</click_statistics>';
                $XMLString .= '<browser_view_statistics>' . Statistics::ConvertToXML($CampaignInformation['BrowserViewStatistics'], 'Date') . '</browser_view_statistics>';
                $XMLString .= '<forward_statistics>' . Statistics::ConvertToXML($CampaignInformation['ForwardStatistics'], 'Date') . '</forward_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'opens') {
            $TMPArray = array();

            $ResultSet = Statistics::RetrieveCampaignOpens($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], 0, false, 1000000);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArraySubscriber = ($ArraySubscriber == false ? array() : $ArraySubscriber);
                // Get subscriber information - End

                $TMPArray[] = array_merge($ArraySubscriber, $EachRow);
            }

            if ($Format == 'csv') {
                $CSVString = Statistics::ConvertToCSV($TMPArray);
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<open_statistics>' . Statistics::ConvertToXML($TMPArray) . '</open_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'clicks') {
            $TMPArray = array();

            $ResultSet = Statistics::RetrieveCampaignLinkClicks($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], 'Subscribers', 0, false, 1000000);

            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArraySubscriber = ($ArraySubscriber == false ? array() : $ArraySubscriber);
                // Get subscriber information - End

                $TMPArray[] = array_merge($ArraySubscriber, $EachRow);
            }

            if ($Format == 'csv') {
                $CSVString = Statistics::ConvertToCSV($TMPArray);
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<click_statistics>' . Statistics::ConvertToXML($TMPArray) . '</click_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'forwards') {
            $TMPArray = array();

            $ResultSet = Statistics::RetrieveCampaignForwards($CampaignInformation['CampaignID'], 0, false, 1000000);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArraySubscriber = ($ArraySubscriber == false ? array() : $ArraySubscriber);
                // Get subscriber information - End

                $TMPArray[] = array_merge($ArraySubscriber, $EachRow);
            }

            if ($Format == 'csv') {
                $CSVString = Statistics::ConvertToCSV($TMPArray);
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<forward_statistics>' . Statistics::ConvertToXML($TMPArray) . '</forward_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'browserviews') {
            $TMPArray = array();

            $ResultSet = Statistics::RetrieveCampaignBrowserViews($CampaignInformation['CampaignID'], 0, false, 1000000);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArraySubscriber = ($ArraySubscriber == false ? array() : $ArraySubscriber);
                // Get subscriber information - End

                $TMPArray[] = array_merge($ArraySubscriber, $EachRow);
            }

            if ($Format == 'csv') {
                $CSVString = Statistics::ConvertToCSV($TMPArray);
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<browser_view_statistics>' . Statistics::ConvertToXML($TMPArray) . '</browser_view_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'unsubscriptions') {
            $TMPArray = array();

            $ResultSet = Statistics::RetrieveCampaignUnsubscriptions($CampaignInformation['CampaignID'], 0, false, 1000000);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $ArraySubscriber = ($ArraySubscriber == false ? array() : $ArraySubscriber);
                // Get subscriber information - End

                $TMPArray[] = array_merge($ArraySubscriber, $EachRow);
            }

            if ($Format == 'csv') {
                $CSVString = Statistics::ConvertToCSV($TMPArray);
            } elseif ($Format == 'xml') {
                $XMLString = '<statistics>';
                $XMLString .= '<unsubscription_statistics>' . Statistics::ConvertToXML($TMPArray) . '</unsubscription_statistics>';
                $XMLString .= '</statistics>';
            }
        } elseif ($Statistic == 'bounces') {
            $TMPArray = array();
            foreach ($CampaignInformation['BounceStatisticsTimeFrame'] as $Each) {
                $TMPArray[] = $Each;
            }

            $CSVString = Statistics::ConvertToCSV($TMPArray);
        } elseif ($Statistic == 'bouncedsubscribers') {
            Core::LoadObject('subscribers');

            header("Content-type: application/octetstream");
            header("Content-Disposition: attachment; filename=\"campaign_" . $CampaignInformation['CampaignID'] . "_bounced_subscribers.csv\"");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");

            print '"EmailAddress","BounceType","ListID"';
            print "\n";
            foreach ($CampaignInformation['RecipientLists'] as $EachListID) {
                $query = 'SELECT oempro_subscribers_' . $EachListID . '.EmailAddress, oempro_stats_bounce.BounceType'
                        . ' FROM oempro_subscribers_' . $EachListID
                        . ' JOIN oempro_stats_bounce ON oempro_stats_bounce.RelSubscriberID = oempro_subscribers_' . $EachListID . '.SubscriberID'
                        . ' AND oempro_stats_bounce.RelListID = ' . $EachListID
                        . ' AND oempro_stats_bounce.RelCampaignID = ' . $CampaignInformation['CampaignID']
                        . ' AND oempro_stats_bounce.BounceType IN ("Hard", "Soft")';
                $result = Database::$Interface->ExecuteQuery($query);

                while ($row = mysql_fetch_assoc($result)) {
                    print Subscribers::ReturnCSVLine($row, array('EmailAddress', 'BounceType'), '"', ',') . ',"' . $EachListID . '"' . "\n";
                }
            }

            exit;
        } else {
            exit;
        }

        if ($Format == 'csv') {
            // Make CSV data file downloadable - Start
            header("Content-type: application/octetstream");
            header("Content-Disposition: attachment; filename=\"campaign_statistics.csv\"");
            header('Content-length: ' . strlen($CSVString));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");
            // Make CSV data file downloadable - End
            print $CSVString;
            exit;
        } elseif ($Format == 'xml') {
            // Make XML data file downloadable - Start
            header("Content-type: application/octetstream");
            header("Content-Disposition: attachment; filename=\"campaign_statistics.xml\"");
            header('Content-length: ' . strlen($XMLString));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");
            // Make XML data file downloadable - End
            print $XMLString;
            exit;
        }
    }

    /**
     * Pause campaign controller
     *
     * @author Mert Hurturk
     * */
    function pause($CampaignID) {
        $this->load->helper('url');

        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'campaign.pause',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array('campaignid' => $CampaignID)
        ));

        if ($ArrayReturn->Success == false) {
            if ($ArrayReturn->ErrorCode == 1 || $ArrayReturn->ErrorCode == 2) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
                return;
            } else if ($ArrayReturn->ErrorCode == 3) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['9184'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9183']);
                return;
            }
        }

        redirect(InterfaceAppURL(true) . '/user/campaign/overview/' . $CampaignID);
    }

    /**
     * Resume campaign controller
     *
     * @author Mert Hurturk
     * */
    function resume($CampaignID) {
        $this->load->helper('url');

        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'campaign.resume',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array('campaignid' => $CampaignID)
        ));

        if ($ArrayReturn->Success == false) {
            if ($ArrayReturn->ErrorCode == 1 || $ArrayReturn->ErrorCode == 2) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
                return;
            } else if ($ArrayReturn->ErrorCode == 3) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['9184'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9183']);
                return;
            }
        }

        redirect(InterfaceAppURL(true) . '/user/campaign/overview/' . $CampaignID);
    }

    /**
     * Cancel sending controller
     *
     * @author Mert Hurturk
     * */
    function cancelsending($CampaignID) {
        $this->load->helper('url');

        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'campaign.cancel',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => array('campaignid' => $CampaignID)
        ));

        if ($ArrayReturn->Success == false) {
            if ($ArrayReturn->ErrorCode == 1 || $ArrayReturn->ErrorCode == 2) {
                $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['1497'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1498']);
                return;
            }
        }

        redirect(InterfaceAppURL(true) . '/user/campaign/overview/' . $CampaignID);
    }

    /**
     * Cancel schedule controller
     *
     * @author Mert Hurturk
     * */
    function cancelschedule($CampaignID) {
        $this->load->helper('url');
        $ArrayAPIVars = array(
            'campaignid' => $CampaignID,
            'campaignstatus' => 'Ready',
            'scheduletype' => 'Not Scheduled'
        );
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'campaign.update',
                    'protected' => true,
                    'username' => $this->ArrayUserInformation['Username'],
                    'password' => $this->ArrayUserInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        Core::LoadObject('split_tests');
        $SplitTestSettings = SplitTests::RetrieveTestOfACampaign($CampaignID, $this->ArrayUserInformation['UserID']);
        if ($SplitTestSettings != false) {
            SplitTests::Update($SplitTestSettings[0]['TestID'], array('SplitTestingStatus' => 'Pending'));
        }

        redirect(InterfaceAppURL(true) . '/user/campaign/overview/' . $CampaignID);
    }

    /**
     * Checks if there is an unsubscription link in provided string
     *
     * @return void
     * @author Mert Hurturk
     * */
    function unsubscription_link_required($input_value) {
        if (Emails::DetectTagInContent($input_value, '%Link:Unsubscribe%') == false) {
            $this->form_validation->set_message('unsubscription_link_required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0792']);
            return false;
        } else {
            return true;
        }
    }

}
