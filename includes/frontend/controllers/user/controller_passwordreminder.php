<?php
/**
 * Password Reminder controller
 *
 * @author Cem Hurturk
 */

class Controller_PasswordReminder extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('user_auth');
	Core::LoadObject('api');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	UserAuth::IsLoggedIn(InterfaceAppURL(true).'/user/overview/', false);
	// Check the login session, redirect based on the login session status - End

	// Events - Start {
	if ($this->input->post('Command') == 'ResetPassword')
		{
		$ArrayEventReturn = $this->_EventResetPassword($this->input->post('EmailAddress'));
		}
	// Events - End }
	
	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPassRemind'],
							);
	if (isset($ArrayEventReturn))
		{
		foreach ($ArrayEventReturn as $Key=>$Value)
			{
			$ArrayViewData[$Key] = $Value;
			}
		}
							
	$this->render('user/passwordreminder', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Resets the user password and sends the new login info by email
 *
 * @param string $MD5UserID 
 * @return void
 * @author Cem Hurturk
 */
function reset($MD5UserID)
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	UserAuth::IsLoggedIn(InterfaceAppURL(true).'/user/overview/', false);
	// Check the login session, redirect based on the login session status - End

	// Events - Start {
	if ($MD5UserID != '')
		{
		$ArrayEventReturn = $this->_EventResetPasswordConfirmed($MD5UserID);
		}
	// Events - End }
	
	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPassRemind'],
							);
	if (isset($ArrayEventReturn))
		{
		foreach ($ArrayEventReturn as $Key=>$Value)
			{
			$ArrayViewData[$Key] = $Value;
			}
		}
							
	$this->render('user/passwordreminder', $ArrayViewData);
	// Interface parsing - End
	}


/**
 * Reset password event
 *
 * @param string $Username 
 * @param string $Password 
 * @param string $Captcha 
 * @param string $RememberMe 
 * @return void
 * @author Cem Hurturk
 */
function _EventResetPassword($EmailAddress)
	{
	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'EmailAddress',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
								'rules'		=> 'required|valid_email',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Validate login information - Start {
	$ArrayAPIVars = array(
						'emailaddress'		=>	$EmailAddress,
						'customresetlink'	=>	rawurlencode(base64_encode(InterfaceAppURL(true).'/user/passwordreminder/reset/%s')),
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'user.passwordremind',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect email address
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0285'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0286'],
							);
				break;
			case '3':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0287'],
							);
				break;
			default:
				break;
			}
		}
	else
		{
		// Correct login information

		$_SESSION['PageMessageCache'] = array('Success', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0288'], $this->input->post('EmailAddress')));

		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/user/', 'location', '302');
		}
	// Validate login information - End }
	}

/**
 * Password reset link
 *
 * @param string $MD5UserID 
 * @return void
 * @author Cem Hurturk
 */
function _EventResetPasswordConfirmed($MD5UserID)
	{
	// Validate login information - Start {
	$ArrayAPIVars = array(
						'userid'		=> $MD5UserID,
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'user.passwordreset',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect email address
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0712'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0713'],
							);
				break;
			default:
				break;
			}
		}
	else
		{
		// Correct login information

		$_SESSION['PageMessageCache'] = array('Success', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0288'], $this->input->post('EmailAddress')));

		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/user/', 'location', '302');
		}
	// Validate login information - End }
	}

} // end of class User
?>