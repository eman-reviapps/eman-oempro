<?php

/**
 * Subscribers Controller
 *
 * @package default
 * @author Mert Hurturk
 * */
class Controller_Subscriber extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('lists');
        Core::LoadObject('subscribers');
        Core::LoadObject('segments');
        Core::LoadObject('custom_fields');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Edit controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function edit($list_id, $subscriber_id, $segment_id = 0) {
        $array_view_data = array();

        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscriber.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }
        // User privilege check - End }
        // Argument validation - Start {
        if ($list_id == '' || $list_id == 0)
            throw new Exception('You need to pass list id');
        if ($subscriber_id == '' || $subscriber_id == 0)
            throw new Exception('You need to pass subscriber id');
        // Argument validation - End }
        // Retrieve list information - Start {
        $arr_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), false, false);

        if (!$arr_list_information) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_custom_fields = CustomFields::RetrieveFields(
                        array('*'), array(
                    'RelOwnerUserID' => $this->array_user_information['UserID'],
                    array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $list_id . ')', 'auto-quote' => false)
                        )
        );

        if (!$array_custom_fields)
            $array_custom_fields = array();
        // Retrieve list information - End }
        // Save subscriber information - Start {
        if ($this->input->post('Command') == 'Save') {
            $parameters = array(
                'subscriberlistid' => $list_id,
                'subscriberid' => $subscriber_id,
                'emailaddress' => $this->input->post('EmailAddress'),
                'fields' => array()
            );
            $submitted_custom_fields = $this->input->post('FormValue_Fields');
            foreach ($array_custom_fields as $each) {
                $parameters['fields']['CustomField' . $each['CustomFieldID']] = $submitted_custom_fields['CustomField' . $each['CustomFieldID']];
            }
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'subscriber.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => $parameters
            ));

            if ($array_return['Success'] == false && ($array_return['ErrorCode'] == 10 || $array_return['ErrorCode'] == 9 || $array_return['ErrorCode'] == 8 || $array_return['ErrorCode'] == 7)) {
                if ($array_return['ErrorCode'] == 10) {
                    $array_view_data['PageErrorMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];
                    $array_view_data['PageErrorMessage'] .= ' (' . $array_return['ErrorCustomFieldTitles'] . ')';
                } else if ($array_return['ErrorCode'] == 9) {
                    $array_view_data['PageErrorMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1554'];
                    $array_view_data['PageErrorMessage'] .= ' (' . $array_return['ErrorCustomFieldTitles'] . ')';
                } else if ($array_return['ErrorCode'] == 8) {
                    $array_view_data['PageErrorMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1552'];
                    $array_view_data['PageErrorMessage'] .= ' (' . $array_return['ErrorCustomFieldTitles'] . ')';
                } else if ($array_return['ErrorCode'] == 7) {
                    $array_view_data['PageErrorMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1816'];
                }
            } else {
                $array_view_data['PageSuccessMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1323'];
            }
        }
        // Save subscriber information - End }
        // Retrieve subscriber information - Start {
        $arr_subscriber_information = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $subscriber_id), $list_id);
        // Retrieve subscriber information - End }
        // Retrieve subscriber suppression record - Start {
        Core::LoadObject('suppression_list');
        $arr_subscriber_suppression_record = SuppressionList::GetRecordForAnEmailAddress(
                        $arr_subscriber_information['EmailAddress'], $list_id);
        // Retrieve subscriber suppression record - End }
        // Retrieve all lists that this subscriber is subscribed to - Start {
        $arr_subscribed_lists = Lists::RetrieveSubscribedListsOfAnEmailAddress($this->array_user_information['UserID'], $arr_subscriber_information['EmailAddress']);
        // Retrieve all lists that this subscriber is subscribed to - End }

        foreach ($array_custom_fields as &$each_field) {
            $each_field['html'] = CustomFields::GenerateCustomFieldHTMLCode($each_field, $arr_subscriber_information['CustomField' . $each_field['CustomFieldID']]);
        }

        // Interface parsing - Start {
        $array_view_data = array_merge($array_view_data, array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSubscriberEdit'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $arr_list_information,
            'SegmentID' => $segment_id,
            'SubscriberInformation' => $arr_subscriber_information,
            'SubscribedLists' => $arr_subscribed_lists,
            'CustomFields' => $array_custom_fields,
            'CurrentMenuItem' => 'Subscribers',
            'SuppressionRecord' => $arr_subscriber_suppression_record
        ));

        $this->render('user/subscriber_edit', $array_view_data);
        // Interface parsing - End }
    }

    function snippetActivity($listId, $subscriberId) {
        Core::LoadObject('subscribers');

        $subscriberInformation = Subscribers::RetrieveSubscriber(
                        array('EmailAddress'), array('SubscriberID' => $subscriberId), $listId);

        $cacheMapper = O_Registry::instance()->getMapper('SubscriberActivityCache');
        $cache = $cacheMapper->findByListIdAndSubscriberId($listId, $subscriberId);

        $timeline = $cache->getActivityTimeline();

        if (count($timeline->getActivities()) < 1) {
            echo '{"nodata":true}';
            return;
        }

        $arrayActivityTypes = array(
            'O_SubscriberActivityOpen' => 'open',
            'O_SubscriberActivityClick' => 'click',
            'O_SubscriberActivityBounce' => 'bounce',
            'O_SubscriberActivityForward' => 'forward',
            'O_SubscriberActivityBrowserView' => 'browserView',
            'O_SubscriberActivityUnsubscription' => 'unsubscription'
        );

        $activityJson = array();
        $json = '{"startRead":"' . date('M j, Y', $timeline->getStartTimestamp()) . '","endRead":"' . date('M j, Y', $timeline->getEndTimestamp()) . '","start":"' . $timeline->getStartTimestamp() . '","end":"' . $timeline->getEndTimestamp() . '",activities:[';
        foreach ($timeline->getActivities() as $each) {
            $activityType = $arrayActivityTypes[get_class($each)];
            $activityTimestamp = $each->getTimestamp();
            $activityDate = date('M j, Y H:i', $activityTimestamp);
            $entityType = $each->getEntityType();
            $entityName = $each->getEntityName();
            $jsonString = '{"type":"' . $activityType . '","timestamp":"' . $activityTimestamp . '", "date":"' . $activityDate . '", "entityType":"' . $entityType . '","entityName":"' . htmlspecialchars($entityName, ENT_QUOTES) . '"';
            if (get_class($each) === 'O_SubscriberActivityClick') {
                $jsonString .= ',"linkTitle":"' . htmlspecialchars($each->getLinkTitle(), ENT_QUOTES) . '"';
                $jsonString .= ',"linkURL":"' . htmlspecialchars($each->getLinkUrl(), ENT_QUOTES) . '"';
            }

            $jsonString .= '}';
            $activityJson[] = $jsonString;
        }
        $json .= implode(',', $activityJson);
        $json .= ']}';

        echo $json;
    }

    /**
     * Unsubscribe controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function unsubscribe($list_id, $subscriber_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscriber.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }
        // User privilege check - End }
        // Argument validation - Start {
        if ($list_id == '' || $list_id == 0)
            throw new Exception('You need to pass list id');
        if ($subscriber_id == '' || $subscriber_id == 0)
            throw new Exception('You need to pass subscriber id');
        // Argument validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscriber.unsubscribe',
                    'protected' => false,
                    'parameters' => array(
                        'listid' => $list_id,
                        'subscriberid' => $subscriber_id,
                    )
        ));

        $_SESSION['PageMessageCache'] = array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['1324']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/subscribers/browse/' . $list_id, 'location', '302');
    }

    /**
     * Delete controller method
     *
     * @return void
     * @author Mert Hurturk
     * */
    function delete($list_id, $subscriber_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Subscribers.Delete'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            return;
        }
        // User privilege check - End }
        // Argument validation - Start {
        if ($list_id == '' || $list_id == 0)
            throw new Exception('You need to pass list id');
        if ($subscriber_id == '' || $subscriber_id == 0)
            throw new Exception('You need to pass subscriber id');
        // Argument validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'subscribers' => $subscriber_id,
                    )
        ));

        $this->load->helper('url');
        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1125']);
        redirect(InterfaceAppURL(true) . '/user/subscribers/browse/' . $list_id, 'location', '302');
    }

}
