<?php

/**
 * Email controller
 *
 * @author Mert Hurturk
 */
class Controller_Email extends MY_Controller {

    /**
     * Available modes for creating email
     *
     * @var array
     */
    private $modes = array('confirmation', 'autoresponder', 'campaign');

    /**
     * Available flows for creating email
     *
     * @var array
     */
    private $flows = array('fromscratch', 'fromtemplate', 'copyfromlist', 'fetchfromurl', 'previouscampaign', 'dragdropemail');

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        $this->load->helper('url');

        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('lists');
        Core::LoadObject('emails');
        Core::LoadObject('wizard');
        Core::LoadObject('email_data');
        Core::LoadObject('split_tests');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Create controller
     *
     * @author Mert Hurturk
     * */
    function create($mode, $entity_id, $flow = '', $step_from_url = 0) {
        // Argument validation - Start {
        if (!in_array($mode, $this->modes))
            throw new Exception('Not a valid mode');

        if ($entity_id == '' || $entity_id == 0)
            throw new Exception('You need to pass entity id');

        if ($flow != '' && in_array($flow, $this->flows) == false)
            throw new Exception('Not a valid flow');
        // Argument validation - End }
        // Declare steps - Start {
        $step_settings = new Step('settings', array('Email_wizard_functions', 'step_pre_settings'), array('Email_wizard_functions', 'step_post_settings'));
        $step_settings->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0110']);

        $step_content = new Step('content', array('Email_wizard_functions', 'step_pre_content'), array('Email_wizard_functions', 'step_post_content'));
        $step_content->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0749']);

        $step_preview = new Step('preview', array('Email_wizard_functions', 'step_pre_preview'), array('Email_wizard_functions', 'step_post_preview'));
        $step_preview->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0750']);

        $step_gallery = new Step('gallery', array('Email_wizard_functions', 'step_pre_gallery'), array('Email_wizard_functions', 'step_post_gallery'));
        $step_gallery->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0793']);

        $step_copyfromlist = new Step('copyfromlist', array('Email_wizard_functions', 'step_pre_copyfromlist'), array('Email_wizard_functions', 'step_post_copyfromlist'));
        $step_copyfromlist->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0992']);

        $step_fetchurl = new Step('fetchurl', array('Email_wizard_functions', 'step_pre_fetchurl'), array('Email_wizard_functions', 'step_post_fetchurl'));
        $step_fetchurl->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0790']);

        $step_copyfromcampaign = new Step('copycampaign', array('Email_wizard_functions', 'step_pre_copycampaign'), array('Email_wizard_functions', 'step_post_copycampaign'));
        $step_copyfromcampaign->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['1253']);

//        $step_dragdropemail = new Step('dragdropemail', array('Email_wizard_functions', 'step_pre_dragdropemail'), array('Email_wizard_functions', 'step_post_dragdropemail'));
//        $step_copyfromcampaign->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['9186']);
        // Declare steps - End }
        // Declare flows - Start {
        $flow_fromscratch = new Flow(array($step_settings, $step_content, $step_preview));
        $flow_fromscratch->set_id('fromscratch');
        $flow_fromscratch->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0741']);
        $flow_fromscratch->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0981']);

        $flow_fromtemplate = new Flow(array($step_settings, $step_gallery, $step_content, $step_preview));
        $flow_fromtemplate->set_id('fromtemplate');
        $flow_fromtemplate->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0743']);
        $flow_fromtemplate->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0744']);

        $flow_copyfromlist = new Flow(array($step_copyfromlist, $step_settings, $step_content, $step_preview));
        $flow_copyfromlist->set_id('copyfromlist');
        $flow_copyfromlist->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0982']);
        $flow_copyfromlist->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0983']);

        $flow_fetchurl = new Flow(array($step_settings, $step_fetchurl, $step_preview));
        $flow_fetchurl->set_id('fetchfromurl');
        $flow_fetchurl->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0747']);
        $flow_fetchurl->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0748']);

        $flow_dragdropemail = new Flow(array($step_settings, $step_content, $step_preview));
        $flow_dragdropemail->set_id('dragdropemail');
//        $flow_dragdropemail->set_id('fromscratch');
        $flow_dragdropemail->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['9186']);
        $flow_dragdropemail->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['9186']);

        if ($flow != '' && $flow == 'previouscampaign') {
            if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
                $email_information = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
                if ($email_information->get_mode() == 'Import') {
                    $flow_copyfromcampaign = new Flow(array($step_copyfromcampaign, $step_settings, $step_fetchurl, $step_preview));
                } else {
                    $flow_copyfromcampaign = new Flow(array($step_copyfromcampaign, $step_settings, $step_content, $step_preview));
                }
            } else {
                $flow_copyfromcampaign = new Flow(array($step_copyfromcampaign, $step_settings, $step_content, $step_preview));
            }
        } else {
            $flow_copyfromcampaign = new Flow(array($step_copyfromcampaign, $step_settings, $step_content, $step_preview));
        }
        $flow_copyfromcampaign->set_id('previouscampaign');
        $flow_copyfromcampaign->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0745']);
        $flow_copyfromcampaign->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0746']);
        // Declare flows - End }
        // Relate modes and flows - Start {
        $arr_modes_with_flows = array();
        $arr_modes_with_flows['confirmation'] = array($flow_dragdropemail, $flow_fromscratch, $flow_fromtemplate, $flow_copyfromlist, $flow_fetchurl);
        $arr_modes_with_flows['autoresponder'] = array($flow_dragdropemail, $flow_fromscratch, $flow_fromtemplate, $flow_fetchurl);
        $arr_modes_with_flows['campaign'] = array($flow_dragdropemail, $flow_fromscratch, $flow_fromtemplate, $flow_copyfromcampaign, $flow_fetchurl);
//        $arr_modes_with_flows['campaign'] = array($flow_fromscratch, $flow_fromtemplate, $flow_copyfromcampaign, $flow_fetchurl);
        // Relate modes and flows - End }
        // Setup page titles - Start {
        $arr_page_titles = array();
        $arr_page_titles['confirmation'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0979'];
        $arr_page_titles['autoresponder'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1021'];
        $arr_page_titles['campaign'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1254'];
        // Setup page titles - End }
        // If a flow is set, start wizard - Start {
        if ($flow != '') {
            // Find the current flow object - Start {
            $current_flow_object = null;
            foreach ($arr_modes_with_flows[$mode] as $each_flow_object) {
                if ($each_flow_object->get_id() === $flow) {
                    $current_flow_object = $each_flow_object;
                    break;
                }
            }
            // Find the current flow object - End }

            $wizard_name = 'Email_wizard' . $entity_id;
            if (!isset($_SESSION[SESSION_NAME]['WizardSessionName']) || $_SESSION[SESSION_NAME]['WizardSessionName'] != $wizard_name) {
                $_SESSION[SESSION_NAME]['WizardSessionName'] = $wizard_name;
            }
            $wizard = WizardFactory::get($wizard_name, $current_flow_object);

            $wizard->set_extra_parameter('mode', $mode);
            $wizard->set_extra_parameter('flow', $flow);
            $wizard->set_extra_parameter('entity_id', $entity_id);
            $wizard->set_extra_parameter('user_id', $this->array_user_information['UserID']);
            $wizard->set_extra_parameter('user_information', $this->array_user_information);

            if ($step_from_url !== 0)
                $wizard->set_current_step($step_from_url);

            WizardFactory::save($wizard_name, $wizard);

            if ($this->input->post('FormSubmit') === false) {
                $wizard->run_pre();
            } else {
                $wizard->run_post();
            }
        }
        // If a flow is set, start wizard - End }
        // If flow is not set, clear all session variables - Start {
        if ($flow == '') {
            WizardFactory::delete($wizard_name);
            $_SESSION[SESSION_NAME]['EmailInformation'] = array();
            $_SESSION[SESSION_NAME]['WizardSessionName'] = '';
            unset($_SESSION[SESSION_NAME]['EmailInformation']);
            unset($_SESSION[SESSION_NAME]['WizardSessionName']);
            unset($_SESSION[SESSION_NAME]['StepMessage']);
            foreach ($_SESSION[SESSION_NAME] as $key => $value) {
                if (preg_match('/Email_wizard(.*)/', $key, $arrayMatches) == 1) {
                    unset($_SESSION[SESSION_NAME][$key]);
                }
            }
        }
        // If flow is not set, clear all session variables - End }
        // Interface parsing - Start {
        $array_form_field_defaults = array();
        if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $array_form_field_defaults['EmailName'] = $email->get_email_name();
            $array_form_field_defaults['FromName'] = $email->get_from_name();
            $array_form_field_defaults['FromEmail'] = $email->get_from_email();
            $array_form_field_defaults['ReplyToName'] = $email->get_replyto_name();
            $array_form_field_defaults['ReplyToEmail'] = $email->get_replyto_email();
            $array_form_field_defaults['SameNameEmail'] = $email->get_same_from_replyto() ? true : false;
            $array_form_field_defaults['ImageEmbedding'] = $email->get_image_embedding_enabled();
            $array_form_field_defaults['ContentType'] = $email->get_content_type();
            $array_form_field_defaults['Subject'] = $email->get_subject();
            $array_form_field_defaults['HTMLContent'] = $email->get_html_content();
            $array_form_field_defaults['PlainContent'] = $email->get_plain_content();
            $array_form_field_defaults['FetchURL'] = $email->get_fetch_url();
            $array_form_field_defaults['FetchPlainURL'] = $email->get_fetch_plain_url();
            $array_form_field_defaults['Mode'] = $email->get_mode();
        } else {
            $array_form_field_defaults['EmailName'] = '';
            $array_form_field_defaults['FromName'] = '';
            $array_form_field_defaults['FromEmail'] = '';
            $array_form_field_defaults['ReplyToName'] = '';
            $array_form_field_defaults['ReplyToEmail'] = '';
            $array_form_field_defaults['SameNameEmail'] = true;
            $array_form_field_defaults['ImageEmbedding'] = '';
            $array_form_field_defaults['ContentType'] = 'Both';
            $array_form_field_defaults['Subject'] = '';
            $array_form_field_defaults['HTMLContent'] = '';
            $array_form_field_defaults['PlainContent'] = '';
            $array_form_field_defaults['FetchURL'] = '';
            $array_form_field_defaults['FetchPlainURL'] = '';
            $array_form_field_defaults['Mode'] = '';
        }

        $sub_section = ($flow == '' ? 'flowlist' : $wizard->get_current_step_object()->get_id());

        $array_view_data = array();

        if ($sub_section == 'flowlist') {
            $array_view_data['AvailableFlows'] = $arr_modes_with_flows[$mode];
        } else {
            $array_view_data['CurrentFlow'] = $current_flow_object;
            $array_view_data['CurrentStep'] = $wizard->get_current_step_object();
            $array_view_data['Wizard'] = $wizard;
        }

        if (isset($_SESSION[SESSION_NAME]['StepMessage'])) {
            $message = unserialize($_SESSION[SESSION_NAME]['StepMessage']);
            unset($_SESSION[SESSION_NAME]['StepMessage']);

            $array_view_data = array_merge($array_view_data, $message->get_all_parameters_as_array());
            unset($message);
        }

        if ($mode == 'campaign') {
            $current_menu_item = 'Campaigns';
        } else if ($mode == 'confirmation') {
            $current_menu_item = 'Lists';
        } else if ($mode == 'autoresponder') {
            $current_menu_item = 'Lists';
        }


        $array_view_data = array_merge($array_view_data, array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . $arr_page_titles[$mode],
            'Title' => $arr_page_titles[$mode],
            'UserInformation' => $this->array_user_information,
            'EntityID' => $entity_id,
            'Mode' => $mode,
            'SubSection' => $sub_section,
            'FieldDefaultValues' => $array_form_field_defaults,
            'EditEvent' => false,
            'CurrentMenuItem' => $current_menu_item
        ));

        if ($flow != '') {
            $array_view_data['FormURL'] = InterfaceAppURL(true) . '/user/email/create/' . $mode . '/' . $entity_id . '/' . $current_flow_object->get_id();
        }

        // If mode is campaign, check if a split test exists for this campaign - Start {
        if ($mode == 'campaign') {
            $_SESSION[SESSION_NAME]['SplitTestInformation'] = false;
            $split_test_information = SplitTests::RetrieveTestOfACampaign($entity_id, $this->array_user_information['UserID']);
            if ($split_test_information != false) {
                $_SESSION[SESSION_NAME]['SplitTestInformation'] = $split_test_information[0];
            }
        }
        // If mode is campaign, check if a split test exists for this campaign - End }

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/email_create', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Edit controller
     *
     * @author Mert Hurturk
     * */
    function edit($mode, $entity_id, $email_id, $step_from_url = 0) {
        // Argument validation - Start {
        if (!in_array($mode, $this->modes))
            throw new Exception('Not a valid mode');

        if ($entity_id == '' || $entity_id == 0)
            throw new Exception('You need to pass entity id');

        if ($email_id == '' || $email_id == 0)
            throw new Exception('You need to pass email id');

        if (func_num_args() > 4)
            show_404('');

        if (!is_numeric($step_from_url))
            show_404('');
        // Argument validation - End }
        // Retrieven email information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'emailid' => $email_id
                    )
        ));
        $email_information = $array_return['EmailInformation'];

        $email = new Email_data();
        $email->set_id($email_information['EmailID']);
        $email->set_email_name($email_information['EmailName']);
        $email->set_from_name($email_information['FromName']);
        $email->set_from_email($email_information['FromEmail']);
        $email->set_replyto_name($email_information['ReplyToName']);
        $email->set_replyto_email($email_information['ReplyToEmail']);
        $email->set_same_from_replyto($email->get_from_name() == $email->get_replyto_name() ? true : false);
        $email->set_html_content($email_information['HTMLContent']);
        $email->set_plain_content($email_information['PlainContent']);
        $email->set_image_embedding_enabled(IMAGE_EMBEDDING_ENABLED ? $email_information['ImageEmbedding'] == 'Enabled' : false);
        $email->set_content_type($email_information['ContentType']);
        $email->set_subject($email_information['Subject']);
        $email->set_template_id($email_information['RelTemplateID']);
        $email->set_fetch_url($email_information['FetchURL']);
        $email->set_fetch_plain_url($email_information['FetchPlainURL']);
        //Eman
        $email->setScreenshot($email_information['ScreenshotImage']);
        $email->setHtml_pure($email_information['HTMLPure']);

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        // Retrieven email information - End }
        // Decide flow - Start {
        $flow = 'fromscratch';
        if ($email_information['FetchURL'] != '' || $email_information['FetchPlainURL'] != '') {
            $flow = 'fetchfromurl';
        }

        if ($email_information['RelTemplateID'] != '0') {
            $flow = 'fromtemplate';
        }
        // Decide flow - End }
        // Declare steps - Start {
        $step_settings = new Step('settings', array('Email_wizard_functions', 'step_pre_settings'), array('Email_wizard_functions', 'step_post_settings'));
        $step_settings->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0110']);

        $step_content = new Step('content', array('Email_wizard_functions', 'step_pre_content'), array('Email_wizard_functions', 'step_post_content'));
        $step_content->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0749']);

        $step_preview = new Step('preview', array('Email_wizard_functions', 'step_pre_preview'), array('Email_wizard_functions', 'step_post_preview'));
        $step_preview->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0750']);

        $step_gallery = new Step('gallery', array('Email_wizard_functions', 'step_pre_gallery'), array('Email_wizard_functions', 'step_post_gallery'));
        $step_gallery->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0793']);

        $step_fetchurl = new Step('fetchurl', array('Email_wizard_functions', 'step_pre_fetchurl'), array('Email_wizard_functions', 'step_post_fetchurl'));
        $step_fetchurl->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0790']);
        // Declare steps - End }
        // Declare flows - Start {
        $flow_fromscratch = new Flow(array($step_settings, $step_content, $step_preview));
        $flow_fromscratch->set_id('fromscratch');
        $flow_fromscratch->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0741']);
        $flow_fromscratch->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0981']);

        $flow_fromtemplate = new Flow(array($step_settings, $step_gallery, $step_content, $step_preview));
        $flow_fromtemplate->set_id('fromtemplate');
        $flow_fromtemplate->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0743']);
        $flow_fromtemplate->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0744']);

        $flow_fetchurl = new Flow(array($step_settings, $step_fetchurl, $step_preview));
        $flow_fetchurl->set_id('fetchfromurl');
        $flow_fetchurl->set_title(ApplicationHeader::$ArrayLanguageStrings['Screen']['0747']);
        $flow_fetchurl->set_description(ApplicationHeader::$ArrayLanguageStrings['Screen']['0748']);
        // Declare flows - End }
        // Relate modes and flows - Start {
        $arr_modes_with_flows = array();
        $arr_modes_with_flows['confirmation'] = array($flow_fromscratch, $flow_fromtemplate, $flow_fetchurl);
        $arr_modes_with_flows['autoresponder'] = array($flow_fromscratch, $flow_fromtemplate, $flow_fetchurl);
        $arr_modes_with_flows['campaign'] = array($flow_fromscratch, $flow_fromtemplate, $flow_fetchurl);
        // Relate modes and flows - End }
        // Setup page titles - Start {
        $arr_page_titles = array();
        $arr_page_titles['confirmation'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1311'];
        $arr_page_titles['autoresponder'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1312'];
        $arr_page_titles['campaign'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1310'];
        // Setup page titles - End }
        // Start wizard - Start {
        // Find the current flow object - Start {
        $current_flow_object = null;
        foreach ($arr_modes_with_flows[$mode] as $each_flow_object) {
            if ($each_flow_object->get_id() === $flow) {
                $current_flow_object = $each_flow_object;
                break;
            }
        }
        // Find the current flow object - End }
        $wizard_name = 'Email_wizard' . $entity_id . $email_id;
        if (!isset($_SESSION[SESSION_NAME]['WizardSessionName']) || $_SESSION[SESSION_NAME]['WizardSessionName'] != $wizard_name) {
            $_SESSION[SESSION_NAME]['WizardSessionName'] = $wizard_name;
        }
        $wizard = WizardFactory::get($wizard_name, $current_flow_object);

        $wizard->set_extra_parameter('mode', $mode);
        $wizard->set_extra_parameter('flow', $flow);
        $wizard->set_extra_parameter('entity_id', $entity_id);
        $wizard->set_extra_parameter('user_id', $this->array_user_information['UserID']);
        $wizard->set_extra_parameter('user_information', $this->array_user_information);

        $wizard->set_last_step($current_flow_object->get_step_count());

        if ($step_from_url !== 0)
            $wizard->set_current_step($step_from_url);

        WizardFactory::save($wizard_name, $wizard);

        if ($this->input->post('FormSubmit') === false) {
            $wizard->run_pre();
        } else {
            $wizard->run_post();
        }
        // Start wizard - End }
        // Interface parsing - Start {
        $array_form_field_defaults = array();
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        $array_form_field_defaults['EmailName'] = $email->get_email_name();
        $array_form_field_defaults['FromName'] = $email->get_from_name();
        $array_form_field_defaults['FromEmail'] = $email->get_from_email();
        $array_form_field_defaults['ReplyToName'] = $email->get_replyto_name();
        $array_form_field_defaults['ReplyToEmail'] = $email->get_replyto_email();
        $array_form_field_defaults['SameNameEmail'] = $email->get_same_from_replyto() ? true : false;
        $array_form_field_defaults['ImageEmbedding'] = $email->get_image_embedding_enabled();
        $array_form_field_defaults['ContentType'] = $email->get_content_type();
        $array_form_field_defaults['Subject'] = $email->get_subject();
        $array_form_field_defaults['HTMLContent'] = $email->get_html_content();
        $array_form_field_defaults['PlainContent'] = $email->get_plain_content();
        $array_form_field_defaults['FetchURL'] = $email->get_fetch_url();
        $array_form_field_defaults['FetchPlainURL'] = $email->get_fetch_plain_url();
        $array_form_field_defaults['RelTemplateID'] = $email->get_template_id();

        $sub_section = $wizard->get_current_step_object()->get_id();

        $array_view_data = array();

        $array_view_data['CurrentFlow'] = $current_flow_object;
        $array_view_data['CurrentStep'] = $wizard->get_current_step_object();
        $array_view_data['Wizard'] = $wizard;

        if (isset($_SESSION[SESSION_NAME]['StepMessage'])) {
            $message = unserialize($_SESSION[SESSION_NAME]['StepMessage']);
            unset($_SESSION[SESSION_NAME]['StepMessage']);

            $array_view_data = array_merge($array_view_data, $message->get_all_parameters_as_array());
            unset($message);
        }

        if ($mode == 'campaign') {
            $current_menu_item = 'Campaigns';
        } else if ($mode == 'confirmation') {
            $current_menu_item = 'Lists';
        } else if ($mode == 'autoresponder') {
            $current_menu_item = 'Lists';
        }

        $array_view_data = array_merge($array_view_data, array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . $arr_page_titles[$mode],
            'Title' => $arr_page_titles[$mode],
            'UserInformation' => $this->array_user_information,
            'EntityID' => $entity_id,
            'Mode' => $mode,
            'SubSection' => $sub_section,
            'FieldDefaultValues' => $array_form_field_defaults,
            'FormURL' => InterfaceAppURL(true) . '/user/email/edit/' . $mode . '/' . $entity_id . '/' . $email_id,
            'EditEvent' => true,
            'CurrentMenuItem' => $current_menu_item
        ));

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        // If mode is campaign, check if a split test exists for this campaign - Start {
        if ($mode == 'campaign') {
            $_SESSION[SESSION_NAME]['SplitTestInformation'] = false;
            $split_test_information = SplitTests::RetrieveTestOfACampaign($entity_id, $this->array_user_information['UserID']);
            if ($split_test_information != false) {
                $_SESSION[SESSION_NAME]['SplitTestInformation'] = $split_test_information[0];
            }
        }
        // If mode is campaign, check if a split test exists for this campaign - End }
        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/email_create', $array_view_data);
        // Interface parsing - End }
    }

    function save_content_draft() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);

        $CI = & get_instance();

        $email->set_content_type($CI->input->post('ContentType'));
        $email->set_html_content($CI->input->post('HTMLContent'));
        $email->set_plain_content($CI->input->post('PlainContent'));
        $email->set_subject($CI->input->post('Subject'));

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);

        if ($CI->input->post('EditEvent')) {
            if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                $validate_scope = 'OptIn';
            } elseif ($wizard->get_extra_parameter('mode') == 'autoresponder') {
                $validate_scope = 'AutoResponder';
            } elseif ($wizard->get_extra_parameter('mode') == 'campaign') {
                $validate_scope = 'Campaign';
            }

            // Update email - Start {
            $array_api_vars = array(
                'validatescope' => $validate_scope,
                'emailid' => $email->get_id(),
                'subject' => $email->get_subject(),
                'plaincontent' => $email->get_content_type() == 'Plain' || $email->get_content_type() == 'Both' ? $email->get_plain_content() : '',
                'htmlcontent' => $email->get_content_type() == 'HTML' || $email->get_content_type() == 'Both' ? $email->get_html_content() : ''
            );

            $array_return = API::call(array('format' => 'array', 'command' => 'email.update', 'protected' => true, 'username' => $user_information['Username'], 'password' => $user_information['Password'], 'parameters' => $array_api_vars));
            // Update email - End }
        }
    }

    /**
     * Preview controller
     *
     * @author Mert Hurturk
     * */
    function preview($email_id, $type = 'html', $mode = 'campaign', $entity_id = 0, $command = '') {
        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('subscribers');
        Core::LoadObject('lists');
        Core::LoadObject('emails');
        Core::LoadObject('campaigns');
        Core::LoadObject('personalization');
        // Load other modules - End

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'emailid' => $email_id
                    )
        ));
        $email_information = $array_return['EmailInformation'];

        // Select one of the recipient lists from the campaign - Start
        $campaign_information = array();
        if ($mode == 'campaign') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'campaign.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'campaignid' => $entity_id
                        )
            ));
            $campaign_information = $array_return['Campaign'];
            unset($array_return);
            $array_list = Lists::RetrieveList(array('*'), array('ListID' => $campaign_information['RecipientLists'][0]), false);
        } else if ($mode == 'confirmation') {
            $array_list = Lists::RetrieveList(arraY('*'), array('ListID' => $entity_id));
        } else if ($mode == 'autoresponder') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $entity_id
                        )
            ));
            $autoresponder_information = $array_return['AutoResponder'];
            unset($array_return);
            $array_list = Lists::RetrieveList(arraY('*'), array('ListID' => $autoresponder_information['RelListID']));
        }
        // Select one of the recipient lists from the campaign - End
        // Retrieve a random subscriber from the target list - Start
        if (is_bool($array_list) == false && $array_list != false) {
            $array_random_subscriber = Subscribers::SelectRandomSubscriber($array_list['ListID'], true);
        } else {
            $array_random_subscriber = array(
                'SubscriberID' => 1,
                'EmailAddress' => 'test@test.com',
                'BounceType' => 'Not Bounced',
                'SubscriptionStatus' => 'Subscribed',
                'SubscriptionDate' => date('Y-m-d H:i:s'),
                'SubscriptionIP' => '127.0.0.1',
                'UnsubscriptionDate' => '0000-00-00 00:00:00',
                'UnsubscriptionIP' => '0.0.0.0',
                'OptInDate' => date('Y-m-d H:i:s')
            );
        }
        // Retrieve a random subscriber from the target list - End

        $email_information['Subject'] = Personalization::Personalize($email_information['Subject'], array('Subscriber', 'User'), $array_random_subscriber, $this->array_user_information, $array_list, $campaign_information, array(), true);

        if ($command != '' && $command == 'source') {
            if ($type == 'html') {
                if ($email_information['FetchURL'] != '') {
                    $personalized_url = Personalization::Personalize($email_information['FetchURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->array_user_information, $array_list, $campaign_information, array(), false);
                    $show_content = Campaigns::FetchRemoteContent($personalized_url);
                } else {
                    $show_content = $email_information['HTMLContent'];
                }

                if ($show_content == '') {
                    $type = 'plain';
                }

                if ($type != 'plain') {
                    // Plug-in hook - Start
                    $array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
                    $show_content = $array_plugin_return_vars[1];
                    // Plug-in hook - End
                    // Plug-in hook - Start
                    $array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
                    $show_content = $array_plugin_return_vars[1];
                    // Plug-in hook - End
                }
            }

            if ($type == 'plain') {
                if ($email_information['FetchPlainURL'] != '') {
                    $personalized_url = Personalization::Personalize($email_information['FetchPlainURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->array_user_information, $array_list, $campaign_information, array(), false);
                    $show_content = Campaigns::FetchRemoteContent($personalized_url);
                } else {
                    $show_content = $email_information['PlainContent'];
                }

                // Plug-in hook - Start
                $array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
                $show_content = $array_plugin_return_vars[1];
                // Plug-in hook - End
                // Plug-in hook - Start
                $array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
                $show_content = $array_plugin_return_vars[1];
                // Plug-in hook - End
            }

            // Add header/footer to the email (if exists in the user group) - Start {
            if ($mode != 'confirmation') {
                $ArrayReturn = Personalization::AddEmailHeaderFooter(($type == 'plain' ? $show_content : ''), ($type == 'html' ? $show_content : ''), $this->array_user_information['GroupInformation']);
                if ($type == 'plain') {
                    $show_content = $ArrayReturn[0];
                } else {
                    $show_content = $ArrayReturn[1];
                }
            }
            // Add header/footer to the email (if exists in the user group) - End }
            // Perform personalization (disable personalization and links if subscriber information not provided) - Start
            $show_content = Personalization::Personalize($show_content, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent', 'OptLinks'), $array_random_subscriber, $this->array_user_information, $array_list, $campaign_information, array(), true);
            // Perform personalization (disable personalization and links if subscriber information not provided) - End

            if ($type == 'plain') {
                $show_content = '<pre>' . $show_content . '</pre>';
            }

            print $show_content;
            exit;
        }

        // Interface parsing - Start {
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailPreview'],
            'UserInformation' => $this->array_user_information,
            'EmailInformation' => $email_information,
            'Mode' => $mode,
            'EntityID' => $entity_id,
            'EmailID' => $email_id,
            'Type' => $type
        );

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/email_preview', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
     * Checks if there is an optin reject link in provided string
     *
     * @return boolean
     * @author Mert Hurturk
     * */
    function _reject_link_required($input_value, $param, $callback = '_reject_link_required') {
        if ($this->array_user_information['GroupInformation']['ForceRejectOptLink'] != 'Enabled') {
            return true;
        }

        if (Emails::DetectTagInContent($input_value, '%Link:Reject%') == false) {
            $this->form_validation->set_message($callback, ApplicationHeader::$ArrayLanguageStrings['Screen']['0985']);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if there is an optin confirm link in provided string
     *
     * @return boolean
     * @author Mert Hurturk
     * */
    function _confirm_link_required($input_value, $param, $callback = '_confirm_link_required') {
        if (Emails::DetectTagInContent($input_value, '%Link:Confirm%') == false) {
            $this->form_validation->set_message($callback, ApplicationHeader::$ArrayLanguageStrings['Screen']['1060']);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if there is an unsubscription link in provided string
     *
     * @return boolean
     * @author Mert Hurturk
     * */
    function _unsubscription_link_required($input_value, $param, $callback = '_unsubscription_link_required') {
        if (Emails::DetectTagInContent($input_value, '%Link:Unsubscribe%') == false) {
            $this->form_validation->set_message($callback, ApplicationHeader::$ArrayLanguageStrings['Screen']['0792']);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if there is an unsubscription link in provided remote content url
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _remote_content_unsubscription_link_required($input_value) {
        Core::LoadObject('campaigns');
        Core::LoadObject('emails');

        $content = Campaigns::FetchRemoteContent($input_value);

        return $this->_unsubscription_link_required($content, false, '_remote_content_unsubscription_link_required');
    }

    /**
     * Checks if there is a reject link in provided remote content url
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _remote_content_reject_link_required($input_value) {
        Core::LoadObject('campaigns');

        $content = Campaigns::FetchRemoteContent($input_value);

        return $this->_reject_link_required($content, false, '_remote_content_reject_link_required');
    }

    /**
     * Checks if there is a reject link in provided remote content url
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _remote_content_confirm_link_required($input_value) {
        Core::LoadObject('campaigns');

        $content = Campaigns::FetchRemoteContent($input_value);

        return $this->_confirm_link_required($content, false, '_remote_content_confirm_link_required');
    }

    function _check_for_forbidden_emails($InputValue) {
        $ForbiddenEmails = explode("\n", FORBIDDEN_FROM_ADDRESSES);
        $InputValue = strtolower($InputValue);

        foreach ($ForbiddenEmails as $EachForbidden) {
            $EachForbidden = strtolower($EachForbidden);

            if (strstr($EachForbidden, '*') == false) {
                if (trim($EachForbidden) == $InputValue) {
                    $this->form_validation->set_message('_check_for_forbidden_emails', ApplicationHeader::$ArrayLanguageStrings['Screen']['1620']);
                    return false;
                }
            } else {
                $Pattern = '/' . str_replace('*', '(.*)', trim($EachForbidden)) . '/i';
                if ((preg_match($Pattern, $InputValue) > 0) || (preg_match($Pattern, $InputValue) != false)) {
                    $this->form_validation->set_message('_check_for_forbidden_emails', ApplicationHeader::$ArrayLanguageStrings['Screen']['1620']);
                    return false;
                }
            }
        }

        return true;
    }

}

class Email_wizard_functions {

    /**
     * This function is called before rendering settings screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_settings() {
        $CI = & get_instance();

        if ($CI->input->post('FormSubmit') == false) {
            $CI->form_validation->set_rules("SameNameEmail");
            $CI->form_validation->set_rules("ReplyToName");
            $CI->form_validation->set_rules("ReplyToEmail");
            $CI->form_validation->set_rules("FromName");
            $CI->form_validation->set_rules("FromEmail");

            $CI->form_validation->run();
        }

        $message = new Session_message();
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $message->set_parameter('SplitTestCampaign', false);
        $mode = $wizard->get_extra_parameter('mode');

        if ($mode == 'campaign') {
            $entity_id = $wizard->get_extra_parameter('entity_id');
            $user_information = $wizard->get_extra_parameter('user_information');
            Core::LoadObject('campaigns');

            $campaign_information = Campaigns::RetrieveCampaigns_Enhanced(array(
                        'Criteria' => array(
                            array(
                                'Column' => '%c%.RelOwnerUserID',
                                'Operator' => '=',
                                'Value' => $user_information['UserID']
                            ),
                            array(
                                'Link' => 'AND',
                                'Column' => '%c%.CampaignID',
                                'Operator' => '=',
                                'Value' => $entity_id
                            )
            )));

            $campaign_information = $campaign_information[0];

            if ($campaign_information['SplitTest'] != false) {
                $message->set_parameter('SplitTestCampaign', true, true);
                $wizard->set_extra_parameter('split_test_campaign', true);
                WizardFactory::save($_SESSION[SESSION_NAME]['WizardSessionName'], $wizard);
            }
        }
        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    /**
     * This function is called after settings form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_settings() {
        $CI = & get_instance();

        // Field Validations - Start {
        $CI->form_validation->set_rules("SameNameEmail", "");
        $CI->form_validation->set_rules("ReplyToName", "");
        $CI->form_validation->set_rules("ReplyToEmail", "");
        $CI->form_validation->set_rules("FromName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0767'], 'required');
        $CI->form_validation->set_rules("FromEmail", ApplicationHeader::$ArrayLanguageStrings['Screen']['0768'], 'required|valid_email|callback__check_for_forbidden_emails');

        if ($CI->input->post('SameNameEmail') == false) {
            $CI->form_validation->set_rules("ReplyToName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0772'], 'required');
            $CI->form_validation->set_rules("ReplyToEmail", ApplicationHeader::$ArrayLanguageStrings['Screen']['0773'], 'required|valid_email');
        }
        // Field Validations - End }
        // Run validation - Start {
        if ($CI->form_validation->run() == false) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];
            return false;
        }
        // Run validation - End }
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }

        if ($email->get_email_name() == '' || $CI->input->post('EmailName') != false) {
            $email->set_email_name($CI->input->post('EmailName'));
        }
        $email->set_from_name($CI->input->post('FromName'));
        $email->set_from_email($CI->input->post('FromEmail'));

        if ($CI->input->post('SameNameEmail') == false) {
            $email->set_same_from_replyto(false);
            $email->set_replyto_name($CI->input->post('ReplyToName'));
            $email->set_replyto_email($CI->input->post('ReplyToEmail'));
        } else {
            $email->set_same_from_replyto(true);
        }

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        // Store data in session - End }

        if ($CI->input->post('EditEvent') == 'true') {
            self::update_email();
        }

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * This function is called before rendering content screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_content() {
        $CI = & get_instance();

        if ($CI->input->post('FormSubmit') == false) {
            $CI->form_validation->set_rules("ImageEmbedding", '');
            $CI->form_validation->set_rules("Subject", '');
            $CI->form_validation->set_rules("HTMLContent", '');
            $CI->form_validation->set_rules("PlainContent", '');
            $CI->form_validation->set_rules("ContentType", '');
            $CI->form_validation->run();
        }

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);

        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);

        $user_information = $wizard->get_extra_parameter('user_information');

        list($array_subject_tags, $array_content_tags) = self::retrieve_personalization_tags();

        $array_attachments = self::retrieve_attachments();

        $message = new Session_message();

        $message->set_parameter('SubjectTags', $array_subject_tags);
        $message->set_parameter('ContentTags', $array_content_tags);
        $message->set_parameter('Attachments', $array_attachments);

        if ($wizard->get_extra_parameter('flow') == 'fromtemplate' || $email->get_mode() == 'Template') {
            $message->set_parameter('HTMLContent', $email->get_html_content());
        }

        // If creating a confirmation email in "fromscratch" mode, pre-fill the email contents from defined language texts (default opt-in confirmation message)
        if ($wizard->get_extra_parameter('flow') == 'fromscratch' && $wizard->get_extra_parameter('mode') == 'confirmation' && $email->get_plain_content() == '' && $email->get_html_content() == '') {
            $email->set_subject(ApplicationHeader::$ArrayLanguageStrings['Screen']['1852']);
            $email->set_plain_content(ApplicationHeader::$ArrayLanguageStrings['Screen']['1853']);
            $email->set_content_type('Plain');
            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        }

        //Eman
        if ($wizard->get_extra_parameter('flow') == 'dragdropemail' && ($wizard->get_extra_parameter('mode') == 'campaign' || $wizard->get_extra_parameter('mode') == 'autoresponder') && $email->get_plain_content() == '' && $email->get_html_content() == '') {
            $email->set_content_type('HTML');
            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        }
        //Eman
        if ($wizard->get_extra_parameter('flow') == 'fromscratch' && ( $wizard->get_extra_parameter('mode') == 'campaign' || $wizard->get_extra_parameter('mode') == 'autoresponder') && $email->get_plain_content() == '' && $email->get_html_content() == '') {
            $email->set_content_type('Plain');
            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        }

        $_SESSION[SESSION_NAME]['ContentTags'] = $array_content_tags;
                
        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);

        unset($message);
    }

    /**
     * This function is called after content form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_content() {


        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        $CI = & get_instance();

        $CI->form_validation->set_rules("ImageEmbedding", '');
        $CI->form_validation->set_rules("ContentType", '');
        $CI->form_validation->set_rules("Subject", ApplicationHeader::$ArrayLanguageStrings['Screen']['0106'], 'required');

        // Setup email content validation rules - START

        $make_content_validations = !$CI->input->post('ContinueToBuilder') || $CI->input->post('ContinueToBuilder') == 'false';
        if ($make_content_validations) {
            $arr_html_content_rules = array();
            $arr_plain_content_rules = array();
            if ($CI->input->post('ContentType') == 'HTML' || $CI->input->post('ContentType') == 'Both') {
                $arr_html_content_rules[] = 'required';
                if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                    $arr_html_content_rules[] = 'callback__reject_link_required';
                    $arr_html_content_rules[] = 'callback__confirm_link_required';
                } else {
                    if ($user_information['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled') {
                        $unsubscription_link_exists_in_html_content = false;
                        // If html email header and footer is set in user group settings, check them first to see if they have unsubscription link in them - Start {
                        if ($user_information['GroupInformation']['HTMLEmailHeader'] != '') {
                            $unsubscription_link_exists_in_html_content = $CI->_unsubscription_link_required($user_information['GroupInformation']['HTMLEmailHeader'], false);
                        }
                        if (!$unsubscription_link_exists_in_html_content && $user_information['GroupInformation']['HTMLEmailFooter'] != '') {
                            $unsubscription_link_exists_in_html_content = $CI->_unsubscription_link_required($user_information['GroupInformation']['HTMLEmailFooter'], false);
                        }
                        // If html email header and footer is set in user group settings... - End }
                        if (!$unsubscription_link_exists_in_html_content) {
                            $arr_html_content_rules[] = 'callback__unsubscription_link_required';
                        }
                    }
                }
            }
            if ($CI->input->post('ContentType') == 'Plain' || $CI->input->post('ContentType') == 'Both') {
                $arr_plain_content_rules[] = 'required';
                if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                    $arr_plain_content_rules[] = 'callback__reject_link_required';
                    $arr_plain_content_rules[] = 'callback__confirm_link_required';
                } else {
                    if ($user_information['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled') {
                        $unsubscription_link_exists_in_plain_content = false;
                        // If plain email header and footer is set in user group settings, check them first to see if they have unsubscription link in them - Start {
                        if ($user_information['GroupInformation']['PlainEmailHeader'] != '') {
                            $unsubscription_link_exists_in_plain_content = $CI->_unsubscription_link_required($user_information['GroupInformation']['PlainEmailHeader'], false);
                        }
                        if (!$unsubscription_link_exists_in_plain_content && $user_information['GroupInformation']['PlainEmailFooter'] != '') {
                            $unsubscription_link_exists_in_plain_content = $CI->_unsubscription_link_required($user_information['GroupInformation']['PlainEmailFooter'], false);
                        }
                        // If plain email header and footer is set in user group settings... - End }
                        if (!$unsubscription_link_exists_in_plain_content) {
                            $arr_plain_content_rules[] = 'callback__unsubscription_link_required';
                        }
                    }
                }
            }

            $CI->form_validation->set_rules("HTMLContent", ApplicationHeader::$ArrayLanguageStrings['Screen']['0175'], implode('|', $arr_html_content_rules));
            $CI->form_validation->set_rules("PlainContent", ApplicationHeader::$ArrayLanguageStrings['Screen']['0176'], implode('|', $arr_plain_content_rules));
            if ($CI->form_validation->run() == false) {
                return false;
            }

            // If this is split test campaign, be sure that there is HTML content - START
            if ($wizard->get_extra_parameter('mode') == 'campaign') {
                Core::LoadObject('campaigns');
                $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                            'Criteria' => array('Column' => 'CampaignID', 'Operator' => '=', 'ValueWOQuote' => $wizard->get_extra_parameter('entity_id')),
                            'Content' => FALSE,
                            'Reports' => array()
                ));
                $CampaignInformation = $CampaignInformation[0];
                if ($CampaignInformation['SplitTest'] !== FALSE && $CI->input->post('ContentType') == 'Plain') {
                    $_SESSION['PageMessageCache'][0] = 'Error';
                    $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1713'];
                    return false;
                }
            }
            // If this is split test campaign, be sure that there is HTML content - END
        }
        // Setup email content validation rules - END
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }

        // Html entity decode all link href values. This is needed because TinyMCE
        // converts all ampersands in href attributes to &amp;
        $htmlContent = $CI->input->post('HTMLContent');
        $replacements = array();
        $pregMatch = preg_match_all("/href=\"(.*)\"/uiUm", $htmlContent, $matches, PREG_SET_ORDER);
        if ($pregMatch > 0) {
            foreach ($matches as $eachMatch) {
                $replacements[$eachMatch[1]] = html_entity_decode($eachMatch[1]);
            }
            $htmlContent = str_replace(array_keys($replacements), array_values($replacements), $htmlContent);
        }

        $email->set_image_embedding_enabled(IMAGE_EMBEDDING_ENABLED ? $CI->input->post('ImageEmbedding') == 'Enabled' : false);
        $email->set_content_type($CI->input->post('ContentType'));
        $email->set_subject($CI->input->post('Subject'));
        $email->set_html_content($htmlContent);
        $email->set_plain_content($CI->input->post('PlainContent'));

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        // Store data in session - End }
        // Redirect to email builder - START
        if (!$make_content_validations) {
            $CI->load->helper('url');
            if (HTACCESS_ENABLED) {
                // Check if application is installed under a directory or root - Start {
                $isInstallUnderRoot = false;
                $parsedAppUrl = parse_url(APP_URL);
                $installationDir = '';
                if (!isset($parsedAppUrl['path'])) {
                    $isInstallUnderRoot = true;
                } else {
                    $path = trim($parsedAppUrl['path'], '/');
                    if ($path == '') {
                        $isInstallUnderRoot = true;
                    } else {
                        $installationDir = $path;
                    }
                }
                // Check if application is installed under a directory or root - End }

                $request_uri = $_SERVER['REQUEST_URI'];

                if (!$isInstallUnderRoot) {
                    $request_url = str_replace($installationDir . '/', '', APP_URL) . ltrim($request_uri, '/');
                } else {
                    $request_url = APP_URL . ltrim($request_uri, '/');
                }
            } else {
                $request_uri = $_SERVER['QUERY_STRING'];
                $request_url = InterfaceAppURL(true) . $_SERVER['QUERY_STRING'];
            }
            $session_key = md5($email->get_id());
            $_SESSION[SESSION_NAME][$session_key] = $request_url;
            $_SESSION[SESSION_NAME]['ContentType'] = $CI->input->post('ContentType');
            
//            $builder_type = $CI->input->post('BuilderType');
            if (null != $CI->input->post('BuilderType') && $CI->input->post('BuilderType') == 'dragdrop') {
                redirect(InterfaceAppURL(true) . '/user/emaildragdropbuilder/edit/' . $wizard->get_extra_parameter('mode') . '/' . $session_key . '/' . $wizard->get_extra_parameter('entity_id') . ($email->get_id() > 0 ? '/' . $email->get_id() : ''));
            } else {
                redirect(InterfaceAppURL(true) . '/user/emailcontentbuilder/edit/' . $wizard->get_extra_parameter('mode') . '/' . $session_key . '/' . $wizard->get_extra_parameter('entity_id') . ($email->get_id() > 0 ? '/' . $email->get_id() : ''));
            }
        }

        if ($CI->input->post('EditEvent') == 'true') {
            self::update_email();
        }
        $CI->form_validation->_field_data = array();
        return true;
    }

    /**
     * This function is called before rendering preview screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_preview() {
        $CI = & get_instance();

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');

        $message = new Session_message();

        $email = self::update_email();

        // Update entities - Start {
        if ($wizard->get_extra_parameter('mode') == 'confirmation') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'list.update',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'subscriberlistid' => $wizard->get_extra_parameter('entity_id'),
                            'optinconfirmationemailid' => $email->get_id()
                        )
            ));
        } else if ($wizard->get_extra_parameter('mode') == 'autoresponder') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.update',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $wizard->get_extra_parameter('entity_id'),
                            'emailid' => $email->get_id()
                        )
            ));
        } else if ($wizard->get_extra_parameter('mode') == 'campaign') {
            if ($CI->input->post('EditEvent') != 'true') {
                if ($_SESSION[SESSION_NAME]['SplitTestInformation'] == false) {
                    $array_return = API::call(array(
                                'format' => 'array',
                                'command' => 'campaign.update',
                                'protected' => true,
                                'username' => $user_information['Username'],
                                'password' => $user_information['Password'],
                                'parameters' => array(
                                    'campaignid' => $wizard->get_extra_parameter('entity_id'),
                                    'relemailid' => $email->get_id()
                                )
                    ));
                } else {
                    SplitTests::AddTestEmailForCampaign($_SESSION[SESSION_NAME]['SplitTestInformation']['TestID'], $_SESSION[SESSION_NAME]['SplitTestInformation']['RelCampaignID'], $email->get_id(), $_SESSION[SESSION_NAME]['SplitTestInformation']['RelOwnerUserID']);
                }
            }
        }
        // Update entities - End }
        // Get spam report - Start {
        if (Users::HasPermissions(array('Email.SpamTest'), $user_information['GroupInformation']['Permissions']) == true) {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.spamtest',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'emailid' => $email->get_id(),
                        )
            ));
            $message->set_parameter('SpamReport', $array_return['TestResults']);
            unset($array_return);
        }
        // Get spam report - End }

        $message->set_parameter('EmailID', $email->get_id());

        $message->set_parameter('ShowPreviewMyEmailPromotion', ((PME_USAGE_TYPE == 'User') && (PME_APIKEY == '')) ? TRUE : FALSE);
        $message->set_parameter('IsPreviewMyEmailDisabled', ((PME_USAGE_TYPE == 'Admin') && (PME_APIKEY == '') && (PME_DEFAULT_ACCOUNT == '')) ? TRUE : FALSE);

        // Retrieve previous design tests - Start {
        $message->set_parameter('DesignTests', array());

        if ($message->get_parameter('ShowPreviewMyEmailPromotion') == FALSE) {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.designpreview.getlist',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'emailid' => $email->get_id()
                        )
            ));
            $message->set_parameter('DesignTests', $array_return['PreviewList'] == FALSE ? array() : $array_return['PreviewList'], true);
        }
        // Retrieve previous design tests - End }
        // Analyze email content - Start {
        $ImagesInEmail = Emails::DetectImages($email->get_html_content(), array(), false);
        $TMPArray = $ImagesInEmail;
        $ImagesInEmail = array();

        foreach ($TMPArray[1] as $Index => $EachImage) {
            $ImageInfo = pathinfo($EachImage);
            $ImagesInEmail[strtoupper($ImageInfo['extension'])][] = $EachImage;
        }
        $message->set_parameter('ImagesInEmail', $ImagesInEmail);

        $CSSInEmail = Emails::DetectCSS($email->get_html_content());
        $message->set_parameter('CSSInEmail', $CSSInEmail);

        $JavaScriptInEmail = Emails::DetectJavaScript($email->get_html_content());
        $message->set_parameter('JavaScriptInEmail', $JavaScriptInEmail);
        // Analyze email content - End }

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        unset($email);
    }

    public static function update_email() {

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        $user_information = $wizard->get_extra_parameter('user_information');

        // Create or update email on database - Start {
        // Create email - Start {
        if ($email->get_id() == 0) {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.create',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array()
            ));
            $email->set_id($array_return['EmailID']);
            unset($array_return);
        }
        // Create email - End }

        $validate_scope = '';
        $email_name = '';

        if ($wizard->get_extra_parameter('mode') == 'confirmation') {
            $email_name = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0986'], $wizard->get_extra_parameter('entity_id'));
            $validate_scope = 'OptIn';
        } elseif ($wizard->get_extra_parameter('mode') == 'autoresponder') {
            $email_name = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1023'], $wizard->get_extra_parameter('entity_id'));
            $validate_scope = 'AutoResponder';
        } elseif ($wizard->get_extra_parameter('mode') == 'campaign') {
            $email_name = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1256'], $wizard->get_extra_parameter('entity_id'));
            $validate_scope = 'Campaign';
        }

        // Update email - Start {
        $array_api_vars = array(
            'validatescope' => $validate_scope,
            'emailid' => $email->get_id(),
            'emailname' => $email->get_email_name() == '' ? $email_name : $email->get_email_name(),
            'fromname' => $email->get_from_name(),
            'fromemail' => $email->get_from_email(),
            'replytoname' => $email->get_replyto_name(),
            'replytoemail' => $email->get_replyto_email(),
            'mode' => '',
            'fetchurl' => '',
            'fetchplainurl' => '',
            'subject' => $email->get_subject(),
            'plaincontent' => $email->get_content_type() == 'Plain' || $email->get_content_type() == 'Both' ? $email->get_plain_content() : '',
            'htmlcontent' => $email->get_content_type() == 'HTML' || $email->get_content_type() == 'Both' ? $email->get_html_content() : '',
            'imageembedding' => $email->get_image_embedding_enabled() ? 'Enabled' : 'Disabled',
            'reltemplateid' => $email->get_template_id(),
        );

        if ($wizard->get_extra_parameter('flow') == 'fetchfromurl' || $email->get_mode() == 'Import') {
            $array_api_vars['mode'] = 'Import';
            $array_api_vars['fetchurl'] = $email->get_fetch_url();
            $array_api_vars['fetchplainurl'] = $email->get_fetch_plain_url();
        } else if ($wizard->get_extra_parameter('flow') == 'fromtemplate' || $email->get_mode() == 'Template') {
            $array_api_vars['mode'] = 'Template';
            $array_api_vars['reltemplateid'] = $email->get_template_id();
        }
//        else if ($wizard->get_extra_parameter('flow') == 'dragdropemail' || $email->get_mode() == 'dragdrop') {
//            $array_api_vars['mode'] = 'dragdrop';
//        } 
        else {
            $array_api_vars['mode'] = $email->get_mode() == '' ? 'Empty' : $email->get_mode();
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.update',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => $array_api_vars
        ));
//        print_r($array_api_vars);
//        print_r($array_return);
//        exit();
//        if (isset($_SESSION[SESSION_NAME]['screenshot_name']) && !empty($_SESSION[SESSION_NAME]['screenshot_name'])) {
        $screenshot = $email->getScreenshot();
        if (isset($screenshot) && !empty($screenshot)) {
            $ArrayCriterias = array('EmailID' => $email->get_id());
            $ArrayEmailInformation = array('ScreenshotImage' => $email->getScreenshot(), 'HTMLPure' => $email->getHtml_pure());
            Emails::Update($ArrayEmailInformation, $ArrayCriterias);
        }
//        exit();
        // Update email - End }
        // Retrieve all un assigned attachments and attach to this campaign's email - Start {
        $array_attachments = Attachments::RetrieveAttachments(array('*'), array('RelOwnerUserID' => $wizard->get_extra_parameter('user_id'), 'RelEmailID' => 0));
        foreach ($array_attachments as $each_attachment) {
            Attachments::Update(array('RelEmailID' => $email->get_id()), array('AttachmentID' => $each_attachment['AttachmentID']));
        }
        // Retrieve all un assigned attachments and attach to this campaign's email - End }
        // Create or update email on database - End }

        return $email;
    }

    /**
     * This function is called after preview form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_preview() {
        $CI = & get_instance();

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI->form_validation->set_rules("PreviewEmailAddress", '');
        $CI->form_validation->set_rules("PreviewType", '');

        // Preview event - Start {
        if ($CI->input->post('Command') == 'Preview') {
            // Field validations - Start {
            if ($CI->input->post('PreviewType') == 'email') {
                $CI->form_validation->set_rules("PreviewEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0809'], 'required');
            }
            // Field validations - End }
            // Run validation - Start {
            if ($CI->form_validation->run() == false) {
                return false;
            }
            // Run validation - End }

            if ($CI->input->post('PreviewType') == 'email') {
                if (SCRTY_EMAILPREVIEW_ENABLED) {
                    $floodDetector = new O_Security_Flood_Detector('email_preview');
                    $floodDetector->setMaxAccessForTimePeriod(SCRTY_EMAILPREVIEW_MAX_REQUESTS_ALLOWED, SCRTY_EMAILPREVIEW_MAX_REQUESTS_IN_SECONDS, SCRTY_EMAILPREVIEW_BLOCK_FOR_SECONDS);
                    $ipAddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
                    if ($floodDetector->isFloodedByIP($ipAddress)) {
                        $_SESSION['PageMessageCache'][0] = 'Error';
                        $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1915'];
                        return false;
                    }
                }


                if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                    $list_id = $wizard->get_extra_parameter('entity_id');
                } else if ($wizard->get_extra_parameter('mode') == 'autoresponder') {
                    $array_auto_responder = API::call(array(
                                'format' => 'array',
                                'command' => 'autoresponder.get',
                                'protected' => true,
                                'username' => $user_information['Username'],
                                'password' => $user_information['Password'],
                                'parameters' => array(
                                    'autoresponderid' => $wizard->get_extra_parameter('entity_id'),
                                )
                    ));
                    $array_auto_responder = $array_auto_responder['AutoResponder'];
                    $list_id = $array_auto_responder['RelListID'];
                } else if ($wizard->get_extra_parameter('mode') == 'campaign') {
                    $array_campaign = API::call(array(
                                'format' => 'array',
                                'command' => 'campaign.get',
                                'protected' => true,
                                'username' => $user_information['Username'],
                                'password' => $user_information['Password'],
                                'parameters' => array(
                                    'campaignid' => $wizard->get_extra_parameter('entity_id'),
                                )
                    ));
                    $array_campaign = $array_campaign['Campaign'];
                    $list_id = $array_campaign['RecipientLists'][0];
                }

                if (strpos($CI->input->post('PreviewEmailAddress'), ',') !== false) {
                    $emailAddresses = explode(',', $CI->input->post('PreviewEmailAddress'));
                } else {
                    $emailAddresses = array($CI->input->post('PreviewEmailAddress'));
                }

                foreach ($emailAddresses as $eachEmailAddress) {
                    $eachEmailAddress = trim($eachEmailAddress);
                    $array_api_parameters = array(
                        'format' => 'array',
                        'command' => 'email.emailpreview',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'listid' => $list_id,
                            'emailid' => $email->get_id(),
                            'emailaddress' => $eachEmailAddress,
                            'emailtype' => $wizard->get_extra_parameter('mode') == 'confirmation' ? 'optinconfirmation' : '',
                            'addusergroupheaderfooter' => $wizard->get_extra_parameter('mode') != 'confirmation' ? true : false
                        )
                    );

                    if ($wizard->get_extra_parameter('mode') == 'campaign') {
                        $array_api_parameters['parameters']['campaignid'] = $array_campaign['CampaignID'];
                    }

                    $array_return = API::call($array_api_parameters);
                }

                $_SESSION['PageMessageCache'][0] = 'Success';
                $_SESSION['PageMessageCache'][1] = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0810'], implode(', ', $emailAddresses));
            } elseif ($CI->input->post('PreviewType') == 'browser') {
                $CI->load->helper('url');
                redirect(InterfaceAppURL(true) . '/user/email/preview/' . $email->get_id(), '302');
            }

            return false;
        }
        // Preview event - End }
        // Create design test event - Start {
        else if ($CI->input->post('Command') == 'Test') {
            $array_return = API::call(array(
                        'format' => 'object',
                        'command' => 'email.designpreview.create',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'emailid' => $email->get_id()
                        )
            ));
            return false;
        }
        // Create design test event - End }

        $CI->load->helper('url');

        if ($wizard->get_extra_parameter('mode') == 'confirmation') {
            $entity_id = $wizard->get_extra_parameter('entity_id');
            WizardFactory::delete($_SESSION[SESSION_NAME]['WizardSessionName']);
            $_SESSION[SESSION_NAME]['WizardSessionName'] = '';
            unset($_SESSION[SESSION_NAME]['WizardSessionName']);

            if ($CI->input->post('EditEvent') == 'true') {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1313']);
                redirect(InterfaceAppURL(true) . '/user/list/settings/' . $entity_id, 'location', '302');
            } else {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0989']);
                redirect(InterfaceAppURL(true) . '/user/list/statistics/' . $entity_id, 'location', '302');
            }
        } else if ($wizard->get_extra_parameter('mode') == 'autoresponder') {
            $entity_id = $wizard->get_extra_parameter('entity_id');
            WizardFactory::delete($_SESSION[SESSION_NAME]['WizardSessionName']);
            $_SESSION[SESSION_NAME]['WizardSessionName'] = '';
            unset($_SESSION[SESSION_NAME]['WizardSessionName']);

            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.get',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $wizard->get_extra_parameter('entity_id'),
                        )
            ));

            if ($CI->input->post('EditEvent') == 'true') {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1313']);
                redirect(InterfaceAppURL(true) . '/user/autoresponders/edit/' . $array_return['AutoResponder']['RelListID'] . '/' . $entity_id, 'location', '302');
            } else {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1024']);
                redirect(InterfaceAppURL(true) . '/user/autoresponders/browse/' . $array_return['AutoResponder']['RelListID'], 'location', '302');
            }
        } else if ($wizard->get_extra_parameter('mode') == 'campaign') {
            $is_split = $wizard->get_extra_parameter('split_test_campaign') != null;
            if ($CI->input->post('EditEvent') == 'true') {
                if ($is_split) {
                    $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1361']);
                } else {
                    $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1362']);
                }
            } else {
                if ($is_split) {
                    $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1360']);
                } else {
                    $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0925']);
                }
            }

            $_SESSION[SESSION_NAME]['EmailInformation'] = array();
            unset($_SESSION[SESSION_NAME]['EmailInformation']);
            WizardFactory::delete($_SESSION[SESSION_NAME]['WizardSessionName']);
            $_SESSION[SESSION_NAME]['WizardSessionName'] = '';
            unset($_SESSION[SESSION_NAME]['WizardSessionName']);

            if ($_SESSION[SESSION_NAME]['SplitTestInformation'] == false) {
                $_SESSION[SESSION_NAME]['SplitTestInformation'] = false;
                unset($_SESSION[SESSION_NAME]['SplitTestInformation']);
                redirect(InterfaceAppURL(true) . '/user/campaign/edit/' . $wizard->get_extra_parameter('entity_id') . '#edit-tabs/tab-schedule');
            } else {
                $_SESSION[SESSION_NAME]['SplitTestInformation'] = false;
                unset($_SESSION[SESSION_NAME]['SplitTestInformation']);
                redirect(InterfaceAppURL(true) . '/user/campaign/edit/' . $wizard->get_extra_parameter('entity_id') . '#edit-tabs/tab-test');
            }
        }

        $_SESSION[SESSION_NAME]['EmailInformation'] = array();
        unset($_SESSION[SESSION_NAME]['EmailInformation']);
        WizardFactory::delete($_SESSION[SESSION_NAME]['WizardSessionName']);
        $_SESSION[SESSION_NAME]['WizardSessionName'] = '';
        unset($_SESSION[SESSION_NAME]['WizardSessionName']);

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * This function is called before rendering gallery screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_gallery() {
        Core::LoadObject('templates');

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');
        $group_information = $user_information['GroupInformation'];
        // Get templates - Start {
        $array_templates = Templates::RetrieveTemplates(array('*'), array(
                    array('field' => 'RelOwnerUserID', 'operator' => '=', 'value' => 0),
                    array('field' => 'RelOwnerUserID', 'operator' => '=', 'value' => $wizard->get_extra_parameter('user_id')),
                    array('field' => 'RelOwnerUserID', 'operator' => '=', 'value' => '-' . $group_information['UserGroupID'])
                        ), array('TemplateName' => 'ASC'), 'OR');
        // Get templates - End }

        $message = new Session_message();
        $message->set_parameter('Templates', $array_templates);

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    /**
     * This function is called after gallery form is submitted
     *Edited by Eman
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_gallery() {
        $CI = & get_instance();

        Core::LoadObject('templates');

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);

        // Retrieve template information - Start {
        if ($CI->input->post('EmailTemplateID') != '') {
            $array_templates = Templates::RetrieveTemplate(array('*'), array('TemplateID' => $CI->input->post('EmailTemplateID')));
        }
       
        // Retrieve template information - End }
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }

        if ($CI->input->post('EmailTemplateID') != '') {
            $email->set_content_type(($array_templates['TemplatePlainContent'] != '' ? ($array_templates['TemplateHTMLContent'] != '' ? 'Both' : 'Plain') : ($array_templates['TemplateHTMLContent'] != '' ? 'HTML' : 'HTML')));
            $email->set_subject($array_templates['TemplateSubject']);
            $email->set_html_content($array_templates['TemplateHTMLContent']);
//            $email->set_html_content($array_templates['HTMLPure']);
            $email->setHtml_pure($array_templates['HTMLPure']);
            $email->set_plain_content($array_templates['TemplatePlainContent']);
            $email->set_template_id($array_templates['TemplateID']);
        }

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        unset($email);
        // Store data in session - End }

        if ($CI->input->post('EditEvent') == 'true') {
            self::update_email();
        }

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * This function is called before from list gallery screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_copyfromlist() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);

        // Get lists - Start {
        $array_lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $wizard->get_extra_parameter('user_id'), 'OptInMode' => 'Double'), array('Name' => 'ASC'));
        // Get lists - End }

        $message = new Session_message();
        $message->set_parameter('Lists', $array_lists);

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    /**
     * This function is called after copy from list form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_copyfromlist() {
        $CI = & get_instance();

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');

        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => array(
                        'listid' => $CI->input->post('AnotherList')
                    )
        ));
        $list_information = $array_return['List'];
        unset($array_return);
        // Retrieve list information - End }
        // Retrieve email information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.get',
                    'protected' => true,
                    'username' => $user_information['Username'],
                    'password' => $user_information['Password'],
                    'parameters' => array(
                        'emailid' => $list_information['RelOptInConfirmationEmailID']
                    )
        ));
        $array_email_information = $array_return['EmailInformation'];
        unset($array_return);
        // Retrieve email information - End }
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }

        $email->set_from_name($array_email_information['FromName']);
        $email->set_from_email($array_email_information['FromEmail']);
        $email->set_replyto_name($array_email_information['ReplyToName']);
        $email->set_replyto_email($array_email_information['ReplyToEmail']);
        $email->set_same_from_replyto($email->get_from_name() == $email->get_replyto_name() ? true : false);
        $email->set_image_embedding_enabled(IMAGE_EMBEDDING_ENABLED ? $array_email_information['ImageEmbedding'] == 'Enabled' : false);
        $email->set_content_type($array_email_information['ContentType']);
        $email->set_subject($array_email_information['Subject']);
        $email->set_html_content($array_email_information['HTMLContent']);
        $email->set_plain_content($array_email_information['PlainContent']);

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        unset($email);
        // Store data in session - End }

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * This function is called before from list gallery screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_copycampaign() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);

        // Get campaigns - Start {
        $array_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Criteria' => array(
                        array(
                            'Column' => '%c%.RelOwnerUserID',
                            'Operator' => '=',
                            'Value' => $wizard->get_extra_parameter('user_id')
                        )
                    ),
                    'RowOrder' => array(
                        'Column' => 'CampaignName',
                        'Type' => 'ASC',
                    )
        ));
        // Get campaigns - End }

        $non_split_test_campaigns = array();

        foreach ($array_campaigns as $each) {
            if ($each['SplitTest'] === false)
                $non_split_test_campaigns[] = $each;
        }

        $message = new Session_message();
        $message->set_parameter('Campaigns', $non_split_test_campaigns);

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);
        unset($message);
    }

    /**
     * This function is called after copy from list form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_copycampaign() {
        $CI = & get_instance();

        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');

        // Retrieve campaign information - Start {
        $campaign_information = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Content' => true,
                    'Criteria' => array(
                        array(
                            'Column' => '%c%.RelOwnerUserID',
                            'Operator' => '=',
                            'Value' => $user_information['UserID']
                        ),
                        array(
                            'Link' => 'AND',
                            'Column' => '%c%.CampaignID',
                            'Operator' => '=',
                            'Value' => $CI->input->post('PreviousCampaign')
                        ),
        )));

        if ($campaign_information == false || count($campaign_information) < 1) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1914'];
            return false;
        }

        $campaign_information = $campaign_information[0];
        // Retrieve campaign information - End }
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }
        $email->set_from_name($campaign_information['Email']['FromName']);
        $email->set_from_email($campaign_information['Email']['FromEmail']);
        $email->set_replyto_name($campaign_information['Email']['ReplyToName']);
        $email->set_replyto_email($campaign_information['Email']['ReplyToEmail']);
        $email->set_same_from_replyto(($email->get_from_name() == $email->get_replyto_name()) && ($email->get_from_email() == $email->get_replyto_email()) ? true : false);
        $email->set_image_embedding_enabled(IMAGE_EMBEDDING_ENABLED ? $campaign_information['Email']['ImageEmbedding'] == 'Enabled' : false);
        $email->set_content_type($campaign_information['Email']['ContentType']);
        $email->set_subject($campaign_information['Email']['Subject']);
        $email->set_html_content($campaign_information['Email']['HTMLContent']);
        $email->set_plain_content($campaign_information['Email']['PlainContent']);
        $email->set_mode($campaign_information['Email']['Mode']);
        $email->set_fetch_url($campaign_information['Email']['FetchURL']);
        $email->set_fetch_plain_url($campaign_information['Email']['FetchPlainURL']);
        $email->set_template_id($campaign_information['Email']['RelTemplateID']);
        
        $email->setScreenshot($campaign_information['Email']['ScreenshotImage']);
        $email->setHtml_pure($campaign_information['Email']['HTMLPure']);

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        unset($email);
        // Store data in session - End }

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * This function is called before rendering fetch from url screen
     *
     * @return void
     * @author Mert Hurturk
     */
    public static function step_pre_fetchurl() {
        $CI = & get_instance();

        if ($CI->input->post('FormSubmit') == false) {
            $CI->form_validation->set_rules("ImageEmbedding", '');
            $CI->form_validation->set_rules("Subject", '');
            $CI->form_validation->set_rules("FetchURL", '');
            $CI->form_validation->set_rules("FetchPlainURL", '');
            $CI->form_validation->set_rules("ContentType", '');

            $CI->form_validation->run();
        }


        $email_information = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        $content_type = $email_information->get_content_type();
        list($array_subject_tags, $array_content_tags) = self::retrieve_personalization_tags();

        $array_attachments = self::retrieve_attachments();

        $message = new Session_message();

        $array_content_tags2 = array();
        $counter = 0;
        foreach ($array_content_tags as $key => $value) {
            if ($counter == 4) {
                $array_content_tags2[$key] = $value;
                break;
            }
            $counter++;
        }
        $array_content_tags = $array_content_tags2;
        unset($array_content_tags2);

        $message->set_parameter('SubjectTags', $array_subject_tags);
        $message->set_parameter('ContentTags', $array_content_tags);
        $message->set_parameter('Attachments', $array_attachments);
        $message->set_parameter('ContentType', $content_type);

        $_SESSION[SESSION_NAME]['StepMessage'] = serialize($message);

        unset($message);
    }

    /**
     * This function is called after fetch from url form is submitted
     *
     * @return boolean
     * @author Mert Hurturk
     */
    public static function step_post_fetchurl() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $user_information = $wizard->get_extra_parameter('user_information');

        $CI = & get_instance();

        // Field Validations - Start {
        $CI->form_validation->set_rules("ImageEmbedding", '');
        $CI->form_validation->set_rules("ContentType", '');
        $CI->form_validation->set_rules("Subject", ApplicationHeader::$ArrayLanguageStrings['Screen']['0106'], 'required');

        $arr_fetch_url_rules = array();
        $arr_fetch_plain_url_rules = array();

        if ($CI->input->post('ContentType') == 'HTML') {
            $arr_fetch_url_rules[] = 'prep_url';
            $arr_fetch_url_rules[] = 'required';

            if (($wizard->get_extra_parameter('mode') == 'autoresponder' || $wizard->get_extra_parameter('mode') == 'campaign') && $user_information['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled') {
                $arr_fetch_url_rules[] = 'callback__remote_content_unsubscription_link_required';
            }

            if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                $arr_fetch_url_rules[] = 'callback__remote_content_reject_link_required';
                $arr_fetch_url_rules[] = 'callback__remote_content_confirm_link_required';
            }
        } elseif ($CI->input->post('ContentType') == 'Plain') {
            $arr_fetch_plain_url_rules[] = 'prep_url';
            $arr_fetch_plain_url_rules[] = 'required';

            if ($wizard->get_extra_parameter('mode') == 'autoresponder' && $user_information['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled') {
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_unsubscription_link_required';
            }

            if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_reject_link_required';
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_confirm_link_required';
            }
        } elseif ($CI->input->post('ContentType') == 'Both') {
            $arr_fetch_url_rules[] = 'prep_url';
            $arr_fetch_url_rules[] = 'required';
            $arr_fetch_plain_url_rules[] = 'prep_url';
            $arr_fetch_plain_url_rules[] = 'required';

            if ($wizard->get_extra_parameter('mode') == 'autoresponder' && $user_information['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled') {
                $arr_fetch_url_rules[] = 'callback__remote_content_unsubscription_link_required';
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_unsubscription_link_required';
            }

            if ($wizard->get_extra_parameter('mode') == 'confirmation') {
                $arr_fetch_url_rules[] = 'callback__remote_content_reject_link_required';
                $arr_fetch_url_rules[] = 'callback__remote_content_confirm_link_required';
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_reject_link_required';
                $arr_fetch_plain_url_rules[] = 'callback__remote_content_confirm_link_required';
            }
        }

        if ($wizard->get_extra_parameter('mode') == 'campaign') {
            Core::LoadObject('campaigns');
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        'Criteria' => array('Column' => 'CampaignID', 'Operator' => '=', 'ValueWOQuote' => $wizard->get_extra_parameter('entity_id')),
                        'Content' => FALSE,
                        'Reports' => array()
            ));
            $CampaignInformation = $CampaignInformation[0];
            if ($CampaignInformation['SplitTest'] !== FALSE && $CI->input->post('ContentType') == 'Plain') {
                $_SESSION['PageMessageCache'][0] = 'Error';
                $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1713'];
                return false;
            }
        }

        $CI->form_validation->set_rules("FetchURL", ApplicationHeader::$ArrayLanguageStrings['Screen']['1250'], implode('|', $arr_fetch_url_rules));
        $CI->form_validation->set_rules("FetchPlainURL", ApplicationHeader::$ArrayLanguageStrings['Screen']['1251'], implode('|', $arr_fetch_plain_url_rules));
        // Field Validations - End }
        // Run validation - Start {
        if ($CI->form_validation->run() == false) {
            return false;
        }
        // Run validation - End }
        // Store data in session - Start {
        if (!isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
            $email = new Email_data();
        } else {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
        }

        $email->set_image_embedding_enabled(IMAGE_EMBEDDING_ENABLED ? $CI->input->post('ImageEmbedding') == 'Enabled' : false);
        $email->set_content_type($CI->input->post('ContentType'));
        $email->set_subject($CI->input->post('Subject'));
        $email->set_fetch_url($CI->input->post('FetchURL'));
        $email->set_fetch_plain_url($CI->input->post('FetchPlainURL'));

        $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        // Store data in session - End }

        if ($CI->input->post('EditEvent') == 'true') {
            self::update_email();
        }

        $CI->form_validation->_field_data = array();

        return true;
    }

    /**
     * Returns personalization tags
     *
     * @return array
     * @author Mert Hurturk
     */
    public static function retrieve_personalization_tags() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);

        $user_information = $wizard->get_extra_parameter('user_information');
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
        if ($wizard->get_extra_parameter('mode') == 'confirmation') {
            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('confirmation', $wizard->get_extra_parameter('user_id'), $wizard->get_extra_parameter('entity_id'), ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($wizard->get_extra_parameter('mode') == 'autoresponder') {
            // Get recipient list ids - Start {
            $array_auto_responder = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.get',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $wizard->get_extra_parameter('entity_id'),
                        )
            ));
            $array_auto_responder = $array_auto_responder['AutoResponder'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('autoresponder', $wizard->get_extra_parameter('user_id'), $array_auto_responder['RelListID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($wizard->get_extra_parameter('mode') == 'campaign') {
            // Get recipient list ids - Start {
            $array_campaign = API::call(array(
                        'format' => 'array',
                        'command' => 'campaign.get',
                        'protected' => true,
                        'username' => $user_information['Username'],
                        'password' => $user_information['Password'],
                        'parameters' => array(
                            'campaignid' => $wizard->get_extra_parameter('entity_id'),
                        )
            ));
            $array_campaign = $array_campaign['Campaign'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('campaign', $wizard->get_extra_parameter('user_id'), $array_campaign['RecipientLists'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        }
        // Retrieve personalization tags - End }

        return array($array_subject_tags, $array_content_tags);
    }

    /**
     * Returns attachments
     *
     * @return array
     * @author Mert Hurturk
     */
    public static function retrieve_attachments() {
        $wizard = WizardFactory::get($_SESSION[SESSION_NAME]['WizardSessionName']);
        $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);

        $array_attachments = array();

        // Retrieve attachments - Start {
        $array_attachments = Attachments::RetrieveAttachments(array('*'), array('RelOwnerUserID' => $wizard->get_extra_parameter('user_id'), 'RelEmailID' => '0'));
        if ($email->get_id() != 0) {
            $array_assigned_attachments = Attachments::RetrieveAttachments(array('*'), array('RelOwnerUserID' => $wizard->get_extra_parameter('user_id'), 'RelEmailID' => $email->get_id()));
            $array_attachments = array_merge(($array_attachments === false ? array() : $array_attachments), $array_assigned_attachments);
        }
        // Retrieve attachments - End }

        return $array_attachments;
    }

}
