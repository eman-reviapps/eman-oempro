<?php
/**
 * Tags controller
 *
 * @author Mert Hurturk
 */

class Controller_Tags extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();
	
		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('tags');
		Core::LoadObject('api');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
		// Retrieve user information - End }

		// Check if user account has expired - Start {
		if (Users::IsAccountExpired($this->array_user_information) == true)
			{
			$_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/user/logout/', 'location', '302');
			}
		// Check if user account has expired - End }
	}

	/**
	 * Index controller
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function index()
	{
		// Events - Start {
		if ($this->input->post('Command') == 'DeleteTags')
		{
			$array_event_return = $this->_event_delete_tags($this->input->post('SelectedTags'));
		}
		else if ($this->input->post('Command') == 'CreateTag')
		{
			$array_event_return = $this->_event_create_tag();
		}
		else if ($this->input->post('Command') == 'UpdateTag')
		{
			$array_event_return = $this->_event_update_tag();
		}
		// Events - End }

		// Retrieve clients - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'tags.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				)
			));

		if ($array_return['Success'] == false)
		{
			$array_tags = array();
		}
		else
		{
			$array_tags = $array_return['Tags'] === false ? array() : $array_return['Tags'];
		}
		// Retrieve clients - End }

		// Interface parsing - Start {
		$array_view_data 	= array(
								'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseTags'],
								'CurrentMenuItem'		=> 'Settings',
								'SubSection'			=> 'Tags',
								'UserInformation'		=> $this->array_user_information,
								'Tags'					=> $array_tags,
								);
		$array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
		foreach ($array_event_return as $key=>$value)
		{
			$array_view_data[$key] = $value;
		}

		if (isset($array_event_return) == true)
		{
			$array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
		}

		// Check if there is any message in the message buffer - Start {
		if ($_SESSION['PageMessageCache'][1] != '')
		{
			$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('user/settings', $array_view_data);
		// Interface parsing - End }
	}

	/**
	 * Delete tags event
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function _event_delete_tags($array_tag_ids)
	{
		// Field validations - Start {
		$array_form_rules = array(
								array
									(
									'field'		=> 'SelectedTags[]',
									'label'		=> 'tags',
									'rules'		=> 'required'
									)
								);

		$this->form_validation->set_rules($array_form_rules);
		$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1089']);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, validation_errors());
		}
		// Run validation - End }

		// Delete clients - Start {
		$array_api_vars = array(
							'tags' => implode(',', $array_tag_ids),
							);
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'tags.delete',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$array_api_vars
			));

		if ($array_return['Success'] == false)
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_Return['ErrorCode'][0] : $array_return['ErrorCode']);
			switch ($error_code)
			{
				case '1':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1089']);
					break;
				default:
					break;
			}
		}
		else
		{
			// API Success
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1090']);
			return true;
		}
		// Delete email templates - End }
	}
	
	/**
	 * Create tag event
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function _event_create_tag()
	{
		// Field validations - Start {
		$array_form_rules = array(
								array
									(
									'field'		=> 'Tag',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1091'],
									'rules'		=> 'required'
									)
								);

		$this->form_validation->set_rules($array_form_rules);
		$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1092']);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, validation_errors());
		}
		// Run validation - End }

		// Delete clients - Start {
		$array_api_vars = array(
							'tag' => $this->input->post('Tag'),
							);
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'tag.create',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$array_api_vars
			));

		if ($array_return['Success'] == false)
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_Return['ErrorCode'][0] : $array_return['ErrorCode']);
			switch ($error_code)
			{
				case '1':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1092']);
					break;
				case '2':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1089']);
					break;
				default:
					break;
			}
		}
		else
		{
			// API Success
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1094']);
			return true;
		}
		// Delete email templates - End }
	}

	/**
	 * Update tag event
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function _event_update_tag()
	{
		// Field validations - Start {
		$array_form_rules = array(
								array
									(
									'field'		=> 'Tag',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1091'],
									'rules'		=> 'required'
									)
								);

		$this->form_validation->set_rules($array_form_rules);
		$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1092']);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, validation_errors());
		}
		// Run validation - End }

		// Delete clients - Start {
		$array_api_vars = array(
							'tag' => $this->input->post('Tag'),
							'tagid' => $this->input->post('TagID')
							);
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'tag.update',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$array_api_vars
			));

		if ($array_return['Success'] == false)
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_Return['ErrorCode'][0] : $array_return['ErrorCode']);
			switch ($error_code)
			{
				case '1':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1092']);
					break;
				case '2':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1089']);
					break;
				default:
					break;
			}
		}
		else
		{
			// API Success
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1095']);
			return true;
		}
		// Delete email templates - End }
	}
	
}
?>