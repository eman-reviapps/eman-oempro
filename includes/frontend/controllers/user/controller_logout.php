<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Logout extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('user_auth');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Check the login session, redirect based on the login session status - Start
	// UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
	// Check the login session, redirect based on the login session status - End

	// Delete the user login reminder cookie - Start
	// setcookie(COOKIE_USER_LOGINREMIND, '', time() - 60000);
	// setcookie(COOKIE_USER_LOGINREMIND, '', time() - 60000, '/');
	$url = parse_url($_SERVER['REQUEST_URI']);
	$path = $url['path'];
	setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), str_replace('logout/', '', $path));
	setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), rtrim(str_replace('logout/', '', $path), '/')); // This is for Google Chrome Windows
	setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), '/');
	setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14));
	// Delete the user login reminder cookie - End

	// Logout the user and redirect to login page - Start
	UserAuth::Logout();
	
	if (AdminAuth::IsLoggedIn(false, false))
		{
		header('Location: '.InterfaceAppURL(true).'/admin/users/browse/');
		}
	else
		{
		header('Location: '.InterfaceAppURL(true).'/user/');
		}
	// Logout the user and redirect to login page - End	
	}
} // end of class User
?>