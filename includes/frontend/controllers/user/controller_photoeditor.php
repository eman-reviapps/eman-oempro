<?php

/**
 * Email content builder controller
 *
 * @author Mert Hurturk
 */
class Controller_PhotoEditor extends MY_Controller {

    public $email_html;

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        // Load other modules - End	

//        $this->load->helper('url');
//
//        // Check the login session, redirect based on the login session status - Start
//        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
//        // Check the login session, redirect based on the login session status - End
//        // Retrieve user information - Start {
//        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
//        // Retrieve user information - End }
//        // Check if user account has expired - Start {
//        if (Users::IsAccountExpired($this->array_user_information) == true) {
//            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
//            $this->load->helper('url');
//            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
//        }
        // Check if user account has expired - End }
    }

    public function index() {
        $this->render('user/photo_editor.php');
        // Interface parsing - End }
    }

}

// end of class User
?>