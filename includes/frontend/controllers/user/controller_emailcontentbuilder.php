<?php

/**
 * Email content builder controller
 *
 * @author Mert Hurturk
 */
class Controller_EmailContentBuilder extends MY_Controller {

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('campaigns');
        Core::LoadObject('api');
        Core::LoadObject('email_data');
        Core::LoadObject('personalization');
        // Load other modules - End	

        $this->load->helper('url');

        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, APP_URL . APP_DIRNAME . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Displays iguana content editor.
     *
     * @return void
     * @author Mert Hurturk
     */
    function edit($mode, $session_key, $entity_id, $email_id = 0) {
        // Retrieve personalization tags - Start {
        if ($mode == 'campaign') {
            // Get recipient list ids - Start {
            $array_campaign = API::call(array(
                        'format' => 'array',
                        'command' => 'campaign.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'campaignid' => $entity_id,
                            'retrievestatistics' => false
                        )
            ));
            $array_campaign = $array_campaign['Campaign'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('campaign', $this->array_user_information['UserID'], $array_campaign['RecipientLists'][0], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($mode == 'confirmation') {
            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('confirmation', $this->array_user_information['UserID'], $entity_id, ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        } else if ($mode == 'autoresponder') {
            // Get recipient list ids - Start {
            $array_auto_responder = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponder.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'autoresponderid' => $entity_id,
                        )
            ));
            $array_auto_responder = $array_auto_responder['AutoResponder'];
            // Get recipient list ids - End }

            list($array_subject_tags, $array_content_tags) = Personalization::GetTagsFor('autoresponder', $this->array_user_information['UserID'], $array_auto_responder['RelListID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0667'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0668'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1252']);
        }
        // Retrieve personalization tags - End }

        $ScreenMessage = isset($_SESSION[SESSION_NAME]['ContentBuilderMessage']) ? $_SESSION[SESSION_NAME]['ContentBuilderMessage'] : '';
        unset($_SESSION[SESSION_NAME]['ContentBuilderMessage']);

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailContentBuilder'],
            'Mode' => $mode,
            'EmailID' => $email_id,
            'ReturnURL' => $_SESSION[SESSION_NAME][$session_key],
            'SubjectTags' => $array_subject_tags,
            'ContentTags' => $array_content_tags,
            'SaveURL' => '/user/emailcontentbuilder/edit/' . $mode . '/' . $session_key . '/' . $entity_id . '/' . $email_id,
            'Message' => $ScreenMessage
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/email_content_builder2', $array_view_data);
    }

    /**
     * Saves email content to the session (or directly saves in database) and redirects to the submitted url
     *
     * @return void
     * @author Mert Hurturk
     */
    function saveemail() {

        if ($this->input->post('EmailID') == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $email->set_html_content($this->input->post('EmailHTMLContent'));

            $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
        } else {
            if ($this->input->post('Mode') == 'confirmation') {
                $validation_scope = 'OptIn';
            } elseif ($this->input->post('Mode') == 'autoresponder') {
                $validation_scope = 'AutoResponder';
            } elseif ($this->input->post('Mode') == 'campaign') {
                $validation_scope = 'Campaign';
            }

            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $this->input->post('EmailID'),
                            'htmlcontent' => $this->input->post('EmailHTMLContent'),
                            'validatescope' => $validation_scope
                        )
            ));

            if (isset($_SESSION[SESSION_NAME]['EmailInformation'])) {
                $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
                if ($email instanceof Email_data) {
                    $email->set_html_content($this->input->post('EmailHTMLContent'));
                    $_SESSION[SESSION_NAME]['EmailInformation'] = serialize($email);
                }
            }
        }

        $_SESSION[SESSION_NAME]['ContentBuilderMessage'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1857'];
        redirect($this->input->post('SaveURL'));
    }

    /**
     * Displays email content in iguana content editor.
     *
     * @return void
     * @author Mert Hurturk
     */
    function content($email_id = 0) {
        if ($email_id == 0) {
            $email = unserialize($_SESSION[SESSION_NAME]['EmailInformation']);
            $email_content = $email->get_html_content();
        } else {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $email_id
                        )
            ));
            $email_content = $array_return['EmailInformation']['HTMLContent'];
        }

        print $email_content;
        exit;
    }

    /**
     * Test drives email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function index($CampaignID = '') {
        // Check if template builder session exists - Start {
        if ((!isset($_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent']) || $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] == '') && $CampaignID == '') {
            exit;
        }
        // Check if template builder session exists - End }
        // Get recipient list ids - Start {
        $ArrayRecipientListIDs = array();
        foreach ($_SESSION[SESSION_NAME]['CampaignInformation']['Recipients'] as $Each) {
            $TempArray = explode(':', $Each);
            $ArrayRecipientListIDs[] = $TempArray[0];
        }
        $ArrayRecipientListIDs = array_unique($ArrayRecipientListIDs);
        // Get recipient list ids - End }
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($this->ArrayUserInformation['UserID'], $ArrayRecipientListIDs, ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_campaign_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'Campaign');
        $array_list_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'List');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
        $array_content_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0779'] => $array_campaign_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0780'] => $array_list_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        $array_subject_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        // Retrieve personalization tags - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailContentBuilder'],
            'Flow' => $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'],
            'CampaignID' => $CampaignID,
            'SubjectTags' => $array_subject_tags,
            'ContentTags' => $array_content_tags
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('user/email_content_builder', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Save controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function save($CampaignID = '') {
        $this->load->helper('url');
        if ($CampaignID == '') {
            $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] = $this->input->post('EmailHTMLContent');
            redirect(InterfaceAPPUrl(true) . '/user/campaigns/create/' . $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'] . '/' . ($_SESSION[SESSION_NAME]['CampaignInformation']['Step'] + 1));
        } else {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Content" => true
            ));
            $CampaignInformation = $CampaignInformation[0];
            $ArrayAPIVars = array(
                'emailid' => $CampaignInformation['RelEmailID'],
                'fromname' => $CampaignInformation['Email']['FromName'],
                'fromemail' => $CampaignInformation['Email']['FromEmail'],
                'replytoname' => $CampaignInformation['Email']['ReplyToName'],
                'replytoemail' => $CampaignInformation['Email']['ReplyToEmail'],
                'subject' => $CampaignInformation['Email']['Subject'],
                'plaincontent' => $CampaignInformation['Email']['PlainContent'],
                'htmlcontent' => $this->input->post('EmailHTMLContent'),
                'validatescope' => 'Campaign'
            );
            $ArrayReturn = API::call(array(
                        'format' => 'array',
                        'command' => 'email.update',
                        'protected' => true,
                        'username' => $this->ArrayUserInformation['Username'],
                        'password' => $this->ArrayUserInformation['Password'],
                        'parameters' => $ArrayAPIVars
            ));
            redirect(InterfaceAPPUrl(true) . '/user/campaign/edit/' . $CampaignID);
        }
    }

    /**
     * Displays HTMLContent in the session
     *
     * @return void
     * @author Mert Hurturk
     * */
    function displayHTMLContent($CampaignID = '') {
        // Check if template builder session exists - Start {
        if ((!isset($_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent']) || $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'] == '') && $CampaignID == '') {
            exit;
        }
        // Check if template builder session exists - End }
        // If campaign id is given, retrieve campaign informatino - Start {
        if ($CampaignID != '') {
            $CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
                        "Criteria" => array(
                            array(
                                "Column" => "%c%.CampaignID",
                                "Operator" => "=",
                                "Value" => $CampaignID
                            )
                        ),
                        "Content" => true
            ));
            $CampaignInformation = $CampaignInformation[0];
            $Content = $CampaignInformation['Email']['HTMLContent'];
        }
        // If campaign id is given, retrieve campaign informatino - End }
        else {
            $Content = $_SESSION[SESSION_NAME]['CampaignInformation']['HTMLContent'];
        }


        print $Content;
        exit;
    }

    /**
     * Test drives email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventTestDriveEmailTemplate() {
        $_SESSION['template_builder']['TemplateHTMLContent'] = $_POST['TemplateHTMLContent'];
        $this->load->helper('url');
        redirect(APP_URL . APP_DIRNAME . '/admin/emailtemplates/testdrive');
    }

}

// end of class User
?>