<?php

class Controller_Medialibrary extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('media_library');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    public function index() {
        $this->browse();
    }

    public function browse_simple($message = '') {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Media.Browse'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }

        Core::LoadObject('aws_s3');
        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
        $files = $ObjectS3->getBucket(S3_BUCKET, S3_MEDIALIBRARY_PATH . "/" . $this->array_user_information['UserID'] . "/", null, null, "/");

        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserMediaLibrary'],
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => InterfaceLanguage('Screen', '0552', true, '', false, true),
            'UserInformation' => $this->array_user_information,
            'Files' => $files,
            'Message' => $message,
            'RemoveTinyMCE' => false
        );
        $this->render('user/media_library_simple', $array_view_data);
        // Interface parsing - End }
    }

    public function browse($folderId = 0, $mode = 'page', $message = '', $removeTinyMCE = '0') {
        // User privilege check - Start {
        if (Users::HasPermissions(array('Media.Browse'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }

        if (!in_array($mode, array('page', 'popup')))
            $mode = 'page';

        $mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');
        $currentFolder = $mediaLibraryFolderMapper->findById((int) $folderId);

        if ($currentFolder !== false && $currentFolder->getUserId() != $this->array_user_information['UserID']) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        $folders = $mediaLibraryFolderMapper->findByFolderId((int) $folderId, (int) $this->array_user_information['UserID']);

        $mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
        $files = $mediaLibraryFileMapper->findByFolderId((int) $folderId, (int) $this->array_user_information['UserID']);

        $breadcrumb = $currentFolder == false ? $this->_getFolderBreadcrumb(0) : $this->_getFolderBreadcrumb($folderId);

        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserMediaLibrary'],
            'CurrentMenuItem' => 'Drop',
            'CurrentDropMenuItem' => InterfaceLanguage('Screen', '0552', true, '', false, true),
            'UserInformation' => $this->array_user_information,
            'Folders' => $folders,
            'Files' => $files,
            'CurrentFolder' => $currentFolder,
            'Breadcrumb' => $breadcrumb,
            'Mode' => $mode,
            'Message' => $message,
            'RemoveTinyMCE' => $removeTinyMCE == '0' ? false : true
        );
        $this->render('user/media_library_' . $mode, $array_view_data);
        // Interface parsing - End }
    }

    protected function _getFolderBreadcrumb($folderId) {
        $mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');
        $folder = $mediaLibraryFolderMapper->findById($folderId);
        if (!$folder)
            return array(array(0, InterfaceLanguage('Screen', '0552', true, '', false, true)));

        return array_merge(array(array($folder->getId(), $folder->getName())), $this->_getFolderBreadcrumb($folder->getFolderId()));
    }

    public function upload() {
        if (DEMO_MODE_ENABLED == true) {
            print "<script type=\"text/javascript\" charset=\"utf-8\">parent.media_library.upload_error('" . ApplicationHeader::$ArrayLanguageStrings['Screen']['1847'] . "');</script>";
            exit;
        }

        // User privilege check - Start {
        if (Users::HasPermissions(array('Media.Browse'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }

        if (count($_FILES) > 0) {
            // Media file upload - Start
            $MemoryLimit = ini_get('memory_limit');
            $MemoryLimit_type = strtolower(substr($MemoryLimit, strlen($MemoryLimit) - 1));
            $MemoryLimit = substr($MemoryLimit, 0, strlen($MemoryLimit) - 1);
            $MemoryLimit = ($MemoryLimit_type == "m" ? $MemoryLimit * (1024 * 1024) : ($MemoryLimit_type == "k" ? $MemoryLimit * 1024 : $MemoryLimit ));

            $PostMaxSize = ini_get('post_max_size');
            $PostMaxSize_type = strtolower(substr($PostMaxSize, strlen($PostMaxSize) - 1));
            $PostMaxSize = substr($PostMaxSize, 0, strlen($PostMaxSize) - 1);
            $PostMaxSize = ($PostMaxSize_type == "m" ? $PostMaxSize * (1024 * 1024) : ($PostMaxSize_type == "k" ? $PostMaxSize * 1024 : $PostMaxSize ));

            $MaximumSize = ($PostMaxSize > $MemoryLimit ? $PostMaxSize : $MemoryLimit);

            if ($_FILES['new-media-file']['size'] > $MaximumSize) {
                $NewMediaID = false;
            } else {
//                if (filesize($_FILES['new-media-file']['tmp_name']) > MEDIA_MAX_FILESIZE) {
//                    $NewMediaID = false;
//                } else {
                $MediaData = file_get_contents($_FILES['new-media-file']['tmp_name']);

                $file = new O_Domain_MediaLibraryFile();
                $file->setUserId($this->array_user_information['UserID']);
                $file->setFolderId($this->input->post('ParentFolderID'));
                $file->setType($_FILES['new-media-file']['type']);
                $file->setSize($_FILES['new-media-file']['size']);
                $file->setName($_FILES['new-media-file']['name']);

                if (MEDIA_UPLOAD_METHOD == 'database') {
                    $file->setData(base64_encode($MediaData));
                } else {
                    $file->setData(MEDIA_UPLOAD_METHOD);
                }

                $mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
                $mediaLibraryFileMapper->insert($file);

                $NewMediaID = $file->getId();

                // Upload to file or Amazon S3 service - Start
                if (MEDIA_UPLOAD_METHOD == 'file') {
                    $FileHandler = fopen(DATA_PATH . 'media/' . md5($NewMediaID), 'w');
                    fwrite($FileHandler, $MediaData);
                    fclose($FileHandler);
                } elseif (MEDIA_UPLOAD_METHOD == 's3') {
                    Core::LoadObject('aws_s3');

                    $uri = S3_MEDIALIBRARY_PATH . '/' . ($file->getUserId()) . '/' . $NewMediaID . "_" . uniqid();
                    
                    $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
                    $ObjectS3->putObjectString($MediaData, S3_BUCKET, $uri, S3::ACL_PUBLIC_READ, array(), $file->getType());
                }
                // Upload to file or Amazon S3 service - End
//                }
            }
            if ($NewMediaID == false) {
                $Script = "<script type=\"text/javascript\" charset=\"utf-8\">parent.media_library.upload_error('" . ApplicationHeader::$ArrayLanguageStrings['Screen']['1848'] . "');</script>";
            } else {
                $Script = "<script type=\"text/javascript\" charset=\"utf-8\">parent.media_library.upload_success();</script>";
            }

            print $Script;
            exit;
            // Media file upload - End
        } else {
            print "<script type=\"text/javascript\" charset=\"utf-8\">parent.media_library.upload_error('" . ApplicationHeader::$ArrayLanguageStrings['Screen']['1848'] . "');</script>";
            exit;
        }
    }

}

?>