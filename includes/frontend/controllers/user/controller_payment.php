<?php

/**
 * List controller
 *
 * @author Mert Hurturk
 */
class Controller_Payment extends MY_Controller {

    /**
     * Constructor
     *
     * @author Eman Mohammed
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('currencies');
        Core::LoadObject('api');
        Core::LoadObject('users');
        Core::LoadObject('payments');
        Core::LoadObject('gateway');
        Core::LoadObject('payment_gateways/gateway_iyzico.inc.php');

        $this->load->helper('url');

        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    function checkout() {

        $this->redirectIfNotValid($_SESSION[SESSION_NAME]['CheckoutInformation']);
        $CheckoutInformation = unserialize($_SESSION[SESSION_NAME]['CheckoutInformation']);

        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $CheckoutInformation->UserID), true);
        $this->redirectIfNotValid($ArrayUser, $CheckoutInformation->ErrorURL);

        if (isset($CheckoutInformation->PaymentLogID) && $CheckoutInformation->PaymentLogID != 0) {
            Payments::SetUser($ArrayUser);
            #check valid payment log id
            $ArrayPaymentPeriod = Payments::GetPaymentPeriod($CheckoutInformation->PaymentLogID);
            $this->redirectIfNotValid($ArrayPaymentPeriod, $CheckoutInformation->ErrorURL);
        }
        //empty session
        $this->cleanSession();
        $ArrayViewData = $this->prepareCheckout($CheckoutInformation, $ArrayUser);

        $this->render('user/checkout', $ArrayViewData);
    }

    function payment_result($gateway, $type = '') {

        if (($gateway == 'PayPal_Express') && ($type == 'Ping')) {
            // PayPal Express IPN/PDT	
            Core::LoadObject('payment_gateways/gateway_paypal_express.inc.php');
            PaymentGateway::SetInterface('PayPal_Express');
            PaymentGateway::$Interface->ProcessReturnedData($_POST);
        } else if ($gateway == 'Iyzipay') {

            if (!$_POST) {
                $_SESSION[SESSION_NAME]['PAYMENTFAILED'] = InterfaceLanguage('CheckOut', '0006', false, '', false, false);
                header('Location: ' . Core::InterfaceAppURL() . '/user/');
            }
            // PayPal Express IPN/PDT	
            Core::LoadObject('payment_gateways/gateway_iyzico.inc.php');
            PaymentGateway::SetInterface('Iyzipay');
            $result = PaymentGateway::$Interface->ProcessReturnedData($_POST);

            if ($result[0]) {
                $process = $result[1];
                $_SESSION[SESSION_NAME]['PAYMENTSUCCESS'] = InterfaceLanguage('CheckOut', $result[2], true, '', false, false);

                if ($process == 'Upgrade') {
                    header('Location: ' . Core::InterfaceAppURL() . '/process/upgrade');
                } elseif ($process == 'PayInvoice') {
                    header('Location: ' . Core::InterfaceAppURL() . '/process/invoices');
                } else {
                    header('Location: ' . Core::InterfaceAppURL() . '/user/');
                }
            } else {
                if ($result[1] == 'general') {
                    $_SESSION[SESSION_NAME]['PAYMENTFAILED'] = $result[2];
                } else {
                    $_SESSION[SESSION_NAME]['PAYMENTFAILED'] = InterfaceLanguage('CheckOut', $result[2], true, '', false, false);
                }
                header('Location: ' . Core::InterfaceAppURL() . '/user/');
            }
        }
    }

    private function prepareCheckout($CheckoutInformation, $ArrayUser) {

        PaymentGateway::SetInterface('Iyzipay');
        $ArrayUser = $this->addGeoDataToUser($ArrayUser);
        $ExchangeInfo = $this->getExchangeInfo($ArrayUser['CountryCode']);
        $CurrencyExchangeRate = $ExchangeInfo[0];
        $Currency = $ExchangeInfo[1];

        if (isset($CheckoutInformation->ArrayNewUserGroup) && !empty($CheckoutInformation->ArrayNewUserGroup)) {
            $ArrayUserGroup = $CheckoutInformation->ArrayNewUserGroup;
        } else {
            $ArrayUserGroup = $ArrayUser['GroupInformation'];
        }

        $items = $this->createPaymentItems($ArrayUserGroup, $CheckoutInformation->ServiceFee, $CurrencyExchangeRate);

        $callBackURL = Core::InterfaceAppURL() . '/user/' . PaymentGateway::$Interface->getCallBackURL();
        $ArrayPaymentParameters = $this->createPaymentParameters($CheckoutInformation, $callBackURL, $ExchangeInfo);

        $response = PaymentGateway::$Interface->initializeCheckoutForm($ArrayPaymentParameters, $ArrayUser, $items);

        $status = $response->getStatus();
        $errorCode = $response->getErrorCode();
        $errorMessage = $response->getErrorMessage();
        $checkoutFormContent = $response->getCheckoutFormContent();

        $PaidPrice = ($CheckoutInformation->ServiceFee - $CheckoutInformation->Discount);

        $ArrayViewData = array(
            ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['Checkout'],
            'CurrentMenuItem' => 'Checkout',
            'UserInformation' => $ArrayUser,
            'ErrorCode' => $errorCode,
            'PageErrorMessage' => $errorMessage,
            'CheckoutFormContent' => $checkoutFormContent,
            'PaidPrice' => $PaidPrice,
            'Status' => $status,
            'ExchangeInfo' => $ExchangeInfo,
            'ExchangePrice' => $this->getPriceExchanged($PaidPrice, $CurrencyExchangeRate),
        );
        return $ArrayViewData;
    }

    private function cleanSession() {
        $_SESSION[SESSION_NAME]['CheckoutInformation'] = array();
        unset($_SESSION[SESSION_NAME]['CheckoutInformation']);
    }

    private function redirectIfNotValid($Array, $ErrorURL = '') {

        if ($Array == false) {
            if (!isset($ErrorURL) || empty($ErrorURL)) {
                $location = InterfaceAppURL(true) . '/user/';
            } else {
                $location = $ErrorURL;
            }
            $_SESSION[SESSION_NAME]['PAYMENTFAILED'] = 'Invalid data provided';
            header('Location: ' . $location);
            exit;
        }
    }

    private function addGeoDataToUser($ArrayUser) {

        $GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
        $GeoTagInfo = geoip_record_by_addr($GeoTag, $_SERVER['REMOTE_ADDR']);
        $City = isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : '';
        $Country = isset($GeoTagInfo->country_name) == true ? $GeoTagInfo->country_name : '';
        $CountryCode = isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : '';

        $ArrayUser['IP'] = $_SERVER['REMOTE_ADDR'];
//        $ArrayUser['uCity'] = 'Giza';
//        $ArrayUser['uCountry'] = 'Egypt';
//        $ArrayUser['CountryCode'] = 'EG';
//        $ArrayUser['CountryCode'] = 'TR';

        $ArrayUser['uCity'] = $City;
        $ArrayUser['uCountry'] = $Country;
        $ArrayUser['CountryCode'] = $CountryCode;
        return $ArrayUser;
    }

    private function getExchangeInfo($CountryCode) {
        $Rate = 1;
        $Currency = 'USD';
        $MainCurrency = 'USD';

        if ($CountryCode == 'TR') {
            $Result = Currency::getExchangeInfo($CountryCode);
            if ($Result[0]) {
                $Rate = $Result[1]['ExchangeRate'];
                $Currency = $Result[1]['Currency'];
            }
        }
        return array($Rate, $Currency, $MainCurrency);
    }

    private function getPriceExchanged($Price, $CurrencyExchangeRate) {

        return round(($Price * $CurrencyExchangeRate), 2, PHP_ROUND_HALF_UP);
    }

    private function createPaymentItems($ArrayUserGroup, $ServiceFee, $CurrencyExchangeRate) {

        $Price = $this->getPriceExchanged($ServiceFee, $CurrencyExchangeRate);

        $items = array();
        $items[] = array(
            "ID" => $ArrayUserGroup['UserGroupID'],
            "Name" => $ArrayUserGroup['GroupName'],
            "Category" => $ArrayUserGroup['SubscriptionType'],
            "Price" => $Price,
        );
        return $items;
    }

    private function createPaymentParameters($CheckoutInformation, $callBackURL, $ExchangeInfo) {

        $CurrencyExchangeRate = $ExchangeInfo[0];
        $Currency = $ExchangeInfo[1];

        $IsCreditPurchase = isset($CheckoutInformation->IsCreditPurchase) && $CheckoutInformation->IsCreditPurchase ? $CheckoutInformation->IsCreditPurchase : false;
        $ReputationLevel = isset($CheckoutInformation->ReputationLevel) && !empty($CheckoutInformation->ReputationLevel) ? $CheckoutInformation->ReputationLevel : 'Trusted';

        $LogID = isset($CheckoutInformation->PaymentLogID) && $CheckoutInformation->PaymentLogID != 0 ? $CheckoutInformation->PaymentLogID : 0;
        $NewPackage = isset($CheckoutInformation->NewPackage) && $CheckoutInformation->NewPackage != 0 ? $CheckoutInformation->NewPackage : 0;
        $OldPackage = isset($CheckoutInformation->OldPackage) && $CheckoutInformation->OldPackage != 0 ? $CheckoutInformation->OldPackage : 0;
        $Credits = isset($CheckoutInformation->Credits) && $CheckoutInformation->Credits != 0 ? $CheckoutInformation->Credits : 0;

        $Price = $this->getPriceExchanged($CheckoutInformation->ServiceFee, $CurrencyExchangeRate);
        $PaidPrice = $this->getPriceExchanged(($CheckoutInformation->ServiceFee - $CheckoutInformation->Discount), $CurrencyExchangeRate);

        $ArrayPaymentParameters = array(
            'Process' => $CheckoutInformation->Process,
            'LogID' => $LogID,
            'UserID' => $CheckoutInformation->UserID,
            'NewPackage' => $NewPackage,
            'OldPackage' => $OldPackage,
            'IsCreditPurchase' => $IsCreditPurchase,
            'ReputationLevel' => $ReputationLevel,
            'Price' => $Price,
            'PaidPrice' => $PaidPrice,
            'Credits' => $Credits,
            'Currency' => $Currency,
            'CallbackURL' => $callBackURL
        );
        return $ArrayPaymentParameters;
    }

}
