<?php
/**
* Customfields Controller
*/
class Controller_Segments extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('segments');
		Core::LoadObject('custom_fields');
		Core::LoadObject('lists');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
		// Retrieve user information - End }

		// Check if user account has expired - Start {
		if (Users::IsAccountExpired($this->array_user_information) == true)
			{
			$_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/user/logout/', 'location', '302');
			}
		// Check if user account has expired - End }
	}
	
	/**
	 * Browse segments function
	 * @author Mert Hurturk
	 **/
	function browse($list_id)
	{
		// User privilege check - Start {
		if (Users::HasPermissions(array('Segments.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false)
		{
			show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
			exit;
		}
		// User privilege check - End }

		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'list.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));

		if ($array_return['Success'] == false)
		{
			$this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}

		$array_list_information = $array_return['List'];
		// Retrieve list information - End }

		$is_create_event = false;

		// Events - Start {
		if ($this->input->post('Command') == 'DeleteSegments')
		{
			if (Users::HasPermissions(array('Segments.Delete'), $this->array_user_information['GroupInformation']['Permissions']))
			{
				$array_event_return = $this->_event_delete_segments($list_id);
			}
		}
		else if ($this->input->post('Command') == 'Create')
		{
			$is_create_event = true;
			if (Users::HasPermissions(array('Segment.Create'), $this->array_user_information['GroupInformation']['Permissions']))
			{
				$array_create_event_return = $this->_event_create_segment($list_id);
			}
		}
		// Events - End }

		// Retrieve segments - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'segments.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'subscriberlistid'	=>	$list_id,
				'orderfield'		=>	'SegmentName',
				'ordertype'			=>	'ASC'
				)
			));
		if ($array_return['Success'] == false)
		{
			// Error occurred
		}
		else
		{
			$array_segments = $array_return['Segments'];
			$total_segment_count = $array_return['TotalSegments'] == NULL ? 0 : $array_return['TotalSegments'];
			unset($array_return);
		}
		// Retrieve segments - End }

		// Retrieve custom fields - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'customfields.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'subscriberlistid'	=>	$list_id,
				'orderfield'		=>	'FieldName',
				'ordertype'			=>	'ASC'
				)
			));
		if ($array_return['Success'] == false)
		{
			// Error occurred
		}
		else
		{
			$array_customfields = $array_return['CustomFields'];
			$total_customfield_count = $array_return['TotalCustomFields'] == NULL ? 0 : $array_return['TotalCustomFields'];
			unset($array_return);
		}
		// Retrieve custom fields - End }

		$array_view_data 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseSegments'],
								'UserInformation'				=> $this->array_user_information,
								'Segments'  					=> $array_segments,
								'CustomFields'					=> $array_customfields,
								'DefaultFields'					=> Segments::$RuleDefaultFields,
								'RuleOperators'					=> Segments::$RuleOperators,
								'ListInformation'				=> $array_list_information,
								'CurrentMenuItem'				=> 'Lists',
								'SubSection'					=> 'Segments',
								'IsEditEvent'					=> false,
								'IsCreateEvent'					=> $is_create_event,
								'ListID'						=> $list_id
								);

		$array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

		if (isset($array_event_return))
		{
			foreach ($array_event_return as $key=>$value)
			{
				$array_view_data[$key] = $value;
			}
		}

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
		{
			$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		if (isset($array_event_return) == true)
		{
			$array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
		}

		if (isset($array_create_event_return) == true)
		{
			$array_view_data[($array_create_event_return[0] == false ? 'PageCreateErrorMessage' : 'PageCreateSuccessMessage')] = $array_create_event_return[1];
		}

		$this->render('user/segments', $array_view_data);
	}

	/**
	 * Edit segment function
	 * @author Mert Hurturk
	 **/
	function edit($list_id, $segment_id)
	{
		// User privilege check - Start {
		if (Users::HasPermissions(array('Segments.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false)
		{
			show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
			exit;
		}
		// User privilege check - End }

		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'list.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));

		if ($array_return['Success'] == false)
		{
			$this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}

		$array_list_information = $array_return['List'];
		// Retrieve list information - End }

		// Get segment information - Start {
		$segment = Segments::RetrieveSegment(array('*'), array('SegmentID' => $segment_id, 'RelOwnerUserID'=>$this->array_user_information['UserID']));
		// Get segment information - End }
	
		// Events - Start {
		$array_event_data = array();
		if ($this->input->post('Command') == 'Edit')
		{
			if (Users::HasPermissions(array('Segment.Update'), $this->array_user_information['GroupInformation']['Permissions']))
			{
				$array_event_return = $this->_event_edit_segment($list_id, $segment_id);
				if (isset($array_event_return) == true)
				{
					$array_event_data[($array_event_return[0] == false ? 'PageEditErrorMessage' : 'PageEditSuccessMessage')] = $array_event_return[1];
				}
			}
		}
		// Events - End }
		
		// Retrieve segments - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'segments.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'subscriberlistid'	=>	$list_id,
				'orderfield'		=>	'SegmentName',
				'ordertype'			=>	'ASC'
				)
			));
		if ($array_return['Success'] == false)
		{
			// Error occurred
		}
		else
		{
			$array_segments = $array_return['Segments'];
			$total_segment_count = $array_return['TotalSegments'] == NULL ? 0 : $array_return['TotalSegments'];
			unset($array_return);
		}
		// Retrieve segments - End }

		// Retrieve custom fields - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'customfields.get',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'subscriberlistid'	=>	$list_id,
				'orderfield'		=>	'FieldName',
				'ordertype'			=>	'ASC'
				)
			));
		if ($array_return['Success'] == false)
		{
			// Error occurred
		}
		else
		{
			$array_customfields = $array_return['CustomFields'];
			$total_customfield_count = $array_return['TotalCustomFields'] == NULL ? 0 : $array_return['TotalCustomFields'];
			unset($array_return);
		}
		// Retrieve custom fields - End }

		$array_view_data 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseSegments'],
								'UserInformation'				=> $this->array_user_information,
								'Segments'  					=> $array_segments,
								'Segment'	  					=> $segment,
								'CustomFields'					=> $array_customfields,
								'DefaultFields'					=> Segments::$RuleDefaultFields,
								'RuleOperators'					=> Segments::$RuleOperators,
								'ListInformation'				=> $array_list_information,
								'CurrentMenuItem'				=> 'Lists',
								'SubSection'					=> 'Segments',
								'IsEditEvent'					=> true,
								'IsCreateEvent'					=> $is_create_event,
								'ListID'						=> $list_id
								);

		$array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

		if (isset($array_event_return))
		{
			foreach ($array_event_return as $key=>$value)
			{
				$array_view_data[$key] = $value;
			}
		}

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
		{
			$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		if (isset($array_event_return) == true)
		{
			$array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
		}

		if (isset($array_create_event_return) == true)
		{
			$array_view_data[($array_create_event_return[0] == false ? 'PageCreateErrorMessage' : 'PageCreateSuccessMessage')] = $array_create_event_return[1];
		}

		$this->render('user/segments', $array_view_data);
	}

	/**
	 * Create segment function
	 * @author Mert Hurturk
	 **/
	function _event_create_segment($list_id)
	{
		// Field validations - Start {
		$this->form_validation->set_rules("SegmentName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0757'], 'required');
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
		}
		// Run validation - End }

		// Create segment - Start {
		$api_parameters = array(
			'subscriberlistid'		=> $list_id,
			'segmentname'			=> $this->input->post('SegmentName'),
			'segmentoperator'		=> $this->input->post('SegmentOperator'),
			'segmentrulefield'		=> $this->input->post('SegmentRuleField'),
			'segmentrulefilter'		=> $this->input->post('SegmentRuleFilter'),
			'segmentruleoperator'	=> $this->input->post('SegmentRuleOperator')
			);
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'segment.create',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$api_parameters
			));
		// Create segment - End }

		if ($array_return['Success'] == true)
		{
			$this->load->helper('url');
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1305']);
			redirect(InterfaceAppURL(true).'/user/segments/browse/'.$list_id, 'location', '302');
		}
		else
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
			return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
		}
	}

	/**
	 * Edit segment function
	 * @author Mert Hurturk
	 **/
	function _event_edit_segment($list_id, $segment_id)
	{
		// Field validations - Start {
		$this->form_validation->set_rules("SegmentName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0757'], 'required');
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
		}
		// Run validation - End }

		// Update segment - Start {
		$api_parameters = array(
			'subscriberlistid'		=> $list_id,
			'segmentid'				=> $segment_id,
			'segmentname'			=> $this->input->post('SegmentName'),
			'segmentoperator'		=> $this->input->post('SegmentOperator'),
			'segmentrulefield'		=> $this->input->post('SegmentRuleField'),
			'segmentrulefilter'		=> $this->input->post('SegmentRuleFilter'),
			'segmentruleoperator'	=> $this->input->post('SegmentRuleOperator')
			);
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'segment.update',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	$api_parameters
			));
		// Update segment - End }


		if ($array_return['Success'] == true)
		{
			$this->load->helper('url');
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1249']);
			redirect(InterfaceAppURL(true).'/user/segments/browse/'.$list_id, 'location', '302');
		}
		else
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
			return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
		}
	}

	/**
	 * Delete segments function
	 * @author Mert Hurturk
	 **/
	function _event_delete_segments($list_id)
	{
		// Field validations - Start {
		$array_form_rules = array(
								array
									(
									'field'		=> 'SelectedSegments[]',
									'label'		=> 'segments',
									'rules'		=> 'required',
									),
								);

		$this->form_validation->set_rules($array_form_rules);
		$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['1296']);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, validation_errors());
		}
		// Run validation - End }
		
		// Delete segments - Start {
		$array_return = API::call(array(
			'format'	=>	'object',
			'command'	=>	'segments.delete',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'segments' => implode(',', $this->input->post('SelectedSegments'))
				)
			));

		if ($array_return->Success == false)
		{
			// API Error occurred
			$ErrorCode = (strtolower(gettype($array_return->ErrorCode)) == 'array' ? $array_return->ErrorCode[0] : $array_return->ErrorCode);
			switch ($ErrorCode)
			{
				case '1':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['1296']);
					break;
				default:
					break;
			}
		}
		else
		{
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1297']);
			return true;
		}
		// Delete segments - End }
	}

}

?>