<?php

/**
 * Credits controller
 *
 * @author Mert Hurturk
 */
class Controller_Credits extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->arrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->arrayUserInformation) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Create controller
     *
     * @author Mert Hurturk
     * */
    function index() {
        Core::LoadObject('user_balance');
        Core::LoadObject('user_groups');

        $Balance = UserBalance::getUserBalance($this->arrayUserInformation['UserID']);

        $PayAsYouGoPackages = UserGroups::RetrieveUserGroups(array('*'), array("CreditSystem" => "Enabled", 'IsAdminGroup' => 'No'));

        $array_view_data = array(
            'UserInformation' => $this->arrayUserInformation,
            'Balance' => $Balance,
            'PayAsYouGoPackages' => $PayAsYouGoPackages,
        );

        $this->render('user/credits', $array_view_data);
    }

    function purchase() {

        Core::LoadObject('user_balance');
        Core::LoadObject('payments');
        Core::LoadObject('user_payment');

        $new_package = $_POST['PayAsYouGoPackage'];
        $old_package = $this->arrayUserInformation["RelUserGroupID"];

//        $Credits = $_POST['Credits'];
        $CurrentBalance = UserBalance::getUserBalance($this->arrayUserInformation['UserID']);
        $ArrayNewUserGroup = UserGroups::RetrieveUserGroup($new_package);

        UserPayment::setUser($this->arrayUserInformation);
        $PurchaseInfo = UserPayment::processCreditDiscount($CurrentBalance, $ArrayNewUserGroup);

        $TotalAmount = $PurchaseInfo['TotalAmount'];
        $Discount = $PurchaseInfo['Discount'];
        $NetAmount = $PurchaseInfo['NetAmount'];
        $NewBalance = $PurchaseInfo['NewBalance'];
        $SendPayment = $PurchaseInfo['SendPayment'];
        $Credits = $PurchaseInfo['Credits'];

        if ($SendPayment) {
            $ArrayPaymentParameters = array(
                'Process' => 'CreditPurchase',
                'Credits' => $Credits,
                'NewPackage' => $new_package,
                'OldPackage' => $old_package,
                'ServiceFee' => $TotalAmount,
                'Discount' => $Discount,
                'UserID' => $this->arrayUserInformation["UserID"],
                'IsCreditPurchase' => true,
                'ReputationLevel' => 'Trusted',
                'ErrorURL' => InterfaceAppURL(true) . '/user/credits/',
            );

            $_SESSION[SESSION_NAME]['CheckoutInformation'] = array();
            unset($_SESSION[SESSION_NAME]['CheckoutInformation']);

            $_SESSION[SESSION_NAME]['CheckoutInformation'] = serialize((object) $ArrayPaymentParameters);

            $link = InterfaceAppURL(true) . '/user/' . 'payment/checkout/';
            header('Location: ' . $link);
            exit;
        } else {

            $PaymentLogID = UserPayment::createCreditPurchaseLog($PurchaseInfo);
            $_SESSION[SESSION_NAME]['PAYMENTSUCCESS'] = InterfaceLanguage('CheckOut', '0018', true, '', false, false);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/credits/', 'refresh');
        }
    }

    /**
     * Redirect to payment gateway
     *
     * @param string $Credits 
     * @return void
     * @author Cem Hurturk
     */
    function purchase_old($Credits) {

        $TotalAmount = 0;
        $RedirectURL = '';
        $PriceRange = array();

        // Calculate the total amount - Start {
        $OriginalCredits = $Credits;
        $PriceRange = explode("\n", $this->arrayUserInformation['GroupInformation']['PaymentPricingRange']);
        foreach ($PriceRange as $EachRange) {
            $EachRange = explode('|', $EachRange);
            $Range = $EachRange[0];
            $Price = $EachRange[1];

            if ($Range >= $Credits) {
                $TotalAmount += $Credits * $Price;
                break;
            } else {
                $Credits = $Credits - $Range;
                $Credits = ($Credits < 0 ? 0 : $Credits);
                $TotalAmount += $Range * $Price;
            }
        }
        $Credits = $OriginalCredits;
        // Calculate the total amount - End }
        // Take tax into consideration - Start {
        if (PAYMENT_TAX_PERCENT > 0) {
            $TotalAmount = $TotalAmount + ($TotalAmount * (PAYMENT_TAX_PERCENT / 100));
        }
        // Take tax into consideration - End }

        if (PAYMENT_CREDITS_GATEWAY_URL != '') {
            // Log to the payment log table - Start {
            Core::LoadObject('payments');
            $PaymentLogID = Payments::CreateCreditsPurchaseLog($this->arrayUserInformation, $Credits, $TotalAmount, 0, 0, $TotalAmount, 'Third Party');
            // Log to the payment log table - End }
            // Redirect to third party payment gateway
            $RedirectURL = PAYMENT_CREDITS_GATEWAY_URL;

            $ReplaceList = array();
            foreach ($this->arrayUserInformation as $EachKey => $EachValue) {
                $ReplaceList['_User:' . $EachKey . '_'] = rawurlencode($EachValue);
            }
            $ReplaceList['_Credits_'] = rawurlencode($Credits);
            $ReplaceList['_TotalAmount_'] = rawurlencode($TotalAmount);
            $ReplaceList['_PurchaseLogID_'] = rawurlencode($PaymentLogID);

            $RedirectURL = str_replace(array_keys($ReplaceList), array_values($ReplaceList), $RedirectURL);
            header('Location: ' . $RedirectURL);
            exit;
        } elseif (PAYPALEXPRESSSTATUS == 'Enabled') {
            // Log to the payment log table - Start {
            Core::LoadObject('payments');
            $PaymentLogID = Payments::CreateCreditsPurchaseLog($this->arrayUserInformation, $Credits, $TotalAmount, 0, 0, $TotalAmount, 'PayPal_Express');
            // Log to the payment log table - End }
            // Redirect to PayPal Express gateway
            Core::LoadObject('gateway');
            Core::LoadObject('payment_gateways/gateway_paypal_express.inc.php');

            PaymentGateway::SetInterface('PayPal_Express');
            $ArrayParameters = array(
                'business' => PAYPALEXPRESSBUSINESSNAME,
                // 'amount'			=> number_format($TotalAmount, 2),
                'amount' => Core::FormatCurrency($TotalAmount, false),
                'item_name' => PAYPALEXPRESSPURCHASEDESCRIPTION,
                'currency_code' => PAYPALEXPRESSCURRENCY,
                'address1' => '',
                'address2' => '',
                'city' => '',
                'country' => '',
                'first_name' => '',
                'last_name' => '',
                'lc' => '',
                'night_phone_a' => '',
                'night_phone_b' => '',
                'night_phone_c' => '',
                'state' => '',
                'zip' => '',
            );
            PaymentGateway::$Interface->SetGatewayParameters($ArrayParameters, $this->arrayUserInformation, $PaymentLogID, true);

            PaymentGateway::$Interface->Redirect();
            exit;
        } else {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/account/', 'refresh');
        }
    }

}
