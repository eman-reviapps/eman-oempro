<?php

/**
 * List controller
 *
 * @author Mert Hurturk
 */
class Controller_List extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('lists');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
      Eman
     * */
    function search($array, $key, $value) {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->search($subarray, $key, $value));
            }
        }

        return $results;
    }

    //By Eman
    public static function ListSubscribersGeo() {

        $periodToAdd = ' -' . $_POST['no_of_months'] . ' month';
        $ListID = $_POST['list_id'];
        $UserID = $_POST['user_id'];

        if ($_POST['no_of_months'] == 0) {
            //get all user data-no period
            $CurrentDate = null;
            $StartDate = null;
        } else {
            $CurrentDate = date('Y-m-d');
            $StartDate = date('Y-m-d', strtotime($StartDate . $periodToAdd));
        }
        $output = array();

        $statistics = Subscribers::RetrieveSubscribersGeo($ListID, $UserID, $StartDate, $CurrentDate);

        $GetCountryList = GetCountryList();

        $data = array();
        $countries = array();
        foreach ($statistics as $Row) {
            $data[$Row['Open_Country']] = $Row['Open_Count'];
            //$countries[$Row['Open_Country']] = $Row['Country_Name'];
            $countries[$Row['Open_Country']] = $GetCountryList[strtoupper($Row['Open_Country'])];
        }

        $output = array(
            "geo_data" => $data,
            "countries" => $countries
        );
        print_r(json_encode($output));
        return;
    }

    /**
     * List overview controller
     *
     * @author Mert Hurturk
     * */
    function statistics($list_id = '') {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
//        print_r('<pre>');
//        print_r($array_list_information);
//        print_r('</pre>');
//        exit();
        /* Eman */
        $array_list_information['OpenStatistics'] = Statistics::RetrieveListTotalOpens($list_id, $this->array_user_information['UserID'], 0);
        $array_list_information['ClickStatistics'] = Statistics::RetrieveListTotalClicks($list_id, $this->array_user_information['UserID'], 0);
        $array_list_information['ForwardStatistics'] = Statistics::RetrieveListTotalForward($list_id, $this->array_user_information['UserID'], 0);

        $BounceStatisticsArr = array();
        $BounceStatistics = $array_list_information['BounceStatistics'];

        $search = $this->search($BounceStatistics, 'status', 'Not Bounced');
        $count = count($search);

        if ($count == 1) {
            $row = $search[0];
            $BounceStatisticsArr['Not Bounced'] = array('count' => $row['count'], 'percentage' => $row['percentage']);
        } else {
            $BounceStatisticsArr['Not Bounced'] = array('count' => 0, 'percentage' => 0);
        }

        $search = $this->search($BounceStatistics, 'status', 'Soft');
        $count = count($search);

        if ($count == 1) {
            $row = $search[0];
            $BounceStatisticsArr['Soft'] = array('count' => $row['count'], 'percentage' => $row['percentage']);
        } else {
            $BounceStatisticsArr['Soft'] = array('count' => 0, 'percentage' => 0);
        }

        $search = $this->search($BounceStatistics, 'status', 'Hard');
        $count = count($search);

        if ($count == 1) {
            $row = $search[0];
            $BounceStatisticsArr['Hard'] = array('count' => $row['count'], 'percentage' => $row['percentage']);
        } else {
            $BounceStatisticsArr['Hard'] = array('count' => 0, 'percentage' => 0);
        }
        $array_list_information['BounceStatisticsArr'] = $BounceStatisticsArr;
        /* Eman */
        $array_list_information['UnsubscribesStatistics'] = Statistics::RetrieveListTotalUnSubscribes($list_id, $this->array_user_information['UserID']);

        $array_list_information['OpenPerformanceDifference'] = $array_list_information['OverallOpenPerformance'] - $array_list_information['OverallAccountOpenPerformance'];
        $array_list_information['ClickPerformanceDifference'] = $array_list_information['OverallClickPerformance'] - $array_list_information['OverallAccountClickPerformance'];
        $array_list_information['HighestOpenDay'] = Statistics::RetrieveOpenPerformanceDaysOfList($list_id, $this->array_user_information['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
        $array_list_information['HighestOpenDay'] = $array_list_information['HighestOpenDay'][0][0];
        $array_list_information['HighestClickDay'] = Statistics::RetrieveClickPerformanceDaysOfList($list_id, $this->array_user_information['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
        $array_list_information['HighestClickDay'] = $array_list_information['HighestClickDay'][0][0];
        // Retrieve list information - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserListOverview'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'Statistics'
        );

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }


        $this->render('user/list_statistics', $array_view_data);
        // Interface parsing - End }
    }

    /* By Eman */

    function reports($ListID, $Statistic, $Date = null) {
        Core::LoadObject('statistics');

        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $ListID
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve campaign information - End }

        $view = 'user/campaign_report_opens';

        $ResultColumns['SubscriberID'] = 'SubscriberID';
        $ResultColumns['EmailAddress'] = 'EmailAddress';
        $ResultColumns['Subscriber_IP'] = 'Subscriber_IP';
        $ResultColumns['City'] = 'City';
        $ResultColumns['Country'] = 'Country';

        $ResultSet = null;
        if ($Statistic == 'opens') {
            $ResultSet = Statistics::RetrieveListDateOpens($array_list_information['ListID'], $this->array_user_information['UserID'], false, $Date);
            $ResultColumns['TotalOpens'] = 'TotalOpens';
            $ResultColumns['OpenDate'] = 'OpenDate';
        } elseif ($Statistic == 'unique_opens') {
            $ResultSet = Statistics::RetrieveListDateOpens($array_list_information['ListID'], $this->array_user_information['UserID'], true, $Date);
            $ResultColumns['OpenDate'] = 'OpenDate';
        } elseif ($Statistic == 'clicks') {
            $ResultSet = Statistics::RetrieveListDateClicks($array_list_information['ListID'], $this->array_user_information['UserID'], false, $Date);
            $ResultColumns['TotalClicks'] = 'TotalClicks';
            $ResultColumns['ClickDate'] = 'ClickDate';
        } elseif ($Statistic == 'unique_clicks') {
            $ResultSet = Statistics::RetrieveListDateClicks($array_list_information['ListID'], $this->array_user_information['UserID'], true, $Date);
            $ResultColumns['ClickDate'] = 'ClickDate';
        } elseif ($Statistic == 'forwards') {
            $ResultSet = Statistics::RetrieveListDateForwards($array_list_information['ListID'], $this->array_user_information['UserID'], false, $Date);
            $ResultColumns['TotalForwards'] = 'TotalForwards';
            $ResultColumns['ForwardDate'] = 'ForwardDate';
        } elseif ($Statistic == 'unique_forwards') {
            $ResultSet = Statistics::RetrieveListDateForwards($array_list_information['ListID'], $this->array_user_information['UserID'], true, $Date);
            $ResultColumns['ForwardDate'] = 'ForwardDate';
        } elseif ($Statistic == 'unsubscriptions') {
            $ResultSet = Statistics::RetrieveListUnsubscriptions($array_list_information['ListID'], $this->array_user_information['UserID'], $Date);
            $ResultColumns['UnsubscriptionDate'] = 'UnsubscriptionDate';
        } elseif ($Statistic == 'soft_bounces') {
            $ResultSet = Statistics::RetrieveListBounceDate($array_list_information['ListID'], $this->array_user_information['UserID'], $Date, 'Soft');
            $ResultColumns['BounceDate'] = 'BounceDate';
        } elseif ($Statistic == 'hard_bounces') {
            $ResultSet = Statistics::RetrieveListBounceDate($array_list_information['ListID'], $this->array_user_information['UserID'], $Date, 'Hard');
            $ResultColumns['BounceDate'] = 'BounceDate';
        } elseif ($Statistic == 'spams') {
            $ResultSet = Statistics::RetrieveListDateSPAM($array_list_information['ListID'], $this->array_user_information['UserID'], $Date);
            $ResultColumns['TotalSPAMComplaints'] = 'TotalSPAMComplaints';
            $ResultColumns['DateOfReceive'] = 'DateOfReceive';
        }

        $ArrayDataRows = array();
        $global_custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsGlobal' => 'Yes'));

//        $ResultColumns = array();
//        $i  =  0;
//        while ($i < mysql_num_fields($ResultSet)) {
//            $meta = mysql_fetch_field($ResultSet, $i);
//            $ResultColumns[$meta->name] = $meta->name;
//            $i++;
//        }

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            if ($Statistic == 'spams') {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['SubscriberID']), $EachRow['ListID']);
                $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachRow['ListID']));
            } else {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                $custom_fields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $this->ArrayUserInformation['UserID'], 'RelListID' => $EachRow['RelListID']));
            }
            if ($global_custom_fields != false) {
                $custom_fields = array_merge($custom_fields ? $custom_fields : array(), $global_custom_fields);
            }
            if ($Statistic == 'forwards') {
                $ArraySubscriber['TotalForwards'] = number_format($EachRow['TotalForwards']);
                $ArraySubscriber['ForwardDate'] = number_format($EachRow['ForwardDate']);
            } elseif ($Statistic == 'unique_forwards') {
                $ArraySubscriber['ForwardDate'] = number_format($EachRow['ForwardDate']);
            } elseif ($Statistic == 'clicks') {
                $ArraySubscriber['TotalClicks'] = number_format($EachRow['TotalClicks']);
                $ArraySubscriber['ClickDate'] = $EachRow['ClickDate'];
            } elseif ($Statistic == 'unique_clicks') {
                $ArraySubscriber['ClickDate'] = $EachRow['ClickDate'];
            } elseif ($Statistic == 'opens') {
                $ArraySubscriber['TotalOpens'] = number_format($EachRow['TotalOpens']);
                $ArraySubscriber['OpenDate'] = $EachRow['OpenDate'];
            } elseif ($Statistic == 'unique_opens') {
                $ArraySubscriber['OpenDate'] = $EachRow['OpenDate'];
            } elseif ($Statistic == 'unsubscriptions') {
                $ArraySubscriber['UnsubscriptionDate'] = $EachRow['UnsubscriptionDate'];
            } elseif ($Statistic == 'soft_bounces' || $Statistic == 'hard_bounces') {
                $ArraySubscriber['BounceDate'] = $EachRow['BounceDate'];
            } elseif ($Statistic == 'spams') {
                $ArraySubscriber['TotalSPAMComplaints'] = number_format($EachRow['TotalSPAMComplaints']);
                $ArraySubscriber['DateOfReceive'] = $EachRow['DateOfReceive'];
            }

            foreach ($ArraySubscriber as $key => $value) {
                if (strpos($key, 'CustomField') !== false) {
                    $id = substr($key, 11);
                    foreach ($custom_fields as $custom_field) {
                        $custom_field_ID = $custom_field['CustomFieldID'];
                        $field_name = $custom_field['FieldName'];
                        if ($id == $custom_field_ID) {
                            $ArraySubscriber[$custom_field['FieldName']] = $ArraySubscriber['CustomField' . $id];
                            unset($ArraySubscriber['CustomField' . $id]);
                        }
                    }
                }
            }
            unset($ArraySubscriber['SubscriptionStatus']);
            if ($Statistic != 'unsubscriptions') {
                unset($ArraySubscriber['UnsubscriptionDate']);
                unset($ArraySubscriber['UnsubscriptionIP']);
            }
            unset($ArraySubscriber['BounceType']);
            unset($ArraySubscriber['SubscriptionDate']);
            unset($ArraySubscriber['SubscriptionIP']);
            unset($ArraySubscriber['OptInDate']);

            $ArrayDataRows[] = $ArraySubscriber;
        }


        if ($Statistic == 'opens') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '0837', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0837', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0837', true),
            );
        } elseif ($Statistic == 'unique_opens') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '0094', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0094', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0094', true),
            );
        } elseif ($Statistic == 'clicks') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '0838', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0838', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0838', true),
            );
        } elseif ($Statistic == 'unique_clicks') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '1390', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '1390', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '1390', true),
            );
        } elseif ($Statistic == 'forwards') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '0839', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0839', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0839', true),
            );
        } elseif ($Statistic == 'unique_forwards') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '9125', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9125', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9125', true),
            );
        } elseif ($Statistic == 'unsubscriptions') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '9205', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9205', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9205', true),
            );
        } elseif ($Statistic == 'soft_bounces') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '9213', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9213', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9213', true),
            );
        } elseif ($Statistic == 'hard_bounces') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '9208', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9208', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '9208', true),
            );
        } elseif ($Statistic == 'spams') {
            $StatisticViewData = array(
                'SubSection' => InterfaceLanguage('Screen', '0095', true),
                'Title' => ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0095', true),
                'PageTitle' => " - " . ucwords($array_list_information['Name']) . " - " . InterfaceLanguage('Screen', '0095', true),
            );
        }

        // Interface parsing - Start {
        $ArrayViewData = array(
            'ResultRows' => $ArrayDataRows,
            'ResultColumns' => $ResultColumns,
            'UserInformation' => $this->ArrayUserInformation,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
        );

        $ArrayViewData = array_merge($ArrayViewData, $StatisticViewData);
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        $this->render('user/list_report_details', $ArrayViewData);
        // Interface parsing - End }
    }

    /**
      Eman
     * */
    function listSubsUnsubscData() {
        $ListID = $_POST['list_id'];

        $Days = $_POST['days'];
        $LastXDays = ($Days < 7 ? 7 : $Days);
//        $StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
//        $ToDate = date('Y-m-d', strtotime($StartFromDate . ' +' . ($LastXDays - 1) . ' days'));

        $ToDate = date('Y-m-d', time());
        $StartFromDate = date('Y-m-d', strtotime($ToDate . ' -' . ($LastXDays - 1) . ' days'));

        $type = $_POST['type'];

        Core::LoadObject('statistics');

        $Statistics = Statistics::RetrieveListSubsUnsubsStatistics($ListID, $this->array_user_information['UserID'], $StartFromDate, $ToDate);

        $output = array();

        $i = 1;
        foreach ($Statistics as $key => $row) {
            $exp = explode("-", $key);
            $day = $exp[2];

            $new_row['day'] = $i; //$day;

            if ($type == 'subscriptions') {
                $new_row['TotalSubscriptions'] = $row['TotalSubscriptions'] ? $row['TotalSubscriptions'] : 0;
            } else if ($type == 'unsubscriptions') {
                $new_row['TotalUnsubscriptions'] = $row['TotalUnsubscriptions'] ? $row['TotalUnsubscriptions'] : 0;
            }

            $new_row['date'] = $row['date']; //date('Y-m-d', strtotime($StartFromDate . ' +' . ($day - 1) . ' days'));

            $output[] = $new_row;
            $i++;
        }
        print_r((json_encode($output)));
        return;
    }

    /**
     * List subscription, unsubscription chart data
     *
     * @author Mert Hurturk
     * */
    function chartdata($list_id = '', $days = 30, $statistic = 'subscriptions-unsubscriptions') {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }

        $ToDate = date('Y-m-d H:i:s', time() + ($days * 86400));

        if ($statistic == 'subscriptions-unsubscriptions') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('activity'));

            $ArrayChartGraphColors = explode(',', CHART_COLORS);

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'left'),
                1 => array('title' => '', 'axis' => 'left'),
            );
            $ArrayData['GraphData'] = array();

            for ($TMPCounter = $days; $TMPCounter >= 0; $TMPCounter--) {
                $ArrayData['Series'][] = '<b>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1710'][date('M', strtotime($ToDate . ' -' . $TMPCounter . ' days'))] . ' ' . date('j', strtotime($ToDate . ' -' . $TMPCounter . ' days')) . '</b>';
                $ArrayData['GraphData'][0][] = array(
                    'Value' => (isset($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions']) == true ? $array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0149'], number_format($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions'])),
                    ),
                );
                $ArrayData['GraphData'][1][] = array(
                    'Value' => (isset($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions']) == true ? $array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'])),
                    ),
                );
            }

            Core::LoadObject('chart_data_generator');
            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        } elseif ($statistic == 'bounces') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('bounce'));

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $ArrayXML = array();
            $ArrayXML[] = '<?xml version="1.0" encoding="UTF-8"?>';
            $ArrayXML[] = '<pie>';

            $TMPCounter = 0;
            if (count($array_list_information['BounceStatistics']) > 0) {
                foreach ($array_list_information['BounceStatistics'] as $Key => $ArrayEachData) {
                    $ArrayXML[] = '<slice title="' . $ArrayEachData['status'] . '" color="' . $ArrayChartGraphColors[$TMPCounter] . '">' . $ArrayEachData['count'] . '</slice>';
                    $TMPCounter++;
                }
            } else {
                $ArrayXML[] = '<slice title="' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1171'] . '" color="' . $ArrayChartGraphColors[0] . '">100</slice>';
            }
            $ArrayXML[] = '</pie>';
            header('Content-type: text/xml');
            print($XML = implode("\n", $ArrayXML));
            exit;
        } elseif ($statistic == 'email-domain') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('domain'));

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $ArrayXML = array();
            $ArrayXML[] = '<?xml version="1.0" encoding="UTF-8"?>';
            $ArrayXML[] = '<pie>';

            $TMPCounter = 0;
            if (count($array_list_information['EmailDomainStatistics']) > 0) {
                foreach ($array_list_information['EmailDomainStatistics'] as $Key => $ArrayEachData) {
                    $ArrayXML[] = '<slice title="' . $ArrayEachData['domain'] . '" color="' . $ArrayChartGraphColors[$TMPCounter] . '">' . $ArrayEachData['count'] . '</slice>';
                    $TMPCounter++;
                }
            } else {
                $ArrayXML[] = '<slice title="" color="' . $ArrayChartGraphColors[0] . '"></slice>';
            }
            $ArrayXML[] = '</pie>';
            header('Content-type: text/xml');
            print($XML = implode("\n", $ArrayXML));
            exit;
        }
    }

    /**
     * Sparkline controller
     *
     * @author Mert Hurturk
     * */
    function sparkline($statistic, $list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            exit;
        }
        // User privilege check - End }

        $sparkline_data = array();
        if ($statistic == 'opens') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('open'));

            foreach ($array_list_information['OpenStatistics'] as $each) {
                $sparkline_data[] = $each['Total'];
            }
        } elseif ($statistic == 'clicks') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('click'));

            foreach ($array_list_information['ClickStatistics'] as $each) {
                $sparkline_data[] = $each['Total'];
            }
        } elseif ($statistic == 'forwards') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('forward'));

            foreach ($array_list_information['ForwardStatistics'] as $each) {
                $sparkline_data[] = $each['Total'];
            }
        } elseif ($statistic == 'views') {
            $array_list_information = Lists::RetrieveList(array('*'), array('ListID' => $list_id, 'RelOwnerUserID' => $this->array_user_information['UserID']), true, false, array('view'));

            foreach ($array_list_information['BrowserViewStatistics'] as $each) {
                $sparkline_data[] = $each['Total'];
            }
        }
        $sparkline_data = implode('x', $sparkline_data);
        $this->load->library('OemproSparkLine', array());
        $this->oemprosparkline->draw(array('data' => $sparkline_data));
    }

    /**
     * Create controller
     *
     * @author Mert Hurturk
     * */
    function create() {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Create'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        if (AccountIsUnTrustedCheck($this->array_user_information)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'Create') {
            $array_event_return = $this->_event_create_list();
        }
        // Events - End }

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateList'],
            'UserInformation' => $this->array_user_information,
            'CurrentMenuItem' => 'Lists'
        );

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        $this->render('user/list_create', $array_view_data);
    }

    function deleteconfirmationemail($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array('format' => 'array', 'command' => 'list.get', 'protected' => true, 'username' => $this->array_user_information['Username'], 'password' => $this->array_user_information['Password'], 'parameters' => array('listid' => $list_id)));

        // Retrieve list information - End }
        $this->load->helper('url');
        if ($array_return['Success'] == false) {
            redirect(InterfaceAppURL(true) . '/user/list/settings/' . $list_id);
        } else {
            $array_list_information = $array_return['List'];

            API::call(array('format' => 'array', 'command' => 'email.delete', 'protected' => true, 'username' => $this->array_user_information['Username'], 'password' => $this->array_user_information['Password'], 'parameters' => array('emailid' => $array_list_information['RelOptInConfirmationEmailID'])));
            API::call(array('format' => 'array', 'command' => 'list.update', 'protected' => true, 'username' => $this->array_user_information['Username'], 'password' => $this->array_user_information['Password'], 'parameters' => array('optinconfirmationemailid' => 0, 'subscriberlistid' => $array_list_information['ListID'])));


            redirect(InterfaceAppURL(true) . '/user/list/settings/' . $list_id);
        }
    }

    /**
     * Settings controller
     * @author Mert Hurturk
     * */
    function settings($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        $array_event_data = array();
        if ($this->input->post('Command') == 'Save') {
            $array_event_return = $this->_event_save($list_id);
            $array_event_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        } else if ($this->input->post('Command') == 'NewURL') {
            $array_event_return = $this->_event_new_service_url($list_id);
            $array_event_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }
        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve web service integration urls - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'listintegration.geturls',
                    'protected' => 'true',
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id
                    )
        ));
        $service_urls = $array_return['URLs'];
        // Retrieve web service integration urls - End }
        // Retrieve lists - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            $array_lists = array();
        } else {
            $array_lists = $array_return['Lists'];
            unset($array_return);
        }
        // Retrieve lists - End }
        // Retrieve email information - Start {
        $array_email = array();
        if ($array_list_information['OptInMode'] == 'Double') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.get',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'emailid' => $array_list_information['RelOptInConfirmationEmailID']
                        )
            ));
            $array_email = $array_return['EmailInformation'];
        }
        // Retrieve email information - End }
        // Get personalization tags - Start {
        Core::LoadObject('personalization');
        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($this->array_user_information['UserID'], array($list_id), ApplicationHeader::$ArrayLanguageStrings['Screen']['0665'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0778'], array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_opt_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'Opt');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_content_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0984'] => $array_opt_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
        );
        $array_subject_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
        );
        // Get personalization tags - End }
        // Get attachments - Start {
        $array_attachments = Attachments::RetrieveAttachments(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelEmailID' => $array_email['EmailID']));
        // Get attachments - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserListSettings'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'EmailInformation' => $array_email,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'Settings',
            'ContentTags' => $array_content_tags,
            'SubjectTags' => $array_subject_tags,
            'Attachments' => $array_attachments,
            'ServiceURLs' => $service_urls,
            'UseSameNameAndEmail' => (($array_email['FromName'] == $array_email['ReplyToName']) && ($array_email['FromEmail'] == $array_email['ReplyToEmail'])),
            'IsEditEvent' => true,
            'ListID' => $list_id,
            'Lists' => $array_lists,
            'EmailEditMode' => 'Confirmation'
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        $array_view_data = array_merge($array_view_data, $array_event_data);

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/list_settings', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Synchronization controller
     * @author Mert Hurturk
     * */
    function synchronization($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('List.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false || Users::HasPermissions(array('Subscribers.Import'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Events - Start {
        $array_event_data = array();
        $this->form_validation->set_rules('SyncStatus', '');
        $this->form_validation->set_rules('SyncPeriod', '');
        $this->form_validation->set_rules('SyncSendReportEmail', '');
        $this->form_validation->run();
        if ($this->input->post('Command') == 'Save') {
            if ($this->input->post('MapField') == 'true') {
                $this->_event_map_fields();
            }

            if ($this->input->post('SaveSyncDBSettings') == 'true') {
                $this->_event_save_sync_db_settings();
            }

            $this->_event_save_sync_settings();
        } else if ($this->input->post('Command') == 'resetSyncSettings') {
            $this->_event_reset_sync_db_settings();
        }
        // Events - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));

        $array_custom_fields = $array_return['CustomFields'];
        // Retrieve custom fields - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserListSynchronization'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'CustomFields' => $array_custom_fields,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'Synchronization'
        );

        if (isset($_SESSION[SESSION_NAME]['ListSynchronizationVariables']) && count($_SESSION[SESSION_NAME]['ListSynchronizationVariables']) > 0) {
            $array_view_data['MapFields'] = true;
            $array_view_data['ImportFields'] = $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportFields'];
        }

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        $array_view_data = array_merge($array_view_data, $array_event_data);

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/list_synchronization', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Delete controller
     * @author Mert Hurturk
     * */
    function delete($list_id) {
        if (Users::HasPermissions(array('List.Delete'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }

        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        if (isset($_POST['ListDeleteConfirm']) && $_POST['ListDeleteConfirm'] == 'true') {
            // Delete campaign - Start {
            $array_api_vars = array(
                'lists' => $array_list_information['ListID']
            );
            $array_return = API::call(array(
                        'format' => 'object',
                        'command' => 'lists.delete',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => $array_api_vars
            ));

            if ($array_return->Success == true) {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1135']);

                $this->load->helper('url');
                redirect(InterfaceAppURL(true) . '/user/lists/browse/', 'location', '302');
            }
            // Delete campaign - End }
        }
        // Events - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserListDelete'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'Delete',
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/list_delete', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Forms controller
     * @author Mert Hurturk
     * */
    function forms($list_id) {

        if (AccountIsUnTrustedCheck($this->array_user_information)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }

        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Retrieve all lists of user - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['TotalListCount'] > 0) {
            $array_lists = $array_return['Lists'];
            $index = 0;
            for ($i = 0; $i < count($array_lists); $i++) {
                if ($array_lists[$i]['ListID'] == $list_id) {
                    $index = $i;
                    break;
                }
            }
            array_splice($array_lists, $index, 1);
        } else {
            $array_lists = array();
        }
        // Retrieve all lists of user - End }
        // Generate unsubscription form code - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'listintegration.generateunsubscriptionformhtmlcode',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'unsubscribebuttonstring' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1309']
                    )
        ));
        $unsubscription_html_code = implode("\n", $array_return['HTMLCode']);
        // Generate unsubscription form code - End }
        // Retrieve form styles - Start {
        $formStyles = array();
        $formStylesPath = DATA_PATH . 'form_styles/';
        if (is_dir($formStylesPath)) {
            if ($DirHandler = opendir($formStylesPath)) {
                $TMPCounter = 0;
                while (($EachFile = readdir($DirHandler)) !== false) {
                    if (($EachFile != '.svn') && ($EachFile != '.') && ($EachFile != '..') && preg_match('/(.*)\.css$/', $EachFile)) {
                        $formStyles[] = str_replace('.css', '', $EachFile);
                    }
                }
            }
        }

        // Retrieve form styles - End }
        // Retrieve custom fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'orderby' => 'ASC'
                    )
        ));
        $array_custom_fields = count($array_return['CustomFields']) < 1 ? array() : $array_return['CustomFields'];
        // Retrieve custom fields - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserListForms'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'Forms',
            'Lists' => $array_lists,
            'UnsubscriptionHTML' => $unsubscription_html_code,
            'CustomFields' => $array_custom_fields,
            'FormStyles' => $formStyles
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        $this->render('user/list_forms', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Export statistics controller
     * @author Mert Hurturk
     * */
    function exportstatistics($list_id, $type = 'csv') {
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }

        if ($type == 'csv') {
            $csv_string = ApplicationHeader::$ArrayLanguageStrings['Screen']['1150'] . "\n\n" . Statistics::ConvertToCSV($array_list_information['ActivityStatistics']);
            $csv_string .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0919'] . "\n\n" . Statistics::ConvertToCSV($array_list_information['OpenStatistics']);
            $csv_string .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0920'] . "\n\n" . Statistics::ConvertToCSV($array_list_information['ClickStatistics']);
            $csv_string .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0921'] . "\n\n" . Statistics::ConvertToCSV($array_list_information['BrowserViewStatistics']);
            $csv_string .= "\n\n" . ApplicationHeader::$ArrayLanguageStrings['Screen']['0922'] . "\n\n" . Statistics::ConvertToCSV($array_list_information['ForwardStatistics']);

            // Make CSV data file downloadable - Start
            header("Content-type: application/octetstream");
            header("Content-Disposition: attachment; filename=\"subscriber_list_statistics.csv\"");
            header('Content-length: ' . strlen($csv_string));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");
            // Make CSV data file downloadable - End
            print $csv_string;
            exit;
        } else if ($type == 'xml') {
            $xml_string = '<statistics>';
            $xml_string .= '<activity_statistics>' . Statistics::ConvertToXML($array_list_information['ActivityStatistics']) . '</activity_statistics>';
            $xml_string .= '<open_statistics>' . Statistics::ConvertToXML($array_list_information['OpenStatistics']) . '</open_statistics>';
            $xml_string .= '<click_statistics>' . Statistics::ConvertToXML($array_list_information['ClickStatistics']) . '</click_statistics>';
            $xml_string .= '<browser_view_statistics>' . Statistics::ConvertToXML($array_list_information['BrowserViewStatistics']) . '</browser_view_statistics>';
            $xml_string .= '<forward_statistics>' . Statistics::ConvertToXML($array_list_information['ForwardStatistics']) . '</forward_statistics>';
            $xml_string .= '</statistics>';
            // Make XML data file downloadable - Start
            header("Content-type: application/octetstream");
            header("Content-Disposition: attachment; filename=\"subscriber_list_statistics.xml\"");
            header('Content-length: ' . strlen($xml_string));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");
            // Make XML data file downloadable - End
            print $xml_string;
            exit;
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_reset_sync_db_settings() {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $this->input->post('ListID'),
                        'syncstatus' => 'Enabled',
                        'syncperiod' => $this->input->post('SyncPeriod'),
                        'syncsendreportemail' => $this->input->post('SyncSendReportEmail') == 'Yes' ? 'Yes' : 'No',
                        'syncmysqlhost' => '',
                        'syncmysqlport' => '',
                        'syncmysqlusername' => '',
                        'syncmysqlpassword' => '',
                        'syncmysqldbname' => '',
                        'syncmysqlquery' => '',
                        'syncfieldmapping' => ''
                    )
        ));

        $_SESSION[SESSION_NAME]['ListSynchronizationVariables'] = array();
        unset($_SESSION[SESSION_NAME]['ListSynchronizationVariables']);
    }

    function _event_map_fields() {
        if ($this->input->post('SyncStatus') == 'Enabled' && ($this->input->post('SyncMySQLHost') == '' || $this->input->post('SyncMySQLPort') == '' || $this->input->post('SyncMySQLUsername') == '' || $this->input->post('SyncMySQLPassword') == '' || $this->input->post('SyncMySQLDBName') == '' || $this->input->post('SyncMySQLQuery') == '')) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1206'];

            return false;
        }

        $blockedHosts = explode(',', BLOCKED_MYSQL_HOSTS);
        if (in_array($this->input->post('SyncMySQLHost'), $blockedHosts)) {
            $_SESSION['PageMessageCache'][0] = 'Error';
            $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1820'];

            return false;
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.import',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $this->input->post('ListID'),
                        'importstep' => 1,
                        'importtype' => 'MySQL',
                        'importmysqlhost' => $this->input->post('SyncMySQLHost'),
                        'importmysqlport' => $this->input->post('SyncMySQLPort'),
                        'importmysqlusername' => $this->input->post('SyncMySQLUsername'),
                        'importmysqlpassword' => $this->input->post('SyncMySQLPassword'),
                        'importmysqldatabase' => $this->input->post('SyncMySQLDBName'),
                        'importmysqlquery' => $this->input->post('SyncMySQLQuery'),
                    )
        ));

        if ($array_return['Success'] == false) {
            if ($array_return['ErrorCode'] == 15) {
                $_SESSION['PageMessageCache'][0] = 'Error';
                $_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['1206'];

                return false;
            } else if ($array_return['ErrorCode'] == 16) {
                $_SESSION['PageMessageCache'][0] = 'Error';
                $_SESSION['PageMessageCache'][1] = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1207'], $array_return['MySQLError']);

                return false;
            }
        }

        $_SESSION[SESSION_NAME]['ListSynchronizationVariables'] = array();
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportID'] = $array_return['ImportID'];
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportType'] = 'MySQL';
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportFields'] = $array_return['ImportFields'];
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLHost'] = $this->input->post('SyncMySQLHost');
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLPort'] = $this->input->post('SyncMySQLPort');
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLUsername'] = $this->input->post('SyncMySQLUsername');
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLPassword'] = $this->input->post('SyncMySQLPassword');
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLDBName'] = $this->input->post('SyncMySQLDBName');
        $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLQuery'] = $this->input->post('SyncMySQLQuery');

        return true;
    }

    function _event_save_sync_db_settings() {
        $array_matched_fields = array();
        if (strtolower(gettype($_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportFields'])) === 'array') {
            for ($i = 1; $i < count($_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportFields']) + 1; $i++) {
                if ($this->input->post('MatchedFields' . $i) !== '0') {
                    $array_matched_fields[$_SESSION[SESSION_NAME]['ListSynchronizationVariables']['ImportFields']['FIELD' . $i]] = $this->input->post('MatchedFields' . $i);
                }
            }
        }
        $matched_fields = array();
        foreach ($array_matched_fields as $key => $value) {
            $matched_fields[] = $key . '||||' . $value;
        }
        $matched_fields = implode(',', $matched_fields);

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'syncstatus' => 'Enabled',
                        'subscriberlistid' => $this->input->post('ListID'),
                        'syncmysqlhost' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLHost'],
                        'syncmysqlport' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLPort'],
                        'syncmysqlusername' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLUsername'],
                        'syncmysqlpassword' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLPassword'],
                        'syncmysqldbname' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLDBName'],
                        'syncmysqlquery' => $_SESSION[SESSION_NAME]['ListSynchronizationVariables']['SyncMySQLQuery'],
                        'syncfieldmapping' => $matched_fields
                    )
        ));

        $_SESSION[SESSION_NAME]['ListSynchronizationVariables'] = array();
        unset($_SESSION[SESSION_NAME]['ListSynchronizationVariables']);
    }

    function _event_save_sync_settings() {
        if ($this->input->post('SyncStatus') == 'Disabled') {
            $_SESSION[SESSION_NAME]['ListSynchronizationVariables'] = array();
            unset($_SESSION[SESSION_NAME]['ListSynchronizationVariables']);
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $this->input->post('ListID'),
                        'syncstatus' => $this->input->post('SyncStatus'),
                        'syncperiod' => $this->input->post('SyncPeriod'),
                        'syncsendreportemail' => $this->input->post('SyncSendReportEmail') == 'Yes' ? 'Yes' : 'No'
                    )
        ));
    }

    /**
     * Save list settings event
     * @author Mert Hurturk
     * */
    function _event_save($list_id) {
        // Field validations - Start {
        $this->form_validation->set_rules("OptInMode");
        $this->form_validation->set_rules("HideInSubscriberArea");
        $this->form_validation->set_rules("OptInSubscribeTo");
        $this->form_validation->set_rules("OptInSubscribeToEnabled");
        $this->form_validation->set_rules("OptInUnsubscribeFrom");
        $this->form_validation->set_rules("OptInUnsubscribeFromEnabled");
        $this->form_validation->set_rules("SubscriptionConfirmationPendingPageURL");
        $this->form_validation->set_rules("SubscriptionConfirmationPendingPageURLEnabled");
        $this->form_validation->set_rules("SubscriptionConfirmedPageURL");
        $this->form_validation->set_rules("SubscriptionConfirmedPageURLEnabled");
        $this->form_validation->set_rules("SubscriptionErrorPageURL");
        $this->form_validation->set_rules("SubscriptionErrorPageURLEnabled");
        $this->form_validation->set_rules("SendServiceIntegrationFailedNotification");
//        $this->form_validation->set_rules("OptOutSubscribeTo");
//        $this->form_validation->set_rules("OptOutSubscribeToEnabled");
//        $this->form_validation->set_rules("OptOutUnsubscribeFrom");
//        $this->form_validation->set_rules("OptOutUnsubscribeFromEnabled");
        $this->form_validation->set_rules("OptOutScope");
//        $this->form_validation->set_rules("OptOutAddToSuppressionList");
        $this->form_validation->set_rules("OptOutAddToGlobalSuppressionList");
        $this->form_validation->set_rules("UnsubscriptionConfirmedPageURL");
        $this->form_validation->set_rules("UnsubscriptionConfirmedPageURLEnabled");
        $this->form_validation->set_rules("UnsubscriptionErrorPageURL");
        $this->form_validation->set_rules("UnsubscriptionErrorPageURLEnabled");
        $this->form_validation->set_rules("SendActivityNotification");
        $this->form_validation->set_rules("Name", ApplicationHeader::$ArrayLanguageStrings['Screen']['0949'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'name' => $this->input->post('Name'),
                        'optinmode' => $this->input->post('OptInMode'),
                        'hideinsubscriberarea' => $this->input->post('HideInSubscriberArea') === false ? 'false' : 'true',
                        'optinsubscribeto' => ($this->input->post('OptInSubscribeToEnabled') === false ? 0 : $this->input->post('OptInSubscribeTo')),
                        'optinunsubscribefrom' => ($this->input->post('OptInUnsubscribeFromEnabled') === false ? 0 : $this->input->post('OptInUnsubscribeFrom')),
//                        'optoutsubscribeto' => SUBSCRIBE_ON_UNSUBSCRIPTION_ENABLED == FALSE ? 0 : ($this->input->post('OptOutSubscribeToEnabled') === false ? 0 : $this->input->post('OptOutSubscribeTo')),
                        'optoutsubscribeto' => 0,
//                        'optoutunsubscribefrom' => ($this->input->post('OptOutUnsubscribeFromEnabled') === false ? 0 : $this->input->post('OptOutUnsubscribeFrom')),
                        'optoutunsubscribefrom' => 0,
//                        'optoutscope' => ($this->input->post('OptOutScope') === false ? 'This list' : 'All lists'),
                        'optoutscope' => 'All lists',
//                        'optoutaddtosuppressionlist' => ($this->input->post('OptOutAddToSuppressionList') === false ? 'No' : 'Yes'),
                        'optoutaddtosuppressionlist' => 'No',
                        'optoutaddtoglobalsuppressionlist' => 'Yes',
                        'reqbyemailsearchtoaddress' => EMAIL_REQUESTS_ENABLED ? $this->input->post('ReqByEmailSearchToAddress') : '',
                        'reqbyemailsubscriptioncommand' => EMAIL_REQUESTS_ENABLED ? $this->input->post('ReqByEmailSubscriptionCommand') : '',
                        'reqbyemailunsubscriptioncommand' => EMAIL_REQUESTS_ENABLED ? $this->input->post('ReqByEmailUnsubscriptionCommand') : '',
                        'subscriptionconfirmationpendingpageurl' => $this->input->post('SubscriptionConfirmationPendingPageURL'),
                        'subscriptionconfirmedpageurl' => $this->input->post('SubscriptionConfirmedPageURL'),
                        'subscriptionerrorpageurl' => $this->input->post('SubscriptionErrorPageURL'),
                        'unsubscriptionconfirmedpageurl' => REDIRECT_ON_UNSUBSCRIPTION_ENABLED == FALSE ? '' : $this->input->post('UnsubscriptionConfirmedPageURL'),
                        'unsubscriptionerrorpageurl' => REDIRECT_ON_UNSUBSCRIPTION_ENABLED == FALSE ? '' : $this->input->post('UnsubscriptionErrorPageURL'),
                        'sendserviceintegrationfailednotification' => $this->input->post('SendServiceIntegrationFailedNotification') === false ? 'false' : $this->input->post('SendServiceIntegrationFailedNotification'),
                        'sendactivitynotification' => $this->input->post('SendActivityNotification') === false ? 'false' : $this->input->post('SendActivityNotification'),
                        'subscriberlistid' => $list_id
                    )
        ));

        return array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['1051']);
    }

    /**
     * Add new web service integration url
     * @author Mert Hurturk
     * */
    function _event_new_service_url($list_id) {
        // Field validations - Start {
        $this->form_validation->set_rules("OptInMode");
        $this->form_validation->set_rules("HideInSubscriberArea");
        $this->form_validation->set_rules("OptInSubscribeTo");
        $this->form_validation->set_rules("OptInSubscribeToEnabled");
        $this->form_validation->set_rules("OptInUnsubscribeFrom");
        $this->form_validation->set_rules("OptInUnsubscribeFromEnabled");
        $this->form_validation->set_rules("SubscriptionConfirmationPendingPageURL");
        $this->form_validation->set_rules("SubscriptionConfirmationPendingPageURLEnabled");
        $this->form_validation->set_rules("SubscriptionConfirmedPageURL");
        $this->form_validation->set_rules("SubscriptionConfirmedPageURLEnabled");
        $this->form_validation->set_rules("SendServiceIntegrationFailedNotification");
        $this->form_validation->set_rules("OptOutSubscribeTo");
        $this->form_validation->set_rules("OptOutSubscribeToEnabled");
        $this->form_validation->set_rules("OptOutUnsubscribeFrom");
        $this->form_validation->set_rules("OptOutUnsubscribeFromEnabled");
        $this->form_validation->set_rules("OptOutScope");
        $this->form_validation->set_rules("OptOutAddToSuppressionList");
        $this->form_validation->set_rules("UnsubscriptionConfirmedPageURL");
        $this->form_validation->set_rules("UnsubscriptionConfirmedPageURLEnabled");
        $this->form_validation->set_rules("SendActivityNotification");
        $this->form_validation->set_rules("Name");

        $this->form_validation->set_rules('ServiceURL', ApplicationHeader::$ArrayLanguageStrings['Screen']['1009'], 'required');
        $this->form_validation->set_rules('EventType');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'listintegration.addurl',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'url' => $this->input->post('ServiceURL'),
                        'event' => $this->input->post('EventType'),
                        'subscriberlistid' => $list_id
                    )
        ));

        return array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['1385']);
    }

    /**
     * Create list event
     *
     * @author Mert Hurturk
     * */
    function _event_create_list() {
        // Field validations - Start {
        $this->form_validation->set_rules("OptInMode");
        $this->form_validation->set_rules("HideInSubscriberArea");
        $this->form_validation->set_rules("Name", ApplicationHeader::$ArrayLanguageStrings['Screen']['0949'], 'required');
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create list - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.create',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistname' => $this->input->post('Name')
                    )
        ));

        if ($array_return['Success'] == false) {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            switch ($error_code) {
                case '2':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0950']);
                    break;
                case '3':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0951']);
                    break;
                default:
                    break;
            }
        } else {
            $list_id = $array_return['ListID'];
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'list.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'optinmode' => $this->input->post('OptInMode'),
                            'hideinsubscriberarea' => $this->input->post('HideInSubscriberArea') === false ? 'false' : 'true',
                            'subscriberlistid' => $list_id,
                            'optoutsubscribeto' => 0,
                            'optoutunsubscribefrom' => 0,
                            'optoutscope' => 'All lists',
                            'optoutaddtosuppressionlist' => 'No',
                            'optoutaddtoglobalsuppressionlist' => 'Yes',
                        )
            ));

            Subscribers::CreateCustomGeoColumns($list_id);

            $this->load->helper('url');
            if ($this->input->post('OptInMode') == 'Double') {
                redirect(InterfaceAppURL(true) . '/user/email/create/confirmation/' . $list_id, 'location', '302');
            } else {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0952']);
                redirect(InterfaceAppURL(true) . '/user/list/statistics/' . $list_id, 'location', '302');
            }
        }
        // Create list - End }
    }

}
