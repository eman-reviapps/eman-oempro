<?php

/**
 * Email templates controller
 *
 * @author Mert Hurturk
 */
class Controller_EmailTemplates extends MY_Controller {

    public $email_html;
    private static $SCREENSHOTDIR = 'email_templates';

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('templates');
        Core::LoadObject('api');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
        $this->email_html = <<<EOD
<!DOCTYPE html>
<html lang="en">

    <head>
    <meta name="viewport" content="width=device-width" initial-scale="1.0" user-scalable="yes" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>

body {
  margin: 0;
  padding: 0;
}

body, table, td, p, a, li {
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
}

a {
  word-wrap: break-word;
}

table td {
  border-collapse: collapse;
}

table {
  border-spacing: 0;
  border-collapse: collapse;
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

table, td {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

.ReadMsgBody {
	width:100%;
	background-color: #eeeeee;
}

.ExternalClass {
	width: 100%;
	background-color: #eeeeee;
}

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}

.ExternalClass * {
	line-height: 100%;
}

@media only screen and (max-width: 640px) {

  table[class="main"],td[class="main"] { width:100% !important; min-width: 200px !important; }

  table[class="logo-img"] { width:100% !important; float: none; margin-bottom: 15px;}
  table[class="logo-img"] td { text-align: center !important;}

  table[class="logo-title"] { width:100% !important; float: none;}
  table[class="logo-title"] td { text-align: center; height: auto}
  table[class="logo-title"] h1 { font-size: 24px !important; }
  table[class="logo-title"] h2 { font-size: 18px !important; }

  td[class="header-img"] img { width:100% !important; height:auto !important; }

  td[class="title"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="title"] h1 { font-size: 24px !important; }

  td.block-text { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="block-text"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="block-text"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="block-text"] li { font-size: 16px !important; line-height: 170% !important; }

  td[class="two-columns"] { padding-left: 25px !important; padding-right: 25px !important; }
  table[class="text-column"] { width:100% !important; float: none; margin-bottom: 15px;}

  td[class="image-caption"] { padding-left: 25px !important; padding-right: 25px !important; }
  table[class="image-caption-container"] { width:100% !important;}
  table[class="image-caption-column"] { width:100% !important; float: none;}
  td[class="image-caption-content"] img { width:100% !important; height:auto !important; }
  td[class="image-caption-content"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="image-caption-content"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="image-caption-top-gap"] { height: 15px !important; }
  td[class="image-caption-bottom-gap"] { height: 5px !important; }

  td[class="text"] { width:100% !important; }
  td[class="text"] p { font-size: 16px !important; line-height: 170% !important; }
  td[class="text"] h2 { font-size: 20px !important; line-height: 170% !important; }
  td[class="gap"] { display:none; }

  td[class="header"] { padding: 25px 25px 25px 25px !important; }
  td[class="header"] h1 { font-size: 24px !important; }
  td[class="header"] h2 { font-size: 20px !important; }

  td[class="footer"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="footer"] p { font-size: 13px !important; }
  table[class="footer-side"] { width: 100% !important; float: none !important; }
  td[class="footer-side"] { text-align: center !important; }
  td[class="social-links"] { text-align: center !important; }
  table[class="footer-social-icons"] { float: none !important; margin: 0px auto !important; }
  td[class="social-icon-link"] { padding: 0px 5px !important; }

  td[class="image"] img { width:100% !important; height:auto !important; }
  td[class="image"] { padding-left: 25px !important; padding-right: 25px !important; }

  td[class="image-full"] img { width:100% !important; height:auto !important; }
  td[class="image-full"] { padding-left: 0px !important; padding-right: 0px !important; }

  td[class="image-group"] img { width:100% !important; height:auto !important; margin: 15px 0px 15px 0px !important; }
  td[class="image-group"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="image-in-table"] { width:100% !important; float: none; margin-bottom: 15px;}
  table[class="image-in-table"] td { width:100% !important;}
  table[class="image-in-table"] img { width:100% !important; height:auto !important; }


  td[class="image-text"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="image-text"] p { font-size: 16px !important; line-height: 170% !important; }

  td[class="divider-simple"] { padding-left: 25px !important; padding-right: 25px !important; }

  td[class="divider-full"] { padding-left: 0px !important; padding-right: 0px !important; }

  td[class="social"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="preheader"] { display:none; }
  td[class="preheader-gap"] { display:none; }
  td[class="preheader-link"] { display:none; }
  td[class="preheader-text"] { width:100%; }

  td[class="buttons"] { padding-left: 25px !important; padding-right: 25px !important; }

  table[class="button"] { width:100% !important; float: none; }

  td[class="content-buttons"] { padding-left: 25px !important; padding-right: 25px !important; }
  td[class="buttons-full-width"] { padding-left: 0px !important; padding-right: 0px !important; }
  td[class="buttons-full-width"] a { width:100% !important; padding-left: 0px !important; padding-right: 0px !important; }
  td[class="buttons-full-width"] span { width:100% !important; padding-left: 0px !important; padding-right: 0px !important; }

  table[class="content"] { width:100% !important; float: none !important;}
  td[class="gallery-image"] { width:100% !important; padding: 0px !important;}

  table[class="social"] { width: 100%!important; text-align: center!important; }
  table[class="links"] { width: 100%!important; }
  table[class="links"] td { text-align: center!important; }
  table[class="footer-btn"] { text-align: center!important; width: 100%!important; margin-bottom: 10px; }
  table[class="footer-btn-wrap"] { margin-bottom: 0px; width: 100%!important; }

  td[class="head-social"]  { width: 100%!important; text-align: center!important; padding-top: 20px; }
  td[class="head-logo"]  { width: 100%!important; text-align: center!important; }
  tr[class="header-nav"] { display: none; }

}

</style>
</head>

<body>
    {body}
</body>
</html>
EOD;
    }

    /**
     * Index controller
     *
     * @return void
     * @author Mert Hurturk
     */
    function index() {

        // Privilege check - Start {
        if (Users::HasPermissions(array('EmailTemplates.Manage'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        print_r(InterfacePrivilegeCheck('List.Creates', $this->array_user_information));

        // Privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteEmailTemplates') {
            $array_event_return = $this->_event_delete_email_templates($this->input->post('SelectedEmailTemplates'));
        }
        // Events - End }
        // Retrieve email templates - Start {
        $email_templates = Templates::RetrieveTemplates(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'IsCreatedByAdmin' => 0));
        // Retrieve email templates - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
            'CurrentMenuItem' => 'Email Templates',
            'SubSection' => 'EmailTemplates',
            'UserInformation' => $this->array_user_information,
            'EmailTemplates' => $email_templates,
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/email_templates', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Email template create controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function create() {

        // Privilege check - Start {
        if (Users::HasPermissions(array('EmailTemplates.Manage'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        if (AccountIsUnTrustedCheck($this->array_user_information)) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['9277']);
            exit;
        }
        // Privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'ProceedToBuilder') {
            $array_event_return = $this->_event_proceed_to_builder();
        }
        // Events - End }
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'All');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
        $array_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0671'] => $array_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        // Retrieve personalization tags - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
            'CurrentMenuItem' => 'Settings',
            'UserInformation' => $this->array_user_information,
            'Tags' => $array_tags,
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        $this->render('user/email_template_create', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Email template update controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function edit($template_id) {

        // Events - Start {
        if ($this->input->post('Command') == 'ProceedToBuilder') {
            $ArrayEventReturn = $this->_event_proceed_to_builder();
        } elseif ($this->input->post('Command') == 'Save') {
            $ArrayEventReturn = $this->_event_proceed_to_builder(true);
        }
        // Events - End }
//        $template_id = (isset($template_id) && $template_id != '') ? $template_id : $_SESSION['template_builder']['TemplateID'];
        // Retrieve email template information - Start {
        if ((!isset($template_id) || $template_id == '') && (!isset($_SESSION['template_builder']))) {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/emailtemplates/builder', 'refresh');
        }

        if (isset($_SESSION['template_builder']) && $_SESSION['template_builder'] != '' && (!isset($template_id) || $template_id == '')) {
            $EmailTemplate = (object) $_SESSION['template_builder'];
        } else {
            Core::LoadObject('templates');
            $EmailTemplate = (object) Templates::RetrieveTemplate(array('*'), array('TemplateID' => $template_id));
        }
        // Retrieve email template information - End }
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'All');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
        $array_tags = array(
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0671'] => $array_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );

        // Retrieve personalization tags - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
            'CurrentMenuItem' => 'Settings',
            'TemplateInformation' => $EmailTemplate,
            'UserInformation' => $this->array_user_information,
            'Tags' => $array_tags
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('user/email_template_edit', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Email template preview controller
     *
     * @return void
     * @author Eman
     * */
    function preview($template_id) {
        
    }

    /**
     * Email template dragdrop buildercontroller
     *
     * @return void
     * @author Eman
     * */
    function dragdrop_builder() {

        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
            'TemplateInformation' => $_SESSION['template_builder'],
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('user/email_template_dragdrop_builder', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Email template dragdrop buildercontroller
     *
     * @return void
     * @author Eman
     * */
    function saveajax() {

        Core::LoadObject('aws_s3');

        $pure_html = $this->input->post('pure_html');
        $html = $this->input->post('html');
        $img_val = $this->input->post('img_val');

        $filteredData = substr($img_val, strpos($img_val, ",") + 1);
        $MediaData = base64_decode($filteredData);

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $MediaData, FILEINFO_MIME_TYPE);

        $ArrayInfo = array(
            "type" => $mime_type,
            "size" => getimagesizefromstring($MediaData),
            "tmp_name" => 'dragdrop_template_' . time() . '.png',
        );
        $image_uri = $this->uploadImageS3($MediaData, $ArrayInfo, S3_SCREENSHOTS_PATH);

        $html = ( get_magic_quotes_gpc() ) ? stripslashes($html) : $html;
        $email_html = $this->email_html;
        $email_html = str_replace('{body}', $html, $email_html);


        if (isset($_SESSION['template_builder']['ScreenshotImage']) && $_SESSION['template_builder']['ScreenshotImage'] != '') {
            $old_screenshot = $_SESSION['template_builder']['ScreenshotImage'];
            //check if old screenshot exists,then delete it
            if (isset($old_screenshot) && !empty($old_screenshot)) {
                $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
                $ObjectS3->deleteObject(S3_BUCKET, $old_screenshot);
            }
        }

        $_SESSION['template_builder']['TemplateHTMLContent'] = $email_html;
        $_SESSION['template_builder']['HTMLPure'] = $pure_html;
        $_SESSION['template_builder']['ScreenshotImage'] = $image_uri;

        if (isset($_SESSION['template_builder']['TemplateID']) && $_SESSION['template_builder']['TemplateID'] != '') {

            // Update email template - Start {
            $array_api_vars = array(
                'templateid' => $_SESSION['template_builder']['TemplateID'],
                'relowneruserid' => $_SESSION['template_builder']['RelOwnerUserID'],
                'templatename' => $_SESSION['template_builder']['TemplateName'],
                'templatedescription' => $_SESSION['template_builder']['TemplateDescription'],
                'templatesubject' => $_SESSION['template_builder']['TemplateSubject'],
                'templatehtmlcontent' => $_SESSION['template_builder']['TemplateHTMLContent'],
                'templateplaincontent' => $_SESSION['template_builder']['TemplatePlainContent'],
                'templatethumbnailpath' => $_SESSION['template_builder']['ScreenshotImage']
            );
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.template.update',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'access' => 'user',
                        'parameters' => $array_api_vars
            ));
        } else {
            // Create email template - Start {
            $array_api_vars = array(
                'relowneruserid' => $_SESSION['template_builder']['RelOwnerUserID'],
                'templatename' => $_SESSION['template_builder']['TemplateName'],
                'templatedescription' => $_SESSION['template_builder']['TemplateDescription'],
                'templatesubject' => $_SESSION['template_builder']['TemplateSubject'],
                'templatehtmlcontent' => $_SESSION['template_builder']['TemplateHTMLContent'],
                'templateplaincontent' => $_SESSION['template_builder']['TemplatePlainContent'],
                'templatethumbnailpath' => $_SESSION['template_builder']['ScreenshotImage']
            );
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'email.template.create',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'access' => 'user',
                        'parameters' => $array_api_vars
            ));
            $_SESSION['template_builder']['TemplateID'] = $array_return['TemplateID'];
        }

        if ($array_return['Success'] == false) {
            // Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            $_SESSION['TemplateDragDropBuilderMessage'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Email.Template.Update', $error_code));
            print false;
            exit;
        } else {

            $_SESSION['TemplateDragDropBuilderMessage'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0345']);

            $ArrayCriterias = array('TemplateID' => $_SESSION['template_builder']['TemplateID']);
            $ArrayTemplateInformation = array('ScreenshotImage' => $image_uri, 'HTMLPure' => $pure_html);
            Templates::Update($ArrayTemplateInformation, $ArrayCriterias);
            print $html;
            exit;
        }
    }

    function uploadImageS3($MediaData, $ArrayInfo, $PATH) {

        Core::LoadObject('aws_s3');
        $uri = $PATH . '/' . ($this->array_user_information['UserID']) . '/' . uniqid();

        $ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
        $result = $ObjectS3->putObjectString($MediaData, S3_BUCKET, $uri, S3::ACL_PUBLIC_READ, array(), $ArrayInfo['type']);
        if ($result) {
            return $uri;
        }
        return "";
    }

    /**
     * Setups session data with email template information and redirects to builder function
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_proceed_to_builder($save = false) {
        // Field validations - Start {
        $array_form_rules = array(
            array
                (
                'field' => 'TemplateName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0051'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'TemplateDescription',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0170'],
                'rules' => '',
            ),
            array
                (
                'field' => 'RelOwnerUserID',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0173'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'TemplateSubject',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0106'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'TemplateHTMLContent',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0175'],
                'rules' => '',
            ),
            array
                (
                'field' => 'TemplatePlainContent',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0176'],
                'rules' => '',
            ),
        );

        $this->form_validation->set_rules($array_form_rules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Pass post data to session - Start {
        unset($_POST['Command']);

        $_SESSION['template_builder'] = array();

        foreach ($_POST as $key => $value) {
            $_SESSION['template_builder'][$key] = $value;
        }


//        print_r($_SESSION['template_builder']);
//        exit();
        // Pass post data to session - End }
        //Eman
        // Events - End }
        // Retrieve personalization tags - Start {
        Core::LoadObject('personalization');
//        $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
        $array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'All');
        $array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
        $array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
        $array_tags = array(
//            ApplicationHeader::$ArrayLanguageStrings['Screen']['0670'] => $array_subscriber_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0671'] => $array_link_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0672'] => $array_user_tags,
            ApplicationHeader::$ArrayLanguageStrings['Screen']['0673'] => $array_other_tags
        );
        // Retrieve personalization tags - End }

        $_SESSION['template_builder']['ContentTags'] = $array_tags;

        //Eman

        if ($save) {
            $this->_event_save_email_template();
        }

        // Redirect to email builder function - Start {
        $this->load->helper('url');
//        redirect(InterfaceAppURL(true) . '/user/emailtemplates/builder', 'refresh');
        // Redirect to email builder function - End }
        //Eman
        //Redirect to dragdrop builer - Start {
        redirect(InterfaceAppURL(true) . '/user/emailtemplates/dragdrop_builder', 'refresh');
        //Redirect to dragdrop builer - End {
    }

    /**
     * Saves email tamplate data to database
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_save_email_template() {
        $_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);

        // Update email template - Start {
        $array_api_vars = array(
            'templateid' => $_SESSION['template_builder']['TemplateID'],
            'relowneruserid' => $_SESSION['template_builder']['RelOwnerUserID'],
            'templatename' => $_SESSION['template_builder']['TemplateName'],
            'templatedescription' => $_SESSION['template_builder']['TemplateDescription'],
            'templatesubject' => $_SESSION['template_builder']['TemplateSubject'],
            'templatehtmlcontent' => $_SESSION['template_builder']['TemplateHTMLContent'],
            'templateplaincontent' => $_SESSION['template_builder']['TemplatePlainContent'],
            'templatethumbnailpath' => $_SESSION['template_builder']['TemplateThumbnailFileName']
        );
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.template.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'access' => 'user',
                    'parameters' => $array_api_vars
        ));
        // Update email template - End }

        if ($array_return['Success'] == false) {
            // Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            $_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Email.Template.Update', $error_code));
        } else {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0345']);
        }
        unset($_SESSION['template_builder']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/emailtemplates/', 'refresh');
    }

    /**
     * Create email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_create_email_template() {
        $_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);

        // Create email template - Start {
        $array_api_vars = array(
            'relowneruserid' => $_SESSION['template_builder']['RelOwnerUserID'],
            'templatename' => $_SESSION['template_builder']['TemplateName'],
            'templatedescription' => $_SESSION['template_builder']['TemplateDescription'],
            'templatesubject' => $_SESSION['template_builder']['TemplateSubject'],
            'templatehtmlcontent' => $_SESSION['template_builder']['TemplateHTMLContent'],
            'templateplaincontent' => $_SESSION['template_builder']['TemplatePlainContent'],
            'templatethumbnailpath' => $_SESSION['template_builder']['TemplateThumbnailFileName']
        );
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.template.create',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'access' => 'user',
                    'parameters' => $array_api_vars
        ));
        // Create email template - End }

        if ($array_return['Success'] == false) {
            // Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            $_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Email.Template.Create', $error_code));
        } else {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0344']);
        }
        unset($_SESSION['template_builder']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/emailtemplates/', 'refresh');
    }

    /**
     * Test drives email template
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _event_test_drive_email_template() {
        $_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);
        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/emailtemplates/testdrive');
    }

    /**
     * Delete email templates event
     *
     * @return void
     * @author Mert Hurturk
     */
    function _event_delete_email_templates($array_email_template_ids, $perform_form_validation = true) {
        if ($perform_form_validation == true) {
            // Field validations - Start {
            $ArrayFormRules = array(
                array
                    (
                    'field' => 'SelectedEmailTemplates[]',
                    'label' => 'emailtemplates',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_rules($ArrayFormRules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0346']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }
        Core::LoadObject('templates');
        $Templates = array();
        foreach ($array_email_template_ids as $EachID) {
            $EmailTemplate = (object) Templates::RetrieveTemplate(array('ScreenshotImage'), array('TemplateID' => $EachID));
            $Templates[] = $EmailTemplate;
        }

        // Delete email templates - Start {
        $array_api_vars = array(
            'templates' => implode(',', $array_email_template_ids),
        );
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'email.template.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'access' => 'user',
                    'parameters' => $array_api_vars
        ));

        if ($array_return['Success'] == false) {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_Return['ErrorCode'][0] : $array_return['ErrorCode']);
            switch ($error_code) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0346']);
                    break;
                default:
                    break;
            }
        } else {
            foreach ($Templates as $Template) {
                $screenshot = $Template->ScreenshotImage;
                $this->delete_image($screenshot);
            }

            // API Success
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0347']);
            return true;
        }
        // Delete email templates - End }
    }

    function select() {
        $group = $_POST['group'];
        $path = $_POST['path'];
        $directory = TEMPLATE_PATH . 'dragdrop/elements/' . $group . "/";
        $scanned_files = array_diff(scandir($directory), array('..', '.'));

        $buffer = '';

        $ContentTags = $_SESSION['template_builder']['ContentTags'];

        $personalize_html;
        foreach ($scanned_files as $n => $file) {
            if (file_exists($directory . $file) && strstr($file, '.html')) {
                $buffer .= file_get_contents($directory . $file);
                $buffer = str_replace("{PATH}", TEMPLATE_URL, $buffer);
                $buffer = str_replace('src="elements/', 'src="' . TEMPLATE_URL . "dragdrop/elements/", $buffer);
            }
        }
        if ($group == 'personalize') {
            if (file_exists($directory . $group . '.php') && strstr($file, '.php')) {

                foreach ($ContentTags as $Label => $TagGroup) {
                    foreach ($TagGroup as $Tag => $Label) {
                        $buffer .= file_get_contents($directory . $file);
//                        $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px">' . $Tag . '</div>';
                        if (strstr($Label, 'Link')) {
                            $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px"><a href="' . $Tag . '" >' . $Label . '</a></div>';
                        } else {
                            $tag_html = '<div style="margin:0px 0px 10px 0px;line-height:22px">' . $Tag . '</div>';
                        }
                        $label_html = '<div class="preview">' . $Label . '</div>';

                        $buffer = str_replace('<div style="margin:0px 0px 10px 0px;line-height:22px"></div>', $tag_html, $buffer);
                        $buffer = str_replace('<div class="preview"></div>', $label_html, $buffer);
                    }
                }
            }
        }
//else if (file_exists($directory . $file) && strstr($file, '.php')) {
//                $buffer.= file_get_contents($directory . $file);
//                $buffer = str_replace("{PATH}", TEMPLATE_URL, $buffer);
//                $buffer = str_replace('src="elements/', 'src="' . TEMPLATE_URL . "dragdrop/elements/", $buffer);
//                $buffer = str_replace('<tr></tr>', $personalize_html, $buffer);
//            }

        print $buffer;
    }

    function saveDropImg() {
        $raw_data = $_POST['data'];
        $nomefile = $_POST['filename'];
        $uploaddir = DATA_PATH . "dragdrop/images/";
        $url = "images/";

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $raw_data));

        file_put_contents(
                $uploaddir . $nomefile, $data
        );

        $response = array('percorso' => $url . $nomefile);
        print(json_encode($response));
        exit();
    }

    function videoToImg() {
        $youtubevideo = $_REQUEST['youtubevideo'];
        $youtube = str_replace("https://www.youtube.com/watch?v=", "", $youtubevideo);
        $youtube = str_replace("https://www.youtube.com/watch?v=/", "", $youtube);
        $youtube = str_replace("https://youtu.be/", "", $youtube);
        $img = "http://img.youtube.com/vi/" . $youtube . "/0.jpg";

        $MediaData = file_get_contents($img);
        $image = base64_encode($MediaData);

        print_r($img);
        exit();
    }

    function grab_image($url, $saveto) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }

    function delete_image($image) {

        if (isset($image) && !empty($image)) {
            $image_path = DATA_PATH . self::$SCREENSHOTDIR . "/" . $image;
            if (file_exists($image_path)) {
                unlink($image_path);
            }
        }
    }

}

// end of class User
?>