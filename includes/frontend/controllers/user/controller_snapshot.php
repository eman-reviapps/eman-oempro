<?php
/**
 * Snapshot controller
 *
 * @author Mert Hurturk
 */

class Controller_Snapshot extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('statistics');
		Core::LoadObject('subscribers');
		Core::LoadObject('campaigns');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve user information - Start {
		$this->arrayUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']), true);
		// Retrieve user information - End }

		// Check if user account has expired - Start {
		if (Users::IsAccountExpired($this->arrayUserInformation) == true)
			{
			$_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/user/logout/', 'location', '302');
			}
		// Check if user account has expired - End }
	}

	/**
	 * Create controller
	 *
	 * @author Mert Hurturk
	 **/
	function index()
	{
		$Result = API::call(array(
			'format'	=>	'object',
			'command'	=>	'user.snapshot',
			'protected'	=>	true,
			'access'	=> 'user',
			'username'	=>	$this->arrayUserInformation['Username'],
			'password'	=>	$this->arrayUserInformation['Password'],
			'parameters'=>	array(
				'month'		=> date('Y-m')
				)
		));

		$array_view_data = array(
							'UserInformation' => $this->arrayUserInformation,
							'TotalLists' => $Result->Lists,
							'TotalSubscribers' => $Result->Subscribers,
							'EmailsSentInPeriod' => $Result->EmailsSent,
							'CampaignsSentInPeriod' => $Result->CampaignsSent
							);

		$this->render('user/snapshot', $array_view_data);
	}

	/**
	 * Draws emails sent sparkline
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	function emails_sent()
	{
		$log = Statistics::RetrieveEmailSendingAmountFromActivityLog($this->arrayUserInformation['UserID'], date('Y-m-d', strtotime(date('Y-m-d').' -90 days')), date('Y-m-d'), false, true);
		$logOrderedByDate = array();
		for ($TMPCounter = 90; $TMPCounter >= 0; $TMPCounter--)
			{
			if (! isset($log[date('Y-m-d', strtotime(date('Y-m-d').' -'.$TMPCounter.' days'))]))
				{
				$logOrderedByDate[date('Y-m-d', strtotime(date('Y-m-d').' -'.$TMPCounter.' days'))] = array('TotalSentEmail'=>0);
				}
			else
				{
				$logOrderedByDate[date('Y-m-d', strtotime(date('Y-m-d').' -'.$TMPCounter.' days'))] = array('TotalSentEmail'=>$log[date('Y-m-d', strtotime(date('Y-m-d').' -'.$TMPCounter.' days'))]['TotalSentEmail']);
				}
			}

		$SparklineData = array();

		foreach ($logOrderedByDate as $Each)
			{
			$SparklineData[] = $Each['TotalSentEmail'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData), 234);
	}
}