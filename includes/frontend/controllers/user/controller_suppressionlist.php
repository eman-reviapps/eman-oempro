<?php

/**
 * Suppression list controller
 * @author Mert Hurturk
 * */
class Controller_Suppressionlist extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('suppression_list');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Global suppression list controller
     * @author Mert Hurturk
     * */
    function globallist($StartFrom = 1, $RPP = 25) {
        // Events - Start {
        if ($this->input->post('Command') == 'Delete') {
            $this->_event_delete();
        }
        // Events - End }

        $array_suppressed_emails = SuppressionList::GetEmailAddresses(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => 0), $RPP, ($StartFrom <= 0 ? 0 : ($StartFrom - 1) * $RPP));
        $total_global_suppression = SuppressionList::GetEmailAddresses(array('COUNT(*) AS Total'), array('RelOwnerUserID' => $this->array_user_information['UserID'], 'RelListID' => 0));
        $total_global_suppression = $total_global_suppression[0]['Total'];

        $total_pages = ceil($total_global_suppression / $RPP);

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserGlobalSuppressionList'],
            'UserInformation' => $this->array_user_information,
            'Emails' => $array_suppressed_emails,
            'CurrentMenuItem' => 'Lists',
            'TotalPages' => $total_pages,
            'CurrentPage' => $StartFrom,
            'RPP' => $RPP,
        );

        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }

        $this->render('user/global_suppression_list', $array_view_data);
    }

    /**
     * Move to global suppression list controller
     * @author Eman
     * */
    function move_to_global_supprression() {

        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];

        Core::LoadObject('suppression_list');

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();

            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = SuppressionList::GetEmailAddresses(array('*'), array(
                            'RelOwnerUserID' => $this->array_user_information['UserID'],
                            'SuppressionID' => $EachSuscriberID,
                            'RelListID' => $ListID
                                ), 5, 0);
                $ArrSubscribers[] = $Subscriber[0];
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }


            //mfrod a5ly l listID by zero
            if (is_array($ArrSubscribers)) {

                foreach ($ArrSubscribers as $Subscriber) {

                    $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $this->array_user_information['UserID'],
                                'EmailAddress' => $Subscriber['EmailAddress'],
                                'RelListID' => 0
                                    ), 5, 0);

                    if ($global_suppress) {
                        //if already exists in global suppression list. then delete it
                        SuppressionList::DeleteEmail($Subscriber['EmailAddress'], $ListID, $this->array_user_information['UserID']);
                    } else {
                        // if not exists. then update list_id to 0
                        $ArrayFieldAndValues = array(
                            'RelListID' => 0,
                        );
                        $ArrayCriterias = array(
                            'RelOwnerUserID' => $this->array_user_information['UserID'],
                            'EmailAddress' => $Subscriber['EmailAddress'],
                            'RelListID' => $ListID
                        );

                        SuppressionList::Update($ArrayFieldAndValues, $ArrayCriterias);
                    }
                }
            }
            print_r((json_encode(array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['9245']))));
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    /**
     * Move back to list controller
     * @author Eman
     * */
    function move_back_to_list() {

        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];
        $List = Lists::RetrieveList(array('*'), array('ListID' => $ListID));

        Core::LoadObject('suppression_list');

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();

            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = SuppressionList::GetEmailAddresses(array('*'), array(
                            'RelOwnerUserID' => $this->array_user_information['UserID'],
                            'SuppressionID' => $EachSuscriberID,
                            'RelListID' => $ListID
                                ), 5, 0);
                $ArrSubscribers[] = $Subscriber[0];
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }


            //mfrod a5ly l listID by zero
            if (is_array($ArrSubscribers)) {
                $TotalFailed = 0;
                $TotalMoved = 0;

                foreach ($ArrSubscribers as $Subscriber) {

                    $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $this->array_user_information['UserID'],
                                'EmailAddress' => $Subscriber['EmailAddress'],
                                'RelListID' => 0
                                    ), 5, 0);

                    if ($Subscriber['PrevSubscriptionStatus'] == 'Unsubscribed' || $global_suppress) {
                        //can't be moved back to list. it is unsubscribed from this list .
                        // or it does exists on global list and can't be moved to any list or moved back
                        $TotalFailed++;
                    } else {
                        //delete it from suppression
                        SuppressionList::DeleteEmail($Subscriber['EmailAddress'], $ListID, $this->array_user_information['UserID']);

                        // add subscriber to list
                        $Ret = Subscribers::AddSubscriber_Enhanced(array(
                                    'UserInformation' => $this->array_user_information,
                                    'ListInformation' => $List,
                                    'EmailAddress' => $Subscriber['EmailAddress'],
                                    'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Move',
                                    'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                                    'UpdateIfDuplicate' => true,
                                    'UpdateIfUnsubscribed' => true,
                                    'ApplyBehaviors' => true,
                                    'SendConfirmationEmail' => false,
                                    'UpdateStatistics' => true,
                                    'TriggerWebServices' => false,
                                    'TriggerAutoResponders' => false
                        ));
                        $ArrayFieldAndValues = array(
                            'SubscriptionStatus' => $Subscriber['PrevSubscriptionStatus'],
                            'BounceType' => $Subscriber['PrevBounceType'],
                        );
                        if ($Ret[0]) {
                            Subscribers::Update($List['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $Ret[1]));
                        }
                        $TotalMoved++;
                    }
                }
                if ($TotalMoved > 0) {
                    $msg = $TotalMoved . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9262'] . " , " . $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9268'];
                    print_r((json_encode(array(true, $msg))));
                } else {
                    $msg = $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9268'];
                    print_r((json_encode(array(false, $msg))));
                }
            }
//            print_r((json_encode(array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['9245']))));
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    /**
     * Move back to list controller
     * @author Eman
     * */
    function move_to_another_list() {

        if ($_POST['SelectedSubscribersIDs'] != false) {
            $SelectedSubscribers = explode(',', $_POST['SelectedSubscribersIDs']);
        } else {
            $SelectedSubscribers = array();
        }
        $ListID = $_POST['listid'];
        $List = Lists::RetrieveList(array('*'), array('ListID' => $ListID));

        $ToListID = $_POST['to_listid'];
        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));

        Core::LoadObject('suppression_list');

        if (count($SelectedSubscribers) > 0) {

            $ArrSubscribers = array();
            $ArrSubscribersEmails = array();

            foreach ($SelectedSubscribers as $EachSuscriberID) {
                $Subscriber = SuppressionList::GetEmailAddresses(array('*'), array(
                            'RelOwnerUserID' => $this->array_user_information['UserID'],
                            'SuppressionID' => $EachSuscriberID,
                            'RelListID' => $ListID
                                ), 5, 0);
                $ArrSubscribers[] = $Subscriber[0];
                $ArrSubscribersEmails[] = $Subscriber['EmailAddress'];
            }


            //mfrod a5ly l listID by zero
            if (is_array($ArrSubscribers)) {
                $TotalFailed = 0;
                $TotalMoved = 0;

                foreach ($ArrSubscribers as $Subscriber) {

                    $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $this->array_user_information['UserID'],
                                'EmailAddress' => $Subscriber['EmailAddress'],
                                'RelListID' => 0
                                    ), 5, 0);

                    if ($Subscriber['PrevSubscriptionStatus'] == 'Unsubscribed' || $global_suppress) {
                        //can't be moved to other list.
                        $TotalFailed++;
                    } else {

                        $result = $this->move_subscriber_from_suppression($Subscriber, $ListID, $ToListID, $this->array_user_information);
                        if ($result) {
                            $TotalMoved++;
                        } else {
                            $TotalFailed++;
                        }
                    }
                }
                if ($TotalMoved > 0) {
                    $msg = $TotalMoved . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9262'] . " , " . $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9270'];
                    print_r((json_encode(array(true, $msg))));
                } else {
                    $msg = $TotalFailed . " " . ApplicationHeader::$ArrayLanguageStrings['Screen']['9270'];
                    print_r((json_encode(array(false, $msg))));
                }
            }
//            print_r((json_encode(array(true, ApplicationHeader::$ArrayLanguageStrings['Screen']['9245']))));
        } else {
            print_r((json_encode(array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['9244']))));
        }
        exit();
    }

    private function move_subscriber_from_suppression($Subscriber, $ListID, $ToListID, $User) {

        $ToList = Lists::RetrieveList(array('*'), array('ListID' => $ToListID));

        //wont be added to this list
        $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'PrevSubscriptionStatus' => 'Unsubscribed',
                    'RelListID' => $ToListID
                        ), 5, 0);
        // can be added 3ady
        $list_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                    'RelOwnerUserID' => $User['UserID'],
                    'EmailAddress' => $Subscriber['EmailAddress'],
                    'RelListID' => $ToListID
                        ), 5, 0);

        if ($list_suppress_unsubscribe) {
            //cant be moved to this list
            return false;
        } else {
            if ($list_suppress) {
                //remove it from destination suppression source
                SuppressionList::DeleteEmail($Subscriber['EmailAddress'], $ToListID, $User['UserID']);
            }
            //delete it from current list suppression
            SuppressionList::DeleteEmail($Subscriber['EmailAddress'], $ListID, $User['UserID']);

            //add it to destination list
            $Ret = Subscribers::AddSubscriber_Enhanced(array(
                        'UserInformation' => $User,
                        'ListInformation' => $ToList,
                        'EmailAddress' => $Subscriber['EmailAddress'],
                        'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Move',
                        'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                        'UpdateIfDuplicate' => true,
                        'UpdateIfUnsubscribed' => true,
                        'ApplyBehaviors' => true,
                        'SendConfirmationEmail' => false,
                        'UpdateStatistics' => true,
                        'TriggerWebServices' => false,
                        'TriggerAutoResponders' => false
            ));
            return true;
        }
    }

    function search() {
        $ArrayAPIData = $_POST;

        $array_suppressed_emails = SuppressionList::GetSuppressionList(array('*'), array(
                    'RelOwnerUserID' => $this->array_user_information['UserID'],
                    'RelListID' => $ArrayAPIData['listid']
                        )
        );
        $Lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);

        $this->render('user/list_suppression_filter', array(
            'Emails' => $array_suppressed_emails,
            'Lists' => $Lists,
            'ListID' => $ArrayAPIData['listid'],
        ));
    }

    function browse($list_id = 0) {


        $Lists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $this->array_user_information['UserID']), array('Name' => 'ASC'), array(), 0, 0, FALSE);
        $ListInformation = Lists::RetrieveList(array('*'), array('ListID' => $list_id));

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserGlobalSuppressionList'],
            'UserInformation' => $this->array_user_information,
//            'Emails' => $array_suppressed_emails,
            'ListInformation' => $ListInformation,
            'Lists' => $Lists,
            'ListID' => $list_id,
            'CurrentMenuItem' => 'Lists',
        );

        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }

        $this->render('user/list_suppression_browse', $array_view_data);
    }

    /**
     * Deletes suppressed emails
     * @author Mert Hurturk
     * */
    function _event_delete() {

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'subscribers.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscribers' => implode(',', $this->input->post('SelectedEmails')),
                        'suppressed' => 'true',
                        'subscriberlistid' => 0
                    )
        ));

        if ($array_return['Success'] != false) {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1032']);
            return true;
        }
    }

}

?>