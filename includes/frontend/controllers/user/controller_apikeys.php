<?php

/**
 * Account controller
 *
 * @author Mert Hurturk
 */
class Controller_APIKeys extends MY_Controller {

    /**
     * Constructor
     *
     * @author Mert Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('api');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Mert Hurturk
     */
    function index() {
        // Privilege check - Start {
        if (Users::HasPermissions(array('User.Update'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            header('Location: ' . InterfaceAppURL(true) . '/user/overview/');
            exit;
        }
        // Privilege check - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteAPIKeys') {
            $array_event_return = $this->_event_delete_api_key();
        }
        // Events - End }

        $APIKeyMapper = O_Registry::instance()->getMapper('APIKey');
        $APIKeys = $APIKeyMapper->findByUser($this->array_user_information['UserID']);
        $APIKeys = $APIKeys == false ? array() : $APIKeys;

        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserAPIKeys'],
            'CurrentMenuItem' => 'Settings',
            'SubSection' => 'APIKeys',
            'UserInformation' => $this->array_user_information,
            'APIKeys' => $APIKeys
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/settings', $array_view_data);
        // Interface parsing - End }
    }

    function create() {
        // Events - Start {
        if ($this->input->post('Command') == 'CreateAPIKey') {
            $array_event_return = $this->_event_create_apikey();
        }
        // Events - End }
        // Interface parsing - Start {
        
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateAPIKey'],
            'CurrentMenuItem' => 'Settings',
            'ActiveSettingsItem' => 'APIKeys',
            'UserInformation' => $this->array_user_information
        );
        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        foreach ($array_event_return as $key => $value) {
            $array_view_data[$key] = $value;
        }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/apikey_create', $array_view_data);
        // Interface parsing - End }
    }

    function _event_create_apikey() {
        // Field validations - Start {
        $this->form_validation->set_rules("Note", ApplicationHeader::$ArrayLanguageStrings['Screen']['1946'], 'required');
        $this->form_validation->set_rules("IPAddress");
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }

        $APIKeyMapper = O_Registry::instance()->getMapper('APIKey');
        $APIKey = new O_Domain_APIKey();

        $GeneratedAPIKey = md5($this->array_user_information['UserID'] + rand(1000, 5000) + 5000 + (time() + 1000) . $this->array_user_information['UserID']);
        $GeneratedAPIKey = implode('-', str_split($GeneratedAPIKey, 4));
        $APIKey->setAPIKey($GeneratedAPIKey);
        $APIKey->setUserId($this->array_user_information['UserID']);
        $APIKey->setNote($this->input->post('Note'));
        $APIKey->setIPAddress($this->input->post('IPAddress'));

        try {
            $APIKeyMapper->insert($APIKey);
        } catch (Exception $e) {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/apikeys/create');
            exit;
        }

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1945']);

        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/user/apikeys/');
    }

    function _event_delete_api_key() {
        if (!isset($_POST['SelectedKeys']) || !is_array($_POST['SelectedKeys']) || count($_POST['SelectedKeys']) < 1)
            return;

        $APIKeyMapper = O_Registry::instance()->getMapper('APIKey');
        foreach ($_POST['SelectedKeys'] as $Value) {
            $APIKeyMapper->deleteByKeyIdAndUserId($Value, $this->array_user_information['UserID']);
        }

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1948']);
    }

}

?>