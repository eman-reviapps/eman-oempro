<?php

/**
 * Autoresponders Controller
 */
class Controller_Autoresponders extends MY_Controller {

    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('user_auth');
        Core::LoadObject('users');
        Core::LoadObject('api');
        Core::LoadObject('auto_responders');
        Core::LoadObject('lists');
        Core::LoadObject('campaigns');
        // Load other modules - End	
        // Check the login session, redirect based on the login session status - Start
        UserAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/user/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve user information - Start {
        $this->array_user_information = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)' => $_SESSION[SESSION_NAME]['UserLogin']), true);
        // Retrieve user information - End }
        // Check if user account has expired - Start {
        if (Users::IsAccountExpired($this->array_user_information) == true) {
            $_SESSION['PageMessageCache'] = array('Error', ApplicationHeader::$ArrayLanguageStrings['Screen']['1617']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/logout/', 'location', '302');
        }
        // Check if user account has expired - End }
    }

    /**
     * Browse auto responders function
     * @author Mert Hurturk
     * */
    function browse($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('AutoResponders.Get'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }

        $is_create_event = false;

        // Events - Start {
        if ($this->input->post('Command') == 'DeleteAutoResponders') {
            if (Users::HasPermissions(array('AutoResponders.Delete'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_delete_autoresponders($list_id);
            }
        }
        // Events - End }
        // Retrieve auto responders - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'autoresponders.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'AutoResponderName',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            // Error occurred
        } else {
            $array_autoresponders = $array_return['AutoResponders'];
            $total_autoresponder_count = $array_return['TotalAutoResponders'] == NULL ? 0 : $array_return['TotalAutoResponders'];
            unset($array_return);
        }
        // Retrieve auto responders - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }

        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseAutoResponders'],
            'UserInformation' => $this->array_user_information,
            'AutoResponders' => $array_autoresponders,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'SubSection' => 'AutoResponders',
            'IsCreateEvent' => $is_create_event,
            'ListID' => $list_id
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

        if (isset($array_event_return)) {
            foreach ($array_event_return as $key => $value) {
                $array_view_data[$key] = $value;
            }
        }

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($array_event_return) == true) {
            $array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
        }

        if (isset($array_create_event_return) == true) {
            $array_view_data[($array_create_event_return[0] == false ? 'PageCreateErrorMessage' : 'PageCreateSuccessMessage')] = $array_create_event_return[1];
        }

        $this->render('user/auto_responders', $array_view_data);
    }

    /**
     * Create auto responder function
     * @author Mert Hurturk
     * */
    function create($list_id) {
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        $array_event_data = array();
        if ($this->input->post('Command') == 'Create') {
            if (Users::HasPermissions(array('AutoResponder.Create'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_create_autoresponder($list_id);
                if (isset($array_event_return) == true) {
                    $array_event_data[($array_event_return[0] == false ? 'PageEditErrorMessage' : 'PageEditSuccessMessage')] = $array_event_return[1];
                }
            }
        }
        // Events - End }
        // Retrieve campaigns - Start {
        $array_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Criteria' => array(array('Column' => 'RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID'])),
                    'Content' => false,
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC')
        ));
        // Retrieve campaigns - End }
        // Retrieve fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));

        $array_fields = $array_return['CustomFields'];
        if (count($array_fields) > 0) {
            $tmp_array_fields = array();
            $tmp_array_fields[] = array('CustomFieldID' => 'SubscriptionDate', 'FieldName' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0665']['SubscriptionDate']);
            foreach ($array_fields as $each) {
                if ($each['FieldType'] != 'Date field')
                    continue;
                $tmp_array_fields[] = $each;
            }
            $array_fields = $tmp_array_fields;
            unset($tmp_array_fields);
        } else {
            $array_fields = array();
        }
        // Retrieve fields - End }
        // Parse interface - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCreateAutoResponder'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'AutoResponder' => array(),
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'AutoResponders',
            'Campaigns' => $array_campaigns,
            'Fields' => $array_fields,
            'IsEditEvent' => false,
            'ListID' => $list_id
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        $array_view_data = array_merge($array_view_data, $array_event_data);

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageEditSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageEditNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/auto_responder_create', $array_view_data);
        // Parse interface - End }
    }

    /**
     * Edit auto responder function
     * @author Mert Hurturk
     * */
    function edit($list_id, $auto_responder_id) {
        // Get auto responder information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'autoresponder.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'autoresponderid' => $auto_responder_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['1463']);
            return;
        }

        $auto_responder = $array_return['AutoResponder'];
        // Get auto responder information - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        $array_event_data = array();
        if ($this->input->post('Command') == 'Edit') {
            if (Users::HasPermissions(array('AutoResponder.Update'), $this->array_user_information['GroupInformation']['Permissions'])) {
                $array_event_return = $this->_event_edit_autoresponder($list_id, $auto_responder_id);
                if (isset($array_event_return) == true) {
                    $array_event_data[($array_event_return[0] == false ? 'PageEditErrorMessage' : 'PageEditSuccessMessage')] = $array_event_return[1];
                }
            }
        }
        // Events - End }
        // Retrieve campaigns - Start {
        $array_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
                    'Criteria' => array(array('Column' => 'RelOwnerUserID', 'Operator' => '=', 'Value' => $this->array_user_information['UserID'])),
                    'Content' => false,
                    'RowOrder' => array('Column' => 'CampaignName', 'Type' => 'ASC')
        ));
        // Retrieve campaigns - End }
        // Retrieve fields - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'customfields.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'orderfield' => 'FieldName',
                        'ordertype' => 'ASC'
                    )
        ));

        $array_fields = $array_return['CustomFields'];
        if (count($array_fields) > 0) {
            $tmp_array_fields = array();
            $tmp_array_fields[] = array('CustomFieldID' => 'SubscriptionDate', 'FieldName' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0665']['SubscriptionDate']);
            foreach ($array_fields as $each) {
                if ($each['FieldType'] != 'Date field')
                    continue;
                $tmp_array_fields[] = $each;
            }
            $array_fields = $tmp_array_fields;
            unset($tmp_array_fields);
        } else {
            $array_fields = array();
        }
        // Retrieve fields - End }
        // Parse interface - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseAutoResponders'],
            'UserInformation' => $this->array_user_information,
            'AutoResponder' => $auto_responder,
            'ListInformation' => $array_list_information,
            'CurrentMenuItem' => 'Lists',
            'ActiveListItem' => 'AutoResponders',
            'IsEditEvent' => true,
            'ListID' => $list_id,
            'Campaigns' => $array_campaigns,
            'Fields' => $array_fields
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());
        $array_view_data = array_merge($array_view_data, $array_event_data);

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageEditSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageEditNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('user/auto_responder_create', $array_view_data);
        // Parse interface - End }
    }

    /**
     * Copy auto responders function
     * @author Mert Hurturk
     * */
    function copy($list_id) {
        // User privilege check - Start {
        if (Users::HasPermissions(array('AutoResponder.Create'), $this->array_user_information['GroupInformation']['Permissions']) == false) {
            show_error(ApplicationHeader::$ArrayLanguageStrings['Screen']['1307']);
            exit;
        }
        // User privilege check - End }
        // Retrieve list information - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'list.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'listid' => $list_id
                    )
        ));

        if ($array_return['Success'] == false) {
            $this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
            return;
        }

        $array_list_information = $array_return['List'];
        // Retrieve list information - End }
        // Events - Start {
        if ($this->input->post('Command') == 'Copy') {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'autoresponders.copy',
                        'protected' => true,
                        'username' => $this->array_user_information['Username'],
                        'password' => $this->array_user_information['Password'],
                        'parameters' => array(
                            'sourcelistid' => $this->input->post('SourceListID'),
                            'targetlistid' => $array_list_information['ListID']
                        )
            ));

            $this->load->helper('url');
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1155']);
            redirect(InterfaceAppURL(true) . '/user/autoresponders/browse/' . $list_id, 'location', '302');
        }
        // Events - End }
        // Retrieve lists - Start {
        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'lists.get',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'orderfield' => 'Name',
                        'ordertype' => 'ASC'
                    )
        ));
        if ($array_return['Success'] == false) {
            $array_lists = array();
        } else {
            $array_lists = $array_return['Lists'];
            unset($array_return);
        }

        // Remove self from lists array - Start {
        if (count($array_lists) > 0) {
            foreach ($array_lists as $key => $value) {
                if ($array_lists[$key]['ListID'] == $array_list_information['ListID'])
                    unset($array_lists[$key]);
            }
        }
        // Remove self from lists array - End }
        // Retrieve lists - End }
        // Interface parsing - Start {
        $array_view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserCopyAutoResponders'],
            'UserInformation' => $this->array_user_information,
            'ListInformation' => $array_list_information,
            'Lists' => $array_lists,
            'ActiveListItem' => 'AutoResponders'
        );

        $array_view_data = array_merge($array_view_data, InterfaceDefaultValues());


        $this->render('user/auto_responders_copy', $array_view_data);
        // Interface parsing - End }
    }

    /**
     * Delete auto responder function
     * @author Mert Hurturk
     * */
    function _event_delete_autoresponders($list_id) {
        // Field validations - Start {
        $array_form_rules = array(
            array
                (
                'field' => 'SelectedAutoResponders[]',
                'label' => 'autoresponders',
                'rules' => 'required',
            ),
        );

        $this->form_validation->set_rules($array_form_rules);
        $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0999']);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, validation_errors());
        }
        // Run validation - End }
        // Delete auto responders - Start {
        $array_return = API::call(array(
                    'format' => 'object',
                    'command' => 'autoresponders.delete',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'autoresponders' => implode(',', $this->input->post('SelectedAutoResponders'))
                    )
        ));

        if ($array_return->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($array_return->ErrorCode)) == 'array' ? $array_return->ErrorCode[0] : $array_return->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0999']);
                    break;
                default:
                    break;
            }
        } else {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1000']);
            return true;
        }
        // Delete auto responders - End }
    }

    /**
     * Create auto responder function
     * @author Mert Hurturk
     * */
    function _event_create_autoresponder($list_id) {
        $this->form_validation->set_rules("AutoResponderTriggerType");
        $this->form_validation->set_rules("TriggerTimeType");

        // Field validations - Start {
        $this->form_validation->set_rules("AutoResponderName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1018'], 'required');
        if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberLinkClick') {
            $this->form_validation->set_rules("AutoResponderTriggerValue", ApplicationHeader::$ArrayLanguageStrings['Screen']['1019'], 'required');
        } else if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberCampaignOpen') {
            $this->form_validation->set_rules("AutoResponderTriggerValue-Campaign", ApplicationHeader::$ArrayLanguageStrings['Screen']['1590'], 'required');
        } else if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberDate') {
            $this->form_validation->set_rules("AutoResponderTriggerValue-DateField", ApplicationHeader::$ArrayLanguageStrings['Screen']['1591'], 'required');
            $this->form_validation->set_rules("AutoResponderTriggerValue2", ApplicationHeader::$ArrayLanguageStrings['Screen']['1592'], 'required');
        }

        if ($this->input->post('AutoResponderTriggerType') != 'OnSubscriberDate') {
            $auto_responder_trigger_value2 = '';
        } else {
            $auto_responder_trigger_value2 = $this->input->post('AutoResponderTriggerValue2');
        }

        if ($this->input->post('TriggerTimeType') != 'Immediately') {
            $this->form_validation->set_rules("TriggerTime", ApplicationHeader::$ArrayLanguageStrings['Screen']['1020'], 'required');
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Create auto responder - Start {
        $auto_responder_trigger_value = '';
        switch ($this->input->post('AutoResponderTriggerType')) {
            case 'OnSubscriberCampaignOpen':
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue-Campaign');
                break;
            case 'OnSubscriberDate':
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue-DateField');
                break;
            default:
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue');
                break;
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'autoresponder.create',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'subscriberlistid' => $list_id,
                        'emailid' => 0,
                        'autorespondername' => $this->input->post('AutoResponderName'),
                        'autorespondertriggertype' => $this->input->post('AutoResponderTriggerType'),
                        'autorespondertriggervalue' => $auto_responder_trigger_value,
                        'autorespondertriggervalue2' => $auto_responder_trigger_value2,
                        'triggertimetype' => $this->input->post('TriggerTimeType'),
                        'triggertime' => $this->input->post('TriggerTime')
                    )
        ));
        // Create auto responder - End }

        if ($array_return['Success'] == true) {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/email/create/autoresponder/' . $array_return['AutoResponderID']);
        } else {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
        }
    }

    /**
     * Edit auto responder function
     * @author Mert Hurturk
     * */
    function _event_edit_autoresponder($list_id, $auto_responder_id) {
        $this->form_validation->set_rules("AutoResponderTriggerType");
        $this->form_validation->set_rules("TriggerTimeType");

        // Field validations - Start {
        $this->form_validation->set_rules("AutoResponderName", ApplicationHeader::$ArrayLanguageStrings['Screen']['1018'], 'required');
        if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberLinkClick') {
            $this->form_validation->set_rules("AutoResponderTriggerValue", ApplicationHeader::$ArrayLanguageStrings['Screen']['1019'], 'required');
        } else if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberCampaignOpen') {
            $this->form_validation->set_rules("AutoResponderTriggerValue-Campaign", ApplicationHeader::$ArrayLanguageStrings['Screen']['1590'], 'required');
        } else if ($this->input->post('AutoResponderTriggerType') == 'OnSubscriberDate') {
            $this->form_validation->set_rules("AutoResponderTriggerValue-DateField", ApplicationHeader::$ArrayLanguageStrings['Screen']['1591'], 'required');
            $this->form_validation->set_rules("AutoResponderTriggerValue2", ApplicationHeader::$ArrayLanguageStrings['Screen']['1592'], 'required');
        }

        if ($this->input->post('AutoResponderTriggerType') != 'OnSubscriberDate') {
            $auto_responder_trigger_value2 = '';
        } else {
            $auto_responder_trigger_value2 = $this->input->post('AutoResponderTriggerValue2');
        }

        if ($this->input->post('TriggerTimeType') != 'Immediately') {
            $this->form_validation->set_rules("TriggerTime", ApplicationHeader::$ArrayLanguageStrings['Screen']['1020'], 'required');
        }
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
        }
        // Run validation - End }
        // Update auto responder - Start {
        $auto_responder_trigger_value = '';
        switch ($this->input->post('AutoResponderTriggerType')) {
            case 'OnSubscriberCampaignOpen':
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue-Campaign');
                break;
            case 'OnSubscriberDate':
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue-DateField');
                break;
            default:
                $auto_responder_trigger_value = $this->input->post('AutoResponderTriggerValue');
                break;
        }

        $array_return = API::call(array(
                    'format' => 'array',
                    'command' => 'autoresponder.update',
                    'protected' => true,
                    'username' => $this->array_user_information['Username'],
                    'password' => $this->array_user_information['Password'],
                    'parameters' => array(
                        'autoresponderid' => $auto_responder_id,
                        'autorespondername' => $this->input->post('AutoResponderName'),
                        'autorespondertriggertype' => $this->input->post('AutoResponderTriggerType'),
                        'autorespondertriggervalue' => $auto_responder_trigger_value,
                        'autorespondertriggervalue2' => $auto_responder_trigger_value2,
                        'triggertimetype' => $this->input->post('TriggerTimeType'),
                        'triggertime' => $this->input->post('TriggerTime')
                    )
        ));
        // Update auto responder - End }

        if ($array_return['Success'] == true) {
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1026']);
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/autoresponders/browse/' . $list_id);
        } else {
            // API Error occurred
            $error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
            return array(false, sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], '', $error_code));
        }
    }

}

?>