<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Index extends Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::Controller();
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	echo 'Subscriber - Login';
	}


} // end of class User
?>