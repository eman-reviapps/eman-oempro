<?php
/**
 * Login controller
 *
 * @author Mert Hurturk
 */

class Controller_area extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct() {
		parent::__construct();
	
		// Load other modules - Start
		Core::LoadObject('core');
		Core::LoadObject('api');
		Core::LoadObject('subscriber_auth');
		Core::LoadObject('subscribers');
		Core::LoadObject('users');
		Core::LoadObject('lists');
		// Load other modules - End
	}

	/**
	 * Index controller
	 *
	 * @author Mert Hurturk
	 */
	function index($parameter = '') {
		$this->login($parameter);
	}

	/**
	 * Login controller
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function login($parameter = '') {
		if ($parameter == '') {
			header('Location: '.SUBSCRIBERAREA_LOGOUT_URL);
			exit;
		} else {
			$parameters = Core::DecryptURL($parameter);
		}

		// Events - Start {
		if ($this->input->post('Command') == 'Login') {
			$arrayEventReturn = $this->_eventLogin();
		}
		// Events - End }

		// Interface parsing - Start
		$arrayViewData 	= array(
								'PageTitle'		=> (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberLogin'],
								'Parameter'		=> $parameter,
								'MSubscriberID'	=> $parameters['SubscriberID'],
								'MEmailAddress'	=> $parameters['EmailAddress'],
								'ListID'		=> $parameters['ListID'],
								);

		if (isset($arrayEventReturn)) {
			foreach ($arrayEventReturn as $Key=>$Value) {
				$arrayViewData[$Key] = $Value;
			}
		}

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
			$arrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('subscriber/login', $arrayViewData);
		// Interface parsing - End
	}

	/**
	 * Lists controller
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	function lists() {
		if (($subscriberInformation = SubscriberAuth::ValidateAndPerformLogin(array())) == false) {
			header('Location: '.SUBSCRIBERAREA_LOGOUT_URL);
			exit;
		}

		$listID = $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID'];
		$listInformation = Lists::RetrieveList(array('*'),array('ListID' => $listID), false, false,array());
		$userInformation = Users::RetrieveUser(array('*'), array('UserID' => $listInformation['RelOwnerUserID']), true);

		// Retrieve lists of subscriber - Start {
			$arrayReturn = API::call(array(
				'format'	=>	'array',
				'command'	=>	'subscriber.getlists',
				'access'	=>	'subscriber',
				'parameters'=>	array()
			));
			$subscribedLists = $arrayReturn['SubscribedLists'];
		// Retrieve lists of subscriber - End }

		// Interface parsing - Start
		$arrayViewData 	= array(
								'PageTitle'			=> (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberLists'],
								'SubscribedLists'	=> $subscribedLists,
								'SubscriberInformation'	=> $subscriberInformation,
								'UserInformation'	=> $userInformation,
								'CurrentMenuItem'	=> 'Lists'
								);

		if (isset($arrayEventReturn)) {
			foreach ($arrayEventReturn as $Key=>$Value) {
				$arrayViewData[$Key] = $Value;
			}
		}

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
			$arrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('subscriber/lists', $arrayViewData);
		// Interface parsing - End
	}

	/**
	 * List info function
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	function info($listId = 0, $subscriberId = 0) {

		if (($subscriberInformation = SubscriberAuth::ValidateAndPerformLogin(array())) == false) {
			header('Location: '.SUBSCRIBERAREA_LOGOUT_URL);
			exit;
		}

		Core::LoadObject('lists');
		Core::LoadObject('custom_fields');
		$subscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$listId, 'HideInSubscriberArea' => 'false'));
		$listSubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$subscriberId), $subscriberListInformation['ListID']);
		$customFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$subscriberListInformation['ListID'].')', 'auto-quote' => false), 'RelOwnerUserID' => $subscriberListInformation['RelOwnerUserID']), array('FieldName'=>'ASC'), true);

		$listID = $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID'];
		$listInformation = Lists::RetrieveList(array('*'),array('ListID' => $listID), false, false,array());
		$userInformation = Users::RetrieveUser(array('*'), array('UserID' => $listInformation['RelOwnerUserID']), true);

		if ($listSubscriberInformation['EmailAddress'] != $subscriberInformation['EmailAddress']) {
			header('Location: '.SUBSCRIBERAREA_LOGOUT_URL);
			exit;
		}

		if ($this->input->post('Command') == 'Save') {

			$parameters = array(
				'subscriberlistid' => $subscriberListInformation['ListID'],
				'subscriberid' => $listSubscriberInformation['SubscriberID'],
				'emailaddress' => $this->input->post('EmailAddress'),
				'fields' => array()
				);
			$submittedCustomFields = $this->input->post('FormValue_Fields');

			unset($parameters['emailaddress']);

			foreach ($customFields as $each) {
				$parameters['fields']['CustomField'.$each['CustomFieldID']] = $submittedCustomFields['CustomField'.$each['CustomFieldID']];
			}

			$arrayReturn = API::call(array(
				'format'	=>	'array',
				'command'	=>	'subscriber.update',
				'access'	=>	'subscriber',
				'parameters'=>	$parameters
				));

			if ($arrayReturn['Success'] == false && ($arrayReturn['ErrorCode'] == 10 || $arrayReturn['ErrorCode'] == 9 || $arrayReturn['ErrorCode'] == 8)) {
				if ($arrayReturn['ErrorCode'] == 10) {
					$message = ApplicationHeader::$ArrayLanguageStrings['Screen']['0275'];
				} else if ($arrayReturn['ErrorCode'] == 9) {
					$message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1554'];
				} else if ($arrayReturn['ErrorCode'] == 8) {
					$message = ApplicationHeader::$ArrayLanguageStrings['Screen']['1552'];
				}
				$message = array('Error', $message.' ('.$arrayReturn['ErrorCustomFieldTitles'].': '.$arrayReturn['ErrorCustomFieldDescription'].')');
			} else {
				$message = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1323']);
			}

			$_SESSION['PageMessageCache'] = $message;
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/subscriber/info/'.$listId.'/'.$subscriberId);
		}
		
		if ($customFields != false) {
			foreach ($customFields as &$eachField) {
				$ArrayReplaceList = array();
				$eachField['html'] = CustomFields::GenerateCustomFieldHTMLCode($eachField, $listSubscriberInformation['CustomField'.$eachField['CustomFieldID']]);
			}
		}
		
		// Interface parsing - Start
		$arrayViewData 	= array(
								'PageTitle'			=> (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['SubscriberInfo'],
								'SubscriberInformation' => $listSubscriberInformation,
								'ListInformation'	=> $subscriberListInformation,
								'CustomFields'		=> $customFields,
								'UserInformation'	=> $userInformation,
								'CurrentMenuItem'	=> 'Lists'
								);

		if (isset($arrayEventReturn)) {
			foreach ($arrayEventReturn as $Key=>$Value) {
				$arrayViewData[$Key] = $Value;
			}
		}

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
			$arrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('subscriber/info', $arrayViewData);
		// Interface parsing - End
	}

	/**
	 * Logout controller
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	function logout() {
		SubscriberAuth::Logout();
		header('Location: '.SUBSCRIBERAREA_LOGOUT_URL);
	}

	/**
	 * Login event
	 *
	 * @author Mert Hurturk
	 */
	function _eventLogin() {
		// Field validations - Start {
		$arrayFormRules = array(
			array('field' => 'EmailAddress', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0758'], 'rules' => 'required|valid_email')
		);

		$this->form_validation->set_rules($arrayFormRules);

		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false) {
			return array(false);
		}
		// Run validation - End }

		$arrayAPIVars = array(
							'listid'		=> $this->input->post('ListID'),
							'msubscriberid'	=> $this->input->post('MSubscriberID'),
							'memailaddress'	=> $this->input->post('MEmailAddress'),
							'emailaddress'	=> $this->input->post('EmailAddress'),
							);
		$arrayReturn = API::call(array(
			'format'	=>	'object',
			'command'	=>	'subscriber.login',
			'parameters'=>	$arrayAPIVars
		));

		if ($arrayReturn->Success == false) {
			// Incorrect login information
			$ErrorCode = (strtolower(gettype($arrayReturn->ErrorCode)) == 'array' ? $arrayReturn->ErrorCode[0] : $arrayReturn->ErrorCode);
			switch ($ErrorCode) {
				default:
					return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1476']);
					break;
			}
		} else {
			// Correct login information
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/subscriber/lists/');
		}
	}



}