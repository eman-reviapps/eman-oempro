<?php
class Controller_File extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function view($userId, $mediaId)
	{
		$mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
		$file = $mediaLibraryFileMapper->findById(Core::DecryptNumberAdvanced($mediaId));

		$userId = Core::DecryptNumberAdvanced($userId);
		if ($userId > 0 && $file->getUserId() != $userId)
			exit('File not found');

		if ($file->getData() == 'file') {
			if ($file->getType() == 'application/octet-stream') {
				$filePath = DATA_PATH.'media/'.md5($file->getId());
				header('Content-Type: '.$file->getType());
				header("Content-Type: application/force-download");
				if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE") != false) {
					header('Content-Disposition: attachment; filename='.urlencode(basename($file->getName())));
				} else {
					header('Content-Disposition: attachment; filename="'.urlencode(basename($file->getName())).'"');
				}
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($filePath));
				ob_clean();
				$fileHandler = fopen($filePath, 'rb');
				fpassthru($fileHandler);
				ob_flush();
				flush();
				fclose($fileHandler);
			} else {
				header('Content-Type: '.$file->getType());
				$fileHandler = fopen(DATA_PATH.'media/'.md5($file->getId()), 'rb');
				fpassthru($fileHandler);
				fclose($fileHandler);
			}
		} else {
			header('Content-Type: '.$file->getType());
			Core::LoadObject('aws_s3');
			$s3Object = S3::getObject(S3_BUCKET, S3_MEDIALIBRARY_PATH.'/'.md5($file->getUserId()).'/'.md5($file->getId()));
			if ($s3Object !== false) {
				echo $s3Object->body;
			}
		}

		exit;
	}
}

