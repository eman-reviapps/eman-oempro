<?php

/**
 * About controller
 *
 * @author Cem Hurturk
 */
class Controller_UserGroups extends MY_Controller {

    /**
     * Constructor
     *
     * @author Cem Hurturk
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Index controller
     *
     * @return void
     * @author Cem Hurturk
     */
    function getUserGroups() {

        Core::LoadObject('user_groups');
        $ArrayUserGroups = UserGroups::RetrieveUserGroups(array('*'), array());

        $UserGroupsMonthly = UserGroups::RetrieveUserGroups(array('*'), array("SubscriptionType" => 'Monthly', "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
        $UserGroupsAnnually = UserGroups::RetrieveUserGroups(array('*'), array("SubscriptionType" => "Annually", "CreditSystem" => "Disabled", 'IsAdminGroup' => 'No'));
        $PayAsYouGo = UserGroups::RetrieveUserGroups(array('*'), array("CreditSystem" => "Enabled", 'IsAdminGroup' => 'No'));

        print_r('<pre>');
        print_r($UserGroupsMonthly);
        print_r($PayAsYouGo);
        print_r('</pre>');
        exit();
        
        $output = array();
        $output['PayAsYouGo'] = $PayAsYouGo;
        $output['UserGroupsMonthly'] = $UserGroupsMonthly;
        $output['UserGroupsAnnually'] = $UserGroupsAnnually;

        $callback = $_REQUEST['callback'];

        if ($callback) {
            header('Content-Type: text/javascript, charset=UTF-8');
            echo $callback . '(' . json_encode($output) . ');';
        } else {
            header('Content-Type: application/x-json, charset=UTF-8');
            echo json_encode($output);
        }
        exit();
    }

}

?>