<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Logout extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('client_auth');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Check the login session, redirect based on the login session status - Start
	// UserAuth::IsLoggedIn(false, InterfaceAppURL(true).'/user/');
	// Check the login session, redirect based on the login session status - End

	// Delete the user login reminder cookie - Start
	// setcookie(COOKIE_USER_LOGINREMIND, '', time() - 60000);
	// setcookie(COOKIE_USER_LOGINREMIND, '', time() - 60000, '/');
	setcookie(COOKIE_CLIENT_LOGINREMIND, '', time() - (86400 * 14), '/'.APP_DIRNAME.'/user/');
	setcookie(COOKIE_CLIENT_LOGINREMIND, '', time() - (86400 * 14), '/');
	setcookie(COOKIE_CLIENT_LOGINREMIND, '', time() - (86400 * 14));
	// Delete the user login reminder cookie - End

	// Logout the user and redirect to login page - Start
	ClientAuth::Logout();
	header('Location: '.InterfaceAppURL(true).'/client/');
	// Logout the user and redirect to login page - End	
	}
} // end of class User
?>