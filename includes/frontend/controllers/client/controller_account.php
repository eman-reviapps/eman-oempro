<?php
/**
 * List controller
 *
 * @author Mert Hurturk
 */

class Controller_Account extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('client_auth');
		Core::LoadObject('clients');
		Core::LoadObject('users');
		Core::LoadObject('api');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve client information - Start {
		$this->array_client_information = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
		// Retrieve client information - End }

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('UserID'=>$this->array_client_information['RelOwnerUserID']), true);
		// Retrieve user information - End }
	}

	/**
	 * Account controller
	 *
	 * @author Mert Hurturk
	 **/
	function index()
	{
		if ($this->input->post('Command') == 'save')
		{
			$this->_event_save();
		}
		
		// Interface parsing - Start {
		$array_view_data = array(
							'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientListStatistics'],
							'ClientInformation'				=> $this->array_client_information,
							'UserInformation'				=> $this->array_user_information,
							'ListInformation'				=> $array_list_information,
							'CurrentMenuItem'				=> 'Account',
							);

			// Check if there is any message in the message buffer - Start {
			if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
			{
				$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
				unset($_SESSION['PageMessageCache']);
			}
			// Check if there is any message in the message buffer - End }


		$this->render('client/account', $array_view_data);
		// Interface parsing - End }
	}

	function _event_save()
	{
		// Field validations - Start {
		$this->form_validation->set_rules("ClientUsername", ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'], 'required');
		$this->form_validation->set_rules("ClientName", ApplicationHeader::$ArrayLanguageStrings['Screen']['0051'], 'required');
		$this->form_validation->set_rules("ClientEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0758'], 'required|valid_email');
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
		{
			return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0275']);
		}
		// Run validation - End }
		
		// Update client information - Start {
		$api_parameters = array(
			'access'			=> 'client',
			'clientid'			=> $this->array_client_information['ClientID'],
			'clientname'		=> $this->input->post('ClientName'),
			'clientusername'	=> $this->input->post('ClientUsername'),
			'clientpassword'	=> $this->input->post('ClientPassword'),
			'clientemailaddress'=> $this->input->post('ClientEmailAddress')
		);
		if ($this->input->post('ClientPassword') != false || strlen($this->input->post('ClientPassword')) > 0)
		{
			$api_parameters['clientpassword'] = $this->input->post('ClientPassword');
		}
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.update',
			'protected'	=>	false,
			'access'	=>	'client',
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	$api_parameters
		));

		if ($array_return['Success'] == false)
		{
			// API Error occurred
			$error_code = (strtolower(gettype($array_return['ErrorCode'])) == 'array' ? $array_return['ErrorCode'][0] : $array_return['ErrorCode']);
			switch ($error_code)
			{
				default:
					break;
			}
		}
		else
		{
			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1474']);
		}
		// Update client information - End }
	}
}