<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Index extends MY_Controller
{
/**
 * Constructor
 *
 * @author Mert Hurturk
 */
function __construct()
	{
	parent::__construct();

	// Load other modules - Start
	Core::LoadObject('client_auth');
	Core::LoadObject('api');
	$this->load->library('encrypt');
	// Load other modules - End

	// Check if "remember me" cookie is set. If it's set, validate the client information and login - Start
	if ($_COOKIE[COOKIE_CLIENT_LOGINREMIND] != '')
		{
		// Load other modules - Start
		Core::LoadObject('clients');
		// Load other modules - End

		$RememberValue = $this->encrypt->decode($_COOKIE[COOKIE_CLIENT_LOGINREMIND], SCRTY_SALT);
		$ArrayCriterias = array(
								'md5(ClientID)' => mysql_real_escape_string($RememberValue)
								);
		$ArrayClient = Clients::RetrieveClient(array('*'), $ArrayCriterias);

		if ($ArrayClient != false)
			{
			// Perform the user login - Start
			ClientAuth::Login($ArrayClient['ClientID'], $ArrayClient['ClientUsername'], $ArrayClient['ClientPassword']);
			// Perform the user login - End

			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/client/campaigns/');
			exit;
			}
		}
	// Check if "remember me" cookie is set. If it's set, validate the client information and login - End
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	ClientAuth::IsLoggedIn(InterfaceAppURL(true).'/client/campaigns/', false);
	// Check the login session, redirect based on the login session status - End

	Plugins::HookListener('Action', 'Client.Login.Before');

	// Events - Start {
	if ($this->input->post('Command') == 'Login')
		{
		$ArrayEventReturn = $this->_EventLogin($this->input->post('Username'), $this->input->post('Password'), $this->input->post('RememberMe'));
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> (empty($UserInformation['GroupInformation']['ThemeInformation']['ProductName']) ? PRODUCT_NAME : $UserInformation['GroupInformation']['ThemeInformation']['ProductName']).' '.ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientLogin'],
							);

	if (isset($ArrayEventReturn))
		{
		foreach ($ArrayEventReturn as $Key=>$Value)
			{
			$ArrayViewData[$Key] = $Value;
			}
		}

	// Check if there is any message in the message buffer - Start {
	if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
		{
		$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
		unset($_SESSION['PageMessageCache']);
		}
	// Check if there is any message in the message buffer - End }

	$this->render('client/login', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Login event
 *
 * @param string $Username
 * @param string $Password
 * @param string $Captcha
 * @param string $RememberMe
 * @return void
 * @author Mert Hurturk
 */
function _EventLogin($Username, $Password, $RememberMe = '')
	{
	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'Username',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'Password',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'RememberMe',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'],
								'rules'		=> '',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		Plugins::HookListener('Action', 'Client.Login.ValidationError');
		return array(false);
		}
	// Run validation - End }

	// Validate login information - Start {
	$ArrayAPIVars = array(
						'username'		=> $Username,
						'password'		=> $Password,
						'rememberme'	=> $RememberMe,
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'client.login',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect login information
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		Plugins::HookListener('Action', 'Client.Login.InvalidUser', array($ErrorCode));
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0282'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0283'],
							);
				break;
			case '3':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0281'],
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0281'],
							);
				break;
			}
		}
	else
		{
		// Correct login information

		if ($this->input->post('RememberMe') != '')
			{
				$RememberValue = $this->encrypt->encode($ArrayReturn->ClientInfo->ClientID, SCRTY_SALT);
			setcookie(COOKIE_CLIENT_LOGINREMIND, $RememberValue, time() + (86400 * 14), '/');
			}
		else
			{
			setcookie(COOKIE_CLIENT_LOGINREMIND, '', time() - 3600, '/');
			}
		// Save remember cookie for 2 weeks if the checkbox is checked - End

		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/client/campaigns/');
		}
	// Validate login information - End }
	}

} // end of class User
?>