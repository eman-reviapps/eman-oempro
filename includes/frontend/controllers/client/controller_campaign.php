<?php
/**
 * Campaigns controller
 *
 * @author Mert Hurturk
 */

class Controller_Campaign extends MY_Controller
{
/**
 * Constructor
 *
 * @author Mert Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('client_auth');
	Core::LoadObject('clients');
	Core::LoadObject('users');
	Core::LoadObject('api');
	Core::LoadObject('statistics');
	Core::LoadObject('split_tests');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve client information - Start {
	$this->ArrayClientInformation = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
	// Retrieve client information - End }

	// Retrieve user information - Start {
	$this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID'=>$this->ArrayClientInformation['RelOwnerUserID']), true);
	// Retrieve user information - End }
	}

/**
 * Overview controller
 *
 * @author Mert Hurturk
 */
function overview($CampaignID)
	{
	// Retrieve campaign information - Start {
	$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
		"Criteria"	=>	array(
			array(
				"Column"	=>	"%c%.CampaignID",
				"Operator"	=>	"=",
				"Value"		=>	$CampaignID
				)
			),
		"ClientFilter" => $this->ArrayClientInformation['ClientID']
		));
	$CampaignInformation = $CampaignInformation[0];
	$MostClickedLinks = array();
	$MostClickedLinksResultSet = Statistics::RetrieveCampaignLinkClicks($CampaignID, $this->ArrayUserInformation['UserID']);
	while ($ClickedLink = mysql_fetch_assoc($MostClickedLinksResultSet))
		{
		$MostClickedLinks[] = $ClickedLink;
		}
	$HighestOpens = Statistics::RetrieveOpenPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
	$HighestClicks = Statistics::RetrieveClickPerformanceDaysOfCampaign($CampaignInformation['CampaignID'], $this->ArrayUserInformation['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
	$opened_ratio = ceil((100 * $CampaignInformation['UniqueOpens']) / $CampaignInformation['TotalSent']);
	$bounce_ratio = ceil((100 * $CampaignInformation['TotalHardBounces']) / $CampaignInformation['TotalSent']);
	$not_opened_ratio = 100 - ($opened_ratio + $bounce_ratio);
	$CampaignInformation['TotalBounces'] = $CampaignInformation['TotalHardBounces'] + $CampaignInformation['TotalSoftBounces'];
	$CampaignInformation['HardBounceRatio'] = number_format(($CampaignInformation['TotalHardBounces'] * 100) / $CampaignInformation['TotalSent']);
	$CampaignInformation['SoftBounceRatio'] = number_format(($CampaignInformation['TotalSoftBounces'] * 100) / $CampaignInformation['TotalSent']);
	$CampaignInformation['TotalSpamReports'] = Statistics::RetrieveSPAMComplaintCountOfCampaign($CampaignInformation['CampaignID']);
	
	$SplitTestEmails = array();
	if ($CampaignInformation['SplitTest'] != false)
		{
		$SplitTestEmails = Emails::RetrieveEmailsOfTest($CampaignInformation['SplitTest']['TestID'], $CampaignInformation['CampaignID'], true);
		
		$WinnerEmail = false;
		if ($CampaignInformation['SplitTest']['RelWinnerEmailID'] != 0)
			{
			$WinnerEmail = Emails::RetrieveEmail(array('EmailName'), array('EmailID'=>$CampaignInformation['SplitTest']['RelWinnerEmailID']), false);
			$WinnerEmail = $WinnerEmail['EmailName'];
			}
		}
	// Retrieve campaign information - End }

	// Interface parsing - Start {
	$ArrayViewData 	= array(
							'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientCampaignStatistics'],
							'ClientInformation'				=> $this->ArrayClientInformation,
							'UserInformation'				=> $this->ArrayUserInformation,
							'CampaignInformation'			=> $CampaignInformation,
							'WinnerEmail'					=> $WinnerEmail,
							'SplitTestEmails'				=> $SplitTestEmails,
							'ChartColors'					=> explode(',', CHART_COLORS),
							'MostClickedLinks'				=> $MostClickedLinks,
							'HighestOpens'					=> $HighestOpens,
							'HighestClicks'					=> $HighestClicks,
							'CurrentMenuItem'				=> 'Campaigns',
							'OpenedRatio'					=> $opened_ratio,
							'NotOpenedRatio'				=> $not_opened_ratio,
							'BouncedRatio'					=> $bounce_ratio,
							);

	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
			{
			$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
			}
	// Check if there is any message in the message buffer - End }

	$this->render('client/campaign_overview', $ArrayViewData);
	// Interface parsing - End }
	}

/**
 * Campaign preview function
 *
 * @author Mert Hurturk
 */
function _event_preview($CampaignID)
	{
	// Field validations - Start {
	$this->form_validation->set_rules("PreviewEmailAddress", '');
	$this->form_validation->set_rules("PreviewType", '');
	if ($this->input->post('PreviewType') == 'email')
		{
		$this->form_validation->set_rules("PreviewEmailAddress", ApplicationHeader::$ArrayLanguageStrings['Screen']['0809'], 'required|valid_email');
		}
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return false;
		}
	// Run validation - End }


	if ($this->input->post('PreviewType') == 'email')
		{
		$ArrayReturn = API::call(array(
			'format'	=>	'array',
			'command'	=>	'email.emailpreview',
			'protected'	=>	true,
			'username'	=>	$this->ArrayUserInformation['Username'],
			'password'	=>	$this->ArrayUserInformation['Password'],
			'parameters'=>	array(
				'campaignid'	=> $CampaignID,
				'emailid'		=> $this->input->post('RelEmailID'),
				'emailaddress'	=> $this->input->post('PreviewEmailAddress'),
				)
			));
		return array(
					'PageSuccessMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0810'],
					);
		break;
		}
	elseif ($this->input->post('PreviewType') == 'browser')
		{
		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/user/campaigns/preview/'.$CampaignID, '302');
		}
	}

/**
 * Statistics data controller
 *
 * @author Mert Hurturk
 **/
function statistics($CampaignID, $Statistics, $Days = 5)
	{
	Core::LoadObject('chart_data_generator');
	Core::LoadObject('statistics');

	// Load chart graph colors from the config file - Start {
	$ArrayChartGraphColors = explode(',', CHART_COLORS);
	// Load chart graph colors from the config file - End }

	if ($Statistics == 'opens-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('OpenStatistics')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['OpenStatistics'] as $Each)
			{
			$SparklineData[] = $Each['Total'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'clicks-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('ClickStatistics')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['ClickStatistics'] as $Each)
			{
			$SparklineData[] = $Each['Total'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'forwards-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('ForwardStatistics')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['ForwardStatistics'] as $Each)
			{
			$SparklineData[] = $Each['Total'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'views-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('BrowserViewStatistics')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['BrowserViewStatistics'] as $Each)
			{
			$SparklineData[] = $Each['Total'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'unsubscriptions-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('UnsubscriptionStatistics')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['UnsubscriptionStatistics'] as $Each)
			{
			$SparklineData[] = $Each['Unique'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'bounces-sparkline')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				),
			"Reports"	=>	array(
				"Days"			=>	$Days,
				"Statistics"	=>	array('BounceStatisticsTimeFrame')
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$SparklineData = array();

		foreach ($CampaignInformation['BounceStatisticsTimeFrame'] as $Each)
			{
			$SparklineData[] = $Each['Total'];
			}
		$SparklineData = implode('x', $SparklineData);

		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$SparklineData));
		}
	elseif ($Statistics == 'overview')
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				)
			));
		$CampaignInformation = $CampaignInformation[0];

		$opened_ratio = number_format((100 * $CampaignInformation['UniqueOpens']) / $CampaignInformation['TotalSent']);
		$bounced_ratio = number_format((100 * $CampaignInformation['TotalHardBounces']) / $CampaignInformation['TotalSent']);
		$not_opened_ratio = 100 - ($opened_ratio + $bounced_ratio);
		$not_opened = $CampaignInformation['TotalSent'] - ($CampaignInformation['UniqueOpens'] + $CampaignInformation['TotalHardBounces']);

		$XML = '<pie><slice color="'.$ArrayChartGraphColors[2].'" title="'.$not_opened.'">'.$not_opened_ratio.'</slice><slice color="'.$ArrayChartGraphColors[1].'" title="'.$CampaignInformation['UniqueOpens'].'">'.$opened_ratio.'</slice><slice color="'.$ArrayChartGraphColors[0].'" title="'.$CampaignInformation['TotalHardBounces'].'">'.$bounced_ratio.'</slice></pie>';
		header('Content-type: text/xml');
		print($XML);
		exit;
		}
	else
		{
		$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
			"Criteria"	=>	array(
				array(
					"Column"	=>	"%c%.CampaignID",
					"Operator"	=>	"=",
					"Value"		=>	$CampaignID
					)
				)
			));
		$CampaignInformation = $CampaignInformation[0];
		$LastXDays = ($Days < 7 ? 7 : $Days);
		$StartFromDate = date('Y-m-d', strtotime($CampaignInformation['SendProcessStartedOn']));
		$ToDate = date('Y-m-d', strtotime($StartFromDate.' +'.($LastXDays-1).' days'));
		$OpenStatistics = Statistics::RetrieveCampaignOpenStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);
		$ArrayData = array();
		$ArrayData['Series']	 = array();
		$ArrayData['Graphs']	 = array(
										0 => array('title' => '', 'axis' => 'left'),
										1 => array('title' => '', 'axis' => 'left'),
										);
		$ArrayData['GraphData']	= array();

		if ($Statistics == 'opens-clicks')
			{
			$ClickStatistics = Statistics::RetrieveCampaignClickStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				}
			}
		elseif ($Statistics == 'opens-unsubscriptions')
			{
			$UnsubscriptionStatistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				}

			}
		elseif ($Statistics == 'opens-all')
			{
			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total']) == true ? $OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0891'], number_format($OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'])),
																			),
													);
				}

			}
		elseif ($Statistics == 'clicks-all')
			{
			$ClickStatistics = Statistics::RetrieveCampaignClickStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				$ArrayData['GraphData'][1][] = array(
													'Value'			=> (isset($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total']) == true ? $ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0893'], number_format($ClickStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'])),
																			),
													);
				}

			}
		elseif ($Statistics == 'forwards-all')
			{
			$ForwardStatistics = Statistics::RetrieveCampaignForwardStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0900'], number_format($ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				$ArrayData['GraphData'][1][] = array(
													'Value'			=> (isset($ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total']) == true ? $ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0901'], number_format($ForwardStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'])),
																			),
													);
				}

			}
		elseif ($Statistics == 'browserviews-all')
			{
			$ViewStatistics = Statistics::RetrieveCampaignBrowserViewStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0915'], number_format($ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				$ArrayData['GraphData'][1][] = array(
													'Value'			=> (isset($ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total']) == true ? $ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0915'], number_format($ViewStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Total'])),
																			),
													);
				}

			}
		elseif ($Statistics == 'unsubscriptions-all')
			{
			$UnsubscriptionStatistics = Statistics::RetrieveCampaignUnsubscriptionStatistics($CampaignID, $this->ArrayUserInformation['UserID'], $StartFromDate, $ToDate);

			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0917'], number_format($UnsubscriptionStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				}

			}

		if ($Statistics != 'clicks-all' && $Statistics != 'forwards-all' && $Statistics != 'browserviews-all' && $Statistics != 'unsubscriptions-all')
			{
			for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--)
				{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][1][] = array(
													'Value'			=> (isset($OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique']) == true ? $OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0892'], number_format($OpenStatistics[date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['Unique'])),
																			),
													);
				}
			}

		$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
		header('Content-type: text/xml');
		print($XML);
		exit;
		}

	}

/**
 * Statistic export controller
 *
 * @author Mert Hurturk
 **/
function exportstatistics($CampaignID, $Statistic = 'overview', $Format = 'csv')
	{
	Core::LoadObject('statistics');

	$CampaignInformation = Campaigns::RetrieveCampaigns_Enhanced(array(
		"Criteria"	=>	array(
			array(
				"Column"	=>	"%c%.CampaignID",
				"Operator"	=>	"=",
				"Value"		=>	$CampaignID
				)
			),
		"Reports"	=>	array(
			"Days"			=>	30,
			"Statistics"	=>	array('OpenStatistics', 'ClickStatistics', 'UnsubscriptionStatistics', 'ForwardStatistics', 'BrowserViewStatistics', 'BounceStatisticsTimeFrame')
			)
		));
	$CampaignInformation = $CampaignInformation[0];

	if ($Statistic == 'overview')
		{
		if ($Format == 'csv')
			{
			$CSVString = ApplicationHeader::$ArrayLanguageStrings['Screen']['0918']."\n\n".Statistics::ConvertToCSV($CampaignInformation['UnsubscriptionStatistics'], 'Date');
			$CSVString .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0919']."\n\n".Statistics::ConvertToCSV($CampaignInformation['OpenStatistics'], 'Date');
			$CSVString .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0920']."\n\n".Statistics::ConvertToCSV($CampaignInformation['ClickStatistics'], 'Date');
			$CSVString .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0921']."\n\n".Statistics::ConvertToCSV($CampaignInformation['BrowserViewStatistics'], 'Date');
			$CSVString .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0922']."\n\n".Statistics::ConvertToCSV($CampaignInformation['ForwardStatistics'], 'Date');
			}
		elseif ($Format == 'xml')
			{
			$XMLString = '<statistics>';
			$XMLString .= '<unsubscription_statistics>'.Statistics::ConvertToXML($CampaignInformation['UnsubscriptionStatistics'], 'Date').'</unsubscription_statistics>';
			$XMLString .= '<open_statistics>'.Statistics::ConvertToXML($CampaignInformation['OpenStatistics'], 'Date').'</open_statistics>';
			$XMLString .= '<click_statistics>'.Statistics::ConvertToXML($CampaignInformation['ClickStatistics'], 'Date').'</click_statistics>';
			$XMLString .= '<browser_view_statistics>'.Statistics::ConvertToXML($CampaignInformation['BrowserViewStatistics'], 'Date').'</browser_view_statistics>';
			$XMLString .= '<forward_statistics>'.Statistics::ConvertToXML($CampaignInformation['ForwardStatistics'], 'Date').'</forward_statistics>';
			$XMLString .= '</statistics>';
			}
		}
	elseif ($Statistic == 'bounces')
		{
		$TMPArray = array();
		foreach ($CampaignInformation['BounceStatisticsTimeFrame'] as $Each)
			{
			$TMPArray[] = $Each;
			}

		$CSVString = Statistics::ConvertToCSV($TMPArray);
		}
	else
		{
		exit;
		}
	
	if ($Format == 'csv')
		{
		// Make CSV data file downloadable - Start 
		header("Content-type: application/octetstream");
		header("Content-Disposition: attachment; filename=\"campaign_statistics.csv\"");
		header('Content-length: ' . strlen($CSVString));
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: public");
		// Make CSV data file downloadable - End
		print $CSVString;
		exit;
		}
	elseif ($Format == 'xml')
		{
		// Make XML data file downloadable - Start
		header("Content-type: application/octetstream");
		header("Content-Disposition: attachment; filename=\"campaign_statistics.xml\"");
		header('Content-length: ' . strlen($XMLString));
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: public");
		// Make XML data file downloadable - End
		print $XMLString;
		exit;
		}
	}

/**
 * Test statistics data controller
 *
 * @author Mert Hurturk
 **/
function teststatistics($CampaignID)
	{
	Core::LoadObject('chart_data_generator');
	Core::LoadObject('statistics');
	Core::LoadObject('emails');
	Core::LoadObject('split_tests');

	// Load chart graph colors from the config file - Start {
	$ArrayChartGraphColors = explode(',', CHART_COLORS);
	// Load chart graph colors from the config file - End }

	// Retrieve test information and emails - Start {
	$TestInformation = SplitTests::RetrieveTestOfACampaign($CampaignID, $this->ArrayUserInformation['UserID']);
	$TestInformation = $TestInformation[0];

	$TestEmails = Emails::RetrieveEmailsOfTest($TestInformation['TestID'], $CampaignID);
	// Retrieve test information and emails - End }

	$ArrayData['Series']	 = array(
									0 => ApplicationHeader::$ArrayLanguageStrings['Screen']['0094'],
									1 => ApplicationHeader::$ArrayLanguageStrings['Screen']['1390']
									);

	$ArrayData['Graphs']	= array();

	foreach ($TestEmails as $index=>$EachEmail)
		{
		$TotalUniqueClicksOfEmail = Statistics::TotalUniqueClicksOfCampaign($CampaignID, $this->ArrayUserInformation['UserID'], $EachEmail['EmailID']);
		$TotalUniqueClicksOfEmail = $TotalUniqueClicksOfEmail ? $TotalUniqueClicksOfEmail : 0;

		$TotalUniqueOpensOfEmail = Statistics::TotalUniqueOpensOfCampaign($CampaignID, $this->ArrayUserInformation['UserID'], $EachEmail['EmailID']);
		$TotalUniqueOpensOfEmail = $TotalUniqueOpensOfEmail ? $TotalUniqueOpensOfEmail : 0;

		$ArrayData['Graphs'][] = array();
		$TempArray = array();
		$TempArray[0]	= array(
											'Value'			=> $TotalUniqueOpensOfEmail,
											'Parameters'	=> array(
																	'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0892'], $TotalUniqueOpensOfEmail)
																	)
											);
		$TempArray[1]	= array(
											'Value'			=> $TotalUniqueClicksOfEmail,
											'Parameters'	=> array(
																	'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0874'], $TotalUniqueClicksOfEmail)
																	)
											);

		$ArrayData['GraphData'][] = $TempArray;
		}

		$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
		header('Content-type: text/xml');
		print($XML);
		exit;
	}

function snippetsubscriberactivity($campaign_id, $statistic, $start_from = 0)
	{
	Core::LoadObject('statistics');

	if ($statistic == 'opens')
		{
		$result_set = Statistics::RetrieveCampaignOpens($campaign_id, $this->ArrayUserInformation['UserID'], $start_from);
		$array_subscribers = array();
		while ($each_row = mysql_fetch_assoc($result_set))
			{
			$array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$each_row['RelSubscriberID']), $each_row['RelListID']);
			$array_subscribers[] = array(
										'SubscriberID'		=> $each_row['RelSubscriberID'],
										'Email'				=> ($array_subscriber['EmailAddress'] == '' ? ApplicationHeader::$ArrayLanguageStrings['Screen']['0889'] : $array_subscriber['EmailAddress']),
										'TotalOpens'		=> number_format($each_row['TotalOpens']),
										);
			}
		$view = 'user/campaign_snippet_subscriber_activity_opens';
		$view_data = array(
			'Subscribers'	=>	$array_subscribers
			);
		}
	elseif ($statistic == 'clicks')
		{
		$result_set = Statistics::RetrieveCampaignLinkClicks($campaign_id, $this->ArrayUserInformation['UserID'], 'Subscribers', $start_from);
		$array_subscribers = array();
		while ($each_row = mysql_fetch_assoc($result_set))
			{
			$array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$each_row['RelSubscriberID']), $each_row['RelListID']);
			$array_subscribers[] = array(
											'SubscriberID'			=> $array_subscriber['SubscriberID'],
											'Email'					=> $array_subscriber['EmailAddress'],
											'TotalClicks'			=> number_format($each_row['TotalClicks']),
											);
			}
		$view = 'user/campaign_snippet_subscriber_activity_clicks';
		$view_data = array(
			'Subscribers'	=>	$array_subscribers
			);
		}
	elseif ($statistic == 'unsubscriptions')
		{
		$result_set = Statistics::RetrieveCampaignUnsubscriptions($campaign_id, $start_from);
		$array_subscribers = array();
		while ($each_row = mysql_fetch_assoc($result_set))
			{
			$array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$each_row['RelSubscriberID']), $each_row['RelListID']);
			$array_subscribers[] = array(
										'SubscriberID'			=> $array_subscriber['SubscriberID'],
										'Email'					=> $array_subscriber['EmailAddress'],
										'UnsubscriptionDate'	=> $each_row['UnsubscriptionDate'],
										);
			}
		$view = 'user/campaign_snippet_subscriber_activity_unsubscriptions';
		$view_data = array(
			'Subscribers'	=>	$array_subscribers
			);
		}

	$this->render($view, $view_data);
	}


}