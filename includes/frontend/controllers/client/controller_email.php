<?php
/**
 * Email controller
 *
 * @author Mert Hurturk
 */

class Controller_Email extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		$this->load->helper('url');

		Core::LoadObject('user_auth');
		Core::LoadObject('clients');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('lists');
		Core::LoadObject('emails');
		Core::LoadObject('wizard');
		Core::LoadObject('split_tests');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve client information - Start {
		$this->ArrayClientInformation = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
		// Retrieve client information - End }

		// Retrieve user information - Start {
		$this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID'=>$this->ArrayClientInformation['RelOwnerUserID']), true);
		// Retrieve user information - End }
	}

	/**
	 * Preview controller
	 *
	 * @author Mert Hurturk
	 **/
	function preview($email_id, $type = 'html', $mode = 'campaign', $entity_id = 0, $command = '')
	{
		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('subscribers');
		Core::LoadObject('lists');
		Core::LoadObject('emails');
		Core::LoadObject('campaigns');
		Core::LoadObject('personalization');
		// Load other modules - End

		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'email.get',
			'protected'	=>	true,
			'username'	=>	$this->ArrayUserInformation['Username'],
			'password'	=>	$this->ArrayUserInformation['Password'],
			'parameters'=>	array(
				'emailid'	=>	$email_id
				)
			));
		$email_information = $array_return['EmailInformation'];

		if ($command != '' && $command == 'source')
		{
			// Select one of the recipient lists from the campaign - Start
			$campaign_information = array();
			if ($mode == 'campaign')
			{
				$array_return = API::call(array(
					'format'	=>	'array',
					'command'	=>	'campaign.get',
					'protected'	=>	true,
					'username'	=>	$this->ArrayUserInformation['Username'],
					'password'	=>	$this->ArrayUserInformation['Password'],
					'parameters'=>	array(
						'campaignid'	=>	$entity_id
						)
					));
				$campaign_information = $array_return['Campaign'];
				unset($array_return);
				$array_list = Lists::RetrieveList(array('*'), array('ListID' => $campaign_information['RecipientLists'][0]), false);
			}
			else if ($mode == 'confirmation')
			{
				$array_list = Lists::RetrieveList(arraY('*'), array('ListID' => $entity_id));
			}
			// Select one of the recipient lists from the campaign - End

			// Retrieve a random subscriber from the target list - Start
			$array_random_subscriber = Subscribers::SelectRandomSubscriber($array_list['ListID'], true);
			// Retrieve a random subscriber from the target list - End

			if ($type == 'html')
			{
				if ($email_information['FetchURL'] != '')
				{
					$personalized_url = Personalization::Personalize($email_information['FetchURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), false);
					$show_content = Campaigns::FetchRemoteContent($personalized_url);
				}
				else
				{
					$show_content = $email_information['HTMLContent'];
				}
				
				if ($show_content == '')
				{
					$type = 'plain';
				}
				
				if ($type != 'plain') {
					// Plug-in hook - Start
					$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
					$show_content = $array_plugin_return_vars[1];
					// Plug-in hook - End

					// Plug-in hook - Start
					$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
					$show_content = $array_plugin_return_vars[1];
					// Plug-in hook - End
				}
			}
			
			if ($type == 'plain')
			{
				if ($email_information['FetchPlainURL'] != '')
				{
					$personalized_url = Personalization::Personalize($email_information['FetchPlainURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), false);
					$show_content = Campaigns::FetchRemoteContent($personalized_url);	
				}
				else
				{
					$show_content = $email_information['PlainContent'];
				}

				// Plug-in hook - Start
				$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
				$show_content = $array_plugin_return_vars[1];
				// Plug-in hook - End

				// Plug-in hook - Start
				$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
				$show_content = $array_plugin_return_vars[1];
				// Plug-in hook - End
			}

			// Add header/footer to the email (if exists in the user group) - Start {
			$ArrayReturn = Personalization::AddEmailHeaderFooter(($type == 'plain' ? $show_content : ''), ($type == 'html' ? $show_content : ''), $this->ArrayUserInformation['GroupInformation']);
			if ($type == 'plain')
				{
				$show_content = $ArrayReturn[0];
				}
			else
				{
				$show_content = $ArrayReturn[1];
				}
			// Add header/footer to the email (if exists in the user group) - End }

			// Perform personalization (disable personalization and links if subscriber information not provided) - Start
			$show_content = Personalization::Personalize($show_content, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), true);
			// Perform personalization (disable personalization and links if subscriber information not provided) - End

			if ($type == 'plain')
			{
				$show_content = '<pre>'.$show_content.'</pre>';
			}

			print $show_content;
			exit;
		}

		// Interface parsing - Start {
		$ArrayViewData 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailPreview'],
								'UserInformation'				=> $this->ArrayUserInformation,
								'ClientInformation'				=> $this->ArrayClientInformation,
								'CampaignInformation'			=> $CampaignInformation,
								'EmailInformation'				=> $email_information,
								'Mode'							=> $mode,
								'EntityID'						=> $entity_id,
								'EmailID'						=> $email_id,
								'Type'							=> $type
								);

		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		$this->render('client/email_preview', $ArrayViewData);
		// Interface parsing - End }

	}

}