<?php
/**
 * Campaigns controller
 *
 * @author Mert Hurturk
 */

class Controller_Campaigns extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('client_auth');
	Core::LoadObject('users');
	Core::LoadObject('clients');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve client information - Start {
	$this->ArrayClientInformation = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
	// Retrieve client information - End }

	// Retrieve user information - Start {
	$this->ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID'=>$this->ArrayClientInformation['RelOwnerUserID']), true);
	// Retrieve user information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Decide to go with campaign browse, create or blank state - Start {
	$this->browse();
	// Decide to go with campaign browse, create or blank state - End }	
	}

/**
 * Browse campaigns
 *
 * @return void
 * @author Mert Hurturk
 */
function browse($StartFrom = 1, $RPP = 25, $FilterType = 'ByCategory', $FilterData = 'Sent', $SortField = '', $Order = '')
	{
	// Load other modules - Start
	Core::LoadObject('campaigns');
	// Load other modules - End	

	// Filters - Start {
		$Filter_ByCategory_Sent = array(
			array(
				'Column'	=>	'%c%.RelOwnerUserID',
				'Operator'	=>	'=',
				'Value'		=>	$this->ArrayUserInformation['UserID']
				),
			array(
				'Link'		=>	'AND',
				array(
					'Column'	=>	'%c%.CampaignStatus',
					'Operator'	=>	'=',
					'Value'		=>	'Sent'
					),
				array(
					'Link'		=>	'OR',
					'Column'	=>	'%c%.CampaignStatus',
					'Operator'	=>	'=',
					'Value'		=>	'Failed'
					)
				),
			);
		
		$Filter_ByCategory_Outbox = array(
			array(
				'Column'	=>	'%c%.RelOwnerUserID',
				'Operator'	=>	'=',
				'Value'		=>	$this->ArrayUserInformation['UserID']
				),
			array(
				'Link'		=>	'AND',
				array(
					'Column'	=>	'%c%.CampaignStatus',
					'Operator'	=>	'=',
					'Value'		=>	'Sending'
					),
				array(
					'Link'		=>	'OR',
					array(
						'Column'	=>	'%c%.CampaignStatus',
						'Operator'	=>	'=',
						'Value'		=>	'Ready'
						),
					array(
						'Link'		=>	'AND',
						'Column'	=>	'%c%.ScheduleType',
						'Operator'	=>	'=',
						'Value'		=>	'Immediate'
						),
					)
				)
			);
		
		$Filter_ByCategory_Paused = array(
			array(
				'Column'	=>	'%c%.RelOwnerUserID',
				'Operator'	=>	'=',
				'Value'		=>	$this->ArrayUserInformation['UserID']
				),
			array(
				'Link'		=>	'AND',
				'Column'	=>	'%c%.CampaignStatus',
				'Operator'	=>	'=',
				'Value'		=>	'Paused'
				),
			);
	// Filters - End }

	// Apply filtering and row ordering - Start {
		$CampaignStatusCriteria = $Filter_ByCategory_Sent;
		$CampaignRowOrder = array(
			'Column'	=> $SortField == '' ? 'SendProcessFinishedOn' : $SortField,
			'Type'		=> $Order == '' ? 'DESC' : $Order,
			);
		if (($FilterType == 'ByCategory') && ($FilterData == 'Outbox'))
			{
			$CampaignStatusCriteria = $Filter_ByCategory_Outbox;
			$CampaignRowOrder = array(
				'Column'	=> $SortField == '' ? 'CampaignName' : $SortField,
				'Type'		=> $Order == '' ? 'ASC' : $Order,
				);
			}
		elseif (($FilterType == 'ByCategory') && ($FilterData == 'Paused'))
			{
			$CampaignStatusCriteria = $Filter_ByCategory_Paused;
			$CampaignRowOrder = array(
				'Column'	=> $SortField == '' ? 'CampaignName' : $SortField,
				'Type'		=> $Order == '' ? 'ASC' : $Order,
				);
			}

		$ArrayFilterTags = array();
		if ($FilterTag !== '' && $FilterTag != -1)
			{
			$ArrayFilterTags = explode(':', $FilterTag);
			}
	// Apply filtering and row ordering - End }

	// Retrieve campaigns - Start {
		$Campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
			'Criteria'		=>	$CampaignStatusCriteria,
			'RowOrder'		=>	$CampaignRowOrder,
			'SplitTest'		=>	true,
			'ClientFilter'	=>	$this->ArrayClientInformation['ClientID'],
			'Pagination'	=> array(
				'Offset'		=> ($StartFrom <= 0 ? 0 : ($StartFrom - 1) * $RPP),
				'Rows'			=> $RPP
				)
			));
		$TotalBrowsedCampaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
			'ReturnTotalRows'	=>	true,
			'Criteria'			=>	$CampaignStatusCriteria,
			'RowOrder'			=>	$CampaignRowOrder,
			'ClientFilter'	=>	$this->ArrayClientInformation['ClientID'],
			'Pagination'		=> array(
				'Offset'			=> ($StartFrom <= 0 ? 0 : ($StartFrom - 1) * $RPP),
				'Rows'				=> $RPP
				)
			));
	// Retrieve campaigns - End }

	// Retrieve total number of campaigns - Start {
		$TotalSent = Campaigns::RetrieveCampaigns_Enhanced(array(
			'ReturnTotalRows'	=>	true,
			'Criteria'			=>	$Filter_ByCategory_Sent,
			'ClientFilter' => $this->ArrayClientInformation['ClientID']
			));

		$TotalOutbox = Campaigns::RetrieveCampaigns_Enhanced(array(
			'ReturnTotalRows'	=>	true,
			'Criteria'			=>	$Filter_ByCategory_Outbox,
			'ClientFilter' => $this->ArrayClientInformation['ClientID']
			));

		$TotalPaused = Campaigns::RetrieveCampaigns_Enhanced(array(
			'ReturnTotalRows'	=>	true,
			'Criteria'			=>	$Filter_ByCategory_Paused,
			'ClientFilter' => $this->ArrayClientInformation['ClientID']
			));
	// Retrieve total number of campaigns - End }
		
	// Calculate total pages (pagination) - Start {
	$TotalPages = 1;
	if (($FilterType == 'ByCategory') && ($FilterData == 'Sent'))
		{
		$TotalPages = ceil($TotalSent / $RPP);
		}
	elseif (($FilterType == 'ByCategory') && ($FilterData == 'Outbox'))
		{
		$TotalPages = ceil($TotalOutbox / $RPP);
		}
	elseif (($FilterType == 'ByCategory') && ($FilterData == 'Paused'))
		{
		$TotalPages = ceil($TotalPaused / $RPP);
		}
	// Calculate total pages (pagination) - End }

	// Interface parsing - Start {
		$ArrayViewData 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientBrowseCampaigns'],
								'ClientInformation'				=> $this->ArrayClientInformation,
								'UserInformation'				=> $this->ArrayUserInformation,
								'Campaigns'						=> $Campaigns,
								'TotalPages'					=> $TotalPages,
								'TotalSentCampaigns'			=> $TotalSent,
								'TotalOutboxCampaigns'			=> $TotalOutbox,
								'TotalPausedCampaigns'			=> $TotalPaused,
								'CurrentMenuItem'				=> 'Campaigns',
								'CurrentPage'					=> $StartFrom,
								'RPP'							=> $RPP,
								'FilterType'					=> $FilterType,
								'FilterData'					=> $FilterData,
								'FilterTags'					=> count($ArrayFilterTags) < 1 ? array() : $ArrayFilterTags,
								);

		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		if (isset($ArrayEventReturn))
			{
			foreach ($ArrayEventReturn as $Key=>$Value)
				{
				$ArrayViewData[$Key] = $Value;
				}
			}

		// Check if there is any message in the message buffer - Start {
			if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
				{
				$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
				unset($_SESSION['PageMessageCache']);
				}
		// Check if there is any message in the message buffer - End }

		if (isset($ArrayEventReturn) == true)
			{
			$ArrayViewData[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
			}

		$this->render('client/campaigns', $ArrayViewData);
	// Interface parsing - End }
	}

/**
 * Preview campaign controller
 *
 * @return void
 * @author Mert Hurturk
 **/
function preview($CampaignID, $Type = 'html', $Command = '')
	{
	// Load other modules - Start
	Core::LoadObject('user_auth');
	Core::LoadObject('subscribers');
	Core::LoadObject('lists');
	Core::LoadObject('emails');
	Core::LoadObject('campaigns');
	Core::LoadObject('personalization');
	// Load other modules - End
		
	// Retrieve campaign information - Start {
	$ArrayReturn = API::call(array(
		'format'	=>	'array',
		'command'	=>	'campaign.get',
		'protected'	=>	true,
		'username'	=>	$this->ArrayUserInformation['Username'],
		'password'	=>	$this->ArrayUserInformation['Password'],
		'parameters'=>	array(
			'campaignid'	=>	$CampaignID
			)
		));
	$CampaignInformation = $ArrayReturn['Campaign'];
	unset($ArrayReturn);
	// Retrieve campaign information - End }

	if ($Command != '' && $Command == 'source')
		{
		$ArrayReturn = API::call(array(
			'format'	=>	'array',
			'command'	=>	'email.get',
			'protected'	=>	true,
			'username'	=>	$this->ArrayUserInformation['Username'],
			'password'	=>	$this->ArrayUserInformation['Password'],
			'parameters'=>	array(
				'emailid'	=>	$CampaignInformation['RelEmailID']
				)
			));
		$EmailInformation = $ArrayReturn['EmailInformation'];
		unset($ArrayReturn);

		// Select one of the recipient lists from the campaign - Start
		$ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $CampaignInformation['RecipientLists'][0]), false);
		// Select one of the recipient lists from the campaign - End

		// Retrieve a random subscriber from the target list - Start
		$ArrayRandomSubscriber	= Subscribers::SelectRandomSubscriber($ArrayList['ListID'], true);
		// Retrieve a random subscriber from the target list - End

		if ($Type == 'html')
			{
			if ($EmailInformation['FetchURL'] != '')
				{
				$ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($EmailInformation['FetchURL'], array('Subscriber', 'User'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), false));	
				}
			else
				{
				$ShowContent = $EmailInformation['HTMLContent'];
				}

			// Plug-in hook - Start
			$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $ShowContent, '', $ArrayRandomSubscriber));
				$ShowContent	= $ArrayPlugInReturnVars[1];
			// Plug-in hook - End

			// Plug-in hook - Start
			$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $ShowContent, '', $ArrayRandomSubscriber, 'Browser Preview'));
				$ShowContent	= $ArrayPlugInReturnVars[1];
			// Plug-in hook - End
			}
		else
			{
			if ($EmailInformation['FetchPlainURL'] != '')
				{
				$ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($EmailInformation['FetchPlainURL'], array('Subscriber', 'User'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), false));	
				}
			else
				{
				$ShowContent = $EmailInformation['PlainContent'];
				}

			// Plug-in hook - Start
			$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', '', $ShowContent, $ArrayRandomSubscriber));
				$ShowContent	= $ArrayPlugInReturnVars[2];
			// Plug-in hook - End

			// Plug-in hook - Start
			$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', '', $ShowContent, $ArrayRandomSubscriber, 'Browser Preview'));
				$ShowContent	= $ArrayPlugInReturnVars[2];
			// Plug-in hook - End
			}

		// Add header/footer to the email (if exists in the user group) - Start {
		$ArrayReturn = Personalization::AddEmailHeaderFooter(($type == 'plain' ? $ShowContent : ''), ($type == 'html' ? $ShowContent : ''), $this->ArrayUserInformation['GroupInformation']);
		if ($Type == 'plain')
			{
			$ShowContent = $ArrayReturn[0];
			}
		else
			{
			$ShowContent = $ArrayReturn[1];
			}
		// Add header/footer to the email (if exists in the user group) - End }

		// Perform personalization (disable personalization and links if subscriber information not provided) - Start
		$ShowContent = Personalization::Personalize($ShowContent, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $ArrayRandomSubscriber, $this->ArrayUserInformation, $ArrayList, $CampaignInformation, array(), true);
		// Perform personalization (disable personalization and links if subscriber information not provided) - End

		if ($Type == 'plain')
			{
			$ShowContent = '<pre>'.$ShowContent.'</pre>';
			}

		print $ShowContent;
		exit;
		}

	// Interface parsing - Start {
	$ArrayViewData 	= array(
							'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserBrowseCampaigns'],
							'UserInformation'				=> $this->ArrayUserInformation,
							'CampaignInformation'			=> $CampaignInformation,
							'Mode'							=> $Type,
							'Flow'							=> $_SESSION[SESSION_NAME]['CampaignInformation']['Flow'],
							);

	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	$this->render('user/campaign_preview', $ArrayViewData);
	// Interface parsing - End }
	
	}

} // end of class User
?>