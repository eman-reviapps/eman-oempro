<?php
/**
 * List controller
 *
 * @author Mert Hurturk
 */

class Controller_List extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('client_auth');
		Core::LoadObject('clients');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('lists');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve client information - Start {
		$this->array_client_information = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
		// Retrieve client information - End }

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('UserID'=>$this->array_client_information['RelOwnerUserID']), true);
		// Retrieve user information - End }
	}

	/**
	 * List overview controller
	 *
	 * @author Mert Hurturk
	 **/
	function statistics($list_id = '')
	{
		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.list.get',
			'protected'	=>	false,
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));
			
		if ($array_return['Success'] == false)
		{
			$this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}

		$array_list_information = $array_return['List'];
		
		$array_list_information['OpenPerformanceDifference'] = $array_list_information['OverallOpenPerformance'] - $array_list_information['OverallAccountOpenPerformance'];
		$array_list_information['ClickPerformanceDifference'] = $array_list_information['OverallClickPerformance'] - $array_list_information['OverallAccountClickPerformance'];
		$array_list_information['HighestOpenDay'] = Statistics::RetrieveOpenPerformanceDaysOfList($list_id, $this->array_user_information['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
		$array_list_information['HighestOpenDay'] = $array_list_information['HighestOpenDay'][0][0];
		$array_list_information['HighestClickDay'] = Statistics::RetrieveClickPerformanceDaysOfList($list_id, $this->array_user_information['UserID'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0879']);
		$array_list_information['HighestClickDay'] = $array_list_information['HighestClickDay'][0][0];
		// Retrieve list information - End }

		// Interface parsing - Start {
		$array_view_data = array(
							'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientListStatistics'],
							'ClientInformation'				=> $this->array_client_information,
							'UserInformation'				=> $this->array_user_information,
							'ListInformation'				=> $array_list_information,
							'CurrentMenuItem'				=> 'Lists',
							'SubSection'					=> 'Statistics'
							);

			// Check if there is any message in the message buffer - Start {
			if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
			{
				$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
				unset($_SESSION['PageMessageCache']);
			}
			// Check if there is any message in the message buffer - End }


		$this->render('client/list_statistics', $array_view_data);
		// Interface parsing - End }
	}

	/**
	 * List subscription, unsubscription chart data
	 *
	 * @author Mert Hurturk
	 **/
	function chartdata($list_id = '', $days = 30, $statistic = 'subscriptions-unsubscriptions')
	{
		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.list.get',
			'protected'	=>	false,
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));
			
		if ($array_return['Success'] == false)
		{
			$this->display_user_message(ApplicationHeader::$ArrayLanguageStrings['Screen']['0965'], ApplicationHeader::$ArrayLanguageStrings['Screen']['0966']);
			return;
		}

		$array_list_information = $array_return['List'];
		// Retrieve list information - End }

		$ToDate = date('Y-m-d H:i:s' , time() + ($days * 86400));

		if ($statistic == 'subscriptions-unsubscriptions')
		{
			$ArrayChartGraphColors = explode(',', CHART_COLORS);
			
			$ArrayData = array();
			$ArrayData['Series']	 = array();
			$ArrayData['Graphs']	 = array(
											0 => array('title' => '', 'axis' => 'left'),
											1 => array('title' => '', 'axis' => 'left'),
											);
			$ArrayData['GraphData']	= array();

			for ($TMPCounter = $days; $TMPCounter >= 0; $TMPCounter--)
			{
				$ArrayData['Series'][] = '<b>'.date('M j', strtotime($ToDate.' -'.$TMPCounter.' days')).'</b>';
				$ArrayData['GraphData'][0][] = array(
													'Value'			=> (isset($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalSubscriptions']) == true ? $array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalSubscriptions'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0149'], number_format($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalSubscriptions'])),
																			),
													);
				$ArrayData['GraphData'][1][] = array(
													'Value'			=> (isset($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalUnsubscriptions']) == true ? $array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalUnsubscriptions'] : 0),
													'Parameters'	=> array(
																			'description'	=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($array_list_information['ActivityStatistics'][date('Y-m-d', strtotime($ToDate.' -'.$TMPCounter.' days'))]['TotalUnsubscriptions'])),
																			),
													);
			}

			Core::LoadObject('chart_data_generator');
			$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
			header('Content-type: text/xml');
			print($XML);
			exit;
		}
		elseif ($statistic == 'bounces')
		{
			// Load chart graph colors from the config file - Start {
			$ArrayChartGraphColors = explode(',', CHART_COLORS);
			// Load chart graph colors from the config file - End }

			$ArrayXML = array();
			$ArrayXML[]			= '<?xml version="1.0" encoding="UTF-8"?>';
			$ArrayXML[]			= '<pie>';

			$TMPCounter = 0;
			if (count($array_list_information['BounceStatistics']) > 0)
			{
				foreach ($array_list_information['BounceStatistics'] as $Key=>$ArrayEachData)
					{
					$ArrayXML[]			= '<slice title="'.$ArrayEachData['status'].'" color="'.$ArrayChartGraphColors[$TMPCounter].'">'.$ArrayEachData['count'].'</slice>';
					$TMPCounter++;
					}
			}
			else
			{
				$ArrayXML[]			= '<slice title="'.ApplicationHeader::$ArrayLanguageStrings['Screen']['1171'].'" color="'.$ArrayChartGraphColors[0].'">100</slice>';
			}
			$ArrayXML[]			= '</pie>';
			header('Content-type: text/xml');
			print($XML = implode("\n", $ArrayXML));
			exit;
		}
		elseif ($statistic == 'email-domain')
		{
			// Load chart graph colors from the config file - Start {
			$ArrayChartGraphColors = explode(',', CHART_COLORS);
			// Load chart graph colors from the config file - End }

			$ArrayXML = array();
			$ArrayXML[]			= '<?xml version="1.0" encoding="UTF-8"?>';
			$ArrayXML[]			= '<pie>';

			$TMPCounter = 0;
			if (count($array_list_information['EmailDomainStatistics']) > 0)
			{
				foreach ($array_list_information['EmailDomainStatistics'] as $Key=>$ArrayEachData)
					{
					$ArrayXML[]			= '<slice title="'.$ArrayEachData['domain'].'" color="'.$ArrayChartGraphColors[$TMPCounter].'">'.$ArrayEachData['count'].'</slice>';
					$TMPCounter++;
					}
			}
			else
			{
				$ArrayXML[]			= '<slice title="" color="'.$ArrayChartGraphColors[0].'"></slice>';
			}
			$ArrayXML[]			= '</pie>';
			header('Content-type: text/xml');
			print($XML = implode("\n", $ArrayXML));
			exit;
		}
	}

	/**
	 * Sparkline controller
	 *
	 * @author Mert Hurturk
	 **/
	function sparkline($statistic, $list_id)
	{
		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.list.get',
			'protected'	=>	false,
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));
			
		if ($array_return['Success'] == false)
		{
			exit;
		}

		$array_list_information = $array_return['List'];
		// Retrieve list information - End }

		$sparkline_data = array();
		if ($statistic == 'opens')
		{
			foreach ($array_list_information['OpenStatistics'] as $each)
			{
				$sparkline_data[] = $each['Total'];
			}
		}
		elseif ($statistic == 'clicks')
		{
			foreach ($array_list_information['ClickStatistics'] as $each)
			{
				$sparkline_data[] = $each['Total'];
			}
		}
		elseif ($statistic == 'forwards')
		{
			foreach ($array_list_information['ForwardStatistics'] as $each)
			{
				$sparkline_data[] = $each['Total'];
			}
		}
		elseif ($statistic == 'views')
		{
			foreach ($array_list_information['BrowserViewStatistics'] as $each)
			{
				$sparkline_data[] = $each['Total'];
			}
		}
		$sparkline_data = implode('x', $sparkline_data);
		$this->load->library('OemproSparkLine', array());
		$this->oemprosparkline->draw(array('data'=>$sparkline_data));
	}

	/**
	 * Export statistics controller
	 * @author Mert Hurturk
	 **/
	function exportstatistics($list_id, $type = 'csv')
	{
		// Retrieve list information - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.list.get',
			'protected'	=>	false,
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	array(
				'listid' => $list_id
				)
			));
			
		$array_list_information = $array_return['List'];
		// Retrieve list information - End }

		if ($type == 'csv')
		{
			$csv_string = ApplicationHeader::$ArrayLanguageStrings['Screen']['1150']."\n\n".Statistics::ConvertToCSV($array_list_information['ActivityStatistics']);
			$csv_string .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0919']."\n\n".Statistics::ConvertToCSV($array_list_information['OpenStatistics']);
			$csv_string .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0920']."\n\n".Statistics::ConvertToCSV($array_list_information['ClickStatistics']);
			$csv_string .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0921']."\n\n".Statistics::ConvertToCSV($array_list_information['BrowserViewStatistics']);
			$csv_string .= "\n\n".ApplicationHeader::$ArrayLanguageStrings['Screen']['0922']."\n\n".Statistics::ConvertToCSV($array_list_information['ForwardStatistics']);

			// Make CSV data file downloadable - Start 
			header("Content-type: application/octetstream");
			header("Content-Disposition: attachment; filename=\"subscriber_list_statistics.csv\"");
			header('Content-length: ' . strlen($csv_string));
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Pragma: public");
			// Make CSV data file downloadable - End
			print $csv_string;
			exit;
		}
		else if ($type == 'xml')
		{
			$xml_string = '<statistics>';
			$xml_string .= '<activity_statistics>'.Statistics::ConvertToXML($array_list_information['ActivityStatistics']).'</activity_statistics>';
			$xml_string .= '<open_statistics>'.Statistics::ConvertToXML($array_list_information['OpenStatistics']).'</open_statistics>';
			$xml_string .= '<click_statistics>'.Statistics::ConvertToXML($array_list_information['ClickStatistics']).'</click_statistics>';
			$xml_string .= '<browser_view_statistics>'.Statistics::ConvertToXML($array_list_information['BrowserViewStatistics']).'</browser_view_statistics>';
			$xml_string .= '<forward_statistics>'.Statistics::ConvertToXML($array_list_information['ForwardStatistics']).'</forward_statistics>';
			$xml_string .= '</statistics>';
			// Make XML data file downloadable - Start
			header("Content-type: application/octetstream");
			header("Content-Disposition: attachment; filename=\"subscriber_list_statistics.xml\"");
			header('Content-length: ' . strlen($xml_string));
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Pragma: public");
			// Make XML data file downloadable - End
			print $xml_string;
			exit;
		}
	}
}