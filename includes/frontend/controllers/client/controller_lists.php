<?php
/**
 * Lists controller
 *
 * @author Mert Hurturk
 */

class Controller_Lists extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
		{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('client_auth');
		Core::LoadObject('clients');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('statistics');
		Core::LoadObject('suppression_list');
		Core::LoadObject('lists');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		ClientAuth::IsLoggedIn(false, InterfaceAppURL(true).'/client/');
		// Check the login session, redirect based on the login session status - End

		// Retrieve client information - Start {
		$this->array_client_information = Clients::RetrieveClient(array('*'), array('CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword))'=>$_SESSION[SESSION_NAME]['ClientLogin']), true);
		// Retrieve client information - End }

		// Retrieve user information - Start {
		$this->array_user_information = Users::RetrieveUser(array('*'), array('UserID'=>$this->array_client_information['RelOwnerUserID']), true);
		// Retrieve user information - End }

		}

	function index()
		{
		$this->browse();
		}

	/**
	 * Browse controller
	 *
	 * @author Mert Hurturk
	 **/
	function browse()
		{
		// Retrieve lists - Start {
		$array_return = API::call(array(
			'format'	=>	'array',
			'command'	=>	'client.lists.get',
			'protected'	=>	true,
			'username'	=>	$this->array_client_information['ClientUsername'],
			'password'	=>	$this->array_client_information['ClientPassword'],
			'parameters'=>	array(
				'orderfield'		=>	'Name',
				'ordertype'			=>	'ASC'
				)
			));
		if ($array_return['Success'] == false)
			{
			// Error occurred
			}
		else
			{
			$array_lists = $array_return['Lists'];
			$total_list_count = $array_return['TotalListCount'];
			unset($array_return);
			}
		// Retrieve lists - End }
		
		$array_view_data 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['ClientBrowseLists'],
								'ClientInformation'				=> $this->array_client_information,
								'UserInformation'				=> $this->array_user_information,
								'Lists'							=> $array_lists,
								'CurrentMenuItem'				=> 'Lists'
								);

		$array_view_data = array_merge($array_view_data, InterfaceDefaultValues());

		if (isset($array_event_return))
			{
			foreach ($array_event_return as $key=>$value)
				{
				$array_view_data[$key] = $value;
				}
			}

		// Check if there is any message in the message buffer - Start {
			if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
				{
				$array_view_data[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
				unset($_SESSION['PageMessageCache']);
				}
		// Check if there is any message in the message buffer - End }

		if (isset($array_event_return) == true)
			{
			$array_view_data[($array_event_return[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $array_event_return[1];
			}

		$this->render('client/lists', $array_view_data);
		}
	
	/**
	 * Delete list event
	 *
	 * @author Mert Hurturk
	 */
	function _event_delete_lists($array_list_ids, $redirect_on_success = true, $perform_form_validation = true)
		{
		if ($perform_form_validation == true)
			{
			// Field validations - Start {
			$array_form_rules = array(
									array
										(
										'field'		=> 'SelectedLists[]',
										'label'		=> 'lists',
										'rules'		=> 'required',
										),
									);

			$this->form_validation->set_rules($array_form_rules);
			$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0936']);
			// Field validations - End }

			// Run validation - Start {
			if ($this->form_validation->run() == false)
				{
				return array(false, validation_errors());
				}
			// Run validation - End }
			}

		// Delete lists - Start {
		$array_return = API::call(array(
			'format'	=>	'object',
			'command'	=>	'lists.delete',
			'protected'	=>	true,
			'username'	=>	$this->array_user_information['Username'],
			'password'	=>	$this->array_user_information['Password'],
			'parameters'=>	array(
				'lists'		=> implode(',', $array_list_ids)
				)
			));

		if ($array_return->Success == false)
			{
			// API Error occurred
			$ErrorCode = (strtolower(gettype($array_return->ErrorCode)) == 'array' ? $array_return->ErrorCode[0] : $array_return->ErrorCode);
			switch ($ErrorCode)
				{
				case '1':
					return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0936']);
					break;
				default:
					break;
				}
			}
		else
			{
			// API Success
			if ($redirect_on_success == true)
				{
				$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0937']);

				$this->load->helper('url');
				redirect(InterfaceAppURL(true).'/user/lists/browse/', 'location', '302');
				}
			else
				{
				return true;
				}
			}
		// Delete campaigns - End }
		}
}
?>