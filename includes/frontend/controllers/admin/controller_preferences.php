<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Preferences extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'UpdatePreferences')
		{
		$ArrayEventReturn = $this->_EventEditPreferences();
		}
	// Events - End }

	// Retrieve available language packs in the system - Start {
	$ArrayLanguages = Core::DetectLanguages();	
	// Retrieve available language packs in the system - End }
	

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPreferences'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'Preferences',
							'Languages'				=> $ArrayLanguages,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Update preferences event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditPreferences()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage'	=> 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'DEFAULT_LANGUAGE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0421'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'ADMIN_CAPTCHA',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0427'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'USER_CAPTCHA',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0430'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'RUN_CRON_IN_USER_AREA',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1480'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FORBIDDEN_FROM_ADDRESSES',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1618'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'USERAREA_FOOTER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1621'],
								'rules'		=> '',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }
	
	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'DEFAULT_LANGUAGE'						=> $this->input->post('DEFAULT_LANGUAGE'),
							'DISPLAY_TRIGGER_SEND_ENGINE_LINK'		=> $this->input->post('DISPLAY_TRIGGER_SEND_ENGINE_LINK'),
							'ADMIN_CAPTCHA'							=> $this->input->post('ADMIN_CAPTCHA'),
							'USER_CAPTCHA'							=> $this->input->post('USER_CAPTCHA'),
							'RUN_CRON_IN_USER_AREA'					=> $this->input->post('RUN_CRON_IN_USER_AREA'),
							'FORBIDDEN_FROM_ADDRESSES'				=> $this->input->post('FORBIDDEN_FROM_ADDRESSES'),
							'USERAREA_FOOTER'						=> $this->input->post('USERAREA_FOOTER'),
							);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0431'],
					);
		}
	// Update settings - End }		
	}



} // end of class User
