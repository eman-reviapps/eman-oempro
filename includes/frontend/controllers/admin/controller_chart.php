<?php
/**
 * Chart controller
 *
 * @author Cem Hurturk
 */

class Controller_Chart extends MY_Controller
{
/**
 * Array of chart graph colors
 *
 * @var string
 */
var $ArrayChartGraphColors		= array();

/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	// AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	
	// Load chart graph colors from the config file - Start {
	$this->ArrayChartGraphColors = explode(',', CHART_COLORS);
	// Load chart graph colors from the config file - End }	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	}

function Settings($ChartType = 'line', $ReloadInterval = 0)
	{
	if ($ChartType == 'line')
		{
		$XML = '<?xml version="1.0"?>
			<settings>'.
				($ReloadInterval > 0 ? '<reload_data_interval>'.$ReloadInterval.'</reload_data_interval>' : '')
				.'<add_time_stamp>false</add_time_stamp>
				<text_size>10</text_size>
				<text_color>'.CHART_TEXT_COLOR.'</text_color>
				<colors>'.CHART_COLORS.'</colors>
				<preloader_on_reload>1</preloader_on_reload>
				<background>
					<alpha>0</alpha>
					<border_alpha>0</border_alpha>
				</background>
				<plot_area>
					<margins>
						<left>15</left>
						<right>15</right>
						<top>15</top>
						<bottom>30</bottom>
					</margins>
				</plot_area>
				<grid>
					<x>
						<enabled>true</enabled>
						<alpha>5</alpha>
						<approx_count>5</approx_count>
					</x>
					<y_left>
						<enabled>true</enabled>
						<alpha>10</alpha>
						<approx_count>3</approx_count>
						<dashed>true</dashed>
						<dash_length>1</dash_length>
					</y_left>
					<y_right>
						<enabled>false</enabled>
						<alpha>10</alpha>
						<approx_count>3</approx_count>
						<dashed>true</dashed>
						<dash_length>1</dash_length>
					</y_right>
				</grid>
				<axes>
					<x>
						<tick_length>10</tick_length>
						<width>1</width>
						<color>'.CHART_AXES_COLOR.'</color>
					</x>
					<y_left>
						<width>0</width>
						<color>FFFFFF</color>
					</y_left>
					<y_right>
						<width>0</width>
						<color>FFFFFF</color>
					</y_right>
				</axes>
				<values>
					<x>
						<frequency>1</frequency>
						<skip_first>1</skip_first>
						<skip_last>1</skip_last>
						<inside>false</inside>
					</x>
					<y_left>
						<inside>1</inside>
						<frequency>1</frequency>
					</y_left>
					<y_right>
						<inside>1</inside>
						<frequency>1</frequency>
					</y_right>
				</values>
				<indicator>
					<x_balloon_enabled>true</x_balloon_enabled>
					<color>'.CHART_INDICATOR_COLOR.'</color>
					<x_balloon_text_color>'.CHART_INDICATOR_TEXTCOLOR.'</x_balloon_text_color>
					<zoomable>0</zoomable>
					<line_alpha>50</line_alpha>
					<selection_color>0D8ECF</selection_color>
					<selection_alpha>20</selection_alpha>
				</indicator>
				<balloon>
					<only_one>false</only_one>
					<corner_radius>2</corner_radius>
				</balloon>
				<scroller>
					<enabled>0</enabled>
				</scroller>
				<legend>
					<enabled>0</enabled>
				</legend>
				<zoom_out_button>
					<text_color_hover>FF0F00</text_color_hover>
				</zoom_out_button>
				<graphs>';
			for ($i=0; $i<=10; $i++)
				{
				$XML .= '
						<graph gid="'.$i.'">
							<color_hover>'.$this->ArrayChartGraphColors[$i].'</color_hover>
							<line_width>2</line_width>
							<line_alpha>100</line_alpha>
							<fill_alpha>0</fill_alpha>
							<fill_color>'.$this->ArrayChartGraphColors[$i].'</fill_color>
							<balloon_color>'.$this->ArrayChartGraphColors[$i].'</balloon_color>
							<balloon_alpha>80</balloon_alpha>
							<visible_in_legend>0</visible_in_legend>
							<bullet>round_outlined</bullet>
							<bullet_size>6</bullet_size>
							<bullet_color>'.$this->ArrayChartGraphColors[$i].'</bullet_color>
							<bullet_alpha>100</bullet_alpha>
							<balloon_text>{description}</balloon_text>
						</graph>
					';
				}
			$XML .= '</graphs>
				<labels>
					<label lid="0">
						<y>25</y>
						<text_size>13</text_size>
						<align>center</align>
					</label>
				</labels>
				<error_messages>
					<color>e1e1e1</color>
					<text_color>000000</text_color>
					<text_size>12</text_size>
				</error_messages>
				<strings>
					<no_data/>
					<export_as_image/>
					<collecting_data/>
				</strings>
				<context_menu>
					<default_items>
						<zoom>false</zoom>
						<print>false</print>
					</default_items>
				</context_menu>
			</settings>
				';
		}
	if ($ChartType == 'bar')
		{
		$XML = '<?xml version="1.0"?>
			<settings>'.
				($ReloadInterval > 0 ? '<reload_data_interval>'.$ReloadInterval.'</reload_data_interval>' : '')
				.'<add_time_stamp>false</add_time_stamp>
				<text_size>10</text_size>
				<text_color>'.CHART_TEXT_COLOR.'</text_color>
				<colors>'.CHART_COLORS.'</colors>
				<preloader_on_reload>0</preloader_on_reload>
				<background>
					<alpha>0</alpha>
					<border_alpha>0</border_alpha>
				</background>
				<plot_area>
					<margins>
						<left>15</left>
						<right>15</right>
						<top>15</top>
						<bottom>30</bottom>
					</margins>
				</plot_area>
				<column>
					<width>70</width>
				</column>
				<grid>
					<category>
						<width>1</width>
						<alpha>10</alpha>
						<approx_count>3</approx_count>
						<dashed>true</dashed>
						<dash_length>1</dash_length>
					</category>
					<value>
						<width>1</width>
						<alpha>10</alpha>
						<approx_count>3</approx_count>
						<dashed>true</dashed>
						<dash_length>1</dash_length>
					</value>
				</grid>
				<axes>
					<category>
						<tick_length>10</tick_length>
						<width>1</width>
						<color>'.CHART_AXES_COLOR.'</color>
					</category>
					<value>
						<width>0</width>
						<color>FFFFFF</color>
					</value>
				</axes>
				<values>
					<category>
						<frequency>1</frequency>
						<skip_first>1</skip_first>
						<skip_last>1</skip_last>
						<inside>false</inside>
					</category>
					<value>
						<inside>1</inside>
						<frequency>1</frequency>
					</value>
				</values>
				<balloon>
					<only_one>false</only_one>
					<corner_radius>2</corner_radius>
				</balloon>
				<scroller>
					<enabled>0</enabled>
				</scroller>
				<legend>
					<enabled>0</enabled>
				</legend>
				<zoom_out_button>
					<text_color_hover>FF0F00</text_color_hover>
				</zoom_out_button>
				<graphs>';
			for ($i=0; $i<=10; $i++)
				{
				$XML .= '
						<graph gid="'.$i.'">
							<color_hover>'.$this->ArrayChartGraphColors[$i].'</color_hover>
							<line_width>2</line_width>
							<line_alpha>100</line_alpha>
							<fill_alpha>0</fill_alpha>
							<fill_color>'.$this->ArrayChartGraphColors[$i].'</fill_color>
							<balloon_color>'.$this->ArrayChartGraphColors[$i].'</balloon_color>
							<balloon_alpha>80</balloon_alpha>
							<visible_in_legend>0</visible_in_legend>
							<balloon_text>{description}</balloon_text>
						</graph>
					';
				}
			$XML .= '</graphs>
				<labels>
					<label lid="0">
						<y>25</y>
						<text_size>13</text_size>
						<align>center</align>
					</label>
				</labels>
				<error_messages>
					<color>e1e1e1</color>
					<text_color>000000</text_color>
					<text_size>12</text_size>
				</error_messages>
				<strings>
					<no_data/>
					<export_as_image/>
					<collecting_data/>
				</strings>
				<context_menu>
					<default_items>
						<zoom>false</zoom>
						<print>false</print>
					</default_items>
				</context_menu>
			</settings>
				';
		}
	else if ($ChartType == 'pie')
		{
		$XML = '<?xml version="1.0"?>
			<settings>'.
				($ReloadInterval > 0 ? '<reload_data_interval>'.$ReloadInterval.'</reload_data_interval>' : '')
			  .'<background>
			    <alpha>0</alpha>
			    <border_alpha>0</border_alpha>
			  </background>
			  <legend>
			    <enabled>0</enabled>
			  </legend>
			  <pie>
				<outline_alpha>100</outline_alpha>
			    <colors>'.implode(',',array_reverse($this->ArrayChartGraphColors)).'</colors>
				<radius>45%</radius>
			    <y>50%</y>
			    <gradient/>
			  </pie>
			  <balloon>
			    <show><![CDATA[{percents}%<br>{title}]]></show>
			  </balloon>
			  <data_labels>
			    <enabled>0</enabled>
			  </data_labels>
			  <animation>
			    <pull_out_on_click>false</pull_out_on_click>
			  </animation>
			</settings>
			';
		}

	header('Content-type: text/xml');
	print($XML);
	exit;
	}


} // end of class User
?>