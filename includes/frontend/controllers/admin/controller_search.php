<?php
/**
 * Search controller
 *
 * @author Mert Hurturk
 */

class Controller_Search extends MY_Controller
{
/**
 * Constructor
 *
 * @author Mert Hurturk
 */
function __construct()
	{
	parent::__construct();

	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Mert Hurturk
 */
function index()
	{
	// Events - Start {
	// Events - End }

	// Search users - Start {
	$Users = Database::$Interface->GetRows_Enhanced(array(
		'Fields' => array('*'),
		'Tables' => array(MYSQL_TABLE_PREFIX.'users'),
		'Criteria' => array(
			array('Column' => 'FirstName', 'Operator' => 'LIKE', 'Value' => '%'.$this->input->post('keyword').'%'),
			array('Column' => 'LastName', 'Operator' => 'LIKE', 'Value' => '%'.$this->input->post('keyword').'%', 'Link' => 'OR'),
			array('Column' => 'EmailAddress', 'Operator' => 'LIKE', 'Value' => '%'.$this->input->post('keyword').'%', 'Link' => 'OR'),
			array('Column' => 'Username', 'Operator' => 'LIKE', 'Value' => '%'.$this->input->post('keyword').'%', 'Link' => 'OR')
			),
		'Pagination' => array('Offset' => 0, 'Rows' => 20)
		));
	// Search users - Start }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'Users'	=>	$Users
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	$this->render('admin/search', $ArrayViewData);
	// Interface parsing - End
	}


} // end of class User
?>