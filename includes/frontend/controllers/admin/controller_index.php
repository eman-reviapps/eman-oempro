<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Index extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();

	// Load other modules - Start
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	$this->load->library('encrypt');
	// Load other modules - End

	// Check if "remember me" cookie is set. If it's set, validate the user information and login - Start
	if ($_COOKIE[COOKIE_ADMIN_LOGINREMIND] != '')
		{
		// Load other modules - Start
		Core::LoadObject('admins');
		// Load other modules - End

		$RememberValue = $this->encrypt->decode($_COOKIE[COOKIE_ADMIN_LOGINREMIND], SCRTY_SALT);
		$ArrayCriterias = array(
								'AdminID' => mysql_real_escape_string($RememberValue),
								);
		$ArrayAdmin = Admins::RetrieveAdmin(array('*'), $ArrayCriterias);

		if ($ArrayAdmin != false)
			{
			// Perform the admin login - Start
			AdminAuth::Login($ArrayAdmin['AdminID'], $ArrayAdmin['Username'], $ArrayAdmin['Password']);
			// Perform the admin login - End

			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/admin/overview/', 'location', '302');
			exit;
			}
		}
	// Check if "remember me" cookie is set. If it's set, validate the user information and login - End
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(InterfaceAppURL(true).'/admin/overview/', false);
	// Check the login session, redirect based on the login session status - End

	Plugins::HookListener('Action', 'Admin.Login.Before');

	// Events - Start {
	if ($this->input->post('Command') == 'Login')
		{
		$ArrayEventReturn = $this->_EventLogin($this->input->post('Username'), $this->input->post('Password'), $this->input->post('Captcha'), $this->input->post('RememberMe'));
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminLogin'],
							);
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	// Check if there is any message in the message buffer - Start {
	if ($_SESSION['PageMessageCache'][1] != '')
		{
		$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
		unset($_SESSION['PageMessageCache']);
		}
	// Check if there is any message in the message buffer - End }

	$this->render('admin/login', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Login event
 *
 * @param string $Username
 * @param string $Password
 * @param string $Captcha
 * @param string $RememberMe
 * @return void
 * @author Cem Hurturk
 */
function _EventLogin($Username, $Password, $Captcha = '', $RememberMe = '')
	{
	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'Username',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'Password',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'RememberMe',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'],
								'rules'		=> '',
								),
							);

	if (ADMIN_CAPTCHA == true)
		{
		$ArrayFormRules[] = array
								(
								'field'		=> 'Captcha',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0279'],
								'rules'		=> 'required|callback__CallbackCaptchaCheck',
								);
		}
	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		Plugins::HookListener('Action', 'Admin.Login.ValidationError');
		return array(false);
		}
	// Run validation - End }

	// Validate login information - Start {
	$ArrayAPIVars = array(
						'username'		=> $Username,
						'password'		=> $Password,
						'captcha'		=> $Captcha,
						'rememberme'	=> $RememberMe,
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'admin.login',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect login information
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		Plugins::HookListener('Action', 'Admin.Login.InvalidUser', array($ErrorCode));
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0282'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0283'],
							);
				break;
			case '3':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0281'],
							);
				break;
			case '4':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0284'],
							);
				break;
			case '5':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0280'],
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0281'],
							);
				break;
			}
		}
	else
		{
		// Correct login information

		// Reset the CAPTCHA string in the session to avoid displaying the same CAPTCHA in the future - Start {
		unset($_SESSION[SESSION_NAME]['AdminCaptcha']);
		// Reset the CAPTCHA string in the session to avoid displaying the same CAPTCHA in the future - End }

		// Save remember cookie for 2 weeks if the checkbox is checked - Start
		if ($this->input->post('RememberMe') != '')
			{
			$RememberValue = $this->encrypt->encode($ArrayReturn->AdminInfo->AdminID, SCRTY_SALT);
			setcookie(COOKIE_ADMIN_LOGINREMIND, $RememberValue, time() + (86400 * 14), '/');
			}
		else
			{
			setcookie(COOKIE_ADMIN_LOGINREMIND, '', time() - 3600, '/');
			}
		// Save remember cookie for 2 weeks if the checkbox is checked - End

		$this->load->helper('url');
		// TODO: Change the target redirect to "overview" from "users" below
		redirect(InterfaceAppURL(true).'/admin/overview/', 'location', '302');
		}
	// Validate login information - End }
	}

/**
 * Verifies the CAPTCHA
 *
 * @return void
 * @author Cem Hurturk
 */
function _CallbackCaptchaCheck($Captcha)
	{
	if ($_SESSION[SESSION_NAME]['AdminCaptcha'] != $Captcha)
		{
		$this->form_validation->set_message('_CallbackCaptchaCheck', ApplicationHeader::$ArrayLanguageStrings['Screen']['0280']);
		return false;
		}
	else
		{
		return true;
		}
	}

} // end of class User
?>