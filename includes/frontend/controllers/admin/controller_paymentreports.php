<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_PaymentReports extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	// Events - End }

	// Calculate revenues - Start {
	Core::LoadObject('statistics');
	Core::LoadObject('payments');
	
	$current_months_revenue = Statistics::RetrieveRevenueStatistics(date('Y-m'), date('Y-m'));
	$current_months_revenue = $current_months_revenue[date('Y-m')]['TotalAmount'];
		$previous_month = date('Y-m', strtotime(date('Y-m').' -1 month 00:00:00'));
	$previous_months_revenue = Statistics::RetrieveRevenueStatistics($previous_month, $previous_month);
	$previous_months_revenue = $previous_months_revenue[$previous_month]['TotalAmount'];
	$revenue_difference = $current_months_revenue - $previous_months_revenue;
	$revenue_difference_ratio = number_format((100 * $revenue_difference) / $current_months_revenue);
	
	$total_revenue = Statistics::RetrieveTotalRevenue();

	$total_paid_revenue = Statistics::RetrieveTotalPaidRevenue();
	$total_paid_revenue = $total_paid_revenue == NULL ? 0 : $total_paid_revenue;
	$total_not_paid_revenue = Statistics::RetrieveTotalNotPaidRevenue();
	$total_not_paid_revenue = $total_not_paid_revenue == NULL ? 0 : $total_not_paid_revenue;

	$total_paid_revenue_ratio = round((100*$total_paid_revenue)/$total_revenue);
	$total_not_paid_revenue_ratio = 100 - $total_paid_revenue_ratio;
	
	$top_user_revenue_information = Statistics::RetrieveTopUserRevenueForDate(date('Y-m'));

	$all_time_top_revenue_users = Statistics::RetrieveAllTimeTopUserRevenues(5);

	$all_time_receivables = Payments::RetrieveAllTimeReceivables();
	// Calculate revenues - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'					=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPaymentReports'],
							'CurrentMenuItem'			=> 'Drop',
							'CurrentDropMenuItem'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['9079'],
							'RevenueDifferenceRatio'	=> $revenue_difference_ratio,
							'CurrentMonthsRevenue'		=> $current_months_revenue,
							'PreviousMonthsRevenue'		=> $previous_months_revenue,
							'TotalRevenue'				=> $total_revenue,
							'TotalNotPaidRevenue'		=> $total_not_paid_revenue,
							'TotalNotPaidRevenueRatio'	=> $total_not_paid_revenue_ratio,
							'TotalPaidRevenueRatio'		=> $total_paid_revenue_ratio,
							'TopUserInformation'		=> $top_user_revenue_information,
							'AllTimeTopUsers'			=> $all_time_top_revenue_users,
							'AllTimeReceivables'		=> $all_time_receivables
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	if (isset($ArrayEventReturn) && count($ArrayEventReturn) > 0)
		{
		foreach ($ArrayEventReturn as $Key=>$Value)
			{
			$ArrayViewData[$Key] = $Value;
			}
		}
							
	$this->render('admin/payment_rerports', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * undocumented function
 *
 * @return void
 * @author Mert Hurturk
 **/
function chartdata($type)
	{
	Core::LoadObject('chart_data_generator');
	
	// Load chart graph colors from the config file - Start {
	$ArrayChartGraphColors = explode(',', CHART_COLORS);
	// Load chart graph colors from the config file - End }
	
	if ($type == 'Revenue')
		{
		// Load revenue statistics - Start {
		Core::LoadObject('statistics');
		$ArrayStatistics = Statistics::RetrieveRevenueStatistics(date('Y-m', strtotime(date('Y-m').' -1 year 00:00:00')), date('Y-m'));
		// Load revenue statistics - End }

		$ArrayData = array();
		$ArrayData['Series']	 = array();
		$ArrayData['Graphs']	 = array(
										0 => array('title' => '', 'axis' => 'left', 'line_width' => 3, 'bullet_size' => 10, 'bullet_color' => $ArrayChartGraphColors[1], 'balloon_color' => $ArrayChartGraphColors[1], 'color' => $ArrayChartGraphColors[1], 'color_hover' => $ArrayChartGraphColors[1], 'fill_color' => $ArrayChartGraphColors[1], 'fill_alpha' => 20, 'line_alpha' => 100),
										);
		$ArrayData['GraphData']	= array();

		for ($TMPCounter = 12; $TMPCounter >= 0; $TMPCounter--)
			{
			$ArrayData['Series'][] = '<b>'.date('M y', strtotime(date('Y-m').' -'.$TMPCounter.' months')).'</b>';
			$ArrayData['GraphData'][0][] = array(
												'Value'			=> $ArrayStatistics[date('Y-m', strtotime(date('Y-m').' -'.$TMPCounter.' months'))]['TotalAmount'],
												'Parameters'	=> array(
																		'description'	=> number_format($ArrayStatistics[date('Y-m', strtotime(date('Y-m').' -'.$TMPCounter.' months'))]['TotalAmount'], 2, '.', ',').' '.PAYMENT_CURRENCY,
																		),
												);
			}

		$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
		header('Content-type: text/xml');
		print($XML);
		exit;
		}
	else if ($type == 'RevenueStatus')
		{
		// Load revenue statistics - Start {
		Core::LoadObject('statistics');
		$ArrayStatistics = array();
		// Load revenue statistics - End }

		$ArrayData = array();

		// Calculate revenues - Start {
		Core::LoadObject('statistics');
		$total_revenue = Statistics::RetrieveTotalRevenue();

		$total_paid_revenue = Statistics::RetrieveTotalPaidRevenue();
		$total_paid_revenue = $total_paid_revenue == NULL ? 0 : $total_paid_revenue;
		$total_not_paid_revenue = Statistics::RetrieveTotalNotPaidRevenue();
		$total_not_paid_revenue = number_format($total_not_paid_revenue == NULL ? 0 : $total_not_paid_revenue);

		$total_paid_revenue_ratio = round((100*$total_paid_revenue)/$total_revenue);
		$total_not_paid_revenue_ratio = 100 - $total_paid_revenue_ratio;

		// $total_paid_revenue = number_format($total_paid_revenue, 2, '.', ',');
		$total_paid_revenue = Core::FormatCurrency($total_paid_revenue);
		$total_not_paid_revenue = Core::FormatCurrency($total_not_paid_revenue);
		// Calculate revenues - End }

		$XML = '<pie><slice title="'.$total_paid_revenue.'" color="#E434FA">'.$total_paid_revenue_ratio.'</slice><slice title="'.$total_not_paid_revenue.'" color="#ED561B">'.$total_not_paid_revenue_ratio.'</slice></pie>';
		header('Content-type: text/xml');
		print($XML);
		exit;
		}
	}

} // end of class User
?>