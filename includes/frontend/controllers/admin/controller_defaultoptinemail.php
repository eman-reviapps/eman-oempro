<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_DefaultOptInEmail extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'EditDefaultOptInEmail')
		{
		$ArrayEventReturn = $this->_EventEditDefaultOptInEmail();
		}
	// Events - End }

	// Retrieve personalization tags - Start {
	Core::LoadObject('personalization');
	$array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
	$array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'Opt');
	$array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
	$array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
	$array_tags = array(
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0670']	=>	$array_subscriber_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0671']	=>	$array_link_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0672']	=>	$array_user_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0673']	=>	$array_other_tags
		);
	// Retrieve personalization tags - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminDefaultOptInEmail'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'DefaultOptInEmail',
							'Tags'					=> $array_tags
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Default opt-in email update event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditDefaultOptInEmail()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'DEFAULT_OPTIN_EMAIL_SUBJECT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0183'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'DEFAULT_OPTIN_EMAIL_BODY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0184'],
								'rules'		=> 'required',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
	$ArrayAPIVars = array(
						'DEFAULT_OPTIN_EMAIL_SUBJECT'		=> $this->input->post('DEFAULT_OPTIN_EMAIL_SUBJECT'),
						'DEFAULT_OPTIN_EMAIL_BODY'			=> $this->input->post('DEFAULT_OPTIN_EMAIL_BODY'),
						);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			case '7':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0336'],
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		}
	// Update settings - End }

	return array(
				'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0337'],
				);
	}

} // end of class User
?>