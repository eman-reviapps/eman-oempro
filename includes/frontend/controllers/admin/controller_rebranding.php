<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Rebranding extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	Core::LoadObject('admins');
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'EditRebranding')
		{
		$ArrayEventReturn = $this->_EventEditRebranding();
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminRebranding'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'Rebranding',
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	// Check if there is any message in the message buffer - Start {
	if ($_SESSION['PageMessageCache'][1] != '')
		{
		$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
		unset($_SESSION['PageMessageCache']);
		}
	// Check if there is any message in the message buffer - End }

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Re-branding settings update event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditRebranding()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage'	=> 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'PRODUCT_NAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0156'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FORWARD_TO_FRIEND_HEADER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0194'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'FORWARD_TO_FRIEND_FOOTER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0195'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'REPORT_ABUSE_FRIEND_HEADER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0194'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'REPORT_ABUSE_FRIEND_FOOTER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0195'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'DEFAULT_SUBSCRIBERAREA_LOGOUT_URL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0068'],
								'rules'		=> '',
								),
							);

	$ArrayFormRules[] = array
							(
							'field'		=> 'USER_SIGNUP_HEADER',
							'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0194'],
							'rules'		=> '',
							);
	$ArrayFormRules[] = array
							(
							'field'		=> 'USER_SIGNUP_FOOTER',
							'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0195'],
							'rules'		=> '',
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'PRODUCT_NAME'							=> $this->input->post('PRODUCT_NAME'),
							'FORWARD_TO_FRIEND_HEADER'				=> $this->input->post('FORWARD_TO_FRIEND_HEADER'),
							'FORWARD_TO_FRIEND_FOOTER'				=> $this->input->post('FORWARD_TO_FRIEND_FOOTER'),
							'REPORT_ABUSE_FRIEND_HEADER'			=> $this->input->post('REPORT_ABUSE_FRIEND_HEADER'),
							'REPORT_ABUSE_FRIEND_FOOTER'			=> $this->input->post('REPORT_ABUSE_FRIEND_FOOTER'),
							'USER_SIGNUP_HEADER'					=> $this->input->post('USER_SIGNUP_HEADER'),
							'USER_SIGNUP_FOOTER'					=> $this->input->post('USER_SIGNUP_FOOTER'),
							'DEFAULT_SUBSCRIBERAREA_LOGOUT_URL'		=> $this->input->post('DEFAULT_SUBSCRIBERAREA_LOGOUT_URL'),
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		}
	// Update settings - End }

	$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0338']);

	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/rebranding/', 'location', '302');
	}

} // end of class User
?>