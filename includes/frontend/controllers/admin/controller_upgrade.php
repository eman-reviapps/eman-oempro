<?php
/**
 * Auto Upgrade controller
 *
 * @author Cem Hurturk
 */
class Controller_Upgrade extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	if ($this->input->post('Command') == 'Upgrade' && DEMO_MODE_ENABLED == false)
		{
		$ArrayEventReturn = $this->_EventUpgrade();
		}
	// Events - End }

	// Learn the latest available version - Start {
	Core::LoadObject('install');
	$LatestAvailableVersion = Install::WhatIsLatestVersion();
	// Learn the latest available version - End }

	// Check if new version exists - Start {
	$NewVersionExists = (Install::IsNewVersion($LatestAvailableVersion) == false ? false : true);

	if ($NewVersionExists == false)
		{
		$_SESSION['PageMessageCache'][0] = 'Error';
		$_SESSION['PageMessageCache'][1] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0484'];
		}
	// Check if new version exists - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminAbout'],
							'CurrentMenuItem'		=> '',
							'SubSection'			=> '',
							'LatestVersion'			=> $LatestAvailableVersion,
							'NewVersionExists'		=> $NewVersionExists,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	// Check if there is any message in the message buffer - Start {
	if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
		{
		$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
		unset($_SESSION['PageMessageCache']);
		}
	// Check if there is any message in the message buffer - End }

	$this->render('admin/upgrade_v2', $ArrayViewData);
	// Interface parsing - End
	}

function _EventUpgrade()
	{
	die('This feature is disabled. Please use the manual upgrade method to upgrade your Oempro version');
	return;

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'FTPHost',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0473'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FTPUsername',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0474'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FTPPassword',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0475'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FTPPath',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0485'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'FTPPort',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0478'],
								'rules'		=> 'required|integer',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Upgrade Process - Start {
	Core::LoadObject('json');

	$ArrayReturn = Install::DownloadLatestVersion(true, 'FTP', $this->input->post('FTPHost'), $this->input->post('FTPUsername'), $this->input->post('FTPPassword'), $this->input->post('FTPPort'), $this->input->post('FTPPath'));

	if ($ArrayReturn[0] != false)
		{
		return array('PageSuccessMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0494'], $ArrayReturn[2]));
		}
	else
		{
		if ($ArrayReturn[1] == 'VERSION_CHECK_FAILED')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0487']);
			}
		elseif ($ArrayReturn[1] == 'NO_NEW_VERSION')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0484']);
			}
		elseif ($ArrayReturn[1] == 'DOMAIN_NOT_AUTHORIZED')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0488']);
			}
		elseif ($ArrayReturn[1] == 'CONNECTION_FAILURE')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0489']);
			}
		elseif ($ArrayReturn[1] == 'FTP_CONNECTION_FAILED')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0490']);
			}
		elseif ($ArrayReturn[1] == 'FTP_INCORRECT_PATH')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0491']);
			}
		elseif ($ArrayReturn[1] == 'EMPTY_ARCHIVE')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0492']);
			}
		elseif ($ArrayReturn[1] == 'INCOMPATABILE_ARCHIVE')
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0492']);
			}
		else
			{
			return array('PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0493']);
			}
		}
	// Upgrade Process - End }

	}

} // end of class Controller_Upgrade
?>