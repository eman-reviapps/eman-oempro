<?php
/**
 * Password Reminder controller
 *
 * @author Cem Hurturk
 */

class Controller_PasswordReminder extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(InterfaceAppURL(true).'/admin/overview/', false);
	// Check the login session, redirect based on the login session status - End

	// Events - Start {
	if ($this->input->post('Command') == 'ResetPassword')
		{
		$ArrayEventReturn = $this->_EventResetPassword($this->input->post('EmailAddress'));
		}
	// Events - End }
	
	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPassRemind'],
							);
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/passwordreminder', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Resets the administrator password and sends the new login info by email
 *
 * @param string $MD5AdminID 
 * @return void
 * @author Cem Hurturk
 */
function reset($MD5AdminID)
	{
	Core::LoadObject('octeth_template');
	Core::LoadObject('template_engine');

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(InterfaceAppURL(true).'/admin/overview/', false);
	// Check the login session, redirect based on the login session status - End

	// Events - Start {
	if ($MD5AdminID != '')
		{
		$ArrayEventReturn = $this->_EventResetPasswordConfirmed($MD5AdminID);
		}
	// Events - End }
	
	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPassRemind'],
							);
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/passwordreminder', $ArrayViewData);
	// Interface parsing - End

	}


/**
 * Login event
 *
 * @param string $Username 
 * @param string $Password 
 * @param string $Captcha 
 * @param string $RememberMe 
 * @return void
 * @author Cem Hurturk
 */
function _EventResetPassword($EmailAddress)
	{
	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'EmailAddress',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
								'rules'		=> 'required|valid_email',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Validate login information - Start {
	$ArrayAPIVars = array(
						'emailaddress'		=>	$EmailAddress,
						'customresetlink'	=>	rawurlencode(base64_encode(InterfaceAppURL(true).'/admin/passwordreminder/reset/%s')),
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'admin.passwordremind',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect email address
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0285'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0286'],
							);
				break;
			case '3':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0287'],
							);
				break;
			default:
				break;
			}
		}
	else
		{
		// Correct login information

		$_SESSION['PageMessageCache'] = array('Success', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0288'], $this->input->post('EmailAddress')));

		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/admin/', 'location', '302');
		}
	// Validate login information - End }
	}

/**
 * Password reset link
 *
 * @param string $MD5AdminID 
 * @return void
 * @author Cem Hurturk
 */
function _EventResetPasswordConfirmed($MD5AdminID)
	{
	// Validate login information - Start {
	$ArrayAPIVars = array(
						'adminid'		=> $MD5AdminID,
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'admin.passwordreset',
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Incorrect email address
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0289'],
							);
				break;
			case '2':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0290'],
							);
				break;
			default:
				break;
			}
		}
	else
		{
		// Correct login information

		$_SESSION['PageMessageCache'] = array('Success', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0288'], $this->input->post('EmailAddress')));

		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/admin/', 'location', '302');
		}
	// Validate login information - End }
	}

} // end of class User
?>