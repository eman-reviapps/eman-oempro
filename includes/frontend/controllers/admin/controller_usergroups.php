<?php

/**
 * About controller
 *
 * @author Cem Hurturk
 */
class Controller_UserGroups extends MY_Controller {

    /**
     * Constructor
     *
     * @author Cem Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('admins');
        Core::LoadObject('admin_auth');
        Core::LoadObject('api');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve administrator information - Start {
        $this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));
        // Retrieve administrator information - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Cem Hurturk
     */
    function test() {
        Core::LoadObject('user_groups');
        $ArrayUserGroups = UserGroups::RetrieveUserGroups(array('*'), array());
    }

    /**
     * Index controller
     *
     * @return void
     * @author Cem Hurturk
     */
    function index() {
        // Load required modules - Start {
        // Load required modules - End }
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteUserGroups') {
            $ArrayEventReturn = $this->_EventDeleteUserGroup($this->input->post('SelectedUserGroups'));
        }
        // Events - End }
        // Load user groups defined in the system - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'usergroups.get',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array()
        ));

        if ($ArrayReturn->Success == false) {
            // Error occurred
        } else {
            $UserGroups = $ArrayReturn->UserGroups;
        }
        // Load user groups defined in the system - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminUserGroups'],
            'CurrentMenuItem' => 'Settings',
            'SubSection' => 'UserGroups',
            'UserGroups' => $UserGroups,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }
        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($ArrayEventReturn) == true) {
            $ArrayViewData[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
        }

        $this->render('admin/settings', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Create user group controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function create() {
        // Events - Start {
        if ($this->input->post('Command') == 'CreateUserGroup') {
            $ArrayEventReturn = $this->_EventCreateUserGroup();
        }
        // Events - End }
        // Retrieve plugins for displaying in permissions section - Start {
        Core::LoadObject('plugin');
        $ArrayEnabledPlugIns = Plugins::GetPlugInList(true);
        // Retrieve plugins for displaying in permissions section - End }
        // Retrieve ui templates for displayin in user interface section - Start {
        $ArrayTemplates = ThemeEngine::DetectTemplates();
        // Retrieve ui templates for displayin in user interface section - End }
        // Retrieve css settings for each ui template - Start {
        $ArrayCSSSettings = array();
        foreach ($ArrayTemplates as $EachTemplate) {
            $ArrayCSSSettings[$EachTemplate['Code']] = ThemeEngine::LoadCSSSettings($EachTemplate['Code'], true);
        }
        // Retrieve css settings for each ui template - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminCreateUserGroup'],
            'CurrentMenuItem' => 'Settings',
            'Plugins' => $ArrayEnabledPlugIns,
            'Templates' => $ArrayTemplates,
            'CSSSettings' => $ArrayCSSSettings
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('admin/create_user_group', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Edit user group controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function edit($user_group_id) {
        // Retrieve user group information - Start {
        Core::LoadObject('user_groups');
        $user_group_information = UserGroups::RetrieveUserGroup($user_group_id, true);
        if ($user_group_information == false) {
            // Couldn't find user group
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/usergroups/', 'location', '302');
        }
        // Retrieve user group information - End }
        // Events - Start {
        if ($this->input->post('Command') == 'SaveUserGroup') {
            $ArrayEventReturn = $this->_EventSaveUserGroup($user_group_information);
        }
        // Events - End }
        // Retrieve plugins for displaying in permissions section - Start {
        Core::LoadObject('plugin');
        $enabled_plugins = Plugins::GetPlugInList(true);
        // Retrieve plugins for displaying in permissions section - End }
        // Retrieve ui templates for displayin in user interface section - Start {
        $templates = ThemeEngine::DetectTemplates();
        // Retrieve ui templates for displayin in user interface section - End }
        // Retrieve css settings for each ui template - Start {
        $css_settings = array();
        foreach ($templates as $each_template) {
            $css_settings[$each_template['Code']] = ThemeEngine::LoadCSSSettings($each_template['Code'], true);
        }
        // Retrieve css settings for each ui template - End }
        // Interface parsing - Start
        $view_data = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminEditUserGroup'],
            'CurrentMenuItem' => 'Settings',
            'Plugins' => $enabled_plugins,
            'Templates' => $templates,
            'CSSSettings' => $css_settings,
            'UserGroup' => $user_group_information
        );
        $view_data = array_merge($view_data, InterfaceDefaultValues());

        if (isset($ArrayEventReturn) == true) {
            $view_data[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
        }

        $this->render('admin/edit_user_group', $view_data);
        // Interface parsing - End
    }

    /**
     * Creates a user group
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventCreateUserGroup() {
        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'GroupName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0513'],
                'rules' => 'required'
            ),
            array
                (
                'field' => 'SubscriberAreaLogoutURL',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0517'],
                'rules' => 'required'
            ),
            array('field' => 'ProductName', 'label' => '', 'rules' => ''),
            array('field' => 'ForceUnsubscriptionLink', 'label' => '', 'rules' => ''),
            array('field' => 'ForceRejectOptLink', 'label' => '', 'rules' => ''),
            array('field' => 'ForceOptInList', 'label' => '', 'rules' => ''),
            array('field' => 'ThresholdImport', 'label' => '', 'rules' => ''),
            array('field' => 'ThresholdEmailSend', 'label' => '', 'rules' => ''),
            array('field' => 'LimitSubscribers', 'label' => '', 'rules' => ''),
            array('field' => 'LimitLists', 'label' => '', 'rules' => ''),
            array('field' => 'LimitCampaignSendPerPeriod', 'label' => '', 'rules' => ''),
            array('field' => 'LimitEmailSendPerPeriod', 'label' => '', 'rules' => ''),
            array('field' => 'Permissions[]', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentSystem', 'label' => '', 'rules' => ''),
            array('field' => 'CreditSystem', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentSystemChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentCampaignsPerCampaignCost', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentDesignPrevChargePerReq', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentDesignPrevChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentAutoRespondersChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentCampaignsPerRecipient', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentAutoRespondersPerRecipient', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentPricingRange', 'label' => '', 'rules' => ''),
            array
                (
                'field' => 'Template',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0583'],
                'rules' => 'required'
            ),
            array('field' => 'PlainEmailHeader', 'label' => '', 'rules' => ''),
            array('field' => 'PlainEmailFooter', 'label' => '', 'rules' => ''),
            array('field' => 'HTMLEmailHeader', 'label' => '', 'rules' => ''),
            array('field' => 'HTMLEmailFooter', 'label' => '', 'rules' => ''),
            array('field' => 'TrialGroup', 'label' => '', 'rules' => ''),
            array('field' => 'TrialExpireSeconds', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1614'], 'rules' => ($this->input->post('TrialGroup') == 'Yes' ? 'required' : '')),
            array('field' => 'XMailer', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0298'], 'rules' => ''),
            array('field' => 'SendMethod', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0118'], 'rules' => 'required'),
        );

        if ($this->input->post('SendMethod') == 'SMTP') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPHost',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPPort',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0381'],
                'rules' => 'required|integer',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPSecure',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0382'],
                'rules' => '',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPTimeOut',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0387'],
                'rules' => 'required|integer',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPAuth',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0389'],
                'rules' => 'required',
            );
            if ($this->input->post('SendMethodSMTPAuth') == 'true') {
                $ArrayFormRules[] = array(
                    'field' => 'SendMethodSMTPUsername',
                    'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                    'rules' => 'required',
                );
                $ArrayFormRules[] = array(
                    'field' => 'SendMethodSMTPPassword',
                    'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                    'rules' => 'required',
                );
            }
        }
        if ($this->input->post('SendMethod') == 'SMTP-OCTETH') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodOctethSMTPUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodOctethSMTPPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-SENDGRID') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSendgridUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSendgridPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-MAILGUN') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailgunUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailgunPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-MAILJET') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailjetUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailjetPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-SES') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESHost',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESSecure',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0382'],
                'rules' => '',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-POSTMARK') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPostmarkAPIKey',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1870'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SaveToDisk') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSaveToDiskDir',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0317'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'PowerMTA') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPowerMTAVMTA',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0320'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPowerMTADir',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0371'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'LocalMTA') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodLocalMTAPath',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0373'],
                'rules' => 'required',
            );
        }

        $PluginVars = Plugins::HookListener('Filter', 'UserGroup.Update.FieldValidator', array($ArrayFormRules));
        $ArrayFormRules = $PluginVars[0];

        $this->form_validation->set_rules($ArrayFormRules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Create theme - Start {
        $ThemeID = 0;
        // Prepare theme css settings parameter - Start {
        $ThemeCSSSettings = '';

        foreach ($_POST as $key => $value) {
            if (preg_match('/ThemeCSSSettings_/i', $key)) {
                $ThemeCSSSettings .= str_replace('ThemeCSSSettings_', '', $key) . '||||' . $value . "\n";
            }
        }
        // Prepare theme css settings parameter - End }

        $ArrayAPIVars = array(
            'template' => $this->input->post('Template'),
            'themename' => $this->input->post('GroupName'),
            'productname' => $this->input->post('ProductName'),
            'themesettings' => $ThemeCSSSettings
        );
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'theme.create',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Theme.Create', $ErrorCode),
                    );
                    break;
                case '2':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Theme.Create', $ErrorCode),
                    );
                    break;
                case '3':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Theme.Create', $ErrorCode),
                    );
                    break;
                case '25':
                    return array(
                        'PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1628'],
                    );
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            $ThemeID = $ArrayReturn->ThemeID;
        }
        // Create theme - End }
        // Create user group - Start {
        if ($this->input->post('SendMethod') == 'SMTP-OCTETH') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodOctethSMTPUsername');
            $SMTPPassword = $this->input->post('SendMethodOctethSMTPPassword');
            $SMTPHost = 'octeth.smtp.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-SENDGRID') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodSendgridUsername');
            $SMTPPassword = $this->input->post('SendMethodSendgridPassword');
            $SMTPHost = 'smtp.sendgrid.net';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-MAILGUN') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodMailgunUsername');
            $SMTPPassword = $this->input->post('SendMethodMailgunPassword');
            $SMTPHost = 'smtp.mailgun.org';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-MAILJET') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodMailjetUsername');
            $SMTPPassword = $this->input->post('SendMethodMailjetPassword');
            $SMTPHost = 'in.mailjet.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-SES') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodSESUsername');
            $SMTPPassword = $this->input->post('SendMethodSESPassword');
            $SMTPHost = $this->input->post('SendMethodSESHost');
            $SMTPPort = 25;
            $SMTPSecure = $this->input->post('SendMethodSESSecure');
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-POSTMARK') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodPostmarkAPIKey');
            $SMTPPassword = $this->input->post('SendMethodPostmarkAPIKey');
            $SMTPHost = 'smtp.postmarkapp.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } else {
            $SendMethod = $this->input->post('SendMethod');
            $SMTPUsername = $this->input->post('SendMethodSMTPUsername');
            $SMTPPassword = $this->input->post('SendMethodSMTPPassword');
            $SMTPHost = $this->input->post('SendMethodSMTPHost');
            $SMTPPort = $this->input->post('SendMethodSMTPPort');
            $SMTPSecure = $this->input->post('SendMethodSMTPSecure');
            $SMTPTimeout = $this->input->post('SendMethodSMTPTimeOut');
            $SMTPAuth = $this->input->post('SendMethodSMTPAuth') == 'true' ? 'true' : 'false';
        }

        $ArrayAPIVars = array(
            'groupname' => $this->input->post('GroupName'),
            'subscriberarealogouturl' => $this->input->post('SubscriberAreaLogoutURL'),
            'limitsubscribers' => ($this->input->post('LimitSubscribers') == '' ? 0 : $this->input->post('LimitSubscribers')),
            'limitlists' => ($this->input->post('LimitLists') == '' ? 0 : $this->input->post('LimitLists')),
            'limitcampaignsendperperiod' => ($this->input->post('LimitCampaignSendPerPeriod') == '' ? 0 : $this->input->post('LimitCampaignSendPerPeriod')),
            'limitemailsendperperiod' => ($this->input->post('LimitEmailSendPerPeriod') == '' ? 0 : $this->input->post('LimitEmailSendPerPeriod')),
            'relthemeid' => $ThemeID,
            'forceunsubscriptionlink' => ($this->input->post('ForceUnsubscriptionLink') == false ? 'Disabled' : $this->input->post('ForceUnsubscriptionLink')),
            'forcerejectoptlink' => ($this->input->post('ForceRejectOptLink') == false ? 'Disabled' : $this->input->post('ForceRejectOptLink')),
            'forceoptinlist' => ($this->input->post('ForceOptInList') == false ? 'Disabled' : $this->input->post('ForceOptInList')),
            'permissions' => $this->input->post('Permissions'),
            'thresholdimport' => $this->input->post('ThresholdImport'),
            'thresholdemailsend' => $this->input->post('ThresholdEmailSend'),
            'paymentsystem' => $this->input->post('PaymentSystem'),
            'creditsystem' => $this->input->post('CreditSystem'),
            'paymentsystemchargeamount' => ($this->input->post('PaymentSystemChargeAmount') == '' ? 0 : $this->input->post('PaymentSystemChargeAmount')),
            'paymentcampaignspercampaigncost' => ($this->input->post('PaymentCampaignsPerCampaignCost') == '' ? 0 : $this->input->post('PaymentCampaignsPerCampaignCost')),
            'paymentdesignprevchargeperreq' => ($this->input->post('PaymentDesignPrevChargePerReq') == '' ? 0 : $this->input->post('PaymentDesignPrevChargePerReq')),
            'paymentdesignprevchargeamount' => ($this->input->post('PaymentDesignPrevChargeAmount') == '' ? 0 : $this->input->post('PaymentDesignPrevChargeAmount')),
            'paymentautoresponderschargeamount' => ($this->input->post('PaymentAutoRespondersChargeAmount') == '' ? 0 : $this->input->post('PaymentAutoRespondersChargeAmount')),
            'paymentcampaignsperrecipient' => $this->input->post('PaymentCampaignsPerRecipient'),
            'paymentautorespondersperrecipient' => $this->input->post('PaymentAutoRespondersPerRecipient'),
            'paymentpricingrange' => $this->input->post('PaymentPricingRange'),
            'plainemailheader' => $this->input->post('PlainEmailHeader'),
            'plainemailfooter' => $this->input->post('PlainEmailFooter'),
            'htmlemailheader' => $this->input->post('HTMLEmailHeader'),
            'htmlemailfooter' => $this->input->post('HTMLEmailFooter'),
            'trialgroup' => $this->input->post('TrialGroup'),
            'trialexpireseconds' => ($this->input->post('TrialExpireSeconds') * 86400),
            'XMailer' => $this->input->post('XMailer'),
            'SendMethod' => $SendMethod,
            'SendMethodSaveToDiskDir' => $this->input->post('SendMethodSaveToDiskDir'),
            'SendMethodPowerMTAVMTA' => $this->input->post('SendMethodPowerMTAVMTA'),
            'SendMethodPowerMTADir' => $this->input->post('SendMethodPowerMTADir'),
            'SendMethodLocalMTAPath' => $this->input->post('SendMethodLocalMTAPath'),
            'SendMethodSMTPHost' => $SMTPHost,
            'SendMethodSMTPPort' => $SMTPPort,
            'SendMethodSMTPSecure' => $SMTPSecure,
            'SendMethodSMTPTimeOut' => $SMTPTimeout,
            'SendMethodSMTPAuth' => $SMTPAuth,
            'SendMethodSMTPUsername' => $SMTPUsername,
            'SendMethodSMTPPassword' => $SMTPPassword,
        );

        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'usergroup.create',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                default:
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'UserGroup.Create', $ErrorCode),
                    );
                    break;
            }
        } else {
            if (isset($ArrayReturn->UserGroupID) == true) {
                $UserGroup = UserGroups::RetrieveUserGroup($ArrayReturn->UserGroupID, true);

                $ArrayFieldAndValues = array("SubscriptionType" => $this->input->post('SubscriptionType'));
                $ArrayCriterias = array("UserGroupID" => $ArrayReturn->UserGroupID);

                UserGroups::Update($ArrayFieldAndValues, $ArrayCriterias);

                Plugins::HookListener('Action', 'UserGroup.Create.Post', array($UserGroup, $this->input->get_all_post_items(true)));
            }

            // API Success
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0584']);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/usergroups/', 'location', '302');
        }
        // Create user group - End }
    }

    /**
     * Saves a user group
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventSaveUserGroup($UserGroup) {
        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'GroupName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0513'],
                'rules' => 'required'
            ),
            array
                (
                'field' => 'SubscriberAreaLogoutURL',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0517'],
                'rules' => 'required'
            ),
            array('field' => 'ProductName', 'label' => '', 'rules' => ''),
            array('field' => 'ForceUnsubscriptionLink', 'label' => '', 'rules' => ''),
            array('field' => 'ForceRejectOptLink', 'label' => '', 'rules' => ''),
            array('field' => 'ForceOptInList', 'label' => '', 'rules' => ''),
            array('field' => 'ThresholdImport', 'label' => '', 'rules' => ''),
            array('field' => 'ThresholdEmailSend', 'label' => '', 'rules' => ''),
            array('field' => 'LimitSubscribers', 'label' => '', 'rules' => ''),
            array('field' => 'LimitLists', 'label' => '', 'rules' => ''),
            array('field' => 'LimitCampaignSendPerPeriod', 'label' => '', 'rules' => ''),
            array('field' => 'LimitEmailSendPerPeriod', 'label' => '', 'rules' => ''),
            array('field' => 'Permissions[]', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentSystem', 'label' => '', 'rules' => ''),
            array('field' => 'CreditSystem', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentSystemChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentCampaignsPerCampaignCost', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentDesignPrevChargePerReq', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentDesignPrevChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentAutoRespondersChargeAmount', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentCampaignsPerRecipient', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentAutoRespondersPerRecipient', 'label' => '', 'rules' => ''),
            array('field' => 'PaymentPricingRange', 'label' => '', 'rules' => ''),
            array
                (
                'field' => 'Template',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0583'],
                'rules' => 'required'
            ),
            array('field' => 'PlainEmailHeader', 'label' => '', 'rules' => ''),
            array('field' => 'PlainEmailFooter', 'label' => '', 'rules' => ''),
            array('field' => 'HTMLEmailHeader', 'label' => '', 'rules' => ''),
            array('field' => 'HTMLEmailFooter', 'label' => '', 'rules' => ''),
            array('field' => 'TrialGroup', 'label' => '', 'rules' => ''),
            array('field' => 'TrialExpireSeconds', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1614'], 'rules' => ($this->input->post('TrialGroup') == 'Yes' ? 'required' : '')),
            array('field' => 'XMailer', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0298'], 'rules' => ''),
            array('field' => 'SendMethod', 'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0118'], 'rules' => 'required'),
        );

        if ($this->input->post('SendMethod') == 'SMTP') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPHost',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPPort',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0381'],
                'rules' => 'required|integer',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPSecure',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0382'],
                'rules' => '',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPTimeOut',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0387'],
                'rules' => 'required|integer',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSMTPAuth',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0389'],
                'rules' => 'required',
            );
            if ($this->input->post('SendMethodSMTPAuth') == 'true') {
                $ArrayFormRules[] = array(
                    'field' => 'SendMethodSMTPUsername',
                    'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                    'rules' => 'required',
                );
                $ArrayFormRules[] = array(
                    'field' => 'SendMethodSMTPPassword',
                    'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                    'rules' => 'required',
                );
            }
        }
        if ($this->input->post('SendMethod') == 'SMTP-OCTETH') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodOctethSMTPUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodOctethSMTPPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-SENDGRID') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSendgridUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSendgridPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-MAILGUN') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailgunUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailgunPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-MAILJET') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailjetUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodMailjetPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-SES') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESHost',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESSecure',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0382'],
                'rules' => '',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESUsername',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSESPassword',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SMTP-POSTMARK') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPostmarkAPIKey',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1870'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'SaveToDisk') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodSaveToDiskDir',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0317'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'PowerMTA') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPowerMTAVMTA',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0320'],
                'rules' => 'required',
            );
            $ArrayFormRules[] = array(
                'field' => 'SendMethodPowerMTADir',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0371'],
                'rules' => 'required',
            );
        }
        if ($this->input->post('SendMethod') == 'LocalMTA') {
            $ArrayFormRules[] = array(
                'field' => 'SendMethodLocalMTAPath',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0373'],
                'rules' => 'required',
            );
        }

        $PluginVars = Plugins::HookListener('Filter', 'UserGroup.Update.FieldValidator', array($ArrayFormRules));
        $ArrayFormRules = $PluginVars[0];

        $this->form_validation->set_rules($ArrayFormRules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Default SMTP preferences - Start
        if ($this->input->post('SendMethod') == 'SMTP-OCTETH') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodOctethSMTPUsername');
            $SMTPPassword = $this->input->post('SendMethodOctethSMTPPassword');
            $SMTPHost = 'octeth.smtp.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-SENDGRID') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodSendgridUsername');
            $SMTPPassword = $this->input->post('SendMethodSendgridPassword');
            $SMTPHost = 'smtp.sendgrid.net';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-MAILGUN') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodMailgunUsername');
            $SMTPPassword = $this->input->post('SendMethodMailgunPassword');
            $SMTPHost = 'smtp.mailgun.org';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-MAILJET') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodMailjetUsername');
            $SMTPPassword = $this->input->post('SendMethodMailjetPassword');
            $SMTPHost = 'in.mailjet.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-SES') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodSESUsername');
            $SMTPPassword = $this->input->post('SendMethodSESPassword');
            $SMTPHost = $this->input->post('SendMethodSESHost');
            $SMTPPort = 25;
            $SMTPSecure = $this->input->post('SendMethodSESSecure');
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } elseif ($this->input->post('SendMethod') == 'SMTP-POSTMARK') {
            $SendMethod = 'SMTP';
            $SMTPUsername = $this->input->post('SendMethodPostmarkAPIKey');
            $SMTPPassword = $this->input->post('SendMethodPostmarkAPIKey');
            $SMTPHost = 'smtp.postmarkapp.com';
            $SMTPPort = 25;
            $SMTPSecure = '';
            $SMTPTimeout = 15;
            $SMTPAuth = 'true';
        } else {
            $SendMethod = $this->input->post('SendMethod');
            $SMTPUsername = $this->input->post('SendMethodSMTPUsername');
            $SMTPPassword = $this->input->post('SendMethodSMTPPassword');
            $SMTPHost = $this->input->post('SendMethodSMTPHost');
            $SMTPPort = $this->input->post('SendMethodSMTPPort');
            $SMTPSecure = $this->input->post('SendMethodSMTPSecure');
            $SMTPTimeout = $this->input->post('SendMethodSMTPTimeOut');
            $SMTPAuth = $this->input->post('SendMethodSMTPAuth') == 'true' ? 'true' : 'false';
        }
        // Default SMTP preferences - End
        // Validate sending method - Start {
        if ($this->input->post('SendMethod') != 'System') {
            $ArrayResult = array();
            Core::LoadObject('emails');

            if (($this->input->post('SendMethod') == 'SMTP') || ($this->input->post('SendMethod') == 'SMTP-OCTETH') || ($this->input->post('SendMethod') == 'SMTP-POSTMARK') || ($this->input->post('SendMethod') == 'SMTP-SENDGRID') || ($this->input->post('SendMethod') == 'SMTP-MAILGUN') || ($this->input->post('SendMethod') == 'SMTP-MAILJET') || ($this->input->post('SendMethod') == 'SMTP-SES')) {
                $SendMethod = 'SMTP';
                $SMTPHost = $SMTPHost;
                $SMTPPort = $SMTPPort;
                $SMTPSecure = $SMTPSecure;
                $SMTPAuth = $SMTPAuth;
                $SMTPUsername = $SMTPUsername;
                $SMTPPassword = $SMTPPassword;
                $SMTPTimeout = $SMTPTimeout;
                $SMTPDebug = false;
                $SMTPKeepAlive = true;
                $LocalMTAPath = '';
                $ArrayResult = Emails::SendTestEmail($this->ArrayAdminInformation['EmailAddress'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9113'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9114'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
            } elseif ($this->input->post('SendMethod') == 'LocalMTA') {
                $SendMethod = 'LocalMTA';
                $SMTPHost = '';
                $SMTPPort = '';
                $SMTPSecure = '';
                $SMTPAuth = '';
                $SMTPUsername = '';
                $SMTPPassword = '';
                $SMTPTimeout = '';
                $SMTPDebug = '';
                $SMTPKeepAlive = '';
                $LocalMTAPath = $this->input->post('SendMethodLocalMTAPath');

                $ArrayResult = Emails::SendTestEmail($this->ArrayAdminInformation['EmailAddress'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9113'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9114'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
            } elseif ($this->input->post('SendMethod') == 'PHPMail') {
                $SendMethod = 'PHPMail';
                $SMTPHost = '';
                $SMTPPort = '';
                $SMTPSecure = '';
                $SMTPAuth = '';
                $SMTPUsername = '';
                $SMTPPassword = '';
                $SMTPTimeout = '';
                $SMTPDebug = '';
                $SMTPKeepAlive = '';
                $LocalMTAPath = '';

                $ArrayResult = Emails::SendTestEmail($this->ArrayAdminInformation['EmailAddress'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9113'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9114'], ApplicationHeader::$ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
            } elseif (($this->input->post('SendMethod') == 'PowerMTA') || ($this->input->post('SendMethod') == 'SaveToDisk')) {
                if (is_writable(($this->input->post('SendMethod') == 'PowerMTA' ? $this->input->post('SendMethodPowerMTADir') : $this->input->post('SendMethodSaveToDiskDir'))) == false) {
                    $ArrayResult = array(false, isset(ApplicationHeader::$ArrayLanguageStrings['Screen']['9097']) ? ApplicationHeader::$ArrayLanguageStrings['Screen']['9097'] : ApplicationHeader::$ArrayLanguageStrings['Screen']['1325']);
                } else {
                    $ArrayResult = array(true);
                }
            }

            if ($ArrayResult[0] == false) {
                return array(false, (isset($ArrayResult[1]) == true ? $ArrayResult[1] : ApplicationHeader::$ArrayLanguageStrings['Screen']['1628']));
            }
        }
        // Validate sending method - End }
        // Save theme - Start {
        // Prepare theme css settings parameter - Start {
        $ThemeCSSSettings = '';

        foreach ($_POST as $key => $value) {
            if (preg_match('/' . $this->input->post('Template') . '_ThemeCSSSettings_/i', $key)) {
                $ThemeCSSSettings .= str_replace($this->input->post('Template') . '_ThemeCSSSettings_', '', $key) . '||||' . $value . "\n";
            }
        }
        // Prepare theme css settings parameter - End }

        ThemeEngine::Update(array(
            'Template' => $this->input->post('Template'),
            'ThemeName' => $this->input->post('GroupName'),
            'ProductName' => $this->input->post('ProductName'),
            'ThemeSettings' => $ThemeCSSSettings
                ), array(
            'ThemeID' => $this->input->post('RelThemeID')
        ));
        // Save theme - End }
        // Save user group - Start {
        $ArrayFieldAndValues = array(
            'GroupName' => $this->input->post('GroupName'),
            'SubscriptionType' => $this->input->post('SubscriptionType'),
            'SubscriberAreaLogoutURL' => $this->input->post('SubscriberAreaLogoutURL'),
            'ForceUnsubscriptionLink' => ($this->input->post('ForceUnsubscriptionLink') == 'Enabled' ? 'Enabled' : 'Disabled'),
            'ForceRejectOptLink' => ($this->input->post('ForceRejectOptLink') == 'Enabled' ? 'Enabled' : 'Disabled'),
            'ForceOptInList' => ($this->input->post('ForceOptInList') == 'Enabled' ? 'Enabled' : 'Disabled'),
            'Permissions' => implode(',', $this->input->post('Permissions')),
            'LimitSubscribers' => ($this->input->post('LimitSubscribers') == '' ? 0 : $this->input->post('LimitSubscribers')),
            'LimitLists' => ($this->input->post('LimitLists') == '' ? 0 : $this->input->post('LimitLists')),
            'LimitCampaignSendPerPeriod' => ($this->input->post('LimitCampaignSendPerPeriod') == '' ? 0 : $this->input->post('LimitCampaignSendPerPeriod')),
            'LimitEmailSendPerPeriod' => ($this->input->post('LimitEmailSendPerPeriod') == '' ? 0 : $this->input->post('LimitEmailSendPerPeriod')),
            'ThresholdImport' => ($this->input->post('ThresholdImport') == '' ? 0 : $this->input->post('ThresholdImport')),
            'ThresholdEmailSend' => ($this->input->post('ThresholdEmailSend') == '' ? 0 : $this->input->post('ThresholdEmailSend')),
            'PaymentSystem' => $this->input->post('PaymentSystem'),
            'CreditSystem' => $this->input->post('CreditSystem'),
            'PaymentSystemChargeAmount' => ($this->input->post('PaymentSystemChargeAmount') == '' ? 0 : $this->input->post('PaymentSystemChargeAmount')),
            'PaymentCampaignsPerCampaignCost' => ($this->input->post('PaymentCampaignsPerCampaignCost') == '' ? 0 : $this->input->post('PaymentCampaignsPerCampaignCost')),
            'PaymentDesignPrevChargePerReq' => ($this->input->post('PaymentDesignPrevChargePerReq') == '' ? 0 : $this->input->post('PaymentDesignPrevChargePerReq')),
            'PaymentDesignPrevChargeAmount' => ($this->input->post('PaymentDesignPrevChargeAmount') == '' ? 0 : $this->input->post('PaymentDesignPrevChargeAmount')),
            'PaymentAutoRespondersChargeAmount' => ($this->input->post('PaymentAutoRespondersChargeAmount') == '' ? 0 : $this->input->post('PaymentAutoRespondersChargeAmount')),
            'PaymentCampaignsPerRecipient' => $this->input->post('PaymentCampaignsPerRecipient'),
            'PaymentAutoRespondersPerRecipient' => $this->input->post('PaymentAutoRespondersPerRecipient'),
            'PaymentPricingRange' => $this->input->post('PaymentPricingRange'),
            'PlainEmailHeader' => $this->input->post('PlainEmailHeader'),
            'PlainEmailFooter' => $this->input->post('PlainEmailFooter'),
            'HTMLEmailHeader' => $this->input->post('HTMLEmailHeader'),
            'HTMLEmailFooter' => $this->input->post('HTMLEmailFooter'),
            'TrialGroup' => $this->input->post('TrialGroup'),
            'TrialExpireSeconds' => $this->input->post('TrialExpireSeconds') * 86400,
            'XMailer' => $this->input->post('XMailer'),
            'SendMethod' => $SendMethod,
            'SendMethodSaveToDiskDir' => $this->input->post('SendMethodSaveToDiskDir'),
            'SendMethodPowerMTAVMTA' => $this->input->post('SendMethodPowerMTAVMTA'),
            'SendMethodPowerMTADir' => $this->input->post('SendMethodPowerMTADir'),
            'SendMethodLocalMTAPath' => $this->input->post('SendMethodLocalMTAPath'),
            'SendMethodSMTPHost' => $SMTPHost,
            'SendMethodSMTPPort' => $SMTPPort,
            'SendMethodSMTPSecure' => $SMTPSecure,
            'SendMethodSMTPTimeOut' => $SMTPTimeout,
            'SendMethodSMTPAuth' => $SMTPAuth,
            'SendMethodSMTPUsername' => $SMTPUsername,
            'SendMethodSMTPPassword' => $SMTPPassword,
        );

        UserGroups::Update($ArrayFieldAndValues, array("UserGroupID" => $this->input->post('UserGroupID')));

        Plugins::HookListener('Action', 'UserGroup.Update.Post', array($UserGroup, $this->input->get_all_post_items(true)));

        $_SESSION['PageMessageCache'] = array('Success', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0589'], $this->input->post('GroupName')));

        $this->load->helper('url');
        redirect(InterfaceAppURL(true) . '/admin/usergroups/', 'location', '302');
        // Save user group - End }
    }

    /**
     * Deletes user groups
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventDeleteUserGroup($user_group_ids) {
        if (DEMO_MODE_ENABLED == true) {
            return array(false, 'This feature is disabled in demo version.');
        }

        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'SelectedUserGroups',
                'label' => 'users',
                'rules' => 'required',
            ),
        );

        $this->form_validation->set_rules($ArrayFormRules);
        $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0586']);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false, validation_errors());
        }
        // Run validation - End }
        // Delete user groups - Start {
        $ArrayAPIVars = array(
            'usergroupid' => implode(',', $user_group_ids),
        );
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'usergroup.delete',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '4':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0439']);
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0585']);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/usergroups/', 'location', '302');
        }
        // Delete user groups - End }
    }

}

// end of class User
?>