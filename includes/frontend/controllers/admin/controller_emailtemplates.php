<?php
/**
 * Users controller
 *
 * @author Cem Hurturk
 */

class Controller_EmailTemplates extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();

	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	if ($this->input->post('Command') == 'DeleteEmailTemplates')
		{
		$ArrayEventReturn = $this->_EventDeleteEmailTemplates($this->input->post('SelectedEmailTemplates'));
		}
	// Events - End }
	
	// Retrieve email templates - Start {
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'email.templates.get',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	array()
		));

	if ($ArrayReturn->Success == false)
		{
		// Error occurred
		}
	else
		{
		$TotalEmailTemplates 	= $ArrayReturn->TotalTemplateCount;
		$EmailTemplates			= $ArrayReturn->Templates;
		}
	// Retrieve email templates - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'EmailTemplates',
							'TotalEmailTemplates'	=> $TotalEmailTemplates,
							'EmailTemplates'		=> $EmailTemplates,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
		
	if (isset($ArrayEventReturn) == true)
		{
		$ArrayViewData[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
		}

		// Check if there is any message in the message buffer - Start {
		if ($_SESSION['PageMessageCache'][1] != '')
			{
			$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
			}
		// Check if there is any message in the message buffer - End }
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}
	
/**
 * Email template create controller
 *
 * @return void
 * @author Mert Hurturk
 **/
function create()
	{
	// Events - Start {
	if ($this->input->post('Command') == 'ProceedToBuilder')
		{
		$ArrayEventReturn = $this->_EventProceedToBuilder();
		}
	// Events - End }

	// Retrieve user accounts - Start {
	$ArrayAPIVars	= array(
							'recordsperrequest'			=> 0,
							'recordsfrom'				=> 0,
							'relusergroupid'			=> 0,
							'searchfield'				=> '',
							'searchkeyword'				=> '',
							'orderfield'				=> 'FirstName|LastName',
							'ordertype'					=> 'ASC|ASC',
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'users.get',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Error occurred
		}
	else
		{
		$Users		= $ArrayReturn->Users;
		}
	// Retrieve user accounts - End }

	// Retrieve personalization tags - Start {
	Core::LoadObject('personalization');
	$array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
	$array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'All');
	$array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
	$array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
	$array_tags = array(
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0670']	=>	$array_subscriber_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0671']	=>	$array_link_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0672']	=>	$array_user_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0673']	=>	$array_other_tags
		);
	// Retrieve personalization tags - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
							'CurrentMenuItem'		=> 'Settings',
							'Users'					=> $Users,
							'Tags'					=> $array_tags,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	$this->render('admin/create_email_template', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Email template update controller
 *
 * @return void
 * @author Mert Hurturk
 **/
function edit($template_id)
	{
	// Events - Start {
	if ($this->input->post('Command') == 'ProceedToBuilder')
		{
		$ArrayEventReturn = $this->_EventProceedToBuilder();
		}
	elseif ($this->input->post('Command') == 'Save')
		{
		$ArrayEventReturn = $this->_EventProceedToBuilder(true);
		}
	// Events - End }

	// Retrieve email template information - Start {
	if ((!isset($template_id) || $template_id == '') && (!isset($_SESSION['template_builder'])))
		{
		$this->load->helper('url');
		redirect(InterfaceAppURL(true).'/admin/emailtemplates/builder','refresh');
		}
		
	if (isset($_SESSION['template_builder']) && $_SESSION['template_builder'] != '' && (!isset($template_id) || $template_id == ''))
		{
		$EmailTemplate = (object) $_SESSION['template_builder'];
		}
	else
		{
		$ArrayAPIVars	= array(
								'templateid'			=> $template_id
								);
		$ArrayReturn = API::call(array(
			'format'	=>	'object',
			'command'	=>	'email.template.get',
			'protected'	=>	true,
			'username'	=>	$this->ArrayAdminInformation['Username'],
			'password'	=>	$this->ArrayAdminInformation['Password'],
			'parameters'=>	$ArrayAPIVars
			));
		if ($ArrayReturn->Success == false)
			{
			// Error occurred
			}
		else
			{
			$EmailTemplate		= $ArrayReturn->Template;
			}
		}
	// Retrieve email template information - End }

	// Retrieve user accounts - Start {
	$ArrayAPIVars	= array(
							'recordsperrequest'			=> 0,
							'recordsfrom'				=> 0,
							'relusergroupid'			=> 0,
							'searchfield'				=> '',
							'searchkeyword'				=> '',
							'orderfield'				=> 'FirstName|LastName',
							'ordertype'					=> 'ASC|ASC',
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'users.get',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// Error occurred
		}
	else
		{
		$Users		= $ArrayReturn->Users;
		}
	// Retrieve user accounts - End }

	// Retrieve user groups - Start {
		$ArrayReturn = API::call(array(
			'format'	=>	'object',
			'command'	=>	'usergroups.get',
			'protected'	=>	true,
			'username'	=>	$this->ArrayAdminInformation['Username'],
			'password'	=>	$this->ArrayAdminInformation['Password'],
			'parameters'=>	array()
			));

		if ($ArrayReturn->Success == false)
			{
			// Error occurred
			}
		else
			{
			$UserGroups	= $ArrayReturn->UserGroups;
			}
	// Retrieve user groups - End }

	// Retrieve personalization tags - Start {
	Core::LoadObject('personalization');
	$array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags(0, 0, 0, 0, array(), ApplicationHeader::$ArrayLanguageStrings);
	$array_link_tags = Personalization::GetPersonalizationLinkTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0666'], 'All');
	$array_user_tags = Personalization::GetPersonalizationUserTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0667']);
	$array_other_tags = Personalization::GetOtherPersonalizationTags(ApplicationHeader::$ArrayLanguageStrings['Screen']['0668']);
	$array_tags = array(
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0670']	=>	$array_subscriber_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0671']	=>	$array_link_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0672']	=>	$array_user_tags, 
		ApplicationHeader::$ArrayLanguageStrings['Screen']['0673']	=>	$array_other_tags
		);
	// Retrieve personalization tags - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
							'CurrentMenuItem'		=> 'Settings',
							'Users'					=> $Users,
							'UserGroups'			=> $UserGroups,
							'TemplateInformation'	=> $EmailTemplate,
							'Tags'					=> $array_tags
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	$this->render('admin/edit_email_template', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Email template builder (iguana builder) controller
 *
 * @return void
 * @author Mert Hurturk
 **/
function builder($command = '')
	{
	// Events - Start {
	if ($command != '' && $command == 'displayHTMLContent')
		{
		header('content-type: text/html');
		print($_SESSION['template_builder']['TemplateHTMLContent']);
		exit;
		}
	else if ($command != '' && $command == 'testDrive')
		{
		$this->_EventTestDriveEmailTemplate();
		}
	else if ($command != '' && $command == 'save')
		{
		if (isset($_SESSION['template_builder']['TemplateID']) && $_SESSION['template_builder']['TemplateID'] != '')
			{
			$this->_EventSaveEmailTemplate();
			}
		else
			{
			$this->_EventCreateEmailTemplate();
			}
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseEmailTemplates'],
							'TemplateInformation'	=> $_SESSION['template_builder'],
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/email_template_builder', $ArrayViewData);
	// Interface parsing - End
	}	

/**
 * Test drives email template
 *
 * @return void
 * @author Mert Hurturk
 **/
function testdrive($command = '')
	{
	// Check if template builder session exists - Start {
	if (!isset($_SESSION['template_builder']))
		{
		exit;
		}
	// Check if template builder session exists - End }
	
	// Events - Start - Start {
	if ($command != '' && $command == 'displayHTMLContent')
		{
		header('content-type: text/html');
		print($_SESSION['template_builder']['TemplateHTMLContent']);
		exit;
		}
	// Events - Start - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminSettingsPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminEmailTemplateTestDrive'],
							'TemplateInformation'	=> $_SESSION['template_builder'],
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}

	$this->render('admin/email_template_builder_test_drive', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Displays template's thumbnail image
 *
 * @return void
 * @author Mert Hurturk
 **/
function thumbnail($template_id)
	{
	// Retrieve email template information - Start {
	if (!isset($template_id) || $template_id == '')
		{
		exit;
		}

	Core::LoadObject('templates');
	$EmailTemplate = Templates::RetrieveTemplate(array('*'), array('TemplateID' => $template_id));
	// Retrieve email template information - End }

	header('Content-Type: '.$EmailTemplate['TemplateThumbnailType']);
	print(base64_decode($EmailTemplate['TemplateThumbnail']));
	exit;
	}

/**
 * Setups session data with email template information and redirects to builder function
 *
 * @return void
 * @author Mert Hurturk
 **/
function _EventProceedToBuilder($save = false)
	{
	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'TemplateName',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0051'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'TemplateDescription',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0170'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'RelOwnerUserID',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0173'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'TemplateSubject',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0106'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'TemplateHTMLContent',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0175'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'TemplatePlainContent',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0176'],
								'rules'		=> '',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }
	
	// Pass post data to session - Start {
	unset($_POST['Command']);

	$_SESSION['template_builder'] = array();

	// If thumbnail is uploaded, move it to data folder and pass the path to session - Start {
	if (count($_FILES) > 0 && $_FILES['TemplateThumbnail']['tmp_name'] != '') 
		{
		// Check image type - Start
		$ArrayImageInfo = getimagesize($_FILES['TemplateThumbnail']['tmp_name']);
		if ((strtolower($ArrayImageInfo['mime']) != 'image/gif') && (strtolower($ArrayImageInfo['mime']) != 'image/png') && (strtolower($ArrayImageInfo['mime']) != 'image/jpg') && (strtolower($ArrayImageInfo['mime']) != 'image/jpeg'))
			{
			return array('PageErrorMessage'=>ApplicationHeader::$ArrayLanguageStrings['Screen']['0313']);
			}
		// Check image type - End

		// File upload - Start
		$NewFileName	= md5('templatethumbnail_'.$_FILES['TemplateThumbnail']['tmp_name']);
		$FilePath		= DATA_PATH.'tmp/'.$NewFileName;
		move_uploaded_file($_FILES['TemplateThumbnail']['tmp_name'], $FilePath);
		
		$_SESSION['template_builder']['TemplateThumbnailFileName']	= $NewFileName;
		// File upload - End	
		}
	// If thumbnail is uploaded, move it to data folder and pass the path to session - End }

	foreach ($_POST as $key=>$value)
		{
		$_SESSION['template_builder'][$key] = $value;
		}
	// Pass post data to session - End }
	
	if ($save)
		{
		$this->_EventSaveEmailTemplate();
		}
	
	// Redirect to email builder function - Start {
	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/emailtemplates/builder','refresh');
	// Redirect to email builder function - End }
	}

/**
 * Saves email tamplate data to database
 *
 * @return void
 * @author Mert Hurturk
 **/
function _EventSaveEmailTemplate()
	{
	$_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);

	// Update email template - Start {
	$ArrayAPIVars	= array(
							'templateid'				=> $_SESSION['template_builder']['TemplateID'],
							'relowneruserid'			=> $_SESSION['template_builder']['RelOwnerUserID'],
							'templatename'				=> $_SESSION['template_builder']['TemplateName'],
							'templatedescription'		=> $_SESSION['template_builder']['TemplateDescription'],
							'templatesubject'			=> $_SESSION['template_builder']['TemplateSubject'],
							'templatehtmlcontent'		=> $_SESSION['template_builder']['TemplateHTMLContent'],
							'templateplaincontent'		=> $_SESSION['template_builder']['TemplatePlainContent'],
							'templatethumbnailpath'		=> $_SESSION['template_builder']['TemplateThumbnailFileName']
							);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'email.template.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));
	// Update email template - End }

	if ($ArrayReturn->Success == false)
		{
		// Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		$_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Email.Template.Update', $ErrorCode));
		}
	else
		{
		$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0345']);
		}
	unset($_SESSION['template_builder']);
	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/emailtemplates/','refresh');
	}

/**
 * Create email template
 *
 * @return void
 * @author Mert Hurturk
 **/
function _EventCreateEmailTemplate()
	{
	$_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);

	// Create email template - Start {
	$ArrayAPIVars	= array(
							'relowneruserid'			=> $_SESSION['template_builder']['RelOwnerUserID'],
							'templatename'				=> $_SESSION['template_builder']['TemplateName'],
							'templatedescription'		=> $_SESSION['template_builder']['TemplateDescription'],
							'templatesubject'			=> $_SESSION['template_builder']['TemplateSubject'],
							'templatehtmlcontent'		=> $_SESSION['template_builder']['TemplateHTMLContent'],
							'templateplaincontent'		=> $_SESSION['template_builder']['TemplatePlainContent'],
							'templatethumbnailpath'		=> $_SESSION['template_builder']['TemplateThumbnailFileName'],
							'iscreatedbyadmin'			=> 1,
							);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'email.template.create',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));
	// Create email template - End }

	if ($ArrayReturn->Success == false)
		{
		// Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		$_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Email.Template.Create', $ErrorCode));
		}
	else
		{
		$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0344']);
		}
	unset($_SESSION['template_builder']);
	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/emailtemplates/','refresh');
	}

/**
 * Test drives email template
 *
 * @return void
 * @author Mert Hurturk
 **/
function _EventTestDriveEmailTemplate()
	{
	$_SESSION['template_builder']['TemplateHTMLContent'] = str_replace('&quot;', "'", $_POST['TemplateHTMLContent']);
	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/emailtemplates/testdrive');
	}

/**
 * Delete email templates event
 *
 * @return void
 * @author Mert Hurturk
 */
function _EventDeleteEmailTemplates($ArrayEmailTemplateIDs, $PerformFormValidation = true)
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array(false, 'This feature is disabled in demo version.');
		}

	if ($PerformFormValidation == true)
		{
		// Field validations - Start {
		$ArrayFormRules = array(
								array
									(
									'field'		=> 'SelectedEmailTemplates[]',
									'label'		=> 'emailtemplates',
									'rules'		=> 'required'
									)
								);

		$this->form_validation->set_rules($ArrayFormRules);
		$this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0346']);
		// Field validations - End }

		// Run validation - Start {
		if ($this->form_validation->run() == false)
			{
			return array(false, validation_errors());
			}
		// Run validation - End }
		}

	// Delete email templates - Start {
	$ArrayAPIVars = array(
						'templates'		=> implode(',', $ArrayEmailTemplateIDs),
						);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'email.template.delete',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
		switch ($ErrorCode)
			{
			case '1':
				return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0346']);
				break;
			default:
				break;
			}
		}
	else
		{
		// API Success
		$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0347']);
		return true;
		}
	// Delete email templates - End }
	}

} // end of class User
