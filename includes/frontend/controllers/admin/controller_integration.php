<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Integration extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	if ($this->input->post('Command') == 'EditIntegration')
		{
		$ArrayEventReturn = $this->_EventEditIntegrationSettings();
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminIntegration'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'Integration',
							'IsPreviewMyEmailDisabled'	=> ((PME_USAGE_TYPE == 'Admin') && (PME_APIKEY == '') && (PME_DEFAULT_ACCOUNT == '')) ? TRUE : FALSE,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Integration settings update event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditIntegrationSettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage'	=> 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'PME_USAGE_TYPE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0204'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'GOOGLE_ANALYTICS_SOURCE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0434'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'GOOGLE_ANALYTICS_MEDIUM',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0435'],
								'rules'		=> '',
								),
							);
	if ($this->input->post('PME_USAGE_TYPE') == 'Admin')
		{
		$ArrayFormRules[] = array
								(
								'field'		=> 'PME_DEFAULT_APIKEY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0208'],
								'rules'		=> 'required',
								);
		}


	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'PME_USAGE_TYPE'			=> $this->input->post('PME_USAGE_TYPE') == 'Disabled' ? 'Admin' : $this->input->post('PME_USAGE_TYPE'),
							'PME_DEFAULT_APIKEY'		=> $this->input->post('PME_USAGE_TYPE') == 'Disabled' ? '' : $this->input->post('PME_DEFAULT_APIKEY'),
							'GOOGLE_ANALYTICS_SOURCE'	=> $this->input->post('GOOGLE_ANALYTICS_SOURCE'),
							'GOOGLE_ANALYTICS_MEDIUM'	=> $this->input->post('GOOGLE_ANALYTICS_MEDIUM'),
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			case '3':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0340'],
							);
				break;
			case '4':
				$this->form_validation->_field_data['PME_DEFAULT_ACCOUNT']['error'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0628'];
				$this->form_validation->_field_data['PME_DEFAULT_APIKEY']['error'] = ApplicationHeader::$ArrayLanguageStrings['Screen']['0629'];
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0341'],
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0339'],
					);
		}
	// Update settings - End }
	}

} // end of class User
?>