<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Account extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	Core::LoadObject('admins');
	// Load required modules - End }

	// Retrieve administrator information - Start {
	$ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }

	// Events - Start {
	if ($this->input->post('Command') == 'EditAdmin')
		{
		$ArrayEventReturn = $this->_EventEditAdminAccount($ArrayAdminInformation);
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminAccount'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'Account',
							'Admin'					=> $ArrayAdminInformation,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Edit administrator account event
 *
 * @param string $ArrayAdminInformation 
 * @return void
 * @author Cem Hurturk
 */
function _EventEditAdminAccount($ArrayAdminInformation)
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'Name',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0051'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'Username',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'EmailAddress',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
								'rules'		=> 'required|valid_email',
								),
							array
								(
								'field'		=> 'SYSTEM_EMAIL_FROM_NAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0179'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'SYSTEM_EMAIL_FROM_EMAIL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0180'],
								'rules'		=> 'required|valid_email',
								),
							array
								(
								'field'		=> 'ALERT_RECIPIENT_EMAIL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0181'],
								'rules'		=> 'required|valid_email',
								),
							);

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update administrator account - Start {
		$ArrayAPIVars = array(
							'adminid'			=> $this->ArrayAdminInformation['AdminID'],
							'username'			=> $this->input->post('Username'),
							'name'				=> $this->input->post('Name'),
							'emailaddress'		=> $this->input->post('EmailAddress'),
							);
	if ($this->input->post('NewPassword') != '')
		{
		$ArrayAPIVars['password'] = OEMPRO_PASSWORD_SALT.$this->input->post('NewPassword').OEMPRO_PASSWORD_SALT;
		}

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'admin.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Admin.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		if (($this->input->post('NewPassword') != '') || ($this->input->post('Username') != $ArrayAdminInformation['Username']))
			{
			// Retrieve administrator information - Start {
			$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>md5($ArrayAdminInformation['AdminID']).md5($this->input->post('Username')).($this->input->post('NewPassword') == '' ? md5($ArrayAdminInformation['Password']) : md5(md5(OEMPRO_PASSWORD_SALT.$this->input->post('NewPassword').OEMPRO_PASSWORD_SALT)))));
			// Retrieve administrator information - End }

			// Perform the admin login - Start
			AdminAuth::Login($this->ArrayAdminInformation['AdminID'], $this->ArrayAdminInformation['Username'], md5($this->ArrayAdminInformation['Password']), false);
			// Perform the admin login - End
			}
		}
	// Update administrator account - End }
	
	// Update settings - Start {
		$ArrayAPIVars = array(
							'SYSTEM_EMAIL_FROM_NAME'			=> $this->input->post('SYSTEM_EMAIL_FROM_NAME'),
							'SYSTEM_EMAIL_FROM_EMAIL'			=> $this->input->post('SYSTEM_EMAIL_FROM_EMAIL'),
							'SYSTEM_EMAIL_REPLYTO_NAME'			=> $this->input->post('SYSTEM_EMAIL_FROM_NAME'),
							'SYSTEM_EMAIL_REPLYTO_EMAIL'		=> $this->input->post('SYSTEM_EMAIL_FROM_EMAIL'),
							'ALERT_RECIPIENT_EMAIL'				=> $this->input->post('ALERT_RECIPIENT_EMAIL'),
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		}
	// Update settings - End }

	return array(
				'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0335'],
				);
	}



} // end of class User
?>