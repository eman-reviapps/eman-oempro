<?php
class Controller_SystemCheck extends MY_Controller
{
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	}

function index()
	{
	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/about/#form-tabs/tab-2', 'location', '302');
	}
}

?>