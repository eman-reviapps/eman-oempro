<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_UploadsAndMediaLibrary extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	if ($this->input->post('Command') == 'Uploads')
		{
		$ArrayEventReturn = $this->_EventEditUploadSettings();
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminUploadsAndMedia'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'UploadsAndMediaLibrary',
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Upload settings update event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditUploadSettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage'	=> 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'MEDIA_UPLOAD_METHOD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0210'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'IMPORT_MAX_FILESIZE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0221'],
								'rules'		=> 'required|numeric',
								),
							array
								(
								'field'		=> 'ATTACHMENT_MAX_FILESIZE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0223'],
								'rules'		=> 'required|numeric',
								),
							array
								(
								'field'		=> 'MEDIA_MAX_FILESIZE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0224'],
								'rules'		=> 'required|numeric',
								),
							);
	if ($this->input->post('MEDIA_UPLOAD_METHOD') == 's3')
		{
		$ArrayFormRules[] = array
								(
								'field'		=> 'S3_ACCESS_ID',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0215'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'S3_SECRET_KEY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0216'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'S3_BUCKET',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0217'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'S3_MEDIALIBRARY_PATH',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0218'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'S3_URL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0219'],
								'rules'		=> 'required',
								);
		}


	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'MEDIA_UPLOAD_METHOD'		=> $this->input->post('MEDIA_UPLOAD_METHOD'),
							'IMPORT_MAX_FILESIZE'		=> $this->input->post('IMPORT_MAX_FILESIZE'),
							'ATTACHMENT_MAX_FILESIZE'	=> $this->input->post('ATTACHMENT_MAX_FILESIZE'),
							'MEDIA_MAX_FILESIZE'		=> $this->input->post('MEDIA_MAX_FILESIZE'),
							'S3_ACCESS_ID'				=> $this->input->post('S3_ACCESS_ID'),
							'S3_SECRET_KEY'				=> $this->input->post('S3_SECRET_KEY'),
							'S3_BUCKET'					=> $this->input->post('S3_BUCKET'),
							'S3_MEDIALIBRARY_PATH'		=> preg_replace(array('/^\//i', '/\/$/i'), array('', ''), $this->input->post('S3_MEDIALIBRARY_PATH')),
							'S3_URL'					=> preg_replace(array('/^\//i', '/\/$/i'), array('', ''), $this->input->post(('S3_URL'))),
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0342'],
					);
		}
	// Update settings - End }		
	}

} // end of class User
?>