<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_EmailDelivery extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'UpdateEmailDeliverySettings')
		{
		$ArrayEventReturn = $this->_EventEditEmailDeliverySettings();
		if (isset($ArrayEventReturn['PageSuccessMessage']))
			{
			$this->load->helper('url');
			redirect(InterfaceAppURL(true).'/admin/emaildelivery','refresh');
			}
		}
	else if ($this->input->post('Command') == 'TestEmailDeliverySettings')
		{
		$ArrayEventReturn = $this->_EventTestEmailDeliverySettings();
		}
	// Events - End }

	$emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');
	$emailHeaders = $emailHeaderMapper->findAll();

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminEmailDelivery'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'EmailDelivery',
							'EmailHeaders'			=> $emailHeaders
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Test email delivery settings event
 *
 * @return void
 * @author Mert Hurturk
 **/
function _EventTestEmailDeliverySettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}
		
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'email.delivery.test',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	array('dummy'=>'dummy')
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			case '1':
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0319'], $ArrayReturn->EmailSettingsErrorMessage),
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0334'],
					);
		}
	}

/**
 * Update email delivery event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditEmailDeliverySettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'X_MAILER',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0298'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'SEND_METHOD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0118'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'LOAD_BALANCE_STATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0117'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'BOUNCE_CATCHALL_DOMAIN',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0399'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'THRESHOLD_SOFT_BOUNCE_DETECTION',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0401'],
								'rules'		=> 'required|integer',
								),
							array
								(
								'field'		=> 'POP3_BOUNCE_STATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0405'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'REPORT_ABUSE_EMAIL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0412'],
								'rules'		=> 'required|valid_email',
								),
							array
								(
								'field'		=> 'X_COMPLAINTS_TO',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0414'],
								'rules'		=> 'required|valid_email',
								),
							array
								(
								'field'		=> 'FBL_INCOMING_EMAILADDRESS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0416'],
								'rules'		=> 'valid_email',
								),
							array
								(
								'field'		=> 'POP3_FBL_STATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0405'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'CENTRALIZED_SENDER_DOMAIN',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1665'],
								'rules'		=> '',
								),
							);

	if ($this->input->post('POP3_FBL_STATUS') == 'Enabled')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_FBL_HOST',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0409'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_FBL_PORT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0410'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_FBL_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0411'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_FBL_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0412'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_FBL_SSL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1656'],
								'rules'		=> '',
								);
		}
	if ($this->input->post('POP3_BOUNCE_STATUS') == 'Enabled')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_BOUNCE_HOST',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0409'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_BOUNCE_PORT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0410'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_BOUNCE_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0411'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_BOUNCE_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0412'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_BOUNCE_SSL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1656'],
								'rules'		=> '',
								);
		}
	if ($this->input->post('LOAD_BALANCE_STATUS') == 'true')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'LOAD_BALANCE_EMAILS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0395'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'LOAD_BALANCE_SLEEP',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0397'],
								'rules'		=> 'required|integer',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SMTP_HOST',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SMTP_PORT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0381'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SMTP_SECURE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0382'],
								'rules'		=> '',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SMTP_TIMEOUT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0387'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SMTP_AUTH',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0389'],
								'rules'		=> 'required',
								);
		if ($this->input->post('SEND_METHOD_SMTP_AUTH') == 'true')
			{
			$ArrayFormRules[] = array(
									'field'		=> 'SEND_METHOD_SMTP_USERNAME',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
									'rules'		=> 'required',
									);
			$ArrayFormRules[] = array(
									'field'		=> 'SEND_METHOD_SMTP_PASSWORD',
									'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
									'rules'		=> 'required',
									);		
			}
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP-OCTETH')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_OCTETHSMTP_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_OCTETHSMTP_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP-SENDGRID')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SENDGRID_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SENDGRID_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP-MAILGUN')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_MAILGUN_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_MAILGUN_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP-SES')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SES_HOST',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SES_SECURE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0379'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SES_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0375'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SES_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0376'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SMTP-POSTMARK')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_POSTMARK_APIKEY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1870'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'SaveToDisk')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_SAVETODISK_DIR',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0317'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'PowerMTA')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_POWERMTA_VMTA',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0320'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_POWERMTA_DIR',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0371'],
								'rules'		=> 'required',
								);
		}
	if ($this->input->post('SEND_METHOD') == 'LocalMTA')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'SEND_METHOD_LOCALMTA_PATH',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0373'],
								'rules'		=> 'required',
								);
		}
	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }
	
	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
	if ($this->input->post('SEND_METHOD') == 'SMTP-OCTETH')
		{
		$SendMethod			= 'SMTP';
		$SMTPUsername		= $this->input->post('SEND_METHOD_OCTETHSMTP_USERNAME');
		$SMTPPassword		= $this->input->post('SEND_METHOD_OCTETHSMTP_PASSWORD');
		$SMTPHost			= 'octeth.smtp.com';
		$SMTPPort			= 25;
		$SMTPSecure			= '';
		$SMTPTimeout		= 15;
		$SMTPAuth			= 'true';
		}
	elseif ($this->input->post('SEND_METHOD') == 'SMTP-SENDGRID')
		{
		$SendMethod			= 'SMTP';
		$SMTPUsername		= $this->input->post('SEND_METHOD_SENDGRID_USERNAME');
		$SMTPPassword		= $this->input->post('SEND_METHOD_SENDGRID_PASSWORD');
		$SMTPHost			= 'smtp.sendgrid.net';
		$SMTPPort			= 25;
		$SMTPSecure			= '';
		$SMTPTimeout		= 15;
		$SMTPAuth			= 'true';
		}
	elseif ($this->input->post('SEND_METHOD') == 'SMTP-MAILGUN')
		{
		$SendMethod			= 'SMTP';
		$SMTPUsername		= $this->input->post('SEND_METHOD_MAILGUN_USERNAME');
		$SMTPPassword		= $this->input->post('SEND_METHOD_MAILGUN_PASSWORD');
		$SMTPHost			= 'smtp.mailgun.org';
		$SMTPPort			= 25;
		$SMTPSecure			= '';
		$SMTPTimeout		= 15;
		$SMTPAuth			= 'true';
		}
	elseif ($this->input->post('SEND_METHOD') == 'SMTP-SES')
		{
		$SendMethod			= 'SMTP';
		$SMTPUsername		= $this->input->post('SEND_METHOD_SES_USERNAME');
		$SMTPPassword		= $this->input->post('SEND_METHOD_SES_PASSWORD');
		$SMTPHost			= $this->input->post('SEND_METHOD_SES_HOST');
		$SMTPPort			= 25;
		$SMTPSecure			= $this->input->post('SEND_METHOD_SES_SECURE');
		$SMTPTimeout		= 15;
		$SMTPAuth			= 'true';
		}
	elseif ($this->input->post('SEND_METHOD') == 'SMTP-POSTMARK')
		{
		$SendMethod			= 'SMTP';
		$SMTPUsername		= $this->input->post('SEND_METHOD_POSTMARK_APIKEY');
		$SMTPPassword		= $this->input->post('SEND_METHOD_POSTMARK_APIKEY');
		$SMTPHost			= 'smtp.postmarkapp.com';
		$SMTPPort			= 25;
		$SMTPSecure			= '';
		$SMTPTimeout		= 15;
		$SMTPAuth			= 'true';
		}
	else
		{
		$SendMethod			= $this->input->post('SEND_METHOD');
		$SMTPUsername		= $this->input->post('SEND_METHOD_SMTP_USERNAME');
		$SMTPPassword		= $this->input->post('SEND_METHOD_SMTP_PASSWORD');
		$SMTPHost			= $this->input->post('SEND_METHOD_SMTP_HOST');
		$SMTPPort			= $this->input->post('SEND_METHOD_SMTP_PORT');
		$SMTPSecure			= $this->input->post('SEND_METHOD_SMTP_SECURE');
		$SMTPTimeout		= $this->input->post('SEND_METHOD_SMTP_TIMEOUT');
		$SMTPAuth			= $this->input->post('SEND_METHOD_SMTP_AUTH');
		}

		$ArrayAPIVars = array(
							'X_MAILER'								=> $this->input->post('X_MAILER'),
							'CENTRALIZED_SENDER_DOMAIN'				=> ($this->input->post('CENTRALIZED_SENDER_DOMAIN') == false ? -1 : $this->input->post('CENTRALIZED_SENDER_DOMAIN')),
							'SEND_METHOD'							=> $SendMethod,
							'SEND_METHOD_SAVETODISK_DIR'			=> $this->input->post('SEND_METHOD_SAVETODISK_DIR'),
							'SEND_METHOD_POWERMTA_VMTA'				=> $this->input->post('SEND_METHOD_POWERMTA_VMTA'),
							'SEND_METHOD_POWERMTA_DIR'				=> $this->input->post('SEND_METHOD_POWERMTA_DIR'),
							'SEND_METHOD_LOCALMTA_PATH'				=> $this->input->post('SEND_METHOD_LOCALMTA_PATH'),
							'SEND_METHOD_SMTP_HOST'					=> $SMTPHost,
							'SEND_METHOD_SMTP_PORT'					=> $SMTPPort,
							'SEND_METHOD_SMTP_SECURE'				=> $SMTPSecure,
							'SEND_METHOD_SMTP_TIMEOUT'				=> $SMTPTimeout,
							'SEND_METHOD_SMTP_AUTH'					=> $SMTPAuth,
							'SEND_METHOD_SMTP_USERNAME'				=> $SMTPUsername,
							'SEND_METHOD_SMTP_PASSWORD'				=> $SMTPPassword,
							'LOAD_BALANCE_STATUS'					=> $this->input->post('LOAD_BALANCE_STATUS'),
							'LOAD_BALANCE_EMAILS'					=> $this->input->post('LOAD_BALANCE_EMAILS'),
							'LOAD_BALANCE_SLEEP'					=> $this->input->post('LOAD_BALANCE_SLEEP'),
							'BOUNCE_CATCHALL_DOMAIN'				=> $this->input->post('BOUNCE_CATCHALL_DOMAIN'),
							'THRESHOLD_SOFT_BOUNCE_DETECTION'		=> $this->input->post('THRESHOLD_SOFT_BOUNCE_DETECTION'),
							'POP3_BOUNCE_STATUS'					=> $this->input->post('POP3_BOUNCE_STATUS'),
							'POP3_BOUNCE_HOST'						=> $this->input->post('POP3_BOUNCE_HOST'),
							'POP3_BOUNCE_PORT'						=> $this->input->post('POP3_BOUNCE_PORT'),
							'POP3_BOUNCE_USERNAME'					=> $this->input->post('POP3_BOUNCE_USERNAME'),
							'POP3_BOUNCE_PASSWORD'					=> $this->input->post('POP3_BOUNCE_PASSWORD'),
							'POP3_BOUNCE_SSL'						=> ($this->input->post('POP3_BOUNCE_SSL') == 'Yes' ? 'Yes' : 'No'),
							'REPORT_ABUSE_EMAIL'					=> $this->input->post('REPORT_ABUSE_EMAIL'),
							'X_COMPLAINTS_TO'						=> $this->input->post('X_COMPLAINTS_TO'),
							'FBL_INCOMING_EMAILADDRESS'				=> $this->input->post('FBL_INCOMING_EMAILADDRESS'),
							'POP3_FBL_STATUS'						=> $this->input->post('POP3_FBL_STATUS'),
							'POP3_FBL_HOST'							=> $this->input->post('POP3_FBL_HOST'),
							'POP3_FBL_PORT'							=> $this->input->post('POP3_FBL_PORT'),
							'POP3_FBL_USERNAME'						=> $this->input->post('POP3_FBL_USERNAME'),
							'POP3_FBL_PASSWORD'						=> $this->input->post('POP3_FBL_PASSWORD'),
							'POP3_FBL_SSL'							=> ($this->input->post('POP3_FBL_SSL') == 'Yes' ? 'Yes' : 'No'),
							'SEND_BOUNCE_NOTIFICATIONEMAIL'			=> ($this->input->post('SEND_BOUNCE_NOTIFICATIONEMAIL') == false ? 'Disabled' : 'Enabled'),
							);

	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			case '5':
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0319'], $ArrayReturn->EmailSettingsErrorMessage),
							);
				break;
			case '6':
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0411'], $ArrayReturn->EmailSettingsErrorMessage),
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0315'],
					);
		}
	// Update settings - End }		
	}

function snippet_addEmailHeader()
	{
	header('Content-type: application/json');

	if (count($_POST) < 1)
		{
		echo "{'success':false}";
		exit;
		}

	if (!isset($_POST['HeaderName']) || $_POST['HeaderName'] == ''
			|| !isset($_POST['EmailType']) || $_POST['EmailType'] == ''
			|| !isset($_POST['HeaderValue']) || $_POST['HeaderValue'] == ''
	)
		{
		echo "{'success':false}";
		exit;
		}

	$emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');

	$emailHeader = new O_Domain_EmailHeader();
	$emailHeader->setEmailType($_POST['EmailType']);
	$emailHeader->setName($_POST['HeaderName']);
	$emailHeader->setValue($_POST['HeaderValue']);
	$emailHeaderMapper->insert($emailHeader);

	echo "{'success':true,'id':" . $emailHeader->getId() . ",'name':'" . htmlspecialchars($emailHeader->getName(), ENT_QUOTES) . "','type':'" . htmlspecialchars($emailHeader->getEmailType(), ENT_QUOTES) . "','value':'" . htmlspecialchars($emailHeader->getValue(), ENT_QUOTES) . "'}";
	}

function snippet_removeEmailHeaders()
	{
	header('Content-type: application/json');

	if (count($_POST) < 1)
		{
		echo "{'success':false}";
		exit;
		}

	if (!isset($_POST['HeaderIDs']) || $_POST['HeaderIDs'] == '')
		{
		echo "{'success':false}";
		exit;
		}

	$arrayHeaderIds = explode(',', $_POST['HeaderIDs']);
	$emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');

	foreach ($arrayHeaderIds as $each)
		{
		$emailHeaderMapper->deleteByHeaderId($each);
		}

	echo "{'success':true}";
	}

} // end of class User
?>