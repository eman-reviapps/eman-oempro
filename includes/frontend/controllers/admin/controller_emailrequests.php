<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_EmailRequests extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'EditEmailRequests')
		{
		$ArrayEventReturn = $this->_EventEditEmailRequestSettings();
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminEmailRequests'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'EmailRequests',
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Update email delivery event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditEmailRequestSettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage' => 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'POP3_REQUESTS_STATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0405'],
								'rules'		=> 'required',
								),
							);

	if ($this->input->post('POP3_REQUESTS_STATUS') == 'Enabled')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_REQUESTS_HOST',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0409'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_REQUESTS_PORT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0410'],
								'rules'		=> 'required|integer',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_REQUESTS_USERNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0411'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_REQUESTS_PASSWORD',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0412'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'POP3_REQUESTS_SSL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1656'],
								'rules'		=> '',
								);
		}
	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }
	
	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'POP3_REQUESTS_STATUS'							=> $this->input->post('POP3_REQUESTS_STATUS'),
							'POP3_REQUESTS_HOST'							=> $this->input->post('POP3_REQUESTS_HOST'),
							'POP3_REQUESTS_PORT'							=> $this->input->post('POP3_REQUESTS_PORT'),
							'POP3_REQUESTS_USERNAME'						=> $this->input->post('POP3_REQUESTS_USERNAME'),
							'POP3_REQUESTS_PASSWORD'						=> $this->input->post('POP3_REQUESTS_PASSWORD'),
							'POP3_REQUESTS_SSL'								=> ($this->input->post('POP3_REQUESTS_SSL') == 'Yes' ? 'Yes' : 'No'),
							);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			case '5':
				return array(
							'PageErrorMessage'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0319'],
							);
				break;
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0419'],
					);
		}
	// Update settings - End }		
	}



} // end of class User
?>