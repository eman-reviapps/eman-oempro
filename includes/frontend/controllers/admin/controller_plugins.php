<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_Plugins extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	// Events - End }

	// Retrieve the list of available plug-ins in the system - Start {
	$ArrayPlugIns = Plugins::GetPlugInList(false);
	// Retrieve the list of available plug-ins in the system - End }

	$PageSuccessMessage = null;
	$PageErrorMessage = null;

	if (isset($_SESSION['PluginMessage']) == true && is_array($_SESSION['PluginMessage']) == true && isset($_SESSION['PluginMessage'][1]) == true)
	{
		if ($_SESSION['PluginMessage'][0] == false)
		{
			$PageErrorMessage = $_SESSION['PluginMessage'][1];
		}
		else
		{
			$PageSuccessMessage = $_SESSION['PluginMessage'][1];
		}

	}

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPlugIns'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'Plugins',
							'PlugIns'				=> $ArrayPlugIns,
							'PageSuccessMessage'	=> $PageSuccessMessage,
							'PageErrorMessage'		=> $PageErrorMessage,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * Enable the plug-in
 *
 * @author Cem Hurturk
 */
function enable($PlugInCode)
	{
	$this->load->helper('url');

	if (DEMO_MODE_ENABLED == true)
		{
		redirect(InterfaceAppURL(true).'/admin/plugins/', 'location', '302');
		}

	$this->_EnableDisablePlugIn($PlugInCode);
	redirect(InterfaceAppURL(true).'/admin/plugins/', 'redirect');
	}

/**
 * Disable the plug-in
 *
 * @author Cem Hurturk
 */
function disable($PlugInCode)
	{
	$this->load->helper('url');

	if (DEMO_MODE_ENABLED == true)
		{
		redirect(InterfaceAppURL(true).'/admin/plugins/', 'location', '302');
		}

	$this->_EnableDisablePlugIn($PlugInCode);
	redirect(InterfaceAppURL(true).'/admin/plugins/', 'location', '302');
	}

/**
 * Plug-in enable/disable event
 *
 * @param string $PlugInCode 
 * @return void
 * @author Cem Hurturk
 */
function _EnableDisablePlugIn($PlugInCode)
	{
	$IsPlugInEnabled			= false;
	$ArrayEnabledPlugins		= explode(",", ENABLED_PLUGINS);
	$ArrayNewEnabledPlugins		= array();

	if (in_array($PlugInCode, $ArrayEnabledPlugins) == true)
		{
		$IsPlugInEnabled = true;
		}

	if ($IsPlugInEnabled == true)
		{
		foreach ($ArrayEnabledPlugins as $EachEnabledPlugin)
			{
			if ($EachEnabledPlugin != $PlugInCode)
				{
				$ArrayNewEnabledPlugins[] = $EachEnabledPlugin;
				}
			}
		}
	else
		{
		$ArrayNewEnabledPlugins	= $ArrayEnabledPlugins;
		$ArrayNewEnabledPlugins[] = $PlugInCode;
		}

	$ArrayFieldsnValues = array(
								'ENABLED_PLUGINS'		=> implode(',', $ArrayNewEnabledPlugins),
								);
	Core::UpdateSettings($ArrayFieldsnValues, array('ConfigID' => 1));

	if ($IsPlugInEnabled == true)
		{
		// Disable the plugin

		// Load the plug-in - Start
		Plugins::Load($PlugInCode, false);
		// Load the plug-in - End		

		// Disable plug-in - Start {
		Plugins::DisablePlugIn($PlugInCode);
		// Disable plug-in - End }
		}
	else
		{
		// Enable the plugin

		// Load the plug-in - Start
		Plugins::Load($PlugInCode, false);
		// Load the plug-in - End		

		// Disable plug-in - Start {
		Plugins::EnablePlugIn($PlugInCode);
		// Disable plug-in - End }

		// Call load function of plugin - Start
		Plugins::CallLoad($PlugInCode);
		// Call load function of plugin - End		
		}
	}

} // end of class User
?>