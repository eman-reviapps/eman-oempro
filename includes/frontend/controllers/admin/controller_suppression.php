<?php
class Controller_Suppression extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('admin_auth');
		// Load other modules - End

		// Check the login session, redirect based on the login session status - Start
		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
		// Check the login session, redirect based on the login session status - End
	}

	public function index()
	{
		// Events - Start {
		$addEventReturn = false;
		$totalEmailAddressesAdded = 0;
		$deletePatternEventReturn = false;
		$addPatternEventReturn = false;
		$searchEventReturn = false;
		$foundSuppressions = array();
		$deleteEventReturn = false;
		if ($this->input->post('Command') == 'AddSuppression') {
			$addEventReturn = $this->_addEmailAddresses();
			$totalEmailAddressesAdded = $addEventReturn[1];
			$addEventReturn = $addEventReturn[0];
		} else if ($this->input->post('Command') == 'DeleteSuppressionPattern') {
			$deletePatternEventReturn = $this->_deleteSuppressionPattern();
		} else if ($this->input->post('Command') == 'AddSuppressionPattern') {
			$addPatternEventReturn = $this->_addSuppressionPattern();
		} else if ($this->input->post('Command') == 'Search') {
			$searchEventReturn = $this->_search();
			$foundSuppressions = $searchEventReturn[1];
			$searchEventReturn = $searchEventReturn[0];
		} else if ($this->input->post('Command') == 'DeleteSuppression') {
			$deleteEventReturn = $this->_deleteSuppression();
		}
		// Events - End }

		$suppressionMapper = O_Registry::instance()->getMapper('Suppression');
		$ratiosBySource = $suppressionMapper->getRatiosBySource();

		$suppressionPatternMapper = O_Registry::instance()->getMapper('SuppressionPattern');
		$suppressionPatterns = $suppressionPatternMapper->findAll();

		$viewData = array(
			'PageTitle'					=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['Screen']['1884'],
			'RatiosBySource'			=> $ratiosBySource,
			'AddEventReturn'			=> $addEventReturn,
			'TotalEmailAddressesAdded'	=> $totalEmailAddressesAdded,
			'SuppressionPatterns'		=> $suppressionPatterns,
			'DeletePatternEventReturn'	=> $deletePatternEventReturn,
			'AddPatternEventReturn'		=> $addPatternEventReturn,
			'SearchEventReturn'			=> $searchEventReturn,
			'FoundSuppressions'			=> $foundSuppressions,
			'DeleteEventReturn'			=> $deleteEventReturn,
			'CurrentMenuItem'			=> 'Suppression'
		);
		$this->render('admin/suppression', $viewData);
	}

	protected function _search()
	{
		$ArrayFormRules = array(
			array(
				'field'		=> 'EmailAddress',
				'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0129'],
				'rules'		=> 'required',
			)
		);
		$this->form_validation->set_rules($ArrayFormRules);
		if ($this->form_validation->run() == false) {
			return array(false, array());
		}

		$suppressionMapper = O_Registry::instance()->getMapper('Suppression');
		$suppressions = $suppressionMapper->findByEmailAddress($this->input->post('EmailAddress'));

		return array(true, $suppressions);
	}

	protected function _addEmailAddresses()
	{
		$ArrayFormRules = array(
			array(
				'field'		=> 'EmailAddresses',
				'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0129'],
				'rules'		=> 'required',
			)
		);
		$this->form_validation->set_rules($ArrayFormRules);
		if ($this->form_validation->run() == false) {
			return array(false, 0);
		}

		$emailAddresses = explode("\n", $this->input->post('EmailAddresses'));
		$suppressionMapper = O_Registry::instance()->getMapper('Suppression');
		$totalEmailAddressesAdded = $suppressionMapper->insertSystemLevel($emailAddresses);

		return array(true, $totalEmailAddressesAdded);
	}

	protected function _addSuppressionPattern()
	{
		$ArrayFormRules = array(
			array(
				'field'		=> 'Description',
				'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1903'],
				'rules'		=> 'required',
			),
			array(
				'field'		=> 'Pattern',
				'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1897'],
				'rules'		=> 'required',
			)
		);
		$this->form_validation->set_rules($ArrayFormRules);
		if ($this->form_validation->run() == false) {
			return false;
		}

		$pattern = new O_Domain_SuppressionPattern();
		$pattern->setPattern($this->input->post('Pattern'));
		$pattern->setType($this->input->post('PatternType'));
		$pattern->setDescription($this->input->post('Description'));

		$suppressionPatternMapper = O_Registry::instance()->getMapper('SuppressionPattern');
		$suppressionPatternMapper->insert($pattern);

		return true;
	}

	protected function _deleteSuppressionPattern()
	{
		$patternId = $this->input->post('PatternID');

		if (! $patternId)
			return false;

		$suppressionPatternMapper = O_Registry::instance()->getMapper('SuppressionPattern');
		$suppressionPatternMapper->deleteById($patternId);

		return true;
	}

	protected function _deleteSuppression()
	{
		$suppressionId = $this->input->post('SuppressionID');

		if (! $suppressionId)
			return false;

		$suppressionMapper = O_Registry::instance()->getMapper('Suppression');
		$suppressionMapper->deleteById($suppressionId);

		return true;
	}

}