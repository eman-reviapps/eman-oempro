<?php
/**
 * Email controller
 *
 * @author Mert Hurturk
 */

class Controller_Email extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Mert Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		$this->load->helper('url');

		Core::LoadObject('admin_auth');
		Core::LoadObject('users');
		Core::LoadObject('api');
		Core::LoadObject('lists');
		Core::LoadObject('emails');
		Core::LoadObject('wizard');
		Core::LoadObject('split_tests');
		// Load other modules - End	

		// Check the login session, redirect based on the login session status - Start
		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
		// Check the login session, redirect based on the login session status - End
	}

	/**
	 * Preview controller
	 *
	 * @author Mert Hurturk
	 **/
	function preview($email_id, $type = 'html', $mode = 'campaign', $entity_id = 0, $command = '')
	{
		// Load other modules - Start
		Core::LoadObject('user_auth');
		Core::LoadObject('subscribers');
		Core::LoadObject('lists');
		Core::LoadObject('emails');
		Core::LoadObject('campaigns');
		Core::LoadObject('personalization');
		// Load other modules - End

		$email_information = Emails::RetrieveEmail(array('*'), array('EmailID' => $email_id), TRUE);

		// Select one of the recipient lists from the campaign - Start
		$campaign_information = array();
		if ($mode == 'campaign')
		{
			$campaign_information = Campaigns::RetrieveCampaigns_Enhanced(
				array(
					'Criteria' => array('Column' => '%c%.CampaignID', 'Operator' => '=', 'Value' => $entity_id),
					'Reports' => array()
				)
			);
			$campaign_information = $campaign_information[0];
			$array_list = Lists::RetrieveList(array('*'), array('ListID' => $campaign_information['RecipientLists'][0]), false);
		}
		// Select one of the recipient lists from the campaign - End

		// Retrieve a random subscriber from the target list - Start
		$array_random_subscriber = Subscribers::SelectRandomSubscriber($array_list['ListID'], true);
		// Retrieve a random subscriber from the target list - End

		$email_information['Subject'] = Personalization::Personalize($email_information['Subject'], array('Subscriber', 'User'), $array_random_subscriber, $this->ArrayUserinformation, $array_list, $campaign_information, array(), true);

		if ($command != '' && $command == 'source')
		{
			if ($type == 'html')
			{
				if ($email_information['FetchURL'] != '')
				{
					$personalized_url = Personalization::Personalize($email_information['FetchURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), false);
					$show_content = Campaigns::FetchRemoteContent($personalized_url);
				}
				else
				{
					$show_content = $email_information['HTMLContent'];
				}
				
				if ($show_content == '')
				{
					$type = 'plain';
				}
				
				if ($type != 'plain') {
					// Plug-in hook - Start
					$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
					$show_content = $array_plugin_return_vars[1];
					// Plug-in hook - End

					// Plug-in hook - Start
					$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
					$show_content = $array_plugin_return_vars[1];
					// Plug-in hook - End
				}
			}
			
			if ($type == 'plain')
			{
				if ($email_information['FetchPlainURL'] != '')
				{
					$personalized_url = Personalization::Personalize($email_information['FetchPlainURL'], array('Subscriber', 'User'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), false);
					$show_content = Campaigns::FetchRemoteContent($personalized_url);	
				}
				else
				{
					$show_content = $email_information['PlainContent'];
				}

				// Plug-in hook - Start
				$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $show_content, '', $array_random_subscriber));
				$show_content = $array_plugin_return_vars[1];
				// Plug-in hook - End

				// Plug-in hook - Start
				$array_plugin_return_vars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $show_content, '', $array_random_subscriber, 'Browser Preview'));
				$show_content = $array_plugin_return_vars[1];
				// Plug-in hook - End
			}

			// Add header/footer to the email (if exists in the user group) - Start {
			$ArrayReturn = Personalization::AddEmailHeaderFooter(($type == 'plain' ? $show_content : ''), ($type == 'html' ? $show_content : ''), $this->ArrayUserInformation['GroupInformation']);
			if ($type == 'plain')
				{
				$show_content = $ArrayReturn[0];
				}
			else
				{
				$show_content = $ArrayReturn[1];
				}
			// Add header/footer to the email (if exists in the user group) - End }

			// Perform personalization (disable personalization and links if subscriber information not provided) - Start
			$show_content = Personalization::Personalize($show_content, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $array_random_subscriber, $this->ArrayUserInformation, $array_list, $campaign_information, array(), true);
			// Perform personalization (disable personalization and links if subscriber information not provided) - End

			if ($type == 'plain')
			{
				$show_content = '<pre>'.$show_content.'</pre>';
			}

			print $show_content;
			exit;
		}

		// Interface parsing - Start {
		$ArrayViewData 	= array(
								'PageTitle'						=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['UserEmailPreview'],
								'EmailInformation'				=> $email_information,
								'Mode'							=> $mode,
								'EntityID'						=> $entity_id,
								'EmailID'						=> $email_id,
								'Type'							=> $type
								);

		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		$this->render('admin/email_preview', $ArrayViewData);
		// Interface parsing - End }

	}

}