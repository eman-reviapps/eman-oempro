<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Captcha extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index($name)
	{
	$GeneratedString = Core::GenerateCAPTCHA(5, 90, 30, 242, 242, 242, 103, 103, 103, 10, 7, ($_SESSION[SESSION_NAME]['AdminCaptcha'] != '' ? $_SESSION[SESSION_NAME]['AdminCaptcha'] : ''));
	
	$_SESSION[SESSION_NAME][$name] = $GeneratedString;
	
	exit;
	}


} // end of class User
?>