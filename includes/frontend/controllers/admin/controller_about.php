<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */
class Controller_About extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Cem Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('admin_auth');
		// Load other modules - End

		// Check the login session, redirect based on the login session status - Start
		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
		// Check the login session, redirect based on the login session status - End
	}

	/**
	 * Index controller
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function index()
	{
		// Events - Start {
		if ($this->input->post('Command') == 'DatabaseOptimize') {
			Core::OptimizeOemproMySQLTables();

			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0469']);
		}
		if ($this->input->post('Command') == 'DatabaseRepair') {
			Core::RepairOemproMySQLTables();

			$_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0470']);
		}
		if ($this->input->post('Command') == 'DatabaseExport') {
			$ExportSQL = Core::ExportOemproMySQLTables(false);

			Core::SendDownloadHeaders('oempro_db_export.sql', strlen($ExportSQL), $ContentType = "application/octetstream");
			print $ExportSQL;
			exit;
		}
		// Events - End }

		// Learn the latest available version - Start {
		Core::LoadObject('install');
		$LatestAvailableVersion = Install::WhatIsLatestVersion();
		// Learn the latest available version - End }

		// Check PHP settings - Start {
		$ArrayPHPSettingsToShow = array('memory_limit', 'max_execution_time', 'safe_mode', 'magic_quotes_gpc', 'register_globals', 'magic_quotes_runtime', 'max_execution_time');
		$ArrayPHPSettingsCheck = array();
		foreach ($ArrayPHPSettingsToShow as $EachSetting)
		{
			$PHPSettingValue = ini_get($EachSetting);
			$ArrayPHPSettingsCheck[$EachSetting] = ($PHPSettingValue == ''
					? 'false' : $PHPSettingValue);
		}
		// Check PHP settings - End }

		// System check - Start {
		$ArraySystemCheckResults = array();
		$ArrayErrors = Core::SystemCheck(true);
		foreach ($ArrayErrors[1] as $Category => $EachError)
		{
			$ArraySystemCheckResults[$Category][] = $EachError[0];
		}
		// System check - End }

		// Interface parsing - Start {
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminAbout'],
			'CurrentMenuItem' => 'Settings',
			'SubSection' => 'About',
			'LatestVersion' => $LatestAvailableVersion,
			'NewVersionExists' => (Install::IsNewVersion($LatestAvailableVersion) == false
					? false : true),
			'PHPSettingsCheck' => $ArrayPHPSettingsCheck,
			'SystemCheckResults' => $ArraySystemCheckResults,
			'MySQLVersion' => Core::MySQLVersion()
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
			$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success'
					? 'PageSuccessMessage'
					: ($_SESSION['PageMessageCache'][0] == 'Notice'
							? 'PageNoticeMessage'
							: 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
		}
		// Check if there is any message in the message buffer - End }

		$this->render('admin/settings', $ArrayViewData);
		// Interface parsing - End }
	}

	function download($EncodedTargetFile)
	{
		$TargetFile = base64_decode(rawurldecode($EncodedTargetFile));
		$FileName = basename($TargetFile);

		Core::SendDownloadHeaders($FileName, filesize(APP_PATH . $TargetFile), "application/octetstream");
		$FileHandler = fopen(APP_PATH . $TargetFile, 'rb');
		fpassthru($FileHandler);
		fclose($FileHandler);
		return;
	}

	function phpsettings()
	{
		phpinfo();
	}

	function databaseHealthTestResults()
	{
		$DBTableCheckResults = Core::CheckOemproMySQLTables();

		echo '<table class="small-grid" style="margin-top:3px;">';
		echo '<tr>';
		echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0466'] . '</th>';
		echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0467'] . '</th>';
		echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0468'] . '</th>';
		echo '</tr>';

		foreach ($DBTableCheckResults as $Table => $Result) {
			echo '<tr>';
			echo '<td>' . $Table . '</td>';
			echo '<td>' . $Result['MessageType'] . '</td>';
			echo '<td>' . $Result['MessageText'] . '</td>';
			echo '</tr>';
		}

		echo '</table>';
	}

	function databaseStructureTestResults()
	{
		$DBCheckResults = Database::$Interface->CheckDBIntegrity();

		if (count($DBCheckResults) < 1) {
			echo '<div class="form-row no-background-color" id="form-row-SystemCheck">';
			echo '<p>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1669'] . '</p>';
			echo '</div>';
		} else {
			echo '<table class="small-grid" style="margin-top:3px;">';
			echo '<tr>';
			echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0466'] . '</th>';
			echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0468'] . '</th>';
			echo '</tr>';

			foreach ($DBCheckResults as $Table => $Result) {
				echo '<tr>';
				echo '<td>' . $Table . '</td>';
				echo '<td>';
				if (strtolower(gettype($Result)) == 'string') {
					if ($Result == 'FIRST_FIELD_DOESNT_EXIST') {
						echo ApplicationHeader::$ArrayLanguageStrings['Screen']['1670'];
					} elseif ($Result == 'SECOND_FIELD_DOESNT_EXIST') {
						echo ApplicationHeader::$ArrayLanguageStrings['Screen']['1670'];
					} else {
						echo $Result;
					}
				} else {
					foreach ($Result as $EachField => $Result2) {
						if (($Result2 == 'FIRST_FIELD_DOESNT_EXIST') || ($Result2 == 'SECOND_FIELD_DOESNT_EXIST')) {
							echo sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1675'], array($EachField));
						} elseif (($Result2 == 'FIRST_FIELD_TYPE_INCORRECT') || ($Result2 == 'SECOND_FIELD_TYPE_INCORRECT')) {
							echo sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['1671'], array($EachField));
						} else {
							echo $Result2;
						}
					}
				}
				echo '</td>';
				echo '</tr>';
			}
			echo '</table>';
		}
	}

	function fileIntegrityTestResults()
	{
		Core::LoadObject('filesystem');
		$ChangedFiles = FileSystemEngine::LoopFiles(APP_PATH, APP_PATH, array('/_workshop', '/data/tmp', '/data/attachments', '/data/email_templates', '/data/imports', '/data/media', '/data/license.dat', '/data/config.inc.php'), array(), 'MD5Test');

		if (count($ChangedFiles) < 1) {
			echo '<div class="form-row no-background-color" id="form-row-SystemCheck">';
			echo '<p>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['1669'] . '</p>';
			echo '</div>';
		} else {
			echo '<table class="small-grid" style="margin-top:3px;">';
			echo '<tr>';
			echo '<th>' . ApplicationHeader::$ArrayLanguageStrings['Screen']['0482'] . '</th>';
			echo '</tr>';
			foreach ($ChangedFiles as $File => $Params) {
				echo '<tr>';
				echo '<td>' . $File . '</td>';
				echo '</tr>';
			}
			echo '</table>';
		}
	}
}
