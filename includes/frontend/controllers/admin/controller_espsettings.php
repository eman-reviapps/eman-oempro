<?php
/**
 * About controller
 *
 * @author Cem Hurturk
 */

class Controller_ESPSettings extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Load required modules - Start {
	Core::LoadObject('user_groups');
	// Load required modules - End }

	// Events - Start {
	if ($this->input->post('Command') == 'UpdateESPSettings')
		{
		$ArrayEventReturn = $this->_EventEditESPSettings();
		}
	// Events - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminESPSettings'],
							'CurrentMenuItem'		=> 'Settings',
							'SubSection'			=> 'ESPSettings',
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	foreach ($ArrayEventReturn as $Key=>$Value)
		{
		$ArrayViewData[$Key] = $Value;
		}
							
	$this->render('admin/settings', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * ESP Settings Update Event
 *
 * @return void
 * @author Cem Hurturk
 */
function _EventEditESPSettings()
	{
	if (DEMO_MODE_ENABLED == true)
		{
		return array('PageErrorMessage'	=> 'This feature is disabled in demo version.');
		}

	// Field validations - Start {
	$ArrayFormRules = array(
							array
								(
								'field'		=> 'USER_SIGNUP_ENABLED',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0229'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'PAYMENT_CURRENCY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0243'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'PAYMENT_TAX_PERCENT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0244'],
								'rules'		=> 'required|numeric',
								),
							array
								(
								'field'		=> 'PAYMENT_RECEIPT_EMAIL_SUBJECT',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0245'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'PAYMENT_RECEIPT_EMAIL_MESSAGE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0246'],
								'rules'		=> 'required',
								),
							array
								(
								'field'		=> 'PAYPALEXPRESSSTATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0249'],
								'rules'		=> '',
								),
							array
								(
								'field'		=> 'PAYMENT_CREDITS_GATEWAY_URL_STATUS',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1678'],
								'rules'		=> '',
								),
							);

	if ($this->input->post('USER_SIGNUP_ENABLED') == 'true')
		{
		$ArrayFormRules[] = array
								(
								'field'		=> 'USER_SIGNUP_REPUTATION',
								'label'		=> '',
								'rules'		=> '',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'USER_SIGNUP_FIELDS[]',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0240'],
								'rules'		=> '',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'USER_SIGNUP_LANGUAGE',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0018'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array
								(
								'field'		=> 'USER_SIGNUP_GROUPID',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0235'],
								'rules'		=> 'required',
								);
		}

	if ($this->input->post('USER_SIGNUP_GROUPID') == -1)
		{
		$ArrayFormRules[] = array(
								'field'		=> 'USER_SIGNUP_GROUPIDS[]',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0242'],
								'rules'		=> 'required',
								);
		}

	if ($this->input->post('PAYPALEXPRESSSTATUS') == 'Enabled')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'PAYPALEXPRESSBUSINESSNAME',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0250'],
								'rules'		=> 'required|valid_email',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'PAYPALEXPRESSPURCHASEDESCRIPTION',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0170'],
								'rules'		=> 'required',
								);
		$ArrayFormRules[] = array(
								'field'		=> 'PAYPALEXPRESSCURRENCY',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0252'],
								'rules'		=> 'required',
								);
		}

	if ($this->input->post('PAYMENT_CREDITS_GATEWAY_URL_STATUS') == 'Enabled')
		{
		$ArrayFormRules[] = array(
								'field'		=> 'PAYMENT_CREDITS_GATEWAY_URL',
								'label'		=> ApplicationHeader::$ArrayLanguageStrings['Screen']['1678'],
								'rules'		=> 'required',
								);
		}

	$this->form_validation->set_rules($ArrayFormRules);
	// Field validations - End }

	// Run validation - Start {
	if ($this->form_validation->run() == false)
		{
		return array(false);
		}
	// Run validation - End }

	// Update settings - Start {
		$ArrayAPIVars = array(
							'USER_SIGNUP_ENABLED'				=> $this->input->post('USER_SIGNUP_ENABLED'),
							'USER_SIGNUP_REPUTATION'			=> $this->input->post('USER_SIGNUP_REPUTATION'),
							'USER_SIGNUP_FIELDS'				=> implode(',', $this->input->post('USER_SIGNUP_FIELDS')),
							'USER_SIGNUP_LANGUAGE'				=> $this->input->post('USER_SIGNUP_LANGUAGE'),
							'USER_SIGNUP_GROUPID'				=> $this->input->post('USER_SIGNUP_GROUPID'),
							'USER_SIGNUP_GROUPIDS'				=> implode(',', $this->input->post('USER_SIGNUP_GROUPIDS')),
							'PAYMENT_CURRENCY'					=> $this->input->post('PAYMENT_CURRENCY'),
							'PAYMENT_TAX_PERCENT'				=> $this->input->post('PAYMENT_TAX_PERCENT'),
							'PAYMENT_RECEIPT_EMAIL_SUBJECT'		=> $this->input->post('PAYMENT_RECEIPT_EMAIL_SUBJECT'),
							'PAYMENT_RECEIPT_EMAIL_MESSAGE'		=> $this->input->post('PAYMENT_RECEIPT_EMAIL_MESSAGE'),
							'PAYPALEXPRESSSTATUS'				=> $this->input->post('PAYPALEXPRESSSTATUS'),
							'PAYPALEXPRESSBUSINESSNAME'			=> $this->input->post('PAYPALEXPRESSBUSINESSNAME'),
							'PAYPALEXPRESSPURCHASEDESCRIPTION'	=> $this->input->post('PAYPALEXPRESSPURCHASEDESCRIPTION'),
							'PAYPALEXPRESSCURRENCY'				=> $this->input->post('PAYPALEXPRESSCURRENCY'),
							'PAYMENT_CREDITS_GATEWAY_URL'		=> ($this->input->post('PAYMENT_CREDITS_GATEWAY_URL_STATUS') == false ? '' : $this->input->post('PAYMENT_CREDITS_GATEWAY_URL')),
							);
	$ArrayReturn = API::call(array(
		'format'	=>	'object',
		'command'	=>	'settings.update',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	$ArrayAPIVars
		));

	if ($ArrayReturn->Success == false)
		{
		// API Error occurred
		$ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

		switch ($ErrorCode)
			{
			default:
				return array(
							'PageErrorMessage'		=> sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'Settings.Update', $ErrorCode),
							);
				break;
			}
		}
	else
		{
		// API Success
		return array(
					'PageSuccessMessage'	=> ApplicationHeader::$ArrayLanguageStrings['Screen']['0343'],
					);
		}
	// Update settings - End }
	}

} // end of class User
?>