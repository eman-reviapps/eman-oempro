<?php
/**
 * License and Service Info controller
 *
 * @author Cem Hurturk
 */
class Controller_License extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @author Cem Hurturk
	 */
	function __construct()
	{
		parent::__construct();

		// Load other modules - Start
		Core::LoadObject('admin_auth');
		// Load other modules - End

		// Check the login session, redirect based on the login session status - Start
		AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
		// Check the login session, redirect based on the login session status - End
	}

	/**
	 * Index controller
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function index()
		{
		// Interface parsing - Start {
		$ArrayViewData = array(
			'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminLicenseInfo'],
			'CurrentMenuItem' => 'Settings',
			'SubSection' => 'License',
		);
		$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

		// Retrieve license and service information from Octeth servers - Start
		Core::LoadObject('install');
		$LicenseInformation = Install::GetLicenseInfoFromOcteth();
		// Retrieve license and service information from Octeth servers - End

		if (isset($LicenseInformation['LicenseID']) == false || $LicenseInformation['LicenseID'] == '')
			{
			$_SESSION['PageMessageCache'] = array('Error', 'Failed to connect Octeth servers. Please try again later. If problem persists, please contact Octeth Team.');
			header('Location: '.InterfaceAppURL(true).'/admin/about/');
			exit;
			}

		Install::GetLicenseInformation();
		$LicenseFileProperties = Install::$ArrayLicenseProperties;

		// Check if there is any message in the message buffer - Start {
		if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '')
			{
			$ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success'
					? 'PageSuccessMessage'
					: ($_SESSION['PageMessageCache'][0] == 'Notice'
							? 'PageNoticeMessage'
							: 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
			unset($_SESSION['PageMessageCache']);
			}
		// Check if there is any message in the message buffer - End }

		$ArrayViewData = array_merge($ArrayViewData, array(
			'LicenseInformation'		=> $LicenseInformation,
			'LicenseProperties'			=> $LicenseFileProperties,
		));
		$this->render('admin/settings', $ArrayViewData);
		// Interface parsing - End }
		}

	function extend()
		{
		if (DEMO_MODE_ENABLED == true)
			{
			$_SESSION['PageMessageCache'] = array(0 => 'Error', 1 => 'This feature is disabled in demo version.');
			$this->index();
			return ;
			}

		// Retrieve license and service information from Octeth servers - Start
		Core::LoadObject('install');
		$LicenseInformation = Install::GetLicenseInfoFromOcteth();
		// Retrieve license and service information from Octeth servers - End

		Install::GetLicenseInformation();
		$LicenseFileProperties = Install::$ArrayLicenseProperties;

		$EncryptedLicenseInfo = Install::EncryptNumberEnhanced($LicenseInformation['LicenseID']);

		header('Location: http://octeth.com/inapp_gateway/service?referrer='.$EncryptedLicenseInfo);
		exit;
		}

	function purchase()
		{
		if (DEMO_MODE_ENABLED == true)
			{
			$_SESSION['PageMessageCache'] = array(0 => 'Error', 1 => 'This feature is disabled in demo version.');
			$this->index();
			return ;
			}

		// Retrieve license and service information from Octeth servers - Start
		Core::LoadObject('install');
		$LicenseInformation = Install::GetLicenseInfoFromOcteth();
		// Retrieve license and service information from Octeth servers - End

		Install::GetLicenseInformation();
		$LicenseFileProperties = Install::$ArrayLicenseProperties;

		$EncryptedLicenseInfo = Install::EncryptNumberEnhanced($LicenseInformation['LicenseID']);


		if (isset($_POST['AddUserAccounts']) == true && $_POST['AddUserAccounts'] > 0)
			{
			header('Location: http://octeth.com/inapp_gateway/user?quantity='.$_POST['AddUserAccounts'].'&referrer='.$EncryptedLicenseInfo);
			exit;
			}
		else
			{
			$this->index();
			}
		}

	function download()
		{
		if (DEMO_MODE_ENABLED == true)
			{
			$_SESSION['PageMessageCache'] = array(0 => 'Error', 1 => 'This feature is disabled in demo version.');
			$this->index();
			return ;
			}

		Core::LoadObject('install');

		$Status = Install::GetLicenseFile();

		$_SESSION['PageMessageCache'] = $Status;

		$this->index();
		return;
		}

}
