<?php
/**
 * Overview controller
 *
 * @author Cem Hurturk
 */

class Controller_Overview extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admins');
	Core::LoadObject('admin_auth');
	Core::LoadObject('api');
	// Load other modules - End	

	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End

	// Retrieve administrator information - Start {
	$this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	// Retrieve administrator information - End }
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Events - Start {
	// Events - End }
	
	// Retrieve total users - Start {
	$SQLQuery = 'SELECT COUNT(*) FROM '.MYSQL_TABLE_PREFIX.'users';
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
	$total_users = mysql_fetch_array($ResultSet);
	$total_users = $total_users[0];
	// Retrieve total users - End }
	
	// Retriev top 10 active users - Start {
	Core::LoadObject('users');
	Core::LoadObject('lists');
	$array_active_users = array();
	$SQLQuery	= 'SELECT *, COUNT(*) AS TotalCampaigns FROM '.MYSQL_TABLE_PREFIX.'campaigns GROUP BY RelOwnerUserID ORDER BY TotalCampaigns DESC LIMIT 0,10';
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);
	if (mysql_num_rows($ResultSet) > 0)
		{
		while ($EachCampaign = mysql_fetch_assoc($ResultSet))
			{
			$array_active_users[] = array(
				'UserInformation'	=>	Users::RetrieveUser(array('UserID,FirstName,LastName'), array('UserID'=>$EachCampaign['RelOwnerUserID']), false),
				'TotalCampaigns'	=>	$EachCampaign['TotalCampaigns'],
				'TotalLists'		=>	Lists::GetTotal($EachCampaign['RelOwnerUserID'])
				);
			}
		}
	// Retriev top 10 active users - End }

	$LatestProductNews = Core::RetrieveLatestProductNews();

	// Retrieve online users - Start {
	$array_return = API::call(array(
		'format'	=>	'object',
		'command'	=>	'users.get',
		'protected'	=>	true,
		'username'	=>	$this->ArrayAdminInformation['Username'],
		'password'	=>	$this->ArrayAdminInformation['Password'],
		'parameters'=>	array(
			'orderfield'		=>	'FirstName|LastName',
			'ordertype'			=>	'ASC|ASC',
			'relusergroupid'	=>	'Online',
			'recordsperrequest'	=>	0,
			'recordsfrom'		=>	0,
			'searchfield'		=>	'',
			'searchkeyword'		=>	''
			)
		));

	$total_online_users = $array_return->TotalUsers;
	if ($total_online_users > 0)
		{
		$array_online_users = $array_return->Users;
		}
	else
		{
		$array_online_users = array();
		}
	// Retrieve online users - End }
	
	// Retrieve top bounces - Start {
	$SQLQuery	= 'SELECT *, COUNT(*) AS TotalBounces FROM '.MYSQL_TABLE_PREFIX.'stats_bounce GROUP BY RelOwnerUserID ORDER BY TotalBounces DESC LIMIT 0,10';
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);
	$array_bounce_users = array();
	if (mysql_num_rows($ResultSet) > 0)
		{
		while ($EachBounceStat = mysql_fetch_assoc($ResultSet))
			{
			$array_bounce_users[] = array(
				'UserInformation'	=>	Users::RetrieveUser(array('UserID,FirstName,LastName'), array('UserID'=>$EachBounceStat['RelOwnerUserID']), false),
				'TotalBounces'		=>	$EachBounceStat['TotalBounces'],
				);
			}
		}
	// Retrieve top bounces - End }
	
	// Retrieve top spam complaints - Start {
	$SQLQuery	= 'SELECT *, COUNT(*) AS TotalComplaints FROM '.MYSQL_TABLE_PREFIX.'fbl_reports GROUP BY UserID ORDER BY TotalComplaints DESC LIMIT 0,10';
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);
	$array_complaints_users = array();
	if (mysql_num_rows($ResultSet) > 0)
		{
		while ($EachComplaintStat = mysql_fetch_assoc($ResultSet))
			{
			$array_complaints_users[] = array(
				'UserInformation'	=>	Users::RetrieveUser(array('UserID,FirstName,LastName'), array('UserID'=>$EachComplaintStat['UserID']), false),
				'TotalComplaints'	=>	$EachComplaintStat['TotalComplaints'],
				);
			}
		}
	// Retrieve top spam complaints - End }

	// Retrieve campaigns pending approval - Start {
	Core::LoadObject('campaigns');
	$campaigns = Campaigns::RetrieveCampaigns_Enhanced(
		array(
			'Criteria' => array(
				array('Column' => '%c%.CampaignStatus', 'Operator' => '=', 'Value' => 'Pending Approval')
			),
			'Content' => true,
			'SplitTest' => false,
			'Reports' => array()
		)
	);
	if (count($campaigns) > 0) {
		foreach ($campaigns as &$each) {
			$each['UserInformation'] = Users::RetrieveUser(array('*'), array('UserID' => $each['RelOwnerUserID']), false);
		}
	}
	// Retrieve campaigns pending approval - End }

	// Interface parsing - Start
	$ArrayViewData 	= array(
							'PageTitle'				=> ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'].ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminOverview'],
							'CurrentMenuItem'		=> 'Overview',
							'TotalUsers'			=> (int) $total_users,
							'ActiveUsers'			=> $array_active_users,
							'OnlineUsers'			=> $array_online_users,
							'BounceUsers'			=> $array_bounce_users,
							'ComplaintUsers'		=> $array_complaints_users,
							'ApprovalPendingCampaigns' => $campaigns,
							'AdminInformation' => $this->ArrayAdminInformation,
							'LatestProductNews' => $LatestProductNews,
							);
	$ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
	if (isset($ArrayEventReturn) && count($ArrayEventReturn) > 0)
		{
		foreach ($ArrayEventReturn as $Key=>$Value)
			{
			$ArrayViewData[$Key] = $Value;
			}
		}
							
	$this->render('admin/overview', $ArrayViewData);
	// Interface parsing - End
	}

/**
 * undocumented function
 *
 * @return void
 * @author Mert Hurturk
 **/
function chartdata($type, $days = 7)
	{
	Core::LoadObject('chart_data_generator');
	Core::LoadObject('campaigns');

	// Load chart graph colors from the config file - Start {
	$ArrayChartGraphColors = explode(',', CHART_COLORS);
	// Load chart graph colors from the config file - End }

	if ($type == 'DeliveryForecast')
		{
		// Retrieve scheduled campaigns for next 7 days - Start {
		$array_scheduled_campaigns = Campaigns::RetrieveCampaigns(array('*'), array('ScheduleType'=>'Future',array('field'=>'SendDate','operator'=>'>=','value'=>date('Y-m-d')),array('field'=>'SendDate','operator'=>'<=','value'=>date('Y-m-d', strtotime(date('Y-m-d').' +7 days')))));
		// Retrieve scheduled campaigns for next 7 days - End }

		// Calculate estimated recipients for each scheduled campaign - Start {
		foreach ($array_scheduled_campaigns as &$each_campaign)
			{
			$each_campaign['EstimatedRecipients'] = Campaigns::EstimatedCampaignRecipients($each_campaign['CampaignID']);
			}
		// Calculate estimated recipients for each scheduled campaign - End }

		// Group each retrieved campaign in a date array - Start {
		$array_forecast = array();
		for ($i=0; $i<8; $i++)
			{
			$date = date('Y-m-d', strtotime(date('Y-m-d').' +'.$i.' days'));
			$array_forecast[$date] = array();
			
			foreach ($array_scheduled_campaigns as &$each_campaign)
				{
				if ($each_campaign['SendDate'] != $date)
					{
					continue;
					}
				$array_forecast[$date][] = array(
					'CampaignID'			=>	$each_campaign['CampaignID'],
					'CampaignName'			=>	$each_campaign['CampaignName'],
					'RelOwnerUserID'		=>	$each_campaign['RelOwnerUserID'],
					'EstimatedRecipients'	=>	$each_campaign['EstimatedRecipients']
					);
				}
			}
		// Group each retrieved campaign in a date array - End }
		
		// Calculate total estimated recipients and campaigns for each day in date array - Start {
		foreach ($array_forecast as &$each_day)
			{
			if (count($each_day) == 0)
				{
				$each_day['TotalCampaigns'] = 0;
				$each_day['TotalEstimatedRecipients'] = 0;
				}
			else if (count($each_day) == 1)
				{
				$each_day['TotalCampaigns'] = 1;
				$each_day['TotalEstimatedRecipients'] = $each_day[0]['EstimatedRecipients'];
				}
			else if (count($each_day) > 1)
				{
				$total = 0;
				for ($i=0;$i<count($each_day);$i++)
					{
					$total += $each_day[$i]['EstimatedRecipients'];
					}
				$each_day['TotalCampaigns'] = count($each_day);
				$each_day['TotalEstimatedRecipients'] = $total;
				}
			}
		// Calculate total estimated recipients and campaigns for each day in date array - End }

		$ArrayData = array();
		$ArrayData['Series']	 = array();
		$ArrayData['Graphs']	 = array(
										0 => array('title' => '', 'axis' => 'left', 'line_width' => 3, 'bullet_size' => 10, 'bullet_color' => $ArrayChartGraphColors[1], 'balloon_color' => $ArrayChartGraphColors[1], 'color' => $ArrayChartGraphColors[1], 'color_hover' => $ArrayChartGraphColors[1], 'fill_color' => $ArrayChartGraphColors[1], 'fill_alpha' => 20, 'line_alpha' => 100),
										);
		$ArrayData['GraphData']	= array();

		for ($TMPCounter = 0; $TMPCounter <= 7; $TMPCounter++)
			{
			$ArrayData['Series'][] = '<b>'.date('M d, y', strtotime(date('Y-m-d').' +'.$TMPCounter.' days')).'</b>';
			$ArrayData['GraphData'][0][] = array(
												'Value'			=> $array_forecast[date('Y-m-d', strtotime(date('Y-m-d').' +'.$TMPCounter.' days'))]['TotalEstimatedRecipients'],
												'Parameters'	=> array(
																		'description'	=> 'Recipients: '.number_format($array_forecast[date('Y-m-d', strtotime(date('Y-m-d').' +'.$TMPCounter.' days'))]['TotalEstimatedRecipients'])."\nCampaigns: ".number_format($array_forecast[date('Y-m-d', strtotime(date('Y-m-d').' +'.$TMPCounter.' days'))]['TotalCampaigns'])
																		),
												);
			}

		$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
		header('Content-type: text/xml');
		print($XML);
		exit;
		}
	else if ($type == 'DeliveryHistory')
		{
		// Retrieve campaign history - Start {
		$array_sent_campaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
			'RowOrder'		=> array(
				'Column'	=> 'SendProcessFinishedOn',
				'Type'		=> 'ASC'
			),
			'Criteria'		=> array(
				array('Column' => '%c%.CampaignStatus', 'Operator' => '=', 'Value' => 'Sent'),
				array('Column' => '%c%.SendProcessFinishedOn', 'Operator' => '>=', 'Value' => date('Y-m-d', strtotime(date('Y-m-d').' -'.$days.' days')), 'Link' => 'AND')
			),
			'Content'		=> false,
			'SplitTest'	=> false,
			'Reports'		=> array()
		));
		// Retrieve campaign history - End }

		// Calculate total of sent emails in a date array - Start {
		$array_history = array();
		for ($i=$days; $i>0; $i--)
			{
			$date = date('Y-m-d', strtotime(date('Y-m-d').' -'.$i.' days'));
			$array_history[$date] = 0;

			foreach ($array_sent_campaigns as &$each_campaign)
				{
				if (date('Y-m-d', strtotime($each_campaign['SendProcessFinishedOn'])) != $date)
					{
					continue;
					}
				$array_history[$date] += $each_campaign['TotalSent'];
				}
			}
		// Calculate total of sent emails in a date array - End }

		$ArrayData = array();
		$ArrayData['Series']	 = array();
		$ArrayData['Graphs']	 = array(
										0 => array('title' => '', 'axis' => 'left', 'line_width' => 3, 'bullet_size' => 0, 'bullet_color' => $ArrayChartGraphColors[1], 'balloon_color' => $ArrayChartGraphColors[1], 'color' => $ArrayChartGraphColors[1], 'color_hover' => $ArrayChartGraphColors[1], 'fill_color' => $ArrayChartGraphColors[1], 'fill_alpha' => 20, 'line_alpha' => 100),
										);
		$ArrayData['GraphData']	= array();

		for ($TMPCounter = $days; $TMPCounter > 0; $TMPCounter--)
			{
			$date = date('Y-m-d', strtotime(date('Y-m-d').' -'.$TMPCounter.' days'));
			$ArrayData['Series'][] = '<b>'.date('M d, y', strtotime($date)).'</b>';
			$ArrayData['GraphData'][0][] = array(
												'Value'			=> $array_history[$date],
												'Parameters'	=> array(
																		'description'	=> 'Total sent: '.number_format($array_history[$date])
																		),
												);
			}

		$XML = ChartDataGenerator::GenerateXMLData($ArrayData);
		header('Content-type: text/xml');
		print($XML);
		exit;
		}

	}

} // end of class User
?>