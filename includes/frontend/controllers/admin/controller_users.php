<?php

/**
 * Users controller
 *
 * @author Cem Hurturk
 */
class Controller_Users extends MY_Controller {

    /**
     * Constructor
     *
     * @author Cem Hurturk
     */
    function __construct() {
        parent::__construct();

        // Load other modules - Start
        Core::LoadObject('admins');
        Core::LoadObject('admin_auth');
        Core::LoadObject('api');
        // Load other modules - End
        // Check the login session, redirect based on the login session status - Start
        AdminAuth::IsLoggedIn(false, InterfaceAppURL(true) . '/admin/');
        // Check the login session, redirect based on the login session status - End
        // Retrieve administrator information - Start {
        $this->ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))' => $_SESSION[SESSION_NAME]['AdminLogin']));
        // Retrieve administrator information - End }
    }

    /**
     * Index controller
     *
     * @return void
     * @author Cem Hurturk
     */
    function index() {
        // Decide to go with user browse, create or blank state - Start {
        $this->browse();
        // Decide to go with user browse, create or blank state - End }
    }

    /**
     * Browse users
     *
     * @return void
     * @author Cem Hurturk
     */
    function browse($StartFrom = 1, $RPP = 25, $FilterType = 'ByActivity', $FilterData = 'All') {
        // Events - Start {
        if ($this->input->post('Command') == 'DeleteUser') {
            $ArrayEventReturn = $this->_EventDeleteUser($this->input->post('SelectedUsers'));
        } else if ($this->input->post('Command') == 'ChangeStatus') {
            $ArrayEventReturn = $this->_EventChangeStatusOfUsers($this->input->post('SelectedUsers'), $this->input->post('Status'));
        }
        // Events - End }
        // Apply filtering - Start {
        if (($FilterType == 'ByActivity') && ($FilterData == 'All')) {
            $UserGroupID = 0;
        } elseif (($FilterType == 'ByActivity') && ($FilterData == 'OnlineUsers')) {
            $UserGroupID = 'Online';
        } elseif (($FilterType == 'ByUserGroup') && ($FilterData == 'AnyUserGroup')) {
            $UserGroupID = 0;
        } elseif (($FilterType == 'ByUserGroup') && ($FilterData > 0)) {
            $UserGroupID = $FilterData;
        } elseif (($FilterType == 'ByAccountStatus') && ($FilterData == 'Enabled')) {
            $UserGroupID = 'Enabled';
        } elseif (($FilterType == 'ByAccountStatus') && ($FilterData == 'Disabled')) {
            $UserGroupID = 'Disabled';
        } elseif (($FilterType == 'ByReputationLevel') && ($FilterData == 'Trusted')) {
            $UserGroupID = 'Trusted';
        } elseif (($FilterType == 'ByReputationLevel') && ($FilterData == 'Untrusted')) {
            $UserGroupID = 'Untrusted';
        }
        // Apply filtering - End }
        // Retrieve user accounts - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'users.get',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'relusergroupid' => $UserGroupID,
                        'orderfield' => 'FirstName|LastName',
                        'ordertype' => 'ASC|ASC',
                        'recordsperrequest' => $RPP,
                        'recordsfrom' => ($StartFrom <= 0 ? 0 : ($StartFrom - 1) * $RPP)
                    )
        ));

        if ($ArrayReturn->Success == false) {
            // Error occurred
        } else {
            $TotalBrowsedUsers = $ArrayReturn->TotalUsers;

            if ($TotalBrowsedUsers == 0 && $FilterData == 'All') {
                // Redirect to blank state screen if the referrer is not the browse screen to avoid usability issues - Start
                if (preg_match('/^' . InterfaceAppURL(true) . '\/admin\/users\/browse\//i', $_SERVER['HTTP_REFERER']) == false) {
                    $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0291']);
                    $this->create();
                    return;
                }
                // Redirect to blank state screen if the referrer is not the browse screen to avoid usability issues - End
            }

            $Users = $ArrayReturn->Users;

            // Add send activity statistics to the user data - Start {
            Core::LoadObject('statistics');
            $Days = 7; // Number of days to look past
            $Today = date('Y-m-d'); // today
            $PastDate = date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $Days . ' days')); // past date

            $ArrayUserStatistics = array();
            foreach ($Users as $EachUser) {
                $ArrayStatistics = Statistics::RetrieveUserSendActivityStatistics($EachUser->UserID, $PastDate, $Today);
                foreach ($ArrayStatistics as $Date => $Array) {
                    $ArrayUserStatistics[$EachUser->UserID][] = $Array['TotalSent'];
                }
            }
            // Add send activity statistics to the user data - End }
        }
        // Retrieve user accounts - End }
        // Retrieve user groups - Start {
        Core::LoadObject('user_groups');
        $ArrayUserGroups = UserGroups::RetrieveUserGroups(array('*'), array());
        // Retrieve user groups - End }
        // Retrieve total number of users - Start {
        Core::LoadObject('users');
        $TotalUsers = Users::RetrieveUsers(array('*'), array(), array('UserID' => 'ASC'), 0, 0, false, true);
        // Retrieve total number of users - End }
        // Calculate total pages (pagination) - Start {
        $TotalPages = ceil($TotalBrowsedUsers / $RPP);
        // Calculate total pages (pagination) - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminBrowseUsers'],
            'CurrentMenuItem' => 'Users',
            'TotalBrowsedUsers' => $TotalBrowsedUsers,
            'TotalUsers' => $TotalUsers,
            'Users' => $Users,
            'UserStatistics' => $ArrayUserStatistics,
            'TotalPages' => $TotalPages,
            'CurrentPage' => $StartFrom,
            'RPP' => $RPP,
            'FilterType' => $FilterType,
            'FilterData' => $FilterData,
            'UserGroups' => $ArrayUserGroups,
            'AdminInformation' => $this->ArrayAdminInformation,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());

        if (isset($ArrayEventReturn)) {
            foreach ($ArrayEventReturn as $Key => $Value) {
                $ArrayViewData[$Key] = $Value;
            }
        }

        // Check if there is any message in the message buffer - Start {
        if (isset($_SESSION['PageMessageCache']) && $_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        if (isset($ArrayEventReturn) == true) {
            $ArrayViewData[($ArrayEventReturn[0] == false ? 'PageErrorMessage' : 'PageSuccessMessage')] = $ArrayEventReturn[1];
        }

        $this->render('admin/users', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Create new user account
     *
     * @return void
     * @author Cem Hurturk
     */
    function create() {
        // Load required modules - Start {
        Core::LoadObject('user_groups');
        // Load required modules - End }
        // Events - Start {
        if ($this->input->post('Command') == 'CreateUser') {
            $ArrayEventReturn = $this->_EventCreateUser();
        }
        // Events - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminCreateUser'],
            'CurrentMenuItem' => 'Users',
            'AdminInformation' => $this->ArrayAdminInformation,
        );
        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $this->render('admin/create_user', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Edit existing user account
     *
     * @return void
     * @author Cem Hurturk
     */
    function edit($UserID, $SubSection = 'AccountActivity', $Confirm = '', $SubSection2 = '', $ExtraValue1 = '', $ExtraValue2 = '', $ExtraValue3 = '') {
        // Load required modules - Start {
        Core::LoadObject('users');
        Core::LoadObject('user_groups');
        Core::LoadObject('payments');
        // Load required modules - End }
        // Retrieve user account - Start {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
        if ($ArrayUser == false) {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
        }
        Payments::SetUser($ArrayUser);
        // Retrieve user account - End }
        // Events - Start {
        if ($this->input->post('Command') == 'EditUser') {
            $ArrayEventReturn = $this->_EventEditUser($ArrayUser);
            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
            Payments::SetUser($ArrayUser);
        } elseif ($this->input->post('Command') == 'SendMessage') {
            $ArrayEventReturn = $this->_EventSendMessageToUser($ArrayUser);
        } elseif (($SubSection == 'DisableAccount') && ($Confirm == '1')) {
            $ArrayEventReturn = $this->_EventEnableDisableAccount($ArrayUser, 'Disabled');

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/edit/' . $ArrayUser['UserID'] . '/AccountActivity', 'location', '302');
        } elseif (($SubSection == 'EnableAccount') && ($Confirm == '1')) {
            $ArrayEventReturn = $this->_EventEnableDisableAccount($ArrayUser, 'Enabled');

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/edit/' . $ArrayUser['UserID'] . '/AccountActivity', 'location', '302');
        } elseif (($SubSection == 'DeleteAccount') && ($Confirm == '1')) {
            $ArrayEventReturn = $this->_EventDeleteUser(array($ArrayUser['UserID']), false, false);

            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0332']);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
        } elseif ($SubSection == 'LoginFull') {
            $ArrayEventReturn = $this->_EventUserSwitch($ArrayUser, 'Full');

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/', 'location', '302');
        } elseif ($SubSection == 'Logout') {
            $ArrayEventReturn = $this->_EventUserLogout($ArrayUser);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
        } elseif ($SubSection == 'Login') {
            $ArrayEventReturn = $this->_EventUserSwitch($ArrayUser, 'Default');

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/user/', 'location', '302');
        } elseif (($SubSection2 == 'ChartData') && ($ExtraValue1 == 'Campaigns') && ($ExtraValue2 == 'Unsubscriptions')) {
            Core::LoadObject('chart_data_generator');
            Core::LoadObject('statistics');

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $LastXDays = ($ExtraValue3 == '' ? 30 : $ExtraValue3);

            $ArrayEmailsSent = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), false, true);
            $ArrayUnsubscriptions = Statistics::RetrieveUnsubscriptionsAmountFromActivityLog($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), true);

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'right'),
                1 => array('title' => '', 'axis' => 'left', 'bullet_size' => 0, 'color' => 'FFFFFF', 'color_hover' => 'FFFFFF', 'fill_alpha' => 0, 'line_alpha' => 0),
            );
            $ArrayData['GraphData'] = array();

            for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                $ArrayData['Series'][] = '<b>' . date('M j', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days')) . '</b>';
                $ArrayData['GraphData'][0][] = array(
                    'Value' => (isset($ArrayUnsubscriptions[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions']) == true ? $ArrayUnsubscriptions[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($ArrayUnsubscriptions[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'])),
                    ),
                );
                $ArrayData['GraphData'][1][] = array(
                    'Value' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? $ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'] : 0),
                    'Parameters' => array(
                        'bullet' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? 'flag.swf?color=0X' . $ArrayChartGraphColors[0] : ''),
                        'description' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0144'], number_format($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'])) : ''),
                    ),
                );
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        } elseif (($SubSection2 == 'ChartData') && ($ExtraValue1 == 'Campaigns') && ($ExtraValue2 == 'Bounces')) {
            Core::LoadObject('chart_data_generator');
            Core::LoadObject('statistics');

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $LastXDays = ($ExtraValue3 == '' ? 30 : $ExtraValue3);

            $ArrayEmailsSent = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), false, true);
            $ArrayBounces = Statistics::RetrieveBounceDetectionLog($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), true);

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'right'),
                1 => array('title' => '', 'axis' => 'left', 'bullet_size' => 0, 'color' => 'FFFFFF', 'color_hover' => 'FFFFFF', 'fill_alpha' => 0, 'line_alpha' => 0),
            );
            $ArrayData['GraphData'] = array();

            for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                $ArrayData['Series'][] = '<b>' . date('M j', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days')) . '</b>';
                $ArrayData['GraphData'][0][] = array(
                    'Value' => (isset($ArrayBounces[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalHardBounce']) == true ? $ArrayBounces[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalHardBounce'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0148'], number_format($ArrayBounces[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalHardBounce'])),
                    ),
                );
                $ArrayData['GraphData'][1][] = array(
                    'Value' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? $ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'] : 0),
                    'Parameters' => array(
                        'bullet' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? 'flag.swf?color=0X' . $ArrayChartGraphColors[0] : ''),
                        'description' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0144'], number_format($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'])) : ''),
                    ),
                );
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        } elseif (($SubSection2 == 'ChartData') && ($ExtraValue1 == 'Campaigns') && ($ExtraValue2 == 'Spam')) {
            Core::LoadObject('chart_data_generator');
            Core::LoadObject('statistics');

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $LastXDays = ($ExtraValue3 == '' ? 30 : $ExtraValue3);

            $ArrayEmailsSent = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), false, true);
            $ArraySPAMComplaints = Statistics::RetrieveSPAMComplaintCounts($ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'), true);

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'right'),
                1 => array('title' => '', 'axis' => 'left', 'bullet_size' => 0, 'color' => 'FFFFFF', 'color_hover' => 'FFFFFF', 'fill_alpha' => 0, 'line_alpha' => 0),
            );
            $ArrayData['GraphData'] = array();

            for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                $ArrayData['Series'][] = '<b>' . date('M j', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days')) . '</b>';
                $ArrayData['GraphData'][0][] = array(
                    'Value' => (isset($ArraySPAMComplaints[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSPAMComplaints']) == true ? $ArraySPAMComplaints[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSPAMComplaints'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0151'], number_format($ArraySPAMComplaints[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSPAMComplaints'])),
                    ),
                );
                $ArrayData['GraphData'][1][] = array(
                    'Value' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? $ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'] : 0),
                    'Parameters' => array(
                        'bullet' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? 'flag.swf?color=0X' . $ArrayChartGraphColors[0] : ''),
                        'description' => (isset($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail']) == true ? sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0144'], number_format($ArrayEmailsSent[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSentEmail'])) : ''),
                    ),
                );
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        } elseif (($SubSection2 == 'ChartData') && ($ExtraValue1 == 'Subscriptions') && ($ExtraValue2 == 'Unsubscriptions')) {
            Core::LoadObject('chart_data_generator');
            Core::LoadObject('statistics');

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $LastXDays = ($ExtraValue3 == '' ? 30 : $ExtraValue3);

            $ArrayUserActivity = Statistics::RetrieveListActivityStatistics(0, $ArrayUser['UserID'], date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $LastXDays . ' days')), date('Y-m-d'));

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'left'),
                1 => array('title' => '', 'axis' => 'left'),
            );
            $ArrayData['GraphData'] = array();

            for ($TMPCounter = $LastXDays - 1; $TMPCounter >= 0; $TMPCounter--) {
                $ArrayData['Series'][] = '<b>' . date('M j', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days')) . '</b>';
                $ArrayData['GraphData'][0][] = array(
                    'Value' => (isset($ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions']) == true ? $ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0145'], number_format($ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalUnsubscriptions'])),
                    ),
                );
                $ArrayData['GraphData'][1][] = array(
                    'Value' => (isset($ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions']) == true ? $ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions'] : 0),
                    'Parameters' => array(
                        'description' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0149'], number_format($ArrayUserActivity[date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'))]['TotalSubscriptions'])),
                    ),
                );
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        } elseif (($SubSection2 == 'ChartData') && ($ExtraValue1 == 'Payments')) {
            Core::LoadObject('chart_data_generator');

            // Load chart graph colors from the config file - Start {
            $ArrayChartGraphColors = explode(',', CHART_COLORS);
            // Load chart graph colors from the config file - End }

            $ArrayUserPayments = Payments::GetPaymentPeriods();

            $ArrayData = array();
            $ArrayData['Series'] = array();
            $ArrayData['Graphs'] = array(
                0 => array('title' => '', 'axis' => 'left')
            );
            $ArrayData['GraphData'] = array();

            if (count($ArrayUserPayments) < 1) {
                for ($TMPCounter = 30; $TMPCounter >= 0; $TMPCounter--) {
                    $date = date('M j', strtotime(date('Y-m-d') . ' -' . $TMPCounter . ' days'));
                    $ArrayData['Series'][] = '<b>' . $date . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => 0,
                        'Parameters' => array(
                            'description' => Core::FormatCurrency(0),
                        ),
                    );
                }
            } else {
                for ($TMPCounter = count($ArrayUserPayments) - 1; $TMPCounter >= 0; $TMPCounter--) {
                    $date = date('M j', strtotime($ArrayUserPayments[$TMPCounter]['PeriodStartDate']));
                    $ArrayData['Series'][] = '<b>' . $date . '</b>';
                    $ArrayData['GraphData'][0][] = array(
                        'Value' => $ArrayUserPayments[$TMPCounter]['TotalAmount'],
                        'Parameters' => array(
                            'description' => Core::FormatCurrency($ArrayUserPayments[$TMPCounter]['TotalAmount']),
                        ),
                    );
                }
            }

            $XML = ChartDataGenerator::GenerateXMLData($ArrayData);
            header('Content-type: text/xml');
            print($XML);
            exit;
        }
        // Events - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminEditUser'],
            'CurrentMenuItem' => 'Users',
            'User' => $ArrayUser,
            'SubSection' => $SubSection,
        );

        // Add sub-section specific data to the view data - Start {
        if ($SubSection == 'AccountActivity') {
            // Retrieve recent campaign list - Start {
            Core::LoadObject('campaigns');
            $ArrayStatisticsOptions = array();
            $ArrayCampaigns = Campaigns::RetrieveCampaigns(array('*'), array('RelOwnerUserID' => $ArrayUser['UserID']), array('SendProcessFinishedOn' => 'DESC'), 10, 0, $ArrayStatisticsOptions, array(), false, false);
            $ArrayViewData['Campaigns'] = $ArrayCampaigns;
            // Retrieve recent campaign list - End }
            // Retrieve subscriber lists - Start {
            Core::LoadObject('lists');
            $ArrayStatisticsOptions = array();
            $ArraySubscriberLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $ArrayUser['UserID']), array('Name' => 'ASC'), $ArrayStatisticsOptions, 0, 0);
            $ArrayViewData['SubscriberLists'] = $ArraySubscriberLists;
            // Retrieve subscriber lists - End }
        } elseif ($SubSection == 'PaymentHistory') {
            // Retrieve payments - Start {
            $ArrayUserPayments = Payments::GetPaymentPeriods();
            $ArrayViewData['Periods'] = $ArrayUserPayments;
            // Retrieve payments - End }
            // Calculate totals - Start {
            $ArrayTotals = array();
            $ArrayTotals['Paid'] = 0;
            $ArrayTotals['Unpaid'] = 0;
            $ArrayTotals['Waiting'] = 0;
            $ArrayTotals['Waived'] = 0;
            $ArrayTotals['NA'] = 0;
            for ($TMPCounter = 0; $TMPCounter < count($ArrayUserPayments); $TMPCounter++) {
                $ArrayTotals[$ArrayUserPayments[$TMPCounter]['PaymentStatus']] += $ArrayUserPayments[$TMPCounter]['TotalAmount'];
            }
            $ArrayViewData['PaymentStatusTotals'] = $ArrayTotals;
            // Calculate totals - End }
        }
        // Add sub-section specific data to the view data - End }
        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        if (count($ArrayEventReturn) > 0) {
            foreach ($ArrayEventReturn as $Key => $Value) {
                $ArrayViewData[$Key] = $Value;
            }
        }

        $this->render('admin/edit_user', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Invoice controller
     *
     * @return void
     * @author Mert Hurturk
     * */
    function invoice($UserID, $LogID) {
        // Load required modules - Start {
        Core::LoadObject('users');
        Core::LoadObject('payments');
        // Load required modules - End }
        // Retrieve user account - Start {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
        if ($ArrayUser == false) {
            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
        }
        Payments::SetUser($ArrayUser);
        // Retrieve user account - End }
        // Events - Start {
        if ($this->input->post('Command')) {
            $ArrayEventReturn = $this->_EventUpdateInvoice();
        }
        // Events - End }
        // Retrieve log details - Start {
        $ArrayLog = Payments::GetLog($LogID);
        // Retrieve log details - End }
        // Interface parsing - Start
        $ArrayViewData = array(
            'PageTitle' => ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPrefix'] . ApplicationHeader::$ArrayLanguageStrings['PageTitle']['AdminPaymentPeriodInvoice'],
            'User' => $ArrayUser,
            'Period' => $ArrayLog
        );

        // Check if there is any message in the message buffer - Start {
        if ($_SESSION['PageMessageCache'][1] != '') {
            $ArrayViewData[($_SESSION['PageMessageCache'][0] == 'Success' ? 'PageSuccessMessage' : ($_SESSION['PageMessageCache'][0] == 'Notice' ? 'PageNoticeMessage' : 'PageErrorMessage'))] = $_SESSION['PageMessageCache'][1];
            unset($_SESSION['PageMessageCache']);
        }
        // Check if there is any message in the message buffer - End }

        $ArrayViewData = array_merge($ArrayViewData, InterfaceDefaultValues());
        foreach ($ArrayEventReturn as $Key => $Value) {
            $ArrayViewData[$Key] = $Value;
        }

        $this->render('admin/invoice', $ArrayViewData);
        // Interface parsing - End
    }

    /**
     * Draws user sending activity as a spark bar chart
     *
     * @param $data String 'x' delimited string
     *
     * @return void
     * @author Mert Hurturk
     * */
    function activity_spark($data) {
        $this->load->library('OemproSparkBar', array());

        $this->oemprosparkbar->draw(array('data' => $data));
    }

    /**
     * Draws user sending activity as a spark bar chart
     *
     * @param $data String 'x' delimited string
     * @param $width Integer
     * @param $height Integer
     *
     * @return void
     * @author Mert Hurturk
     * */
    function activity_sparkline($data, $width = 70, $height = 18) {
        $this->load->library('OemproSparkLine', array());

        $this->oemprosparkline->draw(array('data' => $data), $width, $height);
    }

    /**
     * Update invoice event
     *
     * @return void
     * @author Mert Hurturk
     * */
    function _EventUpdateInvoice() {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'user.paymentperiods.update',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'userid' => $this->input->post('RelUserID'),
                        'logid' => $this->input->post('LogID'),
                        'discount' => $this->input->post('Discount'),
                        'includetax' => $this->input->post('IncludeTax'),
                        'paymentstatus' => $this->input->post('PaymentStatus'),
                        'sendreceipt' => $this->input->post('SendReceipt'),
                    )
        ));
    }

    /**
     * Create user account event
     *
     * @return void
     * @author Cem Hurturk
     */
    function _EventCreateUser() {
        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'Username',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'Password',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0003'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'EmailAddress',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
                'rules' => 'required|valid_email',
            ),
            array
                (
                'field' => 'TimeZone',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0016'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'Language',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0018'],
                'rules' => '',
            ),
            array
                (
                'field' => 'RelUserGroupID',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0019'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'FirstName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0021'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'LastName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0022'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'CompanyName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0023'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Website',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0299'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Street',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0025'],
                'rules' => '',
            ),
            array
                (
                'field' => 'City',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0026'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Zip',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0028'],
                'rules' => '',
            ),
            array
                (
                'field' => 'State',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0027'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Country',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0029'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Phone',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0030'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Fax',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0031'],
                'rules' => '',
            ),
        );

        $this->form_validation->set_rules($ArrayFormRules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Create user account - Start
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'user.create',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'relusergroupid' => $this->input->post('RelUserGroupID'),
                        'emailaddress' => $this->input->post('EmailAddress'),
                        'username' => $this->input->post('Username'),
                        'password' => $this->input->post('Password'),
                        'firstname' => $this->input->post('FirstName'),
                        'lastname' => $this->input->post('LastName'),
                        'timezone' => $this->input->post('TimeZone'),
                        'language' => $this->input->post('Language'),
                        'reputationlevel' => 'Trusted',
                        'companyname' => $this->input->post('CompanyName'),
                        'website' => $this->input->post('Website'),
                        'street' => $this->input->post('Street'),
                        'city' => $this->input->post('City'),
                        'state' => $this->input->post('State'),
                        'zip' => $this->input->post('Zip'),
                        'country' => $this->input->post('Country'),
                        'phone' => $this->input->post('Phone'),
                        'fax' => $this->input->post('Fax'),
                        'previewmyemailaccount' => '',
                        'previewmyemailapikey' => '',
                    )
        ));


        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '2':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '3':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '4':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '5':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '6':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '7':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '8':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '9':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '10':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '11':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '12':
                    return array(
                        'PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0323'],
                    );
                    break;
                case '13':
                    return array(
                        'PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0324'],
                    );
                    break;
                case '14':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '15':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Create', $ErrorCode),
                    );
                    break;
                case '16':
                    return array(
                        'PageErrorMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0325'],
                    );
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0321']);

            $this->load->helper('url');
            redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
        }
        // Create user account - End
    }

    /**
     * Edit user account event
     *
     * @return void
     * @author Cem Hurturk
     */
    function _EventEditUser($ArrayUser) {
        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'ReputationLevel',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1798'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'Username',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0002'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'EmailAddress',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0010'],
                'rules' => 'required|valid_email',
            ),
            array
                (
                'field' => 'TimeZone',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0016'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'Language',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0018'],
                'rules' => '',
            ),
            array
                (
                'field' => 'RelUserGroupID',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0019'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'FirstName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0021'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'LastName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0022'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'CompanyName',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0023'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Website',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0299'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Street',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0025'],
                'rules' => '',
            ),
            array
                (
                'field' => 'City',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0026'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Zip',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0028'],
                'rules' => '',
            ),
            array
                (
                'field' => 'State',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0027'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Country',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0029'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Phone',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0030'],
                'rules' => '',
            ),
            array
                (
                'field' => 'Fax',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0031'],
                'rules' => '',
            ),
        );

        if ($ArrayUser['GroupInformation']['CreditSystem'] == 'Enabled') {
            $ArrayFormRules[] = array(
                'field' => 'AvailableCredits',
                'label' => ApplicationHeader::$ArrayLanguageStrings['Screen']['1488'],
                'rules' => 'required',
            );
        }

        $this->form_validation->set_rules($ArrayFormRules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Create user account - Start
        $ArrayAPIVars = array(
            'UserID' => $this->input->post('UserID'),
            'RelUserGroupID' => $this->input->post('RelUserGroupID'),
            'EmailAddress' => $this->input->post('EmailAddress'),
            'Username' => $this->input->post('Username'),
            'FirstName' => $this->input->post('FirstName'),
            'LastName' => $this->input->post('LastName'),
            'TimeZone' => $this->input->post('TimeZone'),
            'Language' => $this->input->post('Language'),
            'ReputationLevel' => $this->input->post('ReputationLevel'),
            'CompanyName' => $this->input->post('CompanyName'),
            'Website' => $this->input->post('Website'),
            'Street' => $this->input->post('Street'),
            'City' => $this->input->post('City'),
            'State' => $this->input->post('State'),
            'Zip' => $this->input->post('Zip'),
            'Country' => $this->input->post('Country'),
            'Phone' => $this->input->post('Phone'),
            'Fax' => $this->input->post('Fax'),
            'PreviewMyEmailAccount' => '',
            'PreviewMyEmailAPIKey' => '',
            'AccountStatus' => $this->input->post('AccountStatus'),
        );
        if ($ArrayUser['GroupInformation']['CreditSystem'] == 'Enabled') {
            $ArrayAPIVars['AvailableCredits'] = $this->input->post('AvailableCredits');
        }
        if ($this->input->post('NewPassword') != '') {
            $ArrayAPIVars['Password'] = $this->input->post('NewPassword');
        }

        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'user.update',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => $ArrayAPIVars
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode),
                    );
                    break;
                case '2':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode),
                    );
                    break;
                case '3':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode),
                    );
                    break;
                case '4':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode),
                    );
                    break;
                case '5':
                    return array(
                        'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode),
                    );
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            return array(
                'PageSuccessMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0328'], $ArrayUser['FirstName'] . ' ' . $ArrayUser['LastName']),
            );
        }
        // Create user account - End
    }

    /**
     * Send email message to the user
     *
     * @param string $ArrayUser
     * @return void
     * @author Cem Hurturk
     */
    function _EventSendMessageToUser($ArrayUser) {
        // Field validations - Start {
        $ArrayFormRules = array(
            array
                (
                'field' => 'Subject',
                'label' => $ArrayLanguageStrings['Screen']['0106'],
                'rules' => 'required',
            ),
            array
                (
                'field' => 'Message',
                'label' => $ArrayLanguageStrings['Screen']['0107'],
                'rules' => 'required',
            ),
        );

        $this->form_validation->set_rules($ArrayFormRules);
        // Field validations - End }
        // Run validation - Start {
        if ($this->form_validation->run() == false) {
            return array(false);
        }
        // Run validation - End }
        // Retrieve current loggin in admin information - Start {
        Core::LoadObject('admins');
        $ArrayAdmin = Admins::GetCurrentLoggedInAdmin();
        // Retrieve current loggin in admin information - End }
        // Send message - Start
        $isSent = O_Email_Sender_ForAdmin::send(
                        O_Email_Factory::messageToUser($ArrayUser, $this->input->post('Subject'), $this->input->post('Message'))
        );

        if ($isSent === FALSE) {
            return array(
                'PageErrorMessage' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0329'], $Return),
            );
        } else {
            return array(
                'PageSuccessMessage' => ApplicationHeader::$ArrayLanguageStrings['Screen']['0330'],
            );
        }
        // Send message - End
    }

    /**
     * Delete user account event
     *
     * @return void
     * @author Cem Hurturk
     */
    function _EventDeleteUser($ArrayUserIDs, $RedirectOnSuccess = true, $PerformFormValidation = true) {
        if (DEMO_MODE_ENABLED == true) {
            return array(false, 'This feature is disabled in demo version.');
        }

        if ($PerformFormValidation == true) {
            // Field validations - Start {
            $ArrayFormRules = array(
                array
                    (
                    'field' => 'SelectedUsers[]',
                    'label' => 'users',
                    'rules' => 'required',
                ),
            );

            $this->form_validation->set_rules($ArrayFormRules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0326']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }

        // Delete user accounts - Start {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'users.delete',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'users' => implode(',', $ArrayUserIDs)
                    )
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);
            switch ($ErrorCode) {
                case '1':
                    return array(false, ApplicationHeader::$ArrayLanguageStrings['Screen']['0326']);
                    break;
                default:
                    break;
            }
        } else {
            // API Success
            if ($RedirectOnSuccess == true) {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0327']);

                $this->load->helper('url');
                redirect(InterfaceAppURL(true) . '/admin/users/browse/', 'location', '302');
            } else {
                return true;
            }
        }
        // Delete user accounts - End }
    }

    /**
     * Delete user account event
     *
     * @return void
     * @author Cem Hurturk
     */
    function _EventChangeStatusOfUsers($ArrayUserIDs, $Status = 'Enabled') {
        if ($PerformFormValidation == true) {
            // Field validations - Start {
            $ArrayFormRules = array(
                array
                    (
                    'field' => 'SelectedUsers[]',
                    'label' => 'users',
                    'rules' => 'required',
                ),
            );

            $this->form_validation->set_rules($ArrayFormRules);
            $this->form_validation->set_message('required', ApplicationHeader::$ArrayLanguageStrings['Screen']['0326']);
            // Field validations - End }
            // Run validation - Start {
            if ($this->form_validation->run() == false) {
                return array(false, validation_errors());
            }
            // Run validation - End }
        }

        // Change user account statuses - Start {
        foreach ($ArrayUserIDs as $each) {
            $array_return = API::call(array(
                        'format' => 'array',
                        'command' => 'user.update',
                        'protected' => true,
                        'username' => $this->ArrayAdminInformation['Username'],
                        'password' => $this->ArrayAdminInformation['Password'],
                        'parameters' => array(
                            'userid' => $each,
                            'accountstatus' => $Status
                        )
            ));
        }

        $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['1397']);

        return true;
        // Change user account statuses - End }
    }

    /**
     * Disable the user account
     *
     * @param string $ArrayUser
     * @return void
     * @author Cem Hurturk
     */
    function _EventEnableDisableAccount($ArrayUser, $Type = 'Enabled') {
        $ArrayReturn = API::call(array(
                    'format' => 'object',
                    'command' => 'user.update',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'userid' => $ArrayUser['UserID'],
                        'accountstatus' => $Type
                    )
        ));

        if ($ArrayReturn->Success == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn->ErrorCode)) == 'array' ? $ArrayReturn->ErrorCode[0] : $ArrayReturn->ErrorCode);

            $_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Update', $ErrorCode));
        } else {
            // API Success
            if ($Type == 'Disabled') {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0331']);
            } else {
                $_SESSION['PageMessageCache'] = array('Success', ApplicationHeader::$ArrayLanguageStrings['Screen']['0333']);
            }
        }
        return;
    }

    /**
     * Logs into the user account
     *
     * @param string $ArrayUser
     * @return void
     * @author Cem Hurturk
     */
    function _EventUserSwitch($ArrayUser, $PrivilegeType = 'Default') {
        $ArrayReturn = API::call(array(
                    'format' => 'array',
                    'command' => 'user.switch',
                    'protected' => true,
                    'username' => $this->ArrayAdminInformation['Username'],
                    'password' => $this->ArrayAdminInformation['Password'],
                    'parameters' => array(
                        'userid' => $ArrayUser['UserID'],
                        'privilegetype' => $PrivilegeType
                    )
        ));

        if ($ArrayReturn['Success'] == false) {
            // API Error occurred
            $ErrorCode = (strtolower(gettype($ArrayReturn['ErrorCode'])) == 'array' ? $ArrayReturn['ErrorCode'][0] : $ArrayReturn['ErrorCode']);

            $_SESSION['PageMessageCache'] = array('Error', sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0322'], 'User.Switch', $ErrorCode));
        } else {
            // API Success
        }

        return;
    }

    /**
     * Logs out the user account
     *
     * @param string $ArrayUser
     * @return void
     * @author Cem Hurturk
     */
    function _EventUserLogout($ArrayUser) {
        setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), str_replace('logout/', '', $path));
        setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), rtrim(str_replace('logout/', '', $path), '/')); // This is for Google Chrome Windows
        setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14), '/');
        setcookie(COOKIE_USER_LOGINREMIND, '', time() - (86400 * 14));

        Core::LoadObject('user_auth');
        UserAuth::Logout();

        return;
    }

}

// end of class User
