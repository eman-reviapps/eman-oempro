<?php
/**
 * Login controller
 *
 * @author Cem Hurturk
 */

class Controller_Logout extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Check the login session, redirect based on the login session status - Start
	AdminAuth::IsLoggedIn(false, InterfaceAppURL(true).'/admin/');
	// Check the login session, redirect based on the login session status - End
	
	// Delete the user login reminder cookie - Start
	setcookie(COOKIE_ADMIN_LOGINREMIND, '', time() - 3600);
	setcookie(COOKIE_ADMIN_LOGINREMIND, '', time() - 3600, '/');
	// Delete the user login reminder cookie - End

	// Logout the user and redirect to login page - Start
	AdminAuth::Logout();

	$this->load->helper('url');
	redirect(InterfaceAppURL(true).'/admin/', 'location', '302');
	// Logout the user and redirect to login page - End	
	}
} // end of class User
?>