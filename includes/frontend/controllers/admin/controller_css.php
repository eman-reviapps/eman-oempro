<?php
/**
 * CSS controller
 *
 * @author Cem Hurturk
 */

class Controller_CSS extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	
	// Load other modules - Start
	Core::LoadObject('admin_auth');
	// Load other modules - End	
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	$CSSCacheDir = APP_PATH.'/data/css/';

	// Retrieve CSS file contents - Start {
	$FileContents = file_get_contents(TEMPLATE_PATH.'styles/ui.css');
	// Retrieve CSS file contents - End }

	// Load other modules - Start
	Core::LoadObject('themes');
	include_once(LIBRARY_PATH.'csscolor.inc.php');
	// Load other modules - End

	// Retrieve CSS settings - Start
	$ArrayCSSSettings = ThemeEngine::LoadCSSSettings(TEMPLATE);
	// Retrieve CSS settings - End

	// Retrieve theme information - Start
	$ArrayTheme = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => DEFAULT_THEMEID));
	$TMPArrayThemeSettings = explode("\n", $ArrayTheme['ThemeSettings']);
	// Retrieve theme information - End

	// Check if cache exists. If yes, load it - Start
	if (file_exists($CSSCacheDir.'css_cache_'.$ArrayTheme['ThemeID'].'.css') == true)
	{
		header("Pragma: public");
		header("Cache-Control: maxage=" . (86400 * 5));
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (86400 * 5)) . ' GMT');
		header('Content-type: text/css');
		print file_get_contents($CSSCacheDir.'css_cache_'.$ArrayTheme['ThemeID'].'.css');
		return;
	}
	// Check if cache exists. If yes, load it - End

	// Show product logo if requested - Start
	// Show product logo if requested - End

	// Apply theme settings to the template CSS - Start
	$ArrayThemeSettings = array();
	foreach ($TMPArrayThemeSettings as $EachSetting)
		{
		$TMPArray = explode('||||', $EachSetting);
		$ArrayThemeSettings[$TMPArray[0]] = trim($TMPArray[1]);
		}
	foreach ($ArrayThemeSettings as $Tag=>$Color)
		{
		foreach ($ArrayCSSSettings as &$EachCSSSetting)
			{
			if ($EachCSSSetting['Tag'] == $Tag)
				{
					$EachCSSSetting['Default'] = ($Color == '' ? $EachCSSSetting['Default'] : $Color);
				}
			}
		}

	// Calculate tints for theme settings - Start
	foreach ($ArrayThemeSettings as $ThemeKey1=>$Value1)
		{
		foreach ($ArrayThemeSettings as $ThemeKey2=>$Value2)
			{
			if (strpos($Value2, $ThemeKey1) !== false)
				{
				$difference = str_replace($ThemeKey1, '', $Value2);
				if ($difference == 'FGC')
					{
					$color = new CSS_Color($ArrayThemeSettings[$ThemeKey1]);
					$newcolor = $color->fg['0'];
					$ArrayThemeSettings[$ThemeKey2] = $newcolor;
					}
				else
					{
					$color = new CSS_Color($ArrayThemeSettings[$ThemeKey1]);
					$newcolor = $color->bg[$difference];
					$ArrayThemeSettings[$ThemeKey2] = $newcolor;
					}
					
				if ($difference == '')
					{
					$ArrayThemeSettings[$ThemeKey2] = $ArrayThemeSettings[$ThemeKey1];
					}
				}
			}
		}
	// Calculate tints for theme settings - End

	// Calculate tints for css settings - Start
	foreach ($ArrayCSSSettings as $Key=>$EachSettings1)
		{
		foreach ($ArrayCSSSettings as $EachSettings2)
			{
			if (strpos($EachSettings1['Default'], $EachSettings2['Tag']) !== false)
				{
				$difference = str_replace($EachSettings2['Tag'], '', $EachSettings1['Default']);
				if ($difference == 'FGC')
					{
					$color = new CSS_Color($EachSettings2['Default']);
					$newcolor = $color->fg['0'];
					$ArrayCSSSettings[$Key]['Default'] = $newcolor;
					}
				else
					{
					$color = new CSS_Color($EachSettings2['Default']);
					if ($difference != '')
						{
						$newcolor = $color->bg[$difference];
						}
					else
						{
						$newcolor = $color;
						}
					$ArrayCSSSettings[$Key]['Default'] = $newcolor;
					}

				if ($difference == '')
					{
					$ArrayCSSSettings[$Key]['Default'] = $EachSettings2['Default'];
					}
				}
			}
		}
	// Calculate tints for css settings - End

	foreach ($ArrayCSSSettings as $Key=>$ArrayEachSetting)
		{
		$Pattern = '/'.$ArrayEachSetting['Tag'].'/iU';
		$Replace = (isset($ArrayThemeSettings[$ArrayEachSetting['Tag']]) && $ArrayThemeSettings[$ArrayEachSetting['Tag']] != '' ? $ArrayThemeSettings[$ArrayEachSetting['Tag']] : $ArrayEachSetting['Default']);
		$FileContents = preg_replace($Pattern, $Replace, $FileContents);
		}
	// Apply theme settings to the template CSS - End

	// Perform other replacements - Start
	$Pattern = array('/_Template:URL_/iU', '/_CSSPHP:URL_/iU');
	$Replace = array(InterfaceTemplateURL(true), InterfaceAppURL(true).'/admin/css');
	$FileContents = preg_replace($Pattern, $Replace, $FileContents);
	// Perform other replacements - End

	// Cache the CSS - Start
	file_put_contents($CSSCacheDir.'css_cache_'.$ArrayTheme['ThemeID'].'.css', $FileContents);
	// Cache the CSS - End

	// Print out the parsed CSS file contents - Start {
	header("Pragma: public");
	header("Cache-Control: maxage=" . (86400 * 5));
	header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (86400 * 5)) . ' GMT');
	header('Content-type: text/css');
	echo $FileContents;
	// Print out the parsed CSS file contents - End }
	}


} // end of class User
?>