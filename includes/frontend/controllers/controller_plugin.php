<?php
/**
 * Plugin controller
 *
 * @author Cem Hurturk
 */
class Controller_Plugin extends MY_Controller
{
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	parent::__construct();
	}

/**
 * Index controller
 *
 * @return void
 * @author Cem Hurturk
 */
function index()
	{
	// Fetch parameters - Start {
	$Arguments = func_get_args();
	// Fetch parameters - End }

	// Decide the class and function names - Start {
	$Class = strtolower(array_shift($Arguments));
	$Function = 'ui_'.strtolower(array_shift($Arguments));
	// Decide the class and function names - End }

	// Load the plug-in object - Start {
		$PlugInPath			= PLUGIN_PATH.strtolower($Class).'/'.strtolower($Class).'.php';
		$SystemPlugInPath	= APP_PATH.'/includes/system_plugins/plugin_'.strtolower($Class).'.php';
	if (file_exists($PlugInPath) == true)
		{
		include_once($PlugInPath);
		}
	elseif (file_exists($SystemPlugInPath) == true)
		{
		include_once($SystemPlugInPath);
		$Class = 'plugin_'.$Class;
		}
	else
		{
		show_404('');
		// $Message = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0440'], $Function, $Class);
		// $this->display_public_message($Error, $Message);
		// return;
		}
	// Load the plug-in object - End }

	// Execute the plug-in - Start {
	if(class_exists($Class))
		{
		if(method_exists($Class, $Function))
			{
			call_user_func_array(array($Class, $Function), $Arguments);
			}
		elseif(method_exists($Class, 'ui_index'))
			{
			call_user_func_array(array($Class, 'ui_index'), $Arguments);
			}
		else
			{
			// Display error message if no such method found.
			show_404('');
			// $Message = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0440'], ucfirst($Function), ucfirst($Class));
			// $this->display_public_message($Error, $Message);
			// return;
			}
		}
	else
		{
		// Display error message if no such class found / plugin disabled.
		show_404('');
		// $Message = sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['0441'], ucfirst($Class));
		// $this->display_public_message($Error, $Message);
		// return;
		}	
	// Execute the plug-in - End }
	}
}
?>