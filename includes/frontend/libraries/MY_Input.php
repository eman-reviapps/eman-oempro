<?php

class MY_Input extends CI_Input
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}

	function get_all_post_items($XSSClean = false)
	{
		if (!empty($_POST))
		{
			$Post = array();

			// Loop through the full _POST array and return it
			foreach (array_keys($_POST) as $Key)
			{
				$Post[$Key] = $this->_fetch_from_array($_POST, $Key, $XSSClean);
			}
			return $Post;
		}
		return array();
	}

	function get_all_get_items($XSSClean = false)
	{
		if (!empty($_GET))
		{
			$Get = array();

			// Loop through the full _GET array and return it
			foreach (array_keys($_GET) as $Key)
			{
				$Get[$Key] = $this->_fetch_from_array($_GET, $Key, $XSSClean);
			}
			return $Get;
		}
		return array();
	}
}