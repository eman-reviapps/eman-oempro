<?php

class MY_Form_validation extends CI_Form_validation
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}

	function set_value_to_field($Field, $Value)
	{
		$this->_field_data[$Field]['postdata'] = $Value;
	}

	function set_error_to_field($Field, $Error = '')
	{
		if (isset($this->_field_data[$Field]) == true && $Error != '')
		{
			$this->_field_data[$Field]['error'] = sprintf($Error, $this->_field_data[$Field]['label']);
		}
		elseif (isset($this->_field_data[$Field]) == true && $Error == false)
		{
			$this->_field_data[$Field]['error'] = '';
		}
		else
		{
			return false;
		}

	}

	function get_errors_as_array()
	{
		$errors = array();

		if (isset($this->_field_data) == true && is_array($this->_field_data) == true && count($this->_field_data) > 0)
		{
			foreach ($this->_field_data as $Field=>$Parameters)
			{
				if (isset($Parameters['error']) == true && $Parameters['error'] != '')
				{
					$errors[] = array('Field' => $Field, 'Error' => $Parameters['error']);
				}
			}
		}

		return $errors;
	}

}