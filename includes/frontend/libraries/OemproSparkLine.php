<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * OemproSparkLine
 *
 * This library draws spark line chart
 *
 * @package default
 * @author Mert Hurturk
 **/
class OemproSparkLine {

	/**
	 * Draws spark line chart to the screen
	 *
	 * @param $parameters Array 'data' key should include a string ('x' delimited). Ex: array('data' => '10x20x30x40x50')
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
    function draw($parameters, $width=70, $height=18)
    	{
		// Prepare data - Start
		$array_data = explode('x', $parameters['data']);
		if (count($array_data) < 1 || (count($array_data) == 1 && $array_data[0] == 0))
			{
			$array_data = array('0','0','0','0','0','0','0');
			}
		$data_max = max($array_data);
		// Prepare data - Start

		// Config - Start
		$nodes = count($array_data);
		$node_ratio = $height / $data_max;
		$node_height = ($data_max * $node_ratio == 0 ? $height : $data_max * $node_ratio);
		$node_spacing = $width / $nodes;
		$image_width = ($nodes * $node_spacing) - $node_spacing;
		$image_height = $node_height;
		// Config - End

		// Create image and fill background - Start
		$image = imagecreatetruecolor($image_width, $image_height);
		$black = imagecolorallocate($image, 0,0,0);
		imagecolortransparent($image, $black);
		// Create image and fill background - End

		// Prepare graph ratio - Start
		$ratio = $node_height / $data_max;
		// Prepare graph ratio - End

		$array_plygon_points = array();

		// Draw node fill - Start
		$color_node_fill = imagecolorallocate($image, 230, 242, 250);
		for ($i=0;$i<$nodes-1;$i++)
			{
			$x1 = $i*$node_spacing;
			$x2 = $x1+$node_spacing;
			$y1 = $node_height - ($array_data[$i] * $ratio);
			$y2 = $node_height - ($array_data[$i+1] * $ratio);
			$array_plygon_points[] = $x1;
			$array_plygon_points[] = $y1;
			$array_plygon_points[] = $x2;
			$array_plygon_points[] = $y2;
			}
		$array_plygon_points[] = $array_plygon_points[count($array_plygon_points)-2];
		$array_plygon_points[] = $node_height;
		$array_plygon_points[] = 0;
		$array_plygon_points[] = $node_height;
		imagefilledpolygon($image, $array_plygon_points, count($array_plygon_points)/2, $color_node_fill);
		// Draw node fill - End

		// Draw line - Start
		$color_node_line = imagecolorallocate($image, 0, 119, 204);
		for ($i=0;$i<$nodes-1;$i++)
			{
			$x1 = $i*$node_spacing;
			$x2 = $x1+$node_spacing;
			$y1 = $node_height - ($array_data[$i] * $ratio) - 1;
			$y2 = $node_height - ($array_data[$i+1] * $ratio) - 1;
			$array_plygon_points[] = $x1;
			$array_plygon_points[] = $y1;
			$array_plygon_points[] = $x2;
			$array_plygon_points[] = $y2;
			imageline($image, $x1, $y1, $x2, $y2, $color_node_line);
			}
		// Draw line - End
		ob_clean();
		header("Content-type: image/png");
		imagepng($image);
		imagedestroy($image);
    	}
} // END class

?>
