<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class EmailContentValidator
{
	public $ObjectCI = '';

	function __construct()
	{
		$this->ObjectCI =& get_instance();
	}

	function DetectTagInContent($Content, $Tag, $ExplicitLink = false)
	{
		if ($ExplicitLink == true)
		{
			// If explicit is set to true, it will check for the exact link
			if (preg_match('/href\="' . $Tag . '"/', $Content) == false && preg_match('/href\=\'' . $Tag . '\'/', $Content) == false)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			if (preg_match('/' . $Tag . '/', $Content) == false)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	function RemoveUnwantedCodes($String)
	{
		// Remove scripts
		$String = preg_replace("/<script\b[^>]*>(.*?)<\/script>/uism", "", $String);
		// Remove external stylesheets
		$String = preg_replace("/<link\b[^>]*\/?>/uism", "", $String);

		$ArraySearch = array('<tbody>', '</tbody>');
		$ArrayReplace = array('', '');
		return str_ireplace($ArraySearch, $ArrayReplace, $String);
	}

	function DetectUnwantedCodes($String)
	{
		// Check for scripts - Start
		if (preg_match('/<[\/\/]{0,1}(script)[^><]*>/i', $String) > 0)
		{
			return false;
		}
		// Check for scripts - End

		// Check for objects - Start
		if (preg_match('/<[\/\/]{0,1}(object)[^><]*>/i', $String) > 0)
		{
			return false;
		}
		// Check for objects - End

		// Check for java - Start
		if (preg_match('/<[\/\/]{0,1}(applet)[^><]*>/i', $String) > 0)
		{
			return false;
		}
		// Check for java - End

		return true;
	}

	/**
	 *
	 * Checks Email HTML content to invalid img src
	 * Returns true when all img tags have valid image urls
	 *
	 * @param string $Content
	 *
	 * @return bool
	 */
	function CheckImageURLFormat($Content)
	{
		// This function is bypassed because it gives a weird
		// error on image url validation.
		return true;

		$RegEx = "/\<*img[^\>]*src *= *[\"\']{0,1}([^\"\'\ >]*)/i";

		$Matches = array();

		preg_match_all($RegEx, $Content, $Matches);

		if (isset($Matches[1]) && is_array($Matches[1]))
		{

			foreach ($Matches[1] as $EachImageSrc)
			{

				if (filter_var($EachImageSrc, FILTER_VALIDATE_URL) === false)
				{

					return false;
				}
			}
		}
		return true;
	}

}
