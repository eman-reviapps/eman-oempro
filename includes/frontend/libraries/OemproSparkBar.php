<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * OemproSparkBar
 *
 * This library draws spark bar chart
 *
 * @package default
 * @author Mert Hurturk
 **/
class OemproSparkBar {

	/**
	 * Draws spark bar chart to the screen
	 *
	 * @param $data Array 'data' key should include a string ('x' delimited). Ex: array('data' => '10x20x30x40x50')
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
    function draw($data)
    	{
		// Prepare data - Start
		$array_data = explode('x', $data['data']);
		$data_max = max($array_data);
		// Prepare data - Start

		// Config - Start
		$text_character_width = 6;
		$text_spacing = 0;
		$character_width = 6;
		$bars = count($array_data);
		$bar_width = 1;
		$bar_height = 10;
		$bar_spacing = 2;
		$image_width = (($bar_width+$bar_spacing)*$bars) + (strlen($array_data[count($array_data)-1]) * $text_character_width) + $text_spacing;
		$image_height = $bar_height+1;
		// Config - End

		// Create image and fill background - Start
		$image = imagecreatetruecolor($image_width, $image_height);
		$black = imagecolorallocate($image, 0,0,0);
		imagecolortransparent($image, $black);
		// Create image and fill background - End

		// Draw empty bars - Start
		$color_bar_empty = imagecolorallocate($image, 229, 229, 229);
		for ($i=0;$i<$bars;$i++)
			{
			$x1 = ($i*$bar_width)+($i*$bar_spacing);
			$x2 = $x1+$bar_width;
			imagefilledrectangle($image, $x1, 0, $x2, $bar_height, $color_bar_empty);
			}
		// Draw empty bars - End

		// Draw actial data bars - Start
			// Prepare graph ratio - Start
			if ($data_max == 0)
				{
				$ratio = 0;
				}
			else
				{
				$ratio = $bar_height / $data_max;
				}
			// Prepare graph ratio - End

			$color_bar = imagecolorallocate($image, 137, 137, 137);
			$color_bar_last = imagecolorallocate($image, 0, 119, 204);
			for ($i=0;$i<$bars;$i++)
				{
				$x1 = $i*($bar_width+$bar_spacing);
				$x2 = $x1+$bar_width;
				$y1 = $bar_height;
				$y2 = $bar_height - ($array_data[$i] * $ratio);
				$color = ($i == count($array_data) - 1 ? $color_bar_last : $color_bar);
				imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);
				}
		// Draw actial data bars - End

		// Write last bar's value - Start
		imagestring($image, 1, ($bars*($bar_width+$bar_spacing)) + $text_spacing, $bar_height-6, $array_data[count($array_data)-1], $color_bar_last);
		// Write last bar's value - End
		ob_clean();
		header("Content-type: image/png");
		imagepng($image);
		imagedestroy($image);
    	}
} // END class

?>
