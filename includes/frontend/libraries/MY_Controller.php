<?php
class MY_Controller extends Controller {
 	function __construct()
		{
 		parent::Controller();
		}

	function display_user_message($message_title, $message)
		{
		$arrayViewData = array(
			'message_title' => $message_title,
			'message' => $message,
			'UserInformation' => isset($this->array_user_information) ? $this->array_user_information : (isset($this->ArrayUserInformation) ? $this->ArrayUserInformation : array())
			);
		$this->render('user/message', $arrayViewData);
		}

	function display_public_message($message_title, $message)
		{
		$arrayViewData = array(
			'message_title' => $message_title,
			'message' => $message,
			);
		$this->render('public/message', $arrayViewData);
		}

	function render($view, $data=array())
		{
		// $this->output->enable_profiler(TRUE);

		$this->load->library('user_agent');

		// Decide which sub template (mobile, desktop) to use - Start {
		if ($this->agent->is_mobile())
			{
			// $this->load->view('../../../templates/'.TEMPLATE.'/mobile/'.$view, $data);
			$this->load->view('../../../templates/'.TEMPLATE.'/desktop/'.$view, $data);
			}
		elseif ($this->agent->is_browser())
			{
			$this->load->view('../../../templates/'.TEMPLATE.'/desktop/'.$view, $data);
			}
		else
			{
			$this->load->view('../../../templates/'.TEMPLATE.'/desktop/'.$view, $data);
			}
		// Decide which sub template (mobile, desktop) to use - End }

		}

	function plugin_render($PluginName, $PluginTemplate, $Data = array(), $SkipDefaultTemplateDirScheme = false, $Return = false)
	{
		if ($SkipDefaultTemplateDirScheme == false)
		{
			$HTML = $this->load->view('../../../plugins/' . $PluginName . '/templates/' . TEMPLATE . '/desktop/' . $PluginTemplate, $Data, $Return);
		}
		else
		{
			$HTML = $this->load->view('../../../plugins/' . $PluginName . '/templates/' . $PluginTemplate, $Data, $Return);
		}

		if ($Return == true)
		{
			return $HTML;
		}
	}
}
?>