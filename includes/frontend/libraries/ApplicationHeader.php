<?php
/**
 * Main header library
 *
 * @author Cem Hurturk
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Header library
 *
 * @package default
 * @author Cem Hurturk
 */
class ApplicationHeader
{
/**
 * Language strings
 *
 * @var array
 */
public static $ArrayLanguageStrings				= array();

/**
 * Constructor
 *
 * @author Cem Hurturk
 */
function __construct()
	{
	// CodeIgniter - Start
	$ObjectCodeIgniter =& get_instance();
	// CodeIgniter - End

	// Enable/disable profiler - Start
	$ObjectCodeIgniter->output->enable_profiler(OEMPRO_PROFILER);
	// Enable/disable profiler - End

	// Load CodeIgniter helpers - Start {
	$ObjectCodeIgniter->load->helper(array('form'));
	$ObjectCodeIgniter->load->helper(array('interface'));
	// Load CodeIgniter helpers - End }

	// Load CodeIgniter libraries - Start {
	$ObjectCodeIgniter->load->library(array('form_validation'));
	// Load CodeIgniter libraries - End }
		
	// Load language - Start
	self::LoadLanguageStrings();
	// Load language - End

	// Initialize system plugins - Start
	Core::LoadObject('segments');
	$tmpRulePluginFields = Plugins::HookListener('Filter', 'SubscriberRuleFields', array(Segments::$RulePluginFields));
	Segments::$RulePluginFields = $tmpRulePluginFields[0];
	unset($tmpRulePluginFields);
	// Initialize system plugins - End

	// Set default CodeIgniter form validation messages - Start {
	InterfaceOverrideFormMessages();
	// Set default CodeIgniter form validation messages - End }
	}

/**
 * Loads language strings into array
 *
 * @return void
 * @author Cem Hurturk
 */
function LoadLanguageStrings()
	{
	self::$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
	}


} // end of class Header
?>