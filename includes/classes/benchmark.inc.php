<?php
/**
 * Benchmark class
 *
 * @package default
 * @author Mert Hurturk
 **/
class Benchmark
{
	private $_marks = array();
	private $_status = array();
	
	public function __construct($status = true) {
		$this->_status = $status;
	}
	
	public function mark($key) {
		if (! $this->_status) return;
		
		if (! isset($this->_marks[$key])) $this->_marks[$key] = array();
		
		$this->_marks[$key][] = array(
			'time'	=> microtime(true),
			'memory'=> memory_get_usage()
		);
	}

	public function printResults() {
		if (! $this->_status) return;

		print $this->_calculateResults();
	}
	
	public function mailResults($to, $subject='Benchmark results') {
		if (! $this->_status) return;

		mail($to, $subject, $this->_calculateResults());
	}
	
	public function saveBenchmarkObject($filename) {
		if (! $this->_status) return;

		$file = fopen(APP_PATH.'/data/tmp/'.$filename, 'wb');
		fwrite($file, serialize($this));
		fclose($file);
		
	}
	
	public static function loadBenchmarkObject($filename) {
		if (! is_file(APP_PATH.'/data/tmp/'.$filename)) {
			return false;
		}
		$data = file_get_contents(APP_PATH.'/data/tmp/'.$filename);
		return unserialize($data);
	}
	
	private function _calculateResults() {
		if (! $this->_status) return;

		$resultOutput = '';
		
		foreach ($this->_marks as $key=>$data) {
			$resultOutput .= "\n".str_repeat('=',60)."\n";
			$resultOutput .= $key."\n";
			$resultOutput .= str_repeat('-',60)."\n";
			
			// Calculate average time spent and memory usage - Start {
			$average = 0;
			$sumOfTimeDifferences = 0;
			$sumOfMemDifferences = 0;
			for ($i=count($data)-1;$i>0;$i=$i-2) {
				$sumOfTimeDifferences += ($data[$i]['time'] - $data[$i-1]['time']);
				$sumOfMemDifferences += ($data[$i]['memory'] - $data[$i-1]['memory']);
			}
			$average = (count($data) > 2) ? $sumOfTimeDifferences / (count($data) / 2) : $sumOfTimeDifferences;
			$averageMem = (count($data) > 2) ? $sumOfMemDifferences / (count($data) / 2) : $sumOfMemDifferences;
			// Calculate average time spent and memory usage - End }

			$resultOutput .= "Total time                : ".number_format($sumOfTimeDifferences, 5)."s\n";
			$resultOutput .= "Total executions          : ".(count($data)/2)." times\n";
			$resultOutput .= "Time for each execution   : ".number_format($average, 5)."s\n";
			$resultOutput .= "Memory for each execution : ".($averageMem/1000)."kb";
		}
		return $resultOutput;
	}

} // END class Benchmark