<?php
// Required modules - Start
Core::LoadObject('statistics');
// Required modules - End

/**
 * Auto Responders class
 *
 * This class holds all auto responder related functions
 * @package Oempro
 * @author Octeth
 **/
class AutoResponders extends Core
{
/**
 * Required fields for auto responders
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('AutoResponderName', 'RelOwnerUserID', 'RelListID', 'RelEmailID');	

/**
 * Default values for fields of an auto responder
 *
 * @static array
 **/
public static $ArrayDefaultValuesForFields = array('AutoResponderTriggerType'=>'OnSubscription', 'TriggerTimeType'=>'Immediately');	

/**
 * Returns a single auto responder matching given criterias
 *
 * <code>
 * <?php
 * $ArrayAutoResponderInformation = AutoResponders::RetrieveResponder(array('*'), array('AutoResponderID'=>12));
 * ?>
 * </code>
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving auto responders.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveResponder($ArrayReturnFields, $ArrayCriterias, $RetrieveStatistics=false)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'auto_responders');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayAutoResponder = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayAutoResponder) < 1) { return false; } // if there are no auto responders, return false 

	$ArrayAutoResponder = $ArrayAutoResponder[0];

	if ($RetrieveStatistics)
		{
		$FromDate = date('Y-m-d',strtotime(date('Y-m-d').' -30 days'));
		$ToDate = date('Y-m-d');
		$ArrayAutoResponder['OpenStatistics']		   				= Statistics::RetrieveAutoResponderOpenStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID'], $FromDate, $ToDate);
		$ArrayAutoResponder['ClickStatistics']		   				= Statistics::RetrieveAutoResponderClickStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID'], $FromDate, $ToDate);
		$ArrayAutoResponder['UnsubscriptionStatistics'] 			= Statistics::RetrieveAutoResponderUnsubscriptionStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID'], $FromDate, $ToDate);
		$ArrayAutoResponder['ForwardStatistics']	   				= Statistics::RetrieveAutoResponderForwardStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID'], $FromDate, $ToDate);
		$ArrayAutoResponder['BrowserViewStatistics']  				= Statistics::RetrieveAutoResponderBrowserViewStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID'], $FromDate, $ToDate);
		// $ArrayAutoResponder['BounceStatistics']		   				= Statistics::RetrieveAutoResponderBounceStatistics($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['SubscriberCount']);
		$ArrayAutoResponder['ClickPerformance']		   				= Statistics::RetrieveClickPerformanceDaysOfAutoResponder($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID']);
		// $ArrayAutoResponder['OverallClickPerformance']				= Statistics::RetrieveOverallClickPerformanceOfAutoResponder($ArrayAutoResponder['AutoResponderID']);
		$ArrayAutoResponder['OverallAccountClickPerformance']		= Statistics::RetrieveOverallClickPerformanceOfAccount($ArrayAutoResponder['RelOwnerUserID']);
		$ArrayAutoResponder['OpenPerformance']		   				= Statistics::RetrieveOpenPerformanceDaysOfAutoResponder($ArrayAutoResponder['AutoResponderID'], $ArrayAutoResponder['RelOwnerUserID']);
		// $ArrayAutoResponder['OverallOpenPerformance'] 				= Statistics::RetrieveOverallOpenPerformanceOfAutoResponder($ArraySubscriberList['ListID']);
		$ArrayAutoResponder['OverallAccountOpenPerformance']		= Statistics::RetrieveOverallOpenPerformanceOfAccount($ArrayAutoResponder['RelOwnerUserID'], $ArrayAutoResponder['RelListID']);
		}
	
	return $ArrayAutoResponder;
	}

/**
 * Reteurns all auto responders matching given criterias
 *
 * <code>
 * <?php
 * $ArrayAutoResponders = AutoResponders::RetrieveResponders(array('*'), array('RelOwnerUserID'=>2), array('AutoResponderName'=>'ASC'));
 * ?>
 * </code>
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving auto responders.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveResponders($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('AutoResponderName'=>'ASC'), $SortBasedOnTriggerTime = true)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields			= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables		= array(MYSQL_TABLE_PREFIX.'auto_responders');
	$ArrayCriterias			= $ArrayCriterias;
	$ArrayAutoResponders	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayAutoResponders) < 1) { return false; } // if there are no auto responders, return false

	// Sort auto responders based on their trigger values - Start
	if ($SortBasedOnTriggerTime == true)
		{
		$ArrayNewAutoResponderList = array();
	
		foreach ($ArrayAutoResponders as $Key=>$ArrayEachAutoResponder)
			{
			if ($ArrayEachAutoResponder['TriggerTimeType'] == 'Immediately')
				{
				$ArrayNewAutoResponderList[0][]		= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Seconds later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime']][]				= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Minutes later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime'] * 60][]			= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Hours later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime'] * 3600][]			= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Days later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime'] * 86400][]		= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Weeks later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime'] * 604800][]		= $ArrayEachAutoResponder;
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Months later')
				{
				$ArrayNewAutoResponderList[$ArrayEachAutoResponder['TriggerTime'] * 2592000][]		= $ArrayEachAutoResponder;
				}
			}

		ksort($ArrayNewAutoResponderList);
		
		$ArrayAutoResponders = array();
		foreach ($ArrayNewAutoResponderList as $Period=>$ArrayEach)
			{
			foreach ($ArrayEach as $Index=>$ArrayEachAutoResponder)
				{
				$ArrayAutoResponders[] = $ArrayEachAutoResponder;
				}
			}
		}
	// Sort auto responders based on their trigger values - End

	return $ArrayAutoResponders;
	}
	
/**
 * Returns total number of auto responders of a subscriber list
 *
 * <code>
 * <?php
 * $TotalAutoResponders = AutoResponders::GetTotal(3);
 * ?>
 * </code>
 *
 * @param integer $OwnerListID Owner subscriber list's id
 * @return integer Total number of auto responders of given subscriber list
 * @author Mert Hurturk
 **/
public static function GetTotal($OwnerListID)
	{
	// Check for required fields of this function - Start
	if (!isset($OwnerListID) || $OwnerListID == '') { return false; }
	// Check for required fields of this function - End

	$ArrayFields			= array('COUNT(*) AS TotalResponderCount');
	$ArrayFromTables		= array(MYSQL_TABLE_PREFIX.'auto_responders');
	$ArrayCriterias			= array('RelListID'=>$OwnerListID);
	$ArrayAutoResponders	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	return $ArrayAutoResponders[0]['TotalResponderCount'];
	}

/**
 * Creates a new auto responder with given values
 *
 * <code>
 * <?php
 * $NewAutoResponderID = AutoResponders::Create(array(
 *	'AutoResponderName' => 'Welcome',
 *	'RelOwnerUserID' => '12',
 *	'RelListID' => '2',
 *	'RelEmailID' => '3',
 *	'AutoResponderTriggerType' => 'OnSubscription',
 *	'AutoResponderTriggerTimeType' => 'Immediately'
 *	));
 * ?>
 * </code>
 *
 * @param array $ArrayFieldAndValues Values of new auto responder
 * @return boolean|integer ID of new auto responder
 * @author Mert Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] === '') { print $ArrayFieldAndValues[$EachField]; return false; }
		}
	// Check required values - End

	// Check if any value is missing. if yes, give default values - Start
	foreach (self::$ArrayDefaultValuesForFields as $EachField=>$EachValue)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] === '') { $ArrayFieldAndValues[$EachField] = $EachValue; }
		}
	// Check if any value is missing. if yes, give default values - End

	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'auto_responders');

	$NewAutoResponderID = Database::$Interface->GetLastInsertID();
	return $NewAutoResponderID;
	}

/**
 * Updates auto responder information
 *
 * <code>
 * <?php
 * AutoResponders::Update(array(
 *	'AutoResponderName' => 'Welcome',
 *	'RelOwnerUserID' => '12',
 *	'RelListID' => '2',
 *	'RelEmailID' => '3',
 *	'AutoResponderTriggerType' => 'OnSubscription',
 *	'AutoResponderTriggerTimeType' => 'Immediately'
 *	), array(
 * 	'AutoResponderID' => 2
 *	));
 * ?>
 * </code>
 *
 * @param array $ArrayFieldAndValues Values of new auto responder information
 * @param array $ArrayCriterias Criterias to be matched while updating auto responder
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriterias check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End
		
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'auto_responders');
	return true;
	}

/**
 * Deletes auto responders
 *
 * <code>
 * <?php
 * AutoResponders::Delete(12, array(1,3,5,4));
 * ?>
 * </code>
 *
 * @param integer $OwnerUserID Owner user id of auto responders to be deleted
 * @param array $ArrayAutoResponderIDs Auto responder ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($OwnerUserID, $ArrayAutoResponderIDs, $ListID = 0, $EmailID = 0)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End
	
	Core::LoadObject('transaction_emails');
	Core::LoadObject('emails');

	if (count($ArrayAutoResponderIDs) > 0)
		{
		foreach ($ArrayAutoResponderIDs as $EachID)
			{
			$AutoResponder = self::RetrieveResponder(array('*'), array('AutoResponderID' => $EachID), false);
			if (! $AutoResponder) {
				continue;
			}
			Emails::Delete(array('EmailID' => $AutoResponder['RelEmailID']));
			TransactionEmails::Delete(0, 0, 0, $EachID);
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'AutoResponderID'=>$EachID), MYSQL_TABLE_PREFIX.'auto_responders');
			}
		}
	elseif ($ListID > 0)
		{
		$ArrayAutoResponders = self::RetrieveResponders(array('*'), array('RelOwnerUserID'=>$OwnerUserID, 'RelListID'=>$ListID), array('AutoResponderID'=>'ASC'));
		
		if (count($ArrayAutoResponders) > 0)
			{
			foreach ($ArrayAutoResponders as $Index=>$ArrayEachAutoResponder)
				{
				Emails::Delete(array('EmailID' => $ArrayEachAutoResponder['RelEmailID']));
				TransactionEmails::Delete(0, 0, 0, $ArrayEachAutoResponder['AutoResponderID']);
				}
			}
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelListID'=>$ListID), MYSQL_TABLE_PREFIX.'auto_responders');
		}
	elseif ($EmailID > 0)
		{
		$ArrayAutoResponders = self::RetrieveResponders(array('*'), array('RelOwnerUserID'=>$OwnerUserID, 'RelEmailID'=>$EmailID), array('AutoResponderID'=>'ASC'));
		
		if (count($ArrayAutoResponders) > 0)
			{
			foreach ($ArrayAutoResponders as $Index=>$ArrayEachAutoResponder)
				{
				TransactionEmails::Delete(0, 0, 0, $ArrayEachAutoResponder['AutoResponderID']);
				}
			}
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelEmailID'=>$EmailID), MYSQL_TABLE_PREFIX.'auto_responders');
		}
	elseif ($OwnerUserID > 0)
		{
		$ArrayAutoResponders = self::RetrieveResponders(array('*'), array('RelOwnerUserID'=>$OwnerUserID), array('AutoResponderID'=>'ASC'));
		
		if (count($ArrayAutoResponders) > 0)
			{
			foreach ($ArrayAutoResponders as $Index=>$ArrayEachAutoResponder)
				{
				TransactionEmails::Delete(0, 0, 0, $ArrayEachAutoResponder['AutoResponderID']);
				}
			}
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'auto_responders');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.AutoResponder', array($OwnerUserID, $ArrayAutoResponderIDs, $ListID, $EmailID));
	// Plug-in hook - End
	}

/**
 * Register auto responders to a subscriber for delivery
 *
 * @param string $ListID 
 * @param string $SubscriberID 
 * @param string $UserID 
 * @param string $AutoResponderType
 * @return integer
 * @author Cem Hurturk
 */
public static function RegisterAutoResponders($ListID, $SubscriberID, $UserID, $AutoResponderType, $CriteriaLinkURL = '', $CriteriaCampaignID = 0)
	{
	// Retrieve auto responders of the list with the requested auto responder type - Start
	$ArrayCriterias = array(
							'RelOwnerUserID'			=> $UserID,
							'RelListID'					=> $ListID,
							'AutoResponderTriggerType'	=> $AutoResponderType,
							);
							
	if ($AutoResponderType == 'OnSubscriberLinkClick')
		{
		$ArrayCriterias['AutoResponderTriggerValue'] = $CriteriaLinkURL;
		}
	else if ($AutoResponderType == 'OnSubscriberCampaignOpen')
		{
		$ArrayCriterias['AutoResponderTriggerValue'] = $CriteriaCampaignID;
		}

	$ArrayAutoResponders = self::RetrieveResponders(array('*'), $ArrayCriterias, array('AutoResponderName' => 'ASC'));
	// Retrieve auto responders of the list with the requested auto responder type - End

	// Load required modules - Start
	Core::LoadObject('queue');
	// Load required modules - End

	// Register the auto responder in the queue - Start
	if ($ArrayAutoResponders != FALSE && count($ArrayAutoResponders) > 0)
		{
		foreach ($ArrayAutoResponders as $Key=>$ArrayEachAutoResponder)
			{
			if ($ArrayEachAutoResponder['TriggerTimeType'] == 'Immediately')
				{
				$TimeToSend = date('Y-m-d H:i:s');
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Seconds later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' seconds '.date('H:i:s')));
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Minutes later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' minutes '.date('H:i:s')));
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Hours later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' hours '.date('H:i:s')));
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Days later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' days '.date('H:i:s')));
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Weeks later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' weeks '.date('H:i:s')));
				}
			elseif ($ArrayEachAutoResponder['TriggerTimeType'] == 'Months later')
				{
				$TimeToSend = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +'.$ArrayEachAutoResponder['TriggerTime'].' months '.date('H:i:s')));
				}
			EmailQueue::RegisterIntoQueue($UserID, $ListID, $SubscriberID, $TimeToSend, $ArrayEachAutoResponder['AutoResponderID'], $ArrayEachAutoResponder['RelEmailID'], 'Auto Responder');
			}
		}
	// Register the auto responder in the queue - End

	return count($ArrayAutoResponders);
	}

} // END class Lists
?>