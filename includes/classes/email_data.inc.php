<?php

class Email_data {

    private $id = 0;
    private $email_name = '';
    private $from_name = '';
    private $from_email = '';
    private $replyto_name = '';
    private $replyto_email = '';
    private $same_from_replyto = false;
    private $html_content = '';
    private $image_embedding_enabled = false;
    private $content_type = '';
    private $subject = '';
    private $plain_content = '';
    private $template_id = 0;
    private $fetch_url = '';
    private $fetch_plain_url = '';
    private $mode = '';
    private $screenshot = '';
    private $html_pure= '';

    function __construct() {
        
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function set_mode($mode) {
        $this->mode = $mode;
    }

    public function set_email_name($name) {
        $this->email_name = $name;
    }

    public function set_from_name($name) {
        $this->from_name = $name;
    }

    public function set_from_email($email) {
        $this->from_email = $email;
    }

    public function set_replyto_name($name) {
        $this->replyto_name = $name;
    }

    public function set_replyto_email($email) {
        $this->replyto_email = $email;
    }

    public function set_same_from_replyto($boolean) {
        $this->same_from_replyto = $boolean;
    }

    public function set_html_content($content) {
        $this->html_content = $content;
    }

    public function set_image_embedding_enabled($boolean) {
        $this->image_embedding_enabled = $boolean;
    }

    public function set_content_type($type) {
        $this->content_type = $type;
    }

    public function set_subject($subject) {
        $this->subject = $subject;
    }

    public function set_plain_content($content) {
        $this->plain_content = $content;
    }

    public function set_template_id($id) {
        $this->template_id = $id;
    }

    public function set_fetch_url($url) {
        $this->fetch_url = $url;
    }

    public function set_fetch_plain_url($url) {
        $this->fetch_plain_url = $url;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_email_name() {
        return $this->email_name;
    }

    public function get_from_name() {
        return $this->from_name;
    }

    public function get_from_email() {
        return $this->from_email;
    }

    public function get_replyto_name() {
        return $this->same_from_replyto ? $this->get_from_name() : $this->replyto_name;
    }

    public function get_replyto_email() {
        return $this->same_from_replyto ? $this->get_from_email() : $this->replyto_email;
    }

    public function get_same_from_replyto() {
        return $this->same_from_replyto;
    }

    public function get_html_content() {
        return $this->html_content;
    }

    public function get_image_embedding_enabled() {
        return $this->image_embedding_enabled;
    }

    public function get_content_type() {
        return $this->content_type;
    }

    public function get_subject() {
        return $this->subject;
    }

    public function get_plain_content() {
        return $this->plain_content;
    }

    public function get_template_id() {
        return $this->template_id;
    }

    public function get_fetch_url() {
        return $this->fetch_url;
    }

    public function get_fetch_plain_url() {
        return $this->fetch_plain_url;
    }

    public function get_mode() {
        return $this->mode;
    }

    function getScreenshot() {
        return $this->screenshot;
    }

    function setScreenshot($screenshot) {
        $this->screenshot = $screenshot;
    }
    function getHtml_pure() {
        return $this->html_pure;
    }

    function setHtml_pure($html_pure) {
        $this->html_pure = $html_pure;
    }





}

?>