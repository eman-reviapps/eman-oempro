<?php
/**
 * Split tests class
 *
 * This class holds all split test related functions
 * @package Oempro
 * @author Octeth
 **/
class SplitTests extends Core
{
	/**
	 * Creates new split test
	 *
	 * @return integer
	 * @author Mert Hurturk
	 **/
	public static function CreateNewTest($ArrayFieldsAndValues) {
		// Create a new record in split_tests table - Start
		Database::$Interface->InsertRow($ArrayFieldsAndValues, MYSQL_TABLE_PREFIX.'split_tests');
		$NewTestID = Database::$Interface->GetLastInsertID();
		// Create a new record in split_tests table - End

		return $NewTestID;
	}
	
	/**
	 * Retrieves split test information. Returns false if no test exists.
	 *
	 * @return mixed
	 * @author Mert Hurturk
	 **/
	public static function RetrieveTestOfACampaign($CampaignID, $RelOwnerUserID) {
		$ArrayResult = Database::$Interface->GetRows_Enhanced(array(
			'Fields'	=> array('*'),
			'Tables'	=> array(MYSQL_TABLE_PREFIX.'split_tests'),
			'Criteria'	=> array(
				array('Column' => 'RelCampaignID', 'Operator' => '=', 'ValueWOQuote' => $CampaignID),
				array('Column' => 'RelOwnerUserID', 'Operator' => '=', 'ValueWOQuote' => $RelOwnerUserID, 'Link' => 'AND')
				)
		));

		if (count($ArrayResult) < 1) return false;
		
		return $ArrayResult;
	}

	/**
	 * Adds an email for campaign split test
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	public static function AddTestEmailForCampaign($TestID, $CampaignID, $EmailID, $RelOwnerUserID) {
		// If a record exists already, return - Start {
		$ArrayResult = Database::$Interface->GetRows_Enhanced(array(
			'Fields'	=> array('COUNT(*) AS TotalEmails'),
			'Tables'	=> array(MYSQL_TABLE_PREFIX.'split_test_emails'),
			'Criteria'	=> array(
				array('Column' => 'RelTestID', 'Operator' => '=', 'ValueWOQuote' => $TestID),
				array('Column' => 'RelCampaignID', 'Operator' => '=', 'ValueWOQuote' => $CampaignID, 'Link' => 'AND'),
				array('Column' => 'RelEmailID', 'Operator' => '=', 'ValueWOQuote' => $EmailID, 'Link' => 'AND'),
				array('Column' => 'RelOwnerUserID', 'Operator' => '=', 'ValueWOQuote' => $RelOwnerUserID, 'Link' => 'AND')
				)
		));
		if ($ArrayResult[0]['TotalEmails'] > 0) return;
		// If a record exists already, return - End }

		// Create a new record in split_test_emails table - Start
		$ArrayFieldsAndValues = array(
			'RelTestID'			=> $TestID,
			'RelCampaignID'		=> $CampaignID,
			'RelEmailID'		=> $EmailID,
			'RelOwnerUserID'	=> $RelOwnerUserID
			);
		Database::$Interface->InsertRow($ArrayFieldsAndValues, MYSQL_TABLE_PREFIX.'split_test_emails');
		$NewTestID = Database::$Interface->GetLastInsertID();
		// Create a new record in split_test_emails table - End
	}

	/**
	 * Updates split test information
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	public static function Update($SplitTestID, $ArrayFieldsAndValues) {
		Database::$Interface->UpdateRows($ArrayFieldsAndValues, array('TestID'=>$SplitTestID), MYSQL_TABLE_PREFIX.'split_tests');
		return;
	}

	/**
	 * removes split test email
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	public static function RemoveTestEmail($CampaignID, $TestID, $EmailID, $UserID) {
		Database::$Interface->DeleteRows(array('RelEmailID' => $EmailID, 'RelCampaignID' => $CampaignID, 'RelTestID' => $TestID, 'RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX.'split_test_emails');
		Emails::Delete(array('EmailID'=>$EmailID,'RelUserID'=>$UserID));
	}

	/**
	 * Deletes split test
	 *
	 * @return void
	 * @author Mert Hurturk
	 **/
	public static function RemoveTestOfCampaign($CampaignID, $UserID) {
		Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID, 'RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX.'split_tests');
		Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID, 'RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX.'split_test_emails');
	}
}
?>