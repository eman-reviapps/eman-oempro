<?php

/**
 * Email queue class
 *
 * This class holds all queue related functions
 * @package Oempro
 * @author Octeth
 * */
class EmailQueue extends Core {

    /**
     * Benchmark start time stamp
     *
     * @var int
     * */
    public static $BenchmarkStartTime = 0;

    /**
     * Benchmark end time stamp
     *
     * @var int
     * */
    public static $BenchmarkEndTime = 0;

    /**
     * Benchmark memory usage
     *
     * @var array
     * */
    public static $ArrayBenchmarkMemoryUsage = array();

    /**
     * Registers the email into the queue
     *
     * @param string $UserID
     * @param string $ListID
     * @param string $SubscriberID
     * @param string $TimeToSend
     * @param string $EmailID
     * @param string $QueueType
     * @return void
     * @author Cem Hurturk
     */
    public static function RegisterIntoQueue($UserID, $ListID, $SubscriberID, $TimeToSend, $AutoResponderID, $EmailID, $QueueType) {
        if (($QueueType == 'Transactional') || ($QueueType == 'Auto Responder')) {
            $ArrayFieldnValues = array(
                'TransactionalQueueID' => '',
                'RelOwnerUserID' => $UserID,
                'RelListID' => $ListID,
                'RelSubscriberID' => $SubscriberID,
                'TimeToSend' => $TimeToSend,
                'RelAutoResponderID' => $AutoResponderID,
                'RelEmailID' => $EmailID,
                'Status' => 'Pending',
                'StatusMessage' => '',
                'QueueType' => $QueueType,
                'ProcessTime' => '0000-00-00 00:00:00',
            );
            Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'transactional_email_queue');
        }
        return;
    }

    /**
     * Starts benchmarking time
     *
     * @return void
     * @author Cem Hurturk
     * */
    function StartBenchmarking() {
        self::$BenchmarkStartTime = time();
        self::$ArrayBenchmarkMemoryUsage[] = memory_get_usage();
    }

    /**
     * Stop benchmarking time
     *
     * @return void
     * @author Cem Hurturk
     * */
    function StopBenchmarking() {
        self::$BenchmarkEndTime = time();
        self::$ArrayBenchmarkMemoryUsage[] = memory_get_usage();
    }

    /**
     * Returns number of emails per second
     *
     * @return double
     * @author Cem Hurturk
     * */
    function CalculateBenchmark($TotalEmailsSent) {
        $Duration = self::$BenchmarkEndTime - self::$BenchmarkStartTime;
        $Duration = $Duration <= 0 ? 1 : $Duration;

        $TotalEmailsPerSecond = $TotalEmailsSent / $Duration;

        return $TotalEmailsPerSecond;
    }

    /**
     * Generates queue records for the given campaign ID
     *
     * @return int Returns queue record total
     * @author Cem Hurturk
     * */
    function GenerateQueue($CampaignID, $UserID) {
        $TotalAddedRecipients = 0;
        $TotalRecipients = 0;

        // Check if the queue table for target campaign has been created before. If no, create it - Start {
        $SQLQuery = "SHOW TABLES LIKE 'oempro_queue_c_" . mysql_real_escape_string($CampaignID) . "'";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        if (mysql_num_rows($ResultSet) == 0) {
            // Doesn't exist. Create it.
            $SQLQuery = "CREATE TABLE IF NOT EXISTS `oempro_queue_c_" . mysql_real_escape_string($CampaignID) . "` (
					  `QueueID` int(11) NOT NULL AUTO_INCREMENT,
					  `RelListID` int(11) NOT NULL,
					  `RelSegmentID` int(11) NOT NULL,
					  `RelSubscriberID` int(11) NOT NULL,
					  `EmailAddress` varchar(250) COLLATE UTF8_UNICODE_CI NOT NULL,
					  `Status` enum('Pending','Sending','Sent','Failed') COLLATE UTF8_UNICODE_CI NOT NULL,
					  `StatusMessage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
					  PRIMARY KEY (`QueueID`),
					  UNIQUE KEY `EmailAddress` (`EmailAddress`),
					  KEY `Status` (`Status`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        }
        // Check if the queue table for target campaign has been created before. If no, create it - End }
        // Retrieve recipient subscriber lists and segments - Start
        $ArrayRecipientLists = self::GetRecipientLists($CampaignID, true);
        // Retrieve recipient subscriber lists and segments - End
        // Loop subscribers of recipient lists - Start
        foreach ($ArrayRecipientLists as $Key => $ArrayRecipientList) {
            $ListID = $ArrayRecipientList[0];
            $SegmentID = $ArrayRecipientList[1];
            $ArraySegmentJoins = Segments::GetSegmentJoinQuery($SegmentID);

            $SQLQuery = "INSERT IGNORE INTO " . MYSQL_TABLE_PREFIX . "queue_c_" . mysql_real_escape_string($CampaignID) . " (`QueueID`, `RelListID`, `RelSegmentID`, `RelSubscriberID`, `EmailAddress`, `Status`, `StatusMessage`) ";
            $SQLQuery .= "SELECT '', " . $ListID . ", " . $SegmentID . ", tblSubscribers.SubscriberID, tblSubscribers.EmailAddress, 'Pending', '' ";
            $SQLQuery .= "FROM " . MYSQL_TABLE_PREFIX . "subscribers_" . $ListID . " AS tblSubscribers ";
            if (count($ArraySegmentJoins) > 0) {
                $SQLQuery .= Database::$Interface->GetJoinString(Segments::GetSegmentJoinQuery($SegmentID, false, 'tblSubscribers')) . " ";
            }
            $SQLQuery .= "WHERE tblSubscribers.BounceType!='Hard' AND tblSubscribers.SubscriptionStatus='Subscribed' ";

            if ($SegmentID > 0) {
                // $ArraySegment = Segments::GetSegmentSQLQuery($SegmentID);
                // $SQLQuery .= ' AND ('.Database::$Interface->GetRows(array('*'), array(), $ArraySegment['SegmentRules'], array(), 0, 0, $ArraySegment['SegmentOperator'], false, true).')';

                $ArraySegment = Segments::GetSegmentSQLQuery_Enhanced($SegmentID, false, '', 'and', 0, 'tblSubscribers');
                $SQLQuery .= ' AND (';
                $SQLQuery .= Database::$Interface->GetRows_Enhanced(array(
                    'Tables' => array('dummy'), // We pass a dummy table name because we need only WHERE sql part
                    'Fields' => array('*'),
                    'ReturnSQLWHEREQuery' => true,
                    'RowOrder' => array('Column' => 'SegmentName', 'Type' => 'ASC'),
                    'Criteria' => $ArraySegment,
                    'Joins' => $ArraySegmentJoins
                ));
                $SQLQuery .= ')';
            }

            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery, true);

            // Clean the queue against suppressed emails - Start
            self::RemoveSuppressedSubscribers($CampaignID, $ListID, $UserID);
            // Clean the queue against suppressed emails - End
        }
        // Loop subscribers of recipient lists - End
        // Optimize the outbox table to speed-up the process - Start
        self::OptimizeQueueTable($CampaignID);
        // Optimize the outbox table to speed-up the process - End
        // Calculate the total recipients in the queue - Start
        $TotalRecipients = self::CalculateTotalQueueItems($CampaignID, true);
        // Calculate the total recipients in the queue - End

        return $TotalRecipients;
    }

    /**
     * Returns campaign recipient subscribers list IDs in an array
     *
     * @param int $CampaignID Campaign ID
     * @param bool $RetrieveSegmentIDs if set to true, assosciative array will be returned with both list and segment IDs
     * @return array
     * @author Cem Hurturk
     * */
    function GetRecipientLists($CampaignID, $RetrieveSegmentIDs = false) {
        if ($RetrieveSegmentIDs == false) {
            $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "campaign_recipients WHERE RelCampaignID='" . mysql_real_escape_string($CampaignID) . "' GROUP BY RelListID";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            $ArrayLists = array();

            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayLists[] = $EachRow['RelListID'];
            }
        } else {
            $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "campaign_recipients WHERE RelCampaignID='" . $CampaignID . "'";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            $ArrayLists = array();

            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayLists[] = array($EachRow['RelListID'], $EachRow['RelSegmentID']);
            }
        }

        return $ArrayLists;
    }

    /**
     * Detects if campaign has been paused or not
     *
     * @return bool Returns true if campaign is paused
     * @author Cem Hurturk
     * */
    function IsCampaignPaused($TotalSubscribersLooped, $CampaignID) {
        // Check campaign status. If it's not set to 'Sending', cancel the process - Start
        if ($TotalSubscribersLooped % 25 == 0) {
            $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));

            if ($ArrayCampaign['CampaignStatus'] != 'Sending') {
                return true;
            }
        }
        // Check campaign status. If it's not set to 'Sending', cancel the process - End

        return false;
    }

    /**
     * Removes suppressed subscribers in the recipient lists from campaign queue
     *
     * @param string $CampaignID
     * @return void
     * @author Cem Hurturk
     */
    function RemoveSuppressedSubscribers($CampaignID, $ListID, $UserID) {
        $campaignQueueTableName = "oempro_queue_c_" . mysql_real_escape_string($CampaignID);

        // Get all smart filters and remove smart suppressions - Start
        $suppressionPatternMapper = O_Registry::instance()->getMapper('SuppressionPattern');
        $patterns = $suppressionPatternMapper->findAll();
        if (is_array($patterns) && count($patterns) > 0) {
            $SmartSuppressionSQLQuery = "DELETE FROM $campaignQueueTableName WHERE ";
            $patternQueries = array();
            foreach ($patterns as $eachPattern) {
                $patternQueries[] = "(EmailAddress " . $eachPattern->getType() . " '" . $eachPattern->getPattern() . "')";
            }
            $SmartSuppressionSQLQuery .= " " . implode(" OR ", $patternQueries);
            Database::$Interface->ExecuteQuery($SmartSuppressionSQLQuery);
        }
        // Get all smart filters and remove smart suppressions - End
        // Remove local and global suppressions - Start
        $SuppressionQuery = "DELETE FROM $campaignQueueTableName WHERE EmailAddress IN "
                . "(SELECT EmailAddress FROM oempro_suppression_list AS tblSuppr WHERE tblSuppr.RelListID IN (0, $ListID) AND tblSuppr.RelOwnerUserID IN (0, $UserID))";
        Database::$Interface->ExecuteQuery($SuppressionQuery);
        // Remove local and global suppressions - End
    }

    /**
     * Removes provided subscriber ID from outbox table
     *
     * @author Cem Hurturk
     * */
    function RemoveFromQueue($CampaignID, $QueueID = 0) {
        if (($QueueID > 0) || (count($QueueID) > 0)) {
            if (is_array($QueueID) == true) {
                $SQLQuery = "DELETE FROM " . MYSQL_TABLE_PREFIX . "queue_c_" . mysql_real_escape_string($CampaignID) . " WHERE QueueID IN (" . implode(',', $QueueID) . ")";
                Database::$Interface->ExecuteQuery($SQLQuery);
            } else {
                $SQLQuery = "DELETE FROM " . MYSQL_TABLE_PREFIX . "queue_c_" . mysql_real_escape_string($CampaignID) . " WHERE QueueID='" . $QueueID . "'";
                Database::$Interface->ExecuteQuery($SQLQuery);
            }
        }

        return;
    }

    /**
     * Inserts recipients into the queue table
     *
     * @return int
     * @author Cem Hurturk
     * */
    function InsertIntoQueue($CampaignID, $ArrayFieldnValues) {
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'queue_c_' . mysql_real_escape_string($CampaignID));
        $NewQueueID = Database::$Interface->GetLastInsertID();

        return $NewQueueID;
    }

    /**
     * Returns the number of queue items of the campaign
     *
     * @return int
     * @author Cem Hurturk
     * */
    function CalculateTotalQueueItems($CampaignID, $IncludeSent = false) {
        $ArrayCriterias = array();
        if ($IncludeSent == false) {
            $ArrayCriterias['Status'] = 'Pending';
        }
        $TotalQueueItems = Database::$Interface->GetRows(array('COUNT(*) AS TotalFound'), array(MYSQL_TABLE_PREFIX . 'queue_c_' . mysql_real_escape_string($CampaignID)), $ArrayCriterias, array(), 0, 0, 'AND', false);
        $TotalQueueItems = $TotalQueueItems[0];
        $TotalQueueItems = $TotalQueueItems['TotalFound'];

        return $TotalQueueItems;
    }

    /**
     * Purges queue of the campaign and return totals
     *
     * @return array Returns total sent [0] and total failed [1]
     * @author Cem Hurturk
     * */
    function PurgeQueue($EachCampaignID) {
        $TotalSent = self::RetrieveQueueRows($EachCampaignID, array('COUNT(*) AS TotalSent'), array('Status' => 'Sent'));
        $TotalSent = $TotalSent[0]['TotalSent'];
        $TotalFailed = self::RetrieveQueueRows($EachCampaignID, array('COUNT(*) AS TotalFailed'), array('Status' => 'Failed'));
        $TotalFailed = $TotalFailed[0]['TotalFailed'];

        self::WriteFailedRecipientsToFile($EachCampaignID);

        self::Delete(0, $EachCampaignID);

        return array($TotalSent, $TotalFailed);
    }

    /**
     * Writes failed recipients to a txt file
     *
     * @return void
     * @author Mert Hurturk
     * */
    function WriteFailedRecipientsToFile($CampaignID) {
        $SQLQuery = 'SELECT * FROM `oempro_queue_c_' . $CampaignID . '` WHERE Status = "Failed"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        if (mysql_num_rows($ResultSet) > 0) {
            $FileHandler = fopen(DATA_PATH . 'tmp/cmp_failed_recipients_' . $CampaignID . '_' . date('YmdHis'), 'a');
            while ($eachQueueTable = mysql_fetch_array($ResultSet)) {
                fwrite($FileHandler, '"' . $eachQueueTable['EmailAddress'] . '","' . $eachQueueTable['StatusMessage'] . '"' . "\n");
            }
            fclose($FileHandler);
        }
    }

    /**
     * Returns the queue rows information for the provided campaign ID
     *
     * @param string $ArrayReturnFields
     * @param string $ArrayCriterias
     * @return array | boolean
     * @author Cem Hurturk
     */
    function RetrieveQueueRows($CampaignID, $ArrayReturnFields, $ArrayCriterias, $ProcessAmount = 0) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'queue_c_' . mysql_real_escape_string($CampaignID));
        $ArrayCriterias = $ArrayCriterias;
        $ArrayOrder = array();
        $ArrayQueue = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, $ProcessAmount, 0);

        if (count($ArrayQueue) < 1) {
            return false;
        }

        return $ArrayQueue;
    }

    /**
     * Deletes queue rows
     *
     * @param array $CampaignID
     * @return boolean
     * @author Mert Hurturk
     * */
    function Delete($ListID = 0, $CampaignID = 0, $SegmentID = 0, $SubscriberID = 0) {
        if ($CampaignID > 0) {
            // Drop campaign queue table
            Database::$Interface->ExecuteQuery('DROP TABLE IF EXISTS `' . MYSQL_TABLE_PREFIX . 'queue_c_' . mysql_real_escape_string($CampaignID) . '`');
        } else {
            if ($ListID == 0 && $SegmentID == 0 && $SubscriberID == 0)
                return false;

            // Loop through all campaign queue tables delete necessary rows
            $SQLQuery = "SHOW TABLES LIKE 'oempro_queue_c_%'";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            if (mysql_num_rows($ResultSet) > 0) {
                while ($eachQueueTable = mysql_fetch_array($ResultSet)) {
                    if ($ListID > 0) {
                        Database::$Interface->DeleteRows(array('RelListID' => $ListID), $eachQueueTable[0]);
                    } elseif ($SegmentID > 0) {
                        Database::$Interface->DeleteRows(array('RelSegmentID' => $SegmentID), $eachQueueTable[0]);
                    } elseif ($SubscriberID > 0) {
                        Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), $eachQueueTable[0]);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Optimizes the queue table
     *
     * @return void
     * @author Cem Hurturk
     */
    function OptimizeQueueTable($CampaignID) {
        // Optimize the outbox table to speed-up the process - Start
        $SQLQuery = "OPTIMIZE TABLE " . MYSQL_TABLE_PREFIX . "queue_c_" . mysql_real_escape_string($CampaignID);
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        // Optimize the outbox table to speed-up the process - End

        return;
    }

    /**
     * Loops the queue and sends emails
     *
     * @return int Returns total number of emails sent
     * @author Cem Hurturk
     * */
    function SendEmails($CampaignID, $ArrayUser, $ProcessAmount = 10, $IsAorBSplitTestCampaign = false) {
        $TotalSent = 0;

        $CacheListInformation = array();

        $ObjectBenchmark = new Benchmark(false);

        $ObjectBenchmark->mark('SendEmails function');

        // Load custom headers
        $emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');
        $campaignEmailHeaders = $emailHeaderMapper->findByEmailType(array('all', 'campaign'));

        // Check if user has email sending limitation - Start {
        if ($ArrayUser['GroupInformation']['LimitEmailSendPerPeriod'] > 0) {
            $EmailSendingPerPeriod = $ArrayUser['GroupInformation']['LimitEmailSendPerPeriod'];
        } else {
            $EmailSendingPerPeriod = 0;
        }
        // Check if user has email sending limitation - End }

        $ObjectBenchmark->mark('User email sending limit checked');

        // If email sending limit is set, calculate the number of emails sent so far in this period - Start {
        if ($EmailSendingPerPeriod > 0) {
            $TotalSentInThisPeriod = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUser['UserID'], date('Y-m-01 00:00:00'), date('Y-m-31 23:59:59'));
            $RemainingSendingLimit = $EmailSendingPerPeriod - $TotalSentInThisPeriod;
        } else {
            $EmailSendingLimit = 0;
            $RemainingSendingLimit = 100000000; // This limit is set to 100 million to avoid any suspend during the sending (unlimited sending)
        }
        // If email sending limit is set, calculate the number of emails sent so far in this period - End }

        $ObjectBenchmark->mark('User email sending limit checked');

        $ObjectBenchmark->mark('Campaign retrieved');

        // Retrieve campaign - Start {
        $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));
        // Retrieve campaign - End }

        $ObjectBenchmark->mark('Campaign retrieved');

        $ObjectBenchmark->mark('A/B split checked');

        // If the campaign is an A/B split testing campaign, get the split testing settings - Start {
        if ($IsAorBSplitTestCampaign == true) {
            $SplitTestingSettings = SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']);
            if ($SplitTestingSettings == false) {
                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                return false;
            }

            if ($SplitTestingSettings[0]['RelWinnerEmailID'] > 0) {
                $ArrayCampaign['RelEmailID'] = $SplitTestingSettings[0]['RelWinnerEmailID'];
                $IsAorBSplitTestCampaign = false;
            }
        }
        // If the campaign is an A/B split testing campaign, get the split testing settings - End }

        $ObjectBenchmark->mark('A/B split checked');

        $ObjectBenchmark->mark('Regular email campaign email processing');

        if ($IsAorBSplitTestCampaign == false) {
            // Retrieve email details of the campaign - Start {
            $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayCampaign['RelEmailID']));
            if ($ArrayEmail == false) {
                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                return false;
            }
            // Retrieve email details of the campaign - End }
            // If fetch URL is defined in email properties, fetch the URL and set contents - Start {
            if ($ArrayEmail['FetchURL'] != '') {
                $ArrayEmail['HTMLContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                $ArrayEmail['ContentType'] = 'HTML';

                if ($ArrayEmail['HTMLContent'] === false) {
                    Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                    return false;
                }
            }
            // If fetch URL is defined in email properties, fetch the URL and set contents - End }
            // If plain fetch URL is defined in email properties, fetch the URL and set contents - Start {
            if ($ArrayEmail['FetchPlainURL'] != '') {
                $ArrayEmail['PlainContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                $ArrayEmail['ContentType'] = ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
                if ($ArrayEmail['PlainContent'] === false) {
                    Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                    return false;
                }
            }
            // If plain fetch URL is defined in email properties, fetch the URL and set contents - End }
            // Add header/footer to the email (if exists in the user group) - Start {
            $ArrayReturn = Personalization::AddEmailHeaderFooter($ArrayEmail['PlainContent'], $ArrayEmail['HTMLContent'], $ArrayUser['GroupInformation']);
            $ArrayEmail['PlainContent'] = $ArrayReturn[0];
            $ArrayEmail['HTMLContent'] = $ArrayReturn[1];
            // Add header/footer to the email (if exists in the user group) - End }
        }

        $ObjectBenchmark->mark('Regular email campaign email processing');

        $ObjectBenchmark->mark('Mailer engine set');

        // Load the mailer engine - Start {
        if ($ArrayUser['GroupInformation']['SendMethod'] == 'System') {
            self::LoadSendEngine();
            SendEngine::SetSendMethod();
        } else {
            self::LoadSendEngine($ArrayUser);
            SendEngine::SetSendMethod(
                    true, $ArrayUser['GroupInformation']['SendMethodSMTPHost'], $ArrayUser['GroupInformation']['SendMethodSMTPPort'], $ArrayUser['GroupInformation']['SendMethodSMTPSecure'], $ArrayUser['GroupInformation']['SendMethodSMTPAuth'], $ArrayUser['GroupInformation']['SendMethodSMTPUsername'], $ArrayUser['GroupInformation']['SendMethodSMTPPassword'], $ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'], $ArrayUser['GroupInformation']['SendMethodSMTPDebug'], $ArrayUser['GroupInformation']['SendMethodSMTPKeepAlive'], $ArrayUser['GroupInformation']['SendMethodLocalMTAPath'], $ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'], $ArrayUser['GroupInformation']['SendMethodPowerMTADir'], $ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'], $ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'], ''
            );
        }
        SendEngine::SetEncoding(EMAIL_SOURCE_ENCODING);

        if ($ArrayUser['GroupInformation']['XMailer'] != '') {
            SendEngine::$XMailer = $ArrayUser['GroupInformation']['XMailer'];
        }
        // Load the mailer engine - End }

        $ObjectBenchmark->mark('Mailer engine set');

        $ObjectBenchmark->mark('Total queue items counted');

        // Calculate the total queue items - Start {
        $TotalQueueItems = self::CalculateTotalQueueItems($CampaignID, false);
        // Calculate the total queue items - End }

        $ObjectBenchmark->mark('Total queue items counted');

        $ObjectBenchmark->mark('Number of interval loops counted');

        // Calculate the number of interval loops required for queue items - Stat {
        $TotalIntervals = ceil($TotalQueueItems / $ProcessAmount);
        // Calculate the number of interval loops required for queue items - End }

        $ObjectBenchmark->mark('Number of interval loops counted');

        $ObjectBenchmark->mark('Recipient list retrieved');

        // Retrieve the array of recipient lists - Start {
        $ArrayRecipientLists = self::GetRecipientLists($CampaignID, false);
        // Retrieve the array of recipient lists - End }

        $ObjectBenchmark->mark('Recipient list retrieved');

        $ObjectBenchmark->mark('A/B split campaign calculations made');

        // Set split testing procedure (if the campaign is A/B split testing campaign) - Start {
        if ($IsAorBSplitTestCampaign == true) {
            $TestSize = $SplitTestingSettings[0]['TestSize'];
            $TestEmailVersions = Emails::RetrieveEmailsOfTest($SplitTestingSettings[0]['TestID'], $ArrayCampaign['CampaignID']);
            $TestEmailVersionsCounter = count($TestEmailVersions);

            if ($ArrayCampaign['TotalRecipients'] == $TotalQueueItems) {
                $EachTestRecipients = floor(($TotalQueueItems * ($TestSize / 100)) / $TestEmailVersionsCounter);
            } else {
                $EachTestRecipients = floor(($ArrayCampaign['TotalRecipients'] * ($TestSize / 100)) / $TestEmailVersionsCounter);

                if ($ArrayCampaign['TotalRecipients'] - $TotalQueueItems >= floor(($ArrayCampaign['TotalRecipients'] * ($TestSize / 100)))) {
                    // Testing period already finished
                    if ($SplitTestingSettings[0]['RelWinnerEmailID'] > 0) {
                        $ArrayCampaign['RelEmailID'] = $SplitTestingSettings[0]['RelWinnerEmailID'];
                        $IsAorBSplitTestCampaign = false;

                        // Retrieve email details of the campaign - Start {
                        $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayCampaign['RelEmailID']));
                        if ($ArrayEmail == false) {
                            Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                            return false;
                        }
                        // Retrieve email details of the campaign - End }
                        // If fetch URL is defined in email properties, fetch the URL and set contents - Start {
                        if ($ArrayEmail['FetchURL'] != '') {
                            $ArrayEmail['HTMLContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                            $ArrayEmail['ContentType'] = 'HTML';
                            if ($ArrayEmail['HTMLContent'] === false) {
                                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                                return false;
                            }
                        }
                        // If fetch URL is defined in email properties, fetch the URL and set contents - End }
                        // If plain fetch URL is defined in email properties, fetch the URL and set contents - Start {
                        if ($ArrayEmail['FetchPlainURL'] != '') {
                            $ArrayEmail['PlainContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                            $ArrayEmail['ContentType'] = ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
                            if ($ArrayEmail['PlainContent'] === false) {
                                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                                return false;
                            }
                        }
                        // If plain fetch URL is defined in email properties, fetch the URL and set contents - End }
                        // Add header/footer to the email (if exists in the user group) - Start {
                        $ArrayReturn = Personalization::AddEmailHeaderFooter($ArrayEmail['PlainContent'], $ArrayEmail['HTMLContent'], $ArrayUser['GroupInformation']);
                        $ArrayEmail['PlainContent'] = $ArrayReturn[0];
                        $ArrayEmail['HTMLContent'] = $ArrayReturn[1];
                        // Add header/footer to the email (if exists in the user group) - End }
                    }
                } else {
                    // Still in testing period, decide which version to continue testing

                    $CurrentTestVersion = floor((($ArrayCampaign['TotalRecipients'] - $TotalQueueItems) - 1) / $EachTestRecipients);
                    $CurrentTestVersion = ($CurrentTestVersion < 0 ? 0 : $CurrentTestVersion);
                    $AorBIntervalCounter = ($ArrayCampaign['TotalRecipients'] - $TotalQueueItems) + 1;
                }
            }
        }
        // Set split testing procedure (if the campaign is A/B split testing campaign) - End }

        $ObjectBenchmark->mark('A/B split campaign calculations made');

        $ObjectBenchmark->mark('Regular email campaign email processing');

        if ($IsAorBSplitTestCampaign == false) {
            // Set contents and subject of the email - Start
            if ($ArrayEmail['ContentType'] == 'HTML') {
                // HTML email
                $ContentType = 'HTML';
            } elseif ($ArrayEmail['ContentType'] == 'Plain') {
                // Plain email
                $ContentType = 'Plain';
            } elseif ($ArrayEmail['ContentType'] == 'Both') {
                // MultiPart email
                $ContentType = 'MultiPart';
            }
            // Set contents and subject of the email - End
            // Set email attachments (if attached) and image embedding (if enabled) - Start
            $ArrayEmail['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
                            $ContentType, $ArrayEmail['HTMLContent'], $ArrayCampaign['CampaignID'], $ArrayEmail['EmailID'], $ArrayUser['UserID'], 0, 0, Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $ArrayEmail['EmailID']), array('AttachmentID' => 'ASC')), ($ArrayEmail['ImageEmbedding'] == 'Enabled' ? true : false)
            );
            // Set email attachments (if attached) and image embedding (if enabled) - End
        }

        // Plug-in hook - Start
        $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], array()));
        $ArrayEmail['Subject'] = $ArrayPlugInReturnVars[0];
        $ArrayEmail['HTMLContent'] = $ArrayPlugInReturnVars[1];
        $ArrayEmail['PlainContent'] = $ArrayPlugInReturnVars[2];
        // Plug-in hook - End
        // Plug-in hook - Start
        $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Campaign.Email.Send.Before', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], array(), $ArrayCampaign['CampaignID']));
        $ArrayEmail['Subject'] = $ArrayPlugInReturnVars[0];
        $ArrayEmail['HTMLContent'] = $ArrayPlugInReturnVars[1];
        $ArrayEmail['PlainContent'] = $ArrayPlugInReturnVars[2];
        // Plug-in hook - End

        $ObjectBenchmark->mark('Regular email campaign email processing');

        $ObjectBenchmark->mark('Loop queue items');

        // Retrieve queue items - Start
        $TotalSubscribersLooped = 0;
        $AorBIntervalCounter = ($AorBIntervalCounter > 0 ? $AorBIntervalCounter : 0);
        $CurrentTestVersion = ($CurrentTestVersion > 0 ? $CurrentTestVersion : 0);
        $CurrentTestVersionEmailProcessed = false;

        // Oempro trial version setup
//	$TrialMaxIntervals = 3;

        for ($IntervalCounter = 1; $IntervalCounter <= $TotalIntervals; $IntervalCounter++) {
            $ObjectBenchmark->mark('Campaign status checked');

            // Check campaign status. If it's not set to 'Sending', cancel the process - Start {
            if (self::IsCampaignPaused($TotalSubscribersLooped, $CampaignID) == true) {
                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                break;
            }
            // Check campaign status. If it's not set to 'Sending', cancel the process - End }
            // Check if there's enough credits for the delivery - Start {
            $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);

            if ($CreditsCheckReturn[0] == false) {
                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                break;
            }
            // Check if there's enough credits for the delivery - End }
            // Plug-in hook - Start
            $ArrayPlugInReturnVars = Plugins::HookListener('Action', 'Email.Send.Stop', array('Campaign'));
            if ($ArrayPlugInReturnVars[0] == 'pause') {
                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                break;
            }
            // Plug-in hook - End
            // Trial limit apply - Start
            // TODO: Comment these lines when compiling the official package
//		if ($IntervalCounter > $TrialMaxIntervals)
//		{
//			Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
//			break;
//		}
            // Trial limit apply - End

            $ObjectBenchmark->mark('Campaign status checked');

            $ObjectBenchmark->mark('Campaign last activity date updated');

            // Update campaign's last activity date - Start {
            Campaigns::Update(array('LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $ArrayCampaign['CampaignID']));
            // Update campaign's last activity date - End }

            $ObjectBenchmark->mark('Campaign last activity date updated');

            $ObjectBenchmark->mark('Queue rows retrieved');

            $ArrayQueueRows = self::RetrieveQueueRows($CampaignID, array('*'), array('Status' => 'Pending'), $ProcessAmount);

            $ObjectBenchmark->mark('Queue rows retrieved');

            $ObjectBenchmark->mark('Loop queue rows');

            foreach ($ArrayQueueRows as $Index => $ArrayEachQueueRow) {
                $AorBIntervalCounter++;

                $ObjectBenchmark->mark('If A/B, decide the testing version');

                // If the campaign is A or B split testing, decide the testing version - Start {
                if ($IsAorBSplitTestCampaign == true) {
                    // Decide the testing version - Start {
                    $OldCurrentTestVersion = $CurrentTestVersion;
                    $CurrentTestVersion = ceil($AorBIntervalCounter / $EachTestRecipients);
                    $CurrentTestVersion = ($CurrentTestVersion < 1 ? 1 : $CurrentTestVersion);

                    if ($CurrentTestVersion > count($TestEmailVersions)) {
                        Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                        SplitTests::Update($SplitTestingSettings[0]['TestID'], array('SplitTestingStatus' => 'Waiting For Winner'));
                        break;
                    }

                    if ($OldCurrentTestVersion != $CurrentTestVersion) {
                        $CurrentTestVersionEmailProcessed = false;
                    }
                    // Decide the testing version - End }
                }
                // If the campaign is A or B split testing, decide the testing version - End }

                $ObjectBenchmark->mark('If A/B, decide the testing version');

                $ObjectBenchmark->mark('Campaign sending status checked');

                // Check if there's enough credits for the delivery - Start {
                $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);

                if ($CreditsCheckReturn[0] == false) {
                    Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                    break;
                }
                // Check if there's enough credits for the delivery - End }
                // Plug-in hook - Start
                $ArrayPlugInReturnVars = Plugins::HookListener('Action', 'Email.Send.Stop', array('Campaign'));
                if ($ArrayPlugInReturnVars[0] == 'pause') {
                    Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                    break;
                }
                // Plug-in hook - End
                // Check campaign status. If it's not set to 'Sending', cancel the process - Start {
                if (self::IsCampaignPaused($TotalSubscribersLooped, $CampaignID) == true) {
                    Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                    break;
                }
                // Check campaign status. If it's not set to 'Sending', cancel the process - End }

                $ObjectBenchmark->mark('Campaign sending status checked');

                $ObjectBenchmark->mark('Check remaining sending limit');

                // Check remaining sending limit, if it's exceeded, pause sending process - Start {
                if ($EmailSendingPerPeriod > 0) {
                    if ($RemainingSendingLimit <= 0) {
                        Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                        break;
                    } else {
                        $RemainingSendingLimit = $RemainingSendingLimit - 1;
                    }
                }
                // Check remaining sending limit, if it's exceeded, pause sending process - End }

                $ObjectBenchmark->mark('Check remaining sending limit');

                $ObjectBenchmark->mark('Check if msg/conn is exceeded, re-connect if exceeded');

                // If messages per connection exceeded and enabled, close the connection and re-connect - Start {
                if (SEND_METHOD_SMTP_MSG_CONN != false) {
                    if (($TotalSubscribersLooped % SEND_METHOD_SMTP_MSG_CONN == 0) && ($TotalSubscribersLooped > 0)) {
                        SendEngine::CloseConnections();
                    }
                }
                // If messages per connection exceeded and enabled, close the connection and re-connect - End }

                $ObjectBenchmark->mark('Check if msg/conn is exceeded, re-connect if exceeded');

                $ObjectBenchmark->mark('Load balancing');

                // Load balancing - Start {
                if (LOAD_BALANCE_STATUS == true) {
                    if (($TotalSubscribersLooped % LOAD_BALANCE_EMAILS == 0) && ($TotalSubscribersLooped > 0)) {
                        SendEngine::CloseConnections();
                        sleep(LOAD_BALANCE_SLEEP);
                    }
                }
                // Load balancing - End }

                $ObjectBenchmark->mark('Load balancing');

                $ObjectBenchmark->mark('Retrieve subscriber info');

                // Retrieve subscriber information - Start {
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $ArrayEachQueueRow['RelSubscriberID']), $ArrayEachQueueRow['RelListID']);
                // Retrieve subscriber information - End }

                $ObjectBenchmark->mark('Retrieve subscriber info');

                $ObjectBenchmark->mark('Retrieve list info');

                // Retrieve list information - Start {
                if (!isset($CacheListInformation[$ArrayEachQueueRow['RelListID']])) {
                    $CacheListInformation[$ArrayEachQueueRow['RelListID']] = Lists::RetrieveList(array('*'), array('ListID' => $ArrayEachQueueRow['RelListID']), false, false);
                }
                $ArrayList = $CacheListInformation[$ArrayEachQueueRow['RelListID']];
                // Retrieve list information - End }

                $ObjectBenchmark->mark('Retrieve list info');

                $ObjectBenchmark->mark('If regular email, email personalization');

                if ($IsAorBSplitTestCampaign == false) {
                    // Personalize email subject and contents - Start
                    $TMPSubject = Personalization::Personalize($ArrayEmail['Subject'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);

                    if ($ArrayEmail['HTMLContent'] != '') {
                        $TMPHTMLBody = Personalization::Personalize($ArrayEmail['HTMLContent'], array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);
                    }
                    if ($ArrayEmail['PlainContent'] != '') {
                        $TMPPlainBody = Personalization::Personalize($ArrayEmail['PlainContent'], array('Subscriber', 'Links', 'User', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);
                    }
                    // Personalize email subject and contents - End
                    // Plug-in hook - Start
                    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array($TMPSubject, $TMPHTMLBody, $TMPPlainBody, $ArraySubscriber, 'Campaign'));
                    $TMPSubject = $ArrayPlugInReturnVars[0];
                    $TMPHTMLBody = $ArrayPlugInReturnVars[1];
                    $TMPPlainBody = $ArrayPlugInReturnVars[2];
                    // Plug-in hook - End
                    // Plug-in hook - Start
                    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Campaign.Email.Send.EachRecipient', array($TMPSubject, $TMPHTMLBody, $TMPPlainBody, $ArraySubscriber, $ArrayEmail, $ContentType, $ArrayUser, $ArrayCampaign, $ArrayList));
                    $TMPSubject = $ArrayPlugInReturnVars[0];
                    $TMPHTMLBody = $ArrayPlugInReturnVars[1];
                    $TMPPlainBody = $ArrayPlugInReturnVars[2];
                    $ArraySubscriber = $ArrayPlugInReturnVars[3];
                    $ArrayEmail = $ArrayPlugInReturnVars[4];
                    $ContentType = $ArrayPlugInReturnVars[5];
                    $ArrayUser = $ArrayPlugInReturnVars[6];
                    $ArrayCampaign = $ArrayPlugInReturnVars[7];
                    $ArrayList = $ArrayPlugInReturnVars[8];
                    // Plug-in hook - End
                }

                $ObjectBenchmark->mark('If regular email, email personalization');

                $ObjectBenchmark->mark('Generate abuse message ID');

                // Generate the abuse message ID - Start {
                $AbuseMessageID = self::GenerateAbuseMessageID($ArrayCampaign['CampaignID'], $ArraySubscriber['SubscriberID'], $ArraySubscriber['EmailAddress'], $ArrayEachQueueRow['RelListID'], $ArrayCampaign['RelOwnerUserID'], 0);
                // Generate the abuse message ID - End }

                $ObjectBenchmark->mark('Generate abuse message ID');

                if ($IsAorBSplitTestCampaign == false) {
                    $ObjectBenchmark->mark('Set email properties for regular campaign');

                    $ArrayHeaders = array("X-MSYS-API" => '{"campaign_id":"' . $ArrayCampaign['CampaignID'] . '"}');

                    // Generate email properties - Start
                    SendEngine::SetEmailProperties(
                            $ArrayEmail['FromName'], $ArrayEmail['FromEmail'], $ArraySubscriber['EmailAddress'], $ArraySubscriber['EmailAddress'], $ArrayEmail['ReplyToName'], $ArrayEmail['ReplyToEmail'], $ArrayHeaders, $ContentType, $TMPSubject, $TMPHTMLBody, $TMPPlainBody, $AbuseMessageID, $ArrayCampaign['CampaignID'], $ArrayEmail['EmailID'], $ArrayUser['UserID'], $ArraySubscriber['SubscriberID'], $ArrayList['ListID']
                    );
                    // Generate email properties - End

                    $ObjectBenchmark->mark('Set email properties for regular campaign');
                } else {
                    $ObjectBenchmark->mark('Set email properties for A/B campaign');

                    if ($CurrentTestVersionEmailProcessed == false) {
                        $ArrayEmail = $TestEmailVersions[$CurrentTestVersion - 1];

                        // If fetch URL is defined in email properties, fetch the URL and set contents - Start {
                        if ($ArrayEmail['FetchURL'] != '') {
                            $ArrayEmail['HTMLContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                            $ArrayEmail['ContentType'] = 'HTML';
                            if ($ArrayEmail['HTMLContent'] === false) {
                                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                                return false;
                            }
                        }
                        // If fetch URL is defined in email properties, fetch the URL and set contents - End }
                        // If plain fetch URL is defined in email properties, fetch the URL and set contents - Start {
                        if ($ArrayEmail['FetchPlainURL'] != '') {
                            $ArrayEmail['PlainContent'] = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('User'), array(), $ArrayUser, array(), $ArrayCampaign, array(), false, $ArrayEmail));
                            $ArrayEmail['ContentType'] = ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
                            if ($ArrayEmail['PlainContent'] === false) {
                                Campaigns::Update(array('CampaignStatus' => 'Paused'), array('CampaignID' => $ArrayCampaign['CampaignID']));
                                return false;
                            }
                        }
                        // If plain fetch URL is defined in email properties, fetch the URL and set contents - End }
                        // Set contents and subject of the email - Start {
                        if ($ArrayEmail['ContentType'] == 'HTML') {
                            // HTML email
                            $ContentType = 'HTML';
                        } elseif ($ArrayEmail['ContentType'] == 'Plain') {
                            // Plain email
                            $ContentType = 'Plain';
                        } elseif ($ArrayEmail['ContentType'] == 'Both') {
                            // MultiPart email
                            $ContentType = 'MultiPart';
                        }
                        // Set contents and subject of the email - End }
                        // Add header/footer to the email (if exists in the user group) - Start {
                        $ArrayReturn = Personalization::AddEmailHeaderFooter($ArrayEmail['PlainContent'], $ArrayEmail['HTMLContent'], $ArrayUser['GroupInformation']);
                        $ArrayEmail['PlainContent'] = $ArrayReturn[0];
                        $ArrayEmail['HTMLContent'] = $ArrayReturn[1];
                        // Add header/footer to the email (if exists in the user group) - End }
                        // Set email attachments (if attached) and image embedding (if enabled) - Start {
                        $ArrayEmail['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
                                        $ContentType, $ArrayEmail['HTMLContent'], $ArrayCampaign['CampaignID'], $ArrayEmail['EmailID'], $ArrayUser['UserID'], $ArraySubscriber['SubscriberID'], $ArrayList['ListID'], Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $ArrayEmail['EmailID']), array('AttachmentID' => 'ASC')), ($ArrayEmail['ImageEmbedding'] == 'Enabled' ? true : false)
                        );
                        // Set email attachments (if attached) and image embedding (if enabled) - End }
                        // Plug-in hook - Start {
                        $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], $ArraySubscriber));
                        $ArrayEmail['Subject'] = $ArrayPlugInReturnVars[0];
                        $ArrayEmail['HTMLContent'] = $ArrayPlugInReturnVars[1];
                        $ArrayEmail['PlainContent'] = $ArrayPlugInReturnVars[2];
                        // Plug-in hook - End }

                        $CurrentTestVersionEmailProcessed = true;
                    }

                    // Personalize email subject and contents - Start
                    $TMPSubject = Personalization::Personalize($ArrayEmail['Subject'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);

                    if ($ArrayEmail['HTMLContent'] != '') {
                        $TMPHTMLBody = Personalization::Personalize($ArrayEmail['HTMLContent'], array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);
                    }
                    if ($ArrayEmail['PlainContent'] != '') {
                        $TMPPlainBody = Personalization::Personalize($ArrayEmail['PlainContent'], array('Subscriber', 'Links', 'User', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, $ArrayEmail);
                    }
                    // Personalize email subject and contents - End
                    // Plug-in hook - Start
                    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array($TMPSubject, $TMPHTMLBody, $TMPPlainBody, $ArraySubscriber, 'Campaign'));
                    $TMPSubject = $ArrayPlugInReturnVars[0];
                    $TMPHTMLBody = $ArrayPlugInReturnVars[1];
                    $TMPPlainBody = $ArrayPlugInReturnVars[2];
                    // Plug-in hook - End
                    // Plug-in hook - Start
                    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Campaign.Email.Send.EachRecipient', array($TMPSubject, $TMPHTMLBody, $TMPPlainBody, $ArraySubscriber, $ArrayEmail, $ContentType, $ArrayUser, $ArrayCampaign, $ArrayList));
                    $TMPSubject = $ArrayPlugInReturnVars[0];
                    $TMPHTMLBody = $ArrayPlugInReturnVars[1];
                    $TMPPlainBody = $ArrayPlugInReturnVars[2];
                    $ArraySubscriber = $ArrayPlugInReturnVars[3];
                    $ArrayEmail = $ArrayPlugInReturnVars[4];
                    $ContentType = $ArrayPlugInReturnVars[5];
                    $ArrayUser = $ArrayPlugInReturnVars[6];
                    $ArrayCampaign = $ArrayPlugInReturnVars[7];
                    $ArrayList = $ArrayPlugInReturnVars[8];
                    // Plug-in hook - End
                    // Generate email properties - Start

                    $ArrayHeaders = array("X-MSYS-API" => '{"campaign_id":"' . $ArrayCampaign['CampaignID'] . '"}');

                    SendEngine::SetEmailProperties(
                            $ArrayEmail['FromName'], $ArrayEmail['FromEmail'], $ArraySubscriber['EmailAddress'], $ArraySubscriber['EmailAddress'], $ArrayEmail['ReplyToName'], $ArrayEmail['ReplyToEmail'], $ArrayHeaders, $ContentType, $TMPSubject, $TMPHTMLBody, $TMPPlainBody, $AbuseMessageID, $ArrayCampaign['CampaignID'], $ArrayEmail['EmailID'], $ArrayUser['UserID'], $ArraySubscriber['SubscriberID'], $ArrayList['ListID']
                    );
                    // Generate email properties - End

                    $ObjectBenchmark->mark('Set email properties for A/B campaign');
                }

                $ObjectBenchmark->mark('Send the email and update campaign statistics');

                // Add custom headers
                $campaignEmailHeaderReplacements = array(
                    '%User:ID%' => $ArrayUser['UserID'],
                    '%User:UserID%' => $ArrayUser['UserID'],
                    '%User:EmailAddress%' => $ArrayUser['EmailAddress'],
                    '%UserGroup:ID%' => $ArrayUser['RelUserGroupID'],
                    '%UserGroup:UserGroupID%' => $ArrayUser['RelUserGroupID'],
                    '%Campaign:ID%' => $ArrayCampaign['CampaignID'],
                    '%Campaign:CampaignID%' => $ArrayCampaign['CampaignID'],
                    '%List:ID%' => $ArrayList['ListID'],
                    '%List:ListID%' => $ArrayList['ListID'],
                    '%Subscriber:ID%' => $ArraySubscriber['SubscriberID'],
                    '%Subscriber:SubscriberID%' => $ArraySubscriber['SubscriberID'],
                    '%Subscriber:EmailAddress%' => $ArraySubscriber['EmailAddress'],
                    '%Subscriber:SubscriptionIP%' => $ArraySubscriber['SubscriptionIP'],
                    '%Subscriber:SubscriptionDate%' => $ArraySubscriber['SubscriptionDate']
                );
                foreach ($campaignEmailHeaders as $eachHeader) {
                    SendEngine::$Engine->AddCustomHeader(
                            str_replace(
                                    array_keys($campaignEmailHeaderReplacements), array_values($campaignEmailHeaderReplacements), $eachHeader->__toString()
                            )
                    );
                }

                if (SENDGRID_EMAIL_HEADER == true) {
                    SendEngine::$Engine->AddCustomHeader('X-SMTPAPI: {"unique_args":{"abuse-id":"' . $AbuseMessageID . '"}, "category":"campaign"}');
                }

                // Send the email to the recipient - Start {
                $ArrayResult = SendEngine::SendEmail();

                if (is_array($ArrayResult) == true && $ArrayResult[0] == false) {
                    // Error occurred.
                    // Change the status of queue to 'sent' or 'failed' - Start
                    $ArrayFieldnValues = array(
                        'Status' => 'Failed',
                        'StatusMessage' => $ArrayResult[1],
                    );
                    self::Update($CampaignID, $ArrayFieldnValues, array('QueueID' => $ArrayEachQueueRow['QueueID']));
                    // Change the status of queue to 'sent' or 'failed' - End
                    // Update campaign totals - Start
                    $ArrayFieldnValues = array(
                        'TotalFailed' => $ArrayCampaign['TotalFailed'] + 1,
                    );
                    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));

                    $ArrayCampaign['TotalFailed'] ++;
                    // Update campaign totals - End
                } else {
                    // Email sent.
                    // Change the status of queue to 'sent' or 'failed' - Start
                    $ArrayFieldnValues = array(
                        'Status' => 'Sent',
                    );
                    self::Update($CampaignID, $ArrayFieldnValues, array('QueueID' => $ArrayEachQueueRow['QueueID']));
                    // Change the status of queue to 'sent' or 'failed' - End
                    // Update campaign totals - Start
                    $ArrayFieldnValues = array(
                        'TotalSent' => $ArrayCampaign['TotalSent'] + 1,
                    );
                    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));

                    // Email sending is processed. Track the payment activity - Start
                    Payments::CampaignRecipientSent($ArrayCampaign['CampaignID'], 'Sent');
                    // Email sending is processed. Track the payment activity - End

                    $ArrayCampaign['TotalSent'] ++;
                    // Update campaign totals - End

                    $TotalSent++;
                }
                // Send the email to the recipient - End }
                // Deduct email credits - Start {
                $ArrayUser = Payments::DeductCredits($ArrayUser, true);
                // Deduct email credits - End }

                $ObjectBenchmark->mark('Send the email and update campaign statistics');

                $ObjectBenchmark->mark('Track payment activity');

                // Email sending is processed. Track the payment activity - Start {
                Payments::CampaignRecipientSent($ArrayCampaign['CampaignID'], 'Total');
                // Email sending is processed. Track the payment activity - End }

                $ObjectBenchmark->mark('Track payment activity');

                $ObjectBenchmark->mark('Send engine cleared');

                // Clear send engine cache - Start {
                SendEngine::ClearRecipientCache();
                // Clear send engine cache - End }

                $ObjectBenchmark->mark('Send engine cleared');

                $TotalSubscribersLooped++;
            }

            $ObjectBenchmark->mark('Loop queue rows');
        }
        // Retrieve queue items - End

        $ObjectBenchmark->mark('Loop queue items');

        // Remove temporary image embedding files - Start {
        SendEngine::RemoveTemporaryImageEmbeddingFiles();
        // Remove temporary image embedding files - End }
        // Close the mailer connection - Start {
        SendEngine::CloseConnections();
        // Close the mailer connection - End }

        $ObjectBenchmark->mark('SendEmails function');

        $ObjectBenchmark->printResults();

        return $TotalSent;
    }

    /**
     * Loads the send engine
     *
     * @return void
     * @author Cem Hurturk
     */
    function LoadSendEngine($ArrayUser = array()) {
        Core::LoadObject('send_engine');

        if ($ArrayUser['GroupInformation']['SendMethod'] == 'System') {
            SendEngine::SetEngine(MAIL_ENGINE, SEND_METHOD);
        } else {
            SendEngine::SetEngine(MAIL_ENGINE, $ArrayUser['GroupInformation']['SendMethod']);
        }

        return;
    }

    /**
     * Generates unique message ID to for abuse reporting
     *
     * @return int
     * @author Cem Hurturk
     * */
    function GenerateAbuseMessageID($CampaignID, $SubscriberID, $EmailAddress, $ListID, $UserID, $AutoResponderID) {
        $CampaignID = Core::EncryptNumberAdvanced($CampaignID);
        $SubscriberID = Core::EncryptNumberAdvanced($SubscriberID);
        $EmailAddress = rawurlencode(base64_encode($EmailAddress));
        $ListID = Core::EncryptNumberAdvanced($ListID);
        $UserID = Core::EncryptNumberAdvanced($UserID);
        $AutoResponderID = Core::EncryptNumberAdvanced($AutoResponderID);

        $AbuseMessageID = $CampaignID . '-' . $SubscriberID . '-' . $EmailAddress . '-' . $ListID . '-' . $UserID . '-' . $AutoResponderID;

        // The following line was the old abuse-message-id. It's not being used any more.
//	$AbuseMessageID = rawurlencode(base64_encode($CampaignID.'||||'.$SubscriberID.'||||'.$EmailAddress.'||||'.$ListID.'||||'.$UserID.'||||'.$AutoResponderID));
        return $AbuseMessageID;
    }

    /**
     * Updates queue
     *
     * @param array $ArrayFieldAndValues
     * @param array $ArrayCriterias
     * @return boolean
     * @author Mert Hurturk
     * */
    public static function Update($CampaignID, $ArrayFieldAndValues, $ArrayCriterias) {
        // Check for required fields of this function - Start
        // $ArrayCriterias check
        if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) {
            return false;
        }
        // Check for required fields of this function - End

        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'queue_c_' . mysql_real_escape_string($CampaignID));
        return true;
    }

}

// END class EmailQueue
?>