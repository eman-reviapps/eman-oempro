<?php
// Required modules - Start
Core::LoadObject('attachments');
// Required modules - End

/**
 * Emails class
 *
 * This class holds all email related functions
 * @package Oempro
 * @author Octeth
 **/
class Emails extends Core
{
/**
 * Creates a new email with given values
 *
 * @param array $ArrayFieldAndValues Values of new email
 * @return boolean|integer ID of new email
 * @author Mert Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Create a new record in subscriber_lists table - Start
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'emails');
	$NewEmailID = Database::$Interface->GetLastInsertID();
	// Create a new record in subscriber_lists table - End

	return $NewEmailID;
	}

/**
 * Creates a new design preview job record with given values
 *
 * @param array $ArrayFieldAndValues Values of new email
 * @return boolean|integer ID of new email
 * @author Mert Hurturk
 **/
public static function CreateDesignPreviewJob($ArrayFieldAndValues)
	{
	// Create a new record in subscriber_lists table - Start
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'designpreview_jobs');
	$NewJobID = Database::$Interface->GetLastInsertID();
	// Create a new record in subscriber_lists table - End

	return $NewJobID;
	}

/**
 * Retrieves email record
 *
 * @param string $UserID
 * @param string $EmailID
 * @return array|boolean
 * @author Cem Hurturk
 */
public static function RetrieveEmail($ArrayReturnFields, $ArrayCriterias, $RetrieveAttachments = true)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'emails');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayEmail	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	$ArrayEmail['EmailName'] = addslashes($ArrayEmail['EmailName']);

	if (count($ArrayEmail) < 1) { return false; } // if there are no records, return false

	$ArrayEmail = $ArrayEmail[0];
	if ($RetrieveAttachments == true)
		{
		$ArrayEmail['Attachments'] = Attachments::RetrieveAttachments(array('*'), array('RelEmailID'=>$ArrayEmail['EmailID']));
		}

	return $ArrayEmail;
	}

/**
 * Retrieves design preview job
 *
 * @return array|boolean
 * @author Cem Hurturk
 */
public static function RetrieveDesignPreviewJob($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'designpreview_jobs');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayDesignJobs	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayDesignJobs) < 1) { return false; } // if there are no records, return false

	$ArrayDesignJobs = $ArrayDesignJobs[0];

	return $ArrayDesignJobs;
	}

/**
 * Reteurns all emails matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving emails.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveDesignPreviewJobs($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields			= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables		= array(MYSQL_TABLE_PREFIX.'designpreview_jobs');
	$ArrayCriterias			= $ArrayCriterias;
	$ArrayDesignJobs		= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array('SubmitDate' => 'DESC'));

	if (count($ArrayDesignJobs) < 1) { return false; } // if there are no auto responders, return false

	return $ArrayDesignJobs;
	}

/**
 * Reteurns all emails matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving emails.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveEmails($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('EmailName'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields			= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables		= array(MYSQL_TABLE_PREFIX.'emails');
	$ArrayCriterias			= $ArrayCriterias;
	$ArrayEmails			= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayEmails) < 1) { return false; } // if there are no auto responders, return false

	foreach ($ArrayEmails as &$Each)
		{
		$Each['EmailName'] = addslashes($Each['EmailName']);
		}

	return $ArrayEmails;
	}

/**
 * Reteurns all emails of a test
 *
 * @param integer $TestID
 * @param integer $CampaignID
 * @return array
 * @author Mert Hurturk
 **/
public static function RetrieveEmailsOfTest($TestID, $CampaignID, $RetrieveStatistics = false)
	{
	Core::LoadObject('statistics');

	$SQLQuery = 'SELECT * FROM '.MYSQL_TABLE_PREFIX.'split_test_emails AS reltestsemails INNER JOIN '.MYSQL_TABLE_PREFIX.'emails AS emails ON reltestsemails.RelEmailID = emails.EmailID WHERE reltestsemails.RelTestID = '.$TestID.' AND reltestsemails.RelCampaignID = '.$CampaignID.' ORDER BY emails.EmailName';
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

	if (mysql_num_rows($ResultSet) < 1) return array();

	$ArrayEmails = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		if ($RetrieveStatistics)
			{
			$EachRow['UniqueClicks'] = Statistics::TotalUniqueClicksOfCampaign($CampaignID, $EachRow['RelUserID'], $EachRow['EmailID']);
			$EachRow['UniqueClicks'] = (! $EachRow['UniqueClicks'] ) ? 0 : $EachRow['UniqueClicks'];
			$EachRow['UniqueOpens'] = Statistics::TotalUniqueOpensOfCampaign($CampaignID, $EachRow['RelUserID'], $EachRow['EmailID']);
			$EachRow['UniqueOpens'] = (! $EachRow['UniqueOpens'] ) ? 0 : $EachRow['UniqueOpens'];
			}
		$ArrayEmails[] = $EachRow;
		}

	return $ArrayEmails;
	}

/**
 * Updates email information
 *
 * @param array $ArrayFieldAndValues Values of updated email information
 * @param array $ArrayCriterias Criterias to be matched while updating email
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriterias check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'emails');
	return true;
	}

/**
 * Updates email information
 *
 * @param array $ArrayFieldAndValues Values of updated email information
 * @param array $ArrayCriterias Criterias to be matched while updating email
 * @return boolean
 * @author Mert Hurturk
 **/
public static function UpdateDesignPreviewJob($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriterias check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'designpreview_jobs');
	return true;
	}

/**
 * Deletes emails
 *
 * @param integer $ArrayCriterias Criterias to be matched when deleting emails
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriteriasCheck check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	if (isset($ArrayCriterias['EmailID']) && strpos($ArrayCriterias['EmailID'], ',') !== false)
		{
		$ArrayEmailIDs = explode(',', $ArrayCriterias['EmailID']);
		unset($ArrayCriterias['EmailID']);
		$ArrayCriterias = array_merge($ArrayCriterias);
		foreach ($ArrayEmailIDs as $EachEmailID)
			{
			$ArrayEmail = self::RetrieveEmail(array('*'), array_merge($ArrayCriterias, array("EmailID"=>$EachEmailID)));
			self::_DeleteEmail($ArrayEmail);
			}
		}
	else
		{
		$ArrayEmail = self::RetrieveEmail(array('*'), $ArrayCriterias);
		self::_DeleteEmail($ArrayEmail);
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Email', array($ArrayCriterias));
	// Plug-in hook - End
	}

/**
 * Delete email function
 *
 * @param string $ArrayEmail
 * @return void
 * @author Cem Hurturk
 */
public static function _DeleteEmail($ArrayEmail)
	{
	if ($ArrayEmail != false)
		{
		Database::$Interface->DeleteRows(array('EmailID' => $ArrayEmail['EmailID']), MYSQL_TABLE_PREFIX.'emails');
		Attachments::Delete(array('RelEmailID'=>$ArrayEmail['EmailID']));

		Core::LoadObject('auto_responders');
		Core::LoadObject('campaigns');
		Core::LoadObject('transaction_emails');

		AutoResponders::Delete($ArrayEmail['RelUserID'], array(), 0, $ArrayEmail['EmailID']);
		// TODO: What about campaigns which uses the deleted email as the email source??? (if delete is preferred, it's done below - commented)
		// Campaigns::Delete($ArrayEmail['RelUserID'], array(), $ArrayEmail['EmailID']);
		Emails::DeleteDesignPreviewJob(array('RelEmailID' => $ArrayEmail['EmailID']));
		// TODO: What about subscriber lists which uses the deleted email as opt-in message???
		TransactionEmails::Delete(0, $ArrayEmail['EmailID']);
		}
	else
		{
		return false;
		}
	}

/**
 * Deletes design preview job
 *
 * @param integer $ArrayCriterias Criterias to be matched when deleting emails
 * @return boolean
 * @author Mert Hurturk
 **/
public static function DeleteDesignPreviewJob($ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriteriasCheck check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->DeleteRows($ArrayCriterias, MYSQL_TABLE_PREFIX.'designpreview_jobs');

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.DesignPreviewJob', array($ArrayCriterias));
	// Plug-in hook - End
	}

/**
 * Detects images in the message returns the list
 *
 * @param string $String
 * @param array $ArrayEmbedExcludedObjects
 * @return array
 * @author Cem Hurturk
 **/
public static function DetectImages($String, $ArrayEmbedExcludedObjects, $ExcludeNoEmbedFlagged = false)
	{
	$ArrayObjects = array();

	// src= - Start
		$Pattern = "/src\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $String, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch)
			{
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - Start
			$SkipEmbed = false;
			foreach ($ArrayEmbedExcludedObjects as $EachExcluded)
				{
				if (substr($EachMatch[2], 0, strlen($EachExcluded)) == $EachExcluded)
					{
					$SkipEmbed = true;
					continue;
					}
				}
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - End

			// Check if this image is set to be excluded in image embedding - Start {
			if ((strpos($EachMatch[2], 'no-embed=1') !== false) && ($ExcludeNoEmbedFlagged == true))
				{
				$SkipEmbed = true;
				continue;
				}
			// Check if this image is set to be excluded in image embedding - End }

			if ($SkipEmbed == false)
				{
				$ArrayObjects[] = $EachMatch[2];
				}
			}
		}
	// src= - End

	// background-image:url( - Start
		$Pattern = "/background\-image\:[\s|]url\((.*)\)/iU";
	if (preg_match_all($Pattern, $String, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch)
			{
			$ArrayImages[] = $EachMatch[1];
			}
		}
	// background-image:url( - End

	// background=" - Start
		$Pattern = "/background\=(\"|\'|)(.*)(\"|\'|\>)/iU";
	if (preg_match_all($Pattern, $String, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch)
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - End

	// <link href=" - Start
		$Pattern = "/\<link href\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $String, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch)
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// <link href=" - End

	$ArrayObjects = array_flip(array_flip($ArrayObjects));

	$ArrayParsedURL = parse_url(APP_URL);

	$NewArrayObjects=array();

	foreach ($ArrayObjects as $EachObject)
		{
		// If the object path is defined starting with '/', add website URL to the beginning - Start
		if (substr($EachObject, 0, 1) == '/')
			{

			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].$EachObject;

			$NewArrayObjects[]=$EachObjectRenamed;
			}
		elseif (substr($EachObject, 0, 2) == './')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 1, strlen($EachObject));
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		elseif (substr($EachObject, 0, 3) == '../')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 2, strlen($EachObject));
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		else
			{
			$EachObjectRenamed = $EachObject;
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		// If the object path is defined starting with '/', add website URL to the beginning - End
		}

	return array($ArrayObjects, $NewArrayObjects);
	}

public static function DetectJavaScript($String)
	{
	$ArrayJavaScript = array();

	// JavaScript Tags - Start {
	preg_match_all('/\<script(.*)(type\=\"text\/javascript\")(.*)\>(.*)\<\/script\>/isU', $String, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $Index=>$Each)
		{
		$ArrayJavaScripts['JSTag'][] = $Each[0];
		}
	// JavaScript Tags - End }

	return $ArrayJavaScripts;
	}

public static function DetectCSS($String)
	{
	$ArrayCSS = array();

	// External CSS - Start {
	preg_match_all("/\<link(.*)rel=\"stylesheet\"(.*)href=\"(.*)\"(.*)type=\"text\/css\"(.*)\>/iU", $String, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $Index=>$EachInlineCSS)
		{
		$ArrayCSS['Link'][] = $EachInlineCSS[3];
		}
	// External CSS - End }

	// Inline Styles - Start {
	preg_match_all('/(style=")(.*)(")/iU', $String, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $Index=>$EachInlineCSS)
		{
		$ArrayCSS['Inline'][] = $EachInlineCSS[0];
		}
	// Inline Styles - End }

	// Style Tags - Start {
	preg_match_all('/\<style(.*)type=\"text\/css\"(.*)>(.*)\<\/style\>/isU', $String, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $Index=>$EachInlineCSS)
		{
		$ArrayCSS['StyleTag'][] = $EachInlineCSS[0];
		}
	// Style Tags - End }

	return $ArrayCSS;
	}

/**
 * Sends test email using the provided send method
 *
 * @return void
 * @author Cem Hurturk
 */
public static function SendTestEmail($ToEmail, $Subject, $HTMLBody, $PlainBody, $SendMethod, $SMTPHost = '', $SMTPPort = '', $SMTPSecure = '', $SMTPAuth = '', $SMTPUsername = '', $SMTPPassword = '', $SMTPTimeout = '', $SMTPDebug = '', $SMTPKeepAlive = '', $LocalMTAPath = '')
	{
	// Load send engine - Start
	Core::LoadObject('send_engine');
	SendEngine::SetEngine(MAIL_ENGINE, $SendMethod);
	SendEngine::SetSendMethod(true, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
	// Load send engine - End

	// Set contents and subject of the email - Start
	if (($HTMLBody != '') && ($PlainBody != ''))
		{
		$ContentType						= 'MultiPart';
		}
	elseif (($HTMLBody == '') && ($PlainBody != ''))
		{
		$ContentType						= 'Plain';
		}
	elseif (($HTMLBody != '') && ($PlainBody == ''))
		{
		$ContentType						= 'HTML';
		}
	// Set contents and subject of the email - End

	// Generate email properties - Start
	SendEngine::SetEmailProperties(
		SYSTEM_EMAIL_FROM_NAME,
		SYSTEM_EMAIL_FROM_EMAIL,
		$ToEmail,
		$ToEmail,
		SYSTEM_EMAIL_REPLYTO_NAME,
		SYSTEM_EMAIL_REPLYTO_EMAIL,
		array(),
		$ContentType,
		$Subject,
		$HTMLBody,
		$PlainBody,
		'System testing message',
		0,
		0,
		0,
		0,
		0
		);
	// Generate email properties - End

	// Send the email to the recipient - Start
	$ArrayResult = SendEngine::SendEmail(false);
	// Send the email to the recipient - End

	// Remove temporary image embedding files - Start
	SendEngine::RemoveTemporaryImageEmbeddingFiles();
	// Remove temporary image embedding files - End

	// Close the mailer connection - Start
	SendEngine::CloseConnections();
	// Close the mailer connection - End

	return $ArrayResult;
	}

/**
 * Sends preview email
 *
 * @param string $ToEmail
 * @param string $ArrayUser
 * @param string $ArrayCampaign
 * @param string $ArrayList
 * @param string $ArraySubscriber
 * @param string $ArrayEmail
 * @return void
 * @author Cem Hurturk
 */
public static function SendPreviewEmail($SendMethod, $ToEmail, $ArrayUser, $ArrayCampaign, $ArrayList, $ArraySubscriber, $ArrayEmail, $ReturnEmail = false, $DesignPreview = false, $SpamTest = false, $AddHeaderFooter = true)
	{
	// If fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchURL'] != '')
		{
		$ArrayEmail['HTMLContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false));
		$ArrayEmail['ContentType']		= 'HTML';
		}
	// If fetch URL is defined in email properties, fetch the URL and set contents - End

	// If plain fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchPlainURL'] != '')
		{
		$ArrayEmail['PlainContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false));
		$ArrayEmail['ContentType']		= ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
		}
	// If plain fetch URL is defined in email properties, fetch the URL and set contents - End

	// Load send engine - Start
	Core::LoadObject('send_engine');
	if ($SpamTest == true)
		{
		SendEngine::SetEngine(MAIL_ENGINE, 'SaveToDisk');
		SendEngine::SetSendMethod();
		}
	else
		{
		if ($ArrayUser['GroupInformation']['SendMethod'] == 'System')
			{
			SendEngine::SetEngine(MAIL_ENGINE, $SendMethod);
			SendEngine::SetSendMethod();
			}
		else
			{
			SendEngine::SetEngine(MAIL_ENGINE, $ArrayUser['GroupInformation']['SendMethod']);
			SendEngine::SetSendMethod(true, $ArrayUser['GroupInformation']['SendMethodSMTPHost'], $ArrayUser['GroupInformation']['SendMethodSMTPPort'],
				$ArrayUser['GroupInformation']['SendMethodSMTPSecure'],
				$ArrayUser['GroupInformation']['SendMethodSMTPAuth'],
				$ArrayUser['GroupInformation']['SendMethodSMTPUsername'],
				$ArrayUser['GroupInformation']['SendMethodSMTPPassword'],
				$ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'],
				$ArrayUser['GroupInformation']['SendMethodSMTPDebug'],
				$ArrayUser['GroupInformation']['SendMethodSMTPKeepAlive'],
				$ArrayUser['GroupInformation']['SendMethodLocalMTAPath'],
				$ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'],
				$ArrayUser['GroupInformation']['SendMethodPowerMTADir'],
				$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
				$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
				''
				);
			}
		}

	if ($ArrayUser['GroupInformation']['XMailer'] != '')
		{
		SendEngine::$XMailer = $ArrayUser['GroupInformation']['XMailer'];
		}
	// Load send engine - End

	// Set contents and subject of the email - Start
	if ($ArrayEmail['ContentType'] == 'HTML')
		{
		// HTML email
		$ContentType						= 'HTML';
		}
	elseif ($ArrayEmail['ContentType'] == 'Plain')
		{
		// Plain email
		$ContentType						= 'Plain';
		}
	elseif ($ArrayEmail['ContentType'] == 'Both')
		{
		// MultiPart email
		$ContentType						= 'MultiPart';
		}
	// Set contents and subject of the email - End

	// Set email attachments (if attached) and image embedding (if enabled) - Start
	$ArrayEmail['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
		$ContentType,
		$ArrayEmail['HTMLContent'],
		0,
		$ArrayEmail['EmailID'],
		$ArrayUser['UserID'],
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID'],
		Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $ArrayEmail['EmailID']), array('AttachmentID' => 'ASC')),
		($ArrayEmail['ImageEmbedding'] == 'Enabled' ? true : false)
		);
	// Set email attachments (if attached) and image embedding (if enabled) - End

	// Plug-in hook - Start
	$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], $ArraySubscriber));
		$TMPSubject		= $ArrayPlugInReturnVars[0];
		$TMPHTMLBody	= $ArrayPlugInReturnVars[1];
		$TMPPlainBody	= $ArrayPlugInReturnVars[2];
	// Plug-in hook - End

	// Plug-in hook - Start
	$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array($TMPSubject, $TMPHTMLBody, $TMPPlainBody, $ArraySubscriber, 'Preview'));
		$TMPSubject		= $ArrayPlugInReturnVars[0];
		$TMPHTMLBody	= $ArrayPlugInReturnVars[1];
		$TMPPlainBody	= $ArrayPlugInReturnVars[2];
	// Plug-in hook - End

	// Add header/footer to the email (if exists in the user group) - Start {
	if ($AddHeaderFooter)
		{
		$ArrayReturn = Personalization::AddEmailHeaderFooter($TMPPlainBody, $TMPHTMLBody, $ArrayUser['GroupInformation']);
			$TMPPlainBody	= $ArrayReturn[0];
			$TMPHTMLBody	= $ArrayReturn[1];
		}
	// Add header/footer to the email (if exists in the user group) - End }

	// Personalize email subject and contents - Start
	$TMPSubject			= Personalization::Personalize($TMPSubject, array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), true);

	if ($ArrayEmail['HTMLContent'] != '')
		{
		$TMPHTMLBody	= Personalization::Personalize($TMPHTMLBody, array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent', 'OptLinks'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), true);
		}
	if ($ArrayEmail['PlainContent'] != '')
		{
		$TMPPlainBody	= Personalization::Personalize($TMPPlainBody, array('Subscriber', 'Links', 'User', 'RemoteContent', 'OptLinks'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), true);
		}
	// Personalize email subject and contents - End

	// Generate the abuse message ID - Start
	$AbuseMessageID = EmailQueue::GenerateAbuseMessageID($ArrayCampaign['CampaignID'], $ArraySubscriber['SubscriberID'], $ArraySubscriber['EmailAddress'], $ArrayList['ListID'], $ArrayCampaign['RelOwnerUserID'], 0);
	// Generate the abuse message ID - End

	// Generate email properties - Start
	SendEngine::SetEmailProperties(
		$ArrayEmail['FromName'],
		$ArrayEmail['FromEmail'],
		$ToEmail,
		$ToEmail,
		$ArrayEmail['ReplyToName'],
		$ArrayEmail['ReplyToEmail'],
		array('X-PreviewEmail'=>'TRUE'),
		$ContentType,
		$TMPSubject,
		$TMPHTMLBody,
		$TMPPlainBody,
		$AbuseMessageID,
		$ArrayCampaign['CampaignID'],
		$ArrayEmail['EmailID'],
		$ArrayUser['UserID'],
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID']
		);
	// Generate email properties - End

	// Send the email to the recipient - Start
	if ($DesignPreview == false)
		{
		$ArrayResult = SendEngine::SendEmail(($ReturnEmail == true ? true : false));
		}
	// Send the email to the recipient - End

	// Remove temporary image embedding files - Start
	SendEngine::RemoveTemporaryImageEmbeddingFiles();
	// Remove temporary image embedding files - End

	// Close the mailer connection - Start
	SendEngine::CloseConnections();
	// Close the mailer connection - End

	if (($ReturnEmail == true) && ($DesignPreview == false))
		{
		return $ArrayResult;
		}
	else
		{
		return array($TMPSubject, $TMPHTMLBody, $TMPPlainBody);
		}
	}

/**
 * Sends opt-in confirmation request email
 *
 * This method is deprecated with v4.3.0
 *
 * @deprecated
 * @param string $ToEmail
 * @param string $ArrayList
 * @param string $ArraySubscriber
 * @param string $ArrayEmail
 * @return void
 * @author Cem Hurturk
 */
public static function SendOptInEmail($SendMethod, $ToEmail, $ArrayList, $ArraySubscriber, $ArrayEmail, $ReturnEmail = false, $DesignPreview = false, $ArrayUser = array())
	{
	Core::LoadObject('campaigns');

	// If fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchURL'] != '')
		{
		$ArrayEmail['HTMLContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('Subscriber', 'User'), $ArraySubscriber, array(), $ArrayList, array(), array(), false));
		$ArrayEmail['ContentType']		= 'HTML';
		}
	// If fetch URL is defined in email properties, fetch the URL and set contents - End

	// If plain fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchPlainURL'] != '')
		{
		$ArrayEmail['PlainContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), array(), false));
		$ArrayEmail['ContentType']		= ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
		}
	// If plain fetch URL is defined in email properties, fetch the URL and set contents - End

	// Load send engine - Start
	Core::LoadObject('send_engine');

	if ($ArrayUser['GroupInformation']['SendMethod'] == 'System')
		{
		SendEngine::SetEngine(MAIL_ENGINE, SEND_METHOD);
		SendEngine::SetSendMethod();
		}
	else
		{
		SendEngine::SetEngine(MAIL_ENGINE, $ArrayUser['GroupInformation']['SendMethod']);
		SendEngine::SetSendMethod(
			true,
			$ArrayUser['GroupInformation']['SendMethodSMTPHost'],
			$ArrayUser['GroupInformation']['SendMethodSMTPPort'],
			$ArrayUser['GroupInformation']['SendMethodSMTPSecure'],
			$ArrayUser['GroupInformation']['SendMethodSMTPAuth'],
			$ArrayUser['GroupInformation']['SendMethodSMTPUsername'],
			$ArrayUser['GroupInformation']['SendMethodSMTPPassword'],
			$ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'],
			$ArrayUser['GroupInformation']['SendMethodSMTPDebug'],
			$ArrayUser['GroupInformation']['SendMethodSMTPKeepAlive'],
			$ArrayUser['GroupInformation']['SendMethodLocalMTAPath'],
			$ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'],
			$ArrayUser['GroupInformation']['SendMethodPowerMTADir'],
			$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
			$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
			''
			);
		}

	if ($ArrayUser['GroupInformation']['XMailer'] != '')
		{
		SendEngine::$XMailer = $ArrayUser['GroupInformation']['XMailer'];
		}
	// Load send engine - End

	// Set contents and subject of the email - Start
	if ($ArrayEmail['ContentType'] == 'HTML')
		{
		// HTML email
		$ContentType						= 'HTML';
		}
	elseif ($ArrayEmail['ContentType'] == 'Plain')
		{
		// Plain email
		$ContentType						= 'Plain';
		}
	elseif ($ArrayEmail['ContentType'] == 'Both')
		{
		// MultiPart email
		$ContentType						= 'MultiPart';
		}
	// Set contents and subject of the email - End

	// Set email attachments (if attached) and image embedding (if enabled) - Start
	$ArrayEmail['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
		$ContentType,
		$ArrayEmail['HTMLContent'],
		0,
		$ArrayEmail['EmailID'],
		0,
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID'],
		$ArrayEmail['EmailID'] == 0 ? array() : Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $ArrayEmail['EmailID']), array('AttachmentID' => 'ASC')),
		($ArrayEmail['ImageEmbedding'] == 'Enabled' ? true : false)
		);
	// Set email attachments (if attached) and image embedding (if enabled) - End

	// Plug-in hook - Start
	$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], $ArraySubscriber));
		$TMPSubject		= $ArrayPlugInReturnVars[0];
		$TMPHTMLBody	= $ArrayPlugInReturnVars[1];
		$TMPPlainBody	= $ArrayPlugInReturnVars[2];
	// Plug-in hook - End

	// Plug-in hook - Start
	$ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array($ArrayEmail['Subject'], $ArrayEmail['HTMLContent'], $ArrayEmail['PlainContent'], $ArraySubscriber, 'Opt-In'));
		$TMPSubject		= $ArrayPlugInReturnVars[0];
		$TMPHTMLBody	= $ArrayPlugInReturnVars[1];
		$TMPPlainBody	= $ArrayPlugInReturnVars[2];
	// Plug-in hook - End

	// Personalize email subject and contents - Start
	$TMPSubject			= Personalization::Personalize($TMPSubject, array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), array(), false);

	if ($ArrayEmail['HTMLContent'] != '')
		{
		$TMPHTMLBody	= Personalization::Personalize($TMPHTMLBody, array('Subscriber', 'User', 'OptLinks', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), array(), false);
		}
	if ($ArrayEmail['PlainContent'] != '')
		{
		$TMPPlainBody	= Personalization::Personalize($TMPPlainBody, array('Subscriber', 'User', 'OptLinks', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), array(), false);
		}
	// Personalize email subject and contents - End

	// Generate the abuse message ID - Start
	$AbuseMessageID = EmailQueue::GenerateAbuseMessageID(0, $ArraySubscriber['SubscriberID'], $ArraySubscriber['EmailAddress'], $ArrayList['RelListID'], $ArrayList['RelOwnerUserID'], 0);
	// Generate the abuse message ID - End

	// Generate email properties - Start
	SendEngine::SetEmailProperties(
		$ArrayEmail['FromName'],
		$ArrayEmail['FromEmail'],
		$ToEmail,
		$ToEmail,
		$ArrayEmail['ReplyToName'],
		$ArrayEmail['ReplyToEmail'],
		array(),
		$ContentType,
		$TMPSubject,
		$TMPHTMLBody,
		$TMPPlainBody,
		$AbuseMessageID,
		0,
		$ArrayEmail['EmailID'],
		0,
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID']
		);
	// Generate email properties - End

	// Send the email to the recipient - Start
	$ArrayResult = SendEngine::SendEmail(false);
	// Send the email to the recipient - End

	// Remove temporary image embedding files - Start
	SendEngine::RemoveTemporaryImageEmbeddingFiles();
	// Remove temporary image embedding files - End

	// Close the mailer connection - Start
	SendEngine::CloseConnections();
	// Close the mailer connection - End

	return true;
	}

/**
 * Parses SpamAssassin Test Results
 *
 * @return array
 * @author Cem Hurturk
 **/
function ParseRawSPAMRating($Content, $OldStyle = false)
	{
	$ArrayNewContent = array();

	if ($OldStyle == true)
		{
		preg_match_all('/<Header>[\w\W]*<\/Header>/i', $Content, $ArrayMatches);
			$HeaderContent = $ArrayMatches[0][0];

		preg_match_all('/, score=(.*) required/i', $HeaderContent, $ArrayMatches);
			$Hits = $ArrayMatches[1][0];

		preg_match_all('/ required=(.*) /i', $HeaderContent, $ArrayMatches);
			$Required = $ArrayMatches[1][0];

		preg_match_all('/<Scores>[\w\W]*<\/Scores>/i', $Content, $ArrayMatches);
			$ScoreContent = $ArrayMatches[0][0];

		$ArrayNewContent['Hits']		= $Hits;
		$ArrayNewContent['Required']	= $Required;
		}

	$ArrayLines = explode("\n", $Content);

	$StartCapturing = false;
	$TMPCounter = 0;
	foreach ($ArrayLines as $EachLine)
		{
		$EachLine = trim($EachLine);

		if (substr($EachLine, 0, 13) == '---- --------')
			{
			$StartCapturing = true;
			continue;
			}

		if ($StartCapturing == true)
			{
			$EachPoint	= substr($EachLine, 0, 3);
			$EachCode	= substr($EachLine, 4, 23);
			$EachDesc	= substr($EachLine, 27, strlen($EachLine));

			if (($EachPoint == '') && ($EachCode == '') && ($EachDesc == ''))
				{
				$StartCapturing = false;
				continue;
				}
			if (($EachPoint == '0.0') && ($EachPoint == '-0.'))
				{
				continue;
				}

			$ArrayNewContent['Scores'][$TMPCounter]['Point']		= $EachPoint;
			$ArrayNewContent['Scores'][$TMPCounter]['Code']			= $EachCode;
			$ArrayNewContent['Scores'][$TMPCounter]['Description']	= $EachDesc;

			$TMPCounter++;
			}
		}

	return $ArrayNewContent;
	}

/**
 * Searches for the given tag in the content string
 *
 * @param string $Content Content string
 * @param string $Tag Tag to be searched
 * @return bool
 * @author Cem Hurturk
 **/
function DetectTagInContent($Content, $Tag)
	{
	if (preg_match('/'.$Tag.'/i', $Content) == false)
		{
		return false;
		}
	else
		{
		return true;
		}
	}

} // end of class Emails
?>
