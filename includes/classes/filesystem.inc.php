<?php
/**
 * File system engine for file processes
 *
 * @package default
 * @author Cem Hurturk
 **/
class FileSystemEngine
{
public static $Engine			= '';
public static $ObjectEngine		= '';
public static $Host				= '';
public static $Username			= '';
public static $Password			= '';
public static $Port				= '21';

public static function SetConnectionParameters($Host, $Username, $Password, $Port)
	{
	self::$Host			= $Host;
	self::$Username		= $Username;
	self::$Password		= $Password;
	self::$Port			= $Port;
	}

/**
 * Set the file system engine
 *
 * @param string $Engine 
 * @return void
 * @author Cem Hurturk
 */
public static function SetEngine($Engine = 'File')
	{
	if ($Engine == 'File')
		{
		self::$Engine = 'File';
		self::$ObjectEngine = new FileSystemEngine_File();
		}
	elseif ($Engine == 'FTP')
		{
		self::$Engine = 'FTP';
		self::$ObjectEngine = new FileSystemEngine_FTP();
		}
	}

/**
 * Compares the given md5 with the file md5 or if no md5 is given, it just returns the md5 of the target file
 *
 * @param string $FilePath 
 * @param string $MD5 
 * @return mixed
 * @author Cem Hurturk
 */
public static function CheckMD5($FilePath, $MD5 = '')
	{
	if (file_exists($FilePath) == false) return false;

	$FileMD5 = md5_file($FilePath, false);

	if ($MD5 == '') return $FileMD5;

	if ($MD5 != $FileMD5)
		{
		return false;
		}
	else
		{
		return true;
		}
	}

/**
 * Returns the list of specific MD5 and size checksum of file(s)
 *
 * @return void
 * @author Cem Hurturk
 */
public static function FileIntegrityList($File = '')
	{
	$ArrayIntegrityList = include APP_PATH.'/includes/libraries/flit.php';

	if ($File != '')
		{
		if (isset($ArrayIntegrityList[$File]) == true)
			{
			$Return = $ArrayIntegrityList[$File];
			unset($ArrayIntegrityList);
			return $Return;
			}
		else
			{
			unset($ArrayIntegrityList);
			return false;
			}			
		}
	else
		{
		return $ArrayIntegrityList;
		}
	}

/**
 * Recursively loops files in the provided directory
 *
 * @param string $Directory 
 * @param string $ArrayExcludedDirs 
 * @param string $ArrayMD5List 
 * @return void
 * @author Cem Hurturk
 */
public static function LoopFiles($RootDirectory, $Directory, $ArrayExcludedDirs, $ArrayCache, $ProcessType = 'MD5Test')
	{
	$DirHandler = opendir($Directory);
	while ($EachFile = readdir($DirHandler))
		{
		if (($EachFile != '.') && ($EachFile != '..') && ($EachFile != '.svn') && ($EachFile != '.DS_Store'))
			{
			if (is_file($Directory."/".$EachFile) == true)
				{
				if (in_array(str_replace($RootDirectory, '', $Directory."/".$EachFile), $ArrayExcludedDirs) == false)
					{
					$ArrayFileInfo = self::FileIntegrityList(str_replace($RootDirectory, '', $Directory."/".$EachFile));

					if (self::CheckMD5($Directory."/".$EachFile, $ArrayFileInfo[0]) == false)
						{
						$ArrayCache[str_replace($RootDirectory, '', $Directory."/".$EachFile)] = array(
																	'MD5'			=> self::CheckMD5($Directory.'/'.$EachFile, ''),
																	'ExpectedMD5'	=> $ArrayFileInfo[0],
																	);
						}
					}
				}
			elseif (is_dir($Directory."/".$EachFile) == true)
				{
				if (in_array(str_replace($RootDirectory, '', $Directory."/".$EachFile), $ArrayExcludedDirs) == false)
					{
					$NewDirectory = $Directory."/".$EachFile;
					$ArrayCache = self::LoopFiles($RootDirectory, $NewDirectory, $ArrayExcludedDirs, $ArrayCache);
					}
				}
			}
		}

	closedir($DirHandler);
	return $ArrayCache;
	}


} // END class FileSystemEngine
?>