<?php
/**
 * Tags class
 *
 * This class holds all tag related functions
 * @package Oempro
 * @author Octeth
 **/
class PublicArchives extends Core
{
/**
 * Required fields for a tag
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('RelOwnerUserID', 'Tag');	

/**
 * Generates a public campaign archive page url
 *
 * @param string $TagID 
 * @param string $TemplateURL 
 * @return string URL
 * @author Mert Hurturk
 */
public static function GenerateURL($TagID, $TemplateURL)
	{
	// Generate URL - Start
	$ArrayParameters = array(
		'TagID'			=>	$TagID,
		'TemplateURL'	=>	$TemplateURL
		);

	$EncryptedURLParameters = Core::EncryptURL($ArrayParameters);
	// Generate URL - End

	
	return APP_URL.'archive.php?a='.$EncryptedURLParameters;
	}

public static function GetParametersFromURL($EncryptedURL)
	{
	return Core::DecryptURL($EncryptedURL);
	}

} // END class Clients
?>