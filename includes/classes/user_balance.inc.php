<?php

class UserBalance extends Core {

    public function __construct() {
        
    }

    public static function CreateUserBalance($ArrayFieldAndValues) {

//        $ArrayFieldAndValues = array(
//            "RelUserID" => $data['user_id'],
//            "Balance" => $data['balance'],
//            "CreationDate" => $data['create_date'],
//        );

        $Result = Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX . 'users_balance');

        if (is_bool($Result) == true && $Result == false)
            return false;

        return $Result;
    }

    public static function updateUserBalance($ArrayFieldAndValues, $ArrayCriterias) {

//        $ArrayFieldnValues = array(
//            "Balance" => $data["balance"],
//            "UpdateDate" => $data["update_date"]
//        );
//        $ArrayCriterias = array("RelUserID" => $data["user_id"]);

        $Result = Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'users_balance');

        if (is_bool($Result) == true && $Result == false)
            return false;

        return $Result;
    }

    public static function getUserBalance($user_id) {

        $SQLQuery = "SELECT * FROM oempro_users_balance WHERE RelUserID= $user_id order by LogID desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $Result = array();

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_object($ResultSet)) {
                $Result[] = $EachRow;
            }
        } else {
            return false;
        }

        $row = $Result[0];
        $balance = $row->Balance;

        return $balance;
    }

    public static function checkBalanceExists($user_id) {

        $SQLQuery = "SELECT count(*) as count FROM oempro_users_balance WHERE RelUserID=$user_id";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $Result = array();

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_object($ResultSet)) {
                $Result[] = $EachRow;
            }
        } else {
            return false;
        }

        $row = $Result[0];
        $count = $row->count;

        $exists = $count == 0 ? false : true;

        return $exists;
    }

    public static function processUserBalance($ArrayUser, $ArrayPaymentPeriod, $ReturnBoolean = true) {

        $LogID = $ArrayPaymentPeriod["LogID"];

        //update invoice and check balance
        //get current user package service fee
        $service_fee = empty($ArrayUser['GroupInformation']['PaymentSystemChargeAmount']) ? 0 : $ArrayUser['GroupInformation']['PaymentSystemChargeAmount'];
        $current_balance = UserBalance::getUserBalance($ArrayUser['UserID']);
        $discount = 0;
        $new_balance = 0;
        $send_payment = true;

        if ($current_balance > $service_fee) {
            $discount = $service_fee;
            $new_balance = $current_balance - $discount;
            $send_payment = false;
        } elseif ($current_balance == $service_fee) {
            $discount = $service_fee;
            $new_balance = 0;
            $send_payment = false;
        } else {
            $discount = $current_balance;
            $new_balance = 0;
            $send_payment = true;
        }

        $check_bal_exists = UserBalance::checkBalanceExists($ArrayUser['UserID']);

        if ($check_bal_exists) {
            // if he has balance records,then update current balance
            $ArrayFieldnValues = array(
                "Balance" => $new_balance,
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => $ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFieldnValues, $ArrayCriterias);
        } else {
            // if user has no balance records. then create new record
            $ArrayFieldAndValues = array(
                "RelUserID" => $ArrayUser['UserID'],
                "Balance" => $new_balance,
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFieldAndValues);
        }

        $ReputationLevel = $send_payment ? 'Untrusted' : 'Trusted';
        //update user to be un-trusted untill he pays
        Core::LoadObject('users');
        Users::UpdateUser(array('ReputationLevel' => $ReputationLevel), array('UserID' => $ArrayUser["UserID"]));

        $PaymentStatus = $send_payment ? 'Unpaid' : 'Paid';
        //update invoice with service total if usergroup has service fees
        $sendreceipt = $send_payment ? "Yes" : "No";
        
        Core::LoadObject('api');
        $ArrayReturn = API::call(array(
                    'format' => 'json',
                    'command' => 'user.paymentperiods.update2',
                    'protected' => true,
                    'username' => $ArrayUser['Username'],
                    'password' => $ArrayUser['Password'],
                    'parameters' => array(
                        'userid' => $ArrayUser["UserID"],
                        'logid' => $LogID,
                        'discount' => $discount,
                        'includetax' => 'Exclude',
                        'paymentstatus' => $PaymentStatus,
                        'sendreceipt' => $sendreceipt,
                    )
        ));
        return $ReturnBoolean ? $PaymentStatus : $ArrayReturn;
    }

}
