<?php
$IsCLI = true;

// Include main module - Start
include_once(dirname(__FILE__).'/../../data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('user_auth');
Core::LoadObject('client_auth');
Core::LoadObject('admin_auth');
Core::LoadObject('subscriber_auth');
Core::LoadObject('users');
Core::LoadObject('clients');
Core::LoadObject('admins');
Core::LoadObject('subscribers');
Core::LoadObject('form');
Core::LoadObject('json');
// Load other modules - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'login');
// Load language - End

/**
 * Oempro API Class
 **/
class API
{

	public static $formats = array('json','xml','array','object');
	
	public static $commands = array(
		'user.login'					=> array('file_name' => 'user.login',						'authorization' => null,				'user_permissions' => null),
		'user.passwordremind'			=> array('file_name' => 'user.passwordremind',				'authorization' => null,				'user_permissions' => null),
		'user.passwordreset'			=> array('file_name' => 'user.passwordreset',				'authorization' => null,				'user_permissions' => null),
		'user.update'					=> array('file_name' => 'user.update',						'authorization' => 'admin,user',		'user_permissions' => 'User.Update'),
		'user.create'					=> array('file_name' => 'user.create',						'authorization' => 'admin',				'user_permissions' => null),
		'users.get'						=> array('file_name' => 'users.get',						'authorization' => 'admin',				'user_permissions' => null),
		'user.get'						=> array('file_name' => 'user.get',							'authorization' => 'admin',				'user_permissions' => null),
		'user.snapshot'					=> array('file_name' => 'user.snapshot',					'authorization' => 'admin,user',		'user_permissions' => null),
		'user.addcredits'				=> array('file_name' => 'user.addcredits',					'authorization' => 'admin',				'user_permissions' => null),
		'users.delete'					=> array('file_name' => 'users.delete',						'authorization' => 'admin',				'user_permissions' => null),
		'user.switch'					=> array('file_name' => 'user.switch',						'authorization' => 'admin',				'user_permissions' => null),
		'user.paymentperiods'			=> array('file_name' => 'user.paymentperiods',				'authorization' => 'admin',				'user_permissions' => null),
		'user.paymentperiods.update'	=> array('file_name' => 'user.paymentperiods_update',		'authorization' => 'admin',				'user_permissions' => null),
		'user.paymentperiods.update2'	=> array('file_name' => 'user.paymentperiods_update2',		'authorization' => 'user',				),/*Eman 'user_permissions' => null , 'authorization' => 'user'*/
		'admin.login'					=> array('file_name' => 'admin.login',						'authorization' => null,				'user_permissions' => null),
		'admin.passwordremind'			=> array('file_name' => 'admin.passwordremind',				'authorization' => null,				'user_permissions' => null),
		'admin.passwordreset'			=> array('file_name' => 'admin.passwordreset',				'authorization' => null,				'user_permissions' => null),
		'admin.update'					=> array('file_name' => 'admin.update',						'authorization' => 'admin',				'user_permissions' => null),
		'settings.update'				=> array('file_name' => 'settings.update',					'authorization' => 'admin',				'user_permissions' => null),
		'settings.emailsendingtest'		=> array('file_name' => 'settings.emailsendingtest',		'authorization' => 'admin',				'user_permissions' => null),
		'theme.create'					=> array('file_name' => 'theme.create',						'authorization' => 'admin',				'user_permissions' => null),
		'theme.get'						=> array('file_name' => 'theme.get',						'authorization' => 'admin',				'user_permissions' => null),
		'theme.update'					=> array('file_name' => 'theme.update',						'authorization' => 'admin',				'user_permissions' => null),
		'theme.delete'					=> array('file_name' => 'theme.delete',						'authorization' => 'admin',				'user_permissions' => null),
		'themes.get'					=> array('file_name' => 'themes.get',						'authorization' => 'admin',				'user_permissions' => null),
		'usergroup.create'				=> array('file_name' => 'usergroup.create',					'authorization' => 'admin',				'user_permissions' => null),
		'usergroup.update'				=> array('file_name' => 'usergroup.update',					'authorization' => 'admin',				'user_permissions' => null),
		'usergroup.get'					=> array('file_name' => 'usergroup.get',					'authorization' => 'admin',				'user_permissions' => null),
		'usergroup.delete'				=> array('file_name' => 'usergroup.delete',					'authorization' => 'admin',				'user_permissions' => null),
		'usergroup.duplicate'			=> array('file_name' => 'usergroup.duplicate',				'authorization' => 'admin',				'user_permissions' => null),
		'usergroups.get'				=> array('file_name' => 'usergroups.get',					'authorization' => 'admin',				'user_permissions' => null),
		'attachment.delete'				=> array('file_name' => 'attachment.delete',				'authorization' => 'user',				'user_permissions' => null),
		'email.create'					=> array('file_name' => 'email.create',						'authorization' => 'user',				'user_permissions' => null),
		'email.duplicate'				=> array('file_name' => 'email.duplicate',					'authorization' => 'user',				'user_permissions' => null),
		'email.update'					=> array('file_name' => 'email.update',						'authorization' => 'user',				'user_permissions' => null),
		'emails.get'					=> array('file_name' => 'emails.get',						'authorization' => 'user',				'user_permissions' => null),
		'email.get'						=> array('file_name' => 'email.get',						'authorization' => 'user',				'user_permissions' => null),
		'email.delete'					=> array('file_name' => 'email.delete',						'authorization' => 'user',				'user_permissions' => null),
		'email.personalizationtags'		=> array('file_name' => 'email.personalizationtags',		'authorization' => 'user',				'user_permissions' => null),
		'email.emailpreview'			=> array('file_name' => 'email.emailpreview',				'authorization' => 'user',				'user_permissions' => null),
		'email.spamtest'				=> array('file_name' => 'email.spamtest',					'authorization' => 'user',				'user_permissions' => 'Email.SpamTest'),
		'email.designpreview.create'	=> array('file_name' => 'email.designpreviewcreate',		'authorization' => 'user',				'user_permissions' => 'Email.DesignPreview'),
		'email.designpreview.delete'	=> array('file_name' => 'email.designpreviewdelete',		'authorization' => 'user',				'user_permissions' => 'Email.DesignPreview'),
		'email.designpreview.getlist'	=> array('file_name' => 'email.designpreviewlist',			'authorization' => 'user',				'user_permissions' => 'Email.DesignPreview'),
		'email.designpreview.details'	=> array('file_name' => 'email.designpreviewdetails',		'authorization' => 'user',				'user_permissions' => 'Email.DesignPreview'),
		'email.template.create'			=> array('file_name' => 'email.templatecreate',				'authorization' => 'admin,user',		'user_permissions' => 'EmailTemplates.Manage'),
		'email.templates.get'			=> array('file_name' => 'email.templatesget',				'authorization' => 'admin,user',		'user_permissions' => 'EmailTemplates.Manage'),
		'email.template.get'			=> array('file_name' => 'email.templateget',				'authorization' => 'admin,user',		'user_permissions' => 'EmailTemplates.Manage'),
		'email.template.update'			=> array('file_name' => 'email.templateupdate',				'authorization' => 'admin,user',		'user_permissions' => 'EmailTemplates.Manage'),
		'email.template.delete'			=> array('file_name' => 'email.templatedelete',				'authorization' => 'admin,user',		'user_permissions' => 'EmailTemplates.Manage'),
		'email.delivery.test'			=> array('file_name' => 'email.delivery.test',				'authorization' => 'admin',				'user_permissions' => null),
		'campaign.create'				=> array('file_name' => 'campaign.create',					'authorization' => 'user',				'user_permissions' => 'Campaign.Create'),
		'campaign.copy'					=> array('file_name' => 'campaign.copy',					'authorization' => 'user',				'user_permissions' => 'Campaign.Create'),
		'campaign.update'				=> array('file_name' => 'campaign.update',					'authorization' => 'user',				'user_permissions' => 'Campaign.Update'),
		'campaigns.get'					=> array('file_name' => 'campaigns.get',					'authorization' => 'user',				'user_permissions' => 'Campaigns.Get'),
		'campaign.get'					=> array('file_name' => 'campaign.get',						'authorization' => 'user',				'user_permissions' => 'Campaign.Get'),
		'campaign.pause'				=> array('file_name' => 'campaign.pause',					'authorization' => 'user',				'user_permissions' => 'Campaign.Update'),
		'campaign.resume'				=> array('file_name' => 'campaign.resume',					'authorization' => 'user',				'user_permissions' => 'Campaign.Update'),
		'campaign.cancel'				=> array('file_name' => 'campaign.cancel',					'authorization' => 'user',				'user_permissions' => 'Campaign.Update'),
		'campaigns.delete'				=> array('file_name' => 'campaigns.delete',					'authorization' => 'user',				'user_permissions' => 'Campaign.Delete'),
		'campaigns.archive.geturl'		=> array('file_name' => 'campaigns.archive.geturl',			'authorization' => 'user',				'user_permissions' => 'Campaigns.Get'),
		'list.create'					=> array('file_name' => 'list.create',						'authorization' => 'user',				'user_permissions' => 'List.Create'),
		'list.update'					=> array('file_name' => 'list.update',						'authorization' => 'user',				'user_permissions' => 'List.Update'),
		'lists.delete'					=> array('file_name' => 'lists.delete',						'authorization' => 'user',				'user_permissions' => 'List.Delete'),
		'lists.get'						=> array('file_name' => 'lists.get',						'authorization' => 'user',				'user_permissions' => 'Lists.Get'),
		'list.get'						=> array('file_name' => 'list.get',							'authorization' => 'user',				'user_permissions' => 'List.Get'),
		'customfield.create'			=> array('file_name' => 'customfield.create',				'authorization' => 'user',				'user_permissions' => 'CustomField.Create'),
		'customfield.update'			=> array('file_name' => 'customfield.update',				'authorization' => 'user',				'user_permissions' => 'CustomField.Update'),
		'customfields.get'				=> array('file_name' => 'customfields.get',					'authorization' => 'user',				'user_permissions' => 'CustomFields.Get'),
		'customfields.copy'				=> array('file_name' => 'customfields.copy',				'authorization' => 'user',				'user_permissions' => 'CustomField.Create'),
		'customfields.delete'			=> array('file_name' => 'customfields.delete',				'authorization' => 'user',				'user_permissions' => 'CustomFields.Delete'),
		'listintegration.geturls'		=> array('file_name' => 'listintegration.geturls',			'authorization' => 'user',				'user_permissions' => 'List.Get'),
		'listintegration.addurl'		=> array('file_name' => 'listintegration.addurl',			'authorization' => 'user',				'user_permissions' => 'List.Update'),
		'listintegration.deleteurls'	=> array('file_name' => 'listintegration.deleteurls',		'authorization' => 'user',				'user_permissions' => 'List.Update'),
		'listintegration.testurl'		=> array('file_name' => 'listintegration.testurl',			'authorization' => 'user',				'user_permissions' => 'List.Update'),
		'listintegration.generatesubscriptionformhtmlcode'		=> array('file_name' => 'listintegration.generatesubscriptionformhtmlcode',			'authorization' => 'user',			'user_permissions' => 'List.Get'),
		'listintegration.generateunsubscriptionformhtmlcode'	=> array('file_name' => 'listintegration.generateunsubscriptionformhtmlcode',		'authorization' => 'user',			'user_permissions' => 'List.Get'),
		'media.upload'					=> array('file_name' => 'medialibrary.upload',				'authorization' => 'user',				'user_permissions' => 'Media.Upload'),
		'media.browse'					=> array('file_name' => 'medialibrary.browse',				'authorization' => 'user',				'user_permissions' => 'Media.Browse'),
		'media.retrieve'				=> array('file_name' => 'medialibrary.retrieve',			'authorization' => 'user',				'user_permissions' => 'Media.Retrieve'),
		'media.delete'					=> array('file_name' => 'medialibrary.delete',				'authorization' => 'admin,user',		'user_permissions' => 'Media.Delete'),
		'media.foldercreate'			=> array('file_name' => 'medialibrary.foldercreate',		'authorization' => 'user',				'user_permissions' => 'Media.Upload'),
		'media.folderdelete'			=> array('file_name' => 'medialibrary.folderdelete',		'authorization' => 'user',				'user_permissions' => 'Media.Delete'),
		'autoresponders.get'			=> array('file_name' => 'autoresponders.get',				'authorization' => 'user',				'user_permissions' => 'AutoResponders.Get'),
		'autoresponder.get'				=> array('file_name' => 'autoresponder.get',				'authorization' => 'user',				'user_permissions' => 'AutoResponder.Get'),
		'autoresponders.delete'			=> array('file_name' => 'autoresponders.delete',			'authorization' => 'user',				'user_permissions' => 'AutoResponders.Delete'),
		'autoresponders.copy'			=> array('file_name' => 'autoresponders.copy',				'authorization' => 'user',				'user_permissions' => 'AutoResponder.Create'),
		'autoresponder.update'			=> array('file_name' => 'autoresponder.update',				'authorization' => 'user',				'user_permissions' => 'AutoResponder.Update'),
		'autoresponder.create'			=> array('file_name' => 'autoresponder.create',				'authorization' => 'user',				'user_permissions' => 'AutoResponder.Create'),
		'segments.get'					=> array('file_name' => 'segments.get',						'authorization' => 'user',				'user_permissions' => 'Segments.Get'),
		'segments.delete'				=> array('file_name' => 'segments.delete',					'authorization' => 'user',				'user_permissions' => 'Segments.Delete'),
		'segments.copy'					=> array('file_name' => 'segments.copy',					'authorization' => 'user',				'user_permissions' => 'Segment.Create'),
		'segment.update'				=> array('file_name' => 'segment.update',					'authorization' => 'user',				'user_permissions' => 'Segment.Update'),
		'segment.create'				=> array('file_name' => 'segment.create',					'authorization' => 'user',				'user_permissions' => 'Segment.Create'),
		'subscribers.get'				=> array('file_name' => 'subscribers.get',					'authorization' => 'user',				'user_permissions' => 'Subscribers.Get'),
		'subscribers.search'			=> array('file_name' => 'subscribers.search',				'authorization' => 'user',				'user_permissions' => 'Subscribers.Get'),
		'subscriber.update'				=> array('file_name' => 'subscriber.update',				'authorization' => 'user,subscriber',	'user_permissions' => 'Subscriber.Update'),
		'subscriber.getlists'			=> array('file_name' => 'subscriber.getlists',				'authorization' => 'subscriber',		'user_permissions' => null),
		'subscriber.get'				=> array('file_name' => 'subscriber.get',					'authorization' => 'user,subscriber',	'user_permissions' => 'Subscribers.Get'),
		'subscriber.login'				=> array('file_name' => 'subscriber.login',					'authorization' => null,				'user_permissions' => null),
		'subscriber.subscribe'			=> array('file_name' => 'subscriber.subscribe',				'authorization' => null,				'user_permissions' => null),
		'subscriber.unsubscribe'		=> array('file_name' => 'subscriber.unsubscribe',			'authorization' => null,				'user_permissions' => null),
		'subscriber.optin'				=> array('file_name' => 'subscriber.optin',					'authorization' => null,				'user_permissions' => null),
		'subscribers.delete'			=> array('file_name' => 'subscribers.delete',				'authorization' => 'user',				'user_permissions' => 'Subscribers.Delete'),
		'subscribers.search'			=> array('file_name' => 'subscribers.search',				'authorization' => 'user',				'user_permissions' => 'Subscribers.Get'),
		'subscribers.import'			=> array('file_name' => 'subscribers.import',				'authorization' => 'user',				'user_permissions' => 'Subscribers.Import'),
		'clients.get'					=> array('file_name' => 'clients.get',						'authorization' => 'user',				'user_permissions' => 'Clients.Get'),
		'clients.delete'				=> array('file_name' => 'clients.delete',					'authorization' => 'user',				'user_permissions' => 'Clients.Delete'),
		'client.create'					=> array('file_name' => 'client.create',					'authorization' => 'user',				'user_permissions' => 'Client.Create'),
		'client.update'					=> array('file_name' => 'client.update',					'authorization' => 'user,client',		'user_permissions' => 'Client.Update'),
		'client.assignsubscriberlists'	=> array('file_name' => 'client.assignsubscriberlists',		'authorization' => 'user',				'user_permissions' => 'Client.Create'),
		'client.assigncampaigns'		=> array('file_name' => 'client.assigncampaigns',			'authorization' => 'user',				'user_permissions' => 'Client.Create'),
		'client.assigncampaigns'		=> array('file_name' => 'client.assigncampaigns',			'authorization' => 'user',				'user_permissions' => 'Client.Create'),
		'client.login'					=> array('file_name' => 'client.login',						'authorization' => null,				'user_permissions' => null),
		'client.passwordremind'			=> array('file_name' => 'client.passwordremind',			'authorization' => null,				'user_permissions' => null),
		'client.passwordreset'			=> array('file_name' => 'client.passwordreset',				'authorization' => null,				'user_permissions' => null),
		'client.lists.get'				=> array('file_name' => 'client.lists.get',					'authorization' => 'client',			'user_permissions' => null),
		'client.list.get'				=> array('file_name' => 'client.list.get',					'authorization' => 'client',			'user_permissions' => null),
		'client.campaigns.get'			=> array('file_name' => 'client.campaigns.get',				'authorization' => 'client',			'user_permissions' => null),
		'client.campaign.get'			=> array('file_name' => 'client.campaign.get',				'authorization' => 'client',			'user_permissions' => null),
		'tags.get'						=> array('file_name' => 'tags.get',							'authorization' => 'user',				'user_permissions' => null),
		'tag.create'					=> array('file_name' => 'tag.create',						'authorization' => 'user',				'user_permissions' => null),
		'tag.update'					=> array('file_name' => 'tag.update',						'authorization' => 'user',				'user_permissions' => null),
		'tags.delete'					=> array('file_name' => 'tags.delete',						'authorization' => 'user',				'user_permissions' => null),
		'tag.assigntocampaigns'			=> array('file_name' => 'tag.assigntocampaigns',			'authorization' => 'user',				'user_permissions' => 'Campaigns.Get'),
		'tag.unassignfromcampaigns'		=> array('file_name' => 'tag.unassignfromcampaigns',		'authorization' => 'user',				'user_permissions' => 'Campaigns.Get'),
		'splittest.create'				=> array('file_name' => 'splittest.create',					'authorization' => 'user',				'user_permissions' => 'Campaign.Create')
		);
	
	/**
	 * Every call to API is done via this function
	 *
	 * Parameters
	 * ==========
	 * protected		boolean					If set to false, password will be secured (md5) otherwise used as is. Default is false.
	 * username			string					Required if access is user, admin or client
	 * password			string					Required if access is user, admin or client
	 * command			string					Name of the api command to be called. If the command cannot be found, 
	 *											error returned.
	 * parameters		array					Parameters to be passed to api command.
	 * format			enum					Response format
	 * access			enum	(optional)		For api commands that support multiple authentication, access will 
	 *											define which authentication to be checked. If not set, first authentication 
	 *											defined in commands array will be set as access. 
	 *											VALUES:
	 *												- user
	 *												- admin
	 *												- client
	 *												- subscriber
	 * jsonp_callback	string	(optional)		If set, a javascript function (function name defined in this parameter) 
	 *											will be called and json object will be past as a parameter to this function
	 * 
	 * IMPORTANT:
	 * username, password, emailaddress and listid 
	 * parameters depend heavily on access parameter. 
	 * For more information, check access parameter 
	 * information.
	 *
	 */
	public static function call($parameters)
		{
		// Parameter validations - Start {
			// format validation - Start {
			if (!isset($parameters['format']) || $parameters['format'] == '' || !in_array(strtolower($parameters['format']), self::$formats))
				{
				self::_error(array(
					'format'		=>	'array',
					'errors'		=> array(
						'code'		=>	99997,
						'message'	=>	'Format is not valid'
						)
					));
				}
			// format validation - End }
			// command validation - Start {
			if (is_null($parameters['command']) || $parameters['command'] == '')
				{
				self::_error(array(
					'format'		=>	$parameters['format'],
					'errors'		=> array(
						'code'		=>	99996,
						'message'	=>	'Command not found'
						)
					));
				}
			// command validation - End }
		// Parameter validations - End }

		include TEMPLATE_PATH.'languages/'.strtolower(LANGUAGE).'/'.strtolower(LANGUAGE).'.inc.php';

		$command_name = strtolower($parameters['command']);
		$parameters['jsonp_callback'] = is_null($parameters['jsonp_callback']) ? '' : $parameters['jsonp_callback'];
		$parameters['protected'] = is_null($parameters['protected']) ? false : $parameters['protected'];

		// If command is not found in api class, listen for plug-in api hooks - Start {
		$isPluginAPI = false;
		$pluginAPI = null;
		$pluginCallable = null;
		if (array_key_exists($command_name, self::$commands) == false)
			{
			$pluginAPI = Plugins::HookListener('Action', 'Api.Plugin.Extend', array($command_name));
			if (count($pluginAPI) > 0) 
				{
				$pluginAPI = $pluginAPI[0];
				$pluginCallable = $pluginAPI['PluginCode'].'::'.$pluginAPI['Method'];
				if (isset($pluginAPI['PluginCode']) && isset($pluginAPI['Method']) && isset($pluginAPI['For']) && is_callable($pluginCallable))
					{
					$isPluginAPI = true;
					$command_authentications = explode(',', $pluginAPI['For']);
					if (count($command_authentications) == 1 && $command_authentications[0] == '') $command_authentications = array();
					$command_user_permissions = array();
					}
				}

			if ($isPluginAPI == false) throw new Exception('Invalid command');
			}
		// If command is not found in api class, listen for plug-in api hooks - End }

		// If command is found in api class, include api command file - Start {
		else
			{
			$command_file_name = self::$commands[$command_name]['file_name'];
			$command_authentications = (is_null(self::$commands[$command_name]['authorization']) ? array() : explode(',', self::$commands[$command_name]['authorization']));
			$command_user_permissions = (is_null(self::$commands[$command_name]['user_permissions']) ? array() : explode(',', self::$commands[$command_name]['user_permissions']));
			}
		// If command is found in api class, include api command file - End }

			// Check authentication - Start {
			if (count($command_authentications) < 1)
				{
				$authentication = true;
				$authentication_type = '';
				}
			elseif (count($command_authentications) == 1)
				{
				$authentication = false;
				$authentication_type = $command_authentications[0];
				}
			else
				{
				$authentication = false;
				if (is_null($parameters['access']))
					{
					$authentication_type = $command_authentications[0];
					}
				else
					{
					$authentication_type = $parameters['access'];
					}
				}

				// Get user information - Start {
				if ($authentication_type == 'user')
					{
					$ArrayUserInformation = Users::RetrieveUser(array('*'), array('Username'=>$parameters['username'], 'Password'=>($parameters['protected'] ? $parameters['password'] : md5(OEMPRO_PASSWORD_SALT.$parameters['password'].OEMPRO_PASSWORD_SALT)), 'AccountStatus'=>'Enabled'));
					if ($ArrayUserInformation != false) $authentication = true;
					}
				elseif ($authentication_type == 'admin')
					{
					$ArrayAdminInformation = Admins::RetrieveAdmin(array('*'), array('Username'=>$parameters['username'], 'Password'=>($parameters['protected'] ? $parameters['password'] : md5(OEMPRO_PASSWORD_SALT.$parameters['password'].OEMPRO_PASSWORD_SALT))));
					if ($ArrayAdminInformation != false) $authentication = true;
					}
				elseif ($authentication_type == 'client')
					{
					$ArrayClientInformation = Clients::RetrieveClient(array('*'), array('ClientUsername'=>$parameters['username'], 'ClientPassword'=>$parameters['password']));
					if ($ArrayClientInformation != false) $authentication = true;
					}
				elseif ($authentication_type == 'subscriber')
					{
					$ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('CONCAT(MD5(`SubscriberID`), MD5(`EmailAddress`))' => $_SESSION[SESSION_NAME]['SubscriberLogin']['Salt']), $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']);
					if ($ArraySubscriberInformation != false) $authentication = true;
					}
				// Get user information - End }
			// Check authentication - End }

			// If authentication fails, return error - Start {
			if ($authentication == false)
				{
				return self::_error(array(
					'format'		=>	$parameters['format'],
					'errors'		=>	array(
						'code'		=>	99998,
						'message'	=>	'Authentication failure or session expired'
						)
					));
				}
			// If authentication fails, return error - End }

			else
				{
				// If authentication type is user, check for permissions - Start {
				if ($authentication_type == 'user' && count($command_user_permissions) > 0)
					{
					if (Users::HasPermissions($command_user_permissions, $ArrayUserInformation['GroupInformation']['Permissions']) == false)
						{
						self::_error(array(
							'format'		=>	$parameters['format'],
							'errors'		=>	array(
								'code'		=>	99999,
								'message'	=>	'Not enough privileges'
								)
							));
						}
					}
				// If authentication type is user, check for permissions - End }
				
				try
					{
					// Initialize variables - Start
					$ArrayOutput = array();
					// Initialize variables - End
					
					// lowercase all keys - Start {
					$ArrayAPIData = array();
					$ArrayAPIData['AuthenticationType'] = $authentication_type;
					foreach ($parameters['parameters'] as $Key=>$Value)
						{
						$ArrayAPIData[strtolower($Key)] = $Value;
						}
					// lowercase all keys - End }

					if ($isPluginAPI == false)
						{
						// Include api command file - Start {
						include Core::GetAPIPath($command_file_name);
						// Include api command file - End }
						}
					else
						{
						// Run plugin api command - Start {	
						$ArrayOutput = call_user_func($pluginCallable, $ArrayAPIData);
						// Run plugin api command - End }
						}
					}
				catch (Exception $e)
					{
					if ($e->GetMessage() != '')
						{
						die($e->GetMessage());
						}
					}
				return self::_return(array(
					'format'			=>	$parameters['format'],
					'data'				=>	$ArrayOutput,
					'jsonp_callback'	=>	$parameters['jsonp_callback']
					));
				}
		}
	
	/**
	 * Displays given error messages
	 * 
	 * Parameters
	 * ==========
	 * format		enum		Response format
	 * errors		array		Error messages that will be returned
	 * 							-- Parameters:
	 * 								code		integer			Error code
	 * 								message		string			Error message
	 * 
	 */	
	private function _error($parameters)
		{
		// Parameter validations - Start {
		if (!is_array($parameters) || count($parameters) < 1) return;
		// Parameter validations - End }

		if (is_null($parameters['format'])) $parameters['format'] = 'array';

		$error_output = '';
		$array_error_data = array();
		$array_error_data['success'] = 'false';
		$array_error_data['errors'] = array();

		// Prepare error data - Start {
			// If multiple error messages are given - Start {
			if (is_array($parameters['errors'][0]))
				{
				$array_error_data['errors'] = $parameters['errors'][0];
				}
			// If multiple error messages are given - End }
			// If a single error message is given - Start {
			else
				{
				$array_error_data['errors'] = array($parameters['errors']);
				}
			// If a single error message given - End }
		// Prepare error data - End }

		// Return the result - Start {
		return self::_return(array(
			'format'	=>	$parameters['format'],
			'data'		=>	$array_error_data
			));
		// Return the result - End }
		}
		
	/**
	 * Returns given data
	 * 
	 * Parameters
	 * ==========
	 * format			enum		Response format
	 * data				array		An array containing the data that will be returned
	 * jsonp_callback	string		If set, a javascript function (function name defined in this parameter) will be called 
	 *								and json object will be past as a parameter to this function
	 * 
	 */	
	private function _return($parameters)
		{
		// Parameter validations - Start {
		if (!is_array($parameters) || count($parameters) < 1) return;
		if (is_null($parameters['data']) || count($parameters['data']) < 1) return;
		if (is_null($parameters['format'])) $parameters['format'] = 'array';
		if (!in_array($parameters['format'], self::$formats)) $parameters['format'] = 'array';
		if (is_null($parameters['jsonp_callback'])) $parameters['jsonp_callback'] = '';
		// Parameter validations - End }
		
		switch ($parameters['format'])
			{
			case 'xml':
				return '<?xml version="1.0" encoding="utf-8"?'.'>'.self::_array_to_xml(array('array' => $parameters['data'], 'wrap_with_node' => 'response'));
				break;
			case 'json':
				return self::_array_to_json(array('array' => $parameters['data'], 'jsonp_callback' => $parameters['jsonp_callback']));
				break;
			case 'array':
				return $parameters['data'];
				break;
			case 'object':
				return self::_array_to_object(array('array' => $parameters['data']));
				break;
			}
		}
	
	/**
	 * Converts an array to xml string. Supports nested associative arrays
	 * 
	 * Parameters
	 * ==========
	 * array			array		array to be converted to xml string
	 * wrap_with_node	string		If set, all nodes will be wrapped with given node
	 * 
	 */	
	private function _array_to_xml($parameters)
		{
		// Parameter validations - Start {
		if (!is_array($parameters['array'])) return;
		// Parameter validations - End }
		
		$array = $parameters['array'];
		$string_xml = (is_null($parameters['string_xml']) ? '' : $parameters['string_xml']);
		$wrap_with_node = (isset($parameters['wrap_with_node']) ? $parameters['wrap_with_node'] : null);
		
		if (!is_null($wrap_with_node))
			{
			$string_xml .= '<'.$wrap_with_node.'>';
			}
		
		foreach ($array as $key=>$value)
			{
			if (strtolower(gettype($value)) == 'array')
				{
				if (is_numeric($key) == true) $key = 'node_'.$key;
				$string_xml = self::_array_to_xml(array(
					'string_xml'		=>	$string_xml,
					'array'				=>	$value,
					'wrap_with_node'	=>	$key
					));
				}
			else
				{
				if (is_numeric($key) == true) $key = 'node_'.$key;
				$string_xml .= '<'.$key.'><![CDATA['.$value.']]></'.$key.'>';
				}
			}
		if (!is_null($wrap_with_node))
			{
			$string_xml .= '</'.$wrap_with_node.'>';
			}
		return $string_xml;
		}
	
	/**
	 * Converts an array to json string. Supports nested associative arrays
	 * 
	 * Parameters
	 * ==========
	 * array			array		array to be converted to javascript string
	 * jsonp_callback	string		If set, a javascript function (function name defined in this parameter) will be called 
	 *								and json object will be past as a parameter to this function
	 * 
	 */	
	private function _array_to_json($parameters)
		{
		// Parameter validations - Start {
		if (!is_array($parameters['array'])) return;
		// Parameter validations - End }

		$string_json = '';
		$object_json = new Services_JSON();
		if ($parameters['jsonp_callback'] != '')
			{
			$string_json = $parameters['jsonp_callback'].'('.$object_json->encode($parameters['array']).');';
			}
		else
			{
			$string_json =  $object_json->encode($parameters['array']);
			}
			
		return $string_json;
		}

	/**
	 * Converts an array to object. Supports nested associative arrays
	 * 
	 * Parameters
	 * ==========
	 * array			array		array to be converted to object
	 * 
	 */	
	private function _array_to_object($parameters)
	{
		$emptyCounter = 0;
		
		// Parameter validations - Start {
		if (!is_array($parameters['array'])) return $parameters['array'];
		// Parameter validations - End }

		$object = new stdClass();

		foreach ($parameters['array'] as $key=>$value)
		{
			// NOTE: API libraries return ErrorCode parameter as mixed (array or string). To make ErrorCode traversible, if key is ErrorCode, the value is not converted to an object.
			if ($key === 'ErrorCode')
			{
				$object->$key = $value;
			}
			else
			{
				if ($key == '') {
					$key = 'emptykey'.($emptyCounter++);
				}

				if (is_array($value))
					{
					$object->$key = self::_array_to_object(array('array' => $value));
					}
				else
					{
					$object->$key = $value;
					}
			}
		}
		return $object;
	}
			
} // END class API

?>