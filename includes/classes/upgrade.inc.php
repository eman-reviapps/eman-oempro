<?php
/**
 * Upgrade class
 *
 * This class holds all installation related functions
 * @package Oempro
 * @author Octeth
 **/
class Upgrade extends Core
{
/**
 * Stores the occurred database errors
 *
 * @author Cem Hurturk
 */
public static $ArrayDatabaseErrors = array();

/**
 * Upgrade path
 *
 * @author Cem Hurturk
 */
public static $ArrayUpgradePathInfo = array('4.0.0', '4.0.1', '4.0.2', '4.0.3', '4.0.4', '4.0.5', '4.0.6', '4.0.7',
	'4.1.0', '4.1.03', '4.1.09', '4.1.10', '4.1.11', '4.1.12', '4.1.13', '4.1.14', '4.1.15', '4.1.16', '4.1.17',
	'4.1.18', '4.1.19', '4.2.0', '4.2.1', '4.2.2', '4.2.3', '4.2.4', '4.3.0', '4.3.1', '4.3.2', '4.3.3', '4.3.4',
	'4.3.5', '4.4.0', '4.4.1', '4.5.0', '4.5.1', '4.5.2', '4.5.3', '4.6.0', '4.6.1', '4.6.2', '4.6.3', '4.6.4',
	'4.7.0');



/**
 * Upgrade from v4.0.0 to v4.0.1
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_401()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "DROP TABLE IF EXISTS `oempro_templates_users`;";
	$ArraySQLQueries[] = "DROP TABLE IF EXISTS `oempro_options`;";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_options` (`OptionName` varchar(250) collate utf8_unicode_ci NOT NULL, `OptionValue` text collate utf8_unicode_ci NOT NULL, KEY `OptionName` (`OptionName`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_emails` CHANGE `PlainContent` `PlainContent` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, CHANGE `HTMLContent` `HTMLContent` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_templates` DROP `TemplateVisibility`, DROP `TemplateTags`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_templates` ADD `TemplateSubject` TEXT NOT NULL AFTER `TemplateDescription`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_templates` ADD `TemplateThumbnailType` VARCHAR( 250 ) NOT NULL AFTER `TemplateThumbnail`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_campaigns` ADD `PublishOnRSS` ENUM( 'Enabled', 'Disabled' ) NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups` ADD `ForceOptInList` ENUM( 'Enabled', 'Disabled' ) NOT NULL AFTER `ForceRejectOptLink`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_users` ADD `ForwardToFriendHeader` TEXT NOT NULL, ADD `ForwardToFriendFooter` TEXT NOT NULL;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.0.1 to v4.0.2
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_402()
	{
	return;
	}

/**
 * Upgrade from v4.0.2 to v4.0.3
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_403()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_admins` DROP PRIMARY KEY ,ADD PRIMARY KEY ( `AdminID` ) ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_admins` CHANGE `AdminID` `AdminID` INT( 11 ) NOT NULL AUTO_INCREMENT ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_lists` ADD `OptOutAddToSuppressionList` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No' AFTER `OptOutScope`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_users_payment_log` ADD `PaidGateway` VARCHAR( 250 ) NOT NULL ,ADD `GatewayTransactionID` VARCHAR( 250 ) NOT NULL ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `POP3_BOUNCE_STATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `POP3_BOUNCE_HOST` VARCHAR( 250 ) NOT NULL ,ADD `POP3_BOUNCE_PORT` INT NOT NULL ,ADD `POP3_BOUNCE_USERNAME` VARCHAR( 250 ) NOT NULL ,ADD `POP3_BOUNCE_PASSWORD` VARCHAR( 250 ) NOT NULL ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `POP3_FBL_STATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `POP3_FBL_HOST` VARCHAR( 250 ) NOT NULL ,ADD `POP3_FBL_PORT` INT NOT NULL ,ADD `POP3_FBL_USERNAME` VARCHAR( 250 ) NOT NULL ,ADD `POP3_FBL_PASSWORD` VARCHAR( 250 ) NOT NULL ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `POP3_REQUESTS_STATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `POP3_REQUESTS_HOST` VARCHAR( 250 ) NOT NULL ,ADD `POP3_REQUESTS_PORT` INT NOT NULL ,ADD `POP3_REQUESTS_USERNAME` VARCHAR( 250 ) NOT NULL ,ADD `POP3_REQUESTS_PASSWORD` VARCHAR( 250 ) NOT NULL ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `SEND_BOUNCE_NOTIFICATIONEMAIL` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `REBRANDED_PRODUCT_LOGO` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `REBRANDED_PRODUCT_LOGO_TYPE` VARCHAR( 250 ) NOT NULL ,ADD `PAYPALEXPRESSSTATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled',ADD `PAYPALEXPRESSBUSINESSNAME` VARCHAR( 250 ) NOT NULL ;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `PAYPALEXPRESSPURCHASEDESCRIPTION` VARCHAR( 250 ) NOT NULL ,ADD `PAYPALEXPRESSCURRENCY` VARCHAR( 250 ) NOT NULL ,ADD `PAYPALWPPSTATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Enabled' ,ADD `DISPLAY_TRIGGER_SEND_ENGINE_LINK` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'Yes';";
	$ArraySQLQueries[] = <<<EOF
UPDATE `oempro_config` SET `PAYMENT_RECEIPT_EMAIL_SUBJECT` = '_Period:StartDate_ - _Period:FinishDate_ Payment Receipt, _Period:TotalAmount_  ', `PAYMENT_RECEIPT_EMAIL_MESSAGE` = 'Dear _User:FirstName_,

Your _Period:StartDate_ - _Period:FinishDate_ period payment receipt is included below. Please contact us by replying this email if you have any questions.

_Period:ReceiptDetails_

In order to pay the outstanding balance, please choose one of the payment methods below:

_Payment:Links_' WHERE `oempro_config`.`ConfigID` = 1;
EOF;
	$ArraySQLQueries[] = "UPDATE `oempro_admins` SET `AdminID` = '1' WHERE `oempro_admins`.`AdminID` =0 LIMIT 1 ;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.0.3 to v4.0.4
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_404()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_rel_tags_campaigns` (`RelID` int(11) NOT NULL auto_increment, `RelTagID` int(11) NOT NULL, `RelCampaignID` int(11) NOT NULL, `RelOwnerUserID` int(11) NOT NULL, PRIMARY KEY  (`RelID`), KEY `RelTagID` (`RelTagID`,`RelCampaignID`,`RelOwnerUserID`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_tags` (`TagID` int(11) NOT NULL auto_increment, `Tag` varchar(250) NOT NULL, `RelOwnerUserID` int(11) NOT NULL, PRIMARY KEY  (`TagID`), KEY `Tag` (`Tag`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `USER_SIGNUP_GROUPIDS` VARCHAR( 250 ) NOT NULL AFTER `USER_SIGNUP_GROUPID`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `PAYPALWPPSTATUS` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled' AFTER `PAYPALEXPRESSCURRENCY`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_custom_fields` ADD `IsGlobal` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No';";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_emails` ADD `FetchPlainURL` VARCHAR( 250 ) NOT NULL AFTER `FetchURL`;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_activity` ADD `TotalSentEmail` INT NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_lists` ADD `OptInSubscribeTo` INT NOT NULL, ADD `OptInUnsubscribeFrom` INT NOT NULL, ADD `OptOutSubscribeTo` INT NOT NULL, ADD `OptOutUnsubscribeFrom` INT NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups` ADD `LimitEmailSendPerPeriod` INT NOT NULL, ADD `LimitEmailSendPeriod` ENUM( 'Monthly' ) NOT NULL DEFAULT 'Monthly';";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_users` ADD `AccountStatus` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Enabled';";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.0.4 to v4.0.5
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_405()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` ADD `DEFAULT_OPTIN_EMAIL_SUBJECT` TEXT NOT NULL, ADD `DEFAULT_OPTIN_EMAIL_BODY` TEXT NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups`  ADD `ThresholdImport` INT NOT NULL DEFAULT '0',  ADD `ThresholdEmailSend` INT NOT NULL DEFAULT '0';";
	$ArraySQLQueries[] = "UPDATE `oempro_config` SET `DEFAULT_OPTIN_EMAIL_SUBJECT` = 'Please confirm your subscription', `DEFAULT_OPTIN_EMAIL_BODY` = 'Thank you for your recent subscription.

In order to activate your list subscription, we kindly request you to click the following link:

%Link:Confirm%

If you believe there''s a misunderstanding, you can cancel your subscription request immediately by clicking the following link:

%Link:Reject%

Thank you.


p.s.: This email is sent to %Subscriber:EmailAddress%' WHERE `oempro_config`.`ConfigID` = 1 LIMIT 1;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.0.5 to v4.0.6
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_406()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_config` CHANGE `PAYMENT_TAX_PERCENT` `PAYMENT_TAX_PERCENT` DOUBLE NOT NULL;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.0.6 to v4.0.7
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_407()
	{
	return;
	}

/**
 * Upgrade from v4.0.7 to v4.1.0
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_410()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_processlog` (`ProcessLogID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, `ProcessCode` VARCHAR(250) NOT NULL, `ProcessStartTime` DATETIME NOT NULL, `ProcessFinishTime` DATETIME NOT NULL, `ProcessStatus` ENUM('Processing','Completed') NOT NULL DEFAULT 'Processing', `ProcessResult` VARCHAR(250) NOT NULL, INDEX (`ProcessCode`, `ProcessStatus`)) ENGINE = MyISAM;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_campaigns`  ADD `LastActivityDateTime` DATETIME NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_campaigns` CHANGE `LastActivityDateTime` `LastActivityDateTime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_split_tests` (`TestID` int(11) NOT NULL AUTO_INCREMENT, `RelCampaignID` int(11) NOT NULL, `RelOwnerUserID` int(11) NOT NULL, `TestSize` int(11) NOT NULL, `TestDuration` int(11) NOT NULL, `Winner` enum('Highest Open Rate','Most Unique Clicks') COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`TestID`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_split_test_emails` (`RelID` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `RelTestID` INT(11) NOT NULL, `RelCampaignID` INT(11) NOT NULL, `RelEmailID` INT(11) NOT NULL, `RelOwnerUserID` INT(11) NOT NULL) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_browserview`  ADD `RelEmailID` INT NOT NULL AFTER `RelCampaignID`,  ADD INDEX (`RelEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_forward`  ADD `RelEmailID` INT NOT NULL AFTER `RelCampaignID`,  ADD INDEX (`RelEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_link`  ADD `RelEmailID` INT NOT NULL AFTER `RelCampaignID`,  ADD INDEX (`RelEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_open`  ADD `RelEmailID` INT NOT NULL AFTER `RelCampaignID`,  ADD INDEX (`RelEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_unsubscription`  ADD `RelEmailID` INT NOT NULL AFTER `RelCampaignID`,  ADD INDEX (`RelEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_custom_fields` CHANGE `FieldType` `FieldType` ENUM('Single line','Paragraph text','Multiple choice','Drop down','Checkboxes','Hidden field','Date field','Time field') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_custom_fields`  ADD `Option1` TEXT NOT NULL,  ADD `Option2` TEXT NOT NULL,  ADD `Option3` TEXT NOT NULL,  ADD `Option4` TEXT NOT NULL,  ADD `Option5` TEXT NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_split_tests`  ADD `RelWinnerEmailID` INT NOT NULL DEFAULT '0',  ADD INDEX (`RelWinnerEmailID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_split_tests`  ADD `SplitTestingStatus` ENUM('Pending','Sending Test','Waiting For Winner','Paused By User','Sending Winner','Completed') NOT NULL DEFAULT 'Pending' AFTER `RelOwnerUserID`;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	return;
	}

/**
 * Upgrade from v4.1.0 to v4.1.03
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4103()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_lists` ADD `SendActivityNotification`  ENUM( 'true', 'false' ) NOT NULL DEFAULT 'false'";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups`  ADD `PlainEmailHeader` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,  ADD `PlainEmailFooter` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,  ADD `HTMLEmailHeader` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,  ADD `HTMLEmailFooter` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups`  ADD `TrialGroup` ENUM('Yes','No') NOT NULL DEFAULT 'No',  ADD `TrialExpireSeconds` INT NOT NULL DEFAULT '0'";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `USERAREA_FOOTER` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `FORBIDDEN_FROM_ADDRESSES` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups` ADD `SendMethod` enum('System','SMTP','LocalMTA','PHPMail','PowerMTA','SaveToDisk') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System', ADD `SendMethodLocalMTAPath` text COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodPowerMTAVMTA` text COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodPowerMTADir` text COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodSaveToDiskDir` text COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodSMTPHost` text COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodSMTPPort` int(11) NOT NULL, ADD `SendMethodSMTPSecure` enum('','ssl','tls') COLLATE utf8_unicode_ci NOT NULL DEFAULT '', ADD `SendMethodSMTPAuth` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false', ADD `SendMethodSMTPUsername` varchar(250) COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodSMTPPassword` varchar(250) COLLATE utf8_unicode_ci NOT NULL, ADD `SendMethodSMTPTimeOut` int(11) NOT NULL, ADD `SendMethodSMTPDebug` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false', ADD `SendMethodSMTPKeepAlive` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true', ADD `SendMethodSMTPMsgConn` int(11) NOT NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups`  ADD `XMailer` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_short_urls` (`URLID` int(11) NOT NULL AUTO_INCREMENT, `URL` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, `Hash` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`URLID`), KEY `Hash` (`Hash`), KEY `URL` (`URL`(333))) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_imports`  ADD `FieldTerminator` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `ImportData`,  ADD `FieldEncloser` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `FieldTerminator`";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_lists` ADD `OptOutAddToGlobalSuppressionList`  ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No'";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups`  ADD `PaymentCreditSystem` ENUM('Enabled','Disabled') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Disabled' AFTER `PaymentSystemChargePeriod`,  ADD `PaymentCreditPricing` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `PaymentCreditSystem`";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_users`  ADD `AvailableCredits` INT NOT NULL DEFAULT '0'";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.0 to v4.1.09
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4109()
	{
	}

/**
 * Upgrade from v4.1.09 to v4.1.10 (RC1)
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4110()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_auto_responders` CHANGE `AutoResponderTriggerType` `AutoResponderTriggerType` ENUM('OnSubscription','OnSubscriberLinkClick','OnSubscriberForwardToFriend','OnSubscriberCampaignOpen') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `POP3_BOUNCE_SSL` ENUM('Yes','No') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' AFTER `POP3_BOUNCE_PASSWORD`";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `POP3_FBL_SSL` ENUM('Yes','No') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' AFTER `POP3_FBL_PASSWORD`";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `POP3_REQUESTS_SSL` ENUM('Yes','No') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No' AFTER `POP3_REQUESTS_PASSWORD`";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `CENTRALIZED_SENDER_DOMAIN` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `RUN_CRON_IN_USER_AREA` ENUM('true','false') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true' AFTER `FORBIDDEN_FROM_ADDRESSES`";
	$ArraySQLQueries[] = "CREATE TABLE `oempro_custom_field_values` ( `ValueID` int(11) NOT NULL AUTO_INCREMENT, `RelOwnerUserID` int(11) NOT NULL, `RelListID` int(11) NOT NULL, `RelFieldID` int(11) NOT NULL, `EmailAddress` varchar(250) COLLATE utf8_unicode_ci NOT NULL, `ValueTime` time NOT NULL, `ValueDate` date NOT NULL, `ValueDouble` double NOT NULL, `ValueText` text COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`ValueID`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}

	// Convert old style global custom fields to the new global custom field structures - Start {
	$SQLQuery = "SELECT * FROM oempro_custom_fields WHERE IsGlobal='Yes'";
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

	$CustomFields = array();
	while ($EachCustomField = mysql_fetch_assoc($ResultSet))
		{
		$CustomFields[$EachCustomField['CustomFieldID']] = $EachCustomField;
		}

	$SQLQuery = "SELECT * FROM oempro_subscriber_lists";
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

	while ($EachList = mysql_fetch_assoc($ResultSet))
		{
		$SQLQuery = "EXPLAIN oempro_subscribers_".$EachList['ListID'];
		$ResultSet2 = Database::$Interface->ExecuteQuery($SQLQuery);

		while ($EachSubscriberField = mysql_fetch_assoc($ResultSet2))
			{
			if ((strpos($EachSubscriberField['Field'], 'CustomField') !== false) && ($CustomFields[str_replace('CustomField', '', $EachSubscriberField['Field'])]['IsGlobal'] == 'Yes'))
				{
				$CustomField = $CustomFields[str_replace('CustomField', '', $EachSubscriberField['Field'])];

				$FieldName = 'ValueText';
				if ($CustomField['FieldType'] == 'Date field' || $CustomField['ValidationMethod'] == 'Date')
					{
					$FieldName = 'ValueDate';
					}
				else if ($CustomField['FieldType'] == 'Time field' || $CustomField['ValidationMethod'] == 'Time')
					{
					$FieldName = 'ValueTime';
					}
				else if ($CustomField['ValidationMethod'] == 'Numbers')
					{
					$FieldName = 'ValueDouble';
					}


				$SQLQuery = 'DELETE FROM oempro_custom_field_values WHERE RelFieldID = '.$CustomField['CustomFieldID'].' AND RelOwnerUserID = '.$CustomField['RelOwnerUserID'].' AND EmailAddress = ANY (SELECT EmailAddress FROM oempro_subscribers_'.$EachList['ListID'].')';
				$ResultSet3 = Database::$Interface->ExecuteQuery($SQLQuery);

				$SQLQuery = "INSERT INTO oempro_custom_field_values (`ValueID`, `RelOwnerUserID`, `RelListID`, `RelFieldID`, `EmailAddress`, `".$FieldName."`) SELECT NULL, ".$CustomField['RelOwnerUserID'].", 0, ".$CustomField['CustomFieldID'].", EmailAddress, CustomField".$CustomField['CustomFieldID']." FROM oempro_subscribers_".$EachList['ListID'];
				$ResultSet3 = Database::$Interface->ExecuteQuery($SQLQuery);

				$SQLQuery = "ALTER TABLE `oempro_subscribers_".$EachList['ListID']."` DROP `CustomField".$CustomField['CustomFieldID']."`";
				$ResultSet3 = Database::$Interface->ExecuteQuery($SQLQuery);
				}
			}
		}
	// Convert old style global custom fields to the new global custom field structures - End }

	// Change all themes from weefor to weefive - Start {
	$SQLQuery = "UPDATE oempro_themes SET Template='weefive' WHERE Template='weefor'";
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
	// Change all themes from weefor to weefive - End }
	}

/**
 * Upgrade from v4.1.10 to v4.1.11 (RC2)
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4111()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_user_groups` ADD `CreditSystem` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Disabled' AFTER `PaymentSystem`";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.11 to v4.1.12 (RC3)
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4112()
	{
	$ArraySQLQueries = array();

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.12 to v4.1.13 (Stable Release)
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4113()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_users_credits_purchase_log` (`LogID` int(11) NOT NULL AUTO_INCREMENT,`RelUserID` int(11) NOT NULL,`PurchaseDate` datetime NOT NULL,`PurchasedCredits` int(11) NOT NULL,`GrossAmount` double NOT NULL,`Tax` double NOT NULL,`Discount` double NOT NULL,`NetAmount` double NOT NULL,`PaymentStatus` enum('Unpaid','Waiting','Paid','Waived') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unpaid',`PaymentStatusDate` datetime NOT NULL,`PaidGateway` varchar(250) COLLATE utf8_unicode_ci NOT NULL,`GatewayTransactionID` varchar(250) COLLATE utf8_unicode_ci NOT NULL,PRIMARY KEY (`LogID`),KEY `RelUserID` (`RelUserID`,`PurchaseDate`,`PaymentStatus`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_config`  ADD `PAYMENT_CREDITS_GATEWAY_URL` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `PAYMENT_RECEIPT_EMAIL_MESSAGE`;";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.13 to v4.1.14 (Hot Fix Release)
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4114()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "UPDATE `oempro_config` SET `USER_SIGNUP_LANGUAGE` = 'EN', `DEFAULT_LANGUAGE` = 'EN'";
	$ArraySQLQueries[] = "UPDATE `oempro_users` SET `Language` = 'EN'";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.14 to v4.1.15
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4115()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_custom_field_values` ADD INDEX ( `RelOwnerUserID` , `RelListID` , `RelFieldID` , `EmailAddress` );";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_custom_field_values` ADD INDEX ( `RelFieldID` , `EmailAddress` );";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_browserview` DROP INDEX `RelCampaignID`, ADD INDEX `RelCampaignID` (`RelCampaignID`, `RelListID`, `RelSubscriberID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_forward` DROP INDEX `RelCampaignID`, ADD INDEX `RelCampaignID` (`RelCampaignID`, `RelListID`, `RelSubscriberID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_link` DROP INDEX `RelCampaignID`, ADD INDEX `RelCampaignID` (`RelCampaignID`, `RelListID`, `RelSubscriberID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_open` DROP INDEX `RelCampaignID`, ADD INDEX `RelCampaignID` (`RelCampaignID`, `RelListID`, `RelSubscriberID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_unsubscription` DROP INDEX `RelCampaignID`, ADD INDEX `RelCampaignID` (`RelCampaignID`, `RelListID`, `RelSubscriberID`);";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_lists` CHANGE `SyncStatus` `SyncStatus` ENUM('Enabled','Disabled') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Disabled';";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.15 to v4.1.16
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4116()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE `oempro_queue` ADD INDEX `IDX_QUEUE_01` ( `RelCampaignID` );";
	$ArraySQLQueries[] = "ALTER TABLE `oempro_queue` ADD INDEX `IDX_QUEUE_02` ( `RelCampaignID` , `Status` );";

	foreach ($ArraySQLQueries as $EachSQLQuery)
		{
		$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

		if (Database::$Interface->ReturnError() != '')
			{
			self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
			}
		}
	}

/**
 * Upgrade from v4.1.16 to v4.1.17
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4117()
	{
	$ArraySQLQueries = array();

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

/**
 * Upgrade from v4.1.17 to v4.1.18
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4118()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE  `oempro_templates` ADD  `IsCreatedByAdmin` INT( 1 ) UNSIGNED NOT NULL DEFAULT '0';";
	$ArraySQLQueries[] = "ALTER TABLE  `oempro_auto_responders` CHANGE  `AutoResponderTriggerType`  `AutoResponderTriggerType` ENUM(  'OnSubscription',  'OnSubscriberLinkClick',  'OnSubscriberForwardToFriend', 'OnSubscriberCampaignOpen',  'OnSubscriberDate' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;";
	$ArraySQLQueries[] = "ALTER TABLE  `oempro_auto_responders` ADD  `AutoResponderTriggerValue2` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT  '' AFTER  `AutoResponderTriggerValue`;";
	$ArraySQLQueries[] = "UPDATE  `oempro_custom_fields` SET RelListID = 0 WHERE IsGlobal = 'Yes'";

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

/**
 * Upgrade from v4.1.18 to v4.1.19
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_4119()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE  `oempro_suppression_list` ADD INDEX (  `EmailAddress` );";
	$ArraySQLQueries[] = "ALTER TABLE  `oempro_suppression_list` CHANGE  `SuppressionSource`  `SuppressionSource` ENUM(  'Administrator',  'User',  'SPAM complaint',  'Hard Bounced' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;";

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

/**
 * Upgrade from v4.1.19 to v4.2.0
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_420()
	{
	$ArraySQLQueries = array();

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

/**
 * Upgrade from v4.2.0 to v4.2.1
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_421()
	{
	$ArraySQLQueries = array();

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}


/**
 * Upgrade from v4.2.1 to v4.2.2
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_422()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "CREATE TABLE `oempro_wufoo_integrations` (`IntegrationID` int(11) unsigned NOT NULL AUTO_INCREMENT, `RelOwnerUserID` int(11) unsigned NOT NULL, `RelListID` int(11) unsigned NOT NULL, `HandshakeKey` varchar(100) COLLATE utf8_unicode_ci NOT NULL, `SubDomain` varchar(100) COLLATE utf8_unicode_ci NOT NULL, `ApiKey` varchar(250) COLLATE utf8_unicode_ci NOT NULL, `FormHash` varchar(250) COLLATE utf8_unicode_ci NOT NULL, `FieldMapping` text COLLATE utf8_unicode_ci NOT NULL, `WebhookHash` varchar(250) COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`IntegrationID`), KEY `RelListID` (`RelListID`), KEY `RelOwnerUserID` (`RelOwnerUserID`), KEY `HandshakeKey` (`HandshakeKey`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

/**
 * Upgrade from v4.2.2 to v4.2.3
 *
 * @return void
 * @author Cem Hurturk
 */
function upgrade_423()
	{
	$ArraySQLQueries = array();

	$ArraySQLQueries[] = "ALTER TABLE  `oempro_wufoo_integrations` ADD  `FormName` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER  `FormHash`";
	$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_wufoo_integration_logs` ( `LogID` int(11) unsigned NOT NULL AUTO_INCREMENT, `RelListID` int(11) unsigned NOT NULL, `RelIntegrationID` int(11) unsigned NOT NULL, `RelOwnerUserID` int(11) unsigned NOT NULL, `Message` text COLLATE utf8_unicode_ci NOT NULL, `Status` int(1) unsigned NOT NULL, `DateTime` datetime NOT NULL, PRIMARY KEY (`LogID`), KEY `RelOwnerUserID` (`RelOwnerUserID`), KEY `RelIntegrationID` (`RelIntegrationID`), KEY `RelIntegrationIDandStatus` (`RelIntegrationID`,`Status`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	if (count($ArraySQLQueries) > 0)
		{
		foreach ($ArraySQLQueries as $EachSQLQuery)
			{
			$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

			if (Database::$Interface->ReturnError() != '')
				{
				self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
				}
			}
		}
	}

	/**
	 * Upgrade from v4.2.3 to v4.2.4
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_424()
		{
		$ArraySQLQueries = array();

		$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_highrise_integration_config` (`ConfigID` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `RelOwnerUserID` INT( 11 ) NOT NULL, `Account` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, `APIKey` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, `IsEnabled` INT( 1 ) NOT NULL, INDEX (  `RelOwnerUserID` ) ) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_transactional_email_queue` ADD INDEX `PendingIndex` (`Status`, `TimeToSend`);";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.2.4 to v4.3.0
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_430()
		{
		$ArraySQLQueries = array();

		$ArraySQLQueries[] = "CREATE TABLE `oempro_subscriber_activity_cache` (`CacheID` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `RelListID` INT( 11 ) UNSIGNED NOT NULL, `RelSubscriberID` INT( 11 ) UNSIGNED NOT NULL, `DateTime` DATETIME NOT NULL, `CacheContent` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_subscriber_activity_cache` ADD INDEX  `IDX_MAINQUERY` (  `RelListID` ,  `RelSubscriberID` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_forward` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `ForwardDate` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_open` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `OpenDate` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_link` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `ClickDate` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_bounce` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `BounceDate` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_browserview` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `ViewDate` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_stats_unsubscription` ADD INDEX  `IDX_ACTIVITY` (  `RelListID` ,  `RelSubscriberID` ,  `UnsubscriptionDate` );";
		$ArraySQLQueries[] = "ALTER TABLE  `oempro_subscriber_lists` ADD INDEX  `IDX_OWNERUSER` (  `RelOwnerUserID` )";
		$ArraySQLQueries[] = "CREATE TABLE  `oempro_email_headers` (`HeaderID` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `HeaderName` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, `EmailType` ENUM(  'all',  'campaign',  'autoresponder',  'transactional' ) NOT NULL DEFAULT  'all', `HeaderValue` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
		$ArraySQLQueries[] = "ALTER TABLE  `oempro_email_headers` ADD INDEX (  `EmailType` )";
		$ArraySQLQueries[] = 'UPDATE oempro_config SET PAYMENT_RECEIPT_EMAIL_SUBJECT = "%Period:StartDate% - %Period:FinishDate% Payment Receipt" WHERE PAYMENT_RECEIPT_EMAIL_SUBJECT = "%s - %s Payment Receipt, %s"';
		$ArraySQLQueries[] = 'UPDATE oempro_config SET PAYMENT_RECEIPT_EMAIL_MESSAGE = "Dear %User:FirstName%,\n\nYour %Period:StartDate% - %Period:FinishDate% period payment receipt is included below. Please contact us by replying this email if you have any questions.\n\n%Period:ReceiptDetails%" WHERE PAYMENT_RECEIPT_EMAIL_MESSAGE = "Dear %s,\n\nYour %s - %s period payment receipt is included below. Please contact us by replying this email if you have any questions.\n\n%s"';
		$ArraySQLQueries[] = "CREATE TABLE `oempro_log_user_agents` (`LogID` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `UserAgentString` TEXT NOT NULL, `Referrer` TEXT NOT NULL, `RelListID` INT( 11 ) UNSIGNED NOT NULL, `RelSubscriberID` INT( 11 ) UNSIGNED NOT NULL, `RelCampaignID` INT( 11 ) UNSIGNED NOT NULL, `RelAutoResponderID` INT( 11 ) UNSIGNED NOT NULL, `LogDateTime` DATETIME NOT NULL) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
		$ArraySQLQueries[] = "ALTER TABLE  `oempro_subscriber_lists` ADD  `UnsubscriptionErrorPageURL` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER  `UnsubscriptionConfirmedPageURL`";
		$ArraySQLQueries[] = "ALTER TABLE  `oempro_subscriber_lists` ADD  `SubscriptionErrorPageURL` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER  `SubscriptionConfirmedPageURL`";
		$ArraySQLQueries[] = "CREATE TABLE `oempro_log_campaign_sends` (`LogID` int(11) NOT NULL AUTO_INCREMENT, `RelOwnerUserID` int(11) NOT NULL, `RelCampaignID` int(11) NOT NULL, `RelListID` int(11) NOT NULL, `RelSegmentID` int(11) NOT NULL, `RelSubscriberID` int(11) NOT NULL, `EmailAddress` varchar(250) COLLATE utf8_unicode_ci NOT NULL, `LogDateTime` DATETIME NOT NULL, PRIMARY KEY (`LogID`), KEY `IDX_01` (`RelListID`,`RelSubscriberID`, `RelCampaignID`, `RelOwnerUserID`), KEY `IDX_02` (`RelListID`, `RelOwnerUserID`), KEY `IDX_03` (`EmailAddress`), KEY `IDX_04` (`EmailAddress`, `RelOwnerUserID`), KEY `IDX_05` (`RelOwnerUserID`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
		$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_log_autoresponder_sends` (`LogID` int(11) NOT NULL AUTO_INCREMENT, `RelOwnerUserID` int(11) NOT NULL, `RelAutoResponderID` int(11) NOT NULL, `RelListID` int(11) NOT NULL, `RelSubscriberID` int(11) NOT NULL, `EmailAddress` varchar(250) COLLATE utf8_unicode_ci NOT NULL, `LogDateTime` DATETIME NOT NULL, PRIMARY KEY (`LogID`), KEY `IDX_01` (`RelListID`,`RelSubscriberID`, `RelAutoResponderID`, `RelOwnerUserID`), KEY `IDX_02` (`RelListID`, `RelOwnerUserID`), KEY `IDX_03` (`EmailAddress`), KEY `IDX_04` (`EmailAddress`, `RelOwnerUserID`), KEY `IDX_05` (`RelOwnerUserID`), KEY `IDX_06` (`RelListID`, `RelAutoResponderID`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.3.0 to v4.3.1
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_431()
		{
		$ArraySQLQueries = array();

		$ArraySQLQueries[] = "ALTER TABLE  `oempro_processlog` ADD INDEX  `LogCleaningIndex` (  `ProcessStatus` ,  `ProcessFinishTime` );";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.3.1 to v4.3.2
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_432()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}


	/**
	 * Upgrade from v4.3.2 to v4.3.3
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_433()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.3.3 to v4.3.4
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_434()
		{
		$ArraySQLQueries = array();
		$ArraySQLQueries[] = "ALTER TABLE `oempro_suppression_list` ADD INDEX `IDX_SUPPRESSION_CHECK_FOR_QUEUE` ( `RelListID` , `RelOwnerUserID` );";
		$ArraySQLQueries[] = "CREATE TABLE  `oempro_log_flood_detection` (`LogID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY , `Zone` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `IPAddress` VARCHAR( 15 ) NOT NULL , `IsBlocked` TINYINT NOT NULL , `Count` INT NOT NULL , `Time` INT NOT NULL , `BlockTime` INT NOT NULL) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.3.4 to v4.3.5
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_435()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}


	/**
	 * Upgrade from v4.3.4 to v4.4.0
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_440()
		{
		$ArraySQLQueries = array();
		$ArraySQLQueries[] = "ALTER TABLE `oempro_campaigns` ADD `CreateDateTime` DATETIME NULL;";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_campaigns` ADD INDEX (  `CreateDateTime` );";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}



	/**
	 * Upgrade from v4.4.0 to v4.4.1
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_441()
		{
		$ArraySQLQueries = array();
		$ArraySQLQueries[] = "ALTER TABLE  `oempro_segments` ADD  `SubscriberCount` INT( 11 ) NOT NULL DEFAULT  '0', ADD  `SubscriberCountLastCalculatedOn` DATETIME NOT NULL DEFAULT  '2012-01-01 00:01:01';";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.4.1 to v4.5.0
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_450()
		{
		$ArraySQLQueries = array();
		$ArraySQLQueries[] = "ALTER TABLE `oempro_medialibrary` ADD `RelFolderID` INT( 11 ) NOT NULL;";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_medialibrary` ADD INDEX (  `RelFolderID` );";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_medialibrary` DROP `Tags`;";
		$ArraySQLQueries[] = "ALTER TABLE `oempro_suppression_list` ADD INDEX  `IDX_SUPPR_SOURCE` (  `SuppressionSource` );";
		$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_medialibrary_folders` ( `FolderID` int(11) unsigned NOT NULL AUTO_INCREMENT,  `RelOwnerUserID` int(11) unsigned NOT NULL,  `RelParentFolderID` int(11) unsigned NOT NULL,  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,  PRIMARY KEY (`FolderID`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
		$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_suppression_patterns` (  `PatternID` int(11) unsigned NOT NULL AUTO_INCREMENT,  `Pattern` text COLLATE utf8_unicode_ci NOT NULL,  `Description` text COLLATE utf8_unicode_ci NOT NULL,  `PatternType` enum('REGEXP','NOT REGEXP') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'REGEXP',  PRIMARY KEY (`PatternID`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.5.0 to v4.5.1
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_451()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.5.1 to v4.5.2
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_452()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.5.2 to v4.5.3
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_453()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.5.3 to v4.6.0
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_460()
		{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
		}

	/**
	 * Upgrade from v4.6.0 to v4.6.1
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_461()
	{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
	}

	/**
	 * Upgrade from v4.6.1 to v4.6.2
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_462()
	{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
	}


	/**
	 * Upgrade from v4.6.2 to v4.6.3
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_463()
	{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
	}


	/**
	 * Upgrade from v4.6.3 to v4.6.4
	 *
	 * @return void
	 * @author Cem Hurturk
	 */
	function upgrade_464()
	{
		$ArraySQLQueries = array();

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}
	}

	/**
	 * Upgrade from v4.6.4 to v4.7.0
	 *
	 * @return void
	 * @author Mert Hurturk
	 */
	function upgrade_470()
	{
		$ArraySQLQueries = array();
		$ArraySQLQueries[] = "ALTER TABLE `oempro_log_flood_detection` ADD INDEX `Zone` (`Zone`, `IPAddress`)";
		$ArraySQLQueries[] = "CREATE TABLE IF NOT EXISTS `oempro_users_api_keys` (`APIKeyID` int(11) unsigned NOT NULL AUTO_INCREMENT, `RelOwnerUserID` int(11) NOT NULL, `APIKey` varchar(39) COLLATE utf8_unicode_ci NOT NULL, `Note` varchar(255) COLLATE utf8_unicode_ci NOT NULL, `IPAddress` varchar(15) COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`APIKeyID`), UNIQUE KEY `APIKey` (`APIKey`), KEY `RelOwnerUserID` (`RelOwnerUserID`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		if (count($ArraySQLQueries) > 0)
			{
			foreach ($ArraySQLQueries as $EachSQLQuery)
				{
				$ResultSet = Database::$Interface->ExecuteQuery($EachSQLQuery, true);

				if (Database::$Interface->ReturnError() != '')
					{
					self::$ArrayDatabaseErrors[] = Database::$Interface->ReturnError();
					}
				}
			}

		// REMOVE ALL CSS CACHE FOR THEMES
		$CSSCacheDir = APP_PATH.'/data/css/';
		@rmdir($CSSCacheDir);
		@mkdir($CSSCacheDir);
	}



} // END class Upgrade
?>