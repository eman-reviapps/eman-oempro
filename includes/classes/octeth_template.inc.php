<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| support@octeth.com                        |
+-------------------------------------------+
| Last Update Date: 2004-09-08 15:41        |
+-------------------------------------------+
| Version: v2.0.0.0							|
+-------------------------------------------+
*/

/*
----[ TEMPLATE PARSING CLASS
----[ SYNOPSIS
////////////////////////////////////////// EXAMPLE HTML
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
</head>

<body>
<p>Trest
</p>
<p>_Name_ and _Age_ </p>
<LIST:Records>
<p>_ListName_</p>
</LIST:Records>
<form action="" method="post" enctype="multipart/form-data" name="form1">
    <p>
        <input type="submit" name="Submit" value="Submit">
    </p>
    <p>
        <input type="text" name="FormValue_Name" value="_EnteredName_">
    </p>
    <ERROR:Name>_Error_</ERROR:Name>
    <p>
        <input type="password" name="FormValue_Password" value="_EnteredPassword_">
    </p>
    <ERROR:Password>_Error_</ERROR:Password>
    <p>
        <textarea name="FormValue_Address">_EnteredAddress_</textarea>
</p>
    <ERROR:Address>_Error_</ERROR:Address>
    <p>
        <input type="checkbox" name="FormValue_Option[]" value="1" _SelectedOption1_>Option 1<br>
        <input type="checkbox" name="FormValue_Option[]" value="2" _SelectedOption2_>Option 2<br>
        <input type="checkbox" name="FormValue_Option[]" value="3" _SelectedOption3_>Option 3<br>
        <input type="checkbox" name="FormValue_Option[]" value="4" _SelectedOption4_>Option 4<br>
    </p>
    <ERROR:Option>_Error_</ERROR:Option>
    <p>
        <input name="FormValue_OneOption" type="radio" value="1" _SelectedOneOption1_>1
        <input name="FormValue_OneOption" type="radio" value="2" _SelectedOneOption2_>2
        <input name="FormValue_OneOption" type="radio" value="3" _SelectedOneOption3_>3
    </p>
    <ERROR:OneOption>_Error_</ERROR:OneOption>
    <p>        <select name="FormValue_ListValues">
					<option value="1" _SelectedListValues1_>1</option>
					<option value="2" _SelectedListValues2_>2</option>
					<option value="3" _SelectedListValues3_>3</option>
					<option value="4" _SelectedListValues4_>4</option>
	            </select>
    </p>
    <ERROR:ListValues>_Error_</ERROR:ListValues>
    <p>
        <select name="FormValue_ListOption[]" size="5" multiple>
			<option value="1" _SelectedListOption1_>1</option>
			<option value="2" _SelectedListOption2_>2</option>
			<option value="3" _SelectedListOption3_>3</option>
			<option value="4" _SelectedListOption4_>4</option>
        </select>
</p>
    <ERROR:ListOption>_Error_</ERROR:ListOption>
    <p>
        <input type="hidden" name="HiddenInfo" value="_EnteredHiddenInfo_">
    </p>
    <ERROR:HiddenInfo>_Error_</ERROR:HiddenInfo>
    <input type="file" name="FormValue_File"><ERROR:File>_Error_</ERROR:File>
</form>
<p>&nbsp;</p>
</body>
</html>


////////////////////////////////////////// EXAMPLE CODE
include_once("./system/classes/class_octethtemplate_v2.php");


$ObjectTemplate = new OctethTemplate("./test.html", "file");
if (count($_POST) > 0)
	{
	$ArrayRequiredFields = array(
								"Name"			=>		"Enter your HABALA",
								"ListOption"	=>		"Select options",
								);
	$ArrayErrorMessages = $ObjectTemplate->CheckRequiredFields($ArrayRequiredFields, $ArrayErrorMessages, $_POST, 'FormValue_');
	}


	$ArrayValues = array(
						"Name"			=>		"Cem Hurturk",
						"Age"			=>		25,
						);
$ObjectTemplate->FillIn("", $ArrayValues);

$ObjectTemplate->DefineBlock("LIST:Records");

for ($TMPCounter = 1; $TMPCounter <= 5; $TMPCounter++)
	{
	$ArrayReplaceList['ListName'] = 'Cem'.$TMPCounter;
	$ObjectTemplate->DuplicateBlock("LIST:Records", "LIST:Records", $ArrayReplaceList);
	}

	$ArrayFormFields = array(
							"Name"			=>		"textfield",
							"Password"		=>		"Password",
							"Address"		=>		"TextArea",
							"Option"		=>		"CheckBox",
							"OneOption"		=>		"RadioButton",
							"ListOption"	=>		"List",
							"ListValues"	=>		"DropList",
							"HiddenInfo"	=>		"Hidden",
							"File"			=>		"File",
							);
//	$ArrayErrorMessages['Name']		=	'Please enter your name';
//	$ArrayErrorMessages['Password']		=	'Please enter your password';
//	$ArrayErrorMessages['Address']		=	'Please enter your address';
//	$ArrayErrorMessages['Option']		=	'Please enter your options';
//	$ArrayErrorMessages['OneOption']		=	'Please enter your one options';
//	$ArrayErrorMessages['ListOption']		=	'Please enter your list options';
//	$ArrayErrorMessages['ListValues']		=	'Please enter your list vales';
//	$ArrayErrorMessages['HiddenInfo']		=	'Please enter your hifddden';
//	$ArrayErrorMessages['File']		=	'Please enter your file';
$ObjectTemplate->ParseForm($_POST, $ArrayFormFields, $ArrayErrorMessages);


print $ObjectTemplate->ParseAndOutput();




----[ DESCRIPTION
This object is the advanced version of OctethTemplate v1.0.
This module includes all features of OctethTemplate v1.0 plus
form parsing and required field checking.

----[ CLASS NAME AND VERSION
OctethTemplate v2.0

----[ AUTHOR
Octeth Technologies www.octeth.com <sales@octeth.com>
Cem Hurturk <cem@octeth.com>


---------------------------------------------------------------------------------------
*/

//-------------------------------------------[ OctethTemplate Class
class OctethTemplate
{
//*************************************************************************
// PUBLIC PROPERTIES
//*************************************************************************
var $Template            		=	'';            // this will keep the template
var $TemplateType	      	  	=	'file';        // this will keep the type of template (file or string)
var $TemplateContent	    	=	'';            // this will keep the generated template
var $ArrayReplaceList 	        =	array();	    // list and values of replaces
var $ArrayBlocks           		=	array();   	 // list and contents of blocks
var $ArrayOrgBlocks        		=	array();   	 // list and contents of blocks
var $Error 		               	=	'';            // keeps error string
var $TMPCounter					=	0;	

//*************************************************************************
// PRIVATE PROPERTIES
//*************************************************************************


//*************************************************************************
// PUBLIC METHODS
//*************************************************************************

//*************************************************************************
// Checks if the submitted fields are empty or not
// ---
// IN		Array		List of required fields
// IN		Array		List of error messages for each field
// IN		Array		Values (ex: POST)
// IN		Array		Prefix for field name (ex: FormValue_)
// OUT		Void		N/A
function CheckRequiredFields($ArrayRequiredFields, $ArrayErrorMessages, $ArrayValues, $FieldNamePrefix)
    {
	// Check for required fields - Start
	foreach ($ArrayRequiredFields as $Field=>$ErrorMessage)
		{
		if ($ArrayValues[$FieldNamePrefix.$Field] == '')
			{
			$ArrayErrorMessages[$Field]	=	$ErrorMessage;
			}
		}
	// Check for required fields - Start

	return $ArrayErrorMessages;
	}

//*************************************************************************
// Parses the form
// ---
// IN		Array		Values (POST or any other values)
// IN		Array		Form fields
// IN		Array		Error messages for each form field
// IN		String		Name of the class which will be applied to error fields
// OUT		Void		N/A
function ParseForm($ArrayValues, $ArrayFormFields, $ArrayErrorMessages, $ErrorClassName)
    {
	// Initialize variables - Start
	$FieldValuePrefix		= 'Entered';
	$FieldNamePrefix		= 'FormValue_';
	$ErrorTag				= 'Error';
	$ErrorBlockName			= 'ERROR:';
	// Initialize variables - End

	foreach ($ArrayFormFields as $EachField=>$FieldType)
		{
		// Parse the entered form fields value - Start
		if (strtolower($FieldType) == strtolower("Hidden"))
			{
			// Hidden field
			$ArrayReplaceList[$FieldValuePrefix.$EachField] = htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]));
			}
		elseif (strtolower($FieldType) == strtolower("File"))
			{
			// File field
			$ArrayReplaceList[$FieldValuePrefix.$EachField] = htmlspecialchars($ArrayValues[$FieldNamePrefix.$EachField]);
			}
		elseif (strtolower($FieldType) == strtolower("TextField"))
			{
			// Text field
			$ArrayReplaceList[$FieldValuePrefix.$EachField] = htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]));
			}
		elseif (strtolower($FieldType) == strtolower("TextArea"))
			{
			// Text area field
			$ArrayReplaceList[$FieldValuePrefix.$EachField] = htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]));
			}
		elseif (strtolower($FieldType) == strtolower("Password"))
			{
			// Password field
			$ArrayReplaceList[$FieldValuePrefix.$EachField] = "";
			}
		elseif (strtolower($FieldType) == strtolower("DropList"))
			{
			// Droplist field
			$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]))] = "selected";
			}
		elseif (strtolower($FieldType) == strtolower("List"))
			{
			// List field
			if ((gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "array") || (gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "Array") || (gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "ARRAY"))
				{
				foreach ($ArrayValues[$FieldNamePrefix.$EachField] as $EachSelection)
					{
					$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($EachSelection))] = "selected";
					}
				}
			else
				{
				$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]))] = "selected";
				}
			}
		elseif (strtolower($FieldType) == strtolower("CheckBox"))
			{
			// Checkbox field
			if ((gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "array") || (gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "Array") || (gettype($ArrayValues[$FieldNamePrefix.$EachField]) == "ARRAY"))
				{
				foreach ($ArrayValues[$FieldNamePrefix.$EachField] as $EachSelection)
					{
					$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($EachSelection))] = "checked";
					}
				}
			else
				{
				$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]))] = "checked";
				}
			}
		elseif (strtolower($FieldType) == strtolower("RadioButton"))
			{
			// Radiobutton field
			$ArrayReplaceList['Selected'.$EachField.htmlspecialchars(stripslashes($ArrayValues[$FieldNamePrefix.$EachField]))] = "checked";
			}			
		// Parse the entered form fields value - End

		// Parse the field error blocks - Start
		$TMPErrorBlockName	= $ErrorBlockName.$EachField;

		if ($ArrayErrorMessages[$EachField] != "")
			{
			// Error exists on this field
			$this->DefineBlock($TMPErrorBlockName);
				$ArrayReplaceList[$ErrorTag] = $ArrayErrorMessages[$EachField];
			$this->DuplicateBlock($TMPErrorBlockName, $TMPErrorBlockName, $ArrayReplaceList);

			// Change the class of the field - Start
			if ($ErrorClassName != '')
				{
				$ArrayReplaceList['ErrorClass'.$EachField] = $ErrorClassName;
				$this->Replace($ArrayReplaceList);
				}
			// Change the class of the field - End
			}
		else
			{
			$this->RemoveBlock($TMPErrorBlockName);
			}
		// Parse the field error blocks - End
		}

	$this->Replace($ArrayReplaceList);
	
	return true;
	}

//*************************************************************************
// Insert the submitted value array into replace list with prefix
// ---
// IN		String		Prefix
// IN		Arrat		Assosciative array for keys and values
// OUT		Void		N/A
function FillIn($Prefix, $ArrayValues)
    {
	foreach ($ArrayValues as $Key=>$Value)
		{
        $this->ArrayReplaceList['_'.$Prefix.$Key.'_'] = htmlspecialchars($Value);
        }
	return;
	}

//*************************************************************************
// Loads the template
// ---
// IN		String		The location of the template file or the string of the template content
// IN		String		The type of the first parameter ("file", "string")
// OUT		Boolean		Returns false if an error occurs
function LoadTemplate($Template, $TemplateType = 'file')
    {
	// Assign the variables
	$this->Template			=		$Template;
	$this->TemplateType		=		$TemplateType;
	
	// If the template type is "file", then check if file exists or not - Start
	if ($this->TemplateType == 'file')
		{
		if (file_exists($this->Template))
			{
			$TMPTemplateContent = file($this->Template);
				$TMPTemplateContent = implode('', $TMPTemplateContent);
			$this->TemplateContent = $TMPTemplateContent;
			}
		else
			{
			$this->ReturnError('Couldn\'t find the template');
			return false;
			}
		}
	else
		{
		$this->TemplateContent = $this->Template;
		}
	// If the template type is "file", then check if file exists or not - End

	return true;
	}

//*************************************************************************
// Insert each replace list element to the replace queue
// ---
// IN		Array		The replace list
// OUT		Void		N/A
function Replace($ArrayReplaceList)
    {
	foreach ($ArrayReplaceList as $Key=>$Value)
		{
        $this->ArrayReplaceList['_'.$Key.'_'] = $Value;
        }

	return;
    }

//*************************************************************************
// Define the block and insert the block content into block pool
// ---
// IN		String		The name of the block
// OUT		Void		N/A
function DefineBlock($BlockName)
    {
	// Get the block content and insert it with it's name to associative array
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
	preg_match_all($Pattern, $this->TemplateContent, $Matches, PREG_SET_ORDER);
	$BlockContent = $Matches[0][1];
	
	$this->ArrayBlocks[$BlockName] 		= $BlockContent;
	$this->ArrayOrgBlocks[$BlockName]	= $BlockContent;
	
		$Replace = "<$BlockName::TEMP>";
	$this->TemplateContent = preg_replace($Pattern, $Replace, $this->TemplateContent);

	return;
	}

//*************************************************************************
// Define the block in an existing block and insert the block content into block pool
// ---
// IN		String		The name of the block
// IN		String		The name of the owner block
// OUT		Void		N/A
function DefineBlockInBlock($BlockName, $OwnerBlockName)
    {
	// Get the block content and insert it with it's name to associative array
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
	preg_match_all($Pattern, $this->ArrayBlocks[$OwnerBlockName], $Matches, PREG_SET_ORDER);
	$BlockContent = $Matches[0][1];
	
	$this->ArrayBlocks[$BlockName] 		= $BlockContent;
	$this->ArrayOrgBlocks[$BlockName]	= $BlockContent;
	
		$Replace = "<$BlockName::TEMP>";
	$this->ArrayBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayBlocks[$OwnerBlockName]);

	$this->ArrayOrgBlocks[$OwnerBlockName]	= $this->ArrayBlocks[$OwnerBlockName];

	return;
	}

//*************************************************************************
// Duplicate the block
// ---
// IN		String		The name of the target block
// IN		String		The name of the source block
// IN		Array		The list of the replacements
// OUT		Void		N/A
function DuplicateBlock($TargetBlock, $WhichBlock, $ArrayReplaceList)
    {
    $WhichBlockContent = $this->ArrayBlocks[$WhichBlock];
    $ToTag = "<$TargetBlock::TEMP>";

    // Process the given ArrayReplaceList on the block content
	foreach ($ArrayReplaceList as $Key=>$Value)
        {
        $WhichBlockContent = str_replace('_'.$Key.'_', $Value, $WhichBlockContent);
        }
            
        $Replace = $WhichBlockContent."\n".$ToTag;
    $this->TemplateContent = str_replace($ToTag, $Replace, $this->TemplateContent);

	// Reset the block content
	$this->ArrayBlocks[$WhichBlock] = $this->ArrayOrgBlocks[$WhichBlock];

	return;
	}

//*************************************************************************
// Duplicate the block in an existing block
// ---
// IN		String		The name of the source block
// IN		String		The name of the owner block
// IN		Array		The list of the replacements
// OUT		Void		N/A
function DuplicateBlockInBlock($WhichBlock, $OwnerBlock, $ArrayReplaceList)
    {
    $WhichBlockContent = $this->ArrayBlocks[$WhichBlock];
    $ToTag = "<$WhichBlock::TEMP>";

    // Process the given ArrayReplaceList on the block content
	foreach ($ArrayReplaceList as $Key=>$Value)
        {
        $WhichBlockContent = str_replace('_'.$Key.'_', $Value, $WhichBlockContent);
        }
            
        $Replace = $WhichBlockContent."\n".$ToTag;
    $this->ArrayBlocks[$OwnerBlock] = str_replace($ToTag, $Replace, $this->ArrayBlocks[$OwnerBlock]);

	// Reset the block content
	$this->ArrayBlocks[$WhichBlock] = $this->ArrayOrgBlocks[$WhichBlock];

	return;
	}

//*************************************************************************
// Removes the block from the template
// ---
// IN		String		The name of the block
// OUT		Void		N/A
function RemoveBlock($BlockName)
    {
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
		$Replace = '';
    $this->TemplateContent = preg_replace($Pattern, $Replace, $this->TemplateContent);

	return;
    }

//*************************************************************************
// Removes the block from owner block
// ---
// IN		String		The name of the block
// IN		String		The name of the owner block
// OUT		Void		N/A
function RemoveBlockInBlock($BlockName, $OwnerBlockName)
    {
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
		$Replace = '';
    $this->ArrayBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayBlocks[$OwnerBlockName]);
    $this->ArrayOrgBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayOrgBlocks[$OwnerBlockName]);

	return;
    }

//*************************************************************************
// Removes the block from the template
// ---
// IN		Void		N/A
// OUT		String		Parsed template content
function ParseAndOutput()
    {
    // Remove all trash temporary templates and tags from template_content
    // Clear all block tags (ex: <START> ... </START>) from template_content
	foreach ($this->ArrayBlocks as $Key=>$Value)
        {
            $Pattern	= "<$Key::TEMP>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
            $Pattern    =    "<$Key>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
            $Pattern    =    "</$Key>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
        }

    // Parse replaces
	foreach ($this->ArrayReplaceList as $Key=>$Value)
        {
            $Pattern    	= $Key;
            $Replace       	= $Value;
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
        }
    
    // Clear all variables except TemplateContent
    $this->Clear(true);

    return $this->TemplateContent;
    }

//*************************************************************************
// Clears all contents
// ---
// IN		Boolean		N/A
// OUT		Void		N/A
function Clear($SkipTemplateContent = false)
    {
    $this->Template 		= "";
    $this->TemplateType 	= "";
    $this->ArrayReplaceList	= array();
    $this->ArrayBlocks		= array();
    $this->Error 			= "";

    if ($SkipTemplateContent == false)
        {
        $this->TemplateContent = "";
        }

    return;
    }



//*************************************************************************
// PRIVATE METHODS
//*************************************************************************

//*************************************************************************
// Returns the error message
// ---
// IN		String		The built-in error message
// OUT		Boolean		Returns false always
function ReturnError($Message = "")
    {
    $this->Error = "Error: $Message";
    return false;
    }



}; // end of class

?>