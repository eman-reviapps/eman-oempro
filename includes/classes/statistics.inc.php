<?php

Core::LoadObject('subscribers');

/**
 * Lists statistics
 *
 * This class holds all statistics related functions
 * @package Oempro
 * @author Octeth
 * */
class Statistics extends Core {

    /**
     * Retrieves revenue statistics
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveRevenueStatistics($StartDate, $EndDate) {
        // $ArrayPaymentPeriods = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX.'users_payment_log'), $ArrayCriterias, array('PeriodEndDate' => 'DESC'), 0, 0, 'AND', false, false);
        // Set the date range - Start
        $NumberOfMonths = abs(ceil(((strtotime($EndDate) - strtotime($StartDate)) / 2678400)));
        // Set the date range - End

        $ArrayStatistics = array();
        $SQLQuery = 'SELECT SUM(TotalAmount) AS TotalAmount, DATE_FORMAT(PeriodEndDate, "%Y-%m") AS Date FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log' . ' WHERE (DATE_FORMAT(PeriodStartDate, "%Y-%m") >= "' . $StartDate . '" AND DATE_FORMAT(PeriodStartDate, "%Y-%m") <= "' . $EndDate . '") OR (DATE_FORMAT(PeriodEndDate, "%Y-%m") >= "' . $StartDate . '" AND DATE_FORMAT(PeriodEndDate, "%Y-%m") <= "' . $EndDate . '") GROUP BY crc32(Date)';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayStatistics[$EachRow['Date']] = array();
            $ArrayStatistics[$EachRow['Date']]['TotalAmount'] = $EachRow['TotalAmount'];
        }
        $ArrayMonths = array();

        // Generate the date range - Start
        for ($EachMonth = 0; $EachMonth <= $NumberOfMonths; $EachMonth++) {
            // Format the date - Start
            $FormattedDate = date('Y-m', strtotime(date('Y-m', strtotime($StartDate)) . ' +' . $EachMonth . ' months 00:00:00'));
            // Format the date - End

            if (isset($ArrayStatistics[$FormattedDate]['TotalAmount'])) {
                $ArrayMonths[$FormattedDate]['TotalAmount'] = $ArrayStatistics[$FormattedDate]['TotalAmount'];
            } else {
                $ArrayMonths[$FormattedDate]['TotalAmount'] = 0;
            }
        }
        // Generate the date range - End

        $SQLQuery = "SELECT SUM(NetAmount) AS TotalAmount, DATE_FORMAT(PurchaseDate, '%Y-%m') AS Date FROM " . MYSQL_TABLE_PREFIX . 'users_credits_purchase_log WHERE PaymentStatus="Paid" GROUP BY crc32(Date)';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachMonth = mysql_fetch_assoc($ResultSet)) {
            $ArrayMonths[$EachMonth['Date']]['TotalAmount'] += $EachMonth['TotalAmount'];
        }

        return $ArrayMonths;
    }

    /**
     * Retrieves total revenue
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveTotalRevenue() {
        $SQLQuery = 'SELECT SUM(TotalAmount) AS TotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayStatistic = mysql_fetch_assoc($ResultSet);

        $SQLQuery = "SELECT SUM(NetAmount) AS TotalAmount FROM " . MYSQL_TABLE_PREFIX . 'users_credits_purchase_log WHERE PaymentStatus="Paid"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $TMP = mysql_fetch_assoc($ResultSet);

        $ArrayStatistic['TotalAmount'] += $TMP['TotalAmount'];

        return $ArrayStatistic['TotalAmount'];
    }

    /**
     * Retrieves total revenue
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveTotalPaidRevenue() {
        $SQLQuery = 'SELECT SUM(TotalAmount) AS TotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log WHERE PaymentStatus = "Paid"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayStatistic = mysql_fetch_assoc($ResultSet);

        $SQLQuery = "SELECT SUM(NetAmount) AS TotalAmount FROM " . MYSQL_TABLE_PREFIX . 'users_credits_purchase_log WHERE PaymentStatus="Paid"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $TMP = mysql_fetch_assoc($ResultSet);

        $ArrayStatistic['TotalAmount'] += $TMP['TotalAmount'];

        return $ArrayStatistic['TotalAmount'];
    }

    /**
     * Retrieves total revenue
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveTotalNotPaidRevenue() {
        $SQLQuery = 'SELECT SUM(TotalAmount) AS TotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log WHERE PaymentStatus != "Paid"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayStatistic = mysql_fetch_assoc($ResultSet);

        return $ArrayStatistic['TotalAmount'];
    }

    /**
     * Retrieves all time top user revenues
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveAllTimeTopUserRevenues($count) {
        Core::LoadObject('users');

        $SQLQuery = 'SELECT *, SUM(TotalAmount) AS AllTimeTotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log GROUP BY RelUserID ORDER BY AllTimeTotalAmount DESC Limit 0, ' . $count;
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayResult = array();

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayResult[] = array(
                'PaymentPeriodInformation' => $EachRow,
                'UserInformation' => Users::RetrieveUser(array('*'), array('UserID' => $EachRow['RelUserID']))
            );
        }

        return $ArrayResult;
    }

    /**
     * Retrieves top user revenue for the given date
     *
     * @return void
     * @param $date	%Y-%m
     * @author Mert Hurturk
     * */
    public static function RetrieveTopUserRevenueForDate($date) {
        Core::LoadObject('users');

        $SQLQuery = 'SELECT * , MAX( TotalAmount ) AS MaxTotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log WHERE DATE_FORMAT(PeriodEndDate, "%Y-%m") = "' . $date . '" GROUP BY crc32(DATE_FORMAT(PeriodEndDate, "%Y-%m"))';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $MaxPeriodPurchaseTotal = mysql_fetch_assoc($ResultSet);

        $ArrayResult = array();
        $ArrayResult['PaymentPeriodInformation'] = $MaxPeriodPurchaseTotal;
        $ArrayResult['UserInformation'] = Users::RetrieveUser(array('*'), array('UserID' => $ArrayResult['PaymentPeriodInformation']['RelUserID']));

        return $ArrayResult;
    }

    /**
     * Records activity statistics of the subscriber list
     *
     * @param int $ListID Subscriber list ID
     * @param array $ArrayActivities Activity types in array with corresponding values (Types: 'TotalImport', 'TotalSubscriptions', 'TotalUnsubscriptions', 'TotalHardBounce', 'TotalSoftBounce')
     * @return void
     * @author Cem Hurturk
     * */
    public static function UpdateListActivityStatistics($ListID, $UserID, $ArrayActivities) {
        $CurrentDate = date('Y-m-d');

        $ArrayActivityStatistics = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array('RelListID' => $ListID, 'RelOwnerUserID' => $UserID, 'StatisticsDate' => $CurrentDate), array(), 0, 0, 'AND', false);

        if (count($ArrayActivityStatistics) > 0) {
            // Update existing statistics
            $ArrayActivityStatistics = $ArrayActivityStatistics[0];

            $ArrayFieldnValues = array();
            foreach ($ArrayActivities as $Key => $Value) {
                $ArrayFieldnValues[$Key] = $ArrayActivityStatistics[$Key] + $Value;
            }
            Database::$Interface->UpdateRows($ArrayFieldnValues, array('RelListID' => $ListID, 'RelOwnerUserID' => $UserID, 'StatisticsDate' => $CurrentDate), MYSQL_TABLE_PREFIX . 'stats_activity');
        } else {
            // Create the new statistics
            $ArrayFieldnValues = array(
                'StatisticsID' => '',
                'RelOwnerUserID' => $UserID,
                'RelListID' => $ListID,
                'StatisticsDate' => $CurrentDate,
            );
            foreach ($ArrayActivities as $Key => $Value) {
                $ArrayFieldnValues[$Key] = $Value;
            }

            Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_activity');
        }
    }

    /**
     * Registers the detailed unsubscription report
     *
     * @param string $UserID 
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $CampaignID 
     * @param string $Channel 
     * @return void
     * @author Cem Hurturk
     */
    public static function RegisterUnsubscriptionTrack($UserID, $SubscriberID, $ListID, $CampaignID, $Channel, $AutoResponderID = 0, $EmailID = 0) {
        $ArrayFieldnValues = array(
            'StatID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'UnsubscriptionDate' => date('Y-m-d H:i:s'),
            'Channel' => $Channel,
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_unsubscription');

        return;
    }

    /**
     * Retrieves activity statistics of the subscriber list
     *
     * @param int $ListID Subscriber list ID
     * @param array $UserID Owner user id of subscriber list
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveListActivityStatistics($ListID, $UserID, $FromDate, $ToDate) {
        if ($ListID == 0) {
            $ArrayActivityStatistics = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array(array('field' => 'RelListID', 'operator' => '>', 'value' => 0), 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);
        } else {
            $ArrayActivityStatistics = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array('RelListID' => $ListID, 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);
        }

        $ArrayConvertedStatistics = array();
        foreach ($ArrayActivityStatistics as $EachStatistic) {
            $ArrayConvertedStatistics[$EachStatistic['StatisticsDate']] = $EachStatistic;
        }

        // Initialize variables - Start
        $ArrayDays = array();
        // Initialize variables - End
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayConvertedStatistics[$FormattedDate]) {
                $ArrayDays[$FormattedDate] = array(
                    "TotalSubscriptions" => $ArrayConvertedStatistics[$FormattedDate]['TotalSubscriptions'],
                    "TotalUnsubscriptions" => $ArrayConvertedStatistics[$FormattedDate]['TotalUnsubscriptions'],
                    "TotalImport" => $ArrayConvertedStatistics[$FormattedDate]['TotalImport'],
                    "TotalSoftBounce" => $ArrayConvertedStatistics[$FormattedDate]['TotalSoftBounce'],
                    "TotalHardBounce" => $ArrayConvertedStatistics[$FormattedDate]['TotalHardBounce']
                );
            } else {
                $ArrayDays[$FormattedDate] = array(
                    "TotalSubscriptions" => 0,
                    "TotalUnsubscriptions" => 0,
                    "TotalImport" => 0,
                    "TotalSoftBounce" => 0,
                    "TotalHardBounce" => 0
                );
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /*
     * Eman
     * */

    public static function RetrieveListTotalOpens($ListID, $UserID, $TotalSent) {

        $ArrayStatistics = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['TotalOpens'] = $Row['TotalOpens'];
            $Percentage = round(((100 * $Row['TotalOpens']) / $TotalSent), 1);
            $ArrayStatistics['TotalOpensPercent'] = $Percentage;
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT COUNT(DISTINCT RelSubscriberID) AS UniqueOpens FROM  " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['UniqueOpens'] = $Row['UniqueOpens'];
            $Percentage = round(((100 * $Row['UniqueOpens']) / $TotalSent), 1);
            $ArrayStatistics['UniqueOpensPercent'] = $Percentage;
        }
        // Unique opens calculation - End

        return $ArrayStatistics;
    }

    /**
     * Retrieves open statistics of the subscriber list
     *
     * @param int $ListID Subscriber list ID
     * @param array $UserID Owner user id of subscriber list
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveListOpenStatistics($ListID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalOpens'];
            }
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueOpens FROM  " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueOpens'];
            }
        }
        // Unique opens calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Retrieves open statistics of the auto responder
     *
     * @param int $ListID Auto Responder ID
     * @param array $UserID Owner user id of auto responder
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveAutoResponderOpenStatistics($AutoResponderID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalOpens'];
            }
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueOpens FROM  " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueOpens'];
            }
        }
        // Unique opens calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /*
     * Eman
     * */

    public static function RetrieveListTotalClicks($ListID, $UserID, $TotalSent) {

        $ArrayStatistics = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['TotalClicks'] = $Row['TotalClicks'];
            $Percentage = round(((100 * $Row['TotalClicks']) / $TotalSent), 1);
            $ArrayStatistics['TotalClicksPercent'] = $Percentage;
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT COUNT(DISTINCT RelSubscriberID) AS UniqueClicks FROM  " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['UniqueClicks'] = $Row['UniqueClicks'];
            $Percentage = round(((100 * $Row['UniqueClicks']) / $TotalSent), 1);
            $ArrayStatistics['UniqueClicksPercent'] = $Percentage;
        }
        // Unique opens calculation - End

        return $ArrayStatistics;
    }

    /**
     * Returns the link click statistics of the subscriber list for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveListClickStatistics($ListID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total link clicks calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalClicks'];
            }
        }
        // Total link clicks calculation - End
        // Unique link click calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueClicks FROM  " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueClicks'];
            }
        }
        // Unique link click calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the link click statistics of the auto responder for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderClickStatistics($AutoResponderID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total link clicks calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalClicks'];
            }
        }
        // Total link clicks calculation - End
        // Unique link click calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueClicks FROM  " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueClicks'];
            }
        }
        // Unique link click calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    //By Eman
    public static function RetrieveListTotalUnSubscribes($ListID, $UserID) {

        $ArrayStatistics = array();

        $SQLQuery = "SELECT IFNULL(SUM(TotalSubscriptions),0) AS TotalSubscriptions, IFNULL(SUM(TotalUnsubscriptions),0) AS TotalUnsubscriptions FROM " . MYSQL_TABLE_PREFIX . "stats_activity WHERE RelListID='" . $ListID . "' AND RelOwnerUserID = '" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['TotalSubscriptions'] = $Row['TotalSubscriptions'];
            $ArrayStatistics['TotalUnsubscriptions'] = $Row['TotalUnsubscriptions'];
            $ArrayStatistics['TUnsubscriptionsPercent'] = round(((100 * $Row['TotalUnsubscriptions']) / $Row['TotalSubscriptions']), 1);
        }
        return $ArrayStatistics;
    }

    /**
      Eman
     * */
    public static function RetrieveListTotalForward($ListID, $UserID, $TotalSent) {
        $ArrayStatistics = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['TotalForwards'] = $Row['TotalForwards'];
            $Percentage = round(((100 * $Row['TotalForwards']) / $TotalSent), 1);
            $ArrayStatistics['TotalForwardsPercent'] = $Percentage;
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT COUNT(DISTINCT RelSubscriberID) AS UniqueForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            $Row = mysql_fetch_assoc($ResultSet);
            $ArrayStatistics['UniqueForwards'] = $Row['UniqueForwards'];
            $Percentage = round(((100 * $Row['UniqueForwards']) / $TotalSent), 1);
            $ArrayStatistics['UniqueForwardsPercent'] = $Percentage;
        }
        // Unique opens calculation - End

        return $ArrayStatistics;
    }

    /**
     * Returns the forward statistics of the subscriber list for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveListForwardStatistics($ListID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total forwards calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalForwards'];
            }
        }
        // Total forwards calculation - End
        // Unique forward calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueForwards'];
            }
        }
        // Unique forward calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the forward statistics of the subscriber list for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderForwardStatistics($AutoResponderID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total forwards calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalForwards'];
            }
        }
        // Total forwards calculation - End
        // Unique forward calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueForwards'];
            }
        }
        // Unique forward calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the browser view statistics of the subscriber list for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveListBrowserViewStatistics($ListID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalBrowserViews'];
            }
        }
        // Total browser views calculation - End
        // Unique browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueBrowserViews'];
            }
        }
        // Unique browser views calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the browser view statistics of the subscriber list for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderBrowserViewStatistics($AutoResponderID, $UserID, $FromDate, $ToDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($ToDate) - strtotime($FromDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalBrowserViews'];
            }
        }
        // Total browser views calculation - End
        // Unique browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')>='" . $FromDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueBrowserViews'];
            }
        }
        // Unique browser views calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime($FromDate . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the list of email domains,their totals and percentages in a list
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveListEmailDomainStatistics($ListID, $TotalSubscribers, $LimitBy = 100) {
        $SQLQuery = "SELECT COUNT(*) AS TotalEmails, SUBSTRING_INDEX(EmailAddress, '@', -1) AS Domain FROM " . MYSQL_TABLE_PREFIX . "subscribers_" . $ListID . " WHERE BounceType!='Hard' AND (SubscriptionStatus='Subscribed' OR SubscriptionStatus='Opt-Out Pending') GROUP BY crc32(SUBSTRING_INDEX(EmailAddress, '@', -1)) ORDER BY TotalEmails DESC LIMIT 0, " . $LimitBy;
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) == 0) {
            return array();
        } else {
            $ArrayDomains = array();
            while ($EachDomain = mysql_fetch_assoc($ResultSet)) {
                $ArrayDomains[] = array("domain" => $EachDomain['Domain'], "count" => $EachDomain['TotalEmails']);
            }

            $TMPCounter = 0;

            $ArrayReturn = array();
            $TotalOthers = $TotalSubscribers;
            foreach ($ArrayDomains as $ArrayEachDomain) {
                $TMPPercentage = round((100 * $ArrayEachDomain['count']) / $TotalSubscribers);
                $ArrayReturn[] = array("domain" => $ArrayEachDomain['domain'], "count" => $ArrayEachDomain['count'], "percentage" => $TMPPercentage);
                $TotalOthers -= $ArrayEachDomain['count'];
                $TMPCounter++;
            }

            // Other email domains - Start
            if ($TotalOthers > 0) {
                $TMPPercentage = round((100 * $TotalOthers) / $TotalSubscribers);
                $ArrayReturn[] = array("domain" => 'others', "count" => $TotalOthers, "percentage" => $TMPPercentage);
            }
            // Other email domains - End

            return $ArrayReturn;
        }
    }

    /*
     * Eman
     * */

    public static function RetrieveListTotalSent($ListID) {
        $SQLQuery = "SELECT IFNULL(SUM(TotalSent),0) AS TotalSent FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns INNER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelListID='" . $ListID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $Result = mysql_fetch_assoc($ResultSet);

        return $Result['TotalSent'];
    }

    /**
     * Eman
     * */
    public static function RetrieveListTotalBounces($ListID, $UserID, $TotalSent) {
        $SQLQuery = "SELECT IFNULL(TotalHardBounce,0),IFNULL(TotalSoftBounce,0) FROM " . MYSQL_TABLE_PREFIX . "stats_activity WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) == 0) {
            return array("TotalBounces" => 0, "BouncesPercent" => 0);
        } else {
            $Result = mysql_fetch_assoc($ResultSet);
            $TotalBounces = $Result['TotalHardBounce'] + $Result['TotalSoftBounce'];
            $BouncePercentage = round(((100 * $TotalBounces) / $TotalSent), 1);
            return array("TotalBounces" => $TotalBounces, "BouncesPercent" => $BouncePercentage);
        }
    }

    /**
     * Returns bounce totals and percentages in a list
     *
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveListBounceStatistics($ListID, $TotalSubscribers) {
        $SQLQuery = "SELECT COUNT(*) AS TotalSubscribers, BounceType FROM " . MYSQL_TABLE_PREFIX . "subscribers_" . $ListID . " WHERE (SubscriptionStatus='Subscribed' OR SubscriptionStatus='Opt-Out Pending') GROUP BY crc32(BounceType) ORDER BY TotalSubscribers DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) == 0) {
            return array();
        } else {
            $ArrayReturn = array();
            while ($EachStatus = mysql_fetch_assoc($ResultSet)) {
                $TMPPercentage = round((100 * $EachStatus['TotalSubscribers']) / $TotalSubscribers);
                $ArrayReturn[] = array("status" => $EachStatus['BounceType'], "count" => $EachStatus['TotalSubscribers'], "percentage" => $TMPPercentage);
            }

            return $ArrayReturn;
        }
    }

    /**
     * Returns the total number of opens of a subscriber for a campaign
     *
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $CampaignID 
     * @return integer
     * @author Cem Hurturk
     */
    public static function RetrieveCampaignOpenStatisticsOfSubscriber($SubscriberID, $ListID, $CampaignID, $EmailID = 0) {
        if ($EmailID == 0) {
            $TotalOpens = Database::$Interface->GetRows(array('COUNT(*) AS TotalOpens'), array(MYSQL_TABLE_PREFIX . 'stats_open'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        } else {
            $TotalOpens = Database::$Interface->GetRows(array('COUNT(*) AS TotalOpens'), array(MYSQL_TABLE_PREFIX . 'stats_open'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID, 'RelEmailID' => $EmailID), array(), 0, 0, 'AND', false, false);
        }

        $TotalOpens = $TotalOpens[0]['TotalOpens'];
        return $TotalOpens;
    }

    /**
     * Returns the total number of opens of a subscriber for an auto responder
     *
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $CampaignID 
     * @return integer
     * @author Cem Hurturk
     */
    public static function RetrieveAutoResponderOpenStatisticsOfSubscriber($SubscriberID, $ListID, $AutoResponderID) {
        $TotalOpens = Database::$Interface->GetRows(array('COUNT(*) AS TotalOpens'), array(MYSQL_TABLE_PREFIX . 'stats_open'), array('RelAutoResponderID' => $AutoResponderID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        $TotalOpens = $TotalOpens[0]['TotalOpens'];
        return $TotalOpens;
    }

    /**
     * Returns the number of link click amount of subscriber for the campaign
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignLinkStatisticsOfSubscriber($SubscriberID, $ListID, $CampaignID, $EmailID = 0) {
        if ($EmailID == 0) {
            $TotalClicks = Database::$Interface->GetRows(array('COUNT(*) AS TotalClicks'), array(MYSQL_TABLE_PREFIX . 'stats_link'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        } else {
            $TotalClicks = Database::$Interface->GetRows(array('COUNT(*) AS TotalClicks'), array(MYSQL_TABLE_PREFIX . 'stats_link'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID, 'RelEmailID' => $EmailID), array(), 0, 0, 'AND', false, false);
        }

        $TotalClicks = $TotalClicks[0]['TotalClicks'];
        return $TotalClicks;
    }

    /**
     * Returns the number of link click amount of subscriber for the auto-responder
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderLinkStatisticsOfSubscriber($SubscriberID, $ListID, $AutoResponderID) {
        $TotalClicks = Database::$Interface->GetRows(array('COUNT(*) AS TotalClicks'), array(MYSQL_TABLE_PREFIX . 'stats_link'), array('RelAutoResponderID' => $AutoResponderID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        $TotalClicks = $TotalClicks[0]['TotalClicks'];
        return $TotalClicks;
    }

    /**
     * Returns the number of web browser views of subscriber for the campaign
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignWebBrowserStatisticsOfSubscriber($SubscriberID, $ListID, $CampaignID, $EmailID) {
        $TotalViews = Database::$Interface->GetRows(array('COUNT(*) AS TotalViews'), array(MYSQL_TABLE_PREFIX . 'stats_browserview'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID, 'RelEmailID' => $EmailID), array(), 0, 0, 'AND', false, false);
        $TotalViews = $TotalViews[0]['TotalViews'];
        return $TotalViews;
    }

    /**
     * Returns the number of web browser views of subscriber for the auto responder
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderWebBrowserStatisticsOfSubscriber($SubscriberID, $ListID, $AutoResponderID) {
        $TotalViews = Database::$Interface->GetRows(array('COUNT(*) AS TotalViews'), array(MYSQL_TABLE_PREFIX . 'stats_browserview'), array('RelAutoResponderID' => $AutoResponderID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        $TotalViews = $TotalViews[0]['TotalViews'];
        return $TotalViews;
    }

    /**
     * Returns the number of forwards of subscriber for the campaign
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignForwardStatisticsOfSubscriber($SubscriberID, $ListID, $CampaignID, $EmailID = 0) {
        if ($EmailID == 0) {
            $TotalForwards = Database::$Interface->GetRows(array('COUNT(*) AS TotalForwards'), array(MYSQL_TABLE_PREFIX . 'stats_forward'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        } else {
            $TotalForwards = Database::$Interface->GetRows(array('COUNT(*) AS TotalForwards'), array(MYSQL_TABLE_PREFIX . 'stats_forward'), array('RelCampaignID' => $CampaignID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID, 'RelEmailID' => $EmailID), array(), 0, 0, 'AND', false, false);
        }
        $TotalForwards = $TotalForwards[0]['TotalForwards'];
        return $TotalForwards;
    }

    /**
     * Returns the number of forwards of subscriber for the auto responder
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderForwardStatisticsOfSubscriber($SubscriberID, $ListID, $AutoResponderID) {
        $TotalForwards = Database::$Interface->GetRows(array('COUNT(*) AS TotalForwards'), array(MYSQL_TABLE_PREFIX . 'stats_forward'), array('RelAutoResponderID' => $AutoResponderID, 'RelSubscriberID' => $SubscriberID, 'RelListID' => $ListID), array(), 0, 0, 'AND', false, false);
        $TotalForwards = $TotalForwards[0]['TotalForwards'];
        return $TotalForwards;
    }

    /**
     * Adds the open track to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterOpenTrack($SubscriberID, $ListID, $CampaignID, $UserID, $AutoResponderID = 0, $EmailID = 0) {
        $ArrayFieldnValues = array(
            'OpenID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'OpenDate' => date('Y-m-d H:i:s'),
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_open');

        return;
    }

    /**
     * Adds the open track to the report table
     *
     * @return void
     * @author Eman
     * */
    public static function RegisterOpenTrackWithIP($SubscriberID, $ListID, $CampaignID, $UserID, $AutoResponderID = 0, $EmailID = 0, $Subscriber_IP = '', $Open_City = '', $Open_Country = '') {
        $ArrayFieldnValues = array(
            'OpenID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'OpenDate' => date('Y-m-d H:i:s'),
            'Subscriber_IP' => $Subscriber_IP,
            'Open_City' => $Open_City,
            'Open_Country' => $Open_Country,
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_open');

        return;
    }

    /**
     * Adds the forward track to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterForwardTrack($SubscriberID, $ListID, $CampaignID, $UserID, $AutoResponderID = 0, $EmailID = 0) {
        $ArrayFieldnValues = array(
            'StatID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'ForwardDate' => date('Y-m-d H:i:s'),
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_forward');

        return;
    }

    /**
     * Adds the link click track to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterLinkClickTrack($SubscriberID, $ListID, $CampaignID, $UserID, $LinkURL, $LinkTitle, $AutoResponderID = 0, $EmailID = 0) {
        $ArrayFieldnValues = array(
            'LinkTrackID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'LinkURL' => $LinkURL,
            'LinkTitle' => $LinkTitle,
            'ClickDate' => date('Y-m-d H:i:s'),
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_link');

        return;
    }

    /**
     * Adds the link click track to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterLinkClickTrackWithIP($SubscriberID, $ListID, $CampaignID, $UserID, $LinkURL, $LinkTitle, $AutoResponderID = 0, $EmailID = 0, $Subscriber_IP = '', $Click_City = '', $Click_Country = '') {
        $ArrayFieldnValues = array(
            'LinkTrackID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'LinkURL' => $LinkURL,
            'LinkTitle' => $LinkTitle,
            'ClickDate' => date('Y-m-d H:i:s'),
            'Subscriber_IP' => $Subscriber_IP,
            'Click_City' => $Click_City,
            'Click_Country' => $Click_Country,
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_link');

        return;
    }

    /**
     * Adds the bounce detection to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    function RegisterBounceDetection($UserID, $SubscriberID, $ListID, $CampaignID, $BounceType, $AutoResponderID = 0) {
        $ArrayFieldnValues = array(
            'StatID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'BounceType' => $BounceType,
            'BounceDate' => date('Y-m-d H:i:s'),
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_bounce');

        return;
    }

    /**
     * Returns the total number of clicks that recipient clicked for that URL
     *
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $CampaignID 
     * @param string $UserID 
     * @param string $LinkURL 
     * @return integer
     * @author Cem Hurturk
     */
    public static function SubscriberLinkClickTotal($SubscriberID, $ListID, $CampaignID, $UserID, $LinkURL, $AutoResponderID, $EmailID = 0) {
        $ArrayCriteria = array(
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'LinkURL' => $LinkURL,
        );
        $TotalFound = Database::$Interface->GetRows(array('COUNT(*) AS TotalFound'), array(MYSQL_TABLE_PREFIX . 'stats_link'), $ArrayCriteria, array(), 0, 0, 'AND', false, false);
        $TotalFound = $TotalFound[0]['TotalFound'];

        return $TotalFound;
    }

    /**
     * Returns the total number of campaign opens that recipient for given campaign id
     *
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $CampaignID 
     * @param string $UserID 
     * @return integer
     * @author Mert Hurturk
     */
    public static function SubscriberCampaignOpenTotal($SubscriberID, $ListID, $CampaignID, $UserID) {
        $ArrayCriteria = array(
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
        );
        $TotalFound = Database::$Interface->GetRows(array('COUNT(*) AS TotalFound'), array(MYSQL_TABLE_PREFIX . 'stats_open'), $ArrayCriteria, array(), 0, 0, 'AND', false, false);
        $TotalFound = $TotalFound[0]['TotalFound'];

        return $TotalFound;
    }

    /**
     * Calculates the open rates for days of list
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveOpenPerformanceDaysOfList($ListID, $UserID, $ArrayLanguages = array()) {
        if (count($ArrayLanguages) < 1) {
            global $ArrayLanguageStrings;
            $ArrayLanguages = $ArrayLanguageStrings['Screen']['9049'];
        }

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalOpens, DATE_FORMAT(OpenDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_open` WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( OpenDate,  '%w' )) ORDER BY TotalOpens DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguages[$EachRow['DayOfWeek']], $EachRow['TotalOpens']);
        }

        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the open rates for days of list
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveOpenPerformanceDaysOfAutoResponder($AutoResponderID, $UserID) {
        global $ArrayLanguageStrings;

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalOpens, DATE_FORMAT(OpenDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_open` WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( OpenDate,  '%w' )) ORDER BY TotalOpens DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguageStrings['Screen']['9049'][$EachRow['DayOfWeek']], $EachRow['TotalOpens']);
        }

        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the open rates for days of campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveOpenPerformanceDaysOfCampaign($CampaignID, $UserID, $ArrayLanguages = array()) {
        if (count($ArrayLanguages) < 1) {
            global $ArrayLanguageStrings;
            $ArrayLanguages = $ArrayLanguageStrings['Screen']['9049'];
        }

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalOpens, DATE_FORMAT(OpenDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_open` WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( OpenDate,  '%w' )) ORDER BY TotalOpens DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguages[$EachRow['DayOfWeek']], $EachRow['TotalOpens']);
        }
        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the click rates for days of list
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveClickPerformanceDaysOfList($ListID, $UserID, $ArrayLanguages = array()) {
        if (count($ArrayLanguages) < 1) {
            global $ArrayLanguageStrings;
            $ArrayLanguages = $ArrayLanguageStrings['Screen']['9049'];
        }

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalClicks, DATE_FORMAT(ClickDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_link` WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( ClickDate,  '%w' )) ORDER BY TotalClicks DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguages[$EachRow['DayOfWeek']], $EachRow['TotalClicks']);
        }

        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the click rates for days of auto responder
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveClickPerformanceDaysOfAutoResponder($AutoResponderID, $UserID) {
        global $ArrayLanguageStrings;

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalClicks, DATE_FORMAT(ClickDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_link` WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( ClickDate,  '%w' )) ORDER BY TotalClicks DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguageStrings['Screen']['9049'][$EachRow['DayOfWeek']], $EachRow['TotalClicks']);
        }

        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the click rates for days of campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveClickPerformanceDaysOfCampaign($CampaignID, $UserID, $ArrayLanguages = array()) {
        if (count($ArrayLanguages) < 1) {
            global $ArrayLanguageStrings;
            $ArrayLanguages = $ArrayLanguageStrings['Screen']['9049'];
        }

        // Subscriber list specific statistics
        $SQLQuery = "SELECT COUNT(*) AS TotalClicks, DATE_FORMAT(ClickDate,  '%W') AS DayOfWeek FROM `" . MYSQL_TABLE_PREFIX . "stats_link` WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(DATE_FORMAT( ClickDate,  '%w' )) ORDER BY TotalClicks DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayHighestOpenPerformanceDays = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayHighestOpenPerformanceDays[] = array($ArrayLanguages[$EachRow['DayOfWeek']], $EachRow['TotalClicks']);
        }

        return $ArrayHighestOpenPerformanceDays;
    }

    /**
     * Calculates the open performance of a specific subscriber list or overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallOpenPerformanceOfList($ListID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueOpens) AS TotalUniqueOpens FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns INNER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelListID='" . $ListID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);

        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueOpens']) / $ArrayTotals['TotalSent']);

        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Calculates the open performance of a specific subscriber campaign or overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallOpenPerformanceOfCampaign($CampaignID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueOpens) AS TotalUniqueOpens FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns INNER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelCampaignID='" . $CampaignID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);

        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueOpens']) / $ArrayTotals['TotalSent']);

        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Calculates the open performance of account overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallOpenPerformanceOfAccount($UserID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueOpens) AS TotalUniqueOpens FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns LEFT OUTER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelOwnerUserID='" . $UserID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);
        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueOpens']) / $ArrayTotals['TotalSent']);
        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Calculates the link performance of a specific subscriber list or overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallClickPerformanceOfList($ListID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueClicks) AS TotalUniqueClicks FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns INNER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelListID='" . $ListID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);

        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueClicks']) / $ArrayTotals['TotalSent']);

        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Calculates the link performance of a specific subscriber campaign or overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallClickPerformanceOfCampaign($CampaignID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueClicks) AS TotalUniqueClicks FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns INNER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblAssign.RelCampaignID='" . $CampaignID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);

        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueClicks']) / $ArrayTotals['TotalSent']);

        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Calculates the link performance of account overall
     *
     * @return int
     * @author Cem Hurturk
     * */
    public static function RetrieveOverallClickPerformanceOfAccount($UserID) {
        $SQLQuery = "SELECT SUM(TotalSent) AS TotalSent, SUM(UniqueClicks) AS TotalUniqueClicks FROM " . MYSQL_TABLE_PREFIX . "campaigns AS tblCampaigns LEFT OUTER JOIN " . MYSQL_TABLE_PREFIX . "campaign_recipients AS tblAssign ON tblAssign.RelCampaignID = tblCampaigns.CampaignID WHERE tblCampaigns.RelOwnerUserID='" . $UserID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayTotals = mysql_fetch_assoc($ResultSet);

        $OpenPerformance = round((100 * $ArrayTotals['TotalUniqueClicks']) / $ArrayTotals['TotalSent']);

        $OpenPerformance = ($OpenPerformance == '' ? 0 : $OpenPerformance);

        return $OpenPerformance;
    }

    /**
     * Adds the web browser view track to the report table
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterWebBrowserView($SubscriberID, $ListID, $CampaignID, $UserID, $AutoResponderID = 0, $EmailID = 0) {
        $ArrayFieldnValues = array(
            'StatID' => '',
            'RelOwnerUserID' => $UserID,
            'RelCampaignID' => $CampaignID,
            'RelEmailID' => $EmailID,
            'RelAutoResponderID' => $AutoResponderID,
            'RelListID' => $ListID,
            'RelSubscriberID' => $SubscriberID,
            'ViewDate' => date('Y-m-d H:i:s'),
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'stats_browserview');

        return;
    }

    /**
     * Returns given statistics data as CSV formatted
     *
     * @param string $ArrayStatisticData 
     * @return string
     * @author Mert Hurturk
     */
    public static function ConvertToCSV($ArrayStatisticData, $AddFirstHeaderName = '') {
        $CSVString = "";
        $Keys = array();
        foreach ($ArrayStatisticData as $Each) {
            $Keys = array_keys($Each);
            break;
        }

        $CSVString .= ($AddFirstHeaderName != '' ? '"Date",' : '') . '"' . implode('","', $Keys) . "\"\n";

        foreach ($ArrayStatisticData as $Key => $Value) {
            if ($AddFirstHeaderName != '') {
                $CSVString .= "\"" . $Key . "\",";
            }
            foreach ($Value as $Stat) {
                $CSVString .= "\"" . $Stat . "\",";
            }
            $CSVString = substr($CSVString, 0, -1);
            $CSVString .= "\n";
        }

        return $CSVString;
    }

    /**
     * Returns given statistics data as XML formatted
     *
     * @param string $ArrayStatisticData 
     * @return string
     * @author Mert Hurturk
     */
    public static function ConvertToXML($ArrayStatisticData, $AddFirstHeaderName = '') {
        $XMLString = "";

        foreach ($ArrayStatisticData as $Key => $Value) {
            $XMLString .= '<statistic>';
            if ($AddFirstHeaderName != '') {
                $XMLString .= '<date><![CDATA[' . $Key . ']]></date>';
            }
            foreach ($Value as $Key => $Value) {
                $XMLString .= '<' . $Key . '><![CDATA[' . $Value . ']]></' . $Key . '>';
            }
            $XMLString .= '</statistic>';
        }

        return $XMLString;
    }

    /**
     * Deletes statistics of a list, campaign, subscriber, etc.
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function Delete($ListID = 0, $CampaignID = 0, $SubscriberID = 0, $UserID = 0) {
        if ($ListID > 0) {
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_activity');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_bounce');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_browserview');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_forward');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_link');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_open');
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'stats_unsubscription');
        } elseif ($SubscriberID > 0) {
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_bounce');
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_browserview');
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_forward');
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_link');
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_open');
            Database::$Interface->DeleteRows(array('RelSubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'stats_unsubscription');
        } elseif ($CampaignID > 0) {
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_bounce');
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_browserview');
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_forward');
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_link');
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_open');
            Database::$Interface->DeleteRows(array('RelCampaignID' => $CampaignID), MYSQL_TABLE_PREFIX . 'stats_unsubscription');
        } elseif ($UserID > 0) {
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_activity');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_bounce');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_browserview');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_forward');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_link');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_open');
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'stats_unsubscription');
        }
    }

    //By Eman
    public static function RetrieveUserOpenStatistics($UserID, $StartDate, $FinishDate) {
        //open,click,subscribe
        //1 - open

        $ArrayOpens = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m') AS StatDate, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m')>='" . $StartDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m')) order by (DATE_FORMAT(OpenDate, '%Y-%m'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "StatDate" => $EachRow['StatDate'],
                    "TotalOpens" => $EachRow['TotalOpens'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    //By Eman
    public static function RetrieveUserSubscribeStatistics($UserID, $StartDate, $FinishDate) {


        $ArraySubscribes = array();
        $SQLQuery = "SELECT DATE_FORMAT(StatisticsDate, '%Y-%m') AS StatisticsDate, SUM(IFNULL(TotalSubscriptions,0)+IFNULL(TotalImport,0)) AS TotalSubscriptions, SUM(IFNULL(TotalUnsubscriptions,0)+IFNULL(TotalMoved,0)+IFNULL(TotalSuppressed,0)) AS TotalUnsubscriptions FROM " . MYSQL_TABLE_PREFIX . "stats_activity WHERE RelOwnerUserID = '" . $UserID . "' AND DATE_FORMAT(StatisticsDate, '%Y-%m')>='" . $StartDate . "' AND DATE_FORMAT(StatisticsDate, '%Y-%m')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(StatisticsDate, '%Y-%m')) order by (DATE_FORMAT(StatisticsDate, '%Y-%m'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
//
//        while ($EachRow = mysql_fetch_assoc($Result)) {
//            $ArraySubscribes[$EachRow['StatisticsDate']] = array('TotalSubscriptions' => $EachRow['TotalSubscriptions'], 'TotalUnsubscriptions' => $EachRow['TotalUnsubscriptions']);
//        }

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "StatisticsDate" => $EachRow['StatisticsDate'],
                    "TotalSubscriptions" => $EachRow['TotalSubscriptions'],
                );
                $ArraySubscribes[] = $row;
            }
        }

        return $ArraySubscribes;
    }

    //By Eman
    public static function RetrieveUserClickStatistics($UserID, $StartDate, $FinishDate) {


        $ArrayClicks = array();

        // Total link clicks calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m') AS StatDate, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m')>='" . $StartDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m')) order by (DATE_FORMAT(ClickDate, '%Y-%m'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "StatDate" => $EachRow['StatDate'],
                    "TotalClicks" => $EachRow['TotalClicks'],
                );
                $ArrayClicks[] = $row;
            }
        }


        return $ArrayClicks;
    }

    //By Eman
    public static function RetrieveUserGeoPeopleStatistics($UserID, $StartDate = null, $FinishDate = null) {

        $SQLQuery1 = "SHOW TABLES LIKE 'oempro_octautomation_people_$UserID'";
        $ResultSet1 = Database::$Interface->ExecuteQuery($SQLQuery1);

        if (mysql_num_rows($ResultSet1) < 1) {
            return array();
        }

        $ArrayOpens = array();
//        $SQLQuery = "SELECT    LOWER(Country) Poeple_Country,COUNT(*) People_Count,"
////                . " City Country_Name"
//                . " (select  country_name from ip2location_db9 where country_code = Country limit 1) Country_Name"
//                . "  FROM      oempro_octautomation_people_$UserID "
//                . "  WHERE     RelUserID = $UserID"
//                . "  AND       Country IS NOT NULL AND Country <> ''";
        $SQLQuery = "SELECT    LOWER(Country) Poeple_Country,COUNT(*) People_Count"
                . "  FROM      oempro_octautomation_people_$UserID "
                . "  WHERE     RelUserID = $UserID"
                . "  AND       Country IS NOT NULL AND Country <> ''";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(EntryDate, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(EntryDate, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Poeple_Country" => $EachRow['Poeple_Country'],
                    "People_Count" => $EachRow['People_Count'],
//                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    //By Eman
    public static function RetrieveUserGeoOpenStatistics($UserID, $StartDate = null, $FinishDate = null) {

        $SQLQuery1 = "SHOW TABLES LIKE 'oempro_octautomation_people_delivery_log_$UserID'";
        $ResultSet1 = Database::$Interface->ExecuteQuery($SQLQuery1);

        if (mysql_num_rows($ResultSet1) < 1) {
            return array();
        }

        $ArrayOpens = array();
//        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Open_Count,"
//                . " (select  country_name from ip2location_db9 where country_code = Open_Country limit 1) Country_Name"
//                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
//                . "  WHERE     IsOpened='Yes'"
//                . "  AND       RelUserID = $UserID";
        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Open_Count"
                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
                . "  WHERE     IsOpened='Yes'"
                . "  AND       RelUserID = $UserID";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(Open_Date, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(Open_Date, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Open_Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Open_Country" => $EachRow['Open_Country'],
                    "Open_Count" => $EachRow['Open_Count'],
//                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    //By Eman
    public static function RetrieveUserGeoClickStatistics($UserID, $StartDate = null, $FinishDate = null) {

        $SQLQuery1 = "SHOW TABLES LIKE 'oempro_octautomation_people_delivery_log_$UserID'";
        $ResultSet1 = Database::$Interface->ExecuteQuery($SQLQuery1);

        if (mysql_num_rows($ResultSet1) < 1) {
            return array();
        }

        $ArrayOpens = array();

//        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Click_Count,"
//                . " (select  country_name from ip2location_db9 where country_code = Open_Country limit 1) Country_Name"
//                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
//                . "  WHERE     IsClicked='Yes'"
//                . "  AND       RelUserID = $UserID";
        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Click_Count"
                . "  FROM      oempro_octautomation_people_delivery_log_$UserID "
                . "  WHERE     IsClicked='Yes'"
                . "  AND       RelUserID = $UserID";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(Click_Date, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(Click_Date, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Open_Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Open_Country" => $EachRow['Open_Country'],
                    "Click_Count" => $EachRow['Click_Count'],
//                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    //By Eman
    public static function RetrieveUserGeoOpenNormal($CampaignID = -1, $UserID, $StartDate = null, $FinishDate = null) {

        $ArrayOpens = array();
//        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Open_Count,"
//                . " (select  country_name from ip2location_db9 where country_code = Open_Country limit 1) Country_Name"
//                . "  FROM      " . MYSQL_TABLE_PREFIX . "stats_open "
//                . "  WHERE       RelOwnerUserID = $UserID";
        $SQLQuery = "SELECT    LOWER(Open_Country) Open_Country,COUNT(*) Open_Count"
                . "  FROM      " . MYSQL_TABLE_PREFIX . "stats_open "
                . "  WHERE       RelOwnerUserID = $UserID";

        if ($CampaignID != -1) {
            $SQLQuery .= "  AND       RelCampaignID = $CampaignID";
        }

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(OpenDate, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(OpenDate, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Open_Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Open_Country" => $EachRow['Open_Country'],
                    "Open_Count" => $EachRow['Open_Count'],
//                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    //By Eman
    public static function RetrieveUserGeoClickNormal($CampaignID = -1, $UserID, $StartDate = null, $FinishDate = null) {

        $ArrayClicks = array();

//        $SQLQuery = "SELECT    LOWER(Click_Country) Click_Country,COUNT(*) Click_Count,"
//                . " (select  country_name from ip2location_db9 where country_code = Click_Country limit 1) Country_Name"
//                . "  FROM      " . MYSQL_TABLE_PREFIX . "stats_link "
//                . "  WHERE       RelOwnerUserID = $UserID";
        $SQLQuery = "SELECT    LOWER(Click_Country) Click_Country,COUNT(*) Click_Count"
                . "  FROM      " . MYSQL_TABLE_PREFIX . "stats_link "
                . "  WHERE       RelOwnerUserID = $UserID";

        if ($CampaignID != -1) {
            $SQLQuery .= "  AND       RelCampaignID = $CampaignID";
        }

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(ClickDate, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(ClickDate, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Click_Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Open_Country" => $EachRow['Click_Country'],
                    "Click_Count" => $EachRow['Click_Count'],
                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayClicks[] = $row;
            }
        }
        return $ArrayClicks;
    }

    /**
     * Returns the open statistics of the campaign for the given date range
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignOpenStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalOpens'];
            }
        }
        // Total opens calculation - End
        // Unique opens calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(OpenDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueOpens FROM  " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(OpenDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(OpenDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueOpens'];
            }
        }
        // Unique opens calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the send activity statistics of a user for the given date range
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveUserSendActivityStatistics($UserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        $SQLQuery = "SELECT DATE_FORMAT(SendProcessStartedOn, '%Y-%m-%d') AS StatDate, SUM(TotalSent) AS TotalSent FROM " . MYSQL_TABLE_PREFIX . "campaigns WHERE RelOwnerUserID='" . $UserID . "' AND DATE_FORMAT(SendProcessStartedOn, '%Y-%m-%d')>='" . $StartDate . "' AND DATE_FORMAT(SendProcessStartedOn, '%Y-%m-%d')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(SendProcessStartedOn, '%Y-%m-%d'))";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['TotalSent'] = $EachRow['TotalSent'];
            }
        }

        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if (isset($ArrayStatistics[$FormattedDate]['TotalSent'])) {
                $ArrayDays[$FormattedDate]['TotalSent'] = $ArrayStatistics[$FormattedDate]['TotalSent'];
            } else {
                $ArrayDays[$FormattedDate]['TotalSent'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the open list of the campaign
     *
     * @return resultset
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignOpens($CampaignID, $UserID, $StartFrom, $ReturnTotal = false, $RPP = 25) {
        if ($ReturnTotal == false) {
            // Return the list of subscribers who opened the campaign
            $SQLQuery = "SELECT *, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelSubscriberID ORDER BY TotalOpens DESC LIMIT " . $StartFrom . ", " . $RPP;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            return $ResultSet;
        } else {
            // Return the total number of subscribers who opened the campaign
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            $TotalRecords = mysql_fetch_assoc($ResultSet);
            $TotalRecords = $TotalRecords['TotalRecords'];

            return $TotalRecords;
        }
    }

    /**
     * Returns the open list of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveCampaignDateOpens($CampaignID, $Date) {

        // Return the list of subscribers who opened the campaign
        $SQLQuery = "SELECT *, COUNT(*) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(OpenDate, '%Y-%m-%d')='" . $Date . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalOpens DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        return $ResultSet;
    }

    /**
     * Returns the open list of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveListDateOpens($ListID, $UserID, $unqiue = false, $Date = null) {

        // Return the list of subscribers who opened the campaign
        $CountColumn = $unqiue ? "DISTINCT RelSubscriberID" : "*";
        $SQLQuery = "SELECT *, COUNT($CountColumn) AS TotalOpens FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(OpenDate, '%Y-%m-%d')='" . $Date . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalOpens DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        return $ResultSet;
    }

    /**
     * Returns the campaign activity of the subscriber
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberCampaignActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        // Open activity - Start
        $ArrayTMP = self::RetrieveSubscriberOpenActivity($CampaignID, $SubscriberID, $ListID);

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Open activity - End
        // Link click activity - Start
        $ArrayTMP = self::RetrieveSubscriberLinkActivity($CampaignID, $SubscriberID, $ListID, '', '');

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Link click activity - End
        // Browser view activity - Start
        $ArrayTMP = self::RetrieveSubscriberBrowserViewActivity($CampaignID, $SubscriberID, $ListID);

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Browser view activity - End
        // Forward to friend activity - Start
        $ArrayTMP = self::RetrieveSubscriberForwardActivity($CampaignID, $SubscriberID, $ListID);

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Forward to friend activity - End
        // Unsubscription activity - Start
        $ArrayTMP = self::RetrieveSubscriberUnsubscriptionActivity($CampaignID, $SubscriberID, $ListID);

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Unsubscription activity - End
        // Bounce detection activity - Start
        $ArrayTMP = self::RetrieveSubscriberBounceActivity($CampaignID, $SubscriberID, $ListID);

        foreach ($ArrayTMP as $Key => $Value) {
            $ArraySubscriberActivity[$Key] = $Value;
        }
        // Bounce detection activity - End
        // Sort the activity array based on the key (timestamp) - Start
        krsort($ArraySubscriberActivity);
        // Sort the activity array based on the key (timestamp) - End

        return $ArraySubscriberActivity;
    }

    /**
     * Gets the subscriber open activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberOpenActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY OpenDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArraySubscriberActivity[strtotime($EachRow['OpenDate'])] = array(strtotime($EachRow['OpenDate']), 'Email Open');
        }

        return $ArraySubscriberActivity;
    }

    /**
     * Gets the subscriber browser view activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberBrowserViewActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY ViewDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArraySubscriberActivity[strtotime($EachRow['ViewDate'])] = array(strtotime($EachRow['ViewDate']), 'Browser View');
        }

        return $ArraySubscriberActivity;
    }

    /**
     * Gets the subscriber forward activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberForwardActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY ForwardDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArraySubscriberActivity[strtotime($EachRow['ForwardDate'])] = array(strtotime($EachRow['ForwardDate']), 'Email Forwarded');
        }

        return $ArraySubscriberActivity;
    }

    /**
     * Retrieves averall list activity of a user for given period
     *
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveOverallListActivityOfUser($UserID, $Days = 30) {
        $ArrayActivity = array();

        $SQLQuery = 'SELECT StatisticsDate, SUM(TotalSubscriptions) AS TotalSubscriptions, SUM(TotalUnsubscriptions) AS TotalUnsubscriptions FROM ' . MYSQL_TABLE_PREFIX . 'stats_activity WHERE RelOwnerUserID = ' . $UserID . ' AND StatisticsDate >= "' . date('Y-m-d', strtotime('-' . $Days . ' days', time())) . '" GROUP BY StatisticsDate ORDER BY StatisticsDate ASC';
        $Result = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($Result)) {
            $ArrayActivity[$EachRow['StatisticsDate']] = array('TotalSubscriptions' => $EachRow['TotalSubscriptions'], 'TotalUnsubscriptions' => $EachRow['TotalUnsubscriptions']);
        }

        for ($i = 0; $i < $Days; $i++) {
            $date = date('Y-m-d', strtotime('-' . $i . ' days', time()));
            if (!isset($ArrayActivity[$date])) {
                $ArrayActivity[$date] = array('TotalSubscriptions' => 0, 'TotalUnsubscriptions' => 0);
            }
        }
        return $ArrayActivity;
    }

    /**
     * Gets the subscriber unsubscription activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberUnsubscriptionActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY UnsubscriptionDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArraySubscriberActivity[strtotime($EachRow['UnsubscriptionDate'])] = array(strtotime($EachRow['UnsubscriptionDate']), 'Unsubscribed');
        }

        return $ArraySubscriberActivity;
    }

    /**
      Eman
     * */
    public static function RetrieveSubscriberBounceDate($CampaignID, $BounceDate, $Type) {

        // Return the list of unsubscribed subscribers
        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelCampaignID='" . $CampaignID . "' ";

        if ($BounceDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(BounceDate, '%Y-%m-%d')='" . $BounceDate . "' ";
        }
        if ($Type != null) {
            $SQLQuery .= " AND BounceType='" . $Type . "' ";
        }
        $SQLQuery .= " ORDER BY BounceDate DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
      Eman
     * */
    public static function RetrieveListBounceDate($ListID, $UserID, $BounceDate, $Type) {

        // Return the list of unsubscribed subscribers
        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelListID='" . $ListID . "'  AND RelOwnerUserID='" . $UserID . "' ";

        if ($BounceDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(BounceDate, '%Y-%m-%d')='" . $BounceDate . "' ";
        }
        if ($Type != null) {
            $SQLQuery .= " AND BounceType='" . $Type . "' ";
        }
        $SQLQuery .= " ORDER BY BounceDate DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Gets the subscriber bounce activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberBounceActivity($CampaignID, $SubscriberID, $ListID) {
        $ArraySubscriberActivity = array();

        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY BounceDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArraySubscriberActivity[strtotime($EachRow['BounceDate'])] = array(strtotime($EachRow['BounceDate']), $EachRow['BounceType'] . ' Bounced');
        }

        return $ArraySubscriberActivity;
    }

    /**
     * Returns the link click statistics of the campaign for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignClickStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total link clicks calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalClicks'];
            }
        }
        // Total link clicks calculation - End
        // Unique link click calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ClickDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueClicks FROM  " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ClickDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ClickDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueClicks'];
            }
        }
        // Unique link click calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the forward statistics of the campaign for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignForwardStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total forwards calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalForwards'];
            }
        }
        // Total forwards calculation - End
        // Unique forward calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ForwardDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueForwards FROM  " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ForwardDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ForwardDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueForwards'];
            }
        }
        // Unique forward calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns forwards of the campaign
     *
     * @return resultset
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignForwards($CampaignID, $StartFrom, $ReturnTotal = false, $RPP = 25) {
        if ($ReturnTotal == false) {
            // Return the list of forwarders
            $SQLQuery = "SELECT *, COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' GROUP BY RelSubscriberID ORDER BY TotalForwards DESC LIMIT " . $StartFrom . ", " . $RPP;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        } else {
            // Return the total number of subscribers who viewed the campaign in a web browser
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' GROUP BY RelCampaignID";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            $TotalRecords = mysql_fetch_assoc($ResultSet);
            $TotalRecords = $TotalRecords['TotalRecords'];

            return $TotalRecords;
        }

        return $ResultSet;
    }

    /**
     * Returns forwards of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveCampaignDateForwards($CampaignID, $ForwardDate) {

        // Return the list of forwarders
        $SQLQuery = "SELECT *, COUNT(*) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelCampaignID='" . $CampaignID . "' ";

        if ($ForwardDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(ForwardDate, '%Y-%m-%d') = '" . $ForwardDate . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalForwards DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);


        return $ResultSet;
    }

    /**
     * Returns forwards of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveListDateForwards($ListID, $UserID, $unqiue = false, $ForwardDate = null) {

        // Return the list of forwarders
        $CountColumn = $unqiue ? "DISTINCT RelSubscriberID" : "*";
        $SQLQuery = "SELECT *, COUNT($CountColumn) AS TotalForwards FROM " . MYSQL_TABLE_PREFIX . "stats_forward WHERE RelListID='" . $ListID . "'   AND RelOwnerUserID='" . $UserID . "' ";

        if ($ForwardDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(ForwardDate, '%Y-%m-%d') = '" . $ForwardDate . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalForwards DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);


        return $ResultSet;
    }

    /**
     * Returns the browser view statistics of the campaign for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignBrowserViewStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalBrowserViews'];
            }
        }
        // Total browser views calculation - End
        // Unique browser views calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(ViewDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueBrowserViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')>='" . $StartDate . "' AND DATE_FORMAT(ViewDate, '%Y-%m-%d')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(ViewDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueBrowserViews'];
            }
        }
        // Unique browser views calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns browser views of the campaign
     *
     * @return resultset
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignBrowserViews($CampaignID, $StartFrom, $ReturnTotal = false, $RPP = 25) {
        if ($ReturnTotal == false) {
            // Return the list of browser views
            $SQLQuery = "SELECT *, COUNT(*) AS TotalViews FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelCampaignID='" . $CampaignID . "' GROUP BY RelSubscriberID ORDER BY TotalViews DESC LIMIT " . $StartFrom . ", " . $RPP;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            return $ResultSet;
        } else {
            // Return the total number of subscribers who viewed the campaign in a web browser
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_browserview WHERE RelCampaignID='" . $CampaignID . "' GROUP BY RelCampaignID";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            $TotalRecords = mysql_fetch_assoc($ResultSet);
            $TotalRecords = $TotalRecords['TotalRecords'];
            return $TotalRecords;
        }
    }

    /**
     * Returns the bounce statistics of the campaign for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignBounceStatisticsTimeFrame($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total bounce calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(BounceDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalBounces FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(BounceDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(BounceDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(BounceDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Total'] = $EachRow['TotalBounces'];
            }
        }
        // Total bounce calculation - End
        // Unique bounce calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(BounceDate, '%Y-%m-%d') AS StatDate, COUNT(DISTINCT RelSubscriberID) AS UniqueBounces FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(BounceDate, '%Y-%m-%d')>='" . $StartDate . "' AND DATE_FORMAT(BounceDate, '%Y-%m-%d')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(BounceDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['UniqueBounces'];
            }
        }
        // Unique bounce calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the bounce statistics of the campaign for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignBounceStatistics($CampaignID, $RelOwnerUserID, $TotalRecipients = 0) {
        $SQLQuery = "SELECT TotalRecipients,TotalHardBounces,TotalSoftBounces FROM `" . MYSQL_TABLE_PREFIX . "campaigns` WHERE CampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "'";
        // $SQLQuery = "SELECT COUNT(*) AS TotalSubscribers, BounceType FROM `".MYSQL_TABLE_PREFIX."stats_bounce` WHERE RelCampaignID='".$CampaignID."' AND RelOwnerUserID='".$RelOwnerUserID."' GROUP BY BounceType ORDER BY TotalSubscribers DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $Stats = mysql_fetch_assoc($ResultSet);

        $ArrayReturn = array();

        $TMPPercentage = round((100 * $Stats['TotalHardBounces']) / $Stats['TotalRecipients']);
        $ArrayReturn[] = array("status" => 'Hard Bounced', "count" => $Stats['TotalHardBounces'], "percentage" => $TMPPercentage);
        $TMPPercentage = round((100 * $Stats['TotalSoftBounces']) / $Stats['TotalRecipients']);
        $ArrayReturn[] = array("status" => 'Soft Bounced', "count" => $Stats['TotalSoftBounces'], "percentage" => $TMPPercentage);

        return $ArrayReturn;
    }

    /**
      Eman
     * */
    public static function RetrieveCampaignSpamStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total Unsubscription calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(DateOfReceive, '%Y-%m-%d') AS DateOfReceive, COUNT(*) AS TotalSPAMComplaints FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE CampaignID='" . $CampaignID . "' AND UserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(DateOfReceive, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(DateOfReceive, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(DateOfReceive, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['DateOfReceive']]['Total'] = $EachRow['TotalSPAMComplaints'];
            }
        }
        // Total Unsubscription calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Total']) {
                $ArrayDays[$FormattedDate]['Total'] = $ArrayStatistics[$FormattedDate]['Total'];
            } else {
                $ArrayDays[$FormattedDate]['Total'] = 0;
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
      Eman
     * */
    public static function RetrieveCampaignUnsubscriptionStatistics($CampaignID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total Unsubscription calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalUnsubscriptions FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['TotalUnsubscriptions'];
            }
        }
        // Total Unsubscription calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
      Eman
     * */
    public static function RetrieveListSubsUnsubsStatistics($ListID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total Unsubscription calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(StatisticsDate, '%Y-%m-%d') AS StatDate, SUM(IFNULL(TotalSubscriptions,0)+IFNULL(TotalImport,0)) AS TotalSubscriptions, SUM(IFNULL(TotalUnsubscriptions,0)+IFNULL(TotalMoved,0)+IFNULL(TotalSuppressed,0)) AS TotalUnsubscriptions FROM " . MYSQL_TABLE_PREFIX . "stats_activity WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(StatisticsDate, '%Y-%m-%d')>='" . $StartDate . "' AND DATE_FORMAT(StatisticsDate, '%Y-%m-%d')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(StatisticsDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['TotalSubscriptions'] = $EachRow['TotalSubscriptions'];
                $ArrayStatistics[$EachRow['StatDate']]['TotalUnsubscriptions'] = $EachRow['TotalUnsubscriptions'];
            }
        }

        // Total Unsubscription calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['TotalSubscriptions'] && $ArrayStatistics[$FormattedDate]['TotalUnsubscriptions']) {
                $ArrayDays[$FormattedDate]['TotalSubscriptions'] = $ArrayStatistics[$FormattedDate]['TotalSubscriptions'];
                $ArrayDays[$FormattedDate]['TotalUnsubscriptions'] = $ArrayStatistics[$FormattedDate]['TotalUnsubscriptions'];
            } else {

                if (!$ArrayStatistics[$FormattedDate]['TotalSubscriptions']) {
                    $ArrayDays[$FormattedDate]['TotalSubscriptions'] = 0;
                    $ArrayDays[$FormattedDate]['TotalUnsubscriptions'] = $ArrayStatistics[$FormattedDate]['TotalUnsubscriptions'];
                }

                if (!$ArrayStatistics[$FormattedDate]['TotalUnsubscriptions']) {
                    $ArrayDays[$FormattedDate]['TotalSubscriptions'] = $ArrayStatistics[$FormattedDate]['TotalSubscriptions'];
                    $ArrayDays[$FormattedDate]['TotalUnsubscriptions'] = 0;
                }
            }
            $ArrayDays[$FormattedDate]['date'] = $FormattedDate;
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns the unsubscription statistics of the auto responder for the given date
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveAutoResponderUnsubscriptionStatistics($AutoResponderID, $RelOwnerUserID, $StartDate, $FinishDate) {
        // Set the date range - Start
        $NumberOfDays = ceil(((strtotime($FinishDate) - strtotime($StartDate)) / 86400));
        // Set the date range - End

        $ArrayStatistics = array();
        $ArrayDays = array();

        // Total Unsubscription calculation - Start
        $SQLQuery = "SELECT DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d') AS StatDate, COUNT(*) AS TotalUnsubscriptions FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelAutoResponderID='" . $AutoResponderID . "' AND RelOwnerUserID='" . $RelOwnerUserID . "' AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d %H:%i:%s')>='" . $StartDate . "' AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d %H:%i:%s')<='" . $FinishDate . "' GROUP BY crc32(DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d'))";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayStatistics[$EachRow['StatDate']]['Unique'] = $EachRow['TotalUnsubscriptions'];
            }
        }
        // Total Unsubscription calculation - End
        // Generate the date range - Start
        for ($EachDay = 0; $EachDay <= $NumberOfDays; $EachDay++) {
            // Format the date - Start
            $FormattedDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($StartDate)) . ' +' . $EachDay . ' days 00:00:00'));
            // Format the date - End

            if ($ArrayStatistics[$FormattedDate]['Unique']) {
                $ArrayDays[$FormattedDate]['Unique'] = $ArrayStatistics[$FormattedDate]['Unique'];
            } else {
                $ArrayDays[$FormattedDate]['Unique'] = 0;
            }
        }
        // Generate the date range - End

        return $ArrayDays;
    }

    /**
     * Returns unsubscribed subscribers of the campaign
     *
     * @return resultset
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignUnsubscriptions($CampaignID, $StartFrom, $ReturnTotal = false, $RPP = 25) {
        if ($ReturnTotal == false) {
            // Return the list of unsubscribed subscribers
            $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelCampaignID='" . $CampaignID . "' ORDER BY UnsubscriptionDate DESC LIMIT " . $StartFrom . ", " . $RPP;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            return $ResultSet;
        } else {
            // Return the total number of subscribers who unsubscribed
            $SQLQuery = "SELECT COUNT(*) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelCampaignID='" . $CampaignID . "'";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            $TotalRecords = mysql_fetch_assoc($ResultSet);
            $TotalRecords = $TotalRecords['TotalRecords'];
            return $TotalRecords;
        }
    }

    /**
     * Returns unsubscribed subscribers of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveCampaignDateUnsubscriptions($CampaignID, $UnsubscriptionDate) {

        // Return the list of unsubscribed subscribers
        $SQLQuery = "SELECT * , COUNT(*) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelCampaignID='" . $CampaignID . "' ";

        if ($UnsubscriptionDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d')='" . $UnsubscriptionDate . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY UnsubscriptionDate DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Returns unsubscribed subscribers of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveListUnsubscriptions($ListID, $UserID, $UnsubscriptionDate = null) {

        // Return the list of unsubscribed subscribers
        $SQLQuery = "SELECT * , COUNT(*) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_unsubscription WHERE RelListID='" . $ListID . "' AND RelOwnerUserID='" . $UserID . "' ";

        if ($UnsubscriptionDate != null) {
            $SQLQuery .= " AND DATE_FORMAT(UnsubscriptionDate, '%Y-%m-%d')='" . $UnsubscriptionDate . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY UnsubscriptionDate DESC ";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Returns the link click statistics list of the campaign
     *
     * @return resultset
     * @author Cem Hurturk
     * */
    public static function RetrieveCampaignLinkClicks($CampaignID, $UserID, $GroupBy = 'Links', $StartFrom = 0, $ReturnTotal = false, $RPP = 25) {
        if ($GroupBy == 'Links') {
            if ($ReturnTotal == false) {
                // Return the list of link clicks
                $SQLQuery = "SELECT *, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY crc32(LinkTitle), LinkURL ORDER BY TotalClicks DESC LIMIT " . $StartFrom . ", " . $RPP;
                $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
                return $ResultSet;
            } else {
                // Return the total amount of unique link clicks
                $SQLQuery = "SELECT COUNT(DISTINCT(LinkTitle)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
                $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

                $TotalRecords = mysql_fetch_assoc($ResultSet);
                $TotalRecords = $TotalRecords['TotalRecords'];

                return $TotalRecords;
            }
        } elseif ($GroupBy == 'Subscribers') {
            if ($ReturnTotal == false) {
                // Return the list of clicked subscribers
                $SQLQuery = "SELECT *, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelSubscriberID ORDER BY TotalClicks DESC LIMIT " . $StartFrom . ", " . $RPP;
                $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
                return $ResultSet;
            } else {
                // Return the total amount of clicked subscribers
                $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
                $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
                $TotalRecords = mysql_fetch_assoc($ResultSet);
                $TotalRecords = $TotalRecords['TotalRecords'];
                return $TotalRecords;
            }
        }
    }

    /**
     * Returns the link click statistics list of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveCampaignDateClicks($CampaignID, $Date) {

        // Return the list of clicked subscribers
        $SQLQuery = "SELECT *, COUNT(*) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(ClickDate, '%Y-%m-%d')='" . $Date . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalClicks DESC ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Returns the link click statistics list of the campaign
     *
     * @return resultset
     * @author Eman
     * */
    public static function RetrieveListDateClicks($ListID, $UserID, $unqiue = false, $Date = null) {

        // Return the list of clicked subscribers
        $CountColumn = $unqiue ? "DISTINCT RelSubscriberID" : "*";
        $SQLQuery = "SELECT *, COUNT($CountColumn) AS TotalClicks FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelListID='" . $ListID . "'  AND RelOwnerUserID='" . $UserID . "' ";
        if ($Date != null) {
            $SQLQuery .= " AND DATE_FORMAT(ClickDate, '%Y-%m-%d')='" . $Date . "' ";
        }
        $SQLQuery .= " GROUP BY RelSubscriberID ORDER BY TotalClicks DESC ";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Gets the subscriber link click activity for the given campaign
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function RetrieveSubscriberLinkActivity($CampaignID, $SubscriberID, $ListID, $LinkTitle, $LinkURL) {
        $ArraySubscriberActivity = array();

        if (($LinkURL == '') && ($SubscriberID != '')) {
            // Retrieve a certain subscriber history
            $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelSubscriberID='" . $SubscriberID . "' AND RelListID='" . $ListID . "' ORDER BY ClickDate ASC LIMIT 0,30";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArraySubscriberActivity[strtotime($EachRow['ClickDate'])] = array(strtotime($EachRow['ClickDate']), 'Link Click', $EachRow['LinkURL'], $EachRow['LinkTitle']);
            }
        } elseif (($LinkURL != '') && ($SubscriberID != '')) {
            // Retrieve history of a certain subscriber for a certain link
        } elseif (($LinkURL != '') && ($SubscriberID == '')) {
            // Retrieve history of a certain link
            $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND LinkTitle='" . $LinkTitle . "' AND LinkURL='" . $LinkURL . "' ORDER BY ClickDate ASC LIMIT 0,30";
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                // Get subscriber information - Start
                $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $EachRow['RelSubscriberID']), $EachRow['RelListID']);
                // Get subscriber information - End

                $ArraySubscriberActivity[strtotime($EachRow['ClickDate'])] = array(strtotime($EachRow['ClickDate']), $ArraySubscriber['EmailAddress'], $EachRow['RelSubscriberID'], $EachRow['RelListID']);
            }
        }
        // Sort the activity array based on the key (timestamp) - Start
        krsort($ArraySubscriberActivity);
        // Sort the activity array based on the key (timestamp) - End
        return $ArraySubscriberActivity;
    }

    /**
     * Retrieves admin's total earnings to date
     *
     * @return integer
     * @author Mert Hurturk
     */
    public static function RetrieveAdminTotalEarnings() {
        $SQLQuery = "SELECT SUM(TotalAmount) AS TotalEarnings FROM " . MYSQL_TABLE_PREFIX . "users_payment_log WHERE PaymentStatus = 'Paid'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ResultArray = mysql_fetch_assoc($ResultSet);
        return $ResultArray['TotalEarnings'];
    }

    /**
     * Retrieves admin's total monthly earnings
     *
     * @param $MonthCount How many months
     * @return array
     * @author Mert Hurturk
     */
    public static function RetrieveMonthlyEarnings($NumberOfMonths, $FormatCurrency) {
        // Set the date range - Start
        $FromDate = date('Y-m', strtotime(Date('Y-m-d') . ' -' . $NumberOfMonths . ' months'));
        $ToDate = date('Y-m');
        // Set the date range - End

        $SQLQuery = "SELECT SUM(TotalAmount) AS TotalEarnings, DATE_FORMAT(PaymentStatusDate, '%Y-%m') AS StatDate FROM " . MYSQL_TABLE_PREFIX . "users_payment_log WHERE PaymentStatus = 'Paid' AND DATE_FORMAT(PaymentStatusDate, '%Y-%m') >= '" . $FromDate . "' GROUP BY crc32(StatDate)";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayStatistics = array();
        while ($EachStat = mysql_fetch_assoc($ResultSet)) {
            $ArrayStatistics[$EachStat['StatDate']]['TotalEarnings'] = $EachStat['TotalEarnings'];
        }

        $ArrayMonths = array();
        // Generate the date range - Start
        for ($EachMonth = 1; $EachMonth <= $NumberOfMonths; $EachMonth++) {
            // Format the date - Start
            $FormattedDate = date('Y-m', strtotime($FromDate . ' +' . $EachMonth . ' months'));
            // Format the date - End
            if ($ArrayStatistics[$FormattedDate]['TotalEarnings']) {
                $ArrayMonths[$FormattedDate]['TotalEarnings'] = $ArrayStatistics[$FormattedDate]['TotalEarnings'];
                if ($FormatCurrency) {
                    $ArrayMonths[$FormattedDate]['FormattedTotalEarnings'] = Core::FormatCurrency($ArrayStatistics[$FormattedDate]['TotalEarnings']);
                }
            } else {
                $ArrayMonths[$FormattedDate]['TotalEarnings'] = 0;
                if ($FormatCurrency) {
                    $ArrayMonths[$FormattedDate]['FormattedTotalEarnings'] = Core::FormatCurrency(0);
                }
            }
        }
        // Generate the date range - End

        return $ArrayMonths;
    }

    /**
     * Returns the total number of emails sent between provided dates
     *
     * @param string $UserID 
     * @param string $StartDate 
     * @param string $FromDate 
     * @return void
     * @author Cem Hurturk
     */
    public static function RetrieveEmailSendingAmountFromActivityLog($UserID, $FromDate, $ToDate, $ReturnSum = true, $ReturnWithDateIndex = false) {
        if ($ReturnSum == true) {
            $TotalEmailsSent = Database::$Interface->GetRows(array('SUM(TotalSentEmail) AS TotalEmailsSent'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array('RelListID' => 0, 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>=', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);
            $TotalEmailsSent = $TotalEmailsSent[0]['TotalEmailsSent'];

            return $TotalEmailsSent == NULL ? 0 : $TotalEmailsSent;
        } else {
            $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array('RelListID' => 0, 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>=', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);

            if ($ReturnWithDateIndex == true) {
                $NewArray = array();
                foreach ($ArrayReturn as $Index => $ArrayData) {
                    $NewArray[$ArrayData['StatisticsDate']] = $ArrayData;
                }
                $ArrayReturn = $NewArray;
                unset($NewArray);
            }
            return $ArrayReturn;
        }
    }

    /**
     * Returns the total number of unsubscriptions between provided dates
     *
     * @param string $UserID 
     * @param string $StartDate 
     * @param string $FromDate 
     * @return void
     * @author Cem Hurturk
     */
    public static function RetrieveUnsubscriptionsAmountFromActivityLog($UserID, $FromDate, $ToDate, $ReturnWithDateIndex = false) {
        $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array(array('field' => 'RelListID', 'operator' => '>', 'value' => 0), 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>=', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);

        if ($ReturnWithDateIndex == true) {
            $NewArray = array();
            foreach ($ArrayReturn as $Index => $ArrayData) {
                $NewArray[$ArrayData['StatisticsDate']] = $ArrayData;
            }
            $ArrayReturn = $NewArray;
            unset($NewArray);
        }
        return $ArrayReturn;
    }

    /**
     * Returns the total number of bounces between provided dates
     *
     * @param string $UserID 
     * @param string $StartDate 
     * @param string $FromDate 
     * @return void
     * @author Cem Hurturk
     */
    public static function RetrieveBounceDetectionLog($UserID, $FromDate, $ToDate, $ReturnWithDateIndex = false, $FilterBy = '') {
        $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'stats_activity'), array(array('field' => 'RelListID', 'operator' => '>', 'value' => 0), 'RelOwnerUserID' => $UserID, array('field' => 'StatisticsDate', 'operator' => '<=', 'value' => $ToDate), array('field' => 'StatisticsDate', 'operator' => '>=', 'value' => $FromDate)), array('StatisticsDate' => 'ASC'), 0, 0, 'AND', false);

        if ($ReturnWithDateIndex == true) {
            $NewArray = array();
            foreach ($ArrayReturn as $Index => $ArrayData) {
                $NewArray[$ArrayData['StatisticsDate']] = $ArrayData;
            }
            $ArrayReturn = $NewArray;
            unset($NewArray);
        }
        return $ArrayReturn;
    }

    /*
     * Eman
     */

    public static function RetrieveCampaignDateSPAM($CampaignID, $DateOfReceive) {

        // Return the list of forwarders
        $SQLQuery = "SELECT *, COUNT(*) AS TotalSPAMComplaints FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE CampaignID='" . $CampaignID . "' ";

        if ($DateOfReceive != null) {
            $SQLQuery .= " AND DATE_FORMAT(DateOfReceive, '%Y-%m-%d') = '" . $DateOfReceive . "' ";
        }
        $SQLQuery .= " GROUP BY SubscriberID ORDER BY TotalSPAMComplaints DESC ";


        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /*
     * Eman
     */

    public static function RetrieveListDateSPAM($ListID, $UserID, $DateOfReceive) {

        // Return the list of forwarders
        $SQLQuery = "SELECT *, COUNT(*) AS TotalSPAMComplaints FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE ListID='" . $ListID . "'  AND UserID='" . $UserID . "' ";

        if ($DateOfReceive != null) {
            $SQLQuery .= " AND DATE_FORMAT(DateOfReceive, '%Y-%m-%d') = '" . $DateOfReceive . "' ";
        }
        $SQLQuery .= " GROUP BY SubscriberID ORDER BY TotalSPAMComplaints DESC ";


        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        return $ResultSet;
    }

    /**
     * Returns the list of spam complaints of a user
     *
     * @param string $UserID 
     * @param string $FromDate 
     * @param string $ToDate 
     * @param string $ReturnWithDateIndex 
     * @return void
     * @author Cem Hurturk
     */
    public static function RetrieveSPAMComplaintCounts($UserID, $FromDate, $ToDate, $ReturnWithDateIndex = false) {
        $SQLQuery = "SELECT COUNT(*) AS TotalSPAMComplaints, DATE_FORMAT(DateOfReceive, '%Y-%m-%d') AS DetectionDate FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE UserID='" . $UserID . "' AND DateOfReceive>='" . $FromDate . "' AND DateOfReceive<='" . $ToDate . "' GROUP BY crc32(DATE_FORMAT(DateOfReceive, '%Y-%m-%d')) ORDER BY DetectionDate ASC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayData = array();

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            if ($ReturnWithDateIndex == false) {
                $ArrayData[] = $EachRow;
            } else {
                $ArrayData[$EachRow['DetectionDate']] = $EachRow;
            }
        }

        return $ArrayData;
    }

    /**
     * Returns the list of spam complaints of a campaign
     *
     * @param string $CampaignID 
     * @return void
     * @author Cem Hurturk
     */
    public static function RetrieveSPAMComplaintCountOfCampaign($CampaignID) {
        $SQLQuery = "SELECT COUNT(*) AS TotalSPAMComplaints FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE CampaignID='" . $CampaignID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayData = mysql_fetch_assoc($ResultSet);

        return $ArrayData['TotalSPAMComplaints'];
    }

    /**
      Eman
     */
    public static function RetrieveSPAMComplaintCountOfList($ListID, $TotalSent) {
        $ArrayStatistics = array();

        $SQLQuery = "SELECT COUNT(*) AS TotalSPAMComplaints FROM " . MYSQL_TABLE_PREFIX . "fbl_reports WHERE ListID='" . $ListID . "'";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayData = mysql_fetch_assoc($ResultSet);

        $Percentage = round(((100 * $ArrayData['TotalSPAMComplaints']) / $TotalSent), 2);
        $ArrayStatistics['TotalSPAMComplaints'] = $ArrayData['TotalSPAMComplaints'];
        $ArrayStatistics['TotalSPAMPercent'] = $Percentage;

        return $ArrayStatistics;
    }

    /**
     * Returns the total number of unique opens of a campaign (or an email version of a/b split campaign)
     *
     * @param string $CampaignID 
     * @param string $EmailID 
     * @return void
     * @author Cem Hurturk
     */
    public static function TotalUniqueOpensOfCampaign($CampaignID, $UserID, $EmailID = 0) {
        if ($EmailID > 0) {
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelEmailID='" . $EmailID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
        } else {
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_open WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
        }
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $TotalRecords = mysql_fetch_assoc($ResultSet);
        $TotalRecords = $TotalRecords['TotalRecords'];

        return $TotalRecords;
    }

    /**
     * Returns the total number of unique clicks of a campaign (or an email version of a/b split campaign)
     *
     * @param string $CampaignID 
     * @param string $EmailID 
     * @return void
     * @author Cem Hurturk
     */
    public static function TotalUniqueClicksOfCampaign($CampaignID, $UserID, $EmailID = 0) {
        if ($EmailID > 0) {
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelEmailID='" . $EmailID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
        } else {
            $SQLQuery = "SELECT COUNT(DISTINCT(RelSubscriberID)) AS TotalRecords FROM " . MYSQL_TABLE_PREFIX . "stats_link WHERE RelCampaignID='" . $CampaignID . "' AND RelOwnerUserID='" . $UserID . "' GROUP BY RelCampaignID";
        }
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $TotalRecords = mysql_fetch_assoc($ResultSet);
        $TotalRecords = $TotalRecords['TotalRecords'];

        return $TotalRecords;
    }

}

// END class Statistics
?>