<?php
/**
 * FBL (Feedback Loop) Parser Class
 *
 * @package default
 * @author Cem Hurturk
 **/

class FBL extends Core
{

/**
 * Creates the spam complaint report
 *
 * @return void
 * @author Cem Hurturk
 **/
public static function CreateFBLReport($ArrayFieldnValues)
	{
	Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX.'fbl_reports');

	return Database::$Interface->GetLastInsertID();
	}


} // END class FBL
?>