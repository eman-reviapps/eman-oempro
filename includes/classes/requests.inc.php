<?php
/**
 * Request By Email Parser Class
 *
 * @package default
 * @author Cem Hurturk
 **/

class RequestByEmail extends Core
{
/**
 * Checks if any list has been set for the subject and to email address
 *
 * @param string $Subject 
 * @param string $ToEmailAddress 
 * @return integer|boolean
 * @author Cem Hurturk
 */

function CheckIfAnyListAssigned($Subject, $ToEmailAddress)
	{
	$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."subscriber_lists WHERE ReqByEmailSearchToAddress='".mysql_real_escape_string($ToEmailAddress)."' AND (ReqByEmailSubscriptionCommand='".mysql_real_escape_string($Subject)."' OR ReqByEmailSubscriptionCommand='".mysql_real_escape_string(strtolower($Subject))."' OR ReqByEmailSubscriptionCommand='".mysql_real_escape_string(strtoupper($Subject))."' OR ReqByEmailUnsubscriptionCommand='".mysql_real_escape_string($Subject)."' OR ReqByEmailUnsubscriptionCommand='".mysql_real_escape_string(strtolower($Subject))."' OR ReqByEmailUnsubscriptionCommand='".mysql_real_escape_string(strtoupper($Subject))."')";
	$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
	
	$ArrayLists = array();
	
	if (mysql_num_rows($ResultSet) > 0)
		{
		while ($EachList = mysql_fetch_assoc($ResultSet))
			{
			$ArrayLists[] = $EachList;
			}
		}
	else
		{
		return false;
		}
	
	return $ArrayLists;
	}
} // END class FBL
?>