<?php

/**
 * Phpmailer interface class
 *
 * This class holds functions related with phpmailer
 * @package Oempro
 * @author Octeth
 * */
class SendEngine extends Core {

    /**
     * Intance of send engine interface object
     *
     * @var string
     * */
    public static $Engine;

    /**
     * Engine type ('phpmailer', 'swiftmailer')
     *
     * @var string
     * */
    public static $EngineType;

    /**
     * List of temporary image embedding files
     *
     * @var array
     * */
    public static $ArrayTemporaryImageEmbeddingFiles = array();

    /**
     * 'sender' in email header
     *
     * @var string
     * */
    public static $EmailHeaderSender;

    /**
     * 'receiver/to' in email header
     *
     * @var string
     * */
    public static $EmailHeaderReceiver;

    /**
     * Sending method
     *
     * @var string
     * */
    public static $SendMethod;
    private static $PowerMTAVMTA;
    private static $PowerMTADir;
    private static $SaveToDiskDir;
    public static $XMailer;

    /**
     * Constructor function
     *
     * @return void
     * @author Mert Hurturk
     * */
    private function __construct() {
        
    }

    /**
     * Sets database interface object
     *
     * @return DatabaseInterface
     * @author Mert Hurturk
     * */
    public static function SetEngine($EngineType, $SendMethod) {
        if ($SendMethod == '') {
            self::$SendMethod = SEND_METHOD;
        } else {
            self::$SendMethod = $SendMethod;
        }

        if ($EngineType == 'phpmailer') {
            Core::LoadObject('phpmailer/class.phpmailer.php');

            if (self::$SendMethod == 'SMTP') {
                Core::LoadObject('phpmailer/class.smtp.php');
            }

            self::$EngineType = 'phpmailer';
            self::$Engine = new PHPMailer();
        } elseif ($EngineType == 'swiftmailer') {
            self::$EngineType = 'swiftmailer';
        } else {
            
        }
    }

    public static function SetEncoding($encoding) {
        if ($encoding != '8bit' && $encoding != '7bit' && $encoding != 'binary' && $encoding != 'base64' && $encoding != 'quoted-printable')
            return;

        if (self::$EngineType == 'phpmailer')
            self::$Engine->Encoding = $encoding;
    }

    /**
     * Sets the sending method and initializes parameters
     *
     * @param string $Method 
     * @param array $ArrayParameters 
     * @return void
     * @author Cem Hurturk
     */
    public static function SetSendMethod($SetCustom = false, $SMTPHost = '', $SMTPPort = '', $SMTPSecure = '', $SMTPAuth = '', $SMTPUsername = '', $SMTPPassword = '', $SMTPTimeout = '', $SMTPDebug = '', $SMTPKeepAlive = '', $LocalMTAPath = '', $PowerMTAVMTA = '', $PowerMTADir = '', $SaveToDiskDir = '', $XMailer = '') {
        self::$PowerMTAVMTA = ($SetCustom == false ? SEND_METHOD_POWERMTA_VMTA : $PowerMTAVMTA);
        self::$PowerMTADir = self::CorrectTrailingSlash(($SetCustom == false ? SEND_METHOD_POWERMTA_DIR : $PowerMTADir));
        self::$SaveToDiskDir = self::CorrectTrailingSlash(($SetCustom == false ? SEND_METHOD_SAVETODISK_DIR : $SaveToDiskDir));
        self::$XMailer = ($SetCustom == false ? X_MAILER : $XMailer);

        if (self::$EngineType == 'phpmailer') {
            if (self::$SendMethod == 'SMTP') {
                self::$Engine->IsSMTP();
                self::$Engine->Host = ($SetCustom == false ? SEND_METHOD_SMTP_HOST : $SMTPHost);
                self::$Engine->Port = ($SetCustom == false ? SEND_METHOD_SMTP_PORT : $SMTPPort);
                self::$Engine->SMTPSecure = ($SetCustom == false ? SEND_METHOD_SMTP_SECURE : $SMTPSecure);
                self::$Engine->SMTPAuth = ($SetCustom == false ? SEND_METHOD_SMTP_AUTH : $SMTPAuth);
                self::$Engine->Username = ($SetCustom == false ? SEND_METHOD_SMTP_USERNAME : $SMTPUsername);
                self::$Engine->Password = ($SetCustom == false ? SEND_METHOD_SMTP_PASSWORD : $SMTPPassword);
                self::$Engine->Timeout = ($SetCustom == false ? SEND_METHOD_SMTP_TIMEOUT : $SMTPTimeout);
                self::$Engine->SMTPDebug = ($SetCustom == false ? SEND_METHOD_SMTP_DEBUG : $SMTPDebug);
                self::$Engine->SMTPKeepAlive = ($SetCustom == false ? SEND_METHOD_SMTP_KEEPALIVE : $SMTPKeepAlive);
            } elseif (self::$SendMethod == 'LocalMTA') {
                self::$Engine->IsQmail();
                self::$Engine->Sendmail = ($SetCustom == false ? SEND_METHOD_LOCALMTA_PATH : $LocalMTAPath);
            } elseif (self::$SendMethod == 'PHPMail') {
                self::$Engine->IsMail();
            } elseif (self::$SendMethod == 'PowerMTA') {
                self::$Engine->Mailer = 'save_to_disk';
            } elseif (self::$SendMethod == 'SaveToDisk') {
                self::$Engine->Mailer = 'save_to_disk';
            }
            self::$Engine->Hostname = (isset($_SERVER['SERVER_NAME']) == false ? EMAIL_DELIVERY_HOSTNAME : $_SERVER['SERVER_NAME']);
        }
    }

    /**
     * Attaches images and attachments. Images are embedded
     *
     * @param string $ContentType 
     * @param string $HTMLContent 
     * @param string $CampaignID 
     * @param string $EmailID 
     * @param string $UserID 
     * @param string $SubscriberID 
     * @param string $ListID 
     * @param string $ArrayAttachments 
     * @param string $ImageEmedding 
     * @return string
     * @author Cem Hurturk
     */
    public static function SetAttachmentsAndImageEmbedding($ContentType, $HTMLContent, $CampaignID, $EmailID, $UserID, $SubscriberID, $ListID, $ArrayAttachments = array(), $ImageEmedding = false) {
        if (self::$EngineType == 'phpmailer') {
            self::$Engine->ClearAttachments();

            // Embed images into the email body - Start
            if (($ImageEmedding == true) && (($ContentType == 'HTML') || ($ContentType == 'MultiPart'))) {
                $ArrayImages = Emails::DetectImages($HTMLContent, array(APP_URL . 'track_open.php?'), true);

                $TMPCounter = 0;
                foreach ($ArrayImages[1] as $Index => $EachImageURL) {
                    $CID = md5($CampaignID . $EachImageURL);
                    $ImageContent = Campaigns::FetchRemoteContent($EachImageURL);

                    $ArrayReturn = self::SaveImageToTemporaryFile($ImageContent, DATA_PATH . 'tmp/' . $CID);

                    self::$ArrayTemporaryImageEmbeddingFiles[] = $ArrayReturn['SavePath'];

                    self::$Engine->AddEmbeddedImage($ArrayReturn['SavePath'], $CID, '', 'base64', $ArrayReturn['mime']);
                    unset($ImageContent);

                    $HTMLContent = str_replace(array('"' . $ArrayImages[1][$TMPCounter] . '"', "'" . $ArrayImages[1][$TMPCounter] . "'"), "cid:" . $CID, $HTMLContent);

                    $TMPCounter++;
                }
            }
            // Embed images into the email body - End
            // Insert attachments - Start
            if ($ArrayAttachments != false) {
                foreach ($ArrayAttachments as $Index => $ArrayEachAttachment) {
                    self::$Engine->AddAttachment(DATA_PATH . 'attachments/' . md5($ArrayEachAttachment['AttachmentID']), $ArrayEachAttachment['FileName'], 'base64', 'application/octet-stream');
                }
            }
            // Insert attachments - End
        }

        return $HTMLContent;
    }

    /**
     * Deletes temporary files of image embedding files
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function RemoveTemporaryImageEmbeddingFiles() {
        if (count(self::$ArrayTemporaryImageEmbeddingFiles) > 0) {
            foreach (self::$ArrayTemporaryImageEmbeddingFiles as $EachTemporaryFile) {
                self::DeleteTemporaryImageFile($EachTemporaryFile);
            }
        }
    }

    /**
     * Sets email headers and body properties
     *
     * @param string $FromName 
     * @param string $FromEmail 
     * @param string $ToName 
     * @param string $ToEmail 
     * @param string $ReplyToName 
     * @param string $ReplyToEmail 
     * @param array $ArrayHeaders 
     * @param string $ContentType 
     * @param string $Subject 
     * @param string $HTMLContent 
     * @param string $PlainContent 
     * @param string $AbuseMessageID 
     * @param integer $CampaignID 
     * @param integer $EmailID 
     * @param integer $UserID 
     * @param integer $SubscriberID 
     * @param integer $ListID 
     * @return void
     * @author Cem Hurturk
     */
    public static function SetEmailProperties($FromName, $FromEmail, $ToName, $ToEmail, $ReplyToName, $ReplyToEmail, $ArrayHeaders, $ContentType, $Subject, $HTMLContent, $PlainContent, $AbuseMessageID, $CampaignID, $EmailID, $UserID, $SubscriberID, $ListID, $TransactionEmail = false) {
        $ReturnPathCode = Core::CryptNumber($CampaignID) . '-' . Core::CryptNumber($SubscriberID) . '-' . Core::CryptNumber($ListID) . '-' . Core::CryptNumber($UserID);
        self::$EmailHeaderSender = str_replace(array('_INSERT:EMAILADDRESS_', '_INSERT:MAILSERVERDOMAIN_'), array($ReturnPathCode, (isset($ArraySendEngineMailServer) == true ? $ArraySendEngineMailServer['Domain'] : BOUNCE_CATCHALL_DOMAIN)), BOUNCE_EMAILADDRESS);
        self::$EmailHeaderReceiver = $ToEmail;

        if (self::$EngineType == 'phpmailer') {
            // Set sender and recipient information - Start
            self::$Engine->From = $FromEmail;
            self::$Engine->FromName = $FromName;
            self::$Engine->Sender = self::$EmailHeaderSender;
            self::$Engine->AddReplyTo($ReplyToEmail, $ReplyToName);
            self::$Engine->AddAddress($ToEmail, $ToName);
            // Set sender and recipient information - End
            // Set email headers - Start
            foreach ($ArrayHeaders as $EachHeader => $EachValue) {
                self::$Engine->AddCustomHeader($EachHeader . ':' . $EachValue);
            }

            if (CENTRALIZED_SENDER_DOMAIN != '') {
                self::$Engine->AddCustomHeader('Sender: <user-' . Core::EncryptNumberAdvanced($UserID) . '@' . CENTRALIZED_SENDER_DOMAIN . '>'); // IMPORTANT: This line is quite important for Hotmail. This header pushes Hotmail to check the set email address for SenderID-PRA autharization
            }
            self::$Engine->AddCustomHeader('X-Mailer:' . (self::$XMailer == '' ? X_MAILER : self::$XMailer));
            self::$Engine->AddCustomHeader('X-Complaints-To:' . X_COMPLAINTS_TO);

            // Encrypted query parameters - Start
            $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                        $CampaignID, // Campaign ID
                        '', // Autoresponder ID
                        $SubscriberID, // Subscriber ID
                        $ListID, // List ID
                        (isset($EmailID) == true ? $EmailID : 0), // Email ID
                        0 // Is preview
            ));
            // Encrypted query parameters - End
            if ($TransactionEmail == false) {
                $ListUnsubscribeHeaderValue = '<' . APP_URL . 'u.php?p=' . $EncryptedQuery . '>';
                if (FBL_INCOMING_EMAILADDRESS != '') {
                    $ListUnsubscribeHeaderValue .= ', <mailto:' . FBL_INCOMING_EMAILADDRESS . '?subject=unsubscribe:' . $AbuseMessageID . '>';
                }
                self::$Engine->AddCustomHeader('List-Unsubscribe:' . $ListUnsubscribeHeaderValue);
                self::$Engine->AddCustomHeader('X-MessageID:' . $AbuseMessageID);
                self::$Engine->AddCustomHeader('X-Report-Abuse:' . '<' . X_REPORT_ABUSE_URL . $AbuseMessageID . '>');
            }
            // Set email headers - End
            // Set email subject and content - Start
            self::$Engine->CharSet = CHARSET;

            self::$Engine->Subject = $Subject;

            if ($ContentType == 'HTML') {
                self::$Engine->IsHTML(true);
                self::$Engine->Body = $HTMLContent;
                self::$Engine->AltBody = '';
            } elseif ($ContentType == 'Plain') {
                self::$Engine->IsHTML(false);
                self::$Engine->Body = $PlainContent;
                self::$Engine->AltBody = '';
            } else {
                self::$Engine->Body = $HTMLContent;
                self::$Engine->AltBody = $PlainContent;
            }
            // Set email subject and content - End
        } // end of if (self::$EngineType == 'phpmailer')
    }

    /**
     * Clears the recipient list, headers, etc.
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function ClearRecipientCache() {
        if (self::$EngineType == 'phpmailer') {
            self::$Engine->ClearAddresses();
            self::$Engine->ClearCustomHeaders();
            self::$Engine->ClearAllRecipients();
            self::$Engine->ClearReplyTos();
            self::$Engine->ClearCCs();
            self::$Engine->ClearBCCs();
        }
    }

    /**
     * Sends the email to the recipient
     *
     * @return array
     * @author Cem Hurturk
     */
    public static function SendEmail($JustReturnEmailContent = false) {
        if (self::$EngineType == 'phpmailer') {
            if (($EmailContent = self::$Engine->Send()) == false) {
                return array(false, self::$Engine->ErrorInfo);
            } else {
                if ($JustReturnEmailContent == true) {
                    return $EmailContent;
                } else {
                    // If sending method is set as PowerMTA, add the PowerMTA envolope data and put the new email content into pickup directory for queueing and sending - Start
                    if (self::$SendMethod == 'PowerMTA') {
                        $PMTAEnvolopeData = 'x-sender: ' . self::$EmailHeaderSender . "\n";
                        $PMTAEnvolopeData .= 'x-receiver: ' . self::$EmailHeaderReceiver . "\n";
                        $PMTAEnvolopeData .= 'x-virtual-mta: ' . self::$PowerMTAVMTA . "\n";

                        $EmailContent = $PMTAEnvolopeData . $EmailContent;

                        $FileHandler = fopen(self::$PowerMTADir . rand(100, 999) . rand(100, 999) . rand(100, 999) . rand(1000, 9999) . time(), 'w');
                        fwrite($FileHandler, $EmailContent);
                        fclose($FileHandler);
                    } elseif (self::$SendMethod == 'SaveToDisk') {
                        // Extra process can be done here to make SaveToDisk compatible with your own MTA pickup method
                        $FileHandler = fopen(self::$SaveToDiskDir . rand(100, 999) . rand(100, 999) . rand(100, 999) . rand(1000, 9999) . time(), 'w');
                        fwrite($FileHandler, $EmailContent);
                        fclose($FileHandler);
                    }
                    // If sending method is set as PowerMTA, add the PowerMTA envolope data and put the new email content into pickup directory for queueing and sending - End

                    return array(true);
                }
            }
        } else {
            return array(true);
        }
    }

    function CorrectTrailingSlash($Path) {
        if ((preg_match('/\/$/', $Path) == false) || (preg_match('', $Path) == 0)) {
            $Path = $Path . '/';
        }

        return $Path;
    }

    /**
     * Closes any open SMTP connections
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function CloseConnections() {
        if (self::$EngineType == 'phpmailer') {
            if ((self::$SendMethod == 'SMTP') && (SEND_METHOD_SMTP_KEEPALIVE == true)) {
                self::$Engine->SmtpClose();
            }
        }
    }

    /**
     * Creates the temporary image file
     *
     * @param string $ImageContent 
     * @param string $SavePath 
     * @return string
     * @author Cem Hurturk
     */
    public static function SaveImageToTemporaryFile($ImageContent, $SavePath) {
        $FileHandler = fopen($SavePath, 'w');
        fwrite($FileHandler, $ImageContent);
        fclose($FileHandler);

        unset($ImageContent);

        $ArrayImageInfo = getimagesize($SavePath);

        return array_merge(array('SavePath' => $SavePath), $ArrayImageInfo);
    }

    /**
     * Deletes the temporary image file
     *
     * @param string $TMPFilePath 
     * @return void
     * @author Cem Hurturk
     */
    public static function DeleteTemporaryImageFile($TMPFilePath) {
        unlink($TMPFilePath);
    }

}

// END class SendEngine
?>