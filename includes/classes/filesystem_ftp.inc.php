<?php
/**
 * FTP extension class for file system engine
 *
 * @package default
 * @author Cem Hurturk
 **/
class FileSystemEngine_FTP extends FileSystemEngine
{
public static $ObjectFTP			= '';
public static $LocalEcho			= false;
public static $Verbose				= false;

/**
 * Connect to the server
 *
 * @return void
 * @author Cem Hurturk
 */
public function Connect()
	{
	Core::LoadObject('ftp');

	self::$ObjectFTP = new ftp(self::$Verbose, self::$LocalEcho);
	self::$ObjectFTP->SetServer(self::$Host, self::$Port);
	self::$ObjectFTP->SetTimeout(30);
	self::$ObjectFTP->SetType(FTP_BINARY);
	
	if (self::$ObjectFTP->connect() == false)
		{
		return false;
		}

	if (self::$ObjectFTP->login(self::$Username, self::$Password) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Disconnect from server
 *
 * @return void
 * @author Cem Hurturk
 */
public function Disconnect()
	{
	self::$ObjectFTP->quit();
	return true;
	}

/**
 * Get contents
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetContents($TargetPath, $ContentType = FTP_AUTOASCII)
	{
	self::$ObjectFTP->SetType($ContentType);

	$TMPFile = DATA_PATH.'tmp/ftp_gettmp_'.md5($TargetPath);
	$FileHandler = fopen($TMPFile, 'w+');

	self::$ObjectFTP->fget($FileHandler, $TargetPath);
	fseek($FileHandler, 0); // Skip back to the start of the file being written to

	$Contents = '';

	while (!feof($FileHandler))
		{
		$Contents .= fread($FileHandler, 8192);
		}

	fclose($FileHandler);
	unlink($TMPFile);

	return $Contents;
	}

/**
 * Put contents
 *
 * @return void
 * @author Cem Hurturk
 */
public function PutContents($TargetPath, $Contents, $ContentType = '')
	{
	if ($ContentType == '')
		{
		$ContentType = (Core::IsBinary($Contents) == true ? FTP_BINARY : FTP_ASCII);
		}

	self::$ObjectFTP->SetType($ContentType);

	$TMPFile = DATA_PATH.'tmp/ftp_tmp_'.md5($TargetPath);
	$FileHandler = fopen($TMPFile, 'w+');
		fwrite($FileHandler, $Contents);
		fseek($FileHandler, 0); // Skip back to the start of the file being written to

	$Return = self::$ObjectFTP->fput($TargetPath, $FileHandler);

	fclose($FileHandler);
	unlink($TMPFile);
	
	return $Return;
	}

/**
 * Current path
 *
 * @return void
 * @author Cem Hurturk
 */
public function CurrentPath()
	{
	return self::$ObjectFTP->pwd();
	}

/**
 * Change directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function ChangeDirectory($Path)
	{
	if (self::$ObjectFTP->chdir($Path) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Change permissions
 *
 * @return void
 * @author Cem Hurturk
 */
public function ChangePermissions($Path, $Mode = false, $Recursive = false)
	{
	if ($Mode == false) return false;
	
	if (self::$ObjectFTP->chmod($Path, $Mode, $Recursive) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Copy to another location
 *
 * @return void
 * @author Cem Hurturk
 */
public function Copy($SourcePath, $TargetPath, $Overwrite = false)
	{
	if (($Overwrite == false) && (self::Exists($TargetPath) == true))
		{
		return false;
		}
	
	$Content = self::GetContents($SourcePath);
	
	if ($Content == false)
		{
		return false;
		}
	
	return self::PutContents($TargetPath, $Content, '');
	}

/**
 * Move to another location
 *
 * @return void
 * @author Cem Hurturk
 */
public function Move($SourcePath, $TargetPath)
	{
	if (self::Rename($SourcePath, $TargetPath) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Renames file/directory
 *
 * @param string $Path 
 * @param string $NewPath 
 * @return void
 * @author Cem Hurturk
 */
public function Rename($Path, $NewPath)
	{
	if (self::$ObjectFTP->rename($Path, $NewPath) == false)	
		{
		return false;
		}
	
	return true;
	}

/**
 * Delete
 *
 * @return void
 * @author Cem Hurturk
 */
public function Delete($Path)
	{
	if (self::$ObjectFTP->delete($Path) == false)
		{
		return false;
		}

	return true;
	}

/**
 * Check if exists
 *
 * @return void
 * @author Cem Hurturk
 */
public function Exists($Path)
	{
	if (self::$ObjectFTP->is_exists($Path) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Check if it's a file
 *
 * @return void
 * @author Cem Hurturk
 */
public function IsFile($Path)
	{
	if (self::$ObjectFTP->file_exists($Path) == false)
		{
		return false;
		}
		
	return true;
	}

/**
 * Check if it's a directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function IsDirectory($Path)
	{
	if (self::ChangeDirectory($Path) == true)
		{
		return true;
		}
	else
		{
		return false;
		}
	}

/**
 * Learn the size
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetFileSize($Path)
	{
	if (($FileSize = self::$ObjectFTP->filesize($Path)) == false)
		{
		return false;
		}
	
	return $FileSize;
	}

/**
 * Create a new directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function CreateDirectory($Path)
	{
	if (self::$ObjectFTP->mkdir($Path) == false)
		{
		return false;
		}

	return true;
	}

/**
 * Remove a directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function RemoveDirectory($Path)
	{
	if (self::$ObjectFTP->rmdir($Path) == false)
		{
		return false;
		}

	return true;
	}

/**
 * Get directory list
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetList($Path)
	{
	$TMPArrayList = self::$ObjectFTP->rawlist($Path);
	$ArrayList = array();
	
	foreach ($TMPArrayList as $EachLine)
		{
		$ArrayList[] = self::$ObjectFTP->parselisting($EachLine);
		}
	
	return $ArrayList;
	}
} // END class FileSystemEngine_FTP
?>