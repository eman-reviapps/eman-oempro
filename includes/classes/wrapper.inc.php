<?php
/**
 * Oempro Wrapper Class (communicates with Oempro API)
 *
 * @package default
 * @author Cem Hurturk
 */
class OemproWrapper
{
/**
 * Oempro API URL (absolute URL)
 *
 * @author Cem Hurturk
 */
public static $OemproAPIURL				= '';

/**
 * Timeout (seconds) duration for the connection
 *
 * @author Cem Hurturk
 */
public static $APIConnectionTimeOut		= 60;

/**
 * AIP's session ID
 *
 * @author Cem Hurturk
 */
public static $APISessionID				= '';

/**
 * Initializes the class and variables
 *
 * @return void
 * @author Cem Hurturk
 */
public static function Initialize()
	{
	self::$OemproAPIURL = APP_URL.'api.php';
	}

/**
 * Administrator login API call
 *
 * @param string $Username 
 * @param string $Password 
 * @param string $Captcha 
 * @param string $RememberMe 
 * @return void
 * @author Cem Hurturk
 */
public static function AdminLogin($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Admin.Login',
								'ResponseFormat=JSON',
								'Username='.rawurlencode($ArrayAPIVars['Username']),
								'Password='.rawurlencode($ArrayAPIVars['Password']),
								'Captcha='.rawurlencode($ArrayAPIVars['Captcha']),
								'RememberMe='.rawurlencode($ArrayAPIVars['RememberMe']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}
	
/**
 * User login API call
 *
 * @param string $Username 
 * @param string $Password 
 * @param string $Captcha 
 * @param string $RememberMe 
 * @return void
 * @author Cem Hurturk
 */
public static function UserLogin($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.Login',
								'ResponseFormat=JSON',
								'Username='.rawurlencode($ArrayAPIVars['Username']),
								'Password='.rawurlencode($ArrayAPIVars['Password']),
								'Captcha='.rawurlencode($ArrayAPIVars['Captcha']),
								'RememberMe='.rawurlencode($ArrayAPIVars['RememberMe']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Administrator password confirmation (reminder) API call
 *
 * @param string $EmailAddress 
 * @return void
 * @author Cem Hurturk
 */
public static function AdminPasswordRemind($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Admin.PasswordRemind',
								'ResponseFormat=JSON',
								'EmailAddress='.rawurlencode($ArrayAPIVars['EmailAddress']),
								'CustomResetLink='.rawurlencode(base64_encode(InterfaceAppURL(true).'/admin/passwordreminder/reset/%s')),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * User password confirmation (reminder) API call
 *
 * @param string $EmailAddress 
 * @return void
 * @author Cem Hurturk
 */
public static function UserPasswordRemind($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.PasswordRemind',
								'ResponseFormat=JSON',
								'EmailAddress='.rawurlencode($ArrayAPIVars['EmailAddress']),
								'CustomResetLink='.rawurlencode(base64_encode(InterfaceAppURL(true).'/user/passwordreminder/reset/%s')),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Administrator password reset API call
 *
 * @param string $EmailAddress 
 * @return void
 * @author Cem Hurturk
 */
public static function AdminPasswordReset($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Admin.PasswordReset',
								'ResponseFormat=JSON',
								'AdmindID='.rawurlencode($ArrayAPIVars['MD5AdminID']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * User password reset API call
 *
 * @param string $EmailAddress 
 * @return void
 * @author Cem Hurturk
 */
public static function UserPasswordReset($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.PasswordReset',
								'ResponseFormat=JSON',
								'UserID='.rawurlencode($ArrayAPIVars['MD5UserID']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve user accounts API call
 *
 * @param string $RecordsPerRequest 
 * @param string $RecordsFrom 
 * @param string $UserGroupID 
 * @param string $SearchField 
 * @param string $SearchKeyword 
 * @param string $OrderField 
 * @param string $OrderType 
 * @return void
 * @author Cem Hurturk
 */
public static function UsersGet($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Users.Get',
								'ResponseFormat=JSON',
								'OrderField='.rawurlencode($ArrayAPIVars['OrderField']),
								'OrderType='.rawurlencode($ArrayAPIVars['OrderType']),
								'RelUserGroupID='.rawurlencode($ArrayAPIVars['UserGroupID']),
								'RecordsPerRequest='.rawurlencode($ArrayAPIVars['RecordsPerRequest']),
								'RecordsFrom='.rawurlencode($ArrayAPIVars['RecordsFrom']),
								'SearchField='.rawurlencode($ArrayAPIVars['SearchField']),
								'SearchKeyword='.rawurlencode($ArrayAPIVars['SearchKeyword']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve campaigns API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function CampaignsGet($ArrayAPIVars)
	{
	self::Initialize();

	$ArrayCampaigns = Campaigns::RetrieveCampaigns(array('*'), array('Campaigns.RelOwnerUserID' => rawurlencode($ArrayAPIVars['UserID']), 'Campaigns.CampaignStatus' => rawurlencode($ArrayAPIVars['CampaignStatus'])), array('Campaigns.'.rawurlencode($ArrayAPIVars['OrderField']) => rawurlencode($ArrayAPIVars['OrderType'])), rawurlencode($ArrayAPIVars['RecordsPerRequest']), rawurlencode($ArrayAPIVars['RecordsFrom']), array(), array(), true, false);
	$TotalCampaigns = Campaigns::RetrieveCampaigns(array('*'), array('Campaigns.RelOwnerUserID' => rawurlencode($ArrayAPIVars['UserID']), 'Campaigns.CampaignStatus' => rawurlencode($ArrayAPIVars['CampaignStatus'])), array('Campaigns.'.rawurlencode($ArrayAPIVars['OrderField']) => rawurlencode($ArrayAPIVars['OrderType'])), rawurlencode($ArrayAPIVars['RecordsPerRequest']), rawurlencode($ArrayAPIVars['RecordsFrom']), array(), array(), true, true);

	return array($ArrayCampaigns, $TotalCampaigns);
	}

/**
 * Retrieve campaigns API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function CampaignsGetTotal($ArrayAPIVars)
	{
	self::Initialize();

	$TotalCampaigns = Campaigns::RetrieveCampaigns(array('*'), array('Campaigns.RelOwnerUserID' => rawurlencode($ArrayAPIVars['UserID']), 'Campaigns.CampaignStatus' => rawurlencode($ArrayAPIVars['CampaignStatus'])), array('Campaigns.'.rawurlencode($ArrayAPIVars['OrderField']) => rawurlencode($ArrayAPIVars['OrderType'])), rawurlencode($ArrayAPIVars['RecordsPerRequest']), rawurlencode($ArrayAPIVars['RecordsFrom']), array(), array(), true, true);

	return $TotalCampaigns;
	}

/**
 * User create API call
 *
 * @return void
 * @author Cem Hurturk
 */
public static function UserCreate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.Create',
								'ResponseFormat=JSON',
								'RelUserGroupID='.rawurlencode($ArrayAPIVars['RelUserGroupID']),
								'EmailAddress='.rawurlencode($ArrayAPIVars['EmailAddress']),
								'Username='.rawurlencode($ArrayAPIVars['Username']),
								'Password='.rawurlencode($ArrayAPIVars['Password']),
								'FirstName='.rawurlencode($ArrayAPIVars['FirstName']),
								'LastName='.rawurlencode($ArrayAPIVars['LastName']),
								'TimeZone='.rawurlencode($ArrayAPIVars['TimeZone']),
								'Language='.rawurlencode($ArrayAPIVars['Language']),
								'ReputationLevel='.rawurlencode($ArrayAPIVars['ReputationLevel']),
								'CompanyName='.rawurlencode($ArrayAPIVars['CompanyName']),
								'Website='.rawurlencode($ArrayAPIVars['Website']),
								'Street='.rawurlencode($ArrayAPIVars['Street']),
								'City='.rawurlencode($ArrayAPIVars['City']),
								'State='.rawurlencode($ArrayAPIVars['State']),
								'Zip='.rawurlencode($ArrayAPIVars['Zip']),
								'Country='.rawurlencode($ArrayAPIVars['Country']),
								'Phone='.rawurlencode($ArrayAPIVars['Phone']),
								'Fax='.rawurlencode($ArrayAPIVars['Fax']),
								'PreviewMyEmailAccount='.rawurlencode($ArrayAPIVars['PreviewMyEmailAccount']),
								'PreviewMyEmailAPIKey='.rawurlencode($ArrayAPIVars['PreviewMyEmailAPIKey']),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Updates payment period
 *
 * @return void
 * @author Mert Hurturk
 **/
function PaymentPeriodUpdate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.PaymentPeriods.Update',
								'ResponseFormat=JSON',
								);

	foreach ($ArrayAPIVars as $Key=>$Value)
		{
		$ArrayPostParameters[] = $Key.'='.rawurlencode($Value);
		}

	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * User update API call
 *
 * @return void
 * @author Cem Hurturk
 */
public static function UserUpdate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.Update',
								'ResponseFormat=JSON',
								'UserID='.rawurlencode($ArrayAPIVars['UserID']),
								'RelUserGroupID='.rawurlencode($ArrayAPIVars['RelUserGroupID']),
								'EmailAddress='.rawurlencode($ArrayAPIVars['EmailAddress']),
								'Username='.rawurlencode($ArrayAPIVars['Username']),
								'FirstName='.rawurlencode($ArrayAPIVars['FirstName']),
								'LastName='.rawurlencode($ArrayAPIVars['LastName']),
								'TimeZone='.rawurlencode($ArrayAPIVars['TimeZone']),
								'Language='.rawurlencode($ArrayAPIVars['Language']),
								'ReputationLevel='.rawurlencode($ArrayAPIVars['ReputationLevel']),
								'CompanyName='.rawurlencode($ArrayAPIVars['CompanyName']),
								'Website='.rawurlencode($ArrayAPIVars['Website']),
								'Street='.rawurlencode($ArrayAPIVars['Street']),
								'City='.rawurlencode($ArrayAPIVars['City']),
								'State='.rawurlencode($ArrayAPIVars['State']),
								'Zip='.rawurlencode($ArrayAPIVars['Zip']),
								'Country='.rawurlencode($ArrayAPIVars['Country']),
								'Phone='.rawurlencode($ArrayAPIVars['Phone']),
								'Fax='.rawurlencode($ArrayAPIVars['Fax']),
								'PreviewMyEmailAccount='.rawurlencode($ArrayAPIVars['PreviewMyEmailAccount']),
								'PreviewMyEmailAPIKey='.rawurlencode($ArrayAPIVars['PreviewMyEmailAPIKey']),
								'AccountStatus='.rawurlencode($ArrayAPIVars['AccountStatus']),
								);
	
	if ($ArrayAPIVars['NewPassword'] != '')
		{
		$ArrayPostParameters[] = 'Password='.rawurlencode($ArrayAPIVars['NewPassword']);
		}

	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Delete User API call
 *
 * @param string $ArrayUserIDs 
 * @return void
 * @author Cem Hurturk
 */
public static function DeleteUsers($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Users.Delete',
								'ResponseFormat=JSON',
								'Users='.rawurlencode(implode(',', $ArrayAPIVars['UserIDs'])),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Delete Campaign API call
 *
 * @param string $ArrayUserIDs 
 * @return void
 * @author Mert Hurturk
 */
public static function DeleteCampaigns($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Campaigns.Delete',
								'ResponseFormat=JSON',
								'Campaigns='.rawurlencode(implode(',', $ArrayAPIVars['Campaigns'])),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve email templates API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function EmailTemplatesGet($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Email.Templates.Get',
								'ResponseFormat=JSON'
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve tags API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function TagsGet()
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Tags.Get',
								'ResponseFormat=JSON'
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve email template information API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function EmailTemplateGet($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Email.Template.Get',
								'ResponseFormat=JSON',
								'TemplateID='.$ArrayAPIVars['TemplateID']
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Create email template API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function EmailTemplateCreate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Email.Template.Create',
								'ResponseFormat=JSON',
								'RelOwnerUserID='.rawurlencode($ArrayAPIVars['RelOwnerUserID']),
								'TemplateName='.rawurlencode($ArrayAPIVars['TemplateName']),
								'TemplateDescription='.rawurlencode($ArrayAPIVars['TemplateDescription']),
								'TemplateSubject='.rawurlencode($ArrayAPIVars['TemplateSubject']),
								'TemplateHTMLContent='.rawurlencode($ArrayAPIVars['TemplateHTMLContent']),
								'TemplatePlainContent='.rawurlencode($ArrayAPIVars['TemplatePlainContent']),
								'TemplateThumbnailPath='.rawurlencode($ArrayAPIVars['TemplateThumbnailPath'])
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Create theme API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function ThemeCreate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Theme.Create',
								'ResponseFormat=JSON',
								'Template='.rawurlencode($ArrayAPIVars['Template']),
								'ProductName='.rawurlencode($ArrayAPIVars['ProductName']),
								'ThemeName='.rawurlencode($ArrayAPIVars['ThemeName']),
								'ThemeSettings='.rawurlencode($ArrayAPIVars['ThemeSettings'])
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Create user group API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function UserGroupCreate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=UserGroup.Create',
								'ResponseFormat=JSON',
								'GroupName='.$ArrayAPIVars['GroupName'],
								'SubscriberAreaLogoutURL='.$ArrayAPIVars['SubscriberAreaLogoutURL'],
								'LimitSubscribers='.$ArrayAPIVars['LimitSubscribers'],
								'LimitLists='.$ArrayAPIVars['LimitLists'],
								'LimitCampaignSendPerPeriod='.$ArrayAPIVars['LimitCampaignSendPerPeriod'],
								'LimitEmailSendPerPeriod='.$ArrayAPIVars['LimitEmailSendPerPeriod'],
								'RelThemeID='.$ArrayAPIVars['RelThemeID'],
								'ForceUnsubscriptionLink='.$ArrayAPIVars['ForceUnsubscriptionLink'],
								'ForceRejectOptLink='.$ArrayAPIVars['ForceRejectOptLink'],
								'ForceOptInList='.$ArrayAPIVars['ForceOptInList'],
								'ThresholdImport='.$ArrayAPIVars['ThresholdImport'],
								'ThresholdEmailSend='.$ArrayAPIVars['ThresholdEmailSend'],
								'PaymentSystem='.$ArrayAPIVars['PaymentSystem'],
								'PaymentSystemChargeAmount='.$ArrayAPIVars['PaymentSystemChargeAmount'],
								'PaymentCampaignsPerCampaignCost='.$ArrayAPIVars['PaymentCampaignsPerCampaignCost'],
								'PaymentDesignPrevChargePerReq='.$ArrayAPIVars['PaymentDesignPrevChargePerReq'],
								'PaymentDesignPrevChargeAmount='.$ArrayAPIVars['PaymentDesignPrevChargeAmount'],
								'PaymentAutoRespondersChargeAmount='.$ArrayAPIVars['PaymentAutoRespondersChargeAmount'],
								'PaymentCampaignsPerRecipient='.$ArrayAPIVars['PaymentCampaignsPerRecipient'],
								'PaymentAutoRespondersPerRecipient='.$ArrayAPIVars['PaymentAutoRespondersPerRecipient'],
								'PaymentPricingRange='.$ArrayAPIVars['PaymentPricingRange']
								);
	foreach ($ArrayAPIVars['Permissions'] as $Each)
		{
		$ArrayPostParameters[] = 'Permissions[]='.$Each;
		}
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Delete user group API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function DeleteUserGroups($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=UserGroup.Delete',
								'ResponseFormat=JSON',
								'UserGroupID='.$ArrayAPIVars['UserGroupID']
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Delete email template API call
 *
 * @param array $ArrayAPIVars 
 * @return void
 * @author Mert Hurturk
 */
public static function DeleteEmailTemplates($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Email.Template.Delete',
								'ResponseFormat=JSON',
								'Templates='.rawurlencode(implode(',', $ArrayAPIVars['Templates'])),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}
/**
 * Update email template information API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function EmailTemplateUpdate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Email.Template.Update',
								'ResponseFormat=JSON',
								'TemplateID='.rawurlencode($ArrayAPIVars['TemplateID']),
								'RelOwnerUserID='.rawurlencode($ArrayAPIVars['RelOwnerUserID']),
								'TemplateName='.rawurlencode($ArrayAPIVars['TemplateName']),
								'TemplateDescription='.rawurlencode($ArrayAPIVars['TemplateDescription']),
								'TemplateSubject='.rawurlencode($ArrayAPIVars['TemplateSubject']),
								'TemplateHTMLContent='.rawurlencode($ArrayAPIVars['TemplateHTMLContent']),
								'TemplatePlainContent='.rawurlencode($ArrayAPIVars['TemplatePlainContent']),
								'TemplateThumbnailPath='.rawurlencode($ArrayAPIVars['TemplateThumbnailPath'])
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Switches between user accounts API call
 *
 * @param string $ArrayAPIVars 
 * @return void
 * @author Cem Hurturk
 */
public static function UserSwitch($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=User.Switch',
								'ResponseFormat=JSON',
								'UserID='.$ArrayAPIVars['UserID'],
								'PrivilegeType='.$ArrayAPIVars['PrivilegeType'],
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}


/**
 * Parses and returns the API response as array
 *
 * @param string $APIResponse 
 * @return void
 * @author Cem Hurturk
 */
private function ParseAPIResponse($APIResponse)
	{
	Core::LoadObject('json');
	
	$ObjectJSON = new Services_JSON();
	$ArrayAPIResponse = $ObjectJSON->decode($APIResponse);

	return $ArrayAPIResponse;
	}

/**
 * Admin update API call
 *
 * @return void
 * @author Cem Hurturk
 */
public static function AdminUpdate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Admin.Update',
								'ResponseFormat=JSON',
								'AdminID='.rawurlencode($ArrayAPIVars['AdminID']),
								'Username='.rawurlencode($ArrayAPIVars['Username']),
								'EmailAddress='.rawurlencode($ArrayAPIVars['EmailAddress']),
								'Name='.rawurlencode($ArrayAPIVars['Name']),
								);

	if ($ArrayAPIVars['NewPassword'] != '')
		{
		$ArrayPostParameters[] = 'Password='.rawurlencode($ArrayAPIVars['NewPassword']);
		}

	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Settings update API call
 *
 * @return void
 * @author Cem Hurturk
 */
public static function SettingsUpdate($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();
	
	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=Settings.Update',
								'ResponseFormat=JSON',
								);

	foreach ($ArrayAPIVars as $Key=>$Value)
		{
		$ArrayPostParameters[] = $Key.'='.rawurlencode($Value);
		}

	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);
	
	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

/**
 * Retrieve user groups API call
 *
 * @return void
 * @author Mert Hurturk
 */
public static function UserGroupsGet($ArrayAPIVars)
	{
	self::Initialize();

	$SessionID = session_id();
	session_write_close();

	$ArrayPostParameters = array(
								'SessionID='.rawurlencode($SessionID),
								'Command=UserGroups.Get',
								'ResponseFormat=JSON'
								);
	$ArrayReturn = Core::DataPostToRemoteURL(self::$OemproAPIURL, $ArrayPostParameters, 'POST', false, '', '', self::$APIConnectionTimeOut, false);

	session_id($SessionID);
	session_start();

	if ($ArrayReturn[0] == false) return false;

	$ArrayParsedAPIResponse = self::ParseAPIResponse($ArrayReturn[1]);

	self::$APISessionID = $ArrayParsedAPIResponse->SessionID;

	return $ArrayParsedAPIResponse;
	}

}
?>