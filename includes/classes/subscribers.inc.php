<?php

// Required modules - Start
Core::LoadObject('segments');
Core::LoadObject('custom_fields');
Core::LoadObject('suppression_list');
// Required modules - End

/**
 * Subscribers class
 *
 * This class holds all subscriber related functions
 * @package Oempro
 * @author Octeth
 * */
class Subscribers extends Core {

    public static $CustomFieldInformationCache = array();
    public static $SubscriberListInformationCache = array();

    /**
     * Default fields for rules
     *
     * @static array
     * */
    public static $SearchDefaultFields = array(
        array('CustomFieldID' => 'SubscriberID', 'ValidationMethod' => 'Numbers'),
        array('CustomFieldID' => 'EmailAddress', 'ValidationMethod' => 'Email address'),
        array('CustomFieldID' => 'SubscriptionDate', 'ValidationMethod' => 'Date', 'ValidationRule' => 'Y-m-d'),
        array('CustomFieldID' => 'SubscriptionIP', 'ValidationMethod' => 'IP'),
        array('CustomFieldID' => 'OptInDate', 'ValidationMethod' => 'Date', 'ValidationRule' => 'Y-m-d'),
        array('CustomFieldID' => 'Subscriber_IP', 'ValidationMethod' => 'IP'),
        array('CustomFieldID' => 'City', 'ValidationMethod' => 'Email address'),
        array('CustomFieldID' => 'Country', 'ValidationMethod' => 'Email address'),
    );

    /**
     * List of fields that are hidden (not selectable) in import porcesses
     *
     * @var array
     * */
    public static $ProtectedFieldsInImport = array('SubscriberID', 'BounceType', 'SubscriptionStatus', 'SubscriptionDate', 'SubscriptionIP', 'OptInDate');

    /**
     * Active subscriber criteria array
     *
     * @var array
     * */
    public static $ArrayActiveSubscriberCriterias = array('SubscriptionStatus' => 'Subscribed', array('field' => 'BounceType', 'operator' => '!=', 'value' => 'Hard'));
    public static $ArrayActiveSubscriberCriterias_Enhanced = array(array('Column' => 'SubscriptionStatus', 'Operator' => '=', 'Value' => 'Subscribed'), array('Link' => 'AND', 'Column' => 'BounceType', 'Operator' => '!=', 'Value' => 'Hard'));

    /**
     * Returns a single subscriber matching given criterias and subscriber list
     *
     * <code>
     * <?php
     * $ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>12), 2);
     * ?>
     * </code>
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving subscriber
     * @param int $SubscriberListID Subsriver list's id
     * @return boolean|array
     * @author Mert Hurturk
     * */
    public static function RetrieveSubscriber($ArrayReturnFields, $ArrayCriterias, $SubscriberListID) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ListInformation = Lists::RetrieveList(array('*'), array('ListID' => $SubscriberListID), false, false);

        // Retrieve custom fields of list to reformat custom field data of a subscriber - Start
        $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $SubscriberListID . ')', 'auto-quote' => false), 'RelOwnerUserID' => $ListInformation['RelOwnerUserID']));
        // Retrieve custom fields of list to reformat custom field data of a subscriber - End

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        $ArrayCriterias = $ArrayCriterias;
        $ArrayOrder = array();
        $ArraySubscriber = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        if (count($ArraySubscriber) < 1) {
            return false;
        } // if there are no subscribers, return false

        $ArraySubscriber = $ArraySubscriber[0];

        foreach ($ArrayCustomFields as $EachField) {
            if ($EachField['IsGlobal'] == 'Yes') {
                $ArraySubscriber['CustomField' . $EachField['CustomFieldID']] = CustomFields::GetGlobalFieldData($EachField['RelOwnerUserID'], 0, $ArraySubscriber['EmailAddress'], $EachField);
            }
            $FormattedFieldValue = self::FormatSubscriberFieldValue($ArraySubscriber, $EachField);
            $ArraySubscriber['CustomField' . $EachField['CustomFieldID']] = $FormattedFieldValue;
        }

        return $ArraySubscriber;
    }

    /**
     * Reteurns all subscribers matching given criterias and subscriber list
     *
     * <code>
     * <?php
     * $ArrayActiveSubscribers = Subscribers::RetrieveSubscribers(array('*'), Subscribers::$ArrayActiveSubscriberCriterias, 2, array('EmailAddress'=>'ASC'), 50, 0);
     * ?>
     * </code>
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving subscribers.
     * @param array $ArrayOrder Fields to order.
     * @param integer $RecordCount Subscriber count to retrieve.
     * @param integer $RecordFrom Row number to start retrieving from.
     * @return boolean|array
     * @author Mert Hurturk
     * */
    function RetrieveSubscribers($ArrayReturnFields, $ArrayCriterias, $SubscriberListID, $ArrayOrder = array('EmailAddress' => 'ASC'), $RecordCount = 0, $RecordFrom = 0, $ReturnSQLQuery = false, $ReturnTotal = false) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        // Return only total - Start
        if ($ReturnTotal) {
            $ArrayFields = array('COUNT(*) AS TotalSubscribers');
            $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
            $ArrayCriterias = $ArrayCriterias;
            $ArraySubscribers = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);
            return $ArraySubscribers[0]['TotalSubscribers'];
        }
        // Return only total - End
        // Retrieve custom fields of list to reformat custom field data of a subscriber - Start
        $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $SubscriberListID . ')', 'auto-quote' => false)));
        // Retrieve custom fields of list to reformat custom field data of a subscriber - End

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        $ArrayCriterias = $ArrayCriterias;
        $ArraySubscribers = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, $RecordCount, $RecordFrom, 'AND', $ReturnSQLQuery);

        if ($ReturnSQLQuery == true) {
            return $ArraySubscribers;
        }

        if (count($ArraySubscribers) < 1) {
            return false;
        }  // if there are no subscribers, return false

        foreach ($ArraySubscribers as &$EachSubscriber) {
            foreach ($ArrayCustomFields as $EachField) {
                if ($EachField['IsGlobal'] == 'Yes') {
                    $EachSubscriber['CustomField' . $EachField['CustomFieldID']] = CustomFields::GetGlobalFieldData($EachField['RelOwnerUserID'], 0, $EachSubscriber['EmailAddress'], $EachField);
                }
                $FormattedFieldValue = self::FormatSubscriberFieldValue($EachSubscriber, $EachField);
                $EachSubscriber['CustomField' . $EachField['CustomFieldID']] = $FormattedFieldValue;
            }
        }

        return $ArraySubscribers;
    }

    /**
     * Searches subscribers and returns results as array
     *
     * @param string $Keyword Keywords to search for
     * @param string $Field Subscriber field to search in
     * @param integer $SubscriberListID Subscriber list to search
     * @param integer $SegmentID Segment to search
     * @param boolean $Suppressed If set to true, searches in suppressed subscribers
     * @param array $ArrayOrder Fields to order.
     * @param integer $RecordCount Subscriber count to retrieve.
     * @param integer $RecordFrom Row number to start retrieving from.
     * @param boolean $ReturnSQLQuery If set to true, returns sql query string.
     * @param boolean $ReturnTotal If set to true, returns total.
     * @return array
     * @author Mert Hurturk
     */
    function Search($Keyword, $Field, $SubscriberListID, $SubscriberSegment = 'Active', $ArrayOrder = array('EmailAddress' => 'ASC'), $RecordCount = 0, $RecordFrom = 0, $ReturnSQLQuery = false, $ReturnTotal = false) {
        $ArrayFields = array('*');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

        // Setup search field name and search criterias - Start
        if (($Keyword != '') && ($Field != '')) {
            $FieldName = '';
            for ($i = 0; $i < count(self::$SearchDefaultFields); $i++) {
                if ($Field == self::$SearchDefaultFields[$i]['CustomFieldID']) {
                    // Field name is one of default fields
                    $FieldName = $Field;
                    break;
                }
            }
            if ($FieldName == '') {
                // Field name is a custom field
                $FieldName = "CustomField" . $Field;
            }

            $ArrayCriterias = array(array("field" => $FieldName, "operator" => "LIKE", "value" => "%" . $Keyword . "%"));
        } else {
            $ArrayCriterias = array();
        }
        // Setup search field name and search criterias - End

        $ArraySubscribers = array();

        // Retrieve segment subscribers - Start
        if (is_numeric($SubscriberSegment)) {
            if (!$ReturnTotal) {
                // $ArraySubscribers	= self::RetrieveSegmentSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $SubscriberSegment, $ArrayOrder, $RecordCount, $RecordFrom, true, $ReturnSQLQuery);
                $ArraySubscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
                            'ReturnFields' => array('*'),
                            'ReturnSQLQuery' => $ReturnSQLQuery,
                            'SubscriberListID' => $SubscriberListID,
                            'SegmentID' => $SubscriberSegment,
                            'RowOrder' => array('Column' => $ArrayOrder[0], 'Type' => $ArrayOrder[1]),
                            'Pagination' => array('Offset' => $RecordFrom, 'Rows' => $RecordCount),
                            'Criteria' => $ArrayCriterias
                ));
            } else {
                $ArraySubscribers = self::GetSegmentTotal($SubscriberListID, $SubscriberSegment);
            }
        }
        // Retrieve segment subscribers - End
        else {
            // Retrieve suppressed subscribers - Start
            if ($SubscriberSegment == 'Suppressed') {
                if (!$ReturnTotal) {
                    $ArraySubscribers = self::RetrieveSuppressedSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery);
                } else {
                    $ArraySubscribers = self::GetSuppresedTotal($SubscriberListID);
                }
            }
            // Retrieve suppressed subscribers - End
            // Retrieve unsubscribed subscribers - Start
            elseif ($SubscriberSegment == 'Unsubscribed') {
                $ArrayCriterias["SubscriptionStatus"] = "Unsubscribed";
                $ArraySubscribers = self::RetrieveSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery, $ReturnTotal);
            }
            // Retrieve unsubscribed subscribers - End
            // Retrieve soft bounced subscribers - Start
            elseif ($SubscriberSegment == 'Soft bounced') {
                $ArrayCriterias["BounceType"] = "Soft";
                $ArraySubscribers = self::RetrieveSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery, $ReturnTotal);
            }
            // Retrieve soft bounced subscribers - End
            // Retrieve hard bounced subscribers - Start
            elseif ($SubscriberSegment == 'Hard bounced') {
                $ArrayCriterias["BounceType"] = "Hard";
                $ArraySubscribers = self::RetrieveSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery, $ReturnTotal);
            }
            // Retrieve hard bounced subscribers - End
            // Retrieve hard bounced subscribers - Start
            elseif ($SubscriberSegment == 'Opt-in pending') {
                $ArrayCriterias["SubscriptionStatus"] = "Opt-In Pending";
                $ArraySubscribers = self::RetrieveSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery, $ReturnTotal);
            }
            // Retrieve hard bounced subscribers - End
            // Retrieve active subscribers - Start
            elseif ($SubscriberSegment == 'Active') {
                $ArrayCriterias = array_merge($ArrayCriterias, self::$ArrayActiveSubscriberCriterias);
                $ArraySubscribers = self::RetrieveSubscribers(array('*'), $ArrayCriterias, $SubscriberListID, $ArrayOrder, $RecordCount, $RecordFrom, $ReturnSQLQuery, $ReturnTotal);
            }
            // Retrieve active subscribers - End
        }

        // Return found subscribers
        return $ArraySubscribers;
    }

    /**
     * Reteurns all suppressed subscribers of a subscriber list
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param integer $SubscriberListID Subscriber list id
     * @param array $ArrayOrder Fields to order.
     * @param integer $RecordCount Subscriber count to retrieve.
     * @param integer $RecordFrom Row number to start retrieving from.
     * @return boolean|array
     * @author Mert Hurturk
     * */
    function RetrieveSuppressedSubscribers($ArrayReturnFields, $ArrayCriterias, $SubscriberListID, $ArrayOrder = array('EmailAddress' => 'ASC'), $RecordCount = 0, $RecordFrom = 0, $ReturnSQLQuery = false, $UserID = 0) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'suppression_list');
        $ArrayCriterias = array_merge($ArrayCriterias, array('RelListID' => $SubscriberListID));
        if ($UserID > 0) {
            $ArrayCriterias = array_merge($ArrayCriterias, array('RelOwnerUserID' => $UserID));
        }

        $ArraySubscribers = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, $RecordCount, $RecordFrom, 'AND', $ReturnSQLQuery);

        return $ArraySubscribers;
    }

    /**
     * Reteurns all segment subscribers of a subscriber list
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $SegmentID Segment id
     * @param array $ArrayOrder Fields to order.
     * @param integer $RecordCount Subscriber count to retrieve.
     * @param integer $RecordFrom Row number to start retrieving from.
     * @return boolean|array
     * @author Mert Hurturk
     * */
    function RetrieveSegmentSubscribers($ArrayReturnFields, $ArrayCriterias, $SubscriberListID, $SegmentID, $ArrayOrder = array(), $RecordCount = 0, $RecordFrom = 0, $RetrieveSegmentSubscribers = false, $ReturnSQLQuery = false) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }
        if (is_array($ArrayCriterias) == false) {
            $ArrayCriterias = array();
        }

        // Retrieve custom fields of list to reformat custom field data of a subscriber - Start
        $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $SubscriberListID . ')', 'auto-quote' => false)));
        // Retrieve custom fields of list to reformat custom field data of a subscriber - End

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        $ArraySegment = Segments::GetSegmentSQLQuery($SegmentID);

        $ArrayMergedCriterias = array_merge($ArraySegment['SegmentRules'], $ArrayCriterias);

        Database::$Interface->SetSource(__FILE__, __LINE__);
        $ArraySubscribers = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayMergedCriterias, $ArrayOrder, $RecordCount, $RecordFrom, (isset($ArraySegment['SegmentOperator']) && $ArraySegment['SegmentOperator'] == 'and' ? 'AND' : 'OR'), $ReturnSQLQuery);

        if ($ReturnSQLQuery == true) {
            return $ArraySubscribers;
        }

        if (count($ArraySubscribers) < 1) {
            return false;
        } // if there are no subscribers, return false

        foreach ($ArraySubscribers as &$EachSubscriber) {
            foreach ($ArrayCustomFields as $EachField) {
                $FormattedFieldValue = '';
                if ($EachField['ValidationMethod'] == 'Date' || $EachField['ValidationMethod'] == 'Time') {
                    $FormattedFieldValue = date($EachField['ValidationRule'], strtotime($EachSubscriber['CustomField' . $EachField['CustomFieldID']]));
                } else {
                    $FormattedFieldValue = $EachSubscriber['CustomField' . $EachField['CustomFieldID']];
                }

                $EachSubscriber['CustomField' . $EachField['CustomFieldID']] = $FormattedFieldValue;
            }
        }

        return $ArraySubscribers;
    }

    /**
     * This is an enhanced version of RetrieveSegmentSubscribers method.
     *
     * What changed from original function?
     * =============
     *
     * - Uses enhanced version of GetSegmentSQLQuery()
     * - Uses enhanced version of Database::$Interface->GetRows()
     * - Supports 'AND' and 'OR' in criteria, and criteria can be grouped recursively
     * - Parameters passed as an array
     *
     * Parameters:
     * ==========
     *
     * ReturnFields		array		Column names to be returned
     * ReturnSQLQuery	boolean		If set to true, sql query returned as string
     * SubscriberListID	integer		Subscriber list id
     * SegmentID		integer		Segment id
     * RowOrder			array		If set, ORDER BY statement is added to the query
     * 								-- Parameters:
     * 									Column				string					Column name from segments table to be ordered
     * 									Type				string	(ASC | DESC)	Order type
     * Pagination		array		If set, LIMIT statement is added to the query
     * 								-- Parameters:
     * 									Offset				integer					Offset of the first row
     * 									Rows				integer					Maximum number of rows to return
     * Criteria			array		If set, additional WHERE statement is added to the segment query
     * 								-- Parameters:
     * 									Link			string	(AND | OR)		Link that will be used to link this group with previous one
     * 									Column			string					Column name from campaigns table
     * 									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
     * 									Value			string					Value to be matched
     * 									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
     * 								-- Note: You can group criteria in arrays
     *
     * @param array $ArrayParameters
     * @return void
     * @author Mert Hurturk
     */
    function RetrieveSegmentSubscribers_Enhanced($ArrayParameters) {
        // Default parameters for method - Start {
        $ArrayDefaultParameters = array(
            'ReturnFields' => array('*'),
            'ReturnSQLQuery' => false,
            'RowOrder' => array(),
            'Pagination' => array(),
            'Criteria' => array()
        );
        $ArrayParameters = array_merge($ArrayDefaultParameters, $ArrayParameters);
        // Default parameters for method - End }
        // Parameter validation - Start {
        if (!isset($ArrayParameters['SegmentID']) || $ArrayParameters['SegmentID'] === '')
            throw new Exception('Segment ID is required');
        if (!isset($ArrayParameters['SubscriberListID']) || $ArrayParameters['SubscriberListID'] === '')
            throw new Exception('Subscriber list ID is required');
        // Parameter validation - End }
        // Retrieve subscribers - Start {
        // Database::$Interface->SetSource(__FILE__, __LINE__);
        // Add global custom field values to subscribers - Start {
        Core::LoadObject('lists');
        Core::LoadObject('custom_fields');

        $ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayParameters['SubscriberListID']));
        $ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $ArraySubscriberList['RelOwnerUserID'], 'IsGlobal' => 'Yes'));
        $ArrayGlobalCustomFieldSelects = array();
        if ($ArrayGlobalCustomFields != false) {
            foreach ($ArrayGlobalCustomFields as $EachField) {
                $FieldName = 'ValueText';
                $NullDefault = '""';
                if ($EachField['FieldType'] == 'Date field' || $EachField['ValidationMethod'] == 'Date') {
                    $FieldName = 'ValueDate';
                    $NullDefault = '"0000-00-00"';
                } else if ($EachField['FieldType'] == 'Time field' || $EachField['ValidationMethod'] == 'Time') {
                    $FieldName = 'ValueTime';
                    $NullDefault = '"00:00:00"';
                } else if ($EachField['ValidationMethod'] == 'Numbers') {
                    $FieldName = 'ValueDouble';
                    $NullDefault = '0';
                }

                $ArrayGlobalCustomFieldSelects[] = 'IFNULL((SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $EachField['CustomFieldID'] . ' AND EmailAddress = ' . MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayParameters['SubscriberListID'] . '.EmailAddress LIMIT 1), ' . $NullDefault . ') AS CustomField' . $EachField['CustomFieldID'];
            }
        }
        // Add global custom field values to subscribers - End }

        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayParameters['SubscriberListID']);
        $ArraySegmentCriterias = Segments::GetSegmentSQLQuery_Enhanced($ArrayParameters['SegmentID']);
        $ArraySegmentJoins = Segments::GetSegmentJoinQuery($ArrayParameters['SegmentID']);
        $ArrayMergedCriterias = array_merge($ArraySegmentCriterias, $ArrayParameters['Criteria']);
        $Subscribers = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array_merge($ArrayParameters['ReturnFields'], $ArrayGlobalCustomFieldSelects),
            'Tables' => $ArrayFromTables,
            'Joins' => $ArraySegmentJoins,
            'Criteria' => $ArrayMergedCriterias,
            'RowOrder' => $ArrayParameters['RowOrder'],
            'Pagination' => $ArrayParameters['Pagination'],
            'ReturnSQLQuery' => $ArrayParameters['ReturnSQLQuery']
        ));
        // Retrieve subscribers - End }
        // Return sql query string if requested - Start {
        if ($ArrayDefaultParameters['ReturnSQLQuery'] == true) {
            return $Subscribers;
        }
        // Return sql query string if requested - End }
        // if there are no subscribers, return false
        if (count($Subscribers) < 1) {
            return false;
        }

        // Reformat custom field data of subscribers - Start {
        // Retrieve custom fields of list to reformat custom field data of a subscriber - Start {
        $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $ArrayParameters['SubscriberListID'] . ')', 'auto-quote' => false)));
        // Retrieve custom fields of list to reformat custom field data of a subscriber - End }

        foreach ($Subscribers as &$EachSubscriber) {
            foreach ($ArrayCustomFields as $EachField) {
                $FormattedFieldValue = '';
                if ($EachField['ValidationMethod'] == 'Date' || $EachField['ValidationMethod'] == 'Time') {
                    $FormattedFieldValue = date($EachField['ValidationRule'], strtotime($EachSubscriber['CustomField' . $EachField['CustomFieldID']]));
                } else {
                    $FormattedFieldValue = $EachSubscriber['CustomField' . $EachField['CustomFieldID']];
                }

                $EachSubscriber['CustomField' . $EachField['CustomFieldID']] = $FormattedFieldValue;
            }
        }
        // Reformat custom field data of subscribers - End }

        return $Subscribers;
    }

    /**
     * This is an enhanced version of RetrieveSegmentSubscribers method.
     *
     * What changed from original function?
     * =============
     *
     * - Uses enhanced version of GetSegmentSQLQuery()
     * - Uses enhanced version of Database::$Interface->GetRows()
     * - Supports 'AND' and 'OR' in criteria, and criteria can be grouped recursively
     * - Parameters passed as an array
     *
     * Parameters:
     * ==========
     *
     * ReturnFields		array		Column names to be returned
     * ReturnSQLQuery	boolean		If set to true, sql query returned as string
     * SubscriberListID	integer		Subscriber list id
     * RowOrder			array		If set, ORDER BY statement is added to the query
     * 								-- Parameters:
     * 									Column				string					Column name from segments table to be ordered
     * 									Type				string	(ASC | DESC)	Order type
     * Pagination		array		If set, LIMIT statement is added to the query
     * 								-- Parameters:
     * 									Offset				integer					Offset of the first row
     * 									Rows				integer					Maximum number of rows to return
     * Criteria			array		If set, additional WHERE statement is added to the segment query
     * 								-- Parameters:
     * 									Link			string	(AND | OR)		Link that will be used to link this group with previous one
     * 									Column			string					Column name from campaigns table
     * 									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
     * 									Value			string					Value to be matched
     * 									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
     * 								-- Note: You can group criteria in arrays
     *
     * @param array $ArrayParameters
     * @return void
     * @author Mert Hurturk
     */
    function RetrieveSubscribers_Enhanced($ArrayParameters) {
        // Default parameters for method - Start {
        $ArrayDefaultParameters = array(
            'ReturnFields' => array('*'),
            'ReturnSQLQuery' => false,
            'RowOrder' => array(),
            'Pagination' => array(),
            'Criteria' => array()
        );
        $ArrayParameters = array_merge($ArrayDefaultParameters, $ArrayParameters);

        // Default parameters for method - End }
        // Parameter validation - Start {
        if (!isset($ArrayParameters['SubscriberListID']) || $ArrayParameters['SubscriberListID'] === '')
            throw new Exception('Subscriber list ID is required');
        // Parameter validation - End }
        // Retrieve subscribers - Start {
        // Database::$Interface->SetSource(__FILE__, __LINE__);
        // Add global custom field values to subscribers - Start {
        Core::LoadObject('lists');
        Core::LoadObject('custom_fields');

        $ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayParameters['SubscriberListID']));
        $ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $ArraySubscriberList['RelOwnerUserID'], 'IsGlobal' => 'Yes'));
        $ArrayGlobalCustomFieldSelects = array();
        $ArrayKeysOfGlobalsInReturnFields = array();
        if ($ArrayGlobalCustomFields != false) {
            foreach ($ArrayGlobalCustomFields as $EachField) {
                if (in_array('CustomField' . $EachField['CustomFieldID'], $ArrayParameters['ReturnFields']) || (isset($ArrayParameters['ReturnFields'][0]) && $ArrayParameters['ReturnFields'][0] == '*')) {
                    $FieldName = 'ValueText';
                    $NullDefault = '""';
                    if ($EachField['FieldType'] == 'Date field' || $EachField['ValidationMethod'] == 'Date') {
                        $FieldName = 'ValueDate';
                        $NullDefault = '"0000-00-00"';
                    } else if ($EachField['FieldType'] == 'Time field' || $EachField['ValidationMethod'] == 'Time') {
                        $FieldName = 'ValueTime';
                        $NullDefault = '"00:00:00"';
                    } else if ($EachField['ValidationMethod'] == 'Numbers') {
                        $FieldName = 'ValueDouble';
                        $NullDefault = '0';
                    }

                    $ArrayGlobalCustomFieldSelects[] = 'IFNULL((SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $EachField['CustomFieldID'] . ' AND EmailAddress = ' . MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayParameters['SubscriberListID'] . '.EmailAddress LIMIT 1), ' . $NullDefault . ') AS CustomField' . $EachField['CustomFieldID'];

                    $TMPArray = array_keys($ArrayParameters['ReturnFields'], 'CustomField' . $EachField['CustomFieldID']);
                    $ArrayKeysOfGlobalsInReturnFields[] = $TMPArray[0];
                }
            }
        }
        // Add global custom field values to subscribers - End }
        // Prepare select fields - Start {
        if (count($ArrayKeysOfGlobalsInReturnFields) > 0) {
            foreach ($ArrayKeysOfGlobalsInReturnFields as $Key) {
                unset($ArrayParameters['ReturnFields'][$Key]);
            }
        }
        // Prepare select fields - End }

        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayParameters['SubscriberListID']);
        $Subscribers = Database::$Interface->GetRows_Enhanced(array(
            'Fields' => array_merge($ArrayParameters['ReturnFields'], $ArrayGlobalCustomFieldSelects),
            'Tables' => $ArrayFromTables,
            'Criteria' => $ArrayParameters['Criteria'],
            'RowOrder' => $ArrayParameters['RowOrder'],
            'Pagination' => $ArrayParameters['Pagination'],
            'ReturnSQLQuery' => $ArrayParameters['ReturnSQLQuery']
        ));
        // Retrieve subscribers - End }
        // Return sql query string if requested - Start {
        if ($ArrayParameters['ReturnSQLQuery'] == true) {
            return $Subscribers;
        }
        // Return sql query string if requested - End }
        // if there are no subscribers, return false
        if (count($Subscribers) < 1) {
            return false;
        }


        /* IS THE CODE BELOW (commented out) REALLY NECESSARY?
          // Reformat custom field data of subscribers - Start {
          // Retrieve custom fields of list to reformat custom field data of a subscriber - Start {
          $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array(array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$ArrayParameters['SubscriberListID'].')', 'auto-quote' => false)));
          // Retrieve custom fields of list to reformat custom field data of a subscriber - End }

          foreach ($Subscribers as &$EachSubscriber)
          {
          foreach ($ArrayCustomFields as $EachField)
          {
          $FormattedFieldValue = '';
          if ($EachField['ValidationMethod'] == 'Date' || $EachField['ValidationMethod'] == 'Time')
          {
          $FormattedFieldValue = date($EachField['ValidationRule'], strtotime($EachSubscriber['CustomField'.$EachField['CustomFieldID']]));
          }
          else
          {
          $FormattedFieldValue = $EachSubscriber['CustomField'.$EachField['CustomFieldID']];
          }

          $EachSubscriber['CustomField'.$EachField['CustomFieldID']] = $FormattedFieldValue;
          }
          }
          // Reformat custom field data of subscribers - End }
         */

        return $Subscribers;
    }

    /**
     * Returns total number of active subscribers of a subscriber list
     *
     * <code>
     * <?php
     * $TotalSubscribers = Subscribers::GetActiveTotal(3);
     * ?>
     * </code>
     *
     * @param integer $ListID Subscriber list id
     * @return integer Total number of active subscribers of a subscriber list
     * @author Mert Hurturk
     * */
    function GetActiveTotal($ListID, $SegmentID = 0) {
        // Check for required fields of this function - Start
        if (!isset($ListID) || $ListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($SegmentID != 0) {
            $ArraySubscribers = self::RetrieveSegmentSubscribers_Enhanced(array(
                        'ReturnFields' => array('COUNT(*) AS TotalSubscriberCount'),
                        'SubscriberListID' => $ListID,
                        'SegmentID' => $SegmentID,
                        'Criteria' => array(
                            array('Link' => 'AND', 'Column' => 'BounceType', 'Operator' => '!=', 'Value' => 'Hard')
                        )
            ));
            $TotalSubscriberCount = $ArraySubscribers[0]['TotalSubscriberCount'];
        } else {
            $ArrayFields = array('COUNT(*) AS TotalSubscriberCount');
            $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ListID);
            $ArrayCriterias = self::$ArrayActiveSubscriberCriterias;
            $ArraySubscribers = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);
            $TotalSubscriberCount = $ArraySubscribers[0]['TotalSubscriberCount'];
        }


        return $ArraySubscribers[0]['TotalSubscriberCount'];
    }

    /**
     * Returns total number of segment subscribers of a subscriber list and segment
     *
     * <code>
     * <?php
     * $TotalSegmentSubscribers = Subscribers::GetSegmentTotal(3, 2);
     * ?>
     * </code>
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $SegmentID Segment id
     * @return integer Total number of active subscribers of a subscriber list
     * @author Mert Hurturk
     * */
    function GetSegmentTotal($SubscriberListID, $SegmentID) {
        Core::LoadObject('segments');
        $SQLQuery = Segments::GetSegmentSQLQuery_Enhanced($SegmentID, false, '', 'and', $SubscriberListID);
        $SQLJoin = Segments::GetSegmentJoinQuery($SegmentID, false, '', '', $SubscriberListID);

        $SubscriberCount = Database::$Interface->GetRows_Enhanced(
                array(
                    'Fields' => array('COUNT(*) AS TotalSubscribers'),
                    'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID),
                    'Joins' => $SQLJoin,
                    'Criteria' => $SQLQuery
                )
        );
        return $SubscriberCount[0]['TotalSubscribers'];
    }

    /**
     * Returns total number of suppressed subscribers of a subscriber list
     *
     * <code>
     * <?php
     * $TotalSuppressedSubscribers = Subscribers::GetSuppresedTotal(3);
     * ?>
     * </code>
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return integer Total number of active subscribers of a subscriber list
     * @author Mert Hurturk
     * */
    function GetSuppressedTotal($SubscriberListID, $UserID = 0) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $SQLQuery = "SELECT COUNT(*) as Total FROM " . MYSQL_TABLE_PREFIX . "suppression_list WHERE RelListID = '" . $SubscriberListID . "'" . ($UserID > 0 ? " AND RelOwnerUserID='" . $UserID . "'" : "");
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $Total = mysql_fetch_assoc($ResultSet);
        $Total = $Total['Total'];

        return $Total;
    }

    //By Eman
    function CreateCustomGeoColumns($SubscriberListID) {
        $SQLQuery = "ALTER TABLE oempro_subscribers_" . $SubscriberListID . " add column Subscriber_IP varchar(250) NULL;";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $SQLQuery = "ALTER TABLE oempro_subscribers_" . $SubscriberListID . " add column City varchar(250) NULL;";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $SQLQuery = "ALTER TABLE oempro_subscribers_" . $SubscriberListID . " add column Country varchar(250) NULL;";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

//        $SQLQuery = "ALTER TABLE oempro_subscribers_" . $SubscriberListID . " add column IsAutomationList enum('Yes', 'No') NULL;";
//        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
    }

    //By Eman
    public static function RetrieveSubscribersGeo($SubscriberListID, $UserID, $StartDate = null, $FinishDate = null) {

        $ArrayOpens = array();
//        $SQLQuery = "SELECT    LOWER(Country) Country,COUNT(*) Open_Count,"
//                . " (select  country_name from ip2location_db9 where country_code = Country limit 1) Country_Name"
//                . "  FROM      " . MYSQL_TABLE_PREFIX . "subscribers_" . $SubscriberListID
//                . "  WHERE Country <>  '' ";
        $SQLQuery = "SELECT    LOWER(Country) Country,COUNT(*) Open_Count"
                . "  FROM      " . MYSQL_TABLE_PREFIX . "subscribers_" . $SubscriberListID
                . "  WHERE Country <>  '' ";

        if ($StartDate != null && !empty($StartDate) && !is_null($StartDate)) {
            $SQLQuery .= "  AND    DATE_FORMAT(SubscriptionDate, '%Y-%m') >= '" . $StartDate . "'";
        }
        if ($FinishDate != null && !empty($FinishDate) && !is_null($FinishDate)) {
            $SQLQuery .= "  AND       DATE_FORMAT(SubscriptionDate, '%Y-%m') <= '" . $FinishDate . "'";
        }

        $SQLQuery .= "  GROUP BY  Country"
                . "    ORDER BY  COUNT(*) desc";

        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) > 0) {
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $row = array(
                    "Open_Country" => $EachRow['Country'],
                    "Open_Count" => $EachRow['Open_Count'],
//                    "Country_Name" => $EachRow['Country_Name'],
                );
                $ArrayOpens[] = $row;
            }
        }
        return $ArrayOpens;
    }

    /**
     * Returns total number of found subscribers
     *
     * <code>
     * <?php
     * $TotalFoundSubscribers = Subscribers::GetFoundTotal(3);
     * ?>
     * </code>
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return integer Total number of active subscribers of a subscriber list
     * @author Mert Hurturk
     * */
    function GetFoundTotal($Keyword, $Field, $SubscriberListID, $SegmentID = 0, $Suppressed = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $SQLQuery = array();

        $TotalFound = self::Search($Keyword, $Field, $SubscriberListID, ($Suppressed ? 'Suppressed' : ($SegmentID == 0 ? 'Active' : $SegmentID)), array('SubscriberID' => 'ASC'), 0, 0, false, true);

        return $TotalFound;
    }

    /**
     * Get all subscribers of a subscriber list and list suppressed subscribers 
     * who moved using move to suppression list
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns of email addresses
     * @author Eman Mohammed
     * */
    public static function getListSubscribersWithSupressed($SubscriberListID, $UserID, $Details = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End
        $Emails = array();
        $ActiveEmailsList = array();
        $SuppressedEmailsList = array();

        $ActiveSubscribers = self::RetrieveSubscribers(array('EmailAddress'), self::$ArrayActiveSubscriberCriterias, $SubscriberListID);

        if ($ActiveSubscribers) {
            foreach ($ActiveSubscribers as $Subscriber) {
                $Emails[] = $Subscriber['EmailAddress'];
                $ActiveEmailsList[] = $Subscriber['EmailAddress'];
            }
        }
        $SuppressedEmails = SuppressionList::GetSuppressionList(array('EmailAddress'), array(
                    'RelOwnerUserID' => $UserID,
                    'RelListID' => $SubscriberListID,
                    array('field' => 'PrevSubscriptionStatus', 'operator' => '!=', 'value' => 'Opt-In Pending'),
                    array('field' => 'PrevSubscriptionStatus', 'operator' => '!=', 'value' => 'Opt-Out Pending'),
//                    array('field' => 'PrevSubscriptionStatus', 'operator' => '!=', 'value' => 'Null'),
//                    'PrevSubscriptionStatus' => 'Subscribed',
                        )
        );
        if ($SuppressedEmails) {
            foreach ($SuppressedEmails as $Subscriber) {
                $Emails[] = $Subscriber['EmailAddress'];
                $SuppressedEmailsList[] = $Subscriber['EmailAddress'];
            }
        }
//        print_r('<pre>');
//        print_r($ActiveSubscribers);
//        print_r($SuppressedEmails);
//        print_r($Emails);
//        print_r(array_unique($Emails));
//        print_r('</pre>');
        if ($Details) {
            return array($Emails, $ActiveEmailsList, $SuppressedEmailsList);
        } else {
            return $Emails;
        }
    }

    public static function getGlobalSupressed($UserID) {
        $Emails = array();

        $SuppressedEmails = SuppressionList::GetSuppressionList(array('EmailAddress'), array(
                    'RelOwnerUserID' => $UserID,
                    'RelListID' => 0
                        )
        );
        if ($SuppressedEmails) {
            foreach ($SuppressedEmails as $Subscriber) {
                $Emails[] = $Subscriber['EmailAddress'];
            }
        }
        return $Emails;
    }

    /**
     * Removes all active subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param bool $returnRemovedSubscribers
     * @return array Returns removed email addresses
     * @author Eman Mohammed
     * */
    public static function moveActiveSubscribers_Enhanced($SubscriberListID, $moveSubscribers = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayAffectedSubscribers = self::RetrieveSubscribers(array('*'), self::$ArrayActiveSubscriberCriterias, $SubscriberListID);

        if ($moveSubscribers) {
            Database::$Interface->DeleteRows(self::$ArrayActiveSubscriberCriterias, MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

            // Plug-in hook - Start
            Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
            Plugins::HookListener('Action', 'Delete.Subscriber.Active', array($SubscriberListID));
            // Plug-in hook - End
        }
        return $ArrayAffectedSubscribers;
    }

    /**
     * Removes all active subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param bool $returnRemovedSubscribers
     * @return array Returns removed email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveActiveSubscribers($SubscriberListID, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($returnRemovedSubscribers) {
            $ArrayAffectedEmailAddresses = array();
            $ArraySubscribers = self::RetrieveSubscribers(array('*'), self::$ArrayActiveSubscriberCriterias, $SubscriberListID);
            foreach ($ArraySubscribers as $EachSubscriber) {
                $ArrayAffectedEmailAddresses[] = $EachSubscriber['EmailAddress'];
            }
        }

        Database::$Interface->DeleteRows(self::$ArrayActiveSubscriberCriterias, MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.Active', array($SubscriberListID));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedEmailAddresses : true;
    }

    /**
     * Removes all suppressed subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns removed email addresses
     * @author Mert Hurturk
     * */
    function RemoveSuppressedSubscribers($SubscriberListID, $UserID = 0) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayEmailAddresses = array();
        $ArraySuppressedSubscribers = self::RetrieveSuppressedSubscribers(array('*'), array(), $SubscriberListID);
        if ($UserID != 0) {
            $ArrayGloballySuppressedSubscribers = self::RetrieveSuppressedSubscribers(array('*'), array(), 0, array('EmailAddress' => 'ASC'), 0, 0, false, $UserID);
            $ArraySuppressedSubscribers = array_merge($ArrayGloballySuppressedSubscribers, $ArraySuppressedSubscribers);
        }

        foreach ($ArraySuppressedSubscribers as $Each) {
            $ArrayEmailAddresses[] = $Each['EmailAddress'];
        }

        $ArrayEmailAddresses = array_unique($ArrayEmailAddresses);

        self::RemoveSubscribersByEmailAddresses($SubscriberListID, $ArrayEmailAddresses);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.Suppressed', array($SubscriberListID));
        // Plug-in hook - End

        return $ArrayEmailAddresses;
    }

    /**
     * Removes all soft bounced subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns removed email addresses
     * @author Eman Mohammed
     * */
    public static function moveSoftBouncedSubscribers_Enhanced($SubscriberListID, $moveSubscribers = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End
        //array('field' => 'SubscriptionStatus', 'operator' => '!=', 'value' => 'Unsubscribed')

        $ArrayAffectedSubscribers = array();
        $ArraySubscribers = self::RetrieveSubscribers(array('*'), array('BounceType' => 'Soft', 'SubscriptionStatus' => 'Subscribed'), $SubscriberListID);
        foreach ($ArraySubscribers as $EachSubscriber) {
            $ArrayAffectedSubscribers[] = $EachSubscriber;
        }

        if ($moveSubscribers) {
            Database::$Interface->DeleteRows(array('BounceType' => 'Soft', 'SubscriptionStatus' => 'Subscribed'), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

            // Plug-in hook - Start
            Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
            Plugins::HookListener('Action', 'Delete.Subscriber.SoftBounced', array($SubscriberListID));
            // Plug-in hook - End
        }
        return $ArrayAffectedSubscribers;
    }

    /**
     * Removes all soft bounced subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns removed email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveSoftBouncedSubscribers($SubscriberListID, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($returnRemovedSubscribers) {
            $ArrayAffectedEmailAddresses = array();
            $ArraySubscribers = self::RetrieveSubscribers(array('*'), array('BounceType' => 'Soft'), $SubscriberListID);
            foreach ($ArraySubscribers as $EachSubscriber) {
                $ArrayAffectedEmailAddresses[] = $EachSubscriber['EmailAddress'];
            }
        }

        Database::$Interface->DeleteRows(array('BounceType' => 'Soft'), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.SoftBounced', array($SubscriberListID));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedEmailAddresses : true;
    }

    /**
     * Removes all hard bounced subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns removed email addresses
     * @author Eman Mohammed
     * */
    public static function RemoveHardBouncedSubscribers_Enhanced($SubscriberListID, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($returnRemovedSubscribers) {
            $ArrayAffectedSubscribers = array();
            $ArraySubscribers = self::RetrieveSubscribers(array('*'), array('BounceType' => 'Hard'), $SubscriberListID);
            foreach ($ArraySubscribers as $EachSubscriber) {
                $ArrayAffectedSubscribers[] = $EachSubscriber;
            }
        }

        Database::$Interface->DeleteRows(array('BounceType' => 'Hard'), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.HardBounced', array($SubscriberListID));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedSubscribers : true;
    }

    /**
     * Removes all hard bounced subscribers of a subscriber list
     *
     * @param integer $SubscriberListID Subscriber list id
     * @return array Returns removed email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveHardBouncedSubscribers($SubscriberListID, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($returnRemovedSubscribers) {
            $ArrayAffectedEmailAddresses = array();
            $ArraySubscribers = self::RetrieveSubscribers(array('*'), array('BounceType' => 'Hard'), $SubscriberListID);
            foreach ($ArraySubscribers as $EachSubscriber) {
                $ArrayAffectedEmailAddresses[] = $EachSubscriber['EmailAddress'];
            }
        }

        Database::$Interface->DeleteRows(array('BounceType' => 'Hard'), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.HardBounced', array($SubscriberListID));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedEmailAddresses : true;
    }

    /**
     * Removes not opted in subscribers for x days
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $Days Number of days
     * @return array Returns removed email addresses
     * @author Eman Mohammed
     * */
    public static function moveNotOptedInSubscribers_Enhanced($SubscriberListID, $Days = 3, $moveSubscribers = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($Days) || $Days == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayAffectedSubscribers = array();
        $SQLQuery = 'SELECT * FROM  `' . MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID . '` WHERE SubscriptionStatus = "Opt-In Pending" AND BounceType != "Hard" AND CURDATE() - OptInDate >= ' . $Days;
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayAffectedSubscribers[] = $EachRow;
        }

        if ($moveSubscribers) {
            $SQLQuery = 'DELETE FROM `' . MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID . '` WHERE SubscriptionStatus = "Opt-In Pending" AND BounceType != "Hard" AND CURDATE() - OptInDate >= ' . $Days;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            // Plug-in hook - Start
            Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
            Plugins::HookListener('Action', 'Delete.Subscriber.NotOptedIn', array($SubscriberListID, $Days));
            // Plug-in hook - End
        }
        return $ArrayAffectedSubscribers;
    }

    /**
     * Removes not opted in subscribers for x days
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $Days Number of days
     * @return array Returns removed email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveNotOptedInSubscribers($SubscriberListID, $Days = 3, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($Days) || $Days == '') {
            return false;
        }
        // Check for required fields of this function - End

        if ($returnRemovedSubscribers) {
            $ArrayAffectedEmailAddresses = array();
            $SQLQuery = 'SELECT * FROM  `' . MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID . '` WHERE SubscriptionStatus = "Opt-In Pending" AND CURDATE() - OptInDate >= ' . $Days;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                $ArrayAffectedEmailAddresses[] = $EachRow['EmailAddress'];
            }
        }
        $SQLQuery = 'DELETE FROM `' . MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID . '` WHERE SubscriptionStatus = "Opt-In Pending" AND CURDATE() - OptInDate >= ' . $Days;
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.NotOptedIn', array($SubscriberListID, $Days));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedEmailAddresses : true;
    }

    /**
     * Removes subscribers with given email addresses
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param array $ArrayEmailAddresses Email addresses
     * @author Eman Mohammed
     * */
    public static function moveSubscribersByEmailAddresses_Enhhanced($SubscriberListID, $ArrayEmailAddresses = array(), $moveSubscribers = false) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($ArrayEmailAddresses) || count($ArrayEmailAddresses) < 1) {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayAffectedSubscribers = array();
        foreach ($ArrayEmailAddresses as $Each) {
            // Remove carriage return characters from email addresses - Start
            $EmailAddress = str_replace(array("\n", "\r"), array('', ''), $Each);
            // Remove carriage return characters from email addresses - End
            $Subscriber = self::RetrieveSubscriber(array('*'), array(
                        'EmailAddress' => $EmailAddress,
                        'SubscriptionStatus' => 'Subscribed',
                        array('field' => 'BounceType', 'operator' => '!=', 'value' => 'Hard')
                            ), $SubscriberListID);
            if ($Subscriber) {
                $ArrayAffectedSubscribers[] = $Subscriber;
            }
        }
        if ($moveSubscribers) {
            foreach ($ArrayEmailAddresses as $Each) {
                // Remove carriage return characters from email addresses - Start
                $EmailAddress = str_replace(array("\n", "\r"), array('', ''), $Each);
                // Remove carriage return characters from email addresses - End

                if (self::ValidateEmailAddress($EmailAddress)) {
                    Database::$Interface->DeleteRows(
                            array('EmailAddress' => $EmailAddress,
                        'SubscriptionStatus' => 'Subscribed',
                        array('field' => 'BounceType', 'operator' => '!=', 'value' => 'Hard')
                            ), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
                }
            }

//        'SubscriptionStatus' => 'Subscribed'
            // Plug-in hook - Start
            Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
            Plugins::HookListener('Action', 'Delete.Subscriber.ByEmail', array($SubscriberListID, $ArrayEmailAddresses));
            // Plug-in hook - End
        }
        return $ArrayAffectedSubscribers;
    }

    /**
     * Removes subscribers with given email addresses
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param array $ArrayEmailAddresses Email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveSubscribersByEmailAddresses($SubscriberListID, $ArrayEmailAddresses = array()) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($ArrayEmailAddresses) || count($ArrayEmailAddresses) < 1) {
            return false;
        }
        // Check for required fields of this function - End

        foreach ($ArrayEmailAddresses as $Each) {
            // Remove carriage return characters from email addresses - Start
            $EmailAddress = str_replace(array("\n", "\r"), array('', ''), $Each);
            // Remove carriage return characters from email addresses - End

            if (self::ValidateEmailAddress($EmailAddress))
                Database::$Interface->DeleteRows(array('EmailAddress' => $EmailAddress), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.ByEmail', array($SubscriberListID, $ArrayEmailAddresses));
        // Plug-in hook - End
    }

    /**
     * Removes subscribers with given ids
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param array $ArraySubscriberIDs Subsriber ids
     * @author Mert Hurturk
     * */
    function RemoveSubscribersByID($SubscriberListID, $ArraySubscriberIDs) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($ArraySubscriberIDs) || count($ArraySubscriberIDs) < 1) {
            return false;
        }
        // Check for required fields of this function - End

        foreach ($ArraySubscriberIDs as $Each) {
            Database::$Interface->DeleteRows(array('SubscriberID' => $Each), MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.ByID', array($SubscriberListID, $ArraySubscriberIDs));
        // Plug-in hook - End
    }

    /**
     * Removes subscribers from suppression list with given ids
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param array $ArraySuppressionIDs Subsriber ids
     * @author Mert Hurturk
     * */
    function RemoveSuppressedSubscribersByIDFromSuppressionList($SubscriberListID, $ArraySuppressionIDs) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID === '') {
            return false;
        }
        if (!isset($ArraySuppressionIDs) || count($ArraySuppressionIDs) < 1) {
            return false;
        }
        // Check for required fields of this function - End

        foreach ($ArraySuppressionIDs as $Each) {
            Database::$Interface->DeleteRows(array('SuppressionID' => $Each, 'RelListID' => $SubscriberListID), MYSQL_TABLE_PREFIX . 'suppression_list');
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.SuppressedSubscribersByID', array($SubscriberListID, $ArraySuppressionIDs));
        // Plug-in hook - End
    }

    /**
     * Removes subscribers of a segment
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $SegmentID Segment id
     * @return array Removed subscriber email addresses
     * @author Mert Hurturk
     * */
    public static function moveSubscribersOfSegment_Enhanced($SubscriberListID, $SegmentID, $moveSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($SegmentID) || $SegmentID == '') {
            return false;
        }
        // Check for required fields of this function - End
        // $SegmentQueryArray = Segments::GetSegmentSQLQuery($SegmentID);
        $SegmentQueryArray = Segments::GetSegmentSQLQuery_Enhanced($SegmentID);
        $SegmentJoinArray = Segments::GetSegmentJoinQuery($SegmentID);

        $ArrayAffectedSubscribers = array();
        $ArraySubscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
                    'ReturnFields' => array('*'),
                    'SubscriberListID' => $SubscriberListID,
                    'SegmentID' => $SegmentID
        ));

        foreach ($ArraySubscribers as $EachSubscriber) {
            $ArrayAffectedSubscribers[] = $EachSubscriber;
        }
        if ($moveSubscribers) {
            Database::$Interface->DeleteRows_Enhanced(array(
                'Table' => MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID,
                'Criteria' => $SegmentQueryArray,
                'Joins' => $SegmentJoinArray
            ));

            // Plug-in hook - Start
            Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
            Plugins::HookListener('Action', 'Delete.Subscriber.Segment', array($SubscriberListID, $SegmentID));
            // Plug-in hook - End
        }
        return $ArrayAffectedSubscribers;
    }

    /**
     * Removes subscribers of a segment
     *
     * @param integer $SubscriberListID Subscriber list id
     * @param integer $SegmentID Segment id
     * @return array Removed subscriber email addresses
     * @author Mert Hurturk
     * */
    public static function RemoveSubscribersOfSegment($SubscriberListID, $SegmentID, $returnRemovedSubscribers = true) {
        // Check for required fields of this function - Start
        if (!isset($SubscriberListID) || $SubscriberListID == '') {
            return false;
        }
        if (!isset($SegmentID) || $SegmentID == '') {
            return false;
        }
        // Check for required fields of this function - End
        // $SegmentQueryArray = Segments::GetSegmentSQLQuery($SegmentID);
        $SegmentQueryArray = Segments::GetSegmentSQLQuery_Enhanced($SegmentID);
        $SegmentJoinArray = Segments::GetSegmentJoinQuery($SegmentID);

        if ($returnRemovedSubscribers) {
            $ArrayAffectedEmailAddresses = array();

            $ArraySubscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
                        'ReturnFields' => array('*'),
                        'SubscriberListID' => $SubscriberListID,
                        'SegmentID' => $SegmentID
            ));

            foreach ($ArraySubscribers as $EachSubscriber) {
                $ArrayAffectedEmailAddresses[] = $EachSubscriber['EmailAddress'];
            }
        }

        Database::$Interface->DeleteRows_Enhanced(array(
            'Table' => MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID,
            'Criteria' => $SegmentQueryArray,
            'Joins' => $SegmentJoinArray
        ));

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Subscriber', array($SubscriberListID));
        Plugins::HookListener('Action', 'Delete.Subscriber.Segment', array($SubscriberListID, $SegmentID));
        // Plug-in hook - End

        return $returnRemovedSubscribers ? $ArrayAffectedEmailAddresses : true;
    }

    /**
     * Returns CSV formatted string of given subscribers and fields
     *
     * @param array $ArraySubscribers Subscribers
     * @param array $ArrayFields Fields
     * @return string
     * @author Mert Hurturk
     * */
    function GetCSVData($ArraySubscribers, $ArrayFields, $ArrayLanguageStrings = '') {
        if ($ArrayLanguageStrings == '') {
            global $ArrayLanguageStrings;
            $TMPArrayLanguageStrings = $ArrayLanguageStrings['Screen'];
        } else {
            $TMPArrayLanguageStrings = $ArrayLanguageStrings;
        }

        $Data = '';

        // Add column header line - Start {
        foreach ($ArrayFields as $ColumnHeader) {
            $HeaderText = '';
            // Check if this field is default or custom field - Start {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            // Check if this field is default or custom field - End }
            if ($IsDefaultField) {
                $HeaderText = $TMPArrayLanguageStrings[$ColumnHeader];
            } else {
                // Retrieve custom field information - Start {
                $ArrayCustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => str_replace('CustomField', '', $ColumnHeader)));
                $HeaderText = $ArrayCustomFieldInformation['FieldName'];
                // Retrieve custom field information - End }
            }
            $Data .= '"' . $HeaderText . '",';
        }

        $Data = substr($Data, 0, -1); // Remove comma from end of the line
        $Data .= "\n"; // Add new line character at the end of line
        // Add column header line - End }
        // Add data lines - Start {
        // Convert custom field ids to CustomFieldID s - Start {
        foreach ($ArrayFields as &$ColumnHeader) {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            if (!$IsDefaultField) {
                $ColumnHeader = $ColumnHeader;
            }
        }
        // Convert custom field ids to CustomFieldID s - End }

        foreach ($ArraySubscribers as $EachSubscriber) {
            $Data .= self::ReturnCSVLine($EachSubscriber, $ArrayFields, '"', ',');
            $Data .= "\n";
        }
        // Add data lines - End }

        return $Data;
    }

    function GetCSVDataHeader($ArrayFields, $ArrayLanguageStrings = '') {
        if ($ArrayLanguageStrings == '') {
            global $ArrayLanguageStrings;
            $TMPArrayLanguageStrings = $ArrayLanguageStrings['Screen'];
        } else {
            $TMPArrayLanguageStrings = $ArrayLanguageStrings;
        }

        $Data = '';

        foreach ($ArrayFields as $ColumnHeader) {
            $HeaderText = '';
            // Check if this field is default or custom field - Start {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            // Check if this field is default or custom field - End }
            if ($IsDefaultField) {
                $HeaderText = $TMPArrayLanguageStrings[$ColumnHeader];
            } else {
                // Retrieve custom field information - Start {
                $ArrayCustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => str_replace('CustomField', '', $ColumnHeader)));
                $HeaderText = $ArrayCustomFieldInformation['FieldName'];
                // Retrieve custom field information - End }
            }
            $Data .= '"' . $HeaderText . '",';
        }

        $Data = substr($Data, 0, -1); // Remove comma from end of the line
        return $Data;
    }

    function GetCSVDataForASubscriber($ArrayFields, $ArraySubscriber) {
        // Convert custom field ids to CustomFieldID s - Start {
        foreach ($ArrayFields as &$ColumnHeader) {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            if (!$IsDefaultField) {
                $ColumnHeader = $ColumnHeader;
            }
        }
        // Convert custom field ids to CustomFieldID s - End }

        return self::ReturnCSVLine($ArraySubscriber, $ArrayFields, '"', ',');
    }

    /**
     * Returns a csv formatted line
     *
     * @param	$ArrayData Data that will be converted to csv line
     * @param	$ArrayIncludeFields Fields that will be excluded from csv line
     * @param	$StringEncloser Character that will enclose values
     * @param	$StringDelimeter Character that will delimete the values
     * @return 	string
     * @author Mert Hurturk
     * */
    public static function ReturnCSVLine($ArrayData, $ArrayIncludeFields, $StringEncloser, $StringDelimeter) {
        $CSVLine = "";
        foreach ($ArrayIncludeFields as $EachField) {
            if ($ArrayData[$EachField] == NULL || isset($ArrayData[$EachField])) {
                $data = str_replace('\\', '\\' . $StringEncloser, $ArrayData[$EachField]);
                $data = str_replace($StringEncloser, '\\' . $StringEncloser, $ArrayData[$EachField]);
                $data = str_replace($StringDelimeter, '\\' . $StringDelimeter, $data);
                $CSVLine .= $StringEncloser . $data . $StringEncloser . $StringDelimeter;
            }
        }
        // Remove delimeter from end of the line
        $CSVLine = substr($CSVLine, 0, -1);

        return $CSVLine;
    }

    /**
     * Returns XML formatted string of given subscribers and fields
     *
     * @return array $ArraySubscribers Subscribers
     * @return array $ArrayFields Fields
     * @return string
     * @author Mert Hurturk
     * */
    public static function GetXMLData($ArraySubscribers, $ArrayFields, $ArrayLanguageStrings = '') {
        if ($ArrayLanguageStrings == '') {
            global $ArrayLanguageStrings;
            $TMPArrayLanguageStrings = $ArrayLanguageStrings['Screen'];
        } else {
            $TMPArrayLanguageStrings = $ArrayLanguageStrings;
        }

        $Data = '';
        $Data .= "<?xml version=\"1.0\"?>\n"; // Add header node
        $Data .= "<subscribers>\n"; // Add wrapper start node
        // Convert custom field ids to CustomFieldID s - Start {
        foreach ($ArrayFields as &$ColumnHeader) {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            if (!$IsDefaultField) {
                $ColumnHeader = $ColumnHeader;
            }
        }
        // Convert custom field ids to CustomFieldID s - End }
        // Add data nodes - Start {
        foreach ($ArraySubscribers as $EachSubscriber) {
            $Data .= "<subscriber>\n";
            foreach ($EachSubscriber as $Field => $Value) {
                if (in_array($Field, $ArrayFields)) {
                    $Field = strtolower($Field); // Convert letters to lower case for key tags
                    $Value = "<![CDATA[" . $Value . "]]>"; // Escape html characters in field values
                    $Data .= "<" . $Field . ">" . $Value . "</" . $Field . ">\n";
                }
            }
            $Data .= "</subscriber>\n";
        }
        // Add data nodes - End }

        $Data .= "</subscribers>"; // Add wrapper end node

        return $Data;
    }

    /**
     * Returns TAB formatted string of given subscribers and fields
     *
     * @return array $ArraySubscribers Subscribers
     * @return array $ArrayFields Fields
     * @return string
     * @author Mert Hurturk
     * */
    public static function GetTABData($ArraySubscribers, $ArrayFields, $ArrayLanguageStrings = '') {
        if ($ArrayLanguageStrings == '') {
            global $ArrayLanguageStrings;
            $TMPArrayLanguageStrings = $ArrayLanguageStrings['Screen'];
        } else {
            $TMPArrayLanguageStrings = $ArrayLanguageStrings;
        }

        $Data = '';

        // Add column header line - Start {
        foreach ($ArrayFields as &$ColumnHeader) {
            $HeaderText = '';
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            if ($IsDefaultField) {
                $HeaderText = $TMPArrayLanguageStrings[$ColumnHeader];
            } else {
                $ArrayCustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => str_replace('CustomField', '', $ColumnHeader)));
                $HeaderText = $ArrayCustomFieldInformation['FieldName'];
            }
            $Data .= $HeaderText . "\t";
        }
        $Data = substr($Data, 0, -1); // Remove tab from end of the line
        $Data .= "\n"; // Add new line character at the end of line
        // Add column header line - End }
        // Convert custom field ids to CustomFieldID s - Start {
        foreach ($ArrayFields as &$ColumnHeader) {
            $IsDefaultField = false;
            foreach (Segments::$RuleDefaultFields as $EachDefaultField) {
                if ($EachDefaultField['CustomFieldID'] == $ColumnHeader) {
                    $IsDefaultField = true;
                    break;
                }
            }
            if (!$IsDefaultField) {
                $ColumnHeader = $ColumnHeader;
            }
        }
        // Convert custom field ids to CustomFieldID s - End }
        // Add data lines - Start {
        foreach ($ArraySubscribers as $EachSubscriber) {
            $Data .= self::ReturnCSVLine($EachSubscriber, $ArrayFields, '', "\t");
            $Data .= "\n";
        }
        // Add data lines - End }

        return $Data;
    }

    /**
     * Returns the value of a field of a subscriber formatted
     *
     * @param array $SubscriberInformation Subscriber information
     * @param array $FieldInformation  Field information
     * @return string
     * @author Mert Hurturk
     */
    public static function FormatSubscriberFieldValue($SubscriberInformation, $FieldInformation) {
        $FormattedFieldValue = '';
        if ($FieldInformation['ValidationMethod'] == 'Date' || $FieldInformation['ValidationMethod'] == 'Time') {
            $FormattedFieldValue = date($FieldInformation['ValidationRule'], strtotime($SubscriberInformation['CustomField' . $FieldInformation['CustomFieldID']]));
        } else {
            $FormattedFieldValue = $SubscriberInformation['CustomField' . $FieldInformation['CustomFieldID']];
        }
        return $FormattedFieldValue;
    }

    /**
     * Creates the import row in the database and returns fields for mapping
     *
     * @param string $UserID
     * @param string $ListID
     * @param string $ImportType
     * @param string $ImportData
     * @param string $FilePath
     * @param string $MySQLHost
     * @param string $MySQLPort
     * @param string $MySQLUsername
     * @param string $MySQLPassword
     * @param string $MySQLDatabase
     * @param string $MySQLQuery
     * @return array
     * @author Cem Hurturk
     */
    public static function Import($UserID, $ListID, $ImportType, $ImportData, $FilePath, $MySQLHost, $MySQLPort, $MySQLUsername, $MySQLPassword, $MySQLDatabase, $MySQLQuery, $FieldTerminator = ",", $FieldEncloser = "") {
        // Retrieve the import data based on import type - Start
        if ($ImportType == 'Copy') {
            // // Load CSV parser - Start
            // include_once(LIBRARY_PATH.'/magicparser.inc.php');
            // // Load CSV parser - End
            //
		// // Parse the import data - Start
            // $CSVFormatString = MagicParser_getFormat("string://".$ImportData);
            //
		// if ($CSVFormatString == false)
            // 	{
            // 	return array(false, 5);
            // 	}
            // else
            // 	{
            // 	// FIXME: Causing memory exhaust. Why do we need this function below???
            // 	MagicParser_parse("string://".$ImportData, "CSVHandler", $CSVFormatString);
            // 	}
            // // Parse the import data - End
            // Move data from MySQL to Oempro import data - Start
            $TMPFileName = DATA_PATH . 'imports/' . md5(rand(0, 5000) . rand(0, 5000) . date('Ymd'));
            $TMPImportData = $ImportData;
            $ImportData = 'FILE:' . $TMPFileName;
            $FileHandler = fopen($TMPFileName, 'w');
            fwrite($FileHandler, $TMPImportData);
            fclose($FileHandler);
            unset($TMPImportData);
            // Move data from MySQL to Oempro import data - End
            // Parse the import data - Start {
            global $ArrayImportFields;

            $FileHandler = fopen($TMPFileName, 'r');
            if ($FieldEncloser == '') {
                while (($EachLine = fgetcsv($FileHandler, 0, $FieldTerminator)) !== false) {
                    $ArrayImportFields[0] = array();
                    foreach ($EachLine as $Index => $EachValue) {
                        $ArrayImportFields[0]['FIELD' . ($Index + 1)] = $EachValue;
                    }
                    break;
                }
            } else {
                while (($EachLine = fgetcsv($FileHandler, 0, $FieldTerminator, $FieldEncloser)) !== false) {
                    $ArrayImportFields[0] = array();
                    foreach ($EachLine as $Index => $EachValue) {
                        $ArrayImportFields[0]['FIELD' . ($Index + 1)] = $EachValue;
                    }
                    break;
                }
            }
            fclose($FileHandler);
            // Parse the import data - End }
        } elseif ($ImportType == 'File') {
            // Open the file and get the contents into the variable - Start
            // $FileHandler = fopen($FilePath, 'r');
            //
		// $BriefData				= '';
            // $ImportData				= 'FILE:'.$FilePath;
            //
		// while (feof($FileHandler) == false)
            // 	{
            // 	// Read only one line
            // 	$BriefData .= fread($FileHandler, 8192);
            // 	break;
            // 	}
            // fclose($FileHandler);
            // Open the file and get the contents into the variable - End
            // // Load CSV parser - Start
            // include_once(LIBRARY_PATH.'/magicparser.inc.php');
            // // Load CSV parser - End
            //
		// // Parse the import data - Start
            // $CSVFormatString = MagicParser_getFormat("string://".$BriefData);
            //
		// if ($CSVFormatString == false)
            // 	{
            // 	return array(false, 5);
            // 	}
            // else
            // 	{
            // 	MagicParser_parse("string://".$BriefData, "CSVHandler", $CSVFormatString);
            // 	}
            // // Parse the import data - End
            // Parse the import data - Start {
            $ImportData = 'FILE:' . $FilePath;

            global $ArrayImportFields;

            $FileHandler = fopen($FilePath, 'r');
            if ($FieldEncloser == '') {
                while (($EachLine = fgetcsv($FileHandler, 0, $FieldTerminator)) !== false) {
                    $ArrayImportFields[0] = array();
                    foreach ($EachLine as $Index => $EachValue) {
                        $ArrayImportFields[0]['FIELD' . ($Index + 1)] = $EachValue;
                    }
                    break;
                }
            } else {
                while (($EachLine = fgetcsv($FileHandler, 0, $FieldTerminator, $FieldEncloser)) !== false) {
                    $ArrayImportFields[0] = array();
                    foreach ($EachLine as $Index => $EachValue) {
                        $ArrayImportFields[0]['FIELD' . ($Index + 1)] = $EachValue;
                    }
                    break;
                }
            }
            fclose($FileHandler);
            // Parse the import data - End }
        } elseif ($ImportType == 'MySQL') {
            global $ArrayImportFields;

            // Connect to target MySQL database, retrieve data  - Start
            $ObjectMySQLTest = new MySqlDatabaseInterface();
            $ConnectionResult = $ObjectMySQLTest->Connect($MySQLHost . ':' . $MySQLPort, $MySQLUsername, $MySQLPassword, $MySQLDatabase, '');
            if ($ConnectionResult == false) {
                return array(false, 15);
            }

            $ObjectMySQLTest->ExecuteQuery('SET collation_connection = utf8_unicode_ci');
            $ObjectMySQLTest->ExecuteQuery('SET NAMES utf8');
            $ResultSet = $ObjectMySQLTest->ExecuteQuery($MySQLQuery);
            if ($ResultSet == false) {
                return array(false, 18);
            }

            // Fetch the first row and gather result columns for field mapping - Start
            $FirstRow = mysql_fetch_assoc($ResultSet);

            $TMPCounter = 1;
            foreach (array_keys($FirstRow) as $EachKey) {
                $ArrayImportFields[0]['FIELD' . $TMPCounter] = $EachKey;
                $TMPCounter++;
            }
            // Fetch the first row and gather result columns for field mapping - End

            @mysql_data_seek($ResultSet, 0);

            // Move data from MySQL to Oempro import data - Start
            $TMPFileName = DATA_PATH . 'imports/' . md5(rand(0, 5000) . rand(0, 5000) . date('Ymd'));
            $ImportData = 'FILE:' . $TMPFileName;
            $FileHandler = fopen($TMPFileName, 'w');
            while ($EachRow = mysql_fetch_assoc($ResultSet)) {
                fwrite($FileHandler, implode('|', array_values($EachRow)) . "\n");
            }
            fclose($FileHandler);
            // Move data from MySQL to Oempro import data - End

            $ObjectMySQLTest->CloseConnection();
            // Connect to target MySQL database, retrieve data  - End
        } else {
            return array(false, 17);
        }
        // Retrieve the import data based on import type - End
        // Create the import data in the database - Start
        $ArrayFieldAndValues = array(
            'ImportID' => '',
            'RelUserID' => $UserID,
            'RelListID' => $ListID,
            'ImportDate' => date('Y-m-d H:i:s'),
            'ImportDuration' => 0,
            'ImportStatus' => 'Not Ready',
            'ImportData' => $ImportData,
            'FieldTerminator' => $FieldTerminator,
            'FieldEncloser' => $FieldEncloser,
            'DuplicateData' => '',
            'FailedData' => '',
            'TotalSubscribers' => 0,
            'TotalImported' => 0,
            'TotalDuplicates' => 0,
            'TotalFailed' => 0,
        );
        Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX . 'subscriber_imports');
        $NewImportID = Database::$Interface->GetLastInsertID();
        // Create the import data in the database - End

        return array(true, $NewImportID);
    }

    /**
     * Retrieves and returns the import record
     *
     * @param string $UserID
     * @param string $ListID
     * @param string $ImportID
     * @param array $ArrayCriterias
     * @return boolean|integer
     * @author Cem Hurturk
     */
    public static function GetImportRecord($UserID, $ListID, $ImportID, $ArrayCriterias = array()) {
        $ArrayFields = array('*');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscriber_imports');
        $ArrayCriterias = array_merge(array('RelUserID' => $UserID, 'RelListID' => $ListID, 'ImportID' => $ImportID), $ArrayCriterias);
        $ArrayOrder = array();
        $ArrayImportRecord = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        return $ArrayImportRecord;
    }

    /**
     * Updates the import data
     *
     * @param string $UserID
     * @param string $ListID
     * @param string $ImportID
     * @param string $ArrayUpdateFields
     * @param string $ArrayCriterias
     * @return void
     * @author Cem Hurturk
     */
    public static function UpdateImportRecord($UserID, $ListID, $ImportID, $ArrayUpdateFields, $ArrayCriterias = array()) {
        Database::$Interface->UpdateRows($ArrayUpdateFields, array_merge(array('RelUserID' => $UserID, 'RelListID' => $ListID, 'ImportID' => $ImportID), $ArrayCriterias), MYSQL_TABLE_PREFIX . 'subscriber_imports');
        return;
    }

    /**
     * Calculates the total number of subscriber available in the account
     *
     * @return void
     * @author Eman Mohammed
     */
    public static function getTotalSubscribersOnTheAccount_Enhanced($UserID, $ReturnEmails = false) {
        $TotalSubscribers = 0;
        $Subscribers = array();

        Core::LoadObject('lists');

        $ArrayLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $UserID), array('ListID' => 'ASC'), array(), 0, 0);

        foreach ($ArrayLists as $Index => $ArrayEachList) {
            $ArraySubscribers = self::getListSubscribersWithSupressed($ArrayEachList['ListID'], $UserID);
            $Subscribers = array_unique(array_merge($Subscribers, $ArraySubscribers), SORT_REGULAR);
        }
        //get global suppression list - remove it from array
        $GlobalSuppressed = self::getGlobalSupressed($UserID);

        $FinalSubscribrs = array();
        foreach ($Subscribers as $Subscriber) {
            $exists = array_search($Subscriber, $GlobalSuppressed); //in_array

            if ($exists === false) {
                $FinalSubscribrs[] = $Subscriber;
            }
        }
        return $ReturnEmails ? $FinalSubscribrs : count($FinalSubscribrs);
    }

    /**
     * get the details of number of subscriber available in the account
     *
     * @return void
     * @author Eman Mohammed
     */
    public static function getSubscribersOnTheAccountDetails($UserID, $ReturnEmails = false) {
        $TotalSubscribers = 0;
        $Subscribers = array();

        Core::LoadObject('lists');

        $ArrayLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $UserID), array('ListID' => 'ASC'), array(), 0, 0);

        $Lists = array();
        $TotalUniqueCount = 0;

        foreach ($ArrayLists as $Index => $ArrayEachList) {

            $ListUniqueSuppressedCount = 0;
            //total count fo subscribers in current loop
            $TotalUniqueCount = count($Subscribers);

            $Result = self::getListSubscribersWithSupressed($ArrayEachList['ListID'], $UserID, true);
            $ArraySubscribers = $Result[0]; //All subscribers
            $ListOriginalCount = count($ArraySubscribers);

            $ArrayActiveSubscribers = $Result[1];  //active subscribers
            $ArraySuppressesSubscribers = $Result[2];  // suppressed subscribers

            $PrevSubscribersCount = count($Subscribers); // subscribers count before adding unique active subscribers to list
            //merge unique active subscribers to list
            $Subscribers = array_unique(array_merge($Subscribers, $ArrayActiveSubscribers), SORT_REGULAR);

            //get diffrence between count of emails added to subscribers list and actual count
            $Difference = ($PrevSubscribersCount + count($ArrayActiveSubscribers)) - count($Subscribers);

            //unique active count 
            $ListUniqueActiveCount = count($ArrayActiveSubscribers) - $Difference;

            $PrevSubscribersCount = count($Subscribers);

            $Subscribers = array_unique(array_merge($Subscribers, $ArraySuppressesSubscribers), SORT_REGULAR);

            $Difference = ($PrevSubscribersCount + count($ArraySuppressesSubscribers)) - count($Subscribers);
            $ListUniqueSuppressedCount = count($ArraySuppressesSubscribers) - $Difference;

            $ListUniqueCount = $ListUniqueActiveCount + $ListUniqueSuppressedCount;

//            $ListUniqueCount = $ListOriginalCount - $Difference;

            $List = Lists::RetrieveList(array('*'), array('ListID' => $ArrayEachList['ListID']));
            $Lists[] = array(
                "ListID" => $ArrayEachList['ListID'],
                "ListTotalCount" => count($ArraySubscribers),
                "ListUniqueCount" => $ListUniqueCount,
                "ListUniqueActiveCount" => $ListUniqueActiveCount,
                "ListUniqueSuppressedCount" => $ListUniqueSuppressedCount,
                "ListInformation" => $List,
            );
        }
        //get global suppression list - remove it from array
        $GlobalSuppressed = self::getGlobalSupressed($UserID);

//        $Lists[] = array(
//            "ListID" => 0,
//            "ListTotalCount" => count($GlobalSuppressed),
//            "ListUniqueCount" => count($GlobalSuppressed),
//            "ListUniqueActiveCount" => 0,
//            "ListUniqueSuppressedCount" => 0,
//            "ListInformation" => array(
//                "Name" => "Suppression List",
//                "SubscriberCount" => count($GlobalSuppressed),
//            )
//        );

        return $Lists;
    }

    /**
     * Calculates the total number of subscriber available in the account
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function TotalSubscribersOnTheAccount($UserID) {
        $TotalSubscribers = 0;

        Core::LoadObject('lists');

        $ArrayLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $UserID), array('ListID' => 'ASC'), array(), 0, 0);

        foreach ($ArrayLists as $Index => $ArrayEachList) {
            $TotalSubscribers += self::GetActiveTotal($ArrayEachList['ListID']);
            // $TotalSubscribers += self::GetFoundTotal('', '', $ArrayEachList['ListID'], 0, false);
        }

        return $TotalSubscribers;
    }

    /**
     * Add new subscriber into subscriber list
     *
     * @param string $ArrayList
     * @param string $EmailAddress
     * @param string $ArrayOtherFields
     * @param string $SubscriptionStatus
     * @param string $SubscriptionIP
     * @return integer|array
     * @author Cem Hurturk
     * @Updated Eman Mohammed
     */
    public static function AddSubscriber($ArrayUserInformation, $ArrayList, $EmailAddress, $ArrayOtherFields, $SubscriptionStatus = 'Subscribed', $SubscriptionIP = '', $StopOnDuplicate = false, $UpdateAlreadyUnsubscribed = false) {
        // Check if monthly limit exceeded for storing subscribers - Start
//        if (($ArrayUserInformation['GroupInformation']['LimitSubscribers'] > 0) && (self::TotalSubscribersOnTheAccount($ArrayUserInformation['UserID']) >= $ArrayUserInformation['GroupInformation']['LimitSubscribers'])) {
        //Eman
        if (($ArrayUserInformation['GroupInformation']['LimitSubscribers'] > 0) && (self::getTotalSubscribersOnTheAccount_Enhanced($ArrayUserInformation['UserID']) >= $ArrayUserInformation['GroupInformation']['LimitSubscribers'])) {
            return array(false, 'failed');
        }
        // Check if monthly limit exceeded for storing subscribers - End
        // Validate email address format - Start
        if (self::ValidateEmailAddress($EmailAddress) == false) {
            return array(false, 'invalid');
        }
        // Validate email address format - End
        // Check if the email address already exists in the subscriber list - Start
        $ShouldMemberUpdated = false;

        if (($ArraySubscriber = self::CheckIfSubscriberExists($EmailAddress, $ArrayList['ListID'], false, 0)) != false) {
            if (($ArraySubscriber['SubscriptionStatus'] == 'Unsubscribed') && ($UpdateAlreadyUnsubscribed == false)) {
                return array(false, 'duplicate');
            }

            if ($StopOnDuplicate == false) {
                $ShouldMemberUpdated = true;
            } else {
                if (($ArraySubscriber['SubscriptionStatus'] == 'Opt-In Pending') || ($ArraySubscriber['SubscriptionStatus'] == 'Subscribed')) {
                    return array(false, 'duplicate');
                } else {
                    $ShouldMemberUpdated = true;
                }
            }
        }
        // Check if the email address already exists in the subscriber list - End
        // Add subscriber - Start
        if ($ShouldMemberUpdated == false) {
            $ArrayFieldnValues = array(
                'SubscriberID' => '',
                'EmailAddress' => $EmailAddress,
                'BounceType' => 'Not Bounced',
                'SubscriptionStatus' => $SubscriptionStatus,
                'SubscriptionDate' => ($SubscriptionStatus == 'Subscribed' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
                'SubscriptionIP' => $SubscriptionIP,
                'UnsubscriptionDate' => '0000-00-00 00:00:00',
                'UnsubscriptionIP' => '0.0.0.0',
                'OptInDate' => ($SubscriptionStatus == 'Opt-In Pending' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
            );

            if (count($ArrayOtherFields) > 0) {
                foreach ($ArrayOtherFields as $Key => $Value) {
                    if (strtolower(gettype($Value)) == 'array') {
                        $ArrayFieldnValues[$Key] = implode('||||', $Value);
                    } else {
                        $ArrayFieldnValues[$Key] = $Value;
                    }
                }
            }

            $ResultSet = Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayList['ListID']);
            $NewSubscriberID = Database::$Interface->GetLastInsertID();
        } else {
            $ArrayFieldnValues = array(
                'BounceType' => 'Not Bounced',
                'SubscriptionStatus' => $SubscriptionStatus,
                'SubscriptionDate' => ($SubscriptionStatus == 'Subscribed' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
                'SubscriptionIP' => $SubscriptionIP,
                'UnsubscriptionDate' => '0000-00-00 00:00:00',
                'UnsubscriptionIP' => '0.0.0.0',
                'OptInDate' => ($SubscriptionStatus == 'Opt-In Pending' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
            );

            if (count($ArrayOtherFields) > 0) {
                foreach ($ArrayOtherFields as $Key => $Value) {
                    if (strtolower(gettype($Value)) == 'array') {
                        $ArrayFieldnValues[$Key] = implode('||||', $Value);
                    } else {
                        $ArrayFieldnValues[$Key] = $Value;
                    }
                }
            }

            $ArrayCriterias = array(
                'SubscriberID' => $ArraySubscriber['SubscriberID'],
            );
            Database::$Interface->UpdateRows($ArrayFieldnValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayList['ListID']);

            $NewSubscriberID = $ArraySubscriber['SubscriberID'];
        }
        // Add subscriber - End

        return $NewSubscriberID;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function AddSubscriber_Enhanced($Parameters) {
        Core::LoadObject('custom_fields');

        // Possible return values - Start {
        $LIMIT_EXCEEDED = 1;
        $INVALID_EMAIL_ADDRESS = 2;
        $DUPLICATE_EMAIL_ADDRESS = 3;
        $UNSUBSCRIBED_EMAIL_ADDRESS = 4;
        $INVALID_USER_INFORMATION = 5;
        $INVALID_LIST_INFORMATION = 6;
        // Possible return values - End }
        // Default parameters for method - Start {
        $DefaultParameters = array(
            'CheckSubscriberLimit' => true,
            'UserInformation' => array(),
            'ListInformation' => array(),
            'EmailAddress' => '',
            'IPAddress' => '',
            'SubscriptionStatus' => '',
            'OtherFields' => array(),
            'UpdateIfDuplicate' => false,
            'UpdateIfUnsubscribed' => false,
            'ApplyBehaviors' => false,
            'SendConfirmationEmail' => false,
            'UpdateStatistics' => false,
            'TriggerWebServices' => false,
            'TriggerAutoResponders' => false,
            'IsSubscriptionAction' => false
        );
        $Parameters = array_merge($DefaultParameters, $Parameters);
        // Default parameters for method - End }

        try {
            // Parameter validation - Start {
            if (!is_array($Parameters['UserInformation']) || count($Parameters['UserInformation']) < 1)
                throw new Exception($INVALID_USER_INFORMATION);
            if (!is_array($Parameters['ListInformation']) || count($Parameters['ListInformation']) < 1)
                throw new Exception($INVALID_LIST_INFORMATION);
            $Parameters['EmailAddress'] = trim($Parameters['EmailAddress']);
            if (!self::ValidateEmailAddress($Parameters['EmailAddress']))
                throw new Exception($INVALID_EMAIL_ADDRESS);
            // Parameter validation - End }

            $SubscriberInformation = self::CheckIfSubscriberExists($Parameters['EmailAddress'], $Parameters['ListInformation']['ListID'], false, 0);
            $DoesSubscriberExist = $SubscriberInformation != false;
            $IsSubscriberUnsubscribed = $DoesSubscriberExist && $SubscriberInformation['SubscriptionStatus'] == 'Unsubscribed';
            if ($DoesSubscriberExist && ($Parameters['UpdateIfDuplicate'] == false || ($Parameters['IsSubscriptionAction'] == true && !$IsSubscriberUnsubscribed)))
                throw new Exception($DUPLICATE_EMAIL_ADDRESS);
            if ($IsSubscriberUnsubscribed && $Parameters['UpdateIfUnsubscribed'] == false)
                throw new Exception($UNSUBSCRIBED_EMAIL_ADDRESS);

            // Add subscriber - Start {
            if (!$DoesSubscriberExist) {
                // Check if monthly limit is exceeded for storing subscribers - Start {
                if ($Parameters['CheckSubscriberLimit']) {
                    $IsSubscriberCountLimited = $Parameters['UserInformation']['GroupInformation']['LimitSubscribers'] > 0;
//                    $IsSubscriberCountIsBiggerThanLimit = self::TotalSubscribersOnTheAccount($Parameters['UserInformation']['UserID']) >= $Parameters['UserInformation']['GroupInformation']['LimitSubscribers'];
                    //Updated by Eman
                    $IsSubscriberCountIsBiggerThanLimit = self::getTotalSubscribersOnTheAccount_Enhanced($Parameters['UserInformation']['UserID']) >= $Parameters['UserInformation']['GroupInformation']['LimitSubscribers'];
                    if ($IsSubscriberCountLimited && $IsSubscriberCountIsBiggerThanLimit)
                        throw new Exception($LIMIT_EXCEEDED);
                }
                // Check if monthly limit is exceeded for storing subscribers - End }
                // Prepare subscriber parameters - Start {
                $SubscriptionStatus = ($Parameters['SubscriptionStatus'] == '' ? ($Parameters['ListInformation']['OptInMode'] == 'Single' ? 'Subscribed' : 'Opt-In Pending') : $Parameters['SubscriptionStatus']);
                $ArrayFieldnValues = array(
                    'SubscriberID' => '',
                    'EmailAddress' => $Parameters['EmailAddress'],
                    'BounceType' => 'Not Bounced',
                    'SubscriptionStatus' => $SubscriptionStatus,
                    'SubscriptionDate' => ($SubscriptionStatus == 'Subscribed' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
                    'SubscriptionIP' => $Parameters['IPAddress'],
                    'UnsubscriptionDate' => '0000-00-00 00:00:00',
                    'UnsubscriptionIP' => '0.0.0.0',
                    'OptInDate' => ($SubscriptionStatus == 'Opt-In Pending' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'),
                );
                $GlobalCustomFields = array();
                if (count($Parameters['OtherFields']) > 0) {
                    foreach ($Parameters['OtherFields'] as $Key => $Value) {
                        if (substr($Key, 0, 11) == 'CustomField') {
                            if (isset(self::$CustomFieldInformationCache[substr($Key, 11)])) {
                                $ArrayCustomField = self::$CustomFieldInformationCache[substr($Key, 11)];
                            } else {
                                self::$CustomFieldInformationCache[substr($Key, 11)] = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => substr($Key, 11)));
                                $ArrayCustomField = self::$CustomFieldInformationCache[substr($Key, 11)];
                            }
                            if ($ArrayCustomField['IsGlobal'] == 'Yes') {
                                $GlobalCustomFields[$Key] = (strtolower(gettype($Value)) == 'array' ? implode('||||', $Value) : $Value);
                                continue;
                            }
                        }

                        $ArrayFieldnValues[$Key] = (strtolower(gettype($Value)) == 'array' ? implode('||||', $Value) : $Value);
                    }
                }
                // Prepare subscriber parameters - End }
                // Add to the database
                Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'subscribers_' . $Parameters['ListInformation']['ListID']);
                $NewSubscriberID = Database::$Interface->GetLastInsertID();
                $ReturnSubscriberID = $NewSubscriberID;

                foreach ($GlobalCustomFields as $EachField => $Value) {
                    $CustomFieldID = substr($EachField, 11);
                    if (isset(self::$CustomFieldInformationCache[$CustomFieldID])) {
                        $CustomFieldInformation = self::$CustomFieldInformationCache[$CustomFieldID];
                    } else {
                        self::$CustomFieldInformationCache[$CustomFieldID] = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID));
                        $CustomFieldInformation = self::$CustomFieldInformationCache[$CustomFieldID];
                    }
                    CustomFields::UpdateGlobalFieldData($Parameters['UserInformation']['UserID'], 0, $ArrayFieldnValues['EmailAddress'], $CustomFieldInformation, $Value);
                }

                // Retrieve the new subscriber information
                // $NewSubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $NewSubscriberID), $Parameters['ListInformation']['ListID']);
                $NewSubscriberInformation = array_merge($ArrayFieldnValues, $GlobalCustomFields);
                $NewSubscriberInformation['SubscriberID'] = $ReturnSubscriberID;
                $IsSubscribed = $NewSubscriberInformation['SubscriptionStatus'] == 'Subscribed';

                // Send confirmation email - Start {
                if ($Parameters['SendConfirmationEmail'] && $Parameters['ListInformation']['OptInMode'] == 'Double' && !$IsSubscribed) {
                    self::SendConfirmationEmail($NewSubscriberInformation, $Parameters['ListInformation'], $Parameters['UserInformation']);
                }
                // Send confirmation email - End }
                // Update statistics - Start {
                if ($Parameters['UpdateStatistics'] && $IsSubscribed) {
                    Core::LoadObject('statistics');
                    $ArrayActivities = array('TotalSubscriptions' => 1);
                    Statistics::UpdateListActivityStatistics($Parameters['ListInformation']['ListID'], $Parameters['UserInformation']['UserID'], $ArrayActivities);
                }
                // Update statistics - End }
                // Trigger web services - Start {
                if ($Parameters['TriggerWebServices'] && $IsSubscribed) {
                    Core::LoadObject('web_service_integration');
                    $WebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $Parameters['ListInformation']['ListID'], 'RelOwnerUserID' => $Parameters['UserInformation']['UserID'], 'EventType' => 'subscription'), array('ServiceURL' => 'ASC'));
                    if ($WebServices != FALSE && count($WebServices) > 0) {
                        foreach ($WebServices as $Key => $EachWebService) {
                            WebServiceIntegration::WebServiceConnection('Subscription', $NewSubscriberInformation, $EachWebService['ServiceURL']);
                        }
                    }
                }
                // Trigger web services - End }
                // Trigger auto responders - Start {
                if ($Parameters['TriggerAutoResponders'] && $IsSubscribed) {
                    Core::LoadObject('auto_responders');
                    AutoResponders::RegisterAutoResponders($Parameters['ListInformation']['ListID'], $NewSubscriberInformation['SubscriberID'], $Parameters['UserInformation']['UserID'], 'OnSubscription');
                }
                // Trigger auto responders - End }
                // Apply list behaviors - Start {
                // Subscribe to behavior - Start {
                $DoesHaveSubscribeToBehavior = $Parameters['ListInformation']['OptInSubscribeTo'] != 0 && $Parameters['ListInformation']['OptInSubscribeTo'] != '';
                if ($DoesHaveSubscribeToBehavior && $IsSubscribed) {
                    $BehaviorSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $Parameters['ListInformation']['OptInSubscribeTo']));
                    if ($BehaviorSubscriberListInformation != false) {
                        Subscribers::AddSubscriber_Enhanced(array(
                            'CheckSubscriberLimit' => false,
                            'UserInformation' => $Parameters['UserInformation'],
                            'ListInformation' => $BehaviorSubscriberListInformation,
                            'EmailAddress' => $NewSubscriberInformation['EmailAddress'],
                            'IPAddress' => $NewSubscriberInformation['SubscriptionIP'],
                            'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                            'UpdateIfDuplicate' => true,
                            'UpdateIfUnsubscribed' => true,
                            'ApplyBehaviors' => true,
                            'SendConfirmationEmail' => false,
                            'UpdateStatistics' => true,
                            'TriggerWebServices' => false,
                            'TriggerAutoResponders' => false
                        ));
                    }
                }
                // Subscribe to behavior - End }
                // Unsubscribe from behavior - Start {
                $DoesHaveUnsubscribeToBehavior = $Parameters['ListInformation']['OptInUnsubscribeFrom'] != 0 && $Parameters['ListInformation']['OptInUnsubscribeFrom'] != '';
                if ($DoesHaveUnsubscribeToBehavior && $IsSubscribed) {
                    $BehaviorUnsubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $Parameters['ListInformation']['OptInUnsubscribeFrom']));
                    if ($BehaviorUnsubscriberListInformation != false) {
                        $BehaviorSubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $NewSubscriberInformation['EmailAddress']), $BehaviorUnsubscriberListInformation['ListID']);
                        if ($BehaviorSubscriberInformation != false) {
                            Subscribers::Unsubscribe($BehaviorUnsubscriberListInformation, $Parameters['UserInformation']['UserID'], 0, 0, $BehaviorSubscriberInformation['EmailAddress'], $BehaviorSubscriberInformation['SubscriberID'], $NewSubscriberInformation['SubscriptionIP'], '');
                        }
                    }
                }
                // Unsubscribe from behavior - End }
                // Apply list behaviors - End }
            }
            // Add subscriber - End }
            // Update subscriber - Start {
            else {
                // Prepare subscriber parameters - Start {
                $ArrayFieldnValues = array(
                    'EmailAddress' => $Parameters['EmailAddress']
                );
                $GlobalCustomFields = array();
                if (count($Parameters['OtherFields']) > 0) {
                    foreach ($Parameters['OtherFields'] as $Key => $Value) {
                        if (substr($Key, 0, 11) == 'CustomField') {
                            $ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => substr($Key, 11), 'RelOwnerUserID' => $Parameters['UserInformation']['UserID']));
                            if ($ArrayCustomField['IsGlobal'] == 'Yes') {
                                $GlobalCustomFields[$Key] = (strtolower(gettype($Value)) == 'array' ? implode('||||', $Value) : $Value);
                                continue;
                            }
                        }

                        $ArrayFieldnValues[$Key] = (strtolower(gettype($Value)) == 'array' ? implode('||||', $Value) : $Value);
                    }
                }
                // Prepare subscriber parameters - End }

                if ($Parameters['IsSubscriptionAction']) {
                    if ($IsSubscriberUnsubscribed) {
                        $ArrayFieldnValues['SubscriptionStatus'] = $Parameters['ListInformation']['OptInMode'] == 'Single' ? 'Subscribed' : 'Opt-In Pending';
                    }
                }

                $ArrayCriterias = array(
                    'SubscriberID' => $SubscriberInformation['SubscriberID'],
                );

                Database::$Interface->UpdateRows($ArrayFieldnValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'subscribers_' . $Parameters['ListInformation']['ListID']);

                foreach ($GlobalCustomFields as $EachField => $Value) {
                    $CustomFieldID = substr($EachField, 11);
                    $CustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID));
                    CustomFields::UpdateGlobalFieldData($Parameters['UserInformation']['UserID'], 0, $ArrayFieldnValues['EmailAddress'], $CustomFieldInformation, $Value);
                }

                $SubscriberInformation = array_merge($SubscriberInformation, $ArrayFieldnValues);
                $SubscriberInformation = array_merge($SubscriberInformation, $GlobalCustomFields);
                $ReturnSubscriberID = $SubscriberInformation['SubscriberID'];

                if ($Parameters['IsSubscriptionAction'] == true) {
                    // Send confirmation email - Start {
                    if ($Parameters['SendConfirmationEmail'] && $Parameters['ListInformation']['OptInMode'] == 'Double' && $SubscriberInformation['SubscriptionStatus'] == 'Opt-In Pending') {
                        self::SendConfirmationEmail($SubscriberInformation, $Parameters['ListInformation'], $Parameters['UserInformation']);
                    }
                    // Send confirmation email - End }
                    // Update statistics - Start {
                    if ($Parameters['UpdateStatistics'] && $SubscriberInformation['SubscriptionStatus'] == 'Subscribed') {
                        Core::LoadObject('statistics');
                        $ArrayActivities = array('TotalSubscriptions' => 1);
                        Statistics::UpdateListActivityStatistics($Parameters['ListInformation']['ListID'], $Parameters['UserInformation']['UserID'], $ArrayActivities);
                    }
                    // Update statistics - End }
                    // Remove from local suppression list - Start {
                    $suppressionMapper = O_Registry::instance()->getMapper('Suppression');
                    $suppressionMapper->deleteByEmailAddressAndListId($SubscriberInformation['EmailAddress'], $Parameters['ListInformation']['ListID']);
                    // Remove from local suppression list - End }
                    // Trigger web services - Start {
                    if ($Parameters['TriggerWebServices'] && $SubscriberInformation['SubscriptionStatus'] == 'Subscribed') {
                        Core::LoadObject('web_service_integration');
                        $WebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $Parameters['ListInformation']['ListID'], 'RelOwnerUserID' => $Parameters['UserInformation']['UserID'], 'EventType' => 'subscription'), array('ServiceURL' => 'ASC'));
                        if ($WebServices != FALSE && count($WebServices) > 0) {
                            foreach ($WebServices as $Key => $EachWebService) {
                                WebServiceIntegration::WebServiceConnection('Subscription', $SubscriberInformation, $EachWebService['ServiceURL']);
                            }
                        }
                    }
                    // Trigger web services - End }
                    // Trigger auto responders - Start {
                    if ($Parameters['TriggerAutoResponders'] && $SubscriberInformation['SubscriptionStatus'] == 'Subscribed') {
                        Core::LoadObject('auto_responders');
                        AutoResponders::RegisterAutoResponders($Parameters['ListInformation']['ListID'], $SubscriberInformation['SubscriberID'], $Parameters['UserInformation']['UserID'], 'OnSubscription');
                    }
                    // Trigger auto responders - End }
                    // Apply list behaviors - Start {
                    // Subscribe to behavior - Start {
                    $DoesHaveSubscribeToBehavior = $Parameters['ListInformation']['OptInSubscribeTo'] != 0 && $Parameters['ListInformation']['OptInSubscribeTo'] != '';
                    if ($DoesHaveSubscribeToBehavior && $SubscriberInformation['SubscriptionStatus'] == 'Subscribed') {
                        $BehaviorSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $Parameters['ListInformation']['OptInSubscribeTo']));
                        if ($BehaviorSubscriberListInformation != false) {
                            Subscribers::AddSubscriber_Enhanced(array(
                                'CheckSubscriberLimit' => false,
                                'UserInformation' => $Parameters['UserInformation'],
                                'ListInformation' => $BehaviorSubscriberListInformation,
                                'EmailAddress' => $SubscriberInformation['EmailAddress'],
                                'IPAddress' => $SubscriberInformation['SubscriptionIP'],
                                'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                                'UpdateIfDuplicate' => true,
                                'UpdateIfUnsubscribed' => true,
                                'ApplyBehaviors' => true,
                                'SendConfirmationEmail' => false,
                                'UpdateStatistics' => true,
                                'TriggerWebServices' => false,
                                'TriggerAutoResponders' => false
                            ));
                        }
                    }
                    // Subscribe to behavior - End }
                    // Unsubscribe from behavior - Start {
                    $DoesHaveUnsubscribeToBehavior = $Parameters['ListInformation']['OptInUnsubscribeFrom'] != 0 && $Parameters['ListInformation']['OptInUnsubscribeFrom'] != '';
                    if ($DoesHaveUnsubscribeToBehavior && $SubscriberInformation['SubscriptionStatus'] == 'Subscribed') {
                        $BehaviorUnsubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $Parameters['ListInformation']['OptInUnsubscribeFrom']));
                        if ($BehaviorUnsubscriberListInformation != false) {
                            $BehaviorSubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $SubscriberInformation['EmailAddress']), $BehaviorUnsubscriberListInformation['ListID']);
                            if ($BehaviorSubscriberInformation != false) {
                                Subscribers::Unsubscribe($BehaviorUnsubscriberListInformation, $Parameters['UserInformation']['UserID'], 0, 0, $BehaviorSubscriberInformation['EmailAddress'], $BehaviorSubscriberInformation['SubscriberID'], $SubscriberInformation['SubscriptionIP'], '');
                            }
                        }
                    }
                    // Unsubscribe from behavior - End }
                    // Apply list behaviors - End }
                }
            }
            // Update subscriber - End }
        } catch (Exception $e) {
            return array(false, (int) $e->getMessage());
        }

        return array(true, $ReturnSubscriberID);
    }

    public static function SendConfirmationEmail($SubscriberInformation, $ListInformation, $UserInformation) {
        Core::LoadObject('emails');
        Core::LoadObject('personalization');

        // Set email content - Start {
        $EmailInformation = false;
        $DoesListHaveConfirmationEmail = $ListInformation['RelOptInConfirmationEmailID'] != 0 && $ListInformation['RelOptInConfirmationEmailID'] != '';
        if ($DoesListHaveConfirmationEmail) {
            $EmailInformation = Emails::RetrieveEmail(array('*'), array('EmailID' => $ListInformation['RelOptInConfirmationEmailID']));
        }

        if (!$DoesListHaveConfirmationEmail || !$EmailInformation) {
            // Default opt-in confirmation email
            $EmailInformation = array(
                'EmailID' => 0,
                'RelUserID' => $UserInformation['UserID'],
                'EmailName' => '',
                'FromName' => $UserInformation['FirstName'] . ' ' . $UserInformation['LastName'],
                'FromEmail' => $UserInformation['EmailAddress'],
                'ReplyToName' => $UserInformation['FirstName'] . ' ' . $UserInformation['LastName'],
                'ReplyToEmail' => $UserInformation['EmailAddress'],
                'ContentType' => 'Plain',
                'Mode' => 'Empty',
                'FetchURL' => '',
                'Subject' => DEFAULT_OPTIN_EMAIL_SUBJECT,
                'PlainContent' => DEFAULT_OPTIN_EMAIL_BODY,
                'HTMLContent' => '',
                'ImageEmbedding' => 'Disabled',
                'RelTemplateID' => 0,
            );
        }
        // Set email content - End }

        O_Email_Sender_ForUserGroup::send(
                $UserInformation['GroupInformation'], O_Email_Factory::optInConfirmationEmail(
                        $EmailInformation, $SubscriberInformation, $ListInformation, $UserInformation
                )
        );
    }

    /**
     * Updates subscriber information
     *
     * @param array $ArrayFieldAndValues Values of new subscriber information
     * @param array $ArrayCriterias Criterias to be matched while updating subscriber
     * @return boolean
     * @author Mert Hurturk
     * */
    public static function Update($SubscriberListID, $ArrayFieldAndValues, $ArrayCriterias) {
        // Check for required fields of this function - Start
        // $ArrayCriterias check
        if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) {
            return false;
        }
        // Check for required fields of this function - End
        // If email address is being updated, validate it - Start
        if (isset($ArrayFieldAndValues['EmailAddress']) && ($ArrayFieldAndValues['EmailAddress'] != '') && ($ArrayCriterias['SubscriberID'] != '') && (isset($ArrayCriterias['SubscriberID']))) {
            // Validate email address format - Start
            if (self::ValidateEmailAddress($ArrayFieldAndValues['EmailAddress']) == false) {
                return array(false, 'invalid');
            }
            // Validate email address format - End
            // Check if the email address already exists in the subscriber list - Start
            $ShouldMemberUpdated = false;

            if (($ArraySubscriber = self::CheckIfSubscriberExists($ArrayFieldAndValues['EmailAddress'], $SubscriberListID, false, $ArrayCriterias['SubscriberID'])) != false) {
                return array(false, 'duplicate');
            }
            // Check if the email address already exists in the subscriber list - End
        }
        // If email address is being updated, validate it - End


        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'subscribers_' . $SubscriberListID);
        return true;
    }

    /**
     * Validates email address format
     *
     * @param string $EmailAddress
     * @return boolean
     * @author Cem Hurturk
     */
    public static function ValidateEmailAddress($EmailAddress) {
        if (!(preg_match("/^[\#\+\._a-z0-9-]+([\'\&|-|\.\+][\#\+\._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/i", $EmailAddress))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the provided email address already exists in the subscriber list or not
     *
     * @param string $EmailAddress
     * @param integer $ListID
     * @param boolean $IgnoreUnsubscribed
     * @return mixed false if duplicate does not exist. Subscriber information if exists
     * @author Cem Hurturk
     */
    public static function CheckIfSubscriberExists($EmailAddress, $ListID, $IgnoreUnsubscribed = true, $IgnoreSubscriberID = 0) {
        $EmailAddress = mysql_real_escape_string($EmailAddress, Database::$Interface->MySQLConnection);
        $SQLQuery = "SELECT * FROM " . MYSQL_TABLE_PREFIX . "subscribers_" . $ListID . " WHERE EmailAddress='" . $EmailAddress . "'" . ($IgnoreUnsubscribed == true ? " AND (SubscriptionStatus = 'Opt-In Pending' OR SubscriptionStatus='Subscribed' OR SubscriptionStatus='Opt-Out Pending')" : "") . ($IgnoreSubscriberID > 0 ? " AND SubscriberID!='" . $IgnoreSubscriberID . "'" : "");
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) == 0) {
            return false;
        } else {
            $ArraySubscriber = mysql_fetch_assoc($ResultSet);
            return ($ArraySubscriber);
        }
    }

    /**
     * Checks if the provided value already exists in the subscriber database for the provided custom field ID
     *
     * @param string $ArrayCustomField
     * @param string $CustomFieldValue
     * @return boolean
     * @author Cem Hurturk
     */
    public static function CheckCustomFieldValueUniqueness($ArrayCustomField, $CustomFieldValue, $ExcludeSubscriberID = 0, $ListID = 0, $ExcludeSubscriberEmailAddress = '') {
        if ($ArrayCustomField['IsUnique'] == 'Yes') {
            if ($ArrayCustomField['IsGlobal'] == 'Yes') {
                $FieldName = 'ValueText';
                if ($ArrayCustomField['FieldType'] == 'Date field' || $ArrayCustomField['ValidationMethod'] == 'Date') {
                    $FieldName = 'ValueDate';
                } else if ($ArrayCustomField['FieldType'] == 'Time field' || $ArrayCustomField['ValidationMethod'] == 'Time') {
                    $FieldName = 'ValueTime';
                } else if ($ArrayCustomField['ValidationMethod'] == 'Numbers') {
                    $FieldName = 'ValueDouble';
                }

                $SQLQuery = 'SELECT COUNT(ValueID) AS TotalFound FROM `' . MYSQL_TABLE_PREFIX . 'custom_field_values` WHERE RelFieldID = "' . $ArrayCustomField['CustomFieldID'] . '" AND RelOwnerUserID = "' . $ArrayCustomField['RelOwnerUserID'] . '" AND `' . $FieldName . '` = "' . Database::$Interface->MakeValueSecureForQuery($CustomFieldValue) . '"' . (!empty($ExcludeSubscriberEmailAddress) ? ' AND EmailAddress != "' . Database::$Interface->MakeValueSecureForQuery($ExcludeSubscriberEmailAddress) . '"' : '');
                $Results = Database::$Interface->ExecuteQuery($SQLQuery);
                $ArrayRows = mysql_fetch_assoc($Results);

                if ($ArrayRows['TotalFound'] > 0) {
                    return false;
                } else {
                    return true;
                }
            } else {
                $ArrayCriteria = array('CustomField' . $ArrayCustomField['CustomFieldID'] => $CustomFieldValue);

                if ($ExcludeSubscriberID > 0) {
                    $ArrayCriteria[] = array('field' => 'SubscriberID', 'operator' => '!=', 'value' => $ExcludeSubscriberID);
                }

                $ArrayRows = self::RetrieveSubscribers(array('COUNT(*) AS TotalFound'), $ArrayCriteria, ($ArrayCustomField['RelListID'] == 0 ? $ListID : $ArrayCustomField['RelListID']), array('EmailAddress' => 'ASC'), 0, 0);

                if ($ArrayRows == false) {
                    return false;
                } else {
                    $ArrayRows = $ArrayRows[0];

                    if ($ArrayRows['TotalFound'] > 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } else {
            return true;
        }
    }

    /**
     * Unsubscribe the subscriber from the subscriber list
     *
     * @param string $ArrayList
     * @param string $UserID
     * @param string $CampaignID
     * @param string $EmailAddress
     * @param string $SubscriberID
     * @param string $UnsubscriptionIP
     * @param string $Channel
     * @return void
     * @author Cem Hurturk
     */
    public static function Unsubscribe_Enhnaced($ArrayList, $UserID, $CampaignID, $AutoResponderID, $EmailAddress, $SubscriberID, $UnsubscriptionIP, $Channel, $EmailID = 0) {
        // Load other modules - Start
        Core::LoadObject('statistics');
        Core::LoadObject('lists');
        Core::LoadObject('campaigns');
        Core::LoadObject('auto_responders');
        Core::LoadObject('transaction_emails');
        // Load other modules - End

        if (!is_array($ArrayList)) {
            $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => (int) $ArrayList), false, false, array());
            if ($ArrayList === false) {
                return;
            }
        }

        // Load subscriber information - Start {
        $ArraySubscriber = self::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
        // Load subscriber information - End }
        // Unsubscribe from this subscrber list only

        $ArrayFieldnValues = array(
            'SubscriptionStatus' => 'Unsubscribed',
            'UnsubscriptionDate' => date('Y-m-d'),
            'UnsubscriptionIP' => $UnsubscriptionIP,
        );
        Database::$Interface->UpdateRows($ArrayFieldnValues, array('EmailAddress' => $EmailAddress, 'SubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayList['ListID']);

        // Remove subscriber from auto responder queues - Start
        Core::LoadObject('transaction_emails');
        TransactionEmails::Delete($ArrayList['ListID'], 0, $SubscriberID, 0);
        // Remove subscriber from auto responder queues - End
        // Update activity statistics - Start
        $ArrayActivities = array(
            'TotalUnsubscriptions' => 1,
        );
        Statistics::UpdateListActivityStatistics($ArrayList['ListID'], $UserID, $ArrayActivities);
        // Update activity statistics - End
        // Update campaign statistics - Start
        $ArrayFieldAndValues = array(
            'TotalUnsubscriptions' => array(false, 'TotalUnsubscriptions + 1'),
        );

        if ($CampaignID > 0) {
            Campaigns::Update($ArrayFieldAndValues, array('CampaignID' => $CampaignID, 'RelOwnerUserID' => $UserID));
        } elseif ($AutoResponderID > 0) {
            AutoResponders::Update($ArrayFieldAndValues, array('AutoResponderID' => $AutoResponderID));
        }

        Statistics::RegisterUnsubscriptionTrack($UserID, $SubscriberID, $ArrayList['ListID'], ($CampaignID > 0 ? $CampaignID : 0), $Channel, ($AutoResponderID > 0 ? $AutoResponderID : 0), $EmailID);
        // Update campaign statistics - End
        // Trigger web services - Start
        Core::LoadObject('web_service_integration');
        $ArrayWebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $ArrayList['ListID'], 'RelOwnerUserID' => $UserID, 'EventType' => 'unsubscription'), array('ServiceURL' => 'ASC'));
        if (count($ArrayWebServices) > 0) {
            foreach ($ArrayWebServices as $Key => $ArrayEachWebService) {
                WebServiceIntegration::WebServiceConnection('Unsubscription', $ArraySubscriber, $ArrayEachWebService['ServiceURL']);
            }
        }
        // Trigger web services - End

        return;
    }

    /**
     * 
     * @author Eman
     * */
    public static function UpdateListActivityStatistics($ListID, $UserID, $ArrayActivities) {
        Core::LoadObject('statistics');
        Statistics::UpdateListActivityStatistics($ListID, $UserID, $ArrayActivities);
    }

    /**
     * Unsubscribe the subscriber from the subscriber list
     *
     * @param string $ArrayList
     * @param string $UserID
     * @param string $CampaignID
     * @param string $EmailAddress
     * @param string $SubscriberID
     * @param string $UnsubscriptionIP
     * @param string $Channel
     * @return void
     * @author Cem Hurturk
     */
    public static function Unsubscribe($ArrayList, $UserID, $CampaignID, $AutoResponderID, $EmailAddress, $SubscriberID, $UnsubscriptionIP, $Channel, $EmailID = 0) {
        // Load other modules - Start
        Core::LoadObject('statistics');
        Core::LoadObject('lists');
        Core::LoadObject('campaigns');
        Core::LoadObject('auto_responders');
        Core::LoadObject('transaction_emails');
        // Load other modules - End

        if (!is_array($ArrayList)) {
            $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => (int) $ArrayList), false, false, array());
            if ($ArrayList === false) {
                return;
            }
        }

        // Load subscriber information - Start {
        $ArraySubscriber = self::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
        // Load subscriber information - End }

        if ($ArrayList['OptOutScope'] == 'All lists') {
            // Unsubscribe from all user subscriber lists
            // Load other modules - Start
            Core::LoadObject('lists');
            // Load other modules - End
            // Loop each subscriber list of this user and unsubscribe the subscriber - Start
            $ArrayLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $UserID), array('ListID' => 'ASC'));

            foreach ($ArrayLists as $Index => $ArrayEachList) {
                $ArrayFieldnValues = array(
                    'SubscriptionStatus' => 'Unsubscribed',
                    'UnsubscriptionDate' => date('Y-m-d'),
                    'UnsubscriptionIP' => $UnsubscriptionIP,
                );
                $UpdatedSubscriptions = Database::$Interface->UpdateRows($ArrayFieldnValues, array('EmailAddress' => $EmailAddress), MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayEachList['ListID']);

                if ($UpdatedSubscriptions > 0) {
                    $SubscriberInformationForEachList = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $EmailAddress), $ArrayEachList['ListID']);

                    // Remove subscriber from auto responder queues - Start
                    Core::LoadObject('transaction_emails');
                    TransactionEmails::Delete($ArrayEachList['ListID'], 0, $SubscriberInformationForEachList['SubscriberID'], 0);
                    // Remove subscriber from auto responder queues - End
                    // Update activity statistics - Start
                    $ArrayActivities = array(
                        'TotalUnsubscriptions' => 1,
                    );
                    Statistics::UpdateListActivityStatistics($ArrayEachList['ListID'], $UserID, $ArrayActivities);
                    // Update activity statistics - End
                    // Update campaign statistics - Start
                    Statistics::RegisterUnsubscriptionTrack($UserID, $SubscriberInformationForEachList['SubscriberID'], $ArrayEachList['ListID'], ($CampaignID > 0 ? $CampaignID : 0), $Channel, ($AutoResponderID > 0 ? $AutoResponderID : 0), $EmailID);
                    // Update campaign statistics - End
                    // Trigger web services - Start
                    Core::LoadObject('web_service_integration');
                    $ArrayWebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $ArrayEachList['ListID'], 'RelOwnerUserID' => $UserID, 'EventType' => 'unsubscription'), array('ServiceURL' => 'ASC'));
                    if (count($ArrayWebServices) > 0) {
                        foreach ($ArrayWebServices as $Key => $ArrayEachWebService) {
                            WebServiceIntegration::WebServiceConnection('Unsubscription', $SubscriberInformationForEachList, $ArrayEachWebService['ServiceURL']);
                        }
                    }
                    // Trigger web services - End
                }
            }
            // Loop each subscriber list of this user and unsubscribe the subscriber - End
            // Update statistics - Start
            $ArrayFieldAndValues = array(
                'TotalUnsubscriptions' => array(false, 'TotalUnsubscriptions + 1'),
            );
            if ($CampaignID > 0) {
                Campaigns::Update($ArrayFieldAndValues, array('CampaignID' => $CampaignID, 'RelOwnerUserID' => $UserID));
            } elseif ($AutoResponderID > 0) {
                AutoResponders::Update($ArrayFieldAndValues, array('AutoResponderID' => $AutoResponderID));
            }
            // Update statistics - End
        } else {
            // Unsubscribe from this subscrber list only

            $ArrayFieldnValues = array(
                'SubscriptionStatus' => 'Unsubscribed',
                'UnsubscriptionDate' => date('Y-m-d'),
                'UnsubscriptionIP' => $UnsubscriptionIP,
            );
            Database::$Interface->UpdateRows($ArrayFieldnValues, array('EmailAddress' => $EmailAddress, 'SubscriberID' => $SubscriberID), MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayList['ListID']);

            // Remove subscriber from auto responder queues - Start
            Core::LoadObject('transaction_emails');
            TransactionEmails::Delete($ArrayList['ListID'], 0, $SubscriberID, 0);
            // Remove subscriber from auto responder queues - End
            // Update activity statistics - Start
            $ArrayActivities = array(
                'TotalUnsubscriptions' => 1,
            );
            Statistics::UpdateListActivityStatistics($ArrayList['ListID'], $UserID, $ArrayActivities);
            // Update activity statistics - End
            // Update campaign statistics - Start
            $ArrayFieldAndValues = array(
                'TotalUnsubscriptions' => array(false, 'TotalUnsubscriptions + 1'),
            );

            if ($CampaignID > 0) {
                Campaigns::Update($ArrayFieldAndValues, array('CampaignID' => $CampaignID, 'RelOwnerUserID' => $UserID));
            } elseif ($AutoResponderID > 0) {
                AutoResponders::Update($ArrayFieldAndValues, array('AutoResponderID' => $AutoResponderID));
            }

            Statistics::RegisterUnsubscriptionTrack($UserID, $SubscriberID, $ArrayList['ListID'], ($CampaignID > 0 ? $CampaignID : 0), $Channel, ($AutoResponderID > 0 ? $AutoResponderID : 0), $EmailID);
            // Update campaign statistics - End
            // Trigger web services - Start
            Core::LoadObject('web_service_integration');
            $ArrayWebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $ArrayList['ListID'], 'RelOwnerUserID' => $UserID, 'EventType' => 'unsubscription'), array('ServiceURL' => 'ASC'));
            if (count($ArrayWebServices) > 0) {
                foreach ($ArrayWebServices as $Key => $ArrayEachWebService) {
                    WebServiceIntegration::WebServiceConnection('Unsubscription', $ArraySubscriber, $ArrayEachWebService['ServiceURL']);
                }
            }
            // Trigger web services - End
        }

        return;
    }

    /**
     * Gets subscriber list fields and returns
     *
     * @param int $UserID ID number of the owner user id of the list
     * @param int $ListID ID number of the subscriber list
     * @param array $ArrayAliases Corresponding aliases array for fields
     * @param array $ArrayExcludeList Exluded field names
     * @param string $CustomFieldPrefix Custom field prefix
     * @return array
     * @author Cem Hurturk
     * */
    function GetSubscriberFields($UserID, $ListID, $ArrayAliases, $CustomFieldPrefix, $ArrayExcludeList = array()) {
        global $ArrayLanguageStrings;

        // Load other modules - Start
        Core::LoadObject('custom_fields');
        Core::LoadObject('lists');
        // Load other modules - End
        // Prepare list array - Start
        $ArrayList = array();
        if (is_numeric($ListID) == true) {
            $ArrayList[] = $ListID;
        } else {
            $ArrayList = $ListID;
        }
        // Prepare list array - End
        // Loop each list and retrieve fields - Start
        $ArraySubscriberFields = array();

        foreach ($ArrayList as $EachListID) {
            // Check if the list ID belongs to the provided user ID - Start
            $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $EachListID, 'RelOwnerUserID' => $UserID));

            if ($ArrayList == false) {
                continue;
            }
            // Check if the list ID belongs to the provided user ID - End
            // Retrieve fields - Start
            $SQLQuery = 'DESCRIBE `' . MYSQL_TABLE_PREFIX . 'subscribers_' . $EachListID . '`';
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            while ($EachField = mysql_fetch_assoc($ResultSet)) {
                if (in_array($EachField['Field'], $ArrayExcludeList) == true) {
                    continue;
                }

                if (substr($EachField['Field'], 0, 11) == 'CustomField') {
                    continue;
                } else {
                    $ArraySubscriberFields[$EachField['Field']] = ($ArrayAliases[$EachField['Field']] != '' ? $ArrayAliases[$EachField['Field']] : $EachField['Field']);
                }
            }

            // Add custom fields - Start {
            $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelListID' => $EachListID, 'RelOwnerUserID' => $UserID));
            foreach ($ArrayCustomFields as $EachField) {
                if (in_array('CustomField' . $EachField['CustomFieldID'], $ArrayExcludeList) == true) {
                    continue;
                }

                $ArraySubscriberFields['CustomField' . $EachField['CustomFieldID']] = $CustomFieldPrefix . ($EachField['FieldName'] != '' ? $EachField['FieldName'] : 'CustomField' . $EachField['CustomFieldID']) . ' (' . addslashes($ArrayList['Name']) . ')';
            }
            // Add custom fields - End }
            // Add global custom fields - Start {
            $ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('IsGlobal' => 'Yes', 'RelOwnerUserID' => $UserID));
            foreach ($ArrayGlobalCustomFields as $EachField) {
                if (in_array('CustomField' . $EachField['CustomFieldID'], $ArrayExcludeList) == true) {
                    continue;
                }

                $ArraySubscriberFields['CustomField' . $EachField['CustomFieldID']] = $CustomFieldPrefix . ($EachField['FieldName'] != '' ? $EachField['FieldName'] : 'CustomField' . $EachField['CustomFieldID']) . ' (' . $ArrayAliases['Global'] . ')';
            }
            // Add global custom fields - End }
            // Retrieve fields - End
        }
        // Loop each list and retrieve fields - End
        // Sort fields - Start
        asort($ArraySubscriberFields);
        // Sort fields - End

        return $ArraySubscriberFields;
    }

    /**
     * Returns the bounce history total of the subscriber
     *
     * @return array Returns an array with number of soft bounces [0], number of hard bounces [1] and number of not identified [2]
     * @author Cem Hurturk
     * */
    function SubscriberBounceHistory($SubscriberID, $ListID) {
        $SQLQuery = "SELECT BounceType, COUNT(*) AS Totals FROM " . MYSQL_TABLE_PREFIX . "stats_bounce WHERE RelListID='" . mysql_real_escape_string($ListID) . "' AND RelSubscriberID='" . mysql_real_escape_string($SubscriberID) . "' GROUP BY BounceType";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayTotals = array();
        while ($EachTotal = mysql_fetch_assoc($ResultSet)) {
            $ArrayTotals[$EachTotal['BounceType']] = $EachTotal['Totals'];
        }

        return array($ArrayTotals['Soft'], $ArrayTotals['Hard'], $ArrayTotals['Not Identified']);
    }

    /**
     * Selects a random subscriber from the provided list. If there is no subscriber, returns a dummy subscriber information
     *
     * @param int $ListID Subscriber list ID
     * @param bool $ReturnDummy Returns dummy if set to true
     * @return array Subscriber Information
     * @author Cem Hurturk
     * */
    function SelectRandomSubscriber($ListID, $ReturnDummy = true) {
        $ArraySubscriber = Subscribers::RetrieveSubscribers_Enhanced(array(
                    'ReturnFields' => array('*'),
                    'ReturnSQLQuery' => false,
                    'SubscriberListID' => $ListID,
                    'Pagination' => array('Offset' => 0, 'Rows' => 1),
                    'Criteria' => array(
                        array('Column' => 'SubscriptionStatus', 'Operator' => '=', 'Value' => 'Subscribed'),
                        array('Column' => 'BounceType', 'Operator' => '!=', 'Value' => 'Hard', 'Link' => 'AND')
                    )
        ));

        if (!$ArraySubscriber) {
            $SQLQuery = "DESCRIBE " . MYSQL_TABLE_PREFIX . "subscribers_" . $ListID;
            $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

            $ArraySubscriber = array();

            while ($ArrayTable = mysql_fetch_assoc($ResultSet)) {
                if ($ArrayTable['Field'] == 'SubscriberID') {
                    $ArraySubscriber[$ArrayTable['Field']] = 1;
                } elseif ($ArrayTable['Field'] == 'EmailAddress') {
                    $ArraySubscriber[$ArrayTable['Field']] = 'dummy@dummy.com';
                } elseif ($ArrayTable['Field'] == 'BounceType') {
                    $ArraySubscriber[$ArrayTable['Field']] = 'Not Bounced';
                } elseif ($ArrayTable['Field'] == 'SubscriptionStatus') {
                    $ArraySubscriber[$ArrayTable['Field']] = 'Subscribed';
                } elseif ($ArrayTable['Field'] == 'SubscriptionDate') {
                    $ArraySubscriber[$ArrayTable['Field']] = date('Y-m-d H:i:s', strtotime(date('Y-m-d -5 days H:i:s')));
                } elseif ($ArrayTable['Field'] == 'SubscriptionIP') {
                    $ArraySubscriber[$ArrayTable['Field']] = '127.0.0.1';
                } elseif ($ArrayTable['Field'] == 'UnsubscriptionDate') {
                    $ArraySubscriber[$ArrayTable['Field']] = '0000-00-00 00:00:00';
                } elseif ($ArrayTable['Field'] == 'UnsubscriptionIP') {
                    $ArraySubscriber[$ArrayTable['Field']] = '0.0.0.0';
                } elseif ($ArrayTable['Field'] == 'OptInDate') {
                    $ArraySubscriber[$ArrayTable['Field']] = '0.0.0.0';
                } else {
                    $ArraySubscriber[$ArrayTable['Field']] = 'Dummy';
                }
            }
        } else {
            $ArraySubscriber = $ArraySubscriber[0];
        }
        return $ArraySubscriber;
    }

    /**
     * Deletes import data of a list
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function DeleteImportData($ListID = 0) {
        if ($ListID > 0) {
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'subscriber_imports');
        }
    }

    /**
     * Process required processes to delete subscriber related data
     *
     * @param string $SubscriberID
     * @return void
     * @author Cem Hurturk
     */
    public static function DeleteSubscriberProcesses($UserID, $SubscriberID, $ListID = 0) {
        Core::LoadObject('queue');
        Core::LoadObject('statistics');
        Core::LoadObject('transaction_emails');

        Core::DeleteFBLReports($UserID, 0, 0, $SubscriberID);
        EmailQueue::Delete(0, 0, 0, $SubscriberID);
        Statistics::Delete(0, 0, $SubscriberID);
        TransactionEmails::Delete($ListID, 0, $SubscriberID);

        return;
    }

}

// END class Lists
?>
