<?php

/**
 * Oempro
 * (c)Copyright Octeth Ltd. All rights reserved.
 *
 * Source code can NOT be distributed/resold without the permission of Octeth Ltd.
 * For more details about licensing, please refer to http://www.octeth.com/
 *
 **/

/**
 * MySql database interface class
 *
 * @package Oempro
 * @author Mert Hurturk
 **/
class MySqlDatabaseInterface extends DatabaseInterface
{
/**
 * undocumented class variable
 *
 * @var string
 **/
public static $MasterFile;

/**
 * undocumented
 *
 * @author Cem Hurturk
 */
public static $Errors = array();

/**
 * undocumented class variable
 *
 * @var string
 **/
public static $MasterLineNumber;

/**
 * MySQL connection resource
 *
 * @var resource
 **/
var $MySQLConnection;

public static $IsDatabaseMissing = false;

/**
 * Constructor function
 *
 * @return void
 * @author Mert Hurturk
 **/
function __construct() {}

/**
 * Sets the source file and line number for error tracking
 *
 * @return void
 * @author Cem Hurturk
 **/
public static function SetSource($File, $Line)
	{
	self::$MasterFile		= $File;
	self::$MasterLineNumber = $Line;
	}

/**
 * Connects to database
 *
 * @param string MySql server
 * @param string The username
 * @param string The password
 * @param string The name of the database to be selected
 * @param string Table name prefix
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public function Connect($Host, $Username, $Password, $Database, $TableNamePrefix)
	{
	try
		{
		$this->MySQLConnection = mysql_connect($Host, $Username, $Password, true);
		if (!$this->MySQLConnection)
			{
			throw new Exception("MySql Error: Couldn't connect to database");
			}

		$this->SetServerAddress($Host);
		$this->SetUsername($Username);
		$this->SetPassword($Password);
		$this->SetTableNamePrefix($TableNamePrefix);
		$this->SetDatabaseName($Database);

		$SelectedDatabase = mysql_select_db($Database);
		if (!$SelectedDatabase)
			{
			self::$IsDatabaseMissing = true;
			throw new Exception("MySql Error: Couldn't select database");
			}


		// if (class_exists(Debug)) Debug::Success('MySql Success: Connected to database', __FILE__, __LINE__);

		return true;
		}
	catch (Exception $e)
		{
		$this->Errors[] = $e->getMessage()." - ".mysql_error();
		$this->ErrorMessage = $e->getMessage()." - ".mysql_error();

		// if (class_exists(Debug)) Debug::Error($this->ErrorMessage, __FILE__, $e->getLine());

		return false;
		}
	}

public function CreateDatabase()
{
	if (!$this->MySQLConnection)
		{
			$this->Errors[] = "Database doesn't exist. Failed to create it. Please create the database manually.";
			$this->ErrorMessage = "Database doesn't exist. Failed to create it. Please create the database manually.";
			return false;
		}

	$ResultSet	= @mysql_query("CREATE DATABASE  `".mysql_real_escape_string($this->Name)."` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;", $this->MySQLConnection);
	$MySqlError = mysql_error($this->MySQLConnection);

	if ($MySqlError != '')
		{
			$this->Errors[] = "Database doesn't exist. Failed to create it. Please create the database manually.";
			$this->ErrorMessage = "Database doesn't exist. Failed to create it. Please create the database manually.";
			return false;
		}

	return true;
}

public function IsDatabaseMissing()
{
	return self::$IsDatabaseMissing;
}

/**
 * Runs given query
 *
 * @param string Query to be executed on database
 *
 * @return mixed If error occures returns false else returns resource
 * @author Mert Hurturk
 **/
public function ExecuteQuery($Query, $SkipMySQLErrorExceptions = false)
	{
	$ArrayFetchedRows	= array();

	try
		{
		// $time = microtime(true);
		$ResultSet	= @mysql_query($Query, $this->MySQLConnection);
		// $time = microtime(true) - $time;
		// file_put_contents(DATA_PATH.'/oempro_sql.sql', $time . ' - ' . $Query . "\n", FILE_APPEND);
		$MySqlError = mysql_error($this->MySQLConnection);

		if ($MySqlError != '')
			{
			if ($SkipMySQLErrorExceptions == false)
				{
				Core::RecordToLogFile('MySQL', $Query."\n\n".$MySqlError, self::$MasterFile, self::$MasterLineNumber);

				self::$MasterFile		= '';
				self::$MasterLineNumber = '';

				throw new Exception("MySql Error: ".$MySqlError." (".$Query.")");
				}
			}

		self::$MasterFile		= '';
		self::$MasterLineNumber = '';

		// if (class_exists(Debug)) Debug::Success('MySql Query Success: ('.$Query.')', __FILE__, __LINE__);

		return $ResultSet;
		}
	catch (Exception $e)
		{
		// if (class_exists(Debug)) Debug::Error($e->getMessage(), __FILE__, $e->getLine());

		$this->Errors[] = $e->getMessage();

		 print($e->getMessage());
		 exit;

		return false;
		}
	}

/**
 * Inserts a new row into the selected table
 *
 * @param array Fields and values to be inserted
 * @param string Table name
 *
 * @return mixed If error occures returns false else returns resource
 * @author Mert Hurturk
 **/
public function InsertRow($ArrayFieldnValues, $TableName)
	{
	// Escape values - Start
	foreach ($ArrayFieldnValues as $Field=>$Value)
		{
		$ArrayFieldnValues[$Field] = $this->MakeValueSecureForQuery($Value);
		}
	// Escape values - End


	$FieldList = '`'.implode('`,`', array_keys($ArrayFieldnValues)).'`';
	$ValueList = '"'.implode('", "', array_values($ArrayFieldnValues)).'"';

	$SQLQuery	= 'INSERT INTO '.$TableName.' ('.$FieldList.') VALUES ('.$ValueList.')';

	$ResultSet	= $this->ExecuteQuery($SQLQuery);

	return $ResultSet;
	}

/**
 * Returns the ID number of the last insert
 *
 * @return integer
 * @author Cem Hurturk
 */
public function GetLastInsertID()
	{
	return mysql_insert_id($this->MySQLConnection);
	}

/**
 * Closes the MySQL connection
 *
 * @return void
 * @author Cem Hurturk
 */
public function CloseConnection()
	{
	mysql_close($this->MySQLConnection);
	return;
	}

/**
 * Deletes rows from selected table
 *
 * @param array Criterias to be matched while deleting rows
 * @param string Table name
 *
 * @return mixed If error occures returns false else returns resource
 * @author Mert Hurturk
 **/
public function DeleteRows($ArrayCriterias, $TableName, $WhereOperator='AND')
	{
	// Escape and prepare criteria SQL part - Start
	$SQLQueryCriteriaPart = array();
	foreach ($ArrayCriterias as $Field=>$Value)
		{
		if (is_array($Value))
			{
			$SQLQueryCriteriaPart[] = '`'.$Value['field'].'` '.$Value['operator'].(isset($Value['value']) ? ' "'.$this->MakeValueSecureForQuery($Value['value']).'"' : '');
			}
		else
			{
			if (strpos(strtolower($Field), 'md5') === false || strpos(strtolower($Field), 'concat') === false)
				{
				$SQLQueryCriteriaPart[] = '`'.$Field.'`="'.$this->MakeValueSecureForQuery($this->MakeValueSecureForQuery($Value)).'"';
				}
			else
				{
				$SQLQueryCriteriaPart[] = $Field.'="'.$this->MakeValueSecureForQuery($this->MakeValueSecureForQuery($Value)).'"';
				}
			}
		}
	$SQLQueryCriteriaPart = implode(' '.$WhereOperator.' ', $SQLQueryCriteriaPart);
	// Escape and prepare criteria SQL part - End

	$SQLQuery	= 'DELETE FROM '.$TableName.' WHERE '.$SQLQueryCriteriaPart;
	$ResultSet	= $this->ExecuteQuery($SQLQuery);

	return $ResultSet;
	}

/**
 * This is an enhanced version of DeleteRows method.
 *
 * What changed from original function?
 * =============
 *
 * - Supports 'AND' and 'OR' in criteria, and criteria can be grouped recursively
 * - Parameters passed as an array
 *
 * Parameters:
 * ==========
 *
 * Table			string		Table that data to be deleted from
 * Criteria			array		If set, WHERE statement is added to the query
 *								-- Parameters:
 *									Link			string	(AND | OR)		Link that will be used to link this group with previous one
 *									Column			string					Column name from campaigns table
 *									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
 *									Value			string					Value to be matched
 *									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
 *								-- Note: You can group criteria in arrays
 * Joins			array		If set, JOIN statements is added to the query
 *								-- Parameters:
 *									Type			string					Join type
 *									Table			string					Join table
 *									ON				string					On query of join
 *
 * @param array $ArrayParameters
 * @return void
 * @author Mert Hurturk
 */
public function DeleteRows_Enhanced($ArrayParameters)
	{
	// Parameter validation - Start {
	if (!isset($ArrayParameters['Table']) || $ArrayParameters['Table'] == '') throw new Exception('There must be a table name');
	if (!isset($ArrayParameters['Criteria']) || !is_array($ArrayParameters['Criteria']) || count($ArrayParameters['Criteria']) < 1) throw new Exception('There must be at least one criteria');
	// Parameter validation - End }

	// Prepare query - Start {
		// DELETE part
		$SQLQueryDeletePart = 'DELETE '.$ArrayParameters['Table'].'.* ';

		// FROM part
		$SQLQueryFromPart = 'FROM '.$ArrayParameters['Table'];

		// JOIN part
		$SQLJoinPart = '';
		if (count($ArrayParameters['Joins']) > 0)
			{
			$SQLJoinPart = self::GetJoinString($ArrayParameters['Joins']);
			}

		// WHERE part
		$SQLQueryWherePart = 'WHERE '.self::GetCriteriaString($ArrayParameters['Criteria']);

		// Final query
		$SQLQuery = $SQLQueryDeletePart.' '.$SQLQueryFromPart.' '.$SQLJoinPart.' '.$SQLQueryWherePart;
	// Prepare query - End }

	$ResultSet = $this->ExecuteQuery($SQLQuery);

	return $ResultSet;
	}

/**
 * Updates rows of a table
 *
 * @param array Fields and values to be updated
 * @param array Match criterias of the rows to be updated
 * @param string Table name
 *
 * @return mixed If error occures returns false else returns resource
 * @author Mert Hurturk
 **/
public function UpdateRows($ArrayFieldnValues, $ArrayCriterias, $TableName)
	{
	// Escape and preare update SQL part - Start
	$SQLQueryPart = array();
	foreach ($ArrayFieldnValues as $Field=>$Value)
		{
		if (strtolower(gettype($Value)) == 'array')
			{
			$SQLQueryPart[] = '`'.$Field.'`='.$this->MakeValueSecureForQuery($Value[1]);
			}
		else
			{
			$SQLQueryPart[] = '`'.$Field.'`="'.$this->MakeValueSecureForQuery($Value).'"';
			}
		}
	$SQLQueryPart = implode(', ', $SQLQueryPart);
	// Escape and preare update SQL part - End

	// Escape and prepare criteria SQL part - Start
	$SQLQueryCriteriaPart = array();
	if (count($ArrayCriterias) > 0)
		{
		foreach ($ArrayCriterias as $Field=>$Value)
			{
			if (strpos(strtolower($Field), 'md5') === false)
				{
				$SQLQueryCriteriaPart[] = '`'.$Field.'`="'.$this->MakeValueSecureForQuery($Value).'"';
				}
			else
				{
				$SQLQueryCriteriaPart[] = $Field.'="'.$this->MakeValueSecureForQuery($Value).'"';
				}
			}
		$SQLQueryCriteriaPart = implode(' AND ', $SQLQueryCriteriaPart);
 		}
	// Escape and prepare criteria SQL part - End
	$SQLQuery = 'UPDATE '.$TableName.' SET '.$SQLQueryPart.' WHERE '.$SQLQueryCriteriaPart;
	$ResultSet	= $this->ExecuteQuery($SQLQuery);

	return mysql_affected_rows($this->MySQLConnection);
	}

/**
 * Retrieves rows from a table
 *
 * @param array Fields to be retrieved from given tables
 * @param array Tables which contains the data to be retrieved
 * @param array Criterias to be matched
 *
 * @return array
 * @author Mert Hurturk
 **/
public function GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder = array(), $RecordCount=0, $StartFrom=0, $WhereOperator='AND', $OnlyReturnQuery=false, $OnlyReturnWhereQuery = false)
	{
	// Preare select SQL part - Start
	$SQLQuerySelectPart = array();
	foreach ($ArrayFields as $EachField)
		{
		$SQLQuerySelectPart[] = $EachField;
		}
	$SQLQuerySelectPart = implode(', ', $SQLQuerySelectPart);
	// Preare select SQL part - End

	// Preare from SQL part - Start
	$SQLQueryFromPart = array();
	foreach ($ArrayFromTables as $EachTable)
		{
		$SQLQueryFromPart[] = $EachTable;
		}
	$SQLQueryFromPart = '`'.implode('`, `', $SQLQueryFromPart).'`';
	// Preare from SQL part - End

	// Prepare order SQL part - Start
	$SQLQueryOrderPart = array();
	if (count($ArrayOrder) > 0)
		{
		foreach ($ArrayOrder as $Field=>$Value)
			{
			$SQLQueryOrderPart[] = '`'.$Field.'` '.$Value;
			}
		}
	$SQLQueryOrderPart = implode(', ', $SQLQueryOrderPart);
	// Prepare order SQL part - End

	// Escape and prepare criteria SQL part - Start
	$SQLQueryCriteriaPart = array();
	$SQLQueryExcludeFromORCriteriaPart = array();	// this is the conditions which will be exluded from OR and inlucded with AND. Ex: (field1=value1 OR field2=value2) AND excluded=value3

	if (strtolower(getType($ArrayCriterias)) == 'array' && count($ArrayCriterias) > 0)
		{
		foreach ($ArrayCriterias as $Field=>$Value)
			{
			if (is_array($Value))
				{
				if (isset($Value['exclude_or']) && $Value['exclude_or'] == true)
					{
					$SQLQueryExcludeFromORCriteriaPart[] = '`'.$Value['field'].'` '.$Value['operator'].(isset($Value['value']) ? ' "'.$this->MakeValueSecureForQuery($Value['value']).'"' : '');
					}
				elseif ((isset($Value['auto-quote']) == true) && ($Value['auto-quote'] == false))
					{
					$SQLQueryCriteriaPart[] = '`'.$Value['field'].'` '.$Value['operator'].(isset($Value['value']) ? ' '.$this->MakeValueSecureForQuery($Value['value']) : '');
					}
				else
					{
					$SQLQueryCriteriaPart[] = '`'.$Value['field'].'` '.$Value['operator'].(isset($Value['value']) ? ' "'.$this->MakeValueSecureForQuery($Value['value']).'"' : '');
					}
				}
			else
				{
				if (strpos(strtolower($Field), 'md5') === false && strpos(strtolower($Field), 'concat') === false)
					{
					$SQLQueryCriteriaPart[] = '`'.$Field.'`="'.$this->MakeValueSecureForQuery($Value).'"';
					}
				else
					{
					$SQLQueryCriteriaPart[] = $Field.'="'.$this->MakeValueSecureForQuery($Value).'"';
					}
				}
			}
		$SQLQueryCriteriaPart = implode(' '.$WhereOperator.' ', $SQLQueryCriteriaPart);
		$SQLQueryExcludeFromORCriteriaPart = implode(' '.$WhereOperator.' ', $SQLQueryExcludeFromORCriteriaPart);
 		}
	else
		{
		if (strtolower(getType($ArrayCriterias)) == 'string')
			{
			$SQLQueryCriteriaPart = $ArrayCriterias;
			}
		else
			{
			$SQLQueryCriteriaPart = '';
			}
		}
	// Escape and prepare criteria SQL part - End

	// If $OnlyReturnWhereQuery is set to true, return the WHERE condition - Start
	if ($OnlyReturnWhereQuery == true)
		{
		return $SQLQueryCriteriaPart;
		}
	// If $OnlyReturnWhereQuery is set to true, return the WHERE condition - End

	// Prepare limit SQL part - Start
	$SQLQueryLimitPart = '';
	if ($RecordCount != 0)
		{
		$SQLQueryLimitPart = $StartFrom.', '.$RecordCount;
		}
	// Prepare limit SQL part - End

	// $SQLQuery = 'SELECT '.$SQLQuerySelectPart.' FROM '.$SQLQueryFromPart.' '.(count($ArrayCriterias) > 0 ? 'WHERE '.($SQLQueryExcludeFromORCriteriaPart == '' ? $SQLQueryCriteriaPart : '('.$SQLQueryCriteriaPart.') AND '.$SQLQueryExcludeFromORCriteriaPart) : '').(count($ArrayOrder) > 0 ? ' ORDER BY '.$SQLQueryOrderPart : '').($SQLQueryLimitPart != '' ? ' LIMIT '.$SQLQueryLimitPart : '');
	$SQLQuery = 'SELECT '.$SQLQuerySelectPart.' FROM '.$SQLQueryFromPart.' '.(strtolower(getType($ArrayCriterias)) == 'array' && count($ArrayCriterias) > 0 ? 'WHERE '.($SQLQueryExcludeFromORCriteriaPart == '' ? $SQLQueryCriteriaPart : '('.$SQLQueryCriteriaPart.') AND '.$SQLQueryExcludeFromORCriteriaPart) : ($SQLQueryCriteriaPart == '' ? '' : 'WHERE '.$SQLQueryCriteriaPart)).(count($ArrayOrder) > 0 ? ' ORDER BY '.$SQLQueryOrderPart : '').($SQLQueryLimitPart != '' ? ' LIMIT '.$SQLQueryLimitPart : '');

	if ($OnlyReturnQuery)
		{
		return $SQLQuery;
		}

	$ResultSet	= $this->ExecuteQuery($SQLQuery);
	$ArrayRows = array();

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayRows[] = $EachRow;
		}

	return $ArrayRows;
	}

/**
 * This is an enhanced version of GetRows method.
 *
 * What changed from original function?
 * =============
 *
 * - Supports 'AND' and 'OR' in criteria, and criteria can be grouped recursively
 * - Parameters passed as an array
 *
 * Parameters:
 * ==========
 *
 * Fields			array		Column names to be returned
 * Tables			array		Tables that data to be selected from
 * ReturnSQLResult	boolean		If set to true, sql result returned
 * ReturnSQLQuery	boolean		If set to true, sql query returned as string
 * ReturnSQLWHEREQuery	boolean		If set to true, sql where query returned as string
 * RowOrder			array		If set, ORDER BY statement is added to the query
 *								-- Parameters:
 *									Column				string					Column name from segments table to be ordered
 *									Type				string	(ASC | DESC)	Order type
 * Pagination		array		If set, LIMIT statement is added to the query
 *								-- Parameters:
 *									Offset				integer					Offset of the first row
 *									Rows				integer					Maximum number of rows to return
 * Criteria			array		If set, WHERE statement is added to the query
 *								-- Parameters:
 *									Link			string	(AND | OR)		Link that will be used to link this group with previous one
 *									Column			string					Column name from campaigns table
 *									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
 *									Value			string					Value to be matched
 *									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
 *								-- Note: You can group criteria in arrays
 * Joins			array		If set, JOIN statements is added to the query
 *								-- Parameters:
 *									Type			string					Join type
 *									Table			string					Join table
 *									ON				string					On query of join
 *
 * @param array $ArrayParameters
 * @return void
 * @author Mert Hurturk
 */
public function GetRows_Enhanced($ArrayParameters)
	{
	// Default parameters for method - Start {
	$ArrayDefaultParameters = array(
		'Fields'		=>	array('*'),
		'ReturnSQLResult'=>	false,
		'ReturnSQLQuery'=>	false,
		'ReturnSQLWHEREQuery'=>	false,
		'RowOrder'		=>	array(),
		'Pagination'	=>	array(),
		'Criteria'		=>	array(),
		'Joins'			=>	array()
		);
	$ArrayParameters = array_merge($ArrayDefaultParameters, $ArrayParameters);
	// Default parameters for method - End }

	// Parameter validation - Start {
	if (!isset($ArrayParameters['Tables']) || !is_array($ArrayParameters['Tables']) || count($ArrayParameters['Tables']) < 1) throw new Exception('There must be at least one table declared');
	// Parameter validation - End }

	// Prepare query - Start {
		// SELECT part
		$SQLQuerySelectPart = 'SELECT '.implode(', ', $ArrayParameters['Fields']);

		// FROM part
		$SQLQueryFromPart = 'FROM '.implode('`, `', $ArrayParameters['Tables']);

		// JOIN part
		$SQLJoinPart = '';
		if (count($ArrayParameters['Joins']) > 0)
			{
			$SQLJoinPart = self::GetJoinString($ArrayParameters['Joins']);
			}

		// WHERE part
		$SQLQueryWherePart = '';
		if (count($ArrayParameters['Criteria']) > 0)
			{
			$SQLQueryWherePart = str_replace(array('( AND','( OR'), array('(','('), self::GetCriteriaString($ArrayParameters['Criteria']));
			if ($ArrayParameters['ReturnSQLWHEREQuery'])
				{
				return $SQLQueryWherePart;
				}
			$SQLQueryWherePart = 'WHERE '.$SQLQueryWherePart;
			}

		// ORDER part
		$SQLQueryOrderPart = '';
		if (isset($ArrayParameters['RowOrder']['Column']) && isset($ArrayParameters['RowOrder']['Type']))
			{
			$SQLQueryOrderPart = 'ORDER BY '.$ArrayParameters['RowOrder']['Column'].' '.$ArrayParameters['RowOrder']['Type'];
			unset($TMPArray);
			}

		// LIMIT part
		$SQLQueryLimitPart = '';
		if (isset($ArrayParameters['Pagination']['Offset']) && isset($ArrayParameters['Pagination']['Rows']))
			{
			$SQLQueryLimitPart = 'LIMIT '.$ArrayParameters['Pagination']['Offset'].', '.$ArrayParameters['Pagination']['Rows'];
			}

		// Final query
		$SQLQuery = $SQLQuerySelectPart.' '.$SQLQueryFromPart.' '.$SQLJoinPart.' '.$SQLQueryWherePart.' '.$SQLQueryOrderPart.' '.$SQLQueryLimitPart;
		$SQLQuery = Plugins::HookListener('Filter', 'MysqlQueryGetRowsFilter', array($SQLQuery));
		$SQLQuery = $SQLQuery[0];
	// Prepare query - End }

	// If requested, return only query string
	if ($ArrayParameters['ReturnSQLQuery'])
		{
		return $SQLQuery;
		}

	$ResultSet = $this->ExecuteQuery($SQLQuery);
	if ($ArrayParameters['ReturnSQLResult'])
		{
		return $ResultSet;
		}
	$ArrayRows = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayRows[] = $EachRow;
		}

	return $ArrayRows;
	}

/**
 * Generates the MySQL database backup
 *
 * @param boolean if true, backups structure
 * @param boolean if true, backups data
 *
 * @return void
 * @author Cem Hurturk
 **/
public function Backup($BackupStructure = true, $BackupData = true)
	{
	$SQLBackupFileName  		= $this->Name."_".date("Ymd_His").".sql";

	header("Content-type: application/octetstream");
	header("Content-Disposition: attachment; filename=\"".$SQLBackupFileName."\"");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Pragma: public");

	$ResultSet = mysql_list_tables($this->Name);
		$TotalTables = mysql_num_rows($ResultSet);

	print('#-----------------------------------------'."\n");
	print('# Database Backup                         '."\n");
	print('#-----------------------------------------'."\n");
	print('# Backup Date: '.date('Y-m-d H:i:s')."\n");
	print("\n");

	for ($TMPCounter = 0; $TMPCounter < $TotalTables; $TMPCounter++)
		{
		$TableName = mysql_tablename($ResultSet, $TMPCounter);

		print('#-------------------------------------=[ '.$TableName."\n");
		print("\n");
		print("\n");

		if ($BackupStructure == true)
			{
			print($this->GetTableStructure($TableName));
			}

		print("\n");
		print("\n");
		print("\n");

		if ($BackupData == true)
			{
			$ArrayFields 		= array('*');
			$ArrayFromTables	= array($TableName);
			$ArrayCriterias		= array();
			$ArrayData			= $this->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

			foreach ($ArrayData as $Key=>$ArrayEachRow)
				{
				$FieldList = '`'.implode('`,`', array_keys($ArrayEachRow)).'`';

				print("INSERT INTO `".$TableName."` (".$FieldList.") VALUES (");

				$TMPCounter2 = 1;
				$ArrayValues = array_values($ArrayEachRow);
				$Total = count($ArrayValues);
				foreach ($ArrayValues as $EachValue)
					{
					if ($TMPCounter2 != $Total)
						{
						$Comma = ', ';
						}
					else
						{
						$Comma = '';
						}
					if (isset($EachValue) == false)
						{
						print(' NULL'.$Comma);
						}
					elseif ($EachValue != '')
						{
						print("'".addslashes($EachValue)."'".$Comma);
						}
					else
						{
						print(" ''".$Comma);
						}
					$TMPCounter2++;
					}
				print("); \n");
				}
			}

		print("\n");
		print("\n");
		print("\n");
		}

	exit;

	return true;
	}

/**
 * Generates the MySQL database table structure in SQL language
 *
 * @param string Table name
 *
 * @return string
 * @author Cem Hurturk
 **/
function GetTableStructure($TableName)
	{
	$ArraySQLQueryLines		= array();
	$ArraySQLQueryLines[]	= "DROP TABLE IF EXISTS `".$TableName."`;"."\n";
	$ArraySQLQueryLines[]	= "CREATE TABLE `".$TableName."` ("."\n";

	$SQLQuery = "SHOW FIELDS FROM ".$TableName;
	$ResultSet = $this->ExecuteQuery($SQLQuery);

	$ArrayFields = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayFields[] = $EachRow;
		}
	$TotalFields = count($ArrayFields);

	$TMPCounter = 0;

	foreach ($ArrayFields as $Key=>$ArrayEachField)
		{
		$TMPCounter++;

		$ArraySQLQueryLines[]	= "`".$ArrayEachField['Field']."` ".$ArrayEachField['Type'];

        if(isset($ArrayEachField["Default"]) && (!empty($ArrayEachField["Default"]) || $ArrayEachField["Default"] == "0"))
			{
			$ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " DEFAULT '".$ArrayEachField["Default"]."'";
			}

        if($ArrayEachField["Null"] != "YES")
			{
            $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " NOT NULL";
			}

        if($ArrayEachField["Extra"] != "")
			{
            $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " ".$ArrayEachField["Extra"];
			}

		if ($TotalFields > $TMPCounter)
			{
	        $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= ",\n";
			}
		else
			{
	        $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= "";
			}
		}

	$SQLQuery = "SHOW KEYS FROM ".$TableName;
	$ResultSet = $this->ExecuteQuery($SQLQuery);

	$ArrayKeys = array();
	while ($EachRow = mysql_fetch_assoc($ArrayKeys))
		{
		$ArrayKeys[] = $EachRow;
		}
	$TotalKeys = count($ArrayKeys);

	$TMPCounter = 0;

	$ArrayIndexList = array();

	foreach ($ArrayKeys as $Key=>$ArrayEachKey)
		{
		$EachIndexName = $ArrayEachKey['Key_name'];

		if(($EachIndexName != "PRIMARY") && ($EachIndexName['Non_unique'] == 0))
			{
            $EachIndexName="UNIQUE|".$EachIndexName;
			}

         if(isset($ArrayIndexList[$EachIndexName]) == false)
		 	{
			$ArrayIndexList[$EachIndexName] = array();
			}

         $ArrayIndexList[$EachIndexName][] = "`".$ArrayEachKey['Column_name']."`";
		}

	foreach ($ArrayIndexList as $Key=>$ArrayColumns)
		{
		$ArraySQLQueryLines[] = ","."\n";

		if ($Key == 'PRIMARY')
			{
			$ArraySQLQueryLines[] = "PRIMARY KEY (".implode(',', $ArrayColumns).")";
			}
		elseif (substr($Key, 0, 6) == 'UNIQUE')
			{
			$ArraySQLQueryLines[] = "UNIQUE ".substr($Key, 7)." (".implode(',', $ArrayColumns).")";
			}
		else
			{
			$ArraySQLQueryLines[] = "KEY ".$Key."` (".implode(',', $ArrayColumns).")";
			}
		}

	$ArraySQLQueryLines[] = "\n);";

	return implode("", $ArraySQLQueryLines);
	}


/**
 * Make values secure for SQL query
 *
 * @param mixed Value to be secured for query
 *
 * @return mixed
 * @author Mert Hurturk
 **/
public function MakeValueSecureForQuery($Value)
	{
	return mysql_real_escape_string($Value, $this->MySQLConnection);
	}

/**
 * Returns the latest MySQL error
 *
 * @return string | void
 * @author Cem Hurturk
 */
public function ReturnError()
	{
	return mysql_error($this->MySQLConnection);
	}

/**
 * Returns all generated errors
 *
 * @return void
 * @author Cem Hurturk
 */
public function ReturnErrors()
	{
	return $this->Errors;
	}

/**
 * Generates a criteria string to be used in WHERE statements.
 *
 *	ArrayCriteria	array		-- Parameters:
 *									Link			string	(AND | OR)		Link that will be used to link this group with previous one
 *									Column			string					Column name from campaigns table
 *									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
 *									Value			string					Value to be matched
 *									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
 *								-- Note: You can group criteria in arrays
 *
 * @param string $ArrayCriteria
 * @return void
 * @author Mert Hurturk
 */
public function GetCriteriaString($ArrayCriteria, $TableAliases = array())
	{
	$ArrayTableAliasSearch = array_keys($TableAliases);
	$ArrayTableAliasReplace = array_values($TableAliases);

	$Query = '';

	if (!isset($ArrayCriteria['Column']))
		{
		$Query .= (isset($ArrayCriteria['Link']) ? $ArrayCriteria['Link'].' ' : '').'( ';
		foreach ($ArrayCriteria as $EachCriteria)
			{
			if (is_array($EachCriteria))
				{
				$Query .= $this->GetCriteriaString($EachCriteria, $TableAliases);
				}
			}
		$Query .= ') ';

		return $Query;
		}
	else
		{
		$ArrayCriteria = Plugins::HookListener('Filter', 'MysqlCriteriaFilter', array($ArrayCriteria));
		$ArrayCriteria = $ArrayCriteria[0];
		return $Query = (isset($ArrayCriteria['Link']) ? $ArrayCriteria['Link'].' ' : '').str_replace($ArrayTableAliasSearch, $ArrayTableAliasReplace, $ArrayCriteria['Column']).' '.$ArrayCriteria['Operator']. ' '.(isset($ArrayCriteria['ValueWOQuote']) != '' ? $ArrayCriteria['ValueWOQuote'] : '"'.$this->MakeValueSecureForQuery($ArrayCriteria['Value']).'"').' ';
		}
	}

/**
 * Generates a criteria string to be used in JOIN statements.
 *
 * Joins			array		If set, JOIN statements is added to the query
 *								-- Parameters:
 *									Type			string					Join type
 *									Table			string					Join table
 *									ON				string					On query of join
 *
 * @param string $ArrayJoins
 * @return void
 * @author Mert Hurturk
 */
public function GetJoinString($ArrayJoins)
	{
	$Query = '';

	if (count($ArrayJoins) < 1) return $Query;

	if (! is_array($ArrayJoins[0]))
		{
		$Query = $ArrayJoins['Type'].' '.$ArrayJoins['Table'].' ON '.$ArrayJoins['ON'].' ';
		}
	else
		{
		foreach ($ArrayJoins as $EachJoin)
			{
			$Query .= self::GetJoinString($EachJoin).' ';
			}
		}

	return $Query;
	}

public function CheckDBIntegrity()
	{
	$DBIntegrityList = include APP_PATH.'/includes/libraries/dbit.php';

	// Generate the structure of the current database - Start {
	$ArrayDBStructure = array();

	$ResultSet = $this->ExecuteQuery("SHOW TABLES;");
	while ($EachTable = mysql_fetch_row($ResultSet))
		{
		$EachTable = $EachTable[0];
		if ((strpos($EachTable, 'oempro_') !== false) && (strpos($EachTable, 'oempro_subscribers_') === false) && (strpos($EachTable, 'oct') === false))
			{
			$ResultSet2 = $this->ExecuteQuery("DESCRIBE ".$EachTable);
			while ($EachField = mysql_fetch_assoc($ResultSet2))
				{
				$ArrayDBStructure[$EachTable][$EachField['Field']] = $EachField;
				}
			}
		}
	// Generate the structure of the current database - End }

	// Compare db structures (original to installation) - Start {
	$DBDiff = array();

	foreach ($DBIntegrityList as $Table=>$Fields)
		{
		if (isset($ArrayDBStructure[$Table]) == false)
			{
			$DBDiff[$Table] = 'FIRST_TABLE_DOESNT_EXIST';
			}
		foreach ($Fields as $EachField=>$Parameters)
			{
			if (isset($ArrayDBStructure[$Table][$EachField]) == false)
				{
				$DBDiff[$Table][$EachField] = 'FIRST_FIELD_DOESNT_EXIST';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Type'] != $DBIntegrityList[$Table][$EachField]['Type'])
				{
				$DBDiff[$Table][$EachField] = 'FIRST_FIELD_TYPE_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Key'] != $DBIntegrityList[$Table][$EachField]['Key'])
				{
				$DBDiff[$Table][$EachField] = 'FIRST_FIELD_KEY_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Default'] != $DBIntegrityList[$Table][$EachField]['Default'])
				{
				$DBDiff[$Table][$EachField] = 'FIRST_FIELD_DEFAULT_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Extra'] != $DBIntegrityList[$Table][$EachField]['Extra'])
				{
				$DBDiff[$Table][$EachField] = 'FIRST_FIELD_EXTRA_INCORRECT';
				}
			}
		}
	// Compare db structures (original to installation) - End }

	// Compare db structures (original to installation) - Start {
	foreach ($ArrayDBStructure as $Table=>$Fields)
		{
		if (isset($DBIntegrityList[$Table]) == false)
			{
			$DBDiff[$Table] = 'SECOND_TABLE_DOESNT_EXIST';
			}
		foreach ($Fields as $EachField=>$Parameters)
			{
			if (isset($DBIntegrityList[$Table][$EachField]) == false)
				{
				$DBDiff[$Table][$EachField] = 'SECOND_FIELD_DOESNT_EXIST';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Type'] != $DBIntegrityList[$Table][$EachField]['Type'])
				{
				$DBDiff[$Table][$EachField] = 'SECOND_FIELD_TYPE_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Key'] != $DBIntegrityList[$Table][$EachField]['Key'])
				{
				$DBDiff[$Table][$EachField] = 'SECOND_FIELD_KEY_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Default'] != $DBIntegrityList[$Table][$EachField]['Default'])
				{
				$DBDiff[$Table][$EachField] = 'SECOND_FIELD_DEFAULT_INCORRECT';
				}
			elseif ($ArrayDBStructure[$Table][$EachField]['Extra'] != $DBIntegrityList[$Table][$EachField]['Extra'])
				{
				$DBDiff[$Table][$EachField] = 'SECOND_FIELD_EXTRA_INCORRECT';
				}
			}
		}
	// Compare db structures (original to installation) - End }

	return $DBDiff;
	}

public function DatabaseVersion()
	{
	return mysql_get_server_info();
	}

} // END class MySqlDatabaseInterface extends DatabaseInterface
