<?php

class Spark {

    private static $__accessKey = 'Authorization';
    private static $__apiKey = '8cf1a2eb3340eff94820f3e41c3c2a07fe7c7aa2';
    private static $__baseURL = 'https://api.sparkpost.com/api/v1';
    private static $__endpoint;
    private static $__comamnd;

    public static function deliverabilityByCampaign($from, $to = null, $campaigns = "", $metrics = "") {

        self::$__endpoint = 'metrics';
        self::$__comamnd = "deliverability/campaign";

        $url = self::$__baseURL . "/" . self::$__endpoint . "/" . self::$__comamnd;
        $url .= "?from=" . $from;

        if ($to != null && isset($to)) {
            $url .= "&to=" . $to;
        }
        if ($campaigns != "" && !empty($campaigns)) {
            $url .= "&campaigns=" . $campaigns;
        }
        if ($metrics != "" && !empty($metrics)) {
            $url .= "&metrics=" . $metrics;
        } else {
            $url .= "&metrics=" . self::getAllMetrics();
        }
        $response = (new SparkRequest())->execute($url, array(), 'GET', True, self::$__accessKey, self::$__apiKey, 30, false);

        if (!$response[0]) {
            return array(false, $response[1]); //error in curl
        }
        $result = json_decode($response[1]);
        if (isset($result->errors)) {
            return array(false, $result->errors); //error in request
        }
        $campaign_metrics = array();
        foreach ($result->results as $campaign_metric) {
            $campaign_metrics[$campaign_metric->campaign_id] = $campaign_metric;
        }

        return array(true,$campaign_metrics);
    }

    private static function getAllMetrics() {
        return "count_injected,count_bounce,count_rejected,count_delivered,count_delivered_first,count_delivered_subsequent,total_delivery_time_first,total_delivery_time_subsequent,total_msg_volume,count_policy_rejection,count_generation_rejection,count_generation_failed,count_inband_bounce,count_outofband_bounce,count_soft_bounce,count_hard_bounce,count_block_bounce,count_admin_bounce,count_undetermined_bounce,count_delayed,count_delayed_first,count_rendered,count_unique_rendered,count_unique_confirmed_opened,count_clicked,count_unique_clicked,count_targeted,count_sent,count_accepted,count_spam_complaint";
    }

}

final class SparkRequest {

    public function execute($URL, $ArrayPostParameters, $HTTPRequestType = 'POST', $HTTPAuth = false, $HTTPUsername = '', $HTTPPassword = '', $ConnectTimeOutSeconds = 60, $ReturnHeaders = false) {
        $PostParameters = implode('&', $ArrayPostParameters);

        $CurlHandler = curl_init();
        curl_setopt($CurlHandler, CURLOPT_URL, $URL);

        if ($HTTPRequestType == 'GET') {
            curl_setopt($CurlHandler, CURLOPT_HTTPGET, true);
        } elseif ($HTTPRequestType == 'PUT') {
            curl_setopt($CurlHandler, CURLOPT_PUT, true);
        } elseif ($HTTPRequestType == 'DELETE') {
            curl_setopt($CurlHandler, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($CurlHandler, CURLOPT_POST, true);
            curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $PostParameters);
        } else {
            curl_setopt($CurlHandler, CURLOPT_POST, true);
            curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $PostParameters);
        }

        curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CurlHandler, CURLOPT_CONNECTTIMEOUT, $ConnectTimeOutSeconds);
        curl_setopt($CurlHandler, CURLOPT_TIMEOUT, $ConnectTimeOutSeconds);
        curl_setopt($CurlHandler, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
        curl_setopt($CurlHandler, CURLOPT_SSL_VERIFYPEER, false);

        // The option doesn't work with safe mode or when open_basedir is set.
        if ((ini_get('safe_mode') != false) && (ini_get('open_basedir') != false)) {
            curl_setopt($CurlHandler, CURLOPT_FOLLOWLOCATION, true);
        }

        if ($ReturnHeaders == true) {
            curl_setopt($CurlHandler, CURLOPT_HEADER, true);
        } else {
            curl_setopt($CurlHandler, CURLOPT_HEADER, false);
        }

        if ($HTTPAuth == true) {
            $header = array();
            $header[] = $HTTPUsername . ':' . $HTTPPassword;

            curl_setopt($CurlHandler, CURLOPT_HTTPHEADER, $header);
        }

        $RemoteContent = curl_exec($CurlHandler);

        if (curl_error($CurlHandler) != '') {
            return array(false, curl_error($CurlHandler));
        }

        curl_close($CurlHandler);

        return array(true, $RemoteContent);
    }

}
