<?php
/**
 * Oempro
 * (c)Copyright Octeth Ltd. All rights reserved.
 * 
 * Source code can NOT be distributed/resold without the permission of Octeth Ltd.
 * For more details about licensing, please refer to http://www.octeth.com/
 *
 **/

/**
 * Database class is factory class that decides which database interface to use
 *
 * @package Oempro
 * @author Mert Hurturk
 **/
class Database
{
/**
 * MySQL constant
 *
 * @var integer
 **/
const MYSQL = 1;

/**
 * Intance of database interface object
 *
 * @var string
 **/
public static $Interface;

/**
 * Constructor function
 *
 * @return void
 * @author Mert Hurturk
 **/
private function __construct() {}

/**
 * Sets database interface object
 *
 * @return DatabaseInterface
 * @author Mert Hurturk
 **/
public static function SetDatabaseInterface($DatabaseType)
	{
	switch ($DatabaseType)
		{
		case (self::MYSQL):
			self::$Interface = new MySqlDatabaseInterface;
		default:
			self::$Interface = new MySqlDatabaseInterface;
		}
	}
} // END class Database
?>