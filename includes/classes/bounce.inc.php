<?php
/**
 * Bounce Management Class
 *
 * @package default
 * @author Cem Hurturk
 **/

class BounceEngine extends Core
{
/**
 * Detects the bounce type and returns it. Returns false if it can not be detected
 *
 * @return string
 * @author Cem Hurturk
 **/
function DetectBounceType($MessageBody)
	{
	global $ArrayBounceDetectionPatterns;
	foreach ($ArrayBounceDetectionPatterns as $EachPattern=>$EachBounceType)
		{
		if (stripos($MessageBody, $EachPattern) === false)
			{
			// Do nothing
			}
		else
			{
			return $EachBounceType;
			}
		}

	return false;
	}


} // END class BounceEngine
?>