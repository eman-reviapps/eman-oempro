<?php

/**
 * Oempro4
 * (c)Copyright Octeth Ltd. All rights reserved.
 * 
 * Source code can NOT be distributed/resold without the permission of Octeth Ltd.
 * For more details about licensing, please refer to http://www.octeth.com/
 *
 * */

/**
 * Plugin class
 *
 * @package Oempro
 * @author Mert Hurturk
 * */
class Plugins {

    /**
     * Plugins root path
     *
     * @var string
     * */
    private static $PlugInRootPath = '';

    /**
     * Stores the registered plug-in hooks
     *
     * @var string
     * */
    private static $ArrayRegisteredHooks = array();

    /**
     * Constructor function
     *
     * @return void
     * @author Mert Hurturk
     * */
    private function __construct() {
        
    }

    /**
     * Sets plugins directory root path
     *
     * @param string Path of the plugins directory's root path
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function SetPlugInRootPath($Path) {
        self::$PlugInRootPath = $Path;
    }

    /**
     * Loads given plugins
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function LoadAll($ApplyUserPermissions = true, $ArrayUserPermissions = '') {
        // System wide plug-ins - Start {
        $SystemPluginDir = APP_PATH . '/includes/system_plugins/';
        if ($DirHandler = opendir($SystemPluginDir)) {
            $TMPCounter = 0;
            while (($EachFile = readdir($DirHandler)) !== false) {
                if (($EachFile != '.') && ($EachFile != '..') && (preg_match('/^plugin_/i', $EachFile) > 0) && (filetype($SystemPluginDir . $EachFile) == 'file')) {
                    include_once($SystemPluginDir . $EachFile);
                    $ClassName = str_replace('.php', '', $EachFile);
                    call_user_func_array(array($ClassName, 'load_' . $ClassName), array());
                }
            }
            closedir($DirHandler);
        }
        // System wide plug-ins - End }

        foreach (explode(",", ENABLED_PLUGINS) as $EachPlugin) {
            if (($ApplyUserPermissions == true) && ((in_array('Plugin.' . $EachPlugin, $ArrayUserPermissions) == false) && (in_array('*', $ArrayUserPermissions) == false))) {
                continue;
            }
            self::Load($EachPlugin);
        }

        return true;
    }

    /**
     * Includes activated plug-ins
     *
     * @param string plugin name
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function Load($PluginDirectoryName, $CallLoadFunction = true) {
        if (file_exists(self::$PlugInRootPath . $PluginDirectoryName . '/' . $PluginDirectoryName . '.php') == true) {
            include_once(self::$PlugInRootPath . $PluginDirectoryName . '/' . $PluginDirectoryName . '.php');
            if ($CallLoadFunction) {
                self::CallLoad($PluginDirectoryName);
            }
        }
    }

    /**
     * Calls plug-in load function
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function CallLoad($PluginCode) {
        if (method_exists($PluginCode, 'load_' . $PluginCode)) {
            call_user_func_array(array($PluginCode, 'load_' . $PluginCode), array());
        }
    }

    /**
     * Registers the hook of plugin
     *
     * @param string Hook type to be registered [Filter, Action, Menu]
     * @param string Hook name to be registered
     * @param string Class name to be called
     * @param string Function name to be called
     * @param integer Priority of the plugin
     * @param integer Accepted number of arguments of the plugin function to be called
     *
     * @return void
     * @author Cem Hurturk
     * */
    public static function RegisterHook($HookType, $HookName, $ClassName, $FunctionName, $Priority = 10, $AcceptedArguments = 0) {
        self::$ArrayRegisteredHooks[$HookType][$HookName][] = array(
            'ExecuteClassName' => $ClassName,
            'ExecuteFunctionName' => $FunctionName,
            'ExecutePriority' => $Priority,
            'AcceptedArguments' => $AcceptedArguments,
        );
    }

    /**
     * Registers a function to be called when a plugin is enabled
     *
     * @param string Plugin directory name
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RegisterEnableHook($PluginDirectory) {
        self::RegisterHook('Action', 'System.Plugin.Enable', $PluginDirectory, 'enable_' . $PluginDirectory, 10, 0);
    }

    /**
     * Registers a function to be called when a plugin is disabled
     *
     * @param string Plugin directory name
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RegisterDisableHook($PluginDirectory) {
        self::RegisterHook('Action', 'System.Plugin.Disable', $PluginDirectory, 'disable_' . $PluginDirectory, 10, 0);
    }

    /**
     * Registers a function to be called when a menu is being populated in the user interface
     *
     * @param string Plugin directory name
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RegisterMenuHook($PluginDirectory, $FunctionName) {
        self::RegisterHook('Action', 'System.Menu.Add', $PluginDirectory, $FunctionName, 10, 2);
    }

    /**
     * Registers a function to be called as an API method
     *
     * @param string API Command name
     * @param string Plugin directory name
     * @param array Authentication method. (admin, user)
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RegisterAPIHook($Command, $PluginDirectory, $For = array()) {
        $Command = strtolower($Command);
        plugin_api_extend::$Extensions[$Command] = array(
            'PluginCode' => $PluginDirectory,
            'For' => $For,
            'Method' => 'api_' . str_replace('.', '_', $Command)
        );
    }

    /**
     * Checks if there is any registered plug-in hooks for the provided hook
     *
     * @param string Hook type to be registered [Filter, Action, Menu]
     * @param string Hook name to be registered
     * @param array Parameters that will be passed to plugin function
     * @param string Strict function name to be called
     *
     * @return array
     * @author Cem Hurturk
     * */
    public static function HookListener($HookType, $HookName, $ArrayParameters, $FunctionName = '') {
        $ActionHookResultArray = array();
        // Loop each registered action of the hook - Start
        if (isset(self::$ArrayRegisteredHooks[$HookType]) && isset(self::$ArrayRegisteredHooks[$HookType][$HookName])) {
            foreach (self::$ArrayRegisteredHooks[$HookType][$HookName] as $Index => $EachRegisteredHook) {
                // Check if the registered action is a valid function - Start
                if (is_callable(array($EachRegisteredHook['ExecuteClassName'], $EachRegisteredHook['ExecuteFunctionName'])) == true) {
                    if (($FunctionName != '') && ($FunctionName != $EachRegisteredHook['ExecuteFunctionName'])) {
                        continue;
                    }

                    // Splice the array to suit action argument totals - Start
                    $TMPArrayParameters = is_array($ArrayParameters) ? $ArrayParameters : array();
                    array_splice($TMPArrayParameters, ($EachRegisteredHook['AcceptedArguments'] == 0 ? count($ArrayParameters) : $EachRegisteredHook['AcceptedArguments']));
                    // Splice the array to suit action argument totals - End
                    // Execute the action - Start
                    $PluginResult = call_user_func_array(array($EachRegisteredHook['ExecuteClassName'], $EachRegisteredHook['ExecuteFunctionName']), $TMPArrayParameters);
                    if ($HookType == 'Action') {
                        $ActionHookResultArray[] = $PluginResult;
                    } else {
                        $ArrayParameters = $PluginResult;
                    }
                    unset($TMPArrayParameters);
                    // Execute the action - End
                }
                // Check if the registered action is a valid function - End
            }
        }
        // Loop each registered action of the hook - End

        if ($HookType == 'Action') {
            return $ActionHookResultArray;
        } else {
            return $ArrayParameters;
        }
    }

    /**
     * Checks if there is any registered plug-in hook for enabling plugin
     *
     * @param string Plugin directory name
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function HookListenerPlugInEnable($PluginDirectory) {
        self::HookListener('Action', 'Enable', array(), 'enable_' . $PluginDirectory);
    }

    /**
     * Checks if there is any registered plug-in hook for disabling plugin
     *
     * @param string Plugin directory name
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function HookListenerPlugInDisable($PluginDirectory) {
        self::HookListener('Action', 'Disable', array(), 'disable_' . $PluginDirectory);
    }

    /**
     * Checks plug-in directory and returns all plugins
     *
     * @param boolean $ReturnEnabledOnly 
     * @return array
     * @author Cem Hurturk
     */
    public static function GetPlugInList($ReturnEnabledOnly = false) {
        $ArrayPlugIns = array();
        $ArrayEnabledPlugIns = explode(",", ENABLED_PLUGINS);

        if ($DirHandler = opendir(self::$PlugInRootPath)) {
            $TMPCounter = 0;
            while (($EachFile = readdir($DirHandler)) !== false) {
                if (($EachFile != '.') && ($EachFile != '..') && (filetype(self::$PlugInRootPath . $EachFile) == 'dir') && (file_exists(self::$PlugInRootPath . $EachFile . '/' . $EachFile . '.php') == true)) {
                    $FileContents = file_get_contents(self::$PlugInRootPath . $EachFile . '/' . $EachFile . '.php');

                    // Search for plugin information - Start
                    if (preg_match('/Name:\s(.*)\n([\w\W]*Description:\s(.*))/i', $FileContents, $ArrayMatches) == true) {
                        if (($ArrayMatches[1] != '') && ($ArrayMatches[3] != '')) {
                            if (($ReturnEnabledOnly == true) && (in_array($EachFile, $ArrayEnabledPlugIns) == false)) {
                                continue;
                            }

                            preg_match('/[\/\/\*] Minimum Oempro Version: (.*)\n/i', $FileContents, $ArrayMatches2);

                            $ArrayPlugIns[$TMPCounter]['Code'] = $EachFile;
                            $ArrayPlugIns[$TMPCounter]['Name'] = $ArrayMatches[1];
                            $ArrayPlugIns[$TMPCounter]['Description'] = $ArrayMatches[3];
                            $ArrayPlugIns[$TMPCounter]['MinOemproVersion'] = ($ArrayMatches2[1] == '' ? '4.0.0' : $ArrayMatches2[1]);
                            $ArrayPlugIns[$TMPCounter]['Path'] = self::$PlugInRootPath . $EachFile;
                            $ArrayPlugIns[$TMPCounter]['Status'] = (in_array($EachFile, $ArrayEnabledPlugIns) == true ? true : false);

                            $TMPCounter++;
                        }
                    }
                    // Search for plugin information - End
                }
            }
            closedir($DirHandler);
        }

        return $ArrayPlugIns;
    }

    /**
     * Checks the status of plug-in
     *
     * @param string $PlugInCode 
     * @return void
     * @author Cem Hurturk
     */
    public static function IsPlugInEnabled($PlugInCode) {
        $ArrayEnabledPlugIns = explode(",", ENABLED_PLUGINS);

        if (in_array($PlugInCode, $ArrayEnabledPlugIns) == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Detects template tags and executes them
     *
     * @return void
     * @author Cem Hurturk
     * Example: _{PlugInTag:classname:functionname}_
     * */
    public static function DetectTemplateTagsInTemplate($TemplateContent) {
        // Analyze the template content and check if there is any defined template plug-in tag - Start 

        $TMPPattern = "/\_\{PlugInTag\:(.*)\}\_/iU";
        if (preg_match_all($TMPPattern, $TemplateContent, $ArrayMatches, PREG_SET_ORDER)) {
            // Plug-in templates found. Loop and execute each one of them - Start
            foreach ($ArrayMatches as $EachMatch) {
                $ArrayCallInfo = explode(':', $EachMatch[1]);

                // Check if the plug-in template tag is a valid function or not - Start

                if (is_callable(array($ArrayCallInfo[0], $ArrayCallInfo[1])) == true) {
                    // Function is valid. Execute and get the content - Start
                    $Replace = call_user_func(array($ArrayCallInfo[0], $ArrayCallInfo[1]));
                    // Function is valid. Execute and get the content - End
                } else {
                    // Function is not valid. Replace the tag with an emtpy string - Start
                    $Replace = '';
                    // Function is not valid. Replace the tag with an emtpy string - End
                }
                // Check if the plug-in template tag is a valid function or not - End
                // Replace the plug-in template tag with the function return - Start 
                $TemplateContent = preg_replace('/_{PlugInTag:' . $EachMatch[1] . '}_/iU', $Replace, $TemplateContent);
                // Replace the plug-in template tag with the function return - End
            }
            // Plug-in templates found. Loop and execute each one of them - End
        }
        // Analyze the template content and checks if there is any defined template plug-in tag - End

        return $TemplateContent;
    }

    /**
     * Runs template tags of plug-ins
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function RunPlugInTemplateTag($ClassName, $MethodName, $Return = false) {
        if (is_callable(array($ClassName, $MethodName)) == true) {
            // Function is valid. Execute and get the content - Start
            $Response = call_user_func(array($ClassName, $MethodName));
            // Function is valid. Execute and get the content - End
        } else {
            // Function is not valid. Replace the tag with an emtpy string - Start
            $Response = '';
            // Function is not valid. Replace the tag with an emtpy string - End
        }

        if ($Return == false) {
            print($Response);
        } else {
            return $Response;
        }
    }

    /**
     * Loads the plug-in language file
     *
     * @param string $PlugInCode 
     * @param string $Language 
     * @return array
     * @author Cem Hurturk
     */
    public static function LoadPlugInLanguage($PlugInCode, $Language) {
        $LanguageFilePath = PLUGIN_PATH . $PlugInCode . '/languages/' . strtolower($Language) . '/' . strtolower($Language) . '.inc.php';
        if (file_exists($LanguageFilePath) == false) {
            die('<strong>Plug-in language file not found!</strong><br>' . $LanguageFilePath);
        }
        include_once($LanguageFilePath);

        return $ArrayPlugInLanguageStrings;
    }

    /**
     * Disables a specific plug-in
     *
     * @param string $PlugInCode 
     * @return void
     * @author Cem Hurturk
     */
    public static function DisablePlugIn($PlugInCode) {
        // Check if the registered action is a valid function - Start
        if (is_callable(array($PlugInCode, 'disable_' . $PlugInCode)) == true) {
            // Execute the action - Start
            call_user_func_array(array($PlugInCode, 'disable_' . $PlugInCode), array());
            // Execute the action - End
        }
        // Check if the registered action is a valid function - End

        return;
    }

    /**
     * Enable a specific plug-in
     *
     * @param string $PlugInCode 
     * @return void
     * @author Cem Hurturk
     */
    public static function EnablePlugIn($PlugInCode) {
        // Check if the registered action is a valid function - Start
        if (is_callable(array($PlugInCode, 'enable_' . $PlugInCode)) == true) {
            // Execute the action - Start
            call_user_func_array(array($PlugInCode, 'enable_' . $PlugInCode), array());
            // Execute the action - End
        }
        // Check if the registered action is a valid function - End

        return;
    }

}

// END static class Plugins
?>