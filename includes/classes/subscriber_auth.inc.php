<?php
/**
 * SubscriberAuth class
 *
 * This class holds all subscriber authorization related functions
 * @package Oempro
 * @author Octeth
 **/
class SubscriberAuth extends Core
{
/**
 * Logs in the subscriber
 *
 * @param string $MD5SubscriberID 
 * @param string $MD5EmailAddress 
 * @param string $MD5ListID 
 * @return void
 * @author Cem Hurturk
 */

function Login($MD5SubscriberID, $MD5EmailAddress, $ListID)
	{
	$_SESSION[SESSION_NAME]['SubscriberLogin'] = array();
	$_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']	= $ListID;
	$_SESSION[SESSION_NAME]['SubscriberLogin']['Salt']		= $MD5SubscriberID.$MD5EmailAddress;
	
	return;
	}

/**
 * Logs the subscriber out
 *
 * @return void
 * @author Cem Hurturk
 */
function Logout()
	{
	unset($_SESSION[SESSION_NAME]['SubscriberLogin']);
	return;
	}

/**
 * Checks if subscriber is logged in or not.
 *
 * @param string $LoggedInRedirectURL If set, logged in subscriber will be redirected to this URL
 * @param string $LoggedOutRedirectURL If set, not logged in subscriber will be redirected to this URL
 * @return booelan
 * @author Cem Hurturk
 */
function IsLoggedIn($LoggedInRedirectURL, $LoggedOutRedirectURL)
	{
	if (isset($_SESSION[SESSION_NAME]['SubscriberLogin']) == true)
		{
		// Session exists. Check if the login information is valid.
		$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."subscribers_".$_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']." WHERE CONCAT(MD5(SubscriberID), MD5(EmailAddress)) = '".mysql_real_escape_string($_SESSION[SESSION_NAME]['SubscriberLogin']['Salt'])."'";
		$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

		if (mysql_num_rows($ResultSet) == 1)
			{
			// Login information is correct
			if ($LoggedInRedirectURL != false)
				{
				header('Location: '.$LoggedInRedirectURL);
				exit;
				}
			else
				{
				return true;
				}
			}
		else
			{
			// Login information is incorrect
			if ($LoggedOutRedirectURL != false)
				{
				header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
				exit;
				}
			else
				{
				return false;
				}
			}
		}
	else
		{
		// Session does not exist. subscriber not logged in.
		if ($LoggedOutRedirectURL != false)
			{
			header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
			exit;
			}
		else
			{
			return false;
			}
		}
	}

/**
 * Validates the login information and returns logged in subscriber info
 *
 * @param string $ArrayParameter 
 * @return array|boolean
 * @author Cem Hurturk
 */

function ValidateAndPerformLogin($ArrayParameter)
	{
	if (self::IsLoggedIn(false, false))
		{
		// Logged in
		$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('CONCAT(MD5(`SubscriberID`), MD5(`EmailAddress`))' => $_SESSION[SESSION_NAME]['SubscriberLogin']['Salt']), $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']);

		if ($ArraySubscriber == false)
			{
			return false;
			}

		}
	else
		{
		// Not logged in
		if (($ArrayParameter['ListID'] == '') || ($ArrayParameter['SubscriberID'] == '') || ($ArrayParameter['EmailAddress'] == '') || ($ArrayParameter['ValidateEmailAddress'] == ''))
			{
			return false;
			}
		else
			{
			$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('CONCAT(MD5(`SubscriberID`), MD5(`EmailAddress`))' => $ArrayParameter['SubscriberID'].$ArrayParameter['EmailAddress'], 'EmailAddress' => $ArrayParameter['ValidateEmailAddress']), $ArrayParameter['ListID']);

			if ($ArraySubscriber == false)
				{
				return false;
				}

			SubscriberAuth::Login($ArrayParameter['SubscriberID'], $ArrayParameter['EmailAddress'], $ArrayParameter['ListID']);
			}
		}
	
	return $ArraySubscriber;
	}

} // END class Auth
?>