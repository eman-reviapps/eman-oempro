<?php

// Required modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('statistics');
// Required modules - End

/**
 * Lists class
 *
 * This class holds all subscriber list related functions
 * @package Oempro
 * @author Octeth
 * */
class Lists extends Core {

    public static $SubscriberListCache = array();

    /**
     * Required fields for subscriber lists
     *
     * @static array
     * */
    public static $ArrayRequiredFields = array('Name', 'RelOwnerUserID');

    /**
     * Default values for fields of a subscriber list
     *
     * @static array
     * */
    public static $ArrayDefaultValuesForFields = array('OptInMode' => 'Single', 'OptOutScope' => 'This list', 'OptOutAddToSuppressionList' => 'No', 'SendServiceIntegrationFailedNotification' => 'false', 'SubscriptionConfirmationPendingPageURL' => '', 'SubscriptionConfirmedPageURL' => '', 'UnsubscriptionConfirmedPageURL' => '', 'HideInSubscriberArea' => 'false', 'ReqByEmailSearchToAddress' => '', 'ReqByEmailSubscriptionCommand' => '', 'ReqByEmailUnsubscriptionCommand' => '', 'SyncStatus' => 'Disabled', 'SyncMySQLPort' => '3306');

    /**
     * Returns subscribed lists' information of a given email address
     *
     * @return array
     * @author Mert Hurturk
     * */
    public static function RetrieveSubscribedListsOfAnEmailAddress($user_id, $email_address) {
        // Retrieve all lists of a user - Start {
        $all_lists_of_user = Lists::RetrieveLists(array('ListID', 'Name'), array('RelOwnerUserID' => $user_id), array('Name' => 'ASC'), array(), 0, 0, FALSE);
        // Retrieve all lists of a user - End }
        // Loop through all user's lists and check if given email address is subscribed to that list - Start {
        $subscribed_lists = array();
        foreach ($all_lists_of_user as &$each_list) {
            $subscribers = Database::$Interface->GetRows_Enhanced(array(
                'Fields' => array('SubscriberID'),
                'Tables' => array('oempro_subscribers_' . $each_list['ListID']),
                'Criteria' => array(
                    array('Column' => 'EmailAddress', 'Operator' => '=', 'Value' => $email_address)
                )
            ));
            if (count($subscribers) > 0) {
                $each_list['SubscriberID'] = $subscribers[0]['SubscriberID'];
                $subscribed_lists[] = $each_list;
            }
        }
        // Loop through all user's lists and check if given email address is subscribed to that list - End }

        return $subscribed_lists;
    }

    /*
     * Eman
     */

    public static function RetrieveListCustom($ArrayReturnFields, $ArrayCriterias, $RetrieveStatistics = false, $RetrieveSubscriberCount = true, $ArrayStatistics = array('bounce', 'unsubscribes', 'open', 'click', 'forward','spam')) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        if ($ArrayReturnFields[0] == '*' && array_key_exists('ListID', $ArrayCriterias) && $RetrieveStatistics == false && $RetrieveSubscriberCount == false) {
            if (isset(self::$SubscriberListCache[$ArrayCriterias['ListID']])) {
                return self::$SubscriberListCache[$ArrayCriterias['ListID']];
            }
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscriber_lists');
        $ArrayCriterias = $ArrayCriterias;
        $ArrayOrder = array();
        $ArraySubscriberList = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        if (count($ArraySubscriberList) < 1) {
            return false;
        } // if there are no subscriber lists, return false 

        $ArraySubscriberList = $ArraySubscriberList[0];

        self::$SubscriberListCache[$ArrayCriterias['ListID']] = $ArraySubscriberList;

//        if ($RetrieveSubscriberCount) {
            $ArraySubscriberList['SubscriberCount'] = Subscribers::GetActiveTotal($ArraySubscriberList['ListID']);
//             print_r($ArraySubscriberList['SubscriberCount']);
//        exit();
//        }
       
        $ArraySubscriberList['TotalSent'] = Statistics::RetrieveListTotalSent($ArraySubscriberList['ListID']);
        
        if ($RetrieveStatistics) {

            if (in_array('bounce', $ArrayStatistics))
                $ArraySubscriberList['BounceStatistics'] = Statistics::RetrieveListTotalBounces($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'],$ArraySubscriberList['SubscriberCount']);

            if (in_array('open', $ArrayStatistics))
                $ArraySubscriberList['OpenStatistics'] = Statistics::RetrieveListTotalOpens($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'],$ArraySubscriberList['SubscriberCount']);

            if (in_array('click', $ArrayStatistics))
                $ArraySubscriberList['ClickStatistics'] = Statistics::RetrieveListTotalClicks($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'],$ArraySubscriberList['SubscriberCount']);

            if (in_array('forward', $ArrayStatistics))
                $ArraySubscriberList['ForwardStatistics'] = Statistics::RetrieveListTotalForward($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $ArraySubscriberList['SubscriberCount']);

              if (in_array('unsubscribes', $ArrayStatistics))
                $ArraySubscriberList['UnsubscribesStatistics'] = Statistics::RetrieveListTotalUnSubscribes($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID']);

              
            if (in_array('spam', $ArrayStatistics))
                $ArraySubscriberList['SpamStatistics'] = Statistics::RetrieveSPAMComplaintCountOfList($ArraySubscriberList['ListID']);
           
        }

        return $ArraySubscriberList;
    }

    /**
     * Returns a single subscriber list matching given criterias
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving subscriber list.
     * @return boolean|array
     * @author Mert Hurturk
     * */
    public static function RetrieveList($ArrayReturnFields, $ArrayCriterias, $RetrieveStatistics = false, $RetrieveSubscriberCount = true, $ArrayStatistics = array('bounce', 'domain', 'activity', 'open', 'click', 'forward', 'view', 'openperf', 'clickperf', 'oopenperf', 'oclickperf', 'oaopenperf', 'oaclickperf', 'spam')) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        if ($ArrayReturnFields[0] == '*' && array_key_exists('ListID', $ArrayCriterias) && $RetrieveStatistics == false && $RetrieveSubscriberCount == false) {
            if (isset(self::$SubscriberListCache[$ArrayCriterias['ListID']])) {
                return self::$SubscriberListCache[$ArrayCriterias['ListID']];
            }
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscriber_lists');
        $ArrayCriterias = $ArrayCriterias;
        $ArrayOrder = array();
        $ArraySubscriberList = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        if (count($ArraySubscriberList) < 1) {
            return false;
        } // if there are no subscriber lists, return false 

        $ArraySubscriberList = $ArraySubscriberList[0];

        self::$SubscriberListCache[$ArrayCriterias['ListID']] = $ArraySubscriberList;

        if ($RetrieveSubscriberCount) {
            $ArraySubscriberList['SubscriberCount'] = Subscribers::GetActiveTotal($ArraySubscriberList['ListID']);
        }

        if ($RetrieveStatistics) {
            $FromDate = date('Y-m-d', strtotime(date('Y-m-d') . ' -30 days'));
            $ToDate = date('Y-m-d');
            if (in_array('domain', $ArrayStatistics))
                $ArraySubscriberList['EmailDomainStatistics'] = Statistics::RetrieveListEmailDomainStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['SubscriberCount'], 4);

            if (in_array('bounce', $ArrayStatistics))
                $ArraySubscriberList['BounceStatistics'] = Statistics::RetrieveListBounceStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['SubscriberCount']);

            if (in_array('activity', $ArrayStatistics))
                $ArraySubscriberList['ActivityStatistics'] = Statistics::RetrieveListActivityStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $FromDate, $ToDate);

            if (in_array('open', $ArrayStatistics))
                $ArraySubscriberList['OpenStatistics'] = Statistics::RetrieveListOpenStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $FromDate, $ToDate);

            if (in_array('click', $ArrayStatistics))
                $ArraySubscriberList['ClickStatistics'] = Statistics::RetrieveListClickStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $FromDate, $ToDate);

            if (in_array('forward', $ArrayStatistics))
                $ArraySubscriberList['ForwardStatistics'] = Statistics::RetrieveListForwardStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $FromDate, $ToDate);

            if (in_array('view', $ArrayStatistics))
                $ArraySubscriberList['BrowserViewStatistics'] = Statistics::RetrieveListBrowserViewStatistics($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID'], $FromDate, $ToDate);

            if (in_array('openperf', $ArrayStatistics))
                $ArraySubscriberList['OpenPerformance'] = Statistics::RetrieveOpenPerformanceDaysOfList($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID']);

            if (in_array('clickperf', $ArrayStatistics))
                $ArraySubscriberList['ClickPerformance'] = Statistics::RetrieveClickPerformanceDaysOfList($ArraySubscriberList['ListID'], $ArraySubscriberList['RelOwnerUserID']);

            if (in_array('oopenperf', $ArrayStatistics))
                $ArraySubscriberList['OverallOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfList($ArraySubscriberList['ListID']);

            if (in_array('oclickperf', $ArrayStatistics))
                $ArraySubscriberList['OverallClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfList($ArraySubscriberList['ListID']);

            if (in_array('oaopenperf', $ArrayStatistics))
                $ArraySubscriberList['OverallAccountClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfAccount($ArraySubscriberList['RelOwnerUserID']);

            if (in_array('oaclickperf', $ArrayStatistics))
                $ArraySubscriberList['OverallAccountOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfAccount($ArraySubscriberList['RelOwnerUserID'], $ArraySubscriberList['ListID']);

            if (in_array('spam', $ArrayStatistics))
                $ArraySubscriberList['TotalSpamComplaints'] = Statistics::RetrieveSPAMComplaintCountOfList($ArraySubscriberList['ListID'],$ArraySubscriberList['SubscriberCount']);
        }

        return $ArraySubscriberList;
    }

    /**
     * Reteurns all subscriber lists matching given criterias
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving subscriber lists.
     * @param array $ArrayOrder Fields to order.
     * @param array $ArrayStatisticsOptions [Statistics] => array(OpenStatistics=>true), [Days] => 30
     * @return boolean|array
     * @author Mert Hurturk
     * */
    public static function RetrieveLists($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('Name' => 'ASC'), $ArrayStatisticsOptions = array(), $RecordCount = 0, $RecordFrom = 0, $ReturnSubscriberCount = TRUE) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscriber_lists');
        $ArrayCriterias = $ArrayCriterias;
        $ArraySubscriberLists = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, $RecordCount, $RecordFrom);

        if (count($ArraySubscriberLists) < 1) {
            return false;
        }  // if there are no subscriber lists, return false

        foreach ($ArraySubscriberLists as &$EachList) {
            if ($ReturnSubscriberCount) {
                $EachList['SubscriberCount'] = Subscribers::GetActiveTotal($EachList['ListID']);
            }
            if (isset($ArrayStatisticsOptions['Days'])) {
                $FromDate = date('Y-m-d', strtotime(date('Y-m-d') . ' -' . $ArrayStatisticsOptions['Days'] . ' days'));
            } else {
                $FromDate = date('Y-m-d', strtotime(date('Y-m-d') . ' -30 days'));
            }
            $ToDate = date('Y-m-d');
            if (isset($ArrayStatisticsOptions['Statistics']['EmailDomainStatistics']) && $ArrayStatisticsOptions['Statistics']['EmailDomainStatistics'] == true) {
                $EachList['EmailDomainStatistics'] = Statistics::RetrieveListEmailDomainStatistics($EachList['ListID'], $EachList['SubscriberCount'], 4);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['BounceStatistics']) && $ArrayStatisticsOptions['Statistics']['BounceStatistics'] == true) {
                $EachList['BounceStatistics'] = Statistics::RetrieveListBounceStatistics($EachList['ListID'], $EachList['SubscriberCount']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['ActivityStatistics']) && $ArrayStatisticsOptions['Statistics']['ActivityStatistics'] == true) {
                $EachList['ActivityStatistics'] = Statistics::RetrieveListActivityStatistics($EachList['ListID'], $EachList['RelOwnerUserID'], $FromDate, $ToDate);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OpenStatistics']) && $ArrayStatisticsOptions['Statistics']['OpenStatistics'] == true) {
                $EachList['OpenStatistics'] = Statistics::RetrieveListOpenStatistics($EachList['ListID'], $EachList['RelOwnerUserID'], $FromDate, $ToDate);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['ClickStatistics']) && $ArrayStatisticsOptions['Statistics']['ClickStatistics'] == true) {
                $EachList['ClickStatistics'] = Statistics::RetrieveListClickStatistics($EachList['ListID'], $EachList['RelOwnerUserID'], $FromDate, $ToDate);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['ForwardStatistics']) && $ArrayStatisticsOptions['Statistics']['ForwardStatistics'] == true) {
                $EachList['ForwardStatistics'] = Statistics::RetrieveListForwardStatistics($EachList['ListID'], $EachList['RelOwnerUserID'], $FromDate, $ToDate);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['BrowserViewStatistics']) && $ArrayStatisticsOptions['Statistics']['BrowserViewStatistics'] == true) {
                $EachList['BrowserViewStatistics'] = Statistics::RetrieveListBrowserViewStatistics($EachList['ListID'], $EachList['RelOwnerUserID'], $FromDate, $ToDate);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OpenPerformance']) && $ArrayStatisticsOptions['Statistics']['OpenPerformance'] == true) {
                $EachList['OpenPerformance'] = Statistics::RetrieveOpenPerformanceDaysOfList($EachList['ListID'], $EachList['RelOwnerUserID']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['ClickPerformance']) && $ArrayStatisticsOptions['Statistics']['ClickPerformance'] == true) {
                $EachList['ClickPerformance'] = Statistics::RetrieveClickPerformanceDaysOfList($EachList['ListID'], $EachList['RelOwnerUserID']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OverallOpenPerformance']) && $ArrayStatisticsOptions['Statistics']['OverallOpenPerformance'] == true) {
                $EachList['OverallOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfList($EachList['ListID']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OverallClickPerformance']) && $ArrayStatisticsOptions['Statistics']['OverallClickPerformance'] == true) {
                $EachList['OverallClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfList($EachList['ListID']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OverallAccountClickPerformance']) && $ArrayStatisticsOptions['Statistics']['OverallAccountClickPerformance'] == true) {
                $EachList['OverallAccountClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfAccount($EachList['RelOwnerUserID']);
            }
            if (isset($ArrayStatisticsOptions['Statistics']['OverallAccountOpenPerformance']) && $ArrayStatisticsOptions['Statistics']['OverallAccountOpenPerformance'] == true) {
                $EachList['OverallAccountOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfAccount($EachList['RelOwnerUserID'], $EachList['ListID']);
            }
        }

        return $ArraySubscriberLists;
    }

    /**
     * Returns total number of subscriber lists of a user
     *
     * @param integer $OwnerUserID Owner user's id
     * @return integer|boolean Total number of subscriber lists of given user
     * @author Mert Hurturk
     * */
    public static function GetTotal($OwnerUserID) {
        // Check for required fields of this function - Start
        if (!isset($OwnerUserID) || $OwnerUserID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayFields = array('COUNT(*) AS TotalListCount');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'subscriber_lists');
        $ArrayCriterias = array('RelOwnerUserID' => $OwnerUserID);
        $ArraySubscriberLists = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

        return $ArraySubscriberLists[0]['TotalListCount'];
    }

    /**
     * Creates a new subscriber list with given values
     *
     * @param array $ArrayFieldAndValues Values of new subscriber list
     * @return boolean|integer ID of new subscriber list
     * @author Mert Hurturk
     * */
    public static function Create($ArrayFieldAndValues) {
        // Check required values - Start
        foreach (self::$ArrayRequiredFields as $EachField) {
            if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                print $ArrayFieldAndValues[$EachField];
                return false;
            }
        }
        // Check required values - End
        // Check if any value is missing. if yes, give default values - Start
        foreach (self::$ArrayDefaultValuesForFields as $EachField => $EachValue) {
            if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                $ArrayFieldAndValues[$EachField] = $EachValue;
            }
        }
        // Check if any value is missing. if yes, give default values - End
        // Set the value of 'CreatedOn' field - Start
        $ArrayFieldAndValues['CreatedOn'] = date('Y-m-d H:i:s');
        // Set the value of 'CreatedOn' field - End
        // Create a new record in subscriber_lists table - Start
        Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX . 'subscriber_lists');
        $NewSubscriberListID = Database::$Interface->GetLastInsertID();
        // Create a new record in subscriber_lists table - End
        // Create a new subscribers table for storing this list's subscribers - Start
        $SQLQuery = "CREATE TABLE  `" . MYSQL_TABLE_PREFIX . "subscribers_" . $NewSubscriberListID . "` (
				 `SubscriberID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				 `EmailAddress` VARCHAR( 250 ) NOT NULL ,
				 `BounceType` ENUM(  'Not Bounced',  'Hard',  'Soft' ) NOT NULL DEFAULT  'Not Bounced',
				 `SubscriptionStatus` ENUM(  'Opt-In Pending',  'Subscribed',  'Opt-Out Pending',  'Unsubscribed' ) NOT NULL ,
				 `SubscriptionDate` DATE NOT NULL ,
				 `SubscriptionIP` VARCHAR( 250 ) NOT NULL ,
				 `UnsubscriptionDate` DATE NOT NULL ,
				 `UnsubscriptionIP` VARCHAR( 250 ) NOT NULL ,
				 `OptInDate` DATE NOT NULL ,
				INDEX (  `EmailAddress` )
				) CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = MYISAM;";
        Database::$Interface->ExecuteQuery($SQLQuery);
        // Create a new subscribers table for storing this list's subscribers - End
        // Retrieve new list information - Start {
        $ArrayNewSubscriberList = self::RetrieveList(array('*'), array('ListID' => $NewSubscriberListID), false);
        // Retrieve new list information - End }

        return $NewSubscriberListID;
    }

    /**
     * Updates subscriber list information
     *
     * @param array $ArrayFieldAndValues Values of new subscriber list information
     * @param array $ArrayCriterias Criterias to be matched while updating subscriber list
     * @return boolean
     * @author Mert Hurturk
     * */
    public static function Update($ArrayFieldAndValues, $ArrayCriterias) {
        // Check for required fields of this function - Start
        // $ArrayCriterias check
        if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) {
            return false;
        }
        // Check for required fields of this function - End
        // Check required values - Start
        foreach (self::$ArrayRequiredFields as $EachField) {
            if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                return false;
            }
        }
        // Check required values - End

        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'subscriber_lists');
        return;
    }

    /**
     * Deletes subscriber lists
     *
     * @param integer $OwnerUserID Owner user id of subscriber lists to be deleted
     * @param array $ArraySubscriberListIDs Subscriber list ids to be deleted
     * @return boolean
     * @author Mert Hurturk
     * */
    public static function Delete($OwnerUserID, $ArraySubscriberListIDs) {
        // Check for required fields of this function - Start
        // $OwnerUserID check
        if (!isset($OwnerUserID) || $OwnerUserID == '') {
            return false;
        }
        // Check for required fields of this function - End

        Core::LoadObject('auto_responders');
        Core::LoadObject('campaigns');
        Core::LoadObject('custom_fields');
        Core::LoadObject('queue');
        Core::LoadObject('clients');
        Core::LoadObject('segments');
        Core::LoadObject('statistics');
        Core::LoadObject('subscribers');
        Core::LoadObject('web_service_integration');
        Core::LoadObject('suppression_list');
        Core::LoadObject('transaction_emails');
        Core::LoadObject('emails');

        if (count($ArraySubscriberListIDs) > 0) {
            foreach ($ArraySubscriberListIDs as $EachID) {
                $ArrayList = self::RetrieveList(array('*'), array('RelOwnerUserID' => $OwnerUserID, 'ListID' => $EachID), array('ListID' => 'ASC'), array(), 0, 0);
                if (!$ArrayList) {
                    continue;
                }
                Database::$Interface->DeleteRows(array('ListID' => $EachID, 'RelOwnerUserID' => $OwnerUserID), MYSQL_TABLE_PREFIX . 'subscriber_lists');

                AutoResponders::Delete($OwnerUserID, array(), $EachID);
                Campaigns::DeleteCampaignRecipients($OwnerUserID, array($EachID));
                Core::DeleteFBLReports($OwnerUserID, $EachID);
                EmailQueue::Delete($EachID);
                Clients::DeleteAssignedClientLists(0, $EachID);
                Segments::Delete($OwnerUserID, array(), $EachID);
                CustomFields::Delete($OwnerUserID, array(), $EachID);
                Statistics::Delete($EachID);
                Subscribers::DeleteImportData($EachID);
                WebServiceIntegration::Delete($OwnerUserID, array(), $EachID);
                SuppressionList::Delete($EachID);
                TransactionEmails::Delete($EachID);
                Emails::Delete(array('EmailID' => $ArrayList['RelOptInConfirmationEmailID']));
                self::DeleteSyncLogs($EachID);

                // Delete the subscriber list table - Start
                $SQLQuery = "DROP TABLE `oempro_subscribers_" . $EachID . "`";
                Database::$Interface->ExecuteQuery($SQLQuery);
                // Delete the subscriber list table - End
            }
        } else {
            $ArrayLists = self::RetrieveLists(array('*'), array('RelOwnerUserID' => $OwnerUserID), array('ListID' => 'ASC'), array(), 0, 0);

            if (count($ArrayLists) > 0) {
                foreach ($ArrayLists as $Index => $ArrayEachList) {
                    Database::$Interface->DeleteRows(array('ListID' => $ArrayEachList['ListID']), MYSQL_TABLE_PREFIX . 'subscriber_lists');

                    AutoResponders::Delete($OwnerUserID, array(), $ArrayEachList['ListID']);
                    Campaigns::DeleteCampaignRecipients($OwnerUserID, array($ArrayEachList['ListID']));
                    CustomFields::Delete($OwnerUserID, array(), $ArrayEachList['ListID']);
                    Core::DeleteFBLReports($OwnerUserID, $ArrayEachList['ListID']);
                    EmailQueue::Delete($ArrayEachList['ListID']);
                    Clients::DeleteAssignedClientLists(0, $ArrayEachList['ListID']);
                    Segments::Delete($OwnerUserID, array(), $ArrayEachList['ListID']);
                    Statistics::Delete($ArrayEachList['ListID']);
                    Subscribers::DeleteImportData($ArrayEachList['ListID']);
                    WebServiceIntegration::Delete($OwnerUserID, array(), $ArrayEachList['ListID']);
                    SuppressionList::Delete($ArrayEachList['ListID']);
                    TransactionEmails::Delete($ArrayEachList['ListID']);
                    Emails::Delete(array('EmailID' => $ArrayEachList['RelOptInConfirmationEmailID']));
                    self::DeleteSyncLogs($ArrayEachList['ListID']);

                    // Delete the subscriber list table - Start
                    $SQLQuery = "DROP TABLE `oempro_subscribers_" . $ArrayEachList['ListID'] . "`";
                    Database::$Interface->ExecuteQuery($SQLQuery);
                    // Delete the subscriber list table - End
                }
            }
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.List', array($OwnerUserID, $ArraySubscriberListIDs));
        // Plug-in hook - End
    }

    /**
     * Deletes the synchronization log of a list
     *
     * @param int $ListID 
     * @return void
     * @author Cem Hurturk
     */
    public static function DeleteSyncLogs($ListID) {
        // Check for required fields of this function - Start
        // $ArraySubscriberListIDs check
        if (!isset($ListID) || !is_numeric($ListID)) {
            return false;
        }
        // Check for required fields of this function - End

        Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'list_sync_log');

        return;
    }

    /**
     * Parses and returns the parsed mapped fields in array
     *
     * @param string $MappedFields 
     * @return array
     * @author Cem Hurturk
     */
    function ParseSyncMappedFields($MappedFields) {
        $TMPArrayMappedFields = explode(',', $MappedFields);
        ;
        $ArrayMappedFields = array();

        foreach ($TMPArrayMappedFields as $EachMap) {
            $TMPArray = explode('||||', $EachMap);
            $ArrayMappedFields[$TMPArray[1]] = $TMPArray[0];
        }

        return $ArrayMappedFields;
    }

    /**
     * Updates the log of the synchronization
     *
     * @param string $ListID 
     * @param string $Status 
     * @param string $StatusMessage 
     * @param string $Duration 
     * @return integer
     * @author Cem Hurturk
     */
    function LogListSync($ListID, $Status, $StatusMessage, $Duration, $LogID = 0) {
        if ($LogID == 0) {
            $ArrayFieldnValues = array(
                'LogID' => '',
                'RelListID' => $ListID,
                'ProcessDate' => date('Y-m-d H:i:s'),
                'ProcessStatus' => $Status,
                'ProcessStatusMessage' => $StatusMessage,
                'ProcessDuration' => $Duration,
            );
            Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'list_sync_log');
            $LogID = Database::$Interface->GetLastInsertID();
        } else {
            $ArrayFieldnValues = array(
                'ProcessDate' => date('Y-m-d H:i:s'),
                'ProcessStatus' => $Status,
                'ProcessStatusMessage' => $StatusMessage,
                'ProcessDuration' => $Duration,
            );
            Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => $LogID, 'RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'list_sync_log');
        }

        return $LogID;
    }

    /**
     * Checks if any sync is in process for the provided list ID
     *
     * @param string $ListID 
     * @return boolean
     * @author Cem Hurturk
     */
    function CheckIfAnyActiveSyncLogExists($ListID) {
        $ThresholdDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d') . ' ' . LIST_SYNC_HALT_THRESHOLD . ' ' . date('H:i:s')));

        $TotalFound = Database::$Interface->GetRows(array('COUNT(*) AS TotalFound'), array(MYSQL_TABLE_PREFIX . 'list_sync_log'), array('RelListID' => $ListID, 'ProcessStatus' => 'Processing', array('field' => 'ProcessDate', 'operator' => '>', 'value' => $ThresholdDate)), array(), 0, 0, 'AND', false, false);
        $TotalFound = $TotalFound[0];
        $TotalFound = $TotalFound['TotalFound'];

        return $TotalFound;
    }

}

// END class Lists
?>