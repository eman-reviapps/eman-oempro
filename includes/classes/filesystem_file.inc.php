<?php
/**
 * FTP extension class for file system engine
 *
 * @package default
 * @author Cem Hurturk
 **/
class FileSystemEngine_File extends FileSystemEngine
{
/**
 * Connect to the server
 *
 * @return void
 * @author Cem Hurturk
 */
public function Connect()
	{
	return true;
	}

/**
 * Disconnect from server
 *
 * @return void
 * @author Cem Hurturk
 */
public function Disconnect()
	{
	return true;
	}

/**
 * Get contents
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetContents($TargetPath, $ContentType = '')
	{
	return @file_get_contents($TargetPath);
	}

/**
 * Put contents
 *
 * @return void
 * @author Cem Hurturk
 */
public function PutContents($TargetPath, $Contents, $ContentType = '', $Mode = '')
	{
	if (($FileHandler = fopen($TargetPath, 'w'.$ContentType)) == false)
		{
		return false;
		}

	fwrite($FileHandler, $Contents);
	fclose($FileHandler);
	
	self::ChangePermissions($TargetPath, $Mode);
	return true;
	}

/**
 * Current path
 *
 * @return void
 * @author Cem Hurturk
 */
public function CurrentPath()
	{
	return @getcwd();
	}

/**
 * Change directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function ChangeDirectory($Path)
	{
	return @chdir($Path);
	}

/**
 * Change permissions
 *
 * @return void
 * @author Cem Hurturk
 */
public function ChangePermissions($Path, $Mode = false, $Recursive = false)
	{
	if ($Mode == false) return false;
	if (self::Exists($Path) == false) return false;
	
	if (self::$ObjectFTP->chmod($Path, $Mode, $Recursive) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Copy to another location
 *
 * @return void
 * @author Cem Hurturk
 */
public function Copy($SourcePath, $TargetPath, $Overwrite = false)
	{
	if (($Overwrite == false) && (self::Exists($TargetPath) == true))
		{
		return false;
		}

	return copy($SourcePath, $TargetPath);
	}

/**
 * Move to another location
 *
 * @return void
 * @author Cem Hurturk
 */
public function Move($SourcePath, $TargetPath)
	{
	if (self::Rename($SourcePath, $TargetPath) == false)
		{
		return false;
		}
	
	return true;
	}

/**
 * Renames file/directory
 *
 * @param string $Path 
 * @param string $NewPath 
 * @return void
 * @author Cem Hurturk
 */
public function Rename($Path, $NewPath)
	{
	return rename($Path, $NewPath);
	}

/**
 * Delete
 *
 * @return void
 * @author Cem Hurturk
 */
public function Delete($Path)
	{
	return unlink($Path);
	}

/**
 * Check if exists
 *
 * @return void
 * @author Cem Hurturk
 */
public function Exists($Path)
	{
	return file_exists($Path);
	}

/**
 * Check if it's a file
 *
 * @return void
 * @author Cem Hurturk
 */
public function IsFile($Path)
	{
	return is_file($Path);
	}

/**
 * Check if it's a directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function IsDirectory($Path)
	{
	return is_dir($Path);
	}

/**
 * Learn the size
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetFileSize($Path)
	{
	return filesize($Path);
	}

/**
 * Create a new directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function CreateDirectory($Path, $Permissions = false, $Owner = false, $Group = false)
	{
	if ($Permissions == false) return false;
	return mkdir($Path, $Permissions);	
	}

/**
 * Remove a directory
 *
 * @return void
 * @author Cem Hurturk
 */
public function RemoveDirectory($Path)
	{
	return @rmdir($Path);
	}

/**
 * Get directory list
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetList($Path)
	{
	// Not ready.
	
	return true;
	}
} // END class FileSystemEngine_File
?>