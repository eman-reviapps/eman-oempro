<?php

/**
 * Personalization class
 *
 * This class holds all personalization related functions
 * @package Oempro
 * @author Octeth
 * */
class Personalization extends Core {

    /**
     * Returns the list of personalization tags for subscribers
     *
     * @param string $UserID
     * @param string $ListID
     * @param string $ArrayAliases
     * @param string $CustomFieldPrefix
     * @param string $ArrayExcludeList
     * @return void
     * @author Cem Hurturk
     */
    function GetSubscriberPersonalizationTags($UserID, $ListID, $ArrayAliases, $CustomFieldPrefix, $ArrayExcludeList = array(), $ArrayLanguageStrings = array()) {
        // Load other modules - Start
        Core::LoadObject('subscribers');
        // Load other modules - End

        $ArrayPersonalizationTags = array();

        $ArraySubscriberFields = Subscribers::GetSubscriberFields($UserID, $ListID, $ArrayAliases, $CustomFieldPrefix, $ArrayExcludeList);

        foreach ($ArraySubscriberFields as $Field => $FieldName) {
            $ArrayPersonalizationTags['%Subscriber:' . $Field . '%'] = $FieldName;
        }

        // $ArrayPersonalizationTags['[condition1<>condition2,true_value,false_value]'] = $ArrayAliases['Conditional'];
        $ArrayPersonalizationTags['[IF:condition1<>condition2] true content block [ELSE] false content block [ENDIF]'] = $ArrayAliases['Conditional'];

        return $ArrayPersonalizationTags;
    }

    /**
     * Returns the list of all available personalization link tags in array
     *
     * @param array $ArrayLanguage language array
     * @param string $Category Category to retrieve links of ('Opt', 'Campaign')
     * @return array
     * @author Cem Hurturk
     * */
    function GetPersonalizationLinkTags($ArrayLanguage, $Category = 'All') {
        $ArrayAvailableLinkTags = array(
            'Opt' => array(
                '%Link:Confirm%' => $ArrayLanguage['%Link:Confirm%'],
                '%Link:Reject%' => $ArrayLanguage['%Link:Reject%'],
            ),
            'Campaign' => array(
                '%Link:Forward%' => $ArrayLanguage['%Link:Forward%'],
                '%Link:WebBrowser%' => $ArrayLanguage['%Link:WebBrowser%'],
                '%Link:ReportAbuse%' => $ArrayLanguage['%Link:ReportAbuse%'],
                '%Link:SocialShare:Twitter%' => $ArrayLanguage['%Link:SocialShare:Twitter%'],
                '%Link:SocialShare:Facebook%' => $ArrayLanguage['%Link:SocialShare:Facebook%'],
                '%RemoteContent=http://%' => $ArrayLanguage['%RemoteContent%'],
            ),
            'List' => array(
                '%Link:Unsubscribe%' => $ArrayLanguage['%Link:Unsubscribe%'],
                '%Link:SubscriberArea%' => $ArrayLanguage['%Link:SubscriberArea%'],
            ),
        );

        // Plug-in hook - Start
        // $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'PersonalizationTags.List.Campaigns', array($ArrayAvailableLinkTags));
        // $ArrayAvailableLinkTags = $ArrayPlugInReturnVars[0];
        // $ArrayAvailableLinkTags['Campaign'] = array_merge($ArrayAvailableLinkTags['Campaign'], $ArrayPlugInReturnVars);
        // Plug-in hook - End

        if ($Category == 'Opt') {
            return $ArrayAvailableLinkTags['Opt'];
        } elseif ($Category == 'Campaign') {
            return $ArrayAvailableLinkTags['Campaign'];
        } elseif ($Category == 'List') {
            return $ArrayAvailableLinkTags['List'];
        } else {
            return array_merge($ArrayAvailableLinkTags['Opt'], $ArrayAvailableLinkTags['Campaign'], $ArrayAvailableLinkTags['List']);
        }
    }

    /**
     * Returns the list of all available personalization user tags
     *
     * @param array $ArrayLanguage language array
     * @return array
     * @author Cem Hurturk
     * */
    function GetPersonalizationUserTags($ArrayLanguage) {
        $ArrayAvailableUserTags = array(
            '%User:FirstName%' => $ArrayLanguage['%User:FirstName%'],
            '%User:LastName%' => $ArrayLanguage['%User:LastName%'],
            '%User:EmailAddress%' => $ArrayLanguage['%User:EmailAddress%'],
            '%User:CompanyName%' => $ArrayLanguage['%User:CompanyName%'],
            '%User:Website%' => $ArrayLanguage['%User:Website%'],
            '%User:Street%' => $ArrayLanguage['%User:Street%'],
            '%User:City%' => $ArrayLanguage['%User:City%'],
            '%User:State%' => $ArrayLanguage['%User:State%'],
            '%User:Zip%' => $ArrayLanguage['%User:Zip%'],
            '%User:Country%' => $ArrayLanguage['%User:Country%'],
            '%User:Phone%' => $ArrayLanguage['%User:Phone%'],
            '%User:Fax%' => $ArrayLanguage['%User:Fax%'],
            '%User:TimeZone%' => $ArrayLanguage['%User:TimeZone%'],
        );

        return $ArrayAvailableUserTags;
    }

    /**
     * Returns the list of all available personalization user tags
     *
     * @param array $ArrayLanguage language array
     * @return array
     * @author Cem Hurturk
     * */
    function GetPersonalizationListTags($ArrayLanguage) {
        $ArrayAvailableUserTags = array(
            '%List:ID%' => $ArrayLanguage['%List:ID%'],
            '%List:Name%' => $ArrayLanguage['%List:Name%']
        );

        return $ArrayAvailableUserTags;
    }

    /**
     * Returns personalization tags that are not classified under other categories
     *
     * @param string $ArrayLanguage
     * @return void
     * @author Cem Hurturk
     */
    function GetOtherPersonalizationTags($ArrayLanguage) {
        $ArrayAvailableUserTags = array(
            '%Date=...%' => $ArrayLanguage['Date']
        );

        return $ArrayAvailableUserTags;
    }

    /**
     * Checks if the detected link is a built-in link of any Sendloop module
     *
     * @return bool Returns true if it's a system link
     * @author Cem Hurturk
     * */
    function IsSystemLink($Link) {
        // Initialize variables - Start
        $Result = false;
        // Initialize variables - End

        $ArraySystemLinks = array(
            'linktag.php',
            'track_open.php',
            'track_link.php',
            'unsubscribe.php',
            'opt_confirm.php',
            'web_browser.php',
            'report_abuse.php',
            'forward_email.php',
            'subscriber/',
            'to.php',
            'tl.php',
            'u.php',
            'oc.php',
            'wb.php',
            'f.php'
        );

        foreach ($ArraySystemLinks as $EachLink) {
            if (substr($Link, 0, strlen(APP_URL . $EachLink)) == APP_URL . $EachLink) {
                $Result = true;
                break;
            }
        }

        return $Result;
    }

    /**
     * Finds all links the HTML string
     *
     * @return array Returns links and title attibutes in array
     * @author Cem Hurturk
     * */
    function FindAllLinksInHTML($String) {
        // Initialize variables - Start
        $ArrayLinks = array();
        // Initialize variables - End
        // Insert each <a> into new line - Start
        $NewString = str_replace("</a>", "</a>\n", $String);
        // Insert each <a> into new line - End
        // Find all links - Start
        $TMPPattern = "/<a(.*)>.*<\/a>/isU";
        preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);
        // Find all links - End
        // Find "href" and "title" parameters inside links - Start
        $TMPCounter = 0;

        foreach ($ArrayMatches as $EachIndex => $ArrayEachMatch) {
            // Find the title of the link - Start
            $SearchOn = $ArrayEachMatch[1];
            $TMPPattern = "/title=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            $ArrayLinks[$TMPCounter]['Title'] = $ArraySubMatches[0][1];
            $ArrayLinks[$TMPCounter]['FullTitle'] = $ArraySubMatches[0][0];
            // Find the title of the link - End
            // Find the link of the link - Start
            $TMPPattern = "/href=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            $ArrayLinks[$TMPCounter]['Link'] = $ArraySubMatches[0][1];
            $ArrayLinks[$TMPCounter]['FullLink'] = $ArraySubMatches[0][0];
            // Find the link of the link - End
            // Find the nochange parameter in the link - Start
            $TMPPattern = "/class=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            if (count($ArraySubMatches) > 0) {
                if (strpos('no-link-track', $ArraySubMatches[0][1]) !== false && $ArraySubMatches[0][1] != "link") {
                    $ArrayLinks[$TMPCounter]['NoLinkTrack'] = true;
                }
            }
            // Find the nochange parameter in the link - End

            $ArrayLinks[$TMPCounter]['AllLink'] = $ArrayEachMatch[0];

            $TMPCounter++;
        }

        // Find "href" and "title" parameters inside links - End
        // Find map links - Start
        $TMPPattern = "/<area(.*)>/isU";
        preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);

        foreach ($ArrayMatches as $EachIndex => $ArrayEachMatch) {
            // Find the title of the link - Start
            $SearchOn = $ArrayEachMatch[1];
            $TMPPattern = "/title=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            $ArrayLinks[$TMPCounter]['Title'] = $ArraySubMatches[0][1];
            $ArrayLinks[$TMPCounter]['FullTitle'] = $ArraySubMatches[0][0];
            // Find the title of the link - End
            // Find the link of the link - Start
            $TMPPattern = "/href=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            $ArrayLinks[$TMPCounter]['Link'] = $ArraySubMatches[0][1];
            $ArrayLinks[$TMPCounter]['FullLink'] = $ArraySubMatches[0][0];
            // Find the link of the link - End
            // Find the nochange parameter in the link - Start
            $TMPPattern = "/class=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

            if (count($ArraySubMatches) > 0) {
                if (strpos('no-link-track', $ArraySubMatches[0][1]) !== false && $ArraySubMatches[0][1] != "link") {
                    $ArrayLinks[$TMPCounter]['NoLinkTrack'] = true;
                }
            }
            // Find the nochange parameter in the link - End

            $ArrayLinks[$TMPCounter]['AllLink'] = $ArrayEachMatch[0];

            $TMPCounter++;
        }
        // Find map links - End


        $NewArrayLinks = array();
        foreach ($ArrayLinks as $Link) {
            if (isset($Link['Link']) && !empty($Link['Link'])) {
                $NewArrayLinks[] = $Link;
            }
        }
//        print_r('<pre>');
//        print_r($NewArrayLinks);
//        print_r('</pre>');
//        exit();
        return $NewArrayLinks;
    }

    /**
     * Conditional personalization for versions older than v4.2.2
     *
     * @return string
     * @author Cem Hurturk, Kubilay Eksioglu
     * */
    function ConditionalPersonalization_ForOlderThanV422($StringToPersonalize) {
        // Example: "[4<=2, Text\, true, Text2\, false]";
        // preg_replace while using char "\" does not work properly, replace "\," with "&," to handle problem.
        $StringToPersonalize = str_replace("\\,", "&,", $StringToPersonalize);

        $TMPArraySearchList = array();
        $TMPArrayReplaceList = array();

        // Find all conditional personalization tags - STARTED
        // $TMPPattern = "/\[(.*)\]/iU";
        $TMPPattern = "/\[([^\[\]]+,[^\[\]]+)\]/iU"; // Only match square brackets with commas inside
        preg_match_all($TMPPattern, $StringToPersonalize, $ArrayMatches, PREG_SET_ORDER);
        // Find all conditional personalization tags - FINISHED
        // Loop each condition - Start
        foreach ($ArrayMatches as $EachMatch) {
            $ConditionalSet = $EachMatch[1];

            /* replace ampersand-commas with pipes to preserve exploding - Begin */
            $ConditionalSet = str_replace("&,", "||", $ConditionalSet);
            /* replace ampersand-commas with pipes to preserve exploding - End */

            // Split condition into parts (0 -> logic, 1 -> true, 2 -> false) - Start
            $ArrayConditionParts = explode(',', $ConditionalSet);
            // Split condition into parts (0 -> logic, 1 -> true, 2 -> false) - End
            // Learn the condition - Start
            preg_match('/(.*?)([!<>][=]|[<>=])(.*)/i', $ArrayConditionParts[0], $TMPArray);
            $LeftCondition = $TMPArray[1];
            $RightCondition = $TMPArray[3];
            $Operator = $TMPArray[2];
            // Learn the condition - End

            /* Replace pipes with normal commas to display properly - Begin */
            $ArrayConditionParts[1] = str_replace("||", ",", $ArrayConditionParts[1]);
            $ArrayConditionParts[2] = str_replace("||", ",", $ArrayConditionParts[2]);
            /* Replace pipes with normal commas to display properly - End */

            // Perform true or false personalization - Start
            if (($Operator == '=') && ($LeftCondition == $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } elseif (($Operator == '>') && ($LeftCondition > $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } elseif (($Operator == '<') && ($LeftCondition < $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } elseif (($Operator == '!=') && ($LeftCondition != $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } elseif (($Operator == '<=') && ($LeftCondition <= $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } elseif (($Operator == '>=') && ($LeftCondition >= $RightCondition)) {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[1];
            } else {
                $TMPArraySearchList[] = $EachMatch[0];
                $TMPArrayReplaceList[] = $ArrayConditionParts[2];
            }
            // Perform true or false personalization - End
        }
        // Loop each condition - End

        foreach ($TMPArraySearchList as $Index => $Val) {
            $TMPArraySearchList[$Index] = '/' . preg_quote($Val, '/') . '/';
        }

        $StringToPersonalize = preg_replace($TMPArraySearchList, $TMPArrayReplaceList, $StringToPersonalize);
        return $StringToPersonalize;
    }

    /**
     * Personalizes the given string
     *
     * @param string $StringToPersonalize String to personalize
     * @param array $ArrayPersonalizationScope Define what kind of personalization to do
     * @return string
     * @author Cem Hurturk
     * */
    function Personalize($StringToPersonalize, $ArrayPersonalizationScope, $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, $ArrayAutoResponder, $IsPreview = false, $ArrayEmail = array(), $DisablePersonalization = false) {
        // Perform personalization of standard tags - Start {
        $StringToPersonalize = self::StandardTagPersonalization($StringToPersonalize);
        // Perform personalization of standard tags - End }
        // Other personalizations - Start {
        $TMPArraySearchList = array();
        $TMPArrayReplaceList = array();

        if (count($ArrayList) > 0) {
            $TMPArraySearchList[] = '/%List:ID%/';
            $TMPArrayReplaceList[] = $ArrayList['ListID'];
            $TMPArraySearchList[] = '/%List:Name%/';
            $TMPArrayReplaceList[] = $ArrayList['Name'];
        }

        foreach ($ArrayPersonalizationScope as $EachScope) {
            if ($EachScope == 'Subscriber') {
                // Subscriber information
                foreach (array_keys($ArraySubscriber) as $Field) {
                    $TMPArraySearchList[] = '/%Subscriber:' . $Field . '%/';
                    $TMPArrayReplaceList[] = $ArraySubscriber[$Field];
                }
            } elseif ($EachScope == 'User') {
                // User (Sender) information
                $ArrayReplaceList = array(
                    'FirstName' => $ArrayUser['FirstName'],
                    'LastName' => $ArrayUser['LastName'],
                    'EmailAddress' => $ArrayUser['EmailAddress'],
                    'CompanyName' => $ArrayUser['CompanyName'],
                    'Website' => $ArrayUser['Website'],
                    'Street' => $ArrayUser['Street'],
                    'City' => $ArrayUser['City'],
                    'State' => $ArrayUser['State'],
                    'Zip' => $ArrayUser['Zip'],
                    'Country' => $ArrayUser['Country'],
                    'Phone' => $ArrayUser['Phone'],
                    'Fax' => $ArrayUser['Fax'],
                    'TimeZone' => $ArrayUser['TimeZone'],
                );
                foreach ($ArrayReplaceList as $Field => $Value) {
                    $TMPArraySearchList[] = '/%User:' . $Field . '%/';
                    $TMPArrayReplaceList[] = $Value;
                }
            } elseif ($EachScope == 'List') {
                // Subscriber list information
                $ArrayReplaceList = array(
                    'Name' => $ArrayList['Name'],
                );
                foreach ($ArrayReplaceList as $Field => $Value) {
                    $TMPArraySearchList[] = '/%List:' . $Field . '%/';
                    $TMPArrayReplaceList[] = $Value;
                }
            } elseif ($EachScope == 'OptLinks') {
                // Opt-in/out links
                // Opt-in/out confirmation link - Start
                if (strpos($StringToPersonalize, '%Link:Confirm%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayList['ListID'], // List ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                1, // Is confirm
                                ($IsPreview == true ? 1 : 0) // Is preview
                    ));
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:Confirm%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        $TMPArrayReplaceList[] = APP_URL . 'oc.php?p=' . $EncryptedQuery;
                    }
                }
                // Opt-in/out confirmation link - End
                // Opt-in/out reject link - Start
                if (strpos($StringToPersonalize, '%Link:Reject%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayList['ListID'], // List ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                0, // Is reject
                                ($IsPreview == true ? 1 : 0) // Is preview
                    ));
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:Reject%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        $TMPArrayReplaceList[] = APP_URL . 'oc.php?p=' . $EncryptedQuery;
                    }
                }
                // Opt-in/out reject link - End
            } elseif ($EachScope == 'Links') {
                // Links
                // Unsubscription link - Start
                if (strpos($StringToPersonalize, '%Link:Unsubscribe%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                $ArrayAutoResponder['AutoResponderID'], // Autoresponder ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                $ArrayList['ListID'], // List ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                ($IsPreview == true ? 1 : 0), // Is preview
                                (OPTOUT_CONFIRMATION == true ? 1 : 0) // Request opt-out confirmation
                    ));
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:Unsubscribe%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        // $TMPArrayReplaceList[]		= APP_URL.'unsubscribe.php?FormValue_CampaignID='.$ArrayCampaign['CampaignID'].'&FormValue_SubscriberID='.$ArraySubscriber['SubscriberID'].'&FormValue_ListID='.$ArrayList['ListID'].'&FormValue_Command=Subscriber.Unsubscribe'.($IsPreview == true ? '&Preview=1' : '');
                        $TMPArrayReplaceList[] = APP_URL . 'u.php?p=' . $EncryptedQuery;
                    }
                }
                // Unsubscription link - End
                // Forward to friend link - Start
                if (strpos($StringToPersonalize, '%Link:Forward%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                $ArrayAutoResponder['AutoResponderID'], // Autoresponder ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                $ArrayList['ListID'], // List ID
                                ($IsPreview == true ? 1 : 0) // Is preview
                    ));
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:Forward%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        // $TMPArrayReplaceList[]		= APP_URL.'forward_email.php?CampaignID='.$ArrayCampaign['CampaignID'].'&SubscriberID='.$ArraySubscriber['SubscriberID'].'&ListID='.$ArrayList['ListID'].($IsPreview == true ? '&Preview=1' : '');
                        $TMPArrayReplaceList[] = APP_URL . 'f.php?p=' . $EncryptedQuery;
                    }
                }
                // Forward to friend link - End
                // Browser version link - Start
                if (strpos($StringToPersonalize, '%Link:WebBrowser%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                $ArrayAutoResponder['AutoResponderID'], // Autoresponder ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                $ArrayList['ListID'], // List ID
                                ($IsPreview == true ? 1 : 0) // Is preview
                    ));
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:WebBrowser%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        // $TMPArrayReplaceList[]		= APP_URL.'web_browser.php?CampaignID='.$ArrayCampaign['CampaignID'].'&SubscriberID='.$ArraySubscriber['SubscriberID'].'&ListID='.$ArrayList['ListID'].($IsPreview == true ? '&Preview=1' : '');
                        $TMPArrayReplaceList[] = APP_URL . 'wb.php?p=' . $EncryptedQuery;
                    }
                }
                // Browser version link - End
                // Social sharing links - Twitter - Start
                if (strpos($StringToPersonalize, '%Link:SocialShare:Twitter%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                0, // Autoresponder ID
                    ));
                    $Hash = Core::ShortenLink(APP_URL . 'wb.php?p=' . $EncryptedQuery);
                    $CampaignPublicLink = APP_URL . 'link.php?p=' . rawurlencode($Hash);
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:SocialShare:Twitter%/';
                    $TMPArrayReplaceList[] = 'http://twitter.com/intent/tweet/?url=' . rawurlencode($CampaignPublicLink);
                }
                // Social sharing links - Twitter - End
                // Social sharing links - Twitter - Start
                if (strpos($StringToPersonalize, '%Link:SocialShare:Facebook%') !== false) {
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                0, // Autoresponder ID
                    ));
                    $Hash = Core::ShortenLink(APP_URL . 'wb.php?p=' . $EncryptedQuery);
                    $CampaignPublicLink = APP_URL . 'link.php?p=' . rawurlencode($Hash);
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:SocialShare:Facebook%/';

                    $TMPArrayReplaceList[] = 'http://www.facebook.com/sharer/sharer.php?u=' . rawurlencode($CampaignPublicLink) . '&t=' . rawurlencode($ArrayEmail['Subject']);
                }
                // Social sharing links - Twitter - End
                // Report abuse link - Start
                if (strpos($StringToPersonalize, '%Link:ReportAbuse%') !== false) {
                    Core::LoadObject('queue');
                    $CampaignID = isset($ArrayCampaign['CampaignID']) ? $ArrayCampaign['CampaignID'] : 0;
                    $AutoResponderID = isset($ArrayAutoResponder['AutoResponderID']) ? $ArrayAutoResponder['AutoResponderID'] : 0;
                    $AbuseMessageID = EmailQueue::GenerateAbuseMessageID($CampaignID, $ArraySubscriber['SubscriberID'], $ArraySubscriber['EmailAddress'], $ArrayList['ListID'], $ArrayList['RelOwnerUserID'], $AutoResponderID);
                    $ReportAbuseURL = X_REPORT_ABUSE_URL . $AbuseMessageID;

                    $TMPArraySearchList[] = '/%Link:ReportAbuse%/';
                    $TMPArrayReplaceList[] = $ReportAbuseURL;
                }
                // Report abuse link - End
                // Link to subscriber area - Start
                if (strpos($StringToPersonalize, '%Link:SubscriberArea%') !== false) {
                    // Encrypted query parameters - Start
                    $ArrayQueryParameters = array(
                        'SubscriberID' => md5($ArraySubscriber['SubscriberID']),
                        'EmailAddress' => md5($ArraySubscriber['EmailAddress']),
                        'ListID' => $ArrayList['ListID'],
                    );
                    $EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
                    $EncryptedQuery = rtrim($EncryptedQuery, '%3D');
                    // Encrypted query parameters - End

                    $TMPArraySearchList[] = '/%Link:SubscriberArea%/';
                    if ($DisablePersonalization == true) {
                        $TMPArrayReplaceList[] = '#';
                    } else {
                        // '?p=' is removed from subscriber area link and changed to new url
                        // $TMPArrayReplaceList[]		= APP_URL.'subscriber/?p='.$EncryptedQuery;
                        // We can not user InterfaceAppURL in the below line because, InterfaceHelper library is not accessible from here
                        $TMPArrayReplaceList[] = APP_URL . APP_DIRNAME . (HTACCESS_ENABLED == false ? '/index.php?' : '') . '/subscriber/login/' . $EncryptedQuery;
                    }
                }
                // Link to subscriber area - End
            } elseif ($EachScope == 'OpenTracking') {
                // Track email opens
                // Encrypted query parameters - Start
                $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                            $ArrayCampaign['CampaignID'], // Campaign ID
                            (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                            $ArrayAutoResponder['AutoResponderID'], // Autoresponder ID
                            $ArraySubscriber['SubscriberID'], // Subscriber ID
                            $ArrayList['ListID'], // List ID
                            ($IsPreview == true ? 1 : 0) // Is preview
                ));
                // Encrypted query parameters - End

                if ($DisablePersonalization == false) {
                    // $OpenTrackURL 	= '<img src="'.APP_URL.'track_open.php?SubscriberID='.$ArraySubscriber['SubscriberID'].'&ListID='.$ArrayList['ListID'].'&CampaignID='.$ArrayCampaign['CampaignID'].($IsPreview == true ? '&Preview=1' : '').'" width="5" height="2" alt=".">';
                    $OpenTrackURL = '<img src="' . APP_URL . 'to.php?p=' . $EncryptedQuery . '" width="5" height="2" alt=".">';

                    $TMPArraySearchList[] = '/<\/body>/i';
                    $TMPArrayReplaceList[] = "\n" . $OpenTrackURL . "\n\n</body>";
                }
            }
        }

        $StringToPersonalize = preg_replace($TMPArraySearchList, $TMPArrayReplaceList, $StringToPersonalize);
        // Other personalizations - End }
        // Perform remote content personalization - Start {
        if (in_array('RemoteContent', $ArrayPersonalizationScope) == true) {
            $TMPPattern = "/%RemoteContent=(.*)%/isU";
            if (preg_match_all($TMPPattern, $StringToPersonalize, $ArrayMatches, PREG_SET_ORDER)) {
                foreach ($ArrayMatches as $EachMatch) {
                    $TMPPattern = "%RemoteContent=" . $EachMatch[1] . "%";
                    $TMPReplace = Core::FetchRemoteContent($EachMatch[1]);
                    $StringToPersonalize = str_replace($TMPPattern, $TMPReplace, $StringToPersonalize);
                }
            }
        }
        // Perform remote content personalization - End }
        // Perform modifiers - Start {
        $modifier_parser = new O_Email_Content_ModifierParser();
        $StringToPersonalize = $modifier_parser->parse($StringToPersonalize);
        unset($modifier_parser);
        // Perform modifiers - End }
        // Perform conditional personalization - Start {
        $conditional_parser = new O_Email_Content_ConditionalParser();
        $StringToPersonalize = $conditional_parser->parse($StringToPersonalize);
        unset($conditional_parser);
        $StringToPersonalize = self::ConditionalPersonalization_ForOlderThanV422($StringToPersonalize);
        // Perform conditional personalization - End }
        // Perform plugin personalization - Start {
        $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Personalization.Content', array($StringToPersonalize, $ArrayPersonalizationScope, $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, $ArrayAutoResponder, $IsPreview, $ArrayEmail, $DisablePersonalization));
        if (is_array($ArrayPlugInReturnVars)) {
            $StringToPersonalize = $ArrayPlugInReturnVars[0];
        }
        // Perform plugin personalization - End }
        // Perform link tracking - Start {
        if (in_array('LinkTracking', $ArrayPersonalizationScope) == true) {
            // Track click through

            $ArrayLinks = self::FindAllLinksInHTML($StringToPersonalize);
//            print_r('<pre>');
//            print_r($ArrayLinks);
//            print_r('</pre>');
//            exit();
            // Loop each detected link - Start
            foreach ($ArrayLinks as $EachIndex => $ArrayEachLink) {
                // If link is not anchor (#) or mailto: or system link or set not to be tracked , proceed - Start
                if ((((isset($ArrayEachLink['NoLinkTrack']) == false) || ($ArrayEachLink['NoLinkTrack'] != true))) && (substr($ArrayEachLink['Link'], 0, 1) != '#') && (strtolower(substr($ArrayEachLink['Link'], 0, 7)) != 'mailto:') && (self::IsSystemLink($ArrayEachLink['Link']) == false)) {
                    // Prepare detected links and their titles - Start
                    $TMPLinkTitle = rawurlencode($ArrayEachLink['Title']);
                    $TMPLinkURL = rawurlencode($ArrayEachLink['Link']);
                    // Prepare detected links and their titles - End
                    // Encrypted query parameters - Start
                    $EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
                                $ArrayCampaign['CampaignID'], // Campaign ID
                                (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0), // Email ID
                                $ArrayAutoResponder['AutoResponderID'], // Autoresponder ID
                                $ArraySubscriber['SubscriberID'], // Subscriber ID
                                $ArrayList['ListID'], // List ID
                                ($IsPreview == true ? 1 : 0) // Is preview
                    ));
                    $EncryptedQuery .= '/' . $TMPLinkTitle . '/' . $TMPLinkURL;
                    // Encrypted query parameters - End
                    // Register the link  - Start
                    if ($DisablePersonalization == true) {
                        $ReplaceString = $ArrayEachLink['Link'];
                    } else {
                        // $ReplaceString = APP_URL.'track_link.php?SubscriberID='.$ArraySubscriber['SubscriberID'].'&ListID='.$ArrayList['ListID'].'&CampaignID='.$ArrayCampaign['CampaignID'].'&LinkURL='.$TMPLinkURL.'&LinkTitle='.$TMPLinkTitle.($IsPreview == true ? '&Preview=1' : '');
                        $ReplaceString = APP_URL . 'tl.php?p=' . $EncryptedQuery;
                    }
                    // Register the link  - End
                    // Replace link href with the new one in the link - Start
                    $TMPPattern = $ArrayEachLink['FullLink'];
                    $TMPAllLink = str_replace($TMPPattern, 'href="' . $ReplaceString . '"', $ArrayEachLink['AllLink']);
                    // Replace link href with the new one in the link - End
                    // Replace the link in the string - Start
                    $TMPPattern = $ArrayEachLink['AllLink'];
                    $TMPReplace = $TMPAllLink;
                    $StringToPersonalize = str_replace($TMPPattern, $TMPReplace, $StringToPersonalize);
                    // Replace the link in the string - End
                }
                // If link is not anchor (#) or mailto: or system link, proceed - End
            }
            // Loop each detected link - End
        }
        // Perform link tracking - End }

        return $StringToPersonalize;
    }

    /**
     * Personalization of standard tags including %Date=...%
     *
     * @param string $StringToPersonalize
     * @return void
     * @author Cem Hurturk
     */
    function StandardTagPersonalization($StringToPersonalize) {
        $TMPPattern = "/%Date=(.*)%/isU";
        if (preg_match_all($TMPPattern, $StringToPersonalize, $ArrayMatches, PREG_SET_ORDER)) {
            foreach ($ArrayMatches as $EachMatch) {
                $TMPPattern = "%Date=" . $EachMatch[1] . "%";
                $TMPReplace = date($EachMatch[1]);
                $StringToPersonalize = str_replace($TMPPattern, $TMPReplace, $StringToPersonalize);
            }
        }

        return $StringToPersonalize;
    }

    function GetTagsFor($Mode, $UserID, $ListID, $LanguageAliasesForSubscriberTags, $LanguageAliasesForLinkTags, $LanguageAliasesForUserTags, $LanguageAliasesForOtherTags, $CustomFieldPrefix, $LanguageAliasesForLabels) {
        if ($Mode == 'campaign') {
            $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($UserID, $ListID, $LanguageAliasesForSubscriberTags, $CustomFieldPrefix);
            $array_campaign_link_tags = Personalization::GetPersonalizationLinkTags($LanguageAliasesForLinkTags, 'Campaign');
            $array_list_link_tags = Personalization::GetPersonalizationLinkTags($LanguageAliasesForLinkTags, 'List');
            $array_list_tags = Personalization::GetPersonalizationListTags($LanguageAliasesForOtherTags);
            $array_user_tags = Personalization::GetPersonalizationUserTags($LanguageAliasesForUserTags);
            $array_other_tags = Personalization::GetOtherPersonalizationTags($LanguageAliasesForOtherTags);
            $array_content_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[1] => $array_campaign_link_tags,
                $LanguageAliasesForLabels[2] => $array_list_link_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
                $LanguageAliasesForLabels[4] => $array_other_tags
            );
            $array_subject_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
                $LanguageAliasesForLabels[4] => $array_other_tags
            );
        }
        // TODO: add  hook listeners to confirmation and autoresponder
        else if ($Mode == 'confirmation') {
            $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($UserID, array($ListID), $LanguageAliasesForSubscriberTags, $CustomFieldPrefix);
            $array_opt_tags = Personalization::GetPersonalizationLinkTags($LanguageAliasesForLinkTags, 'Opt');
            $array_user_tags = Personalization::GetPersonalizationUserTags($LanguageAliasesForUserTags);
            $array_list_tags = Personalization::GetPersonalizationListTags($LanguageAliasesForOtherTags);
            $array_content_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[5] => $array_opt_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
            );
            $array_subject_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
            );
        } else if ($Mode == 'autoresponder') {
            $array_subscriber_tags = Personalization::GetSubscriberPersonalizationTags($UserID, array($ListID), $LanguageAliasesForSubscriberTags, $CustomFieldPrefix);
            $array_campaign_link_tags = Personalization::GetPersonalizationLinkTags($LanguageAliasesForLinkTags, 'Campaign');
            $array_list_link_tags = Personalization::GetPersonalizationLinkTags($LanguageAliasesForLinkTags, 'List');
            $array_user_tags = Personalization::GetPersonalizationUserTags($LanguageAliasesForUserTags);
            $array_other_tags = Personalization::GetOtherPersonalizationTags($LanguageAliasesForOtherTags);
            $array_list_tags = Personalization::GetPersonalizationListTags($LanguageAliasesForOtherTags);
            $array_content_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[1] => $array_campaign_link_tags,
                $LanguageAliasesForLabels[2] => $array_list_link_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
                $LanguageAliasesForLabels[4] => $array_other_tags
            );
            $array_subject_tags = array(
                $LanguageAliasesForLabels[0] => $array_subscriber_tags,
                $LanguageAliasesForLabels[6] => $array_list_tags,
                $LanguageAliasesForLabels[3] => $array_user_tags,
                $LanguageAliasesForLabels[4] => $array_other_tags
            );
        }

        $array_subject_tags = Plugins::HookListener('Filter', 'PersonalizationTags.' . ucwords($Mode) . '.Subject', array($array_subject_tags));
        $array_subject_tags = $array_subject_tags[0];
        $array_content_tags = Plugins::HookListener('Filter', 'PersonalizationTags.' . ucwords($Mode) . '.Content', array($array_content_tags));
        $array_content_tags = $array_content_tags[0];

        return array($array_subject_tags, $array_content_tags);
    }

    function AddEmailHeaderFooter($PlainContent, $HTMLContent, $ArrayUserGroup) {
        if ($ArrayUserGroup['PlainEmailHeader'] != '') {
            $PlainContent = $ArrayUserGroup['PlainEmailHeader'] . $PlainContent;
        }
        if ($ArrayUserGroup['PlainEmailFooter'] != '') {
            $PlainContent = $PlainContent . $ArrayUserGroup['PlainEmailFooter'];
        }
        if ($ArrayUserGroup['HTMLEmailHeader'] != '') {
            // Locate <body> tag - STARTED
            preg_match_all('/<(body)(.*)>/iU', $HTMLContent, $ArrayMatches, PREG_SET_ORDER);
            $TargetTag = $ArrayMatches[0][0];
            // Locate <body> tag - FINISHED

            $HTMLContent = str_replace($TargetTag, $TargetTag . "\n\n" . $ArrayUserGroup['HTMLEmailHeader'], $HTMLContent);
        }
        if ($ArrayUserGroup['HTMLEmailFooter'] != '') {
            // Locate </body> tag - STARTED
            preg_match_all('/<(\/body)(.*)>/iU', $HTMLContent, $ArrayMatches, PREG_SET_ORDER);
            $TargetTag = $ArrayMatches[0][0];
            // Locate </body> tag - FINISHED

            $HTMLContent = str_replace($TargetTag, $ArrayUserGroup['HTMLEmailFooter'] . "\n\n" . $TargetTag, $HTMLContent);
        }

        return array($PlainContent, $HTMLContent);
    }

}

// END class Personalization
?>
