<?php

class Step
{
	private $id = null;
	private $title = null;
	private $pre_function = null;
	private $pre_function_class = null;
	private $post_function = null;
	private $post_function_class = null;

	function __construct($step_id, $pre_function, $post_function) {
		$this->id = $step_id;

		$this->pre_function_class = $pre_function[0];
		$this->pre_function = $pre_function[1];

		$this->post_function_class = $post_function[0];
		$this->post_function = $post_function[1];
	}

	public function pre() {
		return call_user_func(array($this->pre_function_class, $this->pre_function));
	}

	public function post() {
		return call_user_func(array($this->post_function_class, $this->post_function));
	}

	public function get_id() {
		return $this->id;
	}

	public function set_title($title) {
		$this->title = $title;
	}

	public function get_title() {
		return $this->title;
	}
}

class Flow
{
	private $steps = array();
	private $id = '';
	private $title = '';
	private $description = '';

	function __construct($arr_steps = array()) {
		if (count($arr_steps) > 0) {
			foreach ($arr_steps as $each_step) {
				if ($each_step instanceof Step) {
					$this->add_step($each_step);
				}
			}
		}
	}

	public function add_step(Step $new_step) {
		$this->steps[] = $new_step;
	}

	public function get_step($step_number) {
		return $this->steps[$step_number];
	}

	public function get_step_count() {
		return count($this->steps);
	}

	public function set_id($id) {
		$this->id = $id;
	}

	public function set_title($title) {
		$this->title = $title;
	}

	public function set_description($description) {
		$this->description = $description;
	}

	public function get_id() {
		return $this->id;
	}

	public function get_title() {
		return $this->title;
	}

	public function get_description() {
		return $this->description;
	}

	public function run_pre($step) {
		return $this->steps[$step - 1]->pre();
	}

	public function run_post($step) {
		return $this->steps[$step - 1]->post();
	}
}

class Wizard
{
	public $name = '';
	public $flow = null;
	public $current_step = 1;
	public $extra_parameters = array();
	public $last_step = 1;

	function __construct($name, $flow = '') {
		$this->name = $name;
		$this->flow = $flow;
	}

	function set_extra_parameter($parameter_name, $parameter_value, $overwrite = false) {
		if (isset($this->extra_parameters[$parameter_name]) && $overwrite == false) return;

		$this->extra_parameters[$parameter_name] = $parameter_value;
	}

	function get_extra_parameter($parameter_name) {
		if (!isset($this->extra_parameters[$parameter_name])) return null;
		return $this->extra_parameters[$parameter_name];
	}

	function get_all_parameters_as_array() {
		return $this->extra_parameters;
	}

	function run_pre() {
		if ($this->current_step < $this->flow->get_step_count() + 1) {
			$this->flow->run_pre($this->current_step);
		}
	}

	function run_post() {
		$post_return = $this->flow->run_post($this->current_step);
		if ($post_return == true) {
			$this->current_step = $this->current_step + 1;
			if ($this->current_step >= $this->flow->get_step_count() + 1) {
				$this->current_step = 1;
				$_SESSION[SESSION_NAME][$this->name] = array();
				unset($_SESSION[SESSION_NAME][$this->name]);
			} else {
				if ($this->current_step > $this->last_step) $this->set_last_step($this->current_step);
				$_SESSION[SESSION_NAME][$this->name] = serialize($this);
			}
		}
		$this->run_pre();
	}

	function get_current_step() {
		return $this->current_step;
	}

	function get_current_step_object() {
		return $this->flow->get_step($this->current_step - 1);
	}

	function set_current_step($step) {
		if ($this->flow->get_step_count() < $step) $step = $this->flow->get_step_count();
		if ($this->last_step < $step) $step = $this->get_last_step();
		$this->current_step = $step;
	}

	function set_last_step($step) {
		$this->last_step = $step;
	}

	function get_last_step() {
		return $this->last_step;
	}

	function set_flow($flow) {
		$this->flow = $flow;
	}
}

class WizardFactory
{
	public static function get($session_variable, $flow = '') {
		if (isset($_SESSION[SESSION_NAME][$session_variable])) {
			$unserialized = unserialize(
				$_SESSION[SESSION_NAME][$session_variable]
			);
			if ($unserialized instanceof Wizard) {
				$unserialized->set_flow($flow);
				$_SESSION[SESSION_NAME][$session_variable] = serialize($unserialized);
				return $unserialized;
			}
		}
		$wizard = new Wizard($session_variable, $flow);
		$_SESSION[SESSION_NAME][$session_variable] = serialize($wizard);
		return $wizard;
	}

	public static function save($session_variable, Wizard $wizard) {
		$_SESSION[SESSION_NAME][$session_variable] = serialize($wizard);
	}

	public static function delete($session_variable) {
		$_SESSION[SESSION_NAME][$session_variable] = array();
		unset($_SESSION[SESSION_NAME][$session_variable]);
	}
}

class Session_message
{
	private $extra_parameters = array();

	function __construct() {}

	function set_parameter($parameter_name, $parameter_value, $overwrite = false)
	{
		if (isset($this->extra_parameters[$parameter_name]) && $overwrite == false) return;

		$this->extra_parameters[$parameter_name] = $parameter_value;
	}

	function get_parameter($parameter_name)
	{
		if (!isset($this->extra_parameters[$parameter_name])) return null;
		return $this->extra_parameters[$parameter_name];
	}

	function get_all_parameters_as_array()
	{
		return $this->extra_parameters;
	}
}