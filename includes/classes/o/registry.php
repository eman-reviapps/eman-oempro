<?php
class O_Registry
{
	/**
	 * @var O_Registry
	 */
	protected static $_instance;

	private $_values = array();

	protected function _get($key)
	{
		if (!isset($this->_values[$key]))
			return NULL;
		return $this->_values[$key];
	}

	protected function _set($key, $val)
	{
		$this->_values[$key] = $val;
	}

	/**
	 * @static
	 * @return O_Registry
	 */
	public static function instance()
	{
		if (!self::$_instance) {
			self::$_instance = new O_Registry();
		}
		return self::$_instance;
	}

	/**
	 * @throws O_Integration_Exception
	 * @return O_Integration_Wufoo_Module
	 */
	public function getWufooIntegrationModule()
	{
		if (is_null($this->_get('O_Integration_Wufoo_Module'))) {
			if (!class_exists('O_Integration_Wufoo_Module'))
				throw new O_Integration_Exception('Wufoo integration module not found');
			$this->_set('O_Integration_Wufoo_Module',
				new O_Integration_Wufoo_Module(
					new O_Integration_Wufoo_PostWebHookDataSource(),
					$this->getMapper('WufooIntegration')
				));
		}

		return $this->_get('O_Integration_Wufoo_Module');
	}

	/**
	 * @throws O_Integration_Exception
	 * @param string $name
	 * @return O_Mapper_Abstract
	 */
	public function getMapper($name)
	{
		$className = 'O_Mapper_' . $name;
		if (is_null($this->_get($className))) {
			if (!class_exists($className))
				throw new O_Integration_Exception('Mapper class not found');
			$this->_set($className,
				new $className(
					$this->getDatabaseConnection()
				));
		}

		return $this->_get($className);
	}

	public function getDB()
	{
		return Database::$Interface->MySQLConnection;
	}

	public function getDatabaseConnection()
	{
		if (is_null($this->_get('O_DatabaseConnection'))) {
			$dbConn = new O_DatabaseConnection($this->getDB());
			$this->_set('O_DatabaseConnection', $dbConn);
		}
		return $this->_get('O_DatabaseConnection');
	}
}
