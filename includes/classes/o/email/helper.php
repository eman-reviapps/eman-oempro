<?php
class O_Email_Helper
{
	const EMAIL_TYPE_OPTINCONFIRMATION = 'ETOC';

	/**
	 * Generates a return path string and code for opt-in confirmation email
	 * 
	 * @static
	 * @param integer $subscriberId
	 * @param integer $listId
	 * @param integer $userId
	 * @return array (0=>returnPathString, 1=>returnPathCode)
	 */
	public static function generateReturnPathForOptInConfirmationEmail($subscriberId, $listId, $userId)
	{
		$returnPathCode = self::EMAIL_TYPE_OPTINCONFIRMATION . '-'
				. self::generateReturnPathCode(array($subscriberId, $listId, $userId));
		$returnPath = str_replace(
			array('_INSERT:EMAILADDRESS_', '_INSERT:MAILSERVERDOMAIN_'),
			array($returnPathCode, BOUNCE_CATCHALL_DOMAIN),
			BOUNCE_EMAILADDRESS
		);

		return array($returnPath, $returnPathCode);
	}

	/**
	 * Generates a return path code for given numbers
	 *
	 * @static
	 * @param array $numbers
	 * @return string
	 */
	public static function generateReturnPathCode($numbers)
	{
		$arrayNumbers = array();
		foreach ($numbers as $each) {
			$arrayNumbers[] = Core::CryptNumber($each);
		}
		return implode('-', $arrayNumbers);
	}

	/**
	 * Decodes abuse x message id and returns campaign id, subscriber id,
	 * subscriber email address, list id, user id and auto responder id (in this order)
	 *
	 * @static
	 * @param string $xMessageID Message ID
	 * @return array
	 */
	public static function decodeAbuseXMessageID($xMessageID)
	{
		$decodedMessageID = base64_decode(rawurldecode($xMessageID));
		if (strpos($decodedMessageID, '||||') !== false) {
			return self::decodeOldAbuseXMessageID($xMessageID);
		} else {
			return self::decodeNewAbuseXMessageID($xMessageID);
		}
	}

	/**
	 * Decodes abuse x message id that is generated with Oempro version >= 4.4.0
	 *
	 * @static
	 * @param $xMessageID Message ID
	 * @return array
	 */
	public static function decodeNewAbuseXMessageID($xMessageID)
	{
		$ids = explode('-', $xMessageID);

		if (count($ids) === 5) {
			// v4.4.0 did not have subscriber email address in new abuse x message id
			$campaignID = Core::DecryptNumberAdvanced($ids[0]);
			$subscriberID = Core::DecryptNumberAdvanced($ids[1]);
			$subscriberEmailAddress = '';
			$listID = Core::DecryptNumberAdvanced($ids[2]);
			$userID = Core::DecryptNumberAdvanced($ids[3]);
			$autoResponderID = Core::DecryptNumberAdvanced($ids[4]);

			return array($campaignID, $subscriberID, $subscriberEmailAddress, $listID, $userID, $autoResponderID);
		} else {
			// With v4.4.1 we have also added subscriber email address to the new abuse x message id
			$campaignID = Core::DecryptNumberAdvanced($ids[0]);
			$subscriberID = Core::DecryptNumberAdvanced($ids[1]);
			$subscriberEmailAddress = base64_decode(rawurldecode($ids[2]));
			$listID = Core::DecryptNumberAdvanced($ids[3]);
			$userID = Core::DecryptNumberAdvanced($ids[4]);
			$autoResponderID = Core::DecryptNumberAdvanced($ids[5]);

			return array($campaignID, $subscriberID, $subscriberEmailAddress, $listID, $userID, $autoResponderID);
		}
	}

	/**
	 * Decodes abuse x message id that is generated with Oempro version < 4.4.0
	 *
	 * @static
	 * @param $xMessageID Message ID
	 * @throws O_FBL_Exception
	 * @return array
	 */
	public static function decodeOldAbuseXMessageID($xMessageID)
	{
		$decodedMessageID = base64_decode(rawurldecode($xMessageID));
		$decodedMessageID = explode('||||', $decodedMessageID);

		if (count($decodedMessageID) < 5)
		{
			throw new O_FBL_Exception('Invalid fbl message',
				O_FBL_Exception::INVALID_FBL_MESSAGE);
		}

		$campaignID = $decodedMessageID[0];
		$subscriberID = $decodedMessageID[1];
		$subscriberEmailAddress = $decodedMessageID[2];
		$listID = $decodedMessageID[3];
		$userID = $decodedMessageID[4];
		$autoResponderID = $decodedMessageID[5];

		return array($campaignID, $subscriberID, $subscriberEmailAddress, $listID, $userID, $autoResponderID);
	}
}
