<?php
/**
 * Base email class
 */
class O_Email_Base implements O_Email_Interface
{
	/**
	 * @var bool Wheter image embedding is enabled or disabled, default is FALSE
	 */
	protected $_isImageEmbeddingEnabled = FALSE;

	/**
	 * @var array Contains from name and email address
	 */
	protected $_from = array();

	/**
	 * @var array Contains to name and email address
	 */
	protected $_to = array();

	/**
	 * @var array Contains reply to name and email address
	 */
	protected $_replyTo = array();

	/**
	 * @var string Subject
	 */
	protected $_subject = '';

	/**
	 * @var string Plain content
	 */
	protected $_plainContent = '';

	/**
	 * @var string Html content
	 */
	protected $_htmlContent = '';

	/**
	 * @var array Headers
	 */
	protected $_headers = array();

	/**
	 * @var array Attachments
	 */
	protected $_attachments = array();

	/**
	 * @var array Replacements
	 */
	protected $_replacements = array();

	public function __construct()
	{
		
	}

	/**
	 * Enables image embedding
	 * @return void
	 */
	public function enableImageEmbedding()
	{
		$this->_isImageEmbeddingEnabled = TRUE;
	}

	/**
	 * Disables image embedding
	 * @return void
	 */
	public function disableImageEmbedding()
	{
		$this->_isImageEmbeddingEnabled = FALSE;
	}

	/**
	 * Returns TRUE if image embedding is enabled, FALSE otherwise
	 * @return bool
	 */
	public function isImageEmbeddingEnabled()
	{
		return $this->_isImageEmbeddingEnabled;
	}

	/**
	 * Sets from name and email address of the email
	 * @param string $name
	 * @param string $email
	 * @return void
	 */
	public function setFrom($name, $email)
	{
		$this->_from[0] = $name;
		$this->_from[1] = $email;
	}

	/**
	 * Sets to name and email address of the email
	 * @param string $name To name
	 * @param string $email To email address
	 * @return void
	 */
	public function setTo($name, $email)
	{
		$this->_to[0] = $name;
		$this->_to[1] = $email;
	}

	/**
	 * Sets reply to name and email address of the email
	 * @param string $name Reply to name
	 * @param string $email Reply to  email address
	 * @return void
	 */
	public function setReplyTo($name, $email)
	{
		$this->_replyTo[0] = $name;
		$this->_replyTo[1] = $email;
	}

	/**
	 * Sets subject of the email
	 * @param string $subject Subject
	 * @return void
	 */
	public function setSubject($subject)
	{
		$this->_subject = $subject;
	}

	/**
	 * Sets html content of the email
	 * @param string $content Html content
	 * @return void
	 */
	public function setHtmlContent($content)
	{
		$this->_htmlContent = $content;
	}

	/**
	 * Sets plain content of the email
	 * @param string $content Plain content
	 * @return void
	 */
	public function setPlainContent($content)
	{
		$this->_plainContent = $content;
	}

	/**
	 * Returns from email address
	 * @return string
	 */
	public function getFromEmail()
	{
		return $this->_from[1];
	}

	/**
	 * Returns from name
	 * @return string
	 */
	public function getFromName()
	{
		return $this->_from[0];
	}

	/**
	 * Returns to email address
	 * @return string
	 */
	public function getToEmail()
	{
		return $this->_to[1];
	}

	/**
	 * Returns to name
	 * @return string
	 */
	public function getToName()
	{
		return $this->_to[0];
	}

	/**
	 * Returns reply to name
	 * @return string
	 */
	public function getReplyToName()
	{
		return $this->_replyTo[0];
	}

	/**
	 * Returns reply to email address
	 * @return string
	 */
	public function getReplyToEmail()
	{
		return $this->_replyTo[1];
	}

	/**
	 * Returns subject
	 * @return string
	 */
	public function getSubject()
	{
		return $this->_subject;
	}

	/**
	 * Returns html content
	 * @return string
	 */
	public function getHtmlContent()
	{
		return $this->_htmlContent;
	}

	/**
	 * Returns plain content
	 * @return string
	 */
	public function getPlainContent()
	{
		return $this->_plainContent;
	}

	/**
	 * Adds a custom header to email
	 * @param string $key Header name
	 * @param string $value Header value
	 * @return void
	 */
	public function addHeader($key, $value)
	{
		$this->_headers[$key] = $value;
	}

	/**
	 * Add a string attachment to the email
	 * @param string $data String data to be attached to the email
	 * @param string $fileName Filename of the string data to be attached to the email
	 * @return void
	 */
	public function addStringAttachment($data, $fileName, $mimeType = 'application/octet-stream', $encoding = 'base64')
	{
		$attachment = $this->_getEmptyAttachmentArray();
		$attachment['type'] = 'string';
		$attachment['data'] = $data;
		$attachment['name'] = $fileName;
		$attachment['encoding'] = $encoding;
		$attachment['fileType'] = $mimeType;
		$this->_attachments[] = $attachment;
	}

	/**
	 * Add a string attachment to the email
	 * @param string $data String data to be attached to the email
	 * @param string $fileName Filename of the string data to be attached to the email
	 * @return void
	 */
	public function addInlineAttachment($filePath, $cid, $mimeType = 'application/octet-stream', $encoding = 'base64')
	{
		$attachment = $this->_getEmptyAttachmentArray();
		$attachment['type'] = 'inline';
		$attachment['path'] = $filePath;
		$attachment['cid'] = $cid;
		$attachment['encoding'] = $encoding;
		$attachment['fileType'] = $mimeType;
		$this->_attachments[] = $attachment;
	}

	/**
	 * Add a file attachment to the email
	 * @param string $filePath Path to the attached file
	 * @return void
	 */
	public function addFileAttachment($filePath, $fileName, $mimeType = 'application/octet-stream', $encoding = 'base64')
	{
		$attachment = $this->_getEmptyAttachmentArray();
		$attachment['type'] = 'inline';
		$attachment['path'] = $filePath;
		$attachment['name'] = $fileName;
		$attachment['encoding'] = $encoding;
		$attachment['fileType'] = $mimeType;
		$this->_attachments[] = $attachment;
	}

	protected function _getEmptyAttachmentArray()
	{
		return array(
			'type' => '',
			'data' => '',
			'path' => '',
			'name' => '',
			'cid' => '',
			'encoding' => '',
			'fileType' => ''
		);
	}

	/**
	 * Returns all headers
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->_headers;
	}

	/**
	 * Returns a specific
	 * @return string|false
	 */
	public function getHeader($name)
	{
		if (!array_key_exists($name, $this->_headers))
			return FALSE;
		return $this->_headers[$name];
	}

	/**
	 * Returns all attachment file paths
	 * @return array
	 */
	public function getAttachments()
	{
		return $this->_attachments;
	}

	/**
	 * Returns all replacements
	 * @return array
	 */
	public function getReplacements()
	{
		return $this->_replacements;
	}

	/**
	 * Adds a replacement variable and its value
	 * @param string $searchFor The string to be searched for
	 * @param string $replaceWith The string that will be replaced with
	 * @return void
	 */
	public function addReplacement($searchFor, $replaceWith)
	{
		$this->_replacements[$searchFor] = $replaceWith;
	}
}
