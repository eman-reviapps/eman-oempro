<?php
/**
 * Main interface for all email content providers.
 */
interface O_Email_ContentProvider_Interface
{
	/**
	 * @abstract
	 * @return string Subject
	 */
	public function getSubject();

	/**
	 * @abstract
	 * @return string Html content
	 */
	public function getHtmlContent();

	/**
	 * @abstract
	 * @return string Plain content
	 */
	public function getPlainContent();
}