<?php
/**
 * This content provider gets content from a file. The format for the content
 * file must be as defined below.
 * --
 *
 * [!!--SUBJECT-START--!!]
 * Subject of the email here
 * [!!--SUBJECT-END--!!]
 * 
 * [!!--HTML-CONTENT-START--!!]
 * Html content of the email here
 * [!!--HTML-CONTENT-END--!!]
 *
 * [!!--PLAIN-CONTENT-START--!!]
 * Plain content of the email here
 * [!!--PLAIN-CONTENT-END--!!]
 *
 * --
 */
class O_Email_ContentProvider_File implements O_Email_ContentProvider_Interface
{
	/**
	 * @var string Path to the content file
	 */
	protected $_filePath = '';

	/**
	 * @var bool Whether the content is read from file or not
	 */
	protected $_isContentRead = FALSE;

	/**
	 * @var bool Whether any errors occured during content parsing
	 */
	protected $_anyErrorsDuringContentParsing = FALSE;

	/**
	 * @var string Subject
	 */
	protected $_subject = '';

	/**
	 * @var string Html content
	 */
	protected $_htmlContent = '';

	/**
	 * @var string Plain content
	 */
	protected $_plainContent = '';

	/**
	 * Sets the path of the content file
	 * @param string $filePath Path to the content file
	 * @return bool If file is not found or not readable returns false, otherwise
	 * returns true
	 */
	public function setFile($filePath)
	{
		if (! is_file($filePath) || ! is_readable($filePath))
			return false;

		$this->_filePath = $filePath;
	}

	public function getSubject()
	{
		if (! $this->_isContentRead) $this->_readAndParseContent();
		return $this->_subject;
	}

	public function getHtmlContent()
	{
		if (! $this->_isContentRead) $this->_readAndParseContent();
		return $this->_htmlContent;
	}

	public function getPlainContent()
	{
		if (! $this->_isContentRead) $this->_readAndParseContent();
		return $this->_plainContent;
	}

	public function _readAndParseContent()
	{
		$content = file_get_contents($this->_filePath);

		$subjectRegEx = '/\[\!\!\-\-SUBJECT\-START\-\-\!\!\](.*)\[\!\!\-\-SUBJECT\-END\-\-\!\!\]/isU';
		$htmlContentRegEx = '/\[\!\!\-\-HTML\-CONTENT\-START\-\-\!\!\](.*)\[\!\!\-\-HTML\-CONTENT\-END\-\-\!\!\]/isU';
		$plainContentRegEx = '/\[\!\!\-\-PLAIN\-CONTENT\-START\-\-\!\!\](.*)\[\!\!\-\-PLAIN\-CONTENT\-END\-\-\!\!\]/isU';


		$subjectMatchResult = preg_match_all($subjectRegEx, $content, $arrayMatches, PREG_SET_ORDER);
		if ($subjectMatchResult === 0) $this->_anyErrorsDuringContentParsing = TRUE;
		$this->_subject = str_replace(array("\n", "\r"), '', $arrayMatches[0][1]);

		$htmlContentMatchResult = preg_match_all($htmlContentRegEx, $content, $arrayMatches, PREG_SET_ORDER);
		if ($htmlContentMatchResult === 0) $this->_anyErrorsDuringContentParsing = TRUE;
		$this->_htmlContent = $arrayMatches[0][1];

		$plainContentMatchResult = preg_match_all($plainContentRegEx, $content, $arrayMatches, PREG_SET_ORDER);
		if ($plainContentMatchResult === 0) $this->_anyErrorsDuringContentParsing = TRUE;
		$this->_plainContent = $arrayMatches[0][1];

		$this->_isContentRead = TRUE;
	}
}
