<?php
/**
 * Main interface for all email object converters.
 */
interface O_Email_Converter_Interface
{
	/**
	 * @abstract
	 * @return array array(headers, body)
	 */
	public function convert(O_Email_Base $email);

	/**
	 * @abstract
	 * @param array $headers
	 * @return void
	 */
	public function setIgnoredHeaders($headers);
}