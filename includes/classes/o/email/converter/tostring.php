<?php
/**
 * Converts given email object into string. Handels atachments, image embedding
 * and if necessary caches image embedding for bulk sending.
 *
 * Note: Some of the methods in this class are inspired by or directly copied
 * from PHPMailer class. Thanks to Andy Prevost and other contributers.
 *
 * @author Mert Hurturk
 */
class O_Email_Converter_ToString implements O_Email_Converter_Interface
{
	// CONTENT TYPE CONSTANTS
	const CT_HTML = 'text/html';
	const CT_PLAIN = 'text/plain';
	const CT_BOTH = 'multipart/alternative';

	// MESSAGE TYPE CONSTANTS
	const MT_PLAIN = 'plain';
	const MT_ATTACHMENTS = 'attachments';
	const MT_ALT = 'alt';
	const MT_ALT_ATTACHMENTS = 'alt_attachments';

	/**
	 * @var string Line ending type
	 */
	protected $_lineEnding = "\r\n";

	/**
	 * @var string Email message character set
	 */
	protected $_charSet = 'utf-8';

	/**
	 * @var string Host name used in Message-ID header (if no Message-ID header
	 * exists in email, otherwise this is ignored)
	 */
	protected $_hostName = '';

	/**
	 * @var array Headers in this array will be ignored while converting email
	 * to string. Useful for mail() method which does not need To: and Subject:
	 * headers
	 */
	protected $_ignoredHeaders = array();

	/**
	 * @var string Email message content transfer encoding. Accepted values are
	 * "8bit", "7bit", "binary", "base64", "quoted-printable"
	 */
	protected $_encoding = 'quoted-printable';

	/**
	 * @var bool Whether attachment caching is enabled or not. Useful for
	 * bulk sends. Disabled by default.
	 */
	protected $_isImageEmbedCacheEnabled = FALSE;

	/**
	 * @var string Path to save embedded image files. No trailing slashes.
	 */
	protected $_embeddedImagesSavePath = '';

	/**
	 * @var array All image files saved during image embedding. We store these
	 * because when convert process is completed and if attachment caching is
	 * disabled, those files will be deleted. If attachment caching is enabled,
	 * you should manually call deleteSavedEmbedImageFiles() method.
	 */
	protected $_savedEmbedImageFiles = array();

	/**
	 * @var array Embed image cache. Used only when attachment caching is enabled.
	 */
	protected $_cachedEmbedImageFiles = array();

	/**
	 * @var array Sources that will be ignored while embedding images
	 */
	protected $_imageSrcThatWillBeIgnoredWhenEmbedding = array();

	public function __construct()
	{
		$this->setEmbeddedImagesSavePath(DATA_PATH . 'tmp/');
	}

	/**
	 * Sets email content encoding
	 * @param string $encoding Email message content transfer encoding. Accepted values are "8bit", "7bit", "binary", "base64", "quoted-printable"
	 */
	public function setEncoding($encoding)
	{
		$valid_values = array('8bit', '7bit', 'binary', 'base64', 'quoted-printable');
		if (! in_array($encoding, $valid_values))
			return;

		$this->_encoding = $encoding;
	}

	/**
	 * Sets sources that will be ignored while embedding images
	 * @param array $sources
	 * @return void
	 */
	public function setIgnoredImageSourcesForEmbedding($sources)
	{
		$this->_imageSrcThatWillBeIgnoredWhenEmbedding = $sources;
	}

	/**
	 * Sets the path to save the embedded image files.
	 * @param string $path Path. No trailing slashes
	 * @return void
	 */
	public function setEmbeddedImagesSavePath($path)
	{
		$this->_embeddedImagesSavePath = rtrim($path, DIRECTORY_SEPARATOR);
	}

	/**
	 * Enables attachment caching which makes attachment and image embedding
	 * only once, useful for bulk sends.
	 * @return void
	 */
	public function enableImageEmbedCache()
	{
		$this->_isImageEmbedCacheEnabled = TRUE;
	}

	/**
	 * Disables attachment caching which makes attachment and image embedding
	 * every time email is converted to string.
	 * @return void
	 */
	public function disableAttachmentCaching()
	{
		$this->_isImageEmbedCacheEnabled = FALSE;
	}

	/**
	 * The headers set with this method will be ignored while converting
	 * the email to string
	 * @param array $headers
	 * @return void
	 */
	public function setIgnoredHeaders($headers)
	{
		$this->_ignoredHeaders = $headers;
	}

	/**
	 * Converts email object into an string email source text
	 * @return array array(headers, body)
	 */
	public function convert(O_Email_Base $email)
	{
		$uniqueIdForBoundary = md5(uniqid(time()));
		$boundaryOne = 'b1_' . $uniqueIdForBoundary;
		$boundaryTwo = 'b2_' . $uniqueIdForBoundary;

		$contentType = $this->_getContentType($email);
		$this->_convertImagesToInlineAttachments($contentType, $email);
		$messageType = $this->_getMessageType($contentType, $email);

		$header = $this->_getHeader($contentType, $messageType, $boundaryOne, $email);
		$body = $this->_getBody($contentType, $messageType, $boundaryOne, $boundaryTwo, $email);

		$this->cleanUp();

		return array($header, $body);
	}

	/**
	 * Cleans up embed image cache and removes embed image temporary files
	 * from file system
	 * @return void
	 */
	public function cleanUp()
	{
		if (!$this->_isImageEmbedCacheEnabled) {
			$this->_removeSavedEmbedImageFiles();
			$this->_emptyEmbedImageCache();
		}
	}


	/**
	 * Returns body part of the email message as string
	 * @param string $contentType
	 * @param string $messageType
	 * @param string $boundary
	 * @param O_Email_Base $email
	 * @return string
	 */
	protected function _getBody($contentType, $messageType, $boundaryOne, $boundaryTwo, O_Email_Base $email)
	{
		$body = '';

		if ($messageType == self::MT_ALT) {
			$body .= $this->_getStartBoundaryText(
				$boundaryOne, $this->_charSet, 'text/plain', $this->_encoding);
			$body .= $this->_encodeString($email->getPlainContent(), $this->_encoding);
			$body .= str_repeat($this->_lineEnding, 2);
			$body .= $this->_getStartBoundaryText(
				$boundaryOne, $this->_charSet, 'text/html', $this->_encoding);
			$body .= $this->_encodeString($email->getHtmlContent(), $this->_encoding);
			$body .= str_repeat($this->_lineEnding, 2);
			$body .= $this->_getEndBoundaryText($boundaryOne);
		} else if ($messageType == self::MT_PLAIN) {
			if ($contentType == self::CT_HTML) {
				$body .= $this->_encodeString($email->getHtmlContent(), $this->_encoding);
			} else if ($contentType == self::CT_PLAIN) {
				$body .= $this->_encodeString($email->getPlainContent(), $this->_encoding);
			}
		} else if ($messageType == self::MT_ATTACHMENTS) {
			$body .= $this->_getStartBoundaryText(
				$boundaryOne, $this->_charSet, $contentType, $this->_encoding);
			$body .= $this->_encodeString(
				$contentType == self::CT_HTML
						? $email->getHtmlContent() : $email->getPlainContent(),
				$this->_encoding
			);
			$body .= $this->_lineEnding;
			$body .= $this->_attachAll($boundaryOne, $email);
		} else if ($messageType == self::MT_ALT_ATTACHMENTS) {
			$body .= sprintf("--%s%s", $boundaryOne, $this->_lineEnding);
			$body .= sprintf(
				"Content-Type: %s;%s" . "\tboundary=\"%s\"%s",
				'multipart/alternative',
				$this->_lineEnding,
				$boundaryTwo,
				$this->_lineEnding . $this->_lineEnding
			);
			$body .= $this->_getStartBoundaryText(
				$boundaryTwo, $this->_charSet, 'text/plain', $this->_encoding);
			$body .= $this->_lineEnding;
			$body .= $this->_encodeString($email->getPlainContent(), $this->_encoding);
			$body .= str_repeat($this->_lineEnding, 2);
			$body .= $this->_getStartBoundaryText(
				$boundaryTwo, $this->_charSet, 'text/html', $this->_encoding);
			$body .= $this->_lineEnding;
			$body .= $this->_encodeString($email->getHtmlContent(), $this->_encoding);
			$body .= str_repeat($this->_lineEnding, 2);
			$body .= $this->_getEndBoundaryText($boundaryTwo);
			$body .= $this->_attachAll($boundaryOne, $email);
		}

		return $body;
	}

	/**
	 * Encodes string to requested format.
	 * @param string $str
	 * @param string $encoding
	 * @return string
	 */
	protected function _encodeString($str, $encoding = 'base64')
	{
		$encoded = '';
		switch (strtolower($encoding)) {
			case 'base64':
				$encoded = chunk_split(base64_encode($str), 76, $this->_lineEnding);
				break;
			case '7bit':
			case '8bit':
				$encoded = $this->_fixEOL($str);
				if (substr($encoded, -(strlen($this->_lineEnding))) != $this->_lineEnding)
					$encoded .= $this->_lineEnding;
				break;
			case 'binary':
				$encoded = $str;
				break;
			case 'quoted-printable':
				$encoded = $this->_encodeQP($str);
				break;
		}
		return $encoded;
	}

	/**
	 * Encode string to quoted-printable
	 * @param string $input
	 * @param int $line_max
	 * @param bool $space_conv
	 * @return string
	 */
	protected function _encodeQP($input = '', $line_max = 76, $space_conv = false)
	{
		// Use the faster native encoding if PHP version is or bigger then 5.3.0
		if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
			return quoted_printable_encode($input);
		}

		$hex = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
		$lines = preg_split('/(?:\r\n|\r|\n)/', $input);


		// According to the RFC822, RFC2822 and RFC5322, line endings in a
		// message should be CRLF in a quoted-pritable encoded email message,
		// but that causes some major problems with Hotmail. It seems that
		// Hotmail can not decode a quoted-printable encoded email message
		// with CRLF line endings and corrupts the message source. So we are
		// using LF here.
		$eol = "\n";

		$escape = '=';
		$output = '';
		while (list(, $line) = each($lines)) {
			$linlen = strlen($line);
			$newline = '';
			for ($i = 0; $i < $linlen; $i++) {
				$c = substr($line, $i, 1);
				$dec = ord($c);
				if (($i == 0) && ($dec == 46)) { // convert first point in the line into =2E
					$c = '=2E';
				}
				if ($dec == 32) {
					if ($i == ($linlen - 1)) { // convert space at eol only
						$c = '=20';
					} else if ($space_conv) {
						$c = '=20';
					}
				} elseif (($dec == 61) || ($dec < 32) || ($dec > 126)) { // always encode "\t", which is *not* required
					$h2 = floor($dec / 16);
					$h1 = floor($dec % 16);
					$c = $escape . $hex[$h2] . $hex[$h1];
				}
				if ((strlen($newline) + strlen($c)) >= $line_max) { // CRLF is not counted
					$output .= $newline . $escape . $eol; //  soft line break; " =\r\n" is okay
					$newline = '';
					// check if newline first character will be point or not
					if ($dec == 46) {
						$c = '=2E';
					}
				}
				$newline .= $c;
			} // end of for
			$output .= $newline . $eol;
		} // end of while
		return trim($output);
	}

	/**
	 * Returns boundary start text
	 * @param string $boundary
	 * @param string $charSet
	 * @param string $contentType
	 * @param string $encoding
	 * @return string
	 */
	protected function _getStartBoundaryText($boundary, $charSet, $contentType, $encoding)
	{
		$text = '';
		$text .= $this->_textLine('--' . $boundary);
		$text .= sprintf("Content-Type: %s; charset = \"%s\"", $contentType, $charSet);
		$text .= $this->_lineEnding;
		$text .= $this->_headerLine('Content-Transfer-Encoding', $encoding);
		$text .= $this->_lineEnding;
		return $text;
	}

	/**
	 * Returns boundary end text
	 * @param string $boundary
	 * @return string
	 */
	protected function _getEndBoundaryText($boundary)
	{
		return $this->_lineEnding . '--' . $boundary . '--' . $this->_lineEnding;
	}


	/**
	 * Returns content type of the email
	 * @param O_Email_Base $email
	 * @return string
	 */
	protected function _getContentType(O_Email_Base $email)
	{
		$isHTMLemail = $email->getHtmlContent() != ''
				&& $email->getPlainContent() == '';

		$isPLAINemail = $email->getHtmlContent() == ''
				&& $email->getPlainContent() != '';

		$isBOTHemail = $email->getHtmlContent() != ''
				&& $email->getPlainContent() != '';

		$contentType = '';

		if ($isHTMLemail)
			$contentType = self::CT_HTML;

		if ($isPLAINemail)
			$contentType = self::CT_PLAIN;

		if ($isBOTHemail)
			$contentType = self::CT_BOTH;

		return $contentType;
	}

	/**
	 * Returns message type
	 * @param string $contentType
	 * @param O_Email_Base $email
	 * @return string
	 */
	protected function _getMessageType($contentType, O_Email_Base $email)
	{
		$messageType = '';

		$mailHasAttachments = count($email->getAttachments()) > 0;
		if (!$mailHasAttachments && $contentType != self::CT_BOTH) {
			$messageType = self::MT_PLAIN;
		} else {
			if ($mailHasAttachments) {
				$messageType = self::MT_ATTACHMENTS;
			}
			if ($contentType == self::CT_BOTH && !$mailHasAttachments) {
				$messageType = self::MT_ALT;
			}
			if ($contentType == self::CT_BOTH && $mailHasAttachments) {
				$messageType = self::MT_ALT_ATTACHMENTS;
			}
		}

		return $messageType;
	}

	/**
	 * Returns header part of the email message as string
	 * @param string $contentType
	 * @param string $messageType
	 * @param string $boundary
	 * @param O_Email_Base $email
	 * @return string
	 */
	protected function _getHeader($contentType, $messageType, $boundary, O_Email_Base $email)
	{
		$header = '';

		// Date: header
		$header .= $this->_headerLine('Date', $this->_getRFC822dateString());

		// Return-Path: header
		if (!$this->_isHeaderToBeIgnored('Return-Path')) {
			$header .= $this->_headerLine(
				'Return-Path',
				!$email->getHeader('Return-Path')
						? $email->getFromEmail()
						: $email->getHeader('Return-Path')
			);
		}

		// To: header
		if (!$this->_isHeaderToBeIgnored('To')) {
			$header .= $this->_addressHeaderLine(
				'To', $email->getToName(), $email->getToEmail());
		}

		// From: header
		$header .= $this->_addressHeaderLine(
			'From', $email->getFromName(), $email->getFromEmail());

		// Reply-to: header
		$header .= $this->_addressHeaderLine(
			'Reply-to', $email->getReplyToName(), $email->getReplyToEmail());

		// Subject: header
		if (!$this->_isHeaderToBeIgnored('Subject')) {
			$header .= $this->_headerLine(
				'Subject',
				$this->_encodeHeaderValue(
					$this->_secureHeaderValue($email->getSubject())
				),
				TRUE
			);
		}

		// Message-ID: header
		$header .= $this->_headerLine(
			'Message-ID',
			!$email->getHeader('Message-ID')
					? $this->_generateUniqueMessageID()
					: $email->getHeader('Message-ID')
		);

		// Custom headers
		// To prevent forging duplicate headers, use an ignore list for
		// the headers set above.
		$customHeadersIgnoreList = array(
			'date', 'return-path', 'to', 'from', 'reply-to', 'subject',
			'message-id', 'mime-version');
		foreach ($email->getHeaders() as $name => $value) {
			if (in_array(strtolower($name), $customHeadersIgnoreList))
				continue;
			$header .= $this->_headerLine($name, $value);
		}

		// Mime-Version: header
		$header .= $this->_headerLine('MIME-Version', '1.0');

		// MIME
		$header .= $this->_getMailMIME($contentType, $messageType, $email, $boundary);

		return $header;
	}

	/**
	 * Return MIME part text
	 * @param string $contentType
	 * @param string $messageType
	 * @param O_Email_Base $email
	 * @param string $boundary
	 * @return string
	 */
	protected function _getMailMIME($contentType, $messageType, O_Email_Base $email, $boundary)
	{
		$MIME = '';

		switch ($messageType) {
			case self::MT_PLAIN:
				$MIME .= $this->_headerLine('Content-Transfer-Encoding', $this->_encoding);
				$MIME .= sprintf("Content-Type: %s; charset=\"%s\"", $contentType, $this->_charSet);
				break;
			case self::MT_ATTACHMENTS:
				/* fall through */
			case self::MT_ALT_ATTACHMENTS:
				if ($this->_inlineImageExists($email)) {
					$MIME .= sprintf(
//						"Content-Type: %s;%s\ttype=\"text/html\";%s\tboundary=\"%s\"%s", // This is removed because type="text/html" is unnecessary
						"Content-Type: %s;%s\tboundary=\"%s\"%s",
						'multipart/related',
//						$this->_lineEnding,
						$this->_lineEnding,
						$boundary,
						$this->_lineEnding
					);
				} else {
					$MIME .= $this->_headerLine('Content-Type', 'multipart/mixed;');
					$MIME .= $this->_textLine("\tboundary=\"" . $boundary . '"');
				}
				break;
			case self::MT_ALT:
				$MIME .= $this->_headerLine('Content-Type', 'multipart/alternative;');
				$MIME .= $this->_textLine("\tboundary=\"" . $boundary . '"');
				break;
		}

		$MIME .= str_repeat($this->_lineEnding, 2);

		return $MIME;
	}

	/**
	 * Returns true if an inline attachment is present
	 * @return bool
	 */
	protected function _inlineImageExists(O_Email_Base $email)
	{
		foreach ($email->getAttachments() as $eachAttachment) {
			if ($eachAttachment['type'] == 'inline')
				return TRUE;
		}
		return FALSE;
	}

	/**
	 * Returns 32bit unique ID string
	 * @return string
	 */
	protected function _generateUniqueMessageID()
	{
		return '<' . md5(uniqid(time())) . '@' . $this->_getServerHostName() . '>';
	}

	/**
	 * Sets hostname for Message-ID header
	 * @param string $hostname 
	 * @return void
	 */
	public function setServerHostName($hostname)
	{
		$this->_hostName = $hostname;
	}

	/**
	 * Returns server host name
	 * @return string
	 */
	protected function _getServerHostName()
	{
		if (!empty($this->_hostName)) {
			$result = $this->_hostName;
		} elseif (isset($_SERVER['SERVER_NAME'])) {
			$result = $_SERVER['SERVER_NAME'];
		} else {
			$result = "localhost.localdomain";
		}

		return $result;
	}

	/**
	 * Checks if given header should be ignored or not
	 * @param string $name
	 * @return bool
	 */
	protected function _isHeaderToBeIgnored($name)
	{
		if (in_array($name, $this->_ignoredHeaders))
			return TRUE;
		return FALSE;
	}

	/**
	 * Returns a formatted header line with line ending at the end
	 * @param string $name
	 * @param string $value
	 * @return string
	 */
	protected function _headerLine($name, $value, $ignoreSecuring = FALSE)
	{
		return $name . ': '
				. ($ignoreSecuring ? $value : $this->_secureHeaderValue($value))
				. $this->_lineEnding;
	}

	/**
	 * Returns a text line with line ending at the end
	 * @param string $value
	 * @return string
	 */
	protected function _textLine($value)
	{
		return $value . $this->_lineEnding;
	}

	/**
	 * Returns a formatted address header line with line ending at the end
	 * @param string $name
	 * @param string $email
	 * @return string
	 */
	protected function _addressHeaderLine($headerName, $name, $email)
	{
		$header = $headerName . ': ';

		if ($name != '') {
			$header .= $this->_encodeHeaderValue($this->_secureHeaderValue($name)) . ' '
					. '<' . $this->_secureHeaderValue($email) . '>';
		} else {
			$header .= $this->_secureHeaderValue($email);
		}

		$header .= $this->_lineEnding;

		return $header;
	}

	/**
	 * Returns an RFC 822 formatted date string
	 * @return string
	 */
	protected function _getRFC822dateString()
	{
		return date('r');
	}

	/**
	 * Returns a secured header value
	 * @param string $value
	 * @return string
	 */
	protected function _secureHeaderValue($value)
	{
		return str_replace(array("\r", "\n"), "", trim($value));
	}

	/**
	 * Returns an encoded header value
	 * @param string $value
	 * @param string $position
	 * @return mixed|string
	 */
	protected function _encodeHeaderValue($value, $position = 'text')
	{
		$x = 0;

		switch (strtolower($position)) {
			case 'phrase':
				if (!preg_match('/[\200-\377]/', $value)) {
					/* Can't use addslashes as we don't know what value has magic_quotes_sybase. */
					$encoded = addcslashes($value, "\0..\37\177\\\"");
					if (($value == $encoded) && !preg_match('/[^A-Za-z0-9!#$%&\'*+\/=?^_`{|}~ -]/', $value)) {
						return $encoded;
					} else {
						return '"' . $encoded . '"';
					}
				}
				$x = preg_match_all('/[^\040\041\043-\133\135-\176]/', $value, $matches);
				break;
			case 'comment':
				$x = preg_match_all('/[()"]/', $value, $matches);
			/* Fall-through */
			case 'text':
			default:
				$x += preg_match_all('/[\000-\010\013\014\016-\037\177-\377]/', $value, $matches);
				break;
		}

		if ($x == 0) {
			return $value;
		}

		$maxLen = 75 - 7 - strlen($this->_charSet);

		/* Try to select the encoding which should produce the shortest output */
		if (strlen($value) / 3 < $x) {
			$encoding = 'B';
			if (function_exists('mb_strlen') && $this->_strHasMultiByteChars($value)) {
				$encoded = $this->_base64EncodeAndWrapMultiByteString($value);
			} else {
				$encoded = base64_encode($value);
				$maxLen -= $maxLen % 4;
				$encoded = trim(chunk_split($encoded, $maxLen, "\n"));
			}
		} else {
			$encoding = 'Q';
			$encoded = $this->_encodeQ($value, $position);
			$encoded = $this->_wrapText($encoded, $maxLen, true);
			$encoded = str_replace('=' . $this->_lineEnding, "\n", trim($encoded));
		}

		$encoded = preg_replace('/^(.*)$/m', " =?" . $this->_charSet . "?$encoding?\\1?=", $encoded);
		$encoded = trim(str_replace("\n", $this->_lineEnding, $encoded));

		return $encoded;
	}

	/**
	 * Checks if a string contains multi-byte characters
	 * @param string $str
	 * @return bool
	 */
	protected function _strHasMultiByteChars($str)
	{
		if (function_exists('mb_strlen')) {
			return (strlen($str) > mb_strlen($str, $this->_charSet));
		} else {
			return FALSE;
		}
	}

	/**
	 * Correctly encodes and wraps long multi-byte strings for mail headers
	 * without breaking lines within a character.
	 * @param string $str
	 * @return string
	 */
	protected function _base64EncodeAndWrapMultiByteString($str)
	{
		$start = "=?" . $this->_charSet . "?B?";
		$end = "?=";
		$encoded = "";

		$mb_length = mb_strlen($str, $this->_charSet);
		// Each line must have length <= 75, including $start and $end
		$length = 75 - strlen($start) - strlen($end);
		// Average multi-byte ratio
		$ratio = $mb_length / strlen($str);
		// Base64 has a 4:3 ratio
		$offset = $avgLength = floor($length * $ratio * .75);

		for ($i = 0; $i < $mb_length; $i += $offset) {
			$lookBack = 0;

			do {
				$offset = $avgLength - $lookBack;
				$chunk = mb_substr($str, $i, $offset, $this->_charSet);
				$chunk = base64_encode($chunk);
				$lookBack++;
			}
			while (strlen($chunk) > $length);

			$encoded .= $chunk . $this->_lineEnding;
		}

		// Chomp the last linefeed
		$encoded = substr($encoded, 0, -strlen($this->_lineEnding));

		unset($offset);
		return $encoded;
	}

	/**
	 * Encode string to q encoding
	 * @param string $str
	 * @param string $position
	 * @return string
	 */
	protected function _encodeQ($str, $position = 'text')
	{
		/* There should not be any EOL in the string */
		$encoded = preg_replace("[\r\n]", '', $str);

		switch (strtolower($position)) {
			case 'phrase':
				$encoded = preg_replace("/([^A-Za-z0-9!*+\/ -])/e", "'='.sprintf('%02X', ord('\\1'))", $encoded);
				break;
			case 'comment':
				$encoded = preg_replace("/([\(\)\"])/e", "'='.sprintf('%02X', ord('\\1'))", $encoded);
			case 'text':
			default:
				/* Replace every high ascii, control =, ? and _ characters */
				$encoded = preg_replace('/([\000-\011\013\014\016-\037\075\077\137\177-\377])/e',
					"'='.sprintf('%02X', ord('\\1'))", $encoded);
				break;
		}

		/* Replace every spaces to _ (more readable than =20) */
		$encoded = str_replace(' ', '_', $encoded);

		return $encoded;
	}


	/**
	 * Wraps message for use with mailers that do not
	 * automatically perform wrapping and for quoted-printable.
	 * Original written by philippe.
	 * @access public
	 * @return string
	 */
	protected function _wrapText($message, $length, $qp_mode = false)
	{
		$soft_break = ($qp_mode) ? sprintf(" =%s", $this->_lineEnding) : $this->_lineEnding;
		// If utf-8 encoding is used, we will need to make sure we don't
		// split multibyte characters when we wrap
		$is_utf8 = (strtolower($this->_charSet) == "utf-8");

		$message = $this->_fixEOL($message);
		if (substr($message, -1) == $this->_lineEnding) {
			$message = substr($message, 0, -1);
		}

		$line = explode($this->_lineEnding, $message);
		$message = '';
		for ($i = 0; $i < count($line); $i++) {
			$line_part = explode(' ', $line[$i]);
			$buf = '';
			for ($e = 0; $e < count($line_part); $e++) {
				$word = $line_part[$e];
				if ($qp_mode and (strlen($word) > $length)) {
					$space_left = $length - strlen($buf) - 1;
					if ($e != 0) {
						if ($space_left > 20) {
							$len = $space_left;
							if ($is_utf8) {
								$len = $this->_UTF8charBoundary($word, $len);
							} elseif (substr($word, $len - 1, 1) == "=") {
								$len--;
							} elseif (substr($word, $len - 2, 1) == "=") {
								$len -= 2;
							}
							$part = substr($word, 0, $len);
							$word = substr($word, $len);
							$buf .= ' ' . $part;
							$message .= $buf . sprintf("=%s", $this->_lineEnding);
						} else {
							$message .= $buf . $soft_break;
						}
						$buf = '';
					}
					while (strlen($word) > 0) {
						$len = $length;
						if ($is_utf8) {
							$len = $this->_UTF8charBoundary($word, $len);
						} elseif (substr($word, $len - 1, 1) == "=") {
							$len--;
						} elseif (substr($word, $len - 2, 1) == "=") {
							$len -= 2;
						}
						$part = substr($word, 0, $len);
						$word = substr($word, $len);

						if (strlen($word) > 0) {
							$message .= $part . sprintf("=%s", $this->_lineEnding);
						} else {
							$buf = $part;
						}
					}
				} else {
					$buf_o = $buf;
					$buf .= ($e == 0) ? $word : (' ' . $word);

					if (strlen($buf) > $length and $buf_o != '') {
						$message .= $buf_o . $soft_break;
						$buf = $word;
					}
				}
			}
			$message .= $buf . $this->_lineEnding;
		}

		return $message;
	}

	/**
	 * Changes every end of line from CR or LF to set line ending
	 * @param string $str
	 * @return string
	 */
	protected function _fixEOL($str)
	{
		$str = str_replace("\r\n", "\n", $str);
		$str = str_replace("\r", "\n", $str);
		$str = str_replace("\n", $this->_lineEnding, $str);
		return $str;
	}

	/**
	 * Finds last character boundary prior to maxLength in a utf-8
	 * quoted (printable) encoded string.
	 * Original written by Colin Brown.
	 * @param string $encodedText utf-8 QP text
	 * @param string $maxLength find last character boundary prior to this length
	 * @return int
	 */
	protected function _UTF8charBoundary($encodedText, $maxLength)
	{
		$foundSplitPos = false;
		$lookBack = 3;
		while (!$foundSplitPos) {
			$lastChunk = substr($encodedText, $maxLength - $lookBack, $lookBack);
			$encodedCharPos = strpos($lastChunk, "=");
			if ($encodedCharPos !== false) {
				// Found start of encoded character byte within $lookBack block.
				// Check the encoded byte value (the 2 chars after the '=')
				$hex = substr($encodedText, $maxLength - $lookBack + $encodedCharPos + 1, 2);
				$dec = hexdec($hex);
				if ($dec < 128) { // Single byte character.
					// If the encoded char was found at pos 0, it will fit
					// otherwise reduce maxLength to start of the encoded char
					$maxLength = ($encodedCharPos == 0) ? $maxLength :
							$maxLength - ($lookBack - $encodedCharPos);
					$foundSplitPos = true;
				} elseif ($dec >= 192) { // First byte of a multi byte character
					// Reduce maxLength to split at start of character
					$maxLength = $maxLength - ($lookBack - $encodedCharPos);
					$foundSplitPos = true;
				} elseif ($dec < 192) { // Middle byte of a multi byte character, look further back
					$lookBack += 3;
				}
			} else {
				// No encoded character found
				$foundSplitPos = true;
			}
		}
		return $maxLength;
	}

	protected function _convertImagesToInlineAttachments($contentType, O_Email_Base $email)
	{
		if (!$email->isImageEmbeddingEnabled() || $contentType == self::CT_PLAIN)
			return;

		$isEmbedImagesCached = $this->_isImageEmbedCacheEnabled
				&& count($this->_cachedEmbedImageFiles) > 0;

		$htmlContent = $email->getHtmlContent();

		if ($isEmbedImagesCached) {
			foreach ($this->_cachedEmbedImageFiles as $imageUrl => $data) {
				$email->addInlineAttachment(
					$data['fileName'],
					$data['cid'],
					$data['mime']
				);

				$htmlContent = preg_replace(
					"/('|\")" . preg_quote($imageUrl, '/') . "\\1/uUsm",
						'$1cid:' . $data['cid'] . '$1', $htmlContent
				);
			}
		} else {
			$imageURLs = $this->_getImageURLsFromHTMLContentForEmbedding(
				$email->getHtmlContent(), $this->_imageSrcThatWillBeIgnoredWhenEmbedding);

			if (count($imageURLs) < 1)
				return;

			foreach ($imageURLs as $eachImageURL) {
				$cid = md5($eachImageURL);
				$imageContent = $this->_getImageContent($eachImageURL);
				if ($imageContent === FALSE)
					continue;

				$temporaryImageFileName = $this->_embeddedImagesSavePath
						. DIRECTORY_SEPARATOR . $cid;
				$this->_saveImageContentToFile($imageContent, $temporaryImageFileName);

				$this->_savedEmbedImageFiles[] = $temporaryImageFileName;

				$imageInfo = getimagesize($temporaryImageFileName);
				$email->addInlineAttachment(
					$temporaryImageFileName, $cid, $imageInfo['mime']);

				if ($this->_isImageEmbedCacheEnabled) {
					$this->_cachedEmbedImageFiles[$eachImageURL] = array(
						'fileName' => $temporaryImageFileName,
						'cid' => $cid,
						'mime' => $imageInfo['mime']
					);
				}

				$htmlContent = preg_replace(
					"/('|\")" . preg_quote($eachImageURL, '/') . "\\1/uUsm",
						'$1cid:' . $cid . '$1', $htmlContent
				);
			}
		}
		$email->setHtmlContent($htmlContent);
	}

	protected function _saveImageContentToFile($imageContent, $savePath)
	{
		file_put_contents($savePath, $imageContent);
		unset($imageContent);
	}

	protected function _getImageContent($imageUrl)
	{
		//TODO: Calling a static method from an outside class. Needs refactoring.
		$RemoteContent = Core::DataPostToRemoteURL(
			$imageUrl, array(), 'GET', false, '', '', 60, false);

		if ($RemoteContent[0] == false) {
			return false;
		} else {
			return $RemoteContent[1];
		}
	}

	/**
	 * Returns image urls in given content
	 * @param string $content
	 * @param array $excludedURLs
	 * @return array
	 */
	protected function _getImageURLsFromHTMLContentForEmbedding($content, $excludedURLs)
	{
		$foundURLs = array();

		// find urls in src attributes
		$result = preg_match_all("/src=('|\")(.*)\\1([^\/>]*?)/uUim", $content, $matches, PREG_SET_ORDER);
		if ($result > 0) {
			foreach ($matches as $eachMatch) {
				if (in_array($eachMatch[2], $excludedURLs))
					continue;

				if (isset($eachMatch[3]) && strpos($eachMatch[3], 'no-embed=1') !== FALSE)
					continue;

				if (isset($eachMatch[3]) && strpos($eachMatch[3], 'no-embed="1"') !== FALSE)
					continue;

				$foundURLs[$eachMatch[2]] = $eachMatch[2];
			}
		}

		// find urls in style attributes
		$result = preg_match_all("/style=(\"|')(?:background\\-image\\:)(?:url)?\\((\"|')(.*)\\2\\).*\\1/uiUm", $content, $matches, PREG_SET_ORDER);
		if ($result > 0) {
			foreach ($matches as $eachMatch) {
				if (in_array($eachMatch[3], $excludedURLs))
					continue;

				if (strpos($eachMatch[3], 'no-embed=1') !== FALSE)
					continue;

				$foundURLs[$eachMatch[3]] = $eachMatch[3];
			}
		}

		return $foundURLs;
	}

	/**
	 * Removes saved embed image files from file system
	 * @return void
	 */
	protected function _removeSavedEmbedImageFiles()
	{
		if (count($this->_savedEmbedImageFiles) > 0) {
			foreach ($this->_savedEmbedImageFiles as $eachFile) {
				unlink($eachFile);
			}
		}
		$this->_savedEmbedImageFiles = array();
	}

	/**
	 * Empties embed image cache
	 * @return void
	 */
	protected function _emptyEmbedImageCache()
	{
		$this->_cachedEmbedImageFiles = array();
	}

	/**
	 * Attaches all attachments into the source of the message
	 * @param string $boundary
	 * @param O_Email_Base $email
	 * @return string
	 */
	protected function _attachAll($boundary, O_Email_Base $email)
	{
		$mime = array();

		foreach ($email->getAttachments() as $eachAttachment) {
			$mime[] = sprintf("--%s%s", $boundary, $this->_lineEnding);
			$mime[] = sprintf(
				"Content-Type: %s; name=\"%s\"%s",
				$eachAttachment['fileType'],
				$this->_encodeHeaderValue($this->_secureHeaderValue($eachAttachment['name'])),
				$this->_lineEnding
			);
			$mime[] = sprintf("Content-Transfer-Encoding: %s%s",
				$eachAttachment['encoding'], $this->_lineEnding);

			if ($eachAttachment['type'] == 'inline')
				$mime[] = sprintf("Content-ID: <%s>%s",
					$eachAttachment['cid'], $this->_lineEnding);

			$mime[] = sprintf("Content-Disposition: %s; filename=\"%s\"%s",
				$eachAttachment['type'] == 'inline' ? 'inline' : 'attachment',
				$this->_encodeHeaderValue($this->_secureHeaderValue($eachAttachment['name'])),
					$this->_lineEnding . $this->_lineEnding
			);

			$mime[] = $eachAttachment['type'] == 'string'
					? $this->_encodeString($eachAttachment['data'], $eachAttachment['encoding'])
					: $this->_encodeFile($eachAttachment['path'], $eachAttachment['encoding']);

			$mime[] = str_repeat($this->_lineEnding, 2);
		}

		$mime[] = sprintf("--%s--%s", $boundary, $this->_lineEnding);
		return join('', $mime);
	}

	/**
	 * Encodes attachment in requested format
	 * @param string $path
	 * @param string $encoding
	 * @return string
	 */
	protected function _encodeFile($path, $encoding = 'base64')
	{
		$file_buffer = file_get_contents($path);
		$file_buffer = $this->_encodeString($file_buffer, $encoding);
		return $file_buffer;
	}
}
