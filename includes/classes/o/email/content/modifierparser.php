<?php
/**
 * Parses modifier commands in email content
 * --
 * [TITLE:mErT hURTurk] -> Mert Hurturk
 * [UPPER:mert hurturk] -> MERT HURTURK
 * [LOWER:MERT HURTURK] -> mert hurturk
 */
class O_Email_Content_ModifierParser
{
	/**
	 * Parses the content for modifiers and evaluates them.
	 *
	 * @param string $content Content to be parsed
	 * @return string
	 * @author Mert Hurturk
	 */
	public function parse($content)
	{
		$pattern = "/\[(?P<modifier>TITLE|UPPER|LOWER)\:(?P<subject>.*)\]/Us";
		return preg_replace_callback($pattern, array($this, 'evaluate'), $content);
	}

	/**
	 * Evalutates modifier matches in an email content and returns result
	 *
	 * @param mixed $matches
	 * @return string
	 * @author Mert Hurturk
	 */
	public function evaluate($matches)
	{
		if (! is_array($matches))
			return $matches;

		$available_modifiers = array(
			'TITLE' => '_modifier_title',
			'UPPER' => '_modifier_upper',
			'LOWER' => '_modifier_lower'
		);

		if (! in_array($matches['modifier'], array_keys($available_modifiers)))
			return $matches[0];

		return call_user_func(array($this, $available_modifiers[$matches['modifier']]), $matches['subject']);
	}

	/**
	 * Converts a string to title case
	 *
	 * @param string $subject
	 * @return string
	 * @author Mert Hurturk
	 */
	protected function _modifier_title($subject)
	{
		if (function_exists('mb_convert_case')) {
			return mb_convert_case($subject, MB_CASE_TITLE, 'UTF-8');
		} else {
			return ucwords(strtolower($subject));
		}
	}

	/**
	 * Converts a string to upper case
	 *
	 * @param string $subject
	 * @return string
	 * @author Mert Hurturk
	 */
	protected function _modifier_upper($subject)
	{
		if (function_exists('mb_convert_case')) {
			return mb_convert_case($subject, MB_CASE_UPPER, 'UTF-8');
		} else {
			return strtoupper($subject);
		}
	}

	/**
	 * Converts a string to lower case
	 *
	 * @param string $subject
	 * @return string
	 * @author Mert Hurturk
	 */
	protected function _modifier_lower($subject)
	{
		if (function_exists('mb_convert_case')) {
			return mb_convert_case($subject, MB_CASE_LOWER, 'UTF-8');
		} else {
			return strtolower($subject);
		}
	}
}