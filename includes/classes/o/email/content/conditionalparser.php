<?php
/**
 * Parses conditional IF ELSE blocks in email condent.
 * --
 * [IF:3>4]
 * This won't be displayed in email content.
 * [ELSE]
 * Since 4 is bigger than 3, this content will be displayed in email
 * [ENDIF]
 * --
 * IF blocks can be nested within each other.
 */
class O_Email_Content_ConditionalParser
{
	/**
	 * Parses the content for IF blocks and evaluates them.
	 *
	 * @param string $content Content to be parsed
	 * @return string
	 * @author Mert Hurturk
	 */
	public function parse($content)
	{
		if (is_array($content)) {
			$expression = $content['expression'];
			$true_block = $content['block_true'];
			$false_block = isset($content['block_false']) ? $content['block_false'] : '';
			$content = $this->evaluate_expression($expression) ? $true_block: $false_block;
		}

		// RECURSIVE PATTERN MATCHING MADNESS
		// The following is written in an ancient language that even
		// the author don't understand what it says sometimes. It is written,
		// tested, and tested again and again and it works. WORKS! YAY!
		$pattern = "/\[IF:(?P<expression>.*)?\](?P<block_true>(?:(?>[^[]*?)|(?:\[)(?!(IF:(?:.*)?\]))|(?R))*)(\[ELSE\](?P<block_false>(?:(?>[^[]*?)|(?:\[)(?!(IF:(?:.*)?\]))|(?R))*))?\[ENDIF\]/Us";

		return preg_replace_callback($pattern, array($this, 'parse'), $content);
	}

	/**
	 * Evaluates a given expression and returns a boolean
	 *
	 * @param string $expression A string expression(Ex: 4>2, Good!=Bad)
	 * @return bool
	 * @author Mert Hurturk
	 */
	public function evaluate_expression($expression)
	{
		$are_there_any_operators = strpbrk($expression, '<>=!') !== FALSE;
		if ($are_there_any_operators) {
			$are_there_any_matches = preg_match("/^(.*)([!<>=]+?)(.*)?$/uUi", $expression, $expression_matches);
			if ($are_there_any_matches && count($expression_matches) == 4) {
				$left = $expression_matches[1];
				$operator = $expression_matches[2];
				$right = $expression_matches[3];

				if (preg_match("/^\d*$/", $left))
					$left = (int) $left;

				if (preg_match("/^\d*$/", $right))
					$right = (int) $right;

				if ($operator == '=' && $left == $right) {
					$expression_result = true;
				} else if ($operator == '!=' && $left != $right) {
					$expression_result = true;
				} else if ($operator == '>' && $left > $right) {

					$expression_result = true;
				} else if ($operator == '<' && $left < $right) {
					$expression_result = true;
				} else if ($operator == '>=' && $left >= $right) {
					$expression_result = true;
				} else if ($operator == '<=' && $left <= $right) {
					$expression_result = true;
				} else {
					$expression_result = false;
				}
			}
		} else {
			if ($expression != '') {
				$expression_result = true;
			} else {
				$expression_result = false;
			}
		}

		return $expression_result;
	}
}