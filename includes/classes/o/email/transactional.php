<?php
/**
 * Base email class for all transactional emails. This class should not be
 * constructed directly. O_Email_Factory should be used for creating emails.
 */
class O_Email_Transactional extends O_Email_Base
{
	public function __construct()
	{
		parent::__construct();
	}
}
