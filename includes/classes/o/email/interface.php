<?php
/**
 * Main interface for all emails.
 */
interface O_Email_Interface
{
	/**
	 * @abstract
	 * @param string $name From name
	 * @param string $email From email address
	 * @return void
	 */
	public function setFrom($name, $email);

	/**
	 * Returns from email address
	 * @abstract
	 * @return string
	 */
	public function getFromEmail();

	/**
	 * Returns from name
	 * @abstract
	 * @return string
	 */
	public function getFromName();

	/**
	 * @abstract
	 * @param string $name To name
	 * @param string $email To email address
	 * @return void
	 */
	public function setTo($name, $email);

	/**
	 * Returns to email address
	 * @abstract
	 * @return string
	 */
	public function getToEmail();

	/**
	 * Returns to name
	 * @abstract
	 * @return string
	 */
	public function getToName();

	/**
	 * @abstract
	 * @param string $name Reply to name
	 * @param string $email Reply to  email address
	 * @return void
	 */
	public function setReplyTo($name, $email);

	/**
	 * Returns reply to name
	 * @abstract
	 * @return string
	 */
	public function getReplyToName();

	/**
	 * Returns reply to email address
	 * @abstract
	 * @return string
	 */
	public function getReplyToEmail();

	/**
	 * @abstract
	 * @param string $key Header name
	 * @param string $value Header value
	 * @return void
	 */
	public function addHeader($key, $value);

	/**
	 * @abstract
	 * @return array|string
	 */
	public function getHeaders();

	/**
	 * Returns a specific
	 * @abstract
	 * @return string|false
	 */
	public function getHeader($name);

	/**
	 * @abstract
	 * @param string $subject Subject
	 * @return void
	 */
	public function setSubject($subject);

	/**
	 * @abstract
	 * @return string
	 */
	public function getSubject();

	/**
	 * @abstract
	 * @param string $content Html content
	 * @return void
	 */
	public function setHtmlContent($content);

	/**
	 * @abstract
	 * @return string
	 */
	public function getHtmlContent();

	/**
	 * @abstract
	 * @param string $content Plain content
	 * @return void
	 */
	public function setPlainContent($content);

	/**
	 * @abstract
	 * @return string
	 */
	public function getPlainContent();

	/**
	 * Add a string attachment to the email
	 * @abstract
	 * @param string $data String data to be attached to the email
	 * @param string $fileName Filename of the string data to be attached to the email
	 * @return void
	 */
	public function addStringAttachment($data, $fileName, $mimeType = 'application/octet-stream', $encoding = 'base64');

	/**
	 * Add a string attachment to the email
	 * @abstract
	 * @param string $data String data to be attached to the email
	 * @param string $fileName Filename of the string data to be attached to the email
	 * @return void
	 */
	public function addInlineAttachment($filePath, $cid, $mimeType = 'application/octet-stream', $encoding = 'base64');

	/**
	 * Add a file attachment to the email
	 * @abstract
	 * @param string $filePath Path to the attached file
	 * @return void
	 */
	public function addFileAttachment($filePath, $fileName, $mimeType = 'application/octet-stream', $encoding = 'base64');

	/**
	 * @abstract
	 * @return array
	 */
	public function getAttachments();

	/**
	 * Returns all replacements
	 * @abstract
	 * @return array
	 */
	public function getReplacements();

	/**
	 * Adds a replacement variable and its value
	 * @abstract
	 * @param string $searchFor The string to be searched for
	 * @param string $replaceWith The string that will be replaced with
	 * @return void
	 */
	public function addReplacement($searchFor, $replaceWith);
}
