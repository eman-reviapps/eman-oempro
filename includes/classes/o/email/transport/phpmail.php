<?php
class O_Email_Transport_PHPMail extends O_Email_Transport_Base
	implements O_Email_Transport_Interface
{
	public function send(O_Email_Base $email)
	{
		$this->addTransportHeaders($email);

		$converter = new O_Email_Converter_ToString();
		$converter->setEncoding(EMAIL_SOURCE_ENCODING);
		$converter->setServerHostName(EMAIL_DELIVERY_HOSTNAME);
		// While converting email object to string, To: and Subject: headers
		// should be ignored for mail function because they are passed as
		// function arguments.
		$converter->setIgnoredHeaders(array('To', 'Subject'));
		$message = $converter->convert($email);

		$params = sprintf("-oi -f%s", $email->getHeader('Return-Path'));
		$oldFrom = ini_get('sendmail_from');
		ini_set('sendmail_from', $email->getHeader('Return-Path'));

		$isSuccessful = @mail(
			$email->getToEmail(), // To address
			'=?UTF-8?B?' . base64_encode($email->getSubject()) . '?=', // Subject
			$message[1], // Body
			$message[0], // Headers
			$params // Parameters
		);

		if (isset($oldFrom)) {
			ini_set('sendmail_from', $oldFrom);
		}

		if (!$isSuccessful)
			throw new O_Email_Transport_Exception(
				O_Email_Transport_Exception::PHPMAIL_ERROR, $email);
	}
}
