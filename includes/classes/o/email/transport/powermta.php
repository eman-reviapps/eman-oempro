<?php
class O_Email_Transport_PowerMTA extends O_Email_Transport_Base
	implements O_Email_Transport_Interface
{
	protected $_path = '';
	protected $_vmtaName = '';

	public function __construct($path, $vmtaName)
	{
		$this->_path = rtrim($path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
		$this->_vmtaName = $vmtaName;
	}

	public function send(O_Email_Base $email)
	{
		$this->addTransportHeaders($email);

		$email->addHeader('X-Sender', $email->getHeader('Return-Path'));
		$email->addHeader('X-Receiver', $email->getToEmail());
		$email->addHeader('X-Virtual-MTA', $this->_vmtaName);

		$converter = new O_Email_Converter_ToString();
		$converter->setEncoding(EMAIL_SOURCE_ENCODING);
		$converter->setServerHostName(EMAIL_DELIVERY_HOSTNAME);
		$message = $converter->convert($email);

		if (!is_dir($this->_path) || !is_writable($this->_path))
			throw new O_Email_Transport_Exception(
				O_Email_Transport_Exception::FILE_ERROR, $email, 'Save path is not writable');

		$fileName = time().rand(100,999).rand(100,999).rand(100,999).rand(1000,9999);
		$isSuccessful = @file_put_contents($this->_path.$fileName, $message[0].$message[1]);

		if ($isSuccessful === FALSE)
			throw new O_Email_Transport_Exception(
				O_Email_Transport_Exception::FILE_ERROR, $email, 'Couldn\'t write the email to the save path');
	}
}
