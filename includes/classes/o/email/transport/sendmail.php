<?php
class O_Email_Transport_SendMail extends O_Email_Transport_Base
	implements O_Email_Transport_Interface
{
	protected $_path = '';

	public function __construct($path)
	{
		$this->_path = $path;
	}

	public function send(O_Email_Base $email)
	{
		$this->addTransportHeaders($email);

		$converter = new O_Email_Converter_ToString();
		$converter->setEncoding(EMAIL_SOURCE_ENCODING);
		$converter->setServerHostName(EMAIL_DELIVERY_HOSTNAME);
		$message = $converter->convert($email);

		$sendMail = sprintf(
			"%s -oi -f %s -t",
			escapeshellcmd($this->_path),
			escapeshellarg($email->getHeader('Return-Path'))
		);

		if (! @$mail = popen($sendMail, 'w'))
			throw new O_Email_Transport_Exception(
				O_Email_Transport_Exception::SENDMAIL_ERROR, $email);

		fputs($mail, $message[0]);
		fputs($mail, $message[1]);

		$isSuccessful = pclose($mail);

		if ($isSuccessful != 0)
			throw new O_Email_Transport_Exception(
				O_Email_Transport_Exception::SENDMAIL_ERROR, $email);

	}
}
