<?php
class O_Email_Transport_Exception extends Exception
{
	const PHPMAIL_ERROR = 1;
	const FILE_ERROR = 2;
	const SENDMAIL_ERROR = 3;
	const SMTP_ERROR = 4;

	protected $_email;

	public function __construct($code, O_Email_Base $email, $message = '')
	{
		parent::__construct($message, $code);
		$this->_email = $email;
	}

	public function getEmail()
	{
		return $this->_email;
	}
}
