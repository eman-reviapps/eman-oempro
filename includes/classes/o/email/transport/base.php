<?php
class O_Email_Transport_Base
{
	const TRNSPRT_FILE = 'FILE';
	const TRNSPRT_PHPMAIL = 'PHPMAIL';
	const TRNSPRT_POWERMTA = 'POWERMTA';
	const TRNSPRT_SENDMAIL = 'SENDMAIL';
	const TRNSPRT_SMTP = 'SMTP';

	const TRNSPRT_HEADER_NAME = 'X-TransportData';

	public function addTransportHeaders(O_Email_Base $email)
	{
		$value = array('PV'.PRODUCT_VERSION);
		switch (get_class($this)) {
			case 'O_Email_Transport_File':
				$value[] = 'TT'.self::TRNSPRT_FILE;
				break;
			case 'O_Email_Transport_PHPMail':
				$value[] = 'TT'.self::TRNSPRT_PHPMAIL;
				break;
			case 'O_Email_Transport_PowerMTA':
				$value[] = 'TT'.self::TRNSPRT_POWERMTA;
				break;
			case 'O_Email_Transport_SendMail':
				$value[] = 'TT'.self::TRNSPRT_SENDMAIL;
				break;
			case 'O_Email_Transport_SMTP':
				$value[] = 'TT'.self::TRNSPRT_SMTP;
				break;
		}

		$value = implode('-', $value);
		$value = base64_encode($value);

		$email->addHeader(self::TRNSPRT_HEADER_NAME, $value);
	}
}
