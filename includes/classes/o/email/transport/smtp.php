<?php

class O_Email_Transport_SMTP extends O_Email_Transport_Base implements O_Email_Transport_Interface {

    const SEC_NONE = 'none';
    const SEC_SSL = 'ssl';
    const SEC_TLS = 'tls';

    protected $_host = '';
    protected $_port = '';
    protected $_security = self::SEC_NONE;
    protected $_authentication = FALSE;
    protected $_username = '';
    protected $_password = '';
    protected $_timeout = 0;
    protected $_debug = FALSE;
    protected $_keepAlive = FALSE;
    protected $_connection;
    protected $_smtpInstance;
    protected $_errors = array();

    public function __construct($host, $port) {
        $this->_host = $host;
        $this->_port = $port;
    }

    public function setSecurity($security) {
        $this->_security = $security;
    }

    public function isAuthenticated($bool) {
        $this->_authentication = $bool;
    }

    public function setUsernameAndPassword($username, $password) {
        $this->_username = $username;
        $this->_password = $password;
    }

    public function enableDebug() {
        $this->_debug = TRUE;
    }

    public function keepAlive($bool) {
        $this->_keepAlive = $bool;
    }

    public function setTimeout($seconds) {
        $this->_timeout = (int) $seconds;
    }

    public function send(O_Email_Base $email) {
        $this->addTransportHeaders($email);

        $converter = new O_Email_Converter_ToString();
        $converter->setEncoding(EMAIL_SOURCE_ENCODING);
        $converter->setServerHostName(EMAIL_DELIVERY_HOSTNAME);
        $message = $converter->convert($email);

        $this->_errors = array();

        if (!$this->_connect())
            throw new O_Email_Transport_Exception(
            O_Email_Transport_Exception::SMTP_ERROR, $email, implode(',', $this->_errors)
            );

        if (!$this->_smtpInstance->Mail($email->getHeader('Return-Path'))) {
            $this->_smtpInstance->Reset();
            throw new O_Email_Transport_Exception(
            O_Email_Transport_Exception::SMTP_ERROR, $email, 'From address failed: ' . $email->getHeader('Return-Path')
            );
        }

        if (!$this->_smtpInstance->Recipient($email->getToEmail())) {
            $this->_smtpInstance->Reset();
            throw new O_Email_Transport_Exception(
            O_Email_Transport_Exception::SMTP_ERROR, $email, 'To address failed: ' . $email->getToEmail()
            );
        }

        if (!$this->_smtpInstance->Data($message[0] . $message[1])) {
            $this->_smtpInstance->Reset();
            throw new O_Email_Transport_Exception(
            O_Email_Transport_Exception::SMTP_ERROR, $email, 'Data not accepted'
            );
        }

        if ($this->_keepAlive) {
            $this->_smtpInstance->Reset();
        } else {
            $this->_close();
        }
    }

    protected function _connect() {
        $this->_errors = array();

        if (is_null($this->_smtpInstance))
            $this->_smtpInstance = new O_SMTP();

        if ($connection = $this->_smtpInstance->Connected())
            return $connection;

        $this->_smtpInstance->do_debug = $this->_debug;

        $isTLS = $this->_security == self::SEC_TLS;
        $isSSL = $this->_security == self::SEC_SSL;

        $isConnected = $this->_smtpInstance->Connect(
                ($isSSL ? 'ssl://' : '') . $this->_host, $this->_port, $this->_timeout
        );

        if ($isConnected) {
            $hello = $this->_host;
            $this->_smtpInstance->Hello($hello);

            if ($isTLS) {
                if (!$this->_smtpInstance->StartTLS()) {
                    $this->_errors[] = 'TLS connection error';
                    $this->_smtpInstance->Reset();
                    $isConnected = FALSE;
                }

                //We must resend HELLO after tls negociation
                $this->_smtpInstance->Hello($hello);
            }

            if ($this->_authentication) {
                $isAuthenticated = $this->_smtpInstance->Authenticate(
                        $this->_username, $this->_password);
                if (!$isAuthenticated) {
                    $this->_errors[] = 'Authentication error';
                    $this->_smtpInstance->Reset();
                    $isConnected = FALSE;
                }
            }

            if (!$isConnected)
                return FALSE;

            return TRUE;
        }
    }

    protected function _close() {
        if (!is_null($this->_smtpInstance)) {
            if ($this->_smtpInstance->Connected()) {
                $this->_smtpInstance->Quit();
                $this->_smtpInstance->Close();
            }
        }
    }

}
