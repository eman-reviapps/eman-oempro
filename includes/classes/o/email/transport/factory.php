<?php
class O_Email_Transport_Factory
{
	public static $transports = array();

	public static function adminTransport()
	{
		if (isset(self::$transports['admin']))
			return self::$transports['admin'];

		if (SEND_METHOD == 'PHPMail') {
			$transport = new O_Email_Transport_PHPMail();
		} else if (SEND_METHOD == 'SaveToDisk') {
			$transport = new O_Email_Transport_File(SEND_METHOD_SAVETODISK_DIR);
		} else if (SEND_METHOD == 'PowerMTA') {
			$transport = new O_Email_Transport_PowerMTA(
				SEND_METHOD_POWERMTA_DIR, SEND_METHOD_POWERMTA_VMTA);
		} else if (SEND_METHOD == 'LocalMTA') {
			$transport = new O_Email_Transport_SendMail(SEND_METHOD_LOCALMTA_PATH);
		} else if (SEND_METHOD == 'SMTP') {
			$transport = new O_Email_Transport_SMTP(
				SEND_METHOD_SMTP_HOST, SEND_METHOD_SMTP_PORT);

			switch (SEND_METHOD_SMTP_SECURE) {
				case 'ssl':
					$security = O_Email_Transport_SMTP::SEC_SSL;
					break;
				case 'tls':
					$security = O_Email_Transport_SMTP::SEC_TLS;
					break;
				default:
					$security = O_Email_Transport_SMTP::SEC_NONE;
					break;
			}
			$transport->setSecurity($security);

			$isAuthenticated = false;
			if (!is_bool(SEND_METHOD_SMTP_AUTH)) {
				$isAuthenticated = SEND_METHOD_SMTP_AUTH == 'true'
						? true : false;
			} else {
				$isAuthenticated = SEND_METHOD_SMTP_AUTH;
			}

			$transport->isAuthenticated($isAuthenticated);

			$transport->setUsernameAndPassword(
				SEND_METHOD_SMTP_USERNAME, SEND_METHOD_SMTP_PASSWORD);

			$transport->setTimeout(SEND_METHOD_SMTP_TIMEOUT);

			if (SEND_METHOD_SMTP_DEBUG)
				$transport->enableDebug();

			if (SEND_METHOD_SMTP_KEEPALIVE) {
				$transport->keepAlive(TRUE);
			} else {
				$transport->keepAlive(FALSE);
			}
		}

		self::$transports['admin'] = $transport;

		return self::$transports['admin'];
	}

	public static function userGroupTransport($ugi)
	{
		if (isset(self::$transports['usergroups'])
				&& isset(self::$transports['usergroups'][$ugi['UserGroupID']]))
			return self::$transports['usergroups'][$ugi['UserGroupID']];

		if ($ugi['SendMethod'] == 'System')
			return self::adminTransport();


		if ($ugi['SendMethod'] == 'PHPMail') {
			$transport = new O_Email_Transport_PHPMail();
		} else if ($ugi['SendMethod'] == 'SaveToDisk') {
			$transport = new O_Email_Transport_File($ugi['SendMethodSaveToDiskDir']);
		} else if ($ugi['SendMethod'] == 'PowerMTA') {
			$transport = new O_Email_Transport_PowerMTA(
				$ugi['SendMethodPowerMTADir'], $ugi['SendMethodPowerMTAVMTA']);
		} else if ($ugi['SendMethod'] == 'LocalMTA') {
			$transport = new O_Email_Transport_SendMail($ugi['SendMethodLocalMTAPath']);
		} else if ($ugi['SendMethod'] == 'SMTP') {
			$transport = new O_Email_Transport_SMTP(
				$ugi['SendMethodSMTPHost'], $ugi['SendMethodSMTPPort']);

			switch ($ugi['SendMethodSMTPSecure']) {
				case 'ssl':
					$security = O_Email_Transport_SMTP::SEC_SSL;
					break;
				case 'tls':
					$security = O_Email_Transport_SMTP::SEC_TLS;
					break;
				default:
					$security = O_Email_Transport_SMTP::SEC_NONE;
					break;
			}
			$transport->setSecurity($security);

			$transport->isAuthenticated(
				$ugi['SendMethodSMTPAuth'] == 'true' ? TRUE : FALSE);

			$transport->setUsernameAndPassword(
				$ugi['SendMethodSMTPUsername'], $ugi['SendMethodSMTPPassword']);

			$transport->setTimeout($ugi['SendMethodSMTPTimeOut']);

			if ($ugi['SendMethodSMTPDebug'] == 'true')
				$transport->enableDebug();

			if ($ugi['SendMethodSMTPKeepAlive'] == 'true') {
				$transport->keepAlive(TRUE);
			} else {
				$transport->keepAlive(FALSE);
			}
		}

		if (!isset(self::$transports['usergroups']))
			self::$transports['usergroups'] = array();

		self::$transports['usergroups'][$ugi['UserGroupID']] = $transport;

		return self::$transports['usergroups'][$ugi['UserGroupID']];
	}
}
