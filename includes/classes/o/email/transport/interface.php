<?php
interface O_Email_Transport_Interface
{
	public function send(O_Email_Base $email);
}
