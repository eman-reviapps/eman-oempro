<?php
class O_Email_Sender_ForAdmin
{
	public static function send(O_Email_Base $email)
	{
		O_Email_Personalizer::personalize($email);
		
		$transport = O_Email_Transport_Factory::adminTransport();

		try {
			$transport->send($email);
		} catch (O_Email_Transport_Exception $e) {
			return $e->getMessage();
		}

		return TRUE;
	}
}

