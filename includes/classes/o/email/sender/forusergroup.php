<?php
class O_Email_Sender_ForUserGroup
{
	public static function send($userGroupInformation, O_Email_Base $email)
	{
		O_Email_Personalizer::personalize($email);

		//TODO:Add plugins hooks before and after email sent
		$transport = O_Email_Transport_Factory::userGroupTransport($userGroupInformation);
		$transport->send($email);
		//TODO:Close the connection?
	}
}

