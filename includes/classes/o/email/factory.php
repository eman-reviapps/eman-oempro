<?php
/**
 * This factory is responsible for constructing Email objects
 */
class O_Email_Factory
{
	public static $remoteContentCache = array();

	/**
	 * Base transactional email with all custom headers applied
	 *
	 * @static
	 * @return O_Email_Transactional
	 */
	public static function transactionalEmail()
	{
		$email = new O_Email_Transactional();

		/**
		 * @var O_Mapper_EmailHeader $headerMapper
		 */
		$headerMapper = O_Registry::instance()->getMapper('EmailHeader');
		$headers = $headerMapper->findByEmailType(array('all', 'transactional'));
		if ($headers !== FALSE) {
			/**
			 * @var O_Domain_EmailHeader $eachHeader
			 */
			foreach ($headers as $eachHeader) {
				$email->addHeader($eachHeader->getName(), $eachHeader->getValue());
			}
		}

		return $email;
	}

	public static function applyEmailDataFromDBRowArray(O_Email_Base $email, $emailInformation)
	{
		$email->setFrom($emailInformation['FromName'], $emailInformation['FromEmail']);
		$email->setReplyTo($emailInformation['ReplyToName'], $emailInformation['ReplyToEmail']);
		$email->setSubject($emailInformation['Subject']);

		if ($emailInformation['Mode'] == 'Import') {
			self::fetchRemoteContentAndSetEmailContent($email, $emailInformation);
		} else {
			$email->setPlainContent($emailInformation['PlainContent']);
			$email->setHtmlContent($emailInformation['HTMLContent']);
		}

		if ($emailInformation['ImageEmbedding'] == 'Enabled')
			$email->enableImageEmbedding();

		Core::LoadObject('attachments');
		$attachments = array();
		if ($emailInformation['EmailID'] != 0)
			$attachments = Attachments::RetrieveAttachments(
				array('*'), array('RelEmailID' => $emailInformation['EmailID']),
				array('AttachmentID' => 'ASC'));

		foreach ($attachments as $eachAttachment) {
			$email->addFileAttachment(
				DATA_PATH . 'attachments/' . md5($eachAttachment['AttachmentID']),
				$eachAttachment['FileName'],
				$eachAttachment['FileMimeType']
			);
		}
	}

	public static function fetchRemoteContentAndSetEmailContent(O_Email_Base $email, $emailInformation)
	{
		if ($emailInformation['ContentType'] == 'Both' || $emailInformation['ContentType'] == 'HTML') {
			if (!isset(self::$remoteContentCache[$emailInformation['FetchURL']])) {
				$remoteContent = Core::DataPostToRemoteURL($emailInformation['FetchURL'], array(), 'GET', false, '', '', 60, false);
				if (!$remoteContent[0]) {
					$remoteContent = '';
				} else {
					$remoteContent = $remoteContent[1];
				}
				self::$remoteContentCache[$emailInformation['FetchPlainURL']] = $remoteContent;
			} else {
				$remoteContent = self::$remoteContentCache[$emailInformation['FetchURL']];
			}
			$email->setHtmlContent($remoteContent);
		}
		if ($emailInformation['ContentType'] == 'Both' || $emailInformation['ContentType'] == 'Plain') {
			if (!isset(self::$remoteContentCache[$emailInformation['FetchPlainURL']])) {
				$remoteContent = Core::DataPostToRemoteURL($emailInformation['FetchPlainURL'], array(), 'GET', false, '', '', 60, false);
				if (!$remoteContent[0]) {
					$remoteContent = '';
				} else {
					$remoteContent = $remoteContent[1];
				}
				self::$remoteContentCache[$emailInformation['FetchPlainURL']] = $remoteContent;
			} else {
				$remoteContent = self::$remoteContentCache[$emailInformation['FetchPlainURL']];
			}
			$email->setPlainContent($remoteContent);
		}
		unset($remoteContent);
	}

	/**
	 * Opt-in confirmation email
	 *
	 * @static
	 * @uses O_Email_Factory::transactionalEmail() to create the email
	 * @param array $emailInformation Email information array returned from DB
	 * @param array $subscriberInformation Subscriber information array returned from DB
	 * @param array $listInformation List information array returned from DB
	 * @param array $userInformation User information array returned from DB
	 * @param bool $isPreview Whether this is a preview email or not
	 * @return O_Email_Transactional
	 */
	public static function optInConfirmationEmail($emailInformation, $subscriberInformation, $listInformation, $userInformation, $isPreview = false)
	{
		$email = self::transactionalEmail();
		self::applyEmailDataFromDBRowArray($email, $emailInformation);

		$email->setTo('', $subscriberInformation['EmailAddress']);

		list($returnPath, $returnPathCode) = O_Email_Helper::generateReturnPathForOptInConfirmationEmail(
			$subscriberInformation['SubscriberID'],
			$listInformation['ListID'],
			$userInformation['UserID']
		);

		$email->addHeader('Return-Path', $returnPath);

		$xMailer = X_MAILER;
		if (isset($userInformation['GroupInformation']['XMailer'])
				&& $userInformation['GroupInformation']['XMailer'] != '')
			$xMailer = $userInformation['GroupInformation']['XMailer'];
		$email->addHeader('X-Mailer', $xMailer);

		$email->addHeader('X-Complaints-To', X_COMPLAINTS_TO);

		// Encrypted query parameters - Start
		$encryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
																	   0, // Campaign ID
																	   0, // Autoresponder ID
																	   $subscriberInformation['SubscriberID'], // Subscriber ID
																	   $listInformation['ListID'], // List ID
																	   0, // Email ID
																	   (int) $isPreview // Is preview
																  ));
		// Encrypted query parameters - End
		$email->addHeader('List-Unsubscribe', '<' . APP_URL . 'u.php?p=' . $encryptedQuery . '>');

		$messageId = base64_encode($returnPathCode);
		$email->addHeader('X-MessageID', $messageId);

		O_Email_Personalizer::addReplacementsForSubscriberInformation(
			$email, $subscriberInformation);
		O_Email_Personalizer::addReplacementsForUserInformation(
			$email, $userInformation);
		O_Email_Personalizer::addReplacementsForListInformation(
			$email, $listInformation);
		O_Email_Personalizer::addOptInOutLinkReplacements(
			$email, $subscriberInformation['SubscriberID'], $listInformation['ListID'], $isPreview);

		return $email;
	}

	/**
	 * Base system notification email with template content. The email
	 * constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::transactionalEmail() to create the email
	 * @return O_Email_Transactional
	 */
	public static function systemNotificationEmail()
	{
		$email = self::transactionalEmail();

		$email->addHeader('X-Complaints-To', X_COMPLAINTS_TO);

		// System notification emails get content from a file template
		// located under template/emails folder
		$contentProvider = new O_Email_ContentProvider_File();
		$contentProvider->setFile(
			TEMPLATE_PATH . 'emails/systemnotification.txt'
		);
		$email->setSubject($contentProvider->getSubject());
		$email->setPlainContent($contentProvider->getPlainContent());
		$email->setHtmlContent($contentProvider->getHtmlContent());

		// Load language strings. This is needed for CLI modules because
		// ApplicationHeader class and its ArrayLanguageStrings variable is
		// not available for CLI scripts. ApplicationHeader class included
		// only by CodeIgniter front controller.
		if (!class_exists('ApplicationHeader')) {
			if (count(O_LanguageStrings::$strings) < 1) {
				O_LanguageStrings::$strings = Core::LoadLanguage(
					LANGUAGE, '', TEMPLATE_PATH . 'languages');
			}
		} else {
			O_LanguageStrings::$strings = ApplicationHeader::$ArrayLanguageStrings;
		}


		return $email;
	}

	/**
	 * Base admin system notification email with from, to and reply to
	 * information set. The email constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::systemNotificationEmail() to create the email
	 * @return O_Email_Transactional
	 */
	public static function adminSystemNotificationEmail()
	{
		$email = self::systemNotificationEmail();

		$email->addHeader('Return-Path', SYSTEM_EMAIL_FROM_EMAIL);
		$email->addHeader('X-Mailer', X_MAILER);

		// Set from, to and reply to information to system information
		$email->setFrom(SYSTEM_EMAIL_FROM_NAME, SYSTEM_EMAIL_FROM_EMAIL);
		$email->setTo('', ALERT_RECIPIENT_EMAIL);
		$email->setReplyTo(SYSTEM_EMAIL_REPLYTO_NAME, SYSTEM_EMAIL_REPLYTO_EMAIL);

		return $email;
	}

	/**
	 * Base user system notification email with from, to and reply to
	 * information set. The email constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @return O_Email_Transactional
	 */
	public static function userSystemNotificationEmail($userInformation)
	{
		$email = self::adminSystemNotificationEmail();

		$email->setTo('', $userInformation['EmailAddress']);

		if (isset($userInformation['GroupInformation']['XMailer'])
				&& $userInformation['GroupInformation']['XMailer'] != '') {
			$email->addHeader('X-Mailer', $userInformation['GroupInformation']['XMailer']);
		}

		return $email;
	}

	/**
	 * Base bounce handling error email. The email constructed is a
	 * transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param string $bounceMessageSource Whole source of bounce message as string
	 * @return O_Email_Transactional
	 */
	public static function bounceHandlingErrorEmail($bounceMessageSource)
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add bounce message to the email as attachment
		$email->addStringAttachment($bounceMessageSource, 'bounce_message.txt');

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '1771')
		);

		return $email;
	}

	/**
	 * Notification email when a new campaign is pending for approval.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @return O_Email_Transactional
	 */
	public static function campaignApprovalPendingNotification()
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '1805')
		);

		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1807')
		);

		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1806')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when an abuse report
	 * is received.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param int $messageId Id of the reported message
	 * @param string $message Abuse message
	 * @param string $messageName Name of the reporter
	 * @param string $messageEmailAddress Email address of the reporter
	 * @param array $campaignInformation Campaign information array
	 * @param array $listInformation List information array
	 * @param array $subscriberInformation Subscriber information array
	 * @param array $userInformation User information array with group information
	 * @return O_Email_Transactional
	 */
	public static function abuseReport($messageId, $message, $messageName,
									   $messageEmailAddress, $campaignInformation,
									   $listInformation, $subscriberInformation,
									   $userInformation)
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add replacements
		$email->addReplacement('%Replace:Subject%',
			str_replace('%Message:MessageID%',
				$messageId, O_LanguageStrings::get('Screen', '1573')));

		$email->addReplacement('%Replace:MessageHeader%',
			str_replace('%Message:MessageID%',
				$messageId, O_LanguageStrings::get('Screen', '1573')));

		$email->addReplacement('%Replace:MessageDescription%',
			nl2br(O_LanguageStrings::get('Screen', '1574')));

		O_Email_Personalizer::addReplacementsForUserInformation(
			$email, $userInformation);
		O_Email_Personalizer::addReplacementsForUserGroupInformation(
			$email, $userInformation['GroupInformation']);
		O_Email_Personalizer::addReplacementsForListInformation(
			$email, $listInformation);
		O_Email_Personalizer::addReplacementsForSubscriberInformation(
			$email, $subscriberInformation);
		O_Email_Personalizer::addReplacementsForCampaignInformation(
			$email, $campaignInformation);

		$email->addReplacement('%Message:Text%', $message);
		$email->addReplacement('%Message:MessageID%', $messageId);
		$email->addReplacement('%Message:Name%', $messageName);
		$email->addReplacement('%Message:EmailAddress%', $messageEmailAddress);

		return $email;
	}

	/**
	 * Base fbl handling error email. The email constructed is a
	 * transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param string $fblMessageSource Whole source of fbl message as string
	 * @return O_Email_Transactional
	 */
	public static function fblHandlingErrorEmail($fblMessageSource)
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add bounce message to the email as attachment
		$email->addStringAttachment($fblMessageSource, 'fbl_message.txt');

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '1782')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when no bounce patterns
	 * matched against received bounce message. The email constructed is a
	 * transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::bounceHandlingErrorEmail() to create the email
	 * @param string $bounceMessageSource Whole source of bounce message as string
	 * @return O_Email_Transactional
	 */
	public static function noBouncePatternsMatched($bounceMessageSource)
	{
		// Get base bounce handling error notification email
		$email = self::bounceHandlingErrorEmail($bounceMessageSource);

		// Add replacements
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1772')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1773')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when one of the subscriber,
	 * user, campaign or list information could not be found in bounce message.
	 * The email constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::bounceHandlingErrorEmail() to create the email
	 * @param string $bounceMessageSource Whole source of bounce message as string
	 * @return O_Email_Transactional
	 */
	public static function invalidBounceInformation($bounceMessageSource)
	{
		// Get base bounce handling error notification email
		$email = self::bounceHandlingErrorEmail($bounceMessageSource);

		// Add replacements
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1774')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1775')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when one of the subscriber,
	 * user, campaign or list information could not be found in fbl message.
	 * The email constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::fblHandlingErrorEmail() to create the email
	 * @param string $fblMessageSource Whole source of fbl message as string
	 * @return O_Email_Transactional
	 */
	public static function invalidFblInformation($fblMessageSource)
	{
		// Get base fbl handling error notification email
		$email = self::fblHandlingErrorEmail($fblMessageSource);

		// Add replacements
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1783')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1784')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when the message received
	 * in bounce inbox does not seem to be a bounce message. The email
	 * constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::bounceHandlingErrorEmail() to create the email
	 * @param string $bounceMessageSource Whole source of bounce message
	 * @return O_Email_Transactional
	 */
	public static function notABounceMessage($bounceMessageSource)
	{
		// Get base bounce handling error notification email
		$email = self::bounceHandlingErrorEmail($bounceMessageSource);

		// Add replacements
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1777')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1776')
		);

		return $email;
	}

	/**
	 * System notification email that is sent to admin when the message received
	 * in bounce inbox does not seem to be a bounce message. The email
	 * constructed is a transactional email.
	 *
	 * @static
	 * @uses O_Email_Factory::fblHandlingErrorEmail() to create the email
	 * @param string $fblMessageSource Whole source of bounce message
	 * @return O_Email_Transactional
	 */
	public static function notAFblMessage($fblMessageSource)
	{
		// Get base fbl handling error notification email
		$email = self::fblHandlingErrorEmail($fblMessageSource);

		// Add replacements
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1785')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '1786')
		);

		return $email;
	}

	/**
	 * Delivery settings test email that is sent when admin is testing email
	 * delivery settings.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @return O_Email_Transactional
	 */
	public static function adminDeliverySettingsTest()
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '9113')
		);
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '9113')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '9115')
		);

		return $email;
	}

	/**
	 * Admin notification email that is sent to admin when a user exceeds his
	 * user group's import threshold
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param integers $subscribersImported Number of subscribers imported
	 * @return O_Email_Transactional
	 */
	public static function userExceededImportThreshold($userInformation, $subscribersImported)
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '9163')
		);
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '9163')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '9164')
		);

		O_Email_Personalizer::addReplacementsForUserInformation(
			$email, $userInformation);

		O_Email_Personalizer::addReplacementsForUserGroupInformation(
			$email, $userInformation['GroupInformation']);

		$email->addReplacement('%SubscriberImported%', $subscribersImported);

		return $email;
	}

	/**
	 * Admin notification email that is sent to admin when a user exceeds his
	 * user group's import threshold
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param array $campaignInformation Campaign information array
	 * @return O_Email_Transactional
	 */
	public static function userExceededEmailSendThreshold($userInformation, $campaignInformation)
	{
		// Get base admin system notification email
		$email = self::adminSystemNotificationEmail();

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '9165')
		);
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1788')
		);
		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '9166')
		);

		O_Email_Personalizer::addReplacementsForUserInformation(
			$email, $userInformation);

		O_Email_Personalizer::addReplacementsForUserGroupInformation(
			$email, $userInformation['GroupInformation']);

		O_Email_Personalizer::addReplacementsForCampaignInformation(
			$email, $campaignInformation);

		return $email;
	}

	/**
	 * User notification email that is sent to user when a subscription occurs
	 * on one of his lists
	 *
	 * @static
	 * @uses O_Email_Factory::userSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param array $subscriberInformation Subscriber informatino array
	 * @param array $listInformation List information array
	 * @return O_Email_Transactional
	 */
	public static function subscriptionNotification($userInformation, $subscriberInformation, $listInformation)
	{
		$email = self::userSystemNotificationEmail($userInformation);

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '1780')
		);
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1780')
		);

		$messageBody = '';
		$customFieldReplacements = array();

		Core::LoadObject('custom_fields');

		foreach ($subscriberInformation as $key => $value) {
			if (strpos($key, 'CustomField') !== false) {
				$customField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => str_replace('CustomField', '', $key)));
				$customFieldReplacements['%CFName' . $customField['CustomFieldID'] . '%'] = $customField['FieldName'];
				$messageBody .= '%CFName' . $customField['CustomFieldID'] . '% [ID: ' . $customField['CustomFieldID'] . ']: %Subscriber:CustomField' . $customField['CustomFieldID'] . '%<br>';
			} else {
				$messageBody .= $key . ': %Subscriber:' . $key . '%' . "<br>";
			}
		}

		$email->addReplacement('%Replace:MessageDescription%', $messageBody);

		O_Email_Personalizer::addReplacementsForSubscriberInformation(
			$email, $subscriberInformation);

		O_Email_Personalizer::addReplacementsForListInformation(
			$email, $listInformation);

		foreach ($customFieldReplacements as $key => $value) {
			$email->addReplacement($key, $value);
		}

		return $email;
	}

	/**
	 * User notification email that is sent to user when an unsubscription
	 * occurs on one of his lists
	 *
	 * @static
	 * @uses O_Email_Factory::subscriptionNotification() to create the email
	 * @param array $userInformation User information array with group information
	 * @param array $subscriberInformation Subscriber informatino array
	 * @param array $listInformation List information array
	 * @return O_Email_Transactional
	 */
	public static function unsubscriptionNotification($userInformation, $subscriberInformation, $listInformation)
	{
		$email = self::subscriptionNotification(
			$userInformation, $subscriberInformation, $listInformation);

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '1781')
		);
		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1781')
		);

		return $email;
	}

	/**
	 * User notification email that is sent to user when a list synchronization
	 * is complete
	 *
	 * @static
	 * @uses O_Email_Factory::userSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param array $listInformation List information array
	 * @param integer $numberOfSyncedSubscribers Number of synced subscribers
	 * @return O_Email_Transactional
	 */
	public static function listSyncNotification($userInformation, $listInformation, $numberOfSyncedSubscribers)
	{
		$email = self::userSystemNotificationEmail($userInformation);

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', O_LanguageStrings::get('Screen', '9054'));

		$email->addReplacement(
			'%Replace:MessageHeader%', O_LanguageStrings::get('Screen', '1787'));

		$email->addReplacement(
			'%Replace:MessageDescription%', O_LanguageStrings::get('Screen', '9055'));

		O_Email_Personalizer::addReplacementsForListInformation(
			$email, $listInformation);

		$email->addReplacement('%SyncSubscriberCount%', $numberOfSyncedSubscribers);

		return $email;
	}

	/**
	 * Email message that is sent to user by admin through "Send Message"
	 * screen in user edit screen
	 *
	 * @static
	 * @uses O_Email_Factory::userSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param string $subject Message subject
	 * @param string $message Message content
	 * @return O_Email_Transactional
	 */
	public static function messageToUser($userInformation, $subject, $message)
	{
		$email = self::userSystemNotificationEmail($userInformation);

		// Add replacements
		$email->addReplacement('%Replace:Subject%', '[' . PRODUCT_NAME . '] - ' . $subject);

		$email->addReplacement('%Replace:MessageHeader%', $subject);

		$email->addReplacement('%Replace:MessageDescription%', nl2br($message));

		return $email;
	}

	/**
	 * Email message that is sent to the recipient when requested to be reminded
	 * for his password.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param string $to Recipient email address
	 * @param string $username Account username
	 * @param string $resetLink Reset link
	 * @return O_Email_Transactional
	 */
	public static function passwordRemind($to, $username, $resetLink)
	{
		$email = self::adminSystemNotificationEmail();

		$email->setTo('', $to);

		// Add replacements
		$email->addReplacement('%Replace:Subject%',
			O_LanguageStrings::get('Screen', '1529'));

		$email->addReplacement('%Replace:MessageHeader%',
			O_LanguageStrings::get('Screen', '1529'));

		$email->addReplacement('%Replace:MessageDescription%',
			nl2br(sprintf(O_LanguageStrings::get('Screen', '1530'), $username, $resetLink)));

		return $email;
	}

	/**
	 * Email message that is sent to the recipient when new password is created.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param string $to Recipient email address
	 * @param string $username Account username
	 * @param string $password New password
	 * @return O_Email_Transactional
	 */
	public static function passwordReset($to, $username, $password)
	{
		$email = self::adminSystemNotificationEmail();

		$email->setTo('', $to);

		// Add replacements
		$email->addReplacement('%Replace:Subject%',
			O_LanguageStrings::get('Screen', '1531'));

		$email->addReplacement('%Replace:MessageHeader%',
			O_LanguageStrings::get('Screen', '1531'));

		$email->addReplacement('%Replace:MessageDescription%',
			nl2br(sprintf(O_LanguageStrings::get('Screen', '1532'), $username, $password)));

		return $email;
	}

	/**
	 * Email message that is sent to user when admin requests a payment.
	 *
	 * @static
	 * @uses O_Email_Factory::userSystemNotificationEmail() to create the email
	 * @param array $userInformation User information array with group information
	 * @param array $periodInformation Period information array
	 * @return O_Email_Transactional
	 */
	public static function invoice($userInformation, $periodInformation)
	{
		$email = self::userSystemNotificationEmail($userInformation);

		// Add replacements
		$email->addReplacement(
			'%Replace:Subject%', PAYMENT_RECEIPT_EMAIL_SUBJECT);
		$email->addReplacement(
			'%Replace:MessageHeader%', PAYMENT_RECEIPT_EMAIL_SUBJECT);
		$email->addReplacement(
			'%Replace:MessageDescription%', nl2br(PAYMENT_RECEIPT_EMAIL_MESSAGE));

		// Prepare receipt string
		$receipt = '';
		$receipt .= O_LanguageStrings::$strings['Screen']['9082']
				. $periodInformation['CampaignsSent'] . ' (' .
				$periodInformation['ChargePerCampaignSent'] . ')' . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9080']
				. $periodInformation['CampaignsTotalRecipients'] . ' (' .
				$periodInformation['ChargeTotalCampaignRecipients'] . ')' . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9081']
				. $periodInformation['AutoRespondersSent'] . ' (' .
				$periodInformation['ChargeTotalAutoRespondersSent'] . ')' . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9083']
				. $periodInformation['DesignPreviewRequests'] . ' (' .
				$periodInformation['ChargeTotalDesignPrevRequests'] . ')' . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9084']
				. $periodInformation['ChargeAutoResponderPeriod'] . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9085']
				. $periodInformation['ChargeDesignPreviewPeriod'] . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9086']
				. $periodInformation['ChargeSystemPeriod'] . "\n";
		if ($periodInformation['Discount'] != '0') {
			$receipt .= O_LanguageStrings::$strings['Screen']['9087']
					. $periodInformation['Discount'] . "\n";
		}
		$receipt .= O_LanguageStrings::$strings['Screen']['9088']
				. $periodInformation['Tax'] . "\n";
		$receipt .= '--------------------------------------------' . "\n";
		$receipt .= O_LanguageStrings::$strings['Screen']['9089']
				. $periodInformation['TotalAmount'] . "\n";

		$paymentLinks = array();
		if ((PAYPALEXPRESSSTATUS == 'Enabled')
				&& (PAYPALEXPRESSBUSINESSNAME != '')
				&& (PAYPALEXPRESSPURCHASEDESCRIPTION != '')
				&& (PAYPALEXPRESSCURRENCY != '')) {

			$encryptionArray = array(
				'PaymentLogID' => $periodInformation['LogID'],
				'UserID' => $userInformation['UserID'],
				'PaymentGateway' => 'PayPal_Express',
			);
			$link = APP_URL . 'payment.php?p=' . Core::EncryptURL($encryptionArray);
			$paymentLinks[] = O_LanguageStrings::$strings['Screen']['9132'] . "\n"
					. O_LanguageStrings::$strings['Screen']['9133'] . "\n" . $link;
		}

		$plugInReturnVars = Plugins::HookListener(
			'Filter', 'PaymentReceipt.Email.PaymentLinks', array($paymentLinks));
		$paymentLinks = $plugInReturnVars[0];

		if (count($paymentLinks) > 0) {
			$paymentLinks = implode("\n----\n", $paymentLinks);
		}

		O_Email_Personalizer::addReplacementsForUserInformation(
			$email, $userInformation);
		O_Email_Personalizer::addReplacementsForUserGroupInformation(
			$email, $userInformation['GroupInformation']);

		$email->addReplacement('%Period:StartDate%',
			$periodInformation['PeriodStartDate']);
		$email->addReplacement('%Period:FinishDate%',
			$periodInformation['PeriodEndDate']);
		$email->addReplacement('%Period:TotalAmount%',
			$periodInformation['TotalAmount']);
		$email->addReplacement('%Period:ReceiptDetails%', nl2br($receipt));
		$email->addReplacement('%Payment:Links%', nl2br($paymentLinks));

		return $email;
	}
        
        /**
	 * Email message that is sent to the recipient when new password is created.
	 *
	 * @static
	 * @uses O_Email_Factory::adminSystemNotificationEmail() to create the email
	 * @param string $to Recipient email address
	 * @param string $username Account username
	 * @param string $password New password
	 * @return O_Email_Transactional
	 */
	public static function sendCreditLimitNotice($userInformation)
	{
		$email = self::userSystemNotificationEmail($userInformation);

		// Add replacements
		$email->addReplacement('%Replace:Subject%',
			O_LanguageStrings::get('Screen', '9306'));

		$email->addReplacement('%Replace:MessageHeader%',
			O_LanguageStrings::get('Screen', '9306'));

		$email->addReplacement('%Replace:MessageDescription%',
			nl2br(O_LanguageStrings::get('Screen', '9307')));

		return $email;
	}
}
