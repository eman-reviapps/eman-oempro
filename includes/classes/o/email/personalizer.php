<?php
class O_Email_Personalizer
{
	/**
	 * @var O_Email_Personalizer
	 */
	public static $instance = NULL;

	public static function personalize(O_Email_Base $email)
	{
		if (is_null(self::$instance))
			self::$instance = new O_Email_Personalizer();

		self::$instance->applyBasicReplacementsToContent($email);
		self::$instance->applyBasicReplacementsToHeaders($email);
		self::$instance->applyRemoteContentReplacement($email);
		self::$instance->applyDateReplacement($email);
		self::$instance->applyModifiers($email);
		self::$instance->applyConditionals($email);
	}

	public function applyRemoteContentReplacement(O_Email_Base $email)
	{
		//TODO:implement O_Email_Personalizer::applyRemoteContentReplacement
	}

	public function applyDateReplacement(O_Email_Base $email)
	{
		//TODO:implement O_Email_Personalizer::applyDateReplacement
	}

	public function applyModifiers(O_Email_Base $email)
	{
		$modifier_parser = new O_Email_Content_ModifierParser();
		$email->setHtmlContent($modifier_parser->parse($email->getHtmlContent()));
		$email->setPlainContent($modifier_parser->parse($email->getPlainContent()));
		unset($modifier_parser);
	}

	public function applyConditionals(O_Email_Base $email)
	{
		$conditional_parser = new O_Email_Content_ConditionalParser();
		$email->setHtmlContent($conditional_parser->parse($email->getHtmlContent()));
		$email->setPlainContent($conditional_parser->parse($email->getPlainContent()));
		unset($conditional_parser);
	}

	public function applyBasicReplacementsToContent(O_Email_Base $email)
	{
		$replacements = $email->getReplacements();
		// Subject replacements
		$email->setSubject(str_replace(
			array_keys($replacements),
			array_values($replacements),
			$email->getSubject())
		);
		// Html content replacements
		$email->setHtmlContent(str_replace(
			array_keys($replacements),
			array_values($replacements),
			$email->getHtmlContent())
		);
		// Plain content replacements
		$email->setPlainContent(str_replace(
			array_keys($replacements),
			array_values($replacements),
			$email->getPlainContent())
		);
	}

	public function applyBasicReplacementsToHeaders(O_Email_Base $email)
	{
		$headers = $email->getHeaders();
		$replacements = $email->getReplacements();
		foreach ($headers as $name => $value) {
			$email->addHeader($name, str_replace(
				array_keys($replacements),
				array_values($replacements),
				$value
			));
		}
	}


	// Replacement Adder Functions - START

	public static function addReplacementsForSubscriberInformation(O_Email_Base $email, $si)
	{
		foreach ($si as $key => $value) {
			$email->addReplacement('%Subscriber:' . $key . '%', $value);
		}
	}

	public static function addReplacementsForUserInformation(O_Email_Base $email, $ui)
	{
		$userReplaceList = array(
			'ID' => $ui['UserID'],
			'UserID' => $ui['UserID'],
			'FirstName' => $ui['FirstName'],
			'LastName' => $ui['LastName'],
			'EmailAddress' => $ui['EmailAddress'],
			'CompanyName' => $ui['CompanyName'],
			'Website' => $ui['Website'],
			'Street' => $ui['Street'],
			'City' => $ui['City'],
			'State' => $ui['State'],
			'Zip' => $ui['Zip'],
			'Country' => $ui['Country'],
			'Phone' => $ui['Phone'],
			'Fax' => $ui['Fax'],
			'TimeZone' => $ui['TimeZone'],
		);
		foreach ($userReplaceList as $key => $value) {
			$email->addReplacement('%User:' . $key . '%', $value);
		}
	}

	public static function addReplacementsForUserGroupInformation(O_Email_Base $email, $ugi)
	{
		foreach ($ugi as $key => $value) {
			$email->addReplacement('%UserGroup:' . $key . '%', $value);
		}
	}

	public static function addReplacementsForCampaignInformation(O_Email_Base $email, $ci)
	{
		foreach ($ci as $key => $value) {
			$email->addReplacement('%Campaign:' . $key . '%', $value);
		}
	}

	public static function addReplacementsForListInformation(O_Email_Base $email, $li)
	{
		$listReplaceList = array(
			'ListID' => $li['ListID'],
			'ID' => $li['ListID'],
			'Name' => $li['Name'],
		);
		foreach ($listReplaceList as $key => $value) {
			$email->addReplacement('%List:' . $key . '%', $value);
		}
	}

	public static function addOptInOutLinkReplacements(O_Email_Base $email, $sid, $lid, $isPreview = FALSE)
	{
		$encryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
			$lid, // List ID
			$sid, // Subscriber ID
			1, // Is confirm
			($isPreview == TRUE ? 1 : 0) // Is preview
		));

		$email->addReplacement('%Link:Confirm%', APP_URL.'oc.php?p='.$encryptedQuery);

		$encryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
			$lid, // List ID
			$sid, // Subscriber ID
			0, // Is reject
			($isPreview == TRUE ? 1 : 0) // Is preview
		));

		$email->addReplacement('%Link:Reject%', APP_URL.'oc.php?p='.$encryptedQuery);
	}

	public static function addSubscriberLinkReplacements()
	{
		//Todo:implement O_Email_Personalizer::addSubscriberLinkReplacements
	}

	public static function addListLinkReplacements()
	{
		//Todo:implement O_Email_Personalizer::addListLinkReplacements
	}

	public static function addCampaignLinkReplacements()
	{
		//Todo:implement O_Email_Personalizer::addCampaignReplacements
	}

	public static function addAutoResponderLinkReplacements()
	{
		//Todo:implement O_Email_Personalizer::addAutoResponderReplacements
	}

	public static function addOpenTracking()
	{
		//Todo:implement O_Email_Personalizer::addOpenTracking
	}

	public static function addLinkTracking()
	{
		//Todo:implement O_Email_Personalizer::addLinkTracking
	}

}
