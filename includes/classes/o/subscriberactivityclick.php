<?php
class O_SubscriberActivityClick extends O_SubscriberActivity
{
	protected $_linkTitle = '';
	protected $_linkUrl = '';

	public function __construct($id, $name, $type, $statId, $timestamp, $linkTitle, $linkUrl)
	{
		parent::__construct($id, $name, $type, $statId, $timestamp);

		$this->_linkTitle = $linkTitle;
		$this->_linkUrl = $linkUrl;
	}

	public function getLinkTitle()
	{
		return $this->_linkTitle;
	}

	public function getLinkUrl()
	{
		return $this->_linkUrl;
	}
}
