<?php
class O_FBL_Processor
	{
	public static function process_mailgun($Data)
		{
		}

	public static function process_sendgrid($SendgridData)
		{
		// Add email address to suppression list
		Core::LoadObject('suppression_list');
		SuppressionList::Add(
			array(
				'SuppressionID' => '',
				'RelListID' => 0,
				'RelOwnerUserID' => $SendgridData['UserID'],
				'SuppressionSource' => 'SPAM complaint',
				'EmailAddress' => $SendgridData['EmailAddress']
			)
		);

		// Un-subscribe subscriber
		Core::LoadObject('subscribers');
		Subscribers::Unsubscribe($SendgridData['ListID'], $SendgridData['UserID'], $SendgridData['CampaignID'], 0,
			$SendgridData['EmailAddress'], $SendgridData['SubscriberID'], '0.0.0.0', 'SPAM complaint');

		// Save FBL report
		Core::LoadObject('fbl');
		FBL::CreateFBLReport(
			array(
				'SenderEmail' => '',
				'CampaignID' => $SendgridData['CampaignID'],
				'SubscriberID' => $SendgridData['SubscriberID'],
				'EmailAddress' => $SendgridData['EmailAddress'],
				'ListID' => $SendgridData['ListID'],
				'UserID' => $SendgridData['UserID'],
				'DateOfReceive' => date('Y-m-d H:i:s'),
				'FBLEmail' => var_export($SendgridData, true)
			)
		);

		}

	public static function process($emailMessage)
		{
		$unsubscriptionChannel = '';

		// Convert all line endings to LF
		$emailMessage = preg_replace("#(\r\n|\r)#s", "\n", $emailMessage);

		// Check if this is a unsubscription request
		$isUnsubscriptionRequest = FALSE;
		$isSubjectFound = preg_match_all(
			"/Subject: (.*)\n/i", $emailMessage, $subjects, PREG_SET_ORDER);
		
		if ($isSubjectFound > 0 && strpos($subjects[0][1], 'unsubscribe:') !== FALSE)
			{
			$xMessageID = explode(':', trim($subjects[0][1]));
			if (count($xMessageID) < 2)
				{
				throw new O_FBL_Exception('MessageID in unsubscribe subject not found',
					O_FBL_Exception::MESSAGE_ID_NOT_FOUND);
				}
			$isUnsubscriptionRequest = true;
			$xMessageID = $xMessageID[1];
			$unsubscriptionChannel = 'List-Unsubscribe header';
			}
		else
			{
			// Find X-MessageID header
			$isMessageIDfound = preg_match_all(
				"/X-MessageID: (.*)\n/i", $emailMessage, $messageIDs, PREG_SET_ORDER);

			if ($isMessageIDfound == 0)
				{
				throw new O_FBL_Exception('X-MessageID header not found',
					O_FBL_Exception::MESSAGE_ID_NOT_FOUND);
				}

			// Fetch message id. If there are more than one x-messageid headers,
			// fetch the last one.
			$xMessageID = trim($messageIDs[count($messageIDs) - 1][1]);
			$unsubscriptionChannel = 'SPAM complaint';
			}

		// Decode message id
		list($campaignID, $subscriberID, $subscriberEmailAddress, $listID, $userID, $autoResponderID) = O_Email_Helper::decodeAbuseXMessageID($xMessageID);

		// Un-subscribe subscriber
		Core::LoadObject('subscribers');
		Subscribers::Unsubscribe($listID, $userID, $campaignID, 0,
			$subscriberEmailAddress, $subscriberID, '0.0.0.0', $unsubscriptionChannel);

		// Add email address to user's global suppression list
		if ($isUnsubscriptionRequest === FALSE)
			{
			Core::LoadObject('suppression_list');
			SuppressionList::Add(
				array(
					'SuppressionID' => '',
					'RelListID' => 0,
					'RelOwnerUserID' => $userID,
					'SuppressionSource' => 'SPAM complaint',
					'EmailAddress' => $subscriberEmailAddress
				)
			);
			}

		// Save FBL report
		Core::LoadObject('fbl');
		FBL::CreateFBLReport(
			array(
				'SenderEmail' => '',
				'CampaignID' => $campaignID,
				'SubscriberID' => $subscriberID,
				'EmailAddress' => $subscriberEmailAddress,
				'ListID' => $listID,
				'UserID' => $userID,
				'DateOfReceive' => date('Y-m-d H:i:s'),
				'FBLEmail' => $emailMessage
			)
		);

		}
	}
