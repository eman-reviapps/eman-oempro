<?php
class O_FBL_Exception extends Exception
{
	const MESSAGE_ID_NOT_FOUND = 1;
	const INVALID_USER_ID = 2;
	const INVALID_CAMPAIGN_ID = 3;
	const INVALID_LIST_ID = 4;
	const INVALID_SUBSCRIBER_ID = 5;
	const INVALID_FBL_MESSAGE = 6;
	const POP3_CONNECTION_ERROR = 7;
}
