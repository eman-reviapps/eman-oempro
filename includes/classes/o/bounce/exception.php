<?php
class O_Bounce_Exception extends Exception
{
	const INVALID_USER_ID = 1;
	const INVALID_CAMPAIGN_ID = 2;
	const INVALID_LIST_ID = 3;
	const INVALID_SUBSCRIBER_ID = 4;
	const INVALID_BOUNCE_MESSAGE = 5;
	const POP3_CONNECTION_ERROR = 6;
	const MESSAGE_ID_NOT_FOUND = 7;
	const BOUNCE_TYPE_NOT_RECOGNIZED = 8;
	const INVALID_AUTO_RESPONDER_ID = 9;
}
