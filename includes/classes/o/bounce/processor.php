<?php
class O_Bounce_Processor
	{
	const HARD_BOUNCE = 'Hard';
	const SOFT_BOUNCE = 'Soft';

	public static function process_mailgun($Data, $softToHardThreshold)
		{
		}

	public static function process_sendgrid($SendgridData, $softToHardThreshold)
		{
		// Detect bounce type
			$Status = explode('.', $SendgridData['Status']);
		$bounceType = ($Status[0] == 4 ? self::SOFT_BOUNCE : self::HARD_BOUNCE);

		if (!$bounceType)
			{
			throw new O_Bounce_Exception('Bounce type not recognized',
				O_Bounce_Exception::BOUNCE_TYPE_NOT_RECOGNIZED);
			}

		// Retrieve user information
		Core::LoadObject('users');
		$arrayUser = Users::RetrieveUser(array('*'), array('UserID' => $SendgridData['UserID']));
		if (!$arrayUser)
			{
			throw new O_Bounce_Exception('Invalid user id',
				O_Bounce_Exception::INVALID_USER_ID);
			}

		// Retrieve campaign information
		Core::LoadObject('campaigns');
		$arrayCampaign = Campaigns::RetrieveCampaign(array('*'),
			array('CampaignID' => $SendgridData['CampaignID'], 'RelOwnerUserID' => $arrayUser['UserID']));
		if (!$arrayCampaign && $autoResponderID == 0)
			{
			throw new O_Bounce_Exception('Invalid campaign id',
				O_Bounce_Exception::INVALID_CAMPAIGN_ID);
			}

		// Retrieve auto responder information
		$arrayAutoResponder = FALSE;
		Core::LoadObject('auto_responders');
		if ($arrayCampaign == FALSE)
			{
			$arrayAutoResponder = AutoResponders::RetrieveResponder(
				array('*'),
				array(
					'AutoResponderID' => $SendgridData['AutoResponderID'],
					'RelOwnerUserID' => $arrayUser['UserID']
				)
			);
			if (!$arrayAutoResponder)
				{
				throw new O_Bounce_Exception('Invalid auto responder id',
					O_Bounce_Exception::INVALID_AUTO_RESPONDER_ID);
				}
			}

		// Retrieve list information
		Core::LoadObject('lists');
		$arrayList = Lists::RetrieveList(array('*'),
			array('ListID' => $SendgridData['ListID'], 'RelOwnerUserID' => $SendgridData['UserID']));
		if (!$arrayList)
			{
			throw new O_Bounce_Exception('Invalid list id',
				O_Bounce_Exception::INVALID_LIST_ID);
			}

		// Retrieve subscriber information
		Core::LoadObject('subscribers');
		$arraySubscriber = Subscribers::RetrieveSubscriber(array('*'),
			array('SubscriberID' => $SendgridData['SubscriberID']), $SendgridData['ListID']);
		if (!$arraySubscriber)
			{
			throw new O_Bounce_Exception('Invalid subscriber id',
				O_Bounce_Exception::INVALID_SUBSCRIBER_ID);
			}

		// Retrieve subscriber's bounce history
		$subscriberBounceHistory = Subscribers::SubscriberBounceHistory(
			$SendgridData['SubscriberID'], $SendgridData['ListID']);

		$addToSuppressionList = FALSE;
		$subscriberBounceStatus = '';
		$listStatistics = array();
		$campaignStatistics = array();
		if ($bounceType == self::HARD_BOUNCE)
			{
			$subscriberBounceStatus = self::HARD_BOUNCE;
			$campaignStatistics = array(
				'TotalHardBounces' => array(false, 'TotalHardBounces + 1'));
			$listStatistics = array('TotalHardBounce' => 1);
			$addToSuppressionList = TRUE;
			}
		else {
		if ($bounceType == self::SOFT_BOUNCE)
			{
			if ($softToHardThreshold < $subscriberBounceHistory[0])
				{
				$subscriberBounceStatus = self::SOFT_BOUNCE;
				$listStatistics = array('TotalHardBounce' => 1);
				$addToSuppressionList = TRUE;
				}
			else
				{
				$subscriberBounceStatus = 'Soft';
				$listStatistics = array('TotalSoftBounce' => 1);
				}
			$campaignStatistics = array(
				'TotalSoftBounces' => array(false, 'TotalSoftBounces + 1'));
			}
		}

		// Update subscriber's bounce status
		Subscribers::Update(
			$SendgridData['ListID'],
			array('BounceType' => $subscriberBounceStatus),
			array('SubscriberID' => $SendgridData['SubscriberID'])
		);

		// Update campaign statistics
		if ($arrayCampaign != FALSE)
			{
			Campaigns::Update(
				$campaignStatistics,
				array('CampaignID' => $SendgridData['CampaignID'])
			);
			}
		else
			{
			if ($arrayAutoResponder != FALSE)
				{
				AutoResponders::Update(
					$campaignStatistics,
					array('AutoResponderID' => $SendgridData['AutoResponderID'])
				);
				}
			}

		// Update activity statistics
		Core::LoadObject('statistics');
		Statistics::UpdateListActivityStatistics(
			$SendgridData['ListID'],
			$SendgridData['UserID'],
			$listStatistics
		);

		// Add email address to suppression list
		if ($addToSuppressionList)
			{
			Core::LoadObject('suppression_list');
			SuppressionList::Add(
				array(
					'SuppressionID' => '',
					'RelListID' => 0,
					'RelOwnerUserID' => $SendgridData['UserID'],
					'SuppressionSource' => 'Hard Bounced',
					'EmailAddress' => $arraySubscriber['EmailAddress']
				)
			);
			}

		// Record the bounce activity
		Statistics::RegisterBounceDetection($SendgridData['UserID'], $SendgridData['SubscriberID'], $SendgridData['ListID'],
			$SendgridData['CampaignID'], $bounceType, $SendgridData['AutoResponderID']);
		}

	public static function process($emailMessage, $bounceDomain, $softToHardThreshold)
		{
		// Convert all line endings to LF
		$emailMessage = preg_replace("#(\r\n|\r)#s", "\n", $emailMessage);

		// Find bounce id
		$isBounceIDfound = preg_match_all(
			"/To: <?bounce-(.*)@" . preg_quote($bounceDomain) . "/ui",
			$emailMessage, $bounceIDs, PREG_SET_ORDER);

		if ($isBounceIDfound == 0)
			{
			throw new O_Bounce_Exception('Not a valid bounce message',
				O_Bounce_Exception::INVALID_BOUNCE_MESSAGE);
			}

		// Decode bounce id and decode it
		$bounceID = trim(str_replace('@' . $bounceDomain,
			'', $bounceIDs[0][1]));
		$decodedBounceID = explode('-', $bounceID);
		foreach ($decodedBounceID as &$each)
			{
			$each = Core::DecryptNumber($each);
			}
		$campaignID = $decodedBounceID[0];
		$subscriberID = $decodedBounceID[1];
		$listID = $decodedBounceID[2];
		$userID = $decodedBounceID[3];
		$autoResponderID = 0;

		// Detect X-MessageID header and decode it (used only for auto
		// responder id)
		$isMessageIDfound = preg_match_all(
			"/X-MessageID: (.*)\n/i", $emailMessage, $messageIDs, PREG_SET_ORDER);
		if ($isMessageIDfound == 0)
			{
			$xMessageID = trim($messageIDs[count($messageIDs) - 1][1]);
			// All return variables of decoding process are prefixed with X to not to interfere with the ids found thorugh bounce id.
			// If campaign id equals to zero, then auto responder id is set from the x message id.
			list($XcampaignID, $XsubscriberID, $XsubscriberEmailAddress, $XlistID, $XuserID, $XautoResponderID) = O_Email_Helper::decodeAbuseXMessageID($xMessageID);
			if ($XcampaignID == 0)
				{
				$autoResponderID = $XautoResponderID;
				}
			unset($XcampaignID, $XsubscriberID, $XsubscriberEmailAddress, $XlistID, $XuserID, $XautoResponderID);
			}

		// Detect bounce type
		$bounceType = self::detectBounceType($emailMessage);
		if (!$bounceType)
			{
			throw new O_Bounce_Exception('Bounce type not recognized',
				O_Bounce_Exception::BOUNCE_TYPE_NOT_RECOGNIZED);
			}

		// Retrieve user information
		Core::LoadObject('users');
		$arrayUser = Users::RetrieveUser(array('*'), array('UserID' => $userID));
		if (!$arrayUser)
			{
			throw new O_Bounce_Exception('Invalid user id',
				O_Bounce_Exception::INVALID_USER_ID);
			}

		// Retrieve campaign information
		Core::LoadObject('campaigns');
		$arrayCampaign = Campaigns::RetrieveCampaign(array('*'),
			array('CampaignID' => $campaignID, 'RelOwnerUserID' => $userID));
		if (!$arrayCampaign && $autoResponderID == 0)
			{
			throw new O_Bounce_Exception('Invalid campaign id',
				O_Bounce_Exception::INVALID_CAMPAIGN_ID);
			}

		// Retrieve auto responder information
		$arrayAutoResponder = FALSE;
		Core::LoadObject('auto_responders');
		if ($arrayCampaign == FALSE)
			{
			$arrayAutoResponder = AutoResponders::RetrieveResponder(
				array('*'),
				array(
					'AutoResponderID' => $autoResponderID,
					'RelOwnerUserID' => $userID
				)
			);
			if (!$arrayAutoResponder)
				{
				throw new O_Bounce_Exception('Invalid auto responder id',
					O_Bounce_Exception::INVALID_AUTO_RESPONDER_ID);
				}
			}

		// Retrieve list information
		Core::LoadObject('lists');
		$arrayList = Lists::RetrieveList(array('*'),
			array('ListID' => $listID, 'RelOwnerUserID' => $userID));
		if (!$arrayList)
			{
			throw new O_Bounce_Exception('Invalid list id',
				O_Bounce_Exception::INVALID_LIST_ID);
			}

		// Retrieve subscriber information
		Core::LoadObject('subscribers');
		$arraySubscriber = Subscribers::RetrieveSubscriber(array('*'),
			array('SubscriberID' => $subscriberID), $listID);
		if (!$arraySubscriber)
			{
			throw new O_Bounce_Exception('Invalid subscriber id',
				O_Bounce_Exception::INVALID_SUBSCRIBER_ID);
			}

		// Retrieve subscriber's bounce history
		$subscriberBounceHistory = Subscribers::SubscriberBounceHistory(
			$subscriberID, $listID);

		$addToSuppressionList = FALSE;
		$subscriberBounceStatus = '';
		$listStatistics = array();
		$campaignStatistics = array();
		if ($bounceType == self::HARD_BOUNCE)
			{
			$subscriberBounceStatus = self::HARD_BOUNCE;
			$campaignStatistics = array(
				'TotalHardBounces' => array(false, 'TotalHardBounces + 1'));
			$listStatistics = array('TotalHardBounce' => 1);
			$addToSuppressionList = TRUE;
			}
		else {
		if ($bounceType == self::SOFT_BOUNCE)
			{
			if ($softToHardThreshold < $subscriberBounceHistory[0])
				{
				$subscriberBounceStatus = self::SOFT_BOUNCE;
				$listStatistics = array('TotalHardBounce' => 1);
				$addToSuppressionList = TRUE;
				}
			else
				{
				$subscriberBounceStatus = 'Soft';
				$listStatistics = array('TotalSoftBounce' => 1);
				}
			$campaignStatistics = array(
				'TotalSoftBounces' => array(false, 'TotalSoftBounces + 1'));
			}
		}

		// Update subscriber's bounce status
		Subscribers::Update(
			$listID,
			array('BounceType' => $subscriberBounceStatus),
			array('SubscriberID' => $subscriberID)
		);

		// Update campaign statistics
		if ($arrayCampaign != FALSE)
			{
			Campaigns::Update(
				$campaignStatistics,
				array('CampaignID' => $campaignID)
			);
			}
		else
			{
			if ($arrayAutoResponder != FALSE)
				{
				AutoResponders::Update(
					$campaignStatistics,
					array('AutoResponderID' => $autoResponderID)
				);
				}
			}

		// Update activity statistics
		Core::LoadObject('statistics');
		Statistics::UpdateListActivityStatistics(
			$listID,
			$userID,
			$listStatistics
		);

		// Add email address to suppression list
		if ($addToSuppressionList)
			{
			Core::LoadObject('suppression_list');
			SuppressionList::Add(
				array(
					'SuppressionID' => '',
					'RelListID' => 0,
					'RelOwnerUserID' => $userID,
					'SuppressionSource' => 'Hard Bounced',
					'EmailAddress' => $arraySubscriber['EmailAddress']
				)
			);
			}

		// Record the bounce activity
		Statistics::RegisterBounceDetection($userID, $subscriberID, $listID,
			$campaignID, $bounceType, $autoResponderID);
		}

	public static function detectBounceType($emailMessage)
		{
		global $ArrayBounceDetectionPatterns;
		$bouncePatterns = $ArrayBounceDetectionPatterns;
		foreach ($bouncePatterns as $pattern => $bounceType)
			{
			if (stripos($emailMessage, $pattern) !== FALSE)
				{
				return $bounceType == 'Hard' ?
						self::HARD_BOUNCE : self::SOFT_BOUNCE;
				}
			}

		return FALSE;
		}
	}
