<?php
class O_SubscriberActivity
{
	const CAMPAIGN = 'campaign';
	const AUTORESPONDER = 'autoresponder';

	protected $_timestamp = '';
	protected $_entityName = '';
	protected $_entityId = 0;
	protected $_sourceEntityType = '';
	protected $_sourceStatisticsId = 0;

	public function __construct($id, $name, $type, $statId, $timestamp)
	{
		$this->_entityName = $name;
		$this->_entityId = $id;
		$this->_sourceEntityType = $type;
		$this->_sourceStatisticsId = $statId;
		$this->_timestamp = $timestamp;
	}

	public function getEntityType()
	{
		return $this->_sourceEntityType;
	}

	public function getEntityName()
	{
		return $this->_entityName;
	}

	public function getEntityId()
	{
		return $this->_entityId;
	}

	public function getTimestamp()
	{
		return $this->_timestamp;
	}
}
