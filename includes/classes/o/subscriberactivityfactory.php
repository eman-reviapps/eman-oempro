<?php
class O_SubscriberActivityFactory
{
	public static function createFromDb($rowData)
	{
		switch ($rowData['ActivityType']) {
			case 'open':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityOpen(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date']
					);
				} else {
					$activity = new O_SubscriberActivityOpen(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date']
					);
				}
				break;
			case 'click':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityClick(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date'],
						$rowData['LinkTitle'],
						$rowData['LinkURL']
					);
				} else {
					$activity = new O_SubscriberActivityClick(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date'],
						$rowData['LinkTitle'],
						$rowData['LinkURL']
					);
				}
				break;
			case 'bounce':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityBounce(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date'],
						$rowData['BounceType']
					);
				} else {
					$activity = new O_SubscriberActivityBounce(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date'],
						$rowData['BounceType']
					);
				}
				break;
			case 'forward':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityForward(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date']
					);
				} else {
					$activity = new O_SubscriberActivityForward(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date']
					);
				}
				break;
			case 'browserView':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityBrowserView(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date']
					);
				} else {
					$activity = new O_SubscriberActivityBrowserView(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date']
					);
				}
				break;
			case 'unsubscription':
				if ($rowData['RelCampaignID'] != 0) {
					$activity = new O_SubscriberActivityUnsubscription(
						$rowData['RelCampaignID'],
						$rowData['CampaignName'],
						O_SubscriberActivity::CAMPAIGN,
						$rowData['StatID'],
						$rowData['Date']
					);
				} else {
					$activity = new O_SubscriberActivityUnsubscription(
						$rowData['RelAutoResponderID'],
						$rowData['AutoResponderName'],
						O_SubscriberActivity::AUTORESPONDER,
						$rowData['StatID'],
						$rowData['Date']
					);
				}
				break;
		}
		return $activity;
	}
}
