<?php
interface O_Security_Flood_LogMapperInterface {
	public function findByIPandZone($zoneName, $ipAddress);
}
