<?php
/**
 * Flood detection utility
 *
 * This class lets you detect flood attacks on your scripts.
 * @author Mert Hurturk <mert@octeth.com>
 */
class O_Security_Flood_Detector
{
	/**
	 * @var string Zone name
	 */
	protected $_zoneName;

	/**
	 * @var int Maximum number of access allowed for given time period
	 */
	protected $_maxAccessCount = 3;

	/**
	 * @var int Time period for seconds (Default is a minute)
	 */
	protected $_timePeriod = 60;

	/**
	 * @var int Block period for seconds (Default is an hour)
	 */
	protected $_blockPeriod = 3600;

	/**
	 * @var O_Security_Flood_LogMapperInterface Log mapper
	 */
	protected $_logMapper;

	/**
	 * @param string $zoneName Zone to be checked for flood attack
	 */
	public function __construct($zoneName)
	{
		$this->_zoneName = $zoneName;
	}

	/**
	 * Sets log mapper
	 *
	 * @param O_Security_Flood_LogMapperInterface $logMapper
	 */
	public function setLogMapper(O_Security_Flood_LogMapperInterface $logMapper)
	{
		$this->_logMapper = $logMapper;
	}

	/**
	 * Checks if given IP address is flooding the zone
	 *
	 * @param string $ipAddress IP address to be checked
	 * @param int	$time	  Current time in seconds. If left blank, time() method will be used
	 * @param bool	$record	  Whether to record this request or not
	 *
	 * @return bool
	 */
	public function isFloodedByIP($ipAddress, $time = 0, $record = true)
	{
		if (is_null($this->_logMapper))
			$this->_logMapper = O_Registry::instance()->getMapper('LogFlood');

		if ($time < 1)
			$time = time();

		$log = $this->_logMapper->findByIPandZone($this->_zoneName, $ipAddress);

		if (!$log) {
			if ($record) {
				$log = new O_Domain_LogFlood();
				$log->setIPAddress($ipAddress);
				$log->setZone($this->_zoneName);
				$log->setTime($time);
				$log->setCount(1);
				$this->_logMapper->insert($log);
			}
			return false;
		}

		if ($log->isBlocked()) {
			if ($log->isBlockExpired($this->_blockPeriod, $time)) {
				$log->reset($time);
				if ($record === false) {
					$log->setCount(0);
				}
				$this->_logMapper->update($log);
				return false;
			} else {
				return true;
			}
		}

		if ($log->isTimeExpired($this->_timePeriod, $time)) {
			$log->reset($time);
			$this->_logMapper->update($log);
			return false;
		}

		if ($log->getCount() >= $this->_maxAccessCount) {
			$log->block($time);
			$this->_logMapper->update($log);
			return true;
		}

		if ($record) {
			$log->setCount($log->getCount() + 1);
			$this->_logMapper->update($log);
		}

		return false;
	}

	/**
	 * @param int $accessCount  Maximum number of access allowed
	 * @param int $timePeriod   Time period in seconds
	 * @param int $blockPeriod  Block period in seconds
	 */
	public function setMaxAccessForTimePeriod($accessCount, $timePeriod, $blockPeriod)
	{
		$this->_maxAccessCount = $accessCount;
		$this->_timePeriod = $timePeriod;
		$this->_blockPeriod = $blockPeriod;
	}
}