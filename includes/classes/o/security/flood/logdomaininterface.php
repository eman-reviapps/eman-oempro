<?php
interface O_Security_Flood_LogDomainInterface {
	public function isBlocked();
	public function isBlockExpired();
	public function isTimeExpired();
	public function getCount();
}
