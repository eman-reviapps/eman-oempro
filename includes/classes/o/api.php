<?php
/**
 * A static factory class for creating API command objects. All API command
 * objects should be accessed through this factory.
 */
class O_API
{
	public static $repository = array();

	/**
	 * Returns a command object. Command name should be in following format:
	 *
	 * customfield.update
	 *
	 * Command group name and action name should be separated by a dot.
	 *
	 * @static
	 * @param string $commandName Name of the command
	 * @return O_API_ICommand
	 */
	public static function getCommand($commandName)
	{
		$commandName = str_replace('.', '_', strtolower($commandName));
		if (isset(self::$repository[$commandName]))
			return self::$repository[$commandName];

		$command = new $commandName();
		self::$repository[$commandName] = $command;
		return $command;
	}
}

