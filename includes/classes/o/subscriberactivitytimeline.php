<?php
class O_SubscriberActivityTimeLine
{
	/**
	 * @var array List of subscriber activities
	 */
	protected $_activities = array();

	protected $_start = 0;
	protected $_end = 0;

	public function __construct()
	{

	}

	public function addActivity(O_SubscriberActivity $activity)
	{
		if ($activity->getTimestamp() > $this->_end)
			$this->_end = $activity->getTimestamp();

		if ($this->_start == 0 || $activity->getTimestamp() < $this->_start)
			$this->_start = $activity->getTimestamp();

		$this->_activities[] = $activity;
	}

	public function getActivities()
	{
		return $this->_activities;
	}

	public function getStartTimestamp()
	{
		return $this->_start;
	}

	public function getEndTimestamp()
	{
		return $this->_end;
	}
}
