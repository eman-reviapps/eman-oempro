<?php
class O_Integration_Highrise_Connector
{
	private $_apiKey = '';
	private $_subDomain = '';

	public function __construct($apiKey, $subDomain)
	{
		$this->_apiKey = $apiKey;
		$this->_subDomain = $subDomain;
	}

	public function validateAccount()
	{
		try {
			$this->talkWithHighrise('me.xml');
			return true;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	public function getCompanies()
	{
		try {
			$response = $this->talkWithHighrise('companies.xml');
			$xml = $response[1];
			$companies = array();
			if (!isset($xml->company)) return $companies;
			foreach ($xml->company as $eachCompany) {
				$companies[] = array(
					'id' => (string) $eachCompany->id,
					'name' => (string) $eachCompany->name
				);
			}
			return $companies;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	public function getTags()
	{
		try {
			$response = $this->talkWithHighrise('tags.xml');
			$xml = $response[1];
			$tags = array();
			if (!isset($xml->tag)) return $tags;
			foreach ($xml->tag as $eachTag) {
				$tags[] = array(
					'id' => (string) $eachTag->id,
					'name' => (string) $eachTag->name
				);
			}
			return $tags;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	public function getContacts($offset = 0)
	{
		try {
			$response = $this->talkWithHighrise('people.xml?n='.$offset);
			$xml = $response[1];
			$contacts = array();
			if (!isset($xml->person)) return $contacts;
			foreach ($xml->person as $eachPerson) {
				$contacts[] = $this->_parsePersonInformation($eachPerson);
			}
			$contacts = array_merge($contacts, $this->getContacts($offset + 500));
			return $contacts;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	public function getContactsByCompany($id)
	{
		try {
			$response = $this->talkWithHighrise('companies/'.$id.'/people.xml?n='.$offset);
			$xml = $response[1];
			$contacts = array();
			if (!isset($xml->person)) return $contacts;
			foreach ($xml->person as $eachPerson) {
				$contacts[] = $this->_parsePersonInformation($eachPerson);
			}
			$contacts = array_merge($contacts, $this->getContacts($offset + 500));
			return $contacts;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	public function getContactsByTag($id)
	{
		try {
			$response = $this->talkWithHighrise('people.xml?tag_id='.$id.'&n='.$offset);
			$xml = $response[1];
			$contacts = array();
			if (!isset($xml->person)) return $contacts;
			foreach ($xml->person as $eachPerson) {
				$contacts[] = $this->_parsePersonInformation($eachPerson);
			}
			$contacts = array_merge($contacts, $this->getContacts($offset + 500));
			return $contacts;
		} catch (O_Integration_Highrise_InvalidAuthenticationException $e) {
			return false;
		}
	}

	private function _parsePersonInformation($eachPerson)
	{
		$data = array(
			(string) $eachPerson->{'id'},
			(string) $eachPerson->{'first-name'},
			(string) $eachPerson->{'last-name'}
		);

		$contactData = $eachPerson->{'contact-data'};

		foreach ($contactData->{'email-addresses'} as $eachEmailAddress) {
			$data[] = (string) $eachEmailAddress->{'email-address'}->address;
		}

		return $data;
	}

	public function talkWithHighrise($uri)
	{
		$url = 'http://'.$this->_subDomain.'.highrisehq.com/'.$uri;

		$options = array( 'http' => array(
			'method'			=> 'GET',
			'follow_location'	=> 1,
			'header'			=> "Authorization: Basic " . base64_encode($this->_apiKey.':X'),
			'max_redirects'		=> 30,
			'timeout'			=> 60
		));

		$context = stream_context_create($options);
		$response = @file_get_contents($url, false, $context);

		$lastStatusIndex = 0;
		$lastStatusString = '';
		foreach ($http_response_header as $index=>$data) {
			if (preg_match('/Status\:\s(.*)/', $data, $match)) {
				$lastStatusIndex = $index;
				$lastStatusString = $match[1];
			}
		}

		if (preg_match('/401\sUnauthorized/', $lastStatusString)) {
			throw new O_Integration_Highrise_InvalidAuthenticationException();
		}

		$headers = array_splice($http_response_header, $lastStatusIndex);

		return array($headers, simplexml_load_string($response));
	}
}
