<?php
/**
 * This class provides main functionality for integration Wufoo and Oempro
 * throught webhook data sent from Wufoo to Oempro.
 * 
 * @throws O_Integration_Exception
 */
class O_Integration_Wufoo_Module
{
	/**
	 * Whether integration is enabled or disabled
	 * @var bool
	 */
	protected $_status;

	/**
	 * Integration process result that will be logged
	 * @var O_Integration_Wufoo_Result
	 */
	protected $_result;

	/**
	 * The array that is sent from wufoo webhook
	 * @var array
	 */
	protected $_rawWebHookData = array();

	/**
	 * @var O_Integration_Wufoo_WebHookDataSourceInterface
	 */
	protected $_webHookDataSource;

	/**
	 * @var O_Mapper_WufooIntegration
	 */
	protected $_mapperWufooIntegration;

	/**
	 * @var O_Domain_WufooIntegration
	 */
	protected $_integration;

	/**
	 * @var O_Domain_Subscriber
	 */
	protected $_subscriber;

	/**
	 * @param O_Integration_Wufoo_WebHookDataSourceInterface $dataSource
	 * @param O_Mapper_WufooIntegration $integrationMapper
	 * @return void
	 */
	public function __construct(O_Integration_Wufoo_WebHookDataSourceInterface $dataSource,
								O_Mapper_WufooIntegration $integrationMapper)
	{
		$this->_webHookDataSource = $dataSource;
		$this->_mapperWufooIntegration = $integrationMapper;
	}


	/**
	 * Sets whether integration is enabled or disabled
	 * 
	 * @param bool $status
	 * @return void
	 */
	public function setStatus($status)
	{
		$this->_status = $status;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return 'WUFOO';
	}

	/**
	 * @return O_Integration_Wufoo_Result
	 */
	public function getResult()
	{
		return $this->_result;
	}

	/**
	 * Processes webhook data.
	 *
	 * If any error occurs during the processing, error message is an status
	 * stored in _result property and can be accessed through getResult method.
	 *
	 * @throws O_Integration_Exception
	 * @return void
	 */
	public function processWebHookData()
	{
		try {
			if (!$this->_status)
				throw new O_Integration_Exception('Integration error: Integration is disabled');

			// Load and validate data
			$this->_collectAndValidateRawWebHookData();
			$hsObject = $this->_constructHandshakeObject();
			$this->_validateAndGetUserListAndIntegrationFromDb($hsObject);
			$this->_validateFieldMapping();
			$this->_getSubscriberInformation();

			$hookDataFields = $this->_getFieldsFromHookData();
			$fieldMapping = $this->_integration->getFieldMapping();

			if (!isset($hookDataFields[$fieldMapping['EmailAddress'][0]]))
				throw new O_Integration_Exception('Integration error: Email address is not provided');
			
			$emailAddress = $hookDataFields[$fieldMapping['EmailAddress'][0]];

			$isSubscriptionAction = ($this->_subscriber === FALSE)
					|| ($this->_subscriber['SubscriptionStatus'] == 'Unsubscribed')
					? TRUE : FALSE;


			Core::LoadObject('custom_fields');
			$customFields = CustomFields::RetrieveFields(array('*'),
				array('RelOwnerUserID' => $this->_user['UserID']));

			$otherFields = array();
			if ($customFields != FALSE && count($customFields) > 1) {
				foreach ($customFields as $eachField) {
					if (isset($fieldMapping['CustomField' . $eachField['CustomFieldID']])) {
						$mapping = $fieldMapping['CustomField' . $eachField['CustomFieldID']];
						$mappingType = $mapping[1];
						$mapping = $mapping[0];
						if (!is_array($mapping)) {
							if (isset($hookDataFields[$mapping])) {
								if ($mappingType == 'date') {
									$formattedDate = date('Y-m-d', strtotime($hookDataFields[$mapping]));
									$otherFields['CustomField' . $eachField['CustomFieldID']] = $formattedDate;
								} else {
									$otherFields['CustomField' . $eachField['CustomFieldID']] = $hookDataFields[$mapping];
								}
							}
						} else {
							$tmp = array();
							foreach ($mapping as $eachMapping) {
								if (isset($hookDataFields[$eachMapping])) {
									if (empty($hookDataFields[$eachMapping]))
										continue;
									$tmp[] = $hookDataFields[$eachMapping];
								}
							}
							if ($mappingType == 'shortname') {
								$otherFields['CustomField' . $eachField['CustomFieldID']] = implode(' ', $tmp);
							} else {
								$otherFields['CustomField' . $eachField['CustomFieldID']] = $tmp;
							}
						}
					}
				}
			}
			
			$subscriptionResult = Subscribers::AddSubscriber_Enhanced(array(
				'CheckSubscriberLimit' => FALSE,
				'UserInformation' => $this->_user,
				'ListInformation' => $this->_list,
				'EmailAddress' => $emailAddress,
				'IPAddress' => $this->_rawWebHookData['IP'] . ' - Wufoo Integration',
				'SubscriptionStatus' => '',
				'IsSubscriptionAction' => $isSubscriptionAction,
				'OtherFields' => $otherFields,
				'UpdateIfDuplicate' => TRUE,
				'SendConfirmationEmail' => TRUE,
				'UpdateIfDuplicate' => TRUE,
				'UpdateIfUnsubscribed' => TRUE,
				'ApplyBehaviors' => TRUE,
				'UpdateStatistics' => TRUE,
				'TriggerWebServices' => TRUE,
				'TriggerAutoResponders' => TRUE
			));
			if (! $subscriptionResult[0]) {
				$message = '';
				switch ($subscriptionResult[1]) {
					case 1:
						$message = 'Subscription error: Subscriber limit exceeded';
						break;
					case 2:
						$message = 'Subscription error: Invalid email address';
						break;
					case 5:
						$message = 'Subscription error: Invalid user information';
						break;
					case 6:
						$message = 'Subscription error: Invalid list information';
						break;
				}
				throw new O_Integration_Exception($message);
			}

			$this->_result = new O_Integration_Wufoo_Result(
				$this->getCode(), TRUE, 'SubscriberID:'.$subscriptionResult[1]);
		} catch (O_Integration_Exception $e) {
			$this->_result = new O_Integration_Wufoo_Result(
				$this->getCode(), FALSE, $e->getMessage());
		}
	}

	/**
	 * Loads subscriber information from database. If no subscriber is found,
	 * subscriber information is set to false.
	 *
	 * Subscriber information is stored in _subscriber property.
	 * 
	 * @return void
	 */
	protected function _getSubscriberInformation()
	{
		Core::LoadObject('subscribers');

		$hookDataFields = $this->_getFieldsFromHookData();
		$fieldMapping = $this->_integration->getFieldMapping();

		$this->_subscriber = Subscribers::RetrieveSubscriber(
			array('*'),
			array('EmailAddress' => $hookDataFields[$fieldMapping['EmailAddress'][0]]),
			$this->_list['ListID']);
	}

	/**
	 * Validates field mapping. If field mapping is not valid,
	 * O_Integration_Exception is thrown.
	 *
	 * For now, only checks if email address is mapped or not.
	 *
	 * @throws O_Integration_Exception
	 * @return void
	 */
	protected function _validateFieldMapping()
	{
		$fieldMapping = $this->_integration->getFieldMapping();
		if (!isset($fieldMapping['EmailAddress']))
			throw new O_Integration_Exception('Integration error: Email address not mapped');
	}

	/**
	 * Checks if user, list and integration exist in database. If not,
	 * throws O_Integration_Exception.
	 *
	 * @throws O_Integration_Exception
	 * @param O_Integration_Wufoo_HandshakeKey $hsObject
	 * @return void
	 */
	protected function _validateAndGetUserListAndIntegrationFromDb(
		O_Integration_Wufoo_HandshakeKey $hsObject)
	{
		Core::LoadObject('users');
		Core::LoadObject('lists');

		$integration = $this->_mapperWufooIntegration->findById(
			$hsObject->getIntegrationId());
		if (!$integration)
			throw new O_Integration_Exception('Integration error: Integration information not found');

		$this->_integration = $integration;

		$user = Users::RetrieveUser(array('*'),
			array('UserID' => $hsObject->getUserId()), true);
		if (!$user)
			throw new O_Integration_Exception('Subscription error: User information not found');

		$this->_user = $user;

		$list = Lists::RetrieveList(array('*'),
			array('ListID' => $hsObject->getListId()));
		if (!$list)
			throw new O_Integration_Exception('Subscription error: List information not found');

		$this->_list = $list;
	}

	/**
	 * @return O_Domain_WufooIntegration
	 */
	public function getIntegrationObject()
	{
		return $this->_integration;
	}

	/**
	 * Returns only field information from webhook data received from Wufoo.
	 * 
	 * @return array
	 */
	protected function _getFieldsFromHookData()
	{
		$fields = array();
		foreach ($this->_rawWebHookData as $key => $value) {
			if (preg_match('/^Field[\d]+$/', $key)) {
				$fields[$key] = $value;
			}
		}
		return $fields;
	}

	/**
	 * Constructs a handshake key object from the handshake key string value
	 * sent from Wufoo with webhook data. If handshake key string is not valid,
	 * throws O_Integration_Exception.
	 *
	 * @throws O_Integration_Exception
	 * @return bool|O_Integration_Wufoo_HandshakeKey
	 */
	protected function _constructHandshakeObject()
	{
		$hs = O_Integration_Wufoo_HandshakeKey::createFromWebHookData(
			$this->_rawWebHookData['HandshakeKey']
		);

		if (!$hs) throw new O_Integration_Exception('Integration error: Invalid handshake key');

		return $hs;
	}

	/**
	 * Loads webhook data with the help of webhook data source object and
	 * validates the data.
	 *
	 * If loading or validation fails, O_Integration_Exception is thrown.
	 *
	 * @throws O_Integration_Exception
	 * @return void
	 */
	protected function _collectAndValidateRawWebHookData()
	{
		if (is_null($this->_webHookDataSource))
			throw new O_Integration_Exception('Integration error: Webhook data source is not provided');
		$this->_rawWebHookData = $this->_webHookDataSource->getData();
		if (!$this->_isRawWebHookDataValid())
			throw new O_Integration_Exception('Integration error: Webhook data is not valid (No HandshakeKey)');
	}

	/**
	 * Validates raw webhook data
	 * @return bool
	 */
	protected function _isRawWebHookDataValid()
	{
		if (!isset($this->_rawWebHookData['HandshakeKey'])
				|| $this->_rawWebHookData['HandshakeKey'] == '')
			return false;
		return true;
	}

	/**
	 * Returns unprocessed, raw webhook data
	 * @return array
	 */
	public function getRawWebHookData()
	{
		return $this->_rawWebHookData;
	}
}
