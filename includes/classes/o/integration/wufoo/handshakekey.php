<?php
/**
 *
 * This class represents a Handshake key object. Handshake key objects are used
 * for recognizing the integration setup in database when data received
 * from Wufoo through a web hook.
 *
 * Handshake key string consists of four segments separated by dashes.
 *
 * <code>
 * $hs = new O_Integration_Wufoo_HandshakeKey(1, 2, 3, axff);
 * // or
 * $hs = O_Integration_Wufoo_HandshakeKey::createFromWebHookData('rt-tr-xs-axff');
 * </code>
 *
 */
class O_Integration_Wufoo_HandshakeKey
{
	/**
	 * Owner user id of the integration
	 * @var int
	 */
	protected $_userId;

	/**
	 * List id of the integration
	 * @var int
	 */
	protected $_listId;

	/**
	 * Id that is representing the integration in database
	 * @var int
	 */
	protected $_integrationId;

	/**
	 * Form hash code that represents the form in wufoo 
	 * @var string
	 */
	protected $_formHash;

	/**
	 * Creates a handshake key object from handshake key string
	 *
	 * @static
	 * @param string $keyString Handshake key string
	 * @return bool|O_Integration_Wufoo_HandshakeKey
	 */
	public static function createFromWebHookData($keyString)
	{
		$keyString = explode('-', $keyString);
		if (count($keyString) != 4) return FALSE;

		return new O_Integration_Wufoo_HandshakeKey(
			intval($keyString[0], 36) - 1000, // User id
			intval($keyString[1], 36) - 1000, // List id
			intval($keyString[2], 36) - 1000, // Integration id
			$keyString[3] // Form hash
		);
	}

	/**
	 * Constructor method for handshake key class
	 * @param int $userId User id
	 * @param int $listId List id
	 * @param int $integrationId Integration id
	 * @param string $formHash Form hash code
	 */
	public function __construct($userId, $listId, $integrationId, $formHash)
	{
		$this->_userId = $userId;
		$this->_listId = $listId;
		$this->_integrationId = $integrationId;
		$this->_formHash = $formHash;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return base_convert($this->_userId + 1000, 10, 36) . '-'
				. base_convert($this->_listId + 1000, 10, 36) . '-'
				. base_convert($this->_integrationId+ 1000, 10, 36) . '-'
				. $this->_formHash;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->_userId;
	}

	/**
	 * @return int
	 */
	public function getListId()
	{
		return $this->_listId;
	}

	/**
	 * @return int
	 */
	public function getIntegrationId()
	{
		return $this->_integrationId;
	}

	/**
	 * @return string
	 */
	public function getFormHash()
	{
		return $this->_formHash;
	}
}
