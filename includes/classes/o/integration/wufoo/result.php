<?php
class O_Integration_Wufoo_Result
{
	protected $_isSuccessfullyRan;
	protected $_integrationModuleCode;
	protected $_message;

	public function __construct($moduleCode, $isSuccess, $message)
	{
		$this->_integrationModuleCode = $moduleCode;
		$this->_isSuccessfullyRan = $isSuccess;
		$this->_message = $message;
	}

	public function isSuccess()
	{
		return $this->_isSuccessfullyRan;
	}

	public function getModuleCode()
	{
		return $this->_integrationModuleCode;
	}

	public function getMessage()
	{
		return $this->_message;
	}
}
