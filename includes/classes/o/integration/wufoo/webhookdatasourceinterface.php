<?php
/**
 * This interface provides basic functionality to all webhook data source
 * providers for Wufoo
 */
interface O_Integration_Wufoo_WebHookDataSourceInterface
{
	/**
	 * @abstract
	 * @return array
	 */
	public function getData();
}
