<?php
/**
 * This class retrieves wufoo webhook data from POST
 */
class O_Integration_Wufoo_PostWebHookDataSource
	implements O_Integration_Wufoo_WebHookDataSourceInterface
{
	/**
	 * @return array
	 */
	public function getData()
	{
		return $_POST;
	}
}
