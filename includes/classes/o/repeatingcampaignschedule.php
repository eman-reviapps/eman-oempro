<?php
/**
 * This class is responsible for holding schedule data for a repeating campaign
 * and guessing the next closest sending date and time
 */
class O_RepeatingCampaignSchedule
{
	/**
	 * If this is set, this date time will be used for guessing the next send
	 * date time. If not set, current date time will be used.
	 * @var string Date time in Y-m-d H:i
	 */
	protected $_dateTime = '';

	/**
	 * @var string A full numeric representation of the year of send date, 4 digits
	 * This value is calculated with guessNextSendDate method
	 */
	protected $_year;

	/**
	 * @var string Numeric representation of the month of send date, with leading
	 * zeros. This value is calculated with guessNextSendDate method
	 */
	protected $_month;

	/**
	 * @var string Numeric representation of the day of send date, 2 digits with
	 * leading zeros. This value is calculated with guessNextSendDate method
	 */
	protected $_day;

	/**
	 * @var string Numeric representation of the weekday of send date. Sunday is 0.
	 * This value is calculated with guessNextSendDate method
	 */
	protected $_dayOfWeek;

	/**
	 * @var string Hour of the day of send date, 2 digits with leading zero.
	 * This value is calculated with guessNextSendDate method
	 */
	protected $_hour;

	/**
	 * @var string Minute of the hour of send date, 2 digits with leading zero.
	 * This value is calculated with guessNextSendDate method
	 */
	protected $_minute;

	/**
	 * @var array Schedule's send months.
	 */
	protected $_months;

	/**
	 * @var array Schedule's send days
	 */
	protected $_days;

	/**
	 * @var array Schedule's send hours
	 */
	protected $_hours;

	/**
	 * @var array Schedule's send minutes
	 */
	protected $_minutes;

	/**
	 * @var string Schedule's send day mode.
	 */
	protected $_dayMode = self::DAY_MODE_WEEK;

	protected $_isDayAdvanced = false;
	protected $_isMonthAdvanced = false;
	protected $_isYearAdvanced = false;
	protected $_isHourAdvanced = false;

	const DAY_MODE_MONTH = 'MONTH';
	const DAY_MODE_WEEK = 'WEEK';

	/**
	 * Constructor method
	 * @param mixed $months * or array of month numbers (without leading zeros)
	 * @param mixed $daysOfWeek * or array of week day numbers (without leading zeros)
	 * @param mixed $daysOfMonth * or array of number of days of month (without leading zeros)
	 * @param array $hours array of hours of the day (without leading zeros)
	 * @param array $minutes array of minutes of the day (without leading zeros)
	 */
	public function __construct($months, $daysOfWeek, $daysOfMonth, $hours, $minutes)
	{
		list($this->_months, $this->_days, $this->_hours, $this->_minutes)
				= $this->_cleanInput($months, $daysOfWeek, $daysOfMonth, $hours, $minutes);
	}

	/**
	 * Cleans constructor input and determines the day method
	 * @param mixed $months * or array of month numbers (without leading zeros)
	 * @param mixed $daysOfWeek * or array of week day numbers (without leading zeros)
	 * @param mixed $daysOfMonth * or array of number of days of month (without leading zeros)
	 * @param array $hours
	 * @param array $minutes
	 * @return array months, days, hours, minutes
	 */
	protected function _cleanInput($months, $daysOfWeek, $daysOfMonth, $hours, $minutes)
	{
		if (is_string($months) && $months != '*') $months = '*';
		if (is_array($months)) array_walk($months, 'intval');
		if (is_string($daysOfMonth) && ($daysOfMonth != '*' && $daysOfMonth != '')) $daysOfMonth = '*';
		if (is_array($daysOfMonth) && (count($daysOfMonth) == 1 && $daysOfMonth[0] == '')) $daysOfMonth = '';
		if (is_array($daysOfMonth)) array_walk($daysOfMonth, 'intval');
		if (is_string($daysOfWeek) && ($daysOfWeek != '*' && $daysOfWeek != '')) $daysOfWeek = '*';
		if (is_array($daysOfWeek) && (count($daysOfWeek) == 1 && $daysOfWeek[0] == '')) $daysOfWeek = '';
		if (is_array($daysOfWeek)) array_walk($daysOfWeek, 'intval');
		array_walk($hours, 'intval');
		array_walk($minutes, 'intval');

		$days = empty($daysOfWeek) ? $daysOfMonth : $daysOfWeek;

		if (empty($daysOfWeek))
			$this->_dayMode = self::DAY_MODE_MONTH;

		return array($months, $days, $hours, $minutes);

	}

	/**
	 * Guesses the next closest send date time and returns it
	 * @param string currentDateTime If passed a Y-m-d H:i format date string
	 * passed, it will be used as current date time.
	 * @return string Date string with Y-m-d H:i format
	 */
	public function guessNextSendDateTime($currentDateTime = '')
	{
		if (empty($currentDateTime))
			$currentDateTime = date('Y-m-d H:i');

		$this->_dateTime = $currentDateTime;

		$this->_guessYear();
		$this->_guessMonth();
		$this->_guessDay();
		$this->_guessHour();
		$this->_guessMinute();

		return $this->_year . '-'
				. str_pad($this->_month, 2, '0', STR_PAD_LEFT) . '-'
				. str_pad($this->_day, 2, '0', STR_PAD_LEFT) . ' '
				. str_pad($this->_hour, 2, '0', STR_PAD_LEFT) . ':'
				. str_pad($this->_minute, 2, '0', STR_PAD_LEFT);
	}

	/**
	 * For initialization always sets year as current year.
	 */
	protected function _guessYear()
	{
		$this->_year = date('Y', strtotime($this->_dateTime));
	}

	/**
	 * Guesses the next closest month. If already passed the last month in
	 * schedule, advances year.
	 */
	protected function _guessMonth()
	{
		if (is_string($this->_months) && $this->_months == '*') {
			$this->_month = date('n', strtotime($this->_dateTime));
		} else {
			if ($this->_isYearAdvanced) {
				$this->_month = $this->_months[0];
				return;
			}
			$currentMonth = date('n', strtotime($this->_dateTime));

			$isFound = FALSE;
			$foundMonth = '';
			foreach ($this->_months as $eachMonth) {
				if ($eachMonth >= $currentMonth) {
					if ($eachMonth > $currentMonth)
						$this->_isMonthAdvanced = true;
					$isFound = TRUE;
					$foundMonth = $eachMonth;
					break;
				}
			}

			if (!$isFound) {
				$foundMonth = $this->_months[0];
				$this->_advanceYear();
			}

			$this->_month = $foundMonth;
		}
	}

	/**
	 * Guesses the next closest day. If already passed the last day in schedule,
	 * advances month..
	 */
	protected function _guessDay()
	{
		if (is_string($this->_days) && $this->_days == '*') {
			if ($this->_isMonthAdvanced || $this->_isYearAdvanced) {
				$this->_day = 1;
			} else {
				$this->_day = date('j', strtotime($this->_dateTime));
			}
		} else {
			if ($this->_dayMode == self::DAY_MODE_MONTH) {
				if ($this->_isMonthAdvanced || $this->_isYearAdvanced) {
					$this->_day = $this->_days[0];
					return;
				}

				$currentDay = date('j', strtotime($this->_dateTime));

				$isFound = FALSE;
				$foundDay = '';
				foreach ($this->_days as $eachDay) {
					if ($eachDay >= $currentDay) {
						if ($eachDay > $currentDay)
							$this->_isDayAdvanced = true;
						$isFound = TRUE;
						$foundDay = $eachDay;
						break;
					}
				}

				if (!$isFound) {
					$foundDay = $this->_days[0];
					$this->_advanceMonth();
				} else {
					while ($foundDay > date('t', strtotime($this->_year . '-' . $this->_month))) {
						$this->_advanceMonth();
					}
					$foundDay = $this->_days[0];
				}

				$this->_day = $foundDay;
			} else {
				$ISO8601conversion = array(7, 1, 2, 3, 4, 5, 6);
				$currentDayOfWeek = date('w', strtotime($this->_dateTime));
				$currentDayOfWeek = $ISO8601conversion[$currentDayOfWeek];

				$isFound = FALSE;
				$foundDayOfWeek = '';

				foreach ($this->_days as $eachDay) {
					$eachDayISO8601 = $ISO8601conversion[$eachDay];

					if ($eachDayISO8601 >= $currentDayOfWeek) {
						$isFound = TRUE;
						$foundDayOfWeek = $eachDayISO8601;
						break;
					}
				}

				if (!$isFound) {
					$this->_isDayAdvanced = TRUE;
					$foundDayOfWeek = $this->_days[0];
				}
				$this->_dayOfWeek = $foundDayOfWeek;
				if ($currentDayOfWeek > $foundDayOfWeek) {
					$dayDiff = (7 - $currentDayOfWeek) + $foundDayOfWeek;
				} else {
					$dayDiff = $foundDayOfWeek - $currentDayOfWeek;
				}
				$secDiff = $dayDiff * 24 * 60 * 60;
				$foundDay = strtotime($this->_dateTime) + $secDiff;
				$foundMonth = date('n', $foundDay);
				$foundDay = date('j', $foundDay);

				$currentMonth = date('n', strtotime($this->_dateTime));
				if ($foundMonth > $currentMonth) {
					$this->_advanceMonth();
				}

				$this->_day = $foundDay;
			}
		}
	}

	/**
	 * Guesses the next closest hour. If already passed the last hour in schedule,
	 * advances day.
	 */
	protected function _guessHour()
	{
		if ($this->_isDayAdvanced || $this->_isMonthAdvanced || $this->_isYearAdvanced) {
			$this->_hour = $this->_hours[0];
			return;
		}

		$currentHour = date('G', strtotime($this->_dateTime));

		$isFound = FALSE;
		$foundHour = '';
		foreach ($this->_hours as $eachHour) {
			if ($eachHour >= $currentHour) {
				if ($eachHour > $currentHour)
					$this->_isHourAdvanced = true;
				$isFound = TRUE;
				$foundHour = $eachHour;
				break;
			}
		}

		if (!$isFound) {
			$foundHour = $this->_hours[0];
			$this->_advanceDay();
		}

		$this->_hour = $foundHour;
	}

	/**
	 * Guesses the next closest minute. If already passed the last minute in schedule,
	 * advances hour.
	 */
	protected function _guessMinute()
	{
		if ($this->_isHourAdvanced
				|| $this->_isDayAdvanced || $this->_isMonthAdvanced
				|| $this->_isYearAdvanced) {
			$this->_minute = $this->_minutes[0];
			return;
		}

		$currentMinute = intval(date('i', strtotime($this->_dateTime)));

		$isFound = FALSE;
		$foundMinute = '';
		foreach ($this->_minutes as $eachMinute) {
			if ($eachMinute >= $currentMinute) {
				$isFound = TRUE;
				$foundMinute = $eachMinute;
				break;
			}
		}

		if (!$isFound) {
			$foundMinute = $this->_minutes[0];
			$this->_advanceHour();
		}

		$this->_minute = $foundMinute;
	}

	/**
	 * Advances the year
	 */
	protected function _advanceYear()
	{
		$this->_year = date('Y', strtotime($this->_year . '-01-01 +1 year'));

		$this->_isYearAdvanced = true;
	}

	/**
	 * Advances the month. Calls _advanceYear method if necessary.
	 */
	protected function _advanceMonth()
	{
		$currentMonth = date('n', strtotime($this->_dateTime));
		if ($currentMonth == 12) {
			$this->_advanceYear();
			if (is_string($this->_months) && $this->_months == '*') {
				$this->_month = 1;
			} else {
				$this->_month = $this->_months[0];
			}
		} else {
			if (is_string($this->_months) && $this->_months == '*') {
				$this->_month = date('n', strtotime('2010-' . $this->_month . '-01 +1 months'));
			} else {
				$monthIndex = array_search(
					$this->_month,
					$this->_months
				);
				if ($monthIndex + 1 >= count($this->_months)) {
					$this->_month = $this->_months[0];
					$this->_advanceYear();
				} else {
					$this->_month = $this->_months[$monthIndex + 1];
				}
			}
		}
		$this->_isMonthAdvanced = true;
	}

	/**
	 * Advances the day. Calls _advanceMonth method if necessary.
	 */
	protected function _advanceDay()
	{
		$currentDay = date('j', strtotime($this->_dateTime));
		$numberOfDaysInMonth = date('t', strtotime($this->_year . '-' . $this->_month));

		if ($currentDay == $numberOfDaysInMonth) {
			$this->_advanceMonth();
			if (is_string($this->_days) && $this->_days == '*') {
				$this->_day = 1;
			} else {
				$this->_day = $this->_days[0];
			}
		} else {
			if (is_string($this->_days) && $this->_days == '*') {
				$this->_day = date('d', strtotime($this->_year . '-' . $this->_month . '-' . date('d', strtotime($this->_dateTime)) . ' +1 day'));
			} else {
				if ($this->_dayMode == self::DAY_MODE_MONTH) {
					$dayIndex = array_search(
						$this->_day,
						$this->_days
					);
					if ($dayIndex + 1 >= count($this->_days)) {
						$this->_day = $this->_days[0];
						$this->_advanceMonth();
					} else {
						$this->_day = $this->_days[$dayIndex + 1];
					}
				} else {
					$dayIndex = array_search(
						$this->_dayOfWeek,
						$this->_days
					);
					$passedAWeek = false;
					if ($dayIndex + 1 >= count($this->_days)) {
						$this->_dayOfWeek = $this->_days[0];
						$passedAWeek = true;
					} else {
						$this->_dayOfWeek = $this->_days[$dayIndex + 1];
					}

					$ISO8601conversion = array(7, 1, 2, 3, 4, 5, 6);
					$currentDayOfWeek = date('w', strtotime($this->_dateTime));
					$currentDayOfWeek = $ISO8601conversion[$currentDayOfWeek];

					if ($passedAWeek) {
						$dayDiff = (7 - $currentDayOfWeek) + $this->_dayOfWeek;
						$secDiff = $dayDiff * 24 * 60 * 60;
					} else {
						$dayDiff = $this->_dayOfWeek - $currentDayOfWeek;
						$secDiff = $dayDiff * 24 * 60 * 60;
					}

					$foundDay = strtotime($this->_dateTime) + $secDiff;
					$foundMonth = date('n', $foundDay);
					$foundDay = date('j', $foundDay);

					$currentMonth = date('n', strtotime($this->_dateTime));
					if ($foundMonth > $currentMonth) {
						$this->_advanceMonth();
					}

					$this->_day = $foundDay;
				}
			}
		}

		$this->_isDayAdvanced = true;
	}

	/**
	 * Advances the hour. Calls _advanceDay method if necessary.
	 */
	protected function _advanceHour()
	{
		$currentHour = date('G', strtotime($this->_dateTime));
		if ($currentHour == 23) {
			$this->_advanceDay();
			$this->_hour = $this->_hours[0];
		} else {
			$hourIndex = array_search(
				$this->_hour,
				$this->_hours
			);
			if ($hourIndex + 1 >= count($this->_hours)) {
				$this->_hour = $this->_hours[0];
				$this->_advanceDay();
			} else {
				$this->_hour = $this->_hours[$hourIndex + 1];
			}
		}
		$this->_isHourAdvanced = true;
	}
}
