<?php
/**
 * This class represents a condition in a sql query.
 */
class O_Sql_Condition implements O_Sql_ICondition
{
	/**
	 * Field name
	 * @var string
	 */
	protected $_field = '';

	/**
	 * Condition operator
	 * @var null|string
	 */
	protected $_operator = '';

	/**
	 * Value
	 * @var null|string
	 */
	protected $_value = '';

	/**
	 * Whether to encapsulate value with quotes or not
	 * @var bool
	 */
	protected $_encapsulateValueWithQuotes = TRUE;

	/**
	 * @param string $field Field name or expression
	 * @param null|string $operator Condition operator
	 * @param null|string $value
	 * @param bool $encapsulateValueWithQuotes 
	 */
	public function __construct($field, $operator = NULL, $value = NULL
		, $encapsulateValueWithQuotes = TRUE)
	{
		if (!isset($operator) && !isset($value)) {
			$operator = 'IS';
			$value = 'NULL';
			$encapsulateValueWithQuotes = FALSE;
		}

		if (is_int($value)) {
			$encapsulateValueWithQuotes = FALSE;
		}

		$this->_field = $field;
		$this->_operator = $operator;
		$this->_value = $value;
		$this->_encapsulateValueWithQuotes = $encapsulateValueWithQuotes;
	}

	public static function isNull($field)
	{
		return new O_Sql_Condition($field, 'IS', 'NULL', FALSE);
	}

	public static function isNotNull($field)
	{
		return new O_Sql_Condition($field, 'IS', 'NOT NULL', FALSE);
	}

	public function __toString()
	{
		$value = $this->_value;
		if ($this->_encapsulateValueWithQuotes) {
			$value = '"'.$this->escapeValue($this->_value).'"';
		}

		return '(' . $this->_field
				. (isset($this->_operator) ? ' ' . $this->_operator : '')
				. (isset($this->_value) ? ' ' . $value : '')
				. ')';
	}

	public function escapeValue($value)
	{
		$search = array("\\", "\0", "\n", "\r", "\x1a", "'", '"');
		$replace = array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"');
		return str_replace($search, $replace, $value);
	}
}
