<?php
/**
 * Interface for all query join classes.
 *
 * All query join classes must implement this interface
 */
interface O_Sql_IJoin
{
	public function __toString();
}
