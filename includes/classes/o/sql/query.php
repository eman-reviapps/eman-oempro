<?php
abstract class O_Sql_Query
{
	/**
	 * An array of queries to be prepended to the query
	 * @var array
	 */
	protected $_comments = array();

	/**
	 * Compiled comments
	 * @var string
	 */
	protected $_compiledComments = '';

	/**
	 * Whether the comments have been changed since last compile
	 * @var bool
	 */
	protected $_commentsChanged = TRUE;

	/**
	 * Implements PHP magic __toString method to convert the query to a string.
	 * @return string
	 */
	abstract function __toString();

	/**
	 * Adds a comment to the query
	 * @param string $comment
	 */
	public function addComment($comment)
	{
		$this->_comments[] = $comment;
		$this->_commentsChanged = TRUE;
	}

	/**
	 * Compiles comments and store compiled comments in $_compiledComments
	 */
	protected function compileComments()
	{
		if (!$this->_commentsChanged) return NULL;
		if (count($this->_comments) < 1) return NULL;
		$this->_compiledComments .= "\n-- ".implode("\n-- ", $this->_comments)."\n";
		$this->_commentsChanged = FALSE;
	}
}
