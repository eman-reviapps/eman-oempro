<?php
/**
 * Represents a condition group in sql query
 *
 * @authoer Mert Hurturk
 */
class O_Sql_ConditionGroup implements O_Sql_ICondition
{
	protected $_conditions = array();
	protected $_operator = 'AND';

	/**
	 * @param string $operator Condition operator
	 * @return void
	 */
	public function __construct($operator = 'AND')
	{
		$this->_operator = $operator;
	}

	/**
	 * @param O_Sql_ICondition $condition Condition
	 * @return void
	 */
	public function add(O_Sql_ICondition $condition)
	{
		$this->_conditions[] = $condition;
	}

	public function __toString()
	{
		$stringArray = array();
		foreach ($this->_conditions as $each) {
			$stringArray[] = $each->__toString();
		}
		return "(".implode(" ".$this->_operator." ", $stringArray).")";
	}
}
