<?php
class O_Sql_DeleteQuery extends O_Sql_QueryWithConditions
{
	/**
	 * Name of the table
	 * @var string
	 */
	protected $_table = '';

	/**
	 * @param string $table Name of the table
	 *
	 */
	public function __construct($table)
	{
		$this->_table = $table;
	}

	public function __toString()
	{
		$this->compileComments();
		$this->compileCondition();

		$sql = '';
		$sql .= $this->_compiledComments;
		$sql .= 'DELETE FROM ' . $this->_table;
		if (!empty($this->_compiledConditions))
			$sql .= ' WHERE '.$this->_compiledConditions;

		return $sql;
	}
}
