<?php
/**
 * This class represents a join in a sql query.
 */
class O_Sql_Join implements O_Sql_IJoin
{
	/**
	 * Name of the table
	 * @var string
	 */
	protected $_table = '';

	/**
	 * Join condition
	 * @var O_Sql_ICondition
	 */
	protected $_condition = NULL;

	/**
	 * Join type
	 * @var string
	 */
	protected $_type = '';

	const TYPE_INNER = 'INNER';
	const TYPE_LEFT = 'LEFT';
	const TYPE_RIGHT = 'RIGHT';

	/**
	 * @param string $type Join type
	 * @param string $table Table name and alias if exists
	 * @param O_Sql_ICondition $condition Condition of the join
	 */
	public function __construct($type, $table, O_Sql_ICondition $condition)
	{
		$this->_type = $type;
		$this->_table = $table;
		$this->_condition = $condition;
	}

	public static function left($table, O_Sql_ICondition $condition)
	{
		return new O_Sql_Join(self::TYPE_LEFT, $table, $condition);
	}

	public static function inner($table, O_Sql_ICondition $condition)
	{
		return new O_Sql_Join(self::TYPE_INNER, $table, $condition);
	}

	public static function right($table, O_Sql_ICondition $condition)
	{
		return new O_Sql_Join(self::TYPE_RIGHT, $table, $condition);
	}

	public function __toString()
	{
		return $this->_type.' JOIN '.$this->_table.' ON '.$this->_condition->__toString();
	}
}
