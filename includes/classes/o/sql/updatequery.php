<?php
class O_Sql_UpdateQuery extends O_Sql_QueryWithConditions
{
	/**
	 * Name of the table
	 * @var string
	 */
	protected $_table = '';

	/**
	 * Fields and values to be updated. Keys are field names, values are values.
	 * @var array
	 */
	protected $_fieldsAndValues = array();

	/**
	 * @param string $table Name of the table
	 */
	public function __construct($table)
	{
		$this->_table = $table;
	}

	public function setFields($array)
	{
		foreach ($array as $key => $value) {
			$this->_fieldsAndValues[$key] = $value;
		}
	}

	public function __toString()
	{
		$this->compileComments();
		$this->compileCondition();

		$sql = '';
		$sql .= $this->_compiledComments;
		$sql .= 'UPDATE ' . $this->_table . ' SET ';

		$fieldsAndValues = array();
		foreach ($this->_fieldsAndValues as $field => $value) {

			$valuePrefix = '';
			$valuePostfix = '';

			if (is_string($value)
					&& !preg_match('/MD5/', $value)) {
				$valuePrefix = '"';
				$valuePostfix = '"';
			}

			$fieldsAndValues[] = $field . ' = ' . $valuePrefix . $value . $valuePostfix;
		}

		$sql .= implode(', ', $fieldsAndValues);

		if (!empty($this->_compiledConditions))
			$sql .= ' WHERE ' . $this->_compiledConditions;

		return $sql;
	}
}
