<?php
abstract class O_Sql_QueryWithConditions extends O_Sql_Query
{
	/**
	 * Condition object
	 * @var O_Sql_Condition
	 */
	protected $_condition = NULL;

	/**
	 * Whether the condition has been changed since last compile
	 * @var bool
	 */
	protected $_conditionChanged = TRUE;

	/**
	 * Compiled conditions
	 * @var string
	 */
	protected $_compiledConditions = '';

	/**
	 * Sets condition of the query
	 * @param O_Sql_Condition $condition
	 */
	public function setCondition(O_Sql_ICondition $condition)
	{
		$this->_condition = $condition;
		$this->_conditionChanged = TRUE;
	}

	/**
	 * Compiles conditions and store compiled conditions in $_compiledConditions
	 */
	protected function compileCondition()
	{
		if (! $this->_conditionChanged) return NULL;
		if ($this->_condition == NULL) return NULL;
		$this->_compiledConditions = trim($this->_condition->__toString());
		$this->_conditionChanged = FALSE;
	}
}
