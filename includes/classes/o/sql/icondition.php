<?php
/**
 * Interface for all query condition classes.
 *
 * All query condition classes must implement this interface
 */
interface O_Sql_ICondition
{
	public function __toString();
}
