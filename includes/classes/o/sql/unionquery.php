<?php
class O_Sql_UnionQuery extends O_Sql_Query
{
	/**
	 * Select queries
	 * @var array
	 */
	protected $_selectQueries = array();

	/**
	 * Union order
	 * @var string
	 */
	protected $_order = '';

	/**
	 * Unionlimit
	 * @var string
	 */
	protected $_limit = '';

	public function __construct()
	{
	}

	/**
	 * Sets select fields. Keys are for field names, values are for aliases.
	 * @param array $fields
	 */
	public function addSelectQuery(O_Sql_SelectQuery $query)
	{
		$this->_selectQueries[] = $query;
	}

	public function setOrder($field, $order)
	{
		$this->_order = $field . ' ' . $order;
	}

	public function setRange($startFrom, $numberOfRows)
	{
		$this->_limit = $startFrom . ', ' . $numberOfRows;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$this->compileComments();

		$sql = $this->_compiledComments;

		$selectQueries = array();
		foreach ($this->_selectQueries as $each) {
			$selectQueries[] = $each->__toString();
		}

		$sql .= '('.implode(') UNION (', $selectQueries).')';

		if (!empty($this->_order))
			$sql .= ' ORDER BY ' . $this->_order;

		if (!empty($this->_limit))
			$sql .= ' LIMIT ' . $this->_limit;

		return $sql;
	}
}
