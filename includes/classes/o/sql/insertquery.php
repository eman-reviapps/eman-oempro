<?php
class O_Sql_InsertQuery extends O_Sql_Query
{
	protected $_fieldsAndValues = array();

	/**
	 * Name of the table
	 * @var string
	 */
	protected $_table = '';

	/**
	 * @param string $table Name of the table
	 *
	 */
	public function __construct($table)
	{
		$this->_table = $table;
	}

	public function setFieldsAndValues($fieldsAndValues)
	{
		$this->_fieldsAndValues = $fieldsAndValues;
	}

	public function __toString()
	{
		$this->compileComments();

		$sql = $this->_compiledComments;
		$sql .= 'INSERT INTO ' . $this->_table . ' ';
		$sql .= '(' . implode(', ', array_keys($this->_fieldsAndValues)) . ') ';
		$sql .= 'VALUES ("'.implode('", "', array_values($this->_fieldsAndValues)).'")';

		return $sql;
	}
}
