<?php
class O_Sql_SelectQuery extends O_Sql_QueryWithConditions
{
	/**
	 * Name of the table
	 * @var string
	 */
	protected $_table = '';

	/**
	 * Select fields
	 * @var array
	 */
	protected $_fields = array();

	/**
	 * Select order
	 * @var string
	 */
	protected $_order = '';

	/**
	 * Select group by
	 * @var string
	 */
	protected $_groupBy = '';

	/**
	 * Select limit
	 * @var string
	 */
	protected $_limit = '';

	/**
	 * Array of joins
	 * @var array
	 */
	protected $_joins = array();

	/**
	 * @param string $table Name of the table
	 *
	 */
	public function __construct($table)
	{
		$this->_table = $table;
	}

	/**
	 * Sets select fields. Keys are for field names, values are for aliases.
	 * @param array $fields
	 */
	public function setFields($fields)
	{
		$this->_fields = $fields;
	}

	public function setOrder($field, $order)
	{
		$this->_order = $field . ' ' . $order;
	}

	/**
	 * Sets group by fields
	 * @param array $fields Field names
	 */
	public function setGroupBy($fields)
	{
		$this->_groupBy = implode(', ', $fields);
	}

	public function setRange($startFrom, $numberOfRows)
	{
		$this->_limit = $startFrom . ', ' . $numberOfRows;
	}

	public function addJoin(O_Sql_IJoin $join)
	{
		$this->_joins[] = $join;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$this->compileComments();
		$this->compileCondition();

		if (count($this->_fields) < 1) {
			$fieldString = $this->_table . '.*';
		} else {
			$fieldString = array();
			foreach ($this->_fields as $field => $alias) {
				if (is_int($field)) {
					$fieldString[] = $alias;
				} else {
					$fieldString[] = $field . ' AS ' . $alias;
				}
			}
			$fieldString = implode(', ', $fieldString);
		}

		$sql = $this->_compiledComments;
		$sql .= 'SELECT ' . $fieldString . ' ';
		$sql .= 'FROM ' . $this->_table . ' ';

		if (count($this->_joins) > 0) {
			foreach ($this->_joins as $eachJoin) {
				$sql .= $eachJoin->__toString().' ';
			}
		}
		if (!empty($this->_compiledConditions))
			$sql .= 'WHERE ' . $this->_compiledConditions;

		if (!empty($this->_groupBy))
			$sql .= ' GROUP BY ' . $this->_groupBy;

		if (!empty($this->_order))
			$sql .= ' ORDER BY ' . $this->_order;

		if (!empty($this->_limit))
			$sql .= ' LIMIT ' . $this->_limit;

		return $sql;
	}
}
