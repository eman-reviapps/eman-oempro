<?php
class O_SubscriberActivityBounce extends O_SubscriberActivity
{
	protected $_bounceType = '';

	public function __construct($id, $name, $type, $statId, $timestamp, $bounceType)
	{
		parent::__construct($id, $name, $type, $statId, $timestamp);

		$this->_bounceType = $bounceType;
	}

	public function getBounceType()
	{
		return $this->_bounceType;
	}
}
