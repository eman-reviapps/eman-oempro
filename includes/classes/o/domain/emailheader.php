<?php
class O_Domain_EmailHeader extends O_Domain_Abstract
{
	protected $_name;
	protected $_emailType;
	protected $_value;

	protected function _init()
	{
		
	}

	public function getName()
	{
		return $this->_name;
	}

	public function setName($name)
	{
		$this->_name = $name;
	}

	public function getEmailType()
	{
		return $this->_emailType;
	}

	public function setEmailType($emailType)
	{
		$this->_emailType = $emailType;
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function setValue($value)
	{
		$this->_value = $value;
	}

	public function __toString()
	{
		return $this->_name.':'.$this->_value;
	}
}
