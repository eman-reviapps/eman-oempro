<?php
class O_Domain_HighriseIntegrationConfig extends O_Domain_Abstract
{
	protected $_account;
	protected $_apiKey;
	protected $_userId;
	protected $_isEnabled;

	protected function _init()
	{
	}

	public function getAccount()
	{
		return $this->_account;
	}

	public function setAccount($account)
	{
		$this->_account = $account;
	}

	public function getApiKey()
	{
		return $this->_apiKey;
	}

	public function setApiKey($apiKey)
	{
		$this->_apiKey = $apiKey;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function isEnabled($value = NULL)
	{
		if (is_null($value)) {
			return $this->_isEnabled;
		} else {
			$this->_isEnabled = $value;
		}
	}

}
