<?php
class O_Domain_LogAutoResponderSend extends O_Domain_Abstract
{
	protected $_relOwnerUserID;
	protected $_relAutoResponderID;
	protected $_relListID;
	protected $_relSubscriberID;
	protected $_EmailAddress;
	protected $_dateTime;

	protected function _init()
	{
		$this->setDateTime(date('Y-m-d H:i:s'));
	}

	public function getRelOwnerUserID()
	{
		return $this->_relOwnerUserID;
	}

	public function setRelOwnerUserID($relOwnerUserID)
	{
		$this->_relOwnerUserID = $relOwnerUserID;
	}

	public function getRelAutoResponderID()
	{
		return $this->_relAutoResponderID;
	}

	public function setRelAutoResponderID($relAutoResponderID)
	{
		$this->_relAutoResponderID = $relAutoResponderID;
	}

	public function getRelListID()
	{
		return $this->_relListID;
	}

	public function setRelListID($relListID)
	{
		$this->_relListID = $relListID;
	}

	public function getRelSubscriberID()
	{
		return $this->_relSubscriberID;
	}

	public function setRelSubscriberID($relSubscriberID)
	{
		$this->_relSubscriberID = $relSubscriberID;
	}

	public function getEmailAddress()
	{
		return $this->_EmailAddress;
	}

	public function setEmailAddress($EmailAddress)
	{
		$this->_EmailAddress = $EmailAddress;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}
}
