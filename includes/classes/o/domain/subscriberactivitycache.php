<?php
class O_Domain_SubscriberActivityCache extends O_Domain_Abstract
{
	protected $_subscriberId;
	protected $_listId;
	protected $_dateTime;
	protected $_cacheContent;

	/**
	 * @var O_SubscriberActivityTimeLine
	 */
	protected $_activityTimeline = NULL;

	protected $_cacheExpireTime = 86400; // a day (in seconds)

	public function isExpired()
	{
		if (time() - strtotime($this->getDateTime()) > $this->_cacheExpireTime)
			return true;

		return false;
	}

	protected function _init()
	{
	}

	public function getSubscriberId()
	{
		return $this->_subscriberId;
	}

	public function setSubscriberId($subscriberId)
	{
		$this->_subscriberId = $subscriberId;
	}

	public function getListId()
	{
		return $this->_listId;
	}

	public function setListId($listId)
	{
		$this->_listId = $listId;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}

	public function getCacheContent()
	{
		return $this->_cacheContent;
	}

	public function setCacheContent($cacheContent)
	{
		$this->_activityTimeline = NULL;
		$this->_cacheContent = $cacheContent;
	}

	public function getActivityTimeline()
	{
		if ($this->_activityTimeline == NULL)
			$this->_buildActivityTimeline();
		return $this->_activityTimeline;
	}

	protected function _buildActivityTimeline()
	{
		$timeline = new O_SubscriberActivityTimeLine();
		$activityArray = unserialize($this->_cacheContent);
		foreach ($activityArray as $eachActivityRow) {
			$timeline->addActivity(O_SubscriberActivityFactory::createFromDb(
				$eachActivityRow
			));
		}

		$this->_activityTimeline = $timeline;
	}
}
