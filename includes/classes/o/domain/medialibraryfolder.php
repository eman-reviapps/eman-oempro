<?php
class O_Domain_MediaLibraryFolder extends O_Domain_Abstract
{
	protected $_userId;
	protected $_name;
	protected $_folderId;

	protected function _init()
	{
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function setName($name)
	{
		$this->_name = $name;
	}

	public function getFolderId()
	{
		return $this->_folderId;
	}

	public function setFolderId($folderId)
	{
		$this->_folderId = $folderId;
	}

	public function getUrl()
	{
		return '';
	}
}
