<?php
class O_Domain_WufooIntegrationLog extends O_Domain_Abstract
{
	protected $_userId;
	protected $_listId;
	protected $_integrationId;
	protected $_message;
	protected $_status;
	protected $_dateTime;

	protected function _init()
	{
		$this->setDateTime(date('Y-m-d H:i:s'));
	}

	public function setUserId($id)
	{
		$this->_userId = $id;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setListId($id)
	{
		$this->_listId = $id;
	}

	public function getListId()
	{
		return $this->_listId;
	}

	public function setIntegrationId($id)
	{
		$this->_integrationId = $id;
	}

	public function getIntegrationId()
	{
		return $this->_integrationId;
	}

	public function setMessage($message)
	{
		$this->_message = $message;
	}

	public function getMessage()
	{
		return $this->_message;
	}

	public function setStatus($status)
	{
		$this->_status = (int) $status;
	}

	public function getStatus()
	{
		return $this->_status;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}
}
