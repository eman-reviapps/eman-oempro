<?php
abstract class O_Domain_Abstract
{
	protected $_id = 0;
	protected $_rawDbColumns = array();

	abstract protected function _init();
	public function __construct($id = 0)
	{
		$this->_id = $id;
		$this->_init();
	}
	public function setId($id)
	{
		$this->_id = $id;
	}
	public function getId()
	{
		return $this->_id;
	}

	public function getRawDbColumns()
	{
		return $this->_rawDbColumns;
	}

	public function setRawDbColumns($data)
	{
		$this->_rawDbColumns = $data;
	}
}
