<?php
class O_Domain_APIKey extends O_Domain_Abstract
{
	protected $_apiKey;
	protected $_userId;
	protected $_note;
	protected $_ipAddress;

	protected function _init()
	{
		
	}

	public function getAPIKey()
	{
		return $this->_apiKey;
	}

	public function setAPIKey($key)
	{
		$this->_apiKey = $key;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getNote()
	{
		return $this->_note;
	}

	public function setNote($note)
	{
		$this->_note = $note;
	}

	public function getIPAddress()
	{
		return $this->_ipAddress;
	}

	public function setIPAddress($ipAddress)
	{
		$this->_ipAddress = $ipAddress;
	}
}
