<?php
class O_Domain_SuppressionPattern extends O_Domain_Abstract
{
	protected $_pattern;
	protected $_type;
	protected $_description;

	protected function _init()
	{
	}

	public function getPattern()
	{
		return $this->_pattern;
	}

	public function setPattern($pattern)
	{
		$this->_pattern = $pattern;
	}

	public function getType()
	{
		return $this->_type;
	}

	public function setType($type)
	{
		$this->_type = $type;
	}

	public function getDescription()
	{
		return $this->_description;
	}

	public function setDescription($description)
	{
		$this->_description = $description;
	}

}
