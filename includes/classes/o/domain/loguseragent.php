<?php
class O_Domain_LogUserAgent extends O_Domain_Abstract
{
	protected $_userAgentString;
	protected $_referrer;
	protected $_relListID;
	protected $_relSubscriberID;
	protected $_relCampaignID;
	protected $_relAutoResponderID;
	protected $_dateTime;

	protected function _init()
	{
		
	}

	public function getUserAgentString()
	{
		return $this->_userAgentString;
	}

	public function setUserAgentString($userAgentString)
	{
		$this->_userAgentString = $userAgentString;
	}

	public function getReferrer()
	{
		return $this->_referrer;
	}

	public function setReferrer($referrer)
	{
		$this->_referrer = $referrer;
	}

	public function getRelListID()
	{
		return $this->_relListID;
	}

	public function setRelListID($relListID)
	{
		$this->_relListID = $relListID;
	}

	public function getRelSubscriberID()
	{
		return $this->_relSubscriberID;
	}

	public function setRelSubscriberID($relSubscriberID)
	{
		$this->_relSubscriberID = $relSubscriberID;
	}

	public function getRelCampaignID()
	{
		return $this->_relCampaignID;
	}

	public function setRelCampaignID($relCampaignID)
	{
		$this->_relCampaignID = $relCampaignID;
	}

	public function getRelAutoResponderID()
	{
		return $this->_relAutoResponderID;
	}

	public function setRelAutoResponderID($relAutoResponderID)
	{
		$this->_relAutoResponderID = $relAutoResponderID;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}
}
