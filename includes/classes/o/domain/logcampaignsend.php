<?php
class O_Domain_LogCampaignSend extends O_Domain_Abstract
{
	protected $_relOwnerUserID;
	protected $_relCampaignID;
	protected $_relListID;
	protected $_relSegmentID;
	protected $_relSubscriberID;
	protected $_EmailAddress;
	protected $_dateTime;

	protected function _init()
	{
		$this->setDateTime(date('Y-m-d H:i:s'));
	}

	public function getRelOwnerUserID()
	{
		return $this->_relOwnerUserID;
	}

	public function setRelOwnerUserID($relOwnerUserID)
	{
		$this->_relOwnerUserID = $relOwnerUserID;
	}

	public function getRelCampaignID()
	{
		return $this->_relCampaignID;
	}

	public function setRelCampaignID($relCampaignID)
	{
		$this->_relCampaignID = $relCampaignID;
	}

	public function getRelListID()
	{
		return $this->_relListID;
	}

	public function setRelListID($relListID)
	{
		$this->_relListID = $relListID;
	}

	public function getRelSegmentID()
	{
		return $this->_relSegmentID;
	}

	public function setRelSegmentID($relSegmentID)
	{
		$this->_relSegmentID = $relSegmentID;
	}

	public function getRelSubscriberID()
	{
		return $this->_relSubscriberID;
	}

	public function setRelSubscriberID($relSubscriberID)
	{
		$this->_relSubscriberID = $relSubscriberID;
	}

	public function getEmailAddress()
	{
		return $this->_EmailAddress;
	}

	public function setEmailAddress($EmailAddress)
	{
		$this->_EmailAddress = $EmailAddress;
	}

	public function getDateTime()
	{
		return $this->_dateTime;
	}

	public function setDateTime($dateTime)
	{
		$this->_dateTime = $dateTime;
	}
}
