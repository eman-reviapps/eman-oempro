<?php
class O_Domain_Suppression extends O_Domain_Abstract
{
	protected $_listId = '';
	protected $_userId = '';
	protected $_emailAddress = '';
	protected $_source = '';

	protected function _init()
	{
	}

	public function getListId()
	{
		return $this->_listId;
	}

	public function setListId($listId)
	{
		$this->_listId = $listId;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getEmailAddress()
	{
		return $this->_emailAddress;
	}

	public function setEmailAddress($emailAddress)
	{
		$this->_emailAddress = $emailAddress;
	}

	public function getSource()
	{
		return $this->_source;
	}

	public function setSource($source)
	{
		$this->_source = $source;
	}


}