<?php
class O_Domain_WufooIntegration extends O_Domain_Abstract
{
	protected $_user;
	protected $_userId;
	protected $_list;
	protected $_listId;
	protected $_handshakeKey = NULL;
	protected $_wufooSubDomain;
	protected $_apiKey;
	protected $_formHash;
	protected $_formName;
	protected $_fieldMapping;
	protected $_webhookHash;
	protected $_parsedFieldMapping = NULL;
	protected $_successfulLogs = 0;

	protected function _init()
	{
	}

	public function setSuccessfulLogCount($count)
	{
		$this->_successfulLogs = $count;
	}

	public function getSuccessfulLogCount()
	{
		return $this->_successfulLogs;
	}

	public function setUserId($id)
	{
		$this->_userId = $id;
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setListId($id)
	{
		$this->_listId = $id;
	}

	public function getListId()
	{
		return $this->_listId;
	}

	public function setHandshakeKey($hsk)
	{
		$this->_handshakeKey = $hsk;
	}

	public function getHandshakeKey()
	{
		return $this->_handshakeKey;
	}

	public function setWufooSubDomain($domain)
	{
		$this->_wufooSubDomain = $domain;
	}

	public function getWufooSubDomain()
	{
		return $this->_wufooSubDomain;
	}

	public function setApiKey($key)
	{
		$this->_apiKey = $key;
	}

	public function getApiKey()
	{
		return $this->_apiKey;
	}

	public function setFormHash($hash)
	{
		$this->_formHash = $hash;
	}

	public function getFormHash()
	{
		return $this->_formHash;
	}

	public function setFormName($name)
	{
		$this->_formName = $name;
	}

	public function getFormName()
	{
		return $this->_formName;
	}

	public function setFieldMapping($mappingString)
	{
		$this->_fieldMapping = $mappingString;
	}

	public function getFieldMapping($asString = FALSE)
	{
		if ($asString) return $this->_fieldMapping;

		if (is_null($this->_parsedFieldMapping))
			$this->_parseFieldMapping();
		return $this->_parsedFieldMapping;
	}

	protected function _parseFieldMapping()
	{
		$this->_parsedFieldMapping = array();
		$tmp = explode(",*,*,", $this->_fieldMapping);
		foreach ($tmp as $each) {
			$mappingRow = substr(trim($each), 3, -3);
			$mappingRow = explode('=>', $mappingRow);
			$oemproField = $mappingRow[0];
			$wufooField = $mappingRow[1];
			$wufooFieldType = $mappingRow[2];

			if (preg_match('/(Field[\d]+?,)+/', $wufooField)) {
				$wufooField = explode(',', $wufooField);
			}

			$this->_parsedFieldMapping[$oemproField] = array($wufooField,
															 $wufooFieldType);
		}
	}

	public function getWebhookHash()
	{
		return $this->_webhookHash;
	}

	public function setWebhookHash($webhookHash)
	{
		$this->_webhookHash = $webhookHash;
	}
}
