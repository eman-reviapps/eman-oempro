<?php
class O_Domain_LogFlood extends O_Domain_Abstract
{
	protected $_zone;
	protected $_time;
	protected $_ipAddress;
	protected $_isBlocked = false;
	protected $_blockTime = 0;
	protected $_count = 0;

	protected function _init()
	{

	}

	public function getZone()
	{
		return $this->_zone;
	}

	public function setZone($zone)
	{
		$this->_zone = $zone;
	}

	public function getTime()
	{
		return $this->_time;
	}

	public function setTime($time)
	{
		$this->_time = $time;
	}

	public function getIpAddress()
	{
		return $this->_ipAddress;
	}

	public function setIpAddress($ipAddress)
	{
		$this->_ipAddress = $ipAddress;
	}

	public function isBlocked()
	{
		return $this->_isBlocked;
	}

	public function setIsBlocked($isBlocked)
	{
		$this->_isBlocked = $isBlocked;
	}

	public function getBlockTime()
	{
		return $this->_blockTime;
	}

	public function setBlockTime($blockTime)
	{
		$this->_blockTime = $blockTime;
	}

	public function getCount()
	{
		return $this->_count;
	}

	public function setCount($count)
	{
		$this->_count = $count;
	}

	public function isBlockExpired($period, $now)
	{
		if ($now - $this->_blockTime > $period)
			return true;

		return false;
	}

	public function isTimeExpired($period, $now)
	{
		if ($now - $this->_time > $period)
			return true;

		return false;
	}

	public function block($time)
	{
		$this->setIsBlocked(true);
		$this->setBlockTime($time);
	}

	public function reset($time)
	{
		$this->setIsBlocked(false);
		$this->setBlockTime(0);
		$this->setCount(1);
		$this->setTime($time);
	}
}