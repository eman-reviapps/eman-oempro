<?php
class O_Domain_MediaLibraryFile extends O_Domain_Abstract
{
	protected $_userId;
	protected $_data;
	protected $_type;
	protected $_size;
	protected $_name;
	protected $_folderId;
	protected $_url;

	protected function _init()
	{
	}

	public function getUserId()
	{
		return $this->_userId;
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function getData()
	{
		return $this->_data;
	}

	public function setData($data)
	{
		$this->_data = $data;
	}

	public function getType()
	{
		return $this->_type;
	}

	public function setType($type)
	{
		$this->_type = $type;
	}

	public function getSize()
	{
		return $this->_size;
	}

	public function setSize($size)
	{
		$this->_size = $size;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function setName($name)
	{
		$this->_name = $name;
	}

	public function getFolderId()
	{
		return $this->_folderId;
	}

	public function setFolderId($folderId)
	{
		$this->_folderId = $folderId;
	}

	public function getUrl($baseUrl)
	{
		if (empty($this->_url)) {
			if ($this->getType() == 's3') {
				$this->_url = S3_URL.'/'.S3_MEDIALIBRARY_PATH.'/'.md5($this->getUserId()).'/'.md5($this->getId());
			} else {
				$this->_url = $baseUrl.'/public/file/view/'.Core::EncryptNumberAdvanced($this->getUserId()).'/'.Core::EncryptNumberAdvanced($this->getId());
			}
		}

		return $this->_url;
	}

	public function isImage()
	{
		return preg_match('/^image\/.*$/', $this->getType());
	}
}
