<?php
class O_DatabaseQueryResult
{
	protected $_resultResource = NULL;
	public $num_rows = 0;

	public function __construct($resultResource)
	{
		$this->_resultResource = $resultResource;
		$this->num_rows = mysql_num_rows($this->_resultResource);
	}

	public function fetch_assoc()
	{
		return mysql_fetch_assoc($this->_resultResource);
	}
}