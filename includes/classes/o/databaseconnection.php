<?php
class O_DatabaseConnection
{
	protected $_resource = NULL;

	public function __construct($resource)
	{
		$this->_resource = $resource;
	}

	public function insert($query)
	{
		$result = mysql_query($query, $this->_resource);
		return mysql_insert_id($this->_resource);
	}

	/**
	 * @param O_Sql_Query $query
	 * @return O_DatabaseQueryResult|int
	 */
	public function query(O_Sql_Query $query)
	{
		$result = mysql_query($query->__toString(), $this->_resource);

		if ($result === FALSE) {
			Core::RecordToLogFile(
				'MySQL', $query->__toString()."\n\n".mysql_error($this->_resource)
				, '', '');
			throw new Exception(mysql_error($this->_resource));
			exit;
		}

		if ($query instanceof O_Sql_InsertQuery) {
			return mysql_insert_id($this->_resource);
		} else {
			return new O_DatabaseQueryResult($result);
		}
	}


	public function real_escape_string($value)
	{
		return mysql_real_escape_string($value, $this->_resource);
	}
}