<?php
/**
 * An autoloader class for auto loading O_ classes
 */
class O_AutoLoader
{
	/**
	 * Class path
	 * @var string
	 */
	protected $_classPath = '';
	
	/**
	 * Constructor method
	 * @param string $classPath Class path
	 */
	public function __construct($classPath)
	{
		$this->_classPath = $classPath;
		$this->_registerAutoLoader();
	}

	/**
	 * Registers an autoload method
	 */
	protected function _registerAutoLoader()
	{
		spl_autoload_register(array($this, 'loadClass'));
	}
	
	/**
	 * Class loader method
	 * @param string $className Class name
	 */
	public function loadClass($className)
	{
		$folders = explode('_', strtolower($className));
		$file = array_pop($folders);

		$pathToFile = rtrim($this->_classPath, '/')
			.DIRECTORY_SEPARATOR
			.implode(DIRECTORY_SEPARATOR, $folders)
			.DIRECTORY_SEPARATOR
			.strtolower($file)
			.'.php';

		if (! file_exists($pathToFile)) return false;
		include $pathToFile;
	}
}