<?php
abstract class O_Mapper_Abstract
{
	protected $_db;
	protected $_cache = array();

	public function __construct($db)
	{
		$this->_db = $db;
	}

	/**
	 * @abstract
	 * @return O_Sql_SelectQuery
	 */
	abstract protected function _getSqlQueryForFindById($id);

	abstract protected function _constructObject($dbRowArray);

	/**
	 * @throws O_Finder_Exception
	 * @param int $id
	 * @return bool|O_Domain_Abstract
	 */
	public function findById($id)
	{
		if (array_key_exists($id, $this->_cache))
			return $this->_cache[$id];

		$query = $this->_getSqlQueryForFindById($id);
		$rs = $this->_db->query($query);

		if ($rs->num_rows < 1) return FALSE;

		$object = $this->_constructObject($rs->fetch_assoc());

		$this->_cache[$id] = $object;

		return $object;
	}
}
