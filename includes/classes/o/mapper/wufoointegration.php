<?php
class O_Mapper_WufooIntegration extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_wufoo_integrations');
		$query->setCondition(new O_Sql_Condition(
			'IntegrationID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$integration = new O_Domain_WufooIntegration($dbRowArray['IntegrationID']);
		$integration->setRawDbColumns($dbRowArray);
		$integration->setUserId($dbRowArray['RelOwnerUserID']);
		$integration->setListId($dbRowArray['RelListID']);
		$integration->setHandshakeKey($dbRowArray['HandshakeKey']);
		$integration->setWufooSubDomain($dbRowArray['SubDomain']);
		$integration->setApiKey($dbRowArray['ApiKey']);
		$integration->setFormHash($dbRowArray['FormHash']);
		$integration->setFormName($dbRowArray['FormName']);
		$integration->setFieldMapping($dbRowArray['FieldMapping']);
		$integration->setWebhookHash($dbRowArray['WebhookHash']);
		return $integration;
	}

	public function insert(O_Domain_WufooIntegration $obj)
	{
		$sql = 'INSERT INTO oempro_wufoo_integrations (RelOwnerUserID, RelListID, SubDomain, ApiKey, FormHash, FormName, FieldMapping)';
		$sql .= ' VALUES (' . $obj->getUserId() . ',' . $obj->getListId() . ',"' . $obj->getWufooSubDomain() . '","' . $obj->getApiKey() . '","' . $obj->getFormHash() . '","' . $obj->getFormName() . '","' . $obj->getFieldMapping(TRUE) . '")';
		$id = $this->_db->insert($sql);
		$obj->setId($id);
		$this->updateHandshakeKey($obj);
	}

	public function updateHandshakeKey(O_Domain_WufooIntegration $obj)
	{
		$obj->setHandshakeKey(new O_Integration_Wufoo_HandshakeKey(
			$obj->getUserId(),
			$obj->getListId(),
			$obj->getId(),
			$obj->getFormHash()
		));

		$hskey = $obj->getHandshakeKey();

		$query = new O_Sql_UpdateQuery('oempro_wufoo_integrations');
		$query->addComment('Wufoo integration update handshake key query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setFields(array(
							   'HandshakeKey' => $hskey->__toString()
						  ));
		$query->setCondition(new O_Sql_Condition(
			'IntegrationID', '=', $obj->getId()
		));

		$rs = $this->_db->query($query);
	}

	public function updateWebhookHash(O_Domain_WufooIntegration $obj)
	{
		$query = new O_Sql_UpdateQuery('oempro_wufoo_integrations');
		$query->addComment('Wufoo integration update webhook hash query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setFields(array(
							   'WebhookHash' => $obj->getWebhookHash()
						  ));
		$query->setCondition(new O_Sql_Condition(
			'IntegrationID', '=', $obj->getId()
		));
		$rs = $this->_db->query($query);
	}

	public function findAllByUserId($userId)
	{
		$integrationLogMapper = O_Registry::instance()->getMapper('WufooIntegrationLog');

		$query = new O_Sql_SelectQuery('oempro_wufoo_integrations');
		$query->addComment('Wufoo integration find all integrations by user id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition(
			'RelOwnerUserID', '=', $userId
		));

		$rs = $this->_db->query($query);

		$integrations = array();
		if ($rs->num_rows > 0) {
			while ($row = $rs->fetch_assoc()) {
				$integration = $this->_constructObject($row);
				$integration->setSuccessfulLogCount(
					$integrationLogMapper->countSuccessfulByIntegrationId(
						$integration->getId()));
				$integrations[] = $integration;
			}
		}

		return $integrations;
	}

	public function getLastIntegration($userId)
	{
		$query = new O_Sql_SelectQuery('oempro_wufoo_integrations');
		$query->addComment('Wufoo integration get last integration');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('RelOwnerUserID', '=', (int) $userId));
		$query->setOrder('IntegrationID', 'DESC');
		$query->setRange(0, 1);

		$rs = $this->_db->query($query);

		if ($rs->num_rows < 1) return FALSE;

		return $this->_constructObject($rs->fetch_assoc());
	}

	public function delete($id, $userId)
	{
		$query = new O_Sql_DeleteQuery('oempro_wufoo_integrations');
		$query->addComment('Wufoo integration delete integration by id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('IntegrationID', '=', (int) $id));

		$integrationLogMapper = O_Registry::instance()->getMapper('WufooIntegrationLog');
		$integrationLogMapper->deleteByIntegrationId($id);

		$rs = $this->_db->query($query);
	}
}
