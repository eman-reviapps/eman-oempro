<?php
class O_Mapper_LogFlood extends O_Mapper_Abstract implements O_Security_Flood_LogMapperInterface
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_log_flood_detection');
		$query->setCondition(new O_Sql_Condition(
				'LogID', '=', $id
			));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$log = new O_Domain_LogFlood($dbRowArray['LogID']);
		$log->setZone($dbRowArray['Zone']);
		$log->setIpAddress($dbRowArray['IPAddress']);
		$log->setIsBlocked($dbRowArray['IsBlocked'] == 1 ? true : false);
		$log->setCount($dbRowArray['Count']);
		$log->setTime($dbRowArray['Time']);
		$log->setBlockTime($dbRowArray['BlockTime']);
		return $log;
	}

	public function insert(O_Domain_LogFlood $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_log_flood_detection');
		$query->addComment('Flood log insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
			'Zone' => $obj->getZone(),
			'IPAddress' => $obj->getIpAddress(),
			'IsBlocked' => $obj->isBlocked() ? 1 : 0,
			'Count' => $obj->getCount(),
			'Time' => $obj->getTime(),
			'BlockTime' => $obj->getBlockTime()
		));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function findByIPandZone($zoneName, $ipAddress)
	{
		$query = new O_Sql_SelectQuery('oempro_log_flood_detection');
		$query->addComment('Flood log find log by zone and ip address');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$zoneCondition = new O_Sql_Condition('Zone', '=', $zoneName);
		$ipCondition = new O_Sql_Condition('IPAddress', '=', $ipAddress);

		$queryCondition = new O_Sql_ConditionGroup('AND');
		$queryCondition->add($zoneCondition);
		$queryCondition->add($ipCondition);

		$query->setCondition($queryCondition);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			return $this->_constructObject($rs->fetch_assoc());
		} else {
			return FALSE;
		}
	}

	public function update(O_Domain_LogFlood $obj)
	{
		$query = new O_Sql_UpdateQuery('oempro_log_flood_detection');
		$query->addComment('Flood log update query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setFields(array(
			'Zone' => $obj->getZone(),
			'IPAddress' => $obj->getIpAddress(),
			'IsBlocked' => $obj->isBlocked() ? 1 : 0,
			'Count' => $obj->getCount(),
			'Time' => $obj->getTime(),
			'BlockTime' => $obj->getBlockTime()
		));

		$zoneCondition = new O_Sql_Condition('Zone', '=', $obj->getZone());
		$ipCondition = new O_Sql_Condition('IPAddress', '=', $obj->getIpAddress());

		$queryCondition = new O_Sql_ConditionGroup('AND');
		$queryCondition->add($zoneCondition);
		$queryCondition->add($ipCondition);

		$query->setCondition($queryCondition);
		$rs = $this->_db->query($query);
	}
}
