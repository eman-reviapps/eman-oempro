<?php
class O_Mapper_HighriseIntegrationConfig extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_highrise_integration_config');
		$query->setCondition(new O_Sql_Condition(
			'ConfigID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$config = new O_Domain_HighriseIntegrationConfig($dbRowArray['ConfigID']);
		$config->setRawDbColumns($dbRowArray);
		$config->setAccount($dbRowArray['Account']);
		$config->setApiKey($dbRowArray['APIKey']);
		$config->setUserId($dbRowArray['RelOwnerUserID']);
		$config->isEnabled($dbRowArray['IsEnabled'] == '1' ? true : false);
		return $config;
	}

	public function insert(O_Domain_HighriseIntegrationConfig $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_highrise_integration_config');
		$query->addComment('Highrise integration config insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'RelOwnerUserID' => $obj->getUserId(),
										'Account' => $obj->getAccount(),
										'APIKey' => $obj->getApiKey(),
										'IsEnabled' => 1
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function findByUserId($userId)
	{
		$query = new O_Sql_SelectQuery('oempro_highrise_integration_config');
		$query->addComment('Highrise integration find config by user id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition(
			'RelOwnerUserID', '=', $userId, FALSE
		));

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			return $this->_constructObject($rs->fetch_assoc());
		} else {
			return FALSE;
		}
	}

	public function update(O_Domain_HighriseIntegrationConfig $obj)
	{
		$query = new O_Sql_UpdateQuery('oempro_highrise_integration_config');
		$query->addComment('Highrise integration config update query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setFields(array(
							   'Account' => $obj->getAccount(),
							   'APIKey' => $obj->getApiKey(),
							   'IsEnabled' => $obj->isEnabled() ? 1 : 0
						  ));
		$query->setCondition(new O_Sql_Condition(
			'ConfigID', '=', $obj->getId()
		));
		$rs = $this->_db->query($query);
	}

	public function deleteByUserId($userId)
	{
		$query = new O_Sql_DeleteQuery('oempro_highrise_integration_log');
		$query->addComment('Highrise integration config delete by user id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('RelOwnerUserID', '=', (int) $userId));

		$rs = $this->_db->query($query);
	}
}
