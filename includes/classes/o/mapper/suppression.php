<?php
class O_Mapper_Suppression extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_suppression_list');
		$query->setCondition(new O_Sql_Condition(
			'SuppressionID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$cache = new O_Domain_Suppression($dbRowArray['SuppressionID']);
		$cache->setListId($dbRowArray['RelListID']);
		$cache->setUserId($dbRowArray['RelOwnerUserID']);
		$cache->setEmailAddress($dbRowArray['EmailAddress']);
		$cache->setSource($dbRowArray['SuppressionSource']);
		return $cache;
	}

	public function findByEmailAddress($emailAddress)
	{
		$query = new O_Sql_SelectQuery('oempro_suppression_list');
		$condition = new O_Sql_Condition('EmailAddress', 'LIKE', $emailAddress);
		$query->setCondition($condition);
		$query->setOrder('SuppressionSource', 'ASC');

		$rs = $this->_db->query($query);

		if ($rs->num_rows < 0)
			return false;

		$resultArray = array();
		while ($row = $rs->fetch_assoc()) {
			$resultArray[] = $this->_constructObject($row);
		}
		return $resultArray;
	}

	public function getRatiosBySource()
	{
		$query = new O_Sql_SelectQuery('oempro_suppression_list');
		$query->setFields(array(
			'(SELECT COUNT(*) FROM oempro_suppression_list)' => 'Total',
			'COUNT(*)' => 'SourceTotal',
			'((100 * COUNT(*)) / (SELECT COUNT(*) FROM oempro_suppression_list))' => 'Percent',
			'SuppressionSource' => 'Source'
		));
		$query->setGroupBy(array('SuppressionSource'));
		$query->setOrder('SourceTotal', 'DESC');

		$rs = $this->_db->query($query);

		if ($rs->num_rows < 0)
			return FALSE;

		$resultArray = array();

		while ($row = $rs->fetch_assoc()) {
			$resultArray[] = $row;
		}

		return $resultArray;
	}

	public function insertSystemLevel($emailAddresses = array())
	{
		$totalEmailAddressesInserted = 0;
		foreach ($emailAddresses as $each) {
			$query = new O_Sql_SelectQuery('oempro_suppression_list');

			$condition1 = new O_Sql_Condition('RelListID', '=', 0, false);
			$condition2 = new O_Sql_Condition('RelOwnerUserID', '=', 0, false);
			$condition3 = new O_Sql_Condition('EmailAddress', '=', $each);

			$conditionGroup = new O_Sql_ConditionGroup('AND');
			$conditionGroup->add($condition1);
			$conditionGroup->add($condition2);
			$conditionGroup->add($condition3);

			$query->setCondition($conditionGroup);

			$rs = $this->_db->query($query);

			if ($rs->num_rows > 0)
				continue;

			$insertQuery = new O_Sql_InsertQuery('oempro_suppression_list');
			$insertQuery->setFieldsAndValues(array(
				'RelListID' => 0,
				'RelOwnerUserID' => 0,
				'SuppressionSource' => 'Administrator',
				'EmailAddress' => $each
			));
			$this->_db->query($insertQuery);
			$totalEmailAddressesInserted++;
		}
		return $totalEmailAddressesInserted;
	}

	public function deleteById($suppressionId)
	{
		$query = new O_Sql_DeleteQuery('oempro_suppression_list');
		$query->addComment('Delete suppression by suppression id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('SuppressionID', '=', (int) $suppressionId));

		$rs = $this->_db->query($query);
	}

	public function deleteByEmailAddressAndListId($emailAddress, $listId)
	{
		$query = new O_Sql_DeleteQuery('oempro_suppression_list');
		$query->addComment('Delete suppression by suppression id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition1 = new O_Sql_Condition('EmailAddress', '=', $emailAddress);
		$condition2 = new O_Sql_Condition('RelListID', '=', $listId, false);

		$conditionGroup = new O_Sql_ConditionGroup('AND');
		$conditionGroup->add($condition1);
		$conditionGroup->add($condition2);


		$query->setCondition($conditionGroup);

		$rs = $this->_db->query($query);
	}


}
