<?php
class O_Mapper_LogUserAgents extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_log_user_agents');
		$query->setCondition(new O_Sql_Condition(
			'LogID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$log = new O_Domain_LogUserAgent();

		$log->setUserAgentString($dbRowArray['UserAgentString']);
		$log->setReferrer($dbRowArray['Referrer']);
		$log->setRelListID($dbRowArray['RelListID']);
		$log->setRelSubscriberID($dbRowArray['RelSubscriberID']);
		$log->setRelCampaignID($dbRowArray['RelCampaignID']);
		$log->setRelAutoResponderID($dbRowArray['RelAutoResponderID']);
		$log->setDateTime($dbRowArray['LogDateTime']);

		return $log;
	}

	public function insert(O_Domain_LogUserAgent $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_log_user_agents');
		$query->addComment('User agent log insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'UserAgentString' => $this->_db->real_escape_string($obj->getUserAgentString()),
										'Referrer' => $this->_db->real_escape_string($obj->getReferrer()),
										'RelListID' => $this->_db->real_escape_string($obj->getRelListID()),
										'RelSubscriberID' => $this->_db->real_escape_string($obj->getRelSubscriberID()),
										'RelCampaignID' => $this->_db->real_escape_string($obj->getRelCampaignID()),
										'RelAutoResponderID' => $this->_db->real_escape_string($obj->getRelAutoResponderID()),
										'LogDateTime' => $this->_db->real_escape_string($obj->getDateTime())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}
}
