<?php
class O_Mapper_SubscriberActivityCache extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_subscriber_activity_cache');
		$query->setCondition(new O_Sql_Condition(
			'CacheID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$cache = new O_Domain_SubscriberActivityCache($dbRowArray['CacheID']);
		$cache->setRawDbColumns($dbRowArray);
		$cache->setListId($dbRowArray['RelListID']);
		$cache->setSubscriberId($dbRowArray['RelSubscriberID']);
		$cache->setDateTime($dbRowArray['DateTime']);
		$cache->setCacheContent($dbRowArray['CacheContent']);
		return $cache;
	}

	/**
	 * @param int $listId
	 * @param int $subscriberId
	 * @return O_Domain_SubscriberActivityCache
	 */
	public function findByListIdAndSubscriberId($listId, $subscriberId)
	{
		$query = new O_Sql_SelectQuery('oempro_subscriber_activity_cache');
		$query->addComment('Subscriber activity cache find by list id and subscriber id');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$listCondition = new O_Sql_Condition('RelListID', '=', $listId, FALSE);
		$subscriberCondition = new O_Sql_Condition('RelSubscriberID', '=',
			$subscriberId, FALSE);

		$conditionGroup = new O_Sql_ConditionGroup();
		$conditionGroup->add($listCondition);
		$conditionGroup->add($subscriberCondition);

		$query->setCondition($conditionGroup);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$cache = $this->_constructObject($rs->fetch_assoc());
			if ($cache->isExpired())
				$this->_buildAndUpdate($cache);
		} else {
			$cache = $this->_insertEmptyCache($listId, $subscriberId);
			$this->_buildAndUpdate($cache);
		}

		return $cache;
	}

	protected function _insertEmptyCache($listId, $subscriberId)
	{
		$cache = new O_Domain_SubscriberActivityCache();
		$cache->setCacheContent('');
		$cache->setDateTime(date('Y-m-d H:i:s'));
		$cache->setListId($listId);
		$cache->setSubscriberId($subscriberId);

		$this->insert($cache);

		return $cache;
	}

	protected function _buildAndUpdate(O_Domain_SubscriberActivityCache $cache)
	{
		$listId = $cache->getListId();
		$subscriberId = $cache->getSubscriberId();

		$joinCampaign = O_Sql_Join::left('oempro_campaigns AS cmps',
			new O_Sql_Condition('cmps.CampaignID', '=', 'stats.RelCampaignID', FALSE));

		$joinAutoResponder = O_Sql_Join::left('oempro_auto_responders AS rspndrs',
			new O_Sql_Condition('rspndrs.AutoResponderID', '=', 'stats.RelAutoResponderID', FALSE));

		$conditionList = new O_Sql_Condition('stats.RelListID', '=', $listId, FALSE);
		$conditionSubscriber = new O_Sql_Condition('stats.RelSubscriberID', '=', $subscriberId, FALSE);
		$conditionGroup = new O_Sql_ConditionGroup();
		$conditionGroup->add($conditionList);
		$conditionGroup->add($conditionSubscriber);

		// Open activity query
		$openActivityQuery = new O_Sql_SelectQuery('oempro_stats_open AS stats');
		$openActivityQuery->setFields(
			array(
				 '"open"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.OpenDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.OpenID as StatID',
				 '"-"' => 'BounceType',
				 '"--"' => 'LinkTitle',
				 '"---"' => 'LinkURL'
			));
		$openActivityQuery->addJoin($joinCampaign);
		$openActivityQuery->addJoin($joinAutoResponder);
		$openActivityQuery->setCondition($conditionGroup);

		// Click activity query
		$clickActivityQuery = new O_Sql_SelectQuery('oempro_stats_link AS stats');
		$clickActivityQuery->setFields(
			array(
				 '"click"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.ClickDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.LinkTrackID as StatID',
				 '"-"' => 'BounceType',
				'stats.LinkTitle',
				'stats.LinkURL'
			));
		$clickActivityQuery->addJoin($joinCampaign);
		$clickActivityQuery->addJoin($joinAutoResponder);
		$clickActivityQuery->setCondition($conditionGroup);

		// Bounce activity query
		$bounceActivityQuery = new O_Sql_SelectQuery('oempro_stats_bounce AS stats');
		$bounceActivityQuery->setFields(
			array(
				 '"bounce"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.BounceDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.StatID',
				 'stats.BounceType',
				 '"-"' => 'LinkTitle',
				 '"--"' => 'LinkURL'
			));
		$bounceActivityQuery->addJoin($joinCampaign);
		$bounceActivityQuery->addJoin($joinAutoResponder);
		$bounceActivityQuery->setCondition($conditionGroup);

		// Forward activity query
		$forwardActivityQuery = new O_Sql_SelectQuery('oempro_stats_forward AS stats');
		$forwardActivityQuery->setFields(
			array(
				 '"forward"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.ForwardDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.StatID',
				 '"-"' => 'BounceType',
				 '"--"' => 'LinkTitle',
				 '"---"' => 'LinkURL'
			));
		$forwardActivityQuery->addJoin($joinCampaign);
		$forwardActivityQuery->addJoin($joinAutoResponder);
		$forwardActivityQuery->setCondition($conditionGroup);

		// Browser view activity query
		$browserViewActivityQuery = new O_Sql_SelectQuery('oempro_stats_browserview AS stats');
		$browserViewActivityQuery->setFields(
			array(
				 '"browserView"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.ViewDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.StatID',
				 '"-"' => 'BounceType',
				 '"--"' => 'LinkTitle',
				 '"---"' => 'LinkURL'
			));
		$browserViewActivityQuery->addJoin($joinCampaign);
		$browserViewActivityQuery->addJoin($joinAutoResponder);
		$browserViewActivityQuery->setCondition($conditionGroup);

		// Unsubscription activity query
		$unsubscriptionActivityQuery = new O_Sql_SelectQuery('oempro_stats_unsubscription AS stats');
		$unsubscriptionActivityQuery->setFields(
			array(
				 '"unsubscription"' => 'ActivityType',
				 'UNIX_TIMESTAMP(stats.UnsubscriptionDate)' => 'Date',
				 'stats.RelCampaignID',
				 'cmps.CampaignName',
				 'stats.RelAutoResponderID',
				 'rspndrs.AutoResponderName',
				 'stats.StatID',
				 '"-"' => 'BounceType',
				 '"--"' => 'LinkTitle',
				 '"---"' => 'LinkURL'
			));
		$unsubscriptionActivityQuery->addJoin($joinCampaign);
		$unsubscriptionActivityQuery->addJoin($joinAutoResponder);
		$unsubscriptionActivityQuery->setCondition($conditionGroup);

		$unionQuery = new O_Sql_UnionQuery();
		$unionQuery->addSelectQuery($openActivityQuery);
		$unionQuery->addSelectQuery($clickActivityQuery);
		$unionQuery->addSelectQuery($forwardActivityQuery);
		$unionQuery->addSelectQuery($bounceActivityQuery);
		$unionQuery->addSelectQuery($browserViewActivityQuery);
		$unionQuery->addSelectQuery($unsubscriptionActivityQuery);
		$unionQuery->setOrder('Date', 'ASC');

		$rs = $this->_db->query($unionQuery);

		$cache->setDateTime(date('Y-m-d H:i:s'));

		if ($rs->num_rows > 0) {
			$activityArray = array();
			while ($row = $rs->fetch_assoc()) {
				$activityArray[] = $row;
			}

			$cache->setCacheContent(serialize($activityArray));
		} else {
			$cache->setCacheContent('');
		}

		$this->update($cache);
	}

	public function update(O_Domain_SubscriberActivityCache $obj)
	{
		$query = new O_Sql_UpdateQuery('oempro_subscriber_activity_cache');
		$query->addComment('Subscriber activity cache update query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setFields(array(
							   'RelListID' => $obj->getListId(),
							   'RelSubscriberID' => $obj->getSubscriberId(),
							   'DateTime' => $obj->getDateTime(),
								'CacheContent' => $this->_db->real_escape_string($obj->getCacheContent())
						  ));
		$query->setCondition(new O_Sql_Condition(
			'CacheID', '=', $obj->getId()
		));

		$rs = $this->_db->query($query);
	}

	public function insert(O_Domain_SubscriberActivityCache $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_subscriber_activity_cache');
		$query->addComment('Subscriber activity cache insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'RelListID' => $obj->getListId(),
										'RelSubscriberID' => $obj->getSubscriberId(),
										'DateTime' => $obj->getDateTime(),
										 'CacheContent' => $this->_db->real_escape_string($obj->getCacheContent())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}


}

