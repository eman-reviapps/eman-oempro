<?php
class O_Mapper_EmailHeader extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_email_headers');
		$query->setCondition(new O_Sql_Condition(
			'HeaderID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$header = new O_Domain_EmailHeader($dbRowArray['HeaderID']);
		$header->setRawDbColumns($dbRowArray);
		$header->setName($dbRowArray['HeaderName']);
		$header->setEmailType($dbRowArray['EmailType']);
		$header->setValue($dbRowArray['HeaderValue']);
		return $header;
	}

	public function insert(O_Domain_EmailHeader $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_email_headers');
		$query->addComment('Custom email header insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'HeaderName' => $this->_db->real_escape_string($obj->getName()),
										'EmailType' => $this->_db->real_escape_string($obj->getEmailType()),
										'HeaderValue' => $this->_db->real_escape_string($obj->getValue())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function findByEmailType($emailTypes = array('all'))
	{
		$query = new O_Sql_SelectQuery('oempro_email_headers');
		$query->addComment('Find custom email header by email type query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$conditionGroup = new O_Sql_ConditionGroup('OR');
		foreach ($emailTypes as $eachEmailType) {
			$condition = new O_Sql_Condition('EmailType', '=', $eachEmailType, TRUE);
			$conditionGroup->add($condition);
		}

		$query->setCondition($conditionGroup);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$headers = array();
			while ($row = $rs->fetch_assoc()) {
				$headers[] = $this->_constructObject($row);
			}
			return $headers;
		} else {
			return FALSE;
		}
	}

	public function findAll()
	{
		$query = new O_Sql_SelectQuery('oempro_email_headers');
		$query->addComment('Find custom email header by email type query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$headers = array();
			while ($row = $rs->fetch_assoc()) {
				$headers[] = $this->_constructObject($row);
			}
			return $headers;
		} else {
			return FALSE;
		}
	}

	public function deleteByHeaderId($headerId)
	{
		$query = new O_Sql_DeleteQuery('oempro_email_headers');
		$query->addComment('Delete custom email header by header id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('HeaderID', '=', (int) $headerId));

		$rs = $this->_db->query($query);
	}
}
