<?php
class O_Mapper_MediaLibraryFile extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_medialibrary');
		$query->setCondition(new O_Sql_Condition(
			'MediaID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$media = new O_Domain_MediaLibraryFile($dbRowArray['MediaID']);
		$media->setUserId($dbRowArray['RelOwnerUserID']);
		$media->setData($dbRowArray['MediaData']);
		$media->setType($dbRowArray['MediaType']);
		$media->setSize($dbRowArray['MediaSize']);
		$media->setName($dbRowArray['MediaName']);
		$media->setFolderId($dbRowArray['RelFolderID']);
		$media->setRawDbColumns($dbRowArray);
		return $media;
	}

	public function insert(O_Domain_MediaLibraryFile $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_medialibrary');
		$query->addComment('Media library insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
			'RelOwnerUserID' => $this->_db->real_escape_string($obj->getUserId()),
			'MediaData' => $this->_db->real_escape_string($obj->getData()),
			'MediaType' => $this->_db->real_escape_string($obj->getType()),
			'MediaSize' => $this->_db->real_escape_string($obj->getSize()),
			'MediaName' => $this->_db->real_escape_string($obj->getName()),
			'RelFolderID' => $this->_db->real_escape_string($obj->getFolderId()),
		));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function deleteById($fileId)
	{
		$query = new O_Sql_DeleteQuery('oempro_medialibrary');
		$query->addComment('Delete media library files by folder id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('MediaID', '=', (int) $fileId));

		$rs = $this->_db->query($query);
	}

	public function findByFolderId($folderId, $ownerUserId)
	{
		$query = new O_Sql_SelectQuery('oempro_medialibrary');
		$query->addComment('Find media by folder query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition1 = new O_Sql_Condition('RelFolderID', '=', $folderId, FALSE);
		$condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $ownerUserId, FALSE);
		$condition3 = new O_Sql_ConditionGroup();
		$condition3->add($condition1);
		$condition3->add($condition2);
		$query->setCondition($condition3);

		$query->setOrder('MediaName', 'ASC');

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$files = array();
			while ($row = $rs->fetch_assoc()) {
				$files[] = $this->_constructObject($row);
			}
			return $files;
		} else {
			return FALSE;
		}
	}

}