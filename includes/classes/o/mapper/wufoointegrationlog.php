<?php
class O_Mapper_WufooIntegrationLog extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_wufoo_integration_logs');
		$query->setCondition(new O_Sql_Condition(
			'LogID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$log = new O_Domain_WufooIntegrationLog($dbRowArray['LogID']);
		$log->setListId($dbRowArray['RelListID']);
		$log->setIntegrationId($dbRowArray['RelIntegrationID']);
		$log->setUserId($dbRowArray['RelOwnerUserID']);
		$log->setMessage($dbRowArray['Message']);
		$log->setStatus($dbRowArray['Status']);
		$log->setDateTime($dbRowArray['DateTime']);
		return $log;
	}

	public function insert(O_Domain_WufooIntegrationLog $log)
	{
		$query = new O_Sql_InsertQuery('oempro_wufoo_integration_logs');
		$query->setFieldsAndValues(array(
										'RelListID' => $log->getListId(),
										'RelOwnerUserID' => $log->getUserId(),
										'RelIntegrationID' => $log->getIntegrationId(),
										'Message' => $log->getMessage(),
										'Status' => $log->getStatus(),
										'DateTime' => $log->getDateTime()
								   ));
		$id = $this->_db->insert($query->__toString());
		$log->setId($id);
	}

	public function findAllByIntegrationId($integrationId)
	{
		$query = new O_Sql_SelectQuery('oempro_wufoo_integration_logs');
		$query->addComment('Wufoo integration find all logs by integration id query');
		$query->addComment('This query is generated in '.__FILE__.' on line '.__LINE__);
		$query->setCondition(new O_Sql_Condition(
			'RelIntegrationID', '=', $integrationId
		));

		$rs = $this->_db->query($query);

		$logs = array();
		if ($rs->num_rows > 0) {
			while ($row = $rs->fetch_assoc()) {
				$logs[] = $this->_constructObject($row);
			}
		}

		return $logs;
	}

	public function countSuccessfulByIntegrationId($integrationId)
	{
		$query = new O_Sql_SelectQuery('oempro_wufoo_integration_logs');
		$query->addComment('Wufoo integration find all logs by integration id query');
		$query->addComment('This query is generated in '.__FILE__.' on line '.__LINE__);
		$query->setFields(array(
							   'COUNT(*) AS Total'
						  ));
		$query->setCondition(new O_Sql_Condition(
			'RelIntegrationID', '=', $integrationId
		));

		$rs = $this->_db->query($query);
		$result = $rs->fetch_assoc();
		return $result['Total'];
	}

	public function deleteByIntegrationId($integrationId)
	{
		$query = new O_Sql_DeleteQuery( 'oempro_wufoo_integration_logs');
		$query->addComment('Wufoo integration delete logs by integration id query');
		$query->addComment('This query is generated in '.__FILE__.' on line '.__LINE__);
		$query->setCondition(new O_Sql_Condition('RelIntegrationID', '=', (int) $integrationId));

		$rs = $this->_db->query($query);
	}
}
