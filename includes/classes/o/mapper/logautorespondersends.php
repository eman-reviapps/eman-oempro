<?php
class O_Mapper_LogAutoResponderSends extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_log_autoresponder_sends');
		$query->setCondition(new O_Sql_Condition(
			'LogID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		return NULL;
	}

	public function insert(O_Domain_LogAutoResponderSend $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_log_autoresponder_sends');
		$query->addComment('Auto responder send history log insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'RelOwnerUserID' => $this->_db->real_escape_string($obj->getRelOwnerUserID()),
										'RelAutoResponderID' => $this->_db->real_escape_string($obj->getRelAutoResponderID()),
										'RelListID' => $this->_db->real_escape_string($obj->getRelListID()),
										'RelSubscriberID' => $this->_db->real_escape_string($obj->getRelSubscriberID()),
										'EmailAddress' => $this->_db->real_escape_string($obj->getEmailAddress()),
										'LogDateTime' => $this->_db->real_escape_string($obj->getDateTime())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}
}
