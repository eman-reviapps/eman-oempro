<?php
class O_Mapper_MediaLibraryFolder extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_medialibrary_folders');
		$query->setCondition(new O_Sql_Condition(
			'FolderID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$folder = new O_Domain_MediaLibraryFolder($dbRowArray['FolderID']);
		$folder->setUserId($dbRowArray['RelOwnerUserID']);
		$folder->setName($dbRowArray['Name']);
		$folder->setFolderId($dbRowArray['RelParentFolderID']);
		return $folder;
	}

	public function insert(O_Domain_MediaLibraryFolder $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_medialibrary_folders');
		$query->addComment('Media library folder insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
			'RelOwnerUserID' => $this->_db->real_escape_string($obj->getUserId()),
			'Name' => $this->_db->real_escape_string($obj->getName()),
			'RelParentFolderID' => $this->_db->real_escape_string($obj->getFolderId()),
		));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function deleteByFolderId($folderId)
	{
		$query = new O_Sql_DeleteQuery('oempro_medialibrary_folders');
		$query->addComment('Delete media library folder by folder id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('FolderID', '=', (int) $folderId));

		$rs = $this->_db->query($query);
	}

	public function findByFolderId($folderId, $ownerUserId)
	{
		$query = new O_Sql_SelectQuery('oempro_medialibrary_folders');
		$query->addComment('Find media library folder by parent folder id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition1 = new O_Sql_Condition('RelParentFolderID', '=', $folderId, FALSE);
		$condition2 = new O_Sql_Condition('RelOwnerUserID', '=', $ownerUserId, FALSE);
		$condition3 = new O_Sql_ConditionGroup();
		$condition3->add($condition1);
		$condition3->add($condition2);
		$query->setCondition($condition3);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$files = array();
			while ($row = $rs->fetch_assoc()) {
				$files[] = $this->_constructObject($row);
			}
			return $files;
		} else {
			return FALSE;
		}
	}

}