<?php
class O_Mapper_APIKey extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_users_api_keys');
		$query->setCondition(new O_Sql_Condition(
			'APIKeyID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$apikey = new O_Domain_APIKey($dbRowArray['APIKeyID']);
		$apikey->setRawDbColumns($dbRowArray);
		$apikey->setAPIKey($dbRowArray['APIKey']);
		$apikey->setUserId($dbRowArray['RelOwnerUserID']);
		$apikey->setNote($dbRowArray['Note']);
		$apikey->setIPAddress($dbRowArray['IPAddress']);
		return $apikey;
	}

	public function insert(O_Domain_APIKey $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_users_api_keys');
		$query->addComment('API key insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
										'APIKey' => $this->_db->real_escape_string($obj->getAPIKey()),
										'RelOwnerUserID' => $this->_db->real_escape_string($obj->getUserId()),
										'Note' => $this->_db->real_escape_string($obj->getNote()),
										'IPAddress' => $this->_db->real_escape_string($obj->getIPAddress())
								   ));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function findByUser($userId)
	{
		$query = new O_Sql_SelectQuery('oempro_users_api_keys');
		$query->addComment('Find API keys by user');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$condition = new O_Sql_Condition('RelOwnerUserID', '=', $userId, FALSE);
		$query->setCondition($condition);

		$rs = $this->_db->query($query);

		if ($rs->num_rows > 0) {
			$keys = array();
			while ($row = $rs->fetch_assoc()) {
				$keys[] = $this->_constructObject($row);
			}
			return $keys;
		} else {
			return FALSE;
		}
	}

	public function deleteByKeyIdAndUserId($keyId, $userId)
	{
		$query = new O_Sql_DeleteQuery('oempro_users_api_keys');
		$query->addComment('Delete API keys by api key id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		
		$conditionGroup = new O_Sql_ConditionGroup('AND');
		$conditionGroup->add(new O_Sql_Condition('APIKeyID', '=', (int) $keyId));
		$conditionGroup->add(new O_Sql_Condition('RelOwnerUserID', '=', (int) $userId));

		$query->setCondition($conditionGroup);

		$rs = $this->_db->query($query);
	}
}
