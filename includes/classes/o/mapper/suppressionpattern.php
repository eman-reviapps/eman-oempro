<?php
class O_Mapper_SuppressionPattern extends O_Mapper_Abstract
{
	protected function _getSqlQueryForFindById($id)
	{
		$query = new O_Sql_SelectQuery('oempro_suppression_patterns');
		$query->setCondition(new O_Sql_Condition(
			'PatternID', '=', $id
		));
		return $query;
	}

	protected function _constructObject($dbRowArray)
	{
		$pattern = new O_Domain_SuppressionPattern($dbRowArray['PatternID']);
		$pattern->setPattern($dbRowArray['Pattern']);
		$pattern->setType($dbRowArray['PatternType']);
		$pattern->setDescription($dbRowArray['Description']);
		return $pattern;
	}

	public function insert(O_Domain_SuppressionPattern $obj)
	{
		$query = new O_Sql_InsertQuery('oempro_suppression_patterns');
		$query->addComment('Suppression pattern insert query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);

		$query->setFieldsAndValues(array(
			'Pattern' => $this->_db->real_escape_string($obj->getPattern()),
			'PatternType' => $this->_db->real_escape_string($obj->getType()),
			'Description' => $this->_db->real_escape_string($obj->getDescription())
		));

		$id = $this->_db->query($query);
		$obj->setId($id);
	}

	public function deleteById($patternId)
	{
		$query = new O_Sql_DeleteQuery('oempro_suppression_patterns');
		$query->addComment('Delete suppression pattern by pattern id query');
		$query->addComment('This query is generated in ' . __FILE__ . ' on line ' . __LINE__);
		$query->setCondition(new O_Sql_Condition('PatternID', '=', (int) $patternId));

		$rs = $this->_db->query($query);
	}

	public function findAll()
	{
		$query = new O_Sql_SelectQuery('oempro_suppression_patterns');
		$query->setOrder('PatternID', 'ASC');

		$rs = $this->_db->query($query);

		if ($rs->num_rows < 1)
			return false;

		$patterns = array();
		while ($row = $rs->fetch_assoc()) {
			$patterns[] = $this->_constructObject($row);
		}

		return $patterns;
	}

}