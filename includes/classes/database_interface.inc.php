<?php
/**
 * Oempro
 * (c)Copyright Octeth Ltd. All rights reserved.
 * 
 * Source code can NOT be distributed/resold without the permission of Octeth Ltd.
 * For more details about licensing, please refer to http://www.octeth.com/
 *
 **/

/**
 * Abstract class for Database interfaces which allows to communicate with database server
 *
 * @package Oempro
 * @author Mert Hurturk
 **/
abstract class DatabaseInterface
{
/**
 * Table name prefix
 *
 * @var string
 **/
protected $TableNamePrefix = '';

/**
 * Database server address
 *
 * @var string
 **/
protected $ServerHost;

/**
 * Database server username
 *
 * @var string
 **/
protected $Username;

/**
 * Database server password
 *
 * @var string
 **/
protected $Password;

/**
 * Database name
 *
 * @var string
 **/
protected $Name;

/**
 * Error message of last occured error
 *
 * @var string
 **/
public $ErrorMessage = '';

/**
 * Connects to database
 *
 * @param string MySql server
 * @param string The username
 * @param string The password
 * @param string The name of the database to be selected
 * @param string Table name prefix
 *
 * @return boolean
 * @author Mert Hurturk
 **/
abstract function Connect($Host, $Username, $Password, $Database, $TableNamePrefix);

/**
 * Runs given query
 *
 * @param string Query to be executed on database
 *
 * @return mixed If error occures returns false else returns result set
 * @author Mert Hurturk
 **/
abstract function ExecuteQuery($Query);

/**
 * Inserts a new row into the selected table
 *
 * @param array Fields and values to be inserted
 * @param string Table name
 *
 * @return mixed If an error occurs return false else return row id
 * @author Mert Hurturk
 **/
abstract function InsertRow($ArrayFieldnValues, $TableName);

/**
 * Deletes rows from selected table
 *
 * @param array Criterias to be matched while deleting rows
 * @param string Table name
 *
 * @return boolean
 * @author Mert Hurturk
 **/
abstract function DeleteRows($ArrayCriterias, $TableName);

/**
 * Updates rows of a table
 *
 * @param array Fields and values to be updated
 * @param array Match criterias of the rows to be updated
 * @param string Table name
 *
 * @return mixed If error occurs return false else returns number of affected rows
 * @author Mert Hurturk
 **/
abstract function UpdateRows($ArrayFieldnValues, $ArrayCriterias, $TableName);

/**
 * Retrieves rows from a table
 *
 * @param array Fields to be retrieved from given tables
 * @param array Tables which contains the data to be retrieved
 * @param array Criterias to be matched
 * @param array Fields to be ordered array('FieldName'=>'OrderType')
 *
 * @return array
 * @author Mert Hurturk
 **/
abstract function GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder = array());

/**
 * Make values secure for SQL query
 *
 * @param mixed Value to be secured for query
 *
 * @return void
 * @author Mert Hurturk
 **/
abstract function MakeValueSecureForQuery ($Value);

/**
 * Adds an option to database
 *
 * @param string Option name
 * @param string Option value
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public function AddOption($OptionName, $OptionValue)
	{
	$ArrayFieldnValues	= array(
							'OptionName'		=> $OptionName,
							'OptionValue'		=> $OptionValue
							);
	$this->InsertRow($ArrayFieldnValues, $this->TableNamePrefix.'options');
	}

/**
 * Saves option to database
 *
 * @param string Option name
 * @param string Option value
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public function SaveOption($OptionName, $OptionValue)
	{
	$ArrayOptions = $this->GetOption($OptionName);
	if (count($ArrayOptions) > 0)
		{
		$ArrayFieldnValues	= array(
								'OptionValue'		=> $OptionValue,
								);
		$ArrayCriterias 	= array(
								'OptionName'		=> $ArrayOptions[0]['OptionName'],
								);
		$this->UpdateRows($ArrayFieldnValues, $ArrayCriterias, $this->TableNamePrefix.'options');
		}
	else
		{
		$this->AddOption($OptionName, $OptionValue);
		}
	}

/**
 * Removes an option from database
 *
 * @param string Option name
 *
 * @return void
 * @author Mert Hurturk
 **/
public function RemoveOption($OptionName)
	{
	$ArrayCriterias		= array(
								"OptionName"	=>	$OptionName
								);
	return $this->DeleteRows($ArrayCriterias, $this->TableNamePrefix.'options');
	}

/**
 * Gets an option from database
 *
 * @param string Option name
 *
 * @return array
 * @author Mert Hurturk
 **/
public function GetOption($OptionName)
	{
	$ArrayFields		= array('*');
	$ArrayFromTables	= array($this->TableNamePrefix.'options');
	$ArrayCriterias		= array(
								"OptionName"	=>	$OptionName
								);
	return $this->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);
	}

/**
 * Generates the MySQL database backup
 *
 * @param boolean if true, backups structure
 * @param boolean if true, backups data
 *
 * @return void
 * @author Cem Hurturk
 **/
abstract function Backup($BackupStructure = true, $BackupData = true);

/**
 * Sets database server address
 *
 * @param string Database server host address
 *
 * @return void
 * @author Mert Hurturk
 **/
protected function SetServerAddress($Address)
	{
	$this->ServerHost = $Address;
	}

/**
 * Sets database server username
 *
 * @param string Database username
 *
 * @return void
 * @author Mert Hurturk
 **/
protected function SetUsername($Name)
	{
	$this->Username = $Name;
	}

/**
 * Sets database server password
 *
 * @param string Database password
 *
 * @return void
 * @author Mert Hurturk
 **/
protected function SetPassword($Pass)
	{
	$this->Password = $Pass;
	}

/**
 * Sets database name
 *
 * @param string Database name
 *
 * @return void
 * @author Mert Hurturk
 **/
protected function SetDatabaseName($DatabaseName)
	{
	$this->Name = $DatabaseName;
	}

/**
 * Sets table name prefix
 *
 * @param string Table name prefix
 *
 * @return void
 * @author Mert Hurturk
 **/
protected function SetTableNamePrefix($Prefix)
	{
	$this->TableNamePrefix = $Prefix;
	}

/**
 * Returns table name prefix
 *
 * @return string
 * @author Mert Hurturk
 **/
public function GetTableNamePrefix()
	{
	return $this->TableNamePrefix;
	}
} // END abstract class DatabaseInterface
?>