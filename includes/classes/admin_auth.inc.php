<?php
/**
 * AdminAuth class
 *
 * This class holds all admin authorization related functions
 * @package Oempro
 * @author Octeth
 **/
class AdminAuth extends Core
{
/**
 * Logins administrator
 *
 * @param integer $AdminID Admin id
 * @param string $Username Username
 * @param string $Password Admin password
 * @return void
 * @author Cem Hurturk
 */
function Login($AdminID, $Username, $Password, $ConvertPasswordToMD5 = true)
	{
	// The below line is commented because while updating admin account, we needed to pass password as MD5. So it must be optional
	// to record admin information to session with or without MD5 password
	// $_SESSION[SESSION_NAME]['AdminLogin'] = md5($AdminID).md5($Username).md5($Password);
	if ($ConvertPasswordToMD5 == true)
		{
		$_SESSION[SESSION_NAME]['AdminLogin'] = md5($AdminID).md5($Username).md5($Password);
		}
	else
		{
		$_SESSION[SESSION_NAME]['AdminLogin'] = md5($AdminID).md5($Username).$Password;
		}

	return;
	}

/**
 * Logs the admin out
 *
 * @return void
 * @author Cem Hurturk
 */
function Logout()
	{
	unset($_SESSION[SESSION_NAME]['AdminLogin']);
	return;
	}

/**
 * Checks if admin is logged in or not.
 *
 * @param string $LoggedInRedirectURL If set, logged in admin will be redirected to this URL
 * @param string $LoggedOutRedirectURL If set, not logged in admin will be redirected to this URL
 * @return booelan
 * @author Cem Hurturk
 */
function IsLoggedIn($LoggedInRedirectURL, $LoggedOutRedirectURL)
	{
	if (isset($_SESSION[SESSION_NAME]['AdminLogin']) == true)
		{
		if ($_SESSION[SESSION_NAME]['AdminLogin'] != '')
			{
			// Session exists. Check if the login information is valid.
			$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."admins WHERE CONCAT(MD5(AdminID), MD5(Username), MD5(Password)) = '".mysql_real_escape_string($_SESSION[SESSION_NAME]['AdminLogin'])."'";
			$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

			if (mysql_num_rows($ResultSet) == 1)
				{
				// Login information is correct
				if ($LoggedInRedirectURL != false)
					{
					header('Location: '.$LoggedInRedirectURL);
					exit;
					}
				else
					{
					return true;
					}
				}
			else
				{
				// Login information is incorrect
				if ($LoggedOutRedirectURL != false)
					{
					header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
					exit;
					}
				else
					{
					return false;
					}
				}
			}
		else
			{
			// Session does not exist. Admin not logged in.
			if ($LoggedOutRedirectURL != false)
				{
				header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
				exit;
				}
			else
				{
				return false;
				}
			}
		}
	else
		{
		// Session does not exist. Admin not logged in.
		if ($LoggedOutRedirectURL != false)
			{
			header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
			exit;
			}
		else
			{
			return false;
			}
		}
	}

	

	
} // END class AdminAuth
?>