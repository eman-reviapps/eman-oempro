<?php

// Required modules - Start
Core::LoadObject('users');
// Required modules - End

/**
 * Payment processing, calculating and reporting class
 *
 * @package default
 * @author Cem Hurturk
 * */
class UserPayment extends Core {

    private static $ArrayUser = array();
    private static $OldPackage = array();
    private static $NewPackage = array();
    private static $CurrentPaymentPeriod = array();

    public static function setUser($ArrayUser) {
        self::$ArrayUser = $ArrayUser;
    }

    public static function setOldPackage($OldPackage) {
        self::$OldPackage = $OldPackage;
    }

    public static function setNewPackage($NewPackage) {
        self::$NewPackage = $NewPackage;
    }

    public static function setCuurentPaymentPeriod($ArrayPaymentPeriod) {
        self::$CurrentPaymentPeriod = $ArrayPaymentPeriod;
    }

    public static function processDiscount() {

        $discount = 0;
        $new_balance = 0;
        $new_credits = 0;
        $send_payment = false;
        $remainder = 0;
        //if upgrade from package to higher package or downgrade to lower package
        //get old package and new package fees
        $new_service_fee = empty(self::$NewPackage['PaymentSystemChargeAmount']) ? 0 : self::$NewPackage['PaymentSystemChargeAmount'];
        $current_balance = UserBalance::getUserBalance(self::$ArrayUser['UserID']);

        if (self::$OldPackage['CreditSystem'] == 'Enabled') {
            $remainder = self::getCreditRemainder();
        } else {
            $remainder = self::getPeriodRemainder();
        }

        $total_balance = $remainder + $current_balance;

        if (self::$NewPackage['CreditSystem'] == 'Enabled') {

            $PurchaseInfo = self::processCreditDiscount($total_balance, self::$NewPackage);

            $total_amount = $PurchaseInfo['TotalAmount'];
            $new_service_fee = $PurchaseInfo['TotalAmount'];
            $discount = $PurchaseInfo['Discount'];
            $net_amount = $PurchaseInfo['NetAmount'];
            $new_balance = $PurchaseInfo['NewBalance'];
            $new_credits = $PurchaseInfo['Credits'];
            $send_payment = $PurchaseInfo['SendPayment'];
        } else {
            $Result = self::calcDiscountAndBalance($total_balance, $new_service_fee);

            $discount = $Result['Discount'];
            $new_balance = $Result['NewBalance'];
            $send_payment = $Result['SendPayment'];
        }

        return array(
            'Discount' => $discount,
            'NewBalance' => $new_balance,
            'SendPayment' => $send_payment,
            'ServiceFee' => $new_service_fee,
            'Credits' => $new_credits,
            'TotalAmount' => $total_amount,
            'NetAmount' => $net_amount,
        );
    }

    public static function processCreditDiscount($CurrentBalance, $UserGroup) {

        $PriceRangeArray = explode("\n", $UserGroup['PaymentPricingRange']);
        $PriceRange = explode('|', $PriceRangeArray[0]);

        $Credits = $PriceRange[0]; //5000
        $Rate = $PriceRange[1]; //0.006    
        $TotalAmount = $Credits * $Rate; //5000*0.006 = 30

        return self::calcDiscountAndBalanceCredit($Credits, $CurrentBalance, $TotalAmount);
    }

    private static function calcDiscountAndBalanceCredit($Credits, $TotalBalance, $TotalAmount) {
        $SendPayment = false;
        $NewBalance = 0;
        $Discount = 0;

        // Calculate the total amount - End }
        // Take tax into consideration - Start {
//        if (PAYMENT_TAX_PERCENT > 0) {
//            $TotalAmount = $TotalAmount + ($TotalAmount * (PAYMENT_TAX_PERCENT / 100));
//        }

        if ($TotalBalance >= $TotalAmount) {
            $Discount = $TotalAmount;
            $NetAmount = 0;
            $SendPayment = false;
            $NewBalance = $TotalBalance - $TotalAmount;
        } else {
            $Discount = $TotalBalance;
            $NetAmount = $TotalAmount - $Discount;
            $SendPayment = true;
            $NewBalance = 0;
        }

        return array(
            'TotalAmount' => $TotalAmount,
            'Discount' => $Discount,
            'NetAmount' => $NetAmount,
            'NewBalance' => $NewBalance,
            'SendPayment' => $SendPayment,
            'Credits' => $Credits,
        );
    }

    private static function calcDiscountAndBalanceCredit_old($total_balance, $PriceRange) {
        $discount = 0;
        $new_balance = 0;
        $new_credits = 0;
        $send_payment = false;

        $PriceRange = explode('|', $PriceRange[0]);

        $Range = $PriceRange[0]; //5000
        $Rate = $PriceRange[1]; //0.006    
        $Price = $Range * $Rate; //5000*0.006 = 30

        if ($total_balance > $Price) {
            $discount = $Price;
            $new_balance = $total_balance - $Price;
            $new_credits = $Range;
        } else {
            $discount = $total_balance;
            $new_balance = 0;
            $new_credits = (($discount * $Range) / $Price);
        }
        return array(
            "Discount" => $discount,
            "NewBalance" => $new_balance,
            "Credits" => $new_credits,
            "SendPayment" => $send_payment,
        );
    }

    private static function calcDiscountAndBalance($total_balance, $new_service_fee) {
        $discount = 0;
        $new_balance = 0;
        $send_payment = false;

        if ($total_balance > $new_service_fee) {
            $discount = $new_service_fee;
            $new_balance = $total_balance - $new_service_fee;
            $send_payment = false;
        } elseif ($total_balance == $new_service_fee) {
            $discount = $new_service_fee;
            $new_balance = 0;
            $send_payment = false;
        } else {
            $discount = $total_balance;
            $new_balance = 0;
            $send_payment = true;
        }
        return array(
            "Discount" => $discount,
            "NewBalance" => $new_balance,
            "SendPayment" => $send_payment,
        );
    }

    private static function getPeriodRemainder() {
        $old_service_fee = empty(self::$OldPackage['PaymentSystemChargeAmount']) ? 0 : self::$OldPackage['PaymentSystemChargeAmount'];

        $currentDate = strtotime(date('Y-m-d'));
        $period_start_date = strtotime(self::$CurrentPaymentPeriod['PeriodStartDate']);
        $period_end_date = strtotime(self::$CurrentPaymentPeriod['PeriodEndDate']);

        $all_days = (($period_end_date - $period_start_date) / (60 * 60 * 24));
        $actual_days = (($currentDate - $period_start_date) / (60 * 60 * 24));
        $cost_of_day = round(($old_service_fee / $all_days), 2, PHP_ROUND_HALF_UP);

        if (self::$OldPackage['SubscriptionType'] == 'Monthly') {
            $remainder = $old_service_fee - round(($cost_of_day * $actual_days), 1, PHP_ROUND_HALF_UP);
        } else if (self::$OldPackage['SubscriptionType'] == 'Annually') {
            $cost_of_month = 0;
            $UserGroupMonthly = UserGroups::RetrieveUserGroups(array('*'), array(
                        "SubscriptionType" => 'Monthly',
                        "CreditSystem" => "Disabled",
                        "PaymentSystem" => "Enabled",
                        "IsAdminGroup" => "No",
                        'LimitSubscribers' => self::$OldPackage['LimitSubscribers']
                            )
            );

            if ($UserGroupMonthly) {
                $UserGroupMonthly = $UserGroupMonthly[0];
                $cost_of_month = empty($UserGroupMonthly['PaymentSystemChargeAmount']) ? 0 : $UserGroupMonthly['PaymentSystemChargeAmount'];
            }

            $month1 = date('m', $currentDate);
            $month2 = date('m', $period_start_date);
            $no_of_months = $month1 - $month2;
            if ($cost_of_month == 0) {
                $remainder = $old_service_fee - round(($cost_of_day * $actual_days), 1, PHP_ROUND_HALF_UP);
            } else {
                $remainder = $old_service_fee - round(($cost_of_month * $no_of_months), 1, PHP_ROUND_HALF_UP);
            }
        }
//        $remainder = $old_service_fee - round((($actual_days * $old_service_fee) / $all_days), 2, PHP_ROUND_HALF_UP);
        return $remainder;
    }

    private static function getCreditRemainder() {

        $AvailableCredits = self::$ArrayUser['AvailableCredits'];

        $PriceRange = explode("\n", self::$OldPackage['PaymentPricingRange']);
        $PriceRange = explode('|', $PriceRange[0]);

        $Range = $PriceRange[0]; //5000
        $Rate = $PriceRange[1]; //0.006    5000*0.006 = 30
        $Price = $Range * $Rate;

        return round((($AvailableCredits * $Price) / $Range), 2, PHP_ROUND_HALF_UP);
    }

    public static function doCreditPurchase($CreditInfo, $ArrayFieldAndValues) {

        //check user balance record already exists
        $check_bal_exists = UserBalance::checkBalanceExists(self::$ArrayUser['UserID']);

        if ($check_bal_exists) {
            $ArrayFields = array(
                "Balance" => $CreditInfo['NewBalance'],
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => self::$ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFields, $ArrayCriterias);
        } else {
            $ArrayFields = array(
                "RelUserID" => self::$ArrayUser['UserID'],
                "Balance" => $CreditInfo['NewBalance'],
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFields);
        }

        UserPayment::updateCredits($ArrayFieldAndValues, $CreditInfo);
        return true;
    }

    public static function processCreditPurchase($ArrayParameters) {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayParameters['UserID']), true);
        if ($ArrayUser == false) {
            return array(false, '0007');
        }

        $ArrayOldUserGroup = UserGroups::RetrieveUserGroup($ArrayParameters["OldGroupID"]);
        if ($ArrayOldUserGroup == false) {
            return array(false, '0011'); //Invalid Old GroupID
        }
        $ArrayNewUserGroup = UserGroups::RetrieveUserGroup($ArrayParameters['NewGroupID']);
        if ($ArrayNewUserGroup == false) {
            return array(false, '0012'); //Invalid New GroupID
        }

        Payments::SetUser($ArrayUser);
        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return array(false, '0014');
        }
        if (strtolower($ArrayParameters['PaymentStatus']) == 'success') {
            self::$ArrayUser = $ArrayUser;
            $CurrentBalance = UserBalance::getUserBalance(self::$ArrayUser['UserID']);
//            $PurchaseInfo = self::processCreditDiscount($CurrentBalance, self::$ArrayUser['GroupInformation']);
            $PurchaseInfo = self::processCreditDiscount($CurrentBalance, $ArrayNewUserGroup);
            $PaymentLogID = self::createCreditPurchaseLog($PurchaseInfo, $ArrayParameters);

            //here
            if ($ArrayNewUserGroup['LimitEmailSendPerPeriod'] > $ArrayOldUserGroup['LimitEmailSendPerPeriod']) {
                Users::UpdateUser(array('RelUserGroupID' => $ArrayParameters['NewGroupID']), array('UserID' => self::$ArrayUser["UserID"]));
            }
            //make user trusted
            Users::UpdateUser(array('ReputationLevel' => 'Trusted', 'CreditNoticeSent' => 'No'), array('UserID' => self::$ArrayUser["UserID"]));
            return array(true);
        }
        return array(false, '0005');
    }

    public function processUpgrade($ArrayParameters) {
// Retrieve user information - Start {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayParameters['UserID']), true);
        if ($ArrayUser == false) {
            return array(false, '0007'); //Invalid UserID
        }

        $ArrayOldUserGroup = UserGroups::RetrieveUserGroup($ArrayParameters["OldGroupID"]);
        if ($ArrayOldUserGroup == false) {
            return array(false, '0011'); //Invalid Old GroupID
        }
        $ArrayNewUserGroup = UserGroups::RetrieveUserGroup($ArrayParameters['NewGroupID']);
        if ($ArrayNewUserGroup == false) {
            return array(false, '0012'); //Invalid New GroupID
        }

        self::setUser($ArrayUser);
        self::setOldPackage($ArrayOldUserGroup);
        self::setNewPackage($ArrayNewUserGroup);

        Payments::SetUser($ArrayUser);
        $old_logID = Payments::GetCurrentPaymentPeriodIfExists();

        $CurrentPaymentPeriod = isset($old_logID) ? Payments::GetLog($old_logID) : '';
        self::setCuurentPaymentPeriod($CurrentPaymentPeriod);
        $UpgradeInfo = self::processDiscount();

        if (strtolower($ArrayParameters['PaymentStatus']) == 'success') {
            $Result = self::doUpgrade($UpgradeInfo, $ArrayParameters['NewGroupID'], $ArrayParameters);

            if (!$Result[0]) {
                return array(false, $Result[2]);
            }
        }
        return array(true);
    }

    public static function doUpgrade($UpgradeInfo, $new_package, $ArrayParameters = array()) {

        //check user balance record already exists
        $check_bal_exists = UserBalance::checkBalanceExists(self::$ArrayUser['UserID']);

        if ($check_bal_exists) {
            $ArrayFieldnValues = array(
                "Balance" => $UpgradeInfo['NewBalance'],
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => self::$ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFieldnValues, $ArrayCriterias);
        } else {
            $ArrayFieldAndValues = array(
                "RelUserID" => self::$ArrayUser['UserID'],
                "Balance" => $UpgradeInfo['NewBalance'],
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFieldAndValues);
        }
        //update user group
        Users::UpdateUser(array('RelUserGroupID' => $new_package, 'CreditNoticeSent' => 'No'), array('UserID' => self::$ArrayUser["UserID"]));

        if (self::$NewPackage['CreditSystem'] == 'Enabled') {
            //close the period
            Payments::ClosePaymentLog(self::$CurrentPaymentPeriod['LogID']);
            //calc credits from new balance
            $PaymentLogID = self::createCreditPurchaseLog($UpgradeInfo, $ArrayParameters);

            return array(true, $PaymentLogID);
        } else {

            if (self::$OldPackage['CreditSystem'] == 'Enabled') {
                Users::UpdateUser(array('AvailableCredits' => $UpgradeInfo['Credits']), array('UserID' => self::$ArrayUser['UserID']));
            }

            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => self::$ArrayUser['UserID']), true);
            self::$ArrayUser = $ArrayUser;
            Payments::SetUser($ArrayUser);
            Payments::NewPaymentPeriod();
            //get payment LogID unique identifier
            $ArrayPaymentPeriodNew = Payments::GetLog();
            $LogID = $ArrayPaymentPeriodNew["LogID"];
            $PaymentStatus = $UpgradeInfo['SendPayment'] ? 'Unpaid' : 'Paid';
            //Update user invoice
            $ArrayReturn = API::call(array(
                        'format' => 'JSON',
                        'command' => 'user.paymentperiods.update2',
                        'protected' => true,
                        'username' => self::$ArrayUser['Username'],
                        'password' => self::$ArrayUser['Password'],
                        'parameters' => array(
                            'userid' => self::$ArrayUser["UserID"],
                            'logid' => $LogID,
                            'discount' => $UpgradeInfo['Discount'],
                            'includetax' => 'Exclude',
                            'paymentstatus' => $PaymentStatus,
                            'sendreceipt' => "No",
                        )
            ));
            if ($ArrayReturn['Success'] == false) {

                return array(false, $LogID, '0016'); //Failed to update payment
            } else {
                if ($UpgradeInfo['SendPayment']) {
                    return self::updatePaymentPurcahseLog($LogID, $ArrayParameters);
                }
                return array(true, $LogID);
            }
        }
    }

    private static function updatePaymentPurcahseLog($LogID, $ArrayParameters = array()) {
        //process payment upgrade
        $ArrayPaymentPeriod = Payments::GetPaymentPeriod($LogID);
        if ($ArrayPaymentPeriod == false) {
            return array(false, $LogID, '0013'); //Invalid Payment Period
        }
        // Retrieve user information - End }
        // Check if payment system is enabled - Start {
        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return array(false, $LogID, '0014'); //Payment System disabled
        }
        // Check if payment system is enabled - End }
        // Check if this payment has already been processed - Start {
        if ($ArrayPaymentPeriod['PaymentStatus'] == 'Paid') {
            return array(false, $LogID, '0015'); //Already paid before
        }
        // Check if this payment has already been processed - End }
        // Process the received data - Start {

        if (strtolower($ArrayParameters['PaymentStatus']) == 'success') {
            $ArrayFieldAndValues = array(
                'PaymentStatus' => 'Paid',
                'PaymentStatusDate' => date('Y-m-d H:i:s'),
                'PaidGateway' => $ArrayParameters['Gateway'],
                'GatewayTransactionID' => $ArrayParameters['PaymentTransactionId'],
                'PaymentId' => $ArrayParameters['PaymentId'],
            );

            Payments::Update($ArrayFieldAndValues, array('LogID' => $ArrayPaymentPeriod['LogID']));
            return array(true, $LogID);
        }
    }

    public static function createCreditPurchaseLog($PurchaseInfo, $ArrayParameters = array()) {

        $TotalAmount = $PurchaseInfo['TotalAmount'];
        $Discount = $PurchaseInfo['Discount'];
        $NetAmount = $PurchaseInfo['NetAmount'];
        $NewBalance = $PurchaseInfo['NewBalance'];
        $Credits = $PurchaseInfo['Credits'];

        $PaymentLogID = Payments::CreateCreditsPurchaseLog(self::$ArrayUser, $Credits, $TotalAmount, 0, $Discount, $NetAmount, 'iyziko');
        //update credits , upfate user log
        $PaidGateway = isset($ArrayParameters) && isset($ArrayParameters['Gateway']) ? $ArrayParameters['Gateway'] : 'Balance';
        $PaymentTransactionId = isset($ArrayParameters) && isset($ArrayParameters['PaymentTransactionId']) ? $ArrayParameters['PaymentTransactionId'] : '';
        $PaymentId = isset($ArrayParameters) && isset($ArrayParameters['PaymentId']) ? $ArrayParameters['PaymentId'] : '';

        $ArrayFieldAndValues = array(
            'PaymentStatus' => 'Paid',
            'PaymentStatusDate' => date('Y-m-d H:i:s'),
            'PaidGateway' => $PaidGateway,
            'GatewayTransactionID' => $PaymentTransactionId,
            'PaymentId' => $PaymentId,
        );
        $CreditInfo = array(
            'PaymentLogID' => $PaymentLogID,
            'Credits' => $Credits,
            'TotalAmount' => $TotalAmount,
            'NewBalance' => $NewBalance,
        );
//             
        $PurchaseLog = Payments::RetrieveCreditsPurchaseLog($PaymentLogID);
        UserPayment::setCuurentPaymentPeriod($PurchaseLog);
        UserPayment::doCreditPurchase($CreditInfo, $ArrayFieldAndValues);
        return $PaymentLogID;
    }

    private static function updateCredits($ArrayFieldAndValues, $ArrayParameters) {

        Payments::UpdateCreditPurchaseLog($ArrayFieldAndValues, array('LogID' => $ArrayParameters['PaymentLogID']));
        // Add credits to the user account - Start {
        Users::UpdateUser(array('AvailableCredits' => array(true, '`AvailableCredits` + ' . $ArrayParameters['Credits'])), array('UserID' => self::$ArrayUser['UserID']));
        //make user trusted
        Users::UpdateUser(array('ReputationLevel' => 'Trusted', 'CreditNoticeSent' => 'No'), array('UserID' => self::$ArrayUser["UserID"]));
    }

    public static function processUserRenew($ArrayPaymentPeriod, $ReturnBoolean = true) {
        $LogID = $ArrayPaymentPeriod["LogID"];

        //update invoice and check balance
        //get current user package service fee
        $service_fee = empty(self::$ArrayUser['GroupInformation']['PaymentSystemChargeAmount']) ? 0 : self::$ArrayUser['GroupInformation']['PaymentSystemChargeAmount'];
        $current_balance = UserBalance::getUserBalance(self::$ArrayUser['UserID']);
        $discount = 0;
        $new_balance = 0;
        $send_payment = true;

        if ($current_balance > $service_fee) {
            $discount = $service_fee;
            $new_balance = $current_balance - $discount;
            $send_payment = false;
        } elseif ($current_balance == $service_fee) {
            $discount = $service_fee;
            $new_balance = 0;
            $send_payment = false;
        } else {
            $discount = $current_balance;
            $new_balance = 0;
            $send_payment = true;
        }

        $check_bal_exists = UserBalance::checkBalanceExists(self::$ArrayUser['UserID']);

        if ($check_bal_exists) {
// if he has balance records,then update current balance
            $ArrayFieldnValues = array(
                "Balance" => $new_balance,
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => self::$ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFieldnValues, $ArrayCriterias);
        } else {
// if user has no balance records. then create new record
            $ArrayFieldAndValues = array(
                "RelUserID" => self::$ArrayUser['UserID'],
                "Balance" => $new_balance,
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFieldAndValues);
        }

        $ReputationLevel = $send_payment ? 'Untrusted' : 'Trusted';
//update user to be un-trusted untill he pays
        Core::LoadObject('users');
        Users::UpdateUser(array('ReputationLevel' => $ReputationLevel), array('UserID' => self::$ArrayUser["UserID"]));

        $PaymentStatus = $send_payment ? 'Unpaid' : 'Paid';
//update invoice with service total if usergroup has service fees
        $sendreceipt = $send_payment ? "Yes" : "No";

        Core::LoadObject('api');
        $ArrayReturn = API::call(array(
                    'format' => 'json',
                    'command' => 'user.paymentperiods.update2',
                    'protected' => true,
                    'username' => self::$ArrayUser['Username'],
                    'password' => self::$ArrayUser['Password'],
                    'parameters' => array(
                        'userid' => self::$ArrayUser["UserID"],
                        'logid' => $LogID,
                        'discount' => $discount,
                        'includetax' => 'Exclude',
                        'paymentstatus' => $PaymentStatus,
                        'sendreceipt' => $sendreceipt,
                    )
        ));
        return $ReturnBoolean ? $PaymentStatus : $ArrayReturn;
    }

    public static function processPayInvoice($ArrayParameters) {
// Retrieve user information - Start {
        $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayParameters['UserID']), true);
        if ($ArrayUser == false) {
            return array(false, '0007');
        }

        Payments::SetUser($ArrayUser);
        $ArrayPaymentPeriod = Payments::GetPaymentPeriod($ArrayParameters['LogID']);
        if ($ArrayPaymentPeriod == false) {
            return array(false, '0013');
        }
        self::setUser($ArrayUser);
// Retrieve user information - End }
// Check if payment system is enabled - Start {
        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return array(false, '0014');
        }
// Check if payment system is enabled - End }
// Check if this payment has already been processed - Start {
        if ($ArrayPaymentPeriod['PaymentStatus'] == 'Paid') {
            return array(false, '0015');
        }
// Check if this payment has already been processed - End }
// Process the received data - Start {

        if (strtolower($ArrayParameters['PaymentStatus']) == 'success') {
            $ArrayFieldAndValues = array(
                'PaymentStatus' => 'Paid',
                'PaymentStatusDate' => date('Y-m-d H:i:s'),
                'PaidGateway' => $ArrayParameters['Gateway'],
                'GatewayTransactionID' => $ArrayParameters['PaymentTransactionId'],
                'PaymentId' => $ArrayParameters['PaymentId'],
            );

            Payments::Update($ArrayFieldAndValues, array('LogID' => $ArrayPaymentPeriod['LogID']));

            if ($ArrayParameters['ReputationLevel'] == 'Trusted') {
                //update user to be trusted here        
                Users::UpdateUser(array('ReputationLevel' => $ArrayParameters['ReputationLevel']), array('UserID' => $ArrayUser['UserID']));
            }
            return array(true);
        }
        return array(false, '0019');
    }

}

// END class PaymentProcess
?>