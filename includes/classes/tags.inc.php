<?php
/**
 * Tags class
 *
 * This class holds all tag related functions
 * @package Oempro
 * @author Octeth
 **/
class Tags extends Core
{
/**
 * Required fields for a tag
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('RelOwnerUserID', 'Tag');	

/**
 * Reteurns all tags matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving tags.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveTags($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('Tag'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'tags');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayTags			= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayTags) < 1) { return false; } // if there are no tags, return false

	return $ArrayTags;
	}

/**
 * Reteurns all tags of a campaign
 *
 * @param integer $CampaignID Id of campaign
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveTagsOfCampaign($CampaignID)
	{
	$SQLQuery	= "SELECT tblTags.TagID, tblTags.Tag FROM ".MYSQL_TABLE_PREFIX."rel_tags_campaigns AS tblAssign INNER JOIN ".MYSQL_TABLE_PREFIX."tags AS tblTags ON tblAssign.RelTagID=tblTags.TagID WHERE tblAssign.RelCampaignID='".$CampaignID."' ORDER BY tblTags.Tag ASC";
	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);

	$ArrayRows = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayRows[] = $EachRow;
		}

	return $ArrayRows;
	}

/**
 * Returns total number of tags of a user
 *
 * @param integer $OwnerUserID User's id.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function GetTotal($OwnerUserID)
	{
	// Check for required fields of this function - Start
	if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	$ArrayFields		= array('COUNT(*) AS TotalTagCount');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'tags');
	$ArrayCriterias		= array('RelOwnerUserID'=>$OwnerUserID);
	$ArrayClients = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	if (count($ArrayClients) < 1) { return false; } // if there are no tags, return false

	return $ArrayClients[0]['TotalTagCount'];
	}
		
/**
 * Creates a new tag with given values
 *
 * @param array $ArrayFieldAndValues Values of new tag
 * @return boolean|integer ID of new tag
 * @author Mert Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'tags');
	
	$NewTagID = Database::$Interface->GetLastInsertID();
	return $NewTagID;
	}
	
/**
 * Updates tag information
 *
 * @param array $ArrayFieldAndValues Values of new tag information
 * @param array $ArrayCriterias Criterias to be matched while updating tag
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'tags');
	return true;
	}

/**
 * Deletes tags
 *
 * @param integer $OwnerUserID Owner user id of tags to be deleted
 * @param array $ArrayTagIDs Tag ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($OwnerUserID, $ArrayTagIDs = array())
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	if (count($ArrayTagIDs) > 0)
		{
		foreach ($ArrayTagIDs as $EachID)
			{
			self::DeleteCampaignRelationships($EachID, 0, $OwnerUserID);
		
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'TagID'=>$EachID), MYSQL_TABLE_PREFIX.'tags');
			}
		}	
	elseif (count($ArrayTagIDs) == 0)
		{
		self::DeleteCampaignRelationships(0, 0, $OwnerUserID);
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'tags');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Tag', array($OwnerUserID, $ArrayTagIDs));
	// Plug-in hook - End
	}

/**
 * Assigns a tag to campaign
 *
 * @param array $ArrayCampaignIDs IDs of campaigns
 * @param array $TagID ID of tag
 * @return null
 * @author Mert Hurturk
 **/
public static function AssignToCampaigns($ArrayCampaignIDs, $TagID, $OwnerUserID)
	{
	// Check if tag id is given - Start
	if (!isset($TagID) || $TagID == '') return false;
	// Check if tag id is given - End

	// Check if any campaign id is given - Start
	if (count($ArrayCampaignIDs) < 1) return false;
	// Check if any campaign id is given - End

	$ArrayFieldAndValues = array();
	$ArrayFieldAndValues['RelTagID'] = $TagID;
	$ArrayFieldAndValues['RelOwnerUserID'] = $OwnerUserID;
	foreach ($ArrayCampaignIDs as $EachID)
		{
		$ArrayFieldAndValues['RelCampaignID'] = $EachID;
		Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'rel_tags_campaigns');
		}

	return;
	}

/**
 * Unassigns a tag from campaign
 *
 * @param array $ArrayCampaignIDs IDs of campaigns
 * @param array $TagID ID of tag
 * @return null
 * @author Mert Hurturk
 **/
public static function UnassignFromCampaigns($ArrayCampaignIDs, $TagID, $OwnerUserID)
	{
	// Check if tag id is given - Start
	if (!isset($TagID) || $TagID == '') return false;
	// Check if tag id is given - End

	// Check if any campaign id is given - Start
	if (count($ArrayCampaignIDs) < 1) return false;
	// Check if any campaign id is given - End

	// Delete all relations of given tag - Start
	
	foreach ($ArrayCampaignIDs as $EachCampaignID)
		{
		self::DeleteCampaignRelationships($TagID, $EachCampaignID, $OwnerUserID);
		}
	// Delete all relations of given tag - End

	return;
	}

/**
 * Delete campaign and tag relations 
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public static function DeleteCampaignRelationships($TagID = 0, $CampaignID = 0, $OwnerUserID)
	{
	if ($TagID > 0 && $CampaignID > 0)
		{
		Database::$Interface->DeleteRows(array('RelTagID'=>$TagID,'RelCampaignID'=>$CampaignID,'RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'rel_tags_campaigns');
		}
	else
		{
		if ($TagID > 0)
			{
			Database::$Interface->DeleteRows(array('RelTagID'=>$TagID,'RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'rel_tags_campaigns');
			}
		elseif ($CampaignID > 0)
			{
			Database::$Interface->DeleteRows(array('RelCampaignID'=>$CampaignID,'RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'rel_tags_campaigns');
			}
		else
			{
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'rel_tags_campaigns');
			}
		}
	}

/**
 * Returns the tag information
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return boolean|array
 * @author Cem Hurturk
 */
public static function RetrieveTag($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'tags');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayTag			= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayTag) < 1) { return false; } // if there are no tags, return false 
		
	$ArrayTag = $ArrayTag[0];

	return $ArrayTag;
	}

} // END class Clients
?>