<?php
/**
 * WebServiceIntegration class
 *
 * This class holds all web service integration of subscriber lists related functions
 * @package Oempro
 * @author Octeth
 **/
class WebServiceIntegration extends Core
{	

/**
 * Returns web service integration urls matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param string $ArrayCriterias Criterias to be matched while retrieving urls.
 * @param string $ArrayOrder Fields to order.
 * @return array
 * @author Mert Hurturk
 */
public static function RetrieveURLs($ArrayReturnFields, $ArrayCriterias, $ArrayOrder=array('ServiceURL'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'subscriber_list_web_service_integrations');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayURLs = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayURLs) < 1) { return false; } // if there are no urls, return false

	return $ArrayURLs;
	}
		
/**
 * Add a new web service integration url
 *
 * @param array $ArrayFieldAndValues Values of new segment
 * @return bool/integer
 * @author Mert Hurturk
 **/
public static function AddURL($ArrayFieldAndValues)
	{
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'subscriber_list_web_service_integrations');
	
	$NewURLID = Database::$Interface->GetLastInsertID();
	return $NewURLID;
	}
	
/**
 * Deletes web service integration url
 *
 * @param integer $OwnerUserID Owner user id
 * @param array $ArrayURLIDs URL ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
function Delete($OwnerUserID, $ArrayURLIDs, $ListID = 0)
	{
	if (count($ArrayURLIDs) > 0)
		{
		foreach ($ArrayURLIDs as $EachID)
			{
			Database::$Interface->DeleteRows(array('WebServiceIntegrationID'=>$EachID,'RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'subscriber_list_web_service_integrations');
			}
		}
	elseif ($ListID > 0)
		{
		Database::$Interface->DeleteRows(array('RelListID'=>$ListID,'RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'subscriber_list_web_service_integrations');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.WebserviceIntegration', array($OwnerUserID, $ArrayURLIDs, $ListID));
	// Plug-in hook - End
	}
	
/**
 * Tests remote URL
 *
 * @return string
 * @author Cem Hurturk
 **/
function TestURL($URL)
	{
	$CurlHandler = curl_init();
	    curl_setopt($CurlHandler, CURLOPT_URL, $URL);
	    curl_setopt($CurlHandler, CURLOPT_HEADER, 1); // get the header
	    curl_setopt($CurlHandler, CURLOPT_NOBODY, 1); // and *only* get the header
	    curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1); // get the response as a string from curl_exec(), rather than echoing it
	    curl_setopt($CurlHandler, CURLOPT_FRESH_CONNECT, 1); // don't use a cached version of the url
    if (!curl_exec($CurlHandler)) { return false; }

    $httpcode = curl_getinfo($CurlHandler, CURLINFO_HTTP_CODE);
	if ($httpcode < 400)
		{
		return array(true,0);
		}
	else
		{
		return array(false,$httpcode);
		}
	}

/**
 * Sends the data to the web servce
 *
 * @param string $Action 
 * @param string $ArraySubscriber 
 * @param string $WebServiceURL 
 * @return array
 * @author Cem Hurturk
 */
function WebServiceConnection($Action, $ArraySubscriber, $WebServiceURL)
	{
	$XMLData	 = '';
	$XMLData	.= '<?xml version="1.0" encoding="UTF-8" ?>';
	$XMLData	.= '<webservice>';
	$XMLData	.= '<action><![CDATA['.$Action.']]></action>';
	$XMLData	.= '<action_timestamp><![CDATA['.time().']]></action_timestamp>';
	$XMLData	.= '<subscriber>';
	foreach ($ArraySubscriber as $EachField=>$EachValue)
		{
		$XMLData	.= '<'.$EachField.'><![CDATA['.$EachValue.']]></'.$EachField.'>';
		}
	$XMLData	.= '</subscriber>';
	$XMLData	.= '</webservice>';

	$ArrayPostParameters = array(
								'WebServiceData='.rawurlencode($XMLData),
								);
	$ArrayReturn = Core::DataPostToRemoteURL($WebServiceURL, $ArrayPostParameters, 'POST', false, '', '', 60, true);

	return $ArrayReturn;
	}



} // END class Lists
?>