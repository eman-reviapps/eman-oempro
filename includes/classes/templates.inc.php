<?php
/**
 * Templates class
 *
 * This class holds all template related functions
 * @package Oempro
 * @author Octeth
 **/
class Templates extends Core
{	
/**
 * Reteurns all templates matching given criterias
 *
 * @param integer $RelOwnerUserID Owner user id.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveTemplates($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('TemplateName' => 'ASC'), $CriteriaOperator = 'AND')
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'templates');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayTemplates		= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, 0, 0, $CriteriaOperator, false, false);

	if (count($ArrayTemplates) < 1) { return false; } // if there are no email templates, return false

	foreach ($ArrayTemplates as $Key=>$ArrayEachTemplate)
		{
		$ArrayTemplates[$Key]['TemplateMD5ID']	= md5($ArrayEachTemplate['TemplateID']);
		}

	return $ArrayTemplates;
	}

/**
 * Reteurns template matching given criterias
 *
 * @param integer $RelOwnerUserID Owner user id.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveTemplate($ArrayReturnFields, $ArrayCriterias, $CriteriaOperator = 'AND')
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'templates');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayTemplate		= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, 0, 0, $CriteriaOperator, false, false);

	if (count($ArrayTemplate) < 1) { return false; } // if there are no clients, return false 

	$ArrayTemplate = $ArrayTemplate[0];

	return $ArrayTemplate;
	}

/**
 * Creates a new email template with given values
 *
 * @param array $ArrayFieldAndValues Values of new email template
 * @return boolean|integer ID of new email template
 * @author Cem Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Create a new record in subscriber_lists table - Start
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'templates');
	$NewTemplateID = Database::$Interface->GetLastInsertID();
	// Create a new record in subscriber_lists table - End

	return $NewTemplateID;
	}

/**
 * Updates client information
 *
 * @param array $ArrayFieldAndValues Values of new client information
 * @param array $ArrayCriterias Criterias to be matched while updating client
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'templates');
	return true;
	}

/**
 * Deletes templates
 *
 * @param array $ArrayTemplateIDs Template ids to be deleted
 * @return boolean
 * @author Cem Hurturk
 **/
public static function Delete($ArrayTemplateIDs)
	{
	if (count($ArrayTemplateIDs) > 0)
		{
		foreach ($ArrayTemplateIDs as $EachID)
			{
			Database::$Interface->DeleteRows(array('TemplateID'=>$EachID), MYSQL_TABLE_PREFIX.'templates');
			}
		}	

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Template', array($ArrayTemplateIDs));
	// Plug-in hook - End

	return;
	}

	
} // END class Clients
?>