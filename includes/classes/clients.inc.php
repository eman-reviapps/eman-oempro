<?php
Core::LoadObject('lists');
Core::LoadObject('campaigns');

/**
 * Clients class
 *
 * This class holds all client related functions
 * @package Oempro
 * @author Octeth
 **/
class Clients extends Core
{	
/**
 * Required fields for a client
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('RelOwnerUserID', 'ClientUsername', 'ClientPassword', 'ClientEmailAddress', 'ClientName');	

/**
 * Default values for fields of a client
 *
 * @static array
 **/
public static $ArrayDefaultValuesForFields = array('ClientAccountStatus'=>'Enabled');	

/**
 * Reteurns all cleints matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving clients.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveClients($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('ClientName'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'clients');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayClients		= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayClients) < 1) { return false; } // if there are no clients, return false

	foreach ($ArrayClients as &$EachClient)
		{
		$EachClient['AssignedSubscriberLists']	= self::RetrieveAssignedSubscriberLists($EachClient['ClientID']);
		$EachClient['AssignedCampaigns']		= self::RetrieveAssignedCampaigns($EachClient['ClientID']);
		}

	return $ArrayClients;
	}

/**
 * Returns all assigned subscriber lists of given client
 *
 * @param integer $ClientID Client's id.
 * @param boolean $RetrieveDetailedInformation If set to true, detailed list information retrieved
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveAssignedSubscriberLists($ClientID, $RetrieveDetailedInformation=false)
	{
	// Check for required fields of this function - Start
	if (!isset($ClientID) || $ClientID == '') { return false; }
	// Check for required fields of this function - End

	$ArrayFields			= array('*');
	$ArrayFromTables		= array(MYSQL_TABLE_PREFIX.'rel_clients_subscriberlists');
	$ArrayCriterias			= array('RelClientID'=>$ClientID);
	$ArraySubscriberLists	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	if (count($ArraySubscriberLists) < 1) { return false; } // if there are no lists assigned, return false
	
	$ArrayAssignedSubscriberLists = array();
	foreach ($ArraySubscriberLists as $Each)
		{
		if ($RetrieveDetailedInformation)
			{
			$ArrayAssignedSubscriberLists[] = Lists::RetrieveList(array('*'), array('ListID'=>$Each['RelSubscriberListID']), true);
			}
		else
			{
			$ArrayAssignedSubscriberLists[] = array('SubscriberListID'=>$Each['RelSubscriberListID']);
			}
		}
	
	return $ArrayAssignedSubscriberLists;
	}

public static function RetrieveClientsOfCampaign($CampaignID)
	{
	$ArrayFields		= array('*');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'rel_clients_campaigns');
	$ArrayCriterias		= array('RelCampaignID'=>$CampaignID);
	$ArrayClients  		= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	if (count($ArrayClients) < 1) { return false; } // if there are no clients assigned, return false

	$ArrayAssignedClients = array();
	foreach ($ArrayClients as $Each)
		{
		$ArrayAssignedClients[] = array('ClientID'=>$Each['RelClientID']);
		}
	
	return $ArrayAssignedClients;
	}

/**
 * Returns all assigned campaigns of given client
 *
 * @param integer $ClientID Client's id.
 * @param boolean $RetrieveDetailedInformation If set to true, detailed list information retrieved
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveAssignedCampaigns($ClientID, $RetrieveDetailedInformation=false)
	{
	// Check for required fields of this function - Start
	if (!isset($ClientID) || $ClientID == '') { return false; }
	// Check for required fields of this function - End
		
	$ArrayFields		= array('*');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'rel_clients_campaigns');
	$ArrayCriterias		= array('RelClientID'=>$ClientID);
	$ArrayCampaigns = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	if (count($ArrayCampaigns) < 1) { return false; } // if there are no campaigns assigned, return false
	
	$ArrayAssignedCampaigns = array();
	foreach ($ArrayCampaigns as $Each)
		{
		if ($RetrieveDetailedInformation)
			{
			$ArrayAssignedCampaigns[] = Campaigns::RetrieveCampaign(array('*'), array('CampaignID'=>$Each['RelCampaignID']));
			}
		else
			{
			$ArrayAssignedCampaigns[] = array('CampaignID'=>$Each['RelCampaignID']);
			}
		}
	
	return $ArrayAssignedCampaigns;
	}

/**
 * Returns total number of clients of a user
 *
 * @param integer $OwnerUserID User's id.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function GetTotal($OwnerUserID)
	{
	// Check for required fields of this function - Start
	if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	$ArrayFields		= array('COUNT(*) AS TotalClientCount');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'clients');
	$ArrayCriterias		= array('RelOwnerUserID'=>$OwnerUserID);
	$ArrayClients = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

	if (count($ArrayClients) < 1) { return false; } // if there are no clients, return false

	return $ArrayClients[0]['TotalClientCount'];
	}
		
/**
 * Creates a new client with given values
 *
 * @param array $ArrayFieldAndValues Values of new client
 * @return boolean|integer ID of new client
 * @author Mert Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	// Check if any value is missing. if yes, give default values - Start
	foreach (self::$ArrayDefaultValuesForFields as $EachField=>$EachValue)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { $ArrayFieldAndValues[$EachField] = $EachValue; }
		}
	// Check if any value is missing. if yes, give default values - End

	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'clients');
	
	$NewClientID = Database::$Interface->GetLastInsertID();
	return $NewClientID;
	}
	
/**
 * Updates client information
 *
 * @param array $ArrayFieldAndValues Values of new client information
 * @param array $ArrayCriterias Criterias to be matched while updating client
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'clients');
	return true;
	}

/**
 * Assigns subscriber lists to a client
 *
 * @param integer $ClientID Client id of the client that subscriber lists will be assigned to
 * @param array $ArraySubscruberListIDs Subscriber list ids to be assigned
 * @return boolean
 * @author Mert Hurturk
 **/
public static function AssignSubscriberLists($ClientID, $ArraySubscriberListIDs)
	{
	// Check for required fields of this function - Start
		// $ClientID check
		if (!isset($ClientID) || $ClientID == '') { return false; }
		// $ArraySubscriberListIDs check
		if (!isset($ArraySubscriberListIDs) || count($ArraySubscriberListIDs) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->DeleteRows(array('RelClientID'=>$ClientID), MYSQL_TABLE_PREFIX.'rel_clients_subscriberlists');

	foreach ($ArraySubscriberListIDs as $Each)
		{
		Database::$Interface->InsertRow(array('RelClientID'=>$ClientID, 'RelSubscriberListID'=>$Each), MYSQL_TABLE_PREFIX.'rel_clients_subscriberlists');
		}
		
	return true;
	}
	
/**
 * Assigns campaigns to a client
 *
 * @param integer $ClientID Client id of the client that campaigns will be assigned to
 * @param array $ArrayCampaignIDs Campaign ids to be assigned
 * @return boolean
 * @author Mert Hurturk
 **/
public static function AssignCampaigns($ClientID, $ArrayCampaignIDs)
	{
	// Check for required fields of this function - Start
		// $ClientID check
		if (!isset($ClientID) || $ClientID == '') { return false; }
		// $ArraySubscriberListIDs check
		if (!isset($ArrayCampaignIDs) || count($ArrayCampaignIDs) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->DeleteRows(array('RelClientID'=>$ClientID), MYSQL_TABLE_PREFIX.'rel_clients_campaigns');

	foreach ($ArrayCampaignIDs as $Each)
		{
		Database::$Interface->InsertRow(array('RelClientID'=>$ClientID, 'RelCampaignID'=>$Each), MYSQL_TABLE_PREFIX.'rel_clients_campaigns');
		}
		
	return true;
	}
	
/**
 * Deletes clients
 *
 * @param integer $OwnerUserID Owner user id of clients to be deleted
 * @param array $ArrayClientIDs Client ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($OwnerUserID, $ArrayClientIDs)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	if (count($ArrayClientIDs) > 0)
		{
		foreach ($ArrayClientIDs as $EachID)
			{
			self::DeleteAssignedClientLists($EachID, 0);
			self::DeleteAssignedClientCampaigns($EachID, 0);
		
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'ClientID'=>$EachID), MYSQL_TABLE_PREFIX.'clients');
			}
		}	
	elseif (count($ArrayClientIDs) == 0)
		{
		$ArrayClients = self::RetrieveClients(array('*'), array('RelOwnerUserID' => $OwnerUserID), array('ClientID'=>'ASC'));
	
		if (count($ArrayClients) > 0)
			{
			foreach ($ArrayClients as $Index=>$ArrayEachClient)
				{
				self::DeleteAssignedClientLists($ArrayEachClient['ClientID'], 0);
				self::DeleteAssignedClientCampaigns($ArrayEachClient['ClientID'], 0);
				}
			}

		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'clients');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Client', array($OwnerUserID, $ArrayClientIDs));
	// Plug-in hook - End
	}

/**
 * Delete client and list relations 
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public static function DeleteAssignedClientLists($ClientID, $ListID = 0)
	{
	if ($ClientID > 0)
		{
		Database::$Interface->DeleteRows(array('RelClientID'=>$ClientID), MYSQL_TABLE_PREFIX.'rel_clients_subscriberlists');
		}
	elseif ($ListID > 0)
		{
		Database::$Interface->DeleteRows(array('RelSubscriberListID'=>$ListID), MYSQL_TABLE_PREFIX.'rel_clients_subscriberlists');
		}
	else
		{
		return false;
		}
	}

/**
 * Delete client and campaign relations 
 *
 * @return boolean
 * @author Mert Hurturk
 **/
public static function DeleteAssignedClientCampaigns($ClientID, $CampaignID = 0)
	{
	if ($ClientID > 0)
		{
		Database::$Interface->DeleteRows(array('RelClientID'=>$ClientID), MYSQL_TABLE_PREFIX.'rel_clients_campaigns');
		}
	elseif ($CampaignID > 0)
		{
		Database::$Interface->DeleteRows(array('RelCampaignID'=>$CampaignID), MYSQL_TABLE_PREFIX.'rel_clients_campaigns');
		}
	else
		{
		return false;
		}
	}

/**
 * Returns the client information
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return boolean|array
 * @author Cem Hurturk
 */
public static function RetrieveClient($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'clients');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayClient = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayClient) < 1) { return false; } // if there are no clients, return false 
		
	$ArrayClient = $ArrayClient[0];

	return $ArrayClient;
	}

	
} // END class Clients
?>