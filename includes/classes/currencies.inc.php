<?php

class Currency extends Core {

    private static $BASE_URL = "http://www.apilayer.net/api/";
    private static $API_KEY = "6db2aee643139508edfacab7e28f7ae2";

    const TR = "TRY";
    const EUR = "EUR";
    const USD = "USD";
    const GBP = "GBP";
    const IRR = "IRR";

    public function __construct() {
        
    }

    public static function getLiveExchangeRate($Currencies = '', $Live = true) {
        $url = self::$BASE_URL;
        if ($Live) {
            $url .= "live";
        }
        $url .= "?access_key=" . self::$API_KEY;

        if (isset($Currencies) && !empty($Currencies)) {
            $url .= "&currencies=" . $Currencies;
        }
        $json = file_get_contents($url);
        $obj = json_decode($json);

        return $obj;
    }

    public static function getExchangeInfo($CountryCode) {

        if ($CountryCode == "TR") {
            $Result = self::getLiveExchangeRate(self::TR);
            if (!$Result->success) {
                return array(false,
                    array(
                        "ErrorCode" => $Result->error->code,
                        "ErrorType" => $Result->error->type,
                    )
                );
            } else {
                $ExchangeInfo = array(
                    "ExchangeRate" => $Result->quotes->USDTRY,
                    "Currency" => self::TR,
                );
                return array(true, $ExchangeInfo);
            }
        }
    }

}
