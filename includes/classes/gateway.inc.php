<?php

/**
 * Payment Gateway Engine
 *
 * @package default
 * @author Cem Hurturk
 * */
class PaymentGateway {

    /**
     * List of available payment gateways
     *
     * @var array
     * */
    public static $ArrayLoadedGateways = array();

    /**
     * Payment gateway
     *
     * @var string
     * */
    public static $Interface;

    /**
     * Constructor function
     *
     * @return void
     * @author Mert Hurturk
     * */
    private function __construct() {
        
    }

    /**
     * Sets the payment interface
     *
     * @return void
     * @author Cem Hurturk
     * */
    public function SetInterface($Interface) {
        if ($Interface == 'PayPal_Express') {
            self::$Interface = new PaymentGateway_PayPal_Express;
        } elseif ($Interface == 'Iyzipay') {
            self::$Interface = new PaymentGateway_Iyzico;
        }

        return;
    }

    /**
     * Returns the list of payment gateways available
     *
     * @return void
     * @author Cem Hurturk
     */
    public function LoadInterfaces() {
        $Directory = APP_PATH . '/includes/classes/payment_gateways';

        if (is_dir($Directory) == true) {
            if ($DirHandler = opendir($Directory)) {
                while (($EachFile = readdir($DirHandler)) !== false) {
                    if (($EachFile != '.') && ($EachFile != '..') && (substr($EachFile, 0, 8) == 'gateway_')) {
                        include_once($Directory . '/' . $EachFile);
                        $ClassName = 'PaymentGateway_' . strtoupper(str_replace(array('gateway_', '.inc.php'), array('', ''), $EachFile));

                        if ($ClassName == 'PaymentGateway_PAYPAL_EXPRESS') {
                            self::$ArrayLoadedGateways[$ClassName] = new $ClassName;
                        }
                    }
                }
                closedir($DirHandler);
            }
        }
    }

}

// END class PaymentGateway
?>