<?php
/**
 * Transaction emails class
 *
 * This class holds all transaction email related functions
 * @package Oempro
 * @author Octeth
 **/
class TransactionEmails extends Core
{
/**
 * Returns all transaction emails matching given criteria
 *
 * @param array $ArrayReturnFields
 * @param array $ArrayCriterias
 * @param array $ArrayOrder
 * @param int $StartFrom
 * @param int $RetrieveCount
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveTransactionEmails($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('TransactionalQueueID'=>'ASC'), $StartFrom = 0, $RetrieveCount = 0)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'transactional_email_queue');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayTransactionalQueue = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder, $RetrieveCount, $StartFrom);

	if (count($ArrayTransactionalQueue) < 1) { return false; } // if there are no clients, return false

	return $ArrayTransactionalQueue;
	}

/**
 * Returns the transaction email queue information
 *
 * @param string $ArrayReturnFields
 * @param string $ArrayCriterias
 * @return boolean|array
 * @author Cem Hurturk
 */
public static function RetrieveTransactionEmail($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'transactional_email_queue');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayTransactionalQueue = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayTransactionalQueue) < 1) { return false; } // if there are no clients, return false

	$ArrayTransactionalQueue = $ArrayTransactionalQueue[0];

	return $ArrayTransactionalQueue;
	}

/**
 * Updates transaction queue information
 *
 * @param array $ArrayFieldAndValues
 * @param array $ArrayCriterias
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'transactional_email_queue');
	return true;
	}


/**
 * Sends the email to the subscriber
 *
 * @param string $ArrayQueue
 * @param string $ArrayUser
 * @param string $ArrayList
 * @param string $ArraySubscriber
 * @param string $ArrayEmail
 * @return void
 * @author Cem Hurturk
 */
public static function SendEmail($ArrayQueue, $ArrayUser, $ArrayList, $ArraySubscriber, $ArrayAutoResponder, $ArrayEmail)
	{
	// Load custom headers
	$emailHeaderMapper = O_Registry::instance()->getMapper('EmailHeader');
	$autoResponderEmailHeaders = $emailHeaderMapper->findByEmailType(array('all', 'autoresponder'));

	// If fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchURL'] != '')
		{
		$ArrayEmail['HTMLContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), $ArrayAutoResponder, false));
		$ArrayEmail['ContentType']		= 'HTML';
		}
	// If fetch URL is defined in email properties, fetch the URL and set contents - End

	// If plain fetch URL is defined in email properties, fetch the URL and set contents - Start
	if ($ArrayEmail['FetchPlainURL'] != '')
		{
		$ArrayEmail['PlainContent']		= Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayAutoResponder, array(), false));
		$ArrayEmail['ContentType']		= ($ArrayEmail['ContentType'] == 'HTML' ? 'Both' : 'Plain');
		}
	// If plain fetch URL is defined in email properties, fetch the URL and set contents - End

	// Load the mailer engine - Start
	if ($ArrayUser['GroupInformation']['SendMethod'] == 'System')
		{
		EmailQueue::LoadSendEngine();
		SendEngine::SetSendMethod();
		}
	else
		{
		EmailQueue::LoadSendEngine($ArrayUser);
		SendEngine::SetSendMethod(
			true,
			$ArrayUser['GroupInformation']['SendMethodSMTPHost'],
			$ArrayUser['GroupInformation']['SendMethodSMTPPort'],
			$ArrayUser['GroupInformation']['SendMethodSMTPSecure'],
			$ArrayUser['GroupInformation']['SendMethodSMTPAuth'],
			$ArrayUser['GroupInformation']['SendMethodSMTPUsername'],
			$ArrayUser['GroupInformation']['SendMethodSMTPPassword'],
			$ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'],
			$ArrayUser['GroupInformation']['SendMethodSMTPDebug'],
			$ArrayUser['GroupInformation']['SendMethodSMTPKeepAlive'],
			$ArrayUser['GroupInformation']['SendMethodLocalMTAPath'],
			$ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'],
			$ArrayUser['GroupInformation']['SendMethodPowerMTADir'],
			$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
			$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
			''
			);
		}

	if ($ArrayUser['GroupInformation']['XMailer'] != '')
		{
		SendEngine::$XMailer = $ArrayUser['GroupInformation']['XMailer'];
		}
	// Load the mailer engine - End

	// Set contents and subject of the email - Start
	if ($ArrayEmail['ContentType'] == 'HTML')
		{
		// HTML email
		$ContentType						= 'HTML';
		}
	elseif ($ArrayEmail['ContentType'] == 'Plain')
		{
		// Plain email
		$ContentType						= 'Plain';
		}
	elseif ($ArrayEmail['ContentType'] == 'Both')
		{
		// MultiPart email
		$ContentType						= 'MultiPart';
		}
	// Set contents and subject of the email - End

	// Set email attachments (if attached) and image embedding (if enabled) - Start
	$ArrayEmail['HTMLContent'] = SendEngine::SetAttachmentsAndImageEmbedding(
		$ContentType,
		$ArrayEmail['HTMLContent'],
		0,
		$ArrayEmail['EmailID'],
		$ArrayUser['UserID'],
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID'],
		Attachments::RetrieveAttachments(array('*'), array('RelEmailID' => $ArrayEmail['EmailID']), array('AttachmentID' => 'ASC')),
		($ArrayEmail['ImageEmbedding'] == 'Enabled' ? true : false)
		);
	// Set email attachments (if attached) and image embedding (if enabled) - End

	// Add header/footer to the email (if exists in the user group) - Start {
	$ArrayReturn = Personalization::AddEmailHeaderFooter(($ArrayEmail['PlainContent'] != '' ? $ArrayEmail['PlainContent'] : ''), ($ArrayEmail['HTMLContent'] != '' ? $ArrayEmail['HTMLContent'] : ''), $ArrayUser['GroupInformation']);
		$ArrayEmail['PlainContent']	= $ArrayReturn[0];
		$ArrayEmail['HTMLContent']	= $ArrayReturn[1];
	// Add header/footer to the email (if exists in the user group) - End }

	// Personalize email subject and contents - Start
	$TMPSubject			= Personalization::Personalize($ArrayEmail['Subject'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), $ArrayAutoResponder, false);

	if ($ArrayEmail['HTMLContent'] != '')
		{
		$TMPHTMLBody	= Personalization::Personalize($ArrayEmail['HTMLContent'], array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), $ArrayAutoResponder, false);
		}
	if ($ArrayEmail['PlainContent'] != '')
		{
		$TMPPlainBody	= Personalization::Personalize($ArrayEmail['PlainContent'], array('Subscriber', 'Links', 'User', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, array(), $ArrayAutoResponder, false);
		}
	// Personalize email subject and contents - End

	// Generate the abuse message ID - Start
	$AbuseMessageID = EmailQueue::GenerateAbuseMessageID(0, $ArraySubscriber['SubscriberID'], $ArraySubscriber['EmailAddress'], $ArrayQueue['RelListID'], $ArrayAutoResponder['RelOwnerUserID'], $ArrayAutoResponder['AutoResponderID']);
	// Generate the abuse message ID - End

	// Generate email properties - Start
	SendEngine::SetEmailProperties(
		$ArrayEmail['FromName'],
		$ArrayEmail['FromEmail'],
		$ArraySubscriber['EmailAddress'],
		$ArraySubscriber['EmailAddress'],
		$ArrayEmail['ReplyToName'],
		$ArrayEmail['ReplyToEmail'],
		array(),
		$ContentType,
		$TMPSubject,
		$TMPHTMLBody,
		$TMPPlainBody,
		$AbuseMessageID,
		0,
		$ArrayEmail['EmailID'],
		$ArrayUser['UserID'],
		$ArraySubscriber['SubscriberID'],
		$ArrayList['ListID']
		);
	SendEngine::SetEncoding(EMAIL_SOURCE_ENCODING);
	// Generate email properties - End

	// Add custom headers
	$autoResponderEmailHeaderReplacements = array(
		'%User:ID%' => $ArrayUser['UserID'],
		'%User:UserID%' => $ArrayUser['UserID'],
		'%User:EmailAddress%' => $ArrayUser['EmailAddress'],
		'%UserGroup:ID%' => $ArrayUser['RelUserGroupID'],
		'%UserGroup:UserGroupID%' => $ArrayUser['RelUserGroupID'],
		'%AutoResponder:ID%' => $ArrayAutoResponder['AutoResponderID'],
		'%AutoResponder:AutoResponderID%' => $ArrayAutoResponder['AutoResponderID'],
		'%AutoResponder:TriggerType%' => $ArrayAutoResponder['AutoResponderTriggerType'],
		'%List:ID%' => $ArrayList['ListID'],
		'%List:ListID%' => $ArrayList['ListID'],
		'%Subscriber:ID%' => $ArraySubscriber['SubscriberID'],
		'%Subscriber:SubscriberID%' => $ArraySubscriber['SubscriberID'],
		'%Subscriber:EmailAddress%' => $ArraySubscriber['EmailAddress'],
		'%Subscriber:SubscriptionIP%' => $ArraySubscriber['SubscriptionIP'],
		'%Subscriber:SubscriptionDate%' => $ArraySubscriber['SubscriptionDate']
	);
	foreach ($autoResponderEmailHeaders as $eachHeader) {
		SendEngine::$Engine->AddCustomHeader(
			str_replace(
				array_keys($autoResponderEmailHeaderReplacements),
				array_values($autoResponderEmailHeaderReplacements),
				$eachHeader->__toString()
			)
		);
	}

	if (SENDGRID_EMAIL_HEADER == true)
		{
		SendEngine::$Engine->AddCustomHeader('X-SMTPAPI: {"unique_args":{"abuse-id":"'.$AbuseMessageID.'"}, "category":"autoresponder"}');
		}

	// Send the email to the recipient - Start
	$ArrayResult = SendEngine::SendEmail();

	if ($ArrayResult[0] == false)
		{
		// Error occurred.

		// Change the status of queue to 'sent' or 'failed' - Start
			$ArrayFieldnValues = array(
									'Status'		=> 'Failed',
									'StatusMessage'	=> $ArrayResult[1],
									);
		self::Update($ArrayFieldnValues, array('TransactionalQueueID' => $ArrayQueue['TransactionalQueueID']));
		// Change the status of queue to 'sent' or 'failed' - End
		}
	else
		{
		// Email sent.

		// Change the status of queue to 'sent' or 'failed' - Start
			$ArrayFieldnValues = array(
									'Status'		=> 'Sent',
									);
		self::Update($ArrayFieldnValues, array('TransactionalQueueID' => $ArrayQueue['TransactionalQueueID']));
		// Change the status of queue to 'sent' or 'failed' - End

		// Email sending is processed. Track the payment activity - Start
		Payments::AutoResponderRecipientSent($ArrayAutoResponder['AutoResponderID'], 'Sent');
		// Email sending is processed. Track the payment activity - End
		}
	// Send the email to the recipient - End

	// Remove temporary image embedding files - Start
	SendEngine::RemoveTemporaryImageEmbeddingFiles();
	// Remove temporary image embedding files - End

	// Email sending is processed. Track the payment activity - Start
	Payments::AutoResponderRecipientSent($ArrayAutoResponder['AutoResponderID'], 'Total');
	// Email sending is processed. Track the payment activity - End

	// Clear send engine cache - Start
	SendEngine::ClearRecipientCache();
	// Clear send engine cache - End

	// Close the mailer connection - Start
	SendEngine::CloseConnections();
	// Close the mailer connection - End
	}

/**
 * Deletes transactional queue
 *
 * @return void
 * @author Cem Hurturk
 */
public static function Delete($ListID = 0, $EmailID = 0, $SubscriberID = 0, $AutoResponderID = 0)
	{
	if (($ListID > 0) && ($SubscriberID == 0))
		{
		// Delete entries of a list
		Database::$Interface->DeleteRows(array('RelListID'=>$ListID), MYSQL_TABLE_PREFIX.'transactional_email_queue');
		}
	elseif ($EmailID > 0)
		{
		// Delete entries of an email content
		Database::$Interface->DeleteRows(array('RelEmailID'=>$EmailID), MYSQL_TABLE_PREFIX.'transactional_email_queue');
		}
	elseif (($SubscriberID > 0) && ($ListID > 0))
		{
		// Delete entries of a subscriber in a list
		Database::$Interface->DeleteRows(array('RelSubscriberID'=>$SubscriberID, 'RelListID'=>$ListID), MYSQL_TABLE_PREFIX.'transactional_email_queue');
		}
	elseif ($AutoResponderID > 0)
		{
		// Delete entries of an auto responder
		Database::$Interface->DeleteRows(array('RelAutoResponderID'=>$AutoResponderID), MYSQL_TABLE_PREFIX.'transactional_email_queue');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.TransactionEmail', array($ListID, $EmailID, $SubscriberID, $AutoResponderID));
	// Plug-in hook - End
	}

} // END class TransactionEmails
?>