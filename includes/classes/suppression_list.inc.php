<?php

/**
 * SuppressionList class
 *
 * This class handles all suppression list related functions
 *
 * @package Oempro
 * @author Octeth
 * */
class SuppressionList extends Core {

    /**
     * Retrieves all suppressed subscribers' email addresses of a subscriber list
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving suppressed email addresses.
     * @return array|boolean
     * @author Mert Hurturk
     * */
    public static function GetEmailAddresses($ArrayReturnFields, $ArrayCriterias, $RecordCount = 0, $StartFrom = 0) {
        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'suppression_list');
        $ArrayEmailAddresses = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array('EmailAddress' => 'ASC'), $RecordCount, $StartFrom);

        if (count($ArrayEmailAddresses) < 1) {
            return false;
        } // If there are no email addresses, return false

        return $ArrayEmailAddresses;
    }

    /**
     * Retrieves all suppressed subscribers' email addresses of a subscriber list
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving suppressed email addresses.
     * @return array|boolean
     * @author Mert Hurturk
     * */
    public static function GetSuppressionList($ArrayReturnFields, $ArrayCriterias) {
        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'suppression_list');
        $ArrayEmailAddresses = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array('EmailAddress' => 'ASC'));

        if (count($ArrayEmailAddresses) < 1) {
            return false;
        } // If there are no email addresses, return false

        return $ArrayEmailAddresses;
    }

    /**
     * Retrieves total number of globally suppressed subscribers for a given user
     *
     * @return integer
     * @author Mert Hurturk
     * */
    public static function GetTotal($UserID, $ListID = 0) {
        $ArrayFields = array('COUNT(*) AS Total');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'suppression_list');
        $ArrayTotal = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, array('RelOwnerUserID' => $UserID, 'RelListID' => $ListID));

        if (count($ArrayTotal) < 1) {
            return 0;
        } // If there are no email addresses, return false

        return $ArrayTotal[0]['Total'];
    }

    /**
     * Adds a new suppressed email address to suppression list
     *
     * @param array $ArrayFieldAndValues Values of new suppression list entry
     * @return integer
     * @author Mert Hurturk
     * */
    public static function Add($ArrayFieldAndValues) {
        // Check if email already exists in suppression list - Start {
        $ArrayFields = array('*');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'suppression_list');
        $ArrayCriterias = array('EmailAddress' => $ArrayFieldAndValues['EmailAddress'], 'RelListID' => $ArrayFieldAndValues['RelListID'], 'RelOwnerUserID' => $ArrayFieldAndValues['RelOwnerUserID']);
        $ArrayEmailAddress = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array('EmailAddress' => 'ASC'));

        if (count($ArrayEmailAddress) > 0) {
            return array('', 'duplicate');
        }
        // Check if email already exists in suppression list - End }

        Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX . 'suppression_list');
        $NewSuppressionID = Database::$Interface->GetLastInsertID();

        return $NewSuppressionID;
    }

    /**
     * Updates suppression information
     *
     * @param array $ArrayFieldAndValues Values of new subscriber information
     * @param array $ArrayCriterias Criterias to be matched while updating subscriber
     * @return boolean
     * @author Eman Mohammed
     * */
    public static function Update($ArrayFieldAndValues, $ArrayCriterias) {
        // Check for required fields of this function - Start
        // $ArrayCriterias check
        if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) {
            return false;
        }
        // Check for required fields of this function - End
        if (isset($ArrayFieldAndValues['EmailAddress']) && ($ArrayFieldAndValues['EmailAddress'] != '') && ($ArrayCriterias['SubscriberID'] != '') && (isset($ArrayCriterias['SubscriberID']))) {
            // Validate email address format - Start
            if (self::ValidateEmailAddress($ArrayFieldAndValues['EmailAddress']) == false) {
                return array(false, 'invalid');
            }
        }
        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'suppression_list');
        return true;
    }

    /**
     * Deletes suppression list of a list
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function Delete($ListID = 0, $UserID = 0) {
        if ($ListID > 0) {
            Database::$Interface->DeleteRows(array('RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'suppression_list');
        } elseif ($UserID > 0) {
            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $UserID), MYSQL_TABLE_PREFIX . 'suppression_list');
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.SuppresionList', array($ListID, $UserID));
        // Plug-in hook - End
    }

    /**
     * Deletes suppression email of a list
     *
     * @return void
     * @author Eman
     */
    public static function DeleteEmail($Email, $ListID, $UserID) {

        Database::$Interface->DeleteRows(array(
            'RelListID' => $ListID,
            'RelOwnerUserID' => $UserID,
            'EmailAddress' => $Email
                ), MYSQL_TABLE_PREFIX . 'suppression_list');

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.SuppresionList', array($ListID, $UserID));
        Plugins::HookListener('Action', 'Delete.Subscriber.SuppressedSubscribersByID', array($ListID, array($Email)));

        // Plug-in hook - End
    }

    public static function GetRecordForAnEmailAddress($EmailAddress, $ListID) {
        $SuppRecord = Database::$Interface->GetRows_Enhanced(
                array(
                    'Fields' => array('*'),
                    'Tables' => array(MYSQL_TABLE_PREFIX . 'suppression_list'),
                    'RowOrder' => array(
                        'Column' => 'RelListID',
                        'Type' => 'ASC'
                    ),
                    'Criteria' => array(
                        array(
                            'Column' => 'EmailAddress',
                            'Operator' => '=',
                            'Value' => $EmailAddress
                        ),
                        array(
                            'Link' => 'AND',
                            'Column' => 'RelListID',
                            'Operator' => 'IN',
                            'ValueWOQuote' => "(0, $ListID)"
                        )
                    )
                )
        );

        $array_return = array("IsGloballySuppressed" => false, "IsLocallySuppressed" => false, "SuppressionSource" => "");

        foreach ($SuppRecord as $Each) {
            if ($Each['RelListID'] == 0) {
                $array_return['IsGloballySuppressed'] = true;
            } else {
                $array_return['IsLocallySuppressed'] = true;
            }
            $array_return['SuppressionSource'] = $Each['SuppressionSource'];
        }

        return $array_return;
    }

}

// END class SuppressionList extends Core