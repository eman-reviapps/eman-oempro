<?php
/**
* POP3 IMAP Class
*/
class POP3Engine extends Core
{
/**
 * Is IMAP extension enabled
 *
 * @author Cem Hurturk
 */
private static $ImapEnabled				= false;

/**
 * POP3 Host
 *
 * @author Cem Hurturk
 */
private static $POP3Host				= '';

/**
 * POP3 Port
 *
 * @author Cem Hurturk
 */
private static $POP3Port				= 110;

/**
 * POP3 Username
 *
 * @author Cem Hurturk
 */
private static $POP3Username			= '';

/**
 * POP3 Password
 *
 * @author Cem Hurturk
 */
private static $POP3Password			= '';

/**
 * POP3 SSL Connection
 *
 * @author Cem Hurturk
 */
private static $POP3SSL					= false;

/**
 * Connection resource
 *
 * @author Cem Hurturk
 */
private static $ConnectionResource		= '';
	
/**
 * Constructor
 *
 * @author Cem Hurturk
 */
public function IsEngineReady()
	{
	if ((extension_loaded('imap') == false) && (extension_loaded('IMAP') == false) && (extension_loaded('Imap') == false))
		{
		self::$ImapEnabled = false;
		return false;
		}
	else
		{
		self::$ImapEnabled = true;
		return true;
		}
	}

/**
 * Sets the parameters for POP3 connection
 *
 * @param string $Host 
 * @param string $Port 
 * @param string $Username 
 * @param string $Password 
 * @return void
 * @author Cem Hurturk
 */
public static function SetParameters($Host, $Port, $Username, $Password, $SSL = false)
	{
	self::$POP3Host		= $Host;
	self::$POP3Port		= $Port;
	self::$POP3Username	= $Username;
	self::$POP3Password	= $Password;
	self::$POP3SSL		= $SSL;
	}

/**
 * Performs connection to POP3 server
 *
 * @return void
 * @author Cem Hurturk
 */
public static function Connect()
	{
	self::$ConnectionResource = imap_open('{'.self::$POP3Host.':'.self::$POP3Port.'/pop3'.(self::$POP3SSL == true ? '/ssl' : '').(ENABLE_IMAP_NOVALIDATE_SSL == true ? '/novalidate-cert' : '').'}INBOX', self::$POP3Username, self::$POP3Password);

	$ArrayErrors = self::GetErrors();

	if (count($ArrayErrors) > 0)
		{
		return $ArrayErrors;	
		}
	else
		{
		return true;
		}
	}

/**
 * Returns errors occurred as an array
 *
 * @return void
 * @author Cem Hurturk
 */
public function GetErrors()
	{
	return imap_errors();
	}

/**
 * Returns the number of messages that exist in the POP3 server
 *
 * @return void
 * @author Cem Hurturk
 */
public static function GetTotalMessages()
	{
	return imap_num_msg(self::$ConnectionResource);
	}

/**
 * Returns the raw email
 *
 * @param string $EmailNo 
 * @return void
 * @author Cem Hurturk
 */
public static function GetRawEmail($EmailNo)
	{
	return imap_fetchheader(self::$ConnectionResource, $EmailNo).imap_body(self::$ConnectionResource, $EmailNo);
	}

/**
 * Deletes an email from the POP3 server
 *
 * @param string $EmailNo 
 * @return void
 * @author Cem Hurturk
 */
public static function DeleteEmail($EmailNo, $DeleteImmediately = false)
	{
	imap_delete(self::$ConnectionResource, $EmailNo);
	
	if ($DeleteImmediately == true)
		{
		imap_expunge(self::$ConnectionResource);
		}
	return ;
	}

/**
 * Disconnect from the POP3 server
 *
 * @return void
 * @author Cem Hurturk
 */
public static function Disconnect()
	{
	imap_expunge(self::$ConnectionResource);
	imap_close(self::$ConnectionResource);
	}

} // end of class POP3Engine

?>