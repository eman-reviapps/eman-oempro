<?php
/**
 * User Groups class
 *
 * This class holds all user group related functions
 * @package Oempro
 * @author Octeth
 **/
class UserGroups extends Core {
/**
 * Loads the group of user ID
 *
 * @param string $UserID 
 * @return array
 * @author Cem Hurturk
 */
function RetrieveUserGroup($UserGroupID, $IgnoreUserLoginPrivType = false)
	{
	$ArrayFields		= array('*');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'user_groups');
	$ArrayCriterias		= array('UserGroupID' => $UserGroupID); 
	$ArrayOrder			= array();
	$ArrayUserGroup = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);
	
	if (count($ArrayUserGroup) != 1) { return false; } // if there are no users, return false 

	$ArrayUserGroup = $ArrayUserGroup[0];

	// If privilege is set to Full, set privileges to * (this is used for admin to switch to the user with full privileges) - Start
	if (($IgnoreUserLoginPrivType == false) && (isset($_SESSION[SESSION_NAME]['UserLoginPrivType']) == true) && ($_SESSION[SESSION_NAME]['UserLoginPrivType'] == 'Full'))
		{
		$ArrayUserGroup['Permissions'] = '*';
		}
	// If privilege is set to Full, set privileges to * (this is used for admin to switch to the user with full privileges) - End

	Core::LoadObject('themes');
	$ArrayUserGroup['ThemeInformation'] = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => $ArrayUserGroup['RelThemeID']));

	return $ArrayUserGroup;
	}

/**
 * Creates a new user group with given values
 *
 * @param array $ArrayFieldAndValues Values of new user group
 * @return boolean|integer ID of new user group
 * @author Cem Hurturk
 **/
public static function CreateUserGroup($ArrayFieldAndValues)
	{
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'user_groups');
	
	$NewUserGroupID = Database::$Interface->GetLastInsertID();
	return $NewUserGroupID;
	}

public static function RetrieveUserGroupCount()
	{
		$ArrayFields		= array('COUNT(*) AS Total');
		$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'user_groups');
		$ArrayCriterias		= array();
		$ArrayUserGroupCount= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

		return $ArrayUserGroupCount[0]['Total'];
	}

/**
 * Returns all cleints users given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving users.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Cem Hurturk
 **/
public static function RetrieveUserGroups($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('UserGroupID'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'user_groups');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayUserGroups	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayUserGroups) < 1) { return false; } // if there are no clients, return false

	$SQLQuery = 'SELECT COUNT(*) FROM '.MYSQL_TABLE_PREFIX.'users WHERE RelUserGroupID = %s';
	foreach ($ArrayUserGroups as &$EachUserGroup)
		{
		$Result = Database::$Interface->ExecuteQuery(sprintf($SQLQuery, $EachUserGroup['UserGroupID']));
		$Result = mysql_fetch_array($Result);
		$Result = $Result[0];
		$EachUserGroup['TotalUsers'] = $Result;
		}

	return $ArrayUserGroups;
	}

/**
 * Deletes user groups
 *
 * @param array $ArrayThemeIDs
 * @return boolean
 * @author Cem Hurturk
 **/
public static function Delete($UserGroupID)
	{
	Database::$Interface->DeleteRows(array('UserGroupID'=>$UserGroupID), MYSQL_TABLE_PREFIX.'user_groups');

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.UserGroup', array($UserGroupID));
	// Plug-in hook - End
	}

/**
 * Updates user group information
 *
 * @param array $ArrayFieldAndValues
 * @param array $ArrayCriterias
 * @return boolean
 * @author Cem Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'user_groups');
	return true;
	}


} // END class UserGroups
?>