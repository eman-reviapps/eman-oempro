<?php
// Required modules - Start
Core::LoadObject('emails');
Core::LoadObject('queue');
Core::LoadObject('statistics');
Core::LoadObject('tags');
Core::LoadObject('split_tests');
// Required modules - End

/**
 * Campaigns class
 *
 * This class holds all campaign related functions
 * @package Oempro
 * @author Octeth
 **/
class Campaigns extends Core
{
/**
 * Required fields for campaigns
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('CampaignName', 'RelOwnerUserID');	

/**
 * Default values for fields of a campaigns
 *
 * @static array
 **/
public static $ArrayDefaultValuesForFields = array(
		'CampaignStatus'					=>	'Ready',
		'ScheduleType'						=>	'Not Scheduled',
		'SendProcessStartedOn'				=> '0000-00-00 00:00:00',
		'SendProcessFinishedOn'				=> '0000-00-00 00:00:00',
		'TotalRecipients'					=> 0,
		'TotalSent'							=> 0,
		'TotalFailed'						=> 0,
		'TotalOpens'						=> 0,
		'UniqueOpens'						=> 0,
		'TotalClicks'						=> 0,
		'UniqueClicks'						=> 0,
		'TotalForwards'						=> 0,
		'UniqueForwards'					=> 0,
		'TotalViewsOnBrowser'				=> 0,
		'UniqueViewsOnBrowser'				=> 0,
		'TotalUnsubscriptions'				=> 0,
		'TotalHardBounces'					=> 0,
		'TotalSoftBounces'					=> 0,
		'BenchmarkEmailsPerSecond'			=> 0,
		'ApprovalUserExplanation'			=> '',
		'GoogleAnalyticsDomains'			=> ''
	);

public static function CopyCampaign($SourceCampaign, $UpdatedFields)
{
	$Email = Emails::RetrieveEmail(array('*'), array('EmailID' => $SourceCampaign['RelEmailID']), array('EmailName'=>'ASC'), true);

	if (is_bool($Email) == false && is_array($Email) == true)
	{
		$NewEmail = array(
			'EmailID' => '',
			'RelUserID' => $Email['RelUserID'],
			'EmailName' => $Email['EmailName'],
			'FromName' => $Email['FromName'],
			'FromEmail' => $Email['FromEmail'],
			'ReplyToName' => $Email['ReplyToName'],
			'ReplyToEmail' => $Email['ReplyToEmail'],
			'ContentType' => $Email['ContentType'],
			'Mode' => $Email['Mode'],
			'FetchURL' => $Email['FetchURL'],
			'FetchPlainURL' => $Email['FetchPlainURL'],
			'Subject' => $Email['Subject'],
			'PlainContent' => $Email['PlainContent'],
			'HTMLContent' => $Email['HTMLContent'],
			'ImageEmbedding' => $Email['ImageEmbedding'],
			'RelTemplateID' => $Email['RelTemplateID'],
			'ScreenshotImage' => $Email['ScreenshotImage'],
			'HTMLPure' => $Email['HTMLPure'],
		);
		$NewEmailID = Emails::Create($NewEmail);
	}

	$NewCampaignParameters = $UpdatedFields;
	$NewCampaignParameters['CampaignID'] = '';
	$NewCampaignParameters['RelEmailID'] = $NewEmailID;
	$NewCampaignID = self::CreateCampaign($NewCampaignParameters);

	return $NewCampaignID;
}

/**
 * Creates new campaign record
 *
 * @param array $ArrayFieldAndValues 
 * @return integer|array
 * @author Cem Hurturk
 */
public static function CreateCampaign($ArrayFieldAndValues)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { print $ArrayFieldAndValues[$EachField]; return false; }
		}
	// Check required values - End

	// Check if any value is missing. if yes, give default values - Start
	foreach (self::$ArrayDefaultValuesForFields as $EachField=>$EachValue)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { $ArrayFieldAndValues[$EachField] = $EachValue; }
		}
	// Check if any value is missing. if yes, give default values - End

	// Create a new record in campaigns table - Start
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'campaigns');
	$NewCampaignID = Database::$Interface->GetLastInsertID();
	// Create a new record in campaigns table - End

	return $NewCampaignID;
	}

/**
 * Adds the list or segment as a recipient to a campaign
 *
 * @param string $CampaignID 
 * @param string $UserID 
 * @param string $ListID 
 * @param string $SegmentID 
 * @return void
 * @author Cem Hurturk
 */
public static function AddRecipientToCampaign($CampaignID, $UserID, $ListID, $SegmentID)
	{
	$ArrayFieldnValues = array(
								'AssignID'			=> '',
								'RelOwnerUserID'	=> $UserID,
								'RelCampaignID'		=> $CampaignID,
								'RelListID'			=> ($ListID != 0 ? $ListID : 0),
								'RelSegmentID'		=> ($SegmentID != 0 ? $SegmentID : 0),
								);

	$ResultSet = Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX.'campaign_recipients');
	return;
	}

/**
 * Removes the recipients of a campaign
 *
 * @param string $CampaignID 
 * @param string $UserID 
 * @return void
 * @author Cem Hurturk
 */
public static function RemoveRecipientsOfCampaign($CampaignID, $UserID)
	{
	Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$UserID, 'RelCampaignID'=>$CampaignID), MYSQL_TABLE_PREFIX.'campaign_recipients'); 
	return;
	}

/**
 * Returns the campaign information for the provided campaign ID
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @param array $ArrayStatisticsOptions [Statistics] => array(OpenStatistics=>true), [Days] => 30
 * @return array | boolean
 * @author Cem Hurturk
 */

public static function RetrieveCampaign($ArrayReturnFields, $ArrayCriterias, $ArrayStatisticsOptions=array(), $ReturnTagList=false)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'campaigns');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayCampaign = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayCampaign) < 1) { return false; } // if there are no subscriber lists, return false 
		
	$ArrayCampaign = $ArrayCampaign[0];

	// $ArrayCampaign['EstimatedRecipients'] = self::EstimatedCampaignRecipients($ArrayCampaign['CampaignID']);
	$ArrayCampaign['RecipientLists'] = EmailQueue::GetRecipientLists($ArrayCampaign['CampaignID']);
	$ArrayCampaign['RecipientSegments'] = EmailQueue::GetRecipientLists($ArrayCampaign['CampaignID'], true);
	if ($ReturnTagList == true)
		{
		$ArrayCampaign['Tags'] = Tags::RetrieveTagsOfCampaign($ArrayCampaign['CampaignID']);
		}
	if (isset($ArrayStatisticsOptions['Days']))
		{
		$StatisticsEndDate = date('Y-m-d H:i:s', strtotime($ArrayCampaign['SendProcessStartedOn'].' +'.$ArrayStatisticsOptions['Days'].' days'));
		}
	if (isset($ArrayStatisticsOptions['Statistics']['OpenStatistics']) && $ArrayStatisticsOptions['Statistics']['OpenStatistics'] == true)
		{
		$ArrayCampaign['OpenStatistics'] = Statistics::RetrieveCampaignOpenStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['SendProcessStartedOn'], $StatisticsEndDate);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['ClickStatistics']) && $ArrayStatisticsOptions['Statistics']['ClickStatistics'] == true)
		{
		$ArrayCampaign['ClickStatistics'] = Statistics::RetrieveCampaignClickStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['SendProcessStartedOn'], $StatisticsEndDate);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['UnsubscriptionStatistics']) && $ArrayStatisticsOptions['Statistics']['UnsubscriptionStatistics'] == true)
		{
		$ArrayCampaign['UnsubscriptionStatistics'] = Statistics::RetrieveCampaignUnsubscriptionStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['SendProcessStartedOn'], $StatisticsEndDate);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['ForwardStatistics']) && $ArrayStatisticsOptions['Statistics']['ForwardStatistics'] == true)
		{
		$ArrayCampaign['ForwardStatistics'] = Statistics::RetrieveCampaignForwardStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['SendProcessStartedOn'], $StatisticsEndDate);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['BrowserViewStatistics']) && $ArrayStatisticsOptions['Statistics']['BrowserViewStatistics'] == true)
		{
		$ArrayCampaign['BrowserViewStatistics'] = Statistics::RetrieveCampaignBrowserViewStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['SendProcessStartedOn'], $StatisticsEndDate);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['BounceStatistics']) && $ArrayStatisticsOptions['Statistics']['BounceStatistics'] == true)
		{
		$ArrayCampaign['BounceStatistics'] = Statistics::RetrieveCampaignBounceStatistics($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID'], $ArrayCampaign['TotalRecipients']);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['ClickPerformance']) && $ArrayStatisticsOptions['Statistics']['ClickPerformance'] == true)
		{
		$ArrayCampaign['OverallClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfCampaign($ArrayCampaign['CampaignID']);
		$ArrayCampaign['ClickPerformance'] = Statistics::RetrieveClickPerformanceDaysOfCampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']);
		$ArrayCampaign['OverallAccountClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfAccount($ArrayCampaign['RelOwnerUserID']);
		}
	if (isset($ArrayStatisticsOptions['Statistics']['OpenPerformance']) && $ArrayStatisticsOptions['Statistics']['OpenPerformance'] == true)
		{
		$ArrayCampaign['OverallOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfCampaign($ArrayCampaign['CampaignID']);
		$ArrayCampaign['OpenPerformance'] = Statistics::RetrieveOpenPerformanceDaysOfCampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']);
		$ArrayCampaign['OverallAccountOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfAccount($ArrayCampaign['RelOwnerUserID']);
		}
	
	// Retrieve split test information - Start {
	$ArrayCampaign['SplitTest'] = false;
	$SplitTestInformation = SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']);
	$ArrayCampaign['SplitTest'] = (! $SplitTestInformation ? $SplitTestInformation : $SplitTestInformation[0]);
	// Retrieve split test information - End }
	
	return $ArrayCampaign;
	}

/**
 * Updates campaign information
 *
 * @param array $ArrayFieldAndValues Values of new subscriber list information
 * @param array $ArrayCriterias Criterias to be matched while updating subscriber list
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriterias check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'campaigns');
	return;
	}

/**
 * Reteurns all subscriber lists matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving campaigns.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveCampaigns($ArrayReturnFields, $ArrayCriterias, $ArrayOrder=array('CampaignName'=>'ASC'), $RecordCount=0, $RecordFrom=0, $ArrayStatisticsOptions=array(), $ArrayTags=array(), $ReturnTagList=false, $OnlyReturnTotal=false)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }
	if (count($ArrayTags) > 0) { $ReturnTagList = true; }

	// Prepare SQL query - Start
		$SQLQuery = '';

		// Select part - Start
		if ($OnlyReturnTotal)
			{
			$SQLQuery .= 'SELECT COUNT(*) AS TotalCampaigns';
			}
		else
			{
			$SQLQuery .= 'SELECT Campaigns.*';
			}
		// Select part - End

		// From part - Start
		$SQLQuery .= ' FROM '.MYSQL_TABLE_PREFIX.'campaigns AS Campaigns';
		if (count($ArrayTags) > 0)
			{
			for ($i=0;$i<count($ArrayTags);$i++)
				{
				$SQLQuery .= ', '.MYSQL_TABLE_PREFIX.'rel_tags_campaigns AS TagRelations'.$i;
				}
			}
		// From part - End
	
		// Where part - Start
		if ((strtolower(getType($ArrayCriterias)) == 'array' && count($ArrayCriterias) > 0) || (strtolower(getType($ArrayCriterias)) == 'string') || (count($ArrayTags) > 0))
			{
			$SQLQuery .= ' WHERE';
			}
		
		if (strtolower(getType($ArrayCriterias)) == 'array' && count($ArrayCriterias) > 0)
			{
			$IndexCounter = 0;
			foreach ($ArrayCriterias as $Field=>$Value)
				{
				if ($IndexCounter < 1)
					{
					$Spacer = ' ';
					}
				else
					{
					$Spacer = ' AND ';
					}

				if (is_array($Value))
					{
					$SQLQuery .= $Spacer.$Value['field'].' '.$Value['operator'].' "'.Database::$Interface->MakeValueSecureForQuery($Value['value']).'"';
					}
				else
					{
					$SQLQuery .= $Spacer.$Field.'="'.Database::$Interface->MakeValueSecureForQuery($Value).'"';
					}
				$IndexCounter++;
				}
			}
		else
			{
			if (strtolower(getType($ArrayCriterias)) == 'string')
				{
				$SQLQuery .= ' '.$ArrayCriterias;
				}
			}

		if (count($ArrayTags) > 0)
			{
			for ($i=0;$i<count($ArrayTags);$i++)
				{
				$SQLQuery .= ' AND Campaigns.CampaignID = TagRelations'.$i.'.RelCampaignID AND TagRelations'.$i.'.RelTagID = '.$ArrayTags[$i];
				}
			}
		// Where part - End
	
		// Group by part - Start
		if (count($ArrayTags) > 0)
			{
			if ($OnlyReturnTotal == false)
				{
				$SQLQuery .= ' GROUP BY Campaigns.CampaignID';
				}
			}
		// Group by part - End

		// Order by part - Start
		if (count($ArrayOrder) > 0)
			{
			foreach ($ArrayOrder as $Field=>$Value)
				{
				$SQLQuery .= ' ORDER BY '.$Field.' '.$Value;
				}
			}
		// Order by part - End
	
		// Limit part - Start
		if ($RecordCount != 0)
			{
			$SQLQuery .= ' LIMIT '.$RecordFrom.', '.$RecordCount;
			}
		// Limit part - End
	// Prepare SQL query - End

	$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);
	$ArrayCampaigns = array();
	if (mysql_num_rows($ResultSet) > 0)
		{
		if ($OnlyReturnTotal)
			{
		 		$TempArray = mysql_fetch_assoc($ResultSet);
			$ArrayCampaigns = $TempArray['TotalCampaigns'];
			}
		else
			{
			while ($EachCampaign = mysql_fetch_assoc($ResultSet))
				{
				// Lets take a precaution - Start
				$EachCampaign['SendProcessStartedOn'] = ($EachCampaign['SendProcessStartedOn'] == '0000-00-00 00:00:00' ? date('Y-m-d 00:00:00', strtotime(date('Y-m-d').' -1 day')) : $EachCampaign['SendProcessStartedOn']);
				// Lets take a precaution - End

				// $EachCampaign['EstimatedRecipients'] = self::EstimatedCampaignRecipients($EachCampaign['CampaignID']);
				$EachCampaign['RecipientLists'] = EmailQueue::GetRecipientLists($EachCampaign['CampaignID']);
				$EachCampaign['RecipientSegments'] = EmailQueue::GetRecipientLists($EachCampaign['CampaignID'], true);

				if ($ReturnTagList == true)
					{
					$EachCampaign['Tags'] = Tags::RetrieveTagsOfCampaign($EachCampaign['CampaignID']);
					}
				if (isset($ArrayStatisticsOptions['Days']))
					{
					$StatisticsEndDate = date('Y-m-d H:i:s', strtotime($EachCampaign['SendProcessStartedOn'].' +'.$ArrayStatisticsOptions['Days'].' days'));
					}
				if (isset($ArrayStatisticsOptions['Statistics']['OpenStatistics']) && $ArrayStatisticsOptions['Statistics']['OpenStatistics'] == true)
					{
					$EachCampaign['OpenStatistics'] = Statistics::RetrieveCampaignOpenStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['ClickStatistics']) && $ArrayStatisticsOptions['Statistics']['ClickStatistics'] == true)
					{
					$EachCampaign['ClickStatistics'] = Statistics::RetrieveCampaignClickStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['UnsubscriptionStatistics']) && $ArrayStatisticsOptions['Statistics']['UnsubscriptionStatistics'] == true)
					{
					$EachCampaign['UnsubscriptionStatistics'] = Statistics::RetrieveCampaignUnsubscriptionStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['ForwardStatistics']) && $ArrayStatisticsOptions['Statistics']['ForwardStatistics'] == true)
					{
					$EachCampaign['ForwardStatistics'] = Statistics::RetrieveCampaignForwardStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['BrowserViewStatistics']) && $ArrayStatisticsOptions['Statistics']['BrowserViewStatistics'] == true)
					{
					$EachCampaign['BrowserViewStatistics'] = Statistics::RetrieveCampaignBrowserViewStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['BounceStatistics']) && $ArrayStatisticsOptions['Statistics']['BounceStatistics'] == true)
					{
					$EachCampaign['BounceStatistics'] = Statistics::RetrieveCampaignBounceStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['BounceStatisticsTimeFrame']) && $ArrayStatisticsOptions['Statistics']['BounceStatisticsTimeFrame'] == true)
					{
					$EachCampaign['BounceStatisticsTimeFrame'] = Statistics::RetrieveCampaignBounceStatisticsTimeFrame($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['ClickPerformance']) && $ArrayStatisticsOptions['Statistics']['ClickPerformance'] == true)
					{
					$EachCampaign['OverallClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfCampaign($EachCampaign['CampaignID']);
					$EachCampaign['ClickPerformance'] = Statistics::RetrieveClickPerformanceDaysOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
					$EachCampaign['OverallAccountClickPerformance'] = Statistics::RetrieveOverallClickPerformanceOfAccount($EachCampaign['RelOwnerUserID']);
					}
				if (isset($ArrayStatisticsOptions['Statistics']['OpenPerformance']) && $ArrayStatisticsOptions['Statistics']['OpenPerformance'] == true)
					{
					$EachCampaign['OverallOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfCampaign($EachCampaign['CampaignID']);
					$EachCampaign['OpenPerformance'] = Statistics::RetrieveOpenPerformanceDaysOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
					$EachCampaign['OverallAccountOpenPerformance'] = Statistics::RetrieveOverallOpenPerformanceOfAccount($EachCampaign['RelOwnerUserID']);
					}

				$ArrayCampaigns[] = $EachCampaign;
				}
			}
		}
	else
		{
		return false;
		}
	return $ArrayCampaigns;
	}

/**
 * This function is enhanced version of RetrieveCampaigns.
 * 
 * What changed from original function?
 * =============
 * 
 * - SQL query is generated within function and more clean
 * - Supports 'AND' and 'OR' in criteria, and criteria can be grouped recursively
 * - Improved search functionality. Now supports multiple search fields and values.
 * - Parameters passed as an array
 * - Repeating scheduled campaigns now have the nearest repeating date and time set as RecSendDate and RecSendTime columns
 *
 * Parameters:
 * ==========
 *
 * ReturnSQLQuery	boolean		If set to true, sql query returned as string
 * ReturnTotalRows	boolean		If set to true, row total is returned (Using COUNT mysql function)
 * RowOrder			array		If set, ORDER BY statement is added to the query
 *								-- Parameters:
 *									Column				string					Column name from campaigns table to be ordered
 *									Type				string	(ASC | DESC)	Order type
 *								-- Note: You can only order by campaigns table columns
 * Pagination		array		If set, LIMIT statement is added to the query
 *								-- Parameters:
 *									Offset				integer					Offset of the first row
 *									Rows				integer					Maximum number of rows to return
 * Criteria			array		If set, WHERE statement is added to the query
 *								-- Parameters:
 *									Link			string	(AND | OR)		Link that will be used to link this group with previous one
 *									Column			string					Column name from campaigns table
 *									Operator		string					Operator. ex: '=', '!=', 'LIKE', ...
 *									Value			string					Value to be matched
 *									ValueWOQuote	string					If this is set, this value will be used and no quotes will be applied around the value
 *								-- Note: You can group criteria in arrays
 *								-- Note: Use %c% in column name to make it replaced with campaigns table alias
 * TagFilter		array		If set, only campaigns with given tags assigned retrieved
 * ClientFilter		integer		If set, only campaigns assigned to that client id will be retrieved
 * Content			boolean		If set to true, campaigns email content will also be returned
 * SplitTest		boolean		If set to true, split test information is returned with campaign
 * Reports			array		If set, chosen statistics will be retrieved for each campaign
 *								-- Parameters:
 *									Days			integer					How many days of statistics will be retrieved
 *									Statistics		array					Statistics are:
 *																				- OpenStatistics
 *																				- ClickStatistics
 *																				- UnsubscriptionStatistics
 *																				- ForwardStatistics
 *																				- BrowserViewStatistics
 *																				- BounceStatistics
 *																				- BounceStatisticsTimeFrame
 *																				- ClickPerformance
 *																				- OpenPerformance
 *								    Subscribers		array					Whether to return subscribers that showed the activities listed below:
 *																				- SubscribersOpened
 *																				- SubscribersClicked
 *
 * @param array $ArrayParameters
 * @return void
 * @author Mert Hurturk
 */
public static function RetrieveCampaigns_Enhanced($ArrayParameters)
	{
	$TablePrefix				=	MYSQL_TABLE_PREFIX;
	$ArrayTableAliases			=	array(
		'%c%'	=>	'Campaigns',
		'%tr%'	=>	'TagRelation',
		'%t%'	=>	'Tags',
		'%cr%'	=>	'ClientRelation'
		);
		
	$Query = '';
	
	// Setup SELECT part of the query - Start {
		$Query = 'SELECT ';

		// Add COUNT statement if ReturnTotalRows is set to true - Start {
		if (isset($ArrayParameters['ReturnTotalRows']) && $ArrayParameters['ReturnTotalRows'] == true)
			{
			$Query .= 'COUNT(*) AS TotalRows ';
			}
		// Add COUNT statement if ReturnTotalRows is set to true - End }

		// if ReturnTotalRows is set to false, add table names - Start {
		else
			{
			$Query .= $ArrayTableAliases['%c%'].'.* ';
			}
		// if ReturnTotalRows is set to false, add table names - End }

		$Query .= "\n";
	// Setup SELECT part of the query - End }

	// Setup FROM part of the query - Start {
		$Query .= 'FROM ';
		$Query .= $TablePrefix.'campaigns AS '.$ArrayTableAliases['%c%'].' ';

		// If any tag filter is given, add a from part for each given tag - Start {
			if (isset($ArrayParameters['TagFilter']) && count($ArrayParameters['TagFilter']) > 0)
				{
				foreach ($ArrayParameters['TagFilter'] as $EachTag)
					{
					$Query .= ', '.MYSQL_TABLE_PREFIX.'rel_tags_campaigns AS '.$ArrayTableAliases['%tr%'].$EachTag.' ';
					}
				}
		// If any tag filter is given, add a from part for each given tag - End }
		
		// If client filter is given, add a from part for client table - Start {
			if (isset($ArrayParameters['ClientFilter']) && $ArrayParameters['ClientFilter'] != '')
				{
				$Query .= ', '.MYSQL_TABLE_PREFIX.'rel_clients_campaigns AS '.$ArrayTableAliases['%cr%'].' ';
				}
		// If client filter is given, add a from part for client table - End }

		$Query .= "\n";
	// Setup FROM part of the query - End }

	// Setup WHERE part of the query - Start {
		if (isset($ArrayParameters['Criteria']) && count($ArrayParameters['Criteria']) > 0)
			{
			$Query .= 'WHERE ';
			
			$Query .= Database::$Interface->GetCriteriaString($ArrayParameters['Criteria'], $ArrayTableAliases);
			
			// If any tag filter is given, add a where part for each given tag - Start {
				if (isset($ArrayParameters['TagFilter']) && count($ArrayParameters['TagFilter']) > 0)
					{
					$ArrayTagCriteria = array();
					$ArrayTagCriteria['Link'] = 'AND';
					for ($i=0; $i<count($ArrayParameters['TagFilter']); $i++)
						{
						$ArrayTMPTagCriteria = array();
						if ($i > 0)
							{
							$ArrayTMPTagCriteria['Link'] = 'AND';
							}
						$ArrayTMPTagCriteria[] = array(
							'Column'		=>	$ArrayTableAliases['%c%'].'.CampaignID',
							'Operator'		=>	'=',
							'ValueWOQuote'	=>	$ArrayTableAliases['%tr%'].$ArrayParameters['TagFilter'][$i].'.RelCampaignID'
							);
						$ArrayTMPTagCriteria[] = array(
							'Link'		=>	'AND',
							'Column'	=>	$ArrayTableAliases['%tr%'].$ArrayParameters['TagFilter'][$i].'.RelTagID',
							'Operator'	=>	'=',
							'Value'		=>	$ArrayParameters['TagFilter'][$i]
							);
						$ArrayTagCriteria[] = $ArrayTMPTagCriteria;
						}
						
					$Query .= Database::$Interface->GetCriteriaString($ArrayTagCriteria, $ArrayTableAliases);
					}
			// If any tag filter is given, add a where part for each given tag - End }

			// If client filter is given, add a where part for given client id - Start {
				if (isset($ArrayParameters['ClientFilter']) && $ArrayParameters['ClientFilter'] != '')
					{
					$ArrayClientCriteria = array('Link'=>'AND');
					$ArrayClientCriteria[] = array(
						'Column'	=> $ArrayTableAliases['%c%'].'.CampaignID',
						'Operator'	=> '=',
						'ValueWOQuote'	=>	$ArrayTableAliases['%cr%'].'.RelCampaignID'
						);
					$ArrayClientCriteria[] = array(
						'Link'		=>	'AND',
						'Column'	=>	$ArrayTableAliases['%cr%'].'.RelClientID',
						'Operator'	=>	'=',
						'Value'		=>	$ArrayParameters['ClientFilter']
						);

					$Query .= Database::$Interface->GetCriteriaString($ArrayClientCriteria, $ArrayTableAliases);
					}
			// If client filter is given, add a where part for given client id - End }

			
			$Query .= "\n";
			}
	// Setup WHERE part of the query - End }

	// Setup GROUP part of the query - Start {
		if (isset($ArrayParameters['TagFilter']) && count($ArrayParameters['TagFilter']) > 0)
			{
			if (!isset($ArrayParameters['ReturnTotalRows']) || $ArrayParameters['ReturnTotalRows'] == false)
				{
				$Query .= 'GROUP BY '.$ArrayTableAliases['%c%'].'.CampaignID ';
				}
			}

		if ((! isset($ArrayParameters['TagFilter']) || count($ArrayParameters['TagFilter']) < 1) && (isset($ArrayParameters['ClientFilter']) && $ArrayParameters['ClientFilter'] != ''))
			{
			if (!isset($ArrayParameters['ReturnTotalRows']) || $ArrayParameters['ReturnTotalRows'] == false)
				{
				$Query .= 'GROUP BY '.$ArrayTableAliases['%c%'].'.CampaignID ';
				}
			}
	// Setup GROUP part of the query - End }

	// Setup ORDER part of the query - Start {
		if (isset($ArrayParameters['RowOrder']) && isset($ArrayParameters['RowOrder']['Column']) && isset($ArrayParameters['RowOrder']['Type']))
			{
			$OrderColumnName	= trim($ArrayParameters['RowOrder']['Column']);
			$OrderType			= trim($ArrayParameters['RowOrder']['Type']);
		
			if ($OrderColumnName != '' && $OrderType != '')
				{
				$Query .= 'ORDER BY '.$ArrayTableAliases['%c%'].'.'.$OrderColumnName.' '.$OrderType.' ';
				$Query .= "\n";
				}
			}
	// Setup ORDER part of the query - End }

	// Setup LIMIT part of the query - Start {
		if (isset($ArrayParameters['Pagination']) && isset($ArrayParameters['Pagination']['Offset']) && isset($ArrayParameters['Pagination']['Rows']))
			{
			$LimitOffset		= trim($ArrayParameters['Pagination']['Offset']);
			$LimitRows			= trim($ArrayParameters['Pagination']['Rows']);

			if ($LimitOffset != '' && $LimitRows != '')
				{
				$Query .= 'LIMIT '.$LimitOffset.','.$LimitRows.' ';
				$Query .= "\n";
				}
			}
	// Setup LIMIT part of the query - End }

	// Return sql query string if ReturnSQLQuery is set to true - Start {
		if (isset($ArrayParameters['ReturnSQLQuery']) && $ArrayParameters['ReturnSQLQuery'] == true)
			{
			return $Query;
			}
	// Return sql query string if ReturnSQLQuery is set to true - End }
	
	// Run SQL query - Start {
	$ResultSet = Database::$Interface->ExecuteQuery($Query);
	// Run SQL query - End }

	// Pass sql result to an array - Start {
		$ArrayCampaigns = array();
	if (mysql_num_rows($ResultSet) > 0)
		{
		// If ReturnTotalRows is set to true, pass only count value to array  - Start {
		if (isset($ArrayParameters['ReturnTotalRows']) && $ArrayParameters['ReturnTotalRows'] == true)
			{
	 		$TempArray = mysql_fetch_assoc($ResultSet);
			$ArrayCampaigns = $TempArray['TotalRows'];
			}
		// If ReturnTotalRows is set to true, pass only count value to array  - End }
		
		// Pass rows to an array and make calculations - Start {
		else
			{
			Core::LoadObject('emails');
			while ($EachCampaign = mysql_fetch_assoc($ResultSet))
				{
				// $EachCampaign['EstimatedRecipients'] = self::EstimatedCampaignRecipients($EachCampaign['CampaignID']);
				if (!isset($ArrayParameters['RecipientLists']) || (isset($ArrayParameters['RecipientLists']) && $ArrayParameters['RecipientLists'] == TRUE))
					{
					$EachCampaign['RecipientLists'] = EmailQueue::GetRecipientLists($EachCampaign['CampaignID']);
					}
				if (!isset($ArrayParameters['RecipientSegments']) || (isset($ArrayParameters['RecipientSegments']) && $ArrayParameters['RecipientSegments'] == TRUE))
					{
					$EachCampaign['RecipientSegments'] = EmailQueue::GetRecipientLists($EachCampaign['CampaignID'], true);
					}
				if (!isset($ArrayParameters['Tags']) || (isset($ArrayParameters['Tags']) && $ArrayParameters['Tags'] == TRUE))
					{
					$EachCampaign['Tags'] = Tags::RetrieveTagsOfCampaign($EachCampaign['CampaignID']);
					}

				if (isset($ArrayParameters['Reports']) && count($ArrayParameters['Reports']) > 1)
					{
					$StatisticsEndDate = date('Y-m-d H:i:s', strtotime($EachCampaign['SendProcessStartedOn'].' +'.$ArrayParameters['Reports']['Days'].' days'));

					foreach ($ArrayParameters['Reports']['Statistics'] as $EachStatistic)
						{
						if ($EachStatistic == 'OpenStatistics')
							{
							$EachCampaign['OpenStatistics']					= Statistics::RetrieveCampaignOpenStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'ClickStatistics')
							{
							$EachCampaign['ClickStatistics']				= Statistics::RetrieveCampaignClickStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'UnsubscriptionStatistics')
							{
							$EachCampaign['UnsubscriptionStatistics']		= Statistics::RetrieveCampaignUnsubscriptionStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'ForwardStatistics')
							{
							$EachCampaign['ForwardStatistics']				= Statistics::RetrieveCampaignForwardStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'BrowserViewStatistics')
							{
							$EachCampaign['BrowserViewStatistics']			= Statistics::RetrieveCampaignBrowserViewStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'BounceStatistics')
							{
							$EachCampaign['BounceStatistics']				= Statistics::RetrieveCampaignBounceStatistics($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
							}
						elseif ($EachStatistic == 'BounceStatisticsTimeFrame')
							{
							$EachCampaign['BounceStatisticsTimeFrame']		= Statistics::RetrieveCampaignBounceStatisticsTimeFrame($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachCampaign['SendProcessStartedOn'], $StatisticsEndDate);
							}
						elseif ($EachStatistic == 'ClickPerformance')
							{
							$EachCampaign['OverallClickPerformance']		= Statistics::RetrieveOverallClickPerformanceOfCampaign($EachCampaign['CampaignID']);
							$EachCampaign['ClickPerformance']				= Statistics::RetrieveClickPerformanceDaysOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
							$EachCampaign['OverallAccountClickPerformance']	= Statistics::RetrieveOverallClickPerformanceOfAccount($EachCampaign['RelOwnerUserID']);
							}
						elseif ($EachStatistic == 'OpenPerformance')
							{
							$EachCampaign['OverallOpenPerformance']			= Statistics::RetrieveOverallOpenPerformanceOfCampaign($EachCampaign['CampaignID']);
							$EachCampaign['OpenPerformance']				= Statistics::RetrieveOpenPerformanceDaysOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
							$EachCampaign['OverallAccountOpenPerformance']	= Statistics::RetrieveOverallOpenPerformanceOfAccount($EachCampaign['RelOwnerUserID']);
							}
						}

					foreach ($ArrayParameters['Reports']['Subscribers'] as $EachStatistic)
						{
						if ($EachStatistic == 'SubscribersOpened')
							{
							$activity_result_set = Statistics::RetrieveCampaignOpens($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], 0);
							$array_subscribers = array();
							while ($each_row = mysql_fetch_assoc($activity_result_set))
								{
								$array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$each_row['RelSubscriberID']), $each_row['RelListID']);
								$tmp_array = array(
													'SubscriberID'		=> $each_row['RelSubscriberID'],
													'ListID'			=> $each_row['RelListID'],
													'Email'				=> ($array_subscriber['EmailAddress'] == '' ? '' : $array_subscriber['EmailAddress']),
													'TotalOpens'		=> number_format($each_row['TotalOpens']),
													'Activities'		=> array()
													);
								$array_subscriber_activity = Statistics::RetrieveSubscriberCampaignActivity($EachCampaign['CampaignID'], $array_subscriber['SubscriberID'], $each_row['RelListID']);
								foreach ($array_subscriber_activity as $timestamp=>$array_activity)
									{
									$activity_time = date('Y-m-d H:i:s', $array_activity[0]);
									$activity_name = $array_activity[1];
									$tmp_array['Activities'][] = array('time'=>$activity_time,'activity'=>$activity_name);
									}
								$array_subscribers[] = $tmp_array;
								}
							$EachCampaign['SubscribersOpened'] = $array_subscribers;
							}
						else if ($EachStatistic == 'SubscribersClicked')
							{
							$activity_result_set = Statistics::RetrieveCampaignLinkClicks($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], 'Subscribers', 0);
							$array_subscribers = array();
							while ($each_row = mysql_fetch_assoc($activity_result_set))
								{
								$array_subscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$each_row['RelSubscriberID']), $each_row['RelListID']);
								$tmp_array = array(
													'SubscriberID'		=> $each_row['RelSubscriberID'],
													'ListID'			=> $each_row['RelListID'],
													'Email'				=> ($array_subscriber['EmailAddress'] == '' ? '' : $array_subscriber['EmailAddress']),
													'TotalClicks'		=> number_format($each_row['TotalClicks']),
													'Activities'		=> array()
													);
								$array_subscriber_activity = Statistics::RetrieveSubscriberCampaignActivity($EachCampaign['CampaignID'], $array_subscriber['SubscriberID'], $each_row['RelListID']);
								foreach ($array_subscriber_activity as $timestamp=>$array_activity)
									{
									$activity_time = date('Y-m-d H:i:s', $array_activity[0]);
									$activity_name = $array_activity[1];
									$tmp_array['Activities'][] = array('time'=>$activity_time,'activity'=>$activity_name);
									}
								$array_subscribers[] = $tmp_array;
								}
							$EachCampaign['SubscribersClicked'] = $array_subscribers;
							}
						}
					}

				// If schedule type is recursive, find nearest send date and time - Start {
					if ($EachCampaign['ScheduleType'] == 'Recursive')
						{
						$repeatingCampaignSchedule = new O_RepeatingCampaignSchedule(
							$EachCampaign['ScheduleRecMonths'] == '*' ? '*' : explode(',', $EachCampaign['ScheduleRecMonths']),
							$EachCampaign['ScheduleRecDaysOfWeek'] == '*' ? '*' : explode(',', $EachCampaign['ScheduleRecDaysOfWeek']),
							$EachCampaign['ScheduleRecDaysOfMonth'] == '*' ? '*' : explode(',', $EachCampaign['ScheduleRecDaysOfMonth']),
							explode(',', $EachCampaign['ScheduleRecHours']),
							explode(',', $EachCampaign['ScheduleRecMinutes'])
						);

						$currentTime = '';
						$found = preg_match("/[+|-][\d]{2}/uiUsm", $EachCampaign['SendTimeZone'], $matches);
						if ($found > 0) {
							$found = str_replace('+0', '+', $matches[0]);
							$currentTime = date('Y-m-d H:i', strtotime(intval($found).' hours'));
						}

						$guessedScheduleDate = $repeatingCampaignSchedule->guessNextSendDateTime($currentTime);

						$EachCampaign['RecSendDate'] = date('Y-m-d', strtotime($guessedScheduleDate));
						$EachCampaign['RecSendTime'] = date('H:i', strtotime($guessedScheduleDate));
						$EachCampaign['Schedule'] = $repeatingCampaignSchedule;
						}
				// If schedule type is recursive, find nearest send date and time - End }

				// Retrieve split test information - Start {
				if (!isset($ArrayParameters['SplitTests']) || (isset($ArrayParameters['SplitTests']) && $ArrayParameters['SplitTests'] == TRUE))
					{
					$EachCampaign['SplitTest'] = false;
					$SplitTestInformation = SplitTests::RetrieveTestOfACampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID']);
					if ($SplitTestInformation != false)
						{
						$EachCampaign['SplitTest'] = $SplitTestInformation[0];
						}
					}
				// Retrieve split test information - End }

				// Retrieve campaign content - Start {
				if (isset($ArrayParameters['Content']) && $ArrayParameters['Content'] == true)
					{
					if ($EachCampaign['SplitTest'] != false)
						{
						$EachCampaign['Emails'] = Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);
						}
					else
						{
						$EachCampaign['Email'] = Emails::RetrieveEmail(array('*'), array('EmailID' => $EachCampaign['RelEmailID']));
						}
					}
				// Retrieve campaign content - End }
				
				$ArrayCampaigns[] = $EachCampaign;
				}
			}
		// Pass rows to an array and make calculations - End }
		}
	// Pass sql result to an array - End }

	return $ArrayCampaigns;
	}

/**
 * Deletes campaigns
 *
 * @param integer $OwnerUserID Owner user id of campaigns to be deleted
 * @param array $ArrayCampaignsIDs Campaign ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
function Delete($OwnerUserID, $ArrayCampaignIDs, $EmailID = 0)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	Core::LoadObject('queue');
	Core::LoadObject('clients');
	Core::LoadObject('statistics');
	Core::LoadObject('split_tests');

	if (count($ArrayCampaignIDs) > 0)
		{
		foreach ($ArrayCampaignIDs as $EachID)
			{
			self::DeleteCampaignRecipients($OwnerUserID, array(), $EachID);
			Core::DeleteFBLReports($OwnerUserID, 0, $EachID);
			EmailQueue::Delete(0, $EachID);
			Clients::DeleteAssignedClientCampaigns(0, $EachID);
			Statistics::Delete(0, $EachID);
			self::DeleteRecursiveCampaignLog($EachID);
			Tags::DeleteCampaignRelationships(0, $EachID, $OwnerUserID);
			SplitTests::RemoveTestOfCampaign($EachID, $OwnerUserID);
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'CampaignID'=>$EachID), MYSQL_TABLE_PREFIX.'campaigns');
			}
		}
	elseif ($EmailID > 0)
		{
		$ArrayCampaigns = self::RetrieveCampaigns(array('*'), array('RelEmailID' => $EmailID), array('CampaignName' => 'ASC'), 0, 0);
		
		if (count($ArrayCampaigns) > 0)
			{
			foreach ($ArrayCampaigns as $Index=>$ArrayEachCampaign)
				{
				self::DeleteCampaignRecipients($OwnerUserID, array(), $ArrayEachCampaign['CampaignID']);
				Core::DeleteFBLReports($OwnerUserID, 0, $ArrayEachCampaign['CampaignID']);
				EmailQueue::Delete(0, $ArrayEachCampaign['CampaignID']);
				Clients::DeleteAssignedClientCampaigns(0, $ArrayEachCampaign['CampaignID']);
				Statistics::Delete(0, $ArrayEachCampaign['CampaignID']);
				SplitTests::RemoveTestOfCampaign($ArrayEachCampaign['CampaignID'], $OwnerUserID);
				self::DeleteRecursiveCampaignLog($ArrayEachCampaign['CampaignID']);
				}
			}
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelEmailID'=>$EmailID), MYSQL_TABLE_PREFIX.'campaigns');
		}
	elseif ($OwnerUserID > 0)
		{
		$ArrayCampaigns = self::RetrieveCampaigns(array('*'), array('RelOwnerUserID' => $OwnerUserID), array('CampaignName' => 'ASC'), 0, 0);
		
		if (count($ArrayCampaigns) > 0)
			{
			foreach ($ArrayCampaigns as $Index=>$ArrayEachCampaign)
				{
				self::DeleteCampaignRecipients($OwnerUserID, array(), $ArrayEachCampaign['CampaignID']);
				Core::DeleteFBLReports($OwnerUserID, 0, $ArrayEachCampaign['CampaignID']);
				EmailQueue::Delete(0, $ArrayEachCampaign['CampaignID']);
				Clients::DeleteAssignedClientCampaigns(0, $ArrayEachCampaign['CampaignID']);
				Statistics::Delete(0, $ArrayEachCampaign['CampaignID']);
				self::DeleteRecursiveCampaignLog($ArrayEachCampaign['CampaignID']);
				SplitTests::RemoveTestOfCampaign($ArrayEachCampaign['CampaignID'], $OwnerUserID);
				}
			}
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID), MYSQL_TABLE_PREFIX.'campaigns');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Campaign', array($OwnerUserID, $ArrayCampaignIDs, $EmailID));
	// Plug-in hook - End

	return true;
	}

/**
 * Deletes recursive process log of a campaign
 *
 * @param integer $CampaignID 
 * @return void
 * @author Cem Hurturk
 */
function DeleteRecursiveCampaignLog($CampaignID)
	{
	Database::$Interface->DeleteRows(array('RelCampaignID'=>$CampaignID), MYSQL_TABLE_PREFIX.'campaigns_recursive_log');
	}

/**
 * Deletes campaign recipients
 *
 * @return boolean
 * @author Mert Hurturk
 **/
function DeleteCampaignRecipients($OwnerUserID, $ArrayListIDs, $CampaignID = 0, $SegmentID = 0)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	if (count($ArrayListIDs) > 0)
		{
		foreach ($ArrayListIDs as $EachID)
			{
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelListID'=>$EachID), MYSQL_TABLE_PREFIX.'campaign_recipients');
			}
		}
	elseif ($CampaignID > 0)
		{
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelCampaignID'=>$CampaignID), MYSQL_TABLE_PREFIX.'campaign_recipients');
		}
	elseif ($SegmentID > 0)
		{
		Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'RelSegmentID'=>$SegmentID), MYSQL_TABLE_PREFIX.'campaign_recipients');
		}

	return true;
	}

/**
 * Fetches the remote URL and returns as string
 *
 * @param string $URL URL of the remote content
 * @return string
 * @author Cem Hurturk
 **/
function FetchRemoteContent($URL)
	{
	$RemoteContent = Core::DataPostToRemoteURL($URL, array(), 'GET', false, '', '', 60, false);

	if ($RemoteContent[0] == false)
		{
		return false;
		}
	else
		{
		return $RemoteContent[1];
		}
	}

/**
 * Returns the list of pending campaigns for delivery
 *
 * @return array
 * @author Cem Hurturk
 */
function GetPendingCampaigns()
	{
	$ArrayPendingCampaigns = array();

	// Regular email campaigns - Start {
	$TMPArray = self::RetrieveCampaigns(array('*'), array('CampaignStatus' => 'Ready', array('field' => 'ScheduleType', 'operator' => '!=', 'value' => 'Not Scheduled')), array('CampaignID'=>'ASC'), 0, 0);

	if ($TMPArray !== FALSE)
	{
		foreach ($TMPArray as $Index=>$EachCampaign)
			{
			if ($EachCampaign['ScheduleType'] == 'Immediate')
				{
				$ArrayPendingCampaigns[] = $EachCampaign['CampaignID'];
				}
			elseif ($EachCampaign['ScheduleType'] == 'Future')
				{
				// Check if scheduled time is equeal or before now
				$TimeZoneDifferenceSeconds = Core::CalculateTimeZoneDifference(date('Z'), Core::ConvertTimeZoneString($EachCampaign['SendTimeZone']));

				$ScheduledDate = $EachCampaign['SendDate'].' '.($TimeZoneDifferenceSeconds > 0 ? '+'.$TimeZoneDifferenceSeconds : $TimeZoneDifferenceSeconds).' seconds '.$EachCampaign['SendTime'];

				if (strtotime($ScheduledDate) <= time())
					{
					$ArrayPendingCampaigns[] = $EachCampaign['CampaignID'];
					}
				}
			elseif ($EachCampaign['ScheduleType'] == 'Recursive')
				{
				if (self::IsRecCampaignReadyToSendNow($EachCampaign) == true)
					{
					$newCampaignInformation = $EachCampaign;
					unset($newCampaignInformation['CampaignID']);
					unset($newCampaignInformation['RecipientLists']);
					unset($newCampaignInformation['RecipientSegments']);
					$newCampaignInformation['CampaignStatus'] = 'Ready';
					$newCampaignInformation['ScheduleType'] = 'Immediate';
					$newCampaignInformation['ScheduleRecDaysOfWeek'] = '';
					$newCampaignInformation['ScheduleRecDaysOfMonth'] = '';
					$newCampaignInformation['ScheduleRecMonths'] = '';
					$newCampaignInformation['ScheduleRecHours'] = '';
					$newCampaignInformation['ScheduleRecMinutes'] = '';
					$newCampaignInformation['ScheduleRecSendMaxInstance'] = '';
					$newCampaignInformation['ScheduleRecSentInstances'] = '';
					$newCampaignInformation['TotalSent'] = '0';
					$newCampaignInformation['TotalFailed'] = '0';
					$newCampaignInformation['TotalOpens'] = '0';
					$newCampaignInformation['UniqueOpens'] = '0';
					$newCampaignInformation['TotalClicks'] = '0';
					$newCampaignInformation['UniqueClicks'] = '0';
					$newCampaignInformation['TotalForwards'] = '0';
					$newCampaignInformation['UniqueForwards'] = '0';
					$newCampaignInformation['TotalViewsOnBrowser'] = '0';
					$newCampaignInformation['UniqueViewsOnBrowser'] = '0';
					$newCampaignInformation['TotalUnsubscriptions'] = '0';
					$newCampaignInformation['TotalHardBounces'] = '0';
					$newCampaignInformation['TotalSoftBounces'] = '0';
					$newCampaignInformation['BenchmarkEmailsPerSecond'] = '0';
					$newCampaignInformation['LastActivityDateTime'] = '0000-00-00 00:00:00';

					$newCampaignID = Campaigns::CreateCampaign($newCampaignInformation);
					foreach ($EachCampaign['RecipientSegments'] as $each) {
						Campaigns::AddRecipientToCampaign($newCampaignID, $newCampaignInformation['RelOwnerUserID'], $each[0], $each[1]);
					}

					Core::LoadObject('tags');
					$tags = Tags::RetrieveTagsOfCampaign($EachCampaign['CampaignID']);
					if (count($tags) > 0) {
						foreach ($tags as $eachTag) {
							Tags::AssignToCampaigns(array($newCampaignID), $eachTag['TagID'], $EachCampaign['RelOwnerUserID']);
						}
					}

					Core::LoadObject('clients');
					$clients = Clients::RetrieveClientsOfCampaign($EachCampaign['CampaignID']);
					if ($clients != false && count($clients) > 0) {
						foreach ($clients as $eachClient) {
							Clients::AssignCampaigns($eachClient['ClientID'], array($EachCampaign['CampaignID']));
						}
					}

					$ArrayPendingCampaigns[] = $newCampaignID;
					}
				}
			}
	}
	// Regular email campaigns - End }

	// A/B split testing campaigns - Start {
	$PausedCampaigns = Campaigns::RetrieveCampaigns_Enhanced(array(
		"Criteria"	=>	array(
			array(
				'Column'	=>	'%c%.CampaignStatus',
				'Operator'	=>	'=',
				'Value'		=>	'Paused'
				),
			array(
				'Link'		=>	'AND',
				'Column'	=>	'%c%.ScheduleType',
				'Operator'	=>	'!=',
				'Value'		=>	'Not Scheduled'
				),
			)
		));

	Core::LoadObject('statistics');
	Core::LoadObject('split_tests');

	foreach ($PausedCampaigns as $Index=>$EachCampaign)
		{
		if ($EachCampaign['SplitTest'] != false)
			{
			// Check if the campaign has been paused by user or by A/B testing procedure - Start {
			if ($EachCampaign['SplitTest']['SplitTestingStatus'] != 'Waiting For Winner')
				{
				continue;
				}
			// Check if the campaign has been paused by user or by A/B testing procedure - End }

			$TestDuration	= $EachCampaign['SplitTest']['TestDuration'];
			$Winner			= $EachCampaign['SplitTest']['Winner'];

			if ((strtotime($EachCampaign['LastActivityDateTime']) + $TestDuration) <= time())
				{
				$TestEmailVersions	= Emails::RetrieveEmailsOfTest($EachCampaign['SplitTest']['TestID'], $EachCampaign['CampaignID']);

				// Select the winner email version and update split test record - Start {
				if ($Winner == 'Highest Open Rate')
					{
					$TMPUniqueOpens = array();
					
					foreach ($TestEmailVersions as $Index=>$EachTestEmail)
						{
						$TMPUniqueOpens[] = Statistics::TotalUniqueOpensOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachTestEmail['EmailID']);
						}
					arsort($TMPUniqueOpens);
					$TMPUniqueOpens = array_flip($TMPUniqueOpens);

					$WinnerEmailVersion = $TestEmailVersions[array_shift($TMPUniqueOpens)];

					SplitTests::Update($EachCampaign['SplitTest']['TestID'], array('RelWinnerEmailID' => $WinnerEmailVersion['EmailID']));
					}
				elseif ($Winner == 'Most Unique Clicks')
					{
					$TMPUniqueClicks = array();
					
					foreach ($TestEmailVersions as $Index=>$EachTestEmail)
						{
						$TMPUniqueClicks[] = Statistics::TotalUniqueClicksOfCampaign($EachCampaign['CampaignID'], $EachCampaign['RelOwnerUserID'], $EachTestEmail['EmailID']);
						}
					arsort($TMPUniqueClicks);
					$TMPUniqueClicks = array_flip($TMPUniqueClicks);

					$WinnerEmailVersion = $TestEmailVersions[array_shift($TMPUniqueClicks)];

					SplitTests::Update($EachCampaign['SplitTest']['TestID'], array('RelWinnerEmailID' => $WinnerEmailVersion['EmailID']));
					}
				// Select the winner email version and update split test record - End }

				$ArrayPendingCampaigns[] = $EachCampaign['CampaignID'];

				self::Update(array('CampaignStatus' => 'Ready'), array('CampaignID' => $EachCampaign['CampaignID']));
				}
			}
		}
	// A/B split testing campaigns - End }

	return $ArrayPendingCampaigns;
	}

function IsRecCampaignReadyToSendNow($ArrayCampaign)
	{
	// Set criterias - Start
	$CurrentDayOfMonth		= date('j');
	$CurrentDayOfWeek		= date('w');
	$CurrentMonth			= date('n');
	$CurrentHour			= date('H');
	$CurrentMinute			= date('i');
	// Set criterias - End

	// Strip leading zero from the minute - Start
	if (substr($CurrentMinute, 0, 1) == '0')
		{
		$CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
		}
	// Strip leading zero from the minute - End

	// Round current minute to 0, 15, 30 or 45 - Start
	if (($CurrentMinute >= 0) && ($CurrentMinute <= 14))
		{
		$CurrentMinute = 0;
		}
	elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29))
		{
		$CurrentMinute = 15;
		}
	elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44))
		{
		$CurrentMinute = 30;
		}
	elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59))
		{
		$CurrentMinute = 45;
		}
	// Round current minute to 0, 15, 30 or 45 - End

	// Check if campaign recursive schedule settings match the current time - Start
	if (($ArrayCampaign['ScheduleRecMonths'] == '*') || ($ArrayCampaign['ScheduleRecMonths'] == $CurrentMonth) || (eregi('^'.$CurrentMonth.',', $ArrayCampaign['ScheduleRecMonths']) == true) || (eregi(','.$CurrentMonth.',', $ArrayCampaign['ScheduleRecMonths']) == true) || (eregi(','.$CurrentMonth.'$', $ArrayCampaign['ScheduleRecMonths']) == true))
		{
		if (((($ArrayCampaign['ScheduleRecDaysOfMonth'] == '*') || ($ArrayCampaign['ScheduleRecDaysOfMonth'] == $CurrentDayOfMonth) || (eregi('^'.$CurrentDayOfMonth.',', $ArrayCampaign['ScheduleRecDaysOfMonth']) == true) || (eregi(','.$CurrentDayOfMonth.',', $ArrayCampaign['ScheduleRecDaysOfMonth']) == true) || (eregi(','.$CurrentDayOfMonth.'$', $ArrayCampaign['ScheduleRecDaysOfMonth']) == true)) || (($ArrayCampaign['ScheduleRecDaysOfWeek'] == '*') || ($ArrayCampaign['ScheduleRecDaysOfWeek'] == $CurrentDayOfWeek) || (eregi('^'.$CurrentDayOfWeek.',', $ArrayCampaign['ScheduleRecDaysOfWeek']) == true) || (eregi(','.$CurrentDayOfWeek.',', $ArrayCampaign['ScheduleRecDaysOfWeek']) == true) || (eregi(','.$CurrentDayOfWeek.'$', $ArrayCampaign['ScheduleRecDaysOfWeek']) == true))) || (($ArrayCampaign['ScheduleRecDaysOfMonth'] == $CurrentDayOfMonth) && ($ArrayCampaign['ScheduleRecDaysOfWeek'] == $CurrentDayOfWeek)))
			{
			if (($ArrayCampaign['ScheduleRecHours'] == '*') || ($ArrayCampaign['ScheduleRecHours'] == $CurrentHour) || (eregi('^'.$CurrentHour.',', $ArrayCampaign['ScheduleRecHours']) == true) || (eregi(','.$CurrentHour.',', $ArrayCampaign['ScheduleRecHours']) == true) || (eregi(','.$CurrentHour.'$', $ArrayCampaign['ScheduleRecHours']) == true))
				{
				if (($ArrayCampaign['ScheduleRecMinutes'] == '*') || ($ArrayCampaign['ScheduleRecMinutes'] == $CurrentMinute) || (eregi('^'.$CurrentMinute.',', $ArrayCampaign['ScheduleRecMinutes']) == true) || (eregi(','.$CurrentMinute.',', $ArrayCampaign['ScheduleRecMinutes']) == true) || (eregi(','.$CurrentMinute.'$', $ArrayCampaign['ScheduleRecMinutes']) == true))
					{
					if (($ArrayCampaign['ScheduleRecSendMaxInstance'] > $ArrayCampaign['ScheduleRecSentInstances']) || ($ArrayCampaign['ScheduleRecSendMaxInstance'] == 0))
						{
						// Check if this sending has been ran before or not - Start
						if (self::CheckIfRecursiveCampaignExecutedNow($ArrayCampaign['CampaignID'], $CurrentDayOfWeek, $CurrentDayOfMonth, $CurrentMonth, $CurrentHour, $CurrentMinute) == true)
							{
							return false;
							}
						// Check if this sending has been ran before or not - End

						// Record the process to recursive log - Start
						// THIS IS COMMENTED OUT BECAUSE IT CAUSED TO LOG ALL RECURSIVE CAMPAIGNS, NOT JUST THE SENDING ONE AND THIS PREVENTED TO SEND ALL RECURSIVE CAMPAIGNS
						 self::LogToCampaignRecursiveLog($ArrayCampaign['CampaignID'], $CurrentDayOfWeek, $CurrentDayOfMonth, $CurrentMonth, $CurrentHour, $CurrentMinute);
						// Record the process to recursive log - End

						// Increase the recursive sending instance counter - Start
						// THIS IS COMMENTED OUT BECAUSE IT CAUSED TO LOG ALL RECURSIVE CAMPAIGNS, NOT JUST THE SENDING ONE AND THIS PREVENTED TO SEND ALL RECURSIVE CAMPAIGNS
						 self::Update(array('ScheduleRecSentInstances' => $ArrayCampaign['ScheduleRecSentInstances'] + 1), array('CampaignID' => $ArrayCampaign['CampaignID']));
						// Increase the recursive sending instance counter - End

						return true;
						}
					}
				}
			}
		}
	// Check if campaign recursive schedule settings match the current time - End

	return false;
	}

/**
 * Checks if the recursive campaign is executed now
 *
 * @param integer $CampaignID
 * @param string $CurrentDayOfWeek 
 * @param string $CurrentDayOfMonth 
 * @param string $CurrentMonth 
 * @param string $CurrentHour 
 * @param string $CurrentMinute 
 * @return boolean
 * @author Cem Hurturk
 */
function CheckIfRecursiveCampaignExecutedNow($CampaignID, $DayOfWeek, $DayOfMonth, $Month, $Hour, $Minute)
	{
	$TotalFound = Database::$Interface->GetRows(array('COUNT(*) AS TotalFound'), array(MYSQL_TABLE_PREFIX.'campaigns_recursive_log'), array('RelCampaignID' => $CampaignID, 'ProcessDayOfWeek' => $DayOfWeek, 'ProcessDayOfMonth' => $DayOfMonth, 'ProcessMonth' => $Month, 'ProcessHour' => $Hour, 'ProcessMinute' => $Minute), array(), 0, 0, 'AND', false, false);
	$TotalFound = $TotalFound[0]['TotalFound'];
	
	if ($TotalFound > 0)
		{
		return true;
		}
	else
		{
		return false;
		}
	}

/**
 * Logs the recursive campaign process
 *
 * @param integer $CampaignID
 * @param string $CurrentDayOfWeek 
 * @param string $CurrentDayOfMonth 
 * @param string $CurrentMonth 
 * @param string $CurrentHour 
 * @param string $CurrentMinute 
 * @return void
 * @author Cem Hurturk
 */
function LogToCampaignRecursiveLog($CampaignID, $DayOfWeek, $DayOfMonth, $Month, $Hour, $Minute)
	{
	$ArrayFieldnValues = array(
								'LogID'					=> '',
								'RelCampaignID'			=> $CampaignID,
								'ProcessDayOfWeek'		=> $DayOfWeek,
								'ProcessDayOfMonth'		=> $DayOfMonth,
								'ProcessMonth'			=> $Month,
								'ProcessHour'			=> $Hour,
								'ProcessMinute'			=> $Minute,
								'ProcessDate'			=> date('Y-m-d H:i:s'),
								);
	Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX.'campaigns_recursive_log');
	
	return;
	}

/**
 * Returns the estimated number of recipients of the campaign. This number can not be absolutely unless your campaign is being processed by send engine
 *
 * @return int
 * @author Cem Hurturk
 **/
function EstimatedCampaignRecipients($CampaignID)
	{
	$EstimatedCampaignRecipients = 0;

	Core::LoadObject('queue');
	Core::LoadObject('subscribers');
	
	// Retrieve recipient subscriber lists and segments - Start
	$ArrayRecipientLists = EmailQueue::GetRecipientLists($CampaignID, true);
	// Retrieve recipient subscriber lists and segments - End

	// Loop subscribers of recipient lists - Start
	$EstimatedCampaignRecipients = 0;
	if (count($ArrayRecipientLists) > 0)
		{
		foreach ($ArrayRecipientLists as $Key=>$ArrayRecipientList)
			{
			$ListID				= $ArrayRecipientList[0];
			$SegmentID			= $ArrayRecipientList[1];

			// $EstimatedCampaignRecipients += Subscribers::GetFoundTotal('', '', $ListID, $SegmentID, false);
			$EstimatedCampaignRecipients += Subscribers::GetActiveTotal($ListID, $SegmentID);
			}
		}
	// Loop subscribers of recipient lists - End

	return $EstimatedCampaignRecipients;
	}

/**
 * Returns the list of campaigns that are sent to the target subscriber list
 *
 * @param string $ListID 
 * @return void
 * @author Cem Hurturk
 */
function GetCampaignsSentToList($ListID)
	{
	$ArrayRelations = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX.'campaign_recipients'), array('RelListID' => $ListID), array(), 0, 0, 'AND', false, false);

	$ArrayCampaignList = array();
	foreach ($ArrayRelations as $Key=>$ArrayEachRelation)
		{
		$ArrayCampaign = self::RetrieveCampaign(array('*'), array('CampaignID' => $ArrayEachRelation['RelCampaignID'], 'CampaignStatus' => 'Sent', 'PublishOnRSS' => 'Enabled'), array());

		if ($ArrayCampaign != false)
			{
			$ArrayCampaignList[] = $ArrayCampaign;
			}
		}

	return $ArrayCampaignList;
	}

/**
 * Calculates unique open rates, click rates, etc.
 *
 * @return void
 * @author Cem Hurturk
 */
function CalculateCampaignStatisticsRates($ArrayCampaign, $StatisticsType = 'UniqueOpenRate')
	{
	$Data = 0;
	if ($StatisticsType == 'UniqueOpenRate')
		{
		$Data = ceil((100 * $ArrayCampaign['UniqueOpens']) / $ArrayCampaign['TotalSent']);
		}

	return ($Data == false ? 0 : $Data);
	}

} // END class Campaigns
?>