<?php

// Required modules - Start
Core::LoadObject('subscribers');
// Required modules - End

/**
 * Segments class
 *
 * This class holds all segment related functions
 * @package Oempro
 * @author Octeth
 * */
class Segments extends Core {

    /**
     * Required fields for segments
     *
     * @static array
     * */
    public static $ArrayRequiredFields = array('RelOwnerUserID', 'RelListID', 'SegmentName', 'SegmentRules');

    /**
     * Default values for fields of a segment
     *
     * @static array
     * */
    public static $ArrayDefaultValuesForFields = array('SegmentOperator' => 'and');

    /**
     * Default fields for rules
     *
     * @static array
     * */
    public static $RuleDefaultFields = array(
        array('CustomFieldID' => 'SubscriberID', 'ValidationMethod' => 'Numbers'),
        array('CustomFieldID' => 'EmailAddress', 'ValidationMethod' => 'Email address'),
        array('CustomFieldID' => 'BounceType', 'ValidationMethod' => 'Enum', 'Values' => '[[Not Bounced]||[Not Bounced]],,,[[Hard Bounced]||[Hard]],,,[[Soft Bounced]||[Soft]]'),
        array('CustomFieldID' => 'SubscriptionStatus', 'ValidationMethod' => 'Enum', 'Values' => '[[Opt-In Pending]||[Opt-In Pending]],,,[[Subscribed]||[Subscribed]],,,[[Opt-Out Pending]||[Opt-Out Pending]],,,[[Unsubscribed]||[Unsubscribed]]'),
        array('CustomFieldID' => 'SubscriptionDate', 'ValidationMethod' => 'Date', 'ValidationRule' => 'Y-m-d'),
        array('CustomFieldID' => 'SubscriptionIP', 'ValidationMethod' => 'IP'),
        array('CustomFieldID' => 'OptInDate', 'ValidationMethod' => 'Date', 'ValidationRule' => 'Y-m-d'),
        array('CustomFieldID' => 'Subscriber_IP', 'ValidationMethod' => 'IP'),
        array('CustomFieldID' => 'City', 'ValidationMethod' => 'Email address'),
        array('CustomFieldID' => 'Country', 'ValidationMethod' => 'Email address'),
    );
    public static $RulePluginFields = array();

    /**
     * Activity fields for rules
     *
     * @static array
     * */
    public static $RuleActivityFields = array(
        array('CustomFieldID' => 'Opens'),
        array('CustomFieldID' => 'Clicks'),
        array('CustomFieldID' => 'BrowserViews'),
        array('CustomFieldID' => 'Forwards'));

    /**
     * Activity table names
     *
     * @static array
     * */
    public static $RuleActivityTables = array(
        'Opens' => 'stats_open',
        'Clicks' => 'stats_link',
        'BrowserViews' => 'stats_browserview',
        'Forwards' => 'stats_forward');

    /**
     * Rule operators
     *
     * @static array
     * */
    public static $RuleOperators = array(
        array('Name' => 'Disabled', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain', 'Begins with', 'Ends with', 'Is set', 'Is not set')),
        array('Name' => 'Numbers', 'Operators' => array('Equals to', 'Is greater than', 'Is smaller than')),
        array('Name' => 'Letters', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain', 'Begins with', 'Ends with', 'Is set', 'Is not set')),
        array('Name' => 'Numbers and letters', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain', 'Begins with', 'Ends with', 'Is set', 'Is not set')),
        array('Name' => 'Email address', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain', 'Begins with', 'Ends with', 'Is set', 'Is not set')),
        array('Name' => 'URL', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain', 'Begins with', 'Ends with', 'Is set', 'Is not set')),
        array('Name' => 'Date', 'Operators' => array('Is', 'Is not', 'Is before', 'Is after', 'Is set', 'Is not set')),
        array('Name' => 'Time', 'Operators' => array('Is', 'Is not', 'Is before', 'Is after', 'Is set', 'Is not set')),
        array('Name' => 'Enum', 'Operators' => array('Is', 'Is not', 'Contains', 'Does not contain')),
        array('Name' => 'IP', 'Operators' => array('Equals to', 'Is greater than', 'Is smaller than')));

    /**
     * Rule sql operators
     *
     * @static array
     * */
    public static $ArraySQLOperators = array(
        'Contains' => 'LIKE',
        'Does not contain' => 'NOT LIKE',
        'Begins with' => 'LIKE',
        'Ends with' => 'LIKE',
        'Equals to' => '=',
        'Is greater than' => '>',
        'Is smaller than' => '<',
        'Is before' => '<',
        'Is after' => '>',
        'Is set' => 'IS NOT NULL',
        'Is not set' => 'IS NULL',
        'Is' => '=',
        'Is not' => '!=');

    /**
     * Rule sql operators
     *
     * @static array
     * */
    public static $ArraySQLOperators_Enhanced = array(
        'Contains' => 'LIKE',
        'Does not contain' => 'NOT LIKE',
        'Begins with' => 'LIKE',
        'Ends with' => 'LIKE',
        'Equals to' => '=',
        'Is greater than' => '>',
        'Is smaller than' => '<',
        'Is before' => '<',
        'Is after' => '>',
        'Is set' => array('IS NOT', '!='),
        'Is not set' => array('IS', '='),
        'Is' => '=',
        'Is not' => '!=',
        'At most' => '<=',
        'At least' => '>=',
        'Only' => '=');
    public static $ArraySQLOperators_Regexp = array(
        'Is' => 'REGEXP',
        'Is not' => 'NOT REGEXP'
    );

    /**
     * Rule sql operators
     *
     * @static array
     * */
    public static $ArraySQLValues = array(
        'Contains' => '%%%s%%',
        'Does not contain' => '%%%s%%',
        'Begins with' => '%s%%',
        'Ends with' => '%%%s',
        'Equals to' => '%s',
        'Is greater than' => '%s',
        'Is smaller than' => '%s',
        'Is before' => '%s',
        'Is after' => '%s',
        'Is set' => array('NULL', ''),
        'Is not set' => array('NULL', ''),
        'Is' => '%s',
        'Is not' => '%s');

    /**
     * Returns a single segment matching given criterias
     *
     * <code>
     * <?php
     * $ArraySegmentInformation = Segments::RetrieveSegment(array('*'), array('SegmentID'=>12));
     * ?>
     * </code>
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving segment.
     * @param boolean $Details if set to true, subscriber count of segment also returned
     * @return boolean|array
     * @author Mert Hurturk
     * */
    public static function RetrieveSegment($ArrayReturnFields, $ArrayCriterias, $Details = true) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'segments');
        $ArrayCriterias = $ArrayCriterias;
        $ArrayOrder = array();
        $ArraySegment = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        if (count($ArraySegment) < 1) {
            return false;
        } // if there are no segments, return false

        $ArraySegment = $ArraySegment[0];
        if ($Details) {
            if (time() - strtotime($ArraySegment['SubscriberCountLastCalculatedOn']) >= 86400) {
                $ArraySegment['SubscriberCount'] = Subscribers::GetSegmentTotal($ArraySegment['RelListID'], $ArraySegment['SegmentID']);
                $UpdatedFields = array(
                    'SubscriberCount' => $ArraySegment['SubscriberCount'],
                    'SubscriberCountLastCalculatedOn' => date('Y-m-d H:i:s')
                );
                self::Update($UpdatedFields, array('SegmentID' => $ArraySegment['SegmentID']));
            }
        }
        $ArraySegment['RulesArray'] = self::GetRulesAsArray($ArraySegment['SegmentRules']);
        return $ArraySegment;
    }

    /**
     * Reteurns all segments matching given criterias
     *
     * <code>
     * <?php
     * $ArraySegments = Segments::RetrieveSegments(array('*'), array('RelListID'=>2), array('SegmentName'=>'ASC'));
     * ?>
     * </code>
     *
     * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
     * @param array $ArrayCriterias Criterias to be matched while retrieving segments.
     * @param array $ArrayOrder Fields to order.
     * @param array $RetrieveTotals If set to true, retrieves subscriber counts.
     * @return boolean|array
     * @author Mert Hurturk
     * */
    function RetrieveSegments($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('SegmentName' => 'ASC'), $RetrieveTotals = true) {
        if (count($ArrayReturnFields) < 1) {
            $ArrayReturnFields = array('*');
        }

        $ArrayFields = array(implode(', ', $ArrayReturnFields));
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'segments');
        $ArrayCriterias = $ArrayCriterias;
        $ArraySegments = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

        if (count($ArraySegments) < 1) {
            return false;
        }  // if there are no segments, return false

        foreach ($ArraySegments as &$EachSegment) {
            if ($RetrieveTotals) {
                if (time() - strtotime($EachSegment['SubscriberCountLastCalculatedOn']) >= 86400) {
                    $EachSegment['SubscriberCount'] = Subscribers::GetSegmentTotal($EachSegment['RelListID'], $EachSegment['SegmentID']);
                    $UpdatedFields = array(
                        'SubscriberCount' => $EachSegment['SubscriberCount'],
                        'SubscriberCountLastCalculatedOn' => date('Y-m-d H:i:s')
                    );
                    self::Update($UpdatedFields, array('SegmentID' => $EachSegment['SegmentID']));
                }
            }
            $EachSegment['RulesArray'] = self::GetRulesAsArray($EachSegment['SegmentRules']);
        }

        return $ArraySegments;
    }

    /**
     * Returns total number of segments of a list
     *
     * <code>
     * <?php
     * $TotalSegments = Segments::GetTotal(3);
     * ?>
     * </code>
     *
     * @param integer $OwnerListID Owner list's id
     * @return integer|boolean Total number of segments of given list
     * @author Mert Hurturk
     * */
    function GetTotal($OwnerListID) {
        // Check for required fields of this function - Start
        if (!isset($OwnerListID) || $OwnerListID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArrayFields = array('COUNT(*) AS TotalSegmentCount');
        $ArrayFromTables = array(MYSQL_TABLE_PREFIX . 'segments');
        $ArrayCriterias = array('RelListID' => $OwnerListID);
        $ArraySegments = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias);

        if (count($ArraySegments) < 1) {
            return false;
        }

        return $ArraySegments[0]['TotalSegmentCount'];
    }

    /**
     * Creates a new segment with given values
     *
     * <code>
     * <?php
     * $NewSegmentID = Segmets::Create(array(
     * 	'RelOwnerUserID' => '2',
     * 	'RelListID' => '3',
     * 	'SegmentName' => 'Hotmail users',
     * 	'SegmentRules' => Segments::GetRuleAsString('EmailAddress', 'Ends with', '@hotmail.com')
     * 	));
     * ?>
     * </code>
     *
     * @param array $ArrayFieldAndValues Values of new segment
     * @return boolean|integer ID of new segment
     * @author Mert Hurturk
     * */
    function Create($ArrayFieldAndValues) {
        // Check required values - Start
        foreach (self::$ArrayRequiredFields as $EachField) {
            if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                print $ArrayFieldAndValues[$EachField];
                return false;
            }
        }
        // Check required values - End
        // Check if any value is missing. if yes, give default values - Start
        foreach (self::$ArrayDefaultValuesForFields as $EachField => $EachValue) {
            if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                $ArrayFieldAndValues[$EachField] = $EachValue;
            }
        }
        // Check if any value is missing. if yes, give default values - End

        Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX . 'segments');

        $NewSegmentID = Database::$Interface->GetLastInsertID();
        return $NewSegmentID;
    }

    /**
     * Updates segment information
     *
     * <code>
     * <?php
     * Segmets::Update(array(
     * 	'SegmentName' => 'Hotmail users',
     * 	'SegmentRules' => Segments::GetRuleAsString('EmailAddress', 'Ends with', '@hotmail.com')
     * 	), array(
     * 	'SegmentID' => 2
     * 	));
     * ?>
     * </code>
     *
     * @param array $ArrayFieldAndValues Values of new segment information
     * @param array $ArrayCriterias Criterias to be matched while updating segment
     * @return boolean
     * @author Mert Hurturk
     * */
    function Update($ArrayFieldAndValues, $ArrayCriterias) {
        // Check for required fields of this function - Start
        // $ArrayCriterias check
        if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) {
            return false;
        }
        // Check for required fields of this function - End
        // Check required values - Start
        foreach (self::$ArrayRequiredFields as $EachField) {
            if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') {
                return false;
            }
        }
        // Check required values - End

        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'segments');
        return;
    }

    /**
     * Deletes segments
     *
     * <code>
     * <?php
     * Segments::Delete(12, array(1,3,5,4));
     * ?>
     * </code>
     *
     * @param integer $OwnerUserID Owner user id of segments to be deleted
     * @param array $ArraySegmentIDs Segment ids to be deleted
     * @return boolean
     * @author Mert Hurturk
     * */
    function Delete($OwnerUserID, $ArraySegmentIDs, $ListID = 0) {
        // Check for required fields of this function - Start
        // $OwnerUserID check
        if (!isset($OwnerUserID) || $OwnerUserID == '') {
            return false;
        }
        // Check for required fields of this function - End

        Core::LoadObject('campaigns');
        Core::LoadObject('queue');

        if (count($ArraySegmentIDs) > 0) {
            foreach ($ArraySegmentIDs as $EachID) {
                Campaigns::DeleteCampaignRecipients($OwnerUserID, array(), 0, $EachID);
                EmailQueue::Delete(0, 0, $EachID);

                Database::$Interface->DeleteRows(array('RelOwnerUserID' => $OwnerUserID, 'SegmentID' => $EachID), MYSQL_TABLE_PREFIX . 'segments');
            }
        } elseif ($ListID > 0) {
            $ArraySegments = self::RetrieveSegments(array('*'), array('RelListID' => $ListID), array('SegmentName' => 'ASC'));

            if (count($ArraySegments) > 0) {
                foreach ($ArraySegments as $Index => $ArrayEachSegment) {
                    Campaigns::DeleteCampaignRecipients($OwnerUserID, array(), 0, $ArrayEachSegment['SegmentID']);
                    EmailQueue::Delete(0, 0, $ArrayEachSegment['SegmentID']);
                }
            }

            Database::$Interface->DeleteRows(array('RelOwnerUserID' => $OwnerUserID, 'RelListID' => $ListID), MYSQL_TABLE_PREFIX . 'segments');
        }

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.Segments', array($OwnerUserID, $ArraySegmentIDs, $ListID));
        // Plug-in hook - End

        return true;
    }

    /**
     * Returns segment rule as a formatted string
     *
     * <code>
     * <?php
     * $ArraySegmentRuleString = Segments::GetRukeAsString('EmailAddress','Ends with','@hotmail.com');
     * ?>
     * </code>
     *
     * @param string $RuleField Field id
     * @param string $RuleOperator Rule operator
     * @param boolean $RuleFilter Filter to be matched
     * @return string
     * @author Mert Hurturk
     */
    function GetRuleAsString($RuleField, $RuleOperator, $RuleFilter) {
        return '[[' . $RuleField . ']||[' . $RuleOperator . ']||[' . $RuleFilter . ']]';
    }

    /**
     * Returns segment rules as an array
     *
     * <code>
     * <?php
     * $ArraySegmentRules = Segments::GetRulesAsArray('[[EmailAddress]||[Ends with]||[@hotmail.com]],,,[[SubscriberID]||[Is greater than]||[25]]');
     * ?>
     * </code>
     *
     * @param string $SegmentRules Value of SegmentRules field of a segment
     * @return array
     * @author Mert Hurturk
     */
    function GetRulesAsArray($SegmentRules) {
        $returnArraySegmentRule = array();

        $arrayRules = explode(',::,', $SegmentRules);

        $arrayRuleGroups = (isset($arrayRules[1]) ? $arrayRules[1] : array());
        $arrayRules = $arrayRules[0];

        if (substr($arrayRules, 0, 3) == '((!' && substr($arrayRules, strlen($arrayRules) - 3, strlen($arrayRules)) == '!))') {
            $arrayRuleGroups = $arrayRules;
            $arrayRules = '';
        }

        if ($arrayRules != '') {
            foreach (explode(',,,', $arrayRules) as $eachRuleString) {
                $returnArraySegmentRule[] = self::ConvertRuleStringToArray($eachRuleString);
            }
        }

        if (count($arrayRuleGroups) > 0) {
            foreach (explode(',#,', $arrayRuleGroups) as $eachRuleGroupString) {
                $tmpGroupArray = array();
                foreach (explode(',,,', $eachRuleGroupString) as $eachRuleString) {
                    $tmpGroupArray[] = self::ConvertRuleStringToArray($eachRuleString);
                }
                $returnArraySegmentRule[] = $tmpGroupArray;
            }
        }

        return $returnArraySegmentRule;
    }

    function ConvertRuleStringToArray($string) {
        $ruleArray = array();
        $string = str_replace('((!', '', $string);
        $string = str_replace('[[', '', $string);
        $string = str_replace(']]', '', $string);
        $string = str_replace('!))', '', $string);

        $arrayEachRule = explode(']||[', $string);

        $isActivityRule = false;
        foreach (self::$RuleActivityFields as $each) {
            if ($each['CustomFieldID'] == $arrayEachRule[0]) {
                $isActivityRule = true;
                break;
            }
        }

        if (!$isActivityRule) {
            $ruleArray = array('field' => $arrayEachRule[0], 'operator' => $arrayEachRule[1], 'filter' => $arrayEachRule[2]);
        } else {
            $tmpArray = array('field' => $arrayEachRule[0], 'campaignFilter' => $arrayEachRule[1], 'campaignID' => $arrayEachRule[2], 'timesFilter' => $arrayEachRule[3], 'timesValue' => $arrayEachRule[4]);
            if (isset($arrayEachRule[5])) {
                $tmpArray['specificLink'] = $arrayEachRule[5];
            }
            $ruleArray = $tmpArray;
        }

        return $ruleArray;
    }

    /**
     * Returns sql query of a segment
     *
     * <code>
     * <?php
     * $SegmentSQLQuery = Segments::GenerateSegmentSQLQuery(2);
     * ?>
     * </code>
     *
     * @param array $Segment Segment ID
     * @return string
     * @author Mert Hurturk
     * */
    function GetSegmentSQLQuery($SegmentID) {
        // Check for required fields of this function - Start
        if (!isset($SegmentID) || $SegmentID == '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArraySegment = array();
        $ArraySQLQueryParts = array();

        if ($SegmentID > 0) {
            $ArraySegmentInformation = self::RetrieveSegment(array('*'), array('SegmentID' => $SegmentID), false);
            $ArraySegmentRules = self::GetRulesAsArray($ArraySegmentInformation['SegmentRules']);

            $ArraySegment['SegmentOperator'] = $ArraySegmentInformation['SegmentOperator'];

            foreach ($ArraySegmentRules as $EachRule) {
                $MatchCount = 0;
                foreach (self::$RuleDefaultFields as $EachDefault) {
                    if ($EachDefault['CustomFieldID'] == $EachRule['field']) {
                        $MatchCount++;
                        break;
                    }
                }

                if ($MatchCount == 0) {
                    $EachRule['field'] = 'CustomField' . $EachRule['field'];
                }

                $ArraySQLQueryPart = array();
                if ($EachRule['operator'] == 'Contains') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = "%" . $EachRule['filter'] . "%";
                } else if ($EachRule['operator'] == 'Does not contain') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = "%" . $EachRule['filter'] . "%";
                } else if ($EachRule['operator'] == 'Begins with') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = $EachRule['filter'] . "%";
                } else if ($EachRule['operator'] == 'Ends with') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = "%" . $EachRule['filter'];
                } else if ($EachRule['operator'] == 'Equals to') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = $EachRule['filter'];
                } else if ($EachRule['operator'] == 'Is set') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                } else if ($EachRule['operator'] == 'Is not set') {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                } else {
                    $ArraySQLQueryPart['field'] = $EachRule['field'];
                    $ArraySQLQueryPart['operator'] = self::$ArraySQLOperators[$EachRule['operator']];
                    $ArraySQLQueryPart['value'] = $EachRule['filter'];
                }
                $ArraySQLQueryParts[] = $ArraySQLQueryPart;
            }
        }

        $ActiveSubscriberQueryPart = array();
        $ActiveSubscriberQueryPart['field'] = 'SubscriptionStatus';
        $ActiveSubscriberQueryPart['operator'] = '=';
        $ActiveSubscriberQueryPart['value'] = 'Subscribed';

        if ($SegmentID > 0) {
            $ActiveSubscriberQueryPart['exclude_or'] = true;
        }
        $ArraySQLQueryParts[] = $ActiveSubscriberQueryPart;

        $ArraySegment['SegmentRules'] = $ArraySQLQueryParts;

        return $ArraySegment;
    }

    /**
     * This method is an enhanced version of GetSegmentSQLQuery method.
     *
     * What changed from original function?
     * =============
     *
     * - Generated criteria arrays are compatible with Database::$Interface->GetCriteriaString()
     * - Older version did not support 'Is set' and 'Is not set' operators
     *
     * Parameters:
     * ==========
     *
     * SegmentID		integer		Segment ID
     * ReturnAsString	boolean		If set to true, sql query returned as string. Otherwise a criteria array is returned.
     * RulesString		string		If set to any string and segment id is set to 0, sql query will be populated from this string
     *
     * @return mixed
     * @author Mert Hurturk
     * */
    function GetSegmentSQLQuery_Enhanced($SegmentID, $ReturnAsString = false, $RulesString = '', $RuleOperator = 'and', $ListID = 0, $ListTableAlias = '') {
        // Check for required fields of this function - Start
        if (!isset($SegmentID) || $SegmentID === '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArraySegment = array();
        $ArraySQLQueryParts = array();

        if ($SegmentID > 0) {
            $ArraySegmentInformation = self::RetrieveSegment(array('*'), array('SegmentID' => $SegmentID), false);
            $ArraySegmentRules = self::GetRulesAsArray($ArraySegmentInformation['SegmentRules']);
            $ArraySQLQueryParts = self::GetQueryArray($ArraySegmentRules, $ArraySegmentInformation['SegmentOperator'], $ArraySegmentInformation['RelListID'], $ListTableAlias);
        } else if ($SegmentID === 0 && $RulesString != '') {
            $ArraySQLQueryParts = self::GetQueryArray(self::GetRulesAsArray($RulesString), $RuleOperator, $ListID, $ListTableAlias);
        }


        // If SubscriptionStatus or BounceType fields are provided with the query, Active subscription (Subscribers::$ArrayActiveSubscriberCriterias_Enhanced)
        // criterias are not added automatically to the end of the query. Otherwise, they are added. If BounceType field is provided, it should have
        // "Hard" value to trigger the ignoring of Active subscription criterias.
        $isSubscriberStatusPresent = false;
        foreach ($ArraySQLQueryParts as $eachPart) {
            if ($eachPart['Column'] == 'SubscriptionStatus' || ($eachPart['Column'] == 'BounceType' && $eachPart['Value'] == 'Hard')) {
                $isSubscriberStatusPresent = true;
            }
        }

        if (!$isSubscriberStatusPresent) {
            Core::LoadObject('subscribers');
            if (count($ArraySQLQueryParts) > 0) {
                $ArraySQLQueryParts = array($ArraySQLQueryParts, array('Link' => 'AND', Subscribers::$ArrayActiveSubscriberCriterias_Enhanced));
            } else {
                $ArraySQLQueryParts[] = array('Link' => 'AND', Subscribers::$ArrayActiveSubscriberCriterias_Enhanced);
            }
        }

        if ($ReturnAsString) {
            return Database::$Interface->GetCriteriaString($ArraySQLQueryParts);
        }

        return $ArraySQLQueryParts;
    }

    function GetQueryArray($RulesArray, $Operator = 'and', $ListID, $ListAlias = '') {
        $ArraySQLQueryParts = array();
        $counter = 0;
        foreach ($RulesArray as $EachRule) {
            if (is_array($EachRule[0])) {
                $subCounter = 0;
                $tmpSubRuleArray = array();
                foreach ($EachRule as $EachSubRule) {
                    $result = self::GetSegmentSQLQueryOfASingleRule($EachSubRule, $subCounter > 0 ? ($Operator == 'and' ? 'OR' : 'AND') : '', $ListID, $ListAlias);
                    if ($result == false)
                        continue;
                    $tmpSubRuleArray[] = $result;
                    $subCounter++;
                }
                if ($subCounter > 0) {
                    $tmpSubRuleArray['Link'] = $Operator == 'and' ? 'AND' : 'OR';
                }
                if (count($tmpSubRuleArray) > 0) {
                    $ArraySQLQueryParts[] = $tmpSubRuleArray;
                }
            } else {
                $MatchCount = 0;
                $JoinTableCount = 0;
                foreach (self::$RuleActivityFields as $EachActivityField) {
                    if ($EachActivityField['CustomFieldID'] == $EachRule['field']) {
                        $MatchCount++;
                        break;
                    }
                }

                if ($MatchCount > 0) {
                    $JoinTableCount++;
                    $ArraySQLQueryParts[] = self::GetSegmentSQLQueryOfASingleActivityRule($EachRule, $counter > 0 ? ($Operator == 'and' ? 'AND' : 'OR') : '', 'JoinTable' . $JoinTableCount);
                } else {
                    $result = self::GetSegmentSQLQueryOfASingleRule($EachRule, $counter > 0 ? ($Operator == 'and' ? 'AND' : 'OR') : '', $ListID, $ListAlias);
                    if ($result == false)
                        continue;
                    $ArraySQLQueryParts[] = $result;
                }
                $counter++;
            }
        }

        return $ArraySQLQueryParts;
    }

    function GetSegmentJoinQuery($SegmentID, $ReturnAsString = false, $SubscriberTableAlias = '', $RulesString = '', $ListID = 0) {
        // Check for required fields of this function - Start
        if (!isset($SegmentID) || $SegmentID === '') {
            return false;
        }
        // Check for required fields of this function - End

        $ArraySegment = array();
        $ArrayJoinQueryParts = array();

        if ($SegmentID > 0) {
            $ArraySegmentInformation = self::RetrieveSegment(array('*'), array('SegmentID' => $SegmentID), false);
            $ArraySegmentRules = self::GetRulesAsArray($ArraySegmentInformation['SegmentRules']);
            $ArrayJoinQueryParts = self::GetJoinArray($ArraySegmentRules, $SubscriberTableAlias, $ArraySegmentInformation['RelListID']);
        } else if ($SegmentID == 0 && $RulesString != '') {
            $ArrayJoinQueryParts = self::GetJoinArray(self::GetRulesAsArray($RulesString), $SubscriberTableAlias, $ListID);
        }

        if ($ReturnAsString) {
            return Database::$Interface->GetJoinString($ArrayJoinQueryParts);
        }

        return $ArrayJoinQueryParts;
    }

    function GetJoinArray($ArraySegmentRules, $SubscriberTableAlias, $ListID) {
        $ArrayJoinQueryParts = array();
        $counter = 0;
        $JoinTableCount = 0;
        foreach ($ArraySegmentRules as $EachRule) {
            if (!is_array($EachRule[0])) {
                $MatchCount = 0;
                foreach (self::$RuleActivityFields as $EachActivityField) {
                    if ($EachActivityField['CustomFieldID'] == $EachRule['field']) {
                        $MatchCount++;
                        break;
                    }
                }

                if ($MatchCount < 1) {
                    continue;
                }

                $JoinTableCount++;
                $TableName = 'JoinTable' . $JoinTableCount;

                if (($EachRule['timesFilter'] == 'At most' && $EachRule['timesValue'] == 0) || ($EachRule['timesFilter'] == 'Only' && $EachRule['timesValue'] == 0)) {
                    $JoinWhereQuery = '';
                    if ($EachRule['campaignFilter'] == 'Specific') {
                        $JoinWhereQuery = ' AND RelCampaignID = ' . $EachRule['campaignID'];
                    }

                    $ArrayJoinQueryPart = array(
                        'Type' => 'LEFT OUTER JOIN',
                        'Table' => '(SELECT RelSubscriberID FROM ' . MYSQL_TABLE_PREFIX . self::$RuleActivityTables[$EachRule['field']] . ' WHERE RelListID = ' . $ListID . $JoinWhereQuery . ' GROUP BY RelSubscriberID) AS ' . $TableName,
                        'ON' => ($SubscriberTableAlias == '' ? MYSQL_TABLE_PREFIX . 'subscribers_' . $ListID : $SubscriberTableAlias) . '.SubscriberID = ' . $TableName . '.RelSubscriberID'
                    );
                } else {
                    $JoinWhereQuery = '';
                    if ($EachRule['field'] == 'Clicks') {
                        if (isset($EachRule['specificLink']) && $EachRule['specificLink'] != '') {
                            $JoinWhereQuery .= ($JoinWhereQuery == '' ? ' WHERE ' : ' AND ') . 'LinkURL = "' . mysql_escape_string($EachRule['specificLink']) . '"';
                        }
                    }
                    if ($EachRule['campaignFilter'] != 'Any') {
                        if ($EachRule['campaignFilter'] == 'Specific') {
                            $JoinWhereQuery .= ($JoinWhereQuery == '' ? ' WHERE ' : ' AND ') . 'RelCampaignID = ' . $EachRule['campaignID'];
                        }

                        $JoinWhereQuery .= ($JoinWhereQuery == '' ? ' WHERE ' : ' AND ') . 'RelListID = ' . $ListID;

                        $ArrayJoinQueryPart = array(
                            'Type' => 'INNER JOIN',
                            'Table' => '(SELECT RelSubscriberID, COUNT(*) AS Total FROM ' . MYSQL_TABLE_PREFIX . self::$RuleActivityTables[$EachRule['field']] . $JoinWhereQuery . ' GROUP BY RelSubscriberID) AS ' . $TableName,
                            'ON' => ($SubscriberTableAlias == '' ? MYSQL_TABLE_PREFIX . 'subscribers_' . $ListID : $SubscriberTableAlias) . '.SubscriberID = ' . $TableName . '.RelSubscriberID'
                        );
                    } else {
                        $ArrayJoinQueryPart = array(
                            'Type' => 'INNER JOIN',
                            'Table' => '(SELECT RelSubscriberID, RelListID, MAX(SubQuery.Total) AS Total FROM (SELECT RelSubscriberID, RelListID, COUNT(*) AS Total FROM ' . MYSQL_TABLE_PREFIX . self::$RuleActivityTables[$EachRule['field']] . $JoinWhereQuery . ' GROUP BY RelSubscriberID, RelListID, RelCampaignID) AS SubQuery GROUP BY RelSubscriberID, RelListID) AS ' . $TableName,
                            'ON' => ($SubscriberTableAlias == '' ? MYSQL_TABLE_PREFIX . 'subscribers_' . $ListID : $SubscriberTableAlias) . '.SubscriberID = ' . $TableName . '.RelSubscriberID AND ' . $TableName . '.RelListID = ' . $ListID
                        );
                    }
                }
                $ArrayJoinQueryParts[] = $ArrayJoinQueryPart;
            }
        }
        return $ArrayJoinQueryParts;
    }

    function GetSegmentSQLQueryOfASingleRule($RuleArray, $Link = '', $ListID, $ListAlias = '') {
        $ArraySQLQueryPart = array();
        $IsCustomField = FALSE;
        if (strpos($RuleArray['field'], 'CustomField') !== FALSE) {
            Core::LoadObject('custom_fields');
            $fieldIdPattern = '/CustomField([\d]+)/';
            preg_match($fieldIdPattern, $RuleArray['field'], $arrayMatches);
            if (count($arrayMatches) <> 2)
                return false;
            $CustomFieldWhole = $arrayMatches[0];
            $CustomFieldID = $arrayMatches[1];
            $ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID));
            if ($ArrayCustomField != false) {
                $IsCustomField = TRUE;
                if ($ArrayCustomField['IsGlobal'] == 'Yes') {
                    $FieldName = 'ValueText';
                    if ($ArrayCustomField['FieldType'] == 'Date field' || $ArrayCustomField['ValidationMethod'] == 'Date') {
                        $FieldName = 'ValueDate';
                    } else if ($ArrayCustomField['FieldType'] == 'Time field' || $ArrayCustomField['ValidationMethod'] == 'Time') {
                        $FieldName = 'ValueTime';
                    } else if ($ArrayCustomField['ValidationMethod'] == 'Numbers') {
                        $FieldName = 'ValueDouble';
                    }

                    $fieldSelect = '(SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $ArrayCustomField['CustomFieldID'] . ' AND EmailAddress = ' . ($ListAlias == '' ? MYSQL_TABLE_PREFIX . 'subscribers_' . $ListID : $ListAlias) . '.EmailAddress LIMIT 1)';

                    if (strpos($RuleArray['field'], 'DATE_FORMAT') !== FALSE) {
                        $fieldSelect = preg_replace('/' . $CustomFieldWhole . '/', $fieldSelect, $RuleArray['field']);
                    }

                    $RuleArray['field'] = $fieldSelect;
                }
            } else {
                return false;
            }
        }

        if ($RuleArray['operator'] == 'Is set' || $RuleArray['operator'] == 'Is not set') {
            $ArraySQLQueryPart = array(
                array(
                    'Column' => $RuleArray['field'],
                    'Operator' => self::$ArraySQLOperators_Enhanced[$RuleArray['operator']][0],
                    'ValueWOQuote' => sprintf(self::$ArraySQLValues[$RuleArray['operator']][0], $RuleArray['filter'])
                ),
                array(
                    'Link' => 'OR',
                    'Column' => $RuleArray['field'],
                    'Operator' => self::$ArraySQLOperators_Enhanced[$RuleArray['operator']][1],
                    'Value' => sprintf(self::$ArraySQLValues[$RuleArray['operator']][1], $RuleArray['filter'])
                )
            );
            if ($Link != '') {
                $ArraySQLQueryPart['Link'] = $Link;
            }
        } else {
            $ArraySQLQueryPart = array(
                'Column' => $RuleArray['field'],
                'Operator' => self::$ArraySQLOperators_Enhanced[$RuleArray['operator']],
                'Value' => sprintf(self::$ArraySQLValues[$RuleArray['operator']], $RuleArray['filter'])
            );

            if ($IsCustomField && $ArrayCustomField['FieldType'] == 'Checkboxes') {
                $ArraySQLQueryPart['Operator'] = self::$ArraySQLOperators_Regexp[$RuleArray['operator']];
                $Expression = "(^(%s)([||||]|(.*)^))|(^(%s)$)|([||||]|(.*)^)(%s)((.*)^|[||||])|(([||||]|(.*)^)(%s)$)";
                $ArraySQLQueryPart['Value'] = str_replace('%s', preg_quote($ArraySQLQueryPart['Value']), $Expression);
            }

            if ($RuleArray['operator'] == 'Is not') {
                $ArraySQLQueryPart = array(
                    $ArraySQLQueryPart,
                    array(
                        'Link' => 'OR',
                        'Column' => $ArraySQLQueryPart['Column'],
                        'Operator' => 'IS',
                        'ValueWOQuote' => 'NULL'
                    )
                );
            }

            if ($Link != '') {
                $ArraySQLQueryPart['Link'] = $Link;
            }
        }

        return $ArraySQLQueryPart;
    }

    function GetSegmentSQLQueryOfASingleActivityRule($RuleArray, $Link, $TableName) {
        $ArraySQLQueryPart = array();

        if (($RuleArray['timesFilter'] == 'At most' && $RuleArray['timesValue'] == 0) || ($RuleArray['timesFilter'] == 'Only' && $RuleArray['timesValue'] == 0)) {
            $ArraySQLQueryPart = array(
                'Column' => $TableName . '.RelSubscriberID',
                'Operator' => 'IS',
                'ValueWOQuote' => 'NULL'
            );
            if ($Link != '') {
                $ArraySQLQueryPart['Link'] = $Link;
            }
        } else {
            $ArraySQLQueryPart = array(
                'Column' => $TableName . '.Total',
                'Operator' => self::$ArraySQLOperators_Enhanced[$RuleArray['timesFilter']],
                'ValueWOQuote' => $RuleArray['timesValue']
            );
            if ($Link != '') {
                $ArraySQLQueryPart['Link'] = $Link;
            }
        }

        return $ArraySQLQueryPart;
    }

}

// END class Segments
