<?php
/**
 * Form handler class
 *
 * @package Oempro
 * @author Cem Hurturk
 **/
class FormHandler extends Core
{
/**
 * Checks for required fields
 *
 * @param array $ArrayFormValues Form values
 * @param array $ArrayFormFields Form fields
 * @return array
 * @author Cem Hurturk
 **/
function RequiredFieldValidator($ArrayFormValues, $ArrayFormFields)
	{
	$ArrayErrorFields = array();

	foreach ($ArrayFormFields as $Field=>$ErrorCode)
		{
		if (!isset($ArrayFormValues[strtolower($Field)]) || $ArrayFormValues[strtolower($Field)] === '')
			{
			$ArrayErrorFields[] = $ErrorCode;
			}
		}

	return $ArrayErrorFields;
	}

/**
 * Email address valdiator
 *
 * @param string $EmailAddress Email address to be validated
 * @return bool
 * @author Cem Hurturk
 **/
function EmailValidator($EmailAddress)
	{
    if (!(preg_match("/^[\+\._a-z0-9-]+([\&|-|\.\+][\+\._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/i", $EmailAddress)))
        {
        return false;
        }
    else
        {
        return true;
        }
	}
} // END class FormHandler
?>