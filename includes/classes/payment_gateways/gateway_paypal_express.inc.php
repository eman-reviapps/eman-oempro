<?php

/**
 * PayPal Express Payment Gateway
 *
 * @package default
 * @author Cem Hurturk
 * */
class PaymentGateway_PayPal_Express {

    /**
     * Gateway parameters
     *
     * @var array
     */
    public static $ArrayGatewayParameters = array();

    /**
     * Payment Gateway URL
     *
     * @var string
     * */
//    private static $GatewayURL = "https://www.paypal.com/cgi-bin/webscr";
    private static $GatewayURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";

    /**
     * Constructor
     *
     * @return void
     * @author Cem Hurturk
     * */
    public function __construct() {
        
    }

    /**
     * Sets the payment gateway parameters
     *
     * @return void
     * @author Cem Hurturk
     * */
    public function SetGatewayParameters($ArrayParameters, $ArrayUser = array(), $LogID = 0, $IsCreditPurchase = false, $ReputationLevel = 'None') {
        Core::LoadObject('payments');

        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return false;
        }

        self::$ArrayGatewayParameters = array(
            'cmd' => '_xclick',
            'no_shipping' => 1,
            'rm' => 2,
            'no_note' => 1,
            'tax' => 0,
            'custom' => $ArrayUser['UserID'] . '-' . $LogID . '-' . $ReputationLevel . ($IsCreditPurchase == true ? '-crediting' : ''),
            'notify_url' => APP_URL . 'payment_result.php?Gateway=PayPal_Express&Type=Ping',
            'cancel_return' => Core::InterfaceAppURL() . '/user/',
            'return' => Core::InterfaceAppURL() . '/user/',
        );

        self::$ArrayGatewayParameters = array_merge(self::$ArrayGatewayParameters, $ArrayParameters);
    }

    /**
     * Redirects to the payment gateway
     *
     * @return void
     * @author Cem Hurturk
     * */
    public function Redirect() {
        Core::LoadObject('payments');

        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return false;
        }

        global $ArrayLanguageStrings;

        $HTMLCode = '
				<html>
				<head>
					<meta http-equiv="Content-type" content="text/html; charset=utf-8">
					<title>' . $ArrayLanguageStrings['Screen']['9131'] . '</title>
				</head>
				<body>
					<form id="form1" action="' . self::$GatewayURL . '" method="post">
						';

        foreach (self::$ArrayGatewayParameters as $Key => $Value) {
            $HTMLCode .= '<input type="hidden" name="' . $Key . '" value="' . $Value . '" />' . "\n";
        }

        $HTMLCode .= '
					</form>
					<script type="text/javascript" charset="utf-8">
						document.getElementById(\'form1\').submit();
					</script>
				</body>
				</html>
				';

        print $HTMLCode;
        exit;
    }

    /**
     * Retrieves the received data from payment gateway and processes it
     *
     * @param string $ArrayParameters 
     * @return void
     * @author Cem Hurturk
     */
    public function ProcessReturnedData($ArrayParameters) {
        // Get the custom data received. Set the user ID and payment log ID - Start
        $CustomData = $ArrayParameters['custom'];
        $CustomData = explode('-', $CustomData);
        $UserID = $CustomData[0];
        $LogID = $CustomData[1];
        $ReputationLevel = $CustomData[2];
        $IsCreditPurchase = (isset($CustomData[3]) == true && $CustomData[3] == 'crediting' ? true : false);

        if (($UserID == '') || ($UserID == 0) || ($LogID == '') || ($LogID == 0)) {
            return false;
        }
        // Get the custom data received. Set the user ID and payment log ID - End

        if ($IsCreditPurchase == true) {
            // Retrieve user information - Start {
            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
            if ($ArrayUser == false) {
                return false;
            }

            Payments::SetUser($ArrayUser);
            // Retrieve user information - End }
            // Check if payment system is enabled - Start {
            if (Payments::IsPaymentSystemEnabled(true) == false) {
                return false;
            }
            // Check if payment system is enabled - End }
            // Check if this payment has already been processed - Start {
            $PurchaseLog = Payments::RetrieveCreditsPurchaseLog($LogID);
            if ($PurchaseLog['PaymentStatus'] != 'Unpaid') {
                return false;
            }
            // Check if this payment has already been processed - End }
            // Process the received data - Start {
            $TransactionID = $ArrayParameters['txn_id'];
            $PaymentAmount = $ArrayParameters['payment_gross'];
            $PaymentCurrency = $ArrayParameters['mc_currency'];
            $PaymentType = $ArrayParameters['payment_type'];
            $PaymentStatus = $ArrayParameters['payment_status'];

            if (strtolower($PaymentStatus) == 'completed') {
                $ArrayFieldAndValues = array(
                    'PaymentStatus' => 'Paid',
                    'PaymentStatusDate' => date('Y-m-d H:i:s'),
                    'PaidGateway' => 'PayPal_Express',
                    'GatewayTransactionID' => $TransactionID,
                );

                Payments::UpdateCreditPurchaseLog($ArrayFieldAndValues, array('LogID' => $PurchaseLog['LogID']));

                // Add credits to the user account - Start {
                Users::UpdateUser(array('AvailableCredits' => array(true, '`AvailableCredits` + ' . $PurchaseLog['PurchasedCredits'])), array('UserID' => $ArrayUser['UserID']));
                $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayUser['UserID']));
                // Add credits to the user account - End }
            }
            // Process the received data - End }
        } else {
            // Retrieve user information - Start {
            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $UserID), true);
            if ($ArrayUser == false) {
                return false;
            }

            Payments::SetUser($ArrayUser);

            $ArrayPaymentPeriod = Payments::GetPaymentPeriod($LogID);
            if ($ArrayPaymentPeriod == false) {
                return false;
            }
            // Retrieve user information - End }
            // Check if payment system is enabled - Start {
            if (Payments::IsPaymentSystemEnabled(true) == false) {
                return false;
            }
            // Check if payment system is enabled - End }
            // Check if this payment has already been processed - Start {
            if ($ArrayPaymentPeriod['PaymentStatus'] == 'Paid') {
                return false;
            }
            // Check if this payment has already been processed - End }
            // Process the received data - Start {
            $TransactionID = $ArrayParameters['txn_id'];
            $PaymentAmount = $ArrayParameters['payment_gross'];
            $PaymentCurrency = $ArrayParameters['mc_currency'];
            $PaymentType = $ArrayParameters['payment_type'];
            $PaymentStatus = $ArrayParameters['payment_status'];

            if (strtolower($PaymentStatus) == 'completed') {
                $ArrayFieldAndValues = array(
                    'PaymentStatus' => 'Paid',
                    'PaymentStatusDate' => date('Y-m-d H:i:s'),
                    'PaidGateway' => 'PayPal_Express',
                    'GatewayTransactionID' => $TransactionID,
                );

                Payments::Update($ArrayFieldAndValues, array('LogID' => $ArrayPaymentPeriod['LogID']));
            }
            // Process the received data - End }
        }

        if ($ReputationLevel == 'Trusted') {
            //update user to be trusted here
            Users::UpdateUser(array('ReputationLevel' => $ReputationLevel), array('UserID' => $ArrayUser['UserID']));
        }
        return;
    }

}

// END class PaymentGateway_PAYPAL_EXPRESS
?>