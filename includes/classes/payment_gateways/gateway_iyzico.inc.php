<?php

/**
 * Iyzico Payment Gateway
 *
 * @package default
 * @author Eman Mohammed
 * */
class PaymentGateway_Iyzico {

    /**
     * Gateway parameters
     *
     * @var array
     */
    public static $ArrayGatewayParameters = array();
//    private static $GatewayURL = "https://sandbox-api.iyzipay.com/";
//    private static $APIKEY = "sandbox-1Pm9JLfYZ2xnbuf5EYi5HrhdQWNKGG4j";
//    private static $SECRETKEY = "sandbox-z8rvToXdb7GlBASMQGTBFbNNovoSOa2E";
    private static $GatewayURL = "https://api.iyzipay.com/";
    private static $APIKEY = "70XvJiiCs6QcWVZvR3447t2JA3TaLUbi";
    private static $SECRETKEY = "kvoqKRXTTofz8KK3hRlvFTH9GxN5Mi0F";
    private $Options;
    private $gateway = "Iyzipay";
    private $callBackURL = 'payment/payment_result';

    /**
     * Constructor
     *
     * @return void
     * @author Cem Hurturk
     * */
    public function __construct() {

        Core::LoadObject('payment_gateways/iyzipay/IyzipayBootstrap.inc.php');
        IyzipayBootstrap::init();

        $options = new \Iyzipay\Options();

        $options->setApiKey(self::$APIKEY);
        $options->setSecretKey(self::$SECRETKEY);
        $options->setBaseUrl(self::$GatewayURL);

        $this->Options = $options;
    }

    public function getOptions() {
        return $this->Options;
    }

    public function getCallBackURL() {
//        return $this->callBackURL . "?Gateway=" . $this->gateway;
        return $this->callBackURL . "/" . $this->gateway;
    }

    public function getGateway() {
        return $this->gateway;
    }

    public function test($ArrayParameters, $ArrayUser = array(), $items = array()) {
//        # make request
//        $iyzipayResource = \Iyzipay\Model\ApiTest::retrieve($this->Options);
//
//        # print result
//        print_r('<pre>');
//        print_r($iyzipayResource);
//        print_r('</pre>');
# create request class
        $request = $this->contructRequest($ArrayParameters);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId("12");
        $buyer->setName("Eman");
        $buyer->setSurname("Mohammed");
        $buyer->setGsmNumber("+201099967696");
        $buyer->setEmail("eman_1786@yahoo.com");
        $buyer->setIdentityNumber("engeman08");
        $buyer->setLastLoginDate("2016-11-01 12:43:35");
        $buyer->setRegistrationDate("2016-06-01 15:12:09");
        $buyer->setRegistrationAddress("Badrashin Giza,Egypt");
        $buyer->setIp($_SERVER['REMOTE_ADDR']);
        $buyer->setCity("Giza");
        $buyer->setCountry("Egypt");
        $buyer->setZipCode("22222");
        $request->setBuyer($buyer);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName("Eman Mohammed");
        $billingAddress->setCity("Giza");
        $billingAddress->setCountry("Egypt");
        $billingAddress->setAddress("Badrashin Giza,Egypt");
        $billingAddress->setZipCode("22222");
        $request->setBillingAddress($billingAddress);

        $basketItems = array();
        $firstBasketItem = new \Iyzipay\Model\BasketItem();
        $firstBasketItem->setId("3");
        $firstBasketItem->setName("501-2500 Subscribers");
        $firstBasketItem->setCategory1("Monthly");
        $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
        $firstBasketItem->setPrice(43.34);
        $basketItems[0] = $firstBasketItem;

        $request->setBasketItems($basketItems);
        $checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, $this->Options);

        return $checkoutFormInitialize;
    }

    public function initializeCheckoutForm($ArrayParameters, $ArrayUser = array(), $items = array()) {
        Core::LoadObject('payments');

        if (Payments::IsPaymentSystemEnabled(true) == false) {
            return false;
        }
        # create request class
        $request = $this->contructRequest($ArrayParameters);
        # create buyer object
        $buyer = $this->constructBuyerObject($ArrayUser);
        $request->setBuyer($buyer);

        # create billing address object
        $billingAddress = $this->constructBillingAddressObject($ArrayUser);
        $request->setBillingAddress($billingAddress);

        $basketItems = $this->contructBasketItems($items);
        $request->setBasketItems($basketItems);

        # make request
        //$checkoutFormInitialize
        return \Iyzipay\Model\CheckoutFormInitialize::create($request, $this->Options);
    }

    private function contructRequest($ArrayParameters) {

        $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();

        $request->setLocale(\Iyzipay\Model\Locale::EN);
        $request->setConversationId($this->constructConversationId($ArrayParameters));

        $request->setBasketId($this->constructBasketId($ArrayParameters));

        $request->setPrice($ArrayParameters['Price']);
        $request->setPaidPrice($ArrayParameters['PaidPrice']);

        if ($ArrayParameters['Currency'] == 'TRY') {
            $request->setCurrency(\Iyzipay\Model\Currency::TL);
        } else {
            $request->setCurrency(\Iyzipay\Model\Currency::USD);
        }

        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::SUBSCRIPTION);

        $request->setCallbackUrl($ArrayParameters["CallbackURL"]);
        $request->setEnabledInstallments(array(1));

        return $request;
    }

    private function constructBasketId($ArrayParameters) {
        return $ArrayParameters['LogID'] . '-' . $ArrayParameters['Process'] . '-' . $ArrayParameters['UserID'] . '-' . $ArrayParameters['OldPackage'] . '-' . $ArrayParameters['NewPackage'] . '-' . $ArrayParameters['ReputationLevel'] . ($ArrayParameters['IsCreditPurchase'] == true ? '-crediting' : '') . '-' . $ArrayParameters['Credits'];
    }

    private function constructConversationId($ArrayParameters) {
        return $ArrayParameters['UserID'] . "-" . mt_rand() . "-" . time();
    }

    private function constructBuyerObject($ArrayUser) {

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($ArrayUser['UserID']);
        $buyer->setName($ArrayUser['FirstName']);
        $buyer->setSurname($ArrayUser['LastName']);
        $buyer->setEmail($ArrayUser['EmailAddress']);
        $buyer->setIdentityNumber("user_" . $ArrayUser['Username']);

        $buyer->setLastLoginDate($ArrayUser['LastActivityDateTime']);
        $buyer->setRegistrationDate($ArrayUser['UserSince']);

        $address = isset($ArrayUser['Street']) && !empty($ArrayUser['Street']) ? $ArrayUser['Street'] : $ArrayUser['uCity'] . " " . $ArrayUser['uCountry'];
        $buyer->setRegistrationAddress($address);
        $buyer->setIp($ArrayUser['IP']);
        $buyer->setCity($ArrayUser['uCity']);
        $buyer->setCountry($ArrayUser['uCountry']);

        if (isset($ArrayUser['Zip']) && !empty($ArrayUser['Zip'])) {
            $buyer->setZipCode($ArrayUser['Zip']);
        }
        return $buyer;
    }

    private function constructBillingAddressObject($ArrayUser) {

        $billingAddress = new \Iyzipay\Model\Address();

        $name = $ArrayUser['FirstName'] . " " . $ArrayUser['LastName'];

        $billingAddress->setContactName($name);

        $billingAddress->setCity($ArrayUser['uCity']);
        $billingAddress->setCountry($ArrayUser['uCountry']);

        $address = isset($ArrayUser['Street']) && !empty($ArrayUser['Street']) ? $ArrayUser['Street'] : $ArrayUser['uCity'] . " " . $ArrayUser['uCountry'];
        $billingAddress->setAddress($address);

        if (isset($ArrayUser['Zip']) && !empty($ArrayUser['Zip'])) {
            $billingAddress->setZipCode($ArrayUser['Zip']);
        }

        return $billingAddress;
    }

    private function contructBasketItems($items) {

        $basketItems = array();

        foreach ($items as $item) {
            $basketItem = new \Iyzipay\Model\BasketItem();
            $basketItem->setId($item['ID']);
            $basketItem->setName($item['Name']);
            $basketItem->setCategory1($item['Category']);
            $basketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
            $basketItem->setPrice($item['Price']);
            $basketItems[] = $basketItem;
        }
        return $basketItems;
    }

    /**
     * Retrieves the received data from payment gateway and processes it
     *
     * @param string $ArrayParameters 
     * @return void
     * @author Cem Hurturk
     */
    public function ProcessReturnedData($ArrayParameters) {
        // Get the custom data received. Set the user ID and payment log ID - Start
        $Token = $ArrayParameters['token'];

        # create request class
        $request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        $request->setLocale(\Iyzipay\Model\Locale::EN);
        $request->setToken($Token);

        # make request
        $response = \Iyzipay\Model\CheckoutForm::retrieve($request, $this->Options);

        $status = $response->getStatus();
        $payment_status = $response->getPaymentStatus();
        $errorCode = $response->getErrorCode();
        $errorMessage = $response->getErrorMessage();
        $systemTime = $response->getSystemTime();

        if (strtolower($payment_status) != 'success' || strtolower($status) != 'success') {
            if (isset($errorCode) && !empty($errorCode)) {
                return array(false, 'general', $errorCode . "-" . $errorMessage);
            } else {
                return array(false, '', '0019');
            }
        }

        $paymentId = $response->getPaymentId();
        $basketId = $response->getBasketId();

        $paymentItems = $response->getPaymentItems();
        $paymentItem = $paymentItems[0];
        $paymentTransactionId = $paymentItem->getPaymentTransactionId();

        $CustomData = $basketId;
        $CustomData = explode('-', $CustomData);
        $LogID = $CustomData[0];
        $Process = $CustomData[1];
        $UserID = $CustomData[2];
        $OldGroupID = $CustomData[3];
        $NewGroupID = $CustomData[4];
        $ReputationLevel = $CustomData[5];
        $IsCreditPurchase = (isset($CustomData[6]) == true && $CustomData[6] == 'crediting' ? true : false);
        $Credits = $CustomData[7];

        if (($UserID == '') || ($UserID == 0) || ($Process == '')) {
            return array(false, $Process, '0007');
        }
        // Get the custom data received. Set the user ID and payment log ID - End

        $ProcessParameters = array(
            "UserID" => $UserID,
            "LogID" => $LogID,
            "OldGroupID" => $OldGroupID,
            "NewGroupID" => $NewGroupID,
            "PaymentStatus" => $payment_status,
            "PaymentTransactionId" => $paymentTransactionId,
            "PaymentId" => $paymentId,
            "Gateway" => $this->gateway,
            "ReputationLevel" => $ReputationLevel,
            "Credits" => $Credits,
        );
        Core::LoadObject('api');
        Core::LoadObject('payments');
        Core::LoadObject('user_groups');
        Core::LoadObject('user_balance');
        Core::LoadObject('user_payment');

        if ($IsCreditPurchase == true) {

            $Result = UserPayment::processCreditPurchase($ProcessParameters);
            if (!$Result[0]) {
                return array(false, $Process, $Result[1]);
            }
            return array(true, $Process, '0018');
            // Process the received data - End }
        } else if ($Process == 'Upgrade') {

            $Result = UserPayment::processUpgrade($ProcessParameters);

            if (!$Result[0]) {
                return array(false, $Process, $Result[1]);
            }
            return array(true, $Process, '0003');
        } else if ($Process == 'PayInvoice') {

            $Result = UserPayment::processPayInvoice($ProcessParameters);
            if (!$Result) {
                return array(false, $Process, $Result[1]);
            }
            return array(true, $Process, '0009');
        }
    }

}

// END class PaymentGateway_PAYPAL_EXPRESS
?>