<?php

require_once('config.php');

# create request class
$request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
$request->setLocale(\Iyzipay\Model\Locale::EN);
$request->setConversationId("98765421");
//$request->setToken("token");
$request->setToken("acacb86d-b099-42e2-9165-7836cbdb7eba");

# make request
$response = \Iyzipay\Model\CheckoutForm::retrieve($request, Config::options());

# print result
print_r('<pre>');
print_r($response);
print_r('</pre>');

//print_r("conversationId=" . $response->getConversationId());
//print_r("locale=" . $response->getLocale());
//print_r("status=" . $response->getStatus());
//print_r("errorCode=" . $response->getErrorCode());
//print_r("errorMessage=" . $response->getErrorMessage());
//print_r("errorGroup=" . $response->getErrorGroup());
//print_r("systemTime=" . $response->getSystemTime());
//print_r("token=" . $response->getToken()); //get user id with that token
//print_r("callbackUrl=" . $response->getCallbackUrl());
///* Shows the status of payment transaction.
//  Valid values are;SUCCESS, FAILURE,
//  INIT_THREEDS, CALLBACK_THREEDS,
//  BKM_POS_SELECTED, CALLBACK_PECCO
// */
//print_r("paymentStatus=" . $response->getPaymentStatus());
///* Id of the payment. Merchants should keep payment ID in their system. 
//  (This id will be used for cancel requests) */
//print_r("paymentId=" . $response->getPaymentId());
///* Total amount of basket. The sum of basket items’
//  amount must be equal to the price of basket */
//print_r("price=" . $response->getPrice());
///* Final price that will be captured from card.
//  Merchants should store this value in their system */
//print_r("paidPrice=" . $response->getPaidPrice());
//print_r("installment=" . $response->getInstallment());
//
//print_r("basketId=" . $response->getBasketId()); //Merchant's basket ID.
//print_r("binNumber=" . $response->getBinNumber()); //First 6 digit of card
//print_r("cardAssociation=" . $response->getCardAssociation()); //Valid values are VISA, MASTER_CARD,AMERICAN_EXPRESS
//print_r("cardFamily=" . $response->getCardFamily()); //Valid values are Bonus, Axess, World, Maximum,Paraf, CardFinans, AsyaCard, ...
//print_r("cardType=" . $response->getCardType()); //Valid values are CREDIT_CARD, DEBIT_CARD,PREPAID_CARD
////print_r("cardToken=" . $response->getCardToken());
////print_r("cardUserKey=" . $response->getCardUserKey());
///* Fraud risk status for payment request. Valid values
//  are -1, 0, 1. This will always return 1 for checkout
//  form solution */
//print_r("fraudStatus=" . $response->getFraudStatus());
//print_r("iyziCommissionFee=" . $response->getIyziCommissionFee());
//print_r("iyziCommissionRateAmount=" . $response->getIyziCommissionRateAmount());
//print_r("merchantCommissionRate=" . $response->getMerchantCommissionRate());
//print_r("merchantCommissionRateAmount=" + $response->getMerchantCommissionRateAmount());
//
//$itemTransactions = $response->getItemTransactions();
//
//if (isset($itemTransactions) && !empty($itemTransactions)) {
//    foreach ($itemTransactions as $itemTransaction) {
//        /* ID for each breakdown of payment. Merchants
//          should keep paymentTransactionID in their
//          system. (This id will be used for refund requests) */
//        print_r("paymentTransactionId=" . $itemTransaction->getPaymentTransactionId());
//        print_r("itemId=" + $itemTransaction->getItemId());
//        print_r("paidPrice=" + $itemTransaction->getPaidPrice());
//        print_r("price=" + $itemTransaction->getPrice());
//        print_r("transactionStatus=" + $itemTransaction->getTransactionStatus());
//        print_r("blockageRate=" + $itemTransaction->getBlockageRate());
//        print_r("blockageRateAmountMerchant=" + $itemTransaction->getBlockageRateAmountMerchant());
//        print_r("blockageRateAmountSubMerchant=" + $itemTransaction->getBlockageRateAmountSubMerchant());
//        print_r("blockageResolvedDate=" + $itemTransaction->getBlockageResolvedDate());
//        print_r("iyziCommissionFee=" + $itemTransaction->getIyziCommissionFee());
//        print_r("iyziCommissionRateAmount=" + $itemTransaction->getIyziCommissionRateAmount());
//        print_r("merchantCommissionRate=" + $itemTransaction->getMerchantCommissionRate());
//        print_r("merchantCommissionRateAmount=" + $itemTransaction->getMerchantCommissionRateAmount());
//        print_r("merchantPayoutAmount=" + $itemTransaction->getMerchantPayoutAmount());
//        print_r("subMerchantKey=" + $itemTransaction->getSubMerchantKey());
//        print_r("subMerchantPayoutAmount=" + $itemTransaction->getSubMerchantPayoutAmount());
//        print_r("subMerchantPayoutRate=" + $itemTransaction->getSubMerchantPayoutRate());
//        print_r("subMerchantPrice=" + $itemTransaction->getSubMerchantPrice());
//    }
//}

