<?php
/**
 * Media Library class
 *
 * This class holds all media library related functions
 * @package Oempro
 * @author Octeth
 **/
class MediaLibrary extends Core
{	
/**
 * Returns the media library information
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return boolean|array
 * @author Cem Hurturk
 */
public static function RetrieveMedia($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'medialibrary');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayMediaLibrary	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayMediaLibrary) < 1) { return false; } // if there are no clients, return false 
		
	$ArrayMediaLibrary = $ArrayMediaLibrary[0];

	return $ArrayMediaLibrary;
	}

/**
 * Uploads media to the media library
 *
 * @param string $ArrayFieldAndValues 
 * @param string $MediaData 
 * @param string $UserID 
 * @return integer
 * @author Cem Hurturk
 */
public static function UploadMedia($ArrayFieldAndValues, $MediaData, $UserID)
	{
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'medialibrary');
	
	$NewMediaID = Database::$Interface->GetLastInsertID();
	
	// Upload to file or Amazon S3 service - Start
	if (MEDIA_UPLOAD_METHOD == 'file')
		{
		$FileHandler = fopen(DATA_PATH.'media/'.md5($NewMediaID), 'w');
			fwrite($FileHandler, base64_decode($MediaData));
		fclose($FileHandler);
		}
	elseif (MEDIA_UPLOAD_METHOD == 's3')
		{
		Core::LoadObject('aws_s3');

		$ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
		$ObjectS3->putObjectString(base64_decode($MediaData), S3_BUCKET, S3_MEDIALIBRARY_PATH.'/'.md5($UserID).'/'.md5($NewMediaID), S3::ACL_PUBLIC_READ, array(), (isset($ArrayFieldAndValues['MediaType']) == true ? $ArrayFieldAndValues['MediaType'] : ''));
		}
	// Upload to file or Amazon S3 service - End

	return $NewMediaID;
	}

/**
 * Reteurns all media library items matching the provided criteria
 *
 * @param array $ArrayReturnFields
 * @param array $ArrayCriterias
 * @param array $ArrayOrder
 * @return boolean|array
 * @author Cem Hurturk
 **/
public static function RetrieveMediaItems($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('MediaID'=>'ASC'), $UserID)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'medialibrary');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayMediaItems	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayMediaItems) < 1) { return false; }

	foreach ($ArrayMediaItems as $Index=>$ArrayEachItem)
		{
		// Encrypted query parameters - Start
		$ArrayQueryParameters = array(
									'MediaID'			=> $ArrayEachItem['MediaID'],
									'UserID'			=> $UserID,
									);
		$EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
		// Encrypted query parameters - End

		$ArrayMediaItems[$Index]['MediaURL'] = APP_URL.'image_viewer.php?p='.$EncryptedQuery;
		unset($ArrayMediaItems[$Index]['MediaData']);
		}

	return $ArrayMediaItems;
	}

/**
 * Delete media library items
 *
 * @param $ArrayMediaFile
 * @return void
 * @author Cem Hurturk
 **/
public static function DeleteMediaLibraryItem($ArrayMedia, $UserID)
	{
	// Delete the media item - Start
	Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$UserID, 'MediaID'=>$ArrayMedia['MediaID']), MYSQL_TABLE_PREFIX.'medialibrary');
	// Delete the media item - End

	// Delete media file from database, file or s3 - Start
	if (MEDIA_UPLOAD_METHOD == 'file')
		{
		unlink(DATA_PATH.'media/'.md5($ArrayMedia['MediaID']));
		}
	elseif (MEDIA_UPLOAD_METHOD == 's3')
		{
		Core::LoadObject('aws_s3');

		$ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
		$ObjectS3->deleteObject(S3_BUCKET, S3_MEDIALIBRARY_PATH.'/'.md5($UserID).'/'.md5($ArrayMedia['MediaID']));
		}
	// Delete media file from database, file or s3 - End

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.MediaLibraryItem', array($ArrayMedia, $UserID));
	// Plug-in hook - End
	
	return;
	}

	
} // END class MediaLibrary
?>