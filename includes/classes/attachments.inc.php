<?php
/**
 * Attachments class
 *
 * This class holds all attachment related functions
 * @package Oempro
 * @author Octeth
 **/
class Attachments extends Core
{	
/**
 * Required fields for an attachment
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('RelEmailID', 'RelOwnerUserID', 'FileName', 'FileMimeType', 'FileSize', 'FileContent');	

/**
 * Reteurns attachment matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving attachments.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveAttachment($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'emails_attachments');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayAttachment	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayAttachment) < 1) { return false; } // if there are no attachments, return false

	$ArrayAttachment = $ArrayAttachment[0];

	$ArrayAttachment['AttachmentMD5ID'] = md5($ArrayAttachment['AttachmentID']);

	return $ArrayAttachment;
	}
	
/**
 * Reteurns all attachments matching given criterias
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving attachments.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveAttachments($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('FileName'=>'ASC'))
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'emails_attachments');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayAttachments	= Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayAttachments) < 1) { return false; } // if there are no attachments, return false

	foreach ($ArrayAttachments as &$EachAttachment)
		{
		$EachAttachment['AttachmentMD5ID'] = md5($EachAttachment['AttachmentID']);
		}

	return $ArrayAttachments;
	}

public static function Copy($RelEmailID, $AttachmentInformation)
	{
	$ArrayFieldAndValues = array(
		"RelEmailID"	=> $RelEmailID,
		"RelOwnerUserID"=> $AttachmentInformation['RelOwnerUserID'],
		"FileName"		=> $AttachmentInformation['FileName'],
		"FileMimeType"	=> $AttachmentInformation['FileMimeType'],
		"FileSize"		=> $AttachmentInformation['FileSize']
		);
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'emails_attachments');
	$AttachmentID = Database::$Interface->GetLastInsertID();
	if (copy(DATA_PATH.'attachments/'.md5($AttachmentInformation['AttachmentID']), DATA_PATH.'attachments/'.md5($AttachmentID)))
		{
		return true;
		}
	else
		{
		Attachments::Delete(array('AttachmentID'=>$AttachmentID));
		return false;
		}
	}

/**
 * Uploads the attachment file to database
 *
 * @param array $ArrayFieldAndValues
 * @return array Attachment ID, Attachment MD5ID
 * @author Mert Hurturk
 **/
public static function Upload($RelEmailID, $RelOwnerUserID, $ArrayFileInformation)
	{
	// Check required values - Start
	if ($RelEmailID === '' || $RelOwnerUserID === '') { return false; }
	if (count($ArrayFileInformation) < 1) { return false; }
	// Check required values - End

	if (ini_get('file_uploads') == 'off' || ini_get('file_uploads') == false || ini_get('file_uploads') == 'false' || ini_get('upload_max_filesize') == '0')
		{
		return false;
		}

	$UploadMaxFilesize = ini_get('upload_max_filesize');
	$UploadMaxFilesize_type = strtolower(substr($UploadMaxFilesize, strlen($UploadMaxFilesize)-1));
	$UploadMaxFilesize = substr($UploadMaxFilesize, 0, strlen($UploadMaxFilesize)-1);
	$UploadMaxFilesize = ($UploadMaxFilesize_type == "m" ? $UploadMaxFilesize*(1024*1024) : ($UploadMaxFilesize_type == "k" ? $UploadMaxFilesize*1024 : $UploadMaxFilesize ));
	$MaxUploadSize = ($UploadMaxFilesize < ATTACHMENT_MAX_FILESIZE ? $UploadMaxFilesize : ATTACHMENT_MAX_FILESIZE);

	if ($ArrayFileInformation['size'] <= $MaxUploadSize)
		{
		$ArrayFieldAndValues = array(
			"RelEmailID"	=> $RelEmailID,
			"RelOwnerUserID"=> $RelOwnerUserID,
			"FileName"		=> $ArrayFileInformation['name'],
			"FileMimeType"	=> $ArrayFileInformation['type'],
			"FileSize"		=> $ArrayFileInformation['size']
			);
		Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'emails_attachments');
		$AttachmentID = Database::$Interface->GetLastInsertID();
		if (move_uploaded_file($ArrayFileInformation['tmp_name'], DATA_PATH.'attachments/'.md5($AttachmentID)))
			{
			return array($AttachmentID, md5($AttachmentID));
			}
		else
			{
			Attachments::Delete(array('AttachmentID'=>$AttachmentID));
			return false;
			}
		}
	else
		{
		return false;
		}
	}

/**
 * Deletes attachments
 *
 * @param integer $AttachmentID Attachment id
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	if (isset($ArrayCriterias['RelEmailID']))
		{
		$ArrayAttachments = self::RetrieveAttachments(array('*'), array('RelEmailID'=>$ArrayCriterias['RelEmailID']));
		
		foreach ($ArrayAttachments as $EachAttachment)
			{
			self::Delete(array('AttachmentID'=>$EachAttachment['AttachmentID']));
			}
		}
		
	if (isset($ArrayCriterias['AttachmentID']))
		{
		Database::$Interface->DeleteRows($ArrayCriterias, MYSQL_TABLE_PREFIX.'emails_attachments');
		if (file_exists(DATA_PATH.'attachments/'.md5($ArrayCriterias['AttachmentID'])))
			{
			unlink(DATA_PATH.'attachments/'.md5($ArrayCriterias['AttachmentID']));
			}
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Attachment', array($ArrayCriterias));
	// Plug-in hook - End
	}

/**
 * Updates attachment information
 *
 * @param array $ArrayFieldAndValues Values of new attachment information
 * @param array $ArrayCriterias Criterias to be matched while updating attachment
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'emails_attachments');
	return true;
	}

	
} // END class Clients
?>