<?php
/**
 * Template Engine powered by Octeth Template object
 *
 * @package default
 * @author Cem Hurturk
 **/
class TemplateEngine extends OctethTemplate
{
/**
 * List of templates that will be retrieved and merged for parsing
 *
 * @var array
 **/
private $ArrayTemplates				= array();

/**
 * Pqge title
 *
 * @var string
 **/
public $PageTitle					= '';

/**
 * List of form fields and their types
 *
 * @var array
 **/
public $ArrayFormFields				= array();

/**
 * List of form field error messages
 *
 * @var array
 **/
public $ArrayErrorMessages			= array();

/**
 * Name of the class for error containing form fields
 *
 * @var string
 **/
public $ErrorClassName				= 'ErrorClass';

/**
 * User privileges
 *
 * @var string
 **/
public $UserPrivileges				= '';

/**
 * PHP block to eval
 *
 * @var string
 */
public $PHPBlockStart				= '\<\?php';
public $PHPBlockEnd					= '\?\>';

/**
 * undocumented function
 *
 * @return void
 * @author Cem Hurturk
 **/
function __construct($UserPrivileges = -1)
	{
	$this->UserPrivileges = $UserPrivileges;
	}

/**
 * Adds the template to the list for parsing
 *
 * @return void
 * @author Cem Hurturk
 **/
function AddToTemplateList($Template, $Type)
	{
	$this->ArrayTemplates[] = array($Template, $Type);
	}

/**
 * Loads the queued templates in FIFO order and makes the merged template ready for parsing
 *
 * @return void
 * @author Cem Hurturk
 **/
function LoadTemplates()
	{
	$TemplateContent = '';

	foreach ($this->ArrayTemplates as $Index=>$EachTemplate)
		{
		$this->LoadTemplate($EachTemplate[0], $EachTemplate[1]);
		$TemplateContent .= $this->ParseAndOutput();
		}

	$this->LoadTemplate($TemplateContent, 'string');
	
	// Make global replacements - Start
	$this->MakeGlobalReplacements();
	// Make global replacements - End
	}

/**
 * Returns the parsed template content
 *
 * @return string
 * @author Cem Hurturk
 **/
function ParseTemplate($ArrayLanguageStrings, $ArrayFormValues, $Output = false, $ArrayPlugInLanguageStrings = array())
	{
	// Parse screen language strings - Start
	$this->ParseScreenTexts($ArrayLanguageStrings);

	if (count($ArrayPlugInLanguageStrings) > 0)
		{
		$this->ParseScreenTexts($ArrayPlugInLanguageStrings, true);
		}
	// Parse screen language strings - End

	// Parse privilege blocks - Start
	if ($this->UserPrivileges != -1)
		{
		$this->ParsePrivileges();
		}
	// Parse privilege blocks - End

	// Show/Hide license specific blocks - Start
	$this->ParseLicenseBlocks();
	// Show/Hide license specific blocks - End

	// Plug-In Template Tag Detection - Start
//	$this->ProcessPlugInTemplateTags();
	// Plug-In Template Tag Detection - End

	// Plug-in template hooks - Start
//	$this->ProcessPlugInTemplateHooks();
	// Plug-in template hooks - End
	
	// List users for switching - Start {
	$this->ListUsersForSwitching();
	// List users for switching - End }
	
	// PHP eval - Start {
	$this->ParsePHP($this->PHPBlockStart, $this->PHPBlockEnd);
	// PHP eval - End }
	

	// Parse form fields - Start
	$this->ParseForm($ArrayFormValues, $this->ArrayFormFields, $this->ArrayErrorMessages, $this->ErrorClassName);
	// Parse form fields - End
		
	// Send headers to avoid browser caching - Start
	header('Pragma: no-cache');
	header('Cache-Control: no-cache');
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	// Send headers to avoid browser caching - End

	if ($Output == true)
		{
		print($this->ParseAndOutput());
		return;
		}
	else
		{
		return $this->ParseAndOutput();
		}
	}

/**
 * Lists users for switching (admin only)
 *
 * @return void
 * @author Cem Hurturk
 */
function ListUsersForSwitching()
	{
	Core::LoadObject('admin_auth');
	Core::LoadObject('users');

	if (AdminAuth::IsLoggedIn(false, false) == true)
		{
		$this->DefineBlock('SWITCH:User:Top');
		$this->DefineBlockInBlock('LIST:SwitchableUsers', 'SWITCH:User:Top');
		$this->DefineBlock('SWITCH:User:Footer');

		$ArrayUsers = Users::RetrieveUsers(array('*'), array(), array('FirstName'=>'ASC', 'LastName'=>'ASC'), 0, 0, false, false);

		// Get user information - Start
		$ArrayLoggedUserInformation = Users::RetrieveUser(array('*'), array('CONCAT(MD5(UserID), MD5(Username), Password)'=>$_SESSION[SESSION_NAME]['UserLogin']));
		// Get user information - End

		foreach ($ArrayUsers as $Index=>$ArrayEachUser)
			{
			$ArrayReplaceList = array();
			foreach ($ArrayEachUser as $EachField=>$EachValue)
				{
				$ArrayReplaceList['ListUser:'.$EachField] = $EachValue;
				}
			
			if ($ArrayEachUser['UserID'] == $ArrayLoggedUserInformation['UserID'])
				{
				$ArrayReplaceList['ListUser:Selected'] = 'selected';
				}
			else
				{
				$ArrayReplaceList['ListUser:Selected'] = '';
				}

			$this->DuplicateBlockInBlock('LIST:SwitchableUsers', 'SWITCH:User:Top', $ArrayReplaceList);
			}
		
		$ArrayReplaceList = array(
								);
		$this->DuplicateBlock('SWITCH:User:Top', 'SWITCH:User:Top', $ArrayReplaceList);
		$this->DuplicateBlock('SWITCH:User:Footer', 'SWITCH:User:Footer', $ArrayReplaceList);
		}
	else
		{
		$this->RemoveBlock('SWITCH:User:Top');
		$this->RemoveBlock('SWITCH:User:Footer');
		}
	}

/**
 * Detects plug-in template tags and processes them
 *
 * @return void
 * @author Cem Hurturk
 **/
function ProcessPlugInTemplateTags()
	{
	$TMPTemplateContent = $this->ParseAndOutput();

	Core::LoadObject('plugin');
	$TMPTemplateContent = Plugins::DetectTemplateTagsInTemplate($TMPTemplateContent);

	$this->LoadTemplate($TMPTemplateContent, 'string');
	}

/**
 * undocumented function
 *
 * @return void
 * @author Cem Hurturk
 **/
function ProcessPlugInTemplateHooks()
	{
	// Plug-in hook - Start
	$ArrayReturn = Plugins::HookListener('Action', 'System.Menu.Add', array());
		$ArrayMenuItems = $ArrayReturn[0];
	// Plug-in hook - End

	// Parse returned menu items  - Start
	foreach ($ArrayMenuItems as $Key=>$ArrayEachMenuItem)
		{
		$TemplateBlock = 'PLUGIN:MENU:'.$ArrayEachMenuItem['MenuLocation'];

		// If this plug-in tag block is already defined, do not re-define it - Start
		if (isset($this->ArrayOrgBlocks[$TemplateBlock]) == false)
			{
			$this->DefineBlock($TemplateBlock);
			}
		// If this plug-in tag block is already defined, do not re-define it - End
		
		$ArrayReplaceList = array(
								'List:ID'		=> $ArrayEachMenuItem['MenuID'],
								'List:Link'		=> $ArrayEachMenuItem['MenuLink'],
								'List:Title'	=> $ArrayEachMenuItem['MenuTitle'],
								);
		$this->DuplicateBlock($TemplateBlock, $TemplateBlock, $ArrayReplaceList);
		}
	// Parse returned menu items  - End

	// Remove other not used plug-in blocks in the template - Start
	$TMPTemplateContent = $this->ParseAndOutput();
	$ObjectTemplate = new OctethTemplate($TMPTemplateContent, 'string');

	$TMPPattern = "/<PLUGIN:MENU:(.*)>/iU";

	if (preg_match_all($TMPPattern, $TMPTemplateContent, $ArrayMatches, PREG_SET_ORDER))
		{
		$ArrayPrivilegeBlocksToRemove	= array();
		$ArrayPrivilegeBlocksToReplace	= array();
		
		foreach ($ArrayMatches as $EachMatch) 
			{
			$this->RemoveBlock('PLUGIN:MENU:'.$EachMatch[1]);
			}
		}

	$TemplateContent = $this->ParseAndOutput();
	$TemplateContent = str_replace($ArrayPrivilegeBlocksToRemove, $ArrayPrivilegeBlocksToReplace, $TemplateContent);
	// Remove other not used plug-in blocks in the template - End
	
	$this->LoadTemplate($TemplateContent, 'string');
	}

/**
 * Shows/hides license specific template parts
 *
 * @return object
 * @author Cem Hurturk
 */

function ParseLicenseBlocks()
	{
	$TMPTemplateContent = $this->ParseAndOutput();
	$ObjectTemplate = new OctethTemplate($TMPTemplateContent, 'string');

	Core::LoadObject('install');
	Install::GetLicenseInformation();
	

	$ArrayBlocksToRemove[]	 = '<LICENSE:REQUIRED:CORPORATE>';
	$ArrayBlocksToRemove[]	 = '</LICENSE:REQUIRED:CORPORATE>';
	$ArrayBlocksToRemove[]	 = '<LICENSE:REQUIRED:ESP>';
	$ArrayBlocksToRemove[]	 = '</LICENSE:REQUIRED:ESP>';
	$ArrayBlocksToReplace[]	 = '';
	$ArrayBlocksToReplace[]  = '';
	$ArrayBlocksToReplace[]	 = '';
	$ArrayBlocksToReplace[]  = '';

	// Remove unused privilege block - Start
	$TemplateContent = $this->ParseAndOutput();
	$TemplateContent = str_replace($ArrayBlocksToRemove, $ArrayBlocksToReplace, $TemplateContent);
	// Remove unused privilege block - End

	$this->LoadTemplate($TemplateContent, 'string');
	}

/**
 * Applies privileges to the template
 *
 * @return object
 * @author Cem Hurturk
 **/
function ParsePrivileges()
	{
	// Parse privileges - Start
	$TMPTemplateContent = $this->ParseAndOutput();
	$ObjectTemplate = new OctethTemplate($TMPTemplateContent, 'string');

	$ArrayUserPrivileges = explode(',', trim($this->UserPrivileges));

	$TMPPattern = "/<PRIV:(.*)>/iU";

	if (preg_match_all($TMPPattern, $TMPTemplateContent, $ArrayMatches, PREG_SET_ORDER))
		{
		$ArrayPrivilegeBlocksToRemove	= array();
		$ArrayPrivilegeBlocksToReplace	= array();
		
		foreach ($ArrayMatches as $EachMatch) 
			{
			$Mode = '';

			if (eregi("\%", $EachMatch[1]))
				{
				$ArrayPrivilegeBlocks = explode('%', $EachMatch[1]);
				$Mode = 'OR';
				}
			else if (eregi("&", $EachMatch[1]))
				{
				$ArrayPrivilegeBlocks = explode('&', $EachMatch[1]);
				$Mode = 'AND';
				}
			else
				{
				$ArrayPrivilegeBlocks = array($EachMatch[1]);
				$Mode = 'DEFAULT';
				}

			$DisablePrivBlock = false;
			$TMPCounter = 0;

			foreach ($ArrayPrivilegeBlocks as $EachPrivilege)
				{
				// If full privileges (*) by admin, don't disable anything - Start
				if ($this->UserPrivileges == '*')
					{
					$DisablePrivBlock = false;
					break;
					}
				// If full privileges (*) by admin, don't disable anything - End

				// Check for privileges - Start
				if (in_array($EachPrivilege, $ArrayUserPrivileges) == false)
					{
					if ($Mode != 'OR')
						{
						$DisablePrivBlock = true;
						break;
						}
					$TMPCounter++;
					}
				// Check for privileges - End
				}

			if (($Mode == 'OR') && ($TMPCounter == count($ArrayPrivilegeBlocks)))
				{
				$DisablePrivBlock = true;
				}
			
			if ($DisablePrivBlock == true)
				{
				$this->RemoveBlock('PRIV:'.$EachMatch[1]);
				}
			else
				{
				$ArrayPrivilegeBlocksToRemove[]	 = '<PRIV:'.$EachMatch[1].'>';
				$ArrayPrivilegeBlocksToRemove[]	 = '</PRIV:'.$EachMatch[1].'>';
				$ArrayPrivilegeBlocksToReplace[] = '';
				$ArrayPrivilegeBlocksToReplace[] = '';
				}
			}
		}
	// Parse privileges - Start

	// Remove unused privilege block - Start
	$TemplateContent = $this->ParseAndOutput();
	$TemplateContent = str_replace($ArrayPrivilegeBlocksToRemove, $ArrayPrivilegeBlocksToReplace, $TemplateContent);
	// Remove unused privilege block - End

	$this->LoadTemplate($TemplateContent, 'string');
	}

/**
 * Parses screen with language strings
 *
 * @return void
 * @author Cem Hurturk
 **/
function ParseScreenTexts($ArrayLanguageStrings, $IsPlugInLanguage = false)
	{
	$TemplateContent = $this->ParseAndOutput();

	// Parse screen texts - Start	
	if ($IsPlugInLanguage == false)
		{
		$TMPPattern = "/\_\{Language\:(.*)\}\_/iU";
		}
	else
		{
		$TMPPattern = "/\_\{LanguagePlugIn\:(.*)\}\_/iU";
		}

	if (preg_match_all($TMPPattern, $TemplateContent, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArraySubParts = explode(":", $EachMatch[1]);

			if ($ArraySubParts[1] == '')
				{
				$TemplateContent = str_replace('_{'.($IsPlugInLanguage == false ? 'Language' : 'LanguagePlugIn').':'.$EachMatch[1].'}_', $ArrayLanguageStrings["Screen"][$EachMatch[1]], $TemplateContent);
				}
			else
				{
				$TemplateContent = str_replace('_{'.($IsPlugInLanguage == false ? 'Language' : 'LanguagePlugIn').':'.$EachMatch[1].'}_', $ArrayLanguageStrings[$ArraySubParts[0]]["Screen"][$ArraySubParts[1]], $TemplateContent);
				}
			}
		}
	// Parse screen texts - End

	$this->LoadTemplate($TemplateContent, 'string');
	}

/**
 * Performs global replacements such as page title, etc.
 *
 * @return void
 * @author Cem Hurturk
 **/
function MakeGlobalReplacements()
	{
	global $ArrayLanguageStrings;
	
	Core::LoadObject('install');
	Install::GetLicenseInformation();
	

	$ArrayReplaceList = array(
							'Insert:PageTitle'					=> $this->PageTitle,
							'Insert:ThemeURL'					=> TEMPLATE_URL,
							'Insert:PlugInURL'					=> PLUGIN_URL,
							'Insert:AppURL'						=> APP_URL,
							'Insert:AdminURL'					=> Core::InterfaceAppURL().'/admin/',
							'Insert:UserURL'					=> Core::InterfaceAppURL().'/user/',
							'Insert:SubscriberURL'				=> Core::InterfaceAppURL().'/subscriber/',
							'Insert:ClientURL'					=> Core::InterfaceAppURL().'/client/',
							'Insert:CharSet'					=> CHARSET,
							'Insert:CacheRevisionNumber'		=> CACHE_REVISION_NUMBER,
							'Insert:ProductName'				=> PRODUCT_NAME,
							'Insert:ProductVersion'				=> PRODUCT_VERSION,
							'Insert:RegistrantName'				=> Install::$ArrayLicenseProperties['RegistrantName']['value'],
							'Insert:LicenseEdition'				=> Install::$ArrayLicenseProperties['Edition']['value'],
							'Insert:LicenseMaxUsers'			=> (Install::$ArrayLicenseProperties['MaxUsers']['value'] == '-1' ? $ArrayLanguageStrings['Screen']['9106'] : Install::$ArrayLicenseProperties['MaxUsers']['value']),
							'Insert:AuthorizedDomain'			=> Install::$ArrayLicenseProperties['Domain']['value'],
							'Insert:LatestAvailableVersionLink'	=> sprintf($ArrayLanguageStrings['Screen']['9160'], self::GetLatestVersionUpdateLink(true, 'HIDE:IfThereIsNoUpdate')),
							);
	$this->Replace($ArrayReplaceList);

	// Perform system check and display warning message if any problem exists - Start {
	self::DisplaySystemCheckError('HIDE:SystemCheckFailure');
	// Perform system check and display warning message if any problem exists - End }
	
	}

/**
 * Shows/hides system check error message on the interface
 *
 * @param string $TemplateBlock 
 * @return void
 * @author Cem Hurturk
 */
function DisplaySystemCheckError($TemplateBlock)
	{
	Core::LoadObject('admin_auth');
	if (AdminAuth::IsLoggedIn(false, false) == true)
		{
		$ArrayReturn = Core::SystemCheck();

		if ($ArrayReturn[0] == true)
			{
			$this->RemoveBlock($TemplateBlock);
			return '';
			}
		}
	else
		{
		$this->RemoveBlock($TemplateBlock);
		return '';
		}
	}

/**
 * Returns the link for upgrading to latest available version
 *
 * @return void
 * @author Cem Hurturk
 */
function GetLatestVersionUpdateLink($HideBlockOnNoUpdate, $Block)
	{
	Core::LoadObject('admin_auth');
	if (AdminAuth::IsLoggedIn(false, false) == true)
		{
		$LastCheckDate = Database::$Interface->GetOption('OemproLatestVersionLastCheckDate');
		$LatestVersion = Database::$Interface->GetOption('OemproLatestVersion');

		if (($LastCheckDate[0]['OptionValue'] != '') && (strtotime(date('Y-m-d H:i:s')) <= strtotime(date('Y-m-d', strtotime($LastCheckDate[0]['OptionValue'])).' +1 days '.date('H:i:s', strtotime($LastCheckDate[0]['OptionValue'])))) && ($LatestVersion[0]['OptionValue'] != ''))
			{
			// Check from database
			$LatestAvailableVersion = Database::$Interface->GetOption('OemproLatestVersion');
			$LatestAvailableVersion = $LatestAvailableVersion[0]['OptionValue'];

			if (Install::IsNewVersion($LatestAvailableVersion) == false)
				{
				// There's no any new update
				Database::$Interface->SaveOption('OemproLatestVersion', 'N/A');

				$this->RemoveBlock($Block);
				return '';
				}
			else
				{
				// New version is available
				Database::$Interface->SaveOption('OemproLatestVersion', $LatestAvailableVersion);

				return $LatestAvailableVersion;
				}
			}
		else
			{
			// Check from Octeth
			$LatestAvailableVersion = Install::WhatIsLatestVersion();

			Database::$Interface->SaveOption('OemproLatestVersionLastCheckDate', date('Y-m-d H:i:s'));

			if (Install::IsNewVersion($LatestAvailableVersion) == false)
				{
				// There's no any new update
				Database::$Interface->SaveOption('OemproLatestVersion', 'N/A');

				$this->RemoveBlock($Block);
				return '';
				}
			else
				{
				// New version is available
				Database::$Interface->SaveOption('OemproLatestVersion', $LatestAvailableVersion);

				return $LatestAvailableVersion;
				}
			}
		}
	else
		{
		$this->RemoveBlock($Block);
		return '';
		}
	}

/**
 * Parses (evals) PHP code in template pages
 *
 * @param string $PHPBlock 
 * @return void
 * @author Cem Hurturk
 */
function ParsePHP($PHPBlockStart, $PHPBlockEnd)
    {
	$TemplateContent = $this->ParseAndOutput();

		$Pattern = "/".$PHPBlockStart."([\w\W]*)".$PHPBlockEnd."/U";
	preg_match_all($Pattern, $TemplateContent, $ArrayMatches, PREG_SET_ORDER);

	ob_start();
	foreach ($ArrayMatches as $EachMatch)
		{
		eval($EachMatch[1]);
		$Return = ob_get_contents();
		ob_clean();
		$TemplateContent = str_replace($EachMatch[0], $Return, $TemplateContent);
		}
	ob_end_clean();

	$this->LoadTemplate($TemplateContent, 'string');
	
	return;
	}

} // END class TemplateEngine
?>