<?php
/**
 * CustomFields class
 *
 * This class holds all custom field related functions
 * @package Oempro
 * @author Octeth
 **/
class CustomFields extends Core
{
/**
 * Required fields for custom fields
 *
 * @static array
 **/
public static $ArrayRequiredFields = array('RelOwnerUserID', 'RelListID', 'FieldName', 'FieldType');	

/**
 * Default values for fields of a custom field
 *
 * @static array
 **/
public static $ArrayDefaultValuesForFields = array('FieldDefaultValue'=>'', 'ValidationMethod'=>'Disabled', 'IsGlobal'=>'No');	

/**
 * Returns a single custom field matching given criterias
 *
 * <code>
 * <?php
 * $ArrayCustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID'=>12));
 * ?>
 * </code>
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving custom fields.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveField($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'custom_fields');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayCustomField = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayCustomField) < 1) { return false; } // if there are no custom fields, return false 

	$ArrayCustomField = $ArrayCustomField[0];
	$ArrayCustomField['ArrayOptions'] = self::GetOptionsAsArray($ArrayCustomField['FieldOptions']);
	if ($ArrayCustomField['FieldType'] == 'Date field' && $ArrayCustomField['Option1'] != '')
		{
			$Years = explode('-', $ArrayCustomField['Option1']);
		$ArrayCustomField['DateFieldYearsStart'] = $Years[0];
		$ArrayCustomField['DateFieldYearsEnd'] = $Years[1];
		}
	return $ArrayCustomField;
	}

/**
 * Reteurns all custom fields matching given criterias
 *
 * <code>
 * <?php
 * $ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelListID'=>2), array('FieldName'=>'ASC'));
 * ?>
 * </code>
 *
 * @param array $ArrayReturnFields Field names of the values to be returned. If all fields to be returned, pass "*" in an array.
 * @param array $ArrayCriterias Criterias to be matched while retrieving custom fields.
 * @param array $ArrayOrder Fields to order.
 * @return boolean|array
 * @author Mert Hurturk
 **/
public static function RetrieveFields($ArrayReturnFields, $ArrayCriterias, $ArrayOrder = array('FieldName'=>'ASC'), $ExcludeUserOnlyFields = false)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'custom_fields');
	$ArrayCriterias		= $ArrayCriterias;

	if ($ExcludeUserOnlyFields == true)
		{
		$ArrayCriterias['Visibility'] = 'Public';
		}

	$ArrayCustomFields = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayCustomFields) < 1) { return false; }  // if there are no custom fields, return false

	foreach ($ArrayCustomFields as &$EachField)
		{
		if ($EachField['FieldType'] == 'Date field' && $EachField['Option1'] != '')
			{
				$Years = explode('-', $EachField['Option1']);
			$EachField['DateFieldYearsStart'] = $Years[0];
			$EachField['DateFieldYearsEnd'] = $Years[1];
			}
		}

	return $ArrayCustomFields;
	}

/**
 * Returns custom field options as an array
 *
 * <code>
 * <?php
 * $ArrayCustomFieldOptions = CustomFields::GetOptionsAsArray('[[Turkey]||[tr]]*,,,[[Germany]||[de]]');
 * ?>
 * </code>
 *
 * @param string $CustomFieldOptions Value of FieldOptions field of a custom field
 * @return array
 * @author Mert Hurturk
 */
public static function GetOptionsAsArray($CustomFieldOptions)
	{
	$ReturnArrayCustomFieldOption	= array();
	foreach (explode(',,,', $CustomFieldOptions) as $EachOption)
		{
		$EachOption = str_replace('[[', '', $EachOption);
		$IsSelected = 'false';

		if (substr($EachOption, strlen($EachOption) - 1, strlen($EachOption)) == '*')
			{
			$IsSelected = 'true';
			$EachOption = str_replace(']]*', '', $EachOption);
			}
		else
			{
			$EachOption = str_replace(']]', '', $EachOption);
			}
		$ArrayEachOption = explode(']||[', $EachOption);
		$ReturnArrayCustomFieldOption[] = array('label' => $ArrayEachOption[0], 'value' => str_replace(array('\\', '\''), array('\\\\', '\\\''), $ArrayEachOption[1]), 'is_selected' => $IsSelected);
		}
	return $ReturnArrayCustomFieldOption;
	}
	
/**
 * Returns custom field option as a formatted string
 *
 * <code>
 * <?php
 * $ArrayCustomFieldOptionString = CustomFields::GetOptionAsString('Turkey','tr',true);
 * ?>
 * </code>
 *
 * @param string $OptionLabel Label of an option
 * @param string $OptionValue Value of an option
 * @param boolean $IsSelected Determines if the option is selected or not
 * @return string
 * @author Mert Hurturk
 */
public static function GetOptionAsString($OptionLabel, $OptionValue, $IsSelected)
	{
	return '[['.$OptionLabel.']||['.$OptionValue.']]'.($IsSelected == true ? '*' : '');
	}
	
/**
 * Returns total number of custom fields of a subscriber list
 *
 * <code>
 * <?php
 * $TotalCustomFields = CustomFields::GetTotal(3);
 * ?>
 * </code>
 *
 * @param integer $OwnerListID Owner subscriber list's id
 * @return integer|boolean Total number of custom fields of given subscriber list
 * @author Mert Hurturk
 **/
public static function GetTotal($OwnerListID)
	{
	// Check for required fields of this function - Start
	if (!isset($OwnerListID) || $OwnerListID == '') { return false; }
	// Check for required fields of this function - End

	$ArrayFields		= array('COUNT(*) AS TotalFieldCount');
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'custom_fields');
	$ArrayCriterias		= array('RelListID'=>$OwnerListID, array('field' => 'RelListID', 'operator' => '=', 'value' => 0));
	$ArrayCustomFields = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, array(), 0, 0, 'OR', false, false);

	return $ArrayCustomFields[0]['TotalFieldCount'];
	}
	
/**
 * Creates a new custom field with given values
 *
 * <code>
 * <?php
 * $NewCustomFieldID = CustomFields::Create(array(
 *	'RelOwnerUserID' => 12,
 *	'RelListID' => '3',
 *	'FieldName' => 'First name',
 *	'FieldType' => 'Single line',
 *	'FieldDefaultValue' => '',
 *	'ValidationMethod' => 'Letters'
 *	));
 * ?>
 * </code>
 *
 * @param array $ArrayFieldAndValues Values of new custom field
 * @return boolean|integer ID of new custom field
 * @author Mert Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { print $ArrayFieldAndValues[$EachField]; return false; }
		}
	// Check required values - End

	// Check if any value is missing. if yes, give default values - Start
	foreach (self::$ArrayDefaultValuesForFields as $EachField=>$EachValue)
		{
		if (!isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { $ArrayFieldAndValues[$EachField] = $EachValue; }
		}
	// Check if any value is missing. if yes, give default values - End

	// If custom field is set to be global, set RelListID to 0 - Start {
	if ((isset($ArrayFieldAndValues['IsGlobal']) == true) && ($ArrayFieldAndValues['IsGlobal'] == 'Yes'))
		{
		$ArrayFieldAndValues['RelListID'] = 0;
		}
	// If custom field is set to be global, set RelListID to 0 - End }

	// Create a new record in custom_fields table - Start
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'custom_fields');
	$NewCustomFieldID = Database::$Interface->GetLastInsertID();
	// Create a new record in custom_fields table - End
	
	// Add custom_field field to subscribers table - Start
	$ArrayNewCustomField = self::RetrieveField(array('*'), array('CustomFieldID' => $NewCustomFieldID));
	self::CreateDatabaseTableField($SQLDataType, $ArrayFieldAndValues['RelListID'], $NewCustomFieldID, $ArrayNewCustomField, ($ArrayFieldAndValues['RelOwnerUserID'] > 0 ? $ArrayFieldAndValues['RelOwnerUserID'] : 0), ((isset($ArrayFieldAndValues['IsGlobal']) == true) && ($ArrayFieldAndValues['IsGlobal'] != 'Yes') ? false : ((isset($ArrayFieldAndValues['IsGlobal']) == true) && ($ArrayFieldAndValues['IsGlobal'] == 'Yes')) ? true : false));
	// Add custom_field field to subscribers table - End
	
	return $NewCustomFieldID;
	}

/**
 * Creates database table field for the custom field
 *
 * @return void
 * @author Cem Hurturk
 */
public static function CreateDatabaseTableField($SQLDataType, $ListID, $CustomFieldID, $ArrayNewCustomField, $UserID = 0, $IsGlobal = false) 
	{
	if ($ArrayNewCustomField['FieldType'] == 'Time field') {
		$SQLDataType = "TIME";
	} else if ($ArrayNewCustomField['FieldType'] == 'Date field') {
		$SQLDataType = "DATE";
	} else {
		switch ($ArrayNewCustomField['ValidationMethod'])
			{
			case "Numbers":
				$SQLDataType = "DOUBLE";
				break;
			case "Letters":
				$SQLDataType = "TEXT";
				break;
			case "Numbers and letters":
				$SQLDataType = "TEXT";
				break;
			case "Email address":
				$SQLDataType = "TEXT";
				break;
			case "URL":
				$SQLDataType = "TEXT";
				break;
			case "Date":
				$SQLDataType = "DATE";
				break;
			case "Time":
				$SQLDataType = "TIME";
				break;
			case "Custom":
				$SQLDataType = "TEXT";
				break;
			default:
				$SQLDataType = "TEXT";
				break;
			}
	}

	if ($IsGlobal == false)
		{
		// Local custom field. Create the new custom field in the target subscribers table
		$SQLQuery = "ALTER TABLE  `".MYSQL_TABLE_PREFIX."subscribers_".$ListID."` ADD  `CustomField".$CustomFieldID."` ".$SQLDataType." NULL DEFAULT NULL;";
		Database::$Interface->ExecuteQuery($SQLQuery);
		}
	else
		{
		// Global custom field. Create the new custom field in all subscriber lists
		// Core::LoadObject('lists');
		// $ArrayLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $UserID), array('ListID'=>'ASC'), array(), 0, 0);
		// 
		// foreach ($ArrayLists as $Index=>$ArrayEachList)
		// 	{
		// 	$SQLQuery = "ALTER TABLE  `".MYSQL_TABLE_PREFIX."subscribers_".$ArrayEachList['ListID']."` ADD  `CustomField".$CustomFieldID."` ".$SQLDataType." NULL DEFAULT NULL;";
		// 	Database::$Interface->ExecuteQuery($SQLQuery);
		// 	}
		}

	return;
	}

/**
 * undocumented function
 *
 * @return void
 * @author Mert Hurturk
 **/
public static function UpdateGlobalFieldData($UserID, $ListID, $SubscriberEmailAddress, $FieldInformation, $Value)
	{
	// Check if a data is already exists - Start {
	$data = Database::$Interface->GetRows_Enhanced(array(
		'Tables'	=> array(MYSQL_TABLE_PREFIX.'custom_field_values'),
		'Criteria'	=> array(
			array('Column'=>'RelOwnerUserID','Operator'=>'=','Value'=>$UserID),
			array('Column'=>'RelListID','Operator'=>'=','Value'=>$ListID,'Link'=>'AND'),
			array('Column'=>'RelFieldID','Operator'=>'=','Value'=>$FieldInformation['CustomFieldID'],'Link'=>'AND'),
			array('Column'=>'EmailAddress','Operator'=>'=','Value'=>$SubscriberEmailAddress,'Link'=>'AND')
			)));
	// Check if a data is already exists - End }

	$FieldsAndValues = array(
		'RelOwnerUserID'	=> $UserID,
		'RelListID'			=> $ListID,
		'RelFieldID'		=> $FieldInformation['CustomFieldID'],
		'EmailAddress'		=> $SubscriberEmailAddress
		);

	if ($FieldInformation['FieldType'] == 'Time field' || $FieldInformation['FieldType'] == 'Time')
		{
		$FieldsAndValues['ValueTime'] = $Value;
		}
	else if ($FieldInformation['FieldType'] == 'Date field' || $FieldInformation['FieldType'] == 'Date')
		{
		$FieldsAndValues['ValueDate'] = $Value;
		}
	else if ($FieldInformation['ValidationMethod'] == 'Numbers')
		{
		$FieldsAndValues['ValueDouble'] = $Value;
		}
	else
		{
		$FieldsAndValues['ValueText'] = $Value;
		}

	if (count($data) < 1)
		{
		Database::$Interface->InsertRow($FieldsAndValues, MYSQL_TABLE_PREFIX.'custom_field_values');
		}
	else
		{
		Database::$Interface->UpdateRows($FieldsAndValues, array('ValueID'=>$data[0]['ValueID']), MYSQL_TABLE_PREFIX.'custom_field_values');
		}
	
	}

public static function GetGlobalFieldData($UserID, $ListID, $SubscriberEmailAddress, $FieldInformation)
	{
	$data = Database::$Interface->GetRows_Enhanced(array(
		'Tables'	=> array(MYSQL_TABLE_PREFIX.'custom_field_values'),
		'Criteria'	=> array(
			array('Column'=>'RelOwnerUserID','Operator'=>'=','Value'=>$UserID),
			array('Column'=>'RelListID','Operator'=>'=','Value'=>$ListID,'Link'=>'AND'),
			array('Column'=>'EmailAddress','Operator'=>'=','Value'=>$SubscriberEmailAddress,'Link'=>'AND'),
			array('Column'=>'RelFieldID','Operator'=>'=','Value'=>$FieldInformation['CustomFieldID'],'Link'=>'AND')
			)));
	if (count($data) < 1)
		{
		return '';
		}
	else
		{
		$value = '';
		if ($FieldInformation['FieldType'] == 'Time field' || $FieldInformation['FieldType'] == 'Time')
			{
			$value = $data[0]['ValueTime'];
			}
		else if ($FieldInformation['FieldType'] == 'Date field' || $FieldInformation['FieldType'] == 'Date')
			{
			$value = $data[0]['ValueDate'];
			}
		else if ($FieldInformation['ValidationMethod'] == 'Numbers')
			{
			$value = $data[0]['ValueDouble'];
			}
		else
			{
			$value = $data[0]['ValueText'];
			}
		
		return $value;
		}
	}

/**
 * Updates custom field information
 *
 * <code>
 * <?php
 * CustomFields::Update(array(
 *	'FieldName' => 'First name',
 *	'FieldType' => 'Single line',
 *	'FieldDefaultValue' => '',
 *	'ValidationMethod' => 'Letters'
 *	), array(
 * 	'CustomFieldID' => 2
 *	));
 * ?>
 * </code>
 *
 * @param array $ArrayFieldAndValues Values of new custom field information
 * @param array $ArrayCriterias Criterias to be matched while updating custom field
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	// Check for required fields of this function - Start
		// $ArrayCriterias check
		if (!isset($ArrayCriterias) || count($ArrayCriterias) < 1) { return false; }
	// Check for required fields of this function - End

	// Check required values - Start
	foreach (self::$ArrayRequiredFields as $EachField)
		{
		if (isset($ArrayFieldAndValues[$EachField]) && $ArrayFieldAndValues[$EachField] == '') { return false; }
		}
	// Check required values - End

	$CustomFieldInformation = CustomFields::RetrieveField(array('*'), array('CustomFieldID'=>$ArrayCriterias['CustomFieldID']));

	// Global custom field can not be changed to local - Start {
	if ($CustomFieldInformation['IsGlobal'] == 'Yes' && (isset($ArrayFieldAndValues['IsGlobal']) && $ArrayFieldAndValues['IsGlobal'] == 'No'))
		{
		$TMPArray = $ArrayFieldAndValues;
		$ArrayFieldAndValues = array();
		
		foreach ($TMPArray as $Key=>$Value)
			{
			if (($Key != 'IsGlobal') && ($Key != 'RelListID'))
				{
				$ArrayFieldAndValues[$Key] = $Value;
				}
			}
		}
	// Global custom field can not be changed to local - End }

	// If custom field field type or validation method is changed, update field type in list table - Start {
	if ($CustomFieldInformation == false) return;
	$SQLDataType = '';

	$OriginalColumnNameForGlobalValueTable = '';
	if ($CustomFieldInformation['IsGlobal'] == 'Yes') {
		if ($CustomFieldInformation['FieldType'] == 'Time field') { $OriginalColumnNameForGlobalValueTable = 'ValueTime'; }
		elseif ($CustomFieldInformation['FieldType'] == 'Date field') { $OriginalColumnNameForGlobalValueTable = 'ValueDate'; }
		else {
			if ($CustomFieldInformation['ValidationMethod'] == 'Numbers') { $OriginalColumnNameForGlobalValueTable = 'ValueDouble'; }
			else { $OriginalColumnNameForGlobalValueTable = 'ValueText'; }
		}
	}

	if (isset($ArrayFieldAndValues['FieldType']) && $CustomFieldInformation['FieldType'] != $ArrayFieldAndValues['FieldType'])
		{
		if ($ArrayFieldAndValues['FieldType'] == 'Time field')
			{
			$SQLDataType = "TIME";
			$ArrayFieldAndValues['ValidationMethod'] = 'Disabled';
			} 
		else if ($ArrayFieldAndValues['FieldType'] == 'Date field') 
			{
			$SQLDataType = "DATE";
			$ArrayFieldAndValues['ValidationMethod'] = 'Disabled';
			}
		$CustomFieldInformation['FieldType'] = $ArrayFieldAndValues['FieldType'];
		$CustomFieldInformation['ValidationMethod'] = $ArrayFieldAndValues['ValidationMethod'];
		}
	else
		{
		if (isset($ArrayFieldAndValues['ValidationMethod']) && $CustomFieldInformation['ValidationMethod'] != $ArrayFieldAndValues['ValidationMethod'])
			{
			if ($ArrayFieldAndValues['ValidationMethod'] == 'Numbers')
				{
				$SQLDataType = 'DOUBLE';
				}
			else
				{
				$SQLDataType = 'TEXT';
				}
			$CustomFieldInformation['ValidationMethod'] = $ArrayFieldAndValues['ValidationMethod'];
			}
		}

	if ($SQLDataType != '')
		{
		if ($CustomFieldInformation['RelListID'] != 0)
			{
			$SQL = 'ALTER TABLE `'.MYSQL_TABLE_PREFIX.'subscribers_'.$CustomFieldInformation['RelListID'].'` CHANGE `CustomField'.$CustomFieldInformation['CustomFieldID'].'` `CustomField'.$CustomFieldInformation['CustomFieldID'].'` '.$SQLDataType.' NULL DEFAULT NULL';
			Database::$Interface->ExecuteQuery($SQL);
			}
		else
			{
			if ($CustomFieldInformation['IsGlobal'] == 'Yes')
				{
				$ColumnName = '';
				if ($SQLDataType == 'TIME') { $ColumnName = 'ValueTime'; }
				elseif ($SQLDataType == 'DATE') { $ColumnName = 'ValueDate'; }
				elseif ($SQLDataType == 'DOUBLE') { $ColumnName = 'ValueDouble'; }
				elseif ($SQLDataType == 'TEXT') { $ColumnName = 'ValueText'; }

				$SQL = 'UPDATE oempro_custom_field_values SET '.$ColumnName.' = '.$OriginalColumnNameForGlobalValueTable.' WHERE RelFieldID = '.$CustomFieldInformation['CustomFieldID'];
				Database::$Interface->ExecuteQuery($SQL);
				$SQL = 'UPDATE oempro_custom_field_values SET '.$OriginalColumnNameForGlobalValueTable.' = "" WHERE RelFieldID = '.$CustomFieldInformation['CustomFieldID'];
				Database::$Interface->ExecuteQuery($SQL);
				}
			}
		}
	// If custom field field type or validation method is changed, update field type in list table - End }

	// If custom field type is local and changed to global, insert local custom field data as global field data - Start {
	if ($CustomFieldInformation['IsGlobal'] == 'No' && (isset($ArrayFieldAndValues['IsGlobal']) && $ArrayFieldAndValues['IsGlobal'] == 'Yes'))
		{
		$ArrayFieldAndValues['RelListID'] = 0;
			
		$FieldName = 'ValueText';
		if ($CustomFieldInformation['FieldType'] == 'Date field' || $CustomFieldInformation['ValidationMethod'] == 'Date')
			{
			$FieldName = 'ValueDate';
			}
		else if ($CustomFieldInformation['FieldType'] == 'Time field' || $CustomFieldInformation['ValidationMethod'] == 'Time')
			{
			$FieldName = 'ValueTime';
			}
		else if ($CustomFieldInformation['ValidationMethod'] == 'Numbers')
			{
			$FieldName = 'ValueDouble';
			}
		
		$SQLQuery	= "EXPLAIN oempro_subscribers_".$CustomFieldInformation['RelListID'];
		$ResultSet	= Database::$Interface->ExecuteQuery($SQLQuery);

		while ($EachSubscriberField = mysql_fetch_assoc($ResultSet))
			{
			if (strpos($EachSubscriberField['Field'], 'CustomField') !== false)
				{
				$SubscriberFieldCustomFieldID = str_replace('CustomField', '', $EachSubscriberField['Field']);
				if ($SubscriberFieldCustomFieldID == $CustomFieldInformation['CustomFieldID'])
					{
					$SQLQuery = 'DELETE FROM oempro_custom_field_values WHERE RelFieldID = '.$CustomFieldInformation['CustomFieldID'].' AND RelOwnerUserID = '.$CustomFieldInformation['RelOwnerUserID'].' AND EmailAddress = ANY (SELECT EmailAddress FROM oempro_subscribers_'.$CustomFieldInformation['RelListID'].')';
					$ResultSet2 = Database::$Interface->ExecuteQuery($SQLQuery);

					$SQLQuery = "INSERT INTO oempro_custom_field_values (`ValueID`, `RelOwnerUserID`, `RelListID`, `RelFieldID`, `EmailAddress`, `".$FieldName."`) SELECT NULL, ".$CustomFieldInformation['RelOwnerUserID'].", 0, ".$CustomFieldInformation['CustomFieldID'].", EmailAddress, CustomField".$CustomFieldInformation['CustomFieldID']." FROM oempro_subscribers_".$CustomFieldInformation['RelListID'];
					$ResultSet2 = Database::$Interface->ExecuteQuery($SQLQuery);

					$SQLQuery = "ALTER TABLE `oempro_subscribers_".$CustomFieldInformation['RelListID']."` DROP `CustomField".$CustomFieldInformation['CustomFieldID']."`";
					$ResultSet2 = Database::$Interface->ExecuteQuery($SQLQuery);
					}
				}
			}
		}
	// If custom field type is local and changed to global, insert local custom field data as global field data - End }

	if ($CustomFieldInformation['IsGlobal'] == 'Yes' && (isset($ArrayFieldAndValues['RelListID']) && $ArrayFieldAndValues['RelListID'] != 0)) $ArrayFieldAndValues['RelListID'] = 0;

	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'custom_fields');
	return;
	}

/**
 * Deletes custom fields
 *
 * <code>
 * <?php
 * CustomFields::Delete(12, array(1,3,5,4));
 * ?>
 * </code>
 *
 * @param integer $OwnerUserID Owner user id of custom fields to be deleted
 * @param array $ArrayCustomFieldIDs Custom field ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Delete($OwnerUserID, $ArrayCustomFieldIDs, $ListID = 0)
	{
	// Check for required fields of this function - Start
		// $OwnerUserID check
		if (!isset($OwnerUserID) || $OwnerUserID == '') { return false; }
	// Check for required fields of this function - End

	if (count($ArrayCustomFieldIDs) > 0)
		{
		foreach ($ArrayCustomFieldIDs as $EachID)
			{
			// Get Custom Field information - Start
			$ArrayCustomFieldInformation = self::RetrieveField(array('*'),array('CustomFieldID'=>$EachID));
			// Get Custom Field information - End	
		
			// Delete from custom_fields table - Start
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'CustomFieldID'=>$EachID), MYSQL_TABLE_PREFIX.'custom_fields');
		
			// Delete custom_field values from custom_field_values table - Start
			if ($ArrayCustomFieldInformation['IsGlobal'] == 'No')
				{
				$SQLQuery = "ALTER TABLE `".MYSQL_TABLE_PREFIX."subscribers_".$ArrayCustomFieldInformation['RelListID']."` DROP  `CustomField".$EachID."`";
				Database::$Interface->ExecuteQuery($SQLQuery);
				}
			else
				{
				Database::$Interface->DeleteRows_Enhanced(array(
					'Table'	=> MYSQL_TABLE_PREFIX.'custom_field_values',
					'Criteria' => array(
						'Column'	=>	'RelFieldID',
						'Operator'	=>	'=',
						'Value'		=>	$EachID
						)
					));
				}
			// Delete custom_field values from custom_field_values table - End
			}
		}
	else
		{
		if ($ListID > 0)
			{
			$ArrayDeleteCriterias = array('RelOwnerUserID' => $OwnerUserID, 'RelListID' => $ListID);
			}
		else
			{
			$ArrayDeleteCriterias = array('RelOwnerUserID' => $OwnerUserID);
			}
		$ArrayFields = self::RetrieveFields(array('*'), $ArrayDeleteCriterias, array('CustomFieldID' => 'ASC'));
		
		foreach ($ArrayFields as $Index=>$ArrayEachField)
			{
			// Delete from custom_fields table - Start
			Database::$Interface->DeleteRows(array('RelOwnerUserID'=>$OwnerUserID, 'CustomFieldID'=>$ArrayEachField['CustomFieldID']), MYSQL_TABLE_PREFIX.'custom_fields');
		
			// Delete custom_field values from custom_field_values table - Start
			if ($ArrayEachField['IsGlobal'] == 'No')
				{
				$SQLQuery = "ALTER TABLE `".MYSQL_TABLE_PREFIX."subscribers_".$ArrayEachField['RelListID']."` DROP  `CustomField".$ArrayEachField['CustomFieldID']."`";
				Database::$Interface->ExecuteQuery($SQLQuery);
				}
			else
				{
				Database::$Interface->DeleteRows_Enhanced(array(
					'Table'	=> MYSQL_TABLE_PREFIX.'custom_field_values',
					'Criteria' => array(
						'Column'	=>	'RelFieldID',
						'Operator'	=>	'=',
						'Value'		=>	$ArrayEachField['CustomFieldID']
						)
					));
				}
			// Delete custom_field values from custom_field_values table - End
			}
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.CustomField', array($OwnerUserID, $ArrayCustomFieldIDs, $ListID));
	// Plug-in hook - End
	}

/**
 * Returns html code of a custom field
 *
 * <code>
 * <?php
 * $ArrayCustomFieldInformation = CustomFields::RetrieveField(3);
 * CustomFields::GenerateCustomFieldHTMLCode($ArrayCustomFieldInformation);
 * ?>
 * </code>
 *
 * @param array $ArrayCustomFieldIDs Custom field ids to be deleted
 * @return boolean
 * @author Mert Hurturk
 **/
public static function GenerateCustomFieldHTMLCode($ArrayCustomField, $Value = '')
	{
	$CustomFieldHTMLCode = '';

	if ($ArrayCustomField['FieldType'] == 'Time field')
		{
		$Value = explode(':', $Value);
		$CustomFieldHTMLCode = '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]"><option value="00"'.($Value[0] == '00' ? ' selected' : '').'>00</option><option value="01"'.($Value[0] == '01' ? ' selected' : '').'>01</option><option value="02"'.($Value[0] == '02' ? ' selected' : '').'>02</option><option value="03"'.($Value[0] == '03' ? ' selected' : '').'>03</option><option value="04"'.($Value[0] == '04' ? ' selected' : '').'>04</option><option value="05"'.($Value[0] == '05' ? ' selected' : '').'>05</option><option value="06"'.($Value[0] == '06' ? ' selected' : '').'>06</option><option value="07"'.($Value[0] == '07' ? ' selected' : '').'>07</option><option value="08"'.($Value[0] == '08' ? ' selected' : '').'>08</option><option value="09"'.($Value[0] == '09' ? ' selected' : '').'>09</option><option value="10"'.($Value[0] == '10' ? ' selected' : '').'>10</option><option value="11"'.($Value[0] == '11' ? ' selected' : '').'>11</option><option value="12"'.($Value[0] == '12' ? ' selected' : '').'>12</option><option value="13"'.($Value[0] == '13' ? ' selected' : '').'>13</option><option value="14"'.($Value[0] == '14' ? ' selected' : '').'>14</option><option value="15"'.($Value[0] == '15' ? ' selected' : '').'>15</option><option value="16"'.($Value[0] == '16' ? ' selected' : '').'>16</option><option value="17"'.($Value[0] == '17' ? ' selected' : '').'>17</option><option value="18"'.($Value[0] == '18' ? ' selected' : '').'>18</option><option value="19"'.($Value[0] == '19' ? ' selected' : '').'>19</option><option value="20"'.($Value[0] == '20' ? ' selected' : '').'>20</option><option value="21"'.($Value[0] == '21' ? ' selected' : '').'>21</option><option value="22"'.($Value[0] == '22' ? ' selected' : '').'>22</option><option value="23"'.($Value[0] == '23' ? ' selected' : '').'>23</option></select> : ';
		$CustomFieldHTMLCode .= '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]"><option value="00"'.($Value[1] == '00' ? ' selected' : '').'>00</option><option value="01"'.($Value[1] == '01' ? ' selected' : '').'>01</option><option value="02"'.($Value[1] == '02' ? ' selected' : '').'>02</option><option value="03"'.($Value[1] == '03' ? ' selected' : '').'>03</option><option value="04"'.($Value[1] == '04' ? ' selected' : '').'>04</option><option value="05"'.($Value[1] == '05' ? ' selected' : '').'>05</option><option value="06"'.($Value[1] == '06' ? ' selected' : '').'>06</option><option value="07"'.($Value[1] == '07' ? ' selected' : '').'>07</option><option value="08"'.($Value[1] == '08' ? ' selected' : '').'>08</option><option value="09"'.($Value[1] == '09' ? ' selected' : '').'>09</option><option value="10"'.($Value[1] == '10' ? ' selected' : '').'>10</option><option value="11"'.($Value[1] == '11' ? ' selected' : '').'>11</option><option value="12"'.($Value[1] == '12' ? ' selected' : '').'>12</option><option value="13"'.($Value[1] == '13' ? ' selected' : '').'>13</option><option value="14"'.($Value[1] == '14' ? ' selected' : '').'>14</option><option value="15"'.($Value[1] == '15' ? ' selected' : '').'>15</option><option value="16"'.($Value[1] == '16' ? ' selected' : '').'>16</option><option value="17"'.($Value[1] == '17' ? ' selected' : '').'>17</option><option value="18"'.($Value[1] == '18' ? ' selected' : '').'>18</option><option value="19"'.($Value[1] == '19' ? ' selected' : '').'>19</option><option value="20"'.($Value[1] == '20' ? ' selected' : '').'>20</option>';
		$CustomFieldHTMLCode .= '<option value="21"'.($Value[1] == '21' ? ' selected' : '').'>21</option><option value="22"'.($Value[1] == '22' ? ' selected' : '').'>22</option><option value="23"'.($Value[1] == '23' ? ' selected' : '').'>23</option><option value="24"'.($Value[1] == '24' ? ' selected' : '').'>24</option><option value="25"'.($Value[1] == '25' ? ' selected' : '').'>25</option><option value="26"'.($Value[1] == '26' ? ' selected' : '').'>26</option><option value="27"'.($Value[1] == '27' ? ' selected' : '').'>27</option><option value="28"'.($Value[1] == '28' ? ' selected' : '').'>28</option><option value="29"'.($Value[1] == '29' ? ' selected' : '').'>29</option><option value="30"'.($Value[1] == '30' ? ' selected' : '').'>30</option><option value="31"'.($Value[1] == '31' ? ' selected' : '').'>31</option><option value="32"'.($Value[1] == '32' ? ' selected' : '').'>32</option><option value="33"'.($Value[1] == '33' ? ' selected' : '').'>33</option><option value="34"'.($Value[1] == '34' ? ' selected' : '').'>34</option><option value="35"'.($Value[1] == '35' ? ' selected' : '').'>35</option><option value="36"'.($Value[1] == '36' ? ' selected' : '').'>36</option><option value="37"'.($Value[1] == '37' ? ' selected' : '').'>37</option><option value="38"'.($Value[1] == '38' ? ' selected' : '').'>38</option><option value="39"'.($Value[1] == '39' ? ' selected' : '').'>39</option><option value="40"'.($Value[1] == '40' ? ' selected' : '').'>40</option><option value="41"'.($Value[1] == '41' ? ' selected' : '').'>41</option>';
		$CustomFieldHTMLCode .= '<option value="42"'.($Value[1] == '42' ? ' selected' : '').'>42</option><option value="43"'.($Value[1] == '43' ? ' selected' : '').'>43</option><option value="44"'.($Value[1] == '44' ? ' selected' : '').'>44</option><option value="45"'.($Value[1] == '45' ? ' selected' : '').'>45</option><option value="46"'.($Value[1] == '46' ? ' selected' : '').'>46</option><option value="47"'.($Value[1] == '47' ? ' selected' : '').'>47</option><option value="48"'.($Value[1] == '48' ? ' selected' : '').'>48</option><option value="49"'.($Value[1] == '49' ? ' selected' : '').'>49</option><option value="50"'.($Value[1] == '50' ? ' selected' : '').'>50</option><option value="51"'.($Value[1] == '51' ? ' selected' : '').'>51</option><option value="52"'.($Value[1] == '52' ? ' selected' : '').'>52</option><option value="53"'.($Value[1] == '53' ? ' selected' : '').'>53</option><option value="54"'.($Value[1] == '54' ? ' selected' : '').'>54</option><option value="55"'.($Value[1] == '55' ? ' selected' : '').'>55</option><option value="56"'.($Value[1] == '56' ? ' selected' : '').'>56</option><option value="57"'.($Value[1] == '57' ? ' selected' : '').'>57</option><option value="58"'.($Value[1] == '58' ? ' selected' : '').'>58</option><option value="59"'.($Value[1] == '59' ? ' selected' : '').'>59</option>';
		$CustomFieldHTMLCode .= '</select>';
		}
	if ($ArrayCustomField['FieldType'] == 'Date field')
		{
		$Value = explode('-', $Value);
		$CustomFieldHTMLCode = '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]"><option value="">Day</option><option value="01"'.($Value[2] == '01' ? ' selected' : '').'>01</option><option value="02"'.($Value[2] == '02' ? ' selected' : '').'>02</option><option value="03"'.($Value[2] == '03' ? ' selected' : '').'>03</option><option value="04"'.($Value[2] == '04' ? ' selected' : '').'>04</option><option value="05"'.($Value[2] == '05' ? ' selected' : '').'>05</option><option value="06"'.($Value[2] == '06' ? ' selected' : '').'>06</option><option value="07"'.($Value[2] == '07' ? ' selected' : '').'>07</option><option value="08"'.($Value[2] == '08' ? ' selected' : '').'>08</option><option value="09"'.($Value[2] == '09' ? ' selected' : '').'>09</option><option value="10"'.($Value[2] == '10' ? ' selected' : '').'>10</option><option value="11"'.($Value[2] == '11' ? ' selected' : '').'>11</option><option value="12"'.($Value[2] == '12' ? ' selected' : '').'>12</option><option value="13"'.($Value[2] == '13' ? ' selected' : '').'>13</option><option value="14"'.($Value[2] == '14' ? ' selected' : '').'>14</option><option value="15"'.($Value[2] == '15' ? ' selected' : '').'>15</option><option value="16"'.($Value[2] == '16' ? ' selected' : '').'>16</option><option value="17"'.($Value[2] == '17' ? ' selected' : '').'>17</option><option value="18"'.($Value[2] == '18' ? ' selected' : '').'>18</option><option value="19"'.($Value[2] == '19' ? ' selected' : '').'>19</option><option value="20"'.($Value[2] == '20' ? ' selected' : '').'>20</option>';
		$CustomFieldHTMLCode .= '<option value="21"'.($Value[2] == '21' ? ' selected' : '').'>21</option><option value="22"'.($Value[2] == '22' ? ' selected' : '').'>22</option><option value="23"'.($Value[2] == '23' ? ' selected' : '').'>23</option><option value="24"'.($Value[2] == '24' ? ' selected' : '').'>24</option><option value="25"'.($Value[2] == '25' ? ' selected' : '').'>25</option><option value="26"'.($Value[2] == '26' ? ' selected' : '').'>26</option><option value="27"'.($Value[2] == '27' ? ' selected' : '').'>27</option><option value="28"'.($Value[2] == '28' ? ' selected' : '').'>28</option><option value="29"'.($Value[2] == '29' ? ' selected' : '').'>29</option><option value="30"'.($Value[2] == '30' ? ' selected' : '').'>30</option><option value="31"'.($Value[2] == '31' ? ' selected' : '').'>31</option></select> / ';
		$CustomFieldHTMLCode .= '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]"><option value="">Month</option><option value="01"'.($Value[1] == '01' ? ' selected' : '').'>01</option><option value="02"'.($Value[1] == '02' ? ' selected' : '').'>02</option><option value="03"'.($Value[1] == '03' ? ' selected' : '').'>03</option><option value="04"'.($Value[1] == '04' ? ' selected' : '').'>04</option><option value="05"'.($Value[1] == '05' ? ' selected' : '').'>05</option><option value="06"'.($Value[1] == '06' ? ' selected' : '').'>06</option><option value="07"'.($Value[1] == '07' ? ' selected' : '').'>07</option><option value="08"'.($Value[1] == '08' ? ' selected' : '').'>08</option><option value="09"'.($Value[1] == '09' ? ' selected' : '').'>09</option><option value="10"'.($Value[1] == '10' ? ' selected' : '').'>10</option><option value="11"'.($Value[1] == '11' ? ' selected' : '').'>11</option><option value="12"'.($Value[1] == '12' ? ' selected' : '').'>12</option></select> / ';
		$CustomFieldHTMLCode .= '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]"><option value="">Year</option>';
		
			$YearCount = 10;
			$Years = array(2000, 2009);
			if ($ArrayCustomField['Option1'] != '')
				{
				$Years = array($ArrayCustomField['DateFieldYearsStart'], $ArrayCustomField['DateFieldYearsEnd']);
				if ($Years[1] > $Years[0])
					{
					$YearCount = ($Years[1] - $Years[0]);
					}
				}
			for ($i=0; $i<$YearCount+1; $i++)
				{
				$CustomFieldHTMLCode .= '<option value="'.($Years[0] + $i).'" '.($Value[0] == ($Years[0] + $i) ? 'selected' : '').'>'.($Years[0] + $i).'</option>';
				}

		$CustomFieldHTMLCode .= '</select>';
		}
	elseif ($ArrayCustomField['FieldType'] == 'Single line')
		{
		$CustomFieldHTMLCode = '<input type="text" name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].']" value="'.($Value != '' ? htmlspecialchars($Value) : htmlspecialchars($ArrayCustomField['FieldDefaultValue'])).'" id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'" />';
		}
	elseif ($ArrayCustomField['FieldType'] == 'Paragraph text')
		{
		$CustomFieldHTMLCode = '<textarea id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'" name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].']" style="width: 200px; height: 100px;">'.($Value != '' ? htmlspecialchars($Value) : htmlspecialchars($ArrayCustomField['FieldDefaultValue'])).'</textarea>';
		}
	elseif ($ArrayCustomField['FieldType'] == 'Multiple choice')
		{
		$ArrayCustomFieldChoices	= self::GetOptionsAsArray($ArrayCustomField['FieldOptions']);

		$CustomFieldHTMLCode = '';
		$CustomFieldHTMLCode .= '<div class="form-row-radio-field">';
		foreach ($ArrayCustomFieldChoices as $ArrayEachChoice)
			{
			$Checked = ($Value != '' ? ($Value == $ArrayEachChoice['value'] ? 'checked' : '') : ($ArrayEachChoice['is_selected'] == 'true' ? 'checked' : ''));
			$CustomFieldHTMLCode .= '<input type="radio" name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].']" value="'.htmlspecialchars($ArrayEachChoice['value']).'" id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'" '.$Checked.' /> '.htmlspecialchars($ArrayEachChoice['label']).'<br />'."\n";
			}
		$CustomFieldHTMLCode .= '</div>';
		}
	elseif ($ArrayCustomField['FieldType'] == 'Drop down')
		{
		$ArrayCustomFieldChoices	= self::GetOptionsAsArray($ArrayCustomField['FieldOptions']);

		$CustomFieldHTMLCode  = '<select name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].']" id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'">';

		foreach ($ArrayCustomFieldChoices as $ArrayEachChoice)
			{
			$Selected = ($Value != '' ? ($Value == $ArrayEachChoice['value'] ? 'selected' : '') : ($ArrayEachChoice['is_selected'] == 'true' ? 'selected' : ''));
			$CustomFieldHTMLCode .= '<option value="'.htmlspecialchars($ArrayEachChoice['value']).'" '.$Selected.'>'.htmlspecialchars($ArrayEachChoice['label']).'</option>'."\n";
			}

		$CustomFieldHTMLCode  .= '</select>';
		}
	elseif ($ArrayCustomField['FieldType'] == 'Checkboxes')
		{
		$ArrayCustomFieldChoices	= self::GetOptionsAsArray($ArrayCustomField['FieldOptions']);
		$ArrayValues				= explode('||||', $Value);

		$CustomFieldHTMLCode  = '';
		$CustomFieldHTMLCode .= '<div class="form-row-radio-field">';
		foreach ($ArrayCustomFieldChoices as $ArrayEachChoice)
			{
			// $Checked = ($Value != '' ? ($Value == $ArrayEachChoice['value'] ? 'checked' : '') : ($ArrayEachChoice['is_selected'] == 'true' ? 'checked' : ''));
			$Checked = (count($ArrayValues) > 0 ? (in_array($ArrayEachChoice['value'], $ArrayValues) == true ? 'checked' : '') : ($ArrayEachChoice['is_selected'] == 'true' ? 'checked' : ''));
			$CustomFieldHTMLCode .= '<input type="checkbox" name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].'][]" value="'.htmlspecialchars($ArrayEachChoice['value']).'" id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'" '.$Checked.' /> '.htmlspecialchars($ArrayEachChoice['label']).'<br />'."\n";
			}
		$CustomFieldHTMLCode .= '</div>';
		}
	elseif (($ArrayCustomField['FieldType'] == 'Hidden') || ($ArrayCustomField['FieldType'] == 'Hidden field'))
		{
		$CustomFieldHTMLCode = '<input type="hidden" name="FormValue_Fields[CustomField'.$ArrayCustomField['CustomFieldID'].']" value="'.htmlspecialchars($ArrayCustomField['FieldDefaultValue']).'" id="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'" />';
		}

	return $CustomFieldHTMLCode;
	}	

/**
 * Validates the provided value against custom field validation rule
 *
 * @param string $ArrayCustomField 
 * @param string $CustomFieldValue 
 * @return array
 * @author Cem Hurturk
 */

public static function ValidateCustomFieldValue($ArrayCustomField, $CustomFieldValue)
	{
	if ($ArrayCustomField['ValidationMethod'] == 'Disabled')
		{
		return array(true);
		}
	elseif (($ArrayCustomField['ValidationMethod'] == 'Numbers') && (preg_match("/^[0-9]*$/s", $CustomFieldValue) == false))
		{
		return array(false, 'Custom field value is not a numeric value');
		}
	elseif (($ArrayCustomField['ValidationMethod'] == 'Letters') && (preg_match("/^[a-zA-ZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿşŞĞğ\s\-\']*$/s", $CustomFieldValue) == false))
		{
		return array(false, 'Custom field value is not an alpha value');
		}
	elseif (($ArrayCustomField['ValidationMethod'] == 'Numbers and letters') && (preg_match("/^[a-zA-Z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿşŞĞğ_\s]*$/s", $CustomFieldValue) == false))
		{
		return array(false, 'Custom field value is not an alpha numeric value');
		}
	elseif ($ArrayCustomField['ValidationMethod'] == 'Email address')
		{
		Core::LoadObject('subscribers');

		if (Subscribers::ValidateEmailAddress($CustomFieldValue) == false)
			{
			return array(false, 'Custom field value is not an email address');
			}
		}
	elseif (($ArrayCustomField['ValidationMethod'] == 'URL') && (preg_match ('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $CustomFieldValue) == false))
		{
		return array(false, 'Custom field value is not an URL address');
		}
	elseif ($ArrayCustomField['ValidationMethod'] == 'Date')
		{
		// There's no need to check date format with strtotime. It will cause several problems.
		}
	elseif ($ArrayCustomField['ValidationMethod'] == 'Time')
		{
		if (strtotime($CustomFieldValue) == false)
			{
			return array(false, 'Custom field value has invalid time format');
			}
		}
	elseif (($ArrayCustomField['ValidationMethod'] == 'Custom') && (preg_match('/'.$ArrayCustomField['ValidationRule'].'/s', $CustomFieldValue) == false))
		{
		return array(false, 'Custom field value is not properly formatted');
		}
	else
		{
		return array(true);
		}

	return array(true);
	}

} // END class Lists
?>