<?php

/**
 * Admins class
 *
 * This class holds all administrator related functions
 * @package Oempro
 * @author Octeth
 **/
class Admins extends Core
{	
/**
 * Returns the admin information
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return boolean|array
 * @author Cem Hurturk
 */
public static function RetrieveAdmin($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'admins');
	$ArrayCriterias		= $ArrayCriterias;
	$ArrayOrder			= array();
	$ArrayAdmin = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayAdmin) < 1) { return false; } // if there are no clients, return false 
		
	$ArrayAdmin = $ArrayAdmin[0];

	return $ArrayAdmin;
	}

/**
 * Updates admin information
 *
 * @param array $ArrayFieldAndValues Values of new admin information
 * @param array $ArrayCriterias Criterias to be matched while updating admin
 * @return boolean
 * @author Mert Hurturk
 **/
public static function UpdateAdmin($ArrayFieldAndValues, $ArrayCriterias)
	{
	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'admins');
	return true;
	}

/**
 * Creates a new administrator with given values
 *
 * @param array $ArrayFieldAndValues Values of new administrator
 * @return boolean|integer ID of new administrator
 * @author Cem Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'admins');
	
	$NewAdminID = Database::$Interface->GetLastInsertID();
	return $NewAdminID;
	}

/**
 * Returns the current logged in administrator account information
 *
 * @return void
 * @author Cem Hurturk
 */
public static function GetCurrentLoggedInAdmin()
	{
	$ArrayAdminInformation = self::RetrieveAdmin(array('*'), array('CONCAT(MD5(AdminID), MD5(Username), MD5(Password))'=>$_SESSION[SESSION_NAME]['AdminLogin']));
	
	if ($ArrayAdminInformation == false)
		{
		return false;
		}
	else
		{
		return $ArrayAdminInformation;	
		}
	}


} // END class Admins


?>