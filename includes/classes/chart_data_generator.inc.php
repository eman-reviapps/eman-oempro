<?php

/**
 * Chart Data Generator class
 *
 * This class holds all administrator related functions
 * @package Oempro
 * @author Octeth
 **/
class ChartDataGenerator extends Core
{	
/**
 * Returns the generated XML data for the chart engine
 *
 * @param string $ArrayData 
 * @return void
 * @author Cem Hurturk
 */
function GenerateXMLData($ArrayData)
	{
	$XML = array();
	
	$XML[] = '<chart>';

	// Series - Start {
	$XML[] = '<series>';
	$TMPCounter = 0;
	foreach ($ArrayData['Series'] as $Each)
		{
		$XML[] = '<value xid="'.$TMPCounter.'"><![CDATA['.$Each.']]></value>';
		$TMPCounter++;
		}
	$XML[] = '</series>';
	// Series - End }

	// Graphs - Start {
	$XML[] = '<graphs>';

	foreach ($ArrayData['Graphs'] as $Index=>$ArrayGraphSettings)
		{
		$XML[] = '<graph gid="'.$Index.'" '.self::_GenerateGraphSettings($ArrayGraphSettings).'>';

		foreach ($ArrayData['GraphData'][$Index] as $DataIndex=>$ArrayEachData)
			{
			$XML[] = '<value xid="'.$DataIndex.'" '.self::_GenerateGraphSettings($ArrayEachData['Parameters']).'>'.$ArrayEachData['Value'].'</value>';
			}
		
		$XML[] = '</graph>';
		}
	$XML[] = '</graphs>';
	// Graphs - End }

	$XML[] = '</chart>';

	return implode("\n", $XML);
	}

/**
 * Returns the formatted graph settings
 *
 * @param string $ArrayGraphSettings 
 * @return void
 * @author Cem Hurturk
 */
function _GenerateGraphSettings($ArrayGraphSettings)
	{
	$String = '';
	foreach ($ArrayGraphSettings as $Key=>$Value)
		{
		$String .= $Key.'="'.$Value.'" ';
		}
	
	return $String;
	}
} // END class ChartDataGenerator


?>