<?php

// Required modules - Start
Core::LoadObject('users');
// Required modules - End

/**
 * Payment processing, calculating and reporting class
 *
 * @package default
 * @author Cem Hurturk
 * */
class Payments extends Core {

    /**
     * User information array
     *
     * @var array
     * */
    public static $ArrayUser = array();

    /**
     * Log ID of the active period
     *
     * @var integer
     * */
    private static $LogID = 0;

    /**
     * Is payment system enabled
     *
     * @var boolean
     * */
    private static $PaymentSystem = true;

    /**
     * Sets the user information array
     *
     * @param string $ArrayUser 
     * @return void
     * @author Cem Hurturk
     */
    public static function SetUser($ArrayUser) {
        self::$ArrayUser = $ArrayUser;
    }

    /**
     * Checks if payment period exists for the user. If it does not exist, it creates a new one
     *
     * @return void
     * @author Cem Hurturk
     */
    public static function CheckIfPaymentPeriodExists() {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayCriterias = array(
            'RelUserID' => self::$ArrayUser['UserID'],
            array('field' => 'PeriodStartDate', 'operator' => '<=', 'value' => date('Y-m-d')),
            array('field' => 'PeriodEndDate', 'operator' => '>=', 'value' => date('Y-m-d')),
        );
        $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), $ArrayCriterias, array(), 0, 0, 'AND', false, false);

        $TotalFound = count($ArrayReturn);

        // If period log does not exist, create a new one. If exists, return the ID - Start
        if ($TotalFound == 0) {
            self::$LogID = self::CreatePaymentPeriod();
        } else {
            self::$LogID = $ArrayReturn[0]['LogID'];
        }
        // If period log does not exist, create a new one. If exists, return the ID - End

        return;
    }

    public static function NewPaymentPeriod() {

        $ArrayCriterias = array(
            'RelUserID' => self::$ArrayUser['UserID'],
            array('field' => 'PeriodStartDate', 'operator' => '<=', 'value' => date('Y-m-d')),
            array('field' => 'PeriodEndDate', 'operator' => '>=', 'value' => date('Y-m-d')),
        );
        $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), $ArrayCriterias, array("LogID" => "DESC"), 0, 0, 'AND', false, false);

        $TotalFound = count($ArrayReturn);

        // If period log does not exist, create a new one. If exists, return the ID - Start
        if ($TotalFound == 0) {
            self::$LogID = self::CreatePaymentPeriod();
        } else {
            self::$LogID = $ArrayReturn[0]['LogID'];
            self::CloseCurrentPaymentLog();
            self::$LogID = self::CreatePaymentPeriod();
        }
    }

    public static function GetCurrentPaymentPeriodIfExists() {
        $ArrayCriterias = array(
            'RelUserID' => self::$ArrayUser['UserID'],
            array('field' => 'PeriodStartDate', 'operator' => '<=', 'value' => date('Y-m-d')),
            array('field' => 'PeriodEndDate', 'operator' => '>=', 'value' => date('Y-m-d')),
        );
        $ArrayReturn = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), $ArrayCriterias, array("LogID" => "DESC"), 0, 0, 'AND', false, false);

        $TotalFound = count($ArrayReturn);

        // If period log does not exist, create a new one. If exists, return the ID - Start
        if ($TotalFound == 0) {
            return false;
        } else {
            self::$LogID = $ArrayReturn[0]['LogID'];
            return self::$LogID;
        }
    }

    public static function CloseCurrentPaymentLog() {
        //update this log
        $ArrayFieldnValues = array(
            'PeriodEndDate' => date('Y-m-d'),
        );

        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => self::$LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');
    }

    public static function ClosePaymentLog($LogID) {
        //update this log
        $ArrayFieldnValues = array(
            'PeriodEndDate' => date('Y-m-d'),
        );

        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => $LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');
    }

    /**
     * Creates payment period record for the user
     *
     * @return integer
     * @author Cem Hurturk
     */
    private static function CreatePaymentPeriod() {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $GroupID = self::$ArrayUser['RelUserGroupID'];
        $UserGroup = UserGroups::RetrieveUserGroup($GroupID);
        $SubscriptionType = $UserGroup["SubscriptionType"];

        $periodToAdd = $SubscriptionType == 'Annually' ? ' +1 year' : ' +1 month';

        $CurrentDate = date('Y-m-d');
        $PeriodStartDate = '';
        $TMPPrevPeriodStart = (self::$ArrayUser['UserSince'] == '0000-00-00 00:00:00' ? date('Y-m-d') : self::$ArrayUser['UserSince']);

        for ($TMPCounter = 1; $TMPCounter <= 100000; $TMPCounter++) {
            $TMPNextPeriodEnd = date('Y-m-d', strtotime($TMPPrevPeriodStart . $periodToAdd));

            if ((strtotime($CurrentDate) >= strtotime($TMPNextPeriodStart)) && (strtotime($CurrentDate) <= strtotime($TMPNextPeriodEnd))) {
                $PeriodStartDate = $TMPPrevPeriodStart;
                break;
            }

            $TMPNextPeriodStart = date('Y-m-d', strtotime($TMPPrevPeriodStart . $periodToAdd));
            $TMPPrevPeriodStart = $TMPNextPeriodStart;
        }

        $ArrayFieldnValues = array(
            'LogID' => '',
            'RelUserID' => self::$ArrayUser['UserID'],
            'PeriodStartDate' => $PeriodStartDate,
            'PeriodEndDate' => date('Y-m-d', strtotime($PeriodStartDate . $periodToAdd)),
            'CampaignsSent' => 0,
            'CampaignsTotalRecipients' => 0,
            'CampaignsTotalDelivered' => 0,
            'AutoRespondersSent' => 0,
            'AutoRespondersDelivered' => 0,
            'DesignPreviewRequests' => 0,
            'ChargePerCampaignSent' => 0,
            'ChargeAutoResponderPeriod' => 0,
            'ChargeDesignPreviewPeriod' => 0,
            'ChargeSystemPeriod' => 0,
            'ChargeTotalCampaignRecipients' => 0,
            'ChargeTotalAutoRespondersSent' => 0,
            'Discount' => 0,
            'Tax' => 0,
            'TotalAmount' => 0,
            'PaymentStatus' => 'NA',
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'users_payment_log');
        $NewLogID = Database::$Interface->GetLastInsertID();

        $invoice_serial = date("Y", strtotime($PeriodStartDate)) . "" . str_pad($NewLogID, 4, '0', STR_PAD_LEFT);

        $ArrayFieldnValues2 = array(
            'InvoiceSerial' => $invoice_serial,
        );

        Database::$Interface->UpdateRows($ArrayFieldnValues2, array('LogID' => $NewLogID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        return $NewLogID;
    }

    /**
     * Checks if payment system is enabled or not
     *
     * @return boolean
     * @author Cem Hurturk
     */
    public static function IsPaymentSystemEnabled($IgnoreUserGroupPrivs = false) {
        self::$PaymentSystem = true;

        if ((self::$PaymentSystem == true) && ($IgnoreUserGroupPrivs == false)) {
            if (self::$ArrayUser['GroupInformation']['PaymentSystem'] == 'Enabled') {
                return true;
            } else {
                return false;
            }
        } elseif ((self::$PaymentSystem == true) && ($IgnoreUserGroupPrivs == true)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Logs the sent campaign
     *
     * @param string $CampaignID 
     * @return void
     * @author Cem Hurturk
     */
    public static function CampaignSent($CampaignID) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayFieldnValues = array(
            'CampaignsSent' => array(false, 'CampaignsSent + 1'),
        );
        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => self::$LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        return;
    }

    /**
     * Logs the sent recipient of the campaign
     *
     * @param string $CampaignID 
     * @return void
     * @author Cem Hurturk
     */
    public static function CampaignRecipientSent($CampaignID, $SendStatus) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        if ($SendStatus == 'Total') {
            $ArrayFieldnValues = array(
                'CampaignsTotalRecipients' => array(false, 'CampaignsTotalRecipients + 1'),
            );
        } elseif ($SendStatus == 'Sent') {
            $ArrayFieldnValues = array(
                'CampaignsTotalDelivered' => array(false, 'CampaignsTotalDelivered + 1'),
            );
        } else {
            return false;
        }
        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => self::$LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        return;
    }

    /**
     * Logs the sent recipient of the auto responder
     *
     * @param string $AutoResponderID 
     * @return void
     * @author Cem Hurturk
     */
    public static function AutoResponderRecipientSent($AutoResponderID, $SendStatus) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        if ($SendStatus == 'Total') {
            $ArrayFieldnValues = array(
                'AutoRespondersSent' => array(false, 'AutoRespondersSent + 1'),
            );
        } elseif ($SendStatus == 'Sent') {
            $ArrayFieldnValues = array(
                'AutoRespondersDelivered' => array(false, 'AutoRespondersDelivered + 1'),
            );
        } else {
            return false;
        }
        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => self::$LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        return;
    }

    /**
     * Logs the design preview request
     *
     * @param string $JobID
     * @return void
     * @author Cem Hurturk
     */
    public static function DesignPreviewRequest($JobID) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayFieldnValues = array(
            'DesignPreviewRequests' => array(false, 'DesignPreviewRequests + 1'),
        );
        Database::$Interface->UpdateRows($ArrayFieldnValues, array('LogID' => self::$LogID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        return;
    }

    /**
     * Returns the list of payment periods
     *
     * @param string $UserID
     * @return array
     * @author Cem Hurturk
     */
    public static function GetPaymentPeriods($PaymentStatus = '') {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayCriterias = array(
            'RelUserID' => self::$ArrayUser['UserID'],
        );

        if ($PaymentStatus != '') {
            $ArrayCriterias['PaymentStatus'] = $PaymentStatus;
        }

        $ArrayPaymentPeriods = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), $ArrayCriterias, array('PeriodEndDate' => 'DESC'), 0, 0, 'AND', false, false);

        return $ArrayPaymentPeriods;
    }

    /**
     * Returns the list of payment periods
     *
     * @param string $UserID
     * @return array
     * @author Cem Hurturk
     */
    public static function GetPaymentPeriod($LogID) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayCriterias = array(
            'LogID' => $LogID,
            'RelUserID' => self::$ArrayUser['UserID'],
        );
        $ArrayPaymentPeriod = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), $ArrayCriterias, array(), 0, 0, 'AND', false, false);

        if (count($ArrayPaymentPeriod) == 0) {
            return false;
        }

        $ArrayPaymentPeriod = $ArrayPaymentPeriod[0];

        return $ArrayPaymentPeriod;
    }

    /**
     * Calculates the total amount of a period
     *
     * @return double
     * @author Cem Hurturk
     */
    public static function CalculateTotalAmountOfPaymentPeriod($ArrayPaymentPeriod, $IncludeTax = false) {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayTotals = array();

        // CampaignsSent
        $ArrayTotals['ChargePerCampaignSent'] = ($ArrayPaymentPeriod['CampaignsSent'] * self::$ArrayUser['GroupInformation']['PaymentCampaignsPerCampaignCost']);

        // CampaignsTotalRecipients
        if (self::$ArrayUser['GroupInformation']['CreditSystem'] == 'Disabled') {
            $ArrayTotals['ChargeTotalCampaignRecipients'] = self::CalculateFluidPricing($ArrayPaymentPeriod['CampaignsTotalRecipients']);
        }

        // AutoRespondersSent
        if (self::$ArrayUser['GroupInformation']['CreditSystem'] == 'Disabled') {
            $ArrayTotals['ChargeTotalAutoRespondersSent'] = self::CalculateFluidPricing($ArrayPaymentPeriod['AutoRespondersSent']);
        }

        // DesignPreviewRequests
        $ArrayTotals['ChargeTotalDesignPrevRequests'] = ($ArrayPaymentPeriod['DesignPreviewRequests'] * self::$ArrayUser['GroupInformation']['PaymentDesignPrevChargePerReq']);

        // ChargeAutoResponderPeriod
        $ArrayTotals['ChargeAutoResponderPeriod'] = self::$ArrayUser['GroupInformation']['PaymentAutoRespondersChargeAmount'];

        // ChargeDesignPreviewPeriod
        $ArrayTotals['ChargeDesignPreviewPeriod'] = self::$ArrayUser['GroupInformation']['PaymentDesignPrevChargeAmount'];

        // ChargeSystemPeriod
        $ArrayTotals['ChargeSystemPeriod'] = self::$ArrayUser['GroupInformation']['PaymentSystemChargeAmount'];

        // Discount
        $ArrayTotals['Discount'] = $ArrayPaymentPeriod['Discount'];

        // Tax
        $Tax = 0;
        $Total = 0;

        foreach ($ArrayTotals as $Key => $Value) {
            if ($Key != 'Discount') {
                $Total += $Value;
            } else {
                $Total -= $Value;
            }
        }

        $ArrayTotals['SubTotal'] = $Total;
        $ArrayTotals['Tax'] = ($IncludeTax == true ? (($ArrayTotals['SubTotal'] / 100) * PAYMENT_TAX_PERCENT) : 0);
        $ArrayTotals['TotalAmount'] = $ArrayTotals['SubTotal'] + $ArrayTotals['Tax'];

        return $ArrayTotals;
    }

    /**
     * Calculates the total cost of a sent amount based on fluid pricing
     *
     * @param string $Amount 
     * @param string $FluidPricing 
     * @return void
     * @author Cem Hurturk
     */
    public function CalculateFluidPricing($Amount) {
        $ArrayFluidPricing = explode("\n", self::$ArrayUser['GroupInformation']['PaymentPricingRange']);
        $ArrayPricingRange = array();
        $TotalCost = 0;

        if ($Amount == 0) {
            return 0;
        }

        for ($TMPCounter = 0; $TMPCounter < count($ArrayFluidPricing); $TMPCounter++) {
            $TMPArray = explode('|', $ArrayFluidPricing[$TMPCounter]);

            if ($TMPArray[0] == -1) {
                $TotalCost += $Amount * $TMPArray[1];

                break;
            } else {
                $Amount = $Amount - $TMPArray[0];

                if ($Amount < 0) {
                    $LoopAmount = $Amount + $TMPArray[0];
                    $Amount = 0;
                } else {
                    $LoopAmount = $TMPArray[0];
                }

                $TotalCost += $TMPArray[1] * $LoopAmount;
            }
        }

        return $TotalCost;
    }

    /**
     * Retrieves the log information
     *
     * @return array|boolean
     * @author Cem Hurturk
     */
    public static function GetLog($LogID = '') {
        // if (self::IsPaymentSystemEnabled() == false)
        // 	{
        // 	return false;
        // 	}

        $ArrayLog = Database::$Interface->GetRows(array('*'), array(MYSQL_TABLE_PREFIX . 'users_payment_log'), array('LogID' => ($LogID == '' ? self::$LogID : $LogID)), array(), 0, 0, 'AND', false, false);
        $ArrayLog = $ArrayLog[0];

        return $ArrayLog;
    }

    /**
     * Updates payment period information
     *
     * @return boolean
     * @author Mert Hurturk
     * */
    public static function Update($ArrayFieldAndValues, $ArrayCriterias) {
        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'users_payment_log');
        return true;
    }

    /**
     * Formats payment period values
     *
     * @param string $ArrayPaymentPeriod 
     * @return array
     * @author Cem Hurturk
     */
    public static function FormatPaymentValues($ArrayPaymentPeriod, $DateFormat, $NALanguageString = '') {
        foreach ($ArrayPaymentPeriod as $EachKey => $EachValue) {
            if (($EachKey == 'PeriodStartDate') || ($EachKey == 'PeriodEndDate') || ($EachKey == 'PaymentStatusDate')) {
                $ArrayPaymentPeriod[$EachKey] = ($EachValue != '0000-00-00' ? date($DateFormat, strtotime($EachValue)) : $NALanguageString);
            } elseif (($EachKey == 'CampaignsSent') || ($EachKey == 'CampaignsTotalRecipients') || ($EachKey == 'CampaignsTotalDelivered') || ($EachKey == 'AutoRespondersSent') || ($EachKey == 'AutoRespondersDelivered') || ($EachKey == 'DesignPreviewRequests')) {
                $ArrayPaymentPeriod[$EachKey] = number_format($EachValue, 0);
            } elseif (($EachKey == 'ChargePerCampaignSent') || ($EachKey == 'ChargeAutoResponderPeriod') || ($EachKey == 'ChargeDesignPreviewPeriod') || ($EachKey == 'ChargeSystemPeriod') || ($EachKey == 'ChargeTotalCampaignRecipients') || ($EachKey == 'ChargeTotalAutoRespondersSent') || ($EachKey == 'ChargeTotalDesignPrevRequests') || ($EachKey == 'Tax') || ($EachKey == 'TotalAmount')) {
                // $ArrayPaymentPeriod[$EachKey] = number_format($EachValue, 2).' '.PAYMENT_CURRENCY;
                $ArrayPaymentPeriod[$EachKey] = Core::FormatCurrency($EachValue);
            }
        }

        return $ArrayPaymentPeriod;
    }

    /**
     * Retrieves users with payment stats
     *
     * @param string $PaymentStatus Payment status
     * @return void
     * @author Mert Hurturk
     */
    public static function RetrieveUsersWithPaymentStatus($PaymentStatus) {
        $SQLQuery = "SELECT RelUserID,PaymentStatus,SUM(TotalAmount) AS UserTotalAmount FROM " . MYSQL_TABLE_PREFIX . "users_payment_log WHERE " . ($PaymentStatus == 'Unpaid' ? "(PaymentStatus='Unpaid' OR PaymentStatus='NA')" : "PaymentStatus='" . mysql_real_escape_string($PaymentStatus) . "'") . " GROUP BY RelUserID ORDER BY UserTotalAmount DESC";
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);
        $ArrayPaymentLogs = array();
        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayPaymentLogs[] = $EachRow;
        }
        foreach ($ArrayPaymentLogs as &$EachLog) {
            $EachLog['UserInformation'] = Users::RetrieveUser(array('UserID', 'EmailAddress', 'FirstName', 'LastName'), array('UserID' => $EachLog['RelUserID']), false);
            $EachLog['UserTotalAmountFormatted'] = Core::FormatCurrency($EachLog['UserTotalAmount']);
        }
        return $ArrayPaymentLogs;
    }

    /**
     * Retrieves all time receivable periods
     *
     * @return void
     * @author Mert Hurturk
     * */
    public static function RetrieveAllTimeReceivables() {
        Core::LoadObject('users');

        $SQLQuery = 'SELECT *, SUM(TotalAmount) AS SumTotalAmount FROM ' . MYSQL_TABLE_PREFIX . 'users_payment_log WHERE PaymentStatus != "Paid" GROUP BY RelUserID ORDER BY SumTotalAmount DESC';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        $ArrayReceivables = array();

        while ($EachRow = mysql_fetch_assoc($ResultSet)) {
            $ArrayReceivables[] = array(
                'PaymentPeriodInformation' => $EachRow,
                'UserInformation' => Users::RetrieveUser(array('*'), array('UserID' => $EachRow['RelUserID']), false)
            );
        }

        return $ArrayReceivables;
    }

    /**
     * Deletes payment logs of a user
     *
     * @param string $UserID 
     * @return void
     * @author Cem Hurturk
     */
    public static function DeletePaymentLogs($UserID) {
        Database::$Interface->DeleteRows(array('RelUserID' => $UserID), MYSQL_TABLE_PREFIX . 'users_payment_log');

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Delete.PaymentLogs', array($UserID));
        // Plug-in hook - End
    }

    public static function CheckAvailableCredits($ArrayUser) {
        if (($ArrayUser['GroupInformation']['PaymentSystem'] == 'Enabled') && ($ArrayUser['GroupInformation']['CreditSystem'] == 'Enabled') && ($ArrayUser['AvailableCredits'] == 0)) {
            return array(false, $ArrayUser['AvailableCredits']);
        } else {
            return array(true, '');
        }
    }

    public static function DeductCredits($ArrayUser, $UpdateUserVariable = false) {
        if ($ArrayUser['GroupInformation']['PaymentSystem'] == 'Enabled' && ($ArrayUser['GroupInformation']['CreditSystem'] == 'Enabled') && ($ArrayUser['AvailableCredits'] > 0)) {
            Users::UpdateUser(array('AvailableCredits' => array(true, '`AvailableCredits` - 1')), array('UserID' => $ArrayUser['UserID']));
            $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayUser['UserID']));
        }
        return $ArrayUser;
    }

    public static function CreateCreditsPurchaseLog($ArrayUser, $PurchasedCredits, $GrossAmount, $Tax, $Discount, $NetAmount, $Gateway) {
        $ArrayFieldnValues = array(
            'LogID' => '',
            'RelUserID' => $ArrayUser['UserID'],
            'PurchaseDate' => date('Y-m-d H:i:s'),
            'PurchasedCredits' => $PurchasedCredits,
            'GrossAmount' => $GrossAmount,
            'Tax' => $Tax,
            'Discount' => $Discount,
            'NetAmount' => $NetAmount,
            'PaymentStatus' => 'Unpaid',
            'PaymentStatusDate' => '0000-00-00 00:00:00',
            'PaidGateway' => $Gateway,
            'GatewayTransactionID' => '',
        );
        Database::$Interface->InsertRow($ArrayFieldnValues, MYSQL_TABLE_PREFIX . 'users_credits_purchase_log');
        $NewLogID = Database::$Interface->GetLastInsertID();

        return $NewLogID;
    }

    public static function RetrieveCreditsPurchaseLog($LogID) {
        $SQLQuery = 'SELECT * FROM ' . MYSQL_TABLE_PREFIX . 'users_credits_purchase_log WHERE LogID="' . mysql_real_escape_string($LogID) . '"';
        $ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

        if (mysql_num_rows($ResultSet) == 0) {
            return false;
        } else {
            return mysql_fetch_assoc($ResultSet);
        }
    }

    public static function UpdateCreditPurchaseLog($ArrayFieldAndValues, $ArrayCriterias) {
        Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX . 'users_credits_purchase_log');
        return true;
    }

}

// END class Payments
?>