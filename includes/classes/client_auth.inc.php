<?php
/**
 * ClientAuth class
 *
 * This class holds all client authorization related functions
 * @package Oempro
 * @author Octeth
 **/
class ClientAuth extends Core
{
/**
 * Logins client
 *
 * @param integer $ClientID Client id
 * @param string $Username Username
 * @param string $Password Client password
 * @return void
 * @author Cem Hurturk
 */
function Login($ClientID, $Username, $Password)
	{
	$_SESSION[SESSION_NAME]['ClientLogin'] = md5($ClientID).md5($Username).md5($Password);
	
	return;
	}

/**
 * Logs the client out
 *
 * @return void
 * @author Cem Hurturk
 */
function Logout()
	{
	unset($_SESSION[SESSION_NAME]['ClientLogin']);
	return;
	}

/**
 * Checks if client is logged in or not.
 *
 * @param string $LoggedInRedirectURL If set, logged in client will be redirected to this URL
 * @param string $LoggedOutRedirectURL If set, not logged in client will be redirected to this URL
 * @return booelan
 * @author Cem Hurturk
 */
function IsLoggedIn($LoggedInRedirectURL, $LoggedOutRedirectURL)
	{
	if (isset($_SESSION[SESSION_NAME]['ClientLogin']) == true)
		{
		if ($_SESSION[SESSION_NAME]['ClientLogin'] != '')
			{
			// Session exists. Check if the login information is valid.
			$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."clients WHERE CONCAT(MD5(ClientID), MD5(ClientUsername), MD5(ClientPassword)) = '".mysql_real_escape_string($_SESSION[SESSION_NAME]['ClientLogin'])."'";
			$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

			if (mysql_num_rows($ResultSet) == 1)
				{
				// Login information is correct
				if ($LoggedInRedirectURL != false)
					{
					header('Location: '.$LoggedInRedirectURL);
					exit;
					}
				else
					{
					return true;
					}
				}
			else
				{
				// Login information is incorrect
				if ($LoggedOutRedirectURL != false)
					{
					header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
					exit;
					}
				else
					{
					return false;
					}
				}
			}
		else
			{
			// Session does not exist. Client not logged in.
			if ($LoggedOutRedirectURL != false)
				{
				header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
				exit;
				}
			else
				{
				return false;
				}
			}
		}
	else
		{
		// Session does not exist. Client not logged in.
		if ($LoggedOutRedirectURL != false)
			{
			header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
			exit;
			}
		else
			{
			return false;
			}
		}
	}

} // END class Auth
?>