<?php
/**
 * Themes class
 *
 * This class holds all user interface theme related functions
 * @package Oempro
 * @author Octeth
 **/
class ThemeEngine extends Core
{	
/**
 * Retrieves a specific theme
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return void
 * @author Cem Hurturk
 */
function RetrieveTheme($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'themes');
	$ArrayCriterias		= $ArrayCriterias; 
	$ArrayOrder			= array();
	$ArrayTheme = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	if (count($ArrayTheme) != 1) { return false; } // if there are no users, return false 

	$ArrayTheme = $ArrayTheme[0];
	
	// Setup theme css settings - Start {
    $TMPArraySettings = explode("\n", $ArrayTheme['ThemeSettings']);
    $ArraySettings = array();
    foreach ($TMPArraySettings as $EachSetting)
        {
        if ($EachSetting != '')
            {
            $EachSetting = trim($EachSetting);
            $ArraySettingParams = explode("||||", $EachSetting);
            $ArraySettings[$ArraySettingParams[0]]    = $ArraySettingParams[1];
            }
        }
	// Setup theme css settings - End }
	$ArrayTheme['ArrayThemeSettings'] = $ArraySettings;
	return $ArrayTheme;
	}

/**
 * Retrieves the list of themes
 *
 * @param string $ArrayReturnFields 
 * @param string $ArrayCriterias 
 * @return void
 * @author Cem Hurturk
 */
function RetrieveThemes($ArrayReturnFields, $ArrayCriterias)
	{
	if (count($ArrayReturnFields) < 1) { $ArrayReturnFields = array('*'); }

	$ArrayFields		= array(implode(', ', $ArrayReturnFields));
	$ArrayFromTables	= array(MYSQL_TABLE_PREFIX.'themes');
	$ArrayCriterias		= $ArrayCriterias; 
	$ArrayOrder			= array();
	$ArrayThemes = Database::$Interface->GetRows($ArrayFields, $ArrayFromTables, $ArrayCriterias, $ArrayOrder);

	return $ArrayThemes;
	}

/**
 * Retrieves CSS settings
 *
 * @return array
 * @author Cem Hurturk
 **/
function LoadCSSSettings($Template, $ReturnVisibleOnly = false)
    {
    $CSS = file_get_contents(APP_PATH.'/templates/'.$Template.'/styles/ui.css');

        $Pattern = '/THEME SETTINGS - START(.*)THEME SETTINGS - END/is';
    preg_match_all($Pattern, $CSS, $ArrayMatches);
    
    $TMPArraySettings = explode("\n", $ArrayMatches[1][0]);

    $ArraySettings = array();
    
    foreach ($TMPArraySettings as $EachSetting)
        {
        if ($EachSetting != '')
            {
            $EachSetting = trim($EachSetting);
            $ArraySettingParams = explode("\t", $EachSetting);

			if (($ReturnVisibleOnly == true) && (preg_match('/^\*/i', $ArraySettingParams[0]) == false))
				{
				continue;
				}

			// If there's asterick (*) at the beginning of the tag (that means it's visible to the end user in theme settings menu in admin area), remove it - Start
			$ArraySettingParams[0] = preg_replace('/^\*/', '', $ArraySettingParams[0]);
			// If there's asterick (*) at the beginning of the tag (that means it's visible to the end user in theme settings menu in admin area), remove it - End

            $ArraySettings[]    = array(
                                        'Description'        => $ArraySettingParams[1],
                                        'Tag'                => $ArraySettingParams[0],
                                        'Default'            => $ArraySettingParams[2],
                                        );
            }
        }

    return $ArraySettings;
    }

/**
 * Detects installed templates in Oempro
 *
 * @return array
 * @author Cem Hurturk
 **/
public static function DetectTemplates()
    {
    $ArrayTemplates = array();

	$TemplatePath = APP_PATH.'/templates';

    if (is_dir($TemplatePath))
        {
        if ($DirHandler = opendir($TemplatePath))
            {
            $TMPCounter = 0;
            while (($EachFile = readdir($DirHandler)) !== false)
                {
                if (($EachFile != '.') && ($EachFile != '..') && (filetype($TemplatePath.'/'.$EachFile) == 'dir'))
                    {
                    $FileContents = file_get_contents($TemplatePath.'/'.$EachFile.'/info.txt');
                    if (preg_match('/Name: (.*)\nDescription: (.*)/i', $FileContents, $ArrayMatches) == true)
                        {
                        if (($ArrayMatches[1] != ''))
                            {
                            $ArrayTemplates[$TMPCounter]['Code'] = $EachFile;
                            $ArrayTemplates[$TMPCounter]['Name'] = $ArrayMatches[1];
                            $ArrayTemplates[$TMPCounter]['Description'] = $ArrayMatches[2];

                            $TMPCounter++;
                            }
                        }
                    }
                }
            }
        closedir($DirHandler);
        }
    return $ArrayTemplates;
    }

/**
 * Creates a new theme
 *
 * @param array $ArrayFieldAndValues
 * @return boolean|integer
 * @author Cem Hurturk
 **/
public static function Create($ArrayFieldAndValues)
	{
	Database::$Interface->InsertRow($ArrayFieldAndValues, MYSQL_TABLE_PREFIX.'themes');
	
	$NewThemeID = Database::$Interface->GetLastInsertID();
	return $NewThemeID;
	}

/**
 * Updates theme information
 *
 * @param array $ArrayFieldAndValues Values of new client information
 * @param array $ArrayCriterias Criterias to be matched while updating client
 * @return boolean
 * @author Mert Hurturk
 **/
public static function Update($ArrayFieldAndValues, $ArrayCriterias)
	{
	Database::$Interface->UpdateRows($ArrayFieldAndValues, $ArrayCriterias, MYSQL_TABLE_PREFIX.'themes');

    $CSSCacheDir = APP_PATH.'/data/css/';
    $CSSCacheFile = $CSSCacheDir.'css_cache_'.$ArrayCriterias['ThemeID'].'.css';
    if (file_exists($CSSCacheFile)) unlink($CSSCacheDir.'css_cache_'.$ArrayCriterias['ThemeID'].'.css');
    
	return true;
	}

/**
 * Deletes themes
 *
 * @param array $ArrayThemeIDs
 * @return boolean
 * @author Cem Hurturk
 **/
public static function Delete($ArrayThemeIDs)
	{
	foreach ($ArrayThemeIDs as $EachID)
		{
		Database::$Interface->DeleteRows(array('ThemeID'=>$EachID), MYSQL_TABLE_PREFIX.'themes');
		}

	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Delete.Theme', array($ArrayThemeIDs));
	// Plug-in hook - End
	}


} // END class ThemeEngine


?>