<?php
/**
 * UserAuth class
 *
 * This class holds all user authorization related functions
 * @package Oempro
 * @author Octeth
 **/
class UserAuth extends Core
{
/**
 * Logins user
 *
 * @param integer $UserID User id
 * @param string $Username Username
 * @param string $Password User password
 * @param string $PrivilegeType '', 'Full', 'Default' If admin is switched to this user, the privilege type can be selected
 * @return void
 * @author Cem Hurturk
 */
function Login($UserID, $Username, $Password, $PrivilegeType = '')
	{
	$_SESSION[SESSION_NAME]['UserLogin']			= md5($UserID).md5($Username).$Password;
	if (($PrivilegeType == '') || ($PrivilegeType == 'Default'))
		{
		unset($_SESSION[SESSION_NAME]['UserLoginPrivType']);
		}
	elseif ($PrivilegeType == 'Full')
		{
		$_SESSION[SESSION_NAME]['UserLoginPrivType']	= 'Full';
		}

	return;
	}

/**
 * Logs the user out
 *
 * @return void
 * @author Cem Hurturk
 */
function Logout()
	{
	unset($_SESSION[SESSION_NAME]['UserLogin']);
	unset($_SESSION[SESSION_NAME]['UserLoginPrivType']);
	return;
	}

/**
 * Checks if user is logged in or not.
 *
 * @param string $LoggedInRedirectURL If set, logged in user will be redirected to this URL
 * @param string $LoggedOutRedirectURL If set, not logged in user will be redirected to this URL
 * @return booelan
 * @author Cem Hurturk
 */
function IsLoggedIn($LoggedInRedirectURL, $LoggedOutRedirectURL)
	{
	if (isset($_SESSION[SESSION_NAME]['UserLogin']) == true)
		{
		if ($_SESSION[SESSION_NAME]['UserLogin'] != '')
			{
			// Session exists. Check if the login information is valid.
			$SQLQuery = "SELECT * FROM ".MYSQL_TABLE_PREFIX."users WHERE AccountStatus = 'Enabled' AND CONCAT(MD5(UserID), MD5(Username), Password) = '".mysql_real_escape_string($_SESSION[SESSION_NAME]['UserLogin'])."'";
			$ResultSet = Database::$Interface->ExecuteQuery($SQLQuery);

			if (mysql_num_rows($ResultSet) == 1)
				{
				// Login information is correct
				if ($LoggedInRedirectURL != false)
					{
					header('Location: '.$LoggedInRedirectURL);
					exit;
					}
				else
					{
					return true;
					}
				}
			else
				{
				// Login information is incorrect
				if ($LoggedOutRedirectURL != false)
					{
					header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
					exit;
					}
				else
					{
					return false;
					}
				}
			}
		else
			{
			// Session does not exist. User not logged in.
			if ($LoggedOutRedirectURL != false)
				{
				header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
				exit;
				}
			else
				{
				return false;
				}
			}
		}
	else
		{
		// Session does not exist. User not logged in.
		if ($LoggedOutRedirectURL != false)
			{
			header('Location: '.$LoggedOutRedirectURL.(REDIRECT_ON_LOGIN == true ? '?RedirectURL='.rawurlencode($_SERVER['REQUEST_URI']) : ''));
			exit;
			}
		else
			{
			return false;
			}
		}
	}

} // END class Auth
?>