<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Retrieve email information - End
// Field validations - End

// Retrieve the list of previously created preview requests - Start
$ArrayPreviews = Emails::RetrieveDesignPreviewJobs(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'RelEmailID' => $ArrayEmail['EmailID']));
// Retrieve the list of previously created preview requests - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'PreviewList'			=> $ArrayPreviews,
					);
// Return results - End
?>