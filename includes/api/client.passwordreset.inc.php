<?php
// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'reset_password');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'clientid'	=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Validate if such an client id exists in the database - Start
$ArrayCriterias = array(
						'MD5(`ClientID`)'		=> $ArrayAPIData['clientid'],
						);
$ArrayClient = Clients::RetrieveClient(array('*'), $ArrayCriterias);

if ($ArrayClient == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Validate if such an client id exists in the database - End

// Reset client password - Start
$NewPassword = Core::GenerateRandomString(5);

	$ArrayFieldnValues	= array(
								'ClientPassword'		=> md5(OEMPRO_PASSWORD_SALT.$NewPassword.OEMPRO_PASSWORD_SALT),
								);
	$ArrayCriterias		= array(
								'ClientID'		=> $ArrayClient['ClientID'],
								);
Clients::Update($ArrayFieldnValues, $ArrayCriterias);
// Reset client password - End

// Send password reminder email to the client - Start
$Username = $ArrayClient['ClientUsername'];
O_Email_Sender_ForAdmin::send(
	O_Email_Factory::passwordReset($ArrayClient['ClientEmailAddress'], $Username, $NewPassword)
);
// Send password reminder email to the client - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End
?>