<?php
$folderId = isset($ArrayAPIData['folderid']) ? (int) $ArrayAPIData['folderid'] : 0;

$mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');
$mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');

$folders = $mediaLibraryFolderMapper->findByFolderId($folderId, $ArrayUserInformation['UserID']);
$files = $mediaLibraryFileMapper->findByFolderId($folderId, $ArrayUserInformation['UserID']);

$folders = $folders == false ? array() : $folders;
$files = $files == false ? array() : $files;

$foldersToOutput = array();
$filesToOutput = array();

foreach ($folders as $eachFolder) {
	$foldersToOutput[] = array(
		'ID' => $eachFolder->getId(),
		'Name' => $eachFolder->getName(),
		'ParentFolderID' => $eachFolder->getFolderId()
	);
}
unset($folders);

foreach ($files as $eachFile) {
	$filesToOutput[] = array(
		'ID' => $eachFile->getId(),
		'Name' => $eachFile->getName(),
		'URL' => $eachFile->getUrl()
	);
}
unset($files);

$ArrayOutput = array(
	'Success'		=> true,
	'ErrorCode'		=> 0,
	'ErrorText'		=> '',
	'Folders'		=> $foldersToOutput,
	'Files'			=> $filesToOutput
);