<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'mediadata'					=> 1,
							'mediatype'					=> 2,
							'mediasize'					=> 3,
							'medianame'					=> 5,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('media_library');
// Load other modules - End

// Field validations - Start
// Check if media file size is in allowed limits - Start
if (strlen(base64_decode($ArrayAPIData['mediadata'])) > MEDIA_MAX_FILESIZE)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4,
						);
	throw new Exception('');
	}
// Check if media file size is in allowed limits - End
// Field validations - End

// Create New Media Library Entry - Start
$ArrayFieldAndValues = array(
							'MediaID'			=> '',
							'RelOwnerUserID'	=> $ArrayUserInformation['UserID'],
							'MediaData'			=> (MEDIA_UPLOAD_METHOD == 'database' ? $ArrayAPIData['mediadata'] : (MEDIA_UPLOAD_METHOD == 'file' ? 'file' : (MEDIA_UPLOAD_METHOD == 's3' ? 's3' : $ArrayAPIData['mediadata']))),
							'MediaType'			=> $ArrayAPIData['mediatype'],
							'MediaSize'			=> strlen(base64_decode($ArrayAPIData['mediadata'])),
							'MediaName'			=> $ArrayAPIData['medianame'],
							'Tags'				=> '',
							);
$NewMediaID = MediaLibrary::UploadMedia($ArrayFieldAndValues, $ArrayAPIData['mediadata'], $ArrayUserInformation['UserID']);
// Create New Media Library Entry - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'MediaLibrary.Upload.Post', array($NewMediaID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'MediaID'			=> $NewMediaID,
					);
// Return results - End
?>