<?php
// Load other modules - Start
Core::LoadObject('campaigns');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'		=> 1,
							);

if ($ArrayAPIData['scheduletype'] == 'Future')
	{
	$ArrayRequiredFields['senddate']		= 6;
	$ArrayRequiredFields['sendtime']		= 7;
	$ArrayRequiredFields['sendtimezone']	= 13;
	}
elseif ($ArrayAPIData['scheduletype'] == 'Recursive')
	{
	if (($ArrayAPIData['schedulerecdaysofweek'] == '') && ($ArrayAPIData['schedulerecdaysofmonth'] == ''))
		{
		$ArrayRequiredFields['schedulerecdaysofweek']		= 8;
		$ArrayRequiredFields['schedulerecdaysofmonth']		= 8;
		}
	$ArrayRequiredFields['schedulerecmonths']			= 9;
	$ArrayRequiredFields['schedulerechours']			= 10;
	$ArrayRequiredFields['schedulerecminutes']			= 11;
	$ArrayRequiredFields['schedulerecsendmaxinstance']	= 12;
	$ArrayRequiredFields['sendtimezone']				= 13;
	}
	
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Be sure that campaign belongs to the logged in user - Start
$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'CampaignID' => $ArrayAPIData['campaignid']));

if ($ArrayCampaign == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Be sure that campaign belongs to the logged in user - End

// Verify campaign status - Start
if (isset($ArrayAPIData['campaignstatus']) && ($ArrayAPIData['campaignstatus'] != 'Draft') && ($ArrayAPIData['campaignstatus'] != 'Ready') && ($ArrayAPIData['campaignstatus'] != 'Sending') && ($ArrayAPIData['campaignstatus'] != 'Paused') && ($ArrayAPIData['campaignstatus'] != 'Pending Approval') && ($ArrayAPIData['campaignstatus'] != 'Sent') && ($ArrayAPIData['campaignstatus'] != 'Failed'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
// Verify campaign status - End

// Verify email ID and check if it belongs to the owner user - Start
if (isset($ArrayAPIData['relemailid']) && ($ArrayAPIData['relemailid'] != ''))
	{
	// Load other modules - Start
	Core::LoadObject('emails');
	// Load other modules - End

	$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['relemailid'], 'RelUserID' => $ArrayUserInformation['UserID']));

	if ($ArrayEmail == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 4
							);
		throw new Exception('');
		}
	}
// Verify email ID and check if it belongs to the owner user - End

// Verify schedule type, date, time and timezone - Start
if ((isset($ArrayAPIData['scheduletype'])) && ($ArrayAPIData['scheduletype'] != 'Not Scheduled') && ($ArrayAPIData['scheduletype'] != 'Immediate') && ($ArrayAPIData['scheduletype'] != 'Future') && ($ArrayAPIData['scheduletype'] != 'Recursive'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5
						);
	throw new Exception('');
	}
// Verify schedule type, date, time and timezone - End
// Field validations - End

// Update campaign information - Start
	$ArrayFieldnValues = array();

	if ($ArrayAPIData['campaignstatus'] != '')
		{
		$ArrayFieldnValues['CampaignStatus'] = $ArrayAPIData['campaignstatus'];
		}
	if ($ArrayAPIData['campaignname'] != '')
		{
		$ArrayFieldnValues['CampaignName'] = $ArrayAPIData['campaignname'];
		}
	if ($ArrayAPIData['relemailid'] != '')
		{
		$ArrayFieldnValues['RelEmailID'] = $ArrayAPIData['relemailid'];
		}
	if ($ArrayAPIData['scheduletype'] != '')
		{
		$ArrayFieldnValues['ScheduleType'] = $ArrayAPIData['scheduletype'];
		}
	if ($ArrayAPIData['senddate'] != '')
		{
		$ArrayFieldnValues['SendDate'] = $ArrayAPIData['senddate'];
		}
	if ($ArrayAPIData['sendtime'] != '')
		{
		$ArrayFieldnValues['SendTime'] = $ArrayAPIData['sendtime'];
		}
	if ($ArrayAPIData['sendtimezone'] != '')
		{
		$ArrayFieldnValues['SendTimeZone'] = $ArrayAPIData['sendtimezone'];
		}
	if (isset($ArrayAPIData['schedulerecdaysofweek']))
		{
		$ArrayFieldnValues['ScheduleRecDaysOfWeek'] = $ArrayAPIData['schedulerecdaysofweek'];
		}
	if (isset($ArrayAPIData['schedulerecdaysofmonth']))
		{
		$ArrayFieldnValues['ScheduleRecDaysOfMonth'] = $ArrayAPIData['schedulerecdaysofmonth'];
		}
	if ($ArrayAPIData['schedulerecmonths'] != '')
		{
		$ArrayFieldnValues['ScheduleRecMonths'] = $ArrayAPIData['schedulerecmonths'];
		}
	if ($ArrayAPIData['schedulerechours'] != '')
		{
		$ArrayFieldnValues['ScheduleRecHours'] = $ArrayAPIData['schedulerechours'];
		}
	if ($ArrayAPIData['schedulerecminutes'] != '')
		{
		$ArrayFieldnValues['ScheduleRecMinutes'] = $ArrayAPIData['schedulerecminutes'];
		}
	if ($ArrayAPIData['schedulerecminutes'] != '')
		{
		$ArrayFieldnValues['ScheduleRecSendMaxInstance'] = $ArrayAPIData['schedulerecsendmaxinstance'];
		}
	if ($ArrayAPIData['approvaluserexplanation'] != '')
		{
		$ArrayFieldnValues['ApprovalUserExplanation'] = $ArrayAPIData['approvaluserexplanation'];
		}
	if (isset($ArrayAPIData['googleanalyticsdomains']))
		{
		$ArrayFieldnValues['GoogleAnalyticsDomains'] = $ArrayAPIData['googleanalyticsdomains'];
		}
	if ($ArrayAPIData['publishonrss'] != '')
		{
		$ArrayFieldnValues['PublishOnRSS'] = ($ArrayAPIData['publishonrss'] == 'Enabled' ? 'Enabled' : 'Disabled');
		}
	if ($ArrayAPIData['recipientlistsandsegments'] != '')
		{
		$ArrayRecipients = explode(',',$ArrayAPIData['recipientlistsandsegments']);
		if (count($ArrayRecipients) > 0)
			{
			Campaigns::RemoveRecipientsOfCampaign($ArrayAPIData['campaignid'], $ArrayUserInformation['UserID']);

			// Before adding recipients, check if there are duplicate lists with "all recipients" and specific segment(s) - Start
			$ArrayToAllRecipients	= array();
			foreach ($ArrayRecipients as $EachRecipient)
				{
				if ($EachRecipient != '')
					{
					$ArrayRecipientData = explode(':', $EachRecipient);
					if ($ArrayRecipientData[1] == 0)
						{
						$ArrayToAllRecipients[] = $ArrayRecipientData[0];
						}
					}
				}
			// Before adding recipients, check if there are duplicate lists with "all recipients" and specific segment(s) - End

			foreach ($ArrayRecipients as $EachRecipient)
				{
				if ($EachRecipient != '')
					{
					$ArrayRecipientData = explode(':', $EachRecipient);
					if (count($ArrayRecipientData) > 1)
						{
						if ((in_array($ArrayRecipientData[0], $ArrayToAllRecipients) == true) && ($ArrayRecipientData[1] == 0))
							{
							Campaigns::AddRecipientToCampaign($ArrayAPIData['campaignid'], $ArrayUserInformation['UserID'], $ArrayRecipientData[0], $ArrayRecipientData[1]);
							}
						if ((in_array($ArrayRecipientData[0], $ArrayToAllRecipients) == false) && ($ArrayRecipientData[1] != 0))
							{
							Campaigns::AddRecipientToCampaign($ArrayAPIData['campaignid'], $ArrayUserInformation['UserID'], $ArrayRecipientData[0], $ArrayRecipientData[1]);
							}
						}
					}
				}
			}
		}

	if ($ArrayCampaign['CampaignStatus'] == 'Draft' && (isset($ArrayFieldnValues['campaignstatus']) && $ArrayFieldnValues['campaignstatus'] == 'Ready')) {
		if ($ArrayUserInformation['ReputationLevel'] == 'Untrusted') {
			$ArrayFieldnValues['CampaignStatus'] = 'Pending Approval';
			O_Email_Sender_ForAdmin::send(
				O_Email_Factory::campaignApprovalPendingNotification()
			);
		}
	}

	if (count($ArrayFieldnValues) > 0)
		{
		Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));
		}
// Update campaign information - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Campaign.Update.Post', array($ArrayCampaign['CampaignID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0
					);
// Return results - End


?>