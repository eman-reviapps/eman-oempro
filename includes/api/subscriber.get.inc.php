<?php
// Check for required fields - Start
$ArrayRequiredFields = array('emailaddress' => 1, 'listid' => 2);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
// Load other modules - End

// Get subscriber - Start
$Subscriber = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $ArrayAPIData['emailaddress']), $ArrayAPIData['listid']);
if (! $Subscriber)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						);
	throw new Exception('');
	}
// Get subscriber - End

// Return results - Start
$ArrayOutput = array('Success'					=> true,
					 'ErrorCode'				=> 0,
					 'SubscriberInformation'	=> $Subscriber
					);
// Return results - End
?>