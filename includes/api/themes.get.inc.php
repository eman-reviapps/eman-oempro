<?php
// Load other modules - Start
Core::LoadObject('themes');
// Load other modules - End

// Check for required fields - Start
// Check for required fields - End

// Get lists - Start
$ArrayThemes = ThemeEngine::RetrieveThemes(array('ThemeID', 'Template', 'ThemeName', 'ProductName', 'ThemeSettings'), array());
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Themes'			=> $ArrayThemes,
					);
// Return results - End
?>