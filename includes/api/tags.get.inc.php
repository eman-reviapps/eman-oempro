<?php
// Load other modules - Start
Core::LoadObject('tags');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Get tags - Start
$ArrayTags = Tags::RetrieveTags(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID']));
// Get tags - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalTagCount'	=> ($ArrayTags != false ? count($ArrayTags) : 0),
					 'Tags'				=> $ArrayTags
					);
// Return results - End
?>