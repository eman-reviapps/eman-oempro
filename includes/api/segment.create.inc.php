<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							'segmentname'			=> 2,
							'segmentoperator'		=> 3
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
		$ArrayErrorMessages = array(
									"1" => 'Missing subscriber list id',
									"2" => 'Missing segment name',
									"3" => 'Missing segment operator'
									);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('segments');
// Load other modules - End

// Create segment - Start
$ArrayNewSegmentInformation = array();
$ArrayNewSegmentInformation['RelListID'] = $ArrayAPIData['subscriberlistid'];
$ArrayNewSegmentInformation['RelOwnerUserID'] = $ArrayUserInformation['UserID'];
$ArrayNewSegmentInformation['SegmentName'] = $ArrayAPIData['segmentname'];
$ArrayNewSegmentInformation['SegmentOperator'] = $ArrayAPIData['segmentoperator'];

	// Prepare segment rules as string - Start
	$ArrayNewSegmentInformation['SegmentRules'] = '';
	foreach ($ArrayAPIData['segmentrulefield'] as $Key=>$Value)
		{
		$ArrayNewSegmentInformation['SegmentRules'] .= Segments::GetRuleAsString($Value, $ArrayAPIData['segmentruleoperator'][$Key], $ArrayAPIData['segmentrulefilter'][$Key]);
		$ArrayNewSegmentInformation['SegmentRules'] .= ',,,';
		}
	$ArrayNewSegmentInformation['SegmentRules'] = substr($ArrayNewSegmentInformation['SegmentRules'], 0, $ArrayNewSegmentInformation['SegmentRules'].length - 3);
	// Prepare segment rules as string - End

$NewSegmentID = Segments::Create($ArrayNewSegmentInformation);
// Create segment - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'SegmentID'		=> $NewSegmentID
					);
// Return results - End
?>