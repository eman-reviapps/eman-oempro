<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Check for required fields - Start
$ArrayRequiredFields = array(
							'adminid'					=> 6,
							'name'						=> 1,
							'username'					=> 2,
							'emailaddress'				=> 4,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('admins');
Core::LoadObject('subscribers');
// Load other modules - End

// Field validations - Start
// Check if the admin account is owned by the logged in admin - Start
if ($ArrayAPIData['adminid'] != $ArrayAdminInformation['AdminID'])
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 8,
						);
	throw new Exception('');
	}
// Check if the admin account is owned by the logged in admin - End

// Validate admin email address format - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7,
						);
	throw new Exception('');
	}
// Validate admin email address format - End
// Field validations - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Administrator.Update.Pre', array($ArrayAdminInformation['AdminID'], $ArrayAPIData['Username'], $ArrayAPIData['Password'], $ArrayAPIData['EmailAddress'], $ArrayAPIData['Name']));
// Plug-in hook - End

// Update admin account - Start
$ArrayFieldAndValues = array(
							'AdminID'				=> $ArrayAdminInformation['AdminID'],
							'Username'				=> $ArrayAPIData['username'],
							'EmailAddress'			=> $ArrayAPIData['emailaddress'],
							'Name'					=> $ArrayAPIData['name'],
							);

if (isset($ArrayAPIData['password']) && ($ArrayAPIData['password'] != ''))
	{
	$ArrayFieldAndValues['Password'] = md5($ArrayAPIData['password']);
	}
							
Admins::UpdateAdmin($ArrayFieldAndValues, array('AdminID' => $ArrayAdminInformation['AdminID']));
// Update admin account - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Administrator.Update.Post', array($ArrayAdminInformation['AdminID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End


?>