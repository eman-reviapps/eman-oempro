<?php
$mode = 'ByEmail';

// Check for required fields - Start
if (! isset($ArrayAPIData['userid']))
	{
	$ArrayRequiredFields = array('emailaddress' => 1);
	}
else
	{
	$ArrayRequiredFields = array('userid' => 2);
	$mode = 'ById';
	}
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('users');
// Load other modules - End

// Get user - Start
$criteria = array();
if ($mode == 'ByEmail')
	{
	$criteria['EmailAddress'] = $ArrayAPIData['emailaddress'];
	}
else if ($mode == 'ById')
	{
	$criteria['UserID'] = $ArrayAPIData['userid'];
	}
$ArrayUser = Users::RetrieveUser(array('*'), $criteria, true);

if (! $ArrayUser)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						);
	throw new Exception('');
	}

// Get user - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'UserInformation'	=> $ArrayUser
					);
// Return results - End
?>