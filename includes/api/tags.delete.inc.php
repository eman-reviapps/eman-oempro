<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'tags'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('tags');
// Load other modules - End

// Delete clients - Start
Tags::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['tags']));
// Delete clients - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>