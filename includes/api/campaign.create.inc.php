<?php

// Load other modules - Start
Core::LoadObject('campaigns');
Core::LoadObject('subscribers');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignname'			=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Create the new campaign record - Start
$NewCampaignID = Campaigns::CreateCampaign(array(
	'RelOwnerUserID'		=>	$ArrayUserInformation['UserID'],
	'CampaignName'			=>	$ArrayAPIData['campaignname'],
	'CreateDateTime'		=>	date('Y-m-d H:i:s'),
	'LastActivityDateTime'	=>	date('Y-m-d H:i:s')
	));
// Create the new campaign record - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Campaign.Create.Post', array($NewCampaignID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'CampaignID'		=> $NewCampaignID,
					);
// Return results - End
?>