<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Check for required fields - Start
$ArrayRequiredFields = array(
							'usergroupid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('user_groups');
// Load other modules - End

// Delete the user group - Start
foreach (explode(',', $ArrayAPIData['usergroupid']) as $EachUserGroupID)
	{
	if ($EachUserGroupID == '')
		{
		continue;
		}
		
	// Get lists - Start
	$ArrayUserGroup = UserGroups::RetrieveUserGroup($EachUserGroupID);

	if ($ArrayUserGroup == false)
		{
		continue;
		}
	// Get lists - End

	// If this user group is set by default for the system or new user signups, do not delete it - Start
	if ($ArrayUserGroup['UserGroupID'] == USER_SIGNUP_GROUPID)
		{
		continue;
		}
	// If this user group is set by default for the system or new user signups, do not delete it - End

	// If this is the last user group, do not delete it - Start
	if (UserGroups::RetrieveUserGroupCount() <= 1)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 4,
							);
		throw new Exception('');
		}
	// If this is the last user group, do not delete it - End

	UserGroups::Delete($EachUserGroupID);

	Plugins::HookListener('Action', 'UserGroup.Delete.Post', array($ArrayUserGroup));


	// Assign users of this group to the "new user sign up group" - Start
	Core::LoadObject('users');
	Users::UpdateUser(array('RelUserGroupID' => USER_SIGNUP_GROUPID), array('RelUserGroupID' => $ArrayUserGroup['UserGroupID']));
	// Assign users of this group to the "new user sign up group" - End
	}
// Delete the user group - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End
?>