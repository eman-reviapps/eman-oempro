<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'template'				=> 1,
							'themename'				=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('themes');
// Load other modules - End

// Field validations - Start
// Check for the template code - Start
$ArrayInstalledTemplates = ThemeEngine::DetectTemplates();

$IsFound = false;
foreach ($ArrayInstalledTemplates as $Index=>$ArrayEachTemplate)
	{
	if ($ArrayEachTemplate['Code'] == $ArrayAPIData['template'])
		{
		$IsFound = true;
		break;
		}
	}
if ($IsFound == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3,
						);
	throw new Exception('');
	}
// Check for the template code - End
// Field validations - End

// Prepare the theme settings - Start
$ArrayCSSSettings			= ThemeEngine::LoadCSSSettings($ArrayAPIData['template'], false);
$ThemeSettings				= '';

$TMPArrayAdminSetCSSSettings= explode("\n", $ArrayAPIData['themesettings']);
$ArrayAdminSetCSSSettings 	= array();
foreach ($TMPArrayAdminSetCSSSettings as $TMPEach)
	{
	$TMPEach = explode('||||', $TMPEach);
	
	$ArrayAdminSetCSSSettings[$TMPEach[0]] = $TMPEach[1];
	}

foreach ($ArrayCSSSettings as $EachSetting)
	{
	$ThemeSettings			.= $EachSetting['Tag'].'||||'.($ArrayAdminSetCSSSettings[$EachSetting['Tag']] == '' ? $EachSetting['Default'] : $ArrayAdminSetCSSSettings[$EachSetting['Tag']])."\n";
	}
// Prepare the theme settings - End

// Create new theme - Start
$NewThemeID = ThemeEngine::Create(array(
									'Template'				=>	$ArrayAPIData['template'],
									'ThemeName'				=>	$ArrayAPIData['themename'],
									'ProductName'			=>	($ArrayAPIData['productname'] != '' ? $ArrayAPIData['productname'] : PRODUCT_NAME),
									'LogoData'				=> '',
									'LogoType'				=> '',
									'LogoSize'				=> 0,
									'LogoFileName'			=> '',
									'ThemeSettings'			=> $ThemeSettings,
									));
// Create new theme - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ThemeID'			=> $NewThemeID,
					);
// Return results - End
?>