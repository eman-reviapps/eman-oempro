<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'listid'			=> 1,
							'msubscriberid'		=> 2,
							'memailaddress'		=> 3,
							'emailaddress'		=> 4,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('users');
// Load other modules - End

// Field validations - Start
// Validate the list ID and list ownership - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['listid']));
if ($ArraySubscriberList == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5
						);
	throw new Exception('');
	}
// Validate the list ID and list ownership - End

// Retrieve owner user information - Start
$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberList['RelOwnerUserID']));

if ($ArrayUserInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 6
						);
	throw new Exception('');
	}
// Retrieve owner user information - End

// Validate email address - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7
						);
	throw new Exception('');
	}
// Validate email address - End

// Verify the checksum - Start
if (isset($ArrayAPIData['memailaddress']) == true && $ArrayAPIData['memailaddress'] != '')
{
	if ($ArrayAPIData['memailaddress'] != md5($ArrayAPIData['emailaddress']))
	{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 8
							);
		throw new Exception('');
	}
}
// Verify the checksum - End

if (($ArraySubscriberInformation = SubscriberAuth::ValidateAndPerformLogin(array('ListID' => $ArrayAPIData['listid'], 'SubscriberID' => $ArrayAPIData['msubscriberid'], 'EmailAddress' => $ArrayAPIData['memailaddress'], 'ValidateEmailAddress' => $ArrayAPIData['emailaddress']))) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 8
						);
	throw new Exception('');
	}
// Field validations - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Subscriber'		=> array('SubscriberID' => $ArraySubscriberInformation['SubscriberID'], 'EmailAddress' => $ArraySubscriberInformation['EmailAddress']),
					);
// Return results - End


?>