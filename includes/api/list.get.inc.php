<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'listid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('lists');
// Load other modules - End

// Get lists - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'ListID'=>$ArrayAPIData['listid']), true);
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> ($ArraySubscriberList == false ? false : true),
					 'ErrorCode'		=> 0,
					 'List'				=> $ArraySubscriberList
					);
// Return results - End
?>