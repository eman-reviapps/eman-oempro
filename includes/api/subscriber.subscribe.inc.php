<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'listid' => 1,
    'emailaddress' => 2,
    'ipaddress' => 3,
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ArrayErrorFields
    );
    throw new Exception('');
}
// Check for required fields - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('custom_fields');
Core::LoadObject('users');
// Load other modules - End
// Field validations - Start
$ArrayLists = explode(',', $ArrayAPIData['listid']);

$FirstListID = array_shift($ArrayLists);

// Validate the list ID and list ownership - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $FirstListID));
if ($ArraySubscriberList == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 4
    );
    throw new Exception('');
}
// Validate the list ID and list ownership - End
// Retrieve owner user information - Start
$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberList['RelOwnerUserID']));
if ($ArrayUserInformation == false || (isset($ArrayUserInformation['AccountStatus']) == true && $ArrayUserInformation['AccountStatus'] == 'Disabled') || (isset($ArrayUserInformation['ReputationLevel']) == true && $ArrayUserInformation['ReputationLevel'] != 'Trusted')
) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 11
    );
    throw new Exception('');
}
// Retrieve owner user information - End
// Detect custom fields passed in the subscription form - Start
$ArrayCustomFieldIDs = array();

foreach ($ArrayAPIData as $Key => $Value) {
    if (strtolower(substr($Key, 0, 11)) == 'customfield') {
        $CustomFieldID = substr($Key, 11, strlen($Key));
        if (!preg_match("/^[\d]+$/uiUs", $CustomFieldID))
            continue;
        $ArrayCustomFieldIDs[$CustomFieldID] = true;
    }
}
// Detect custom fields passed in the subscription form - End
// Validate email address - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 5
    );
    throw new Exception('');
}
// Validate email address - End
// Check if each provided custom field is owned by the user - Start
foreach ($ArrayCustomFieldIDs as $CustomFieldID => $Status) {
    $ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID, 'RelOwnerUserID' => $ArrayUserInformation['UserID'], array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $ArraySubscriberList['ListID'] . ')', 'auto-quote' => false)));

    if ($ArrayCustomField == false) {
        $ArrayCustomFieldIDs[$CustomFieldID] = false;
        unset($ArrayCustomFieldIDs[$CustomFieldID]);
        continue;
    }

    // Custom fields: check required custom fields - Start
    if ($ArrayCustomField['IsRequired'] == 'Yes') {
        if ($ArrayAPIData['customfield' . $ArrayCustomField['CustomFieldID']] == '') {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 6,
                'ErrorCustomFieldID' => $ArrayCustomField['CustomFieldID'],
                'ErrorCustomFieldTitle' => $ArrayCustomField['FieldName']
            );
            throw new Exception('');
        }
    }
    // Custom fields: check required custom fields - End
    // Custom fields: check validation - Start
    if ($ArrayCustomField['ValidationMethod'] != 'Disabled') {
        $ArrayReturn = CustomFields::ValidateCustomFieldValue($ArrayCustomField, $ArrayAPIData['customfield' . $ArrayCustomField['CustomFieldID']]);

        if ($ArrayReturn[0] == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 8,
                'ErrorCustomFieldID' => $ArrayCustomField['CustomFieldID'],
                'ErrorCustomFieldTitle' => $ArrayCustomField['FieldName'],
                'ErrorCustomFieldDescription' => $ArrayReturn[1],
            );
            throw new Exception('');
        }
    }
    // Custom fields: check validation - End
    // Custom fields: check unique custom fields - Start
    if ($ArrayCustomField['IsUnique'] == 'Yes') {
        if (Subscribers::CheckCustomFieldValueUniqueness($ArrayCustomField, $ArrayAPIData['customfield' . $ArrayCustomField['CustomFieldID']], 0, $ArraySubscriberList['ListID'], '') == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 7,
                'ErrorCustomFieldID' => $ArrayCustomField['CustomFieldID'],
                'ErrorCustomFieldTitle' => $ArrayCustomField['FieldName']
            );
            throw new Exception('');
        }
    }
    // Custom fields: check unique custom fields - End
}
// Check if each provided custom field is owned by the user - End
// Field validations - End
// Subscribe the email address - Start
$ArrayOtherFields = array();
$GlobalCustomFields = array();
foreach ($ArrayCustomFieldIDs as $CustomFieldID => $Status) {
    if ($Status == true) {
        $ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID, 'RelOwnerUserID' => $ArrayUserInformation['UserID'], array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, ' . $ArraySubscriberList['ListID'] . ')', 'auto-quote' => false)));
        $NewValue = '';
        if (strtolower(gettype($ArrayAPIData['customfield' . $CustomFieldID])) == 'array') {
            if ($ArrayCustomField['FieldType'] == 'Time field') {
                $NewValue = implode(':', $ArrayAPIData['customfield' . $CustomFieldID]) . ':00';
            } else if ($ArrayCustomField['FieldType'] == 'Date field') {
                $NewValue = $ArrayAPIData['customfield' . $CustomFieldID][2] . '-' . $ArrayAPIData['customfield' . $CustomFieldID][1] . '-' . $ArrayAPIData['customfield' . $CustomFieldID][0];
            } else {
                $NewValue = implode('||||', $ArrayAPIData['customfield' . $CustomFieldID]);
            }
        } else {
            $NewValue = $ArrayAPIData['customfield' . $CustomFieldID];
        }
        $NewValue = mysql_real_escape_string($NewValue);
        $ArrayOtherFields['CustomField' . $CustomFieldID] = $NewValue;
    }
}
$SubscriptionStatus = ($ArraySubscriberList['OptInMode'] == 'Single' ? 'Subscribed' : 'Opt-In Pending');

$SubscriptionResult = Subscribers::AddSubscriber_Enhanced(array(
            'UserInformation' => $ArrayUserInformation,
            'ListInformation' => $ArraySubscriberList,
            'EmailAddress' => $ArrayAPIData['emailaddress'],
            'IPAddress' => $ArrayAPIData['ipaddress'],
            'OtherFields' => $ArrayOtherFields,
            'UpdateIfDuplicate' => true,
            'UpdateIfUnsubscribed' => true,
            'ApplyBehaviors' => true,
            'SendConfirmationEmail' => true,
            'UpdateStatistics' => true,
            'TriggerWebServices' => true,
            'TriggerAutoResponders' => true,
            'IsSubscriptionAction' => true
        ));

if (!$SubscriptionResult[0]) {
    $ErrorCode = 10;
    switch ($SubscriptionResult[1]) {
        case 2:
            $ErrorCode = 5;
            break;
        case 3:
            $ErrorCode = 9;
            break;
        default:
            $ErrorCode = 10;
            break;
    }
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ErrorCode
    );
    throw new Exception('');
}

//Eman - Update subscriber geo-location - Start
$GeoTag = geoip_open(GEO_LOCATION_DATA_PATH, GEOIP_STANDARD);
$GeoTagInfo = geoip_record_by_addr($GeoTag, $ArrayAPIData['ipaddress']);

$Open_City = isset($GeoTagInfo->city) == true ? $GeoTagInfo->city : '';
$Open_Country = isset($GeoTagInfo->country_code) == true ? $GeoTagInfo->country_code : '';

$ArrayFieldAndValues = array(
    'Subscriber_IP' => $ArrayAPIData['ipaddress'],
    'City' => $Open_City,
    'Country' => $Open_Country,
);
Subscribers::Update($ArraySubscriberList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $SubscriptionResult[1]));
//Eman - Update subscriber geo-location - Start

// Subscribe to other lists if present - Start {
if (count($ArrayLists) > 0) {
    foreach ($ArrayLists as $EachList) {
        // Validate the list ID and list ownership - Start
        $ArraySubSubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $EachList));
        if ($ArraySubSubscriberList == false)
            continue;
        // Validate the list ID and list ownership - End
        // Retrieve owner user information - Start
        $ArraySubUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubSubscriberList['RelOwnerUserID']));
        if ($ArraySubUserInformation == false)
            continue;
        // Retrieve owner user information - End

        $SubscriptionResultOther = Subscribers::AddSubscriber_Enhanced(array(
            'UserInformation' => $ArraySubUserInformation,
            'ListInformation' => $ArraySubSubscriberList,
            'EmailAddress' => $ArrayAPIData['emailaddress'],
            'IPAddress' => $ArrayAPIData['ipaddress'],
            'SubscriptionStatus' => 'Subscribed',
            'OtherFields' => array(),
            'UpdateIfDuplicate' => false,
            'UpdateIfUnsubscribed' => false,
            'ApplyBehaviors' => false,
            'SendConfirmationEmail' => false,
            'UpdateStatistics' => true,
            'TriggerWebServices' => false,
            'TriggerAutoResponders' => false
        ));
        Subscribers::Update($ArraySubSubscriberList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $SubscriptionResultOther[1]));
    }
}
// Subscribe to other lists if present - End }
// Retrieve subscriber information - Start {
$SubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriptionResult[1]), $ArraySubscriberList['ListID']);
// Retrieve subscriber information - End }

$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'SubscriberID' => $SubscriptionResult[1],
    'RedirectURL' => ($ArraySubscriberList['OptInMode'] == 'Single' ? $ArraySubscriberList['SubscriptionConfirmedPageURL'] : $ArraySubscriberList['SubscriptionConfirmationPendingPageURL']),
    'Subscriber' => $SubscriberInformation
);



if ($ArraySubscriberList['SendActivityNotification'] == 'true') {
    O_Email_Sender_ForAdmin::send(
            O_Email_Factory::subscriptionNotification(
                    $ArrayUserInformation, $SubscriberInformation, $ArraySubscriberList
            )
    );
}

// Subscribe the email address - End
?>