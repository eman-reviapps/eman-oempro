<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistname'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('lists');
// Load other modules - End

// Field validations - Start
// Check if user has exceeded maximum number of subscriber lists - Start
$TotalListsOfUser = Users::TotalUserLists($ArrayUserInformation['UserID']);

if (($TotalListsOfUser >= $ArrayUserInformation['GroupInformation']['LimitLists']) && ($ArrayUserInformation['GroupInformation']['LimitLists'] > 0))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3)
						);
	throw new Exception('');
	}
// Check if user has exceeded maximum number of subscriber lists - End
// Field validations - End

// Check if a subscriber list exists with given name - Start
$ArraySubscriberListInformation = Lists::RetrieveList(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'Name'=>$ArrayAPIData['subscriberlistname']));
if (count($ArraySubscriberListInformation) > 1)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2)
						);
	throw new Exception('');
	}
// Check if a subscriber list exists with given name - End

// Create list - Start
$ArrayFieldAndValues = array(
							'Name'			=>	$ArrayAPIData['subscriberlistname'],
							'RelOwnerUserID'=>	$ArrayUserInformation['UserID']
							);
							
if ($ArrayUserInformation['GroupInformation']['ForceOptInList'] == 'Enabled')
	{
	$ArrayFieldAndValues['OptInMode'] = 'Double';
	}
$NewSubscriberListID = Lists::Create($ArrayFieldAndValues);
// Create list - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'List.Create.Post', array($NewSubscriberListID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'ListID'			=> $NewSubscriberListID
					);
// Return results - End
?>