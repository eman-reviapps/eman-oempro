<?php
// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Check if email exists - Start
$ArrayEmailInformation = Emails::RetrieveEmail(array('*'), array('EmailID'=>$ArrayAPIData['emailid'],'RelUserID'=>$ArrayUserInformation['UserID']), false);
if ($ArrayEmailInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Check if email exists - End

// Create the new email - Start
unset($ArrayEmailInformation['EmailID']);
$ArrayEmailInformation['EmailName'] = sprintf($ArrayLanguageStrings['Screen']['9140'], $ArrayEmailInformation['EmailName']);
$NewEmailID = Emails::Create(array('RelUserID'=>$ArrayUserInformation['UserID']));
Emails::Update($ArrayEmailInformation, array('EmailID'=>$NewEmailID,'RelUserID'=>$ArrayUserInformation['UserID']));
// Create the new email - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Create.Post', array($NewEmailID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'EmailID'			=> $NewEmailID,
					'EmailName'			=> $ArrayEmailInformation['EmailName']
					);
// Return results - End
?>