<?php
// Load other modules - Start
Core::LoadObject('web_service_integration');
Core::LoadObject('lists');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							'url'					=> 2,
							'event'					=> 3,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing subscriber list id',
								"2" => 'Missing url',
								"3" => 'Missing event type'
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}
// Check for required fields - End

// Check if subscriber list exists - Start
$ArraySubscriberListInformation = Lists::RetrieveList(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'ListID'=>$ArrayAPIData['subscriberlistid']));
if ($ArraySubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid subscriber list id')
						);
	throw new Exception('');
	}
// Check if subscriber list exists - End

// Add URL - Start
$NewURLID = WebServiceIntegration::AddURL(array(
	'RelListID'			=> $ArrayAPIData['subscriberlistid'],
	'EventType'			=> $ArrayAPIData['event'],
	'ServiceURL'		=> $ArrayAPIData['url'],
	'RelOwnerUserID'	=> $ArrayUserInformation['UserID']
	));
// Add URL - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'WebServiceIntegrationID'	=> $NewURLID,
					 'EventType'				=> $ArrayAPIData['event'],
					 'ServiceURL'				=> $ArrayAPIData['url']
					);
// Return results - End
?>