<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'autoresponderid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Missing auto responder id',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('auto_responders');
// Load other modules - End

// Get auto responder - Start
if (!isset($ArrayAPIData['retrievestatistics']) || $ArrayAPIData['retrievestatistics'] == '')
	{
	$ArrayAPIData['retrievestatistics'] = false;
	}
$ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'AutoResponderID'=>$ArrayAPIData['autoresponderid']), $ArrayAPIData['retrievestatistics']);
// Get auto responder - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'AutoResponder'	=> $ArrayAutoResponder
					);
// Return results - End
?>