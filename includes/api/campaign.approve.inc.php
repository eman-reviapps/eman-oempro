<?php
// Load other modules - Start
Core::LoadObject('campaigns');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'		=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $ArrayAPIData['campaignid']));

if ($ArrayCampaign == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}

Campaigns::Update(array('CampaignStatus' => 'Ready'), array('CampaignID' => $ArrayCampaign['CampaignID']));

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0
					);
// Return results - End


?>