<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Check for required fields - Start
$ArrayRequiredFields = array(
							'userid'				=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Be sure that the user ID is the one who is logged in at the moment - Start

if (($ArrayUserInformation['UserID'] != $ArrayAPIData['userid']) && (AdminAuth::IsLoggedIn(false,false) == false))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Be sure that the user ID is the one who is logged in at the moment - End
// Field validations - End

// If this command executed through admin authentication, user information should be retrieved - Start
if (AdminAuth::IsLoggedIn(false, false) == true)
	{
	$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArrayAPIData['userid']));
	
	if ($ArrayUserInformation == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 5,
							);
		throw new Exception('');
		}
	}
// If this command executed through admin authentication, user information should be retrieved - End

// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('subscribers');
// Load other modules - End

// Update user information - Start
	$ArrayFieldnValues = array();
	
	// These values can only be updated by administrator - Start {
	if (AdminAuth::IsLoggedIn(false, false) == true)
		{
		if ($ArrayAPIData['accountstatus'] != '')
			{
			$ArrayFieldnValues['AccountStatus'] = $ArrayAPIData['accountstatus'];
			}
		if ($ArrayAPIData['availablecredits'] != '')
			{
			$ArrayFieldnValues['AvailableCredits'] = $ArrayAPIData['availablecredits'];
			}
		if ($ArrayAPIData['signupipaddress'] != '')
			{
			$ArrayFieldnValues['SignUpIPAddress'] = $ArrayAPIData['signupipaddress'];
			}
		if ($ArrayAPIData['apikey'] != '')
			{
			$ArrayFieldnValues['APIKey'] = $ArrayAPIData['apikey'];
			}
		if ((isset($ArrayAPIData['relusergroupid']) == true) && ($ArrayAPIData['relusergroupid'] != ''))
			{
			$ArrayFieldnValues['RelUserGroupID'] = $ArrayAPIData['relusergroupid'];
			}
		if (($ArrayAPIData['reputationlevel'] != '') && (($ArrayAPIData['reputationlevel'] == 'Trusted') || ($ArrayAPIData['reputationlevel'] == 'Untrusted')))
			{
			$ArrayFieldnValues['ReputationLevel'] = $ArrayAPIData['reputationlevel'];
			}
		if (($ArrayAPIData['usersince'] != '') && ((strtotime($ArrayAPIData['usersince']) != false) && strtotime($ArrayAPIData['usersince']) != -1))
			{
			$ArrayFieldnValues['UserSince'] = date('Y-m-d H:i:s', strtotime($ArrayAPIData['usersince']));
			}
		}
	// These values can only be updated by administrator - End }

		
	if (($ArrayAPIData['emailaddress'] != '') && (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == true))
		{
		$ArrayFieldnValues['EmailAddress'] = $ArrayAPIData['emailaddress'];
		}
	if ($ArrayAPIData['username'] != '')
		{
		// Check if username is unique
		$duplicateUsers = Users::RetrieveUsers(array('*'), array('Username' => $ArrayAPIData['username'], array('field' => 'UserID', 'operator' => '!=', 'value' => $ArrayUserInformation['UserID'])));

		if ($duplicateUsers !== FALSE)
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> 6,
								);
			throw new Exception('');
			}
		$ArrayFieldnValues['Username'] = $ArrayAPIData['username'];
		}
	if ($ArrayAPIData['password'] != '')
		{
		$ArrayFieldnValues['Password'] = md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['password'].OEMPRO_PASSWORD_SALT);
		}
	if ($ArrayAPIData['firstname'] != '')
		{
		$ArrayFieldnValues['FirstName'] = $ArrayAPIData['firstname'];
		}
	if ($ArrayAPIData['lastname'] != '')
		{
		$ArrayFieldnValues['LastName'] = $ArrayAPIData['lastname'];
		}
	if ($ArrayAPIData['companyname'] != '')
		{
		$ArrayFieldnValues['Companyname'] = $ArrayAPIData['companyname'];
		}
	if ($ArrayAPIData['website'] != '')
		{
		$ArrayFieldnValues['Website'] = $ArrayAPIData['website'];
		}
	if ($ArrayAPIData['street'] != '')
		{
		$ArrayFieldnValues['Street'] = $ArrayAPIData['street'];
		}
	if ($ArrayAPIData['city'] != '')
		{
		$ArrayFieldnValues['City'] = $ArrayAPIData['city'];
		}
	if ($ArrayAPIData['state'] != '')
		{
		$ArrayFieldnValues['State'] = $ArrayAPIData['state'];
		}
	if ($ArrayAPIData['zip'] != '')
		{
		$ArrayFieldnValues['Zip'] = $ArrayAPIData['zip'];
		}
	if ($ArrayAPIData['country'] != '')
		{
		$ArrayFieldnValues['Country'] = $ArrayAPIData['country'];
		}
	if ($ArrayAPIData['phone'] != '')
		{
		$ArrayFieldnValues['Phone'] = $ArrayAPIData['phone'];
		}
	if ($ArrayAPIData['fax'] != '')
		{
		$ArrayFieldnValues['Fax'] = $ArrayAPIData['fax'];
		}
	if ($ArrayAPIData['timezone'] != '')
		{
		$ArrayFieldnValues['TimeZone'] = $ArrayAPIData['timezone'];
		}
	if ($ArrayAPIData['language'] != '')
		{
		$ArrayFieldnValues['Language'] = $ArrayAPIData['language'];
		}
	if ($ArrayAPIData['lastactivitydatetime'] != '')
		{
		$ArrayFieldnValues['LastActivityDateTime'] = $ArrayAPIData['lastactivitydatetime'];
		}
	if ((isset($ArrayAPIData['forwardtofriendheader']) == true) && ($ArrayAPIData['forwardtofriendheader'] != ''))
		{
		$ArrayFieldnValues['ForwardToFriendHeader'] = $ArrayAPIData['forwardtofriendheader'];
		}
	if ((isset($ArrayAPIData['forwardtofriendfooter']) == true) && ($ArrayAPIData['forwardtofriendfooter'] != ''))
		{
		$ArrayFieldnValues['ForwardToFriendFooter'] = $ArrayAPIData['forwardtofriendfooter'];
		}


	// Test PreviewMyEmail access settings if they are provided - Start
	if (($ArrayAPIData['previewmyemailaccount'] != '') || ($ArrayAPIData['previewmyemailapikey'] != ''))
		{
		$PMEAPIKey		= ($ArrayAPIData['previewmyemailapikey'] != '' ? $ArrayAPIData['previewmyemailapikey'] : PME_APIKEY);

		// Get available credits via PreviewMyEmail.com API - Start
		$ArrayPostParameters = array(
									'apikey='.$PMEAPIKey
									);
		$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'SystemStatus/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
		// Get available credits via PreviewMyEmail.com API - End

		// Parse the returned data - Start
		if ($ArrayReturn[0] == false)
			{
			// connection error occurred
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> 3
								);
			throw new Exception('');
			}
		else
			{
			$ObjectXML = simplexml_load_string($ArrayReturn[1]);
			if ($ArrayAPIData['previewmyemailapikey'] != '')
				{
				$ArrayFieldnValues['PreviewMyEmailAPIKey'] = $ArrayAPIData['previewmyemailapikey'];
				}
			}
		// Parse the returned data - End
		}
	// Test PreviewMyEmail access settings if they are provided - End

	if (count($ArrayFieldnValues) > 0)
		{
		Users::UpdateUser($ArrayFieldnValues, array('UserID' => $ArrayUserInformation['UserID']));
		}
// Update user information - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'User.Update.Post', array($ArrayUserInformation['UserID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End


?>