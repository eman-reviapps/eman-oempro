<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'usergroupid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('user_groups');
// Load other modules - End

// Duplicate the user group - Start
	// Get user group - Start
	$ArrayUserGroup = UserGroups::RetrieveUserGroup($ArrayAPIData['usergroupid']);

	if ($ArrayUserGroup == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> array(2),
							);
		throw new Exception('');
		}
	// Get user group - End

	// Get theme - Start
	$ArrayTheme = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID'=>$ArrayUserGroup['RelThemeID']));
	// Get theme - End

	// Duplicate theme - Start
	$ArrayTheme['ThemeID'] = '';
	unset($ArrayTheme['ArrayThemeSettings']);
	$NewThemeID = ThemeEngine::Create($ArrayTheme);
	// Duplicate theme - End

	// Create the user group - Start
	$ArrayUserGroup['UserGroupID'] = '';
	$ArrayUserGroup['GroupName'] = sprintf($ArrayLanguageStrings['Screen']['9116'], $ArrayUserGroup['GroupName']);
	$ArrayUserGroup['RelThemeID'] = $NewThemeID;
	unset($ArrayUserGroup['ThemeInformation']);
	$NewUserGroupID = UserGroups::CreateUserGroup($ArrayUserGroup);
	// Create the user group - End
// Duplicate the user group - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'UserGroupID'		=> $NewUserGroupID
					);
// Return results - End
?>