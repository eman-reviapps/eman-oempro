<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							'emailaddress'			=> 2,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('campaigns');
Core::LoadObject('statistics');
Core::LoadObject('queue');
Core::LoadObject('attachments');
Core::LoadObject('personalization');
Core::LoadObject('queue');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
// Retrieve email information - End

// Retrieve campaign information - Start
if ($ArrayAPIData['campaignid'] != '')
	{
	$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $ArrayAPIData['campaignid']));
	if ($ArrayCampaign == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 7
							);
		throw new Exception('');
		}
	}
else
	{
	$ArrayCampaign = array();
	}
// Retrieve campaign information - End

// Retrieve list information - Start
if ($ArrayAPIData['listid'] != '')
	{
	$ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['listid']));
	if ($ArrayList == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 6
							);
		throw new Exception('');
		}
	}
else
	{
	// Select one of the recipient lists from the campaign - Start
	$ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayCampaign['RecipientLists'][0]), false);
	if ($ArrayList == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 6
							);
		throw new Exception('');
		}
	// Select one of the recipient lists from the campaign - End
	}
// Retrieve list information - End

// Validate email address format - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4
						);
	throw new Exception('');
	}
// Validate email address format - End

$AddUserGroupHeaderAndFooter = true;
if (isset($ArrayAPIData['addusergroupheaderfooter']) && ($ArrayAPIData['addusergroupheaderfooter'] == true || $ArrayAPIData['addusergroupheaderfooter'] == false))
	{
	$AddUserGroupHeaderAndFooter = $ArrayAPIData['addusergroupheaderfooter'];
	}
// Field validations - End

// Retrieve a random subscriber from the target list - Start
$ArrayRandomSubscriber	= Subscribers::SelectRandomSubscriber($ArrayList['ListID'], true);
// Retrieve a random subscriber from the target list - End

$ArrayEmail['Subject'] = '['.$ArrayLanguageStrings['Screen']['1355'].'] ' . $ArrayEmail['Subject'];

// Send preview email - Start
if (isset($ArrayAPIData['emailtype']) && $ArrayAPIData['emailtype'] == 'optinconfirmation')
	{
	$optinConfirmationEmail = O_Email_Factory::optInConfirmationEmail($ArrayEmail, $ArrayRandomSubscriber, $ArrayList, $ArrayUserInformation, true);
	$optinConfirmationEmail->setTo('', $ArrayAPIData['emailaddress']);
	O_Email_Sender_ForUserGroup::send($ArrayUserInformation['GroupInformation'], $optinConfirmationEmail);
	}
else
	{
	Emails::SendPreviewEmail(SEND_METHOD, $ArrayAPIData['emailaddress'], $ArrayUserInformation, $ArrayCampaign, $ArrayList, $ArrayRandomSubscriber, $ArrayEmail, false, false, false, $AddUserGroupHeaderAndFooter);
	}

	// Send preview email - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					);
// Return results - End
?>