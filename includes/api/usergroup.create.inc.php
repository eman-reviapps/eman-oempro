<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'groupname'						=> 1,
							'subscriberarealogouturl'		=> 2,
							'limitsubscribers'				=> 5,
							'limitlists'					=> 6,
							'limitcampaignsendperperiod'	=> 7,
							'limitemailsendperperiod'		=> 20,
							'relthemeid'					=> 8,
							'forceunsubscriptionlink'		=> 17,
							'forcerejectoptlink'			=> 18,
							);

if ($ArrayAPIData['paymentsystem'] == 'Enabled')
	{
	$ArrayRequiredFields['paymentcampaignsperrecipient']		= 9;
	$ArrayRequiredFields['paymentcampaignspercampaigncost']		= 10;
	$ArrayRequiredFields['paymentautoresponderschargeamount']	= 11;
	$ArrayRequiredFields['paymentautorespondersperrecipient']	= 12;
	$ArrayRequiredFields['paymentdesignprevchargeamount']		= 13;
	$ArrayRequiredFields['paymentdesignprevchargeperreq']		= 14;
	$ArrayRequiredFields['paymentsystemchargeamount']			= 15;
	
	if (($ArrayAPIData['paymentcampaignsperrecipient'] == 'Enabled') || ($ArrayAPIData['paymentautorespondersperrecipient'] == 'Enabled') || ($ArrayAPIData['creditsystem'] == 'Enabled'))
		{
		$ArrayRequiredFields['paymentpricingrange']				= 16;
		}
	}

if ($ArrayAPIData['trialgroup'] == 'Enabled')
	{
	$ArrayRequiredFields['trialexpireseconds']			= 21;
	}

if (isset($ArrayAPIData['sendmethod']) && ($ArrayAPIData['sendmethod'] != 'System'))
	{
	if (($ArrayAPIData['sendmethod'] != 'SMTP') && ($ArrayAPIData['sendmethod'] != 'LocalMTA') && ($ArrayAPIData['sendmethod'] != 'PHPMail') && ($ArrayAPIData['sendmethod'] != 'PowerMTA') && ($ArrayAPIData['sendmethod'] != 'SaveToDisk'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 22,
							);
		throw new Exception('');
		}
	}
if ((isset($ArrayAPIData['sendmethodsmtpsecure'])) && ($ArrayAPIData['sendmethod'] != 'System'))
	{
	if (($ArrayAPIData['sendmethodsmtpsecure'] != 'ssl') && ($ArrayAPIData['sendmethodsmtpsecure'] != 'tls') && ($ArrayAPIData['sendmethodsmtpsecure'] != ''))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 23,
							);
		throw new Exception('');
		}
	}
if ((isset($ArrayAPIData['sendmethodsmtpauth'])) && ($ArrayAPIData['sendmethod'] != 'System'))
	{
	if (($ArrayAPIData['sendmethodsmtpauth'] != 'true') && ($ArrayAPIData['sendmethodsmtpauth'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 24,
							);
		throw new Exception('');
		}
	}

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('themes');
Core::LoadObject('user_groups');
// Load other modules - End

// Field validations - Start
// Check if theme exists - Start
$ArrayTheme = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => $ArrayAPIData['relthemeid']));

if ($ArrayTheme == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 19,
						);
	throw new Exception('');
	}
// Check if theme exists - End

// Check if email sending settings set correctly - Start {
if ((isset($ArrayAPIData['sendmethod'])) && ($ArrayAPIData['sendmethod'] != 'System'))
	{
	$ArrayResult = array();
	Core::LoadObject('emails');

	if ($ArrayAPIData['sendmethod'] == 'SMTP')
		{
		$SendMethod				= 'SMTP';
		$SMTPHost				= $ArrayAPIData['sendmethodsmtphost'];
		$SMTPPort				= $ArrayAPIData['sendmethodsmtpport'];
		$SMTPSecure				= $ArrayAPIData['sendmethodsmtpsecure'];
		$SMTPAuth				= ($ArrayAPIData['sendmethodsmtpauth'] == 'true' ? true : false);
		$SMTPUsername			= $ArrayAPIData['sendmethodsmtpusername'];
		$SMTPPassword			= $ArrayAPIData['sendmethodsmtppassword'];
		$SMTPTimeout			= $ArrayAPIData['sendmethodsmtptimeout'];
		$SMTPDebug				= false;
		$SMTPKeepAlive			= true;
		$LocalMTAPath			= '';

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif ($ArrayAPIData['sendmethod'] == 'LocalMTA')
		{
		$SendMethod				= 'LocalMTA';
		$SMTPHost				= '';
		$SMTPPort				= '';
		$SMTPSecure				= '';
		$SMTPAuth				= '';
		$SMTPUsername			= '';
		$SMTPPassword			= '';
		$SMTPTimeout			= '';
		$SMTPDebug				= '';
		$SMTPKeepAlive			= '';
		$LocalMTAPath			= $ArrayAPIData['sendmethodlocalmtapath'];

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif ($ArrayAPIData['sendmethod'] == 'PHPMail')
		{
		$SendMethod				= 'PHPMail';
		$SMTPHost				= '';
		$SMTPPort				= '';
		$SMTPSecure				= '';
		$SMTPAuth				= '';
		$SMTPUsername			= '';
		$SMTPPassword			= '';
		$SMTPTimeout			= '';
		$SMTPDebug				= '';
		$SMTPKeepAlive			= '';
		$LocalMTAPath			= '';

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif (($ArrayAPIData['sendmethod'] == 'PowerMTA') || ($ArrayAPIData['sendmethod'] == 'SaveToDisk'))
		{
		// Add / to the end of paths if it does not exist - Start
		if ($ArrayAPIData['sendmethod'] == 'PowerMTA')
			{
			if (substr($ArrayAPIData['sendmethodpowermtadir'], strlen($ArrayAPIData['sendmethodpowermtadir']) - 1, strlen($ArrayAPIData['sendmethodpowermtadir'])) != '/')
				{
				$ArrayAPIData['sendmethodpowermtadir'] = $ArrayAPIData['sendmethodpowermtadir'].'/';
				}
			}
		else
			{
			if (substr($ArrayAPIData['sendmethodsavetodiskdir'], strlen($ArrayAPIData['sendmethodsavetodiskdir']) - 1, strlen($ArrayAPIData['sendmethodsavetodiskdir'])) != '/')
				{
				$ArrayAPIData['sendmethodsavetodiskdir'] = $ArrayAPIData['sendmethodsavetodiskdir'].'/';
				}
			}
		// Add / to the end of paths if it does not exist - End

		if (is_writable(($ArrayAPIData['sendmethod'] == 'PowerMTA' ? $ArrayAPIData['sendmethodpowermtadir'] : $ArrayAPIData['sendmethodsavetodiskdir'])) == false)
			{
			$ArrayResult = array(false, isset($ArrayLanguageStrings['Screen']['9097']) ? $ArrayLanguageStrings['Screen']['9097'] : ApplicationHeader::$ArrayLanguageStrings['Screen']['1325']);
			}
		else
			{
			$ArrayResult = array(true);
			}
		}

	if ($ArrayResult[0] == false)
		{
		$ArrayOutput = array('Success'						=> false,
							 'ErrorCode'					=> 25,
							 'EmailSettingsErrorMessage'	=> $ArrayResult[1],
							);
		throw new Exception('');
		}
	}
// Check if email sending settings set correctly - End }

// Field validations - End

// Create the user group - Start
$ArrayFieldAndValues = array(
							'UserGroupID'							=> '',
							'GroupName'								=> $ArrayAPIData['groupname'],
							'RelThemeID'							=> $ArrayTheme['ThemeID'],
							'SubscriberAreaLogoutURL'				=> $ArrayAPIData['subscriberarealogouturl'],
							'ForceUnsubscriptionLink'				=> ($ArrayAPIData['forceunsubscriptionlink'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'ForceRejectOptLink'					=> ($ArrayAPIData['forcerejectoptlink'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'ForceOptInList'						=> ($ArrayAPIData['forceoptinlist'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'Permissions'							=> implode(',', $ArrayAPIData['permissions']),
							'PaymentSystem'							=> ($ArrayAPIData['paymentsystem'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'CreditSystem'							=> ($ArrayAPIData['creditsystem'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'PaymentPricingRange'					=> $ArrayAPIData['paymentpricingrange'],
							'PaymentCampaignsPerRecipient'			=> ($ArrayAPIData['paymentcampaignsperrecipient'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'PaymentCampaignsPerCampaignCost'		=> $ArrayAPIData['paymentcampaignspercampaigncost'],
							'PaymentAutoRespondersChargeAmount'		=> $ArrayAPIData['paymentautoresponderschargeamount'],
							'PaymentAutoRespondersChargePeriod'		=> 'Monthly',
							'PaymentAutoRespondersPerRecipient'		=> ($ArrayAPIData['paymentautorespondersperrecipient'] == 'Enabled' ? 'Enabled' : 'Disabled'),
							'PaymentDesignPrevChargeAmount'			=> $ArrayAPIData['paymentdesignprevchargeamount'],
							'PaymentDesignPrevChargePeriod'			=> 'Monthly',
							'PaymentDesignPrevChargePerReq'			=> $ArrayAPIData['paymentdesignprevchargeperreq'],
							'PaymentSystemChargeAmount'				=> $ArrayAPIData['paymentsystemchargeamount'],
							'PaymentSystemChargePeriod'				=> 'Monthly',
							'LimitSubscribers'						=> $ArrayAPIData['limitsubscribers'],
							'LimitLists'							=> $ArrayAPIData['limitlists'],
							'LimitCampaignSendPerPeriod'			=> $ArrayAPIData['limitcampaignsendperperiod'],
							'LimitCampaignSendPeriod'				=> 'Monthly',
							'LimitEmailSendPerPeriod'				=> $ArrayAPIData['limitemailsendperperiod'],
							'LimitEmailSendPeriod'					=> 'Monthly',
							'ThresholdImport'						=> $ArrayAPIData['thresholdimport'],
							'ThresholdEmailSend'					=> $ArrayAPIData['thresholdemailsend'],
							'PlainEmailHeader'						=> $ArrayAPIData['plainemailheader'],
							'PlainEmailFooter'						=> $ArrayAPIData['plainemailfooter'],
							'HTMLEmailHeader'						=> $ArrayAPIData['htmlemailheader'],
							'HTMLEmailFooter'						=> $ArrayAPIData['htmlemailfooter'],
							'TrialGroup'							=> $ArrayAPIData['trialgroup'],
							'TrialExpireSeconds'					=> $ArrayAPIData['trialexpireseconds'],
							'XMailer'								=> $ArrayAPIData['xmailer'],
							'SendMethod'				    		=> $ArrayAPIData['sendmethod'],
							'SendMethodSaveToDiskDir'    			=> $ArrayAPIData['sendmethodsavetodiskdir'],
							'SendMethodPowerMTAVMTA'	    		=> $ArrayAPIData['sendmethodpowermtavmta'],
							'SendMethodPowerMTADir'	   				=> $ArrayAPIData['sendmethodpowermtadir'],
							'SendMethodLocalMTAPath'	    		=> $ArrayAPIData['sendmethodlocalmtapath'],
							'SendMethodSMTPHost'		    		=> $ArrayAPIData['sendmethodsmtphost'],
							'SendMethodSMTPPort'		    		=> $ArrayAPIData['sendmethodsmtpport'],
							'SendMethodSMTPSecure'		    		=> $ArrayAPIData['sendmethodsmtpsecure'],
							'SendMethodSMTPTimeOut'		    		=> $ArrayAPIData['sendmethodsmtptimeout'],
							'SendMethodSMTPAuth'		    		=> $ArrayAPIData['sendmethodsmtpauth'],
							'SendMethodSMTPUsername'	    		=> $ArrayAPIData['sendmethodsmtpusername'],
							'SendMethodSMTPPassword'	    		=> $ArrayAPIData['sendmethodsmtppassword'],
							);
$NewUserGroupID = UserGroups::CreateUserGroup($ArrayFieldAndValues);
// Create the user group - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'UserGroupID'		=> $NewUserGroupID,
					);
// Return results - End
?>