<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'tagid'				=> 1,
							'tag'				=> 2
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('tags');
// Load other modules - End

// Field validations - Start

// Update tag - Start
$ArrayFieldAndValues = array(
							'Tag'					=> $ArrayAPIData['tag']
							);
Tags::Update($ArrayFieldAndValues, array('TagID' => $ArrayAPIData['tagid']));
// Update Tag - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End


?>