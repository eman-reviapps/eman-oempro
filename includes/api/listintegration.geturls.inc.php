<?php
// Load other modules - Start
Core::LoadObject('web_service_integration');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Missing information',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Get URLs - Start
$ArrayCriterias['RelListID'] = $ArrayAPIData['subscriberlistid'];
if ($ArrayAPIData['event']) {
	$ArrayCriterias['EventType'] = $ArrayAPIData['event'];	
}
$ArrayCriterias['RelListID'] = $ArrayAPIData['subscriberlistid'];
$ArrayURLs = WebServiceIntegration::RetrieveURLs(array('*'), $ArrayCriterias, array());
// Get URLs - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'URLs'				=> $ArrayURLs
					);
// Return results - End
?>