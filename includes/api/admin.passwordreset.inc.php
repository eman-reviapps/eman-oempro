<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'reset_password');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'adminid'	=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('admins');
// Load other modules - End

// Validate if such an user id exists in the database - Start
$ArrayCriterias = array(
						'MD5(`AdminID`)'		=> $ArrayAPIData['adminid'],
						);
$ArrayAdmin = Admins::RetrieveAdmin(array('*'), $ArrayCriterias);

if ($ArrayAdmin == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// Validate if such an user id exists in the database - End

// Reset user password - Start
$NewPassword = Core::GenerateRandomString(5);

	$ArrayFieldnValues	= array(
								'Password'		=> md5(OEMPRO_PASSWORD_SALT.$NewPassword.OEMPRO_PASSWORD_SALT),
								);
	$ArrayCriterias		= array(
								'AdminID'		=> $ArrayAdmin['AdminID'],
								);
Admins::UpdateAdmin($ArrayFieldnValues, $ArrayCriterias);
// Reset user password - End

// Send password reminder email to the admin - Start
$Username = $ArrayAdmin['Username'];
O_Email_Sender_ForAdmin::send(
	O_Email_Factory::passwordReset($ArrayAdmin['EmailAddress'], $Username, $NewPassword)
);
// Send password reminder email to the admin - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End
?>