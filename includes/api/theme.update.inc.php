<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'themeid'				=> 4,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('themes');
// Load other modules - End

// Field validations - Start
// Check for the theme - Start
$ArrayTheme = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => $ArrayAPIData['themeid']));

if ($ArrayTheme == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5,
						);
	throw new Exception('');
	}
// Check for the theme - End

// Delete the cache file - Start
$CSSCacheFile = APP_PATH.'/data/css/css_cache_'.$ArrayTheme['ThemeID'].'.css';
if (file_exists($CSSCacheFile) == true) unlink($CSSCacheFile);
// Delete the cache file - End

// Check for the template code - Start
if (isset($ArrayAPIData['template']) == true)
	{
	$ArrayInstalledTemplates = ThemeEngine::DetectTemplates();

	$IsFound = false;
	foreach ($ArrayInstalledTemplates as $Index=>$ArrayEachTemplate)
		{
		if ($ArrayEachTemplate['Code'] == $ArrayAPIData['template'])
			{
			$IsFound = true;
			break;
			}
		}
	if ($IsFound == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 3,
							);
		throw new Exception('');
		}
	}
// Check for the template code - End
// Field validations - End

// Prepare the theme settings - Start
$ArrayCSSSettings			= ThemeEngine::LoadCSSSettings($ArrayAPIData['template'], false);
$ThemeSettings				= '';

$TMPArrayAdminSetCSSSettings= explode("\n", $ArrayAPIData['themesettings']);
$ArrayAdminSetCSSSettings 	= array();
foreach ($TMPArrayAdminSetCSSSettings as $TMPEach)
	{
	$TMPEach = explode('||||', $TMPEach);
	
	$ArrayAdminSetCSSSettings[$TMPEach[0]] = $TMPEach[1];
	}

foreach ($ArrayCSSSettings as $EachSetting)
	{
	$ThemeSettings			.= $EachSetting['Tag'].'||||'.($ArrayAdminSetCSSSettings[$EachSetting['Tag']] == '' ? $EachSetting['Default'] : $ArrayAdminSetCSSSettings[$EachSetting['Tag']])."\n";
	}
// Prepare the theme settings - End

// Update theme record - Start
$ArrayFieldAndValues = array(
							'ThemeID'				=>	$ArrayTheme['ThemeID'],
							'Template'				=>	($ArrayAPIData['template'] != '' ? $ArrayAPIData['template'] : $ArrayTheme['Template']),
							'ThemeName'				=>	($ArrayAPIData['themename'] != '' ? $ArrayAPIData['themename'] : $ArrayTheme['ThemeName']),
							'ProductName'			=>	($ArrayAPIData['productname'] != '' ? $ArrayAPIData['productname'] : $ArrayTheme['ProductName']),
							'LogoData'				=>	($ArrayAPIData['logodata'] != '' ? $ArrayAPIData['logodata'] : $ArrayTheme['LogoData']),
							'LogoType'				=>	($ArrayAPIData['logotype'] != '' ? $ArrayAPIData['logotype'] : $ArrayTheme['LogoType']),
							'LogoSize'				=>	($ArrayAPIData['logosize'] != '' ? $ArrayAPIData['logosize'] : $ArrayTheme['LogoSize']),
							'LogoFileName'			=>	($ArrayAPIData['logofilename'] != '' ? $ArrayAPIData['logofilename'] : $ArrayTheme['LogoFileName']),
							'ThemeSettings'			=>	$ThemeSettings,
							);
ThemeEngine::Update($ArrayFieldAndValues, array('ThemeID' => $ArrayTheme['ThemeID']));
// Update theme record - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End


?>