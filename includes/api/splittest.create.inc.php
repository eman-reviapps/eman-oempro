<?php

// Load other modules - Start
Core::LoadObject('split_tests');
Core::LoadObject('campaigns');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'			=> 1,
							'testsize'				=> 2,
							'testduration'			=> 4,
							'winner'				=> 5,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
	// Check if campaign exists - Start {
	$CampaignInformation = Campaigns::RetrieveCampaign(array('COUNT(*) AS TotalCampaigns'), array('CampaignID'=>$ArrayAPIData['campaignid'],'RelOwnerUserID'=>$ArrayUserInformation['UserID']));
	if ($CampaignInformation['TotalCampaigns'] < 1)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 6
							);
		throw new Exception('');
		}
	// Check if campaign exists - End }
// Field validations - End

// Create the split test - Start
$NewTestID = SplitTests::CreateNewTest(array(
	'RelCampaignID'		=>	$ArrayAPIData['campaignid'],
	'RelOwnerUserID'	=>	$ArrayUserInformation['UserID'],
	'SplitTestingStatus'=>	'Pending',
	'TestSize'			=>	$ArrayAPIData['testsize'],
	'TestDuration'		=>	$ArrayAPIData['testduration'],
	'Winner'			=>	$ArrayAPIData['winner'],
	'RelWinnerEmailID'	=>	0
	));
// Create the split test - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'SplitTestID'		=> $NewTestID,
					);
// Return results - End
?>