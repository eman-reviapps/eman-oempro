<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'autoresponderid'		=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing auto responder id',
								"2" => 'Missing auto responder name'
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}

if ($ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberLinkClick' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberCampaignOpen' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberDate')
	{
	$ArrayRequiredFields = array(
								'autorespondertriggervalue'		=> 3
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							 'ErrorText'		=> array('Missing auto responder trigger value')
							);
		throw new Exception('');
		}
	}

if (isset($ArrayAPIData['triggertimetype']) && $ArrayAPIData['triggertimetype'] != 'Immediately')
	{
	$ArrayRequiredFields = array(
								'triggertime'		=> 4
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							 'ErrorText'		=> array('Missing auto responder trigger time')
							);
		throw new Exception('');
		}
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('auto_responders');
// Load other modules - End

// Check if auto responder exists - Start
$ArrayAutoResponderInformation = AutoResponders::RetrieveResponder(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'AutoResponderID'=>$ArrayAPIData['autoresponderid']));
if ($ArrayAutoResponderInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(5),
						 'ErrorText'		=> array('Invalid auto responder id')
						);
	throw new Exception('');
	}
// Check if auto responder exists - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Update auto responder - Start
	// Set values - Start
	$ArrayNewAutoResponderInformation = array();
	if (isset($ArrayAPIData['subscriberlistid']) && $ArrayAPIData['subscriberlistid'] !== '')
		{
		$ArrayNewAutoResponderInformation['RelListID'] = $ArrayAPIData['subscriberlistid'];
		}
	$ArrayNewAutoResponderInformation['RelOwnerUserID'] = $ArrayUserInformation['UserID'];
	// AutoResponderName Field - Start
	if (isset($ArrayAPIData['autorespondername']) && $ArrayAPIData['autorespondername'] != '')
		{
		$ArrayNewAutoResponderInformation['AutoResponderName'] = $ArrayAPIData['autorespondername'];
		}
	// AutoResponderName Field - End
	// AutoResponderTriggerType Field - Start
	if (isset($ArrayAPIData['autorespondertriggertype']) && $ArrayAPIData['autorespondertriggertype'] != '')
		{
		if ($ArrayAPIData['autorespondertriggertype'] == 'OnSubscription' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberLinkClick' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberCampaignOpen' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberForwardToFriend' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberDate')
			{
			$ArrayNewAutoResponderInformation['AutoResponderTriggerType'] = $ArrayAPIData['autorespondertriggertype'];
			}
		else
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> array(7),
								 'ErrorText'		=> array('Invalid auto responder trigger type')
								);
			throw new Exception('');
			}
		}
	// AutoResponderTriggerType Field - End
		// AutoResponderTriggerValue Field - Start
		if (isset($ArrayAPIData['autorespondertriggervalue']))
			{
			$ArrayNewAutoResponderInformation['AutoResponderTriggerValue'] = $ArrayAPIData['autorespondertriggervalue'];			
			}
		// AutoResponderTriggerValue Field - End
		// AutoResponderTriggerValue2 Field - Start
		if (isset($ArrayAPIData['autorespondertriggervalue2']))
			{
			$ArrayNewAutoResponderInformation['AutoResponderTriggerValue2'] = $ArrayAPIData['autorespondertriggervalue2'];			
			}
		// AutoResponderTriggerValue2 Field - End
		// TriggerTimeType Field - Start
		if (isset($ArrayAPIData['triggertimetype']) && $ArrayAPIData['triggertimetype'] != '')
			{
			if ($ArrayAPIData['triggertimetype'] == 'Immediately' || $ArrayAPIData['triggertimetype'] == 'Seconds later' || $ArrayAPIData['triggertimetype'] == 'Minutes later' || $ArrayAPIData['triggertimetype'] == 'Hours later' || $ArrayAPIData['triggertimetype'] == 'Days later' || $ArrayAPIData['triggertimetype'] == 'Weeks later' || $ArrayAPIData['triggertimetype'] == 'Months later')
				{
				$ArrayNewAutoResponderInformation['TriggerTimeType'] = $ArrayAPIData['triggertimetype'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(8),
									 'ErrorText'		=> array('Invalid trigger time type')
									);
				throw new Exception('');
				}
			}
		// TriggerTimeType Field - End
		// TriggerTime Field - Start
		if (isset($ArrayAPIData['triggertime']) && $ArrayAPIData['triggertime'] != '')
			{
			$ArrayNewAutoResponderInformation['TriggerTime'] = $ArrayAPIData['triggertime'];
			}
		// TriggerTime Field - End
		// RelEmailID Field - Start
		if (isset($ArrayAPIData['emailid']) && $ArrayAPIData['emailid'] != '')
			{
			if ($ArrayAPIData['emailid'] != '')
				{
				// Retrieve email information - Start
				Core::LoadObject('emails');
				$ArrayEmail = Emails::RetrieveEmail(array('*'), array('RelUserID' => $ArrayUserInformation['UserID'], 'EmailID' => $ArrayAPIData['emailid']));
	
				if ($ArrayEmail == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(9),
										);
					throw new Exception('');
					}
				else
					{
					$ArrayNewAutoResponderInformation['RelEmailID'] = $ArrayAPIData['emailid'];
					}
				// Retrieve email information - End
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(8)
									);
				throw new Exception('');
				}
			}
		// RelEmailID Field - End
	// Set values - End

AutoResponders::Update($ArrayNewAutoResponderInformation, array('AutoResponderID'=>$ArrayAPIData['autoresponderid']));
// Update auto responder - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Autoresponder.Update.Post', array($ArrayAPIData['autoresponderid']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>