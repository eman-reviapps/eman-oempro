<?php
// Load other modules - Start
Core::LoadObject('lists');
// Load other modules - End

if (!isset($ArrayAPIData['recordsperrequest']) || $ArrayAPIData['recordsperrequest'] == '')
	{
	$ArrayAPIData['recordsperrequest'] = 0;
	}
if (!isset($ArrayAPIData['recordsfrom']) || $ArrayAPIData['recordsfrom'] == '')
	{
	$ArrayAPIData['recordsfrom'] = 0;
	}
if (!isset($ArrayAPIData['orderfield']) || $ArrayAPIData['orderfield'] == '')
	{
	$ArrayAPIData['orderfield'] = 'ListID';
	}
if (!isset($ArrayAPIData['ordertype']) || $ArrayAPIData['ordertype'] == '')
	{
	$ArrayAPIData['ordertype'] = 'ASC';
	}

// Get lists - Start
$ArrayStatisticsOptions = array(
	"Statistics"	=>	array(
		"EmailDomainStatistics"		=>	false,
		"BounceStatistics"			=>	false
		),
	"Days"	=>	30
	);
$ArraySubscriberLists = Lists::RetrieveLists(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID']), array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayStatisticsOptions, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
// Get lists - End

// Generate special encrypted and salted list ID for every subscriber list returned - Start
if ($ArraySubscriberLists != false)
	{
	foreach ($ArraySubscriberLists as $Key=>$ArrayEachList)
		{
		$ArraySubscriberLists[$Key]['EncryptedSaltedListID']	= md5(MD5_SALT.$ArrayEachList['ListID']);
		$ArraySubscriberLists[$Key]['SyncLastDateTime']			= ($ArrayEachList['SyncLastDateTime'] == '0000-00-00 00:00:00' ? $ArrayLanguageStrings['Screen']['9141'] : $ArrayEachList['SyncLastDateTime']);
		}
	$TotalLists = Lists::GetTotal($ArrayUserInformation['UserID']);
	}
else
	{
	$ArraySubscriberLists = array();
	$TotalLists = 0;
	}
// Generate special encrypted and salted list ID for every subscriber list returned - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalListCount'	=> $TotalLists,
					 'Lists'			=> $ArraySubscriberLists
					);
// Return results - End
?>