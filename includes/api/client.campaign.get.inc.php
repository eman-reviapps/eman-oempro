<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('campaigns');
Core::LoadObject('clients');
// Load other modules - End

// Get assigned subscriber lists of client - Start
$AssignedCampaigns = Clients::RetrieveAssignedCampaigns($ArrayClientInformation['ClientID']);
// Get assigned subscriber lists of client - End

// Check if given list id is assigned to client - Start
$IsMatch = false;
foreach ($AssignedCampaigns as $Each)
	{
	if ($Each['CampaignID'] == $ArrayAPIData['campaignid'])
		{
		$IsMatch = true;
		}
	}
// Check if given list id is assigned to client - End

// If no match, give error - Start
if ($IsMatch == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// If no match, give error - End

// Get lists - Start
$ArrayStatisticsOptions = array(
	"Statistics"	=>	array(
		"OpenStatistics"			=>	true,
		"ClickStatistics"			=>	true,
		"UnsubscriptionStatistics"	=>	true,
		"ForwardStatistics"			=>	true,
		"BrowserViewStatistics"		=>	true,
		"BounceStatistics"			=>	true,
		"ClickPerformance"			=>	true,
		"OpenPerformance"			=>	true
		),
	"Days"	=>	15
	);
$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('RelOwnerUserID'=>$ArrayClientInformation['RelOwnerUserID'],'CampaignID'=>$ArrayAPIData['campaignid']), $ArrayStatisticsOptions);
// Get lists - End

$ArrayCampaign['HardBounceRatio'] = number_format(($ArrayCampaign['TotalHardBounces'] * 100) / $ArrayCampaign['TotalSent']);
$ArrayCampaign['SoftBounceRatio'] = number_format(($ArrayCampaign['TotalSoftBounces'] * 100) / $ArrayCampaign['TotalSent']);

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Campaign'			=> $ArrayCampaign
					);
// Return results - End
?>