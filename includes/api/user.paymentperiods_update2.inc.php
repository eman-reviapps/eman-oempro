<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'userid' => 1,
    'logid' => 3,
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ArrayErrorFields,
    );
    throw new Exception('');
}
// Check for required fields - End
// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('payments');
// Load other modules - End
// Field validations - Start
// Check if user exists - Start
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayAPIData['userid']));

if ($ArrayUser == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 2,
    );
    throw new Exception('');
}

Payments::SetUser($ArrayUser);
// Check if user exists - End
// Check if payment period exists - Start
$ArrayPaymentPeriod = Payments::GetPaymentPeriod($ArrayAPIData['logid']);

if ($ArrayPaymentPeriod == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 4,
    );
    throw new Exception('');
}
// Check if payment period exists - End
// Check if include tax has a proper value - Start
if (($ArrayAPIData['includetax'] != 'Include') && ($ArrayAPIData['includetax'] != 'Exclude') && ($ArrayAPIData['includetax'] != '')) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 5,
    );
    throw new Exception('');
}
// Check if include tax has a proper value - End
// Check if payment status has a proper value - Start
if (($ArrayAPIData['paymentstatus'] != 'NA') && ($ArrayAPIData['paymentstatus'] != 'Unpaid') && ($ArrayAPIData['paymentstatus'] != 'Waiting') && ($ArrayAPIData['paymentstatus'] != 'Paid') && ($ArrayAPIData['paymentstatus'] != 'Waived') && ($ArrayAPIData['paymentstatus'] != '')) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 6,
    );
    throw new Exception('');
}
// Check if include tax has a proper value - End
// Field validations - End
// Update payment period - Start
if ($ArrayAPIData['discount'] != '') {
    $ArrayPaymentPeriod['Discount'] = $ArrayAPIData['discount'];
}

if ($ArrayAPIData['includetax'] != '') {
    if ($ArrayAPIData['includetax'] == 'Include') {
        $ArrayTotals = Payments::CalculateTotalAmountOfPaymentPeriod($ArrayPaymentPeriod, true);
    } elseif ($ArrayAPIData['includetax'] == 'Exclude') {
        $ArrayTotals = Payments::CalculateTotalAmountOfPaymentPeriod($ArrayPaymentPeriod, false);
    }
} else {
    if ($ArrayPaymentPeriod['Tax'] == 0) {
        $ArrayTotals = Payments::CalculateTotalAmountOfPaymentPeriod($ArrayPaymentPeriod, false);
    } else {
        $ArrayTotals = Payments::CalculateTotalAmountOfPaymentPeriod($ArrayPaymentPeriod, true);
    }
}

if ($ArrayAPIData['paymentstatus'] != '') {
    $ArrayPaymentPeriod['PaymentStatus'] = $ArrayAPIData['paymentstatus'];
}

$ArrayPaymentPeriod = array_merge($ArrayPaymentPeriod, $ArrayTotals);

$ArrayFieldnValues = array(
    'ChargePerCampaignSent' => $ArrayPaymentPeriod['ChargePerCampaignSent'],
    'ChargeAutoResponderPeriod' => $ArrayPaymentPeriod['ChargeAutoResponderPeriod'],
    'ChargeDesignPreviewPeriod' => $ArrayPaymentPeriod['ChargeDesignPreviewPeriod'],
    'ChargeSystemPeriod' => $ArrayPaymentPeriod['ChargeSystemPeriod'],
    'ChargeTotalCampaignRecipients' => $ArrayPaymentPeriod['ChargeTotalCampaignRecipients'],
    'ChargeTotalAutoRespondersSent' => $ArrayPaymentPeriod['ChargeTotalAutoRespondersSent'],
    'ChargeTotalDesignPrevRequests' => $ArrayPaymentPeriod['ChargeTotalDesignPrevRequests'],
    'Discount' => $ArrayPaymentPeriod['Discount'],
    'Tax' => $ArrayPaymentPeriod['Tax'],
    'TotalAmount' => $ArrayPaymentPeriod['TotalAmount'],
    'PaymentStatus' => $ArrayPaymentPeriod['PaymentStatus'],
    'PaymentStatusDate' => date('Y-m-d'),
);
Payments::Update($ArrayFieldnValues, array('LogID' => $ArrayPaymentPeriod['LogID']));
// Update payment period - End
// Retrieve the updated payment period - Start
$ArrayPaymentPeriod = Payments::GetPaymentPeriod($ArrayPaymentPeriod['LogID']);
// Retrieve the updated payment period - End
// Format values if requested - Start
if ($ArrayAPIData['returnformatted'] == 'Yes') {
    $ArrayPaymentPeriod = Payments::FormatPaymentValues($ArrayPaymentPeriod, $ArrayLanguageStrings['Config']['ShortDateFormat'], $ArrayLanguageStrings['Screen']['9111']);
}
// Format values if requested - End
// Send payment request receipt email to the user - Start
if (($ArrayAPIData['sendreceipt'] == 'Yes') && (PAYMENT_RECEIPT_EMAIL_SUBJECT != '') && (PAYMENT_RECEIPT_EMAIL_MESSAGE != '')) {
    O_Email_Sender_ForAdmin::send(
            O_Email_Factory::invoice($ArrayUser, $ArrayPaymentPeriod));
}
// Send payment request receipt email to the user - End
// Return results - Start
$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'ErrorText' => '',
    'PaymentPeriod' => $ArrayPaymentPeriod,
);
// Return results - End
?>