<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'themeid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('themes');
// Load other modules - End

// Get lists - Start
$ArrayThemes = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => $ArrayAPIData['themeid']));

if ($ArrayThemes == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Theme'			=> $ArrayThemes
					);
// Return results - End
?>