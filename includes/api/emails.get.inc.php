<?php
// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Get tags - Start
$ArrayEmails = Emails::RetrieveEmails(array('*'), array('RelUserID'=>$ArrayUserInformation['UserID']));
// Get tags - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalEmailCount'	=> ($ArrayEmails != false ? count($ArrayEmails) : 0),
					 'Emails'			=> $ArrayEmails
					);
// Return results - End
?>