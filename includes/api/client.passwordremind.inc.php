<?php
// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'forgot_password');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailaddress'	=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
if (FormHandler::EmailValidator($ArrayAPIData['emailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Field validations - End

// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Validate if such an email address exists in the database - Start
$ArrayCriterias = array(
						'ClientEmailAddress'		=> $ArrayAPIData['emailaddress'],
						);
$ArrayClient = Clients::RetrieveClient(array('*'), $ArrayCriterias);

if ($ArrayClient == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Validate if such an email address exists in the database - End

// Send password reminder email to the client - Start
$Username = $ArrayClient['ClientUsername'];
$ClientID = $ArrayClient['ClientID'];
if ($ArrayAPIData['customresetlink'] != '')
	{
	$PasswordResetLink	= sprintf(base64_decode(rawurldecode($ArrayAPIData['customresetlink'])), md5($ClientID));
	}
else
	{
	$PasswordResetLink = Core::InterfaceAppURL().'/client/reset_password.php?reset='.md5($ClientID);
	}


O_Email_Sender_ForAdmin::send(
	O_Email_Factory::passwordRemind($ArrayClient['ClientEmailAddress'], $Username, $PasswordResetLink)
);
// Send password reminder email to the client - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End
?>