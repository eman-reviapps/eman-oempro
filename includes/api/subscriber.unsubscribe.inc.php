<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'listid' => 1,
    'ipaddress' => 2,
);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (($ArrayAPIData['emailaddress'] == '') && ($ArrayAPIData['subscriberid'] == '')) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 3
    );
    throw new Exception('');
}
// Check for required fields - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('users');
Core::LoadObject('auto_responders');
Core::LoadObject('custom_fields');
Core::LoadObject('campaigns');
Core::LoadObject('emails');
// Load other modules - End
// Field validations - Start
// Validate the list ID and list ownership - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['listid']));
if ($ArraySubscriberList == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 4
    );
    throw new Exception('');
}
// Validate the list ID and list ownership - End
// Retrieve owner user information - Start
$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberList['RelOwnerUserID']));

if ($ArrayUserInformation == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 5
    );
    throw new Exception('');
}
// Retrieve owner user information - End
// Validate email address - Start
if ($ArrayAPIData['emailaddress'] != '') {
    if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 6
        );
        throw new Exception('');
    }
}
// Validate email address - End
// Check if the subscriber ID belongs to that list ID - Start
$ArrayCriteria = ($ArrayAPIData['subscriberid'] != '' ? array('SubscriberID' => $ArrayAPIData['subscriberid']) : array('EmailAddress' => $ArrayAPIData['emailaddress']));
$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), $ArrayCriteria, $ArraySubscriberList['ListID']);
if ($ArraySubscriber == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 7
    );
    throw new Exception('');
}
// Check if the subscriber ID belongs to that list ID - End
// Check if the subscriber already unsubscribed - Start
if ($ArraySubscriber['SubscriptionStatus'] == 'Unsubscribed') {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 9
    );
    throw new Exception('');
}
// Check if the subscriber already unsubscribed - End
// Validate the campaign ID and retrieve campaign information - Start
if ($ArrayAPIData['campaignid'] != '') {
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $ArrayAPIData['campaignid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));

    if ($ArrayCampaign == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 8
        );
        throw new Exception('');
    }
}
// Validate the campaign ID and retrieve campaign information - End
// Validate the email ID (if provided) and retrieve email information - Start {
if (($ArrayAPIData['emailid'] != '') && ($ArrayAPIData['emailid'] > 0)) {
    $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']), false);

    if ($ArrayEmail == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 10
        );
        throw new Exception('');
    }
}
// Validate the email ID (if provided) and retrieve email information - End }
// Validate the autoresponder ID and retrieve autoresponder information - Start
if ($ArrayAPIData['autoresponderid'] != '') {
    $ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), array('AutoResponderID' => $ArrayAPIData['autoresponderid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));

    if ($ArrayAutoResponder == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 8
        );
        throw new Exception('');
    }
}
// Validate the autoresponder ID and retrieve autoresponder information - End
// Field validations - End
// Unsubscribe the email address from the list - Start
if ($ArrayAPIData['preview'] != 1) {
    // Unsubscribe the email address from the list - Start
    Subscribers::Unsubscribe($ArraySubscriberList, $ArrayUserInformation['UserID'], (isset($ArrayCampaign) == true ? $ArrayCampaign['CampaignID'] : 0), (isset($ArrayAutoResponder) == true ? $ArrayAutoResponder['AutoResponderID'] : 0), $ArraySubscriber['EmailAddress'], $ArraySubscriber['SubscriberID'], $ArrayAPIData['ipaddress'], ($ArrayAPIData['channel'] != '' ? $ArrayAPIData['channel'] : ''), ($ArrayAPIData['emailid'] > 0 ? $ArrayAPIData['emailid'] : 0));
    // Unsubscribe the email address from the list - End
    // Add into suppression list if set in the subscriber list settings - Start
    if ($ArraySubscriberList['OptOutAddToSuppressionList'] == 'Yes') {
        Core::LoadObject('suppression_list');

        $ArrayFieldnValues = array(
            'SuppressionID' => '',
            'RelListID' => $ArraySubscriberList['ListID'],
            'RelOwnerUserID' => $ArrayUserInformation['UserID'],
            'SuppressionSource' => 'User',
            'EmailAddress' => $ArraySubscriber['EmailAddress'],
            'PrevSubscriptionStatus' => 'Unsubscribed',
            'SourceList' => $ArraySubscriberList['ListID'],
        );
        $NewSuppressionID = SuppressionList::Add($ArrayFieldnValues);
    }
    // Add into suppression list if set in the subscriber list settins - End
    // Add into global suppression list if set in the subscriber list settings - Start
    if ($ArraySubscriberList['OptOutAddToGlobalSuppressionList'] == 'Yes') {
        Core::LoadObject('suppression_list');

        $ArrayFieldnValues = array(
            'SuppressionID' => '',
            'RelListID' => 0,
            'RelOwnerUserID' => $ArrayUserInformation['UserID'],
            'SuppressionSource' => 'User',
            'EmailAddress' => $ArraySubscriber['EmailAddress'],
            'PrevSubscriptionStatus' => 'Unsubscribed',
            'SourceList' => $ArraySubscriberList['ListID'],
        );
        $NewSuppressionID = SuppressionList::Add($ArrayFieldnValues);
    }
    // Add into global suppression list if set in the subscriber list settins - End
    // Check unsubscription behavior of list and behave - Start
    if (SUBSCRIBE_ON_UNSUBSCRIPTION_ENABLED) {
        if ($ArraySubscriberList['OptOutSubscribeTo'] != 0 && $ArraySubscriberList['OptOutSubscribeTo'] != '') {
            $ArrayBehaviorSubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArraySubscriberList['OptOutSubscribeTo']));
            if ($ArrayBehaviorSubscriberList != false) {
                Subscribers::AddSubscriber_Enhanced(array(
                    'UserInformation' => $ArrayUserInformation,
                    'ListInformation' => $ArrayBehaviorSubscriberList,
                    'EmailAddress' => $ArraySubscriber['EmailAddress'],
                    'IPAddress' => $ArrayAPIData['ipaddress'],
                    'OtherFields' => array(), // Other fields are not sent becuase behavior list may not have same custom fields
                    'UpdateIfDuplicate' => true,
                    'UpdateIfUnsubscribed' => true,
                    'ApplyBehaviors' => true,
                    'SendConfirmationEmail' => false,
                    'UpdateStatistics' => true,
                    'TriggerWebServices' => false,
                    'TriggerAutoResponders' => false
                ));
            }
        }
    }

    if ($ArraySubscriberList['OptOutUnsubscribeFrom'] != 0 && $ArraySubscriberList['OptOutUnsubscribeFrom'] != '') {
        $ArrayBehaviorSubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArraySubscriberList['OptOutUnsubscribeFrom']));
        if ($ArrayBehaviorSubscriberList != false) {
            $ArrayBehaviorSubscriber = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $ArraySubscriber['EmailAddress']), $ArrayBehaviorSubscriberList['ListID']);
            if ($ArrayBehaviorSubscriber != false) {
                Subscribers::Unsubscribe($ArrayBehaviorSubscriberList, $ArrayUserInformation['UserID'], 0, 0, $ArrayBehaviorSubscriber['EmailAddress'], $ArrayBehaviorSubscriber['SubscriberID'], $ArrayAPIData['ipaddress'], '', ($ArrayAPIData['emailid'] > 0 ? $ArrayAPIData['emailid'] : 0));
            }
        }
    }
    // Check unsubscription behavior of list and behave - End

    if ($ArraySubscriberList['SendActivityNotification'] == 'true') {
        $ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $ArraySubscriber['SubscriberID']), $ArraySubscriberList['ListID']);
        O_Email_Sender_ForAdmin::send(
                O_Email_Factory::unsubscriptionNotification(
                        $ArrayUserInformation, $ArraySubscriberInformation, $ArraySubscriberList
                )
        );
    }
}
// Unsubscribe the email address from the list - End
// Return results - Start
$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'RedirectURL' => ($ArraySubscriberList['UnsubscriptionConfirmedPageURL'] != '' ? $ArraySubscriberList['UnsubscriptionConfirmedPageURL'] : ''),
);
// Return results - End
?>