<?php
// Load other modules - Start
Core::LoadObject('templates');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'templatename'			=> 1,
								);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// At least one of the email content must be provided (HTML or plain) - Start
if (($ArrayAPIData['templatehtmlcontent'] == '') && ($ArrayAPIData['templateplaincontent'] == ''))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// At least one of the email content must be provided (HTML or plain) - End
// Field validations - End

// Set thumbnail and thumbnail type - Start
if (($ArrayAPIData['templatethumbnailpath'] != '') && (file_exists(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']) == true))
 	{
	$ArrayThumbnailInfo = getimagesize(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']);
	$ThumbnailType = $ArrayThumbnailInfo['mime'];
	}
// Set thumbnail and thumbnail type - End

// Create the new campaign record - Start
$ArrayFieldnValues = array(
						'TemplateID'				=> '',
						'TemplateName'				=> $ArrayAPIData['templatename'],
						'TemplateDescription'		=> ($ArrayAPIData['templatedescription'] != '' ? $ArrayAPIData['templatedescription'] : ''),
						'TemplateSubject'			=> ($ArrayAPIData['templatesubject'] != '' ? $ArrayAPIData['templatesubject'] : ''),
						'TemplateHTMLContent'		=> ($ArrayAPIData['templatehtmlcontent'] != '' ? $ArrayAPIData['templatehtmlcontent'] : ''),
						'TemplatePlainContent'		=> ($ArrayAPIData['templateplaincontent'] != '' ? $ArrayAPIData['templateplaincontent'] : ''),
						'TemplateThumbnail'			=> ($ArrayAPIData['templatethumbnailpath'] != '' && file_exists(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']) == true ? base64_encode(file_get_contents(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath'])) : ''),
						'TemplateThumbnailType'		=> $ThumbnailType,
						'RelOwnerUserID'			=> ((AdminAuth::IsLoggedIn(false, false) == true) ? ($ArrayAPIData['relowneruserid'] != '' ? $ArrayAPIData['relowneruserid'] : 0) : $ArrayUserInformation['UserID']),
						'IsCreatedByAdmin'			=> ((AdminAuth::IsLoggedIn(false, false) == true && UserAuth::IsLoggedIn(false, false) == false) ? 1 : 0)
						);
$NewTemplateID = Templates::Create($ArrayFieldnValues);
// Create the new campaign record - End

// If thumbnail is provided, delete the temporary file - Start
if (file_exists(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']) == true)
	{
	unlink(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']);
	}
// If thumbnail is provided, delete the temporary file - End


// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Template.Create.Post', array($NewEmailID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'TemplateID'		=> $NewTemplateID
					);
// Return results - End
?>