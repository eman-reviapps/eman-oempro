<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('campaigns');
// Load other modules - End

// Get campaign - Start
$days = 15;
$ArrayStatisticsOptions = array();

if (isset($ArrayAPIData['retrievestatistics']) && $ArrayAPIData['retrievestatistics'] == true)
	{
	$ArrayStatisticsOptions = array(
		"Statistics"	=>	array("OpenStatistics", "ClickStatistics", "UnsubscriptionStatistics", "ForwardStatistics", "BrowserViewStatistics", "BounceStatistics", "ClickPerformance", "OpenPerformance"),
		"Subscribers"	=>	array("SubscribersOpened", "SubscribersClicked"),
		"Days"			=>	$days
		);
	}
else
	{
	$ArrayStatisticsOptions = array();
	}
$RetrieveTags	= (isset($ArrayAPIData['retrievetags']) ? $ArrayAPIData['retrievetags'] : false);
// $ArrayCampaign	= Campaigns::RetrieveCampaign(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'CampaignID'=>$ArrayAPIData['campaignid']), $ArrayStatisticsOptions, $RetrieveTags);
$ArrayCampaign	= Campaigns::RetrieveCampaigns_Enhanced(
	array(
		'Criteria'	=> array(
			array('Column' => '%c%.RelOwnerUserID', 'Operator' => '=', 'ValueWOQuote' => $ArrayUserInformation['UserID']),
			array('Column' => '%c%.CampaignID', 'Operator' => '=', 'ValueWOQuote' => $ArrayAPIData['campaignid'], 'Link' => 'AND')
			),
		'Content'	=> true,
		'SplitTest'	=> true,
		'Reports'	=> $ArrayStatisticsOptions
	));

if (count($ArrayCampaign) < 1)
	{
	$ArrayCampaign = array();
	}
else
	{
	$ArrayCampaign = $ArrayCampaign[0];
	$ArrayCampaign['HardBounceRatio'] = number_format(($ArrayCampaign['TotalHardBounces'] * 100) / $ArrayCampaign['TotalSent']);
	$ArrayCampaign['SoftBounceRatio'] = number_format(($ArrayCampaign['TotalSoftBounces'] * 100) / $ArrayCampaign['TotalSent']);
	}
// Get campaign - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Campaign'			=> $ArrayCampaign
					);
// Return results - End
?>