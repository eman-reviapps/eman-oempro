<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'segments'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Segment ids are missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('segments');
// Load other modules - End

// Delete segments - Start
Segments::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['segments']));
// Delete segments - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>