<?php
// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'orderfield'					=> 1,
							'ordertype'						=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Get lists - Start
$ArrayClients = Clients::RetrieveClients(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID']), array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']));
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalClientCount'	=> Clients::GetTotal($ArrayUserInformation['UserID']),
					 'Clients'			=> $ArrayClients
					);
// Return results - End
?>