<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

Core::LoadObject('emails');

$isSuccess = O_Email_Sender_ForAdmin::send(
	O_Email_Factory::adminDeliverySettingsTest());

if ($isSuccess !== TRUE)
	{
	$ArrayOutput = array('Success'						=> false,
						 'ErrorCode'					=> 1,
						 'EmailSettingsErrorMessage'	=> $isSuccess,
						);
	throw new Exception('');
	}
else
	{
	$ArrayOutput = array('Success'						=> true
						);
	throw new Exception('');
	}
?>