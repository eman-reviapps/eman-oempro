<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'clientid'				=> 1,
							'subscriberlistids'		=> 2
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Assign subscriber lists to client - Start
Clients::AssignSubscriberLists($ArrayAPIData['clientid'], explode(',', $ArrayAPIData['subscriberlistids']));
// Assign subscriber lists to client - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>