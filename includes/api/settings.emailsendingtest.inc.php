<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Check for required fields - Start
$ArrayRequiredFields = array();
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Enum validation - Start
if (isset($ArrayAPIData['send_method']))
	{
	if (($ArrayAPIData['send_method'] != 'SMTP') && ($ArrayAPIData['send_method'] != 'LocalMTA') && ($ArrayAPIData['send_method'] != 'PHPMail') && ($ArrayAPIData['send_method'] != 'PowerMTA') && ($ArrayAPIData['send_method'] != 'SaveToDisk'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['send_method_smtp_secure']))
	{
	if (($ArrayAPIData['send_method_smtp_secure'] != 'ssl') && ($ArrayAPIData['send_method_smtp_secure'] != 'tls') && ($ArrayAPIData['send_method_smtp_secure'] != ''))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['send_method_smtp_auth']))
	{
	if (($ArrayAPIData['send_method_smtp_auth'] != 'true') && ($ArrayAPIData['send_method_smtp_auth'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['mail_engine']))
	{
	if (($ArrayAPIData['mail_engine'] != 'phpmailer') && ($ArrayAPIData['mail_engine'] != 'swiftmailer'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
// Enum validation - End

// Check if email sending settings set correctly - Start
if (isset($ArrayAPIData['send_method']))
	{
	$ArrayResult = array();
	Core::LoadObject('emails');

	if ($ArrayAPIData['send_method'] == 'SMTP')
		{
		$SendMethod				= 'SMTP';
		$SMTPHost				= $ArrayAPIData['send_method_smtp_host'];
		$SMTPPort				= $ArrayAPIData['send_method_smtp_port'];
		$SMTPSecure				= $ArrayAPIData['send_method_smtp_secure'];
		$SMTPAuth				= ($ArrayAPIData['send_method_smtp_auth'] == 'true' ? true : false);
		$SMTPUsername			= $ArrayAPIData['send_method_smtp_username'];
		$SMTPPassword			= $ArrayAPIData['send_method_smtp_password'];
		$SMTPTimeout			= $ArrayAPIData['send_method_smtp_timeout'];
		$SMTPDebug				= false;
		$SMTPKeepAlive			= true;
		$LocalMTAPath			= '';

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif ($ArrayAPIData['send_method'] == 'LocalMTA')
		{
		$SendMethod				= 'LocalMTA';
		$SMTPHost				= '';
		$SMTPPort				= '';
		$SMTPSecure				= '';
		$SMTPAuth				= '';
		$SMTPUsername			= '';
		$SMTPPassword			= '';
		$SMTPTimeout			= '';
		$SMTPDebug				= '';
		$SMTPKeepAlive			= '';
		$LocalMTAPath			= $ArrayAPIData['send_method_localmta_path'];

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif ($ArrayAPIData['send_method'] == 'PHPMail')
		{
		$SendMethod				= 'PHPMail';
		$SMTPHost				= '';
		$SMTPPort				= '';
		$SMTPSecure				= '';
		$SMTPAuth				= '';
		$SMTPUsername			= '';
		$SMTPPassword			= '';
		$SMTPTimeout			= '';
		$SMTPDebug				= '';
		$SMTPKeepAlive			= '';
		$LocalMTAPath			= '';

		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
		}
	elseif (($ArrayAPIData['send_method'] == 'PowerMTA') || ($ArrayAPIData['send_method'] == 'SaveToDisk'))
		{
		// Add / to the end of paths if it does not exist - Start
		if ($ArrayAPIData['send_method'] == 'PowerMTA')
			{
			if (substr($ArrayAPIData['send_method_powermta_dir'], strlen($ArrayAPIData['send_method_powermta_dir']) - 1, strlen($ArrayAPIData['send_method_powermta_dir'])) != '/')
				{
				$ArrayAPIData['send_method_powermta_dir'] = $ArrayAPIData['send_method_powermta_dir'].'/';
				}
			}
		else
			{
			if (substr($ArrayAPIData['send_method_savetodisk_dir'], strlen($ArrayAPIData['send_method_savetodisk_dir']) - 1, strlen($ArrayAPIData['send_method_savetodisk_dir'])) != '/')
				{
				$ArrayAPIData['send_method_savetodisk_dir'] = $ArrayAPIData['send_method_savetodisk_dir'].'/';
				}
			}
		// Add / to the end of paths if it does not exist - End
		
		if (is_writable(($ArrayAPIData['send_method'] == 'PowerMTA' ? $ArrayAPIData['send_method_powermta_dir'] : $ArrayAPIData['send_method_savetodisk_dir'])) == false)
			{
			$ArrayResult = array(false, $ArrayLanguageStrings['Screen']['9097']);
			}
		else
			{
			$ArrayResult = array(true);
			}
		}

	if ($ArrayResult[0] == false)
		{
		$ArrayOutput = array('Success'						=> false,
							 'ErrorCode'					=> 1,
							 'EmailSettingsErrorMessage'	=> $ArrayResult[1],
							);
		throw new Exception('');
		}
	}
// Check if email sending settings set correctly - End
// Field validations - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End


?>