<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Missing subscriber list id',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('segments');
// Load other modules - End

// Get segments - Start
$ArraySegments = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'RelListID'=>$ArrayAPIData['subscriberlistid']), array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']));
// Get segments - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalSegmentCount'	=> Segments::GetTotal($ArrayAPIData['subscriberlistid']),
					 'Segments'	=> $ArraySegments
					);
// Return results - End
?>