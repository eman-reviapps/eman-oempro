<?php
// Load other modules - Start
Core::LoadObject('web_service_integration');
Core::LoadObject('custom_fields');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Subscriber list id is missing',
						);
	throw new Exception('');
	}
// Check for required fields - End

if (!isset($ArrayAPIData['htmlspecialchars'])) {
	$ArrayAPIData['htmlspecialchars'] = 'true';
}

// Generate subscription form html code - Start
$HTMLCode  = array();
$HTMLCode[] = '<form action="'.APP_URL.'subscribe.php" method="post" accept-charset="UTF-8">';
$HTMLCode[] = '<div class="o-form-row">';
$HTMLCode[] = '<label for="FormValue_EmailAddress">'.(isset($ArrayAPIData['emailaddressstring']) ? $ArrayAPIData['emailaddressstring'] : $ArrayLanguageStrings['Screen']['EmailAddress']).'</label>';
$HTMLCode[] = '<input type="text" name="FormValue_Fields[EmailAddress]" value="" id="FormValue_EmailAddress">';
$HTMLCode[] = '</div>';

// Add custom fields - Start
$ArraySelectedCustomFields = explode(',', $ArrayAPIData['customfields']);
if (count($ArraySelectedCustomFields) > 0)
	{
	foreach ($ArraySelectedCustomFields as $EachCustomFieldID)
		{
		$ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $EachCustomFieldID, array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$ArrayAPIData['subscriberlistid'].')', 'auto-quote' => false)));
		if ($ArrayCustomField != false)
			{
				if ($ArrayCustomField['FieldType'] == 'Hidden field')
					{
					$HTMLCode[] = CustomFields::GenerateCustomFieldHTMLCode($ArrayCustomField);
					}
				else
					{
					$HTMLCode[] = '<div class="o-form-row">';
					$HTMLCode[] = '<label for="FormValue_CustomField'.$ArrayCustomField['CustomFieldID'].'">'.$ArrayCustomField['FieldName'].'</label>';
					$HTMLCode[] = CustomFields::GenerateCustomFieldHTMLCode($ArrayCustomField);
					$HTMLCode[] = '</div>';
					}
			}
		}
	}
// Add custom fields - End

$HTMLCode[] = '<input type="submit" name="FormButton_Subscribe" value="'.(isset($ArrayAPIData['subscribebuttonstring']) ? $ArrayAPIData['subscribebuttonstring'] : $ArrayLanguageStrings['Screen']['Subscribe']).'" id="FormButton_Subscribe">';
$HTMLCode[] = '<input type="hidden" name="FormValue_ListID" value="'.$ArrayAPIData['subscriberlistid'].'">';
$HTMLCode[] = '<input type="hidden" name="FormValue_Command" value="Subscriber.Add" id="FormValue_Command">';

$HTMLCode[] = '</form>';
// Generate subscription form html code - End

if ($ArrayAPIData['htmlspecialchars'] == 'true') {
	foreach ($HTMLCode as $Key=>&$Each) {
		$Each = htmlspecialchars($Each);
	}
}

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'HTMLCode'			=> $HTMLCode
					);
// Return results - End
?>