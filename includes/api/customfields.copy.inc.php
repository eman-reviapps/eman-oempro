<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'sourcelistid'		=> 1,
							'targetlistid'		=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing source subscriber list id',
								"2" => 'Missing target subscriber list id'
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('custom_fields');
Core::LoadObject('lists');
// Load other modules - End

// Get custom fields of source list - Start
$ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'], array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$ArrayAPIData['sourcelistid'].')', 'auto-quote' => false)), array());
// Get custom fields of source list - End

// Get source subscriber list information - Start
$ArraySourceSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['sourcelistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArraySourceSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid source subscriber id')
						);
	throw new Exception('');
	}
// Get source subscriber list information - End

// Get target subscriber list information - Start
$ArrayTargetSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['targetlistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArrayTargetSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid target subscriber id')
						);
	throw new Exception('');
	}
// Get target subscriber list information - End

// Copy each custom field to target subscriber list - Start
foreach ($ArrayCustomFields as $Each)
	{
	CustomFields::Create(array(
		'RelOwnerUserID' => $ArrayTargetSubscriberListInformation['RelOwnerUserID'],
		'RelListID' => $ArrayAPIData['targetlistid'],
		'FieldName' => $Each['FieldName'],
		'FieldType' => $Each['FieldType'],
		'FieldDefaultValue' => $Each['FieldDefaultValue'],
		'FieldOptions' => $Each['FieldOptions'],
		'ValidationMethod' => $Each['ValidationMethod'],
		'ValidationRule' => $Each['ValidationRule'],
		'IsRequired' => $Each['IsRequired'],
		'IsUnique' => $Each['IsUnique'],
		'Visibility' => $Each['Visibility'],
		'IsGlobal' => $Each['IsGlobal']
		));
	}
// Copy each custom field to target subscriber list - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>