<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Missing subscriber list id',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('custom_fields');
// Load other modules - End

// Get custom fields - Start
$ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'], array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$ArrayAPIData['subscriberlistid'].')', 'auto-quote' => false)), array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']));
// Get custom fields - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalFieldCount'	=> CustomFields::GetTotal($ArrayAPIData['subscriberlistid']),
					 'CustomFields'		=> $ArrayCustomFields
					);
// Return results - End
?>