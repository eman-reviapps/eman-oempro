<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'attachmentid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('attachments');
// Load other modules - End

// Delete attachment - Start
Attachments::Delete(array('RelOwnerUserID'=>$ArrayUserInformation['UserID'], 'AttachmentID'=>$ArrayAPIData['attachmentid']));
// Delete attachment - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Attachment.Delete.Post', array($ArrayUserInformation['UserID'], $ArrayAPIData['attachmentid']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>