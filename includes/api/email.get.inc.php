<?php
// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Create the new campaign record - Start
$ArrayEmailInformation = Emails::RetrieveEmail(array('*'), array('EmailID'=>$ArrayAPIData['emailid'],'RelUserID'=>$ArrayUserInformation['UserID']));
// Create the new campaign record - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'EmailInformation'	=> $ArrayEmailInformation
					);
// Return results - End
?>