<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'lists'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Subscriber list ids are missing',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('lists');
// Load other modules - End

// Delete lists - Start
Lists::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['lists']));
// Delete lists - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'List.Delete.Post', explode(',', $ArrayAPIData['lists']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>