<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Load other modules - Start
Core::LoadObject('templates');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'templates'			=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
$IsAdminLoggedIn	= AdminAuth::IsLoggedIn(false,false);
$IsUserLoggedIn		= UserAuth::IsLoggedIn(false,false);

// Loop every template ID and check if it can be deleted by user or not - Start
$TMPArrayTemplateIDs 	= explode(',', $ArrayAPIData['templates']);
$ArrayTemplateIDs		= array();

foreach ($TMPArrayTemplateIDs as $EachTemplateID)
	{
	// Retrieve template information - Start
	$ArrayTemplate = Templates::RetrieveTemplate(array('*'), array('TemplateID' => $EachTemplateID), 'AND');
	
	if ($ArrayTemplate == false)
		{
		continue;
		}
	// Retrieve template information - End
	
	// Global templates can only be deleted by admin - Start
	if (($ArrayTemplate['RelOwnerUserID'] == 0) && ($IsAdminLoggedIn == true))
		{
		$ArrayTemplateIDs[] = $EachTemplateID;
		}
	elseif ((($ArrayTemplate['RelOwnerUserID'] == $ArrayUserInformation['UserID']) && ($IsUserLoggedIn == true)) || ($IsAdminLoggedIn == true))
		{
		$ArrayTemplateIDs[] = $EachTemplateID;
		}
	// Global templates can only be deleted by admin - End
	}
// Loop every template ID and check if it can be deleted by user or not - End

// Check if any template ID is suitable for deleting - Start
if (count($ArrayTemplateIDs) == 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// Check if any template ID is suitable for deleting - End
// Field validations - End

// Delete templates - Start
Templates::Delete($ArrayTemplateIDs);
// Delete templates - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Template.Delete.Post', array($ArrayAPIData['templates']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					);
// Return results - End
?>