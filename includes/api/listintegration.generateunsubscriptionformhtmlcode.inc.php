<?php
// Load other modules - Start
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Subscriber list id is missing',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Generate unsubscription form html code - Start
$HTMLCode  = array();
$HTMLCode[] = htmlspecialchars('<form action="'.APP_URL.'unsubscribe.php" method="post">');
$HTMLCode[] = htmlspecialchars('<input type="text" name="FormValue_EmailAddress" value="" id="FormValue_EmailAddress">');
$HTMLCode[] = htmlspecialchars('<input type="submit" name="FormButton_Unsubscribe" value="'.(isset($ArrayAPIData['unsubscribebuttonstring']) ? $ArrayAPIData['unsubscribebuttonstring'] : $ArrayLanguageStrings['Screen']['Unsubscribe']).'" id="FormButton_Unsubscribe">');
$HTMLCode[] = htmlspecialchars('<input type="hidden" name="FormValue_ListID" value="'.$ArrayAPIData['subscriberlistid'].'" id="FormValue_ListID">');
$HTMLCode[] = htmlspecialchars('<input type="hidden" name="FormValue_Command" value="Subscriber.Unsubscribe" id="FormValue_Command">');
$HTMLCode[] = htmlspecialchars('</form>');
// Generate unsubscription form html code - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'HTMLCode'			=> $HTMLCode
					);
// Return results - End
?>