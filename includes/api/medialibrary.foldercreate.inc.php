<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
	'foldername'		=> 1,
	'parentfolderid'	=> 2,
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
{
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> $ArrayErrorFields
	);
	throw new Exception('');
}
// Check for required fields - End

$mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');

if ($ArrayAPIData['parentfolderid'] != 0) {
	$parentFolder = $mediaLibraryFolderMapper->findById($ArrayAPIData['parentfolderid'], $ArrayUserInformation['UserID']);
	if (! $parentFolder) {
		$ArrayOutput = array(
			'Success'	=> false,
			'ErrorCode'	=> 3
		);
		throw new Exception('');
	}
}

$newFolder = new O_Domain_MediaLibraryFolder();
$newFolder->setName($ArrayAPIData['foldername']);
$newFolder->setFolderId($ArrayAPIData['parentfolderid']);
$newFolder->setUserId($ArrayUserInformation['UserID']);

$mediaLibraryFolderMapper->insert($newFolder);

// Return results - Start
$ArrayOutput = array(
	'Success'	=> true,
	'ErrorCode'	=> 0,
	'FolderID'	=> $newFolder->getId()
);
// Return results - End
?>