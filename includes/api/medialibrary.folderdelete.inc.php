<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
	'folderid'		=> 1
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
{
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> $ArrayErrorFields
	);
	throw new Exception('');
}
// Check for required fields - End

$mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');
$mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');

$folder = $mediaLibraryFolderMapper->findById($ArrayAPIData['folderid'], $ArrayUserInformation['UserID']);
if (! $folder) {
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> 2
	);
	throw new Exception('');
}

if ($folder->getUserId() != $ArrayUserInformation['UserID']) {
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> 3
	);
	throw new Exception('');
}

function deleteFolderAndItsContents($folderId) {
	$mediaLibraryFolderMapper = O_Registry::instance()->getMapper('MediaLibraryFolder');
	$mediaLibraryFileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');

	$mediaLibraryFolderMapper->deleteByFolderId($folderId);
	$filesInFolder = $mediaLibraryFileMapper->findByFolderId($folderId);

	if ($filesInFolder != false) {
		foreach ($filesInFolder as $eachFile) {
			$mediaLibraryFileMapper->deleteById($eachFile->getId());

			if (MEDIA_UPLOAD_METHOD == 'file') {
				unlink(DATA_PATH.'media/'.md5($eachFile->getId()));
			} elseif (MEDIA_UPLOAD_METHOD == 's3') {
				Core::LoadObject('aws_s3');

				$ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
				$ObjectS3->deleteObject(S3_BUCKET, S3_MEDIALIBRARY_PATH.'/'.md5($eachFile->getUserId()).'/'.md5($eachFile->getId()));
			}
		}
	}

	$foldersInFolder = $mediaLibraryFolderMapper->findByFolderId($folderId, $ArrayUserInformation['UserID']);
	if ($foldersInFolder != false) {
		foreach ($foldersInFolder as $eachFolder) {
			deleteFolderAndItsContents($eachFolder->getId());
		}
	}
}

deleteFolderAndItsContents($ArrayAPIData['folderid']);

// Return results - Start
$ArrayOutput = array(
	'Success'	=> true,
	'ErrorCode'	=> 0
);
// Return results - End
?>