<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'sourcelistid'		=> 1,
							'targetlistid'		=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('segments');
Core::LoadObject('lists');
// Load other modules - End

// Get segments of source list - Start
$ArraySegments = Segments::RetrieveSegments(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'RelListID'=>$ArrayAPIData['sourcelistid']), array());
// Get segments of source list - End

// Get source subscriber list information - Start
$ArraySourceSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['sourcelistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArraySourceSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid source subscriber id')
						);
	throw new Exception('');
	}
// Get source subscriber list information - End

// Get target subscriber list information - Start
$ArrayTargetSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['targetlistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArrayTargetSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid target subscriber id')
						);
	throw new Exception('');
	}
// Get target subscriber list information - End

// Copy each segment to target subscriber list - Start
foreach ($ArraySegments as $Each)
	{
	// FIXME: There's a small bug here. While copying segment (and rules) from other list, if a rule based on a custom field of source list is defined, it will be copied too but that custom field belongs to the source list. A small bug.
	Segments::Create(array(
		'RelOwnerUserID'		=> $ArrayTargetSubscriberListInformation['RelOwnerUserID'],
		'RelListID'				=> $ArrayAPIData['targetlistid'],
		'SegmentName'			=> $Each['SegmentName'],
		'SegmentOperator'		=> $Each['SegmentOperator'],
		'SegmentRules'			=> $Each['SegmentRules'],
		));
	}
// Copy each segment to target subscriber list - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>