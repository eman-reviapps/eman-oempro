<?php
$mode = 'ById';

// Check for required fields - Start
if (UserAuth::IsLoggedIn(false,false) == false)
	{
	if (! isset($ArrayAPIData['userid']))
		{
		$ArrayRequiredFields = array('emailaddress' => 1);
		$mode = 'ByEmail';
		}
	else
		{
		$ArrayRequiredFields = array('userid' => 2);
		}
	}

$ArrayRequiredFields['month'] = 3;

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
$MonthIsValid = preg_match("/\d{4}-\d{2}/uim", $ArrayAPIData['month']);
if (! $MonthIsValid)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4,
						);
	throw new Exception('');
	}
// Field validations - End

// Load other modules - Start
Core::LoadObject('users');
// Load other modules - End

// Get user - Start
$criteria = array();
if ($mode == 'ByEmail')
	{
	$criteria['EmailAddress'] = $ArrayAPIData['emailaddress'];
	}
else if ($mode == 'ById')
	{
	$criteria['UserID'] = UserAuth::IsLoggedIn(false,false) == false ? $ArrayAPIData['userid'] : $ArrayUserInformation['UserID'];
	}
$ArrayUser = Users::RetrieveUser(array('*'), $criteria, true);

if (! $ArrayUser)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5,
						);
	throw new Exception('');
	}

// Get user - End

Core::LoadObject('statistics');
Core::LoadObject('campaigns');
Core::LoadObject('users');
Core::LoadObject('lists');

$EmailsSentInPeriod = Statistics::RetrieveEmailSendingAmountFromActivityLog($ArrayUser['UserID'], date($ArrayAPIData['month'] . '-01 00:00:00'), date($ArrayAPIData['month'] . '-31 23:59:59'));
$CampaignsSentInPeriod = Campaigns::RetrieveCampaigns_Enhanced(array(
	'ReturnTotalRows' => true,
	'Criteria' => array(
		array('Column'=>'RelOwnerUserID','Operator'=>'=','Value'=>$ArrayUser['UserID']),
		array('Column'=>'SendProcessStartedOn','Operator'=>'>=','Value'=>date($ArrayAPIData['month'] . '-01 00:00:00'),'Link'=>'AND'),
		array('Column'=>'SendProcessStartedOn','Operator'=>'<=','Value'=>date($ArrayAPIData['month'] . '-31 23:59:59'),'Link'=>'AND')
	)
));
$TotalLists = Users::TotalUserLists($ArrayUser['UserID']);
//$TotalSubscribers = Subscribers::TotalSubscribersOnTheAccount($ArrayUser['UserID']);
//Edited By Eman Mohammed
$TotalSubscribers = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($ArrayUser['UserID']);


// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'CampaignsSent'	=> $CampaignsSentInPeriod,
					 'EmailsSent'		=> $EmailsSentInPeriod,
					 'Subscribers'		=> $TotalSubscribers,
					 'Lists'			=> $TotalLists
					);
// Return results - End
?>