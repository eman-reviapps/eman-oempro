<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
	'userid' => 1,
	'credits' => 2
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('users');
// Load other modules - End

// Get user - Start
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayAPIData['userid']), false);
if (! $ArrayUser)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						);
	throw new Exception('');
	}

$TotalCredits = (int) $ArrayUser['AvailableCredits'] + (int) $ArrayAPIData['credits'];
Users::UpdateUser(array('AvailableCredits' => $TotalCredits), array('UserID' => $ArrayUser['UserID']));

// Get user - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'TotalCredits'		=> $TotalCredits
					);
// Return results - End
?>