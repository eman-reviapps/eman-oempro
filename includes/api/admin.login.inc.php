<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'username'		=> 1,
							'password'		=> 2
							);

if (ADMIN_CAPTCHA == true && (!isset($ArrayAPIData['disablecaptcha'])))
	{
	$ArrayRequiredFields['captcha'] = 4;
	}

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// If captcha is enabled, verify it - Start
if ((!isset($ArrayAPIData['disablecaptcha'])) && (ADMIN_CAPTCHA == true) && ($ArrayAPIData['captcha'] != $_SESSION[SESSION_NAME]['AdminCaptcha']))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(5),
						);
	throw new Exception('');
	}
// If captcha is enabled, verify it - End
// Field validations - End

// Load other modules - Start
Core::LoadObject('admins');
Core::LoadObject('admin_auth');
// Load other modules - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Administrator.Login.Pre', array($ArrayAPIData['username'], $ArrayAPIData['password']));
// Plug-in hook - End

// Validate if such a username/password pair exists in the database - Start
$ArrayCriterias = array(
						'Username'		=> $ArrayAPIData['username'],
						'Password'		=> md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['password'].OEMPRO_PASSWORD_SALT),
						);
$ArrayAdmin = Admins::RetrieveAdmin(array('*'), $ArrayCriterias);

if ($ArrayAdmin == false)
	{
	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Administrator.Login.Fail', array($ArrayAPIData['username'], $ArrayAPIData['password']));
	// Plug-in hook - End

	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3,
						);
	throw new Exception('');
	}
// Validate if such a username/password pair exists in the database - End

// Perform the admin login - Start
AdminAuth::Login($ArrayAdmin['AdminID'], $ArrayAdmin['Username'], $ArrayAdmin['Password']);
// Perform the admin login - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Administrator.Login.Post', $ArrayAdmin);
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'SessionID'		=> session_id(),
					);
foreach ($ArrayAdmin as $Key=>$Value)
	{
	$ArrayOutput['AdminInfo'][$Key] = $Value;
	}
// Return results - End
?>