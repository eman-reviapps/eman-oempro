<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'segmentid'				=> 1,
							'segmentname'			=> 2,
							'segmentoperator'		=> 3,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing segment id',
								"2" => 'Missing segment name',
								"3" => 'Missing segment operator',
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('segments');
// Load other modules - End

// Check if segment exists - Start
$ArraySegmentInformation = Segments::RetrieveSegment(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'SegmentID'=>$ArrayAPIData['segmentid']));
if ($ArraySegmentInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid segment id')
						);
	throw new Exception('');
	}
// Check if segment exists - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Update segment - Start
	// Set values - Start
	$ArrayNewSegmentInformation = array();
	$ArrayNewSegmentInformation['RelListID'] = $ArrayAPIData['subscriberlistid'];
	$ArrayNewSegmentInformation['RelOwnerUserID'] = $ArrayUserInformation['UserID'];
		// SegmentName Field - Start
		if (isset($ArrayAPIData['segmentname']) && $ArrayAPIData['segmentname'] != '')
			{
			$ArrayNewSegmentInformation['SegmentName'] = $ArrayAPIData['segmentname'];
			}
		// SegmentName Field - End
		// SegmentName Field - Start
		if (isset($ArrayAPIData['segmentoperator']) && $ArrayAPIData['segmentoperator'] != '')
			{
			if ($ArrayAPIData['segmentoperator'] == 'and' || $ArrayAPIData['segmentoperator'] == 'or')
				{
				$ArrayNewSegmentInformation['SegmentOperator'] = $ArrayAPIData['segmentoperator'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(5),
									 'ErrorText'		=> array('Invalid segment operator')
									);
				throw new Exception('');
				}
			}
		// SegmentName Field - End
		
		// Prepare segment rules as string - Start
		$ArrayNewSegmentInformation['SegmentRules'] = '';
		foreach ($ArrayAPIData['segmentrulefield'] as $Key=>$Value)
			{
			$ArrayNewSegmentInformation['SegmentRules'] .= Segments::GetRuleAsString($Value, $ArrayAPIData['segmentruleoperator'][$Key], $ArrayAPIData['segmentrulefilter'][$Key]);
			$ArrayNewSegmentInformation['SegmentRules'] .= ',,,';
			}
		$ArrayNewSegmentInformation['SegmentRules'] = substr($ArrayNewSegmentInformation['SegmentRules'], 0, $ArrayNewSegmentInformation['SegmentRules'].length - 3);
		// Prepare segment rules as string - End
	// Set values - End

Segments::Update($ArrayNewSegmentInformation, array('SegmentID'=>$ArrayAPIData['segmentid']));
// Update segment - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>