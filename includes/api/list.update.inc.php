<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Subscriber list id is missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('lists');
Core::LoadObject('subscribers');
// Load other modules - End

// Check if subscriber list exists - Start
$ArrayListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['subscriberlistid'], 'RelOwnerUserID'=>$ArrayUserInformation['UserID']));
if ($ArrayListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						 'ErrorText'		=> array('Invalid subscriber list id')
						);
	throw new Exception('');
	}
// Check if subscriber list exists - End

// Update list - Start
	// Set values - Start
	$ArrayListInformation = array();
		// Name Field - Start
		if (isset($ArrayAPIData['name']) && $ArrayAPIData['name'] != '') 
			{ 
			$ArrayListInformation['Name'] = $ArrayAPIData['name']; 
			}
		// Name Field - End
		// OptInMode Field - Start
		if (isset($ArrayAPIData['optinmode']) && $ArrayAPIData['optinmode'] != '') 
			{
			if ($ArrayUserInformation['GroupInformation']['ForceOptInList'] == 'Enabled')
				{
				$ArrayListInformation['OptInMode'] = 'Double';
				}
			else
				{
				if ($ArrayAPIData['optinmode'] == 'Single' || $ArrayAPIData['optinmode'] == 'Double')
					{
					$ArrayListInformation['OptInMode'] = $ArrayAPIData['optinmode'];
					}
				else
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(3),
										 'ErrorText'		=> array('Invalid opt in mode')
										);
					throw new Exception('');
					}
				}
			}
		// OptInMode Field - End
		
		// If opt in mode is "double", check the opt-in email - Start
		if (isset($ArrayAPIData['optinconfirmationemailid']))
			{
			$ArrayListInformation['RelOptInConfirmationEmailID'] = $ArrayAPIData['optinconfirmationemailid'];			
			}
		// If opt in mode is "double", check the opt-in email - End
		
		// OptOutAddToSuppressionList Field - Start
		if (isset($ArrayAPIData['optoutaddtosuppressionlist'])) 
			{
			if ($ArrayAPIData['optoutaddtosuppressionlist'] == 'Yes' || $ArrayAPIData['optoutaddtosuppressionlist'] == 'No' ||  $ArrayAPIData['optoutaddtosuppressionlist'] == '')
				{
				$ArrayListInformation['OptOutAddToSuppressionList'] = ($ArrayAPIData['optoutaddtosuppressionlist'] == 'Yes' ? 'Yes' : 'No');
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(10),
									 'ErrorText'		=> array('Invalid opt out suppression list option')
									);
				throw new Exception('');
				}
			}
		// OptOutAddToSuppressionList Field - End

		// OptOutAddToSuppressionList Field - Start
		if (isset($ArrayAPIData['optoutaddtoglobalsuppressionlist'])) 
			{
			if ($ArrayAPIData['optoutaddtoglobalsuppressionlist'] == 'Yes' || $ArrayAPIData['optoutaddtoglobalsuppressionlist'] == 'No' ||  $ArrayAPIData['optoutaddtoglobalsuppressionlist'] == '')
				{
				$ArrayListInformation['OptOutAddToGlobalSuppressionList'] = ($ArrayAPIData['optoutaddtoglobalsuppressionlist'] == 'Yes' ? 'Yes' : 'No');
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(10),
									 'ErrorText'		=> array('Invalid opt out global suppression list option')
									);
				throw new Exception('');
				}
			}
		// OptOutAddToSuppressionList Field - End

		// HideInSubscriberArea Field - Start
		if (isset($ArrayAPIData['hideinsubscriberarea'])) 
			{
			if ($ArrayAPIData['hideinsubscriberarea'] == 'true' || $ArrayAPIData['hideinsubscriberarea'] == 'false' || $ArrayAPIData['hideinsubscriberarea'] == '')
				{
				$ArrayListInformation['hideinsubscriberarea'] = ($ArrayAPIData['hideinsubscriberarea'] == '' ? 'false' : $ArrayAPIData['hideinsubscriberarea']);
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(6),
									 'ErrorText'		=> array('Invalid setting')
									);
				throw new Exception('');
				}
			}
		// HideInSubscriberArea Field - End
		// SendServiceIntegrationFailedNotification Field - Start
		if (isset($ArrayAPIData['sendserviceintegrationfailednotification'])) 
			{
			if ($ArrayAPIData['sendserviceintegrationfailednotification'] == 'true' || $ArrayAPIData['sendserviceintegrationfailednotification'] == 'false' || $ArrayAPIData['sendserviceintegrationfailednotification'] == '')
				{
				$ArrayListInformation['SendServiceIntegrationFailedNotification'] = ($ArrayAPIData['sendserviceintegrationfailednotification'] == '' ? 'false' : $ArrayAPIData['sendserviceintegrationfailednotification']);
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(5),
									 'ErrorText'		=> array('Invalid send service integration notification setting')
									);
				throw new Exception('');
				}
			}
		// SendServiceIntegrationFailedNotification Field - End
		// SendActivityNotification Field - Start
		if (isset($ArrayAPIData['sendactivitynotification'])) 
			{
			if ($ArrayAPIData['sendactivitynotification'] == 'true' || $ArrayAPIData['sendactivitynotification'] == 'false' || $ArrayAPIData['sendactivitynotification'] == '')
				{
				$ArrayListInformation['SendActivityNotification'] = ($ArrayAPIData['sendactivitynotification'] == '' ? 'false' : $ArrayAPIData['sendactivitynotification']);
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(5),
									 'ErrorText'		=> array('Invalid send activity notification setting')
									);
				throw new Exception('');
				}
			}
		// SendActivityNotification Field - End
		// SubscriptionConfirmationPendingPageURL Field - Start
		if (isset($ArrayAPIData['subscriptionconfirmationpendingpageurl'])) 
			{ 
			$ArrayListInformation['SubscriptionConfirmationPendingPageURL'] = $ArrayAPIData['subscriptionconfirmationpendingpageurl']; 
			}
		// SubscriptionConfirmationPendingPageURL Field - End
		// SubscriptionConfirmedPageURL Field - Start
		if (isset($ArrayAPIData['subscriptionconfirmedpageurl'])) 
			{ 
			$ArrayListInformation['SubscriptionConfirmedPageURL'] = $ArrayAPIData['subscriptionconfirmedpageurl']; 
			}
		// SubscriptionConfirmedPageURL Field - End
		// SubscriptionErrorPageURL Field - Start
		if (isset($ArrayAPIData['subscriptionerrorpageurl']))
			{
			$ArrayListInformation['SubscriptionErrorPageURL'] = $ArrayAPIData['subscriptionerrorpageurl'];
			}
		// SubscriptionErrorPageURL Field - End
		// UnsubscriptionConfirmedPageURL Field - Start
		if (isset($ArrayAPIData['unsubscriptionconfirmedpageurl'])) 
			{ 
			$ArrayListInformation['UnsubscriptionConfirmedPageURL'] = $ArrayAPIData['unsubscriptionconfirmedpageurl']; 
			}
		// UnsubscriptionConfirmedPageURL Field - End
		// UnsubscriptionErrorPageURL Field - Start
		if (isset($ArrayAPIData['unsubscriptionerrorpageurl']))
			{
			$ArrayListInformation['UnsubscriptionErrorPageURL'] = $ArrayAPIData['unsubscriptionerrorpageurl'];
			}
		// unsubscriptionErrorPageURL Field - End

		// Request by email settings - Start		
		if (($ArrayAPIData['reqbyemailsearchtoaddress'] != '') && ($ArrayAPIData['reqbyemailsubscriptioncommand'] != '') && ($ArrayAPIData['reqbyemailunsubscriptioncommand'] != ''))
			{
			if (isset($ArrayAPIData['reqbyemailsearchtoaddress'])) 
				{ 
				if (Subscribers::ValidateEmailAddress($ArrayAPIData['reqbyemailsearchtoaddress']) == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(9),
										);
					throw new Exception('');
					}
				else
					{
					$ArrayListInformation['ReqByEmailSearchToAddress'] = $ArrayAPIData['reqbyemailsearchtoaddress']; 
					}
				}
			if (isset($ArrayAPIData['reqbyemailsubscriptioncommand'])) 
				{ 
				$ArrayListInformation['ReqByEmailSubscriptionCommand'] = $ArrayAPIData['reqbyemailsubscriptioncommand']; 
				}
			if (isset($ArrayAPIData['reqbyemailunsubscriptioncommand'])) 
				{ 
				$ArrayListInformation['ReqByEmailUnsubscriptionCommand'] = $ArrayAPIData['reqbyemailunsubscriptioncommand']; 
				}
			}
		// Request by email settings - End

		// Data synchronization settings - Start
		if (isset($ArrayAPIData['syncstatus'])) 
			{
			$ArrayListInformation['SyncStatus'] = $ArrayAPIData['syncstatus'] == 'Enabled' ? 'Enabled' : 'Disabled';

			if (isset($ArrayAPIData['syncperiod']) && isset($ArrayAPIData['syncsendreportemail']))
				{
				if (($ArrayAPIData['syncperiod'] != '') && ($ArrayAPIData['syncsendreportemail'] != ''))
					{
					$ArrayListInformation['SyncPeriod'] 			= $ArrayAPIData['syncperiod'];
					$ArrayListInformation['SyncSendReportEmail']	= ($ArrayAPIData['syncsendreportemail'] == 'Yes' ? 'Yes' : 'No');
					}
				else
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(8),
										);
					throw new Exception('');
					}
				}

			if (isset($ArrayAPIData['syncmysqlport']) && isset($ArrayAPIData['syncmysqlusername']) && isset($ArrayAPIData['syncmysqldbname']) && isset($ArrayAPIData['syncmysqlquery']) && isset($ArrayAPIData['syncfieldmapping']))
				{
				$ArrayListInformation['SyncMySQLHost']			= $ArrayAPIData['syncmysqlhost'];
				$ArrayListInformation['SyncMySQLPort']			= $ArrayAPIData['syncmysqlport'];
				$ArrayListInformation['SyncMySQLUsername']		= $ArrayAPIData['syncmysqlusername'];
				$ArrayListInformation['SyncMySQLPassword']		= $ArrayAPIData['syncmysqlpassword'];
				$ArrayListInformation['SyncMySQLDBName']		= $ArrayAPIData['syncmysqldbname'];
				$ArrayListInformation['SyncMySQLQuery']			= $ArrayAPIData['syncmysqlquery'];
				$ArrayListInformation['SyncFieldMapping']		= $ArrayAPIData['syncfieldmapping'];
				$ArrayListInformation['SyncLastDateTime']		= ($ArrayListInformation['SyncLastDateTime'] != '0000-00-00 00:00:00' ? $ArrayListInformation['SyncLastDateTime'] : '0000-00-00 00:00:00');
				}
			}
		// Data synchronization settings - End

		// Unsubscription behavior settings - Start
			// OptOutScope Field - Start
			if (isset($ArrayAPIData['optoutscope'])) 
				{
				if ($ArrayAPIData['optoutscope'] == 'This list' || $ArrayAPIData['optoutscope'] == 'All lists' ||  $ArrayAPIData['optoutscope'] == '')
					{
					$ArrayListInformation['OptOutScope'] = ($ArrayAPIData['optoutscope'] == 'All lists' ? $ArrayAPIData['optoutscope'] : 'This list');
					}
				else
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(4),
										 'ErrorText'		=> array('Invalid opt out scope')
										);
					throw new Exception('');
					}
				}
			// OptOutScope Field - End

			// OptOutSubscribeTo Field - Start
			if (isset($ArrayAPIData['optoutsubscribeto'])) 
				{
				$ArrayListInformation['OptOutSubscribeTo'] = ($ArrayAPIData['optoutsubscribeto'] == '' ? 0 : $ArrayAPIData['optoutsubscribeto']);
				}
			// OptOutSubscribeTo Field - End

			// OptOutUnsubscribeFrom Field - Start
			if (isset($ArrayAPIData['optoutunsubscribefrom'])) 
				{
				$ArrayListInformation['OptOutUnsubscribeFrom'] = ($ArrayAPIData['optoutunsubscribefrom'] == '' ? 0 : $ArrayAPIData['optoutunsubscribefrom']);
				}
			// OptOutUnsubscribeFrom Field - End
		// Unsubscription behavior settings - End
		
		// Subscription behavior settings - Start
			// OptInSubscribeTo Field - Start
			if (isset($ArrayAPIData['optinsubscribeto'])) 
				{
				$ArrayListInformation['OptInSubscribeTo'] = ($ArrayAPIData['optinsubscribeto'] == '' ? 0 : $ArrayAPIData['optinsubscribeto']);
				}
			// OptInSubscribeTo Field - End

			// OptInUnsubscribeFrom Field - Start
			if (isset($ArrayAPIData['optinunsubscribefrom'])) 
				{
				$ArrayListInformation['OptInUnsubscribeFrom'] = ($ArrayAPIData['optinunsubscribefrom'] == '' ? 0 : $ArrayAPIData['optinunsubscribefrom']);
				}
			// OptInUnsubscribeFrom Field - End
		// Subscription behavior settings - End
		
	// Set values - End

Lists::Update(
	$ArrayListInformation,
	array('ListID'=>$ArrayAPIData['subscriberlistid'],'RelOwnerUserID'=>$ArrayUserInformation['UserID'])
	);
// Update list - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'List.Update.Post', array($ArrayAPIData['subscriberlistid']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>