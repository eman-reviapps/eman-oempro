<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'tag'						=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('tags');
// Load other modules - End

// Field validations - Start
// Check if tag already exists in the system - Start
$TotalFound = Tags::RetrieveTags(array('COUNT(*) AS TotalFound'), array('Tag' => $ArrayAPIData['tag'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Check if tag already exists in the system - End
// Field validations - End

// Create Tag - Start
$NewTagID = Tags::Create(array(
	'RelOwnerUserID'=>	$ArrayUserInformation['UserID'],
	'Tag'			=>	$ArrayAPIData['tag']
	));
// Create Tag - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TagID'			=> $NewTagID
					);
// Return results - End
?>