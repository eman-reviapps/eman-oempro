<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'userid'				=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('payments');
// Load other modules - End

// Field validations - Start
// Check if user exists - Start
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayAPIData['userid']));

if ($ArrayUser == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}

Payments::SetUser($ArrayUser);
// Check if user exists - End

// Validate payment status value - Start
if (($ArrayAPIData['paymentstatus'] != '') && ($ArrayAPIData['paymentstatus'] != 'NA') && ($ArrayAPIData['paymentstatus'] != 'Unpaid') && ($ArrayAPIData['paymentstatus'] != 'Waiting') && ($ArrayAPIData['paymentstatus'] != 'Paid') && ($ArrayAPIData['paymentstatus'] != 'Waived'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3,
						);
	throw new Exception('');
	}
// Validate payment status value - End

// Field validations - End

// Retrieve user periods and totals - Start
$ArrayUserPaymentPeriods = Payments::GetPaymentPeriods(($ArrayAPIData['paymentstatus'] != '' ? $ArrayAPIData['paymentstatus'] : ''));
// Retrieve user periods and totals - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'PaymentPeriods'	=> ''
					);
foreach ($ArrayUserPaymentPeriods as $Key=>$Value)
	{
	// Format values if requested - Start
	if ($ArrayAPIData['returnformatted'] == 'Yes')
		{
		$Value = Payments::FormatPaymentValues($Value, $ArrayLanguageStrings['Config']['ShortDateFormat'], $ArrayLanguageStrings['Screen']['9111']);
		}
	// Format values if requested - End
	
	$ArrayOutput['PaymentPeriods'][$Key] = $Value;
	}

// Return results - End

?>