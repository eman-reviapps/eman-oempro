<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'clientname'				=> 1,
							'clientusername'			=> 2,
							'clientpassword'			=> 3,
							'clientemailaddress'		=> 4,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('clients');
Core::LoadObject('subscribers');
// Load other modules - End

// Field validations - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['clientemailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5,
						);
	throw new Exception('');
	}

// Check if username already exists in the system - Start
$TotalFound = Clients::RetrieveClients(array('COUNT(*) AS TotalFound'), array('ClientUsername' => $ArrayAPIData['clientusername']), array('ClientID'=>'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 6,
						);
	throw new Exception('');
	}
// Check if username already exists in the system - End

// Check if email address already exists in the system - Start
$TotalFound = Clients::RetrieveClients(array('COUNT(*) AS TotalFound'), array('ClientEmailAddress' => $ArrayAPIData['clientemailaddress']), array('ClientID'=>'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7,
						);
	throw new Exception('');
	}
// Check if email address already exists in the system - End

// Field validations - End

// Create Client - Start
$NewClientID = Clients::Create(array(
	'RelOwnerUserID'=>	$ArrayUserInformation['UserID'],
	'ClientUsername'=>	$ArrayAPIData['clientusername'],
	'ClientPassword'=>	md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['clientpassword'].OEMPRO_PASSWORD_SALT),
	'ClientEmailAddress' =>	$ArrayAPIData['clientemailaddress'],
	'ClientName'	=>	$ArrayAPIData['clientname'],
	'ClientAccountStatus'	=>	'Enabled'
	));
// Create Client - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Client.Create.Post', array($NewClientID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'ClientID'			=> $NewClientID
					);
// Return results - End
?>