<?php
$IsAPIKeyCall = isset($ArrayAPIData['apikey']) && $ArrayAPIData['apikey'] != '';

// Check for required fields - Start
if ($IsAPIKeyCall)
	{
	$ArrayRequiredFields = array();
	}
else
	{
	$ArrayRequiredFields = array(
								'username'		=> 1,
								'password'		=> 2
								);
	}

if ($IsAPIKeyCall == false && USER_CAPTCHA == true && (!isset($ArrayAPIData['disablecaptcha'])))
	{
	$ArrayRequiredFields['captcha'] = 4;
	}

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// If captcha is enabled, verify it - Start
if ($IsAPIKeyCall === false && (!isset($ArrayAPIData['disablecaptcha'])) && (USER_CAPTCHA == true) && ($ArrayAPIData['captcha'] != $_SESSION[SESSION_NAME]['UserCaptcha']))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(5),
						);
	throw new Exception('');
	}
// If captcha is enabled, verify it - End
// Field validations - End

// Validate if such a username/password pair exists in the database - Start
if ($IsAPIKeyCall)
	{
	$ArrayUser = Users::RetrieveUserWithAPIKey($ArrayAPIData['apikey']);
	}
else
	{
	$ArrayCriterias = array(
							'Username'		=> $ArrayAPIData['username'],
							'Password'		=> md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['password'].OEMPRO_PASSWORD_SALT),
							'AccountStatus'	=> 'Enabled'
							);
	$ArrayUser = Users::RetrieveUser(array('*'), $ArrayCriterias);
	}

if ($ArrayUser == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						 'ErrorText'		=> array('Invalid login information')
						);
	throw new Exception('');
	}
// Validate if such a username/password pair exists in the database - End

// Perform the user login - Start
UserAuth::Login($ArrayUser['UserID'], $ArrayUser['Username'], $ArrayUser['Password']);
// Perform the user login - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'User.Login.Post', array($ArrayUser['UserID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'SessionID'		=> session_id(),
					 'UserInfo'			=> array(),
					);

// Add the user information to the API output but hide the user group information for security risk (smtp passwords, etc.)
$ArrayUser['GroupInformation'] = null;
$ArrayUser['Password'] = '****** masked ******';
$ArrayUser['PreviewMyEmailAccount'] = null;
$ArrayUser['PreviewMyEmailAPIKey'] = null;

foreach ($ArrayUser as $Key=>$Value)
	{
	$ArrayOutput['UserInfo'][$Key] = $Value;
	}
// Return results - End

?>