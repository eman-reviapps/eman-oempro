<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'listid'		=> 1,
							'subscriberid'	=> 2,
							'mode'			=> 3,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('users');
// Load other modules - End

// Field validations - Start
// Validate the list ID and list ownership - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['listid']));
if ($ArraySubscriberList == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4
						);
	throw new Exception('');
	}
// Validate the list ID and list ownership - End

// Retrieve owner user information - Start
$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberList['RelOwnerUserID']));
if ($ArrayUserInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5
						);
	throw new Exception('');
	}
// Retrieve owner user information - End

// Check if the subscriber ID belongs to that list ID - Start
$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $ArrayAPIData['subscriberid']), $ArraySubscriberList['ListID']);
if ($ArraySubscriber == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 6
						);
	throw new Exception('');
	}
// Check if the subscriber ID belongs to that list ID - End

// Check if subscriber is opt-in pending or not - Start
if ($ArraySubscriber['SubscriptionStatus'] != 'Opt-In Pending')
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 8
						);
	throw new Exception('');
	}
// Check if subscriber is opt-in pending or not - End

// Validate the mode - Start
if (($ArrayAPIData['mode'] != 'Confirm') && ($ArrayAPIData['mode'] != 'Reject'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7
						);
	throw new Exception('');
	}
// Validate the mode - End
// Field validations - End

// Perform opt-in confirmation or reject - Start
if ($ArrayAPIData['mode'] == 'Confirm')
	{
	// Update subscriber information - Start	
	$ArrayFieldAndValues = array(
								'SubscriptionStatus'		=> 'Subscribed',
								'SubscriptionDate'			=> date('Y-m-d H:i:s'),
								);
	Subscribers::Update($ArraySubscriberList['ListID'], $ArrayFieldAndValues, array('SubscriberID' => $ArraySubscriber['SubscriberID']));
	// Update subscriber information - End

	// Update statistics - Start
	Core::LoadObject('statistics');
	$ArrayActivities = array(
							'TotalSubscriptions'		=> 1,
							);
	Statistics::UpdateListActivityStatistics($ArraySubscriberList['ListID'], $ArrayUserInformation['UserID'], $ArrayActivities);
	// Update statistics - End

	// Trigger web services - Start
	Core::LoadObject('web_service_integration');
	$ArrayWebServices = WebServiceIntegration::RetrieveURLs(array('*'), array('RelListID' => $ArraySubscriberList['ListID'], 'RelOwnerUserID' => $ArrayUserInformation['UserID'], 'EventType' => 'subscription'), array('ServiceURL'=>'ASC'));

	if (count($ArrayWebServices) > 0)
		{
		foreach ($ArrayWebServices as $Key=>$ArrayEachWebService)
			{
			WebServiceIntegration::WebServiceConnection('Subscription', $ArraySubscriber, $ArrayEachWebService['ServiceURL']);
			}
		}
	// Trigger web services - End

	// Trigger auto-responders - Start
	Core::LoadObject('auto_responders');
	$TotalRegisteredAutoResponders = AutoResponders::RegisterAutoResponders($ArraySubscriberList['ListID'], $ArraySubscriber['SubscriberID'], $ArrayUserInformation['UserID'], 'OnSubscription');
	// Trigger auto-responders - End

	// Apply list behaviors - Start {
		// Subscribe to behavior - Start {
		$DoesHaveSubscribeToBehavior = $ArraySubscriberList['OptInSubscribeTo'] != 0 && $ArraySubscriberList['OptInSubscribeTo'] != '';
		if ($DoesHaveSubscribeToBehavior)
			{
			$BehaviorSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $ArraySubscriberList['OptInSubscribeTo']));
			if ($BehaviorSubscriberListInformation != false)
				{
				Subscribers::AddSubscriber_Enhanced(array(
					'UserInformation'		=> $ArrayUserInformation,
					'ListInformation'		=> $BehaviorSubscriberListInformation,
					'EmailAddress'			=> $ArraySubscriber['EmailAddress'],
					'IPAddress'				=> $ArraySubscriber['SubscriptionIP'],
					'OtherFields'			=> array(), // Other fields are not sent becuase behavior list may not have same custom fields
					'UpdateIfDuplicate'		=> true,
					'UpdateIfUnsubscribed'	=> true,
					'ApplyBehaviors'		=> true,
					'SendConfirmationEmail'	=> false,
					'UpdateStatistics'		=> true,
					'TriggerWebServices'	=> false,
					'TriggerAutoResponders'	=> false
					));
				}
			}
		// Subscribe to behavior - End }

		// Unsubscribe from behavior - Start {
		$DoesHaveUnsubscribeToBehavior = $ArraySubscriberList['OptInUnsubscribeFrom'] != 0 && $ArraySubscriberList['OptInUnsubscribeFrom'] != '';
		if ($DoesHaveUnsubscribeToBehavior)
			{
			$BehaviorUnsubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID' => $ArraySubscriberList['OptInUnsubscribeFrom']));
			if ($BehaviorUnsubscriberListInformation != false)
				{
				$BehaviorSubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress'=>$ArraySubscriber['EmailAddress']), $BehaviorUnsubscriberListInformation['ListID']);
				if ($BehaviorSubscriberInformation != false)
					{
					Subscribers::Unsubscribe($BehaviorUnsubscriberListInformation, $ArrayUserInformation['UserID'], 0, 0, $BehaviorSubscriberInformation['EmailAddress'], $BehaviorSubscriberInformation['SubscriberID'], $ArraySubscriber['SubscriptionIP'], '');
					}
				}
			}
		// Unsubscribe from behavior - End }
	// Apply list behaviors - End }
	}
elseif ($ArrayAPIData['mode'] == 'Reject')
	{
	Subscribers::RemoveSubscribersByID($ArraySubscriberList['ListID'], array($ArraySubscriber['SubscriberID']));
	}
// Perform opt-in confirmation or reject - End

// Retrieve subscriber information - Start {
$SubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $ArrayAPIData['subscriberid']), $ArraySubscriberList['ListID']);
// Retrieve subscriber information - End }

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ProcessedMode'	=> $ArrayAPIData['mode'],
					 'RedirectURL'		=> ($ArrayAPIData['mode'] == 'Confirm' ? $ArraySubscriberList['SubscriptionConfirmedPageURL'] : ''),
					 'Subscriber'		=> $SubscriberInformation
					);
// Return results - End

?>