<?php
// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Retrieve assigned assigned lists - Start
$ArrayCampaigns = Clients::RetrieveAssignedCampaigns($ArrayClientInformation['ClientID'], true);
// Retrieve assigned assigned lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'Campaigns'		=> $ArrayCampaigns,
					 'TotalCampaigns'	=> count($ArrayCampaigns)
					);
// Return results - End
?>