<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberid'		=> 1,
							'subscriberlistid'	=> 2
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}

if (isset($ArrayAPIData['emailaddress']) && ($ArrayAPIData == ''))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
// Load other modules - End

// Check if subscriber list belongs to user and exists - Start
$ArraySubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['subscriberlistid']));
if ($ArraySubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(5)
						);
	throw new Exception('');
	}
// Check if subscriber list belongs to user and exists - End

if (isset($ArraySubscriberInformation) && count($ArraySubscriberInformation) > 0)
	{
	$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberListInformation['RelOwnerUserID']));
	}

// Check if subscriber exists - Start
$ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID'=>$ArrayAPIData['subscriberid']), $ArrayAPIData['subscriberlistid']);
if ($ArraySubscriberInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(6)
						);
	throw new Exception('');
	}
// Check if subscriber exists - End

$ArrayFieldAndValues = array();

// Field validations - Start
if (isset($ArrayAPIData['emailaddress']) && ($ArrayAPIData['emailaddress'] != ''))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> array(4)
							);
		throw new Exception('');
		}
	}

// Validate custom field values - Start
Core::LoadObject('custom_fields');
$ArrayCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], array('field' => 'RelListID', 'operator' => ' IN ', 'value' => '(0, '.$ArraySubscriberListInformation['ListID'].')', 'auto-quote' => false)), array('FieldName'=>'ASC'));

$requiredFieldErrors = array();
$uniqueValueErrors = array();
$customValidationErrors = array();
$customValidationMessages = array();
foreach ($ArrayCustomFields as $Key=>$ArrayEachCustomField)
	{
	// Custom fields: check required custom fields - Start
	if ($ArrayEachCustomField['IsRequired'] == 'Yes')
		{
		if ($ArrayAPIData['fields']['CustomField'.$ArrayEachCustomField['CustomFieldID']] == '')
			{
			$requiredFieldErrors[$ArrayEachCustomField['CustomFieldID']] = $ArrayEachCustomField['FieldName'];
			continue;
			}
		}
	// Custom fields: check required custom fields - End

	// Custom fields: check validation - Start
	if ($ArrayEachCustomField['ValidationMethod'] != 'Disabled')
		{
		$ArrayReturn = CustomFields::ValidateCustomFieldValue($ArrayEachCustomField, $ArrayAPIData['fields']['CustomField'.$ArrayEachCustomField['CustomFieldID']]);

		if ($ArrayReturn[0] == false)
			{
			$customValidationErrors[$ArrayEachCustomField['CustomFieldID']] = $ArrayEachCustomField['FieldName'];
			$customValidationMessages[] = $ArrayReturn[1];
			continue;
			}
		}
	// Custom fields: check validation - End

	// Custom fields: check unique custom fields - Start
	if ($ArrayEachCustomField['IsUnique'] == 'Yes')
		{
		if (Subscribers::CheckCustomFieldValueUniqueness($ArrayEachCustomField, $ArrayAPIData['fields']['CustomField'.$ArrayEachCustomField['CustomFieldID']], $ArraySubscriberInformation['SubscriberID'], $ArrayAPIData['subscriberlistid'], $ArraySubscriberInformation['EmailAddress']) == false)
			{
			$uniqueValueErrors[$ArrayEachCustomField['CustomFieldID']] = $ArrayEachCustomField['FieldName'];
			continue;
			}
		}
	// Custom fields: check unique custom fields - End
	}
if (count($requiredFieldErrors) > 0)
	{
	$ArrayOutput = array('Success'					=> false,
						 'ErrorCode'				=> 8,
						 'ErrorCustomFieldIDs'		=> implode(',', array_keys($requiredFieldErrors)),
						 'ErrorCustomFieldTitles'	=> implode(',', array_values($requiredFieldErrors)),
						);
	throw new Exception('');
	}
if (count($uniqueValueErrors) > 0)
	{
	$ArrayOutput = array('Success'					=> false,
						 'ErrorCode'				=> 9,
						 'ErrorCustomFieldIDs'		=> implode(',', array_keys($uniqueValueErrors)),
						 'ErrorCustomFieldTitles'	=> implode(',', array_values($uniqueValueErrors)),
						);
	throw new Exception('');
	}
if (count($customValidationErrors) > 0)
	{
	$ArrayOutput = array('Success'					=> false,
						 'ErrorCode'				=> 10,
						 'ErrorCustomFieldIDs'		=> implode(',', array_keys($customValidationErrors)),
						 'ErrorCustomFieldTitles'	=> implode(',', array_values($customValidationErrors)),
						 'ErrorCustomFieldDescriptions'	=> implode(',', array_values($customValidationMessages)),
						);
	throw new Exception('');
	}
// Validate custom field values - End

// Field validations - End

// Fields setup - Start
$IsEmailUpdated = false;
if (isset($ArrayAPIData['emailaddress']) && ($ArrayAPIData['emailaddress'] != ''))
	{
	$ArrayFieldAndValues['EmailAddress'] = $ArrayAPIData['emailaddress'];
	$IsEmailUpdated = true;
	}

$GlobalCustomFields = array();
foreach ($ArrayAPIData['fields'] as $Key=>$Value)
	{
	$CustomFieldID = substr($Key, 11);
	if (! preg_match("/^[\d]+$/uiUs", $CustomFieldID)) continue;
	$ArrayCustomField = CustomFields::RetrieveField(array('*'), array('CustomFieldID' => $CustomFieldID, 'RelOwnerUserID' => $ArrayUserInformation['UserID']));

	$NewValue = '';
	if (strtolower(gettype($Value)) == 'array')
		{
		if ($ArrayCustomField['FieldType'] == 'Time field')
			{
			$NewValue = implode(':', $Value).':00';
			}
		else if ($ArrayCustomField['FieldType'] == 'Date field')
			{
			$NewValue = $Value[2].'-'.$Value[1].'-'.$Value[0];
			}
		else
			{
			$NewValue = implode('||||', $Value);
			}
		}
	else
		{
		$NewValue = $Value;
		}

	$NewValue = mysql_real_escape_string($NewValue);
	
	if ($ArrayCustomField['IsGlobal'] == 'Yes')
		{
		$ArrayCustomField['PostValue'] = $NewValue;
		$GlobalCustomFields[] = $ArrayCustomField;
		}
	else
		{
		$ArrayFieldAndValues[$Key] = $NewValue;
		}
	}
// Fields setup - End

// Update subscriber - Start
if (count($ArrayFieldAndValues) > 0)
	{
	$Return = Subscribers::Update($ArrayAPIData['subscriberlistid'], $ArrayFieldAndValues, array('SubscriberID'=>$ArrayAPIData['subscriberid']));
	if (strtolower(gettype($Return)) == 'array')
		{
		if ($Return[1] == 'invalid')
			{
			$ArrayOutput = array('Success'				=> false,
								 'ErrorCode'			=> 4,
								);
			}
		elseif ($Return[1] == 'duplicate')
			{
			$ArrayOutput = array('Success'				=> false,
								 'ErrorCode'			=> 7,
								);
			}
		throw new Exception('');
		}
	}

if ($IsEmailUpdated)
	{
	$ArrayLists = Lists::RetrieveLists(array('ListID'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID']));
	foreach ($ArrayLists as $EachList)
		{
		Subscribers::Update($EachList['ListID'], array('EmailAddress'=>$ArrayFieldAndValues['EmailAddress']), array('EmailAddress'=>$ArraySubscriberInformation['EmailAddress']));
		}
	Database::$Interface->UpdateRows(array('EmailAddress'=>$ArrayFieldAndValues['EmailAddress']), array('EmailAddress'=>$ArraySubscriberInformation['EmailAddress']), MYSQL_TABLE_PREFIX.'custom_field_values');
	}

if (count($GlobalCustomFields) > 0)
	{
	foreach ($GlobalCustomFields as $EachField)
		{
		CustomFields::UpdateGlobalFieldData($ArrayUserInformation['UserID'], 0, ($IsEmailUpdated ? $ArrayFieldAndValues['EmailAddress'] : $ArraySubscriberInformation['EmailAddress']), $EachField, $EachField['PostValue']);
		}
	}
// Update subscriber - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>