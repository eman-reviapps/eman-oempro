<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'customfieldid'			=> 1,
							'fieldname'				=> 2,
							'fieldtype'				=> 3
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing custom field id',
								"2" => 'Missing field name',
								"3" => 'Missing field type'
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}

if ($ArrayAPIData['validationmethod'] == 'Date' || $ArrayAPIData['validationmethod'] == 'Time' || $ArrayAPIData['validationmethod'] == 'Custom')
	{
	$ArrayRequiredFields = array(
								'validationrule'		=> 4
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							 'ErrorText'		=> array('Missing validation rule')
							);
		throw new Exception('');
		}
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('custom_fields');
// Load other modules - End

// Check if custom field exists - Start
$ArrayCustomFieldInformation = CustomFields::RetrieveField(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'CustomFieldID'=>$ArrayAPIData['customfieldid']));
if ($ArrayCustomFieldInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(6),
						 'ErrorText'		=> array('Invalid custom field id')
						);
	throw new Exception('');
	}
// Check if custom field exists - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Update custom field - Start
	// Set values - Start
	$ArrayNewCustomFieldInformation = array();
	$ArrayNewCustomFieldInformation['RelListID'] = $ArrayAPIData['subscriberlistid'];
	$ArrayNewCustomFieldInformation['RelOwnerUserID'] = $ArrayUserInformation['UserID'];
		// FieldName Field - Start
		if (isset($ArrayAPIData['fieldname']) && $ArrayAPIData['fieldname'] != '')
			{
			$ArrayNewCustomFieldInformation['FieldName'] = str_replace(array("\n", "\r"), array('', ''), trim($ArrayAPIData['fieldname']));
			}
		// FieldName Field - End
		// FieldType Field - Start
		if (isset($ArrayAPIData['fieldtype']) && $ArrayAPIData['fieldtype'] != '')
			{
			if ($ArrayAPIData['fieldtype'] == 'Single line' || $ArrayAPIData['fieldtype'] == 'Paragraph text' || $ArrayAPIData['fieldtype'] == 'Multiple choice' || $ArrayAPIData['fieldtype'] == 'Drop down' || $ArrayAPIData['fieldtype'] == 'Checkboxes' || $ArrayAPIData['fieldtype'] == 'Hidden field' || $ArrayAPIData['fieldtype'] == 'Date field' || $ArrayAPIData['fieldtype'] == 'Time field')
				{
				$ArrayNewCustomFieldInformation['FieldType'] = $ArrayAPIData['fieldtype'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(7),
									 'ErrorText'		=> array('Invalid field type')
									);
				throw new Exception('');
				}
			}
		// FieldType Field - End
		// FieldDefaultValue - Start
		if (isset($ArrayAPIData['fieldtype']))
			{
			$ArrayNewCustomFieldInformation['FieldDefaultValue'] = $ArrayAPIData['defaultvalue'];			
			}
		// FieldDefaultValue - End
		// ValidationMethod Field - Start
		if (isset($ArrayAPIData['validationmethod']) && $ArrayAPIData['validationmethod'] != '')
			{
			if ($ArrayAPIData['validationmethod'] == 'Disabled' || $ArrayAPIData['validationmethod'] == 'Numbers' || $ArrayAPIData['validationmethod'] == 'Letters' || $ArrayAPIData['validationmethod'] == 'Numbers and letters' || $ArrayAPIData['validationmethod'] == 'Email address' || $ArrayAPIData['validationmethod'] == 'URL' || $ArrayAPIData['validationmethod'] == 'Date' || $ArrayAPIData['validationmethod'] == 'Time' || $ArrayAPIData['validationmethod'] == 'Custom')
				{
				$ArrayNewCustomFieldInformation['ValidationMethod'] = $ArrayAPIData['validationmethod'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(8),
									 'ErrorText'		=> array('Invalid validation method')
									);
				throw new Exception('');
				}
			}
		// ValidationMethod Field - End
		// ValidationRule Field - Start
		if (isset($ArrayAPIData['validationrule']))
			{
			$ArrayNewCustomFieldInformation['ValidationRule'] = $ArrayAPIData['validationrule'];			
			}
		// ValidationRule Field - End

		// Prepare field options as string - Start
		$ArrayNewCustomFieldInformation['FieldOptions'] = '';
		if ($ArrayNewCustomFieldInformation['FieldType'] == 'Multiple choice' || $ArrayNewCustomFieldInformation['FieldType'] == 'Drop down' || $ArrayNewCustomFieldInformation['FieldType'] == 'Checkboxes')
			{
			foreach ($ArrayAPIData['optionlabel'] as $Key=>$Value)
				{
				$IsSelected = (in_array($Key, $ArrayAPIData['optionselected']) ? true : false );
				$ArrayNewCustomFieldInformation['FieldOptions'] .= CustomFields::GetOptionAsString($Value, $ArrayAPIData['optionvalue'][$Key], $IsSelected);
				$ArrayNewCustomFieldInformation['FieldOptions'] .= ',,,';
				}
			$ArrayNewCustomFieldInformation['FieldOptions'] = substr($ArrayNewCustomFieldInformation['FieldOptions'], 0, $ArrayNewCustomFieldInformation['FieldOptions'].length - 3);
			}
		// Prepare field options as string - End

		// Visibility Field - Start
		if (isset($ArrayAPIData['visibility']) && $ArrayAPIData['visibility'] != '')
			{
			if (($ArrayAPIData['visibility'] == 'Public') || ($ArrayAPIData['visibility'] == 'User Only'))
				{
				$ArrayNewCustomFieldInformation['Visibility'] = $ArrayAPIData['visibility'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(9),
									 'ErrorText'		=> array('Invalid visibility value')
									);
				throw new Exception('');
				}
			}
		// Visibility Field - End

		// IsGlobal Field - Start
		if (isset($ArrayAPIData['isglobal']) && $ArrayAPIData['isglobal'] != '')
			{
			if (($ArrayAPIData['isglobal'] == 'Yes') || ($ArrayAPIData['isglobal'] == 'No'))
				{
				$ArrayNewCustomFieldInformation['IsGlobal'] = $ArrayAPIData['isglobal'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(12),
									 'ErrorText'		=> array('Invalid IsGlobal value')
									);
				throw new Exception('');
				}
			}
		// IsGlobal Field - End

		// IsRequired and IsUnique Fields - Start
		if (isset($ArrayAPIData['isrequired']) && ($ArrayAPIData['isrequired'] != ''))
			{
			if (($ArrayAPIData['isrequired'] == 'Yes') || ($ArrayAPIData['isrequired'] == 'No'))
				{
				$ArrayNewCustomFieldInformation['IsRequired'] = $ArrayAPIData['isrequired'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(10),
									 'ErrorText'		=> array('Invalid IsRequired value')
									);
				throw new Exception('');
				}
			}
		if (isset($ArrayAPIData['isunique']) && ($ArrayAPIData['isunique'] != ''))
			{
			if (($ArrayAPIData['isunique'] == 'Yes') || ($ArrayAPIData['isunique'] == 'No'))
				{
				$ArrayNewCustomFieldInformation['IsUnique'] = $ArrayAPIData['isunique'];
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(11),
									 'ErrorText'		=> array('Invalid IsUnique value')
									);
				throw new Exception('');
				}
			}
		// IsRequired and IsUnique Fields - End
		
		if ($ArrayCustomFieldInformation['FieldType'] == 'Date field' && isset($ArrayAPIData['years']) && $ArrayAPIData['years'] != '')
			{
			$ArrayNewCustomFieldInformation['Option1'] = $ArrayAPIData['years'];
			}
	// Set values - End

CustomFields::Update($ArrayNewCustomFieldInformation, array('CustomFieldID'=>$ArrayAPIData['customfieldid']));
// Update custom field - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'CustomField.Update.Post', array($ArrayAPIData['customfieldid']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>