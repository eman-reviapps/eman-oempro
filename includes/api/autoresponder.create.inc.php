<?php
// Check for required fields - Start {
$ArrayRequiredFields = array(
							'subscriberlistid'			=> 1,
							'autorespondername'			=> 2,
							'autorespondertriggertype'	=> 3,
							'emailid'					=> 8
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}

if ($ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberLinkClick' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberCampaignOpen' || $ArrayAPIData['autorespondertriggertype'] == 'OnSubscriberDate')
	{
	$ArrayRequiredFields = array(
								'autorespondertriggervalue'		=> 4
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							);
		throw new Exception('');
		}
	}

if ($ArrayAPIData['triggertimetype'] == 'Seconds later' && $ArrayAPIData['triggertimetype'] == 'Minutes later' && $ArrayAPIData['triggertimetype'] == 'Hours later' && $ArrayAPIData['triggertimetype'] == 'Days later' && $ArrayAPIData['triggertimetype'] == 'Weeks later' && $ArrayAPIData['triggertimetype'] == 'Months later')
	{
	$ArrayRequiredFields = array(
								'triggertime'		=> 5
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							);
		throw new Exception('');
		}
	}
// Check for required fields - End }

// Field validations - Start {
	// AutoResponderTriggerType Field - Start
	if (isset($ArrayAPIData['autorespondertriggertype']) && $ArrayAPIData['autorespondertriggertype'] != '')
		{
		if ($ArrayAPIData['autorespondertriggertype'] != 'OnSubscription' && $ArrayAPIData['autorespondertriggertype'] != 'OnSubscriberLinkClick' && $ArrayAPIData['autorespondertriggertype'] != 'OnSubscriberCampaignOpen' && $ArrayAPIData['autorespondertriggertype'] != 'OnSubscriberForwardToFriend' && $ArrayAPIData['autorespondertriggertype'] != 'OnSubscriberDate')
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> array(6),
								);
			throw new Exception('');
			}
		}
	// AutoResponderTriggerType Field - End

	// TriggerTimeType Field - Start
	if (isset($ArrayAPIData['triggertimetype']) && $ArrayAPIData['triggertimetype'] != '')
		{
		if ($ArrayAPIData['triggertimetype'] != 'Immediately' && $ArrayAPIData['triggertimetype'] != 'Seconds later' && $ArrayAPIData['triggertimetype'] != 'Minutes later' && $ArrayAPIData['triggertimetype'] != 'Hours later' && $ArrayAPIData['triggertimetype'] != 'Days later' && $ArrayAPIData['triggertimetype'] != 'Weeks later' && $ArrayAPIData['triggertimetype'] != 'Months later')
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> array(7),
								);
			throw new Exception('');
			}
		}
	// TriggerTimeType Field - End

	// Retrieve email information - Start
	if ($ArrayAPIData['emailid'] != 0)
	{
		Core::LoadObject('emails');
		$ArrayEmail = Emails::RetrieveEmail(array('*'), array('RelUserID' => $ArrayUserInformation['UserID'], 'EmailID' => $ArrayAPIData['emailid']));

		if ($ArrayEmail == false)
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> array(9),
								);
			throw new Exception('');
			}
	}
	// Retrieve email information - End
// Field validations - End }

// Load other modules - Start
Core::LoadObject('auto_responders');
// Load other modules - End

// Create auto responder - Start {
$ArrayNewAutoResponderInformation = array();
$ArrayNewAutoResponderInformation['RelListID']					= $ArrayAPIData['subscriberlistid'];
$ArrayNewAutoResponderInformation['RelOwnerUserID']				= $ArrayUserInformation['UserID'];
$ArrayNewAutoResponderInformation['RelEmailID']					= $ArrayEmail['EmailID'];
$ArrayNewAutoResponderInformation['AutoResponderName']			= $ArrayAPIData['autorespondername'];
$ArrayNewAutoResponderInformation['AutoResponderTriggerType']	= $ArrayAPIData['autorespondertriggertype'];
$ArrayNewAutoResponderInformation['AutoResponderTriggerValue'] 	= $ArrayAPIData['autorespondertriggervalue'];			
$ArrayNewAutoResponderInformation['AutoResponderTriggerValue2']	= isset($ArrayAPIData['autorespondertriggervalue2']) ? $ArrayAPIData['autorespondertriggervalue2'] : '';			
$ArrayNewAutoResponderInformation['TriggerTimeType']			= $ArrayAPIData['triggertimetype'];
$ArrayNewAutoResponderInformation['TriggerTime'] 				= $ArrayAPIData['triggertime'];

$NewAutoResponderID = AutoResponders::Create($ArrayNewAutoResponderInformation);
// Create auto responder - End }

// Plug-in hook - Start
Plugins::HookListener('Action', 'Autoresponder.Create.Post', array($NewAutoResponderID));
// Plug-in hook - End

// Return results - Start {
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'AutoResponderID'	=> $NewAutoResponderID
					);
// Return results - End }
?>