<?php
// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							);
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Create the new campaign record - Start
$NewEmailID = Emails::Create(array('RelUserID'=>$ArrayUserInformation['UserID']));
// Create the new campaign record - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Create.Post', array($NewEmailID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'EmailID'			=> $NewEmailID
					);
// Return results - End
?>