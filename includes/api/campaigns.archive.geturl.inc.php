<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'tagid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('tags');
Core::LoadObject('public_archives');
// Load other modules - End

// Check if given tag exists - Start
$ArrayTagInformation = Tags::RetrieveTag(array('*'), array('TagID'=>$ArrayAPIData['tagid'],'RelOwnerUserID'=>$ArrayUserInformation['UserID']));
if ($ArrayTagInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Check if given tag exists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'URL'				=> PublicArchives::GenerateURL($ArrayAPIData['tagid'], (isset($ArrayAPIData['templateurl']) ? $ArrayAPIData['templateurl'] : ''))
					);
// Return results - End
?>