<?php
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('users');
// Load other modules - End

// Field validations - Start
// Validate the list ID and list ownership - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('ListID' => $_SESSION[SESSION_NAME]['SubscriberLogin']['ListID']));
if ($ArraySubscriberList == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 1
						);
	throw new Exception('');
	}
// Validate the list ID and list ownership - End

// Retrieve owner user information - Start
$ArrayUserInformation = Users::RetrieveUser(array('*'), array('UserID' => $ArraySubscriberList['RelOwnerUserID']));

if ($ArrayUserInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Retrieve owner user information - End

// Retrieve subscriber information - Start
$ArraySubscriberInformation = Subscribers::RetrieveSubscriber(array('*'), array('CONCAT(MD5(`SubscriberID`), MD5(`EmailAddress`))' => $_SESSION[SESSION_NAME]['SubscriberLogin']['Salt']), $ArraySubscriberList['ListID']);

if ($ArraySubscriberInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
// Retrieve subscriber information - End

// Search 'public' lists of the user for the email address  - Start
$ArraySubscriberListsOfUser = Lists::RetrieveLists(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'HideInSubscriberArea' => 'false'), array('ListID' => 'ASC'));

$ArraySubscribedLists = array();

foreach ($ArraySubscriberListsOfUser as $Index=>$ArrayEachSubscriberList)
	{
	$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('EmailAddress' => $ArraySubscriberInformation['EmailAddress'], 'SubscriptionStatus' => 'Subscribed'), $ArrayEachSubscriberList['ListID']);
	
	if ($ArraySubscriber != false)
		{
		$ArraySubscribedLists[]	= array('ListID' => $ArrayEachSubscriberList['ListID'], 'ListName' => $ArrayEachSubscriberList['Name'], 'SubscriberID' => $ArraySubscriber['SubscriberID']);
		}
	}
// Search 'public' lists of the user for the email address  - End

// Field validations - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'SubscribedLists'	=> $ArraySubscribedLists
					);
// Return results - End


?>