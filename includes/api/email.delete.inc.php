<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Delete.Pre', array($ArrayUserInformation['UserID'], $ArrayAPIData['emailid']));
// Plug-in hook - End

// Delete emails - Start
Emails::Delete(array('RelUserID'=>$ArrayUserInformation['UserID'], 'EmailID'=>$ArrayAPIData['emailid']));
// Delete emails - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>