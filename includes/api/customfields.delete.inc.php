<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'customfields'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Custom field ids are missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('custom_fields');
// Load other modules - End

// Delete custom fields - Start
CustomFields::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['customfields']));
// Delete custom fields - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'CustomField.Delete.Post', explode(',', $ArrayAPIData['customfields']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>