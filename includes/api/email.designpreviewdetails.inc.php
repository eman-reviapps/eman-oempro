<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							'jobid'					=> 3,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Retrieve email information - End

// Retrieve design preview job information - Start
$ArrayDesignPreviewJob = Emails::RetrieveDesignPreviewJob(array('*'), array('RelEmailID' => $ArrayEmail['EmailID'], 'JobID' => $ArrayAPIData['jobid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));
if ($ArrayDesignPreviewJob == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4
						);
	throw new Exception('');
	}
// Retrieve design preview job information - End
// / Field validations - End

// Request the design preview details from PreviewMyEmail.com API - Start
$ArrayPostParameters = array(
							'apikey='.PME_APIKEY,
							'jobid='.$ArrayDesignPreviewJob['PreviewMyEmailJobID'],
							);
$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'FetchPreview/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
// Request the design preview details from PreviewMyEmail.com API - End

// Parse the returned data - Start
if ($ArrayReturn[0] == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5
						);
	throw new Exception('');
	}
else
	{
	$ObjectXML = simplexml_load_string($ArrayReturn[1]);

	$ArrayDesignPreviewJob['PreviewResults'] = array();

	$TMPCounter = 0;
	foreach ($ObjectXML->preview_details->preview_results->result as $EachPreviewObject)
		{
		$ArrayDesignPreviewJob['PreviewResults'][] = array(
														'ClientCode'		=> (string)$EachPreviewObject->client_code,
														'ImagesOnURL'		=> (string)$EachPreviewObject->image_url_imageson,
														'ImagesOffURL'		=> (string)$EachPreviewObject->image_url_imagesoff,
														'ThumbnailURL'		=> (string)$EachPreviewObject->image_url_thumbnail,
														);
		$TMPCounter++;
		}

	// Sync job data with PreviewMyEmail data - Start
	if ($ArrayDesignPreviewJob['Status'] != (string)$ObjectXML->preview_details->job_status)
		{
		$ArrayFieldAndValues = array(
									'Status'		=> (string)$ObjectXML->preview_details->job_status,
									'CompleteDate'	=> (string)$ObjectXML->preview_details->complete_date,
									);
		Emails::UpdateDesignPreviewJob($ArrayFieldAndValues, array('RelEmailID' => $ArrayEmail['EmailID'], 'JobID' => $ArrayAPIData['jobid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));
		}
	// Sync job data with PreviewMyEmail data - End
	}
// Parse the returned data - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'PreviewRequest'		=> $ArrayDesignPreviewJob,
					);
// Return results - End
?>