<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Check for required fields - Start
$ArrayRequiredFields = array();
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Email address validations - Start
Core::LoadObject('subscribers');
if (isset($ArrayAPIData['system_email_from_email']))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['system_email_from_email']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 1,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['system_email_replyto_email']))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['system_email_replyto_email']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 1,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['alert_recipient_email']))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['alert_recipient_email']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 1,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['report_abuse_email']))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['report_abuse_email']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 1,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['x_complaints_to']))
	{
	if (Subscribers::ValidateEmailAddress($ArrayAPIData['x_complaints_to']) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 1,
							);
		throw new Exception('');
		}
	}
// Email address validations - End

// Enum validation - Start
if (isset($ArrayAPIData['media_upload_method']))
	{
	if (($ArrayAPIData['media_upload_method'] != 'file') && ($ArrayAPIData['media_upload_method'] != 'database') && ($ArrayAPIData['media_upload_method'] != 's3'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['s3_enabled']))
	{
	if (($ArrayAPIData['s3_enabled'] != 'true') && ($ArrayAPIData['s3_enabled'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['load_balance_status']))
	{
	if (($ArrayAPIData['load_balance_status'] != 'true') && ($ArrayAPIData['load_balance_status'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['pme_usage_type']))
	{
	if (($ArrayAPIData['pme_usage_type'] != 'Admin') && ($ArrayAPIData['pme_usage_type'] != 'User'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['user_signup_enabled']))
	{
	if (($ArrayAPIData['user_signup_enabled'] != 'true') && ($ArrayAPIData['user_signup_enabled'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['user_signup_reputation']))
	{
	if (($ArrayAPIData['user_signup_reputation'] != 'Trusted') && ($ArrayAPIData['user_signup_reputation'] != 'Untrusted'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['user_signup_language']))
	{
	// TODO: Perform language validatoin
	}
if (isset($ArrayAPIData['default_language']))
	{
	// TODO: Perform language validatoin
	}
if (isset($ArrayAPIData['user_signup_groupid']))
	{
	// TODO: Perform user group id validation
	}
if (isset($ArrayAPIData['user_signup_groupids']))
	{
	// TODO: Perform user group id validation
	}
if (isset($ArrayAPIData['default_themeid']))
	{
	// TODO: Perform theme id validation
	}
if (isset($ArrayAPIData['admin_captcha']))
	{
	if (($ArrayAPIData['admin_captcha'] != 'true') && ($ArrayAPIData['admin_captcha'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['user_captcha']))
	{
	if (($ArrayAPIData['user_captcha'] != 'true') && ($ArrayAPIData['user_captcha'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['run_cron_in_user_area']))
	{
	if (($ArrayAPIData['run_cron_in_user_area'] != 'true') && ($ArrayAPIData['run_cron_in_user_area'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['send_method']))
	{
	if (($ArrayAPIData['send_method'] != 'SMTP') && ($ArrayAPIData['send_method'] != 'LocalMTA') && ($ArrayAPIData['send_method'] != 'PHPMail') && ($ArrayAPIData['send_method'] != 'PowerMTA') && ($ArrayAPIData['send_method'] != 'SaveToDisk'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['send_method_smtp_secure']))
	{
	if (($ArrayAPIData['send_method_smtp_secure'] != 'ssl') && ($ArrayAPIData['send_method_smtp_secure'] != 'tls') && ($ArrayAPIData['send_method_smtp_secure'] != ''))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['send_method_smtp_auth']))
	{
	if (($ArrayAPIData['send_method_smtp_auth'] != 'true') && ($ArrayAPIData['send_method_smtp_auth'] != 'false'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['mail_engine']))
	{
	if (($ArrayAPIData['mail_engine'] != 'phpmailer') && ($ArrayAPIData['mail_engine'] != 'swiftmailer'))
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 2,
							);
		throw new Exception('');
		}
	}
if (isset($ArrayAPIData['pop3_bounce_status']))
	{
	if ((isset($ArrayAPIData['send_bounce_notificationemail']) == false) || ($ArrayAPIData['send_bounce_notificationemail'] == ''))
		{
		$ArrayAPIData['send_bounce_notificationemail'] = 'Disabled';
		}
	}
if (isset($ArrayAPIData['paypalexpressstatus']))
	{
	if ((isset($ArrayAPIData['paypalexpressstatus']) == false) || ($ArrayAPIData['paypalexpressstatus'] == '') || ($ArrayAPIData['paypalexpressstatus'] != 'Enabled'))
		{
		$ArrayAPIData['paypalexpressstatus'] = 'Disabled';
		}
	}
if (isset($ArrayAPIData['display_trigger_send_engine_link']))
	{
	if ((isset($ArrayAPIData['display_trigger_send_engine_link']) == false) || ($ArrayAPIData['display_trigger_send_engine_link'] == '') || ($ArrayAPIData['display_trigger_send_engine_link'] != 'Yes'))
		{
		$ArrayAPIData['display_trigger_send_engine_link'] = 'No';
		}
	}
if (isset($ArrayAPIData['centralized_sender_domain']))
	{
	if ($ArrayAPIData['centralized_sender_domain'] == -1)
		{
		$ArrayAPIData['centralized_sender_domain'] = '';
		}
	elseif (isset($ArrayAPIData['centralized_sender_domain']) == false)
		{
		unset($ArrayAPIData['centralized_sender_domain']);
		}
	}
// Enum validation - End

// PreviewMyEmail account validation - Start
if ((isset($ArrayAPIData['pme_usage_type'])) && ($ArrayAPIData['pme_usage_type'] == 'Admin') && ($ArrayAPIData['pme_default_apikey'] != ''))
	{
	$PMEAPIKey		= ($ArrayAPIData['pme_default_apikey'] != '' ? $ArrayAPIData['pme_default_apikey'] : PME_APIKEY);

	// Get available credits via PreviewMyEmail.com API - Start
	$ArrayPostParameters = array(
								'apikey='.$PMEAPIKey
								);
	$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'SystemStatus/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
	// Get available credits via PreviewMyEmail.com API - End

	// Parse the returned data - Start
	if ($ArrayReturn[0] == false)
		{
		// connection error occurred
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 3
							);
		throw new Exception('');
		}
	}
// PreviewMyEmail account validation - End

// Check if email sending settings set correctly - Start
// WE HAVE MOVED EMAIL DELIVERY TESTING TO ITS OWN COMMAND: EMAIL.DELIVERY.TEST
// if (isset($ArrayAPIData['send_method']))
// 	{
// 	$ArrayResult = array();
// 	Core::LoadObject('emails');
// 
// 	if ($ArrayAPIData['send_method'] == 'SMTP')
// 		{
// 		$SendMethod				= 'SMTP';
// 		$SMTPHost				= $ArrayAPIData['send_method_smtp_host'];
// 		$SMTPPort				= $ArrayAPIData['send_method_smtp_port'];
// 		$SMTPSecure				= $ArrayAPIData['send_method_smtp_secure'];
// 		$SMTPAuth				= ($ArrayAPIData['send_method_smtp_auth'] == 'true' ? true : false);
// 		$SMTPUsername			= $ArrayAPIData['send_method_smtp_username'];
// 		$SMTPPassword			= $ArrayAPIData['send_method_smtp_password'];
// 		$SMTPTimeout			= $ArrayAPIData['send_method_smtp_timeout'];
// 		$SMTPDebug				= false;
// 		$SMTPKeepAlive			= true;
// 		$LocalMTAPath			= '';
// 
// 		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
// 		}
// 	elseif ($ArrayAPIData['send_method'] == 'LocalMTA')
// 		{
// 		$SendMethod				= 'LocalMTA';
// 		$SMTPHost				= '';
// 		$SMTPPort				= '';
// 		$SMTPSecure				= '';
// 		$SMTPAuth				= '';
// 		$SMTPUsername			= '';
// 		$SMTPPassword			= '';
// 		$SMTPTimeout			= '';
// 		$SMTPDebug				= '';
// 		$SMTPKeepAlive			= '';
// 		$LocalMTAPath			= $ArrayAPIData['send_method_localmta_path'];
// 
// 		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
// 		}
// 	elseif ($ArrayAPIData['send_method'] == 'PHPMail')
// 		{
// 		$SendMethod				= 'PHPMail';
// 		$SMTPHost				= '';
// 		$SMTPPort				= '';
// 		$SMTPSecure				= '';
// 		$SMTPAuth				= '';
// 		$SMTPUsername			= '';
// 		$SMTPPassword			= '';
// 		$SMTPTimeout			= '';
// 		$SMTPDebug				= '';
// 		$SMTPKeepAlive			= '';
// 		$LocalMTAPath			= '';
// 
// 		$ArrayResult = Emails::SendTestEmail($ArrayAdminInformation['EmailAddress'], $ArrayLanguageStrings['Screen']['9113'], $ArrayLanguageStrings['Screen']['9114'], $ArrayLanguageStrings['Screen']['9115'], $SendMethod, $SMTPHost, $SMTPPort, $SMTPSecure, $SMTPAuth, $SMTPUsername, $SMTPPassword, $SMTPTimeout, $SMTPDebug, $SMTPKeepAlive, $LocalMTAPath);
// 		}
// 	elseif (($ArrayAPIData['send_method'] == 'PowerMTA') || ($ArrayAPIData['send_method'] == 'SaveToDisk'))
// 		{
// 		// Add / to the end of paths if it does not exist - Start
// 		if ($ArrayAPIData['send_method'] == 'PowerMTA')
// 			{
// 			if (substr($ArrayAPIData['send_method_powermta_dir'], strlen($ArrayAPIData['send_method_powermta_dir']) - 1, strlen($ArrayAPIData['send_method_powermta_dir'])) != '/')
// 				{
// 				$ArrayAPIData['send_method_powermta_dir'] = $ArrayAPIData['send_method_powermta_dir'].'/';
// 				}
// 			}
// 		else
// 			{
// 			if (substr($ArrayAPIData['send_method_savetodisk_dir'], strlen($ArrayAPIData['send_method_savetodisk_dir']) - 1, strlen($ArrayAPIData['send_method_savetodisk_dir'])) != '/')
// 				{
// 				$ArrayAPIData['send_method_savetodisk_dir'] = $ArrayAPIData['send_method_savetodisk_dir'].'/';
// 				}
// 			}
// 		// Add / to the end of paths if it does not exist - End
// 
// 		if (is_writable(($ArrayAPIData['send_method'] == 'PowerMTA' ? $ArrayAPIData['send_method_powermta_dir'] : $ArrayAPIData['send_method_savetodisk_dir'])) == false)
// 			{
// 			$ArrayResult = array(false, isset($ArrayLanguageStrings['Screen']['9097']) ? $ArrayLanguageStrings['Screen']['9097'] : ApplicationHeader::$ArrayLanguageStrings['Screen']['1325']);
// 			}
// 		else
// 			{
// 			$ArrayResult = array(true);
// 			}
// 		}
// 
// 	if ($ArrayResult[0] == false)
// 		{
// 		$ArrayOutput = array('Success'						=> false,
// 							 'ErrorCode'					=> 5,
// 							 'EmailSettingsErrorMessage'	=> $ArrayResult[1],
// 							);
// 		throw new Exception('');
// 		}
// 	}
// Check if email sending settings set correctly - End


// Check if POP3 information is set correctly for FBL, bounce and/or request processing - Start
if ((isset($ArrayAPIData['pop3_bounce_status'])) && ($ArrayAPIData['pop3_bounce_status'] == 'Enabled'))
	{
	Core::LoadObject('pop3_engine');

	$popMailbox = '{' . $ArrayAPIData['pop3_bounce_host'] . ':' . $ArrayAPIData['pop3_bounce_port']
			. '/pop3' . ($ArrayAPIData['pop3_bounce_ssl'] == 'Yes' ? '/ssl' : '')
			. (ENABLE_IMAP_NOVALIDATE_SSL == true ? '/novalidate-cert' : '')
			. '}INBOX';
	$popConnection = imap_open($popMailbox, $ArrayAPIData['pop3_bounce_username'], $ArrayAPIData['pop3_bounce_password']);

	if ($popConnection === FALSE) {
		$ArrayOutput = array('Success'						=> false,
							 'ErrorCode'					=> 6,
							 'EmailSettingsErrorMessage'	=>  implode(',', imap_errors()),
							);
		throw new Exception('');
	}

	}
if ((isset($ArrayAPIData['pop3_fbl_status'])) && ($ArrayAPIData['pop3_fbl_status'] == 'Enabled'))
	{
	$popMailbox = '{' . $ArrayAPIData['pop3_fbl_host'] . ':' . $ArrayAPIData['pop3_fbl_port']
			. '/pop3' . ($ArrayAPIData['pop3_fbl_ssl'] == 'Yes' ? '/ssl' : '')
			. (ENABLE_IMAP_NOVALIDATE_SSL == true ? '/novalidate-cert' : '')
			. '}INBOX';
	$popConnection = imap_open($popMailbox, $ArrayAPIData['pop3_fbl_username'], $ArrayAPIData['pop3_fbl_password']);

	if ($popConnection === FALSE) {
		$ArrayOutput = array('Success'						=> false,
							 'ErrorCode'					=> 6,
							 'EmailSettingsErrorMessage'	=>  implode(',', imap_errors()),
							);
		throw new Exception('');
	}

	}
if ((isset($ArrayAPIData['pop3_requests_status'])) && ($ArrayAPIData['pop3_requests_status'] == 'Enabled'))
	{
	Core::LoadObject('pop3_engine');
	
	POP3Engine::SetParameters($ArrayAPIData['pop3_requests_host'], $ArrayAPIData['pop3_requests_port'], $ArrayAPIData['pop3_requests_username'], $ArrayAPIData['pop3_requests_password'], ($ArrayAPIData['pop3_requests_ssl'] == 'Yes' ? true : false));

	$Return = POP3Engine::Connect();

	if (strtolower(gettype($Return)) == 'array')
		{
		// Ignore some pop3 error messages - Start
		if (in_array('Mailbox is empty', $Return) == false)
			{
			$ArrayOutput = array('Success'						=> false,
								 'ErrorCode'					=> 6,
								 'EmailSettingsErrorMessage'	=> $Return[0],
								);
			throw new Exception('');
			}
		// Ignore some pop3 error messages - End
		}
	}
// Check if POP3 information is set correctly for FBL, bounce and/or request processing - End

// Check if default opt-in confirmation email includes confirmation link or not - Start {
if ((isset($ArrayAPIData['default_optin_email_subject'])) && (isset($ArrayAPIData['default_optin_email_body'])))
	{
	Core::LoadObject('emails');
	if (Emails::DetectTagInContent($ArrayAPIData['default_optin_email_body'], '%Link:Confirm%') == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 7
							);
		throw new Exception('');
		}
	}
// Check if default opt-in confirmation email includes confirmation link or not - End }

// Field validations - End

// Update settings - Start
$ArrayFieldsToUpdate = explode(',', 'SYSTEM_EMAIL_FROM_NAME,SYSTEM_EMAIL_FROM_EMAIL,SYSTEM_EMAIL_REPLYTO_NAME,SYSTEM_EMAIL_REPLYTO_EMAIL,ALERT_RECIPIENT_EMAIL,REPORT_ABUSE_EMAIL,X_COMPLAINTS_TO,MEDIA_UPLOAD_METHOD,FBL_INCOMING_EMAILADDRESS,THRESHOLD_SOFT_BOUNCE_DETECTION,BOUNCE_CATCHALL_DOMAIN,S3_ENABLED,S3_ACCESS_ID,S3_SECRET_KEY,S3_BUCKET,S3_MEDIALIBRARY_PATH,S3_URL,LOAD_BALANCE_STATUS,LOAD_BALANCE_EMAILS,LOAD_BALANCE_SLEEP,PME_USAGE_TYPE,PME_DEFAULT_ACCOUNT,PME_DEFAULT_APIKEY,USER_SIGNUP_ENABLED,USER_SIGNUP_FIELDS,USER_SIGNUP_REPUTATION,USER_SIGNUP_LANGUAGE,DEFAULT_LANGUAGE,USER_SIGNUP_GROUPID,USER_SIGNUP_GROUPIDS,DEFAULT_THEMEID,PAYMENT_CURRENCY,PAYMENT_TAX_PERCENT,PAYMENT_RECEIPT_EMAIL_SUBJECT,PAYMENT_RECEIPT_EMAIL_MESSAGE,ADMIN_CAPTCHA,USER_CAPTCHA,ENABLED_PLUGINS,SEND_METHOD,SEND_METHOD_LOCALMTA_PATH,SEND_METHOD_POWERMTA_VMTA,SEND_METHOD_POWERMTA_DIR,SEND_METHOD_SAVETODISK_DIR,SEND_METHOD_SMTP_HOST,SEND_METHOD_SMTP_PORT,SEND_METHOD_SMTP_SECURE,SEND_METHOD_SMTP_AUTH,SEND_METHOD_SMTP_USERNAME,SEND_METHOD_SMTP_PASSWORD,SEND_METHOD_SMTP_TIMEOUT,SEND_METHOD_SMTP_DEBUG,SEND_METHOD_SMTP_KEEPALIVE,SEND_METHOD_SMTP_MSG_CONN,IMPORT_MAX_FILESIZE,ATTACHMENT_MAX_FILESIZE,MEDIA_MAX_FILESIZE,X_MAILER,MAIL_ENGINE,GOOGLE_ANALYTICS_SOURCE,GOOGLE_ANALYTICS_MEDIUM,FORWARD_TO_FRIEND_HEADER,FORWARD_TO_FRIEND_FOOTER,REPORT_ABUSE_FRIEND_HEADER,REPORT_ABUSE_FRIEND_FOOTER,USER_SIGNUP_HEADER,USER_SIGNUP_FOOTER,PRODUCT_NAME,DEFAULT_SUBSCRIBERAREA_LOGOUT_URL,POP3_BOUNCE_STATUS,POP3_BOUNCE_HOST,POP3_BOUNCE_PORT,POP3_BOUNCE_USERNAME,POP3_BOUNCE_PASSWORD,POP3_FBL_STATUS,POP3_FBL_HOST,POP3_FBL_PORT,POP3_FBL_USERNAME,POP3_FBL_PASSWORD,POP3_REQUESTS_STATUS,POP3_REQUESTS_HOST,POP3_REQUESTS_PORT,POP3_REQUESTS_USERNAME,POP3_REQUESTS_PASSWORD,SEND_BOUNCE_NOTIFICATIONEMAIL,REBRANDED_PRODUCT_LOGO,REBRANDED_PRODUCT_LOGO_TYPE,PAYPALEXPRESSSTATUS,PAYPALEXPRESSBUSINESSNAME,PAYPALEXPRESSPURCHASEDESCRIPTION,PAYPALEXPRESSCURRENCY,DISPLAY_TRIGGER_SEND_ENGINE_LINK,DEFAULT_OPTIN_EMAIL_SUBJECT,DEFAULT_OPTIN_EMAIL_BODY,USERAREA_FOOTER,FORBIDDEN_FROM_ADDRESSES,USERAREA_FOOTER,RUN_CRON_IN_USER_AREA,POP3_BOUNCE_SSL,POP3_FBL_SSL,POP3_REQUESTS_SSL,CENTRALIZED_SENDER_DOMAIN,PAYMENT_CREDITS_GATEWAY_URL');
$ArrayFieldsnValues = array();

foreach ($ArrayFieldsToUpdate as $EachField)
	{
	if (isset($ArrayAPIData[strtolower($EachField)]) == true)
		{
		$ArrayFieldsnValues[$EachField] = $ArrayAPIData[strtolower($EachField)];
		}
	}

// Set the product logo - Start
if ($ArrayAPIData['displayoriginallogo'] == 'Yes')
	{
	$ArrayFieldsnValues['REBRANDED_PRODUCT_LOGO']		= 'Disabled';
	$ArrayFieldsnValues['REBRANDED_PRODUCT_LOGO_TYPE']	= '';
	}
// Set the product logo - End

Core::UpdateSettings($ArrayFieldsnValues, array('ConfigID' => 1));
// Update settings - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End


?>