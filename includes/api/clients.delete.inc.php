<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'clients'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Clients ids are missing',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Delete clients - Start
Clients::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['clients']));
// Delete clients - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Client.Delete.Post', explode(',', $ArrayAPIData['clients']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>