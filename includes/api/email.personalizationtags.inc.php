<?php
// Load other modules - Start
Core::LoadObject('personalization');
Core::LoadObject('lists');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'scope'				=> 1,
							);
if (in_array('Subscriber', $ArrayAPIData['scope']) == true)
	{
	$ArrayRequiredFields['listid'] = 2;
	}

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Generate personalization tags - Start
$ArrayPersonalizationTags = array();

if (in_array('Subscriber', $ArrayAPIData['scope']) == true)
	{
	$ArraySubscriberTags	= Personalization::GetSubscriberPersonalizationTags($ArrayUserInformation['UserID'], $ArrayAPIData['listid'], $ArrayLanguageStrings['Screen']['9036'], $ArrayLanguageStrings['Screen']['9035'], array(), $ArrayLanguageStrings);
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArraySubscriberTags);
	}
if (in_array('CampaignLinks', $ArrayAPIData['scope']) == true)
	{
	$ArrayLinkTags			= Personalization::GetPersonalizationLinkTags($ArrayLanguageStrings['Screen']['9037'], 'Campaign');
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayLinkTags);
	}
elseif (in_array('OptLinks', $ArrayAPIData['scope']) == true)
	{
	$ArrayLinkTags			= Personalization::GetPersonalizationLinkTags($ArrayLanguageStrings['Screen']['9037'], 'Opt');
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayLinkTags);
	}
elseif (in_array('ListLinks', $ArrayAPIData['scope']) == true)
	{
	$ArrayLinkTags			= Personalization::GetPersonalizationLinkTags($ArrayLanguageStrings['Screen']['9037'], 'List');
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayLinkTags);
	}
elseif (in_array('AllLinks', $ArrayAPIData['scope']) == true)
	{
	$ArrayLinkTags			= Personalization::GetPersonalizationLinkTags($ArrayLanguageStrings['Screen']['9037'], 'All');
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayLinkTags);
	}
if (in_array('User', $ArrayAPIData['scope']) == true)
	{
	$ArrayUserTags			= Personalization::GetPersonalizationUserTags($ArrayLanguageStrings['Screen']['9038']);
	$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayUserTags);
	}

$ArrayOtherTags			= Personalization::GetOtherPersonalizationTags($ArrayLanguageStrings['Screen']['9149']);
$ArrayPersonalizationTags = array_merge($ArrayPersonalizationTags, $ArrayOtherTags);
// Generate personalization tags - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'PersonalizationTags'	=> $ArrayPersonalizationTags,
					);
// Return results - End
?>