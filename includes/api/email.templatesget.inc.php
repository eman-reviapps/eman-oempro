<?php
// Load other modules - Start
Core::LoadObject('templates');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Get templates - Start
$arrayCriterias = array();
if (AdminAuth::IsLoggedIn(false, false))
	{
	if ($ArrayAPIData['AuthenticationType'] == 'admin')
		{
		$arrayCriterias = array();
		} 
	else if ($ArrayAPIData['AuthenticationType'] == 'user')
		{
		$arrayCriterias = array(array('field' => 'RelOwnerUserID', 'operator' => 'IN', 'value' => '('.$ArrayUserInformation['UserID'].', -'.$ArrayUserInformation['RelUserGroupID'].', 0)', 'auto-quote' => false));
		}
	}
elseif (UserAuth::IsLoggedIn(false, false))
	{
	$arrayCriterias = array(array('field' => 'RelOwnerUserID', 'operator' => 'IN', 'value' => '('.$ArrayUserInformation['UserID'].', -'.$ArrayUserInformation['RelUserGroupID'].', 0)', 'auto-quote' => false));
	}

$ArrayTemplates = Templates::RetrieveTemplates(array('TemplateID', 'TemplateName', 'TemplateSubject', 'TemplateHTMLContent', 'TemplatePlainContent'), $arrayCriterias, array('TemplateName' => 'ASC'), 'OR');
// Get templates - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'TotalTemplateCount'	=> ($ArrayTemplates == false ? 0 : count($ArrayTemplates)),
					 'Templates'			=> $ArrayTemplates
					);
// Return results - End

?>