<?php
// Load other modules - Start
Core::LoadObject('templates');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'templateid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Retrieve template information - Start
$ArrayTemplate = Templates::RetrieveTemplate(array('TemplateID', 'TemplateName', 'TemplateDescription', 'TemplateSubject', 'TemplateHTMLContent', 'TemplatePlainContent', 'RelOwnerUserID'), array('TemplateID' => $ArrayAPIData['templateid']), 'AND');
if ($ArrayTemplate == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2)
						);
	throw new Exception('');
	}
// Retrieve template information - End

$IsAdminLoggedIn	= AdminAuth::IsLoggedIn(false,false);
$IsUserLoggedIn		= UserAuth::IsLoggedIn(false,false);

// Field validations - Start
// Check if this template owned by the admin or belongs to that user - Start
if (($ArrayTemplate['RelOwnerUserID'] != $ArrayUserInformation['UserID']) && ($IsAdminLoggedIn == false) && ($ArrayTemplate['RelOwnerUserID'] != 0) && ($ArrayTemplate['RelOwnerUserID'] != '-'.$ArrayUserInformation['RelUserGroupID']))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3)
						);
	throw new Exception('');
	}
// Check if this template owned by the admin or belongs to that user - End
// Field validations - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'Template'			=> $ArrayTemplate
					);
// Return results - End
?>