<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'themes'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

// Load other modules - Start
Core::LoadObject('themes');
// Load other modules - End

// Delete themes - Start
$ArrayThemeIDs = explode(',', $ArrayAPIData['themes']);

foreach ($ArrayThemeIDs as $EachThemeID)
	{
	$ArrayTheme = ThemeEngine::RetrieveTheme(array('*'), array('ThemeID' => $EachThemeID));
	
	if ($ArrayTheme['ThemeID'] != DEFAULT_THEMEID)
		{
		// If this theme is set as system default theme, do not delete it - Start
		if ($ArrayAPIData['themes'] == DEFAULT_THEMEID)
			{
			continue;
			}
		// If this theme is set as system default theme, do not delete it - End

		// If this is the last theme, do not delete - Start
		$TotalFound = ThemeEngine::RetrieveThemes(array('COUNT(*) AS TotalFound'), array('ThemeID' => $ArrayTheme['ThemeID']));
		$TotalFound = $TotalFound[0]['TotalFound'];

		if ($TotalFound <= 1)
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> 2,
								);
			throw new Exception('');
			}
		// If this is the last theme, do not delete - End

		ThemeEngine::Delete(array($ArrayTheme['ThemeID']));

		// Delete the cache file - Start
		$CSSCacheFile = APP_PATH.'/data/css/css_cache_'.$ArrayTheme['ThemeID'].'.css';
		if (file_exists($CSSCacheFile) == true) unlink($CSSCacheFile);
		// Delete the cache file - End

		// Assign the groups that are linked to this theme to the system default theme - Start
		Core::LoadObject('user_groups');
		UserGroups::Update(array('RelThemeID' => DEFAULT_THEMEID), array('RelThemeID' => $ArrayTheme['ThemeID']));
		// Assign the groups that are linked to this theme to the system default theme - End
		}
	}
// Delete themes - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End
?>