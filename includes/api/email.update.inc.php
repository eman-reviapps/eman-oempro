<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							'validatescope'			=> 9,
							);

if ($ArrayAPIData['mode'] == 'Template')
	{
	$ArrayRequiredFields['RelTemplateID']	= 5;
	}

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('emails');
Core::LoadObject('subscribers');
// Load other modules - End

// Check if auto responder exists - Start
$ArrayEmailInformation = Emails::RetrieveEmail(array('*'), array('RelUserID'=>$ArrayUserInformation['UserID'],'EmailID'=>$ArrayAPIData['emailid']));
if ($ArrayEmailInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3)
						);
	throw new Exception('');
	}
// Check if auto responder exists - End

// Field validations - Start
// Validate validation scope - Start
if (($ArrayAPIData['validatescope'] != 'OptIn') && ($ArrayAPIData['validatescope'] != 'Campaign') && ($ArrayAPIData['validatescope'] != 'AutoResponder'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 10,
						);
	throw new Exception('');
	}
// Validate validation scope - End

// FromEmail email address format - Start
if (isset($ArrayAPIData['fromemail']) && Subscribers::ValidateEmailAddress($ArrayAPIData['fromemail']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 6,
						);
	throw new Exception('');
	}
// FromEmail email address format - End

// ReplyToEmail email address format - Start
if (isset($ArrayAPIData['replytoemail']) && Subscribers::ValidateEmailAddress($ArrayAPIData['replytoemail']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7,
						);
	throw new Exception('');
	}
// ReplyToEmail email address format - End

// Field validations - End

// Update auto responder - Start
	$ArrayUpdatedEmailInformation = array();
	// If HTMLContent contains only junk and whitespace, set it empty - Start
	if (isset($ArrayAPIData['htmlcontent']))
		{
		$BodyContent = '';
		preg_match_all ('/<body[^>]*>(.*?)<\/body>/isU', $ArrayAPIData['htmlcontent'], $BodyContent, PREG_SET_ORDER);
		$BodyContent = $BodyContent[0][1];
		$BodyContent = preg_replace("/\s+/", "", $BodyContent);
		if (strlen($BodyContent) == 0)
			{
			$ArrayUpdatedEmailInformation['HTMLContent'] = '';
			}
		else
			{
			$ArrayUpdatedEmailInformation['HTMLContent'] = $ArrayAPIData['htmlcontent'];
			}
		}
	else 
		{
		$ArrayUpdatedEmailInformation['HTMLContent'] = '';
		}
	// If HTMLContent contains only junk and whitespace, set it empty - END

	// Start
	if (isset($ArrayAPIData['plaincontent']))
		{
		$ArrayUpdatedEmailInformation['PlainContent'] = $ArrayAPIData['plaincontent'];
		}
	// End

	// Set values - Start
		// EmailName Field - Start
		if (isset($ArrayAPIData['emailname']) && $ArrayAPIData['emailname'] != '')
			{
			$ArrayUpdatedEmailInformation['EmailName'] = $ArrayAPIData['emailname'];
			}
		// EmailName Field - End
		// FromName Field - Start
		if (isset($ArrayAPIData['fromname']) && $ArrayAPIData['fromname'] != '')
			{
			$ArrayUpdatedEmailInformation['FromName'] = $ArrayAPIData['fromname'];
			}
		// FromName Field - End
		// FromEmail Field - Start
		if (isset($ArrayAPIData['fromemail']) && $ArrayAPIData['fromemail'] != '')
			{
			$ArrayUpdatedEmailInformation['FromEmail'] = $ArrayAPIData['fromemail'];
			}
		// FromEmail Field - End
		// ReplyToName Field - Start
		if (isset($ArrayAPIData['replytoname']) && $ArrayAPIData['replytoname'] != '')
			{
			$ArrayUpdatedEmailInformation['ReplyToName'] = $ArrayAPIData['replytoname'];
			}
		// ReplyToName Field - End
		// ReplyToEmail Field - Start
		if (isset($ArrayAPIData['replytoemail']) && $ArrayAPIData['replytoemail'] != '')
			{
			$ArrayUpdatedEmailInformation['ReplyToEmail'] = $ArrayAPIData['replytoemail'];
			}
		// ReplyToEmail Field - End
		// Mode and FetchURL Field - Start
		if (isset($ArrayAPIData['mode']) && $ArrayAPIData['mode'] != '')
			{
			if ($ArrayAPIData['mode'] != 'Empty' && $ArrayAPIData['mode'] != 'Template' && $ArrayAPIData['mode'] != 'Import')
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(4)
									);
				throw new Exception('');
				}
			$ArrayUpdatedEmailInformation['Mode'] = $ArrayAPIData['mode'];
			$ArrayUpdatedEmailInformation['FetchURL'] = $ArrayAPIData['fetchurl'];
			$ArrayUpdatedEmailInformation['FetchPlainURL'] = $ArrayAPIData['fetchplainurl'];
			}
		// Mode and FetchURL Field - End
		// Subject Field - Start
		if (isset($ArrayAPIData['subject']) && $ArrayAPIData['subject'] != '')
			{
			$ArrayUpdatedEmailInformation['Subject'] = $ArrayAPIData['subject'];
			}
		// Subject Field - End
		// ContentType Field - Start
		if (isset($ArrayUpdatedEmailInformation['Mode']) && $ArrayUpdatedEmailInformation['Mode'] != "Import")
			{
			if ($ArrayUpdatedEmailInformation['HTMLContent'] != '' && $ArrayUpdatedEmailInformation['PlainContent'] != '')
				{
				$ArrayUpdatedEmailInformation['ContentType'] = 'Both';
				}
			elseif ($ArrayUpdatedEmailInformation['HTMLContent'] != '' && $ArrayUpdatedEmailInformation['PlainContent'] == '')
				{
				$ArrayUpdatedEmailInformation['ContentType'] = 'HTML';
				}
			elseif ($ArrayUpdatedEmailInformation['HTMLContent'] == '' && $ArrayUpdatedEmailInformation['PlainContent'] != '')
				{
				$ArrayUpdatedEmailInformation['ContentType'] = 'Plain';
				}
			else
				{
				$ArrayOutput = array('Success'			=> false,
									 'ErrorCode'		=> array(8)
									);
				throw new Exception('');
				}
			}
		elseif (isset($ArrayUpdatedEmailInformation['Mode']) && $ArrayUpdatedEmailInformation['Mode'] == "Import")
			{
			Core::LoadObject('campaigns');

			$ArrayUpdatedEmailInformation['ContentType'] = 'HTML';
			$ArrayUpdatedEmailInformation['HTMLContent'] = Campaigns::FetchRemoteContent($ArrayAPIData['fetchurl']);
			if ($ArrayAPIData['fetchplainurl'] != '')
				{
				$ArrayUpdatedEmailInformation['ContentType'] = 'Both';
				$ArrayUpdatedEmailInformation['PlainContent'] = Campaigns::FetchRemoteContent($ArrayAPIData['fetchplainurl']);
				}
			}
		// ContentType Field - End

		// Validate email content based on validation scope - Start
		if ($ArrayAPIData['validatescope'] == 'OptIn')
			{
			if ($ArrayUpdatedEmailInformation['ContentType'] == 'HTML')
				{
				if (Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Confirm%') == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(13)
										);
					throw new Exception('');
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Reject%') == false) && ($ArrayUserInformation['GroupInformation']['ForceRejectOptLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(15)
										);
					throw new Exception('');
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Both')
				{
				if (Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Confirm%') == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(13)
										);
					throw new Exception('');
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Reject%') == false) && ($ArrayUserInformation['GroupInformation']['ForceRejectOptLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(15)
										);
					throw new Exception('');
					}
				if (Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Confirm%') == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(14)
										);
					throw new Exception('');
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Reject%') == false) && ($ArrayUserInformation['GroupInformation']['ForceRejectOptLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(16)
										);
					throw new Exception('');
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Plain')
				{
				if (Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Confirm%') == false)
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(14)
										);
					throw new Exception('');
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Reject%') == false) && ($ArrayUserInformation['GroupInformation']['ForceRejectOptLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(16)
										);
					throw new Exception('');
					}
				}
			}
		elseif ($ArrayAPIData['validatescope'] == 'Campaign')
			{
			if ($ArrayUpdatedEmailInformation['ContentType'] == 'HTML')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Unsubscribe%') == false) && ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					if (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailHeader'], '%Link:Unsubscribe%') == false && Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailFooter'], '%Link:Unsubscribe%') == false)
						{
						$ArrayOutput = array('Success'			=> false,
											 'ErrorCode'		=> array(11)
											);
						throw new Exception('');
						}
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Both')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Unsubscribe%') == false) && ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					if (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailHeader'], '%Link:Unsubscribe%') == false && Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailFooter'], '%Link:Unsubscribe%') == false)
						{
						$ArrayOutput = array('Success'			=> false,
											 'ErrorCode'		=> array(11)
											);
						throw new Exception('');
						}
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Unsubscribe%') == false) && ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					if (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailHeader'], '%Link:Unsubscribe%') == false && Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailFooter'], '%Link:Unsubscribe%') == false)
						{
						$ArrayOutput = array('Success'			=> false,
											 'ErrorCode'		=> array(12)
											);
						throw new Exception('');
						}
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Plain')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Unsubscribe%') == false) && ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					if (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailHeader'], '%Link:Unsubscribe%') == false && Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailFooter'], '%Link:Unsubscribe%') == false)
						{
						$ArrayOutput = array('Success'			=> false,
											 'ErrorCode'		=> array(12)
											);
						throw new Exception('');
						}
					}
				}
			}
		elseif ($ArrayAPIData['validatescope'] == 'AutoResponder')
			{
			if ($ArrayUpdatedEmailInformation['ContentType'] == 'HTML')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailHeader'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailFooter'], '%Link:Unsubscribe%') == false)
					&& ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(11)
										);
					throw new Exception('');
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Both')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['HTMLContent'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailHeader'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['HTMLEmailFooter'], '%Link:Unsubscribe%') == false)
					&& ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(11)
										);
					throw new Exception('');
					}
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailHeader'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailFooter'], '%Link:Unsubscribe%') == false)
					&& ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(12)
										);
					throw new Exception('');
					}
				}
			elseif ($ArrayUpdatedEmailInformation['ContentType'] == 'Plain')
				{
				if ((Emails::DetectTagInContent($ArrayUpdatedEmailInformation['PlainContent'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailHeader'], '%Link:Unsubscribe%') == false)
					&& (Emails::DetectTagInContent($ArrayUserInformation['GroupInformation']['PlainEmailFooter'], '%Link:Unsubscribe%') == false)
					&& ($ArrayUserInformation['GroupInformation']['ForceUnsubscriptionLink'] == 'Enabled'))
					{
					$ArrayOutput = array('Success'			=> false,
										 'ErrorCode'		=> array(12)
										);
					throw new Exception('');
					}
				}
			}
		// Validate email content based on validation scope - End

		// Reset the HTML content if the email mode is 'import from URL' to avoid update of HTML field - Start {
		if (isset($ArrayUpdatedEmailInformation['Mode']) && $ArrayUpdatedEmailInformation['Mode'] == "Import")
			{
			unset($ArrayUpdatedEmailInformation['HTMLContent']);
			}
		// Reset the HTML content if the email mode is 'import from URL' to avoid update of HTML field - End }
		

		// ImageEmbedding Field - Start
		if (isset($ArrayAPIData['imageembedding']) && $ArrayAPIData['imageembedding'] != '')
			{
			$ArrayUpdatedEmailInformation['ImageEmbedding'] = $ArrayAPIData['imageembedding'];
			}
		// ImageEmbedding Field - End
		// RelTemplateID Field - Start
		if (isset($ArrayAPIData['reltemplateid']) && $ArrayAPIData['reltemplateid'] != '')
			{
			$ArrayUpdatedEmailInformation['RelTemplateID'] = $ArrayAPIData['reltemplateid'];
			}
		// RelTemplateID Field - End
	// Set values - End

Emails::Update($ArrayUpdatedEmailInformation, array('EmailID'=>$ArrayAPIData['emailid'], 'RelUserID'=>$ArrayUserInformation['UserID']));
// Update auto responder - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Update.Post', array($ArrayAPIData['emailid']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0
					);
// Return results - End
?>