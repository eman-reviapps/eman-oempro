<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'username'		=> 1,
							'password'		=> 2
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> ''
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('clients');
Core::LoadObject('client_auth');
// Load other modules - End

// Validate if such a username/password pair exists in the database - Start
$ArrayCriterias = array(
						'ClientUsername'		=> $ArrayAPIData['username'],
						'ClientPassword'		=> md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['password'].OEMPRO_PASSWORD_SALT),
						);
$ArrayClient = Clients::RetrieveClient(array('*'), $ArrayCriterias);

if ($ArrayClient == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3,
						 'ErrorText'		=> ''
						);
	throw new Exception('');
	}
// Validate if such a username/password pair exists in the database - End

// Perform the user login - Start
ClientAuth::Login($ArrayClient['ClientID'], $ArrayClient['ClientUsername'], $ArrayClient['ClientPassword']);
// Perform the user login - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Client.Login.Post', array($ArrayClient['ClientID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'SessionID'		=> session_id(),
					);
foreach ($ArrayClient as $Key=>$Value)
	{
	$ArrayOutput['ClientInfo'][$Key] = $Value;
	}
// Return results - End
?>