<?php
// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Retrieve assigned assigned lists - Start
$ArraySubscriberLists = Clients::RetrieveAssignedSubscriberLists($ArrayClientInformation['ClientID'], true);
// Retrieve assigned assigned lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalListCount'	=> ($ArraySubscriberLists == false ? 0 : count($ArraySubscriberLists)),
					 'Lists'			=> $ArraySubscriberLists
					);
// Return results - End
?>