<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'userid'				=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('user_auth');
// Load other modules - End

// Field validations - Start
// Check if user exists - Start
$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayAPIData['userid']));

if ($ArrayUser == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Check if user exists - End

// Check privilege type - Start
if (($ArrayAPIData['privilegetype'] != 'Default') && ($ArrayAPIData['privilegetype'] != 'Full') && ($ArrayAPIData['privilegetype'] != ''))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3,
						);
	throw new Exception('');
	}
// Check privilege type - End
// Field validations - End

// Switch to the requested user - Start
UserAuth::Login($ArrayUser['UserID'], $ArrayUser['Username'], $ArrayUser['Password'], ($ArrayAPIData['privilegetype'] == 'Full' ? 'Full' : ''));
// Switch to the requested user - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'SessionID'		=> session_id(),
					);
foreach ($ArrayUser as $Key=>$Value)
	{
	$ArrayOutput['UserInfo'][$Key] = $Value;
	}
// Return results - End

?>