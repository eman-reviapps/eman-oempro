<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaigns'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('campaigns');
// Load other modules - End

// Delete campaigns - Start
Campaigns::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['campaigns']));
// Delete campaigns - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Campaign.Delete.Post', explode(',', $ArrayAPIData['campaigns']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>