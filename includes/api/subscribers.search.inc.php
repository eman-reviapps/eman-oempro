<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'listid' => 1,
    'operator' => 2
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ArrayErrorFields
    );
    throw new Exception('');
}
// Check for required fields - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('segments');
// Load other modules - End

if (!isset($ArrayAPIData['recordsperrequest']) || $ArrayAPIData['recordsperrequest'] == '') {
    $ArrayAPIData['recordsperrequest'] = 25;
}
if (!isset($ArrayAPIData['recordsfrom']) || $ArrayAPIData['recordsfrom'] == '') {
    $ArrayAPIData['recordsfrom'] = 0;
}
if (!isset($ArrayAPIData['orderfield']) || $ArrayAPIData['orderfield'] == '') {
    $ArrayAPIData['orderfield'] = 'EmailAddress';
}
if (!isset($ArrayAPIData['ordertype']) || $ArrayAPIData['ordertype'] == '') {
    $ArrayAPIData['ordertype'] = 'ASC';
}

// Retrieve list and check if it belongs to authenticated user - Start
$ArrayListInformation = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['listid']), false, false, array());
if ($ArrayListInformation != false && $ArrayListInformation['RelOwnerUserID'] != $ArrayUserInformation['UserID']) {
    $ArrayOutput = array(
        'Success' => false,
        'ErrorCode' => 3
    );
    throw new Exception('');
}
// Retrieve list and check if it belongs to authenticated user - End
// Retrieve subscribers - Start {
// Add global custom field values to subscribers - Start {
Core::LoadObject('lists');
Core::LoadObject('custom_fields');

$ArrayGlobalCustomFields = CustomFields::RetrieveFields(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'IsGlobal' => 'Yes'));
$ArrayGlobalCustomFieldSelects = array();
if ($ArrayGlobalCustomFields != false) {
    foreach ($ArrayGlobalCustomFields as $EachField) {
        $FieldName = 'ValueText';
        $NullDefault = '""';
        if ($EachField['FieldType'] == 'Date field' || $EachField['ValidationMethod'] == 'Date') {
            $FieldName = 'ValueDate';
            $NullDefault = '"0000-00-00"';
        } else if ($EachField['FieldType'] == 'Time field' || $EachField['ValidationMethod'] == 'Time') {
            $FieldName = 'ValueTime';
            $NullDefault = '"00:00:00"';
        } else if ($EachField['ValidationMethod'] == 'Numbers') {
            $FieldName = 'ValueDouble';
            $NullDefault = '0';
        }

        $ArrayGlobalCustomFieldSelects[] = 'IFNULL((SELECT ' . $FieldName . ' FROM ' . MYSQL_TABLE_PREFIX . 'custom_field_values WHERE RelFieldID = ' . $EachField['CustomFieldID'] . ' AND EmailAddress = ' . MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid'] . '.EmailAddress LIMIT 1), ' . $NullDefault . ') AS CustomField' . $EachField['CustomFieldID'];
    }
}
// Add global custom field values to subscribers - End }


$SQLQuery = Segments::GetSegmentSQLQuery_Enhanced(0, false, $ArrayAPIData['rules'], $ArrayAPIData['operator'], $ArrayAPIData['listid']);
$SQLJoin = Segments::GetSegmentJoinQuery(0, false, '', $ArrayAPIData['rules'], $ArrayAPIData['listid']);

$Subscribers = Database::$Interface->GetRows_Enhanced(array(
    'Fields' => array_merge($ArrayGlobalCustomFieldSelects, array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid'] . '.*')),
    'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid']),
    'Joins' => $SQLJoin,
    'Criteria' => $SQLQuery,
    'Pagination' => array('Offset' => $ArrayAPIData['recordsfrom'], 'Rows' => $ArrayAPIData['recordsperrequest']),
    'RowOrder' => array('Column' => $ArrayAPIData['orderfield'], 'Type' => $ArrayAPIData['ordertype']),
        ));

$SubscriberCount = Database::$Interface->GetRows_Enhanced(array(
    'Fields' => array('COUNT(*) AS TotalSubscribers'),
    'Tables' => array(MYSQL_TABLE_PREFIX . 'subscribers_' . $ArrayAPIData['listid']),
    'Joins' => $SQLJoin,
    'Criteria' => $SQLQuery
        ));
// Retrieve subscribers - End }
// Return results - Start
$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'ErrorText' => '',
    'Subscribers' => $Subscribers,
    'TotalSubscribers' => $SubscriberCount[0]['TotalSubscribers']
);
// Return results - End