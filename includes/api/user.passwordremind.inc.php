<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'forgot_password');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailaddress'	=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Email address is missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
if (FormHandler::EmailValidator($ArrayAPIData['emailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						 'ErrorText'		=> array('Invalid email address'),
						);
	throw new Exception('');
	}
// Field validations - End

// Validate if such an email address exists in the database - Start
$ArrayCriterias = array(
						'EmailAddress'		=> $ArrayAPIData['emailaddress'],
						);
$ArrayUser = Users::RetrieveUser(array('*'), $ArrayCriterias);

if ($ArrayUser == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(3),
						 'ErrorText'		=> array('Email address not found in the database'),
						);
	throw new Exception('');
	}
// Validate if such an email address exists in the database - End

// Send password reminder email to the user - Start
$Username = $ArrayUser['Username'];
$UserID = $ArrayUser['UserID'];
if ($ArrayAPIData['customresetlink'] != '')
	{
	$PasswordResetLink	= sprintf(base64_decode(rawurldecode($ArrayAPIData['customresetlink'])), md5($UserID));
	}
else
	{
	$PasswordResetLink = Core::InterfaceAppURL().'/user/reset_password.php?reset='.md5($UserID);
	}

O_Email_Sender_ForAdmin::send(
	O_Email_Factory::passwordRemind($ArrayUser['EmailAddress'], $Username, $PasswordResetLink)
);
// Send password reminder email to the user - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End
?>