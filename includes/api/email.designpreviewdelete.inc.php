<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							'jobid'					=> 2,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('emails');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
// Retrieve email information - End

// Retrieve design job information - Start
$ArrayDesignPreviewJob = Emails::RetrieveDesignPreviewJob(array('*'), array('JobID' => $ArrayAPIData['jobid'], 'RelEmailID' => $ArrayEmail['EmailID'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']));
if ($ArrayDesignPreviewJob == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 4
						);
	throw new Exception('');
	}
// Retrieve design job information - End
// Field validations - End

// Delete the job via PreviewMyEmail.com API - Start
$ArrayPostParameters = array(
							'apikey='.PME_APIKEY,
							'jobid='.$ArrayDesignPreviewJob['PreviewMyEmailJobID'],
							);
$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'DeletePreview/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
// Delete the job via PreviewMyEmail.com API - End

// Parse the returned data - Start
if ($ArrayReturn[0] == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5
						);
	throw new Exception('');
	}
else
	{
	$ObjectXML = simplexml_load_string($ArrayReturn[1]);
	if ((string)$ObjectXML->result == 'true')
		{
		}
	else
		{
		// Preview does not exist
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 4
							);
		throw new Exception('');
		}
	}
// Parse the returned data - End

// Delete the job record in the database - Start
Emails::DeleteDesignPreviewJob(array('JobID' => $ArrayDesignPreviewJob['JobID']));
// Delete the job record in the database - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.DesignPreview.Delete.Post', array($ArrayUserInformation['UserID'], $ArrayDesignPreviewJob['JobID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					);
// Return results - End
?>