<?php

// Import process requires a lot of memory - Start {
@ini_set('auto_detect_line_endings', true);
// Import process requires a lot of memory - End }


$ArrayImportFields = array();
global $ArrayImportFields;
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
// Load other modules - End
// If import step is 1, proceed with creating the import record and returning fields found in the import data - Start
if ($ArrayAPIData['importstep'] == 1) {
    // Check for required fields - Start {
    $ArrayRequiredFields = array(
        'importtype' => 1,
    );

    if ($ArrayAPIData['importtype'] == 'Copy') {
        $ArrayRequiredFields['importdata'] = 2;
    } elseif ($ArrayAPIData['importtype'] == 'File') {
        $ArrayRequiredFields['importfilename'] = 9;
    } elseif ($ArrayAPIData['importtype'] == 'MySQL') {
        $ArrayRequiredFields['importmysqlhost'] = 10;
        $ArrayRequiredFields['importmysqlport'] = 11;
        $ArrayRequiredFields['importmysqldatabase'] = 12;
        $ArrayRequiredFields['importmysqlquery'] = 14;
    }

    $ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

    if (count($ArrayErrorFields) > 0) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => $ArrayErrorFields
        );
        throw new Exception('');
    }
    // Check for required fields - End }
    // Field validations - Start
    // Be sure that the subscriber list belongs to the logged in user - Start {
    $ArraySubscriberList = Lists::RetrieveList(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'ListID' => $ArrayAPIData['listid']));

    if ($ArraySubscriberList == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 4
        );
        throw new Exception('');
    }
    // Be sure that the subscriber list belongs to the logged in user - End }
    // Be sure that import type is supported - Start {
    if (($ArrayAPIData['importtype'] != 'Copy') && ($ArrayAPIData['importtype'] != 'File') && ($ArrayAPIData['importtype'] != 'MySQL')) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 17
        );
        throw new Exception('');
    }
    // Be sure that import type is supported - End }
    // If import type is MySQL, be sure that connection to target MySQL database table is correct - Start  {
    if ($ArrayAPIData['importtype'] == 'MySQL') {
        $ObjectMySQLTest = new MySqlDatabaseInterface();

        $ConnectionResult = $ObjectMySQLTest->Connect($ArrayAPIData['importmysqlhost'] . ':' . $ArrayAPIData['importmysqlport'], $ArrayAPIData['importmysqlusername'], $ArrayAPIData['importmysqlpassword'], $ArrayAPIData['importmysqldatabase'], '');

        if ($ConnectionResult == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 15
            );
            throw new Exception('');
        }

        $ResultSet = $ObjectMySQLTest->ExecuteQuery($ArrayAPIData['importmysqlquery']);

        if ($ResultSet == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 16,
                'MySQLError' => $ObjectMySQLTest->ReturnError()
            );
            throw new Exception('');
        }

        $ObjectMySQLTest->CloseConnection();
    }
    // If import type is MySQL, be sure that connection to target MySQL database table is correct - End }
    // If import type is file, be sure that source file exists - Start {
    if ($ArrayAPIData['importtype'] == 'File') {
        if (file_exists(DATA_PATH . '/imports/' . $ArrayAPIData['importfilename']) == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 13
            );
            throw new Exception('');
        }
    }
    // If import type is file, be sure that source file exists - End }
    // If import type is File, be sure that the uploaded file size does not exceed the maximum allowed size - Start {
    if ($ArrayAPIData['importtype'] == 'File') {
        $UploadMaxFilesize = ini_get('upload_max_filesize');
        $UploadMaxFilesize_type = strtolower(substr($UploadMaxFilesize, strlen($UploadMaxFilesize) - 1));
        $UploadMaxFilesize = substr($UploadMaxFilesize, 0, strlen($UploadMaxFilesize) - 1);
        $UploadMaxFilesize = ($UploadMaxFilesize_type == "m" ? $UploadMaxFilesize * (1024 * 1024) : ($UploadMaxFilesize_type == "k" ? $UploadMaxFilesize * 1024 : $UploadMaxFilesize ));
        $MaxUploadSize = ($UploadMaxFilesize < IMPORT_MAX_FILESIZE ? $UploadMaxFilesize : IMPORT_MAX_FILESIZE);

        if ($MaxUploadSize < filesize(DATA_PATH . '/imports/' . $ArrayAPIData['importfilename'])) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 18,
                'AllowedMaxSize' => $MaxUploadSize,
            );
            throw new Exception('');
        }
    }
    // If import type is File, be sure that the uploaded file size does not exceed the maximum allowed size - End }
    // Field validations - End
    // Create the import record in the database - Start
    if ($ArrayAPIData['importtype'] == 'Copy') {
        $ImportType = 'Copy';
        $FilePath = '';
        $MySQLHost = '';
        $MySQLPort = '';
        $MySQLUsername = '';
        $MySQLPassword = '';
        $MySQLDatabase = '';
        $MySQLQuery = '';
        $FieldTerminator = $ArrayAPIData['fieldterminator'];
        $FieldEncloser = $ArrayAPIData['fieldencloser'];
    } elseif ($ArrayAPIData['importtype'] == 'File') {
        $ImportType = 'File';
        $FilePath = DATA_PATH . '/imports/' . $ArrayAPIData['importfilename'];
        $MySQLHost = '';
        $MySQLPort = '';
        $MySQLUsername = '';
        $MySQLPassword = '';
        $MySQLDatabase = '';
        $MySQLQuery = '';
        $FieldTerminator = $ArrayAPIData['fieldterminator'];
        $FieldEncloser = $ArrayAPIData['fieldencloser'];
    } elseif ($ArrayAPIData['importtype'] == 'MySQL') {
        $ImportType = 'MySQL';
        $FilePath = '';
        $MySQLHost = $ArrayAPIData['importmysqlhost'];
        $MySQLPort = $ArrayAPIData['importmysqlport'];
        $MySQLUsername = $ArrayAPIData['importmysqlusername'];
        $MySQLPassword = $ArrayAPIData['importmysqlpassword'];
        $MySQLDatabase = $ArrayAPIData['importmysqldatabase'];
        $MySQLQuery = $ArrayAPIData['importmysqlquery'];
        $FieldTerminator = '';
        $FieldEncloser = '';
    } else {
        $ImportType = 'Copy';
        $FilePath = '';
        $MySQLHost = '';
        $MySQLPort = '';
        $MySQLUsername = '';
        $MySQLPassword = '';
        $MySQLDatabase = '';
        $MySQLQuery = '';
        $FieldTerminator = $ArrayAPIData['fieldterminator'];
        $FieldEncloser = $ArrayAPIData['fieldencloser'];
    }

    $ArrayReturn = Subscribers::Import($ArrayUserInformation['UserID'], $ArrayAPIData['listid'], $ImportType, $ArrayAPIData['importdata'], $FilePath, $MySQLHost, $MySQLPort, $MySQLUsername, $MySQLPassword, $MySQLDatabase, $MySQLQuery, $FieldTerminator, $FieldEncloser);

    if ($ArrayReturn[0] == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => $ArrayReturn[1]
        );
        throw new Exception('');
    } else {
        $ImportID = $ArrayReturn[1];
    }
    // Create the import record in the database - End
    // Return results - Start
    $ArrayOutput = array(
        'Success' => true,
        'ErrorCode' => 0,
        'ErrorText' => '',
        'ImportID' => $ImportID,
        'ImportFields' => $ArrayImportFields[0],
    );
    // Return results - End
} elseif ($ArrayAPIData['importstep'] == 2) {
    // Check the status of the import process and retrieve its details - Start
    $ArrayImportRecord = Subscribers::GetImportRecord($ArrayUserInformation['UserID'], $ArrayAPIData['listid'], $ArrayAPIData['importid'], array());
    $ArrayImportRecord = $ArrayImportRecord[0];

    if (($ArrayImportRecord['ImportStatus'] != 'Not Ready') && ($ArrayImportRecord['ImportStatus'] != 'Pending')) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 6
        );
        throw new Exception('');
    }
    // Check the status of the import process and retrieve its details - End
    // Retrieve the list information - Start
    $ArraySubscriberList = Lists::RetrieveList(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'ListID' => $ArrayAPIData['listid']));

    if ($ArraySubscriberList == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 4
        );
        throw new Exception('');
    }
    // Retrieve the list information - End
    // Parse import data - Start
    if (substr($ArrayImportRecord['ImportData'], 0, strlen('FILE:')) == 'FILE:') {
        // Parse the import data - Start {
        $ArrayImportFields = array();
        $FileHandler = fopen(str_replace('FILE:', '', $ArrayImportRecord['ImportData']), 'r');
        if ($ArrayImportRecord['FieldEncloser'] == '') {
            $TMPCounter = 0;
            while (($EachLine = fgetcsv($FileHandler, 1000, ($ArrayImportRecord['FieldTerminator'] == '' ? '|' : $ArrayImportRecord['FieldTerminator']))) !== false) {
                foreach ($EachLine as $Index => $EachValue) {
                    $ArrayImportFields[$TMPCounter]['FIELD' . ($Index + 1)] = $EachValue;
                }
                $TMPCounter++;
            }
        } else {
            $TMPCounter = 0;
            while (($EachLine = fgetcsv($FileHandler, 1000, $ArrayImportRecord['FieldTerminator'], $ArrayImportRecord['FieldEncloser'])) !== false) {
                foreach ($EachLine as $Index => $EachValue) {
                    $ArrayImportFields[$TMPCounter]['FIELD' . ($Index + 1)] = $EachValue;
                }
                $TMPCounter++;
            }
        }
        fclose($FileHandler);
        // Parse the import data - End }
        // Load CSV parser - Start
        // include_once(LIBRARY_PATH.'/magicparser.inc.php');
        // Load CSV parser - End
        // Import data stored in the file (file and databse)
        // $CSVFormatString = MagicParser_getFormat(str_replace('FILE:', '', $ArrayImportRecord['ImportData']));
        // 
        // if ($CSVFormatString == false)
        // 	{
        // 	$ArrayOutput = array('Success'			=> false,
        // 						 'ErrorCode'		=> 5
        // 						);
        // 	throw new Exception('');
        // 	}
        // else
        // 	{
        // 	MagicParser_parse(str_replace('FILE:', '', $ArrayImportRecord['ImportData']), "CSVHandler", $CSVFormatString);
        // 	}
    } else {
        // Load CSV parser - Start
        include_once(LIBRARY_PATH . '/magicparser.inc.php');
        // Load CSV parser - End
        // Import data stored in the database (Copy method)
        $CSVFormatString = MagicParser_getFormat("string://" . $ArrayImportRecord['ImportData']);

        if ($CSVFormatString == false) {
            $ArrayOutput = array('Success' => false,
                'ErrorCode' => 5
            );
            throw new Exception('');
        } else {
            MagicParser_parse("string://" . $ArrayImportRecord['ImportData'], "CSVHandler", $CSVFormatString);
        }
    }
    // Parse import data - End
    // Check if email address field is mapped - Start
    $IsEmailDefined = false;
    $FieldCounter = 1;
    $TotalImported = 0;
    $TotalDuplicates = 0;
    $TotalFailed = 0;
    $TotalData = count($ArrayImportFields);

    foreach ($ArrayImportFields[0] as $Field => $Value) {
        if ($ArrayAPIData['mappedfields'][$Field] == 'EmailAddress') {

            // If email address field is defined more than one time, display error - Start
            if ($IsEmailDefined != false) {
                $ArrayOutput = array('Success' => false,
                    'ErrorCode' => 7
                );
                throw new Exception('');
            } else {
                $IsEmailDefined = $FieldCounter;
            }
            // If email address field is defined more than one time, display error - End
        }
        $FieldCounter++;
    }

    if ($IsEmailDefined == false) {
        $ArrayOutput = array('Success' => false,
            'ErrorCode' => 8
        );
        throw new Exception('');
    }
    // Check if email address field is mapped - End
    // Update import record status and totals - Start
    $ArrayUpdateFields = array(
        'ImportStatus' => 'Importing',
        'TotalSubscribers' => $TotalData,
    );
    Subscribers::UpdateImportRecord($ArrayUserInformation['UserID'], $ArrayAPIData['listid'], $ArrayImportRecord['ImportID'], $ArrayUpdateFields, array());
    // Update import record status and totals - End
    // Import subscribers - Start
    set_time_limit(0);
    ignore_user_abort(true);

    $ImportStartTimeStamp = time();

    if ($ArrayAPIData['addtosuppressionlist'] == 'true' || $ArrayAPIData['addtoglobalsuppressionlist'] == 'true') {
        Core::LoadObject('suppression_list');
    }

    $ArrayAPIData['sendconfirmationemail'] = isset($ArrayAPIData['sendconfirmationemail']) ? $ArrayAPIData['sendconfirmationemail'] : 'false';
    $ArrayAPIData['triggerbehaviors'] = isset($ArrayAPIData['triggerbehaviors']) ? $ArrayAPIData['triggerbehaviors'] : 'false';
    $ArrayAPIData['updateduplicates'] = isset($ArrayAPIData['updateduplicates']) ? $ArrayAPIData['updateduplicates'] : 'true';

    // Check subscriber limit for the user account and reduce the import data if it exceeds the limit - Start {
    $TotalLimited = 0;
    if (($ArrayAPIData['addtosuppressionlist'] != 'true' && $ArrayAPIData['addtoglobalsuppressionlist'] != 'true') && ($ArrayUserInformation['GroupInformation']['LimitSubscribers'] > 0)) {
//        $TotalSubscribersOnTheAccount = Subscribers::TotalSubscribersOnTheAccount($ArrayUserInformation['UserID']);
        //Edited By Eman
        $TotalSubscribersOnTheAccount = Subscribers::getTotalSubscribersOnTheAccount_Enhanced($ArrayUserInformation['UserID']);
//        print_r($TotalSubscribersOnTheAccount);
//        print_r($TotalData);
//        exit();
        if (($TotalSubscribersOnTheAccount + $TotalData) > $ArrayUserInformation['GroupInformation']['LimitSubscribers'] && ($ArrayUserInformation['GroupInformation']['LimitSubscribers'] > 0)) {
            $TotalAllowedSubscribers = $ArrayUserInformation['GroupInformation']['LimitSubscribers'] - $TotalSubscribersOnTheAccount;

            if ($ArrayAPIData['updateduplicates'] == 'true') {
                $TMPArrayDuplicateData = array();
                $TMPArrayNonDuplicateData = array();
                $TMPArrayWrongData = array();

                foreach ($ArrayImportFields as $Key => $ArrayEachData) {
                    foreach ($ArrayEachData as $Field => $Value) {
                        if ($ArrayAPIData['mappedfields'][$Field] == 'EmailAddress') {
                            $ImportEmailAddressField = $Value;
                        }
                    }
                    $SubscriberInformation = Subscribers::RetrieveSubscriber(array('SubscriberID'), array('EmailAddress' => $ImportEmailAddressField), $ArraySubscriberList['ListID']);

                    $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                                'EmailAddress' => $ImportEmailAddressField,
                                'RelListID' => 0
                                    ), 5, 0);

                    //wont be added to this list
                    $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                                'EmailAddress' => $ImportEmailAddressField,
                                'PrevSubscriptionStatus' => 'Unsubscribed',
                                'RelListID' => $ArraySubscriberList['ListID']
                                    ), 5, 0);

                    if ($SubscriberInformation != false) {
                        $TMPArrayDuplicateData[$Key] = $ArrayEachData;
                    } else if ($global_suppress || $list_suppress_unsubscribe) {
                        $TMPArrayWrongData[$Key] = $ArrayEachData;
                    } else if (strpos($ImportEmailAddressField, '@') === false) {
                        $TMPArrayWrongData[$Key] = $ArrayEachData;
                    } else {
                        $TMPArrayNonDuplicateData[$Key] = $ArrayEachData;
                    }
                }
                $TMPArrayNonDuplicateData = array_slice($TMPArrayNonDuplicateData, 0, $TotalAllowedSubscribers);

                $ArrayImportFields = array_merge($TMPArrayNonDuplicateData, $TMPArrayDuplicateData);
//                $ArrayImportFields = array_merge($ArrayImportFields, $TMPArrayWrongData);

                unset($TMPArrayDuplicateData);
                unset($TMPArrayNonDuplicateData);
                unset($TMPArrayWrongData);
            } else {
                //Edited by Eman
                $TMPArrayNonWrongData = array();
                $TMPArrayWrongData = array();

                foreach ($ArrayImportFields as $Key => $ArrayEachData) {
                    foreach ($ArrayEachData as $Field => $Value) {
                        if ($ArrayAPIData['mappedfields'][$Field] == 'EmailAddress') {
                            $ImportEmailAddressField = $Value;
                        }
                    }

                    $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                                'EmailAddress' => $ImportEmailAddressField,
                                'RelListID' => 0
                                    ), 5, 0);

                    //wont be added to this list
                    $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                                'EmailAddress' => $ImportEmailAddressField,
                                'PrevSubscriptionStatus' => 'Unsubscribed',
                                'RelListID' => $ArraySubscriberList['ListID']
                                    ), 5, 0);

                    if ($global_suppress || $list_suppress_unsubscribe) {
                        $TMPArrayWrongData[$Key] = $ArrayEachData;
                    } else if (strpos($ImportEmailAddressField, '@') === false) {
                        $TMPArrayWrongData[$Key] = $ArrayEachData;
                    } else {
                        $TMPArrayNonWrongData[$Key] = $ArrayEachData;
                    }
                }
                $ArrayImportFields = array_slice($TMPArrayNonWrongData, 0, $TotalAllowedSubscribers);

                unset($TMPArrayNonWrongData);
                unset($TMPArrayWrongData);
                //$ArrayImportFields = array_slice($ArrayImportFields, 0, $TotalAllowedSubscribers);
            }

            $TotalLimited = $TotalData - count($ArrayImportFields);
        }
    }
    // Check subscriber limit for the user account and reduce the import data if it exceeds the limit - End }

    foreach ($ArrayImportFields as $Key => $ArrayEachData) {
        // Generate subscriber fields to be imported (except email address) - Start
        $ArrayOtherFields = array();
        $GlobalCustomFields = array();

        foreach ($ArrayEachData as $Field => $Value) {
            if (($ArrayAPIData['mappedfields'][$Field] != 0) && ($ArrayAPIData['mappedfields'][$Field] != 'EmailAddress')) {
                $ArrayOtherFields['CustomField' . $ArrayAPIData['mappedfields'][$Field]] = $Value;
            } elseif ($ArrayAPIData['mappedfields'][$Field] == 'EmailAddress') {
                $ImportEmailAddressField = $Value;
            }
        }
        // Generate subscriber fields to be imported (except email address) - End

        if ($ArrayAPIData['addtosuppressionlist'] == 'true') {
            // Add to suppression list
            $ArrayFieldAndValues = array(
                'SuppressionID' => '',
                'RelListID' => $ArraySubscriberList['ListID'],
                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                'SuppressionSource' => 'User',
                'EmailAddress' => $ImportEmailAddressField,
                'PrevSubscriptionStatus' => 'Subscribed',
                'SourceList' => $ArraySubscriberList['ListID']
            );
            $ArrayResult = SuppressionList::Add($ArrayFieldAndValues);
            $TotalImported++;
        } else if ($ArrayAPIData['addtoglobalsuppressionlist'] == 'true') {
            // Add to global suppression list
            $ArrayFieldAndValues = array(
                'SuppressionID' => '',
                'RelListID' => 0,
                'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                'SuppressionSource' => 'User',
                'EmailAddress' => $ImportEmailAddressField,
                'PrevSubscriptionStatus' => 'Subscribed',
                'SourceList' => $ArraySubscriberList['ListID']
            );
            $ArrayResult = SuppressionList::Add($ArrayFieldAndValues);
            $TotalImported++;
        } else {
            /* Eman */
            //check if subscriber exists in suppression list or not  - Start
            $global_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                        'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                        'EmailAddress' => $ImportEmailAddressField,
                        'RelListID' => 0
                            ), 5, 0);

            //wont be added to this list
            $list_suppress_unsubscribe = SuppressionList::GetEmailAddresses(array('*'), array(
                        'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                        'EmailAddress' => $ImportEmailAddressField,
                        'PrevSubscriptionStatus' => 'Unsubscribed',
                        'RelListID' => $ArraySubscriberList['ListID']
                            ), 5, 0);
            // can be added 3ady
            $list_suppress = SuppressionList::GetEmailAddresses(array('*'), array(
                        'RelOwnerUserID' => $ArrayUserInformation['UserID'],
                        'EmailAddress' => $ImportEmailAddressField,
                        'RelListID' => $ArraySubscriberList['ListID']
                            ), 5, 0);
            /* Eman */

            //check if subscriber exists in suppression list or not  - End
            // Add to subscriber list
            $SubscriptionParameters = array(
                'CheckSubscriberLimit' => false,
                'UserInformation' => $ArrayUserInformation,
                'ListInformation' => $ArraySubscriberList,
                'EmailAddress' => $ImportEmailAddressField,
                'IPAddress' => $_SERVER['REMOTE_ADDR'] . ' - Manual Import',
                'SubscriptionStatus' => '',
                'OtherFields' => $ArrayOtherFields,
                'SendConfirmationEmail' => false,
                'UpdateIfDuplicate' => false,
                'UpdateIfUnsubscribed' => true,
                'ApplyBehaviors' => false,
                'UpdateStatistics' => false,
                'TriggerWebServices' => false,
                'TriggerAutoResponders' => false
            );

            if ($ArrayAPIData['updateduplicates'] == 'true') {
                $SubscriptionParameters['UpdateIfDuplicate'] = true;
            }

            if ($ArrayAPIData['sendconfirmationemail'] == 'true') {
                $SubscriptionParameters['SendConfirmationEmail'] = true;
                $SubscriptionParameters['SubscriptionStatus'] = '';
            } else {
                $SubscriptionParameters['SubscriptionStatus'] = 'Subscribed';
            }

            if ($ArrayAPIData['triggerbehaviors'] == 'true') {
                $SubscriptionParameters['ApplyBehaviors'] = true;
//                $SubscriptionParameters['UpdateStatistics'] = true;
                $SubscriptionParameters['TriggerWebServices'] = true;
                $SubscriptionParameters['TriggerAutoResponders'] = true;
            }
            
            $SubscriptionParameters['UpdateStatistics'] = false;
            
            if ($global_suppress || $list_suppress_unsubscribe) {
                //suppressed. don't add it
                $SQLQuery = "UPDATE " . MYSQL_TABLE_PREFIX . "subscriber_imports SET FailedData = CONCAT(FailedData, '\n" . mysql_real_escape_string($ImportEmailAddressField) . "'), TotalFailed = TotalFailed + 1 WHERE ImportID='" . $ArrayImportRecord['ImportID'] . "'";
                Database::$Interface->ExecuteQuery($SQLQuery);
                $TotalFailed++;
            } else {

                $SubscriptionResult = Subscribers::AddSubscriber_Enhanced($SubscriptionParameters);
                if ($SubscriptionResult[0] == false) {
                    switch ($SubscriptionResult[1]) {
                        case 3:
                            // Duplicate email address
                            $SQLQuery = "UPDATE " . MYSQL_TABLE_PREFIX . "subscriber_imports SET DuplicateData = CONCAT(DuplicateData, '\n" . mysql_real_escape_string($ImportEmailAddressField) . "'), TotalDuplicates = TotalDuplicates + 1 WHERE ImportID='" . $ArrayImportRecord['ImportID'] . "'";
                            Database::$Interface->ExecuteQuery($SQLQuery);
                            $TotalDuplicates++;
                            break;
                        default:
                            // Invalid email address
                            $SQLQuery = "UPDATE " . MYSQL_TABLE_PREFIX . "subscriber_imports SET FailedData = CONCAT(FailedData, '\n" . mysql_real_escape_string($ImportEmailAddressField) . "'), TotalFailed = TotalFailed + 1 WHERE ImportID='" . $ArrayImportRecord['ImportID'] . "'";
                            Database::$Interface->ExecuteQuery($SQLQuery);
                            $TotalFailed++;
                            break;
                    }
                } else {
                    // Import successful
                    $SQLQuery = "UPDATE " . MYSQL_TABLE_PREFIX . "subscriber_imports SET TotalImported = TotalImported + 1 WHERE ImportID='" . $ArrayImportRecord['ImportID'] . "'";
                    Database::$Interface->ExecuteQuery($SQLQuery);
                    $TotalImported++;

                    if ($list_suppress && !$list_suppress_unsubscribe) {
                        //remove it from suppression list
                        SuppressionList::DeleteEmail($ImportEmailAddressField, $ArraySubscriberList['ListID'], $ArrayUserInformation['UserID']);
                    }
                }
            }
        }
    }

    $ImportEndTimeStamp = time();
    // Import subscribers - End
    // Delete the temporary uploaded import file - Start
    if (substr($ArrayImportRecord['ImportData'], 0, strlen('FILE:')) == 'FILE:') {
        unlink(str_replace('FILE:', '', $ArrayImportRecord['ImportData']));
    }
    // Delete the temporary uploaded import file - End
    // Update import record status and process duration - Start
    $ArrayUpdateFields = array(
        'ImportStatus' => 'Completed',
        'ImportDuration' => ($ImportEndTimeStamp - $ImportStartTimeStamp),
    );
    Subscribers::UpdateImportRecord($ArrayUserInformation['UserID'], $ArrayAPIData['listid'], $ArrayImportRecord['ImportID'], $ArrayUpdateFields, array());
    // Update import record status and process duration - End
    // Update subscriber list activity statistics - Start
    Core::LoadObject('statistics');

    $ArrayActivities = array(
        'TotalImport' => $TotalImported,
    );
    Statistics::UpdateListActivityStatistics($ArraySubscriberList['ListID'], $ArrayUserInformation['UserID'], $ArrayActivities);
    // Update subscriber list activity statistics - End
    // Check if activity exceeds user group threshold - Start {
    if (($ArrayUserInformation['GroupInformation']['ThresholdImport'] > 0) && ($TotalImported > $ArrayUserInformation['GroupInformation']['ThresholdImport'])) {
        O_Email_Sender_ForAdmin::send(
                O_Email_Factory::userExceededImportThreshold(
                        $ArrayUserInformation, $TotalImported)
        );

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Threshold.SubscriberImport', array($ArrayUserInformation, $TotalImported, $TotalData, $TotalDuplicates, $TotalFailed));
        // Plug-in hook - End
    }
    // Check if activity exceeds user group threshold - End }
    // Revalidate subscriber count cache of all segments of the target list - Start
    Core::LoadObject('segments');
    Segments::Update(array(
        'SubscriberCountLastCalculatedOn' => date('Y-m-d H:i:s', strtotime('-2 days'))
            ), array(
        'RelListID' => $ArraySubscriberList['ListID']
    ));
    // Revalidate subscriber count cache of all segments of the target list - End
    // Return results - Start
    $ArrayOutput = array(
        'Success' => true,
        'ErrorCode' => 0,
        'ImportID' => $ArrayImportRecord['ImportID'],
        'TotalData' => $TotalData,
        'TotalImported' => $TotalImported,
        'TotalDuplicates' => $TotalDuplicates,
        'TotalFailed' => $TotalFailed,
        'TotalLimited' => $TotalLimited
    );
    // Return results - End
} else {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 3
    );
    throw new Exception('');
}
// If import step is 1, proceed with creating the import record and returning fields found in the import data - End

/**
 * Retrieves parsed CSV data
 *
 * @return void
 * @author Cem Hurturk
 * */
function CSVHandler($ParsedResult) {
    global $ArrayImportFields;
    $ArrayImportFields[] = $ParsedResult;
}

?>