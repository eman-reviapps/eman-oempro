<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'tagid'					=> 1,
							'campaignids'			=> 2
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('tags');
// Load other modules - End

// Assign campaigns to client - Start
Tags::UnassignFromCampaigns(explode(',', $ArrayAPIData['campaignids']), $ArrayAPIData['tagid'], $ArrayUserInformation['UserID']);
Tags::AssignToCampaigns(explode(',', $ArrayAPIData['campaignids']), $ArrayAPIData['tagid'], $ArrayUserInformation['UserID']);
// Assign campaigns to client - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>