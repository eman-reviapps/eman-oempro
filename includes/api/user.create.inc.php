<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'relusergroupid' => 1,
    'emailaddress' => 2,
    'username' => 3,
    'password' => 4,
    'firstname' => 6,
    'lastname' => 7,
    'timezone' => 8,
    'language' => 9,
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ArrayErrorFields,
    );
    throw new Exception('');
}
// Check for required fields - End
// Load other modules - Start
Core::LoadObject('users');
Core::LoadObject('user_groups');
Core::LoadObject('subscribers');
// Load other modules - End
// Field validations - Start
// Validate reputation level - Start
if (($ArrayAPIData['reputationlevel'] != 'Trusted') && ($ArrayAPIData['reputationlevel'] != 'Untrusted') && ($ArrayAPIData['reputationlevel'] != '')) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 15,
    );
    throw new Exception('');
}
// Validate reputation level - End
// Validate email address - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['emailaddress']) == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 10,
    );
    throw new Exception('');
}
// Validate email address - End
// Validate user group ID - Start
$ArrayUserGroup = UserGroups::RetrieveUserGroup($ArrayAPIData['relusergroupid']);

if ($ArrayUserGroup == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 11,
    );
    throw new Exception('');
}
// Validate user group ID - End
// Validate username/password - Start
$TotalFound = Users::RetrieveUsers(array('COUNT(*) AS TotalFound'), array('Username' => $ArrayAPIData['username']), array('UserID' => 'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 12,
    );
    throw new Exception('');
}
// Validate username/password - End
// Validate email address - Start
$TotalFound = Users::RetrieveUsers(array('COUNT(*) AS TotalFound'), array('EmailAddress' => $ArrayAPIData['emailaddress']), array('UserID' => 'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 13,
    );
    throw new Exception('');
}
// Validate email address - End
// Validate time zone format - Start
// Validate time zone format - End
// Validate language - Start
$ArrayLanguages = Core::DetectLanguages();
$IsLanguageCodeOk = false;
foreach ($ArrayLanguages as $Index => $ArrayEachLanguage) {
    if (strtolower($ArrayEachLanguage['Code']) == strtolower($ArrayAPIData['language'])) {
        $IsLanguageCodeOk = true;
        break;
    }
}

if ($IsLanguageCodeOk == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 14,
    );
    throw new Exception('');
}
// Validate language - End
// Field validations - End
// Create Client - Start
$NewUserID = Users::Create(array(
            'UserID' => '',
            'RelUserGroupID' => $ArrayAPIData['relusergroupid'],
            'EmailAddress' => $ArrayAPIData['emailaddress'],
            'Username' => $ArrayAPIData['username'],
            'Password' => md5(OEMPRO_PASSWORD_SALT . $ArrayAPIData['password'] . OEMPRO_PASSWORD_SALT),
            'ReputationLevel' => ($ArrayAPIData['reputationlevel'] == '' ? 'Trusted' : $ArrayAPIData['reputationlevel']),
            'UserSince' => date('Y-m-d H:i:s'),
            'FirstName' => $ArrayAPIData['firstname'],
            'LastName' => $ArrayAPIData['lastname'],
            'CompanyName' => ($ArrayAPIData['companyname'] != '' ? $ArrayAPIData['companyname'] : ''),
            'Website' => ($ArrayAPIData['website'] != '' ? $ArrayAPIData['website'] : ''),
            'Street' => ($ArrayAPIData['street'] != '' ? $ArrayAPIData['street'] : ''),
            'City' => ($ArrayAPIData['city'] != '' ? $ArrayAPIData['city'] : ''),
            'State' => ($ArrayAPIData['state'] != '' ? $ArrayAPIData['state'] : ''),
            'Zip' => ($ArrayAPIData['zip'] != '' ? $ArrayAPIData['zip'] : ''),
            'Country' => ($ArrayAPIData['country'] != '' ? $ArrayAPIData['country'] : ''),
            'Phone' => ($ArrayAPIData['phone'] != '' ? $ArrayAPIData['phone'] : ''),
            'Fax' => ($ArrayAPIData['fax'] != '' ? $ArrayAPIData['fax'] : ''),
            'TimeZone' => ($ArrayAPIData['timezone'] != '' ? $ArrayAPIData['timezone'] : ''),
            'SignUpIPAddress' => 'By Administrator',
            'APIKey' => '',
            'Language' => ($ArrayAPIData['language'] != '' ? $ArrayAPIData['language'] : 'en'),
            'LastActivityDateTime' => '0000-00-00 00:00:00',
            'PreviewMyEmailAccount' => ($ArrayAPIData['previewmyemailaccount'] != '' ? $ArrayAPIData['previewmyemailaccount'] : ''),
            'PreviewMyEmailAPIKey' => ($ArrayAPIData['previewmyemailapikey'] != '' ? $ArrayAPIData['previewmyemailapikey'] : ''),
            'ForwardToFriendHeader' => ($ArrayAPIData['forwardtofriendheader'] != '' ? $ArrayAPIData['forwardtofriendheader'] : ''),
            'ForwardToFriendFooter' => ($ArrayAPIData['forwardtofriendfooter'] != '' ? $ArrayAPIData['forwardtofriendfooter'] : ''),
            'AccountStatus' => ($ArrayAPIData['accountstatus'] != '' ? $ArrayAPIData['accountstatus'] : 'Enabled'),
            'AvailableCredits' => ($ArrayAPIData['availablecredits'] != '' ? $ArrayAPIData['availablecredits'] : 0),
        ));
// Create Client - End
// Check if user ID is returned as 'false'. This means that max number of allowed user accounts in license exceeded - Start
if ($NewUserID == false) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => 16,
    );
    throw new Exception('');
}
// Check if user ID is returned as 'false'. This means that max number of allowed user accounts in license exceeded - End
// Plug-in hook - Start
Plugins::HookListener('Action', 'User.Create', array($NewUserID));
// Plug-in hook - End
// Return results - Start
$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'UserID' => $NewUserID
);
// Return results - End
?>