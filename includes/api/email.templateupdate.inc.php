<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'templateid'				=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('templates');
// Load other modules - End

// Field validations - Start
// Check if the email template is owned by the loggin user (for users only) - Start
if (AdminAuth::IsLoggedIn(false, false) == true)
	{
	$ArrayTemplate = Templates::RetrieveTemplate(array('*'), array('TemplateID' => $ArrayAPIData['templateid']), 'AND');

	if ($ArrayTemplate == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> array(2),
							);
		throw new Exception('');
		}
	}
elseif (UserAuth::IsLoggedIn(false,false))
	{
	$ArrayTemplate = Templates::RetrieveTemplate(array('*'), array('TemplateID' => $ArrayAPIData['templateid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']), 'AND');

	if ($ArrayTemplate == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> array(2),
							);
		throw new Exception('');
		}
	}
else
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// Check if the email template is owned by the loggin user (for users only) - End
// Field validations - End

// Prepare updated fields - Start
if ((isset($ArrayAPIData['templatename']) == true) && ($ArrayAPIData['templatename'] != ''))
	{
	$ArrayFieldnValues['TemplateName'] = $ArrayAPIData['templatename'];
	}
if ((isset($ArrayAPIData['templatedescription']) == true) && ($ArrayAPIData['templatedescription'] != ''))
	{
	$ArrayFieldnValues['TemplateDescription'] = $ArrayAPIData['templatedescription'];
	}
if ((isset($ArrayAPIData['templatesubject']) == true) && ($ArrayAPIData['templatesubject'] != ''))
	{
	$ArrayFieldnValues['TemplateSubject'] = $ArrayAPIData['templatesubject'];
	}
if ((isset($ArrayAPIData['templatehtmlcontent']) == true) && ($ArrayAPIData['templatehtmlcontent'] != ''))
	{
	$ArrayFieldnValues['TemplateHTMLContent'] = $ArrayAPIData['templatehtmlcontent'];
	}
if ((isset($ArrayAPIData['templateplaincontent']) == true) && ($ArrayAPIData['templateplaincontent'] != ''))
	{
	$ArrayFieldnValues['TemplatePlainContent'] = $ArrayAPIData['templateplaincontent'];
	}
if ((isset($ArrayAPIData['relowneruserid']) == true) && ($ArrayAPIData['relowneruserid'] != '') && (AdminAuth::IsLoggedIn(false,false) == true))
	{
	$ArrayFieldnValues['RelOwnerUserID'] = $ArrayAPIData['relowneruserid'];
	}



if ((isset($ArrayAPIData['templatethumbnailpath']) == true) && ($ArrayAPIData['templatethumbnailpath'] != '') && (file_exists(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']) == true))
	{
	$ArrayThumbnailInfo = getimagesize(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']);
	$ThumbnailType = $ArrayThumbnailInfo['mime'];

	$ArrayFieldnValues['TemplateThumbnail']		= base64_encode(file_get_contents(DATA_PATH.'tmp/'.$ArrayAPIData['templatethumbnailpath']));
	$ArrayFieldnValues['TemplateThumbnailType'] = $ThumbnailType;
	}
// Prepare updated fields - End

// Update email template - Start
Templates::Update($ArrayFieldnValues, array('TemplateID'=>$ArrayAPIData['templateid']));
// Update email template - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.Template.Update.Post', array($ArrayTemplate['TemplateID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End


?>