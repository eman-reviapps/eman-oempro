<?php
// Check for required fields - Start
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('campaigns');
// Load other modules - End

$ArrayCriterias = array();

if (!isset($ArrayAPIData['recordsperrequest']) || $ArrayAPIData['recordsperrequest'] == '')
	{
	$ArrayAPIData['recordsperrequest'] = 0;
	}
if (!isset($ArrayAPIData['recordsfrom']) || $ArrayAPIData['recordsfrom'] == '')
	{
	$ArrayAPIData['recordsfrom'] = 0;
	}

$ArrayCriterias["Campaigns.RelOwnerUserID"] = $ArrayUserInformation['UserID'];

if (isset($ArrayAPIData['campaignstatus']) && $ArrayAPIData['campaignstatus'] != '' && $ArrayAPIData['campaignstatus'] != 'All')
	{
	$ArrayCriterias['Campaigns.CampaignStatus'] = $ArrayAPIData['campaignstatus'];
	}

if (isset($ArrayAPIData['searchkeyword']) && $ArrayAPIData['searchkeyword'] != '')
	{
	$ArrayCriterias[] = array(
		"field"		=>	"Campaigns.CampaignName",
		"operator"	=>	"LIKE",
		"value"		=>	"%".$ArrayAPIData['searchkeyword']."%"
		);
	}

if (isset($ArrayAPIData['orderfield']) && $ArrayAPIData['orderfield'] != '' && isset($ArrayAPIData['ordertype']) && $ArrayAPIData['ordertype'] != '')
	{
	$ArrayOrder = array('Campaigns.'.$ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']);
	}
else
	{
	$ArrayOrder = array('Campaigns.CampaignName'=>'ASC');
	}

$RetrieveTags = (isset($ArrayAPIData['retrievetags']) ? $ArrayAPIData['retrievetags'] : false);
$ArrayTagIDs = ((isset($ArrayAPIData['tags']) && $ArrayAPIData['tags'] != '') ? explode(',',$ArrayAPIData['tags']) : array());
// Get campaigns - Start
$ArrayAPIData['retrievestatistics'] = isset($ArrayAPIData['retrievestatistics']) ? $ArrayAPIData['retrievestatistics'] : 'true';
if ($ArrayAPIData['retrievestatistics'] == 'false')
	{
	$ArrayStatisticsOptions = array();
	}
else if ($ArrayAPIData['retrievestatistics'] == 'true')
	{
	$ArrayStatisticsOptions = array(
		"Statistics"	=>	array(
			"OpenStatistics"			=>	true,
			"ClickStatistics"			=>	true,
			"UnsubscriptionStatistics"	=>	true
			),
		"Days"	=>	30
		);
	}
$ArrayCampaigns = Campaigns::RetrieveCampaigns(array('*'), $ArrayCriterias, $ArrayOrder, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], $ArrayStatisticsOptions, $ArrayTagIDs, $RetrieveTags);
$CampaignsTotal = Campaigns::RetrieveCampaigns(array('*'), $ArrayCriterias, $ArrayOrder, 0, 0, $ArrayStatisticsOptions, $ArrayTagIDs, $RetrieveTags, true);
// Get campaigns - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'Campaigns'		=> $ArrayCampaigns,
					 'TotalCampaigns'	=> $CampaignsTotal
					);
// Return results - End
?>