<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'urls'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Web service integration url ids are missing')
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('web_service_integration');
// Load other modules - End

// Delete custom fields - Start
WebServiceIntegration::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['urls']));
// Delete custom fields - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>