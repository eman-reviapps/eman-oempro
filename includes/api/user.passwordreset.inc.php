<?php
if (DEMO_MODE_ENABLED == true)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 'NOT AVAILABLE IN DEMO MODE.'
						);
	throw new Exception('');
	}

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'reset_password');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'userid'	=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('User id is missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('users');
// Load other modules - End

// Validate if such an user id exists in the database - Start
$ArrayCriterias = array(
						'MD5(`UserID`)'		=> $ArrayAPIData['userid'],
						);
$ArrayUser = Users::RetrieveUser(array('*'), $ArrayCriterias);

if ($ArrayUser == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						 'ErrorText'		=> array('Invalid user id'),
						);
	throw new Exception('');
	}
// Validate if such an user id exists in the database - End

// Reset user password - Start
$NewPassword = Core::GenerateRandomString(5);

	$ArrayFieldnValues	= array(
								'Password'		=> md5(OEMPRO_PASSWORD_SALT.$NewPassword.OEMPRO_PASSWORD_SALT),
								);
	$ArrayCriterias		= array(
								'UserID'		=> $ArrayUser['UserID'],
								);
Users::UpdateUser($ArrayFieldnValues, $ArrayCriterias);
// Reset user password - End

// Send password reminder email to the user - Start
$Username = $ArrayUser['Username'];
O_Email_Sender_ForAdmin::send(
	O_Email_Factory::passwordReset($ArrayUser['EmailAddress'], $Username, $NewPassword)
);
// Send password reminder email to the user - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End
?>