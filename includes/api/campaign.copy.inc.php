<?php

// Load other modules - Start
Core::LoadObject('campaigns');
Core::LoadObject('subscribers');
Core::LoadObject('emails');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'			=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Field validations - End

$Campaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $ArrayAPIData['campaignid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']), array(), true);
if (is_bool($Campaign) == true && $Campaign == false)
{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(1),
						 'ErrorText'		=> array('Invalid source campaign')
						);
	throw new Exception('');
}

$Email = Emails::RetrieveEmail(array('*'), array('EmailID' => $Campaign['RelEmailID']), false);
if ((is_bool($Email) == true && $Email == false) || is_null($Email) == true)
{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(1),
						 'ErrorText'		=> array('Invalid source campaign email content')
						);
	throw new Exception('');
}

$NewCampaignID = Campaigns::CopyCampaign($Campaign, array(
	'RelOwnerUserID' => $ArrayUserInformation['UserID'],
	'CampaignStatus' => 'Draft',
	'CampaignName' => sprintf(ApplicationHeader::$ArrayLanguageStrings['Screen']['9182'], $Campaign['CampaignName']),
	'ScheduleType' => 'Not Scheduled',
	'SendDate' => '0000-00-00',
	'SendTime' => '00:00:00',
	'ScheduleRecDaysOfWeek' => '',
	'ScheduleRecDaysOfMonth' => '',
	'ScheduleRecMonths' => '',
	'ScheduleRecHours' => '',
	'ScheduleRecMinutes' => '',
	'ScheduleRecSendMaxInstance' => 0,
	'ScheduleRecSentInstances' => 0,
	'SendProcessStartedOn' => '0000-00-00 00:00:00',
	'SendProcessFinishedOn' => '0000-00-00 00:00:00',
	'TotalRecipients' => 0,
	'TotalSent' => 0,
	'TotalFailed' => 0,
	'TotalOpens' => 0,
	'UniqueOpens' => 0,
	'TotalClicks' => 0,
	'UniqueClicks' => 0,
	'TotalForwards' => 0,
	'UniqueForwards' => 0,
	'TotalViewsOnBrowser' => 0,
	'UniqueViewsOnBrowser' => 0,
	'TotalUnsubscriptions' => 0,
	'TotalHardBounces' => 0,
	'TotalSoftBounces' => 0,
	'BenchmarkEmailsPerSecond' => 0,
	'ApprovalUserExplanation' => '',
	'LastActivityDateTime' => '0000-00-00 00:00:00',
	'CreateDateTime' => date('Y-m-d H:i:s'),
));


// Plug-in hook - Start
Plugins::HookListener('Action', 'Campaign.Copy.Post', array($NewCampaignID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array(
					'Success'			=> true,
					'ErrorCode'			=> 0,
					'NewCampaignID'		=> $NewCampaignID,
					);
// Return results - End
?>