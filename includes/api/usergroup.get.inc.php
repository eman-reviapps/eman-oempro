<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'usergroupid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('user_groups');
// Load other modules - End

// Get lists - Start
$ArrayUserGroup = UserGroups::RetrieveUserGroup($ArrayAPIData['usergroupid']);

if ($ArrayUserGroup == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2,
						);
	throw new Exception('');
	}
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'UserGroup'		=> $ArrayUserGroup
					);
// Return results - End
?>