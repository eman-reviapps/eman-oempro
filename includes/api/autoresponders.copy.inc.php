<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'sourcelistid'		=> 1,
							'targetlistid'		=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayErrorMessages = array(
								"1" => 'Missing source subscriber list id',
								"2" => 'Missing target subscriber list id'
								);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('auto_responders');
Core::LoadObject('lists');
Core::LoadObject('attachments');
// Load other modules - End

// Get auto responders of source list - Start
$ArrayAutoResponders = AutoResponders::RetrieveResponders(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'RelListID'=>$ArrayAPIData['sourcelistid']), array());
// Get auto responders of source list - End

// Get source subscriber list information - Start
$ArraySourceSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['sourcelistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArraySourceSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid source subscriber id')
						);
	throw new Exception('');
	}
// Get source subscriber list information - End

// Get target subscriber list information - Start
$ArrayTargetSubscriberListInformation = Lists::RetrieveList(array('*'), array('ListID'=>$ArrayAPIData['targetlistid'],'RelownerUserID'=>$ArrayUserInformation['UserID']));
if ($ArrayTargetSubscriberListInformation == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(4),
						 'ErrorText'		=> array('Invalid target subscriber id')
						);
	throw new Exception('');
	}
// Get target subscriber list information - End

// Create new emails from each auto responder email - Start
$emailIds = array();
foreach ($ArrayAutoResponders as $Each)
	{
	$emailInformation = Emails::RetrieveEmail(array('*'), array('EmailID' => $Each['RelEmailID']), true);
	unset($emailInformation['EmailID']);
	$emailInformation['EmailName'] .= '[COPY]';
	$attachments = $emailInformation['Attachments'];
	unset($emailInformation['Attachments']);
	$newEmailId = Emails::Create($emailInformation);
	$emailIds[$Each['AutoResponderID']] = $newEmailId;
	if (count($attachments) > 0)
		{
		foreach ($attachments as $eachAttachment)
			{
			Attachments::Copy($newEmailId, $eachAttachment);
			}
		}
	}
// Create new emails from each auto responder email - Start

// Copy each auto responder to target subscriber list - Start
foreach ($ArrayAutoResponders as $Each)
	{
	AutoResponders::Create(array(
		'RelOwnerUserID' => $ArrayTargetSubscriberListInformation['RelOwnerUserID'],
		'RelListID' => $ArrayAPIData['targetlistid'],
		'AutoResponderName' => $Each['AutoResponderName'],
		'RelEmailID' => $emailIds[$Each['AutoResponderID']],
		'AutoResponderTriggerType' => $Each['AutoResponderTriggerType'],
		'AutoResponderTriggerValue' => $Each['AutoResponderTriggerValue'],
		'TriggerTimeType' => $Each['TriggerTimeType'],
		'TriggerTime' => $Each['TriggerTime'],
		));
	}
// Copy each auto responder to target subscriber list - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>