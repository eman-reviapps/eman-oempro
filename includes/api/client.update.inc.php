<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
							'clientid'					=> 6,
							'clientname'				=> 1,
							'clientusername'			=> 2,
							'clientemailaddress'		=> 4,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('clients');
Core::LoadObject('subscribers');
// Load other modules - End

// Field validations - Start
// Validate the account status value- Start
if (($ArrayAPIData['clientaccountstatus'] != '') && (($ArrayAPIData['clientaccountstatus'] != 'Enabled') && ($ArrayAPIData['clientaccountstatus'] != 'Disabled')))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 5,
						 'ErrorText'		=> '',
						);
	throw new Exception('');
	}
// Validate the account status value - End

// Check if username already exists in the system - Start
$TotalFound = Clients::RetrieveClients(array('COUNT(*) AS TotalFound'), array('ClientUsername' => $ArrayAPIData['clientusername'], array('field' => 'ClientID', 'operator' => '!=', 'value' => $ArrayAPIData['clientid'])), array('ClientID'=>'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 9,
						);
	throw new Exception('');
	}
// Check if username already exists in the system - End

// Check if email address already exists in the system - Start
$TotalFound = Clients::RetrieveClients(array('COUNT(*) AS TotalFound'), array('ClientEmailAddress' => $ArrayAPIData['clientemailaddress'], array('field' => 'ClientID', 'operator' => '!=', 'value' => $ArrayAPIData['clientid'])), array('ClientID'=>'ASC'));
$TotalFound = $TotalFound[0]['TotalFound'];

if ($TotalFound > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 10,
						);
	throw new Exception('');
	}
// Check if email address already exists in the system - End

if (isset($ArrayAPIData['access']) && $ArrayAPIData['access'] == 'client')
	{
	// Check if logged in client is the owner of the account - Start
	if ($ArrayClientInformation['ClientID'] != $ArrayAPIData['clientid'])
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 8,
							);
		throw new Exception('');
		}
	// Check if logged in client is the owner of the account - End
	}
else
	{
	// Check if the client account is owned by the user - Start
	if (($ArrayClientInformation = Clients::RetrieveClient(array('*'), array('ClientID' => $ArrayAPIData['clientid'], 'RelOwnerUserID' => $ArrayUserInformation['UserID']))) == false)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 8,
							);
		throw new Exception('');
		}
	// Check if the client account is owned by the user - End
	}
	
// Validate client email address format - Start
if (Subscribers::ValidateEmailAddress($ArrayAPIData['clientemailaddress']) == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 7,
						);
	throw new Exception('');
	}
// Validate client email address format - End
// Field validations - End

// Load other modules - Start
Core::LoadObject('clients');
// Load other modules - End

// Update client account - Start
$ArrayFieldAndValues = array(
							'ClientID'				=> $ArrayClientInformation['ClientID'],
							'ClientUsername'		=> $ArrayAPIData['clientusername'],
							'ClientEmailAddress'	=> $ArrayAPIData['clientemailaddress'],
							'ClientName'			=> $ArrayAPIData['clientname'],
							'ClientAccountStatus'	=> ($ArrayAPIData['clientaccountstatus'] == '' ? $ArrayClientInformation['ClientAccountStatus'] : $ArrayAPIData['clientaccountstatus']),
							'RelOwnerUserID'		=> $ArrayClientInformation['RelOwnerUserID'],
							);
if (isset($ArrayAPIData['clientpassword']) && strlen($ArrayAPIData['clientpassword']) > 0)
	{
	$ArrayFieldAndValues['ClientPassword'] = md5(OEMPRO_PASSWORD_SALT.$ArrayAPIData['clientpassword'].OEMPRO_PASSWORD_SALT);
	}
Clients::Update($ArrayFieldAndValues, array('ClientID' => $ArrayAPIData['clientid']));
// Update client account - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Client.Update.Post', array($ArrayClientInformation['ClientID']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					);
// Return results - End


?>