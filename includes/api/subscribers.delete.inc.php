<?php

// Check for required fields - Start
$ArrayRequiredFields = array(
    'subscribers' => 1,
    'subscriberlistid' => 2
);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0) {
    $ArrayOutput = array('Success' => false,
        'ErrorCode' => $ArrayErrorFields,
        'ErrorText' => array('Subscriber ID numbers are missing'),
    );
    throw new Exception('');
}
// Check for required fields - End
// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End
// Load other modules - Start
Core::LoadObject('subscribers');
// Load other modules - End
// Delete auto responders - Start
if (isset($ArrayAPIData['suppressed']) && $ArrayAPIData['suppressed']) {
    Subscribers::RemoveSuppressedSubscribersByIDFromSuppressionList($ArrayAPIData['subscriberlistid'], explode(',', $ArrayAPIData['subscribers']));
} else {
    Subscribers::RemoveSubscribersByID($ArrayAPIData['subscriberlistid'], explode(',', $ArrayAPIData['subscribers']));
}
// Delete auto responders - End
// Return results - Start
$ArrayOutput = array('Success' => true,
    'ErrorCode' => 0,
    'ErrorText' => ''
);
// Return results - End
?>