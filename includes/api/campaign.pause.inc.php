<?php
// Load other modules - Start
Core::LoadObject('api');
Core::LoadObject('campaigns');
Core::LoadObject('split_tests');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'campaignid'		=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// Be sure that campaign belongs to the logged in user - Start
$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('RelOwnerUserID' => $ArrayUserInformation['UserID'], 'CampaignID' => $ArrayAPIData['campaignid']));

if ($ArrayCampaign == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Be sure that campaign belongs to the logged in user - End

// Verify campaign status - Start
if (isset($ArrayCampaign['CampaignStatus']) && ($ArrayCampaign['CampaignStatus'] != 'Sending'))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
// Verify campaign status - End

// Field validations - End

// Pause campaign - Start
$ArrayAPIVars = array(
	'campaignid'				=>	$ArrayCampaign['CampaignID'],
	'campaignstatus'			=>	'Paused'
	);
$ArrayReturn = API::call(array(
	'format'	=>	'array',
	'command'	=>	'campaign.update',
	'protected'	=>	true,
	'username'	=>	$ArrayUserInformation['Username'],
	'password'	=>	$ArrayUserInformation['Password'],
	'parameters'=>	$ArrayAPIVars
	));

$SplitTestSettings = SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayUserInformation['UserID']);
if ($SplitTestSettings != false)
	{
	SplitTests::Update($SplitTestSettings[0]['TestID'], array('SplitTestingStatus' => 'Paused By User'));
	}
// Pause campaign - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0
					);
// Return results - End


?>