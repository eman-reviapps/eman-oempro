<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'autoresponders'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> array('Auto responder ids are missing'),
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('auto_responders');
// Load other modules - End

// Delete auto responders - Start
AutoResponders::Delete($ArrayUserInformation['UserID'], explode(',', $ArrayAPIData['autoresponders']));
// Delete auto responders - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Autoresponder.Delete.Post', explode(',', $ArrayAPIData['autoresponders']));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> ''
					);
// Return results - End
?>