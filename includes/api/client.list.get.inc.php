<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'listid'				=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('lists');
Core::LoadObject('clients');
// Load other modules - End

// Get assigned subscriber lists of client - Start
$AssignedSubscriberLists = Clients::RetrieveAssignedSubscriberLists($ArrayClientInformation['ClientID']);
// Get assigned subscriber lists of client - End

// Check if given list id is assigned to client - Start
$IsMatch = false;
foreach ($AssignedSubscriberLists as $Each)
	{
	if ($Each['SubscriberListID'] == $ArrayAPIData['listid'])
		{
		$IsMatch = true;
		}
	}
// Check if given list id is assigned to client - End

// If no match, give error - Start
if ($IsMatch == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(2),
						);
	throw new Exception('');
	}
// If no match, give error - End

// Get lists - Start
$ArraySubscriberList = Lists::RetrieveList(array('*'), array('RelOwnerUserID'=>$ArrayClientInformation['RelOwnerUserID'],'ListID'=>$ArrayAPIData['listid']), true);
// Get lists - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'List'				=> $ArraySubscriberList
					);
// Return results - End
?>