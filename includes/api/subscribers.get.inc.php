<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							'subscribersegment'		=> 2,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
// Load other modules - End

// Retrieve list and check if it belongs to authenticated user - Start
$ArrayListInformation = Lists::RetrieveList(array('*'), array('ListID' => $ArrayAPIData['subscriberlistid']), false, false, array());
if ($ArrayListInformation != false && $ArrayListInformation['RelOwnerUserID'] != $ArrayUserInformation['UserID'])
	{
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> 3
	);
	throw new Exception('');
	}
// Retrieve list and check if it belongs to authenticated user - End

if (!isset($ArrayAPIData['recordsperrequest']) || $ArrayAPIData['recordsperrequest'] == '')
	{
	$ArrayAPIData['recordsperrequest'] = 25;
	}
if (!isset($ArrayAPIData['recordsfrom']) || $ArrayAPIData['recordsfrom'] == '')
	{
	$ArrayAPIData['recordsfrom'] = 0;
	}

// Search subscribers - Start
if (isset($ArrayAPIData['searchfield']) && isset($ArrayAPIData['searchkeyword']) && $ArrayAPIData['searchkeyword'] != '' && $ArrayAPIData['searchfield'] != '')
	{
	$ArraySubscribers =	Subscribers::Search($ArrayAPIData['searchkeyword'], $ArrayAPIData['searchfield'], $ArrayAPIData['subscriberlistid'], $ArrayAPIData['subscribersegment'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
	$SubscriberTotal =	Subscribers::Search($ArrayAPIData['searchkeyword'], $ArrayAPIData['searchfield'], $ArrayAPIData['subscriberlistid'], $ArrayAPIData['subscribersegment'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
	}
// Search subscribers - End

// Retrieve subscribers - Start
else
	{
	// Retrieve segment subscribers - Start
	if (is_numeric($ArrayAPIData['subscribersegment']))
		{
		// $ArraySubscribers = Subscribers::RetrieveSegmentSubscribers(array('*'), array(), $ArrayAPIData['subscriberlistid'], $ArrayAPIData['subscribersegment'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$ArraySubscribers = Subscribers::RetrieveSegmentSubscribers_Enhanced(array(
			'ReturnFields'		=>	array('*'),
			'ReturnSQLQuery'	=>	false,
			'SubscriberListID'	=>	$ArrayAPIData['subscriberlistid'],
			'SegmentID'			=>	$ArrayAPIData['subscribersegment'],
			'RowOrder'			=>	array('Column'=>$ArrayAPIData['orderfield'],'Type'=>$ArrayAPIData['ordertype']),
			'Pagination'		=>	array('Offset'=>$ArrayAPIData['recordsfrom'],'Rows'=>$ArrayAPIData['recordsperrequest']),
			'Criteria'			=>	array()
			));
		$SubscriberTotal = Subscribers::GetSegmentTotal($ArrayAPIData['subscriberlistid'], $ArrayAPIData['subscribersegment']);
		}
	// Retrieve segment subscribers - End

	// Retrieve suppressed subscribers - Start
	elseif ($ArrayAPIData['subscribersegment'] == 'Suppressed')
		{
		$ArraySubscribers = Subscribers::RetrieveSuppressedSubscribers(array('*'), array(), $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, $ArrayUserInformation['UserID']);
		$SubscriberTotal = Subscribers::GetSuppressedTotal($ArrayAPIData['subscriberlistid'], $ArrayUserInformation['UserID']);
		}
	// Retrieve suppressed subscribers - End
	
	// Retrieve unsubscribed subscribers - Start
	elseif ($ArrayAPIData['subscribersegment'] == 'Unsubscribed')
		{
		$ArraySubscribers = Subscribers::RetrieveSubscribers(array('*'), array('SubscriptionStatus'=>'Unsubscribed'), $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$SubscriberTotal = Subscribers::RetrieveSubscribers(array('*'), array('SubscriptionStatus'=>'Unsubscribed'), $ArrayAPIData['subscriberlistid'], array(), 0, 0, false, true);
		}
	// Retrieve unsubscribed subscribers - End
	
	// Retrieve soft bounced subscribers - Start
	elseif ($ArrayAPIData['subscribersegment'] == 'Soft bounced')
		{
		$ArraySubscribers = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Soft'), $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$SubscriberTotal = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Soft'), $ArrayAPIData['subscriberlistid'], array(), 0, 0, false, true);
		}
	// Retrieve soft bounced subscribers - End
	
	// Retrieve hard bounced subscribers - Start
	elseif ($ArrayAPIData['subscribersegment'] == 'Hard bounced')
		{
		$ArraySubscribers = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Hard'), $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$SubscriberTotal = Subscribers::RetrieveSubscribers(array('*'), array('BounceType'=>'Hard'), $ArrayAPIData['subscriberlistid'], array(), 0, 0, false, true);
		}
	// Retrieve hard bounced subscribers - End
	
	// Retrieve opt-in pending subscribers - Start
	elseif ($ArrayAPIData['subscribersegment'] == 'Opt-in pending')
		{
		$ArraySubscribers = Subscribers::RetrieveSubscribers(array('*'), array('SubscriptionStatus'=>'Opt-In Pending'), $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$SubscriberTotal = Subscribers::RetrieveSubscribers(array('*'), array('SubscriptionStatus'=>'Opt-In Pending'), $ArrayAPIData['subscriberlistid'], array(), 0, 0, false, true);
		}
	// Retrieve opt-in pending subscribers - End
	
	// Retrieve active subscribers - Start
	else
		{
		$ArraySubscribers = Subscribers::RetrieveSubscribers(array('*'), Subscribers::$ArrayActiveSubscriberCriterias, $ArrayAPIData['subscriberlistid'], array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
		$SubscriberTotal = Subscribers::GetActiveTotal($ArrayAPIData['subscriberlistid']);
		}
	// Retrieve active subscribers - End
	}
// Retrieve subscribers - Start

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'Subscribers'		=> $ArraySubscribers,
					 'TotalSubscribers'		=> $SubscriberTotal
					);
// Return results - End
?>