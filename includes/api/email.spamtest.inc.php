<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('campaigns');
Core::LoadObject('statistics');
Core::LoadObject('queue');
Core::LoadObject('attachments');
Core::LoadObject('personalization');
Core::LoadObject('queue');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Retrieve email information - End
// Field validations - End

// Send preview email - Start
$EmailContent = Emails::SendPreviewEmail('SaveToDisk', 'dummy@dummy.com', $ArrayUserInformation, array(), array(), array(), $ArrayEmail, true, false, true);
// Send preview email - End

// Pass the email content through SpamAssassin filter - Start
if (SPAM_FILTER_METHOD == 'Octeth')
	{
	$ArrayPostParameters = array(
								'FormValue_LicenseKey=',
								'FormValue_Host='.rawurlencode($_SERVER['HTTP_HOST']),
								'FormValue_NewsletterContent='.rawurlencode($EmailContent),
								);
	$ArrayReturn = Core::DataPostToRemoteURL(SPAM_FILTER_URL, $ArrayPostParameters, 'POST', false, '', '', 60, false);

	$Return = $ArrayReturn[1];
	unset($ArrayReturn);

	// Parse the returned results - Start
	if (preg_match('/OCT2\|\|\|/i', $Return) == true)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> 3
							);
		throw new Exception('');
		}
	else
		{
		$ArrayScores = Emails::ParseRawSPAMRating($Return, true);
		}
	// Parse the returned results - End
	}
elseif (SPAM_FILTER_METHOD == 'Local')
	{
	// Perform spam filter check through spamasassin - Start
	$Return = @shell_exec('echo '.escapeshellarg($EmailContent).' | spamassassin -Lt');
	// Perform spam filter check through spamasassin - End

	// Parse the returned results - Start
	$ArraySpamTestResult	= array();
	
	preg_match_all('/X-Spam-Status: (.*)tests/i', $Return, $ArrayMatches);
	$ArraySpamTestResult['SpamStatus']	= $ArrayMatches[0][0];

	preg_match_all('/Content analysis details(.*)$/is', $Return, $ArrayMatches);
	$ArraySpamTestResult['Scores']		= $ArrayMatches[0][0];

	$ArrayScores = Emails::ParseRawSPAMRating($ArraySpamTestResult['Scores'], false);
	// Parse the returned results - End
	}
// Pass the email content through SpamAssassin filter - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'TestResults'			=> $ArrayScores,
					);
// Return results - End
?>