<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'mediaid'					=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('media_library');
// Load other modules - End

$fileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
$file = $fileMapper->findById($ArrayAPIData['mediaid']);

if ($file === false || (AdminAuth::IsLoggedIn(false,false) == false && $file->getUserId() != $ArrayUserInformation['UserID'])) {
	$ArrayOutput = array(
		'Success'	=> false,
		'ErrorCode'	=> 2
	);
	throw new Exception('');
}

if (MEDIA_UPLOAD_METHOD == 'file') {
	unlink(DATA_PATH.'media/'.md5($file->getId()));
} elseif (MEDIA_UPLOAD_METHOD == 's3') {
	Core::LoadObject('aws_s3');
	$ObjectS3 = new S3(S3_ACCESS_ID, S3_SECRET_KEY, false);
	$ObjectS3->deleteObject(S3_BUCKET, S3_MEDIALIBRARY_PATH.'/'.md5($ArrayUserInformation['UserID']).'/'.md5($file->getId()));
}

$fileMapper->deleteById($file->getId());

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					);
// Return results - End
?>