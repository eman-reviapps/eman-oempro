<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'emailid'				=> 1,
							);

$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('campaigns');
Core::LoadObject('statistics');
Core::LoadObject('queue');
Core::LoadObject('attachments');
Core::LoadObject('personalization');
Core::LoadObject('queue');
Core::LoadObject('payments');
// Load other modules - End

// Field validations - Start
// Retrieve email information - Start
$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayAPIData['emailid']));
if ($ArrayEmail == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Retrieve email information - End
// Field validations - End

// Generate the email content - Start
$ArrayEmailProperties = Emails::SendPreviewEmail('SaveToDisk', 'dummy@dummy.com', $ArrayUserInformation, array(), array(), array(), $ArrayEmail, false, true);
// Generate the email content - End

// Send the design preview request to PreviewMyEmail.com API - Start
$ArrayPostParameters = array(
							'apikey='.PME_APIKEY,
							);
$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'SystemStatus/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
if ($ArrayReturn[0] == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
$ClientXML = simplexml_load_string($ArrayReturn[1]);

$ArrayPostParameters = array(
							'apikey='.PME_APIKEY,
							'emailsubject='.base64_encode($ArrayEmailProperties[0]),
							'emailbody='.($ArrayEmailProperties[1] != '' ? urlencode(base64_encode($ArrayEmailProperties[1])) : urlencode(base64_encode($ArrayEmailProperties[2]))),
							);
foreach ($ClientXML->email_client as $eachEmailClient) {
	$ArrayPostParameters[] = 'targetemailapps[]='.$eachEmailClient->client_id;
}
$ArrayReturn = Core::DataPostToRemoteURL(PME_API_URL.'CreatePreview/xml', $ArrayPostParameters, 'POST', false, '', '', 60, false);
// Send the design preview request to PreviewMyEmail.com API - End

// Parse the returned data - Start
if ($ArrayReturn[0] == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 3
						);
	throw new Exception('');
	}
else
	{
	$ObjectXML = simplexml_load_string($ArrayReturn[1]);
	if ((string)$ObjectXML->result == 'true')
		{
		$PreviewMyEmailJobID = (string)$ObjectXML->jobid;
		}
	else
		{
		// Out of credits
		if (PME_USAGE_TYPE == 'Admin')
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> 4
								);
			throw new Exception('');
			}
		else
			{
			$ArrayOutput = array('Success'			=> false,
								 'ErrorCode'		=> 5
								);
			throw new Exception('');
			}
		}
	}
// Parse the returned data - End

// Create the job record in the database - Start
$ArrayFieldAndValues = array(
							'JobID'						=> '',
							'RelOwnerUserID'			=> $ArrayUserInformation['UserID'],
							'RelEmailID'				=> $ArrayEmail['EmailID'],
							'PreviewMyEmailJobID'		=> $PreviewMyEmailJobID,
							'Status'					=> 'Pending',
							'SubmitDate'				=> date('Y-m-d H:i:s'),
							'LastProcessDate'			=> '0000-00-00 00:00:00',
							'CompleteDate'				=> '0000-00-00 00:00:00',
							);
$NewJobID = Emails::CreateDesignPreviewJob($ArrayFieldAndValues);
// Create the job record in the database - End

// Email sending is processed. Track the payment activity - Start
Payments::SetUser($ArrayUserInformation);
Payments::CheckIfPaymentPeriodExists();
Payments::DesignPreviewRequest($NewJobID);
// Email sending is processed. Track the payment activity - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'Email.DesignPreview.Create.Post', array($NewJobID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'				=> true,
					 'ErrorCode'			=> 0,
					 'JobID'				=> $NewJobID,
					 'PreviewMyEmailJobID'	=> $PreviewMyEmailJobID,
					);
// Return results - End
?>