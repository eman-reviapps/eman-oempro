<?php
// Check for required fields - Start
// There's no required fields
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('users');
// Load other modules - End

if (!isset($ArrayAPIData['recordsperrequest']) || $ArrayAPIData['recordsperrequest'] === '')
	{
	$ArrayAPIData['recordsperrequest'] = 25;
	}
if (!isset($ArrayAPIData['recordsfrom']) || $ArrayAPIData['recordsfrom'] === '')
	{
	$ArrayAPIData['recordsfrom'] = 0;
	}

// Prepare browse criteria - Start
$BrowseCriteria = '';
if (isset($ArrayAPIData['relusergroupid']) && $ArrayAPIData['relusergroupid'] != '' && $ArrayAPIData['relusergroupid'] != 0)
	{
	$BrowseCriteria =  'Specific user group';
	}
elseif ((isset($ArrayAPIData['relusergroupid'])) && ($ArrayAPIData['relusergroupid'] === 'Online'))
	{
	$BrowseCriteria =  'Online users';
	}
elseif ((isset($ArrayAPIData['relusergroupid'])) && (($ArrayAPIData['relusergroupid'] === 'Enabled') || ($ArrayAPIData['relusergroupid'] === 'Disabled')))
	{
	$BrowseCriteria =  'Specific account status';
	}
elseif ((isset($ArrayAPIData['relusergroupid'])) && (($ArrayAPIData['relusergroupid'] === 'Trusted') || ($ArrayAPIData['relusergroupid'] === 'Untrusted')))
	{
	$BrowseCriteria =  'Specific account reputation';
	}
// Prepare browse criteria - End

// Set result ordering - Start {
$ArrayOrderBy = array();
if (strpos($ArrayAPIData['orderfield'], '|') === false)
	{
	$ArrayOrderBy = array(
						$ArrayAPIData['orderfield']		=>	$ArrayAPIData['ordertype'],
						);
	}
else
	{
	$TMPArray	= explode('|', $ArrayAPIData['orderfield']);
	$TMPArray2	= explode('|', $ArrayAPIData['ordertype']);
	
	$TMPCounter = 0;
	foreach ($TMPArray as $EachField)
		{
		$ArrayOrderBy[$EachField] = (isset($TMPArray2[$TMPCounter]) == true ? $TMPArray2[$TMPCounter] : $TMPArray2[0]);
		$TMPCounter++;
		}
	unset($TMPCounter, $TMPArray2, $TMPArray);
	}
// Set result ordering - End }

// Get subscribers - Start
if (isset($ArrayAPIData['searchfield']) && isset($ArrayAPIData['searchkeyword']) && $ArrayAPIData['searchkeyword'] != '' && $ArrayAPIData['searchfield'] != '')
	{
	// Search users
	$RelUserGroupID = (isset($ArrayAPIData['relusergroupid']) && $ArrayAPIData['relusergroupid'] != '' ? $ArrayAPIData['relusergroupid'] : 0);

	// $ArrayUsers = Users::Search($ArrayAPIData['searchkeyword'], $ArrayAPIData['searchfield'], $ArrayAPIData['subscriberlistid'], $SegmentID, $IsSuppressed, array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']), $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom']);
	// $UsersTotal = Users::GetFoundTotal($ArrayAPIData['searchkeyword'], $ArrayAPIData['searchfield'], $ArrayAPIData['subscriberlistid'], $SegmentID, $IsSuppressed);

	$ArrayCriterias = array(
						array(
							"field"		=> $ArrayAPIData['searchfield'],
							"operator"	=> "LIKE",
							"value"		=> "%".$ArrayAPIData['searchkeyword']."%"
							)
						);
	
	if ($BrowseCriteria == 'Sepcific user group')
		{
		// Retrieve users of a specific group ID
		$ArrayCriterias['RelUserGroupID'] = $RelUserGroupID;
		}
	elseif ($BrowseCriteria == 'Online users')
		{
		// Retrieve online users
			$OnlineUserThreshold = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' -600 seconds '.date('H:i:s')));

		$ArrayCriterias[] = array('field' => 'LastActivityDateTime', 'operator' => '>=', 'value' => $OnlineUserThreshold);
		}
	elseif ($BrowseCriteria == 'Specific account status')
		{
		// Retrieve users with account status Enabled or Disabled
		$ArrayCriterias['AccountStatus'] = $RelUserGroupID;
		}
	elseif ($BrowseCriteria == 'Specific account reputation')
		{
		// Retrieve users with reputation level Trusted or Untrusted
		$ArrayCriterias['ReputationLevel'] = $RelUserGroupID;
		}

	$ArrayUsers = Users::RetrieveUsers(array('*'), $ArrayCriterias, $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
	$UserTotal	= Users::RetrieveUsers(array('*'), $ArrayCriterias, $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
	}
else
	{
	if ($BrowseCriteria == 'Specific user group')
		{
		// Retrieve users of a specific group ID
		$ArrayUsers = Users::RetrieveUsers(array('*'), array('RelUserGroupID' => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
		$UserTotal	= Users::RetrieveUsers(array('*'), array('RelUserGroupID' => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
		}
	elseif ($BrowseCriteria == 'Online users')
		{
		// Retrieve online users
			$OnlineUserThreshold = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' -600 seconds '.date('H:i:s')));

		$ArrayUsers = Users::RetrieveUsers(array('*'), array(array('field' => 'LastActivityDateTime', 'operator' => '>=', 'value' => $OnlineUserThreshold)), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
		$UserTotal	= Users::RetrieveUsers(array('*'), array(array('field' => 'LastActivityDateTime', 'operator' => '>=', 'value' => $OnlineUserThreshold)), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
		}
	elseif ($BrowseCriteria == 'Specific account status')
		{
		// Retrieve users with account status Enabled or Disabled
		$ArrayUsers = Users::RetrieveUsers(array('*'), array("AccountStatus" => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
		$UserTotal	= Users::RetrieveUsers(array('*'), array("AccountStatus" => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
		}
	elseif ($BrowseCriteria == 'Specific account reputation')
		{
		// Retrieve users with reputation level Trusted or Untrusted
		$ArrayUsers = Users::RetrieveUsers(array('*'), array("ReputationLevel" => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
		$UserTotal	= Users::RetrieveUsers(array('*'), array("ReputationLevel" => $ArrayAPIData['relusergroupid']), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
		}
	else
		{
		// Retrieve all users
		$ArrayUsers = Users::RetrieveUsers(array('*'), array(), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, false);
		$UserTotal	= Users::RetrieveUsers(array('*'), array(), $ArrayOrderBy, $ArrayAPIData['recordsperrequest'], $ArrayAPIData['recordsfrom'], false, true);
		}
	}

// Get subscribers - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'Users'			=> $ArrayUsers,
					 'TotalUsers'		=> $UserTotal
					);
// Return results - End
?>