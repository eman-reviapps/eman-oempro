<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'subscriberlistid'		=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'Missing subscriber list id',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Load other modules - Start
Core::LoadObject('auto_responders');
// Load other modules - End

// Get auto responders - Start
$ArrayAutoResponders = AutoResponders::RetrieveResponders(array('*'), array('RelOwnerUserID'=>$ArrayUserInformation['UserID'],'RelListID'=>$ArrayAPIData['subscriberlistid']), array($ArrayAPIData['orderfield']=>$ArrayAPIData['ordertype']));
// Get auto responders - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'TotalResponderCount'	=> AutoResponders::GetTotal($ArrayAPIData['subscriberlistid']),
					 'AutoResponders'	=> $ArrayAutoResponders
					);
// Return results - End
?>