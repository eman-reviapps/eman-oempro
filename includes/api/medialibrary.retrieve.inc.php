<?php
// Check for required fields - Start
$ArrayRequiredFields = array(
							'mediaid'					=> 1,
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields
						);
	throw new Exception('');
	}
// Check for required fields - End

// Load other modules - Start
Core::LoadObject('media_library');
// Load other modules - End

// Retrieve media library item - Start
$fileMapper = O_Registry::instance()->getMapper('MediaLibraryFile');
$file = $fileMapper->findById($ArrayAPIData['mediaid']);
// Retrieve media library item - End

// Return false if media file not found - Start
if ($file == false)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> 2
						);
	throw new Exception('');
	}
// Return false if media file not found - End

// Return results - Start
$ArrayOutput = array('Success'		=> true,
					 'ErrorCode'	=> 0,
					 'Name'			=> $file->getName(),
					 'Type'			=> $file->getType(),
					 'Size'			=> $file->getSize(),
					 'URL'			=> $file->getUrl()
					);
// Return results - End
?>