<?php
// Load other modules - Start
Core::LoadObject('custom_fields');
// Load other modules - End

// Check for required fields - Start
global $ArrayCustomFieldPresets;
if (isset($ArrayAPIData['presetname']) && !array_key_exists($ArrayAPIData['presetname'], $ArrayCustomFieldPresets))
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> array(5)
						);
	throw new Exception('');
	}

if (isset($ArrayAPIData['presetname']))
	{
	$ArrayRequiredFields = array(
								'subscriberlistid'		=> 1
								);
	}
else
	{
	$ArrayRequiredFields = array(
								'subscriberlistid'		=> 1,
								'fieldname'				=> 2,
								'fieldtype'				=> 3
								);
	}
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
		$ArrayErrorMessages = array(
									"1" => 'Missing subscriber list id',
									"2" => 'Missing field name',
									"3" => 'Missing field type'
									);
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> Core::GetAPIErrorTextsAsArray($ArrayErrorFields, $ArrayErrorMessages)
						);
	throw new Exception('');
	}

if ($ArrayAPIData['fieldtype'] == 'Date field')
	{
	$ArrayRequiredFields = array(
								'years'			=> 5
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);
	}

if ($ArrayAPIData['validationmethod'] == 'Custom')
	{
	$ArrayRequiredFields = array(
								'validationrule'		=> 4
								);
	$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

	if (count($ArrayErrorFields) > 0)
		{
		$ArrayOutput = array('Success'			=> false,
							 'ErrorCode'		=> $ArrayErrorFields,
							 'ErrorText'		=> array('Missing validation rule')
							);
		throw new Exception('');
		}
	}
// Check for required fields - End

// Field validations - Start
// There's no field valdiation for this functionality
// Field validations - End

// Create custom field - Start
$ArrayNewCustomFieldInformation = array();
$ArrayNewCustomFieldInformation['RelListID'] = $ArrayAPIData['subscriberlistid'];
$ArrayNewCustomFieldInformation['RelOwnerUserID'] = $ArrayUserInformation['UserID'];
if (isset($ArrayAPIData['presetname']))
	{
	$ArrayNewCustomFieldInformation['FieldName'] = $ArrayAPIData['presetname'];
	$ArrayNewCustomFieldInformation['FieldType'] = 'Drop down';
	}
else
	{
	$ArrayNewCustomFieldInformation['FieldName'] = str_replace(array("\n", "\r"), array('', ''), trim($ArrayAPIData['fieldname']));
	$ArrayNewCustomFieldInformation['FieldType'] = $ArrayAPIData['fieldtype'];
	}
$ArrayNewCustomFieldInformation['FieldDefaultValue'] = $ArrayAPIData['defaultvalue'];
$ArrayNewCustomFieldInformation['ValidationMethod'] = $ArrayAPIData['validationmethod'];
$ArrayNewCustomFieldInformation['ValidationRule'] = $ArrayAPIData['validationrule'];

	// Prepare field options as string - Start
	$ArrayNewCustomFieldInformation['FieldOptions'] = '';
	if ($ArrayNewCustomFieldInformation['FieldType'] == 'Multiple choice' || $ArrayNewCustomFieldInformation['FieldType'] == 'Drop down' || $ArrayNewCustomFieldInformation['FieldType'] == 'Checkboxes')
		{
		if (isset($ArrayAPIData['presetname']))
			{
			foreach ($ArrayCustomFieldPresets[$ArrayAPIData['presetname']] as $Value)
				{
				$IsSelected = false;
				if ($ArrayAPIData['presetname'] == 'Country')
					{
					$ArrayNewCustomFieldInformation['FieldOptions'] .= CustomFields::GetOptionAsString($Value[1], $Value[0], $IsSelected);
					$ArrayNewCustomFieldInformation['FieldOptions'] .= ',,,';
					}
				else
					{
					$ArrayNewCustomFieldInformation['FieldOptions'] .= CustomFields::GetOptionAsString($Value, $Value, $IsSelected);
					$ArrayNewCustomFieldInformation['FieldOptions'] .= ',,,';
					}
				}
			}
		else
			{
			foreach ($ArrayAPIData['optionlabel'] as $Key=>$Value)
				{
				$IsSelected = (in_array($Key, $ArrayAPIData['optionselected']) ? true : false );
				$ArrayNewCustomFieldInformation['FieldOptions'] .= CustomFields::GetOptionAsString($Value, $ArrayAPIData['optionvalue'][$Key], $IsSelected);
				$ArrayNewCustomFieldInformation['FieldOptions'] .= ',,,';
				}
			}
		$ArrayNewCustomFieldInformation['FieldOptions'] = substr($ArrayNewCustomFieldInformation['FieldOptions'], 0, $ArrayNewCustomFieldInformation['FieldOptions'].length - 3);
		}
	// Prepare field options as string - End

$ArrayNewCustomFieldInformation['IsRequired'] = ($ArrayAPIData['isrequired'] == 'Yes' ? 'Yes' : 'No');
$ArrayNewCustomFieldInformation['IsUnique'] = ($ArrayAPIData['isunique'] == 'Yes' ? 'Yes' : 'No');
$ArrayNewCustomFieldInformation['Visibility'] = ($ArrayAPIData['visibility'] == 'Public' ? 'Public' : 'User Only');
$ArrayNewCustomFieldInformation['IsGlobal'] = ($ArrayAPIData['isglobal'] == 'Yes' ? 'Yes' : 'No');
if ($ArrayNewCustomFieldInformation['FieldType'] == 'Date field')
	{
	$ArrayNewCustomFieldInformation['Option1'] = (isset($ArrayAPIData['years']) && $ArrayAPIData['years'] != '' ? $ArrayAPIData['years'] : (Date('Y') - 10).'-'.(Date('Y') + 10));
	}
$NewCustomFieldID = CustomFields::Create($ArrayNewCustomFieldInformation);
// Create custom field - End

// Plug-in hook - Start
Plugins::HookListener('Action', 'CustomField.Create.Post', array($NewCustomFieldID));
// Plug-in hook - End

// Return results - Start
$ArrayOutput = array('Success'			=> true,
					 'ErrorCode'		=> 0,
					 'ErrorText'		=> '',
					 'CustomFieldID'	=> $NewCustomFieldID
					);
// Return results - End
?>