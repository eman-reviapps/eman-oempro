<?php
// Load other modules - Start
Core::LoadObject('web_service_integration');
// Load other modules - End

// Check for required fields - Start
$ArrayRequiredFields = array(
							'url'		=> 1
							);
$ArrayErrorFields = FormHandler::RequiredFieldValidator($ArrayAPIData, $ArrayRequiredFields);

if (count($ArrayErrorFields) > 0)
	{
	$ArrayOutput = array('Success'			=> false,
						 'ErrorCode'		=> $ArrayErrorFields,
						 'ErrorText'		=> 'URL is missing',
						);
	throw new Exception('');
	}
// Check for required fields - End

// Test URL - Start
$ArrayResult = WebServiceIntegration::TestURL($ArrayAPIData['url']);
// Test URL - End

// Return results - Start
$ArrayOutput = array('Success'			=> $ArrayResult[0],
					 'ErrorCode'		=> $ArrayResult[1],
					 'ErrorText'		=> ''
					);
// Return results - End
?>