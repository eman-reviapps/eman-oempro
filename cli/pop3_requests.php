<?php
/**
 * IMPORTANT:
 * If this CRON script is executed in CLI mode, do not forget that PHP will try
 * to connect your MySQL server through /var/mysql/mysql.sock socket. The path
 * of socket file can not be changed. If mysql.sock is in different location
 * on your server, simply create a symbolic link on /var/mysql/mysql.sock
 *
 * This is a limitation of PHP and there's nothing else to do.
 */

include_once 'init.php';

// Include main module - Start
include_once $DATA_PATH . 'config.inc.php';
// Include main module - End

// Disable web cron module - Start {
if (DISABLE_WEB_CRONS == true)
	{
	print('Disabled.');
	exit;
	}
// Disable web cron module - End }

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('users');
Core::LoadObject('requests');
Core::LoadObject('mime_parser');
Core::LoadObject('rfc822_addresses');
Core::LoadObject('pop3_engine');
// Load other modules - End

if (DEMO_MODE_ENABLED == true)
	{
	print('NOT AVAILABLE IN DEMO MODE.');
	exit;
	}

// Log the process - Start {
$ProcessCode	= 'cron_pop3_requests';
$ProcessLogID	= Core::RegisterToProcessLog($ProcessCode);
// Log the process - End }

// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Request By Email Detection - Start
try
	{
	// Plug-in hook - Start
	Plugins::HookListener('Action', 'Cron.Requests', array());
	// Plug-in hook - End

	include_once(APP_PATH.'/includes/cli/include_pop3_requests.inc.php');

	$ShellMessage = 'Subscriber request emails are received and processed ('.number_format($TotalProcessedRequests).' request emails processed out of '.number_format($TotalLooped).' messages)';
	}
catch (Exception $e)
	{
	if ($e->GetMessage() != '')
		{
		$ShellMessage = $e->GetMessage();
		}
	}
// Request By Email Detection - End

// Log the process - Start {
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);
// Log the process - End }

// Display success message - Start
print($ShellMessage);
// Display success message - End
?>