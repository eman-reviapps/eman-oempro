<?php

/**
 * IMPORTANT:
 * If this CRON script is executed in CLI mode, do not forget that PHP will try
 * to connect your MySQL server through /var/mysql/mysql.sock socket. The path
 * of socket file can not be changed. If mysql.sock is in different location
 * on your server, simply create a symbolic link on /var/mysql/mysql.sock
 *
 * This is a limitation of PHP and there's nothing else to do.
 */
include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

// Disable web cron module - Start {
if (DISABLE_WEB_CRONS == true) {
    print('Disabled.');
    exit;
}
// Disable web cron module - End }

$IsWEBCRON = true;

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('users');
Core::LoadObject('campaigns');
Core::LoadObject('queue');
Core::LoadObject('segments');
Core::LoadObject('personalization');
Core::LoadObject('attachments');
Core::LoadObject('payments');
Core::LoadObject('split_tests');
Core::LoadObject('benchmark');
Core::LoadObject('user_balance');
// Load other modules - End

if (DEMO_MODE_ENABLED == true) {
    print('NOT AVAILABLE IN DEMO MODE.');
    exit;
}

// Log the process - Start {
$ProcessCode = 'cron_web_sendengine';
$ProcessLogID = Core::RegisterToProcessLog($ProcessCode);
// Log the process - End }
// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');
// Load language - End
// Send Engine - Start
try {
    // Plug-in hook - Start
    Plugins::HookListener('Action', 'Cron.SendEngine', array());
    // Plug-in hook - End

    include_once(APP_PATH . '/includes/cli/include_web_send.inc.php');

    $ShellMessage = sprintf('%s campaigns processed and sent.', number_format($TotalProcessedCampaigns));
} catch (Exception $e) {
    if ($e->GetMessage() != '') {
        $ShellMessage = $e->GetMessage();
    }
}
// Send Engine - End
// Log the process - Start {
Core::RegisterToProcessLog($ProcessCode, 'Completed', $ShellMessage, $ProcessLogID);
// Log the process - End }
// Display success message - Start
print($ShellMessage);
// Display success message - End
?>