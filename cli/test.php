<?php
include_once '../../cli/init.php';
include_once $DATA_PATH . 'config.inc.php';
include_once(PLUGIN_PATH . 'oct_register/libraries/helpers/Validation.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/helpers/Curl.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/helpers/Output.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/Oempro.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/api/OemproAPI.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/api/AdminAPI.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/api/UsersAPI.php');
include_once(PLUGIN_PATH . 'oct_register/libraries/api/PaymentAPI.php');

/**
 *
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
// Send queued campaign emails
$TotalProcessedCampaigns = 0;

$ArrayPendingCampaigns = Campaigns::GetPendingCampaigns();

// Generate the queue for each campaign - Start
$TotalProcessedCampaigns = 0;

foreach ($ArrayPendingCampaigns as $Key => $EachCampaignID) {
    // Do not allow to process more than 1 campaign per execution - Start
    if ($TotalProcessedCampaigns > 0) {
        break;
    }
    // Do not allow to process more than 1 campaign per execution - End
    // Retrieve campaign information - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    if ($ArrayCampaign['CampaignStatus'] != 'Ready') {
        // If is being processed or processed already - Start
        continue;
        // If is being processed or processed already - End
    }
    // Retrieve campaign information - End
    // Check if the campaign is an a/b split testing campaign - Start {
    $IsAorBSplitTestCampaign = false;
    if (($ArrayCampaign['RelEmailID'] == 0) && (SplitTests::RetrieveTestOfACampaign($ArrayCampaign['CampaignID'], $ArrayCampaign['RelOwnerUserID']) != false)) {
        $IsAorBSplitTestCampaign = true;
    }
    // Check if the campaign is an a/b split testing campaign - End }
    // If email ID is 0, do not proceed with this campaign - Start
    if (($ArrayCampaign['RelEmailID'] == 0) && ($IsAorBSplitTestCampaign == false)) {
        // If is being processed or processed already - Start
        continue;
        // If is being processed or processed already - End
    }
    // If email ID is 0, do not proceed with this campaign - End
    // Retrieve user information - Start
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => $ArrayCampaign['RelOwnerUserID']));

    if ($ArrayUser == false) {
        continue;
    }
    // Retrieve user information - End
    // Check if user account status is enabled. If not, ignore this campaign - Start
    if ($ArrayUser['AccountStatus'] != 'Enabled') {
        continue;
    }

    //added by Eman - check if is this a renew for package or current period is not finished yet
    $LogExists = Payments::GetCurrentPaymentPeriodIfExists();

    // Check if user account status is enabled. If not, ignore this campaign - End
    // Check if payment period for this user exists for the current period - Start
    Payments::SetUser($ArrayUser);
    Payments::CheckIfPaymentPeriodExists();


    //no period exists. so system did renew for user subscription, Here we will update invoice user total 
    //and send an email to user to pay. make user also untrusted untill he pays
    if (!$LogExists) {

        //get current payment log . if it is just added
        $ArrayPaymentPeriod = Payments::GetLog();
        $LogID = $ArrayPaymentPeriodNew["LogID"];

        //here update invoice and check balance
        //login admin to get session id to use in all subsequent api's calls
        $login = $admin_api->login();
        if (!$login[0]) {
           continue;
        }
        //admin session id
        $admin_session_id = $login[1];

        //get current user package service fee
        $service_fee = empty($ArrayUserGroup['PaymentSystemChargeAmount']) ? 0 : $ArrayUserGroup['PaymentSystemChargeAmount'];
        $current_balance = UserBalance::getUserBalance($ArrayUser['UserID']);
        $discount = 0;
        $new_balance = 0;
        $send_payment = true;

        if ($current_balance > $service_fee) {
            $discount = $service_fee;
            $new_balance = $current_balance - $discount;
            $send_payment = false;
        } elseif ($current_balance == $service_fee) {
            $discount = $service_fee;
            $new_balance = 0;
            $send_payment = false;
        } else {
            $discount = $current_balance;
            $new_balance = 0;
            $send_payment = true;
        }

        $check_bal_exists = UserBalance::checkBalanceExists($ArrayUser['UserID']);

        if ($check_bal_exists) {
            // if he has balance records,then update current balance
            $ArrayFieldnValues = array(
                "Balance" => $new_balance,
                "UpdateDate" => date('Y-m-d H:i:s')
            );
            $ArrayCriterias = array("RelUserID" => $ArrayUser['UserID']);
            UserBalance::updateUserBalance($ArrayFieldnValues, $ArrayCriterias);
        } else {
            // if user has no balance records. then create new record
            $ArrayFieldAndValues = array(
                "RelUserID" => $ArrayUser['UserID'],
                "Balance" => $new_balance,
                "CreationDate" => date('Y-m-d H:i:s'),
            );
            UserBalance::CreateUserBalance($ArrayFieldAndValues);
        }

        $ReputationLevel = $send_payment ? 'Untrusted' : 'Trusted';
        //update user to be un-trusted untill he pays
        Users::UpdateUser(array('ReputationLevel' => $ReputationLevel), array('UserID' => $ArrayUser["UserID"]));

        $payment_api = new PaymentAPI();
        $payment_api->setAdminSessionId($admin_session_id);

        $PaymentStatus = $send_payment ? 'Unpaid' : 'Paid';

        //prepare payment data array
        $data_payment = array(
            "UserID" => $ArrayUser["UserID"],
            "LogID" => $LogID,
            "Discount" => $discount,
            "IncludeTax" => 'Exclude',
            "PaymentStatus" => $PaymentStatus,
            "SendReceipt" => "Yes",
        );
        //update invoice with service total if usergroup has service fees
        $payment_api->updateInvoice($data_payment);
    }

    // Check if payment period for this user exists for the current period - End
    // Check if user has exceeded monthly campaign sending limit - Start
    $ArrayPaymentPeriod = Payments::GetLog();
    if (($ArrayPaymentPeriod['CampaignsSent'] >= $ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod']) && ($ArrayUser['GroupInformation']['LimitCampaignSendPerPeriod'] > 0)) {
        continue;
    }
    // Check if user has exceeded monthly campaign sending limit - End
    // Start benchmarking - Start
    EmailQueue::StartBenchmarking();
    // Start benchmarking - End
    // Check if there's enough credits for the delivery - Start {
    $CreditsCheckReturn = Payments::CheckAvailableCredits($ArrayUser);

    if ($CreditsCheckReturn[0] == false) {
        continue;
    }
    // Check if there's enough credits for the delivery - End }
    // Change campaigns status to 'sending' - Start
    if ($ArrayCampaign['SendProcessStartedOn'] != '0000-00-00 00:00:00') {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
        );
    } else {
        $ArrayFieldnValues = array(
            'CampaignStatus' => 'Sending',
            'SendProcessStartedOn' => date('Y-m-d H:i:s'),
        );
    }
    $ArrayFieldnValues['LastActivityDateTime'] = date('Y-m-d H:i:s');
    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
    // Change campaigns status to 'sending' - End
    // Generate the queue of the campaign - Start
    $TotalRecipients = EmailQueue::GenerateQueue($EachCampaignID, $ArrayCampaign['RelOwnerUserID']);
    // Generate the queue of the campaign - End
    // Update total recipients of the campaign - Start
    Campaigns::Update(array('TotalRecipients' => $TotalRecipients, 'LastActivityDateTime' => date('Y-m-d H:i:s')), array('CampaignID' => $EachCampaignID));
    // Update total recipients of the campaign - End
    // Check if activity exceeds user group threshold - Start {
    if (($ArrayUser['GroupInformation']['ThresholdEmailSend'] > 0) && ($TotalRecipients > $ArrayUser['GroupInformation']['ThresholdEmailSend'])) {
        O_Email_Sender_ForAdmin::send(
                O_Email_Factory::userExceededEmailSendThreshold(
                        $ArrayUser, array('CampaignID' => $ArrayCampaign['CampaignID'], 'CampaignName' => $ArrayCampaign['CampaignName'], 'TotalRecipients' => $TotalRecipients)
                )
        );

        // Plug-in hook - Start
        Plugins::HookListener('Action', 'Threshold.CampaignRecipients', array($ArrayUser, $TotalRecipients, $EachCampaignID));
        // Plug-in hook - End
    }
    // Check if activity exceeds user group threshold - End }
    // Prepare and send emails to the queue - Start
    $TotalEmailsSent = EmailQueue::SendEmails($EachCampaignID, $ArrayUser, 10, $IsAorBSplitTestCampaign);
    // Prepare and send emails to the queue - End
    // Log the activity - Start
    Core::AddToActivityLog($ArrayCampaign['RelOwnerUserID'], 'Campaign Sent', 'Campaign #' . $EachCampaignID . ' sent (' . number_format($TotalEmailsSent) . ' recipients)');
    // Log the activity - End
    // Log the number of sent email in activity statistics  - Start {
    $ArrayActivities = array(
        'TotalSentEmail' => $TotalEmailsSent,
    );
    Statistics::UpdateListActivityStatistics(0, $ArrayCampaign['RelOwnerUserID'], $ArrayActivities);
    // Log the number of sent email in activity statistics  - End }
    // Stop benchmarking - Start
    EmailQueue::StopBenchmarking();
    // Stop benchmarking - End
    // Calculate benchmarking - Start
    $TotalEmailsPerSecond = EmailQueue::CalculateBenchmark($TotalEmailsSent);
    // Calculate benchmarking - End
    // Purge queue records and update campaign - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $EachCampaignID));

    if ($ArrayCampaign['CampaignStatus'] == 'Sending') {
        // Campaign is not paused. Proceed with normal procedure
        $TMPArrayReturn = EmailQueue::PurgeQueue($EachCampaignID);

        $ArrayFieldnValues = array(
            'CampaignStatus' => ($ArrayCampaign['ScheduleType'] == 'Recursive' ? 'Ready' : 'Sent'),
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );

        // Campaign is sent. Track the payment activity - Start
        Payments::CampaignSent($ArrayCampaign['CampaignID']);
        // Campaign is sent. Track the payment activity - End
    } else {
        // Campaign has been paused by user.
        $ArrayFieldnValues = array(
            'SendProcessFinishedOn' => date('Y-m-d H:i:s'),
            'BenchmarkEmailsPerSecond' => number_format($TotalEmailsPerSecond, 4),
        );
    }

    Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $EachCampaignID));
    // Purge queue records and update campaign - End

    $TotalProcessedCampaigns++;
}
// Generate the queue for each campaign - End
?>