<?php

include_once 'init.php';
include_once $DATA_PATH . 'config.inc.php';

// Disable web cron module - Start {
if (DISABLE_WEB_CRONS == true) {
    print('Disabled.');
    exit;
}
// Disable web cron module - End }

$IsWEBCRON = true;

// PHP settings - Start
set_time_limit(0);
ignore_user_abort(true);
session_write_close();
// PHP settings - End
// 
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('users');
Core::LoadObject('campaigns');
Core::LoadObject('queue');
Core::LoadObject('segments');
Core::LoadObject('personalization');
Core::LoadObject('attachments');
Core::LoadObject('payments');
Core::LoadObject('split_tests');
Core::LoadObject('benchmark');
Core::LoadObject('user_balance');
Core::LoadObject('user_payment');
// Load other modules - End

if (DEMO_MODE_ENABLED == true) {
    print('NOT AVAILABLE IN DEMO MODE.');
    exit;
}

// Log the process - Start {
$ProcessCode = 'cron_payment';
$ProcessLogID = Core::RegisterToProcessLog($ProcessCode);

// Log the process - End }
// Load language - Start
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');
// Load language - End
// Cron - Start
try {
    include_once(APP_PATH . '/includes/cli/include_payment.inc.php');

//    $ShellMessage = sprintf('%s campaigns processed and sent.', number_format($TotalProcessedCampaigns));
} catch (Exception $e) {
    if ($e->GetMessage() != '') {
        $ShellMessage = $e->GetMessage();
    }
}
// Cron - End
// Display success message - Start
print($ShellMessage);
// Display success message - End