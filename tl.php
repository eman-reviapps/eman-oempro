<?php
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End

/**
 * Returns query string without parameter key
 * @return string
 */
function getQueryStringWithoutKey() {
	return ltrim($_SERVER['QUERY_STRING'], 'p=');
}

/**
 * Returns an array of parameters obtained from query string.
 * Includes the fix for the issue that is caused by automatically
 * decoded $_GET parameters (automatic decoding of "%2F" into "/")
 *
 * @return array
 */
function explodeParameters() {
	$undecodedParameters = explode('/', getQueryStringWithoutKey(), 8);
	$decodedParameters = array();
	foreach ($undecodedParameters as $each) {
		$decodedParameters[] = urldecode($each);
	}
	return $decodedParameters;
}

$QueryParameters = Core::DecryptQueryStringAsArrayAdvanced(getQueryStringWithoutKey());
$array = explodeParameters();

$isBase64Encoded = FALSE;
if (version_compare(PHP_VERSION, '5.2.0') >= 0) {
	if (base64_decode($array[7], true) !== FALSE) {
		$isBase64Encoded = TRUE;
	}
} else {
	if (preg_match('/[%:]/', $array[7]) != 1 && imap_base64($array[7]) !== FALSE) {
		$isBase64Encoded = TRUE;
	}
}

if ($isBase64Encoded === FALSE) {
	$array[7] = rawurlencode($array[7]);
} else {
	$array[7] = rawurlencode(base64_decode($array[7]));
}

// Security check for Open redirect attack (https://www.owasp.org/index.php/Open_redirect) - Start
$ArrayParsedURL = parse_url(rawurldecode($array[7]));
if ($ArrayParsedURL == false) return false;

$SecureURLSchemes = array('https', 'http');
if (!in_array($ArrayParsedURL['scheme'], $SecureURLSchemes))
{
// If the URL is not a valid URL (like javascript etc.), instead of returning false,
// we are giving a BAD REQUEST response.
// This is because even if we returned false, link tracker would redirect to the URL
	header("HTTP/1.0 400 Bad Request");
	echo 'Bad request';
	exit;
}
// Security check for Open redirect attack (https://www.owasp.org/index.php/Open_redirect) - End

$_POST = array(
	'CampaignID' => $QueryParameters[0],
	'EmailID' => $QueryParameters[1],
	'AutoResponderID' => $QueryParameters[2],
	'SubscriberID' => $QueryParameters[3],
	'ListID' => $QueryParameters[4],
	'Preview' => $QueryParameters[5] == 0 ? '' : $QueryParameters[5],
	'LinkTitle' => $array[6],
	'LinkURL' => $array[7],
	'ShorterLinks' => TRUE
);

include_once 'track_link.php';
