<?php

/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 * */
/**
 * Email open tracking module
 * */
// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End
// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('campaigns');
Core::LoadObject('auto_responders');
Core::LoadObject('statistics');
Core::LoadObject('personalization');
// Load other modules - End
// Set the POST and GET same - Start
if ((count($_POST) == 0) && (count($_GET) > 0)) {
    $_POST = $_GET;
}
// Set the POST and GET same - End
// Decrypt URL parameters - Start
if ($_POST['p'] != '') {
    $ArrayParameters = Core::DecryptURL($_POST['p']);
} else {
    $ArrayParameters = $_POST;
}
// Decrypt URL parameters - End
// Set varilables - Start
$CampaignID = $ArrayParameters['CampaignID'];
$EmailID = $ArrayParameters['EmailID'];
$AutoResponderID = $ArrayParameters['AutoResponderID'];
$SubscriberID = $ArrayParameters['SubscriberID'];
$ListID = $ArrayParameters['ListID'];
$Preview = $ArrayParameters['Preview'];
// Set varilables - End
// Retrieve information - Start
try {
    // Retrieve campaign information - Start
    $ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));
    if ($ArrayCampaign == false) {
        $ArrayCampaign = array();
    }
    // Retrieve campaign information - End
    // Retrieve email information (if provided) - Start {
    if ($EmailID > 0) {
        $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $EmailID), false);
        if ($ArrayEmail == false) {
            $ArrayEmail = array();
        }
    }
    // Retrieve email information (if provided) - End }
    // Retrieve auto-responder information - Start
    $ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), Array('AutoResponderID' => $AutoResponderID));
    if ($ArrayAutoResponder == false) {
        $ArrayAutoResponder = array();
    }
    // Retrieve auto-responder information - End

    if ((count($ArrayAutoResponder) == 0) && (count($ArrayCampaign) == 0)) {
        throw new Exception('Campaign or autoresponder not found');
    }

    // Retrieve user information - Start
    $ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => (isset($ArrayCampaign['RelOwnerUserID']) == true ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID'])), true);
    if ($ArrayUser == false) {
        throw new Exception('User not found');
    }
    // Retrieve user information - End
    // Retrieve email information - Start
    if ($EmailID == 0) {
        $ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => ($ArrayCampaign['RelEmailID'] != '' ? $ArrayCampaign['RelEmailID'] : $ArrayAutoResponder['RelEmailID'])));
        if ($ArrayEmail == false) {
            throw new Exception('Email not found');
        }
    }
    // Retrieve campaign information - End

    if (($ListID != '') && ($SubscriberID != '') && ($Preview == '')) {
        // Retrieve list information - Start
        $ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ListID), false);
        if ($ArrayList == false) {
            throw new Exception('Subscriber list not found');
        }
        // Retrieve list information - End
        // Retrieve subscriber information - Start
        $ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
        if ($ArraySubscriber == false) {
            throw new Exception('Subscriber not found');
        }
        // Retrieve subscriber information - End		
        // Check if this user has viewed this campaign on web browser before - Start
        if (count($ArrayCampaign) > 0) {
            $TotalWebBrowserViews = Statistics::RetrieveCampaignWebBrowserStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayCampaign['CampaignID'], ($EmailID > 0 ? $ArrayEmail['EmailID'] : 0));
        } elseif (count($ArrayAutoResponder) > 0) {
            $TotalWebBrowserViews = Statistics::RetrieveAutoResponderWebBrowserStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayAutoResponder['AutoResponderID']);
        }
        // Check if this user has viewed this campaign on web browser before - End
        // Track web browser view activity - Start
        $ArrayFieldnValues = array();

        if ($TotalWebBrowserViews == 0) {
            // Update 'unique' web browser view statistics - Start
            if (count($ArrayCampaign) > 0) {
                $ArrayFieldnValues['UniqueViewsOnBrowser'] = $ArrayCampaign['UniqueViewsOnBrowser'] + 1;
            } elseif (count($ArrayAutoResponder) > 0) {
                $ArrayFieldnValues['UniqueViewsOnBrowser'] = $ArrayAutoResponder['UniqueViewsOnBrowser'] + 1;
            }
            // Update 'unique' web browser view statistics - End
        }

        // Update 'total' web browser view statistics - Start
        if (count($ArrayCampaign) > 0) {
            $ArrayFieldnValues['TotalViewsOnBrowser'] = $ArrayCampaign['TotalViewsOnBrowser'] + 1;
            Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));
        } elseif (count($ArrayAutoResponder) > 0) {
            $ArrayFieldnValues['TotalViewsOnBrowser'] = $ArrayAutoResponder['TotalViewsOnBrowser'] + 1;
            AutoResponders::Update($ArrayFieldnValues, array('AutoResponderID' => $ArrayAutoResponder['AutoResponderID']));
        }
        // Update 'total' web browser view statistics - End
        // Update forward statistics table - Start
        Statistics::RegisterWebBrowserView($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], ($ArrayCampaign['CampaignID'] != '' ? $ArrayCampaign['CampaignID'] : 0), ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), ($ArrayAutoResponder['AutoResponderID'] != '' ? $ArrayAutoResponder['AutoResponderID'] : 0), ($EmailID > 0 ? $ArrayEmail['EmailID'] : 0));
        // Update forward statistics table - End
    }
} catch (Exception $e) {
    if ($e->GetMessage() != '') {
        print($e->GetMessage());
        exit;
    }
}
// Retrieve information - End
// Decide which content to show - Start
if ($ArrayEmail['FetchURL'] != '') {
    $ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false));

    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $ShowContent, '', $ArraySubscriber));
    $ShowContent = $ArrayPlugInReturnVars[1];
    // Plug-in hook - End
    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $ShowContent, '', $ArraySubscriber, 'Web Browser View'));
    $ShowContent = $ArrayPlugInReturnVars[1];
    // Plug-in hook - End
} elseif ($ArrayEmail['HTMLContent'] != '') {
    $ShowContent = $ArrayEmail['HTMLContent'];

    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', $ShowContent, '', $ArraySubscriber));
    $ShowContent = $ArrayPlugInReturnVars[1];
    // Plug-in hook - End
    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', $ShowContent, '', $ArraySubscriber, 'Web Browser View'));
    $ShowContent = $ArrayPlugInReturnVars[1];
    // Plug-in hook - End
} else {
    if ($ArrayEmail['FetchPlainURL'] != '') {
        $ShowContent = Campaigns::FetchRemoteContent(Personalization::Personalize($ArrayEmail['FetchPlainURL'], array('Subscriber', 'User'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false));
    } else {
        $ShowContent = $ArrayEmail['PlainContent'];
    }

    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.Before', array('', '', $ShowContent, $ArraySubscriber));
    $ShowContent = $ArrayPlugInReturnVars[2];
    // Plug-in hook - End
    // Plug-in hook - Start
    $ArrayPlugInReturnVars = Plugins::HookListener('Filter', 'Email.Send.EachRecipient', array('', '', $ShowContent, $ArraySubscriber, 'Web Browser View'));
    $ShowContent = $ArrayPlugInReturnVars[2];
    // Plug-in hook - End
}
// Decide which content to show - End
// Add header/footer to the email (if exists in the user group) - Start {
if (($ArrayEmail['HTMLContent'] != '') || ($ArrayEmail['FetchURL'] != '')) {
    $TMPPlainBody = '';
    $TMPHTMLBody = $ShowContent;
} else {
    $TMPPlainBody = $ShowContent;
    $TMPHTMLBody = '';
}
$ArrayReturn = Personalization::AddEmailHeaderFooter($TMPPlainBody, $TMPHTMLBody, $ArrayUser['GroupInformation']);
if (($ArrayEmail['HTMLContent'] != '') || ($ArrayEmail['FetchURL'] != '')) {
    $ShowContent = $ArrayReturn[1];
} else {
    $ShowContent = $ArrayReturn[0];
}
// Add header/footer to the email (if exists in the user group) - End }
// Perform personalization (disable personalization and links if subscriber information not provided) - Start
$ShowContent = Personalization::Personalize($ShowContent, array('Links', 'User', 'RemoteContent'), $ArraySubscriber, $ArrayUser, $ArrayList, $ArrayCampaign, array(), false, ($EmailID > 0 ? $EmailID : 0), true);
// Perform personalization (disable personalization and links if subscriber information not provided) - End
// Display content - Start

$header .= '<head>
        <meta property="og:title" content="' . $ArrayCampaign['CampaignName'] . '" /> 
        <meta property="og:site_name" content="' . $ArrayCampaign['CampaignName'] . '" /> 
        <meta property="og:type" content="blog" />                  
        <meta property="og:image" content="' . AWS_END_POINT . S3_BUCKET . "/" . $ArrayEmail['ScreenshotImage'] . '" />';

$ShowContent = str_replace("<head>", $header, $ShowContent);
//<meta property="og:url" content="https://flyinglist.com" /> 
//<meta property="og:description" content="' . $ArrayCampaign['CampaignName'] . '" />
if (($ArrayEmail['HTMLContent'] != '') || ($ArrayEmail['FetchURL'] != '')) {
    print($ShowContent);
} else {
    print(nl2br($ShowContent));
}
// Display content - End

exit;
?>