<?php
// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

$QueryParameters = Core::DecryptQueryStringAsArrayAdvanced($_GET['p']);


$_POST = array(
	'CampaignID'			=> $QueryParameters[0],
	'EmailID'				=> $QueryParameters[1],
	'AutoResponderID'		=> $QueryParameters[2],
	'SubscriberID'			=> $QueryParameters[3],
	'ListID'				=> $QueryParameters[4],
	'Preview'				=> $QueryParameters[5] == 0 ? '' : $QueryParameters[5],
);

include_once 'web_browser.php';