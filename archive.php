<?php
/**
 * 
 *
 * @author Mert Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * Campaign Public Archive Page
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('campaigns');
Core::LoadObject('tags');
Core::LoadObject('public_archives');
Core::LoadObject('users');
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
// Load other modules - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'public_archives');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Decrypt URL parameters - Start
if (isset($_GET['a']) && $_GET['a'] != '')
	{
	$ArrayParameters = PublicArchives::GetParametersFromURL($_GET['a']);
	}
// Decrypt URL parameters - End

// Check if tag id is valid, if not display error page - Start
$ArrayTagInformation = Tags::RetrieveTag(array('*'), array('TagID'=>$ArrayParameters['TagID']));
if ($ArrayTagInformation == false)
	{
	// Error page parsing - Start
	$ObjectTemplate 					= new TemplateEngine();
	$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1524'];
	$ObjectTemplate->ArrayFormFields 	= array(
												);

	$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/error.tpl', 'file');

	$ObjectTemplate->LoadTemplates();

	$ArrayReplaceList = array(
							'Insert:ErrorTitle'			=> $ArrayLanguageStrings['Screen']['0010'],
							'Insert:ErrorDesc'			=> $ArrayLanguageStrings['Screen']['0011'],
							);
	$ObjectTemplate->Replace($ArrayReplaceList);

	$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
	// Error page parsing - End

	exit;
	}
// Check if tag id is valid, if not display error page - End

// Retrieve Campaigns - Start
$ArrayCampaigns = Campaigns::RetrieveCampaigns(array('*'), array(array('field' => 'Campaigns.CampaignID', 'operator' => '>=', 'value' => 0)), array('SendProcessFinishedOn'=>'DESC'), 0, 0, array(), array($ArrayParameters['TagID']), false, false);
if ($ArrayCampaigns == false)
	{
	// Display no entries in archive page - Start
	$ObjectTemplate 					= new TemplateEngine();
	$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1528'];
	$ObjectTemplate->ArrayFormFields 	= array(
												);

	$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/success.tpl', 'file');

	$ObjectTemplate->LoadTemplates();

	$ArrayReplaceList = array(
							'Insert:MessageTitle'			=> $ArrayLanguageStrings['Screen']['0012'],
							'Insert:MessageDesc'			=> $ArrayLanguageStrings['Screen']['0013'],
							);
	$ObjectTemplate->Replace($ArrayReplaceList);

	$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
	
	exit;
	// Display no entries in archive page - End
	}
else
	{
	// List campaigns - Start
	$ObjectTemplate 					= new TemplateEngine();
	$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1528'];
	$ObjectTemplate->ArrayFormFields 	= array(
												);


	// Check if template url exists, if yes fetch template or display default template - Start
	if ($ArrayParameters['TemplateURL'] != '')
		{
		// Fetch remote template - Start
		$RemoteTemplate = Core::DataPostToRemoteURL($ArrayParameters['TemplateURL'], array());
		// Fetch remote template - End
	
		if ($RemoteTemplate[0] == false)
			{
			// Display remote template error page - Start
			$ObjectTemplate 					= new TemplateEngine();
			$ObjectTemplate->PageTitle 			= $ArrayLanguageStrings['Screen']['1524'];
			$ObjectTemplate->ArrayFormFields 	= array(
														);

			$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/error.tpl', 'file');

			$ObjectTemplate->LoadTemplates();

			$ArrayReplaceList = array(
									'Insert:ErrorTitle'			=> $ArrayLanguageStrings['Screen']['1525'],
									'Insert:ErrorDesc'			=> $ArrayLanguageStrings['Screen']['1526'],
									);
			$ObjectTemplate->Replace($ArrayReplaceList);

			$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
			// Display remote template error page - End

			exit;
			}
		else
			{
			$ObjectTemplate->AddToTemplateList($RemoteTemplate[1], 'string');
			}
		}
	else
		{
		$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH.'desktop/public/archive.tpl', 'file');
		}
	// Check if template url exists, if yes fetch template or display default template - End

	$ObjectTemplate->LoadTemplates();

	$ArrayReplaceList = array(
							'Insert:MessageTitle'			=> $ArrayLanguageStrings['Screen']['1527'],
							);
	$ObjectTemplate->Replace($ArrayReplaceList);


	$ObjectTemplate->DefineBlock('LIST:Campaigns');
	foreach ($ArrayCampaigns as $EachCampaign)
		{
		// Browser version link - Start
			// Encrypted query parameters - Start
			$ArrayQueryParameters = array(
										'CampaignID'		=> $EachCampaign['CampaignID'],
										'AutoResponderID'	=> 0,
										'SubscriberID'		=> 0,
										'ListID'			=> 0,
										'Preview'			=> true,
										);
			$EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
			// Encrypted query parameters - End
			$CampaignBrowserURL = APP_URL.'web_browser.php?p='.$EncryptedQuery;
		// Browser version link - End
		
		$ObjectTemplate->DuplicateBlock('LIST:Campaigns','LIST:Campaigns',array(
			"CampaignName"	=>	$EachCampaign['CampaignName'],
			"CampaignURL"	=>	$CampaignBrowserURL,
			"CampaignDate"	=>	date($ArrayLanguageStrings['Config']['LongDateFormat'], strtotime($EachCampaign['SendProcessStartedOn'])),
		));
		}

	$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);

	exit;
	// List campaigns - End
	}
// Retrieve Campaigns - End


?>