<?php
// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

$QueryParameters = Core::DecryptQueryStringAsArrayAdvanced($_GET['p']);


$_POST = array(
	'FormValue_CampaignID'		=> $QueryParameters[0],
	'FormValue_AutoResponderID'	=> $QueryParameters[1],
	'FormValue_SubscriberID'	=> $QueryParameters[2],
	'FormValue_ListID'			=> $QueryParameters[3],
	'FormValue_EmailID'			=> $QueryParameters[4],
	'Preview'					=> $QueryParameters[5] == 0 ? '' : $QueryParameters[5],
	'RequestConfirmation'		=> (isset($QueryParameters[6]) == true ? $QueryParameters[6] : 0),
);

if (isset($_POST['RequestConfirmation']) == true && $_POST['RequestConfirmation'] == 1)
{
	header('Location: uc.php?p='.$_GET['p']);
	exit;
}

include_once 'unsubscribe.php';