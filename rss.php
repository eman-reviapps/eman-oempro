<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth, 11 November, 2007
 * @package default
 **/

/**
 * RSS feed for campaigns
 **/

// Include main module - Start
include_once(dirname(__FILE__).'/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('campaigns');
Core::LoadObject('emails');
Core::LoadObject('personalization');
// Load other modules - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'forward_email');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH.'languages');
// Load language - End

// Retrieve list information - Start
$ArrayList = Lists::RetrieveList(array('*'), array('MD5(CONCAT("'.MD5_SALT.'", ListID))' => $_GET['q']), false);

if ($ArrayList == false)
	{
	print('Incorrect parameters');
	exit;
	}
// Retrieve list information - End

// Retrieve list of campaigns that are sent to this list - Start
$ArrayCampaigns = Campaigns::GetCampaignsSentToList($ArrayList['ListID']);
// Retrieve list of campaigns that are sent to this list - End

// Generate RSS feed - Start
$Now		= date("D, d M Y H:i:s T");

$RSS		= array();
$RSS[]		= '<?xml version="1.0" encoding="ISO-8859-1" ?>';
$RSS[]		= '<rss version="2.0">';
$RSS[]		= '<channel>';
$RSS[]		= '<title><![CDATA['.$ArrayList['Name'].' Emails]]></title>';
$RSS[]		= '<link>'.APP_URL.'rss.php?q='.$_GET['q'].'</link>';
$RSS[]		= '<description><![CDATA['.sprintf($ArrayLanguageStrings['Screen']['9110'], $ArrayList['Name']).']]></description>';
$RSS[]		= '<language>en-us</language>';
$RSS[]		= '<pubDate>'.$Now.'</pubDate>';
$RSS[]		= '<lastBuildDate>'.$Now.'</lastBuildDate>';

foreach ($ArrayCampaigns as $Key=>$ArrayEachCampaign)
	{
	$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $ArrayEachCampaign['RelEmailID']));
	
	if ($ArrayEmail != false)
		{
		// Perform personalization (disable personalization and links if subscriber information not provided) - Start
		$Subject		= Personalization::Personalize($ArrayEmail['Subject'], array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), array(), array(), $ArrayList, $ArrayEachCampaign, array(), true);
		$EmailContent	= Personalization::Personalize(($ArrayEmail['HTMLContent'] == '' ? $ArrayEmail['PlainContent'] : $ArrayEmail['HTMLContent']), array('Subscriber', 'Links', 'User', 'OpenTracking', 'LinkTracking', 'RemoteContent'), array(), array(), $ArrayList, $ArrayEachCampaign, array(), true);
		// Perform personalization (disable personalization and links if subscriber information not provided) - End

		$RSS[]		= '<item>';
		$RSS[]		= '<title><![CDATA['.$Subject.']]></title>';

		// Generate web version for the campaign - Start
		$ArrayQueryParameters = array(
									'CampaignID'		=> $ArrayEachCampaign['CampaignID'],
									'AutoResponderID'	=> 0,
									'SubscriberID'		=> 0,
									'ListID'			=> $ArrayList['ListID'],
									'Preview'			=> true,
									);
		$EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
		// Generate web version for the campaign - End

		$RSS[]		= '<link><![CDATA['.APP_URL.'web_browser.php?p='.$EncryptedQuery.']]></link>';
		$RSS[]		= '<description><![CDATA['.$EmailContent.']]></description>';
		$RSS[]		= '</item>';
		}
	}

$RSS[]		= '</channel>';
$RSS[]		= '</rss>';

// Generate RSS feed - End

header("Content-Type: application/xml; charset=utf-8");
print implode($RSS);

exit;
?>