<?php
// Include libraries
$IsCLI = true;
include_once(dirname(__FILE__) . '/data/config.inc.php');

try {
	// Setup Wufoo Integration Module
	$wi = O_Registry::instance()->getWufooIntegrationModule();
	$wi->setStatus(WUFOO_INTEGRATION_ENABLED);

	// Process webhook data and integration result
	$wi->processWebHookData();
	$integrationResult = $wi->getResult();
	if (!$integrationResult->isSuccess())
		throw new O_Integration_Exception($integrationResult->getMessage());

	// Log successfull webhook processing
	$logMapper = O_Registry::instance()->getMapper('WufooIntegrationLog');
	$log = new O_Domain_WufooIntegrationLog();
	$log->setIntegrationId($wi->getIntegrationObject()->getId());
	$log->setListId($wi->getIntegrationObject()->getListId());
	$log->setUserId($wi->getIntegrationObject()->getUserId());
	$log->setStatus(1);
	$log->setMessage($integrationResult->getMessage());
	$logMapper->insert($log);
} catch (Exception $exception) {
	// Log webhook processing error
	$logMapper = O_Registry::instance()->getMapper('WufooIntegrationLog');
	$log = new O_Domain_WufooIntegrationLog();
	$integrationObject = $wi->getIntegrationObject();
	if ($integratioObject instanceof O_Domain_WufooIntegration) {
		$log->setIntegrationId($wi->getIntegrationObject()->getId());
		$log->setListId($wi->getIntegrationObject()->getListId());
		$log->setUserId($wi->getIntegrationObject()->getUserId());
	}
	$log->setStatus(0);
	$log->setMessage($exception->getMessage());
	$logMapper->insert($log);
}
exit;