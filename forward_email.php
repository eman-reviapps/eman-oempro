<?php
/**
 * @author	Cem Hurturk
 * @version   $Id$
 * @copyright Octeth, 11 November, 2007
 * @package   default
 **/

/**
 * Forward To Friend module
 **/

// Include main module - Start
include_once(dirname(__FILE__) . '/data/config.inc.php');
// Include main module - End

// Load other modules - Start
Core::LoadObject('octeth_template');
Core::LoadObject('template_engine');
Core::LoadObject('subscribers');
Core::LoadObject('lists');
Core::LoadObject('emails');
Core::LoadObject('users');
Core::LoadObject('campaigns');
Core::LoadObject('auto_responders');
Core::LoadObject('statistics');
Core::LoadObject('form');
Core::LoadObject('emails');
// Load other modules - End

// Set the POST and GET same - Start
if ((count($_POST) == 0) && (count($_GET) > 0)) {
	$_POST = $_GET;
}
// Set the POST and GET same - End

// Decrypt URL parameters - Start
if ($_POST['p'] != '') {
	$ArrayParameters = Core::DecryptURL($_POST['p']);
}
else
{
	$ArrayParameters = $_POST;
}
// Decrypt URL parameters - End

// Set varilables - Start
$CampaignID = $ArrayParameters['CampaignID'];
$EmailID = $ArrayParameters['EmailID'];
$AutoResponderID = $ArrayParameters['AutoResponderID'];
$SubscriberID = $ArrayParameters['SubscriberID'];
$ListID = $ArrayParameters['ListID'];
$Preview = $ArrayParameters['Preview'];
$ShowResultPage = false;
// Set varilables - End

// Load language - Start
// $ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, 'forward_email');
$ArrayLanguageStrings = Core::LoadLanguage(LANGUAGE, '', TEMPLATE_PATH . 'languages');
// Load language - End

// Retrieve information - Start
try
{
	// Retrieve campaign information - Start
	$ArrayCampaign = Campaigns::RetrieveCampaign(array('*'), array('CampaignID' => $CampaignID));
	if ($ArrayCampaign == false) {
		$ArrayCampaign = array();
	}
	// Retrieve campaign information - End

	// Retrieve email information (if provided) - Start {
	if ((isset($EmailID) == true) && ($EmailID > 0)) {
		$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => $EmailID), false);
		if ($ArrayEmail == false) {
			$ArrayEmail = array();
		}
	}
	// Retrieve email information (if provided) - End }

	// Retrieve auto-responder information - Start
	$ArrayAutoResponder = AutoResponders::RetrieveResponder(array('*'), Array('AutoResponderID' => $AutoResponderID));
	if ($ArrayAutoResponder == false) {
		$ArrayAutoResponder = array();
	}
	// Retrieve auto-responder information - End

	if ((count($ArrayAutoResponder) == 0) && (count($ArrayCampaign) == 0)) {
		throw new Exception('Campaign or autoresponder not found');
	}

	// Retrieve user information - Start
	$ArrayUser = Users::RetrieveUser(array('*'), array('UserID' => (isset($ArrayCampaign['RelOwnerUserID']) == true ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID'])), true);
	if ($ArrayUser == false) {
		throw new Exception('User not found');
	}
	// Retrieve user information - End

	// Retrieve email information - Start
	if ((isset($EmailID) == false) || ($EmailID == 0)) {
		$ArrayEmail = Emails::RetrieveEmail(array('*'), array('EmailID' => ($ArrayCampaign['RelEmailID'] != '' ? $ArrayCampaign['RelEmailID'] : $ArrayAutoResponder['RelEmailID'])));
		if ($ArrayEmail == false) {
			throw new Exception('Email not found');
		}
	}
	// Retrieve campaign information - End

	if (($ListID != '') && ($SubscriberID != '') && ($Preview == '')) {
		// Retrieve list information - Start
		$ArrayList = Lists::RetrieveList(array('*'), array('ListID' => $ListID), false);
		if ($ArrayList == false) {
			throw new Exception('Subscriber list not found');
		}
		// Retrieve list information - End

		// Retrieve subscriber information - Start
		$ArraySubscriber = Subscribers::RetrieveSubscriber(array('*'), array('SubscriberID' => $SubscriberID), $ArrayList['ListID']);
		if ($ArraySubscriber == false) {
			throw new Exception('Subscriber not found');
		}
		// Retrieve subscriber information - End		
	}
}
catch (Exception $e)
{
	if ($e->GetMessage() != '') {
		print($e->GetMessage());
		exit;
	}
}
// Retrieve information - End

// Page parsing - Start
$ObjectTemplate = new TemplateEngine();

// EVENT: Send forward email to the friend - Start
if (($_POST['FormButton_Forward'] != '') && ($Preview == '')) {
	$detector = new O_Security_Flood_Detector('forward_to_friend');
	$detector->setMaxAccessForTimePeriod(
		SCRTY_FORWARDFRIEND_MAX_REQUESTS_ALLOWED,
		SCRTY_FORWARDFRIEND_MAX_REQUESTS_IN_SECONDS,
		SCRTY_FORWARDFRIEND_BLOCK_FOR_SECONDS);

	$ipAddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	if ($detector->isFloodedByIP($ipAddress)) {
		// DO NOTHING, JUST SHOW THE RESULT PAGE
		// The first rule for preventing a hack is to not let the hacker
		// know that we are aware of the hack in progress.

		$ShowResultPage = true;
	} else {
		$ObjectTemplate->ArrayErrorMessages = array();
		try
		{
			// Check for required fields - Start
			$ArrayRequiredFields = array(
				"FriendName" => $ArrayLanguageStrings['Screen']['1502'],
				"FriendEmail" => $ArrayLanguageStrings['Screen']['1503'],
				"SenderName" => $ArrayLanguageStrings['Screen']['1504'],
				"SenderEmail" => $ArrayLanguageStrings['Screen']['1505'],
			);
			$ObjectTemplate->ArrayErrorMessages = $ObjectTemplate->CheckRequiredFields($ArrayRequiredFields, $ArrayErrorMessages, $_POST, 'FormValue_');

			if (count($ObjectTemplate->ArrayErrorMessages) > 0) {
				throw new Exception($ArrayLanguageStrings['Screen']['1506']);
			}
			// Check for required fields - End

			// Email address validations - Start
			if (Subscribers::ValidateEmailAddress($_POST['FormValue_FriendEmail']) == false) {
				$ObjectTemplate->ArrayErrorMessages['FriendEmail'] = $ArrayLanguageStrings['Screen']['1507'];
			}
			if (Subscribers::ValidateEmailAddress($_POST['FormValue_SenderEmail']) == false) {
				$ObjectTemplate->ArrayErrorMessages['SenderEmail'] = $ArrayLanguageStrings['Screen']['1507'];
			}

			if (count($ObjectTemplate->ArrayErrorMessages) > 0) {
				throw new Exception($ArrayLanguageStrings['Screen']['1506']);
			}
			// Email address validations - End


			$EncryptedQuery = Core::EncryptArrayAsQueryStringAdvanced(array(
				($ArrayCampaign['CampaignID'] == '' ? 0 : $ArrayCampaign['CampaignID']), // Campaign ID
				($EmailID == 0 ? 0 : $ArrayEmail['EmailID']), // Email ID
				($ArrayAutoResponder['AutoResponderID'] == '' ? 0 : $ArrayAutoResponder['AutoResponderID']), // Autoresponder ID
				($SubscriberID != '' ? $SubscriberID : 0), // Subscriber ID
				($ListID != '' ? $ListID : 0), // List ID
				1 // Is preview
			));

			// Send forward email to the friend - Start
			$Subject = sprintf($ArrayLanguageStrings['Screen']['1508'], $_POST['FormValue_SenderName']);
			$WebBrowserLink = APP_URL . 'wb.php?p=' . $EncryptedQuery;
			$AdditionalMessage = ($_POST['FormValue_Message'] != '' ? sprintf($ArrayLanguageStrings['Screen']['1509'], $_POST['FormValue_SenderName'], $_POST['FormValue_Message']) : '');
			$Message = sprintf($ArrayLanguageStrings['Screen']['1510'], $_POST['FormValue_FriendName'], $_POST['FormValue_SenderName'], $WebBrowserLink, $AdditionalMessage);

			Core::LoadObject('send_engine');
			if ($ArrayUser['GroupInformation']['SendMethod'] == 'System')
				{
				SendEngine::SetEngine(MAIL_ENGINE, SEND_METHOD);
				SendEngine::SetSendMethod();
				}
			else
				{
				SendEngine::SetEngine(MAIL_ENGINE, $ArrayUser['GroupInformation']['SendMethod']);
				SendEngine::SetSendMethod(
					true,
					$ArrayUser['GroupInformation']['SendMethodSMTPHost'],
					$ArrayUser['GroupInformation']['SendMethodSMTPPort'],
					$ArrayUser['GroupInformation']['SendMethodSMTPSecure'],
					$ArrayUser['GroupInformation']['SendMethodSMTPAuth'],
					$ArrayUser['GroupInformation']['SendMethodSMTPUsername'],
					$ArrayUser['GroupInformation']['SendMethodSMTPPassword'],
					$ArrayUser['GroupInformation']['SendMethodSMTPTimeOut'],
					$ArrayUser['GroupInformation']['SendMethodSMTPDebug'],
					$ArrayUser['GroupInformation']['SendMethodSMTPKeepAlive'],
					$ArrayUser['GroupInformation']['SendMethodLocalMTAPath'],
					$ArrayUser['GroupInformation']['SendMethodPowerMTAVMTA'],
					$ArrayUser['GroupInformation']['SendMethodPowerMTADir'],
					$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
					$ArrayUser['GroupInformation']['SendMethodSaveToDiskDir'],
					''
					);
				}
			if ($ArrayUser['GroupInformation']['XMailer'] != '')
				{
				SendEngine::$XMailer = $ArrayUser['GroupInformation']['XMailer'];
				}

			// Generate email properties - Start
			SendEngine::SetEmailProperties(
				$_POST['FormValue_SenderName'],
				$_POST['FormValue_SenderEmail'],
				$_POST['FormValue_FriendEmail'],
				$_POST['FormValue_FriendEmail'],
				$_POST['FormValue_SenderName'],
				$_POST['FormValue_SenderEmail'],
				array(),
				'Plain',
				$Subject,
				'',
				$Message,
				'',
				0,
				0,
				0,
				0,
				0,
				true
				);
			// Generate email properties - End

			// Send the email to the recipient - Start
			$ArrayResult = SendEngine::SendEmail(false);
			// Send the email to the recipient - End

			// TODO: Remove the following two lines in the v5 release
			// The following sending method has been deprecated. No longer used but will be kept as commented for a few updates more.
			// Core::SendSystemEmail($_POST['FormValue_FriendEmail'], $_POST['FormValue_SenderName'], $_POST['FormValue_SenderEmail'], $_POST['FormValue_SenderName'], $_POST['FormValue_SenderEmail'], $Subject, $Message);
			// Send forward email to the friend - End

			// Check if this user has forwarded this campaign before - Start
			if (count($ArrayCampaign) > 0) {
				$TotalForwards = Statistics::RetrieveCampaignForwardStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $ArrayCampaign['CampaignID'], ($EmailID > 0 ? $ArrayEmail['EmailID'] : 0));
			}
			elseif (count($ArrayAutoResponder) > 0)
			{
				$TotalForwards = Statistics::RetrieveAutoResponderForwardStatisticsOfSubscriber($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], $AutoResponder['AutoResponderID']);
			}
			// Check if this user has forwarded this campaign before - End

			// Track forward activity - Start
			$ArrayFieldnValues = array();

			if ($TotalForwards == 0) {
				// Update 'unique' open statistics - Start
				if (count($ArrayCampaign) > 0) {
					$ArrayFieldnValues['UniqueForwards'] = $ArrayCampaign['UniqueForwards'] + 1;
				}
				elseif (count($ArrayAutoResponder) > 0)
				{
					$ArrayFieldnValues['UniqueForwards'] = $ArrayAutoResponder['UniqueForwards'] + 1;
				}
				// Update 'unique' open statistics - End

				// Trigger auto-responders if recipient has forward for the first time - Start
				Core::LoadObject('auto_responders');
				$TotalRegisteredAutoResponders = AutoResponders::RegisterAutoResponders($ArrayList['ListID'], $ArraySubscriber['SubscriberID'], ($ArrayCampaign['RelOwnerUserID'] == '' ? $ArrayAutoResponder['RelOwnerUserID'] : $ArrayCampaign['RelOwnerUserID']), 'OnSubscriberForwardToFriend');
				// Trigger auto-responders if recipeint has forwarded for the first time - End
			}

			// Update 'total' forward statistics - Start
			if (count($ArrayCampaign) > 0) {
				$ArrayFieldnValues['TotalForwards'] = $ArrayCampaign['TotalForwards'] + 1;
				Campaigns::Update($ArrayFieldnValues, array('CampaignID' => $ArrayCampaign['CampaignID']));
			}
			elseif (count($ArrayAutoResponder) > 0)
			{
				$ArrayFieldnValues['TotalForwards'] = $ArrayAutoResponder['TotalForwards'] + 1;
				AutoResponders::Update($ArrayFieldnValues, array('AutoResponderID' => $ArrayAutoResponder['AutoResponderID']));
			}
			// Update 'total' forward statistics - End

			// Update forward statistics table - Start
			Statistics::RegisterForwardTrack($ArraySubscriber['SubscriberID'], $ArrayList['ListID'], ($ArrayCampaign['CampaignID'] != '' ? $ArrayCampaign['CampaignID'] : 0), ($ArrayCampaign['RelOwnerUserID'] != '' ? $ArrayCampaign['RelOwnerUserID'] : $ArrayAutoResponder['RelOwnerUserID']), ($ArrayAutoResponder['AutoResponderID'] != '' ? $ArrayAutoResponder['AutoResponderID'] : 0), ($EmailID > 0 ? $ArrayEmail['EmailID'] : 0));
			// Update forward statistics table - End
			// Track forward activity - End

			$ShowResultPage = true;
		}
		catch (Exception $e)
		{
			if ($e->GetMessage() != '') {
				$ObjectTemplate->ArrayErrorMessages['PageErrorMessage'] = $e->GetMessage();
			}
		}
	}
}

// EVENT: Send forward email to the friend - End

$ObjectTemplate->PageTitle = $ArrayLanguageStrings['Screen']['1512'];
$ObjectTemplate->ArrayFormFields = array(
	'PageErrorMessage' => 'TextField',
	'PageNoticeMessage' => 'TextField',
	'FriendName' => 'TextField',
	'FriendEmail' => 'TextField',
	'SenderName' => 'TextField',
	'SenderEmail' => 'TextField',
	'Message' => 'TextArea',
);

// Set header/footer - Start
if ((Users::HasPermissions(array('User.ForwardHeaderFooter.Set'), $ArrayUser['GroupInformation']['Permissions']) == true) && ($ArrayUser['ForwardToFriendHeader'] != '')) {
	$PageHeader = $ArrayUser['ForwardToFriendHeader'];
} elseif (FORWARD_TO_FRIEND_HEADER == '') {
	$PageHeader = TEMPLATE_PATH . 'desktop/public/forward_header.tpl';
} else {
	$PageHeader = FORWARD_TO_FRIEND_HEADER;
}

if ((Users::HasPermissions(array('User.ForwardHeaderFooter.Set'), $ArrayUser['GroupInformation']['Permissions']) == true) && ($ArrayUser['ForwardToFriendFooter'] != '')) {
	$PageFooter = $ArrayUser['ForwardToFriendFooter'];
} elseif (FORWARD_TO_FRIEND_FOOTER == '') {
	$PageFooter = TEMPLATE_PATH . 'desktop/public/forward_footer.tpl';
} else {
	$PageFooter = FORWARD_TO_FRIEND_FOOTER;
}
// Set header/footer - End

$HeaderTemplateString = (substr($PageHeader, 0, 7) == 'http://' || substr($PageHeader, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent($PageHeader) : file_get_contents($PageHeader));
$ObjectTemplate->AddToTemplateList($HeaderTemplateString, 'string');

$ObjectTemplate->AddToTemplateList(TEMPLATE_PATH . 'desktop/public/forward_email.tpl', 'file');

$FooterTemplateString = (substr($PageFooter, 0, 7) == 'http://' || substr($PageFooter, 0, 7) == 'https://' ? Campaigns::FetchRemoteContent($PageFooter) : file_get_contents($PageFooter));
$ObjectTemplate->AddToTemplateList($FooterTemplateString, 'string');

$ObjectTemplate->LoadTemplates();

$EncryptedQuery = '';
if (!isset($_POST['p']) || $_POST['p'] == '') {
	$ArrayQueryParameters = array(
		'CampaignID' => $ArrayCampaign['CampaignID'],
		'EmailID' => (isset($ArrayEmail['EmailID']) == true ? $ArrayEmail['EmailID'] : 0),
		'AutoResponderID' => $ArrayAutoResponder['AutoResponderID'],
		'SubscriberID' => $ArraySubscriber['SubscriberID'],
		'ListID' => $ArrayList['ListID'],
		'Preview' => ($IsPreview == true ? '1' : ''),
	);
	$EncryptedQuery = Core::EncryptURL($ArrayQueryParameters);
}

$ArrayReplaceList = array(
	'Insert:QueryParameter' => $EncryptedQuery != '' ? $EncryptedQuery : $_POST['p'],
	'Insert:SuccessMessage' => sprintf($ArrayLanguageStrings['Screen']['1511'], APP_URL . 'forward_email.php?p=' . ($EncryptedQuery != '' ? $EncryptedQuery : $_POST['p'])),
);
$ObjectTemplate->Replace($ArrayReplaceList);

// Display correct section - Start
if ($ShowResultPage == true) {
	$ObjectTemplate->RemoveBlock('SHOW:Form');
} else {
	$ObjectTemplate->RemoveBlock('SHOW:Result');
}
// Display correct section - End

$ObjectTemplate->ParseTemplate($ArrayLanguageStrings, $_POST, true);
// Page parsing - End

exit;
?>